# Owner: Lily 2017-08-21
# Last Modified: Lily 2017-8-21
# Remark: Add Two Virtual Microscopes
# import time   # add the import time to the main

labWindow = App.focusedWindow()
class AddTwoVirtualMicroscopes(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_AddTwoVirtualMicroscopes01_AddTwoVirtualCam(self):
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1)
            wait(1)
            if not exists(Pattern("VirtualThumb_AllInOne.png")):
                wait(1)
                click(Pattern("GlobalSetting_AllInOne.png"))
                wait(1)
                click(Pattern("ShowVirtuallb_AllInOne.png").targetOffset(480,0)) # to find the drop down button in global setting
                wait(1)
                click(Pattern("AlwaysVirtual_AllInOne.png"))
                wait(1)
                click(Pattern("GlobalOKButt_AllInOne.png"))
                wait(1)
            cams = findAll(Pattern("VirtualThumb_AllInOne.png")) #find all virtual cameras
            wait(1)
            count = 0 
            for i in cams:
                count = count + 1  #count the total of the cams
            wait(1)
            self.assertEqual(count, 2)
            pass

    def test_AddTwoVirtualMicroscopes02_PlayVirtualForSometime(self):
        wait(1)
        with Region(labWindow):        
            wait(1)
            localtime = time.asctime( time.localtime(time.time()) )
            Debug.user("Start play live for virtual camera %s", localtime)            
            click(findAll(Pattern("VirtualThumb_AllInOne.png")).next())

            wait(60)
            localtime = time.asctime( time.localtime(time.time()) )
            Debug.user("Do next step after play live for virtual %s", localtime)
            pass
            
    def test_AddTwoVirtualMicroscopes03_SnapForVirtualCam(self):
        wait(1)
        with Region(labWindow):        
            wait(1)
            click(find(Pattern("SnapButt_AllInOne.png")))
            count = 0
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break
            self.assertTrue(exists("ShareImageTab_AllInOne.png"), 2) # Here only consider after snapping switch to the image tab
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)
            pass            

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(AddTwoVirtualMicroscopes)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()   




            
                