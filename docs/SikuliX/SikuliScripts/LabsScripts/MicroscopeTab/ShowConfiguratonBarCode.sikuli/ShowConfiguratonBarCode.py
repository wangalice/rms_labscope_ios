# Owner: Lily 2017-08-24
# Last Modified: Lily 2017-8-24
# Remark: Show configuration bar code for the microscope

labWindow = App.focusedWindow()
class ShowConfiguratonBarCode(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ShowConfiguratonBarCode01_FindConfigurationBarCodeButton(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(find("ConfigButtMicTab.png"))
            wait(1)
            with Region(labWindow):
                wait(2)
                self.assertTrue(exists("ConfigBarCodeButt_AllInOne.png"), 2) #Check if exists the bar code button
                pass   

    def test_ShowConfiguratonBarCode02_FindModelImportConfigurationByBarCode(self):      
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Location(631, 409))
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists(Pattern("ScanningBarCodeModel_AllInOne.png")):
                count = count + 1
                wait(0.5)
                click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists(Pattern("ScanningBarCodeModel_AllInOne.png")), 2) #Check if exists the model   
            wait(1)
            click("ApplyButt_AllInOne.png")
            pass   
            
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowConfiguratonBarCode)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 





