# Owner: Lily 2017-08-16
# Last Modified: Lily 2017-8-17
# Remark: Configure compound microscope

labWindow = App.focusedWindow()
class ConfigureCompoundMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ConfigureCompoundMicroscope01_findCompoundModel(self):
       
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1)
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(1)
            click("ConfigButtLiveTab_AllInOne.png")
            wait(1)
            click(Location(631, 409))
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("CompoundModulIcon_AllInOne.PNG"):
                count = count + 1
                wait(0.5)
                click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists("CompoundModulIcon_AllInOne.PNG"), 2) #Check if exists the model compound
            pass    

    def test_ConfigureCompoundMicroscope02_FindShadingCorectionForCompound(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            click("CompoundModulIcon_AllInOne.PNG")
            wait(1)          
            self.assertTrue(exists("ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigureCompoundMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass     

    def test_ConfigureCompoundMicroscope04_SetCompoundObjectives(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("CompoundModulIcon_AllInOne.PNG")
            wait(1)
            # set objective values for the revolver       
            for i in range(1,8):
                click("CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(1)
                offsetY = i * 40
                click(Location(1085, 560 + offsetY)) #click different objectives
                wait(1)
                click("NextObjectiveButt_AllInOne.png")     #click button to select next objective position
                wait(1)
                Debug.user("Compound objective pos. %d , location X %d , location Y %d", i , 1085 , 560 + offsetY )
                wait(1)

            self.assertTrue(exists("CompundObjectives_AllInOne.png"), 2) #It is possible that the final displayed results are different in different devices
            pass                
            #click("ApplyButt_AllInOne.png")

    def test_ConfigureCompoundMicroscope05_CompoundAdvanceSetting(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("CompoundAdvanceButt_AllInOne.png") #click advanced icon            
            wait(1)
            click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0,-20))) 
            wait(1)
            self.assertTrue(exists("CamAdapterlb_AllInOne.png"), 2) #Check the exists of Camera Adapter
            wait(1)

            for i in range(1, 4):
                doubleClick(Pattern("MagFactorlb_AllInOne.png").targetOffset(142, 0))
                wait(1)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)

                wait(1)
                if i == 1:                 
                    type("0.009")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:                 
                    type("50")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("MagFactorMaxVal_AllInOne.png"), 2) #50 is the max. value here
                    wait(1)
                if i == 3:                 
                    type("51")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass
            #click("ApplyButt_AllInOne.png") #Apply the modification 


    def test_ConfigureCompoundMicroscope06_NotificationWithoutSaving(self):
        wait(1)
        with Region(labWindow):   
            match= find("ApplyButt_AllInOne.png").above().find("CloseButt.png") #click the x button without save
            wait(1)
            click(match)
            wait(1)
            self.assertTrue(exists("NotificationConfigUnsave_AllInOne.png"), 2)
            wait(1)
            click(find("SaveConfigButt_AllInOne.png"))
            wait(1)                    
            pass

    def test_ConfigureCompoundMicroscope07_ShowRevolverInLive(self):
        wait(1)
        with Region(labWindow):   
            click(Pattern("AnnoLiveButt_AllInOne.png").targetOffset(0, -60)) #click the objective button over the annotation button
            wait(1)
            self.assertTrue(exists(Pattern("RevolverLive_AllInOne.png").similar(0.50)),2) #set the similar 50 in case the different background and the objective value
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)
            pass
            


    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigureCompoundMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()