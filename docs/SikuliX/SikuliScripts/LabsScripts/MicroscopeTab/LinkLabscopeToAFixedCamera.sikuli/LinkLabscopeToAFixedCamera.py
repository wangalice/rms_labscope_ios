# Owner: Lily 2017-08-21
# Last Modified: Lily 2017-8-21
# Remark: Link Labscope To A Fixed Camera

labWindow = App.focusedWindow()
class LinkLabscopeToAFixedCamera(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_LinkLabscopeToAFixedCamera01_FindToggleForSingleMode(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(find("ConfigButtMicTab.png"))
            wait(1)
            click(Location(631, 409))
            wait(1)
            self.assertTrue(exists(Pattern("SingleModeButt_AllInOne.png")), 2) #Check if exists the toggle button for single mode   
            pass            

    def test_LinkLabscopeToAFixedCamera02_SingleModeWillHideMicroScopeTab(self):      
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Pattern("SingleModeButt_AllInOne.png"))
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)
            self.assertFalse(exists(Pattern("MicroscopeTab_AllInOne.png")), 2) #Check if the microscope tab is hidden
            wait(1)         
            pass             

    def test_LinkLabscopeToAFixedCamera03_StayInSingleModeAfterRelaunch(self):      
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Pattern("CloseAppButt_AllInOne.png"))
            wait(10)
            openApp(labscopePath)            
            count=0
            while not exists("ConfirmAppOpened.png"):
                count += 1
                wait(2)
                if exists("ConfirmAppOpened_AllInOne.png"): # try to find the icon in all in one
                    break
                # if can not find the icon for long, exit the test
                if count > 5:
                    Debug.user("icon ConfirmAppOpened was not found")
                    popError("icon ConfirmAppOpened was not found") 
                    exit(1)
            labscope.focus()
            wait(1)
            self.assertFalse(exists(Pattern("MicroscopeTab_AllInOne.png")), 2) #Check if the microscope tab is still hidden after relaunch
            wait(1)
            click("ConfigButtLiveTab_AllInOne.png")
            wait(1)
            click(Pattern("SingleModeButt_AllInOne.png"))
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)
            pass            
    

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(LinkLabscopeToAFixedCamera)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()   