# Owner: Lily 2017-08-17
# Last Modified: Lily 2017-8-17
# Remark: Configure Stereo microscope

labWindow = App.focusedWindow()
class ConfigureStereoMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ConfigureStereoMicroscope01_FindStereoModel(self):
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1)
            wait(1)
            click(find("ConfigButtMicTab.png"))
            wait(1)
            click(Location(631, 409))
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists(Pattern("StereoModulIcon_AllInOne.png")):
                count = count + 1
                wait(0.5)
                click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists(Pattern("StereoModulIcon_AllInOne.png")), 2) #Check if exists the field microscope name
            
            pass   

    def test_ConfigureStereoMicroscope02_FindShadingCorectionForPrimotech(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            click("StereoModulIcon_AllInOne.png")
            wait(1)          
            self.assertTrue(exists("ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigureStereoMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass     
        
    def test_ConfigureStereoMicroscope04_FindShadingCorectionSetUpButton(self):   
         wait(1)
         with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionSetupDisabled_AllInOne"), 2) #Check if the toggle is off
            wait(1)          
            pass 
        
    def test_ConfigureStereoMicroscope05_FindZoomClickValue(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Pattern("StereoModulIcon_AllInOne.png"))         
            wait(1)
            click(Pattern("ZoomClicklb_AllInOne.png").targetOffset(300, 0)) #click the zoom click stop list
            wait(1)
            self.assertTrue(exists(Pattern("ZoomClickList_AllInOne.png")), 2) #Check if exists the value list
            wait(1)
            pass

    def test_ConfigureStereoMicroscope06_ZoomClickValueMax12(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            if exists(Pattern("ZoomClickChecked_AllInOne.png").similar(0.89)):
                checkedList = findAll(Pattern("ZoomClickChecked_AllInOne.png").similar(0.89))
                wait(1)
                #If there are checked item, unchecked them first
                for i in checkedList:
                    click(i)
                    wait(1)
                    Debug.user("Zoom click unchecked count %s ", i)

            if exists(Pattern("ZoomClickUnChecked_AllInOne.png").similar(0.89)):
                uncheckedList = findAll(Pattern("ZoomClickUnChecked_AllInOne.png").similar(0.89))
                wait(1)
                count = 1
                for i in uncheckedList:
                    click(i)
                    wait(1)
                    Debug.user("Zoom click checked count %s ", i)
                    if count == 13:
                        self.assertTrue(exists(Pattern("NotificationZoomClick_AllInOne.png")), 2) #Check if pop up the notification, when more than 12 item are selected      
                        wait(1)
                        click(Pattern("ZoomClickOKButt_AllInOne.png"))
                        break
                    wait(1)
                    count = count + 1
                    
            pass

    def test_ConfigureStereoMicroscope07_SelectObjectiveValue(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Pattern("Objectivelb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("StereoObjectiveVal_AllInOne.png"))
            wait(1)
            self.assertTrue(exists(Pattern("StereoObjectiveSelect_AllInOne.png")), 2) #Check if the objective value set correctly  
            wait(1)   
            pass

    def test_ConfigureStereoMicroscope08_SelectEyepiecesValue(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Pattern("Eyepieceslb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("StereoEyepiecesVal_AllInOne.png"))
            wait(1)
            self.assertTrue(exists(Pattern("StereoEyepiecesSelect_AllInOne.png")), 2) #Check if the objective value set correctly  
            wait(1)  
            pass

    def test_ConfigureStereoMicroscope09_StereoAdvanceCamAdapter(self):
        wait(1)
        with Region(labWindow):       
            wait(1)
            
            click("CompoundAdvanceButt_AllInOne.png") #click advanced icon (same icon as that of compound)         
            wait(1)
            click(Pattern("CamAdapterlb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("StereoAdapterVal_AllInOne.png"))
            wait(1)
            self.assertTrue(exists(Pattern("StereoAdapterSelect_AllInOne.png")), 2) #Check if the objective value set correctly  
            wait(1)  
            pass

    def test_ConfigureStereoMicroscope10_StereoAdvanceAdditionalEyepiece(self):
        wait(1)
        with Region(labWindow):   
            wait(1)
            for i in range(1, 4):
                doubleClick(Pattern("AdditionalEyepieceslb_AllInOne.png").targetOffset(180, 0)) #click the Objective list
                wait(1)
                #Make sure that clear the textbox
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
        
                wait(1)
                if i == 1:                 
                    Debug.user("Type 0.009 for AdditionalEyepieces")
                    wait(1)
                    type("0.009")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)                    
                    self.assertTrue(exists("MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:     
                    Debug.user("Type 5 for AdditionalEyepieces")
                    wait(1)
                    type("5")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)   
                    self.assertTrue(exists("AdditionalMaxVal_AllInOne.png"), 2) #5 is the max. value here
                    wait(1)
                if i == 3:                 
                    Debug.user("Type 5.01 for AdditionalEyepieces")
                    wait(1)
                    type("5.01")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass

    def test_ConfigureStereoMicroscope11_StereoAdvanceAdditionalCamera(self):
        wait(1)
        with Region(labWindow):   
            wait(1)
            for i in range(1, 4):
                doubleClick(Pattern("AdditionalCameralb_AllInOne.png").targetOffset(180, 0)) #click the Objective list
                wait(1)
                #Make sure that clear the textbox
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
        
                wait(1)
                if i == 1:                 
                    Debug.user("Type 0.009 for AdditionalCamera")
                    wait(1)
                    type("0.009")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:                 
                    Debug.user("Type 5 for AdditionalCamera")
                    wait(1)
                    type("5")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("AdditionalMaxVal_AllInOne.png"), 2) #5 is the max. value here
                    wait(1)
                if i == 3:                 
                    Debug.user("Type 5.01 for AdditionalCamera")
                    wait(1)                    
                    type("5.01")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass
        
    def test_ConfigureStereoMicroscope12_NotificationWithoutSaving(self):
        wait(1)
        with Region(labWindow):   
            match= find("ApplyButt_AllInOne.png").above().find("CloseButt.png") #click the x button without save
            wait(1)
            click(match)
            wait(1)
            self.assertTrue(exists("NotificationConfigUnsave_AllInOne.png"), 2)
            wait(1)
            click(find("SaveConfigButt_AllInOne.png"))
            wait(1)                    
            pass   
                      
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigureStereoMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()       