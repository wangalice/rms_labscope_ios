# Owner: Lily 2017-08-15
# Last Modified: Lily 2017-8-16
# Remark: Test for microscope name change
class ChangeMicroscopeName(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ChangeMicroscopeName01_findNameChangingField(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("ConfigButtMicTab.png")
            wait(5)
            click(Location(631, 409))
            wait(5)
            self.assertTrue(exists("MicroNamelb_AllInOne.png"), 2) #Check if exists the field microscope name
            pass

    def test_ChangeMicroscopeName02_Input12Charactors(self):
        wait(1)
        with Region(App.focusedWindow()):
            #find("MicroNamelb_AllInOne.png")
            wait(1)
            enterMicroName(1)
            wait(1)
            self.assertTrue(exists("MicroName12_AllInOne.png", 2)) #The name has a max length for 12
            pass
            
    def test_ChangeMicroscopeName03_InputInvalidCharactors(self):
        wait(1)
        with Region(App.focusedWindow()):
            #find("MicroNamelb_AllInOne.png")
            wait(1)

            enterMicroName(2)
            wait(1)
            self.assertTrue(exists("MicroNameEmpty_AllInOne.png", 2)) #The name has a max length for 12
            pass


    def test_ChangeMicroscopeName04_SetBlankToMicroName(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            enterMicroName(3)
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)
            App.focusedWindow().highlight(2)    
            wait(1)
            with Region(App.focusedWindow()):
                doubleClick(Pattern("MicroNameAutoTest_AllInOne.png"))
                wait(1)
                enterMicroName(0)
                wait(1)
                click(Pattern("ApplyButt_AllInOne.png"))
                wait(1)
                self.assertTrue(exists(Pattern("MicroNameAutoTest_AllInOne.png"), 2)) #When there are more than one called AutoTest, then the result has some problem
                pass   
             
   

def enterMicroName(option):  
    doubleClick(Pattern("MicroNamelb_AllInOne.png").targetOffset(142,-2))
    wait(1)
    type(Key.BACKSPACE)
    wait(1)
    if option==1:
        type("a123456789012345")
    elif option==2:
        type("!@#$%^&*(){}<>,.?")
    elif option==3:
        type("AutoTest")
    
    wait(1)
    pass



def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")
    suit=unittest.TestLoader().loadTestsFromTestCase(ChangeMicroscopeName)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()
    
if __name__ == '__main__':
    main()






