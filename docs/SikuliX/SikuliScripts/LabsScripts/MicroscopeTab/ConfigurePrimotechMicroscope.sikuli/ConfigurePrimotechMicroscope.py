# Owner: Lily 2017-08-21
# Last Modified: Lily 2017-8-21
# Remark: Configure Primotech microscope. It is necessary to have a real camera for this test

labWindow = App.focusedWindow()
class ConfigurePrimotechMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ConfigurePrimotechMicroscope01_findPrimotechModel(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1)
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(1)
            click("ConfigButtLiveTab_AllInOne.png")
            wait(1)
            click(Location(631, 409))
            wait(1)
            with Region(labWindow):
                wait(1)            
                count=0 # Add a count , in case can not find the icon
                while not exists("PrimotechModulIcon_AllInOne.png"):
                    count = count + 1
                    wait(0.5)
                    click(find(Pattern("PageUpButtConfig_AllInOne.png").targetOffset(0, 20)))    #if not find the model, page down
                    if count > 5:
                        break
                wait(1)
                self.assertTrue(exists("PrimotechModulIcon_AllInOne.png"), 2) #Check if exists the field microscope name
                pass    

    def test_ConfigurePrimotechMicroscope02_FindShadingCorectionForPrimotech(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            click("PrimotechModulIcon_AllInOne.png")
            wait(7) #wait for the MNA loading finish            
            self.assertTrue(exists("ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigurePrimotechMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass        
        
    def test_ConfigurePrimotechMicroscope04_SetPrimotechObjectives(self):
        wait(1)
        with Region(labWindow):
            # set objective values for the revolver       
            for i in range(1,6):
                click("CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(2)
                if i == 1:
                    click("Primotech5x_AllInOne.png")
                elif i == 2:
                    click("Primotech10x_AllInOne.png")
                elif i == 3:
                    click("Primotech20x_AllInOne.png")
                elif i == 4:
                    click("Primotech50x_AllInOne.png")
                elif i == 5:
                    click("Primotech100x_AllInOne.png")                    
                    
                wait(1)
                click("NextObjectiveButt_AllInOne.png")     #click button to select next objective position
                wait(1)
                Debug.user("Primotech objective pos. %d", i  )
                wait(1)

            self.assertTrue(exists("PrimotechObjectives_AllInOne.png"), 2) #It is possible that the final displayed results are different in different devices
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)
            pass   

    def test_ConfigurePrimotechMicroscope05_ShowRevolverInLive(self):
        wait(1)
        with Region(labWindow):   
            wait(1)
            click(Pattern("AnnoLiveButt_AllInOne.png").targetOffset(0, -60)) #click the objective button over the annotation button
            wait(1)
            self.assertTrue(exists(Pattern("Scalebar200Live_AllInOne.png").similar(0.60)),2) #200um for 5x, the image is cut from the live of  wision camera        
            wait(1)
            click(Pattern("NextObjectiveLiveButt_AllInOne.png").similar(0.50)) #click to next objective
            wait(1)            
            self.assertTrue(exists(Pattern("Scalebar100Live_AllInOne.png").similar(0.60)),2) #100um for 10x, the image is cut from the live of  wision camera            
            wait(1)           
            pass
            
    def test_ConfigurePrimotechMicroscope06_SameScalebarInImageTab(self):
        wait(1)
        with Region(labWindow):        
            wait(1)
            click(find(Pattern("SnapButt_AllInOne.png")))
            count = 0
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                Debug.user("wait for the share button in image tab count %d", count  )
                if count > 10:
                    break         
            wait(1)
            self.assertTrue(exists(Pattern("Scalebar100Live_AllInOne.png").similar(0.60)),2) #image tab should have the same scale bar. 100um for 10x, the image is cut from the live of  wision camera
            wait(1)   
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)            
            pass

    
def main():

    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigurePrimotechMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()






