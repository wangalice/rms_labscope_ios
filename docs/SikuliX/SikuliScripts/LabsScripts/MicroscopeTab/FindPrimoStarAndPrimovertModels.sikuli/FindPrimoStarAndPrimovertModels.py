# Owner: Lily 2017-08-21
# Last Modified: Lily 2017-8-21
# Remark: Add PrimoStar and Primovert models

labWindow = App.focusedWindow()
class FindPrimoStarAndPrimovertModels(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_FindPrimoStarAndPrimovertModels01_FindPrimoStar(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click("ConfigButtMicTab.png") 
            wait(1)
            click(Location(631, 409))
            wait(1)
            with Region(labWindow):
                wait(1)
                count=0 # Add a count , in case can not find the icon
                while not exists("PrimoStarModulIcon_AllInOne.png"):
                    count = count + 1
                    wait(0.5)
                    click(find(Pattern("PageUpButtConfig_AllInOne.png").targetOffset(0, 20)))    #if not find the model, page down
                    if count > 5:
                        break
                wait(1)
                self.assertTrue(exists("PrimoStarModulIcon_AllInOne.png"), 2) #Check if exists the primo star model
                pass   

    def test_FindPrimoStarAndPrimovertModels02_FindShadingCorectionForPrimoStar(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            click("PrimoStarModulIcon_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)
            pass               

    def test_FindPrimoStarAndPrimovertModels03_ShadingCorectionToggleDefaultIsOff(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)
            pass    

    def test_FindPrimoStarAndPrimovertModels04_FindPrimoVert(self):      
        wait(1)
        with Region(labWindow):
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("PrimoVertModulIcon_AllInOne.png"):
                count = count + 1
                wait(0.5)
                click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0, -20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists("PrimoVertModulIcon_AllInOne.png"), 2) #Check if exists the primo vert model
            wait(1)
            pass   
        
    def test_FindPrimoStarAndPrimovertModels05_FindShadingCorectionForPrimoVert(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            click("PrimoVertModulIcon_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)
            pass  
        
    def test_FindPrimoStarAndPrimovertModels06_ShadingCorectionToggleDefaultIsOff(self):   
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(exists("ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)            
            pass            

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(FindPrimoStarAndPrimovertModels)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 