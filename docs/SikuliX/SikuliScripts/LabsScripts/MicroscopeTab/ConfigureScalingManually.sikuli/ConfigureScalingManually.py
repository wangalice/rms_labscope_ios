# Owner: Lily 2017-08-22
# Last Modified: Lily 2017-8-22
# Remark: Configure scaling manually

labWindow = App.focusedWindow()
class ConfigureScalingManually(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ConfigureScalingManually01_FindManualScalingButton(self):       
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1)
            wait(1)
            click("ConfigButtMicTab.png")
            wait(1)
            with Region(labWindow):
                # find the button for primotech
                click(Location(631, 409))
                wait(2)         
                count=0 # Add a count , in case can not find the icon
                while not exists("PrimotechModulIcon_AllInOne.png"):
                    count = count + 1
                    wait(0.5)
                    click(find(Pattern("PageUpButtConfig_AllInOne.png").targetOffset(0, 20)))    #if not find the model, page down
                    if count > 5:
                        break
                wait(1)
                click("PrimotechModulIcon_AllInOne.png")
                wait(7) #wait for the MNA loading finished
                click("CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(1)
                click("Primotech20x_AllInOne.png")     
                wait(1)
                self.assertTrue(exists("ManualScalingButt_AllInOne.png"), 2) #find the manual scaling button for primotech
                wait(1)
                
                # find the button for primotech
                click(Location(631, 409))
                wait(2)
                count=0 # Add a count , in case can not find the icon
                while not exists("CompoundModulIcon_AllInOne.PNG"):
                    count = count + 1
                    wait(0.5)
                    click(find(Pattern("PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                    if count > 5:
                        break
                wait(1)   
                click("CompoundModulIcon_AllInOne.PNG")
                wait(1) 
                click("CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(1)
                click(Location(1085, 650)) #click an objective
                wait(1)
                self.assertTrue(exists("ManualScalingButt_AllInOne.png"), 2) #find the manual scaling button for primotech
                wait(1)                
                pass    

    def test_ConfigureScalingManually02_ClickBackToGoBack(self):       
        wait(1)
        with Region(labWindow):
            click("ManualScalingButt_AllInOne.png")
            wait(1)
            click("ManualBackButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ManualScalingButt_AllInOne.png"), 2) #back to find the manual button
            wait(1)
            pass

    def test_ConfigureScalingManually03_TheDoneButtonIsDisabled(self):       
        wait(1)
        with Region(labWindow):
            click("ManualScalingButt_AllInOne.png")
            wait(3)
            with Region(labWindow):
                wait(1)
                doubleClick(Pattern("ManualScalinglb_AllInOne.png").similar(0.60).targetOffset(130,0))
                wait(1)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                
                wait(1)
                self.assertTrue(exists(Pattern("ManualDoneDisabled_AllInOne.png").similar(0.80)), 2) #The done button disabled when textbox is blank     
                wait(1)
                type("0")
                wait(1)
                self.assertTrue(exists(Pattern("ManualDoneDisabled_AllInOne.png").similar(0.80)), 2) #The done button disabled when textbox has a value 0
                pass
        

    def test_ConfigureScalingManually04_InputValueForManualScaling(self):       
        wait(1)
        with Region(labWindow):
            wait(1)
            doubleClick(Pattern("ManualScalinglb_AllInOne.png").similar(0.60).targetOffset(130,0))
            wait(1)
            type(Key.BACKSPACE)
            type("!@#$%^&*(){}<>,?") 
            wait(1) 
            type("abcdefghijklmn")
            wait(1)
            type("12345678901234")
            wait(1)
            self.assertTrue(exists("ManualScalingtxt_AllInOne.png"), 2) #symbol and alphabets can not be typed in, the max. length is 10                
            wait(1)
            pass
        
    def test_ConfigureScalingManually05_TheIconShouldBeGreen(self):       
        wait(1)
        with Region(labWindow):
            click(Pattern("ManualDone_AllInOne.png"))
            wait(1)
            # The manual scaling button will turned green after set the value
            # with a similar smaller than 90 the sikuli can not find out the difference of the color
            self.assertTrue(exists(Pattern("ManualScalingSetButt_AllInOne.png").similar(0.92)), 2)             
            wait(1)
            pass

    def test_ConfigureScalingManually06_ChangeObjectCanSetAnotherScaling(self):       
        wait(1)
        with Region(labWindow):
            click("NextObjectiveButt_AllInOne.png")     #click button to select next objective position
            wait(1)
            click("CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives           
            wait(1)
            click(Location(1085, 720)) #click an objective          
            wait(1)
            self.assertTrue(exists(Pattern("ManualScalingButt_AllInOne.png").similar(0.92)), 2)  # The manual scaling for a new objective is not set, the button is yellow    
            wait(1)
            click(Pattern("ApplyButt_AllInOne.png"))
            wait(1)        
            pass        
            
def main():

    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigureScalingManually)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()








