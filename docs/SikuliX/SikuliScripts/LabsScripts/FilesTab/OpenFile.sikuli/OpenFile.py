# Owner:Kiki 2017-08-21
# Last Modified:Kiki 2017-10-12
# Remark:Test for Open File in files tab 

class OpenFile(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_OpenFile01_OpenFileWithDoubleClick(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
             click("FilterIcon.png")
             wait(1)
             click("FilterByImage.png")
             match=find("SelectAllButt.png").above().find("CheckBox.png")            
             match.rightClick()                       
             match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
             match=type('a',KEY_CTRL)
             match=type(Key.BACKSPACE)
             type("TestOpenFileFunction")
             type('a',KEY_CTRL)
             type('c',KEY_CTRL)
             click("SaveFileName.png")
             type(Key.ESC)                          
             wait(2)
             click("SearchButt.png")
             type('v',KEY_CTRL)
             for i in range(2):
                 doubleClick(Location(346, 156))
                 wait(3)
                 self.assertTrue(exists("ChangeFileSize.png"),2)
                 pass                     
                 click("CloseFileInImageTab.png")                 
                 wait(1)
             
    def test_OpenFile02_OpenFileUseEnter(self):                                  
        with Region(App.focusedWindow()): 
             for i in range(2):
                 click(Location(353, 158))                  
                 type(Key.ENTER)                 
                 self.assertTrue(exists("ChangeFileSize.png"),2)
                 pass
                 wait(3)                 
             
             click("CloseFileInImageTab.png") 
             wait(2)
             click("SelectAllButt.png")
             wait(1)
             click("GarbageIcon.png")
             wait(1)
             click("DeleteButtInGarbage.png")
             wait(1)
             click("SearchButtPressed.png")
             wait(2)
    def test_OpenFile03_OpenAccessDeniedImage(self):
        with Region(App.focusedWindow()):
            click("SearchButt.png")
            wait(1)
            type("TempFolder")
            wait(1)
            click("FolderIconInFilesTab.png")
            type(Key.ENTER)
            wait(1)
            click("SearchButtPressed.png")
            wait(1)
            click("SearchButt.png")
            type("AccessDeniedImage")
            click("CheckBox.png")
            type(Key.ENTER)
            self.assertTrue(exists("AlertMessageAboutOpenAccessDeniedFile.png"),2)
            pass              
            EnterFilesTab(self)
    def test_OpenFile04_ChangeNameOfAccessDeniedImage(self):
            wait(2)
            rightClick("CheckBox.png")
            match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
            type("test")
            click("SaveFileName.png")
            self.assertTrue(exists("PopUpMessageWhenChangeAccessDeniedImageName.png"),2)            
            pass
            wait(2)
            type(Key.ENTER)
            click("SearchButtPressed.png")
            wait(1)
            
            
    def test_OpenFile05_OpenUnsupportImage(self):                
        with Region(App.focusedWindow()):
            click("SearchButt.png")
            wait(1)
            type("UnsupportImage")
            wait(1)
            click("CheckBox.png")
            type(Key.ENTER)
            self.assertTrue(exists("AlertMessageAboutOpenUnsupportImage.png"),2)
            pass
            click("CloseFileInImageTab.png")
            wait(1)
            click("SearchButtPressed.png")
            wait(2)
            click("BackButtInFolder.png")
            EnterMicTab(self)
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)  


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OpenFile)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 