# Owner:Kiki 2017-08-17
# Last Modified:Kiki 2017-10-13
# Remark:Test for Show Explorer butt in files tab

class ShowExplorerButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_showExplorerButtonInFilesTab(self):                  
        EnterFilesTab(self)
        wait(1)
        self.assertTrue(exists("OpenInExplorerButt.png"),FOREVER)
        pass        
            


def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)   
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowExplorerButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()