# Owner:Kiki 2017-08-16
# Last Modified:Kiki 2017-10-13
# Remark:Test for Copy butt in files tab

class ShowMetadata(unittest.TestCase):
    def setUp(self):              
        wait(1)    
    def tearDown(self):
        wait(1)
    def test_ShowMetadata_DismissByESC(self):       
        EnterFilesTab(self)
        wait(2)
        rightClick("CheckBox.png")
        self.assertTrue(exists("PopUpOfMetadata.png",FOREVER))
        pass    
        wait(5)        
        type(Key.ESC)               
        EnterMicTab(self)
         
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)     
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowMetadata)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()
    