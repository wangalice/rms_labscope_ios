# Owner:Kiki 2017-08-17
# Last Modified:Kiki 2017-10-13
# Remark:Test for Share butt in files tab

class ShareButt(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)        
    def test_ShareButt01_ClickSelectAllButt(self):                          
        wait(1)
        EnterFilesTab(self)
        click("FilterIcon.png")
        wait(1)
        click("FilterByImage.png") 
        with Region(App.focusedWindow()):            
            click("SelectAllButt.png")  
            self.assertTrue(exists("ShareButt.png"),FOREVER)
            pass                                
            ClickShareButt(self)                 
            wait(1)              
                                   
    
    def test_ShareButt02_ClickCheckBox(self):                
        with Region(App.focusedWindow()):
            try:
                match=find("SelectAllButt.png").above().find("CheckBox.png")
                match.click()                
                wait(1)
                self.assertTrue(exists("ShareButt.png"),FOREVER)           
                pass  
                ClickShareButt(self)              
                EnterMicTab(self)
            except FindFailed:
                Debug.user("Can't find share button")
    def test_ShareButt03_NoShareButtWithNoFilesSelected(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("TheFunctionBarWhenSelectFolder.png"),FOREVER)
            pass
            EnterMicTab(self)

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)            
def ClickShareButt(self):
    click("ShareButt.png")
    wait(1)
    self.assertTrue(exists("PopUpAboutShare.png",FOREVER))
    pass
    click("SaveAsButt.png")
    wait(20)
    type(Key.ESC)
    
    
    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShareButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()
                
     
         
                    
          
            
            
        