# Owner:Kiki 2017-08-18
# Last Modified:Kiki 2017-10-12
# Remark:Test for Garbage icon in files tab

class GarbageIcon(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_GarbageIcon01_ShowGarbageIconWhenClickSelectAllButt(self):                  
        EnterFilesTab(self)        
        wait(1)
        with Region(App.focusedWindow()):            
             click("SelectAllButt.png")
             self.assertTrue(exists("GarbageIcon.png"),2)
             pass
    def test_GarbageIcon02_PopoverDeletePanelWhenClickGarbageIcon_WithClickSelectAllButt(self):
        wait(2)
        click("GarbageIcon.png")
        self.assertTrue(exists("DeleteButtInGarbage.png"),2)
        pass
        EnterMicTab(self)
            
    def test_GarbageIcon03_ShowGarbageIconWhenClickCheckBox(self):
        EnterFilesTab(self)
        wait(1)                     
        click("CheckBox.png")
        self.assertTrue(exists("GarbageIcon.png"),2)
        pass
        wait(2)
    def test_GarbageIcon04_PopoverDeletePanelWhenClickGarbageIcon_WithClickCheckBox(self):                                                                  
        click("GarbageIcon.png")
        wait(1)
        self.assertTrue(exists("DeleteButtInGarbage.png"),2)
        pass
        EnterMicTab(self)
    def test_GarbageIcon05_SelectedImageWillBeDeleteWhenClickDeleteOption(self):
        EnterFilesTab(self)
        find("CheckBox.png").rightClick()
        match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
        match=type('a',KEY_CTRL)
        type(Key.BACKSPACE)
        type("TestGarbageFunction")
        click("SaveFileName.png")
        wait(1)
        click("SearchButt.png")
        type("TestGarbageFunction")
        wait(1)
        click("CheckBox.png")
        wait(1)
        click("GarbageIcon.png")
        wait(1)
        click("DeleteButtInGarbage.png")
        self.assertTrue(exists("DeletedSpecialFileInFilesTab.png"),2)
        pass
        click("SearchButtPressed.png")                                                     
        wait(2)
        EnterMicTab(self)
    def test_GarbageIcon06_PopUpErrorMessageWhenDeleteAccessDeniedImage(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):            
             click("SearchButt.png")
             wait(1)
             type("TempFolder")
             wait(1)
             click("FolderIconInFilesTab.png")
             type(Key.ENTER)
             wait(1)
             click("SearchButtPressed.png")
             wait(1)
             click("SearchButt.png")
             type("AccessDeniedImage")
             click("SelectAllButt.png")
             wait(1)            
             click("GarbageIcon.png")
             wait(1)
             click("DeleteButtInGarbage.png")
             self.assertTrue(exists("PopUpOfDeleteAccessDeniedImage.png"),2)            
             pass
             type(Key.ESC)
             wait(1)
             click("SearchButtPressed.png")
             wait(1)
             click("BackButtInFolder.png")
             EnterMicTab(self)
            
            
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)  


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(GarbageIcon)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()       