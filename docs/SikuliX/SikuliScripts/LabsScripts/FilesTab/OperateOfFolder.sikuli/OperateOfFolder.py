# Owner:Kiki 2017-08-21
# Last Modified:Kiki 2017-10-13
# Remark:Test for Operate of folder in files tab
class OperateOfFolder(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_OperateOfFolder01_OpenFolderByDoubleClick(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            ShowFolderInTop(self)                                    
            match=find("FolderIconInFilesTab.png").right().find("FolderLine.png")
            wait(2)            
            match.doubleClick()
            self.assertTrue(exists("BackButtInFolder.png"),2)
            pass
            click("BackButtInFolder.png")
            wait(2)  
           
    def test_OperateOfFolder02_UseEnterAndBackspace(self):
        match=find("FolderIconInFilesTab.png").right().find("FolderLine.png")
        match.click()        
        wait(2)
        type(Key.ENTER)
        self.assertTrue(exists("BackButtInFolder.png"),2)
        wait(2) 
        type(Key.BACKSPACE)
        wait(2)
    def test_OperateOfFolder03_NoMoveCopyDeleteShareButt(self):
        match=find("FolderIconInFilesTab.png").right().find("FolderLine.png")
        match.click()
        self.assertTrue(exists("TheFunctionBarWhenSelectFolder.png"),2)
        pass
        wait(2)
    def test_OperateOfFolder04_NoCheckedBoxWhenSelectFolder(self): 
        click("SelectAllButt.png")
        self.assertTrue(exists("FolderIconInFilesTab.png"),2)
        wait(2)
        EnterMicTab(self)
def ShowFolderInTop(self):
    click("SortingOrder.png")
    wait(1)
    click("SortByNameDscending.png")
    wait(2)
    click("SortingOrder.png")
    wait(1)
    click("SortByNameAscending.png")

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)  


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OperateOfFolder)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()    