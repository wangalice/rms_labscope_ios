# Owner:Kiki 2017-08-16
# Last Modified:Kiki 2017-11-1
# Remark:Test for Copy butt in files tab


class CopyButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
      
    def test_CopyButt01_ShowCopyButtWhenClickSelectAllButt(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            try:
                click("SelectAllButt.png")
                self.assertTrue(exists("CopyButt.png"),2)
                pass                
                ClickCopyButt(self)                
                EnterMicTab(self)
            except FindFailed:
                Debug.user("Can't find copy button")            
    
    def test_CopyButt02_ShowCopyButtWhenClickCheckBox(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            try:
                click("CheckBox.png")
                self.assertTrue(exists("CopyButt.png"),2)
                pass
                ClickCopyButt(self)              
                EnterMicTab(self)
            except FindFailed:
                Debug.user("Can't find copy button")
    
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)            
def ClickCopyButt(self):
    click("CopyButt.png")
    wait(1)
    self.assertTrue(exists("PopUpCopy.png",FOREVER))
    pass
    type(Key.ESC)
    
    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(CopyButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()     
                
     
         
                    
          
            
            
        