# Owner:Kiki 2017-08-18
# Last Modified:Kiki 2017-10-12
# Remark:Test for Change file's name in files tab

class ChangeFileName(unittest.TestCase):
    def setUp(self):      
        wait(1)        
    def tearDown(self):
        wait(1)
    def test_ChangeFileName01_ChangeTwoFilesWithSameName(self):
        EnterFilesTab(self)        
        click("FilterIcon.png")
        wait(1) 
        click("FilterByImage.png")
        wait(1) 
        click("SortingOrder.png")
        wait(1) 
        click("SortByNameAscending.png")
        wait(1) 
        click("SearchButt.png")
        type('.czi')
        ShowMetadata(self)
        match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
        type('a',KEY_CTRL)
        type(Key.BACKSPACE)
        wait(2)
        type('ChangeFileName')
        click("SaveFileName.png")
        type(Key.ESC)
        click("SortingOrder.png")
        click("SortByNameDscending.png") #Change the sort and choice different file in second time
        ShowMetadata(self)
        match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
        match=type('a',KEY_CTRL)
        type(Key.BACKSPACE)
        type('ChangeFileName')
        click("SaveFileName.png")
        self.assertTrue(exists("PopUpAboutSameFileName.png",2))
        pass
        click("OkToPopUpAboutSameFileName.png")                    
        wait(2)
        click("SearchButtPressed.png")
        wait(1)
        click("SearchButt.png")
        type('ChangeFileName')
        wait(1)
        click("SelectAllButt.png")
        wait(1)
        click("GarbageIcon.png")
        wait(1)
        click("DeleteButtInGarbage.png")
        wait(1)
        click("SearchButtPressed.png")
        EnterMicTab(self)
    def test_ChangeFileName02_TypeInvalidName(self):        
        ShowMetadata(self)
        match=find("PopUpOfMetadata.png").below().find("FilesNameTextBox.png").click()
        match=type('a',KEY_CTRL)
        type(Key.BACKSPACE)
        type('\/:*<>"|?')
        wait(3)
        self.assertTrue(exists("EmptyFilesName.png", 2)) 
        pass        
        EnterMicTab(self)
def ShowMetadata(self):            
    EnterFilesTab(self)
    find("CheckBox.png").rightClick()
    self.assertTrue(exists("PopUpOfMetadata.png",FOREVER))
    pass  
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)   
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ChangeFileName)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()     
              