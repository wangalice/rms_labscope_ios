# Owner:Kiki 2017-08-21
# Last Modified:Kiki 2017-10-13
# Remark:Test for SelectButt in files tab
class SelectButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_SelectButt01_ShowCheckButtForImage(self):
        EnterFilesTab(self)
        wait(1)
        self.assertTrue(exists("CheckBox.png"),2)
        pass
    def test_SelectButt02_SelectedImageWillBeDeselectedIfSwitchTab(self):                            
        click("SelectAllButt.png")                                     
        EnterMicTab(self)
        wait(2)
        EnterFilesTab(self)
        self.assertTrue(exists("CheckBox.png"),2)
        pass     
        wait(2)
        EnterMicTab(self)

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)  
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SelectButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()