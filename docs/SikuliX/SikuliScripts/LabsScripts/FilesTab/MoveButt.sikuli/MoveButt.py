# Owner:Kiki 2017-08-17
# Last Modified:Kiki 2017-10-12
# Remark:Test for Move button in files tab

class MoveButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
      
    def test_MoveButt01_ShowMoveButtWhenClickSelectAllButt(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            try:
                click("SelectAllButt.png") 
                self.assertTrue(exists("MoveButt.png",FOREVER))
                pass                                
                ClickMoveButt(self)                
                EnterMicTab(self)
            except FindFailed:
                Debug.user("Can't find move button")            
    
    def test_MoveButt02_ShowMoveButtWhenClickCheckBox(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            try:
                click("CheckBox.png")
                self.assertTrue(exists("MoveButt.png",FOREVER))
                pass 
                ClickMoveButt(self)              
                EnterMicTab(self)
            except FindFailed:
                Debug.user("Can't find move button")

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)            
def ClickMoveButt(self):
    click("MoveButt.png")
    wait(1)
    self.assertTrue(exists("PopUpCopy.png",FOREVER))
    pass
    type(Key.ESC)

    
    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(MoveButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()             