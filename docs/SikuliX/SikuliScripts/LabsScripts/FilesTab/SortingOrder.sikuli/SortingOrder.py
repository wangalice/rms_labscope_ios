# Owner:Kiki 2017-08-22
# Last Modified:Kiki 2017-10-13
# Remark:Test for Sorting Order in files tab

class SortingOrder(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_SortingOrder01_ShowFourKindsAndEnsureSelected(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):  
            click("SortingOrder.png")
            click("SortByNameAscending.png")
            wait(2)
            click("SortingOrder.png")
            wait(1)
            self.assertTrue(exists("KindsOfSortingOrder.png",2))
            pass 
    def test_SortingOrder02_HighlightCurrentlySelectedSortingOrder_SortByNameDescending(self):    
            wait(2)   
            click("SortByNameDscending.png")
            wait(2)
            click("SortingOrder.png")
            self.assertTrue(exists("SortByNameDscendingSelected.png",2))
            pass
    def test_SortingOrder03_HighlightCurrentlySelectedSortingOrder_SortByDateAscending(self):        
            wait(2)
            click("SortByDateAscending.png")
            wait(2)
            click("SortingOrder.png")
            self.assertTrue(exists("SortByDateAscendingSelected.png",2))
            pass
    def test_SortingOrder04_HighlightCurrentlySelectedSortingOrder_SortByDateDescending(self):
            wait(2)
            click("SortByDateDescending.png")
            wait(2)
            click("SortingOrder.png")
            self.assertTrue(exists("SortByDateDscendingSelected.png",2))
            pass
            EnterMicTab(self)
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)  
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SortingOrder)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 
           