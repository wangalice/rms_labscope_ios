# Owner: Lily 2017-08-25
# Last Modified: Lily 2017-8-25
# Remark: Full Screen Mode In Live

labWindow = App.focusedWindow()
class FullScreenModeInLive(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_FullScreenModeInLive01_EnterFullScreenMode(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click("DisplaySettingIcon_AllInOne.png")
            wait(1)
            click("DSFullScreenIcon_AllInOne.png")
            wait(1)
            self.assertTrue(waitVanish("ZeissLogo_AllInOne.png")) #After enter the full screen mode, the zeiss icon will be disappeared
            pass

    def test_FullScreenModeInLive02_WaitForDisplaySettingIconDisappear(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(waitVanish("DisplaySettingIcon_AllInOne.png",5)) #The display setting icon will be disappeared 
            pass

    def test_FullScreenModeInLive03_WaitForSnapButtonDisappear(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertFalse(exists("SnapButt_AllInOne.png")) #The snap icon will be disappeared
            pass

    def test_FullScreenModeInLive04_MoveMouseTheIconWillBeDisplayed(self):        
        wait(1)
        with Region(labWindow):
            wait(1)
            hover(Location(1004, 524)) # Move the mouse
            wait(1)
            self.assertTrue(exists("DisplaySettingIcon_AllInOne.png"), 2) #Check if exists the display setting icon     
            pass 

    def test_FullScreenModeInLive05_SnapWillExitFullScreenMode(self):        
        with Region(labWindow):
            click("SnapButt_AllInOne.png")  #After move the mouse the snap button will be displayed as well
            wait(5)
            with Region(labWindow):
                count=0
                while not exists("ZeissLogo_AllInOne.png"): 
                    count += 1
                    wait(2)
                    if count > 5:
                        break
                self.assertTrue(exists("ZeissLogo_AllInOne.png"), 2) #Check if exists the display setting icon        
                wait(1)
                click(Pattern("MicroscopeTab_AllInOne.png")) #Go back to microscope tab
                wait(1)
                pass 
   
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(FullScreenModeInLive)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











