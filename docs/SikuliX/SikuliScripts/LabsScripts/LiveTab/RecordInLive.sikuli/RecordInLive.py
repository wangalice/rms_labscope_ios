# Owner: Lily 2017-08-28
# Last Modified: Lily 2017-8-28
# Remark: Record In Live

labWindow = App.focusedWindow()
class RecordInLive(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_RecordInLive01_RecordIconInDropDownList(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png").targetOffset(0, 45)) # Click the arrow under the snap button to show drop down list
            wait(1)
            self.assertTrue(exists("RecordDropDownButt_AllInOne.png")) # Find the record icon in drop down list
            pass

    def test_RecordInLive02_RecordInFullScreenMode(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("RecordDropDownButt_AllInOne.png")
            wait(1)
            click("RecordButt_AllInOne.png")
            wait(1)
            hover(Location(1036, 454))
            wait(1)
            self.assertFalse(exists("ZeissLogo_AllInOne.png")) #Record is in full screen mode
            pass

    def test_RecordInLive03_RecordForMoreThanAnHour(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            localtime = time.asctime( time.localtime(time.time()) )
            Debug.user("Start record %s", localtime)        
            wait(36) #Record for an hour
            self.assertTrue(exists("RecordStopButt_AllInOne.png")) #The record is still continue
            pass
        
    def test_RecordInLive04_PressEscToExitRecord(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            type(Key.ESC)
            wait(1)
            localtime = time.asctime( time.localtime(time.time()) )
            Debug.user("End record %s", localtime)        
            wait(1)
            self.assertTrue(waitVanish("RecordStopButt_AllInOne.png")) #The record will be stopped
            wait(1)
            click(Pattern("RecordButt_AllInOne.png").targetOffset(0, 45)) # Click the arrow under the snap button to show drop down list
            wait(1)
            click("SnapDropDownButt_AllInOne.png")
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(RecordInLive)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











