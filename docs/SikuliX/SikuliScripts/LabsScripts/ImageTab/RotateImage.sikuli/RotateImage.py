# Owner: Lily 2017-09-12
# Last Modified: Lily 2017-09-12
# Remark: rotate in image tab

labWindow = App.focusedWindow()
class RotateImage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_RotateImage01_AddRotateButton(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png"))
            count = 0
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break
            
            wait(1)
            click("AnnoLiveButt_AllInOne.png")
            wait(1)
            click("AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1000, 342),Location(1350, 608))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Rotate image\n" + currTime)
            wait(1)
            click(Location(40, 679))
            wait(1)
            click("AnnoConfirm_AllInOne.png")
            wait(2)            
            click("ImageProcessButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("RotateButt_AllInOne.png")) #Show rotate button        
            pass
        
    def test_RotateImage02_ShowWarningIfImageWithAnnotations(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("RotateButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ProcessWarning_AllInOne.png")) #show warning message
            pass


    def test_RotateImage03_ExistType90CCW(self):
        wait(1)
        with Region(labWindow):
            labWindow.highlight(1)             
            wait(1)
            click("ImageProcessWarningOKButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("Rotate90CCW_AllInOne.png")) #Preview 90 CCW
            pass

    def test_RotateImage04_ExistType180(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(exists("Rotate180_AllInOne.png")) #Preview 180
            pass
        
    def test_RotateImage05_ExistType90CW(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(exists("Rotate90CW_AllInOne.png")) #Preview 90CW
            pass
        
    def test_RotateImage06_ShowUndoRedoButton(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Location(1416, 519)) # choose rotate 180
            wait(1)
            click("AnnoConfirm_AllInOne.png") 
            wait(1)
            self.assertTrue(exists("UndoRedoButt_AllInOne.png")) #Show UndoRedo button
            wait(1)
            click(Pattern("MicroscopeTab.png")) # Go to microscope tab
            wait(1)
            click("SaveConfigButt_AllInOne.png")
            wait(1)
            click("NotRememberPIN_AllInOne.png")
            wait(1)
            pass

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(RotateImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











