# Owner: Lily 2017-09-18
# Last Modified: Lily 2017-09-18
# Remark: Display settings

labWindow = App.focusedWindow()
class DisplaySettingsInImageTab(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_DisplaySettingsInImageTab01_FindDisplayCurve(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break      
            wait(1) 
            click("DisplaySettingIcon_AllInOne.png")
            with Region(labWindow):
                wait(2)
                self.assertTrue(exists("DSDisplayCurveIcon_AllInOne.png"), 2) #Check if exists the icon for display curve
                pass   

    def test_DisplaySettingsInImageTab02_FindOverExposure(self):      
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("DSOverexposureIcon_AllInOne.png"), 2) #Check if exists the icon for overexposure
            pass 


    def test_DisplaySettingsInImageTab03_FindDrawingTube(self):      
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("DSDrawingTubeIcon_AllInOne.png"), 2) #Check if exists the icon for drawing tube
            pass 

    def test_DisplaySettingsInImageTab04_FindLaserPoint(self):      
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("DSLaserPointIcon_AllInOne.png"), 2) #Check if exists the icon for laser point
            pass 

    def test_DisplaySettingsInImageTab05_FindSplitView(self):      
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("DSSplitViewIcon_AllInOne.png"), 2) #Check if exists the icon for split view
            pass 

    def test_DisplaySettingsInImageTab06_FindFullScreen(self):      
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("DSFullScreenIcon_AllInOne.png"), 2) #Check if exists the icon for full screen
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png")) #Go back to microscope tab
            wait(1)
            pass 
 
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(DisplaySettingsInImageTab)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 









