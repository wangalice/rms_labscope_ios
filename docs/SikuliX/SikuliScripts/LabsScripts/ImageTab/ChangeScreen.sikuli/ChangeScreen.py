# Owner:Kiki 2017-09-06
# Last Modified:Kiki 2017-10-17
# Remark:Test for Change screen mode by F11 in image tab


class ChangeScreenByF11(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_ChangeScreen01_ByF11(self):                  
        EnterImageTab(self)
        wait(1)
        with Region(App.focusedWindow()): 
            type(Key.F11)           
            wait(2)                            
            if not exists("PencilIconForConfigFileNameInImageTab.png"):
                pass
            wait(8)
            if not exists("DisplayMenuSetting.png"):  
                pass
            type(Key.F11)
            wait(2)
            self.assertTrue(exists("ShareButt.png"),FOREVER)
            pass                        
            
    def test_ChangeScreen02_F11DisabledInSplitMode(self): 
        
        wait(1)
        with Region(App.focusedWindow()): 
            click("DisplayMenuSetting.png")
            wait(2)
            click("SplitMode.png")
            wait(5)            
            click(Location(1177, 265))
            type(Key.ENTER)
            wait(3)
            type(Key.F11)            
            wait(3)
            self.assertTrue(exists("SnapAndLockIcon.png"))
            wait(2)
            click("CloseFileInImageTab.png")
            wait(3)
            click("DisplayMenuSetting.png")
            wait(3)
            click("SplitMode.png")
            
    def test_ChangeScreen03_OnlyImageCanFullScreen(self):
        click("FilesTab-2.png")
        wait(2)
        click("FilterIcon.png")
        wait(2)
        click("FilterByReport.png")
        wait(2)
        click("SearchButt.png")
        type(".csv")
        doubleClick(Location(324, 158))        
        wait(3)        
        type(Key.F11)
        self.assertTrue(exists("CloseCSVFilesIcon.png"),FOREVER)
        pass
        wait(3)
        #if exists("CloseFilesIcon_Win7.64.png"):
        click("CloseCSVFilesIcon.png")
        wait(2)
        click("SearchButtPressed.png")
        wait(2)
        click("FilterIcon-1.png")
        wait(2)
        click("FilterByImage.png")
        EnterMicTab(self)
            

def EnterImageTab(self):
    wait(1)
    click(Location(217, 118))
    wait(3)
    click("SnapButt.png")
    wait(5)       
    self.assertTrue(exists("ShareButt_Win7.64.png"),FOREVER)
    pass
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab-1.png")
    wait(1)  


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ChangeScreenByF11)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()              