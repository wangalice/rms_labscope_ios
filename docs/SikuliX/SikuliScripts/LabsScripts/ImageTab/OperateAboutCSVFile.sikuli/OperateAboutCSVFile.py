# Owner:Kiki 2017-09-12
# Last Modified:Kiki 2017-10-20
# Remark:Test for CSV file


class OperateAboutCSVFile(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)   
    def test_CSVFile01_AddCSVButt(self):                  
        wait(1)
        with Region(App.focusedWindow()):
            EnterFilesTab(self)
            click("FilterIcon.png")
            wait(2)
            click("FilterByImage.png")
            wait(2)
            click("SearchButt.png")
            wait(2)
            type("TempFolder")
            click("FolderIconInFilesTab.png")
            type(Key.ENTER)
            wait(2)
            click("SearchButtPressed.png")
            wait(2)
            click("SearchButt.png")
            type("NoAnnotation")
            doubleClick(Location(287, 124))            
            wait(2)
            click("ReportButt.png")
            self.assertTrue(exists("PopUpOfReportButt.png"),FOREVER)
            pass
    def test_CSVFile02_GenerateCSVWithNoAnnotation(self):
        wait(2)
        click("CSVButt.png")
        wait(2)
        self.assertTrue(exists("PopUpOfNoMeasurement.png"),FOREVER)
        pass
        click("OkToPopUpAboutSameFileName.png")
    def test_CSVFile03_GenerateCSVWithAnnotationsAndAutoSaved(self):
        EnterFilesTab(self)
        click("SearchButtPressed.png")
        wait(2)
        click("SearchButt.png")
        type("AllAnnotations")
        doubleClick(Location(288, 121))
        wait(2)
        click("ReportButt.png")       
        wait(2)
        click("CSVButt.png")
        wait(2)
        click("CloseCSVFilesIcon.png")
        EnterFilesTab(self)
        click("FilterIcon.png")
        wait(2)
        click("FilterByReport.png")        
        doubleClick(Location(286, 124))
        wait(3)
        self.assertTrue(exists("CSVFileInFilesTab.png"),FOREVER)
        pass
        click("CloseCSVFilesIcon.png")
        wait(2)
        click(Location(291, 121))   #select AllAnnotations.CSV file
        click("GarbageIcon.png")
        wait(2)
        click("DeleteButtInGarbage.png")
        wait(2)
        click("SearchButtPressed.png")
        click("FilterIcon.png")
        wait(2)
        click("FilterByImage.png")
        wait(2)
        click("BackButtInFolder.png")
        EnterMicTab(self)      


def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OperateAboutCSVFile)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 