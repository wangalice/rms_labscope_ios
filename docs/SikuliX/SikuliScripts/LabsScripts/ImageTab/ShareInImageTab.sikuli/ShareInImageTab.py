# Owner: Lily 2017-09-07
# Last Modified: Lily 2017-09-07
# Remark: Share In Image Tab

labWindow = App.focusedWindow()
class ShareInImageTab(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ShareInImageTab01_ShowShareButtonAfterSnap(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break            
            self.assertTrue(exists("ShareImageTab_AllInOne.png", 5)) #Show share button
            pass

    def test_ShareInImageTab02_ShowShareButtonAfterOpenAnImage(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("FilesTab_AllInOne.png")
            wait(1)
            doubleClick("CZIFormatImage_AllInOne.png")
            wait(3)            
            self.assertTrue(exists("ShareImageTab_AllInOne.png")) #Show share button       
            pass

    def test_ShareInImageTab03_ShowShareMenu(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("ShareImageTab_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ShareMenuImageTab_AllInOne.png")) #Show the share menu， also with the mail option to share
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)            
            pass
        
   
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShareInImageTab)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











