# Owner: Lily 2017-09-05
# Last Modified: Lily 2017-09-05
# Remark: Image processing

labWindow = App.focusedWindow()
class ImageProcessing(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ImageProcessing01_ShowShareButtonAfterOpenTheImage(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("FilesTab_AllInOne.png")) #select the files tab
            wait(3)
            doubleClick("CZIFormatImage_AllInOne.png") # Click to open the image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break   
            self.assertTrue(exists("ShareImageTab_AllInOne.png")) # Show share button when open the image
            pass                

    def test_ImageProcessing02_ShowImageProcessingPanel(self):      
        wait(1)
        with Region(labWindow):   
            wait(1)
            click("ImageProcessButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("ImagePorcessPanel_AllInOne.png")) # Show image process panel
            pass

    def test_ImageProcessing03_MoveImageProcessingSliders(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            thumbs = findAll("ImageProcessSliderButt_AllInOne.png")
            for t in list(thumbs):
                dragDrop(t, t.getCenter().right(100))
            wait(1)
            click("ConfirmFunctionbarButt_AllInOne.png")
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab        
            wait(1)
            click("SaveConfigButt_AllInOne.png")
            wait(1)
            click("NotRememberPIN_AllInOne.png")
            wait(1)
            pass


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ImageProcessing)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











