# Owner:Kiki 2017-09-12
# Last Modified:Kiki 2017-11-1
# Remark:Test for cropping function in image tab


class CroppingFunction(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)                   
    def test_CroppingFunction01_DisplayThreeCropShapes(self):                  
        wait(1)
        with Region(App.focusedWindow()):
            EnterFilesTab(self)
            click("SearchButt.png")
            type("TempFolder")
            wait(1)
            click("FolderIconInFilesTab.png")           
            type(Key.ENTER)
            wait(1)
            click("SearchButtPressed.png")
            wait(2)
            click("SearchButt.png")
            type("AllAnnotations")
            doubleClick(Location(293, 117))
            wait(2)
            click("ImageProcessing.png")
            self.assertTrue(exists("CropShape.png"),FOREVER)
            pass
    def test_CroppingFunction02_PopUpMessageWhenUseRectangleCropWithAnnotations(self):
        click("RectangleCropping.png")
        self.assertTrue(exists("PopUpMessageWhenCropWithAnnotations.png"),FOREVER)
        pass
        type(Key.ESC)
        wait(2)
    def test_CroppingFunction03_PopUpMessageWhenUseTriangleCropWithAnnotations(self):   
        click("TriangleCropping.png")
        self.assertTrue(exists("PopUpMessageWhenCropWithAnnotations.png"),FOREVER)
        pass
        type(Key.ESC)
        wait(2)
    def test_CroppingFunction04_PopUpMessageWhenUseCricleCropWithAnnotations(self):
        click("CricleCropping.png")
        self.assertTrue(exists("PopUpMessageWhenCropWithAnnotations.png"),FOREVER)
        pass
        type(Key.ESC)
        wait(2)    
        click("CancleIcon.png")
        EnterFilesTab(self)        
        click("SearchButtPressed.png")
        wait(2)                
        click("BackButtInFolder.png")        
        EnterMicTab(self)
        
        

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(CroppingFunction)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 
