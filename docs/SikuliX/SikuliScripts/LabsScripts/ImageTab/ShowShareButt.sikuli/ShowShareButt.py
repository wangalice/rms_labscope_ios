# Owner:Kiki 2017-09-08
# Last Modified:Kiki 2017-10-17
# Remark:Test for open file in file browser


class ShowShareButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_ShowShareButt01_ShowShareButt(self):                  
        wait(1)
        EnterFilesTab(self)
        with Region(App.focusedWindow()):
            click("CheckBox.png")
            type(Key.ENTER)
            wait(3)            
            click("ShareButt.png")
            wait(3)
            self.assertTrue(exists("ShareMenuInImageTab.png"),FOREVER)
            pass
            EnterMicTab(self)
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowShareButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 