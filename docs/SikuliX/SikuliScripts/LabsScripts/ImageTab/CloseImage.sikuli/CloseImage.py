# Owner:Kiki 2017-09-08
# Last Modified:Kiki 2017-10-17
# Remark:Test for close image in image tab


class CloseImage(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_CloseImage01_CloseImageInImageTabAndReturnToFilesTab(self):                  
        wait(1)
        EnterFilesTab(self)
        with Region(App.focusedWindow()):
            click("CheckBox.png")
            type(Key.ENTER)
            wait(3)            
            click("CloseFileInImageTab.png")
            self.assertTrue(exists("SelectAllButt.png"),FOREVER)
            pass            
            
    def test_CloseImage02_CloseImageInImageTabAndReturnToLiveTab(self):                  
        wait(1)
        EnterMicTab(self)
        click(Location(207, 124))
        wait(5)
        click("SnapButt.png")
        wait(6)                         
        click("CloseFileInImageTab.png")
        self.assertTrue(exists("SnapButt.png"),FOREVER)
        pass
      
    def test_CloseImage03_CloseImageInEditMode(self): 
        click("SnapButt.png")
        wait(5)                 
        click("AnnotationButtInLiveTab.png")
        wait(2)
        click("ArrowAnnotation.png")
        wait(2)
        click("DoneButtInEditMode.png")
        wait(2)        
        click("CloseFileInImageTab.png")
        self.assertTrue(exists("PopUpSaveInEditMode.png"),FOREVER)
        pass
        click("CloseWithoutSave.png")
        self.assertTrue(exists("SnapButt.png"),FOREVER)
        pass
        EnterMicTab(self)
               

def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    self.assertTrue(exists("SortingOrder.png"),FOREVER)
    pass
def EnterMicTab(self):
    wait(1)
    click("MicroscopeTab.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(CloseImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 