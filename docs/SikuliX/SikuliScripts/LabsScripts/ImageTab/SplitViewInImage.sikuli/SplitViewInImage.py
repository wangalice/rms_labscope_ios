# Owner: Lily 2017-09-12
# Last Modified: Lily 2017-09-12
# Remark: Split View In Image

labWindow = App.focusedWindow()
class SplitViewInImage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_SplitViewInImage01_AddSplitViewIcon(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png"))
            count = 0
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break            
            click("DisplaySettingIcon_AllInOne.png")
            wait(1)
            self.assertTrue(exists("DSSplitViewIcon_AllInOne.png")) #Add split view icon in display setting
            pass


    def test_SplitViewInImage02_InFullScreenWhenSplitView(self):
        wait(1)
        with Region(labWindow):
            wait(1)        
            click("DSSplitViewIcon_AllInOne.png")
            wait(1)
            self.assertTrue(waitVanish("ZeissLogo_AllInOne.png")) #After enter the full screen mode, the zeiss icon will be disappeared
            pass

    def test_SplitViewInImage03_TheFullScreenIconWillBeHidden(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click(Location(74, 368))
            wait(1)
            click("DisplaySettingIcon_AllInOne.png")
            wait(1)
            self.assertFalse(exists("DSFullScreenIcon_AllInOne.png")) #The full screen icon will be hidden
            pass

    def test_SplitViewInImage04_OpenAnImage(self):        
        with Region(labWindow):
            click(Location(1346, 85))              
            wait(1)
            doubleClick("CZIFormatImage_AllInOne.png")  #Open an image
            wait(2)
            self.assertTrue(exists("CloseButtImage_AllInOne.png")) #Check if exists the X button for image
            pass 

    def test_SplitViewInImage05_CloseTheImage(self):        
        with Region(labWindow):
            click("CloseButtImage_AllInOne.png")  #Close the image
            wait(5)
            self.assertTrue(waitVanish("CloseButtImage_AllInOne.png")) #The x button will be disappeared when close the image
            pass 

    def test_SplitViewInImage06_OpenAnotherImage(self):        
        with Region(labWindow):
            click(Location(1346, 220))              
            wait(1)
            doubleClick("CZIFormatImage_AllInOne.png")  #Open an image
            wait(2)
            self.assertTrue(exists("CloseButtImage_AllInOne.png")) #Check if exists the X button for image
            pass 

    def test_SplitViewInImage07_ShowSaveIcon(self):        
        with Region(labWindow):
            wait(1)
            self.assertTrue(exists("SaveButtSplitView_AllInOne.png")) #Check if exists the save button
            pass 

    def test_SplitViewInImage08_PressEscToExitSplitView(self):        
        with Region(labWindow):
            wait(1)
            type(Key.ESC)
            wait(1)
            self.assertTrue(exists("ZeissLogo_AllInOne.png"),2) #Check if exists the save button
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)    

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SplitViewInImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











