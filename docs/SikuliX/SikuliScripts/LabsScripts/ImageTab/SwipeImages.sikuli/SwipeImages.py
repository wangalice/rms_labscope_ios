# Owner: Lily 2017-09-08
# Last Modified: Lily 2017-09-08
# Remark: Swipe images

labWindow = App.focusedWindow()
class SwipeImages(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_SwipeImages01_ShowNextArrow(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break      
            wait(1) 
            hover(Location(labWindow.getW()-30 ,200))
            wait(1)
            self.assertTrue(exists("NextImageArrow_AllInOne.png")) #Show next Arrow
            wait(1)
            click("NextImageArrow_AllInOne.png")
            pass

    def test_SwipeImages02_ShowPreviewArrow(self):
        wait(1)
        with Region(labWindow):
            wait(1)           
            self.assertTrue(exists("PreviewImageArrow_AllInOne.png")) #Show preview arrow      
            pass

    def test_SwipeImages03_ClickArrowToSwipeImages(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            for i in range(20):
                if exists("NextImageArrow_AllInOne.png"):
                    click("NextImageArrow_AllInOne.png")
                else:
                    click(Location(labWindow.getW()-20 ,labWindow.getH()/2))       
                wait(2)
            for i in range(10):
                if exists("PreviewImageArrow_AllInOne.png"):
                    click("PreviewImageArrow_AllInOne.png")
                else:
                    click(Location(95 ,labWindow.getH()/2))  
                wait(2)
            wait(1)            
            pass
        
    def test_SwipeImages04_SwipeUnsavedImage(self):
        wait(1)
        with Region(labWindow):
            while not exists("ShareImageTab_AllInOne.png"):
                 click("NextImageArrow_AllInOne.png")
            wait(1)
            click("AnnoLiveButt_AllInOne.png")
            wait(1)
            click("AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1000, 342),Location(1350, 608))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Swiping unsaved image\n" + currTime)
            wait(1)
            click(Location(40, 679))
            wait(1)
            click("AnnoConfirm_AllInOne.png")
            wait(2)
            hover(Location(labWindow.getW()-30 ,200))
            wait(1)
            click(Location(labWindow.getW()-20 ,labWindow.getH()/2)) 
         
            wait(2)
            self.assertTrue(exists("AskOnSaveWindowSwipe_AllInOne.png")) #Pop up ask on save window
            wait(1)
            click("SaveConfigButt_AllInOne.png")
            wait(1)
            click("NotRememberPIN_AllInOne.png")
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(3)               
            pass   
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SwipeImages)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











