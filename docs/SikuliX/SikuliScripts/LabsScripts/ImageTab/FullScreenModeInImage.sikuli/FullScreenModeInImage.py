# Owner: Lily 2017-09-11
# Last Modified: Lily 2017-09-11
# Remark: Full Screen Mode In image

labWindow = App.focusedWindow()
class FullScreenModeInImage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_FullScreenModeInImage01_EnterFullScreenMode(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png"))
            count = 0
            while not exists("ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break            
            click("DisplaySettingIcon_AllInOne.png")
            wait(1)
            click("DSFullScreenIcon_AllInOne.png")
            wait(1)
            self.assertTrue(waitVanish("ZeissLogo_AllInOne.png")) #After enter the full screen mode, the zeiss icon will be disappeared
            pass

    def test_FullScreenModeInImage02_WaitForDisplaySettingIconDisappear(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            self.assertTrue(waitVanish("DisplaySettingIcon_AllInOne.png",5)) #The display setting icon will be disappeared 
            pass

    def test_FullScreenModeInImage03_MoveMouseTheIconWillBeDisplayed(self):        
        wait(1)
        with Region(labWindow):
            wait(1)
            hover(Location(1004, 524)) # Move the mouse
            wait(1)
            self.assertTrue(exists("DisplaySettingIcon_AllInOne.png"), 2) #Check if exists the display setting icon     
            pass 

    def test_FullScreenModeInImage04_DismissDisplaySettingInEditMode(self):        
        with Region(labWindow):
            click("DisplaySettingIcon_AllInOne.png")  #click display setting icon
            wait(1)
            click("DSExitFullScreenIcon_AllInOne.png") #exit full screen mode
            wait(1)
            click("AnnoLiveButt_AllInOne.png")
            wait(1)            
            self.assertFalse(exists("DisplaySettingIcon_AllInOne.png")) #dismiss display setting icon in edit mode        
 
            pass 

    def test_FullScreenModeInImage05_CanNotEnterFullScreenInEditWithF11(self):        
        with Region(labWindow):
            wait(1)
            type(Key.F11)
            wait(1)
            self.assertTrue(exists("ZeissLogo_AllInOne.png")) #Not in full screen mode
            wait(1)
            click("AnnoConfirm_AllInOne.png")
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png")) #Go back to microscope tab
            wait(1)                    
            pass



def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(FullScreenModeInImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











