# Owner: Lily 2017-09-06
# Last Modified: Lily 2017-09-08
# Remark: Snap and Close Image

labWindow = App.focusedWindow()
class SnapAndCloseImage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_SnapAndCloseImage01_HideGarbageIconAfter5s(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(labWindow):
            labWindow.highlight(1) 
            wait(1)
            click(Pattern("MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("GarbageIcon_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break            
            self.assertTrue(waitVanish("GarbageIcon_AllInOne.png", 5)) #The GarbageIcon will disappear after 5s
            pass

    def test_SnapAndCloseImage02_ShowCloseButton(self):
        wait(1)
        with Region(labWindow):
            self.assertTrue(exists("CloseButtImage_AllInOne.png")) #Show close button       
            pass

    def test_SnapAndCloseImage03_DisableImageTabAfterCloseImage(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("CloseButtImage_AllInOne.png")
            wait(1)
            click("ImageTab_AllInOne.png")     
            wait(1)
            self.assertTrue(exists("SnapButt_AllInOne.png")) #After close the image, the image tab is disabled. It will still in live tab (Go back to live if the image is from snapping)
            pass
        
    def test_SnapAndCloseImage04_PopupAskForSaveWhenCloseImageIsEdited(self):
        wait(1)
        with Region(labWindow):
            wait(1)
            click("FilesTab_AllInOne.png")
            wait(1)
            doubleClick("CZIFormatImage_AllInOne.png")
            wait(1)
            click("AnnoLiveButt_AllInOne.png")
            wait(1)
            click("AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1000, 642),Location(1300, 708))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("close unsaved image\n" + currTime)
            wait(1)
            click("AnnoConfirm_AllInOne.png")
            wait(1)
            click("CloseButtImage_AllInOne.png")
            wait(1)
            self.assertTrue(exists("AskOnSaveWindowImage_AllInOne.png")) #Pop up ask on save window
            wait(1)
            click("SaveConfigButt_AllInOne.png")
            wait(1)
            click("NotRememberPIN_AllInOne.png")
            wait(1)            
            pass

    def test_SnapAndCloseImage05_GoBackToWhereImageFromAfterClosing(self):
        wait(1)
        with Region(labWindow):

            wait(1)
            self.assertTrue(exists("OpenInExplorerButt_AllInOne.png")) #Go back to files tab when the image opened from it
            wait(1)
            click(Pattern("MicroscopeTab.png")) #go back to microscope tab
            wait(1)            
            pass
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SnapAndCloseImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











