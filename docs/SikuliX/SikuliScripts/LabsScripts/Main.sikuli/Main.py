# Owner: Eva 2017-08-14
# Last Modified: KiKi 2017-9-20
# Remark: Before running the whole test, please reset the Labscope and set the language to English

from sikuli import *
import os
import sys
import HTMLTestRunner
import unittest
import time
#import CommonBase
#ImagePath.setBundlePath("C:\\SikuliScripts\\LabImages");
imgPath = list(getImagePath())
wait(1)
if 'C:\\SikuliScripts\\LabImages' not in imgPath:
    addImagePath('C:\\SikuliScripts\\LabImages')
wait(1)
if 'C:\\SikuliScripts\\LabImages\\AllInOne' not in imgPath:
    addImagePath('C:\\SikuliScripts\\LabImages\\AllInOne')
if 'C:\\SikuliScripts\\LabImages\\Win7_64_Precision' not in imgPath:
    addImagePath('C:\\SikuliScripts\\LabImages\\Win7_64_Precision')

#Default logging settings
Settings.DebugLogs = True
Settings.LogTime = True
Debug.setLogFile("C:\\SikuliScripts\\ErrorLog\\mainDefaultLog.txt")

#User logging settings
Debug.setUserLogFile("C:\\SikuliScripts\\ErrorLog\\mainUserLog.txt")
#Unit test file path
log_file = 'C:\\SikuliScripts\\ErrorLog\\log_unitTestfile.txt'

labscope = App("Labscope")
labscopePath="C:\\Program Files (x86)\\Carl Zeiss\\Labscope\\Labscope.exe"
class Main():
    for path in imgPath:
        print path
        Debug.user("imgPath %s", path)
    wait(1)

    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("Start time" + localtime + "\n")
    wait(1)
    openApp(labscopePath)
    count=0
    while not exists("ConfirmAppOpened.png"):
        count += 1
        wait(2)
        if exists("ConfirmAppOpened_AllInOne.png"): # try to find the icon in all in one
            break
        # if can not find the icon for long, exit the test
        if count > 5:
            Debug.user("icon ConfirmAppOpened was not found")
            popError("icon ConfirmAppOpened was not found") 
            exit(1)

    labscope.focus()
    labWindow = App.focusedWindow()
    print labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH()
    Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
    # minimium size of window is 1280*900
    #title.setROI(0,0,1280,900)
    #print title.getX(), title.getY(), title.getW(), title.getH()
    with Region(labWindow):
        if exists("ConfigButtMicTab.png"):
            click(findAll("ConfigButtMicTab.png").next())
        if exists("ApplyButt.png"):
            match= find("ApplyButt.png").above().find("CloseButt.png")
            click(match)
        elif exists("ApplyButt_AllInOne.png"): # try to find the icon in all in one
            match= find("ApplyButt_AllInOne.png").above().find("CloseButt.png")
            click(match)
        #match = find("ApplyButt.png").above().find("CloseButt.png")
        #match.click()


        wait(1)
    App.focusedWindow().highlight(2)
    #runScript("C:\\SikuliScripts\\LabImages\\VirtualSnap")
    #region Microscope Tab
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ChangeMicroscopeName")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ConfigureCompoundMicroscope")
    wait(1)  
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ConfigureStereoMicroscope")
    wait(1)     
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\AddTwoVirtualMicroscopes")
    wait(1)     
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\LinkLabscopeToAFixedCamera")
    wait(1)     
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\FindPrimoStarAndPrimovertModels")    
    wait(1)   
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ConfigurePrimotechMicroscope")    
    wait(1)  
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ConfigureScalingManually")    
    wait(1)     
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\LockConfigurationPage")    
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\MicroscopeTab\ShowConfiguratonBarCode")    
    wait(1)    
    
    #region Live Tab
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\DisplaySettingsInLiveTab")    
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\FullScreenModeInLive")    
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\SplitViewInLive")    
    wait(1)  
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\RecordInLive")    
    wait(1)      
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\AddAnnotationsInLive")    
    wait(1)      
    runScript(r"C:\SikuliScripts\LabsScripts\LiveTab\MacroSnap")    
    wait(1)      
    
    #region Image Tab
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\ImageProcessing")
    wait(1)      
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\AddAnnotationsInImage")
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\SnapAndCloseImage")
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\ShareInImageTab")
    wait(1)    
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\SwipeImages")
    wait(1)   
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\FullScreenModeInImage")
    wait(1)      
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\SplitViewInImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\RotateImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\MirrorImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\DisplaySettingsInImageTab")
    wait(1)      
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\ChangeScreen")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\CloseImage")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\CroppingFunction")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\DisplayCurve")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\OpenImageInFileBrowser")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\OperateAboutCSVFile")    
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\ImageTab\ShowShareButt")    
    wait(1)
    runScript(r"")    
    wait(1)
    
    
    
    #region File Tab
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\CopyButt")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\MoveButt")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\ShareButt")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\ShowMetadata")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\ChangeNameOfFiles")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\GarbageIcon")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\OpenFile")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\OperateOfFolder")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\SelectButtInFilesTab")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\ShowExplorerButt")
    wait(1)
    runScript(r"C:\SikuliScripts\LabsScripts\FilesTab\SortingOrder")
    wait(1)
    
    labscope.close()
    localtime = time.asctime(time.localtime(time.time()))
    file.write("End time" + localtime + "\n")
    file.close()