# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Add annotations In Live. 
# Snap three format images: czi, jpg, tif.  The text annotation marked the time

labWindow = App.focusedWindow()
class AddAnnotationsInLive(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_AddAnnotationsInLive01_EnterAnnotationModeInLive(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click("mat_AnnoLiveButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_AnnoIcons_AllInOne.png")) #Enter the annotation mode           
            pass

    def test_AddAnnotationsInLive02_DisableFullScreenModeInEditMode(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            type(Key.F11)
            wait(1)
            self.assertTrue(exists("mat_AnnoIcons_AllInOne.png")) #Not in full screen mode   
            pass

    def test_AddAnnotationsInLive03_ShowColorSizeButton(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_AnnoLine_AllInOne.png") #Add a Line
            Debug.user("Add a line")
            wait(1)
            self.assertTrue(exists("mat_AnnoColorSizeButt_AllInOne.png")) #The button displayed when an annotation is selected and the color is red
            pass
        
    def test_AddAnnotationsInLive04_HideColorSizeButton(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Location(144, 659)) # Click any place to deselect the annotation
            wait(2)
            self.assertFalse(exists("mat_AnnoColorSizeButt_AllInOne.png")) #The button will be hidden when no annotation is selected
            pass

    def test_AddAnnotationsInLive05_5ColorsAndSizesAvailable(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_AnnoSquare_AllInOne.png") # Add a square
            Debug.user("Add a square")
            wait(1)
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists(Pattern("mat_AnnoColorSizePanel_AllInOne.png").similar(0.60))) #Show 5 colors and sizes in pop up panel
            pass

    def test_AddAnnotationsInLive06_DismissThePopOverWhenAddAnnotation(self):
        wait(1)
        with Region(App.focusedWindow()):
            click(Location(544, 959))
            wait(1)         
            click("mat_AnnoArrow_AllInOne.png") # Add arrow
            Debug.user("Add an arrow")
            wait(1)
            click(Location(1339, 903))    
            wait(1)
            self.assertFalse(exists(Pattern("mat_AnnoColorSizePanel_AllInOne.png").similar(0.60))) #Dismiss pop up panel
            pass
            
    def test_AddAnnotationsInLive07_AddAnnotations(self):
        wait(1)
        with Region(App.focusedWindow()):
            click("mat_AnnoCircle_AllInOne.png") # Add circle
            wait(1)
            click(Location(193, 716)) # Add circle
            wait(1)
            click(Location(385, 885)) # Add circle
            wait(1)
            click(Location(434, 636)) # Add circle
            Debug.user("Add a circle")
            wait(1)     
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            click("mat_AnnoColorBlue_AllInOne.png") #change color to blue
            wait(1)
            click("mat_AnnoS_AllInOne.png") #change size to s
            wait(1)
            click(Location(1339, 903))
            wait(1)
            click("mat_AnnoPolygon_AllInOne.png") #Add polygon
            wait(1)
            click(Location(362, 246))
            wait(1)
            click(Location(392, 146))
            wait(1)
            click(Location(262, 46))
            wait(1)
            click(Location(462, 346))
            wait(1)    
            rightClick(Location(482, 366))
            Debug.user("Add a polygon")
            wait(1)
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            click(Pattern("mat_AnnoL_AllInOne.png").targetOffset(0,-57)) #change color to yellow
            wait(1)
            click("mat_AnnoXS_AllInOne.png") #change size to xs
            wait(1)
            click(Location(1339, 903))
            wait(1)
            click("mat_AnnoAngle_AllInOne.png") #Add angle
            Debug.user("Add a angle")
            wait(1)     
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            click(Pattern("mat_AnnoM_AllInOne.png").targetOffset(0,-57)) #change color to green
            wait(1)
            click("mat_AnnoL_AllInOne.png") #change size to L
            wait(1)
            click(Location(1339, 903))            
            wait(1)
            click("mat_AnnoCount_AllInOne.png")
            wait(1)
            click(Location(715, 109))
            wait(1)       
            click(Location(745, 139))
            wait(1)       
            click(Location(765, 179))
            wait(1)  
            click(Location(785, 239))
            wait(1)  
            click(Location(805, 279))
            wait(1)  
            rightClick(Location(815,299))
            Debug.user("Add counts")
            wait(1)
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            click("mat_AnnoColorBlack_AllInOne.png") #change color to black
            wait(1)
            click("mat_AnnoXL_AllInOne.png") #change size to XL            
            wait(1)
            click(Location(1339, 903))
            wait(1)
            click("mat_AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1288, 642),Location(1657, 750))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Edit in live tab \n" + currTime)
            Debug.user("Add text")
            wait(1)
            click("mat_AnnoColorSizeButt_AllInOne.png")
            wait(1)
            click(Pattern("mat_AnnoXL_AllInOne.png").targetOffset(0,-57))
            wait(1)
            click(Location(1339, 903))
            wait(1)
            click("mat_AnnoScalebar_AllInOne.png")
            Debug.user("Add scalebar")            
            wait(1)
            click(Location(1339, 903))
            wait(1)
            click("mat_AnnoConfirm_AllInOne.png")
            wait(1)
            self.assertTrue(exists(Pattern("mat_SnapButt_AllInOne.png"))) #Finish edit
            wait(1)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            wait(1)
            with Region(App.focusedWindow()):
                wait(1)            
                count=0 # Add a count , in case can not find the icon
                while not exists("mat_ShareImageTab_AllInOne.png"):
                    count = count + 1
                    wait(1)
                    if count > 5:
                        break
                click(Pattern("mat_MicroscopeTab_AllInOne.png")) # Go to microscope tab first to prevent pop up the window ask to stay at live tab
                wait(1)                

                pass

    def test_AddAnnotationsInLive08_SnapImages(self):
        wait(1)
        with Region(App.focusedWindow()):   
            click(Location(App.focusedWindow().getW() - 128, 6))  #Pattern("GlobalSetting_AllInOne.png")
            wait(1)
            click("mat_FormatCZIddl_AllInOne.png")
            wait(1)
            click("mat_FormatJPG_AllInOne.png") #select JPG format
            wait(1)
            click(Pattern("mat_GlobalOKButt_AllInOne.png"))            
            wait(1)
            click(Pattern("mat_ImageTab_AllInOne.png").targetOffset(0, -50)) #Go to the live tab                
            wait(1)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            wait(1)
   
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break
        with Region(App.focusedWindow()):
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) # Go to microscope tab first to prevent pop up the window ask to stay at live tab
            wait(1)
            click(Location(App.focusedWindow().getW() - 128, 6))  #Pattern("GlobalSetting_AllInOne.png")
            wait(1)
            click("mat_FormatJPGddl_AllInOne.png")
            wait(1)
            click("mat_FormatTIF_AllInOne.png") #select TIF format
            wait(1)
            click(Pattern("mat_GlobalOKButt_AllInOne.png"))            
            wait(1)
            click(Pattern("mat_ImageTab_AllInOne.png").targetOffset(0, -50)) #Go to the live tab    
            wait(1)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            wait(1)
   
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break
        with Region(App.focusedWindow()):
            click(Location(App.focusedWindow().getW() - 128, 6))  #Pattern("GlobalSetting_AllInOne.png")
            wait(1)
            click("mat_FormatTIFddl_AllInOne.png")
            wait(1)
            click("mat_FormatCZI_AllInOne.png") #select CZI format
            wait(1)
            click(Pattern("mat_GlobalOKButt_AllInOne.png"))            
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab      
            wait(1)
            pass

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(AddAnnotationsInLive)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











