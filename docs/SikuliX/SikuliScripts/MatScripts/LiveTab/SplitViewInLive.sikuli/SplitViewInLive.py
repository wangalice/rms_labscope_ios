# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Split View In Live

labWindow = App.focusedWindow()
class SplitViewInLive(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_SplitViewInLive01_EnterSplitViewMode(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click("mat_DisplaySettingIcon_AllInOne.png")
            wait(1)
            click("mat_DSSplitViewIcon_AllInOne.png")
            wait(1)
            self.assertTrue(waitVanish("mat_ZeissLogo_AllInOne.png")) #After enter the full screen mode, the zeiss icon will be disappeared
            pass

    def test_SplitViewInLive02_WaitForDisplaySettingIconDisappear(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertFalse(exists("mat_SnapButt_AllInOne.png")) #The snap button will be hidden
            pass

    def test_SplitViewInLive03_TheFullScreenIconWillBeHidden(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Location(74, 368))
            wait(1)
            click("mat_DisplaySettingIcon_AllInOne.png")
            wait(1)
            self.assertFalse(exists("mat_DSFullScreenIcon_AllInOne.png")) #The full screen icon will be hidden
            pass

    def test_SplitViewInLive04_TheFocusIndicatorIconWillBeHidden(self):        
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertFalse(exists("mat_DSFocusIndicatorIcon_AllInOne.png")) #The focus indicator icon will be hidden
            pass 

    def test_SplitViewInLive05_OpenAnImage(self):        
        with Region(App.focusedWindow()):
            click(Location(1346, 85))              
            wait(1)
            doubleClick("mat_CZIFormatImage_AllInOne.png")  #Open an image
            wait(2)
            self.assertTrue(exists("mat_CloseButtImage_AllInOne.png")) #Check if exists the X button for image
            pass 

    def test_SplitViewInLive06_CloseTheImage(self):        
        with Region(App.focusedWindow()):
            click("mat_CloseButtImage_AllInOne.png")  #Close the image
            wait(5)
            self.assertTrue(waitVanish("mat_CloseButtImage_AllInOne.png")) #The x button will be disappeared when close the image
            pass 

    def test_SplitViewInLive07_OpenAnotherImage(self):        
        with Region(App.focusedWindow()):
            click(Location(1346, 220))              
            wait(1)
            doubleClick("mat_CZIFormatImage_AllInOne.png")  #Open an image
            wait(2)
            self.assertTrue(exists("mat_CloseButtImage_AllInOne.png")) #Check if exists the X button for image
            pass 

    def test_SplitViewInLive08_ShowSaveIcon(self):        
        with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_SaveButtSplitView_AllInOne.png")) #Check if exists the save button
            pass 

    def test_SplitViewInLive09_PressEscToExitSplitView(self):        
        with Region(App.focusedWindow()):
            wait(1)
            type(Key.ESC)
            wait(1)
            self.assertTrue(exists("mat_ZeissLogo_AllInOne.png"),2) #Check if exists the save button
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab
            wait(1)    

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SplitViewInLive)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











