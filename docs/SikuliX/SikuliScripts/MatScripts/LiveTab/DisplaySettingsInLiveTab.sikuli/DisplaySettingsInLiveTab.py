# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Display settings

labWindow = App.focusedWindow()
class DisplaySettingsInLiveTab(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_DisplaySettingsInLiveTab01_FindDisplayCurve(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click("mat_DisplaySettingIcon_AllInOne.png")
            with Region(App.focusedWindow()):
                wait(2)
                self.assertTrue(exists("mat_DSDisplayCurveIcon_AllInOne.png"), 2) #Check if exists the icon for display curve
                pass   

    def test_DisplaySettingsInLiveTab02_FindOverExposure(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSOverexposureIcon_AllInOne.png"), 2) #Check if exists the icon for overexposure
            pass 

    def test_DisplaySettingsInLiveTab03_FindGraticuleOverlay(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSGraticuleOverlayIcon_AllInOne.png"), 2) #Check if exists the icon for graticule overlay
            pass 

    def test_DisplaySettingsInLiveTab04_FindFocusIndicator(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSFocusIndicatorIcon_AllInOne.png"), 2) #Check if exists the icon for focus indicator
            pass 

    def test_DisplaySettingsInLiveTab05_FindDrawingTube(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSDrawingTubeIcon_AllInOne.png"), 2) #Check if exists the icon for drawing tube
            pass 

    def test_DisplaySettingsInLiveTab06_FindLaserPoint(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSLaserPointIcon_AllInOne.png"), 2) #Check if exists the icon for laser point
            pass 

    def test_DisplaySettingsInLiveTab07_FindSplitView(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSSplitViewIcon_AllInOne.png"), 2) #Check if exists the icon for split view
            pass 

    def test_DisplaySettingsInLiveTab08_FindFullScreen(self):      
        wait(1)
        with Region(App.focusedWindow()):
            self.assertTrue(exists("mat_DSFullScreenIcon_AllInOne.png"), 2) #Check if exists the icon for full screen
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #Go back to microscope tab
            wait(1)
            pass 
 
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(DisplaySettingsInLiveTab)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 









