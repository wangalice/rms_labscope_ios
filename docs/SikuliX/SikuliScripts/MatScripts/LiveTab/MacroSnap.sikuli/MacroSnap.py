# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Macro snap

labWindow = App.focusedWindow()
class MacroSnap(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_MacroSnap01_AddMacroSnapButton(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("mat_SnapButt_AllInOne.png").targetOffset(0, 45)) # Click the arrow under the snap button to show drop down list
            wait(1)
            self.assertTrue(exists("mat_MacroSnapDropDownButt_AllInOne.png")) # Find the record icon in drop down list
            pass

    def test_MacroSnap02_PopupScalingWindow(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_MacroSnapDropDownButt_AllInOne.png")
            wait(1)
            click("mat_MacroSnapButt_AllInOne.png")
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_AddScalingInfo_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break            
            self.assertTrue(exists("mat_AddScalingInfo_AllInOne.png")) #Pop up the window to set scaling
            pass

    def test_MacroSnap03_MacroSnapSuccessfully(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_ManualDone_AllInOne.png")
            wait(1)
            with Region(App.focusedWindow()):
                count=0 # Add a count , in case can not find the icon
                while not exists("mat_ShareImageTab_AllInOne.png"):
                    count = count + 1
                    wait(1)
                    if count > 5:
                        break            
                self.assertTrue(exists("mat_ShareImageTab_AllInOne.png")) #Snap successfully
                pass
        
    def test_MacroSnap04_ShowMacroScalingButton(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_MacroScalingButt_AllInOne.png")) #Show macro scaling button on the function bar
            pass

    def test_MacroSnap05_ShowModifyMacroScalingWindow(self):
        wait(1)
        click("mat_MacroScalingButt_AllInOne.png")
        wait(2)
        with Region(App.focusedWindow()):             
            wait(1)
            self.assertTrue(exists("mat_AddScalingInfo_AllInOne.png")) #Pop up the window to set scaling
            wait(1)
            click(Pattern("mat_ManualDone_AllInOne.png").targetOffset(5, -46))
            wait(1)
        with Region(App.focusedWindow()):             
            wait(1)            
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab
            wait(1)
            pass

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(MacroSnap)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











