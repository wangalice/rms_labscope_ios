# Owner: Lily 2017-09-19
# Last Modified: Lily 2017-09-19
# Remark: Configure Primotech microscope. It is necessary to have a real camera for this test

labWindow = App.focusedWindow()
class ConfigurePrimotechMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ConfigurePrimotechMicroscope01_findPrimotechModel(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1)
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(1)
            click("mat_ConfigButtLiveTab_AllInOne.png")
            wait(6)
            click(Location(631, 409))
            wait(6)
            with Region(App.focusedWindow()):
                wait(1)            
                count=0 # Add a count , in case can not find the icon
                while not exists("mat_PrimotechModulIcon_AllInOne.png"):
                    count = count + 1
                    wait(0.5)
                    click(find(Pattern("mat_PageUpButtConfig_AllInOne.png").targetOffset(0, 20)))    #if not find the model, page down
                    if count > 5:
                        break
                wait(1)
                self.assertTrue(exists("mat_PrimotechModulIcon_AllInOne.png"), 2) #Check if exists the field microscope name
                pass    

    def test_ConfigurePrimotechMicroscope02_FindShadingCorectionForPrimotech(self):   
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_PrimotechModulIcon_AllInOne.png")
            wait(7) #wait for the MNA loading finish            
            self.assertTrue(exists("mat_ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigurePrimotechMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass        
        
    def test_ConfigurePrimotechMicroscope04_SetPrimotechObjectives(self):
        wait(1)
        with Region(App.focusedWindow()):
            # set objective values for the revolver       
            for i in range(1,6):
                click("mat_CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(2)
                if i == 1:
                    click("mat_Primotech5x_AllInOne.png")
                elif i == 2:
                    click("mat_Primotech10x_AllInOne.png")
                elif i == 3:
                    click("mat_Primotech20x_AllInOne.png")
                elif i == 4:
                    click("mat_Primotech50x_AllInOne.png")
                elif i == 5:
                    click("mat_Primotech100x_AllInOne.png")                    
                    
                wait(1)
                click("mat_NextObjectiveButt_AllInOne.png")     #click button to select next objective position
                wait(1)
                Debug.user("Primotech objective pos. %d", i  )
                wait(1)

            self.assertTrue(exists("mat_PrimotechObjectives_AllInOne.png"), 2) #It is possible that the final displayed results are different in different devices
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)
            pass   

    def test_ConfigurePrimotechMicroscope05_ShowRevolverInLive(self):
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            click(Pattern("mat_AnnoLiveButt_AllInOne.png").targetOffset(0, -60)) #click the objective button over the annotation button
            wait(1)
            self.assertTrue(exists(Pattern("mat_Scalebar200Live_AllInOne.png").similar(0.60)),2) #200um for 5x, the image is cut from the live of  wision camera        
            wait(1)
            click(Pattern("mat_NextObjectiveLiveButt_AllInOne.png").similar(0.50)) #click to next objective
            wait(1)            
            self.assertTrue(exists(Pattern("mat_Scalebar100Live_AllInOne.png").similar(0.60)),2) #100um for 10x, the image is cut from the live of  wision camera            
            wait(1)           
            pass
            
    def test_ConfigurePrimotechMicroscope06_SameScalebarInImageTab(self):
        wait(1)
        with Region(App.focusedWindow()):        
            wait(1)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            count = 0
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                #if wait the share button for too long, consider snap is failed
                Debug.user("wait for the share button in image tab count %d", count  )
                if count > 10:
                    break         
            wait(1)
            self.assertTrue(exists(Pattern("mat_Scalebar100Live_AllInOne.png").similar(0.60)),2) #image tab should have the same scale bar. 100um for 10x, the image is cut from the live of  wision camera
            wait(1)   
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab
            wait(1)            
            pass

    
def main():

    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigurePrimotechMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()






