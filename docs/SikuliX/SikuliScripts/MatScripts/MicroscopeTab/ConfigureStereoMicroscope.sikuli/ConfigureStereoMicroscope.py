# Owner: Lily 2017-09-19
# Last Modified: Lily 2017-09-19
# Remark: Configure Stereo microscope

labWindow = App.focusedWindow()
class ConfigureStereoMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ConfigureStereoMicroscope01_FindStereoModel(self):
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1)
            wait(1)
            click(find("mat_ConfigButtMicTab_AllInOne.png"))
            wait(1)
            click(Location(631, 409))
            wait(7)
            count=0 # Add a count , in case can not find the icon
            while not exists(Pattern("mat_StereoModulIcon_AllInOne.png")):
                count = count + 1
                wait(0.5)
                click(find(Pattern("mat_PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists(Pattern("mat_StereoModulIcon_AllInOne.png")), 2) #Check if exists the field microscope name
            
            pass   

    def test_ConfigureStereoMicroscope02_FindShadingCorectionForPrimotech(self):   
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_StereoModulIcon_AllInOne.png")
            wait(1)          
            self.assertTrue(exists("mat_ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigureStereoMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass     
        
    def test_ConfigureStereoMicroscope04_FindShadingCorectionSetUpButton(self):   
         wait(1)
         with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_ShadingCorrectionSetupDisabled_AllInOne"), 2) #Check if the setup disabled
            wait(1)          
            pass 
        
    def test_ConfigureStereoMicroscope05_FindZoomClickValue(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_StereoModulIcon_AllInOne.png"))         
            wait(1)
            click(Pattern("mat_ZoomClicklb_AllInOne.png").targetOffset(300, 0)) #click the zoom click stop list
            wait(1)
            self.assertTrue(exists(Pattern("mat_ZoomClickList_AllInOne.png")), 2) #Check if exists the value list
            wait(1)
            pass

    def test_ConfigureStereoMicroscope06_ZoomClickValueMax12(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            if exists(Pattern("mat_ZoomClickChecked_AllInOne.png").similar(0.89)):
                checkedList = findAll(Pattern("mat_ZoomClickChecked_AllInOne.png").similar(0.89))
                wait(1)
                #If there are checked item, unchecked them first
                for i in checkedList:
                    click(i)
                    wait(1)
                    Debug.user("Zoom click unchecked count %s ", i)

            if exists(Pattern("mat_ZoomClickUnChecked_AllInOne.png").similar(0.89)):
                uncheckedList = findAll(Pattern("mat_ZoomClickUnChecked_AllInOne.png").similar(0.89))
                wait(1)
                count = 1
                for i in uncheckedList:
                    click(i)
                    wait(1)
                    Debug.user("Zoom click checked count %s ", i)
                    if count == 13:
                        self.assertTrue(exists(Pattern("mat_NotificationZoomClick_AllInOne.png")), 2) #Check if pop up the notification, when more than 12 item are selected      
                        wait(1)
                        click(Pattern("mat_ZoomClickOKButt_AllInOne.png"))
                        break
                    wait(1)
                    count = count + 1
                    
            pass

    def test_ConfigureStereoMicroscope07_SelectObjectiveValue(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_Objectivelb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("mat_StereoObjectiveVal_AllInOne.png").similar(0.85))
            wait(1)
            self.assertTrue(exists(Pattern("mat_StereoObjectiveSelect_AllInOne.png").similar(0.85)), 2) #Check if the objective value set correctly  
            wait(1)   
            pass

    def test_ConfigureStereoMicroscope08_SelectEyepiecesValue(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_Eyepieceslb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("mat_StereoEyepiecesVal_AllInOne.png"))
            wait(1)
            self.assertTrue(exists(Pattern("mat_StereoEyepiecesSelect_AllInOne.png")), 2) #Check if the objective value set correctly  
            wait(1)  
            pass

    def test_ConfigureStereoMicroscope09_StereoAdvanceCamAdapter(self):
        wait(1)
        with Region(App.focusedWindow()):       
            wait(1)            
            click("mat_CompoundAdvanceButt_AllInOne.png") #click advanced icon (same icon as that of compound)         
            wait(1)
            click(Pattern("mat_CamAdapterlb_AllInOne.png").targetOffset(200, 0)) #click the Objective list
            wait(1)
            click(Pattern("mat_StereoAdapterVal_AllInOne.png"))
            wait(1)
            self.assertTrue(exists(Pattern("mat_StereoAdapterSelect_AllInOne.png")), 2) #Check if the objective value set correctly  
            wait(1)  
            pass

    def test_ConfigureStereoMicroscope10_StereoAdvanceAdditionalEyepiece(self):
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            for i in range(1, 4):
                doubleClick(Pattern("mat_AdditionalEyepieceslb_AllInOne.png").targetOffset(180, 0)) #click the textbox
                wait(1)
                #Make sure that clear the textbox
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
        
                wait(1)
                if i == 1:                 
                    Debug.user("Type 0.009 for AdditionalEyepieces")
                    wait(1)
                    type("0.009")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)                    
                    self.assertTrue(exists("mat_MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:     
                    Debug.user("Type 5 for AdditionalEyepieces")
                    wait(1)
                    type("5")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)   
                    self.assertTrue(exists("mat_AdditionalMaxVal_AllInOne.png"), 2) #5 is the max. value here
                    wait(1)
                if i == 3:                 
                    Debug.user("Type 5.01 for AdditionalEyepieces")
                    wait(1)
                    type("5.01")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("mat_MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass

    def test_ConfigureStereoMicroscope11_StereoAdvanceAdditionalCamera(self):
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            for i in range(1, 4):
                doubleClick(Pattern("mat_AdditionalCameralb_AllInOne.png").targetOffset(180, 0)) #click the Objective list
                wait(1)
                #Make sure that clear the textbox
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
        
                wait(1)
                if i == 1:                 
                    Debug.user("Type 0.009 for AdditionalCamera")
                    wait(1)
                    type("0.009")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("mat_MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:                 
                    Debug.user("Type 5 for AdditionalCamera")
                    wait(1)
                    type("5")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("mat_AdditionalMaxVal_AllInOne.png"), 2) #5 is the max. value here
                    wait(1)
                if i == 3:                 
                    Debug.user("Type 5.01 for AdditionalCamera")
                    wait(1)                    
                    type("5.01")
                    wait(1)                
                    type(Key.ENTER)
                    wait(1)  
                    self.assertTrue(exists("mat_MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass
        
    def test_ConfigureStereoMicroscope12_NotificationWithoutSaving(self):
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            click("mat_CloseButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_NotificationConfigUnsave_AllInOne.png"), 2)
            wait(1)
            click(find("mat_SaveConfigButt_AllInOne.png"))
            wait(1)                    
            pass   
                      
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigureStereoMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()       