# Owner: Lily 2017-09-19
# Last Modified: 2017-09-19
# Remark: Link Labscope To A Fixed Camera

labWindow = App.focusedWindow()
class LinkLabscopeToAFixedCamera(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_LinkLabscopeToAFixedCamera01_FindToggleForSingleMode(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(find("mat_ConfigButtMicTab_AllInOne.png"))
            wait(6)
            click(Location(631, 409))
            wait(6)
            self.assertTrue(exists(Pattern("mat_SingleModeButt_AllInOne.png")), 2) #Check if exists the toggle button for single mode   
            pass            

    def test_LinkLabscopeToAFixedCamera02_SingleModeWillHideMicroScopeTab(self):      
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_SingleModeButt_AllInOne.png"))
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)
            self.assertFalse(exists(Pattern("mat_MicroscopeTab_AllInOne.png")), 2) #Check if the microscope tab is hidden
            wait(1)         
            pass             

    def test_LinkLabscopeToAFixedCamera03_StayInSingleModeAfterRelaunch(self):      
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_CloseAppButt_AllInOne.png"))
            wait(10)
            matscopePath="C:\\Program Files (x86)\\Carl Zeiss\\Matscope\\Matscope.exe"
            openApp(matscopePath)            
            count=0
            while not exists("mat_ZeissLogo_AllInOne.png"):
                count += 1
                wait(2)
                if exists("mat_ZeissLogo_AllInOne.png"): # try to find the icon in all in one
                    break
                # if can not find the icon for long, exit the test
                if count > 5:
                    Debug.user("mat_icon ZeissLogo_AllInOne.png was not found")
                    popError("mat_icon ZeissLogo_AllInOne.png was not found") 
                    exit(1)

            wait(1)
            self.assertFalse(exists(Pattern("mat_MicroscopeTab_AllInOne.png")), 2) #Check if the microscope tab is still hidden after relaunch
            wait(1)
            click("mat_ConfigButtLiveTab_AllInOne.png")
            wait(6)
            click(Pattern("mat_SingleModeButt_AllInOne.png"))
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab
            wait(1)
            pass            
    

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(LinkLabscopeToAFixedCamera)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()   