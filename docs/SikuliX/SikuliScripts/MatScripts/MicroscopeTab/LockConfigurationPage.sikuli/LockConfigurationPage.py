# Owner: Lily 2017-09-19
# Last Modified: Lily 2017-09-19
# Remark: Add PIN to lock microscope configuration page

labWindow = App.focusedWindow()
class LockConfigurationPage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_LockConfigurationPage01_FindLockConfiguration(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click("mat_ConfigButtMicTab_AllInOne.png") 
            wait(1)
            click(Location(631, 409))
            wait(6)
            with Region(App.focusedWindow()):
                wait(2)
                self.assertTrue(exists("mat_LockConfiglb_AllInOne.png"), 2) #Check if exists the Lock option
                pass   

    # Lock the setting with the PIN remember
    def test_LockConfigurationPage02_SwitchOnLockConfiguration(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_LockConfiglb_AllInOne.png").targetOffset(120,0))
            wait(1)
            click("mat_YesRememberPIN_AllInOne.png") #the app will remember the PIN number

    def test_LockConfigurationPage03_ShowWarningWhenInputLessThan4(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            doubleClick(Pattern("mat_4digiPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            #Invalid can not be input
            type("!@#$%^&*(){}<>,?")
            wait(1) 
            type("abcdefghijklmn")
            wait(1)
            type("12")
            wait(1)
            click(Pattern("mat_ConfirmPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))        
            self.assertTrue(exists("mat_4digiPINWarning_AllInOne.png"), 2) #check the warning message
            pass       
        
    def test_LockConfigurationPage04_PopupMessageWhenInputLessThan4(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)      
            self.assertTrue(exists("mat_4digiPINPopup_AllInOne.png"), 2) #check the warning message            
            wait(1)   
            click("mat_ZoomClickOKButt_AllInOne.png")
            pass

    def test_LockConfigurationPage05_ShowWarningWhenInputNotConfirmed(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            doubleClick(Pattern("mat_4digiPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            type("1234")
            wait(1) 
            doubleClick(Pattern("mat_ConfirmPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))      
            wait(1)
            type("1111")
            wait(1)  
            self.assertTrue(exists("mat_4digiPINNotMatchWarning_AllInOne.png"), 2) #check the warning message
            pass  

    def test_LockConfigurationPage06_PopupMessageWhenInputNotConfirmed(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)      
            self.assertTrue(exists("mat_4digiPINNotMatchPopup_AllInOne.png"), 2) #check the warning message            
            wait(1)   
            click("mat_ZoomClickOKButt_AllInOne.png")
            pass
        
    def test_LockConfigurationPage07_TheMaxLengthOfPINIs4(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            doubleClick(Pattern("mat_4digiPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            # The PIN is 1234 even input more
            type("12345678") 
            wait(1) 
            doubleClick(Pattern("mat_ConfirmPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))      
            wait(1)
            type("12345678")
            wait(1)  
            self.assertTrue(exists("mat_4digitsPIN_AllInOne.png"), 2) # the length is 4
            wait(1)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1)     
            pass 

    def test_LockConfigurationPage08_ShowLockIconInMicTab(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85)), 2) # Show lock icon in mic tab      
            wait(1)     
            pass 
        
    def test_LockConfigurationPage09_RememberPINByCreate(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))    
            wait(1)   
            self.assertTrue(exists("mat_PINRemembered_AllInOne.png"), 2) # the PIN is remembered
            pass 

    def test_LockConfigurationPage10_NotRememberPINByOpenConfigure(self):       
        wait(1)
        with Region(labWindow):
            wait(1)
            click("mat_ZoomClickChecked_AllInOne.png")    
            wait(1)   
            click("mat_PINEnterOK_AllInOne.png")
            wait(6)
            click(Pattern("mat_ApplyButt_AllInOne.png"))
            wait(1) 
            click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))    
            wait(1)
            self.assertTrue(exists("mat_PINNotRemembered_AllInOne.png"), 2) # the PIN is not remembered
            pass 

    def test_LockConfigurationPage11_PopupMessageForEnterError3Times(self):   
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)     
            click("mat_PINEnterOK_AllInOne.png")
            wait(1)     
            click("mat_PINEnterOK_AllInOne.png")
            wait(1)     
            click("mat_PINEnterOK_AllInOne.png")
            wait(1)
            with Region(App.focusedWindow()):  
                wait(1)
                self.assertTrue(exists("mat_PINForgetPopup_AllInOne.png"), 2) #Pop up message for forget PIN
                wait(1)
                click("mat_ForgetPINOKButt_AllInOne.png")
                pass            

    def test_LockConfigurationPage12_UseMasterPINNumber(self):   
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)     
            click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))
            wait(1)     
            doubleClick("mat_EnterPINMicTab_AllInOne.png")
            wait(1)     
            type("h!%jPYtt34")
            wait(1)
            click("mat_PINEnterOK_AllInOne.png")
            wait(6)
            self.assertTrue(exists("mat_ConfigureMicroscopelb_AllInOne.png"), 2) # Enter the configure with master PIN successfully
            pass    

    def test_LockConfigurationPage13_NotRememberPINByCreate(self):       
        wait(1)
        with Region(App.focusedWindow()):
            click(Pattern("mat_LockConfiglb_AllInOne.png").targetOffset(120,0))
            wait(1)
            click(Pattern("mat_LockConfiglb_AllInOne.png").targetOffset(120,0))
            wait(1)
            click("mat_NotRememberPIN_AllInOne.png") #the app will remember the PIN number
            wait(1)
            doubleClick(Pattern("mat_4digiPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            type("1234") 
            wait(1) 
            doubleClick(Pattern("mat_ConfirmPINlb_AllInOne.png").similar(0.60).targetOffset(160,0))      
            wait(1)
            type("1234")
            wait(1) 
            click(Pattern("mat_ApplyButt_AllInOne.png"))            
            wait(1)
            with Region(App.focusedWindow()):
                click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))    
                wait(1)   
                self.assertTrue(exists("mat_PINNotRemembered_AllInOne.png"), 2) # the PIN is not remembered
                pass 

    def test_LockConfigurationPage14_InputNotSavedByCancelClick(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)     
            doubleClick("mat_EnterPINMicTab_AllInOne.png")
            wait(1)     
            type("1234")
            wait(1)   
            click("mat_ZoomClickUnChecked_AllInOne.png")
            wait(1)
            click("mat_PINEnterCancel_AllInOne.png")
            wait(1)
            with Region(App.focusedWindow()):            
                click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))    
                wait(1)   
                self.assertTrue(exists("mat_PINNotRemembered_AllInOne.png"), 2) # the PIN is not remembered
                pass             

    def test_LockConfigurationPage15_RememberPINByOpenConfigure(self):       
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)     
            doubleClick("mat_EnterPINMicTab_AllInOne.png")
            wait(1)     
            type("1234")  
            wait(1)
            click("mat_ZoomClickUnChecked_AllInOne.png")    
            wait(1)   
            click("mat_PINEnterOK_AllInOne.png")
            wait(6)
            with Region(App.focusedWindow()):      
                wait(1)
                click(Pattern("mat_ApplyButt_AllInOne.png"))
            with Region(App.focusedWindow()):           
                wait(1) 
                click(Pattern("mat_LockIconMicTab_AllInOne.png").similar(0.85))    
                wait(1)
                self.assertTrue(exists("mat_PINRemembered_AllInOne.png"), 2) # the PIN is remembered
                wait(1)
                # Remove the PIN and go back to microscope tab
                click("mat_PINEnterOK_AllInOne.png")      
                wait(7)
                click(Pattern("mat_LockConfiglb_AllInOne.png").targetOffset(120,0))
                wait(1)
                click(Pattern("mat_ApplyButt_AllInOne.png"))
                wait(1)
                pass 



def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(LockConfigurationPage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 



