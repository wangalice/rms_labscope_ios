# Owner: Lily 2017-09-19
# Last Modified: Lily 2017-09-19
# Remark: Show configuration bar code for the microscope

labWindow = App.focusedWindow()
class ShowConfiguratonBarCode(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ShowConfiguratonBarCode01_FindConfigurationBarCodeButton(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(find("mat_ConfigButtMicTab_AllInOne.png"))
            wait(7)
            with Region(App.focusedWindow()):
                wait(2)
                self.assertTrue(exists("mat_ConfigBarCodeButt_AllInOne.png"), 2) #Check if exists the bar code button
                pass   

    def test_ShowConfiguratonBarCode02_FindModelImportConfigurationByBarCode(self):      
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Location(631, 409))
            wait(7)
            count=0 # Add a count , in case can not find the icon
            while not exists(Pattern("mat_ScanningBarCodeModel_AllInOne.png")):
                count = count + 1
                wait(0.5)
                click(find(Pattern("mat_PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists(Pattern("mat_ScanningBarCodeModel_AllInOne.png")), 2) #Check if exists the model   
            wait(1)
            click("mat_ApplyButt_AllInOne.png")
            pass   
            
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowConfiguratonBarCode)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 





