# Owner: Lily 2017-08-16
# Last Modified: Lily 2017-8-17
# Remark: Configure compound microscope

matWindow = App.focusedWindow()
class ConfigureCompoundMicroscope(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass
    
    def test_ConfigureCompoundMicroscope01_FindCompoundModel(self):       
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", matWindow.getX(), matWindow.getY(), matWindow.getW(), matWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1)
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click("mat_ConfigButtLiveTab_AllInOne.png")
            wait(5)
            click(Location(631, 409))
            wait(5)
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_CompoundModulIcon_AllInOne.png"):
                count = count + 1
                wait(0.5)
                click(find(Pattern("mat_PageDownButtConfig_AllInOne.png").targetOffset(0,-20)))    #if not find the model, page down
                if count > 5:
                    break
            wait(1)
            self.assertTrue(exists("mat_CompoundModulIcon_AllInOne.png"), 2) #Check if exists the compound modul
            pass    

    def test_ConfigureCompoundMicroscope02_FindShadingCorectionForCompound(self):   
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_CompoundModulIcon_AllInOne.png")
            wait(1)          
            self.assertTrue(exists("mat_ShadingCorrectionlb_AllInOne.png"), 2) #Check if exists the field shading correction
            wait(1)    
            pass  

        
    def test_ConfigureCompoundMicroscope03_ShadingCorectionToggleDefaultIsOff(self):   
         wait(1)
         with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_ShadingCorrectionToggleOff_AllInOne.png"), 2) #Check if the toggle is off
            wait(1)          
            pass     

    def test_ConfigureCompoundMicroscope04_SetCompoundObjectives(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_CompoundModulIcon_AllInOne.png")
            wait(1)
            # set objective values for the revolver       
            for i in range(1,8):
                click("mat_CompoundObjDropDownButt_AllInOne.png") # click drop down button to select objectives
                wait(1)
                offsetY = i * 40
                click(Location(1085, 560 + offsetY)) #click different objectives
                wait(1)
                click("mat_NextObjectiveButt_AllInOne.png")     #click button to select next objective position
                wait(1)
                Debug.user("Compound objective pos. %d , location X %d , location Y %d", i , 1085 , 560 + offsetY )
                wait(1)

            self.assertTrue(exists("mat_CompundObjectives_AllInOne.png"), 2) #It is possible that the final displayed results are different in different devices
            pass                

    def test_ConfigureCompoundMicroscope05_CompoundAdvanceSetting(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_CompoundAdvanceButt_AllInOne.png") #click advanced icon            
            wait(1)
            click(find(Pattern("mat_PageDownButtConfig_AllInOne.png").targetOffset(0,-20))) 
            wait(1)
            self.assertTrue(exists("mat_CamAdapterlb_AllInOne.png"), 2) #Check the exists of Camera Adapter
            wait(1)

            for i in range(1, 4):
                doubleClick(Pattern("mat_MagFactorlb_AllInOne.png").targetOffset(142, 0))
                wait(1)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)
                type(Key.BACKSPACE)

                wait(1)
                if i == 1:                 
                    type("0.009")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("mat_MagFactorMinVal_AllInOne.png"), 2) #It will jump to 0.01 when the value is too small
                    wait(1)
                if i == 2:                 
                    type("50")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("mat_MagFactorMaxVal_AllInOne.png"), 2) #50 is the max. value here
                    wait(1)
                if i == 3:                 
                    type("51")
                    wait(1)
                    type(Key.ENTER)
                    wait(1)
                    self.assertTrue(exists("mat_MagFactorVal_AllInOne.png"), 2) #It will jump to 1 when the value is too big
                    wait(1)
            pass

    def test_ConfigureCompoundMicroscope06_NotificationWithoutSaving(self):
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            click("mat_CloseButt_AllInOne.png") #click the x button without save
            wait(1)
            self.assertTrue(exists("mat_NotificationConfigUnsave_AllInOne.png"), 2)
            wait(1)
            click(find("mat_SaveConfigButt_AllInOne.png"))
            wait(1)                    
            pass

    def test_ConfigureCompoundMicroscope07_ShowRevolverInLive(self):
        wait(1)
        with Region(App.focusedWindow()):   
            click(Pattern("mat_AnnoLiveButt_AllInOne.png").targetOffset(0, -60)) #click the objective button over the annotation button
            wait(1)
            self.assertTrue(exists(Pattern("mat_RevolverLive_AllInOne.png").similar(0.50)),2) #set the similar 50 in case the different background and the objective value
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab
            wait(1)
            pass
            


    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ConfigureCompoundMicroscope)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()