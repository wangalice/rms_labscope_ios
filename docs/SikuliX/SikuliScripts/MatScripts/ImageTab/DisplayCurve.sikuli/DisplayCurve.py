# Owner:Kiki 2017-09-12
# Last Modified: Lily 2017-10-09
# Remark:Test for display curve function

labWindow = App.focusedWindow()
class DisplayCurve(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)                   
    def test_DisplayCurve01_AddDisplayCurveIcon(self):                  
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("mat_SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break      
            wait(1) 
            click("mat_DisplaySettingIcon_AllInOne.png")
            with Region(App.focusedWindow()):
                wait(2)
                self.assertTrue(exists("mat_DSDisplayCurveIcon_AllInOne.png"), 2) #Check if exists the icon for display curve
 
            
    def test_DisplayCurve02_DisplayCurveStateUnchangeWhenSwitchTab(self):                  
        wait(1)
        with Region(App.focusedWindow()):        
            click("mat_DSDisplayCurveIcon_AllInOne.png")
            wait(3)
            click(Location(516, 198))
            wait(2)
            EnterMicTab(self)
            wait(2)
            click("mat_ImageTab_AllInOne.png")
            wait(2)
            click("mat_DisplaySettingIcon_AllInOne.png")
            wait(1)
            self.assertTrue(exists(Pattern("mat_DSDisplayCurvePressedIcon_AllInOne.png").similar(0.99)))            

    def test_DisplayCurve03_DisplayCurveStateUnchangeWhenOpenOtherImage(self):
        EnterFilesTab(self)
        doubleClick("mat_CZIFormatImage_AllInOne.png")
        wait(1)
        click("mat_DisplaySettingIcon_AllInOne.png")
        wait(2)
        self.assertTrue(exists(Pattern("mat_DSDisplayCurvePressedIcon_AllInOne.png").similar(0.99)))    

        click(Location(596, 310))
        wait(1)
        click("mat_DisplaySettingIcon_AllInOne.png")
        wait(1)
        click("mat_DSDisplayCurveIcon_AllInOne.png")
        wait(1)
        click(Location(1473, 214))
        wait(1)
        EnterMicTab(self)          

                
def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(DisplayCurve)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 
