# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Mirror Image

labWindow = App.focusedWindow()
class MirrorImage(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_MirrorImage01_AddMirrorButton(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            count = 0
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break
            
            wait(1)
            click("mat_AnnoLiveButt_AllInOne.png")
            wait(1)
            click("mat_AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1000, 342),Location(1350, 608))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Rotate image\n" + currTime)
            wait(1)
            click(Location(40, 679))
            wait(1)
            click("mat_AnnoConfirm_AllInOne.png")
            wait(2)            
            click("mat_ImageProcessButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_MirrorButt_AllInOne.png")) #Show mirror button        
            pass
        
    def test_MirrorImage02_ShowWarningIfImageWithAnnotations(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click("mat_MirrorButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_ProcessWarning_AllInOne.png")) #show warning message
            pass


    def test_MirrorImage03_ExistHorizontal(self):
        wait(1)
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1)             
            wait(1)
            click("mat_ImageProcessWarningOKButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_MirrorHorizontal_AllInOne.png")) #Preview horizontal
            pass

    def test_MirrorImage04_ExistHorizontalVertical(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_MirrorHorizontalVertical_AllInOne.png")) #Preview Horizontal + Vertical
            pass
        
    def test_MirrorImage05_ExistVertical(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            self.assertTrue(exists("mat_MirrorVertical_AllInOne.png")) #Preview Vertical
            pass
        
    def test_MirrorImage06_ShowUndoRedoButton(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            click(Location(1416, 519)) # choose Horizontal + Vertical
            wait(1)
            click("mat_AnnoConfirm_AllInOne.png") 
            wait(1)
            self.assertTrue(exists("mat_UndoRedoButt_AllInOne.png")) #Show UndoRedo button
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) # Go to microscope tab
            wait(1)
            click("mat_SaveConfigButt_AllInOne.png")
            wait(1)
            click("mat_NotRememberPIN_AllInOne.png")
            wait(1)
            pass

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(MirrorImage)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











