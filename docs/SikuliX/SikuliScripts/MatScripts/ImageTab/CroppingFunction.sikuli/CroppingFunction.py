# Owner:Kiki 2017-09-12
# Last Modified:Lily 2017-09-30
# Remark:Test for cropping function in image tab


class CroppingFunction(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)            
        
    def test_CroppingFunction01_DisplayThreeCropShapes(self):                  
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("mat_SnapButt_AllInOne.png"))
            count = 0
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1) 
                #if wait the share button for too long, consider snap is failed
                if count > 10:
                    break
            
            wait(1)
            click("mat_AnnoLiveButt_AllInOne.png")
            wait(1)
            click("mat_AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(500, 342),Location(800, 608))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Crop image\n" + currTime)
            wait(1)
            click(Location(40, 679))
            wait(1)
            click("mat_AnnoConfirm_AllInOne.png")
            wait(2)            
            click("mat_ImageProcessButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_CropShape_AllInOne.png")) #Show crop button        
            pass
        
    def test_CroppingFunction02_PopUpMessageWhenCropWithAnnotations(self):
        click("mat_CropSquare_AllInOne.png")
        wait(1)
        self.assertTrue(exists("mat_ProcessWarning_AllInOne.png"))
        wait(1)
        click("mat_ImageProcessWarningCancelButt_AllInOne.png")
        wait(2)
        click("mat_CropTriangle_AllInOne.png")
        self.assertTrue(exists("mat_ProcessWarning_AllInOne.png"))
        wait(1)
        click("mat_ImageProcessWarningCancelButt_AllInOne.png")
        wait(2)
        click("mat_CropCircle_AllInOne.png")
        wait(1)
        self.assertTrue(exists("mat_ProcessWarning_AllInOne.png"))
        wait(1)
        click("mat_ImageProcessWarningOKButt_AllInOne.png")
        wait(2)
        click("mat_ConfirmFunctionbarButt_AllInOne.png")
        wait(1)
        click(Pattern("mat_MicroscopeTab_AllInOne.png")) # Go to microscope tab
        wait(1)
        click("mat_SaveConfigButt_AllInOne.png")
        wait(1)
        click("mat_NotRememberPIN_AllInOne.png")
        wait(1)
        pass
        
        

def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(CroppingFunction)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 
