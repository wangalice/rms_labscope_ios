# Owner:Kiki 2017-09-12
# Last Modified:Lily 2017-10-09
# Remark:Test for CSV file

labWindow = App.focusedWindow()
class OperateAboutCSVFile(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)   
    def test_CSVFile01_AddCSVButt(self):                  
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            EnterFilesTab(self)
            click("mat_FilterIcon_AllInOne.png")
            wait(1)
            click("mat_FilterByImage_AllInOne.png")
            wait(1)
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type("TempFolder")
            wait(1)
            doubleClick(Location(391, 150))            
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)
            doubleClick("mat_ImageNoAnnotation_AllInOne.png")
            wait(1)
            click("mat_ReportButton_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_PopUpOfReportButt_AllInOne.png"))

        
    def test_CSVFile02_GenerateCSVWithNoAnnotation(self):
        wait(1)
        click("mat_CSVButt_AllInOne.png")
        wait(1)
        self.assertTrue(exists("mat_PopUpOfNoMeasurement_AllInOne.png"))
        wait(1)
        click("mat_ZoomClickOKButt_AllInOne.png")
    
    def test_CSVFile03_GenerateCSVWithAnnotationsAndAutoSaved(self):
        EnterFilesTab(self)
        doubleClick("mat_ImageAllAnnotations_AllInOne.png")
        wait(1)
        click("mat_ReportButton_AllInOne.png")
        wait(1)
        click("mat_CSVButt_AllInOne.png")
        wait(1)
        click("mat_CloseCSVFile_AllInOne.png")
        EnterFilesTab(self)
        click("mat_FilterIcon_AllInOne.png")
        wait(1)
        click("mat_FilterByReport_AllInOne.png")
        wait(1)
        self.assertTrue(exists("mat_CSVFileInFilesTab_AllInOne.png"))

        doubleClick("mat_CSVFileInFilesTab_AllInOne.png")
        wait(2)
        click("mat_CloseCSVFile_AllInOne.png")
        wait(1)
        click("mat_CSVFileInFilesTab_AllInOne.png")   #select AllAnnotations.CSV file
        wait(1)
        
        click("mat_GarbageIcon_AllInOne.png")
        wait(1)
        click("mat_DeleteButtInGarbage_AllInOne.png")
        wait(2)
        click("mat_FilterIcon_AllInOne.png")
        wait(1)
        click("mat_FilterByImage_AllInOne.png")
        wait(1)
        click("mat_BackButt_AllInOne.png")
        EnterMicTab(self)      
    
def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)       

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OperateAboutCSVFile)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 