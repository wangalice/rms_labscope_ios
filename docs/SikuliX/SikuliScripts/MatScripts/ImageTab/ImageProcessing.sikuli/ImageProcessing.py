# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Image processing

labWindow = App.focusedWindow()
class ImageProcessing(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_ImageProcessing01_ShowShareButtonAfterOpenTheImage(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_FilesTab_AllInOne.png")) #select the files tab
            wait(3)
            doubleClick("mat_CZIFormatImage_AllInOne.png") # Click to open the image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break   
            self.assertTrue(exists("mat_ShareImageTab_AllInOne.png")) # Show share button when open the image
            pass                

    def test_ImageProcessing02_ShowImageProcessingPanel(self):      
        wait(1)
        with Region(App.focusedWindow()):   
            wait(1)
            click("mat_ImageProcessButt_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_ImagePorcessPanel_AllInOne.png")) # Show image process panel
            pass

    def test_ImageProcessing03_MoveImageProcessingSliders(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            thumbs = findAll("mat_ImageProcessSliderButt_AllInOne.png")
            for t in list(thumbs):
                dragDrop(t, t.getCenter().right(100))
            wait(1)
            click("mat_ConfirmFunctionbarButt_AllInOne.png")
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab        
            wait(1)
            click("mat_SaveConfigButt_AllInOne.png")
            wait(1)
            click("mat_NotRememberPIN_AllInOne.png")
            wait(1)
            pass


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ImageProcessing)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











