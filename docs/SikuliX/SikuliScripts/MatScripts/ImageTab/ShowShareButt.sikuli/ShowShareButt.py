# Owner:Kiki 2017-09-08
# Last Modified:Kiki 2017-09-08
# Remark:Test for open file in file browser


class ShowShareButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_ShowShareButt01_ShowShareButt(self):                  
        wait(1)
        EnterFilesTab(self)
        with Region(App.focusedWindow()):
            doubleClick(Location(406, 293))
            wait(3)
            self.assertTrue(exists("ShareButt.png"),FOREVER)
            pass
            click("ShareButt.png")
            wait(3)
            self.assertTrue(exists("ShareMenuInImageTab_Win7.64.png"),FOREVER)
            pass
            EnterMicTab(self)
def EnterFilesTab(self):
    wait(1)
    click("FilesTab.png")
    wait(1)
    self.assertTrue(exists("SortingOrder.png"),FOREVER)
    pass
def EnterMicTab(self):
    wait(1)
    click("MicTabButt.png")
    wait(1)        

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowShareButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 