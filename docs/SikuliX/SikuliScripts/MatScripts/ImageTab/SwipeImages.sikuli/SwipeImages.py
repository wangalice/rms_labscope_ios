# Owner: Lily 2017-09-20
# Last Modified: Lily 2017-09-20
# Remark: Swipe images

labWindow = App.focusedWindow()
class SwipeImages(unittest.TestCase):
    #inital
    def setUp(self):
        pass
    #clean up
    def tearDown(self):
        pass

    def test_SwipeImages01_ShowNextArrow(self):      
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            wait(1)
            click(Pattern("mat_MicroscopeTab_AllInOne.png").targetOffset(160, 50)) #select the first camera
            wait(3)
            click(Pattern("mat_SnapButt_AllInOne.png")) # Snap an image
            wait(1)
            count=0 # Add a count , in case can not find the icon
            while not exists("mat_ShareImageTab_AllInOne.png"):
                count = count + 1
                wait(1)
                if count > 5:
                    break      
            wait(1) 
            hover(Location(App.focusedWindow().getW()-30 ,200))
            wait(1)
            self.assertTrue(exists("mat_NextImageArrow_AllInOne.png")) #Show next Arrow
            wait(1)
            click("mat_NextImageArrow_AllInOne.png")

    def test_SwipeImages02_ShowPreviewArrow(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)           
            self.assertTrue(exists("mat_PreviewImageArrow_AllInOne.png")) #Show preview arrow      

    def test_SwipeImages03_ClickArrowToSwipeImages(self):
        wait(1)
        with Region(App.focusedWindow()):
            wait(1)
            for i in range(20):
                if exists("mat_NextImageArrow_AllInOne.png"):
                    click("mat_NextImageArrow_AllInOne.png")
                else:
                    click(Location(labWindow.getW()-20 ,labWindow.getH()/2))       
                wait(2)
            for i in range(10):
                if exists("mat_PreviewImageArrow_AllInOne.png"):
                    click("mat_PreviewImageArrow_AllInOne.png")
                else:
                    click(Location(95 ,labWindow.getH()/2))  
                wait(2)
            wait(1)            

        
    def test_SwipeImages04_SwipeUnsavedImage(self):
        wait(1)
        with Region(App.focusedWindow()):
            while not exists("mat_ShareImageTab_AllInOne.png"):
                 click("mat_NextImageArrow_AllInOne.png")
            wait(1)
            click("mat_AnnoLiveButt_AllInOne.png")
            wait(1)
            click("mat_AnnoText_AllInOne.png")
            wait(1)
            dragDrop(Location(1000, 342),Location(1350, 608))
            wait(1)
            currTime = time.asctime(time.localtime(time.time()))
            type("Swiping unsaved image\n" + currTime)
            wait(1)
            click(Location(40, 679))
            wait(1)
            click("mat_AnnoConfirm_AllInOne.png")
            wait(2)
            hover(Location(App.focusedWindow().getW()-30 ,200))
            wait(1)
            click(Location(App.focusedWindow().getW()-20 ,App.focusedWindow().getH()/2)) 
         
            wait(2)
            self.assertTrue(exists("mat_AskOnSaveWindowSwipe_AllInOne.png")) #Pop up ask on save window
            wait(1)
            click("mat_SaveConfigButt_AllInOne.png")
            wait(1)
            click("mat_NotRememberPIN_AllInOne.png")
            wait(2)
            click(Pattern("mat_MicroscopeTab_AllInOne.png")) #go back to microscope tab, in this situation has sometimes a little delay so wait 3s
            wait(3)               
 
        
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time" + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(SwipeImages)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 











