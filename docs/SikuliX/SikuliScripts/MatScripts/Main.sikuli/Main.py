# Owner: Lily 2017-09-19
# Last Modified: Lily 2017-10-09
# Remark: Before running the whole test, please reset the Mabscope,set the language to English,reset the MNA
#         Copy the TempFolder under the base folder. Set the right for the AccessDeniedImage to access denied

from sikuli import *
import os
import sys
import HTMLTestRunner
import unittest
import time


imgPath = list(getImagePath())

#Default logging settings
Settings.DebugLogs = True
Settings.LogTime = True
Debug.setLogFile("C:\\SikuliScripts\\ErrorLog\\mat_MainDefaultLog.txt")

#User logging settings
Debug.setUserLogFile("C:\\SikuliScripts\\ErrorLog\\mat_MainUserLog.txt")
#Unit test file path
log_file = 'C:\\SikuliScripts\\ErrorLog\\mat_log_unitTestfile.txt'

matscope = App("Matscope")
matscopePath="C:\\Program Files (x86)\\Carl Zeiss\\Matscope\\Matscope.exe"
class Main():
    if 'C:\\SikuliScripts\\MatImages' not in imgPath:
        addImagePath('C:\\SikuliScripts\\MatImages')
    wait(1)
    if 'C:\\SikuliScripts\\MatImages\\AllInOne' not in imgPath:
        addImagePath('C:\\SikuliScripts\\MatImages\\AllInOne')
    wait(1)    
    if 'C:\\SikuliScripts\\MatImages\\Win7_64_Precision' not in imgPath:
        addImagePath('C:\\SikuliScripts\\MatImages\\Win7_64_Precision')    
    for path in imgPath:
        print path
        Debug.user("imgPath %s", path)
    wait(1)

    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("Start time" + localtime + "\n")
    wait(1)
    openApp(matscopePath)
    count=0
    # Find the Logo to show the Matscope is launched
    while not exists("mat_ZeissLogo_AllInOne.png"):
        count += 1
        wait(2)   
        # if can not find the icon for long, exit the test
        if count > 5:
            Debug.user("icon ZeissLogo_AllInOne.png was not found")
            popError("icon ZeissLogo_AllInOne.png was not found") 
            exit(1)

    #matscope.focus()
    matWindow = App.focusedWindow()
    print matWindow.getX(), matWindow.getY(), matWindow.getW() , matWindow.getH()
    Debug.user("Application resolution is %d %d %d %d", matWindow.getX(), matWindow.getY(), matWindow.getW() , matWindow.getH())
    # minimium size of window is 1280*900
    # title.setROI(0,0,1280,900)
    with Region(matWindow):
        if exists("mat_ConfigButtMicTab_AllInOne.png"):
            click("mat_ConfigButtMicTab_AllInOne.png")
            wait(5)
        with Region(App.focusedWindow()):   
            App.focusedWindow().highlight(2)
            if exists("mat_ApplyButt_AllInOne.png"):
                click("mat_CloseButt_AllInOne.png")
                wait(1)
    matWindow.highlight(2)

    #region Microscope Tab
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ChangeMicroscopeName")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ConfigureCompoundMicroscope")
    wait(1)  
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ConfigureStereoMicroscope")
    wait(1)     
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\AddTwoVirtualMicroscopes")
    wait(1)     
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\LinkLabscopeToAFixedCamera")
    wait(1)     
    #runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\FindPrimoStarAndPrimovertModels")    Matscope has no Primostar and Primovert
    #wait(1)   
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ConfigurePrimotechMicroscope")    
    wait(1)  
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ConfigureScalingManually")    
    wait(1)     
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\LockConfigurationPage")    
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\MicroscopeTab\ShowConfiguratonBarCode")    
    wait(1)    
    
    #region Live Tab
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\DisplaySettingsInLiveTab")    
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\FullScreenModeInLive")    
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\SplitViewInLive")    
    wait(1)  
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\RecordInLive")    
    wait(1)      
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\AddAnnotationsInLive")    
    wait(1)      
    runScript(r"C:\SikuliScripts\MatScripts\LiveTab\MacroSnap")    
    wait(1)      
    
    #region Image Tab
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\ImageProcessing")
    wait(1)      
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\AddAnnotationsInImage")
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\SnapAndCloseImage")
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\ShareInImageTab")
    wait(1)    
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\SwipeImages")
    wait(1)   
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\FullScreenModeInImage")
    wait(1)      
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\SplitViewInImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\RotateImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\MirrorImage")
    wait(1)   
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\DisplaySettingsInImageTab")
    wait(1)        
    #runScript(r"C:\SikuliScripts\MatScripts\ImageTab\ChangeScreen")    
    #wait(1)
    #runScript(r"C:\SikuliScripts\MatScripts\ImageTab\CloseImage")    
    #wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\CroppingFunction")    
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\DisplayCurve")    
    #wait(1)
    #runScript(r"C:\SikuliScripts\MatScripts\ImageTab\OpenImageInFileBrowser")    
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\ImageTab\OperateAboutCSVFile")    
    #wait(1)
    #runScript(r"C:\SikuliScripts\MatScripts\ImageTab\ShowShareButt")    
    wait(1)
     
    #region File Tab
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\CopyButt")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\MoveButt")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\ShareButt")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\ShowMetadata")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\ChangeNameOfFiles")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\GarbageIcon")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\OpenFile")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\OperateOfFolder")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\SelectButtInFilesTab")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\ShowExplorerButt")
    wait(1)
    runScript(r"C:\SikuliScripts\MatScripts\FilesTab\SortingOrder")
    wait(1)
    
    labscope.close()
    localtime = time.asctime(time.localtime(time.time()))
    file.write("End time" + localtime + "\n")
    file.close()