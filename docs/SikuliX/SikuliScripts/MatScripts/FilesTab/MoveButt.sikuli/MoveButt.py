# Owner:Kiki 2017-08-17
# Last Modified: Lily 2017-10-09
# Remark:Test for Move button in files tab

labWindow = App.focusedWindow()
class MoveButt(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
      
    def test_MoveButt01_ClickSelectAllButt(self):                  
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            EnterFilesTab(self)    
            try:
                click("mat_SelectAllButt_AllInOne.png") 
                wait(1)
                self.assertTrue(exists("mat_MoveButt_AllInOne.png"))                         
            except FindFailed:
                Debug.user("Can't find move button")            

    def test_MoveButt02_PopupToSelectPathForMultiSelection(self):
        wait(1)            
        ClickMoveButt(self)
        self.assertTrue(exists("mat_PopUpAboutMove_AllInOne.png"))
        wait(1)
        type(Key.ESC)          
        EnterMicTab(self)   

    def test_MoveButt03_ClickCheckBox(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            try:
                click("mat_CheckboxFilesTab_AllInOne.png")
                wait(1)
                self.assertTrue(exists("mat_MoveButt_AllInOne.png"))
            except FindFailed:
                Debug.user("Can't find move button")

    def test_MoveButt04_PopupToSelectPathForSelectOneImage(self):        
        ClickMoveButt(self)
        self.assertTrue(exists("mat_PopUpAboutMove_AllInOne.png"))
        wait(1)
        type(Key.ESC)  
        EnterMicTab(self)    
        
def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  
    
def ClickMoveButt(self):
    wait(1)
    click("mat_MoveButt_AllInOne.png")
    wait(1)

    
    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(MoveButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()             