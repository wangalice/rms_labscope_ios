# Owner:Kiki 2017-08-21
# Last Modified:Kiki 2017-08-21
# Remark:Test for Operate of folder in files tab
class OperateOfFolder(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_OperateOfFolder01_DoubleClick(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            ShowFolderInTop(self)                                    
            doubleClick(Pattern("mat_FolderIconInFilesTab_AllInOne.png").targetOffset(200,0))
            wait(1)
            self.assertTrue(exists("mat_BackButt_AllInOne.png"))
            wait(1)
            click("mat_BackButt_AllInOne.png")
            wait(2)  
           
    def test_OperateOfFolder02_UseEnterAndBackspace(self):
        doubleClick(Pattern("mat_FolderIconInFilesTab_AllInOne.png").targetOffset(200,0))    
        wait(2)
        type(Key.ENTER)
        self.assertTrue(exists("mat_BackButt_AllInOne.png"),2)
        wait(2) 
        type(Key.BACKSPACE)
        wait(2)
        
    def test_OperateOfFolder03_NoMoveCopyDeleteShareButt(self):
        click(Pattern("mat_FolderIconInFilesTab_AllInOne.png").targetOffset(200,0))    
        wait(1)
        self.assertTrue(exists("mat_TheFunctionBarWhenSelectFolder_AllInOne.png"))    
        wait(2)
        
    def test_OperateOfFolder04_NoCheckedBoxWhenSelectFolder(self): 
        click("mat_SelectAllButt_AllInOne.png")
        wait(1)
        self.assertTrue(exists("mat_FolderIconInFilesTab_AllInOne.png"),2)
        wait(2)
        EnterMicTab(self)
        
def ShowFolderInTop(self):
    click("mat_SortingOrder_AllInOne.png")
    wait(1)
    click("mat_SortByNameDescending_AllInOne.png")
    wait(2)
    click("mat_SortingOrder_AllInOne.png")
    wait(1)
    click("mat_SortByNameAscending_AllInOne.png")

def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OperateOfFolder)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()    