# Owner:Kiki 2017-08-18
# Last Modified:Kiki 2017-08-18
# Remark:Test for Change file s name in files tab

labWindow = App.focusedWindow()
class ChangeFileName(unittest.TestCase):
    def setUp(self):      
        wait(1)        
    def tearDown(self):
        wait(1)
    def test_ChangeFileName01_ChangeTwoFileWithSameName(self):
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):        
            EnterFilesTab(self)        
            click("mat_FilterIcon_AllInOne.png")
            wait(1)
            click("mat_FilterByImage_AllInOne.png")
            wait(1) 
            click("mat_SortingOrder_AllInOne.png")
            wait(1) 
            click("mat_SortByNameAscending_AllInOne.png")
            wait(1) 
            click("mat_SearchButton_AllInOne.png") #to search all czi files
            wait(1)
            type('.czi')
            wait(1)
            rightClick("mat_CheckboxFilesTab_AllInOne.png") #popup metadata
            wait(1)
            click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
            wait(1)
            type('a',KEY_CTRL)
            wait(1)
            type(Key.BACKSPACE)
            wait(2)
            type('ChangeFileName')
            wait(1)
            click("mat_SaveFileName_AllInOne.png")
            wait(1)
            type(Key.ESC)
            wait(1)
            click("mat_SortingOrder_AllInOne.png")
            wait(1)
            click("mat_SortByNameDescending_AllInOne.png") #Change the sort and choice different file in second time
            wait(1)
            rightClick("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            #match=find("PopUpOfMetadata_Win7.64.png").below().find("FilesNameTextBox_Win7.64.png").click()
            click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
            wait(1)
            type('a',KEY_CTRL)
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            type('ChangeFileName')
            wait(1)
            click("mat_SaveFileName_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_PopUpAboutSameFileName_AllInOne.png"))
            wait(1)
            click("mat_OkToPopUpAboutSameFileName_AllInOne.png")            
            
            wait(2)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type('ChangeFileName')
            wait(1)
            click("mat_SelectAllButt_AllInOne.png")
            wait(1)
            click("mat_GarbageIcon_AllInOne.png")
            wait(1)
            click("mat_DeleteButtInGarbage_AllInOne.png")
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            EnterMicTab(self)
            
    def test_ChangeFileName02_TypeInvalidName(self):        
        EnterFilesTab(self)
        rightClick("mat_CheckboxFilesTab_AllInOne.png") #popup metadata
        wait(1)
        click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
        wait(1)
        type('a',KEY_CTRL)
        wait(1)
        type(Key.BACKSPACE)
        wait(1)
        type('\/:*<>"|?')
        wait(3)
        self.assertTrue(exists("mat_EmptyFilesName_AllInOne.png")) 
        EnterMicTab(self)
        

def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  
     
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ChangeFileName)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()     
              