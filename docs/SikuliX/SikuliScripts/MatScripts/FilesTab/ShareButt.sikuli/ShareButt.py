# Owner:Kiki 2017-08-17
# Last Modified:Kiki 2017-08-17
# Remark:Test for Share butt in files tab

labWindow = App.focusedWindow()
class ShareButt(unittest.TestCase):
    def setUp(self):             
        wait(1)                        
    def tearDown(self):
        wait(1)        
    def test_ShareButt01_ClickSelectAllButt(self):                          
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):
            App.focusedWindow().highlight(1) 
            EnterFilesTab(self)    
            click("mat_FilterIcon_AllInOne.png")
            wait(1)
            click("mat_FilterByImage_AllInOne.png")
            wait(1) 
            click("mat_SelectAllButt_AllInOne.png")  
            wait(1)
            self.assertTrue(exists("mat_ShareButt_AllInOne.png"))
            wait(1)                              
                                     

    def test_ShareButt02_PopupToSelectPathForMultiSelection(self):
        ClickShareButt(self)                 
        wait(1)          
        self.assertTrue(exists("mat_PopUpAboutShare_AllInOne.png"))
        wait(1)
        click("mat_SaveAsButt_AllInOne.png")
        while not exists("mat_SaveAsType_AllInOne.png"):
            wait(1)
        type(Key.ESC)            
        
    def test_ShareButt03_ClickCheckBox(self):                
        with Region(App.focusedWindow()):
            try:
                click("mat_CheckboxFilesTab_AllInOne.png")
                wait(1)
                self.assertTrue(exists("mat_ShareButt_AllInOne.png"))           
                wait(1)  
            except FindFailed:
                Debug.user("Can't find share button")
                
    def test_ShareButt04_PopupToSelectPathForSelectOneImage(self):        
        ClickShareButt(self)                 
        wait(1)          
        self.assertTrue(exists("mat_PopUpAboutShare_AllInOne.png"))
        wait(1)
        click("mat_SaveAsButt_AllInOne.png")
        while not exists("mat_SaveAsType_AllInOne.png"):
            wait(1)
        type(Key.ESC)       
        
    def test_ShareButt05_NoShareButtWithNoFilesSelected(self):
        wait(1)
        with Region(App.focusedWindow()):
            EnterMicTab(self)
            wait(1)
            EnterFilesTab(self)
            self.assertTrue(exists("mat_TheFunctionBarWhenSelectFolder_AllInOne.png"))           
            EnterMicTab(self)

def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  
    
def ClickShareButt(self):
    wait(1)
    click("mat_ShareButt_AllInOne.png")
    wait(1)
    
    
def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShareButt)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()
                
     
         
                    
          
            
            
        