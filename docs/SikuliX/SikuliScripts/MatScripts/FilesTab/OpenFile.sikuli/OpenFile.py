# Owner:Kiki 2017-08-21
# Last Modified:Lily 2017-10-13
# Remark:Test for Open File in files tab (make access denied image name as "AccessDeniedImage" in files tab)

class OpenFile(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
        
    def test_OpenFile01_DoubleClick(self):                  
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            click("mat_FilterIcon_AllInOne.png")
            wait(1)
            click("mat_FilterByImage_AllInOne.png")
            wait(1)
            rightClick("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
            type('a',KEY_CTRL)
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            type("Kiki's file")
            wait(1)
            type('a',KEY_CTRL)
            wait(1)
            type('c',KEY_CTRL)
            wait(1)
            click("mat_SaveFileName_AllInOne.png")
            wait(1)
            type(Key.ESC)    
            wait(1)
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type('v',KEY_CTRL)
            wait(1)
            for i in range(2):
                doubleClick(Location(391, 150))       
                wait(1)
                self.assertTrue(exists("mat_ReportButton_AllInOne.png"))                 
                wait(1)
                click("mat_CloseButtImage_AllInOne.png")                 
                wait(1)
             
    def test_OpenFile02_UseEnter(self):              
        with Region(App.focusedWindow()): 
            for i in range(2):
                click(Location(391, 150))         
                wait(1)
                type(Key.ENTER)     
                wait(1)
                self.assertTrue(exists("mat_ReportButton_AllInOne.png"))                 
                wait(1)                              
            click("mat_CloseButtImage_AllInOne.png") 
            wait(2)
            click("mat_SelectAllButt_AllInOne.png")
            wait(1)
            click("mat_GarbageIcon_AllInOne.png")
            wait(1)
            click("mat_DeleteButtInGarbage_AllInOne.png")
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(2)
            
    def test_OpenFile03_OpenAndChangeNameOfAccessDeniedImage(self):
        with Region(App.focusedWindow()):
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type("TempFolder")
            wait(1)
            doubleClick(Location(391, 150))            
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)            
            click("mat_SearchButton_AllInOne.png")
            wait(1)            
            type("AccessDeniedImage")
            wait(1)
            click("mat_CheckboxFilesTab_AllInOne.png")            
            wait(1)
            type(Key.ENTER)
            self.assertTrue(exists("mat_AlertMessageAboutOpenFile_AllInOne.png"))                          
            EnterFilesTab(self)
            rightClick("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
            wait(1)
            type("test")
            wait(1)
            click("mat_SaveFileName_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_PopUpOfChangeAccessDeniedImageName_AllInOne.png"))                        
            wait(2)
            type(Key.ENTER)
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            EnterMicTab(self)
            
    def test_OpenFile04_OpenUnsupportImage(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type("UnsupportImage")
            wait(1)
            click("mat_CheckboxFilesTab_AllInOne.png")
            type(Key.ENTER)
            self.assertTrue(exists("mat_AlertMessageAboutOpenUnsupportImage_AllInOne.png"))            
            EnterFilesTab(self)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)
            click("mat_BackButt_AllInOne.png")            
            EnterMicTab(self)
            
def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)   


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(OpenFile)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main() 