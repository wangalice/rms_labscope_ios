# Owner:Kiki 2017-08-16
# Last Modified:Lily 2017-10-12
# Remark:Test for Copy butt in files tab
labWindow = App.focusedWindow()
class ShowMetadata(unittest.TestCase):
    def setUp(self):              
        wait(1)    
    def tearDown(self):
        wait(1)
    def test_ShowMetadata01_PopupMetadata(self):       
        wait(1)
        Debug.user("Application resolution is %d %d %d %d", labWindow.getX(), labWindow.getY(), labWindow.getW(), labWindow.getH())
        with Region(App.focusedWindow()):        
            EnterFilesTab(self)
            wait(2)
            rightClick("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_PopUpOfMetadata_AllInOne.png"))          

    def test_ShowMetadata02_DismissByESC(self):    
        wait(2)        
        type(Key.ESC)
        wait(2)
        self.assertFalse(exists("mat_PopUpOfMetadata_AllInOne.png"))      
        EnterMicTab(self)

def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  

def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(ShowMetadata)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()
    