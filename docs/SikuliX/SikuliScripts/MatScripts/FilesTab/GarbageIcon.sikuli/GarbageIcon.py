# Owner:Kiki 2017-08-18
# Last Modified:Lily 2017-10-13
# Remark:Test for Garbage icon in files tab

class GarbageIcon(unittest.TestCase):
    def setUp(self):      
        wait(1)
    def tearDown(self):
        wait(1)
    def test_GarbageIcon01_clickSelectAllButt(self):                  
        EnterFilesTab(self)        
        wait(1)
        with Region(App.focusedWindow()):            
             click("mat_SelectAllButt_AllInOne.png")
             self.assertTrue(exists("mat_GarbageIcon_AllInOne.png"))
             wait(1)
             click("mat_GarbageIcon_AllInOne.png")
             wait(1)
             self.assertTrue(exists("mat_DeleteButtInGarbage_AllInOne.png"))
             wait(1)
             EnterMicTab(self)
            
    def test_GarbageIcon02_clickCheckBox(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):       
            
            click("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_GarbageIcon_AllInOne.png"))
            wait(3)
            rightClick("mat_CheckboxFilesTab_AllInOne.png")
            wait(1)
            click(Pattern("mat_PopUpOfMetadata_AllInOne.png").targetOffset(20,30))
            type('a',KEY_CTRL)
            wait(1)
            type(Key.BACKSPACE)
            wait(1)
            type("Kiki's file")
            wait(1)
            type('a',KEY_CTRL)
            wait(1)
            type('c',KEY_CTRL)
            wait(1)
            click("mat_SaveFileName_AllInOne.png")
            wait(1)
            type(Key.ESC)                          
            wait(2)
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type('v',KEY_CTRL)
            try:
                wait(2)
                click("mat_CheckboxFilesTab_AllInOne.png")
                wait(1)
                self.assertTrue(exists("mat_GarbageIcon_AllInOne.png"))
            except FindFailed:
                Debug.user("Can't find checkbox button")
            click("mat_GarbageIcon_AllInOne.png")
            wait(1)
            click("mat_DeleteButtInGarbage_AllInOne.png")
            self.assertTrue(exists("mat_DeletedSpecialFileInFilesTab_AllInOne.png"),2)
            pass
            click("mat_SearchButtonPressed_AllInOne.png")                                                     
            wait(5)
            EnterMicTab(self)
             
    def test_GarbageIcon03_DeleteAccessDeniedImage(self):
        EnterFilesTab(self)
        wait(1)
        with Region(App.focusedWindow()):
            click("mat_FilterIcon_AllInOne.png")
            wait(1)
            click("mat_FilterByImage_AllInOne.png")
            wait(1)
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type("TempFolder")
            wait(1)
            doubleClick(Location(391, 150))            
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)            
            click("mat_SearchButton_AllInOne.png")
            wait(1)
            type("AccessDeniedImage")
            wait(1)
            click("mat_SelectAllButt_AllInOne.png")
            wait(1)            
            click("mat_GarbageIcon_AllInOne.png")
            wait(1)
            click("mat_DeleteButtInGarbage_AllInOne.png")
            wait(1)
            self.assertTrue(exists("mat_PopUpOfDeleteAccessDeniedImage_AllInOne.png"))            
            wait(1)
            type(Key.ESC)
            wait(1)
            click("mat_SearchButtonPressed_AllInOne.png")
            wait(1)
            click("mat_BackButt_AllInOne.png")            
            EnterMicTab(self)
            
            
def EnterFilesTab(self):
    wait(1)
    click("mat_FilesTab_AllInOne.png")
    wait(1)
   
def EnterMicTab(self):
    wait(1)
    click("mat_MicroscopeTab_AllInOne.png")
    wait(1)  


def main():
    file = open(log_file, "a+")
    localtime = time.asctime(time.localtime(time.time()))
    file.write("\nStart time " + localtime + "\n\n")    
    suit=unittest.TestLoader().loadTestsFromTestCase(GarbageIcon)
    test_result=unittest.TextTestRunner(file,verbosity=2).run(suit) 
    file.close()  
    
if __name__ == '__main__':
    main()       