#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
   // var waitTime = 4;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Playing live of Kappa camera!");
    window.collectionViews()[0].cells()["Kappa"].tap();

    for (var i = 1; i <= cycleNum; i++) {
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);
        target.delay(2);
        //go back to portrait mode
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
        target.delay(2);
        UIALogger.logMessage("Switching count: "+ i);
    }
    
}
test("switchDeviceOrientation", testFunc)