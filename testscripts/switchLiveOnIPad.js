#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
   // var waitTime = 4;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    var microscopeTab = window.buttons()["microscopeTab"];

    for (var i = 1; i <= cycleNum; i++) {
        UIALogger.logMessage("Playing live of Kappa camera!");
        window.scrollViews()[0].buttons()["Kappa"].tap();
        target.delay(2);
        microscopeTab.tap();

        UIALogger.logMessage("Playing live of Wision camera!");
        window.scrollViews()[0].buttons()["Wision"].tap();
        target.delay(2);
        microscopeTab.tap();
        
        UIALogger.logMessage("Switching count: "+ i);
    }
    
}
test("switchAndSnap", testFunc)