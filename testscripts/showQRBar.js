#import "tuneup/tuneup.js" 

var testFun = function(target, app){
    var micName = "Kappaold"; // change to test target microscope name
    
    var target = UIATarget.localTarget();
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
    
    UIALogger.logMessage("Play live of camera");
    window.scrollViews()[0].buttons()[micName].tap();
    target.delay(2);
        
    for (var i = 1; i <= cycleNum; i++) {
        UIALogger.logMessage("Tap the configuration button in live tab!");
        target.tap({x:41.00, y:500.00});
        
        target.delay(1);
        target.frontMostApp().navigationBar().rightButton().tap();
        target.delay(1);
        target.frontMostApp().navigationBar().leftButton().tap();
        target.delay(1);
        target.frontMostApp().navigationBar().leftButton().tap();
        target.delay(1);
        
        UIALogger.logMessage("Testing num: "+ i);
        target.delay(2);
    }

}
test("showQRBar", testFun)