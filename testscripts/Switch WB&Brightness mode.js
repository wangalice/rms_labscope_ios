#import "tuneup/tuneup.js"

var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 50;
    var firstCamera = window.scrollViews()[0].buttons()[0];
    var cameraControlButton = window.buttons()[57];

    target.delay(1);
    UIALogger.logMessage("Play live of available real camera!");
    firstCamera.tap();
	target.delay(2);

    for(var i=1; i<cycleNum ;i++) {

    target.delay(1);

    UIALogger.logMessage("Tap the button of camera control!");
    cameraControlButton.tap();
    target.delay(8);
		
	target.frontMostApp().mainWindow().buttons()["Auto / Manual"].tap();
    target.frontMostApp().mainWindow().buttons()["Manual"].tap();
	target.frontMostApp().mainWindow().buttons()["Auto"].tap();
	
    //UIALogger.logMessage("Dismiss the dialogue!");
    //target.tap({x:399.50, y:176.00});

	UIALogger.logMessage("Finish the "+ i + " times cycle!");
    
    }
}

test("continuious tapping camera control button", testFunc)
