#import "tuneup/tuneup.js"

var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
    var snapButton = window.buttons()["snap photo"];
    var closeButton = window.buttons()["closeIcon"];

    window.scrollViews()[0].buttons()[3].tap();
    for(var i=1; i<cycleNum ;i++)
   {
    snapButton.tap();
	UIALogger.logMessage("Current snap num: "+ i);
	target.delay(0.5);  
    closeButton.tap();
    UIATarget.onAlert = function onAlert(alert) {
        var title = alert.name();
        return true;
        }
	//While testing, the default button is "Save". 
    //So, if tester doesn't want to save images during testing,he should change the code.
    target.frontMostApp().alert().defaultButton().tap();
    }
}

test("stressTest for hardware-decoding", testFunc)


