#import "tuneup/tuneup.js"

test("displayTest", function(target, app) {
    
    var window = app.mainWindow();
    var cycleNum = 400;
    var delayTime = 0.5;
    var displayCurveButton = window.buttons()[15];
	
	var wisionCameraButton = window.scrollViews()[0].buttons()[3];
	 
    wisionCameraButton.tap();
    
    UIALogger.logMessage("Start testing");

    for (var i = 1; i <= cycleNum; i++) {
        
        UIALogger.logMessage("Current num of cycle:" + i);
        target.delay(delayTime);
	    displayCurveButton.tap();
	    target.delay(delayTime);
        target.tap({x:300, y:300});
	    
        target.delay(delayTime);
        displayCurveButton.tap();
        target.delay(delayTime);
        target.tap({x:700, y:300});

        target.delay(delayTime);
        displayCurveButton.tap();
        target.delay(delayTime);
        target.tap({x:300, y:450});

        target.delay(delayTime);
        displayCurveButton.tap();
        target.delay(delayTime);
        target.tap({x:700, y:450});
	 
	    if(cycleNum == i){
            UIALogger.logMessage("Finish testing");
        }
    }
})

