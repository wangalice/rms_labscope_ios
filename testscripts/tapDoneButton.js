
#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Playing live of Kappa camera!");
    window.collectionViews()[0].cells()["Kappa"].tap();
    target.delay(1);

    for (var i = 1; i <= cycleNum; i++) {
        UIALogger.logMessage("Tap Zeiss logo!");
        window.buttons()["zeiss logo"].tap();
        target.delay(2);
        UIALogger.logMessage("Tap Done button!");
        target.frontMostApp().navigationBar().leftButton().tap();
        target.delay(2);

        UIALogger.logMessage("Testing time: "+ i);
    }
}
test("tapDoneButton", testFunc)
