#import "tuneup/tuneup.js"
 
    var testFun = function(target, app){
    var target = UIATarget.localTarget();
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
    
    UIALogger.logMessage("Play live of Kappa camera");
    window.scrollViews()[0].buttons()["Kappa"].tap();
    target.delay(2);
    
    for (var i = 1; i <= cycleNum; i++) {

    window.buttons()["cameraControlIcon"].tap();
    target.delay(3);

	UIALogger.logMessage("Deactivating app");	
    target.deactivateAppForDuration(3);
    target.delay(3);
    UIALogger.logMessage("Deactivating num: "+ i);

    }
}
test("Deactivating app", testFun)

