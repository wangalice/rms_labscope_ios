#import "tuneup/tuneup.js"
 
var testFun = function(target, app){
    var target = UIATarget.localTarget();
    var cycleNum = 2000;

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
    
    for (var i = 1; i <= cycleNum; i++) {
    
    UIALogger.logMessage("Deactivating app");
		    target.delay(4);
		
    target.deactivateAppForDuration(5);
    UIALogger.logMessage("Resuming test after deactivation");
    UIALogger.logMessage("Deactivating num: "+ i);

    }
}
test("Deactivating app", testFun)   