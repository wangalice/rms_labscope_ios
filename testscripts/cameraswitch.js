#import "tuneup/tuneup.js"

var testFun = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 1000;
    var microscopeTab = window.buttons()[84];

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
	
for (var i = 1; i <= cycleNum; i++) {
    
    //Choose camera 1
    UIALogger.logMessage("Play live of Kappa camera.");
    window.scrollViews()[0].buttons()[0].tap();
    
    UIALogger.logMessage("Hold on for 2 seconds for camera connection");
    target.delay(2);
	
    //Go back to the microscope tab
    microscopeTab.tap();

    //Choose camera 2
    UIALogger.logMessage("Play live of Kappa camera.");
    window.scrollViews()[0].buttons()[1].tap();

    UIALogger.logMessage("Hold on for 2 seconds for camera connection");
    target.delay(2);

    //Go back to the microscope tab
    microscopeTab.tap();

    UIALogger.logMessage("Switching num: "+ i);

    }
}
test("CameraSwitchTest", testFun)
