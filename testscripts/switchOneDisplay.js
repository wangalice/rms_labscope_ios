#import "tuneup/tuneup.js"

test("displayTest", function(target, app) {
    
    var window = app.mainWindow();
    var cycleNum = 800;
    var delayTime = 0.5;
    
    window.scrollViews()[0].buttons()["thumbnail microscope bkg"].tap();
    
    UIALogger.logMessage("Start testing");

    for (var i = 1; i <= cycleNum; i++) {
        
        UIALogger.logMessage("Current num of cycle:" + i);

        target.delay(delayTime);
        window.buttons()["Display"].tap();
        target.delay(delayTime);
        target.tap({x:204.00, y:235.00});

        if(cycleNum == i){
            UIALogger.logMessage("Finish testing");
        }

    }
})