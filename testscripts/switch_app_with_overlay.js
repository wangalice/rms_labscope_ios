#import "tuneup/tuneup.js"
 
    var testFun = function(target, app){
    var target = UIATarget.localTarget();
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 1000;

    var displaySettingButt = window.buttons()["displaySettingButton"];
    var overlayButt = window.buttons()["overlayButton"];

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
    
    UIALogger.logMessage("Play live of camera");
    window.scrollViews()[0].buttons()[1].tap();
    target.delay(2);
    
    
        
    for (var i = 1; i <= cycleNum; i++) {
        displaySettingButt.tap();
        target.delay(1);
        overlayButt.tap();
        target.delay(1);

        UIALogger.logMessage("Deactivating app");	
        target.deactivateAppForDuration(2);
        target.delay(2);
        UIALogger.logMessage("Deactivating num: "+ i);

    }
}
test("Deactivating app", testFun)
