
#import "tuneup/tuneup.js"

test("memoryUsage", function(target, app) {
    var window = app.mainWindow();
    var cycleNum = 50;
    var isLiveVideoAvailable = 1;
    window.scrollViews()[0].buttons()["thumbnail microscope bkg"].tap();
    window.buttons()["snap photo"].tap();
    
    //If the iPad can play live stream, this scentence should be erased. And falg equals 1    and otherwise flag equals 0.
    if (isLiveVideoAvailable == 1) {     
        window.buttons()[28].tap();
    }     
    
    UIALogger.logMessage("Start testing");
    UIALogger.logMessage(Date());     
    
    for (var i = 0; i < cycleNum; i++) {
        UIALogger.logMessage("Set the image!");
        window.buttons()["Image"].tap();
        UIALogger.logMessage(Date());

        UIALogger.logMessage("Flip the image!");
        window.buttons()["flip vertical icon"].tap();
        UIALogger.logMessage(Date());

        UIALogger.logMessage("Apply the changes!");
        window.buttons()["Image"].tap();
        UIALogger.logMessage(Date());
    
        target.captureScreenWithName("tmp");
    }
});
