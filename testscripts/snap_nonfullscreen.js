#import "tuneup/tuneup.js"

var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 200;
    var snapButton = window.buttons()["snap photo"];
    var liveButton = window.buttons()[74];
    
	//User is now in the live tab.
    for (var i = 1; i <= cycleNum; i++) {
    	snapButton.tap();
    	target.delay(5);
    	liveButton.tap();
    }
}
test("snap_nonfullscreen",testFunc)


