#import "tuneup/tuneup.js"

var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 1000;
    var snapSuccessNum = 0;
    var snapErrorNum = 0;
    var waitTime = 1;
    
    UIALogger.logMessage("Hold on for 1 seconds for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Select Wision camera of hardware-decoding!");
    window.scrollViews()[0].buttons()[3].tap();
    
    UIALogger.logMessage("Hold on for 0.5 seconds for camera connection");
    target.delay(0.5);
    
    UIALogger.logMessage("Start testing");
    
    var snapButton = window.buttons()["snap photo"];
	var saveButton = window.buttons()["Save"];
	//var closeButton = target.frontMostApp().windows()[0].buttons()["closeIcon"];
    //var discardButton = target.frontMostApp().alert().buttons()["Discard"];
    var liveButton = window.buttons()[1];
    
    for (var i = 1; i <= cycleNum; i++) {
        target.delay(0.5);
        //target.captureScreenWithName("Captured image before snapping");
        
        UIALogger.logMessage("Snap image!");
        snapButton.tap();
        UIALogger.logMessage("Snapping num: "+ i);
        
        while (saveButton.isVisible() == 0 && waitTime < 11) {

            target.delay(1);
            UIALogger.logMessage("Have waited for " + waitTime + " seconds!");
            waitTime++;
        }

        if (waitTime == 11) {
            UIALogger.logMessage("Failed to snap a image!");
            snapErrorNum++;
        }

        waitTime = 1;
        
        if (saveButton.isVisible()) {
            //target.captureScreenWithName("Snapped image");
            UIALogger.logMessage("Save the snapped image.");
			saveButton.tap();
			window.popover().tap();
           // discardButton.tap();

            snapSuccessNum++;
            
            UIALogger.logMessage("Back to live video!");
            liveButton.tap();
        } 

        UIALogger.logMessage("Have failed to snap for " + snapErrorNum + " times.");
        UIALogger.logMessage("Have snapped successfully for " + snapSuccessNum + " times.");
    }
}

test("stressTest", testFunc)


