#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();

    var snapButton = window.buttons()["snap"];
    var annotationButton = window.buttons["annotationButton"];
    var filesTab = window.buttons()["filesTab"];
    
    //Play live of Kappa camera
    target.delay(1);
    UIALogger.logMessage("Playing live of Kappa camera!");
    window.scrollViews()[1].buttons()["micButtonKappa"].tap();
    
    //Snap an image from Kappa camera
    UIALogger.logMessage("Snap a new image from Kappa camera!");
    snapButton.tap();
    target.delay(10);
    
    filesTab.tap();
}
test("smokeTest", testFunc)

