#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
   // var waitTime = 4;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    //switch between apps, live tab to home
   // UIALogger.logMessage("Playing live of Kappa camera!");
   // window.collectionViews()[0].cells()["Kappa"].tap();
   // target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);

    //switch between apps, microscope tab to home
    //target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);

    for (var i = 1; i <= cycleNum; i++) {
        
        target.delay(2);
        //Deactivate the app for 3s
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
        target.deactivateAppForDuration(3);
        //target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);

        UIALogger.logMessage("Switching count: "+ i);
    }
    
}
test("switchBetweenApps", testFunc)