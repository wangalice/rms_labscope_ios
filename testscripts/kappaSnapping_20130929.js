#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
    var waitTime = 12;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Playing live of Kappa camera!");
    window.scrollViews()[0].buttons()["Kappa"].tap();
    
    UIALogger.logMessage("Hold on for 1 seconds for camera connection");
    target.delay(1);
    
    UIALogger.logMessage("Start testing");
    
    var snapButton = window.buttons()["snap"];
    var liveTab = window.buttons()["liveTab"];
    
    for (var i = 1; i <= cycleNum; i++) {
        target.delay(1);        
        UIALogger.logMessage("Snap image!");
        snapButton.tap();
        UIALogger.logMessage("Snapping num: "+ i);
        
        UIALogger.logMessage("Hold on for 12 seconds for saving the snapped image!");
        target.delay(waitTime);
      
        //target.delay(1);
		liveTab.tap();
    } 
}
test("stressTest", testFunc)
