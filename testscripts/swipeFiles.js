#import "tuneup/tuneup.js"

test("swipeFiles", function(target, app) {
    var window = app.mainWindow();
    var cycleNum = 50;
	var scrollNum = 0;

	target.frontMostApp().mainWindow().buttons()[57].tap();
        
	target.frontMostApp().mainWindow().tableViews()[0].cells()[0].tap(); //Open the first image in local files tab
    target.frontMostApp().mainWindow().buttons()["Annotate &\nMeasure"].tap();
	target.frontMostApp().mainWindow().buttons()["Done"].tap();
	 
    for(var i = 1; i <= 10; i++)
    {
     window.scrollViews()[1].scrollViews()[1].dragInsideWithOptions({startOffset:{x:0.5, y:0.1},endOffset:{x:0.0, y:0.1},duration:0.2});
	 target.frontMostApp().mainWindow().buttons()["Annotate &\nMeasure"].tap();
	 target.frontMostApp().mainWindow().buttons()["Done"].tap();
    }
 });
