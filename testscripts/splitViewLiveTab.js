var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();
var cycleNum = 1000;
//Connect to one camera
window.scrollViews()[0].buttons()[0].tap();
target.delay(3);
UIALogger.logStart("Split view in the live tab");

for (var i=1; i<= cycleNum; i++){
UIALogger.logMessage("The current cycele is:"+ i );
	
//reset orientation to portrait,with the bottom closest to the home button
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
target.captureScreenWithName("Portrait_bottom close to the home button");
UIALogger.logMessage("Current orientation now " + app.interfaceOrientation());
target.delay(3);
	
//reset orientation to landscape left,with the left side closest to the home button
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);
target.captureScreenWithName("Landscape_left close to the home button");
UIALogger.logMessage("Current orientation now " + app.interfaceOrientation());
target.delay(3);
	
//reset orientation to portrait,with the top closest to the home button
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT_UPSIDEDOWN);
target.captureScreenWithName("Portrait_top close to the home button");
UIALogger.logMessage("Current orientation now " + app.interfaceOrientation());
target.delay(3);
	
//reset orientation to landscape right,with the right side closest to the home button
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPERIGHT);
target.captureScreenWithName("Landscape_right close to the home button");
UIALogger.logMessage("Current orientation now " + app.interfaceOrientation());
target.delay(3);
}
