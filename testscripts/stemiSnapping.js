#import "tuneup/tuneup.js"

    
    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
    var waitTime = 3;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Playing live of motic camera!");
    window.scrollViews()[0].buttons()["Stemi305"].tap();
    
    UIALogger.logMessage("Hold on for 1 seconds for camera connection");
    target.delay(1);
    
    UIALogger.logMessage("Start snapping");
    
    var snapButton = window.buttons()["snap"];
    var liveTab = window.buttons()["liveTab"];
		
    for (var i = 1; i <= cycleNum; i++) {
        target.delay(1);        
        UIALogger.logMessage("Snap image!");
        snapButton.tap();
        UIALogger.logMessage("Snapping num: "+ i);
        
        UIALogger.logMessage("Hold on for 3 seconds for saving the snapped image!");
        target.delay(waitTime);

		liveTab.tap();
    } 
}
test("snappingMotic", testFunc)
