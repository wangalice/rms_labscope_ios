#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
    var waitTime = 6;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    UIALogger.logMessage("Playing live of Kappa camera!");
    window.collectionViews()[0].cells()["Wision"].tap();
    
    UIALogger.logMessage("Hold on for 1 seconds for camera connection");
    target.delay(1);
    var snapButton = window.buttons()["snapImage"];
    UIALogger.logMessage("Start testing");
    
    for (var i = 1; i <= cycleNum; i++) {
        target.delay(1);        
        UIALogger.logMessage("Snap image!");
        snapButton.tap();
        UIALogger.logMessage("Snapping num: "+ i);
        
        UIALogger.logMessage("Hold on for 6 seconds for saving the snapped image!");
        target.delay(waitTime);
        //Tap on screen to get out of full screen mode
       // target.tap({x:286.00, y:134.50});
    } 
}
test("kappaSnappingOnIphone", testFunc)
