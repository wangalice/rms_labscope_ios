#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 500;
   // var waitTime = 4;
    
    UIALogger.logMessage("Hold on for 1 second for camera discovery!");
    target.delay(1);
    

    for (var i = 1; i <= cycleNum; i++) {
        UIALogger.logMessage("Playing live of Motic camera!");
        window.collectionViews()[0].cells()["Stemi305"].tap();
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);
        var snapButton = window.buttons()["snapImage"];

        UIALogger.logMessage("Snap images from Stemi305!");
        snapButton.tap();
        target.delay(2);
        //go back to portrait mode
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
        target.delay(2);

        UIALogger.logMessage("Playing live of Wision camera!");
        window.collectionViews()[0].cells()["Wision"].tap();
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_LANDSCAPELEFT);

        UIALogger.logMessage("Snap images from Wision camera!");
        snapButton.tap();
        target.delay(4);
        target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);
        target.delay(2);
        UIALogger.logMessage("Switching count: "+ i);
    }
    
}
test("switchAndSnap", testFunc)