#import "tuneup/tuneup.js"

    var testFunc = function(target, app) {
    var window = target.frontMostApp().mainWindow();

    UIALogger.logMessage("Hold on for 5 seconds for camera discovery!");
    target.delay(5);
    target.captureScreenWithName("Camera thumbnails");
    
    UIALogger.logMessage("Select Wision camera of hardware-decoding!");
    window.scrollViews()[0].buttons()[3].tap();
    
    UIALogger.logMessage("Hold on for 5 seconds for camera connection!");
    target.delay(5);
    
    UIALogger.logMessage("Start testing!");
    
    var snapButton = window.buttons()["snap photo"];
    var discardButton = window.buttons()["Discard"];
    var annotationButton = window.buttons()["Annotation &\nMeasurement"];
    var hideMeasureButton = window.buttons()[7];
    var doneButton = window.buttons()["Done"];

    var lineButton = window.buttons()["lineIcon"];
    var squareButton = window.buttons()["squareIcon"];
    var circleButton = window.buttons()["circleIcon"];
    var angleButton = window.buttons()["angleIcon"];
        
    UIALogger.logMessage("Snap image!");
    snapButton.tap();
    UIALogger.logMessage("Hold on for 10 seconds for snapping successfully!");
   
    annotationButton.tap(); 
    target.captureScreenWithName("Initial image before adding line annotation!");
    lineButton.tap();
    target.captureScreenWithName("Line annotation with measurement");
    hideMeasureButton.tap();
    target.captureScreenWithName("Line annotation without measurement");
    doneButton.tap();
    UIALogger.logMessage("Finish hiding line annotation measurement!");

    annotationButton.tap();
    target.captureScreenWithName("Initial image before adding square annotation!");
    squareButton.tap();
    target.captureScreenWithName("Square annotation with measurement");
    hideMeasureButton.tap();
    target.captureScreenWithName("Square annotation without measurement");
    doneButton.tap();
    UIALogger.logMessage("Finish hiding square annotation measurement!");

    annotationButton.tap();
    target.captureScreenWithName("Initial image before adding circle annotation!");
    circleButton.tap();
    target.captureScreenWithName("Circle annotation with measurement");
    hideMeasureButton.tap();
    target.captureScreenWithName("Circle annotation without measurement");
    doneButton.tap();
    UIALogger.logMessage("Finish hiding Circle annotation measurement!");

    annotationButton.tap();
    target.captureScreenWithName("Initial image before adding angle annotation!");
    angleButton.tap();
    target.captureScreenWithName("Angle annotation with measurement");
    hideMeasureButton.tap();
    target.captureScreenWithName("Angle annotation without measurement");
    doneButton.tap();
    UIALogger.logMessage("Finish hiding angle annotation measurement!");

    discardButton.tap();
}

test("hideMeasurement", testFunc)
