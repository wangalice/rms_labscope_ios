#import "tuneup/tuneup.js" 

var testFun = function(target, app){
    var micName = "Kappa"; // change to test target microscope name
    
    var target = UIATarget.localTarget();
    var window = target.frontMostApp().mainWindow();
    var cycleNum = 1000;

    var displaySettingButt = window.buttons()["displaySettingButton"];
    var displayCUrveButt = window.buttons()["displayCurveButton"];

    UIALogger.logMessage("Hold on for 2 seconds for camera discovery!");
    target.delay(2);
    
    UIALogger.logMessage("Play live of camera");
    window.scrollViews()[1].buttons()[micName].tap();
    target.delay(2);
        
    for (var i = 1; i <= cycleNum; i++) {
        displaySettingButt.tap();
        target.delay(1);
        displayCUrveButt.tap();
        target.delay(1);

        UIALogger.logMessage("Deactivating app");	
        target.deactivateAppForDuration(2);
        target.delay(2);
        UIALogger.logMessage("Deactivating num: "+ i);
    }
}
test("Deactivating app", testFun)