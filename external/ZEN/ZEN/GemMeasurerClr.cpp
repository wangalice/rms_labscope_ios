// GemMeasurerClr.cpp
//
//  Comments : 
//  Date     : 2013/08/05
//  Author   : Jian Cheng, Helmut Z�phel (HeZ)
//  <copyright file="GemMeasurerClr.cpp" company="Carl Zeiss Microscopy GmbH">
//     Copyright (c) 2013 Carl Zeiss Microscopy GmbH. All rights reserved.
//  </copyright>

#include "GemMeasurerClr.h"

#include <memory.h>
#include <math.h>
#include "GemChords.h"

namespace GemMeasurerClr
{

GemMeasurer::GemMeasurer() :
m_pPolygonCoordinates(0L),
m_pCracksPacked(0L),
m_nPolygonPointsCount(0),
m_IsValid(false),
m_StartX(0),
m_StartY(0) {
    
}
	
GemMeasurer::~GemMeasurer() {
    delete[] m_pPolygonCoordinates;
    m_pPolygonCoordinates = 0;
    
    delete m_pCracksPacked;
    m_pCracksPacked = 0;
}

long GemMeasurer::ConvertCircleToPolygon(double centerX, double centerY, double radius, double **pPolygonCoordinates)
{
	if (radius < 0.000000001)
	{
		return (long)0;
	}

	double PI = 3.1415926535;
	double tolerance = 0.25;
	double threshold = 1.0 - tolerance / radius / 2.0;
	double theta = PI / 4.0;
	double delta = 0.001;
	double value = 0.0;
	do
	{
		theta -= delta;
		value = fabs(sin(theta)) / sqrt(2.0 * (1.0 - cos(theta)));
	}while (theta > 0 && value <= threshold);
    
    if (theta < delta) {
        theta = delta;
    }

	long n = (long)((PI / 2 - theta) / theta + 0.5);	
	n = 2 * n + 1;
	theta = PI / 2 / (n + 1);
	long m = 4;
	long totalPointsCount = m + 1 + m * n;
	*pPolygonCoordinates = new double[totalPointsCount * 2];
	double *pPoints = *pPolygonCoordinates;
	double *pStart = pPoints;	
	double d = cos(theta / 2);
	double deltaRad = radius * (1 - d) / (1 + d);            
	double newRad = radius + deltaRad;
            
    for (long j = 0; j < m; j++)
    {
        double startTheta = PI * j / 2.0;
		*pPoints++ = centerX + newRad * cos(startTheta);
		*pPoints++ = centerY + newRad * sin(startTheta);        
        for (long i = 1; i <= n; i++)
        {
            double angle = startTheta + theta * i;
			*pPoints++ = centerX + newRad * cos(angle);
			*pPoints++ = centerY + newRad * sin(angle);            
        }
    }

	// close the polygon		
	*pPoints++ = pStart[0];			
	*pPoints++ = pStart[1];
	return totalPointsCount;
}
    
void GemMeasurer::SetRegion(const double *pPolygonCoordinates, long polygonPointsCount)
{			
	m_nPolygonPointsCount = polygonPointsCount;
	m_IsValid = false;

    if (pPolygonCoordinates == 0)
	{
		return;
	}
    
    if (m_pPolygonCoordinates) {
        delete[] m_pPolygonCoordinates;
        m_pPolygonCoordinates = 0;
    }

	long arrayLength = polygonPointsCount * 2;
	m_pPolygonCoordinates = new long[arrayLength];
	for (long i = 0; i < arrayLength; i++)
	{		
		m_pPolygonCoordinates[i] = (long)(pPolygonCoordinates[i]);
	}
}
    
void GemMeasurer::SetRegion(const float *pPolygonCoordinates, long polygonPointsCount)
{
    m_nPolygonPointsCount = polygonPointsCount;
    m_IsValid = false;
    
    if (pPolygonCoordinates == 0)
    {
        return;
    }
    
    if (m_pPolygonCoordinates) {
        delete[] m_pPolygonCoordinates;
        m_pPolygonCoordinates = 0;
    }
    
    long arrayLength = polygonPointsCount * 2;
    m_pPolygonCoordinates = new long[arrayLength];
    for (long i = 0; i < arrayLength; i++)
    {		
        m_pPolygonCoordinates[i] = (long)(pPolygonCoordinates[i]);
    }
}

double GemMeasurer::Area()
{	
	double area = 0;
	if (m_pCracksPacked == 0 || m_IsValid == false)	
	{
		CreateCracks();
		m_IsValid = true;
	}
	
	sFeatMoments0(&area, m_pCracksPacked);
	return area;
}

double GemMeasurer::Perimeter()
{
	if (m_pCracksPacked == 0 || m_IsValid == false)	
	{
		CreateCracks();
		m_IsValid = true;
	}
	
	double sqrt2 = 0.7071067811865472440084436210485;
	long perimx1, perimxy1, perimy1;
	if (sFeatPerimeter(&perimx1, &perimxy1, &perimy1))
	{
		return perimx1 + sqrt2 * perimxy1 + perimy1;
	}

	return 0.0;
}

void GemMeasurer::CreateCracks()
{
	if (m_nPolygonPointsCount == 0 || m_pPolygonCoordinates == 0)
	{
		return;
	}

	// convert the polygon coordinates
	long borderSize = 2;
	long minX, minY, maxX, maxY;
	GetPolygonBounds(&minX, &minY, &maxX, &maxY);
	ConvertToLocalCoordinates(minX, minY, borderSize);
	
	// create a binary image and render the polygon in it.
	long width = maxX - minX + 2 * borderSize;
	long height = maxY - minY + 2 * borderSize;
    
    GemChords *ppBinImg = new GemChords(width, height);
	
	RenderPolygon(ppBinImg, m_pPolygonCoordinates, m_nPolygonPointsCount);

	// trace the boundary
	long xseed = m_pPolygonCoordinates[0];
	long yseed = m_pPolygonCoordinates[1];		
	long long allocsize = ((long long) width) * height;
	TraceLocal(ppBinImg, width, height, xseed, yseed, allocsize);

    delete ppBinImg;
}

void GemMeasurer::ConvertToLocalCoordinates(long minX, long minY, long borderSize)
{	
	long i, j, x, y;
	for (long k = 0; k < m_nPolygonPointsCount; k++)
	{
		i = k * 2;
		j = i + 1;
		x = m_pPolygonCoordinates[i];
		y = m_pPolygonCoordinates[j];
		m_pPolygonCoordinates[i] = x - minX + borderSize;
		m_pPolygonCoordinates[j] = y - minY + borderSize;
	}
}

void GemMeasurer::GetPolygonBounds(long *pMinX, long *pMinY, long *pMaxX, long *pMaxY)
{	
	long minX = 100000000;
	long minY = minX;
	long maxX = -100000000;
	long maxY = maxX;
	long x, y;	
	long * pCoordinates = m_pPolygonCoordinates;
	for (long k = 0; k < m_nPolygonPointsCount; k++)
	{
		x = *pCoordinates++;
		y = *pCoordinates++;
		
		minX = minX < x ? minX : x;
		minY = minY < y ? minY : y;
		maxX = maxX > x ? maxX : x;
		maxY = maxY > y ? maxY : y;
	}

	*pMinX = minX;
	*pMinY = minY;
	*pMaxX = maxX;
	*pMaxY = maxY;
}

bool GemMeasurer::sFeatPerimeter(long *perimx1, long *perimxy1, long *perimy1 )
{
	CRACKPACKTYPE *pCracks = m_pCracksPacked;
//	double StartX = m_StartX;
//	double StartY = m_StartY;
    if ( pCracks == 0 || pCracks->size() < 1 )
    {
        *perimx1 = *perimy1 = *perimxy1 = 0; 
        return false;
    }   

    long    nh=0, nv=0, nd=0;

    CRACKPACKTYPE::iterator end = pCracks->end();
    CRACKPACKTYPE::iterator it = pCracks->begin();
    CRACKPACKTYPE::value_type first = *it;
    CRACKPACKTYPE::value_type last = first;
    for (++it; it != end; ++it) {
        CRACKPACKTYPE::value_type crack = *it;
        if (crack == last)
        {
            switch ( crack )
            {
                case CRACK_R:
                case CRACK_L:
                    nh++;
                    break;
                case CRACK_U:
                case CRACK_D:
                    nv++;
                    break;
            }
        }
        else
        {
            nd++;
        }
        
        last = crack;
    }
    
    if (first == last)
    {
        switch ( first )
        {
            case CRACK_R:
            case CRACK_L:
                nh++;
                break;
            case CRACK_U:
            case CRACK_D:
                nv++;
                break;
        }
    }
    else
    {
        nd++;
    }
  
    *perimx1 = nh;
    *perimy1 = nv;
    *perimxy1 = nd;

	return true;
}    

long GemMeasurer::Compare(unsigned char value, unsigned char threshold)
{
	if (value < threshold)
		return 0;
	return value;	
}

bool GemMeasurer::TraceLocal(GemChords *ppLines, long width, long height, long xseed, long yseed, long long allocsize)
{
	if ( !ppLines )
		return false;

	long xsize = width;
	long ysize = height;

	if ( xseed < 0 || xsize <= xseed || yseed < 0 || ysize <= yseed)
	{
		return false;
	}
	
	allocsize /= 2;
	CRACKPACKTYPE *cr = new CRACKPACKTYPE;
    long y1 = yseed;
    long y2 = yseed - 1;
	unsigned char thrs = ppLines->GetPoint(xseed, y1);

	// look for StartX (StartY == yseed)
	long prevc2 = Compare(ppLines->GetPoint(xseed, y1), thrs);
	if ( prevc2 == 0 )
	{
        delete cr;
		return false;
	}

	long StartX = 0, StartY = 0;
	long long area = 0;
	while ( area >= 0 ) // counter clock wise contours (object) only
	{
		long c8, c4, c2, c1;
		// Border search ->
		for ( xseed++; xseed < xsize; xseed++ )
		{
			c1 = Compare(ppLines->GetPoint(xseed, y1), thrs);
			
			if ( prevc2 > c1 ) // 1 -> 0 
				break;
			prevc2 = c1;
		}
		prevc2 = 0;

		if ( yseed != 0 )
		{
			y2 = yseed-1;
			if ( xseed != xsize )
				c4 = Compare(ppLines->GetPoint(xseed, y2), thrs);
			else
				c4 = 0;
			c8 = Compare(ppLines->GetPoint(xseed-1, y2), thrs);
		}
		else
			c8 = c4 = 0;
		long code = (c8<<3) | (c4<<2) | (1<<1) | (0<<0);
		if ( code == 6 ) // ignore holes
			code = 14;

		area = 0;
		cr->clear();
		long x = StartX = xseed;
		long y = StartY = yseed;

		while ( 1 )
		{
			// c8 c4    l2
			// c2 c1    l1
			switch( code )
			{
				case 1: // 00
						// 01 

				case 5: // 01
						// 01 

				case 13:// 11
						// 01 
					if ( allocsize < cr->size() )
                    {
                        delete cr;
						return false;
                    }

                    cr->push_back(CRACK_D);
					y++;
					y2 = y1;
					if ( y != ysize )
					{
						y1 = y;
						if ( x != 0 )
							c2 = Compare(ppLines->GetPoint(x-1,y1), thrs);
						else
							c2 = 0;
						c1 = Compare(ppLines->GetPoint(x,y1), thrs);
					}
					else
						c2 = c1 = 0;
					code = ((0<<3) | (1<<2) | (c2<<1) | (c1<<0));

					if ( code == 6) // ignore holes
						code = 7;
					area += x;
					break;

				case 2: // 00
						// 10

				case 3:	// 00
						// 11

				case 7: // 01
						// 11 
                    cr->push_back(CRACK_L);
					x--;

					if ( x != 0 )
					{
						if ( y != 0 )
							c8 = Compare(ppLines->GetPoint(x-1,y2), thrs);
						else
							c8 = 0;
						c2 = Compare(ppLines->GetPoint(x-1,y1), thrs);
					}
					else 
						c8 = c2 = 0;
					code = ((c8<<3) | (0<<2) | (c2<<1) | (1<<0));
					if ( code == 9 ) // ignore holes
						code = 11;
					break;

				case 4: // 01 
						// 00 

				case 12:// 11
						// 00

				case 14:// 11
						// 10 
                    cr->push_back(CRACK_R);
					x++;
					
					if ( x != xsize )
					{
						if ( y != ysize )
							c1 = Compare(ppLines->GetPoint(x, y1), thrs );
						else
							c1 = 0;
						c4 = Compare(ppLines->GetPoint(x, y2), thrs );

					}
					else
						c4 = c1 = 0;
					code = ((1<<3) | (c4<<2) | (0<<1) | (c1<<0));
					if ( code == 9 )
						code = 13;
					break;
				case 8:  // 10
						 // 00 

				case 10: // 10
						 // 10 

				case 11: // 10
						 // 11 
                    cr->push_back(CRACK_U);
					y--;
					y1 = y2;
					if ( y != 0 )
					{
						y2 = y-1;
						if ( x != xsize )
							c4 = Compare(ppLines->GetPoint(x,y2), thrs );
						else
							c4 = 0;
						c8 = Compare(ppLines->GetPoint(x-1, y2), thrs );
					}
					else
						c8 = c4 = 0;
					code = (c8<<3) | (c4<<2) | (1<<1) | (0<<0);
					if ( code == 6 ) // ignore holes
						code = 14;
					area -= x;
					break;

				case 6: // 01
						// 10 
					break;

				case 9: // 10
						// 01
					break;


				case 0: // 00
						// 00
					break;
				case 15:// 11
						// 11
					break;
			}
			if ( x == StartX && y == StartY )
				break;

		}
	}
	
	m_StartX = StartX;
	m_StartY = StartY;

    if (m_pCracksPacked) {
        delete m_pCracksPacked;
        m_pCracksPacked = 0;
    }
    
	m_pCracksPacked = cr;
		
    return true; 
}

void GemMeasurer::sFeatMoments0( double *pMoments, CRACKPACKTYPE *pCracks )
{
	// Length( pMoments ) == 1
	if ( pCracks == 0 || pCracks->size() < 4 )
	{
		*pMoments = 0;
		return;
	}
    long x=0, area=0;
    
    for (CRACKPACKTYPE::iterator it = pCracks->begin(); it != pCracks->end(); ++it) {
        switch (*it) {
            case CRACK_R:
                x++;
                break;
            case CRACK_U:
                area -= x;
                break;
            case CRACK_L:
                x--;
                break;
            case CRACK_D:
                area += x;
                break;
            default:
                break;
        }
    }

    *pMoments = (area>0) ? area : -area;
}

void GemMeasurer::AddChord(GemChords *ppLines, long y, long xs, long xe)
{
    ppLines->AddChord(xs, xe, y);
}

long GemMeasurer::D2I(double v)
{
	return (long)(v);
}

void GemMeasurer::RenderPolygon(GemChords *ppLines, long *poly, long npoly) // Chord-Brezenham
{
    long x = D2I( *poly++ );
    long y = D2I( *poly++ );
	long xs;

    while ( --npoly != 0 )
    {

        long x2 = D2I( *poly++ ); 
		long y2 = D2I( *poly++ );
        long dx = x2 - x, dy = y2 - y; 		
        long c1, c2, error;
        if ( dx == 0 )
		{
			if ( dy < 0 )
			{
				for ( ; y>y2; y-- )				
					AddChord(ppLines, y, x, x+1 );				
			}
			else if ( dy > 0 ) // if ( dy == 0 )
			{
				for ( ; y<y2; y++ )
					AddChord(ppLines, y, x, x+1 );
			}
		}
		else if ( dx > 0 )
		{
			xs = x;
			if ( dy == 0 )
			{
				AddChord(ppLines, y, x, x2 );	
			}
			else if ( dy < 0 )
			{
				if ( -dy < dx )	 // 1.Oktant
				{
					c1 = - 2 * dy;
					error = c1 - dx;
					c2 = error - dx;
					for (;;) 
					{
						//_debug_printf( L"p %d,%d\n",x,y); 
						if ( ++x >= x2 ) break;
						if ( error < 0 ) 
						{
							error += c1;
						}
						else
						{   
							AddChord(ppLines, y, xs, x );
							xs = x;
							y--;
							error += c2;
						} 
					} 
					AddChord(ppLines, y, xs, x );
				}
				else // -dy >= dx  // 2.Oktant
				{
					c1 = 2 * dx;
					error = c1 + dy;
					c2 = error + dy;  
					for (;;) 
					{						
						AddChord(ppLines, y, xs, x+1 );
						if ( --y <= y2 ) break;
						if ( error < 0 )
						{
							error += c1;
						}                            
						else
						{   
							xs = ++x;
							error += c2;
						} 
					}   
				}
			}
			else // dy > 0	
			{
				if (dy < dx )		// 8.Oktant
				{
					c1 = 2 * dy;
					error = c1 - dx;
					c2 = error - dx;  
					for (;;) 
					{
						if ( ++x >= x2 ) break;
						if ( error < 0 )   
						{
							error += c1;
						}
						else
						{   
							AddChord(ppLines, y, xs, x );
							xs = x;
							y++;
							error += c2;
						} 
					} 
					AddChord(ppLines, y, xs, x );
				}
				else // dy >= dx	// 7.Oktant
				{
					c1 = 2 * dx;
					error = c1 - dy;
					c2 = error - dy;  
					for (;; ) 
					{
 						AddChord(ppLines, y, xs, x+1 );
						if ( ++y >= y2 ) break;
						if ( error < 0 )
						{
							error += c1;
						}
						else
						{   
							xs = ++x;
							error += c2;
						} 
					}   
				}
			}
		}
		else // dx < 0
		{
			xs = x+1;
			if ( dy == 0 )
			{
				AddChord(ppLines, y, x2+1, xs );	
			}
			else if ( dy < 0 )	
			{
				if (dx < dy )	// 4.Oktant
				{
					c1 = - 2 * dy;
					error = c1 + dx;
					c2 = error + dx;  
					for (;;) 
					{
						if ( x2 >= --x ) break;
						if ( error < 0 ) 
						{
							error += c1;
						}
						else
						{   
							AddChord(ppLines, y, x+1, xs );
							xs = x+1;
							y--;
							error += c2;
						} 
					} 
					AddChord(ppLines, y, x+1, xs );
				}
				else // dx >= dy  // 3.Oktant
				{
					c1 = 2 * -dx;
					error = c1 + dy;
					c2 = error + dy;  
					for (;;) 
					{
						AddChord(ppLines, y, x, xs );
						if ( --y <= y2 ) break;
						if ( error < 0 )
						{
							error += c1;
						}                            
						else
						{   
							xs = x--;
							error += c2;
						} 
					}   
				}
			}
			else // dy > 0
			{
				if ( -dx > dy )	// 5.Oktant
				{
					c1 = 2 * dy;
					error = c1 + dx;
					c2 = error + dx;  
					for (;;) 
					{
						if ( x2 >= --x ) break;
						if ( error <= 0 )   
						{
							error += c1;
						}
						else
						{   
							AddChord(ppLines, y, x+1, xs );
							xs = x+1;
							y++;
							error += c2;
						} 
					} 
					AddChord(ppLines, y, x+1, xs );
				}
				else // -dx <= dy // 6.Oktant
				{
					c1 = 2 * -dx;
					error = c1 - dy;
					c2 = error - dy;  
					for (;;) 
					{
 						AddChord(ppLines, y, x, xs );
						if ( ++y >= y2 ) break;
						if ( error < 0 )
						{
							error += c1;
						}
						else
						{   
							xs = x--;
							error += c2;
						} 
					}   
				}
			}
		}
        x = x2; y = y2;                 
	}

	AddChord(ppLines, y, x, x+1 );
}
    
#pragma mark - PackedCracks
    
const size_t PackedCracks::kPackCrackBufferSize = 4096;  // bytes
//static const size_t kPackCracksPerByte = sizeof(PackedCracks::buffer_t) * 8 / 2;  // 8 bit / 2 bit
//static const int kPackCrackMask = 0x3;
//    
//static size_t packedBytesOfSize(size_t size) {
//    return (size + kPackCracksPerByte - 1) / kPackCracksPerByte;
//}

PackedCracks::PackedCracks() : _bitOffset(0) {
}
    
PackedCracks::~PackedCracks() {
}

void PackedCracks::push_back(type_t value) {
    if (_bitOffset == 0) {
        PackedCrackFour pcf;
        pcf.c1 = value;
        buffers.push_back(pcf);
        _bitOffset++;
    } else if (_bitOffset == 1) {
        PackedCrackFour& pcf = buffers.back();
        pcf.c2 = value;
        _bitOffset++;
    } else if (_bitOffset == 2) {
        PackedCrackFour& pcf = buffers.back();
        pcf.c3 = value;
        _bitOffset++;
    } else if (_bitOffset == 3) {
        PackedCrackFour& pcf = buffers.back();
        pcf.c4 = value;
        _bitOffset = 0;
    }
}
    
void PackedCracks::clear() {
    _bitOffset = 0;
    buffers.clear();
}
    
PackedCracks::Iterator PackedCracks::begin() {
    PackedCracks::Iterator it(this);
    it._bitOffset = 0;
    it._byteOffset = 0;
    
    return it;
}

PackedCracks::Iterator PackedCracks::end() {
    PackedCracks::Iterator it(this);
    
    it._byteOffset = this->buffers.size() - 1;
    it._bitOffset = this->_bitOffset;
        
    if (this->_bitOffset == 0) {
        it._byteOffset++;
    }
    
    return it;
}
    
#pragma mark - PackedCracks::Iterator

PackedCracks::Iterator::Iterator(PackedCracks* owner) : _owner(owner) {
}

PackedCracks::Iterator::~Iterator() {
}

PackedCracks::Iterator::type_t
PackedCracks::Iterator::operator * () const {
    const PackedCrackFour& pcf = _owner->buffers[_byteOffset];
    switch (_bitOffset) {
        case 0:
            return pcf.c1;
        case 1:
            return pcf.c2;
        case 2:
            return pcf.c3;
        case 3:
            return pcf.c4;
        default:
            return pcf.c1;
    }
}

PackedCracks::Iterator&
PackedCracks::Iterator::operator ++() {
    _bitOffset ++;
    if (_bitOffset >= 4) {
        _bitOffset = 0;
        _byteOffset++;
    }
    
    return *this;
}
    
bool PackedCracks::Iterator::operator != (const PackedCracks::Iterator& rhs) const {
    return !this->operator==(rhs);
}

bool PackedCracks::Iterator::operator == (const PackedCracks::Iterator& rhs) const {
    return (_byteOffset == rhs._byteOffset &&
            _bitOffset == rhs._bitOffset);
}

}