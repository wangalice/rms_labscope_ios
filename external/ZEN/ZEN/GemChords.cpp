//
//  GemChords.cpp
//  Matscope
//
//  Created by Ralph Jin on 9/26/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "GemChords.h"

#include <assert.h>
#include <memory.h>
#include <algorithm>

namespace GemMeasurerClr {
    class GemChord {
    public:
        typedef GemChords::cord_t cord_t;
        typedef GemChords::chord_size_t chord_size_t;
        
    public:
        GemChord(cord_t xs, cord_t xe, GemChord *next = 0);
        ~GemChord();

        /** @return YES, if it can add chord(xs, xe) onto the chord or to the right of the chord.*/
        bool CanAddChord(cord_t xs, cord_t xe);
        void AddChord(cord_t xs, cord_t xe);

        bool GetPoint(cord_t x);

        chord_size_t GetSubCount();
        
        cord_t FindLeftX(cord_t x, bool predict) const;

    private:
        void MergeRightIfNeed();
    
        cord_t _xs;  // start x
        cord_t _xe;  // end x
        
        GemChord *_next;  // next chord
    };

#pragma mark - Implementation of GemChord
    
    GemChord::GemChord(cord_t xs, cord_t xe, GemChord *next) : _xs(xs), _xe(xe), _next(next) {
    }

    GemChord::~GemChord() {
        delete _next;
    }

    bool GemChord::CanAddChord(cord_t xs, cord_t xe) {
        return xe >= _xs;
    }

    void GemChord::AddChord(cord_t xs, cord_t xe) {
        if (xs > _xe) {  // try to add to the next chord
            if (_next) {
                if (_next->CanAddChord(xs, xe)) {
                    _next->AddChord(xs, xe);
                } else {
                    _next = new GemChord(xs, xe, _next);
                }
            } else {
                _next = new GemChord(xs, xe, _next);
            }
        } else {
            if (_xs > xs) {  // _xs = min(xs, _xs)
                _xs = xs;
            }
            
            if (_xe < xe) {  // _xe = max(xe, _xe)
                _xe = xe;
            }
            
            MergeRightIfNeed();
        }
    }

    void GemChord::MergeRightIfNeed() {
        while (_next && _xe >= _next->_xs) {
            if (_next->_xe > _xe) {  // _xe = max(_xe, _next->xe);
                _xe = _next->_xe;
            }

            // remove temp from the list
            GemChord *temp = _next;
            _next = _next->_next;
            temp->_next = 0;
            delete temp;
        }
    }

    bool GemChord::GetPoint(cord_t x) {
        if (x >= _xs && x < _xe) {
            return true;
        }
        
        if (_next) {
            return _next->GetPoint(x);
        } else {
            return false;
        }
    }

    GemChord::chord_size_t GemChord::GetSubCount() {
        unsigned long count = 0;
        for (GemChord* next = _next; next; next = next->_next) {
            count ++;
        }

        return count;
    }
    
    GemChord::cord_t GemChord::FindLeftX(cord_t x, bool predict) const {
        if (predict) {  // find 1
            const GemChord* prev = NULL;
            for (const GemChord* current = this; current; ) {
                if (x < current->_xs) {
                    return (prev == NULL) ? -1 : prev->_xe - 1;
                } else {
                    if (x < current->_xe) {  // if between current start x and end x
                        return x;
                    }
                }
                
                prev = current;
                current = current->_next;
            }
            return (prev == NULL) ? -1 : prev->_xe - 1;
        } else {  // find 0
            for (const GemChord* current = this; current; ) {
                if (x < current->_xs) {
                    return x;
                } else {
                    if (x < current->_xe) {  // if between current start x and end x
                        return current->_xs - 1;
                    }
                }
                
                current = current->_next;
            }
            
            return x;
        }
    }
    
#pragma mark - Implementation of GemChords

    GemChords::GemChords(chord_size_t width, chord_size_t height) :
    _width(width), _height(height), _min_x(width), _max_x(0), _min_y(height), _max_y(0)  {
        _chords = new GemChordPtr[height];
        memset(_chords, 0, sizeof(GemChordPtr) * height);
    }

    GemChords::~GemChords() {
        cleanup();
    }

    void GemChords::cleanup() {
        for (chord_size_t i = 0; i < _height; i++) {
            GemChordPtr chord = _chords[i];
            delete chord;
        }

        delete[] _chords;
        _chords = NULL;
    }
    
    void GemChords::Clear() {
        for (chord_size_t i = 0; i < _height; i++) {
            delete _chords[i];
            _chords[i] = NULL;
        }
        
        _min_x = _width;
        _max_x = 0;
        _min_y = _height;
        _max_y = 0;
    }

    void GemChords::AddPoint(cord_t x, cord_t y) {
        AddChord(x, x + 1, y);
    }

    void GemChords::AddChord(cord_t xs, cord_t xe, cord_t y) {
        if (y < 0 || y >= _height || xs >= xe) {
            assert(false);  // invalid parameter
            return;
        }
        
        _min_x = std::min(xs, _min_x);
        _max_x = std::max(xe, _max_x);
        _min_y = std::min(y, _min_y);
        _max_y = std::max(y, _max_y);

        GemChordPtr chord = _chords[y];
        if (chord == 0L) {
            chord = new GemChord(xs, xe);
            _chords[y] = chord;
        } else if (chord->CanAddChord(xs, xe)) {
            _chords[y]->AddChord(xs, xe);
        } else {
            chord = new GemChord(xs, xe, chord);
            _chords[y] = chord;
        }
    }

    bool GemChords::GetPoint(cord_t x, cord_t y) const {
        if (y < 0 || y >= _height) {
            return 0;
        }
        
        if (_chords[y] == 0L) {
            return 0;
        } else {
            return _chords[y]->GetPoint(x);
        }
    }

    GemChords::chord_size_t GemChords::GetChordCount(cord_t y) const {
        if (y < 0 || y >= _height) {
            assert(false);  // invalid parameter
            return 0;
        }

        GemChordPtr chord = _chords[y];
        if (chord == 0L) {
            return 0;
        } else {
            return chord->GetSubCount() + 1;
        }
    }
    
    GemChords::cord_t GemChords::FindLeftXOnChord(cord_t x, cord_t y, bool predict) const {
        const GemChordPtr chord = _chords[y];
        if (chord) {
            return chord->FindLeftX(x, predict);
        } else {
            if (predict) {
                return -1;
            } else {
                return x;
            }
        }
    }
    
}  // GemMeasurerClr