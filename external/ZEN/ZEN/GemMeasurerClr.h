// GemMeasurerClr.h
//
//  Comments : 
//  Date     : 2013/08/05
//  Author   : Jian Cheng, Helmut Z�phel
//  <copyright file="GemMeasurerClr.h" company="Carl Zeiss Microscopy GmbH">
//     Copyright (c) 2013 Carl Zeiss Microscopy GmbH. All rights reserved.
//  </copyright>

#ifndef GEM_MEASURER_CLR_H
#define GEM_MEASURER_CLR_H

#include <deque>

#pragma once

//using namespace System;

namespace GemMeasurerClr 
{
	#define CRACK_R     0  // right
	#define CRACK_U     1  // up
	#define CRACK_L     2  // left
	#define CRACK_D     3  // down
    
    class GemChords;  // pre-declare
    
    /// <summary>
    /// Defines the class for packed crack data.
    /// </summary>
    class PackedCracks {
    public:
        typedef unsigned char type_t;
        typedef type_t value_type;
        typedef unsigned long size_t;
        
        typedef unsigned char buffer_t;
        typedef buffer_t *buffer_ptr;
        typedef const buffer_t *const_buffer_ptr;
        
        static const size_t kPackCrackBufferSize;
        
    public:        
        PackedCracks();
        ~PackedCracks();
        
        /// <summary>
        /// iterator class of PackedCrack.
        /// </summary>
        class Iterator {
            typedef PackedCracks::type_t type_t;
            typedef PackedCracks::size_t size_t;
            typedef PackedCracks::buffer_ptr buffer_ptr;
        public:
            Iterator(PackedCracks* owner);
            ~Iterator();
            
            type_t operator * () const;
            
            Iterator& operator++();
            
            bool operator != (const Iterator& rhs) const;
            bool operator == (const Iterator& rhs) const;
        private:
            PackedCracks* _owner;
            size_t _byteOffset;
            size_t _bitOffset;
            
            friend class PackedCracks;
        };
        
        typedef Iterator iterator;
        
        void push_back(type_t crack);
        
        size_t size() const {
            return _bitOffset == 0 ? buffers.size() * 4 : _bitOffset + buffers.size() * 4 - 4;
        }
        
        void clear();
        
        Iterator begin();
        Iterator end();

    private:
        PackedCracks(const PackedCracks &rhs);
        PackedCracks& operator=(const PackedCracks &rhs);
        
        void initWithCapacity(size_t capacity);
        
        unsigned char _bitOffset;
        
        struct PackedCrackFour {
            unsigned char c1 : 2, c2 : 2, c3 : 2, c4 : 2;
        };
        
        std::deque<PackedCrackFour> buffers;
    };
	
	/// <summary>
    /// Defines the class for geometry measurement like area and perimeter. It accepts the polygon coordinates in unit "pixel" as input
	/// and does not take the scaling into account.
    /// </summary>
	class GemMeasurer
	{
        typedef PackedCracks CRACKPACKTYPE;

		public:			

			/// <summary>
			/// Constructor.			
			/// </summary>
			GemMeasurer();
            ~GemMeasurer();

			/// <summary>
			/// Converts the circle to a polygon given its center and radius. The polygon is represented as
			/// a sequence of point coordinates "x1, y1, x2, y2 ...".
			/// Usage:
			///		double* pPolygonCoordinates;
			///		long count = GemMeasurer::ConvertCircleToPolygon(centerX, centerY, radius, &pPolygonCoordinates);
			/// </summary>
			/// <param name="centerX">The X coordinate of the circle center.</param>
			/// <param name="centerY">The Y coordinate of the circle center.</param>
			/// <param name="radius">The circle radius.</param>
			/// <param name="pPolygonCoordinates">The pointer to the coordinates of the polygon points.</param>
			/// <returns>
			/// The count of the polygon points.
			/// </returns>
			static long ConvertCircleToPolygon(double centerX, double centerY, double radius, double **pPolygonCoordinates);

			/// <summary>
			/// Sets the polygon as the region to be measured. 		
			/// </summary>
			/// <param name="pPolygonCoordinates">The pointer to the polygon points coordinates.</param>
			/// <param name="polygonPointsCount">The polygon points count.</param>
			void SetRegion(const double *pPolygonCoordinates, long polygonPointsCount);
            void SetRegion(const float *pPolygonCoordinates, long polygonPointsCount);

			/// <summary>
			/// Calculates the area of the specified region.
			/// </summary>
			/// <returns>
			/// The area in pixels.
			/// </returns>
			double Area();

			/// <summary>
			/// Calculates the perimeter of the specified region.
			/// </summary>
			/// <returns>
			/// The perimeter in pixels.
			/// </returns>		
			double Perimeter();		

		private:
			// polygon points coordinates
			long *m_pPolygonCoordinates;
			long m_nPolygonPointsCount;		
			bool m_IsValid;

			CRACKPACKTYPE *m_pCracksPacked;
			long m_StartX, m_StartY;
				
			void CreateCracks();
			void GetPolygonBounds(long *pMinX, long *pMinY, long *pMaxX, long *pMaxY);
			void ConvertToLocalCoordinates(long minX, long minY, long borderSize);
			bool TraceLocal(GemChords *ppLines, long width, long height, long xseed, long yseed, long long allocsize);
			long Compare(unsigned char value, unsigned char threshold);		
			void sFeatMoments0( double *pMoments, CRACKPACKTYPE *pCracks );
			bool sFeatPerimeter(long *perimx1, long *perimxy1, long *perimy1 );
			static void AddChord(GemChords *ppLines, long y, long xs, long xe);
			void RenderPolygon(GemChords *ppLines, long *poly, long npoly);
			long D2I(double v);	
	};
}

#endif  // GEM_MEASURER_CLR_H
