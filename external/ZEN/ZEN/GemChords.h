//
//  GemChords.h
//  Matscope
//
//  Created by Ralph Jin on 9/26/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef GEM_CHORDS_H
#define GEM_CHORDS_H

namespace GemMeasurerClr
{
    class GemChord;
    
    class GemChords {
    public:
        typedef GemChord *GemChordPtr;
        typedef unsigned long chord_size_t;
        typedef long cord_t;
    public:
        GemChords(chord_size_t width, chord_size_t height);
        ~GemChords();
        
        void Clear();
        
        void AddPoint(cord_t x, cord_t y);
        void AddChord(cord_t xs, cord_t xe, cord_t y);
        
        bool GetPoint(cord_t x, cord_t y) const;

        chord_size_t GetChordCount(cord_t y) const;
        
        chord_size_t GetWidth() const {
            return _width;
        }
        
        chord_size_t GetHeight() const {
            return _height;
        }
        
        /** search from right to left, if point meet requirment.
         @return found x; or -1, if not found and reaches boundary.
         */
        cord_t FindLeftXOnChord(cord_t x, cord_t y, bool predict) const;
        
        cord_t GetMinX() const {
            return _min_x;
        }
        
        cord_t GetMaxX() const {
            return _max_x;
        }
        cord_t GetMinY() const {
            return _min_y;
        }
        cord_t GetMaxY() const {
            return _max_y + 1;
        }
        
    private:
        void cleanup();
        
        GemChordPtr *_chords;
        chord_size_t _width;
        chord_size_t _height;
        cord_t _min_x;
        cord_t _max_x;
        cord_t _min_y;
        cord_t _max_y;
    };
    
}  // namespace GemMeasurerClr

#endif /* defined(GEM_CHORDS_H) */
