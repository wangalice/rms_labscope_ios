//
//  jxrlib.h
//  jxrlib
//
//  Created by Jin Ralph on 4/7/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifdef __cplusplus
extern "C" {
#endif
    
CGImageRef CreateImageForJPEGXRFile(NSString *filePath, BOOL *grayImage);
CGImageRef CreateImageForJPEGXRData(NSData *data, BOOL *grayImage);

NSData *UIImageJPEGXRRepresentation(UIImage *image, BOOL grayImage);

#ifdef __cplusplus
}
#endif