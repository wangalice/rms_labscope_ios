//
//  jxrlib.m
//  jxrlib
//
//  Created by Jin Ralph on 4/7/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "jxrlib.h"
#import <Accelerate/Accelerate.h>
#include <JXRGlue.h>
#include <strcodec.h>

// undefine ERR to suppress a compile warning
#ifdef ERR
#undef ERR
#endif

#pragma mark - class CZMemoryStream
/** memory stream class that wrap a NSMutableData.
 * set position to seek
 * writeBytes may extend internal buffer
 */
@interface CZMemoryStream : NSObject {
@private
    NSMutableData *_buffer;
}

@property (nonatomic, assign) size_t position;
@property (nonatomic, assign, readonly) size_t length;

- (NSMutableData *)buffer;
- (void)readBytes:(void *)bytes length:(size_t)length;
- (void)writeBytes:(const void *)bytes length:(size_t)length;

@end

@implementation CZMemoryStream

- (void)dealloc {
    [_buffer release];
    [super dealloc];
}

- (NSMutableData *)buffer {
    if (_buffer == nil) {
        _buffer = [[NSMutableData alloc] init];
    }
    return _buffer;
}

- (size_t)length {
    return self.buffer.length;
}

- (void)readBytes:(void *)bytes length:(size_t)length {
    if (_buffer.length <= _position) {
        return;
    }
    
    if (_buffer.length < _position + length) {
        length = _buffer.length - _position;
    }
    
    [_buffer getBytes:bytes length:length];
    _position += length;
}

- (void)writeBytes:(const void *)bytes length:(size_t)length {
    if (_position > self.buffer.length) {
        [self.buffer setLength:_position];
    }
    
    if (_position + length > self.buffer.length) {
        if (_position < self.buffer.length) {
            [self.buffer setLength:_position];
        }
        [self.buffer appendBytes:bytes length:length];
    } else {
        [self.buffer replaceBytesInRange:NSMakeRange(_position, length)
                               withBytes:bytes
                                  length:length];
    }
    
    _position += length;
}

@end

#pragma mark - WMPStream(NSMutableData) functions

ERR CloseWS_NSMutableData(struct WMPStream **ppWS) {
    ERR err = WMP_errSuccess;
    
    CZMemoryStream *stream = (CZMemoryStream *)((*ppWS)->state.pvObj);
    [stream release];
    
    Call(WMPFree((void **)ppWS));
    
Cleanup:
    return err;
}

Bool EOSWS_NSMutableData(struct WMPStream *pWS) {
    CZMemoryStream *stream = (CZMemoryStream *)(pWS->state.pvObj);
    return stream.position >= stream.length;
}

ERR ReadWS_NSMutableData(struct WMPStream *pWS, void *pv, size_t cb) {
    ERR err = WMP_errSuccess;
    
    CZMemoryStream *stream = (CZMemoryStream *)(pWS->state.pvObj);
    [stream readBytes:pv length:cb];
    
Cleanup:
    return err;
}

ERR WriteWS_NSMutableData(struct WMPStream *pWS, const void *pv, size_t cb) {
    ERR err = WMP_errSuccess;
    
    CZMemoryStream *stream = (CZMemoryStream *)(pWS->state.pvObj);
    [stream writeBytes:pv length:cb];
    
Cleanup:
    return err;
}

ERR SetPosWS_NSMutableData(struct WMPStream *pWS, size_t offPos) {
    ERR err = WMP_errSuccess;
    
    CZMemoryStream *stream = (CZMemoryStream *)(pWS->state.pvObj);
    stream.position = offPos;
    
    return err;
}

ERR GetPosWS_NSMutableData(struct WMPStream *pWS, size_t *poffPos) {
    CZMemoryStream *stream = (CZMemoryStream *)(pWS->state.pvObj);
    *poffPos = stream.position;
    
    return WMP_errSuccess;
}

ERR CreateWS_NSMutableData(struct WMPStream **ppWS) {
    ERR err = WMP_errSuccess;
    struct WMPStream *pWS = NULL;
    
    Call(WMPAlloc((void **)ppWS, sizeof(**ppWS)));
    pWS = *ppWS;
    
    pWS->state.pvObj = [[CZMemoryStream alloc] init];
    
    pWS->Close = CloseWS_NSMutableData;
    pWS->EOS = EOSWS_NSMutableData;
    
    pWS->Read = ReadWS_NSMutableData;
    pWS->Write = WriteWS_NSMutableData;
    
    pWS->SetPos = SetPosWS_NSMutableData;
    pWS->GetPos = GetPosWS_NSMutableData;
    
Cleanup:
    return err;
}

ERR WriteWS_ReadOnly(struct WMPStream *pWS, const void *pv, size_t cb) {
    return WMP_errFail;
}

#pragma mark -

CGImageRef PKImageEncode_CopyRect(PKImageDecode *pDecoder, PKRect *pRect) {
    CGImageRef image = NULL;
    ERR err = WMP_errSuccess;
    vImage_Error vImageErr = kvImageNoError;
    
    PKPixelFormatGUID enPFFrom = GUID_PKPixelFormatDontCare;
    PKPixelInfo pPIFrom;
    
    U32 cbStride = 0;
    
    U8 *pb = NULL;
    void *rawData = NULL;
    CFDataRef dataRef = NULL;

    // get pixel format
    pDecoder->GetPixelFormat(pDecoder, &enPFFrom);
    
    // calc common stride
    pPIFrom.pGUIDPixFmt = &enPFFrom;
    PixelFormatLookup(&pPIFrom, LOOKUP_FORWARD);
    
    cbStride = (BD_1 == pPIFrom.bdBitDepth ? ((pPIFrom.cbitUnit * pRect->Width + 7) >> 3) : (((pPIFrom.cbitUnit + 7) >> 3) * pRect->Width));
    
    if (pPIFrom.uBitsPerSample == 8 &&
        ((pPIFrom.cChannel == 1 && pPIFrom.cfColorFormat == Y_ONLY) ||
         (pPIFrom.cChannel == 3 && pPIFrom.cfColorFormat == CF_RGB) ||
         (pPIFrom.cChannel == 4 && pPIFrom.cfColorFormat == CF_RGB))) {
        // actual dec/enc with local buffer
        Call(PKAllocAligned((void **)&pb, cbStride * pRect->Height, 128));
        Call(pDecoder->Copy(pDecoder, pRect, pb, cbStride));
        
        bool grayImage = IsEqualGUID(&enPFFrom, &GUID_PKPixelFormat8bppGray);
        
        const size_t channel = pPIFrom.cChannel;
        CGBitmapInfo bitmapInfo = kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Little;;
        
        // convert other channel to 4 channel
        if (channel == 3) {
            vImage_Buffer srcBuffer = {
                pb,
                (vImagePixelCount)pRect->Height,
                (vImagePixelCount)pRect->Width,
                cbStride
            };
            
            cbStride = 4 * pRect->Width;
            size_t size = cbStride * pRect->Height;
            rawData = malloc(size);
            vImage_Buffer dstBuffer = {
                rawData,
                (vImagePixelCount)pRect->Height,
                (vImagePixelCount)pRect->Width,
                cbStride
            };
            
            vImageErr = vImageConvert_BGR888toBGRA8888(&srcBuffer, NULL, 255, &dstBuffer, NO, kvImageNoFlags);
            if (vImageErr != kvImageNoError) {
                goto Cleanup;
            }
            
            dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, rawData, cbStride * pRect->Height, kCFAllocatorMalloc);
            if (dataRef) {
                rawData = NULL;
            }
        } else if (channel == 1) {
            vImage_Buffer srcBuffer = {
                pb,
                (vImagePixelCount)pRect->Height,
                (vImagePixelCount)pRect->Width,
                cbStride
            };
            
            size_t alphaDataSize = cbStride * pRect->Height;
            void *alphaData = malloc(alphaDataSize);
            memset(alphaData, 255, alphaDataSize);
            vImage_Buffer alphaBuffer = {
                alphaData,
                (vImagePixelCount)pRect->Height,
                (vImagePixelCount)pRect->Width,
                cbStride
            };
            
            cbStride = 4 * pRect->Width;
            size_t size = cbStride * pRect->Height;
            void *rawData = malloc(size);
            vImage_Buffer dstBuffer = {
                rawData,
                (vImagePixelCount)pRect->Height,
                (vImagePixelCount)pRect->Width,
                cbStride
            };
            
            vImage_Error vImageErr = vImageConvert_Planar8toARGB8888(&srcBuffer, &srcBuffer, &srcBuffer, &alphaBuffer, &dstBuffer, kvImageNoFlags);
            if (vImageErr != kvImageNoError) {
                goto Cleanup;
            }
            
            free(alphaData);
            alphaData = NULL;
            
            dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (UInt8 *)rawData, size, kCFAllocatorMalloc);
            if (dataRef) {
                rawData = NULL;
            }
        } else { // channel == 4
            dataRef = CFDataCreate(kCFAllocatorMalloc, pb, cbStride * pRect->Height);
        }
        
        CGColorSpaceRef colorSpace = grayImage ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
        CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);

        image = CGImageCreate(pRect->Width, pRect->Height, 8,
                              32, cbStride, colorSpace,
                              bitmapInfo, dataProvider,
                              NULL, true, kCGRenderingIntentDefault);
        
        CGDataProviderRelease(dataProvider);
        CGColorSpaceRelease(colorSpace);
    }
    
Cleanup:
    if (pb) {
        PKFreeAligned((void **)&pb);
    }
    
    if (dataRef) {
        CFRelease(dataRef);
    }
    
    if (rawData) {
        free(rawData);
    }
    return image;
}

static CGImageRef createImageForDecoder(PKImageDecode *pDecoder, BOOL *grayImage) {
    ERR err = WMP_errSuccess;
    
    PKPixelFormatGUID guidPixFormat;
    CGImageRef image = NULL;
    PKPixelInfo PI;
    
    //==== set default color format
    {
        // take deocder color format and try to look up better one
        // (e.g. 32bppBGR -> 24bppBGR etc.)
        PKPixelInfo newPI;
        newPI.pGUIDPixFmt = PI.pGUIDPixFmt = &pDecoder->guidPixFormat;
        Call(PixelFormatLookup(&newPI, LOOKUP_FORWARD));
        Call(PixelFormatLookup(&newPI, LOOKUP_BACKWARD_TIF));
        guidPixFormat = *newPI.pGUIDPixFmt;
    }
    
    // == color transcoding,
    if (IsEqualGUID(&guidPixFormat, &GUID_PKPixelFormat8bppGray) ||
        IsEqualGUID(&guidPixFormat, &GUID_PKPixelFormat16bppGray)) { // ** => Y transcoding
        pDecoder->guidPixFormat = guidPixFormat;
        pDecoder->WMP.wmiI.cfColorFormat = Y_ONLY;
        if (grayImage) {
            *grayImage = YES;
        }
    } else if (IsEqualGUID(&guidPixFormat, &GUID_PKPixelFormat24bppRGB) &&
               pDecoder->WMP.wmiI.cfColorFormat == CMYK) { // CMYK = > RGB
        pDecoder->WMP.wmiI.cfColorFormat = CF_RGB;
        pDecoder->guidPixFormat = guidPixFormat;
        pDecoder->WMP.wmiI.bRGB = 1; //RGB
        if (grayImage) {
            *grayImage = NO;
        }
    } else {
        if (grayImage) {
            *grayImage = NO;
        }
    }
    
    PixelFormatLookup(&PI, LOOKUP_FORWARD);
    
    U8 uAlphaMode;
    if (!!(PI.grBit & PK_pixfmtHasAlpha)) {
        uAlphaMode = 2;  //default is image & alpha for formats with alpha
    } else {
        uAlphaMode = 0;  //otherwise, 0
    }
    
    pDecoder->WMP.wmiSCP.bfBitstreamFormat = 0;
    pDecoder->WMP.wmiSCP.uAlphaMode = uAlphaMode;
    pDecoder->WMP.wmiSCP.sbSubband = 0;
    pDecoder->WMP.bIgnoreOverlap = 0;
    
    pDecoder->WMP.wmiI.cfColorFormat = PI.cfColorFormat;
    
    pDecoder->WMP.wmiI.bdBitDepth = PI.bdBitDepth;
    pDecoder->WMP.wmiI.cBitsPerUnit = PI.cbitUnit;
    
    //==== Validate thumbnail decode parameters =====
    pDecoder->WMP.wmiI.cThumbnailWidth = pDecoder->WMP.wmiI.cWidth;
    pDecoder->WMP.wmiI.cThumbnailHeight = pDecoder->WMP.wmiI.cHeight;
    pDecoder->WMP.wmiI.bSkipFlexbits = FALSE;
    
    pDecoder->WMP.wmiI.cROILeftX = 0;
    pDecoder->WMP.wmiI.cROITopY = 0;
    pDecoder->WMP.wmiI.cROIWidth = pDecoder->WMP.wmiI.cThumbnailWidth;
    pDecoder->WMP.wmiI.cROIHeight = pDecoder->WMP.wmiI.cThumbnailHeight;
    
    pDecoder->WMP.wmiI.oOrientation = O_NONE;
    
    pDecoder->WMP.wmiI.cPostProcStrength = 0;
    
    pDecoder->WMP.wmiSCP.bVerbose = 0;
    
    {
        PKRect rect = {0, 0, 0, 0};
        rect.Width = (I32)(pDecoder->WMP.wmiI.cROIWidth);
        rect.Height = (I32)(pDecoder->WMP.wmiI.cROIHeight);
        
        image = PKImageEncode_CopyRect(pDecoder, &rect);
    }
    
Cleanup:
    if (WMP_errUnsupportedFormat == err) {
        printf("*** ERROR: Unsupported format in JPEG XR ***\n");
    }
    return image;
}

CGImageRef CreateImageForJPEGXRFile(NSString *filePath, BOOL *grayImage) {
    ERR err = WMP_errSuccess;
    
    CGImageRef image = NULL;
    PKCodecFactory *pCodecFactory = NULL;
    PKImageDecode *pDecoder = NULL;
    
    Call(PKCreateCodecFactory(&pCodecFactory, WMP_SDK_VERSION));
    Call(pCodecFactory->CreateDecoderFromFile([filePath fileSystemRepresentation], &pDecoder));
    
    image = createImageForDecoder(pDecoder, grayImage);
    
Cleanup:
    if (pCodecFactory) {
        pCodecFactory->Release(&pCodecFactory);
    }
    
    if (pDecoder) {
        pDecoder->Release(&pDecoder);
    }
    
    if (WMP_errUnsupportedFormat == err) {
        printf("*** ERROR: Unsupported format in JPEG XR ***\n");
    }
    
    return image;
}

CGImageRef CreateImageForJPEGXRData(NSData *data, BOOL *grayImage) {
    ERR err = WMP_errSuccess;
    CGImageRef image = NULL;
    
    const PKIID *pIID = &IID_PKImageWmpDecode;
    
    struct WMPStream *pStream = NULL;
    PKImageDecode *pDecoder = NULL;

    // create stream
    Call(CreateWS_Memory(&pStream, [data bytes], data.length));
    pStream->Write = WriteWS_ReadOnly;
    
    // Create decoder
    Call(PKCodecFactory_CreateCodec(pIID, (void **) &pDecoder));
    
    // attach stream to decoder
    Call(pDecoder->Initialize(pDecoder, pStream));
    pDecoder->fStreamOwner = !0;
    
    image = createImageForDecoder(pDecoder, grayImage);
    
Cleanup:
    if (pDecoder) {
        pDecoder->Release(&pDecoder);
    }
    return image;
}

NSData *UIImageJPEGXRRepresentation(UIImage *image, BOOL grayImage) {
    ERR err = WMP_errSuccess;
    
    struct WMPStream *pEncodeStream = NULL;
    PKCodecFactory *pCodecFactory = NULL;
    PKImageEncode *pEncoder = NULL;
    PKPixelFormatGUID guidPixFormat = grayImage ? GUID_PKPixelFormat8bppGray : GUID_PKPixelFormat32bppBGRA;
    U8 *buffer = NULL;
    U8 *pb = NULL;
    NSData *representation = nil;
    
    //================================
    PKPixelInfo pPITo;
    pPITo.pGUIDPixFmt = &guidPixFormat;
    PixelFormatLookup(&pPITo, LOOKUP_FORWARD);
    
    U32 uWidth = [image size].width;
    U32 uHeight = [image size].height;
    U32 cbStrideTo = (BD_1 == pPITo.bdBitDepth ? ((pPITo.cbitUnit * uWidth + 7) >> 3) : (((pPITo.cbitUnit + 7) >> 3) * uWidth));

    //================================
    buffer = malloc(cbStrideTo * uHeight);
    Call(CreateWS_NSMutableData(&pEncodeStream));
    
    //================================
    Call(PKCreateCodecFactory(&pCodecFactory, WMP_SDK_VERSION));
    Call(pCodecFactory->CreateCodec(&IID_PKImageWmpEncode, &pEncoder));
    
    // init codec parameter
    CWMIStrCodecParam wmiSCP;
    memset(&wmiSCP, 0, sizeof(wmiSCP));
    wmiSCP.bVerbose = FALSE;
    wmiSCP.cfColorFormat = YUV_444;
    wmiSCP.bdBitDepth = BD_LONG;
    wmiSCP.bfBitstreamFormat = FREQUENCY;
    wmiSCP.bProgressiveMode = TRUE;
    wmiSCP.olOverlap = OL_ONE;
    wmiSCP.cNumOfSliceMinus1H = wmiSCP.cNumOfSliceMinus1V = 0;
    wmiSCP.sbSubband = SB_ALL;
    wmiSCP.uAlphaMode = 0;
    wmiSCP.uiDefaultQPIndex = 1;
    wmiSCP.uiDefaultQPIndexAlpha = 1;
    
    {
        PKPixelInfo PI;
        PKRect rect = {0, 0, uWidth, uHeight};
        
        PI.pGUIDPixFmt = &guidPixFormat;
        Call(PixelFormatLookup(&PI, LOOKUP_FORWARD));
        if ((PI.grBit & PK_pixfmtHasAlpha) && wmiSCP.uAlphaMode == 0) {
            wmiSCP.uAlphaMode = 2; // with Alpha and no default, set default as Planar
        }
        
        FailIf(PI.uSamplePerPixel > 1 &&
               PI.uBitsPerSample > 8 &&
               wmiSCP.cfColorFormat != YUV_444, WMP_errInvalidArgument);
        
        //================================
        Call(pEncoder->Initialize(pEncoder, pEncodeStream, &wmiSCP, sizeof(wmiSCP)));
        pEncoder->WMP.wmiSCP.uiDefaultQPIndex = 1;
        
        if (pEncoder->WMP.wmiSCP.uAlphaMode == 2) {
            pEncoder->WMP.wmiSCP_Alpha.uiDefaultQPIndex = wmiSCP.uiDefaultQPIndexAlpha;
        }
        
        Call(pEncoder->SetPixelFormat(pEncoder, guidPixFormat));
        Call(pEncoder->SetSize(pEncoder, rect.Width, rect.Height));
        Call(pEncoder->SetResolution(pEncoder, 96, 96));
        
        //================================
        // actual dec/enc with local buffer
        Call(PKAllocAligned((void **)&pb, cbStrideTo * rect.Height, 128));
        
        CGColorSpaceRef colorSpace = grayImage ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
        CGBitmapInfo bitmapInfo = grayImage ? (CGBitmapInfo)kCGImageAlphaNone : (kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Little);
        CGContextRef context = CGBitmapContextCreate(pb, uWidth, uHeight,
                                                     8, cbStrideTo, colorSpace,
                                                     bitmapInfo);
        CGColorSpaceRelease(colorSpace);
        
        CGContextDrawImage(context, CGRectMake(0, 0, uWidth, uHeight), [image CGImage]);
        CGContextRelease(context);
        
        Call(pEncoder->WritePixels(pEncoder, rect.Height, pb, cbStrideTo));
        
        CZMemoryStream *stream = (CZMemoryStream *)(pEncodeStream->state.pvObj);
        representation = [NSData dataWithData:stream.buffer];
    }
    
Cleanup:
    if (pEncoder) {
        pEncoder->Release(&pEncoder);
    }

    if (pb) {
        PKFreeAligned((void **)&pb);
    }
    
    if (buffer) {
        free(buffer);
        buffer = NULL;
    }
    
    if (pCodecFactory) {
        pCodecFactory->Release(&pCodecFactory);
    }
    
    if (err != WMP_errSuccess) {
        return nil;
    } else {
        return representation;
    }
}
