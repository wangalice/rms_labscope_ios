//
//  jxrlibTests.m
//  jxrlibTests
//
//  Created by Jin Ralph on 4/7/14.
//  Copyright (c) 2014 Microsoft. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>

#import "jxrlib.h"

extern int main1();
extern int main2();

@interface jxrlibTests : XCTestCase

@property (nonatomic, copy) NSString *filePath;

@end

@implementation jxrlibTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSString *testCaseName = NSStringFromSelector(self.invocation.selector);
    NSString *fileName = [@"jxrlibTests " stringByAppendingString:testCaseName];
    fileName = [fileName stringByAppendingPathExtension:@"jxr"];
    
    NSString *path = @"~";
    path = [path stringByExpandingTildeInPath];
    path = [path stringByAppendingPathComponent:fileName];
    
    self.filePath = path;
    [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:NULL];
    
    UIGraphicsBeginImageContext(CGSizeMake(256, 256));
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(c, 1, 0, 0, 1);
    CGContextFillRect(c, CGRectMake(0, 0, 64, 64));
    CGContextSetRGBFillColor(c, 0, 1, 0, 1);
    CGContextFillRect(c, CGRectMake(0, 64, 64, 64));
    CGContextSetRGBFillColor(c, 0, 0, 1, 1);
    CGContextFillRect(c, CGRectMake(0, 128, 64, 64));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data = UIImageJPEGXRRepresentation(image, YES);
    if (data) {
         [data writeToFile:path atomically:TRUE];
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:NULL];
    
    [super tearDown];
}

- (void)testWriteToMem {
    XCTAssertTrue([[NSFileManager defaultManager] fileExistsAtPath:self.filePath]);
}

- (void)testReadFromFile {
    [self measureBlock:^{
        BOOL gray;
        CGImageRef image = CreateImageForJPEGXRFile(self.filePath, &gray);

        XCTAssert(image);
        CGImageRelease(image);
    }];
}

- (void)testReadFromData {
    [self measureBlock:^{
        NSData *data = [NSData dataWithContentsOfFile:self.filePath];
        
        CGImageRef image = CreateImageForJPEGXRData(data, NULL);
        
        XCTAssert(image);
        CGImageRelease(image);
    }];
}

@end
