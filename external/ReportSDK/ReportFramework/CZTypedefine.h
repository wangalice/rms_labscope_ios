//
//  CZTypedefine.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#ifndef ReportSDK_CZTypedefine_h
#define ReportSDK_CZTypedefine_h

/// the generated report type
typedef NS_ENUM(NSUInteger, ReportEngineReportType) {
    ReportEngineReportTypePDF,
    ReportEngineReportTypeRTF,
    ReportEngineReportTypeRTFD,
    ReportEngineReportTypeNone,
};


#endif
