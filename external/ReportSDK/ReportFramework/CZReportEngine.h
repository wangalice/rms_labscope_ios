//
//  CZReportEngine.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZTypedefine.h"
   

/**
 * @brief the delegate of generate report,is used to notify current generating PDF or RTF state,progress.
 * @param none
 * @param none
 */
@protocol GenerateReportDelegate<NSObject>

@required

/**
 * @brief notify current generated report type ,generated progress and file desination.
 * @param type: the needed generated report type.
 * @param percent: the current generating percentage.
 * @param filePath: the file stored location.
 */
- (void)generateProgress:(ReportEngineReportType)type
       currentPercentage:(CGFloat)percent
             destination:(NSString *) filePath;

/**
 * @brief notify the generated state is finished.
 * @param filePath: the file stored location.
 */
 - (void)generateDidFinishDestination:(NSString *) filePath;


/**
 * @brief notify the generated report failed.
 * @param error: indicate some error information.
 */
- (void)generateDidFailWithError:(NSError *)error;

@end


@interface CZReportEngine : NSObject

@property (nonatomic, retain)   NSDictionary                *dataDic;
@property (nonatomic, readonly) NSDictionary                *templateDic;
@property (nonatomic, assign)   id<GenerateReportDelegate>  delegate;
@property (atomic, assign, readonly) BOOL                   isCancelled;

/**
 * @brief a initialized method,is used to construct the report engine.
 * @param data:the original data,is used to combine with the systemplate.
 * @param systemplate:the template defined by external user.
 */
- (id)initEngine:(NSDictionary*)data reportTemplate:(NSDictionary*) systemplate;

/**
 * @brief a asynchronous method.
 * @param fileName: the original data,is used to combine with the systemplate.
 * @param rate: given the image compressed rate, its value should more than 0 but less than 1.
 * @param dpi: given the device's dpi, which is used to convert and decrease the image memory size.
 */
- (void)exportReport:(NSString*)fileName
          reportType:(ReportEngineReportType)type
   imageCompressRate:(CGFloat)rate
              useDpi:(CGFloat)dpi;


/**
 * @brief this method is used to validate the data and template is valid,if not, return boolean value.
 * @param error:contain some error information.
 * @param isForReport:indicate the report data is prepared for generating report.
 * @return boolean value indicate the validation is successful.
 */
- (BOOL)validateReport:(NSError **)error isReport:(BOOL)isForReport;

/**
 * @brief scan all the element in template,if the element need value,it will be added in the array.
 * @return the elements array .
 */
- (NSArray *)scanTemplate;

/**
 * @brief generate the thumbnails base on the thumbanil range.When the range's location is 0,the thumbnail begin with PDF 
 *  page 1,and the range's length should not be more than the total PDF pages
 * @param thumnailRange:the range for generate the PDF thumnails
 * @param aSize: define every PDF thumnails image size.
 * @return the thumbnail image array .
 */
- (NSArray *)generateThumbnails:(NSRange)thumbnailRange
                  thumbnailSize:(CGSize)aSize;

- (void)cancel;

@end
