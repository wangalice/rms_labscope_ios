//
//  CZDictionaryTool.h
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CZDictionaryTool : NSObject

/// conver the dictionay to UIColor object 
+ (UIColor *)dictionay2Color:(NSDictionary *)aDic;

/// make sure the color dictionary is right,if not,use the default color.
+ (NSDictionary *)validateColor:(NSDictionary *)aDic;

///make sure the font size is right,if not,use the default font size.
+ (NSDictionary *)validateFontDic:(NSDictionary *)fontDic;

/// conver the dictionay to CGRect
+ (CGRect)dictionay2Frame:(NSDictionary *)aDic;

/// conver the dictionay to CGSize
+ (CGSize)dictionay2Size:(NSDictionary *)aDic;

///  judge the text has been assigned
+ (BOOL)isHaveAssigned:(NSString *)aText;

/// extract the parameter ,delete the $ symbol
+ (NSString *)extractKey:(NSString *)orginalText;

/// return the default font dictionary (include font size,and font color)
+ (NSDictionary *)defaultFontDic;

/// get the total left space of iOS device
+ (CGFloat) getDiskTotalSpace;

/// return the default color ,if the template did not provide the color difination
+ (NSDictionary *)defaultColor;

+ (NSDictionary *)fontAttributes:(UIFont *)aFont;

+ (CGSize)textSize:(NSString *)text useFont:(UIFont *)aFont;

+ (CGSize)textSize:(NSString *)text
           useFont:(UIFont *)aFont
    constraintSize:(CGSize )aSize;

@end
