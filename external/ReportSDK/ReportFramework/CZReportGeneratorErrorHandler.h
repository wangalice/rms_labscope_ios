//
//  CZReportGeneratorErrorHandler.h
//  ReportSDK
//
//  Created by Johnny on 3/11/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kReportGeneratorErrorDomain     @"com.carlzeiss.Hermens.ReportGeneratorErrorDomain"
#define kErrorMsgIsShowInBrowser        0

enum
{
    /// data error code
    CZReportGeneratorErrorCodeNone = 0,
    CZReportGeneratorErrorCodeUnexpected = -10000,
    
    CZReportGeneratorErrorReportDataError = -1,
    
    CZReportGeneratorErrorTextValueNotExist = -5,
    CZReportGeneratorErrorTextValueTypeNotMatch = -6,
    CZReportGeneratorErrorTextLengthNotMatch = -7,
    
    CZReportGeneratorErrorImageValueNotExist = -10,
    CZReportGeneratorErrorImageValueTypeNotMatch = -11,
    CZReportGeneratorErrorImageContentIsEmpty = -12,
    
    CZReportGeneratorErrorColumnValueNotExist = -15,
    CZReportGeneratorErrorColumnValueTypeNotMatch = -16,
    
    CZReportGeneratorErrorPieValueNotExist = -20,
    CZReportGeneratorErrorPieValueTypeNotMatch = -21,
    
    CZReportGeneratorErrorTableValueNotExist = -25,
    CZReportGeneratorErrorTableValueTypeNotMatch = -26,
    
    CZReportGeneratorErrorHeaderElementValueNotExist = -30,
    CZReportGeneratorErrorHeaderElementValueTypeNotMatch = -31,
    
    CZReportGeneratorErrorTemplateNoBodyContent = -35,

    
    CZReportGeneratorErrorTableValueArraySubelementTypeNotMatch = -36,
    CZReportGeneratorErrorTableValueArrayNoContent = -37,
    
    CZReportGeneratorErrorPieValueArraySubelementTypeNotMatch = -38,
    CZReportGeneratorErrorPieValueArrayNoContent = -39,
    
    CZReportGeneratorErrorColumnValueArraySubelementTypeNotMatch = -40,
    CZReportGeneratorErrorColumnValueArrayNoContent = -41,

    
    CZReportGeneratorErrorImageTitleNotExist = -50,
    CZReportGeneratorErrorImageTitleTypeNotMatch = -51,
    
    
    
    /// The followed is the template error code .
    CZReportGeneratorErrorReportTemplateError = -100,
    CZReportGeneratorErrorTemplateElementLayoutFault = -101,
   
    
    /// Text element error code.
    CZReportGeneratorErrorTextFrameError = -105,
    CZReportGeneratorErrorTextOrderError = -106,
    CZReportGeneratorErrorTextTypeError = -107,
    CZReportGeneratorErrorTextValueError = -108,
    CZReportGeneratorErrorTextCanbreakError = -109,
    CZReportGeneratorErrorTextRequiredError = -110,
    
    CZReportGeneratorErrorTextFrameKeyNotExist = -111,
    CZReportGeneratorErrorTextOrderKeyNotExist= -112,
    CZReportGeneratorErrorTextTypeKeyNotExist = -113,
    CZReportGeneratorErrorTextValueKeyNotExist = -114,
    CZReportGeneratorErrorTextCanbreakKeyNotExist = -115,
    CZReportGeneratorErrorTextRequiredKeyNotExist = -116,
    
    
    
    /// Image element error code.
    CZReportGeneratorErrorImageFrameError = -150,
    CZReportGeneratorErrorImageOrderError = -151,
    CZReportGeneratorErrorImageTypeError = -152,
    CZReportGeneratorErrorImageValueError = -153,
    CZReportGeneratorErrorImageCanbreakError = -154,
    CZReportGeneratorErrorImageRequiredError = -155,
    
    CZReportGeneratorErrorImageFrameKeyNotExist = -156,
    CZReportGeneratorErrorImageOrderKeyNotExist = -157,
    CZReportGeneratorErrorImageTypeKeyNotExist = -158,
    CZReportGeneratorErrorImageValueKeyNotExist = -159,
    CZReportGeneratorErrorImageCanbreakKeyNotExist = -160,
    CZReportGeneratorErrorImageRequiredKeyNotExist = -161,
    
    
    
    /// Column element error code.
    CZReportGeneratorErrorColumnFrameError = -200,
    CZReportGeneratorErrorColumnOrderError = -201,
    CZReportGeneratorErrorColumnTypeError = -202,
    CZReportGeneratorErrorColumnValueError = -203,
    CZReportGeneratorErrorColumnCanbreakError = -204,
    CZReportGeneratorErrorColumnRequiredError = -205,
    
    CZReportGeneratorErrorColumnFrameKeyNotExist = -206,
    CZReportGeneratorErrorColumnOrderKeyNotExist = -207,
    CZReportGeneratorErrorColumnTypeKeyNotExist = -208,
    CZReportGeneratorErrorColumnValueKeyNotExist = -209,
    CZReportGeneratorErrorColumnCanbreakKeyNotExist = -210,
    CZReportGeneratorErrorColumnRequiredKeyNotExist = -211,
    
    
    
    /// Pie chart element error code.
    CZReportGeneratorErrorPieFrameError = -250,
    CZReportGeneratorErrorPieOrderError = -251,
    CZReportGeneratorErrorPieTypeError = -252,
    CZReportGeneratorErrorPieValueError = -253,
    CZReportGeneratorErrorPieCanbreakError = -254,
    CZReportGeneratorErrorPieRequiredError = -255,
    
    CZReportGeneratorErrorPieFrameKeyNotExist = -256,
    CZReportGeneratorErrorPieOrderKeyNotExist = -257,
    CZReportGeneratorErrorPieTypeKeyNotExist = -258,
    CZReportGeneratorErrorPieValueKeyNotExist= -259,
    CZReportGeneratorErrorPieCanbreakKeyNotExist = -260,
    CZReportGeneratorErrorPieRequiredKeyNotExist = -261,
    
    
    
    /// Table element error code.
    CZReportGeneratorErrorTableFrameError = -300,
    CZReportGeneratorErrorTableOrderError = -301,
    CZReportGeneratorErrorTableTypeError = -302,
    CZReportGeneratorErrorTableValueError = -303,
    CZReportGeneratorErrorTableCanbreakError = -304,
    CZReportGeneratorErrorTableRequiredError = -305,
    
    CZReportGeneratorErrorTableColumnDefinitionError = -306,
    CZReportGeneratorErrorTableColumnSubKeysMissing = -307,
    
    CZReportGeneratorErrorTableFrameKeyNotExist = -308,
    CZReportGeneratorErrorTableOrderKeyNotExist = -309,
    CZReportGeneratorErrorTableTypeKeyNotExist = -310,
    CZReportGeneratorErrorTableValueKeyNotExist = -311,
    CZReportGeneratorErrorTableCanbreakKeyNotExist = -312,
    CZReportGeneratorErrorTableRequiredKeyNotExist = -313,
    
    
    
    /// Line element error code.
    CZReportGeneratorErrorLineFrameError = -350,
    CZReportGeneratorErrorLineOrderError = -351,
    CZReportGeneratorErrorLineTypeError = -352,
    CZReportGeneratorErrorLineValueError = -353,
    CZReportGeneratorErrorLineCanbreakError = -354,
    
    CZReportGeneratorErrorLineFrameKeyNotExist = -355,
    CZReportGeneratorErrorLineOrderKeyNotExist = -356,
    CZReportGeneratorErrorLineTypeKeyNotExist = -357,
    CZReportGeneratorErrorLineValueKeyNotExist = -358,
    CZReportGeneratorErrorLineCanbreakKeyNotExist = -359,
    
    
        
    /// Header element error code.
    CZReportGeneratorErrorHeaderElementFrameError = -400,
    CZReportGeneratorErrorHeaderElementOrderError = -401,
    CZReportGeneratorErrorHeaderElementTypeError = -402,
    CZReportGeneratorErrorHeaderElementValueError = -403,
    CZReportGeneratorErrorHeaderElementCanbreakError = -404,
    CZReportGeneratorErrorHeaderElementRequiredError = -405,
    
    CZReportGeneratorErrorHeaderElementFrameKeyNotExist = -406,
    CZReportGeneratorErrorHeaderElementOrderKeyNotExist = -407,
    CZReportGeneratorErrorHeaderElementTypeKeyNotExist = -408,
    CZReportGeneratorErrorHeaderElementValueKeyNotExist = -409,
    CZReportGeneratorErrorHeaderElementCanbreakKeyNotExist = -410,
    CZReportGeneratorErrorHeaderElementRequiredKeyNotExist = -411,
    
    /// Header roote error code.
    CZReportGeneratorErrorHeaderRootFrameError = -412,
    CZReportGeneratorErrorHeaderRootFrameKeyNotExist = -413,

    CZReportGeneratorErrorHeaderRootElementsArrayError = -414,
    CZReportGeneratorErrorHeaderRootElementsArrayKeyNotExist = -415,
    
    CZReportGeneratorErrorHeaderRootTypeError = -416,
    CZReportGeneratorErrorHeaderRootTypeKeyNotExist = -417,
    
     /// Footer roote error code.
    CZReportGeneratorErrorFooterRootFrameError = -500,
    CZReportGeneratorErrorFooterRootFrameKeyNotExist = -501,
    
    CZReportGeneratorErrorFooterRootElementsArrayError = -502,
    CZReportGeneratorErrorFooterRootElementsArrayKeyNotExist = -503,
    
    CZReportGeneratorErrorFooterRootTypeError = -504,
    CZReportGeneratorErrorFooterRootTypeKeyNotExist = -505,
    
    CZReportGeneratorErrorFooterRootPageInfoError = -506,
    CZReportGeneratorErrorFooterRootPageInfoKeyNotExist = -507,
    
    
    /// General can break error code.
    CZReportGeneratorErrorTemplateCanbreakSettingError = -550,
    CZReportGeneratorErrorTemplateElementTitleNotDictionary = -551, //titleText
    CZReportGeneratorErrorTemplateElementTitleTextDefinedError = -552,
    CZReportGeneratorErrorTemplateElementTitleValueNotExist = -553,
    CZReportGeneratorErrorTemplateElementTitleValueTypeError = -554,
    CZReportGeneratorErrorTemplateElementTitleLengthNotMatch = -555,
    
    /// pageInfo element error code.
    CZReportGeneratorErrorPageInfoFrameError = -600,
    CZReportGeneratorErrorPageInfoOrderError = -601,
    CZReportGeneratorErrorPageInfoTypeError = -602,
    CZReportGeneratorErrorPageInfoValueError = -603,
    CZReportGeneratorErrorPageInfoCanbreakError = -604,
    
    CZReportGeneratorErrorPageInfoFrameKeyNotExist = -605,
    CZReportGeneratorErrorPageInfoOrderKeyNotExist = -606,
    CZReportGeneratorErrorPageInfoTypeKeyNotExist = -607,
    CZReportGeneratorErrorPageInfoValueKeyNotExist = -608,
    CZReportGeneratorErrorPageInfoCanbreakKeyNotExist = -609,
    
    /// User and system error.
    CZReportGeneratorErrorSystemError = -1000,
    CZReportGeneratorErrorMemoryNotEnough = -1001,
    CZReportGeneratorErrorExportFileCreateFailed = -1002,
    CZReportGeneratorErrorUserCanceled = -1003,
    
    CZReportGeneratorErrorDataAndTemplateNotValidated = -1050,
    CZReportGeneratorErrorDataAndTemplateValidateFailed = -1051,
    
    CZReportGeneratorErrorPageNumberUnexpected = -1100,
    CZReportGeneratorErrorResourceMissing = -1101,
    CZReportGeneratorErrorWriteRTFToFileUnexpected = -1102,
    
    
    CZReportGeneratorErrorTemplateKeyGeneralError = -1150,
    CZReportGeneratorErrorTemplateKeyMissingError = -1151,
    CZReportGeneratorErrorTemplateKeyDuplicateError = -1152,

    CZReportGeneratorErrorTemplateMetadataError = -1200,
    CZReportGeneratorErrorTemplateMetadataMissing = -1201,
    
    CZReportGeneratorErrorTemplateElementTypeMissing = -1250,
    CZReportGeneratorErrorTemplateElementContentNotDictionary = -1251,
    CZReportGeneratorErrorTemplateElementTypeNotMatch = -1252,
    CZReportGeneratorErrorTemplateElementForbidInHeader = -1253,
    CZReportGeneratorErrorTemplateElementForbidInFooter = -1254
    
};

typedef NSInteger CZReportGeneratorErrorCode;

@interface CZReportGeneratorErrorHandler : NSObject

/**
 * @brief create the error with code and user information,the information is contained in a dictionary.
 * @return NSError object,contain the error code,error detail description.
 */
+ (NSError *)createErrorWithCode:(CZReportGeneratorErrorCode) code userInfo:(NSDictionary*) desc;

/**
 * @brief create the error with code and user information.
 * @return NSError object,contain the error code,error detail description.
 */
+ (NSError*)createErrorWithCode:(CZReportGeneratorErrorCode) code description:(NSString*) desc;

/**
 * @brief get the error information by error code.
 * @return NSError object,contain the error code,error detail description.
 */
+ (NSString*)getErrorMessage:(CZReportGeneratorErrorCode) code;

/**
 * @brief create the error with error code.
 * @return NSError object,contain the error code,error detail description.
 */
+ (NSError*)createErrorWithCode:(CZReportGeneratorErrorCode) code;
@end
