//
//  Global.h
//  
//
//  Created by BCD-MAC-004 on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#ifndef ReportSDK_Global_h
#define ReportSDK_Global_h

#undef CanUseLog

#ifndef CZLog

#ifdef  DEBUG

#ifdef CanUseLog

#define CZLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#define CZLogRect(r) NSLog(@"+++++++(%.1fx%.1f)-(%.1fx%.1f)+++++++", r.origin.x, r.origin.y, r.size.width, r.size.height)
#define CZLogSize(s) NSLog(@"(width:%.1fx-height:%.1f)", s.width, s.height)
#define CZLogPoint(p) NSLog(@"(pX:%.1fx-pY:%.1f)", p.x, p.y)

#else

#define CZLog(...)
#define CZLogRect(r)
#define CZLogPoint(p)
#define CZLogSize(s)

#endif

#define isDebug   1

#else

#define CZLog(...)
#define CZLogRect(r)
#define CZLogPoint(p)
#define CZLogSize(s)
#define isDebug   0

#endif
#endif

#endif

#define FreeObj(obj) if(obj){[obj release]; obj = nil;}

/// define the localizable strings file 
#define kReportLocalStrings @"ReportLocalizable"

/// report template keys defiantion

#define kPageSize                   @"pageSize"
#define kReportTemplateMetadata      @"reportTemplateMetadata"
#define kReportHeader               @"reportHeader"
#define kReportFooter               @"reportFooter"
#define kReportBody                 @"reportBody"
#define kRenderOrder                @"renderOrder"
#define kPageInfo                   @"pageInfo"

/// report template element definition
#define  kHeaderElement             @"headerElement"
#define  kLineElement               @"lineElement"
#define  kPageInfoElement           @"pageInfoElement"
#define  kPieElement                @"pieChartElement"
#define  kColumnElement             @"columnChartElement"
#define  kTableElement              @"tableElement"
#define  kTableColumnCellElement    @"tableColumnCellElement"
#define  kTableColumnElement        @"tableColumnElement"
#define  kImageElement              @"imageElement"
#define  kTextElement               @"textElement"
#define  kCanBreak                  @"canBreak"
#define  kPageBodyHeight            @"PageBodyHeight"
#define  kPageBodyWidth             @"pageBodyWidth"

/// report element common key definition
#define  kOrder                     @"order"
#define  kElementType               @"elementType"
#define  kFrame                     @"frame"
#define  kVersion                   @"version"
#define  kValue                     @"value"
#define  kValues                    @"values"
#define  kUnit                      @"unit"
#define  kTitle                     @"title"
#define  kTitleText                 @"titleText"
#define  kCaption                   @"caption"
#define  kCaptionText               @"titleText"
#define  kIsLargestSegmentShow      @"isLargestSegmentShow"

/// text element sub key definitoin
#define kTextValue                  @"textValue"
#define kFont                       @"font"
#define kFontSize                   @"size"
#define kColor                      @"color"
#define kRedColor                   @"r"
#define kGreenColor                 @"g"
#define kBlueColor                  @"b"
#define kAlphaColor                 @"a"
#define kMinLength                  @"minLength"
#define kMaxLength                  @"maxLength"
#define kFontName                   @"name"

/// pie chart sub key definition
#define kPieComponentArray          @"pieComponentArray"

/// column element sub key definition
#define kColumnComponentArray       @"columnComponentArray"
#define kComponentValue             @"componentValue"
#define kComponentTitle             @"componentTitle"
#define kComponentAdditionalValues  @"additionals"
#define kCurrentValue               @"currentValue"


/// table element sub key definition
#define kTableColumnCellValue       @"tableColumnCellValue"
#define kTableValue                 @"tableValue"
#define kTableEvaluationElement     @"tableEvaluationElement"
#define kTableColumnTitle           @"tableColumnTitle"
#define kTableColumnId              @"tableColumnId"
#define kTableRowFontMargin         1

#define kColumnNum                  @"columnNum"
#define kTableColumns               @"columns"
#define kEvaluationTitle            @"evaluationTitle"
#define kDataEvaluation             @"dataEvaluation"
#define kReplaceValue               @"$value$"

#define kEvaluationMethod           @"method"
#define kAVERAGE                    @"Average"
#define kMAX                        @"Max"
#define kMIN                        @"Min"
#define kSum                        @"Sum"
#define kSTDEV                      @"STDEV"
#define kSTDEVP                     @"STDEVP"
#define kCanBreak                   @"canBreak"

/// image element sub key definition
#define kImageFile                  @"imageFile"
#define kImageCaption               @"imageCaption"
#define kImageRenderMode            @"renderMode"
#define kImageRenderModeTile        @"tile"
#define kImageRenderModeMultipage   @"multipage"


/// header or footer element sub key definition
#define kHeaderElementValue         @"headerElementValue"
#define kHeaderElements             @"headerElements"
#define kFooterElements             @"footerElements"
#define kBodyElements               @"bodyElements"

#define kIsInFooter                 @"IsShowInFooter"
#define kAlignmentCenter            @"\\qc"
#define kAlignmentLeft              @"\\ql"
#define kAlignmentRight             @"\\qr"
#define kLineBreak                  @"\\par"
/// sort class attribute key
#define kRenderOrderSortAttribute   @"_renderOrder"

/// define the common layout values,something like page margin,or render element margin .
#define kEveryPageInter             2
#define kTextRenderBottomMargin     19
#define kTableTitleHeight           24
#define kPageBottomTopMargin        0
#define kDefaultTextFontSize        15.0
#define kMinTextFontSize            6.0
#define kMaxTextFontSize            72.0
/** Pixel = 1/72 inch, twips = 1/1440 inch. */
#define kPixelToTwips               20
#define kPDFFontSizeToRTFFontSize   2.0
#define kPdfToRTFScale              1.0
#define kJpegCompressPercent        1.0
#define kDefaultTextFont            @"ArialMT"
#define kDefaultTextFontForWindows  @"Arial"

/// define the text data verfiy keys

#define kValueFormat                @"valueFormat"
#define kTitleFormat                @"titleFormat"
#define kTitleText                  @"titleText"

#define kValueType                  @"type"
#define kReportHeader               @"reportHeader"
#define kReportBody                 @"reportBody"
#define kReportFooter               @"reportFooter"
#define kReportTemplateMetadata      @"reportTemplateMetadata"

#define kReportName                 @"reportName"
#define kReportDate                 @"date"

#define kRed                        @"red"
#define kGreen                      @"green"
#define kBlue                       @"blue"
#define kCyan                       @"cyan"
#define kYellow                     @"yellow"
#define kMagenta                    @"magenta"
#define kOrange                     @"orange"
#define kPurple                     @"purple"
#define kBrown                      @"brown"
#define kPieColors                  @"pieColors"
#define kReuiredKey                 @"mandatoryField"

#define kErrorCode                  @"errorCode"
#define kErrorDescription           @"errorDescription"
#define kErrorDetails               @"errorDetails"
#define kErrorDes                   @"errorDescription"


#define kStringType                 @"NSString"
#define kArrayType                  @"NSArray"
#define kDictionaryType             @"NSDcitonary"
#define kNumberType                 @"NSNumber"
#define kNoValueKey                 @"keyValueSegmentNotExist"

#define kPageWidth                  612
#define kPageHeight                 792

#define kIsUseImageBase64Data       @"IsUseImageBase64Data"
#define kDataFromTemplate           @"reportSampleData"

/// new added for the table filter
#define kFilter                     @"filter"
#define kType                       @"type"
#define kExpression                 @"expression"

/// new added for default image value
#define kDefaultImageValue          @"defaultValue"

/// add for RTF footer page info
#define kTab                        @"\\tab"


#define kTabDefaultTwipWidth        23

///   Any time, for make rtf be similar with PDF, we should make
///   page width = 12240 - kPaperMargin(X) * 2 = 612 pixel * kTabTwipNum

///   page height = ? - kPaperMargin(Y) * 2 = (719 - headerH - footerH) pixel * kTabTwipNum

#define PaperLayOut                 @"\\deftab23\\paperw12240\\paperh20000\\margl0\\margr0\\margt1440\\margb1440\\footery0"

/// headeryN:Header is N twips from the top of the page (the default is 720).
#define kRTFHeaderHeight            @"\\headery120"





