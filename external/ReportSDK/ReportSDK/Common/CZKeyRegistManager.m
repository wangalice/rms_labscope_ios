//
//  CZKeyRegistManager.m
//  TemplateManagement
//
//  Created by Johnny on 6/27/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import "CZKeyRegistManager.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"

@implementation CZKeyTypeObj

@synthesize key;
@synthesize type;

- (void)dealloc{
    FreeObj(key);
    FreeObj(type);
    [super dealloc];
}

@end



static CZKeyRegistManager *keyReister = nil;

@interface CZKeyRegistManager (){
    NSMutableArray *_keyDicArray;
}

@end

@implementation CZKeyRegistManager

#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (keyReister == nil)
			keyReister = [[CZKeyRegistManager alloc] init];
	}
    return keyReister;
}

- (CZError*)keyDuplicateValidate:(NSArray *)keyDicArray{
    CZError *keyDupError = nil;
    FreeObj(_keyDicArray);
    _keyDicArray = [[NSMutableArray alloc]initWithArray:keyDicArray];
    
    NSMutableArray *keyTypeObjArray = [[NSMutableArray alloc]initWithCapacity:5];
    for (NSDictionary *dic in _keyDicArray) {
        CZKeyTypeObj *keyObj = [self convertElementDicToKeyTypeObj:dic];
        if (keyObj) {
            [keyTypeObjArray addObject:keyObj];
        }
    }
    
    NSMutableArray *duplicateObjArray = [[NSMutableArray alloc] initWithCapacity:5];
    for (CZKeyTypeObj *keyType in keyTypeObjArray) {
        if ([self isDuplicate:keyTypeObjArray compareObj:keyType]) {
            [duplicateObjArray addObject:keyType];
        }
    }
    FreeObj(keyTypeObjArray);
    
    if ([duplicateObjArray count]>0) {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateKeyDuplicateError];
        keyDupError = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                    order:-1
                                         errorElementType:@" "];
        [keyDupError.errorDetailMsgArray addObject:errorMsg];
        for (CZKeyTypeObj *obj in duplicateObjArray) {
            NSString *elementDes = [NSString stringWithFormat:@" duplicated element type is %@, key is %@",obj.type,obj.key];
            
            [keyDupError.errorDetailMsgArray addObject:elementDes];
        }
        keyDupError.errorCode = CZReportGeneratorErrorTemplateKeyDuplicateError;
        [keyDupError autorelease];
        FreeObj(duplicateObjArray);
        return keyDupError;
    }
    FreeObj(duplicateObjArray);
    return nil;
}

- (BOOL) isDuplicate:(NSMutableArray *)totalArray compareObj:(CZKeyTypeObj *)aObj{
    BOOL isDup = NO;
    if (!totalArray || [totalArray count]<1 || !aObj) {
        return isDup;
    }
    for (CZKeyTypeObj *subObj in totalArray) {
        if ([subObj.key isEqualToString:aObj.key]) {
            if(![subObj.type isEqualToString:aObj.type]){
                isDup = YES;
                if ([subObj.type isEqualToString:kPieElement]
                    || [subObj.type isEqualToString:kColumnElement]) {
                    if ([aObj.type isEqualToString:kPieElement]
                        || [aObj.type isEqualToString:kColumnElement]) {
                        isDup = NO;
                    }
                }
                if (isDup) {
                    return isDup;
                }
            }
        }
    }
    return  isDup;
}

- (CZKeyTypeObj *)convertElementDicToKeyTypeObj:(NSDictionary *)elementDic{
    assert([elementDic allKeys]>0);
    CZKeyTypeObj *obj = nil;
    NSString *elementType = [elementDic valueForKey:kElementType];
    NSDictionary *value = [elementDic valueForKey:kValue];
    
    if (!elementType || !value || ![value isKindOfClass:[NSDictionary class]]) {
        return obj;
    }
    if ([elementType isEqualToString:kHeaderElement]) {
        NSString *key = [value valueForKey:kHeaderElementValue];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kHeaderElement;
                return [obj autorelease];
            }
        }
        return obj;
        
    }else if ([elementType isEqualToString: kTextElement]){
        NSString *key = [value valueForKey:kTextValue];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kTextElement;
                return [obj autorelease];
            }
        }
        return obj;
        
    }else if ([elementType isEqualToString: kImageElement]){
        NSString *key  = [value valueForKey:kImageFile];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kImageElement;
                return [obj autorelease];
            }
        }
        return obj;
        
    }else if ([elementType isEqualToString: kPieElement]){
        NSString *key = [value valueForKey:kPieComponentArray];
        assert([key isKindOfClass:[NSString class]]);
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kPieElement;
                return [obj autorelease];
            }
        }
        return obj;
    }else if ([elementType isEqualToString: kColumnElement]){
        NSString *key = [value valueForKey:kColumnComponentArray];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kColumnElement;
                return [obj autorelease];
            }
        }
        return obj;
    }else if ([elementType isEqualToString: kTableElement]){
        NSString *key = [value valueForKey:kTableValue];
        assert([key isKindOfClass:[NSString class]]);
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (key && [key length]>0) {
                obj = [[CZKeyTypeObj alloc]init];
                obj.key =key;
                obj.type = kTableElement;
                return [obj autorelease];
            }
        }
        return obj;
    }
    return obj;
}

- (void)dealloc{
    FreeObj(_keyDicArray);
    [super dealloc];
}

@end
