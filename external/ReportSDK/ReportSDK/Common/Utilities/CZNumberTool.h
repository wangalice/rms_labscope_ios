//
//  CZNumberTool.h
//  ReportSDK
//
//  Created by Johnny on 8/9/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZNumberTool : NSObject

+ (NSNumberFormatter *)sharedNumberFormatter;

+ (NSString *)localizedStringFromNumber:(NSNumber *)number
                              precision:(NSUInteger)precision;

+ (NSString *)localizedStringFromFloat:(CGFloat)number precision:(NSUInteger)precision;


@end
