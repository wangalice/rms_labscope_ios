//
//  NSArray+MutableDeepCopy.m
//  ReportSDK
//
//  Created by Ralph Jin on 1/23/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "NSArray+MutableDeepCopy.h"

@implementation NSArray (MutableDeepCopy)

- (NSMutableArray *)mutableDeepCopy {
    NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:self.count];
    for (id oneValue in self) {
        id oneCopy = nil;
        
        if ([oneValue respondsToSelector:@selector(mutableDeepCopy)]) {
            oneCopy = [oneValue mutableDeepCopy];
        } else if ([oneValue respondsToSelector:@selector(mutableCopy)]) {
            if (![oneValue isKindOfClass:[NSNumber class]] &&
                ![oneValue isKindOfClass:[NSDate class]]) {
                oneCopy = [oneValue mutableCopy];
            }
        }
        
        if (oneCopy == nil) {
            oneCopy = [oneValue copy];
        }
        
        [ret addObject:oneCopy];
        
        [oneCopy release];
    }

    return ret;
}

@end
