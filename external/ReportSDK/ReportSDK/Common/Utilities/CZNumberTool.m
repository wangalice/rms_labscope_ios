//
//  CZNumberTool.m
//  ReportSDK
//
//  Created by Johnny on 8/9/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZNumberTool.h"

@implementation CZNumberTool

+ (NSNumberFormatter *)sharedNumberFormatter {
    static NSNumberFormatter *numberFormatter = nil;
    if (numberFormatter == nil) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
        [numberFormatter setUsesGroupingSeparator:YES];
    }
    
    return numberFormatter;
}

+ (NSString *)localizedStringFromNumber:(NSNumber *)number precision:(NSUInteger)precision {
    NSNumberFormatter *formatter = [CZNumberTool sharedNumberFormatter];
    [formatter setMaximumFractionDigits:precision];
    return [formatter stringFromNumber:number];
}

+ (NSString *)localizedStringFromFloat:(CGFloat)number precision:(NSUInteger)precision {
    return [CZNumberTool localizedStringFromNumber:@(number) precision:precision];
}

@end
