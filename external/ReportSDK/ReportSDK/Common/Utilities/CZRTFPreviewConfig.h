//
//  CZRTFPreviewConfig.h
//  ReportSDK
//
//  Created by Johnny on 5/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZRTFPreviewConfig : NSObject

@property (nonatomic, readonly) BOOL        isForRTF;
@property (nonatomic, retain)   NSString    *fileDestination;
@property (nonatomic, readonly) NSString    *subRTFFilePath;

/**
 * @brief CZRTFPreviewConfig shared instance.
 * @return Returns the CZRTFPreviewConfig’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;

/// used for free the single instance
+ (void)freeInstance;

/// used for config some var to control the RTF generation.
- (void)configForRTF;

/// used for config some var to control the RTFD generation,only RTFD can be previewed by apple.
- (void)configForRTFD;

/// used to convert image data to image file which should put the same folder with TXT.rtf.
- (NSString *)convertImageData2ImageFile:(NSData *)imageData;

@end
