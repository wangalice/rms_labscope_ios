//
//  CZConvertImageToString.m
//  ReportSDK
//
//  Created by Johnny on 5/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

static const char digits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
    '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
    'z' };


#import "CZConvertImageToString.h"
#import "CZRTFTool.h"

@interface CZConvertImageToString() {
    NSString        *_tempFileName;
}

@end

@implementation CZConvertImageToString


static CZConvertImageToString *imageConverter = nil;

#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (imageConverter == nil)
			imageConverter = [[CZConvertImageToString alloc] init];
	}
    return imageConverter;
}

- (id)init{
    self = [super init];
    if (self) {
        NSString *temporaryDirectory = NSTemporaryDirectory();
        //make a full file name
        uint32_t tempID = (arc4random() % 1000000) + 1;
        NSString *fileName = [NSString stringWithFormat:@"%@/tempImageFile%d.txt", temporaryDirectory, tempID];
        FreeObj(_tempFileName);
        _tempFileName = [[NSString alloc]initWithString:fileName];
        CZLog(@"File path and name:%@", _tempFileName);
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        if (![filemgr fileExistsAtPath: _tempFileName]){
            [filemgr createFileAtPath:_tempFileName contents:nil attributes:nil];
        }
       
    }
    return self;
}

/**
 *  This method is used to reduce the memory using.
 *  Every time, creating temp file should delete before temp file
 *  Temp file is created by random number.
 */
- (void)initTempFile{
    @synchronized(self) {
        [self deleteTempFile];
        NSString *temporaryDirectory = NSTemporaryDirectory();
        
        //make a full file name
        uint32_t tempID = (arc4random() % 1000000) + 1;
        NSString *fileName = [NSString stringWithFormat:@"%@/tempImageFile%d.txt", temporaryDirectory, tempID];
        FreeObj(_tempFileName);
        _tempFileName = [[NSString alloc]initWithString:fileName];

        CZLog(@"File path and name:%@", _tempFileName);
        NSFileManager *filemgr;
        filemgr = [NSFileManager defaultManager];
        if (![filemgr fileExistsAtPath: _tempFileName]){
            [filemgr createFileAtPath:_tempFileName contents:nil attributes:nil];
        }
        
    }
}

- (void)deleteTempFile {
    @synchronized(self) {
        if (!_tempFileName && [_tempFileName length] < 1) {
            return;
        }
        
        NSError *deletFileError = nil;
        NSFileManager *filemgr = [NSFileManager defaultManager];
        if ([filemgr fileExistsAtPath: _tempFileName]) {
           [filemgr removeItemAtPath:_tempFileName
                               error:&deletFileError];
        }
    }
}

/**
 *  cover the image binary data content to string. 
 *  eg: image binary data:"f5527xa89"--->NSString:@"f5527xa89"
 */
- (NSString *)convertImageData2String:(NSData *)imageData {
    if (!imageData || [imageData length]<1) {
        return nil;
    }
    
    [self initTempFile];
    //[self openFile];
    NSUInteger len = [imageData length];
    Byte *byteData =(Byte*) [imageData bytes];
    Byte imageCache[1024*32];

    int convertBytesCount = 0;
    
    for (int i = 0; i <len; i++) {
        Byte tmpBytes[2];
        [self convertContent2UnsignedStrData:byteData[i] aShift:4 to:tmpBytes];
        memcpy(imageCache+convertBytesCount, tmpBytes, 2);
        convertBytesCount += 2;
        if (convertBytesCount >= (1024*32)) {
            
            NSData * _imageData = [[NSData alloc] initWithBytes:imageCache
                                                        length:convertBytesCount];
            [self appendDataToFile:_imageData];
            convertBytesCount = 0;
            [_imageData release];
        }
    }
    if (convertBytesCount > 0) {
        NSData * _imageData = [[NSData alloc] initWithBytes:imageCache
                                                    length:convertBytesCount];
        [self appendDataToFile:_imageData];
        [_imageData release];
    }
    //[self closeFile];
    NSError *readFileError = nil;
    NSString *imageStr = [[NSString alloc]initWithContentsOfFile:_tempFileName
                                                        encoding:NSASCIIStringEncoding
                                                           error:&readFileError];
     
    [imageStr autorelease];
    return imageStr;
}

- (void)convertContent2UnsignedStrData:(NSInteger)i aShift:(NSInteger)shift to:(Byte *)bytes {
    char buf[32];
    int charPos = 32;
    int radix = 1 << shift;
    int mask = radix - 1;
    do {
        buf[--charPos] = digits[i & mask];
        i >>= shift;
    } while (i != 0);
    
    char *tempPointer = buf;
    tempPointer +=  charPos;
    
    int offset = 32 - charPos;
    assert(offset < 3);
    if (offset == 1) {
        bytes[0] = '0';
        bytes[1] = *tempPointer;
    } else {
        memcpy(bytes, tempPointer, offset);
    }
}

/**
 *  Because usually image use memory content is large, so when convert image data to string 
 *  should divide the image several segment,and append string data to file ,so that reduce 
 *  the memory using.
 *
 */
- (void)appendDataToFile:(NSData *)theData{
    @synchronized(self) {
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        //assert([filemgr fileExistsAtPath: _tempFileName] == YES);
        if (![filemgr fileExistsAtPath: _tempFileName]) {
            return;
        }
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:_tempFileName];
        if (myHandle == nil) {
            myHandle = [NSFileHandle fileHandleForWritingAtPath:_tempFileName];
            if (myHandle == nil){
                return;
            }
        }
        //assert(myHandle!= nil);
        
        [myHandle seekToEndOfFile];
        [myHandle writeData:theData];
        [myHandle synchronizeFile];
        [myHandle closeFile];
    }
}

- (void)dealloc{
    FreeObj(_tempFileName);
    [super dealloc];
}

@end
