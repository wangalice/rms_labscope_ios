//
//  CZRTFTool.h
//  ReportSDK
//
//  Created by Johnny on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZRTFTool : NSObject

/// convert the element offset frame to RTF grammar string.
/// This method must break the line firstly,and add the space.
+ (NSString *)offsetFrame2RTFString:(CGRect)aFrame;

/// specially for the header element x offset.
+ (NSString *)xOffset2RTFString:(CGFloat)xOffset useFont:(UIFont *)aFont;

/// if xOffset <=0 ,return NULL ,if the xOffset == 0,can not generate linebreak. the external call should consider this situation.
+ (NSString *)xOffset2RTFString:(CGFloat)xOffset;

/// convert the xOffset to the table alignment :left,center,right.
+ (NSString *)xOffset2TableRTFString:(CGFloat)xOffset;

/// convert the xOffset to the text alignment :left,center,right.
+ (NSString *)xOffset2TextRTFString:(CGFloat)xOffset;

+ (NSString *)yOffset2RTFString:(CGFloat)yOffset;

/// i is per byte content,shift can be 2,3,4, for quaternary system, octal，hexadecimal.
+ (NSString *)convertContent2UnsignedStr:(NSInteger)i aShift:(NSInteger)shift;

+ (NSData *)convertContent2UnsignedStrData:(NSInteger) i aShift:(NSInteger)shift;

+ (NSString *)convertImageData2String:(NSData *)imageData;

/// convert the font size to RTF grammar.
+ (NSString *)fontSize2RTFString:(CGFloat)fontSize;

/// convert the chinese to unicode for RTF grammar use.
+ (NSString *)getNSStringUnicode:(NSString *)beforeStr;

/// convert the image data to image file ,this funtion is used to debug.
+ (NSString *) convertImage2ImageFile:(UIImage *)aImage;

+ (NSString *)textBoxLayOutFrameString:(CGRect)aRect;

/** Sample of textbox grammar.
 {\*\do\dobxcolumn\dobymargin\dodhgt\dptxbx\
 {\dptxbxtext\plain \ql
 \fs15\f0 This is textbox f This is textbox This is textbox
 }
 \dpx4000\dpy1900\dpxsize1400\dpysize236 \dplinesolid\dplinecor205\dplinecog255\dplinecob25\dplinew30
 }
 */
+ (NSString *)textBoxPre;

+ (NSString *)textBoxPreInHeaderOrFooter;

+ (NSString *)textBoxEnd:(CGRect)aRect;
+ (NSString *)textBoxEnd:(CGRect)aRect hasLineBreak:(BOOL)hasLineBreak;

@end
