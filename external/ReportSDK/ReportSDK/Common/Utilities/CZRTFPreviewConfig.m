//
//  CZRTFPreviewConfig.m
//  ReportSDK
//
//  Created by Johnny on 5/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFPreviewConfig.h"


@implementation CZRTFPreviewConfig

@synthesize isForRTF;
@synthesize fileDestination;
@synthesize subRTFFilePath;

static CZRTFPreviewConfig *rtfPreViewConfig = nil;

#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (rtfPreViewConfig == nil)
			rtfPreViewConfig = [[CZRTFPreviewConfig alloc] init];
	}
    return rtfPreViewConfig;
}

+ (void)freeInstance{
    if (rtfPreViewConfig) {
        FreeObj(rtfPreViewConfig);
    }
}

- (id)init{
    self = [super init];
    if (self) {
        isForRTF = YES;
    }
    return self;
}


- (void)configForRTF{
    isForRTF = YES;
}

/** specially,for rtfd, it is a folder and should contain the TXT.rtf file and some images that is referenced by TXT.rtf file.
 */
- (void)configForRTFD{
    isForRTF = NO;
    assert([fileDestination hasSuffix:@".rtfd"]);
    
   NSInteger filePathTotalLength = [fileDestination length];
   NSFileManager *fileManager = [NSFileManager defaultManager];
   NSString* dir = [fileDestination substringToIndex:filePathTotalLength-5];
    dir = [dir stringByAppendingString:@".rtfd/"];
    if (dir) {
       BOOL isFilePathValid =  [fileManager createDirectoryAtPath:dir
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        if (isFilePathValid) {
            FreeObj(subRTFFilePath);
            subRTFFilePath =[[NSString alloc] initWithString:[dir stringByAppendingPathComponent:@"TXT.rtf"]];
        }
    }
}

- (NSString *) convertImageData2ImageFile:(NSData *)imageData{
    BOOL isSuc = YES;

    NSInteger filePathTotalLength = [fileDestination length];
    
    NSString* dir = [fileDestination substringToIndex:filePathTotalLength-5];
    dir = [dir stringByAppendingString:@".rtfd/"];
    CZLog(@"the rtfd folder is %@",dir);
    /*
     * Generate image name by srand method,make sure the name is unique 
     * in RTFD folder
     */
    uint32_t srandValue = (arc4random() % 10000000) + 1;
    NSString *name = [NSString stringWithFormat:@"image%d.png",srandValue];
    NSString *imageFileName = [dir stringByAppendingPathComponent:name];
    isSuc = [imageData writeToFile:imageFileName atomically:YES];
    if (isSuc) {
        return name;
    }
    return nil;
}

- (void)dealloc{
    FreeObj(fileDestination);
    FreeObj(subRTFFilePath);
    [super dealloc];
}


@end
