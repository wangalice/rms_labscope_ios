//
//  CZError.h
//  ReportSDK
//
//  Created by Johnny on 6/20/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZErrorSubDomainType) {
    CZReportGeneratorSystemDomain,
    CZReportGeneratorMetadataDomain,
    CZReportGeneratorHeaderDomain,
    CZReportGeneratorBodyDomain,
    CZReportGeneratorFooterDomain,
};



@interface CZError : NSObject

/** this property indicate the error belong to whic sub-domain: eg,error happened
    in report header,or in report body,or system error happened.
 **/
@property (nonatomic,assign) CZErrorSubDomainType subDomainType;

/// this property indocate the error element's sequendce order in the elements array, if error not happend in template element, sequenceOrder = -1 .
@property (nonatomic,assign) NSInteger            sequenceOrder;

/// this property indicate the error element's type (like image,text,header and so on).
@property (nonatomic,retain) NSString             *elementType;

/// this array contain the detail message of error information.
@property (nonatomic,retain) NSMutableArray       *errorDetailMsgArray;

/// indicate the error code.
@property (nonatomic,assign) NSInteger             errorCode;

/// organization the error information and present it by string.
@property (nonatomic,readonly)NSString             *decription;

/// organization the error information and present it by dictionary.
@property (nonatomic,readonly)NSMutableDictionary  *dicInfo;


-(id)initWithDomain:(CZErrorSubDomainType)subDomain order:(NSInteger)aOrder;

-(id)initWithDomain:(CZErrorSubDomainType)subDomain
              order:(NSInteger)aOrder
   errorElementType:(NSString*)type;


@end
