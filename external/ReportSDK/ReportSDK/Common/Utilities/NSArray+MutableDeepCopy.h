//
//  NSArray+MutableDeepCopy.h
//  ReportSDK
//
//  Created by Ralph Jin on 1/23/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (MutableDeepCopy)

/// deeply copy the array. CAUTION the return value is already auto-released
- (id)mutableDeepCopy NS_RETURNS_RETAINED;

@end
