//
//  NSOutputStream+String.m
//  ReportSDK
//
//  Created by Halley Gu on 3/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "NSOutputStream+String.h"

@implementation NSOutputStream (String)

- (void)writeString:(NSString *)string {
    if ([string canBeConvertedToEncoding:NSASCIIStringEncoding]) {
        [self writeString:string withEncoding:NSASCIIStringEncoding];
    } else {
        [self writeString:string withEncoding:string.fastestEncoding];
    }
}

- (void)writeString:(NSString *)string withEncoding:(NSStringEncoding)encoding {
    NSData *data = [string dataUsingEncoding:encoding];
    if (data) {
        [self write:data.bytes maxLength:data.length];
    }
}

@end
