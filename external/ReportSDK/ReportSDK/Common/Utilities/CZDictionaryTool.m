//
//  CZDictionaryTool.m
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZDictionaryTool.h"

#include <sys/stat.h>
#include <sys/mount.h>
#import "NSDictionary+MutableDeepCopy.h"

@implementation CZDictionaryTool

/** In RTF document, we define the tab twips number is 15, shown page wide is 9180 = 15 * 612 twips, 
 *  and paper wide is 12240 twips.
 *
 *  Followed definition is  a example,we usually used the defines from CZGloable.h
 *  #define     kTabTwipNum         15
 *  #define     kPageTwipNum        9180
 *  #define     kPaperTwipNum       12240
 *  #define     kPaperMargin        1530
 *  #define     kRTFTextWidthAdjust 0.95
 */


+ (UIColor *)dictionay2Color:(NSDictionary *)aDic{
    //assert([[aDic allKeys] count]== 4);
    UIColor *color = nil;
    if ([aDic isKindOfClass:[NSDictionary class]]) {
        if ([[aDic allKeys] count]== 4) {
            id rObj = [aDic valueForKey:@"r"];
            id gObj = [aDic valueForKey:@"g"];
            id bObj = [aDic valueForKey:@"b"];
            id aObj = [aDic valueForKey:@"a"];
            if (rObj && gObj && bObj && aObj
                && [rObj isKindOfClass:[NSNumber class]]
                && [gObj isKindOfClass:[NSNumber class]]
                && [bObj isKindOfClass:[NSNumber class]]
                && [aObj isKindOfClass:[NSNumber class]]) {
                CGFloat r = [[aDic valueForKey:@"r"] floatValue];
                CGFloat g = [[aDic valueForKey:@"g"] floatValue];
                CGFloat b = [[aDic valueForKey:@"b"] floatValue];
                CGFloat a = [[aDic valueForKey:@"a"] floatValue];
                color = [UIColor colorWithRed:r green:g blue:b alpha:a];
                return color;
            }
            
        }
    }
    if (!color) {
        color = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.95];
    }
    return color;
}

+ (NSDictionary *)validateColor:(NSDictionary *)aDic{
    if (aDic && [aDic isKindOfClass:[NSDictionary class]]) {
        if ([[aDic allKeys] count]== 4) {
            id rObj = [aDic valueForKey:@"r"];
            id gObj = [aDic valueForKey:@"g"];
            id bObj = [aDic valueForKey:@"b"];
            id aObj = [aDic valueForKey:@"a"];
            if (rObj && gObj && bObj && aObj
                && [rObj isKindOfClass:[NSNumber class]]
                && [gObj isKindOfClass:[NSNumber class]]
                && [bObj isKindOfClass:[NSNumber class]]
                && [aObj isKindOfClass:[NSNumber class]]) {
                return aDic;
            }
            
        }
    }
    return [CZDictionaryTool defaultColor];
}

+ (NSDictionary *)validateFontDic:(NSDictionary *)fontDic{
    if (fontDic && [fontDic isKindOfClass:[NSDictionary class]]) {
        id sizeObj = [fontDic valueForKey:kFontSize];
        id nameObj = [fontDic valueForKey:kFontName];
        if (sizeObj && [sizeObj isKindOfClass:[NSNumber class]]
            && nameObj && [nameObj isKindOfClass:[NSString class]]) {
            return fontDic;
        }else{
            NSMutableDictionary *mutiDic = [NSMutableDictionary dictionaryWithDictionary:fontDic];
            
            if (!(sizeObj && [sizeObj isKindOfClass:[NSNumber class]])){
                [mutiDic setValue:[NSNumber numberWithInteger:kDefaultTextFontSize]
                           forKey:kFontSize];
            }
            if (!(nameObj && [nameObj isKindOfClass:[NSString class]])) {
                [mutiDic setValue:@"" forKey:kFontName];
            }
            return mutiDic;
        }
    }
    return nil;
}



+ (CGRect)dictionay2Frame:(NSDictionary *)aDic{
    //assert([[aDic allKeys] count]== 4);
    if (aDic && [[aDic allKeys] count] == 4) {
        CGFloat pointX = [[aDic valueForKey:@"x"] floatValue];
        CGFloat pointY = [[aDic valueForKey:@"y"] floatValue];
        CGFloat width = [[aDic valueForKey:@"width"] floatValue];
        CGFloat heigth = [[aDic valueForKey:@"height"] floatValue];
        CGRect aRect = CGRectMake(pointX, pointY, width, heigth);
        return aRect;
    }
    return CGRectZero;
}

+ (CGSize)dictionay2Size:(NSDictionary *)aDic{
    //assert([[aDic allKeys] count]== 2);
    if (aDic && [[aDic allKeys] count] == 2) {
        if ([[aDic valueForKey:@"width"] isKindOfClass:[NSNumber class]]
            && [[aDic valueForKey:@"height"] isKindOfClass:[NSNumber class]]) {
            CGFloat width = [[aDic valueForKey:@"width"] floatValue];
            CGFloat heigth = [[aDic valueForKey:@"height"] floatValue];
            CGSize size = CGSizeMake(width, heigth);
            return size;
        }
    }
    return CGSizeZero;
}

+ (BOOL)isHaveAssigned:(NSString *)aText{
    //assert([aText length]>0);
    BOOL isAssigned = YES;
    if (![aText isKindOfClass:[NSString class]]) {
        return isAssigned;
    }
    if (aText && [aText length]>2) {
        NSString *begin = [aText substringToIndex:1];
        NSString *end = [aText substringFromIndex:[aText length]-1];
        if ([begin isEqualToString:@"$"] && [end isEqualToString:@"$"]) {
            isAssigned = NO;
        }
        return isAssigned;
    }
    return isAssigned;
}

+ (NSString *)extractKey:(NSString *)orginalText{
    assert([orginalText length]>2);
    
    CZLog(@"the orginal key is %@",orginalText);
    NSString *begin = [orginalText substringToIndex:1];
    NSString *end = [orginalText substringFromIndex:[orginalText length]-1];
    assert([begin isEqualToString:@"$"]);
    assert([end isEqualToString:@"$"]);
    NSRange parameterRange = NSMakeRange(1, [orginalText length]-2);
    NSString *extractStr = [orginalText substringWithRange:parameterRange];
    CZLog(@"the extract key is %@",extractStr);
    return extractStr;
}

+ (NSDictionary *)defaultColor{
    NSMutableDictionary *color = [[NSMutableDictionary alloc] initWithCapacity:6];
    [color setValue:@(0.0) forKey:kRed];
    [color setValue:@(0.0) forKey:kGreen];
    [color setValue:@(0.0) forKey:kBlue];
    [color setValue:@(1.0) forKey:kAlphaColor];
    return [color autorelease];
}

+ (UIFont *)defaultFont{
    UIFont *font = [UIFont systemFontOfSize:kDefaultTextFontSize];
    return font;
}

+ (NSDictionary *)defaultFontDic{
    NSMutableDictionary *dic =[[[NSMutableDictionary alloc] initWithCapacity:6] autorelease];
    NSMutableDictionary *color = [[NSMutableDictionary alloc] initWithCapacity:6];
    [color setValue:@(0.0) forKey:kRed];
    [color setValue:@(0.0) forKey:kGreen];
    [color setValue:@(0.0) forKey:kBlue];
    [color setValue:@(1.0) forKey:kAlphaColor];
    [dic setValue:color forKey:kColor];
    [dic setValue:@(15.0) forKey:kFontSize];
    [dic setValue:@"" forKey:kFontName];
    [color release];
    return dic;
}

+ (NSDictionary *)fontAttributes:(UIFont *)aFont{
    if (!aFont)
        aFont = [UIFont systemFontOfSize:kDefaultTextFontSize];
    
     NSString *fontName = [aFont fontName];
    CTFontRef aRef = CTFontCreateWithName((CFStringRef)fontName, [aFont pointSize], NULL);
    CTFontDescriptorRef fontDescriptor = CTFontCopyFontDescriptor(aRef);
    NSDictionary* attributes = (NSDictionary *)CTFontDescriptorCopyAttributes(fontDescriptor);
    CFRelease(fontDescriptor);
    CFRelease(aRef);
    [attributes autorelease];
    return attributes;
}


+ (CGSize)textSize:(NSString *)text useFont:(UIFont *)aFont{
    if (!text || [text length]<1) {
        return CGSizeZero;
    }
    
    /**
        NSDictionary *fontAttributes = [CZDictionaryTool fontAttributes:aFont];
        NSAttributedString *attribStr = [[NSAttributedString alloc] initWithString:text
                                                                        attributes:fontAttributes];
        
        CGSize size = [attribStr size];
        FreeObj(attribStr);
    */
    
    if (![[NSThread currentThread] isMainThread]) {
        __block CGSize useSize = CGSizeZero;
        dispatch_sync(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                useSize = [text sizeWithFont:aFont];
                useSize.width = ceil(useSize.width);
                useSize.height = ceil(useSize.height);
            }
        });
        return useSize;
    }else{
        CGSize useSize = [text sizeWithFont:aFont];
        useSize.width = ceil(useSize.width);
        useSize.height = ceil(useSize.height);
        return useSize;
    }
}

+ (CGSize)textSize:(NSString *)text useFont:(UIFont *)aFont constraintSize:(CGSize)aSize{
    if (!text || [text length]<1) {
        return CGSizeZero;
    }
    if (![[NSThread currentThread] isMainThread]) {
        __block CGSize useSize = CGSizeZero;
        dispatch_sync(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                useSize = [text sizeWithFont:aFont constrainedToSize:aSize];
                useSize.width = ceil(useSize.width);
                useSize.height = ceil(useSize.height);
            }
        });
        return useSize;
    }else{
        CGSize useSize = [text sizeWithFont:aFont constrainedToSize:aSize];
        useSize.width = ceil(useSize.width);
        useSize.height = ceil(useSize.height);
        return useSize;
    }
}

+ (CGFloat) getDiskTotalSpace{
    struct statfs buf;
    long long totalspace = 0;
    if(statfs("/private/var", &buf) >= 0){
        totalspace = (long long)buf.f_bsize * buf.f_blocks;
    }
    const CGFloat kBytesPerMegaBytes = 1.0f / 1024.0f/ 1024.0f;
    CGFloat leftSpace = totalspace * kBytesPerMegaBytes;
    return leftSpace;
}

@end
