//
//  CZBase64Data.h
//  ReportSDK
//
//  Created by Johnny on 7/17/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

void *NewBase64Decode(
	const char *inputBuffer,
	size_t length,
	size_t *outputLength);

char *NewBase64Encode(
	const void *inputBuffer,
	size_t length,
	bool separateLines,
	size_t *outputLength);

@interface CZBase64Data:NSObject

+ (NSData *)dataFromBase64String:(NSString *)aString;

//- (NSString *)base64EncodedString;

@end
