//
//  CZReportGeneratorErrorHandler.m
//  ReportSDK
//
//  Created by Johnny on 3/11/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportGeneratorErrorHandler.h"


#ifdef  NSLocalizedString
#undef NSLocalizedString

#define NSLocalizedString(key, comment) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"ReportLocalizable"]

#endif


@implementation CZReportGeneratorErrorHandler

+ (NSError *)createErrorWithCode:(CZReportGeneratorErrorCode) code
                     description:(NSString*) desc{
    NSDictionary *userInfo = nil;
    if (kErrorMsgIsShowInBrowser) {
      userInfo = @{ NSLocalizedDescriptionKey : desc}; 
    }else{
      NSString *localizedUserInfoDes = [self replaceUnicode:desc];
      userInfo = @{ NSLocalizedDescriptionKey : localizedUserInfoDes };
    }
    
    return [NSError errorWithDomain:kReportGeneratorErrorDomain
                               code:code
                           userInfo:userInfo];
}

+ (NSString *)replaceUnicode:(NSString *)unicodeStr {
    
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""];
    NSString *tempStr3 = [[@"\""stringByAppendingString:tempStr2]stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString* returnStr =[NSPropertyListSerialization propertyListFromData:tempData mutabilityOption:NSPropertyListImmutable format:nil errorDescription:nil];
    
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"];
}

+ (NSError *)createErrorWithCode:(CZReportGeneratorErrorCode) code
                        userInfo:(NSDictionary*) desc{
    
    NSDictionary *userInfo = nil;
    if (kErrorMsgIsShowInBrowser) {
        userInfo = @{ NSLocalizedDescriptionKey : desc };
    }else{
        NSString *localizedUserInfoDes = [self replaceUnicode:[desc description]];
        userInfo = @{ NSLocalizedDescriptionKey : localizedUserInfoDes };
    }
    
    return [NSError errorWithDomain:kReportGeneratorErrorDomain
                               code:code
                           userInfo:userInfo];
}


+ (NSError*)createErrorWithCode:(CZReportGeneratorErrorCode) code {
    return [self createErrorWithCode:code
                         description:[self getErrorMessage:code]];
}

+ (NSString*)getErrorMessage:(CZReportGeneratorErrorCode) code {
    if (code == CZReportGeneratorErrorCodeNone) {
        return NSLocalizedString(@"CZErrorCodeNone", @"");
    }
    ///report data error code.
    switch (code) {
        case CZReportGeneratorErrorReportDataError:
            return NSLocalizedString(@"CZErrorReportDataError", @"");
        case CZReportGeneratorErrorTextValueNotExist:
            return NSLocalizedString(@"CZErrorTextValueNotExist", @"");
        case CZReportGeneratorErrorTextValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorTextValueTypeNotMatch", @"");
        case CZReportGeneratorErrorTextLengthNotMatch:
            return NSLocalizedString(@"CZErrorTextLengthNotMatch", @"");
        case CZReportGeneratorErrorImageValueNotExist:
            return NSLocalizedString(@"CZErrorImageValueNotExist", @"");
        case CZReportGeneratorErrorImageValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorImageValueTypeNotMatch", @"");
        case CZReportGeneratorErrorPieValueNotExist:
            return NSLocalizedString(@"CZErrorPieValueNotExist", @"");
        case CZReportGeneratorErrorPieValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorPieValueTypeNotMatch", @"");
        case CZReportGeneratorErrorColumnValueNotExist:
            return NSLocalizedString(@"CZErrorColumnValueNotExist", @"");
        case CZReportGeneratorErrorColumnValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorColumnValueTypeNotMatch", @"");
        case CZReportGeneratorErrorTableValueNotExist:
            return NSLocalizedString(@"CZErrorTableValueNotExist", @"");
        case CZReportGeneratorErrorTableValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorTableValueTypeNotMatch", @"");
        case CZReportGeneratorErrorHeaderElementValueNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementValueNotExist", @"");
        case CZReportGeneratorErrorHeaderElementValueTypeNotMatch:
            return NSLocalizedString(@"CZErrorHeaderElementValueTypeNotMatch", @"");
        case CZReportGeneratorErrorTemplateElementLayoutFault:
            return NSLocalizedString(@"CZErrorTemplateElementLayoutFault", @"");
            
            
        case CZReportGeneratorErrorTableValueArraySubelementTypeNotMatch:
            return NSLocalizedString(@"CZErrorTableValueArraySubelementTypeNotMatch", @"");
        case CZReportGeneratorErrorTableValueArrayNoContent:
            return NSLocalizedString(@"CZErrorTableValueArrayNoContent", @"");
            
        case CZReportGeneratorErrorPieValueArraySubelementTypeNotMatch:
            return NSLocalizedString(@"CZErrorPieValueArraySubelementTypeNotMatch", @"");
        case CZReportGeneratorErrorPieValueArrayNoContent:
            return NSLocalizedString(@"CZErrorPieValueArrayNoContent", @"");
         
        case CZReportGeneratorErrorColumnValueArraySubelementTypeNotMatch:
            return NSLocalizedString(@"CZErrorColumnValueArraySubelementTypeNotMatch", @"");
        case CZReportGeneratorErrorColumnValueArrayNoContent:
            return NSLocalizedString(@"CZErrorColumnValueArrayNoContent", @"");
            
        default:
            break;
    }
    
    /// report template error code
    switch (code) {
        case CZReportGeneratorErrorReportTemplateError:
            return NSLocalizedString(@"CZErrorReportTemplateError", @"");
            
        case CZReportGeneratorErrorTextFrameError:
            return NSLocalizedString(@"CZErrorTextFrameError", @"");
        case CZReportGeneratorErrorTextOrderError:
            return NSLocalizedString(@"CZErrorTextOrderError", @"");
        case CZReportGeneratorErrorTextTypeError: ///
            return NSLocalizedString(@"CZErrorTextTypeError", @"");
        case CZReportGeneratorErrorTextValueError:
            return NSLocalizedString(@"CZErrorTextValueError", @"");
        case CZReportGeneratorErrorTextCanbreakError:
            return NSLocalizedString(@"CZErrorTextCanbreakError", @"");
        case CZReportGeneratorErrorTextRequiredError:
            return NSLocalizedString(@"CZErrorTextRequiredError", @"");
                     
        case CZReportGeneratorErrorTextFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorTextFrameKeyNotExist", @"");
        case CZReportGeneratorErrorTextOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorTextOrderKeyNotExist", @"");
        case CZReportGeneratorErrorTextTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorTextTypeKeyNotExist", @"");
        case CZReportGeneratorErrorTextValueKeyNotExist:
            return NSLocalizedString(@"CZErrorTextValueKeyNotExist", @"");
        case CZReportGeneratorErrorTextCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorTextCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorTextRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorTextRequiredKeyNotExist", @"");
            
            
                        
        case CZReportGeneratorErrorImageFrameError:
            return NSLocalizedString(@"CZErrorImageFrameError", @"");
        case CZReportGeneratorErrorImageOrderError:
            return NSLocalizedString(@"CZErrorImageOrderError", @"");
        case CZReportGeneratorErrorImageTypeError:
            return NSLocalizedString(@"CZErrorImageTypeError", @"");
        case CZReportGeneratorErrorImageValueError:
            return NSLocalizedString(@"CZErrorImageValueError", @"");
        case CZReportGeneratorErrorImageCanbreakError:
             return NSLocalizedString(@"CZErrorImageCanbreakError", @"");
        case CZReportGeneratorErrorImageRequiredError:
            return NSLocalizedString(@"CZErrorImageRequiredError", @"");
         
        case CZReportGeneratorErrorImageFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorImageFrameKeyNotExist", @"");
        case CZReportGeneratorErrorImageOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorImageOrderKeyNotExist", @"");
        case CZReportGeneratorErrorImageTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorImageTypeKeyNotExist", @"");
        case CZReportGeneratorErrorImageValueKeyNotExist:
            return NSLocalizedString(@"CZErrorImageValueKeyNotExist", @"");
        case CZReportGeneratorErrorImageCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorImageCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorImageRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorImageRequiredKeyNotExist", @"");
            
            
            
        case CZReportGeneratorErrorColumnFrameError:
             return NSLocalizedString(@"CZErrorColumnFrameError", @"");
        case CZReportGeneratorErrorColumnOrderError:
            return NSLocalizedString(@"CZErrorColumnOrderError", @"");
        case CZReportGeneratorErrorColumnTypeError:
            return NSLocalizedString(@"CZErrorColumnTypeError", @"");
        case CZReportGeneratorErrorColumnValueError:
            return NSLocalizedString(@"CZErrorColumnValueError", @"");
        case CZReportGeneratorErrorColumnCanbreakError:
            return NSLocalizedString(@"CZErrorColumnCanbreakError", @"");
        case CZReportGeneratorErrorColumnRequiredError:
            return NSLocalizedString(@"CZErrorColumnRequiredError", @"");
            
            
        case CZReportGeneratorErrorColumnFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnFrameKeyNotExist", @"");
        case CZReportGeneratorErrorColumnOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnOrderKeyNotExist", @"");
        case CZReportGeneratorErrorColumnTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnTypeKeyNotExist", @"");
        case CZReportGeneratorErrorColumnValueKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnValueKeyNotExist.", @"");
        case CZReportGeneratorErrorColumnCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorColumnRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorColumnRequiredKeyNotExist", @"");
            
            
            
        case CZReportGeneratorErrorPieFrameError:
            return NSLocalizedString(@"CZErrorPieFrameError", @"");
        case CZReportGeneratorErrorPieOrderError:
            return NSLocalizedString(@"CZErrorPieOrderError", @"");
        case CZReportGeneratorErrorPieTypeError:
            return NSLocalizedString(@"CZErrorPieTypeError", @"");
        case CZReportGeneratorErrorPieValueError:
            return NSLocalizedString(@"CZErrorPieValueError", @"");
        case CZReportGeneratorErrorPieCanbreakError:
            return NSLocalizedString(@"CZErrorPieCanbreakError", @"");
        case CZReportGeneratorErrorPieRequiredError:
            return NSLocalizedString(@"CZErrorPieRequiredError", @"");
            
            
        case CZReportGeneratorErrorPieFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorPieFrameKeyNotExist", @"");
        case CZReportGeneratorErrorPieOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorPieOrderKeyNotExist", @"");
        case CZReportGeneratorErrorPieTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorPieTypeKeyNotExist", @"");
        case CZReportGeneratorErrorPieValueKeyNotExist:
            return NSLocalizedString(@"CZErrorPieValueKeyNotExist", @"");
        case CZReportGeneratorErrorPieCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorPieCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorPieRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorPieRequiredKeyNotExist", @"");

            
            
        case CZReportGeneratorErrorTableFrameError:
            return NSLocalizedString(@"CZErrorTableFrameError", @"");
        case CZReportGeneratorErrorTableOrderError:
            return NSLocalizedString(@"CZErrorTableOrderError", @"");
        case CZReportGeneratorErrorTableTypeError:
            return NSLocalizedString(@"CZErrorTableTypeError", @"");
        case CZReportGeneratorErrorTableValueError:
            return NSLocalizedString(@"CZErrorTableValueError", @"");
        case CZReportGeneratorErrorTableCanbreakError:
           return NSLocalizedString(@"CZErrorTableCanbreakError", @"");
        case CZReportGeneratorErrorTableRequiredError:
            return NSLocalizedString(@"CZErrorTableRequiredError", @"");
        case CZReportGeneratorErrorTableColumnDefinitionError:
            return NSLocalizedString(@"CZErrorTableColumnDefinitionError", @"");
        case CZReportGeneratorErrorTableColumnSubKeysMissing:
            return NSLocalizedString(@"CZErrorTableColumnSubKeysMissing", @"");
            
            
            
        case CZReportGeneratorErrorTableFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorTableFrameKeyNotExist", @"");
        case CZReportGeneratorErrorTableOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorTableOrderKeyNotExist", @"");
        case CZReportGeneratorErrorTableTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorTableTypeKeyNotExist", @"");
        case CZReportGeneratorErrorTableValueKeyNotExist:
            return NSLocalizedString(@"CZErrorTableValueKeyNotExist", @"");
        case CZReportGeneratorErrorTableCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorTableCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorTableRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorTableRequiredKeyNotExist", @"");
            
            
            
            
        case CZReportGeneratorErrorLineFrameError:
            return NSLocalizedString(@"CZErrorLineFrameError", @"");
        case CZReportGeneratorErrorLineOrderError:
            return NSLocalizedString(@"CZErrorLineOrderError", @"");
        case CZReportGeneratorErrorLineTypeError:
            return NSLocalizedString(@"CZErrorLineTypeError", @"");
        case CZReportGeneratorErrorLineValueError:
            return NSLocalizedString(@"CZErrorLineValueError", @"");
        case CZReportGeneratorErrorLineCanbreakError:
            return NSLocalizedString(@"CZErrorLineCanbreakError", @"");

        case CZReportGeneratorErrorLineFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorLineFrameKeyNotExist", @"");
        case CZReportGeneratorErrorLineOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorLineOrderKeyNotExist", @"");
        case CZReportGeneratorErrorLineTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorLineTypeKeyNotExist", @"");
        case CZReportGeneratorErrorLineValueKeyNotExist:
            return NSLocalizedString(@"CZErrorLineValueKeyNotExist", @"");
        case CZReportGeneratorErrorLineCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorLineCanbreakKeyNotExist", @"");
            
            
        case CZReportGeneratorErrorPageInfoFrameError:
            return NSLocalizedString(@"CZErrorPageInfoFrameError", @"");
        case CZReportGeneratorErrorPageInfoOrderError:
            return NSLocalizedString(@"CZErrorPageInfoOrderError", @"");
        case CZReportGeneratorErrorPageInfoTypeError:
            return NSLocalizedString(@"CZErrorPageInfoTypeError", @"");
        case CZReportGeneratorErrorPageInfoValueError:
            return NSLocalizedString(@"CZErrorPageInfoValueError", @"");
        case CZReportGeneratorErrorPageInfoCanbreakError:
            return NSLocalizedString(@"CZErrorPageInfoCanbreakError", @"");
            
        case CZReportGeneratorErrorPageInfoFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorPageInfoFrameKeyNotExist", @"");
        case CZReportGeneratorErrorPageInfoOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorPageInfoOrderKeyNotExist", @"");
        case CZReportGeneratorErrorPageInfoTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorPageInfoTypeKeyNotExist", @"");
        case CZReportGeneratorErrorPageInfoValueKeyNotExist:
            return NSLocalizedString(@"CZErrorPageInfoValueKeyNotExist", @"");
        case CZReportGeneratorErrorPageInfoCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorPageInfoCanbreakKeyNotExist", @"");
            
            
        case CZReportGeneratorErrorHeaderElementFrameError:
            return NSLocalizedString(@"CZErrorHeaderElementFrameError", @"");
        case CZReportGeneratorErrorHeaderElementOrderError:
            return NSLocalizedString(@"CZErrorHeaderElementOrderError", @"");
        case CZReportGeneratorErrorHeaderElementTypeError:
            return NSLocalizedString(@"CZErrorHeaderElementTypeError", @"");
        case CZReportGeneratorErrorHeaderElementValueError:
            return NSLocalizedString(@"CZErrorHeaderElementValueError", @"");
        case CZReportGeneratorErrorHeaderElementCanbreakError:
            return NSLocalizedString(@"CZErrorHeaderElementCanbreakError", @"");
        case CZReportGeneratorErrorHeaderElementRequiredError:
            return NSLocalizedString(@"CZErrorHeaderElementRequiredError", @"");
            
            
        case CZReportGeneratorErrorHeaderElementFrameKeyNotExist: 
            return NSLocalizedString(@"CZErrorHeaderElementFrameKeyNotExist", @"");
        case CZReportGeneratorErrorHeaderElementOrderKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementOrderKeyNotExist", @"");
        case CZReportGeneratorErrorHeaderElementTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementTypeKeyNotExist", @"");
        case CZReportGeneratorErrorHeaderElementValueKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementValueKeyNotExist", @"");
        case CZReportGeneratorErrorHeaderElementCanbreakKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementCanbreakKeyNotExist", @"");
        case CZReportGeneratorErrorHeaderElementRequiredKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderElementRequiredKeyNotExist", @"");
            
        case CZReportGeneratorErrorHeaderRootFrameError:
            return NSLocalizedString(@"CZErrorHeaderRootFrameError", @"");
        case CZReportGeneratorErrorHeaderRootFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderRootFrameKeyNotExist", @"");
            
        case CZReportGeneratorErrorHeaderRootTypeError:
            return NSLocalizedString(@"CZErrorHeaderRootTypeError", @"");
        case CZReportGeneratorErrorHeaderRootTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderRootTypeKeyNotExist", @"");
            
        case CZReportGeneratorErrorHeaderRootElementsArrayError:
            return NSLocalizedString(@"CZErrorHeaderRootElementsArrayError", @"");
        case CZReportGeneratorErrorHeaderRootElementsArrayKeyNotExist:
            return NSLocalizedString(@"CZErrorHeaderRootElementsArrayKeyNotExist", @"");
            
            
            
        case CZReportGeneratorErrorFooterRootFrameError:
            return NSLocalizedString(@"CZErrorFooterRootFrameError", @"");
        case CZReportGeneratorErrorFooterRootFrameKeyNotExist:
            return NSLocalizedString(@"CZErrorFooterRootFrameKeyNotExist", @"");
            
        case CZReportGeneratorErrorFooterRootTypeError:
            return NSLocalizedString(@"CZErrorFooterRootTypeError", @"");
        case CZReportGeneratorErrorFooterRootTypeKeyNotExist:
            return NSLocalizedString(@"CZErrorFooterRootTypeKeyNotExist", @"");
            
        case CZReportGeneratorErrorFooterRootElementsArrayError:
            return NSLocalizedString(@"CZErrorFooterRootElementsArrayError", @"");
        case CZReportGeneratorErrorFooterRootElementsArrayKeyNotExist:
            return NSLocalizedString(@"CZErrorFooterRootElementsArrayKeyNotExist", @"");
            
        case CZReportGeneratorErrorFooterRootPageInfoError:
            return NSLocalizedString(@"CZErrorFooterRootPageInfoError", @"");
        case CZReportGeneratorErrorFooterRootPageInfoKeyNotExist:
            return NSLocalizedString(@"CZErrorFooterRootPageInfoKeyNotExist", @"");
            
            
        case CZReportGeneratorErrorTemplateKeyGeneralError:
            return NSLocalizedString(@"CZErrorTemplateKeyGeneralError", @"");
        case CZReportGeneratorErrorTemplateKeyMissingError:
            return NSLocalizedString(@"CZErrorTemplateKeyMissingError", @"");
        case CZReportGeneratorErrorTemplateKeyDuplicateError:
            return NSLocalizedString(@"CZErrorTemplateKeyDuplicateError", @"");
        case CZReportGeneratorErrorTemplateMetadataError:
            return NSLocalizedString(@"CZErrorTemplateMetadataError", @"");
        case CZReportGeneratorErrorTemplateMetadataMissing:
            return NSLocalizedString(@"CZErrorTemplateMetadataMissing", @"");
        case CZReportGeneratorErrorTemplateNoBodyContent:
            return NSLocalizedString(@"CZErrorTemplateNoBodyContent", @"");
        case CZReportGeneratorErrorTemplateElementTypeMissing:
            return NSLocalizedString(@"CZErrorTemplateElementTypeMissing", @"");
        case CZReportGeneratorErrorTemplateElementTypeNotMatch:
            return NSLocalizedString(@"CZErrorTemplateElementTypeNotMatch", @"");
        case CZReportGeneratorErrorTemplateElementContentNotDictionary:
            return NSLocalizedString(@"CZErrorTemplateElementContentNotDictionary", @"");
        case CZReportGeneratorErrorTemplateElementForbidInHeader:
            return NSLocalizedString(@"CZErrorTemplateElementForbidInHeader", @"");
        case CZReportGeneratorErrorTemplateElementForbidInFooter:
            return NSLocalizedString(@"CZErrorTemplateElementForbidInFooter", @"");
        default:
            break;
    }
    
    /// system or user error code.
    switch (code) {
        case CZReportGeneratorErrorImageContentIsEmpty:
            return NSLocalizedString(@"CZErrorImageContentIsEmpty", @"");
        case CZReportGeneratorErrorTemplateCanbreakSettingError:
            return NSLocalizedString(@"CZErrorTemplateCanbreakSettingError", @"");
            
         /// The next is use for title format and title value validation.
        case CZReportGeneratorErrorTemplateElementTitleNotDictionary:
            return NSLocalizedString(@"CZErrorTemplateElementTitleNotDictionary", @"");
        case CZReportGeneratorErrorTemplateElementTitleTextDefinedError:
            return NSLocalizedString(@"CZErrorTemplateElementTitleTextDefinedError", @"");
        case CZReportGeneratorErrorTemplateElementTitleValueNotExist:
            return NSLocalizedString(@"CZErrorTemplateElementTitleValueNotExist", @"");
        case CZReportGeneratorErrorTemplateElementTitleValueTypeError:
            return NSLocalizedString(@"CZErrorTemplateElementTitleValueTypeError", @"");
        case CZReportGeneratorErrorTemplateElementTitleLengthNotMatch:
            return NSLocalizedString(@"CZErrorTemplateElementTitleLengthNotMatch", @"");
            
            
            
        case CZReportGeneratorErrorUserCanceled:
            return NSLocalizedString(@"CZErrorUserCanceled", @"");
        case CZReportGeneratorErrorSystemError:
            return NSLocalizedString(@"CZErrorSystemError", @"");
        case CZReportGeneratorErrorMemoryNotEnough:
            return NSLocalizedString(@"CZErrorMemoryNotEnough", @"");
        case CZReportGeneratorErrorDataAndTemplateNotValidated:
            return NSLocalizedString(@"CZErrorDataAndTemplateNotValidated", @"");
        case CZReportGeneratorErrorDataAndTemplateValidateFailed:
            return NSLocalizedString(@"CZErrorDataAndTemplateValidateFailed", @"");
        case CZReportGeneratorErrorResourceMissing:
            return NSLocalizedString(@"CZErrorResourceMissing", @"");
        case CZReportGeneratorErrorPageNumberUnexpected:
            return NSLocalizedString(@"CZErrorPageNumberUnexpected", @"");
        case CZReportGeneratorErrorWriteRTFToFileUnexpected:
            return NSLocalizedString(@"CZErrorWriteRTFToFileUnexpected", @"");
        case CZReportGeneratorErrorExportFileCreateFailed:
            return NSLocalizedString(@"CZErrorExportFileCreateFailed", @"");
    }
    NSString *localUnexpected = NSLocalizedString(@"CZErrorCodeUnexpected", @"");
    return [NSString stringWithFormat:@"%@%lx",localUnexpected, (long)code];
}

@end
