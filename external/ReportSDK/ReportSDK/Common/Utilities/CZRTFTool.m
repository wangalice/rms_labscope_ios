//
//  CZRTFTool.m
//  ReportSDK
//
//  Created by Johnny on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFTool.h"
#import "CZDictionaryTool.h"
#import "CZRTFFontColorRegistTable.h"

#define kLineBreakScale     0.8
#define kBodyTextWidthAdjust  18
#define kBodyTextHeightAdjust  9

static const char digits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
    '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
    'z' };

@implementation CZRTFTool

+ (NSString *)boxLayOutFrameString:(CGRect)aRect{
    int32_t xTwips = aRect.origin.x * kPixelToTwips * kPdfToRTFScale;
    int32_t yTwips = ceilf(aRect.origin.y * kPixelToTwips * kPdfToRTFScale);
    
    int32_t widthTwips = ceilf(aRect.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t heightTwips = ceilf(aRect.size.height * kPixelToTwips * kPdfToRTFScale);
    NSString *textLayout = [NSString stringWithFormat:@"\\shpleft%d\\shptop%d\\shpright%d\\shpbottom%d{\\*\\shpinst{\\sp{\\sn fLine}{\\sv FALSE}}{\\sp{\\sn fFilled}{\\sv FALSE}}{\\sp{\\sn dxTextLeft}{\\sv 0}}{\\sp{\\sn dyTextTop}{\\sv 0}}{\\sp{\\sn dxTextRight}{\\sv 0}}{\\sp{\\sn dyTextBottom}{\\sv 0}}{\\sp{\\sn dzColMargin}{\\sv 0}}{\\sp{\\sn ShapeType}{\\sv 202}}}",xTwips,yTwips,xTwips+widthTwips,yTwips+heightTwips];
    return textLayout;
}

+ (NSString *)textBoxLayOutFrameString:(CGRect)aRect {
    return [self boxLayOutFrameString:aRect];
}

+ (NSString *)textBoxPre{
    NSString *textBoxPre = [NSString stringWithFormat:@"%@",@"{\\pard{\\shp\\shpbxcolumn\\shpbymargin{\\shptxt\\plain\\ql"];
    return textBoxPre;
}

+ (NSString *)textBoxPreInHeaderOrFooter{
    NSString *textBoxPre = [NSString stringWithFormat:@"%@",@"{\\pard{\\shp\\shpbxcolumn\\shpbypage{\\shptxt\\plain\\ql"];
    return textBoxPre;
}

+ (NSString *)textBoxEnd:(CGRect)aRect{
    return [self textBoxEnd:aRect hasLineBreak:YES];
}

+ (NSString *)textBoxEnd:(CGRect)aRect hasLineBreak:(BOOL)hasLineBreak {
    NSString *layoutStr = [self textBoxLayOutFrameString:aRect];
    NSString *textBoxEnd = nil;
    if (hasLineBreak) {
        textBoxEnd = [NSString stringWithFormat:@"}%@}\\par}",layoutStr];
    } else {
        textBoxEnd = [NSString stringWithFormat:@"}%@}}",layoutStr];
    }
    return textBoxEnd;
}

+ (NSString *)xOffset2RTFString:(CGFloat)xOffset{
    NSInteger tabNumber = 0;
    NSMutableString *rtfTab = nil;
    tabNumber =ceilf(xOffset);
    if (tabNumber>0) {
        rtfTab = [[[NSMutableString alloc]initWithCapacity:100] autorelease];
        for (NSInteger i =0; i<tabNumber; i++){
            [rtfTab appendString:kTab];
        }
    }
    return rtfTab;
}

+ (NSString *)xOffset2RTFString:(CGFloat)xOffset useFont:(UIFont *)aFont{
    //assert(xOffset >= 0);
    @synchronized(self) {
        NSMutableString *rtfSpace = nil;
        if (xOffset <= 0) {
            return rtfSpace;
        }
        NSString *space = @" ";
        NSString *spaceGrammar = @"\\~";
        UIFont *defaultFont = aFont;
        if (defaultFont == nil) {
            defaultFont = [[CZRTFFontColorRegistTable sharedInstance] currentFont];
        }

        CGSize aSpaceSize = [CZDictionaryTool textSize:space useFont:defaultFont];

        NSInteger spaceNum = ceilf(xOffset/aSpaceSize.width);
    
        if (spaceNum > 0) {
            rtfSpace = [[[NSMutableString alloc]init] autorelease];
            for (NSInteger i = 0; i<spaceNum; i++) {
                [rtfSpace appendString:spaceGrammar];
            }
        }
        return rtfSpace;
    }
}

+ (NSString *)xOffset2TableRTFString:(CGFloat)xOffset{
   CGFloat pageWidth = [[[NSUserDefaults standardUserDefaults] valueForKey:kPageBodyWidth] floatValue
                        ];
    CGFloat margin = pageWidth/3;
    NSString *tableFormat = @"\\trqc";
    if (xOffset < margin) {
        tableFormat = @"\\trql";
    }else if (xOffset > margin * 2){
         tableFormat = @"\\trqr";
    }
    return tableFormat;
}

+ (NSString *)xOffset2TextRTFString:(CGFloat)xOffset{
    CGFloat pageWidth = [[[NSUserDefaults standardUserDefaults] valueForKey:kPageBodyWidth] floatValue
                         ];
    CGFloat margin = pageWidth/3;
    NSString *tableFormat = @"\\qc";
    if (xOffset < margin) {
        tableFormat = @"\\ql";
    }else if (xOffset > margin * 2){
        tableFormat = @"\\qr";
    }
    return tableFormat;
}


+ (NSString *)yOffset2RTFString:(CGFloat)yOffset{
    //assert(yOffset >= 0);
    NSMutableString *rtfStr = nil;
    if (yOffset < 0) {
        return rtfStr;
    }
    NSString *space = @" ";
    NSString *lineBreakGrammar = @"\\par";
    UIFont *defaultFont = [[CZRTFFontColorRegistTable sharedInstance] currentFont];
    //CGSize aSpaceSize = [space sizeWithFont:defaultFont];
    
    /**
    NSAttributedString *spaceAttributeStr = [[NSAttributedString alloc] initWithString:space
                                                                            attributes:[CZDictionaryTool fontAttributes:defaultFont]];
    CGSize aSpaceSize = [spaceAttributeStr size];
    FreeObj(spaceAttributeStr);
    */
    
    CGSize aSpaceSize = [CZDictionaryTool textSize:space useFont:defaultFont];
    
    NSInteger lineSpaceNum = ceilf(yOffset/(aSpaceSize.height * kLineBreakScale));
    if (lineSpaceNum == 0) {
        lineSpaceNum = 1;
    }
    rtfStr = [[[NSMutableString alloc]init] autorelease];
    for (NSInteger i = 0; i<lineSpaceNum; i++) {
            [rtfStr appendString:lineBreakGrammar];
    }
    return rtfStr;
}

+ (NSString *)offsetFrame2RTFString:(CGRect)aFrame{
    NSMutableString *rtfStr = [[NSMutableString alloc]init];
    NSString *yOffsetStr = [CZRTFTool yOffset2RTFString:aFrame.origin.y];
    NSString *xOffsetStr = [CZRTFTool xOffset2RTFString:aFrame.origin.x];
    if (yOffsetStr && [yOffsetStr length]>0) {
        [rtfStr appendString:yOffsetStr];
    }
    
    if (xOffsetStr && [xOffsetStr length]>0) {
        [rtfStr appendString:xOffsetStr];
    }
    if ([rtfStr length]<1) {
        [rtfStr appendString: @"\\~"];
    }
    return [rtfStr autorelease];
}

+ (NSString*)convertContent2UnsignedStr:(NSInteger) i aShift:(NSInteger)shift{
    char *buf = (char*)malloc(32);
    int charPos = 32;
    int radix = 1 << shift;
    int mask = radix - 1;
    do {
        buf[--charPos] = digits[i & mask];
        i >>= shift;
    } while (i != 0);
    
    char *tempPointer = buf;
    tempPointer +=  charPos;
    const char* parameter = tempPointer;
    //NSString * str = [NSString stringWithCString:parameter length:(32 - charPos)];
    NSString *str = [[[NSString alloc]initWithBytes:parameter length:(32 - charPos)
                                           encoding:NSASCIIStringEncoding] autorelease];
    free(buf);
    if ([str length] == 1) {
        NSString *standard = @"0";
        return [standard stringByAppendingString:str];
    }
    return str;
}

+ (NSData *)convertContent2UnsignedStrData:(NSInteger) i aShift:(NSInteger)shift{
    
    char *buf = (char*)malloc(32);
    int charPos = 32;
    int radix = 1 << shift;
    int mask = radix - 1;
    do {
        buf[--charPos] = digits[i & mask];
        i >>= shift;
    } while (i != 0);
    
    char *tempPointer = buf;
    tempPointer +=  charPos;
    const char* parameter = tempPointer;

    NSData *strData = [NSData dataWithBytes:parameter length:(32 - charPos)];
    free(buf);
    if ([strData length] == 1) {
        NSString *standStr = @"0";
        NSMutableData *standStrData =[[[NSMutableData alloc] initWithData:[standStr dataUsingEncoding:NSASCIIStringEncoding]] autorelease];
        [standStrData appendData:strData];
        return standStrData;
    }
    return strData;
}

/// TO DO...consider the performence,should be use asynchronous method.

+ (NSString *)convertImageData2String:(NSData *)imageData{

    if (!imageData || [imageData length]<1) {
        return nil;
    }
    NSUInteger len = [imageData length];
                                                    
    Byte *byteData =(Byte*) [imageData bytes];
    NSMutableData *imageStrData = [[NSMutableData alloc] init];
    for (NSInteger i = 0; i <len; i++) {
        NSData *convertStr = [CZRTFTool convertContent2UnsignedStrData:byteData[i]
                                                                aShift:4];
        [imageStrData appendData:convertStr];
    }
    //free(byteData);
    NSString *imageStr = [[NSString alloc]initWithData:imageStrData
                                              encoding:NSASCIIStringEncoding];
    //CZLog(@"the convert str is %@",imageStr);
    [imageStr autorelease];
    [imageStrData release];
    return imageStr;
}

+ (NSString *)fontSize2RTFString:(CGFloat)fontSize{
    long size = floorf(fontSize * kPDFFontSizeToRTFFontSize + 0.5);
    NSString *RTFStr = [NSString stringWithFormat:@"fs%ld", size];
    return RTFStr;
}

+ (NSString *)getNSStringUnicode:(NSString *)beforeStr {
    NSMutableString *result = [[[NSMutableString alloc] init] autorelease];
    for (int i = 0; i < beforeStr.length; i++) {
        unichar c = [beforeStr characterAtIndex:i];
        NSString *encoded = nil;
        if (c == '\\' || c == '{' || c == '}') {
            encoded = [NSString stringWithFormat:@"\\%C", c];
        } else if (c < 0x7F) {
            encoded = [NSString stringWithCharacters:&c length:1];
        } else {
            int16_t unicodeValue = (uint16_t)c - 0x10000;
            encoded = [NSString stringWithFormat:@"\\u%d?", unicodeValue];
        }
        [result appendString:encoded];
    }
    return result;
}

/**
  This function is used to generate RTFD file.
  Convert the UIImage object to image file and return file path for TXT.rtf reference.
 */

+ (NSString *) convertImage2ImageFile:(UIImage *)aImage{
    NSData *imageData = UIImagePNGRepresentation(aImage);
    BOOL isSuc = YES;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    uint32_t srandValue = (arc4random() % 10000000) + 1;
    NSString *name = [NSString stringWithFormat:@"image%d.png",srandValue];
    NSString *imageFileName = [documentsDirectory stringByAppendingPathComponent:name];
    isSuc = [imageData writeToFile:imageFileName atomically:YES];
    if (isSuc) {
        return name;
    }
    return nil;
}


@end
