//
//  NSOutputStream+String.h
//  ReportSDK
//
//  Created by Halley Gu on 3/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSOutputStream (String)

- (void)writeString:(NSString *)string;

- (void)writeString:(NSString *)string withEncoding:(NSStringEncoding)encoding;

@end
