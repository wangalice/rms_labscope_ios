//
//  CZConvertImageToString.h
//  ReportSDK
//
//  Created by Johnny on 5/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZConvertImageToString : NSObject

/**
 * @brief CZConvertImageToString shared instance.
 * @return Returns the CZConvertImageToString’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;

- (NSString *)convertImageData2String:(NSData *)imageData;

- (void)deleteTempFile;

@end
