//
//  CZError.m
//  ReportSDK
//
//  Created by Johnny on 6/20/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZError.h"
#import "CZGlobal.h"

#ifdef  NSLocalizedString
#undef NSLocalizedString

#define NSLocalizedString(key, comment) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"ReportLocalizable"]

#endif

@implementation CZError

@synthesize subDomainType;
@synthesize sequenceOrder;
@synthesize elementType;
@synthesize errorDetailMsgArray;
@synthesize errorCode;
@synthesize decription;
@synthesize dicInfo;

-(id)initWithDomain:(CZErrorSubDomainType)subDomain order:(NSInteger)aOrder{
    if (self = [super init]) {
        self.subDomainType = subDomain;
        self.sequenceOrder = aOrder;
        errorDetailMsgArray = [[NSMutableArray alloc]initWithCapacity:5];
        return self;
    }
    return nil;
}

-(id)initWithDomain:(CZErrorSubDomainType)subDomain
              order:(NSInteger)aOrder
   errorElementType:(NSString*)type{
    if (self = [super init]) {
        self.subDomainType = subDomain;
        self.sequenceOrder = aOrder;
        self.elementType = type;
        errorDetailMsgArray = [[NSMutableArray alloc]initWithCapacity:5];
        return self;
    }
    return nil;
}


- (NSString *)description{
    if (dicInfo) {
        return [[self dicInfo] description];
    }
    return nil;
}

- (NSDictionary *)dicInfo{
    FreeObj(dicInfo);
    dicInfo = [[NSMutableDictionary alloc ]initWithCapacity:5];
    NSString *domainStr = [self stringForDomainType:self.subDomainType];
    [dicInfo setValue:domainStr
               forKey:NSLocalizedString(@"CZErrorArea",@"")];
    
    
    [dicInfo setValue:[NSNumber numberWithInteger:sequenceOrder]
               forKey:NSLocalizedString(@"CZErrorSequence",@"")];
    
    
    [dicInfo setValue:elementType
               forKey:NSLocalizedString(@"CZErrorType",@"")];
    
    
    [dicInfo setValue:[NSNumber numberWithInteger:errorCode]
               forKey:NSLocalizedString(@"CZErrorOfCode",@"")];
    
    NSMutableString *detail = [[NSMutableString alloc]initWithCapacity:50];
    for (NSString *info in errorDetailMsgArray) {
        if ([info length]>0) {
            [detail appendString:info];
            //[detail appendString:@"//n"];
        }
    }
    
    if ([detail length]>0) {
        [dicInfo setValue:detail
                   forKey:NSLocalizedString(@"CZErrorDetailsInfo",@"")];
    }else{
        [dicInfo setValue:@" "
                   forKey:NSLocalizedString(@"CZErrorDetailsInfo",@"")];

    }
    FreeObj(detail);
    
    return dicInfo;
}


- (NSString *)stringForDomainType:(CZErrorSubDomainType)aType{
    switch (aType) {
        case CZReportGeneratorSystemDomain:
            return NSLocalizedString(@"CZErrorInSystem",@"");
            break;
        case CZReportGeneratorMetadataDomain:
            return NSLocalizedString(@"CZErrorInMetadata",@"");
            break;
        case CZReportGeneratorBodyDomain:
            return NSLocalizedString(@"CZErrorInBody",@"");
            break;
        case CZReportGeneratorFooterDomain:
            return NSLocalizedString(@"CZErrorInFooter",@"");
            break;
        case CZReportGeneratorHeaderDomain:
            return NSLocalizedString(@"CZErrorInHeader",@"");
            break;

        default:
            break;
    }
}


- (void)dealloc{
    FreeObj(errorDetailMsgArray);
    FreeObj(dicInfo);
    FreeObj(elementType);
    FreeObj(decription);
    [super dealloc];
}

@end
