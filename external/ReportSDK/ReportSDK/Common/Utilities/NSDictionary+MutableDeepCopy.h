//
//  NSDictionary+MutableDeepCopy.h
//  ReportSDK
//
//  Created by Johnny on 3/14/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MutableDeepCopy)

/// deeply copy the dictionary.
- (NSMutableDictionary *)mutableDeepCopy NS_RETURNS_RETAINED;

@end
