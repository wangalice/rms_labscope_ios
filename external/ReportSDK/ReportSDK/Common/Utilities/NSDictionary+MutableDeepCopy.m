//
//  NSDictionary+MutableDeepCopy.m
//  ReportSDK
//
//  Created by Johnny on 3/14/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "NSDictionary+MutableDeepCopy.h"

@implementation NSDictionary (MutableDeepCopy)

- (NSMutableDictionary *)mutableDeepCopy {
    NSMutableDictionary *ret = [[NSMutableDictionary alloc] initWithCapacity:[self count]];

    for (id key in [self allKeys]) {
        id oneValue = [self valueForKey:key];
        id oneCopy = nil;
        
        if ([oneValue respondsToSelector:@selector(mutableDeepCopy)]) {
            oneCopy = [oneValue mutableDeepCopy];
        } else if ([oneValue respondsToSelector:@selector(mutableCopy)]) {
            if (![oneValue isKindOfClass:[NSNumber class]] &&
                ![oneValue isKindOfClass:[NSDate class]]) {
                oneCopy = [oneValue mutableCopy];
            }
        }
        
        if (!oneCopy) {
            oneCopy = [oneValue copy];
        }
        
        [ret setValue:oneCopy forKey:key];
        
        [oneCopy release];
    }

    return ret;
}

@end
