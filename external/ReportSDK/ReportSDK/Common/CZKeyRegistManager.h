//
//  CZKeyRegistManager.h
//  TemplateManagement
//
//  Created by Johnny on 6/27/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZError.h"

@interface CZKeyTypeObj : NSObject
@property (nonatomic,retain) NSString   *key;
@property (nonatomic,retain) NSString   *type;

@end


@interface CZKeyRegistManager : NSObject

/**
 * @brief CZRTFPreviewConfig shared instance.
 * @return Returns the CZRTFPreviewConfig’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;

- (CZError*)keyDuplicateValidate:(NSArray *)keyDicArray;

@end
