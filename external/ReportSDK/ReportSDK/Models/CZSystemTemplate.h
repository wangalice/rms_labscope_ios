//
//  CZSystemTemplate.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZError;

@interface CZSystemTemplate : NSObject{
    NSMutableDictionary    *_originalTemplate;
    NSMutableDictionary    *_intergrateDic;
    
    NSMutableArray         *_errorObjArray;
}

@property(nonatomic,retain) NSMutableDictionary    *originalTemplate;
@property(nonatomic,retain) NSMutableDictionary    *intergrateDic;
@property(nonatomic,readonly) NSMutableArray       *errorObjArray;

/// init method.
- (id)initWithDictionary:(NSDictionary *)aDictionary;

/// generate formats from template dictionary.
- (BOOL)generateFormats;

/// validate the template.
- (BOOL)templateValidate;

/// validate the data.
- (BOOL)dataValidate:(NSDictionary *)data;

/// return the elements that need assign value.
- (NSArray *)needValueElements;

@end
