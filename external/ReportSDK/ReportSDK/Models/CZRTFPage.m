//
//  CZRTFPage.m
//  ReportSDK
//
//  Created by Johnny on 9/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFPage.h"
#import "CZPDFPage.h"
#import "NSOutputStream+String.h"

#import "CZHeaderRender.h"
#import "CZFooterRender.h"
#import "CZImageRender.h"
#import "CZTableRender.h"
#import "CZPieRender.h"
#import "CZColumnRender.h"
#import "CZColumnItem.h"

@interface CZRTFPage (){
    BOOL        _isLayOut;
}

/// lay out the page elements.
- (void)layOutEelements;

@end

@implementation CZRTFPage

- (void)layOutEelements{
    _isLayOut = YES;

    if (self.pageFooter) {
        self.pageFooter.totalPageNum = self.totalPageNum;
        self.pageFooter.curPageNum = self.pageIndex;
    }
    
    CGPoint offSet = self.pageBodyOffset;
    offSet.y = 0.0 - self.pageBodyFrame.origin.y;  // RTF no page header, so don't offset additionally in y direction
    for (CZRenderBase *renderObj in self.renders) {
        renderObj.renderFrame = CGRectOffset(renderObj.renderFrame, offSet.x, offSet.y);
    }
}

/**
 \pgwsxnN	N is the page width in twips. A \sectd resets the value to that specified by \paperwN in the document properties.
 \pghsxnN	N is the page height in twips. A \sectd resets the value to that specified by \paperhN in the document properties.
 */

- (void)drawPageIntoStream:(NSOutputStream *)stream {
    if (!_isLayOut) {
        [self layOutEelements];
    }
    if (self.pageIndex!=1) {
      [stream writeString:@"\\page"];
    }
    for (CZRenderBase *renderObj in self.renders) {
        [renderObj rtfRenderIntoStream:stream];
    }
}

- (void)dealloc{
    [super dealloc];
}

@end
