//
//  CZPDFPage.h
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CZHeaderRender.h"
#import "CZFooterRender.h"

@interface CZPDFPage : NSObject{

}

@property (nonatomic)        NSInteger      pageIndex;
@property (nonatomic)        NSInteger      totalPageNum;
@property (nonatomic)        CGSize         pageSize;
@property (nonatomic)        CGRect         pageBodyFrame;
@property (nonatomic,retain) NSMutableArray *renders;
@property (nonatomic,retain) CZFooterRender *pageFooter;
@property (nonatomic,retain) CZHeaderRender *pageHeader;
@property (nonatomic,assign) CGPoint        pageBodyOffset;

/// init method.
- (id) initWithBodyFrame:(CGRect)bodyFrame;

/// render the pdf page to pdf context.
- (void)drawPage;

@end
