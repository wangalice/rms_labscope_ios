//
//  CZPDFPage.m
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPDFPage.h"

@interface CZPDFPage (){
    NSInteger  _pageIndex;
    CGSize     _pageSize;
    CGRect     _pageBodyFrame;
    
    NSMutableArray  *_renders;
    
    CZFooterRender *_pageFooter;
    CZHeaderRender *_pageHeader;
    
    BOOL        _isLayOut;
}

/// lay out the page elements.
- (void)layOutEelements;

@end

@implementation CZPDFPage

@synthesize pageIndex = _pageIndex;
@synthesize totalPageNum;
@synthesize renders = _renders;
@synthesize pageFooter = _pageFooter;
@synthesize pageHeader = _pageHeader;
@synthesize pageSize = _pageSize;
@synthesize pageBodyFrame = _pageBodyFrame;

- (id) initWithBodyFrame:(CGRect)bodyFrame{
    self = [super init];
    if (self) {
        self.pageBodyFrame = bodyFrame;
        _renders = [[NSMutableArray alloc]initWithCapacity:3];
        _isLayOut = NO;
    }
    return self;
}

- (void)layOutEelements{
    
    _isLayOut = YES;

    if (_pageFooter) {
        _pageFooter.totalPageNum = self.totalPageNum;
        _pageFooter.curPageNum = self.pageIndex;
    }
    
    CGPoint offset = self.pageBodyOffset;
    offset.y -= self.pageBodyFrame.origin.y;
    for (CZRenderBase *renderObj in self.renders) {
        renderObj.renderFrame = CGRectOffset(renderObj.renderFrame, offset.x, offset.y);
    }
}

- (void)drawPage{
    if (!_isLayOut) {
        [self layOutEelements];
    }
    /**
     Start a new page,if the page have header,footer, render the header firstly,then render body,and then render footer
     */
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, _pageSize.width, _pageSize.height), nil);
    if (_pageHeader) {
        [_pageHeader render];
    }
    for (CZRenderBase *renderObj in _renders) {
        [renderObj render];
    }
    if (_pageFooter) {
        [_pageFooter render];
    }
}

- (void)dealloc{
    FreeObj(_renders);
    FreeObj(_pageHeader);
    FreeObj(_pageFooter);
    [super dealloc];
}

@end
