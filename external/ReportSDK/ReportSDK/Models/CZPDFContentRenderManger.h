//
//  CZPDFContentRenderManger.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZContentRenderManager.h"

@interface CZPDFContentRenderManger : CZContentRenderManager

- (BOOL)generatePDF:(NSString *)filePath pageRange:(NSRange)aRange;

@end
