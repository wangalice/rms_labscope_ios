//
//  RTFFontColorRegistTable.m
//  ReportSDK
//
//  Created by Johnny on 4/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFFontColorRegistTable.h"
#import <UIKit/UIKit.h>

static CZRTFFontColorRegistTable *registTable = nil;


@interface CZRTFFontColorRegistTable (){
    NSMutableArray *fontTable;
    NSMutableArray *colorTable;
}

@end


@implementation CZRTFFontColorRegistTable

@synthesize fontColorRTFDes = _fontColorRTFDes;
@synthesize fontTableRTFDes = _fontTableRTFDes;
@synthesize preRenderObj;


#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (registTable == nil)
			registTable = [[CZRTFFontColorRegistTable alloc] init];
	}
    
    return registTable;
}

- (id)init{
   self = [super init];
    if (self) {
        fontTable = [[NSMutableArray alloc]initWithCapacity:5];
        
        [fontTable addObject:[UIFont fontWithName:kDefaultTextFont size:3]];

        colorTable = [[NSMutableArray alloc]initWithCapacity:5];
        _fontTableRTFDes = [[NSMutableString alloc]init];
        _fontColorRTFDes = [[NSMutableString alloc]init];
    }
    return self;
}

- (NSString *)registFont2FontTable:(UIFont *)textFont{
    ///if Just For UT, This code should not be replaced.
    @synchronized(self) {
        NSString *fontIndexStr = nil;
        if (fontTable && [fontTable count]>0) {
            BOOL isRegisted = [fontTable containsObject:textFont];
            if (!isRegisted) {
                [fontTable addObject:textFont];
               }
            NSUInteger fontIndex = [fontTable indexOfObject:textFont];
            fontIndexStr = [NSString stringWithFormat:@"f%lu",(unsigned long)fontIndex];
        }
    return fontIndexStr;
    }
}

- (void)registColorToColorTable:(UIColor *)color {
    BOOL isRegisted = [colorTable containsObject:color];
    if (!isRegisted) {
        [colorTable addObject:color];
    }
}

- (NSString *)readTextColorFromColorTable:(UIColor *)color {
    NSString *fontColorStr = nil;
    if (color) {
        [self registColorToColorTable:color];
        NSUInteger colorIndex = [colorTable indexOfObject:color];
        fontColorStr = [NSString stringWithFormat:@"cf%lu", (unsigned long)colorIndex + 1];
    } else {
        fontColorStr = @"cf0";
    }
    
    return fontColorStr;
}

- (NSString *)readBackgoundColorFromColorTable:(UIColor *)color {
    NSString *cellBackgroundColorStr = nil;
    if (color) {
        [self registColorToColorTable:color];
        NSUInteger colorIndex = [colorTable indexOfObject:color];
        cellBackgroundColorStr = [NSString stringWithFormat:@"clcbpat%lu",(unsigned long)colorIndex + 1];
    } else {
        cellBackgroundColorStr = @"clcbpat0";
    }

    return cellBackgroundColorStr;
}

- (UIFont *)currentFont{
    UIFont *curFont = nil;
    if (fontTable && [fontTable count]>0) {
        curFont = [fontTable objectAtIndex:[fontTable count]-1];
    }
    if (!curFont) {
        curFont = [UIFont fontWithName:kDefaultTextFont size:kDefaultTextFontSize];
    }
    return curFont;
}

- (CTFontRef)FontRefWithUIFont:(UIFont *)font{
    if (!font)
        font = [UIFont fontWithName:kDefaultTextFont size:kDefaultTextFontSize];
     NSString *fontName = [font fontName];
     return CTFontCreateWithName((CFStringRef)fontName, [font pointSize], NULL);
}

- (NSString *)fontNameFromFontRef:(CTFontRef)aRef{
    NSDictionary *attributes;
    CTFontDescriptorRef fontDescriptor = CTFontCopyFontDescriptor(aRef);
    attributes = (NSDictionary *)CTFontDescriptorCopyAttributes(fontDescriptor);
    CFRelease(fontDescriptor);
    NSString *fontName = [attributes objectForKey:(id)kCTFontNameAttribute];
    CFRelease(attributes);

    return fontName;

}

- (void)writeFontTableEntryWithIndex:(NSUInteger)fontIndex name:(NSString *)name{
    NSString *indexStr = [NSString stringWithFormat:@"%lu",(unsigned long)fontIndex];
    [_fontTableRTFDes appendString:@"\\f"];
    [_fontTableRTFDes appendString:indexStr];
    [_fontTableRTFDes appendString:@"\\fnil\\fcharset0 "];
    [_fontTableRTFDes appendString:name];
    [_fontTableRTFDes appendString:@";"];
    
    CZLog(@"the font entry is %@",_fontTableRTFDes);
}

- (void)writColorTableEntryWithColor:(UIColor *)fontColor {
    CGColorRef color = [fontColor CGColor];
    int32_t red = 0, green = 0, blue = 0;
    
    assert(CFGetTypeID(color) == CGColorGetTypeID());
    
    CGColorRef cgColor = (CGColorRef)color;
    CGColorSpaceRef colorSpace = CGColorGetColorSpace(cgColor);
    const CGFloat *components = CGColorGetComponents(cgColor);
    switch (CGColorSpaceGetModel(colorSpace)) {
        case kCGColorSpaceModelMonochrome: {
            assert(CGColorSpaceGetNumberOfComponents(colorSpace) == 1);
            assert(CGColorGetNumberOfComponents(cgColor) == 2);
            red = green = blue = (int32_t)round(components[0] * 255.0f);
            break;
        }
        case kCGColorSpaceModelRGB: {
            assert(CGColorSpaceGetNumberOfComponents(colorSpace) == 3);
            assert(CGColorGetNumberOfComponents(cgColor) == 4);
            red = (int32_t)round(components[0] * 255.0f);
            green = (int32_t)round(components[1] * 255.0);
            blue = (int32_t)round(components[2] * 255.0);
            break;
        }
        default: {
            CZLog(@"color = %@ %@", color, cgColor);
            CZLog(@"colorSpace %@", colorSpace);
            break;
        }
    }
    
    [_fontColorRTFDes appendFormat:@"\\red%d\\green%d\\blue%d;", red, green, blue];
}

/** sample of color table definition
 {\colortbl;\red0\green0\blue0;\red255\green0\blue0;}
**/

- (NSString *)fontColorRTFDes{
    NSRange stringRange = NSMakeRange(0, [_fontColorRTFDes length]);
    [_fontColorRTFDes deleteCharactersInRange:stringRange];
    [_fontColorRTFDes appendString:@"{\\colortbl;"];
    for (NSUInteger i = 0; i < [colorTable count]; i++) {
        UIColor *fontColor = [colorTable objectAtIndex:i];
        [self writColorTableEntryWithColor:fontColor];
    }
    [_fontColorRTFDes appendString:@"}\n"];
    CZLog(@"the color entry is %@",_fontColorRTFDes);

    return _fontColorRTFDes;
}

- (NSString *)fontTableRTFDes{
    NSRange stringRange = NSMakeRange(0, [_fontTableRTFDes length]);
    [_fontTableRTFDes deleteCharactersInRange:stringRange];
    [_fontTableRTFDes appendString:@"{\\fonttbl"];
    for (NSUInteger i =0; i< [fontTable count]; i++) {
        UIFont *fontObj = [fontTable objectAtIndex:i];
        CTFontRef fontRef= [self FontRefWithUIFont:fontObj];
        NSString *fontName = [self fontNameFromFontRef:fontRef];
        CFRelease(fontRef);
        if ([fontName isEqualToString:kDefaultTextFont]) {
            fontName = kDefaultTextFontForWindows;
        }
        [self writeFontTableEntryWithIndex:i name:fontName];
    }
     [_fontTableRTFDes appendString:@"}\n"];
    CZLog(@"the font entry is %@",_fontTableRTFDes);

    return _fontTableRTFDes;
}

- (void)clearFonts{
    [fontTable removeAllObjects];
    [fontTable addObject:[UIFont fontWithName:kDefaultTextFont size:3]];
}


- (void)dealloc{
    FreeObj(fontTable);
    FreeObj(colorTable);
    FreeObj(_fontColorRTFDes);
    FreeObj(_fontTableRTFDes);
    FreeObj(preRenderObj);
    [super dealloc];
}
@end
