//
//  RTFFontColorRegistTable.h
//  ReportSDK
//
//  Created by Johnny on 4/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZRenderBase;

#define kSwissFont @"TimesNewRomanPS-BoldItalicMT"
#define kArialFont @"ArialMT"

@interface CZRTFFontColorRegistTable : NSObject{
    NSMutableString       *_fontTableRTFDes;
    NSMutableString       *_fontColorRTFDes;
}
@property (nonatomic, readonly) NSMutableString *fontTableRTFDes;
@property (nonatomic, readonly) NSMutableString *fontColorRTFDes;
@property (nonatomic, retain)   CZRenderBase    *preRenderObj;

/**
 * @brief RTFFontColorRegistTable's shared instance.
 * @return Returns the RTFFontColorRegistTable’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;

/**
 * @brief regist the font to the font table,used in the RTF grammar header. if we have µm² or µm we should use special font: defined it as "fswiss"
 * @param textFont:the text used font,regist it to the table array.
 * @return Returns: return the RTF font index string, like f1 ,f2 ...
 */
- (NSString *)registFont2FontTable:(UIFont *)textFont;

- (void)registColorToColorTable:(UIColor *)color;

/**
 * @brief regist the color to the color table,used in the RTF grammar header.
 * @param textColor:the text used color,regist it to the table array.
 * @return Returns: return the RTF color index string, like fc1 ,fc2 ...
 */
- (NSString *)readTextColorFromColorTable:(UIColor *)color;

/**
 * @brief regist the color to the color table,used in the RTF grammar header.
 * @param backgroundColor:the text background used color,regist it to the table array.
 * @return Returns: return the RTF color index string, like chcbpat1 ,chcbpat2 ...
 */
- (NSString *)readBackgoundColorFromColorTable:(UIColor *)color;

/**
 * @brief return the nearby font been used..
 * @return Returns: return the nearby font.
 */
- (UIFont *)currentFont;

/**
 * @brief clear the fonts in the fontTable,avoid influent the next generation.
 * 
 */
- (void)clearFonts;
@end
