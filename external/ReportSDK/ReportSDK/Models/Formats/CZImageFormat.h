//
//  CZImageFormat.h
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFormatBase.h"

@interface CZImageFormat : CZFormatBase{
    
}

/// init method,inherited from CZFormatBase.
- (id)initWithContent:(NSDictionary *)contentDic;


@end
