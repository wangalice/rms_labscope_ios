//
//  CZPageInfoFormat.m
//  ReportSDK
//
//  Created by Johnny on 8/30/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPageInfoFormat.h"

@implementation CZPageInfoFormat


- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        _isAssigned = YES;
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    
    [super templateValidate];
    BOOL isSuc = [self templateBaseFormat];
    
    return isSuc;
}

- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        canBreakIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kPageInfoElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorPageInfoOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kPageInfoElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorPageInfoOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kPageInfoElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorPageInfoFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kPageInfoElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorPageInfoFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kPageInfoElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorPageInfoTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kPageInfoElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorPageInfoTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kPageInfoElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorPageInfoCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorPageInfoCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kPageInfoElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorPageInfoCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }
    }else{
        isSuc = NO;
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    return isSuc;
}

- (void)dealloc{
    [super dealloc];
}


@end
