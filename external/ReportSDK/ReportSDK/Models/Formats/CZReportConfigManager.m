//
//  CZReportConfigManager.m
//  ReportSDK
//
//  Created by Johnny on 9/2/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportConfigManager.h"

#define kBase72Dpi  72.f

static CZReportConfigManager *configManager = nil;

@implementation CZReportConfigManager

@synthesize imageDpi;
@synthesize imageCompressRate;


#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (configManager == nil)
			configManager = [[CZReportConfigManager alloc] init];
	}
    return configManager;
}

- (id)init{
    self = [super init];
    if (self) {
        self.imageCompressRate = 1.f;
        self.imageDpi = 72.f;
    }
    return self;
}

- (BOOL)imageIsNeedCompress{
    BOOL isNeed = NO;
    if (imageCompressRate >0 && imageCompressRate <1){
        isNeed = YES;
    }
    return isNeed;
}

- (BOOL)isNeedDpiConvert:(UIImage *)originalImage renderRect:(CGRect)aRect{
    BOOL isNeed = NO;
    if (imageDpi>0 && (imageDpi>= (kBase72Dpi + 1) || imageDpi<= (kBase72Dpi -1))) {
        CGSize orignalImageSize = originalImage.size;
        CGSize convertSize = CGSizeMake(ceilf(aRect.size.width * imageDpi/kBase72Dpi),
                                        ceilf(aRect.size.height * imageDpi/kBase72Dpi));
        if (convertSize.width < orignalImageSize.width) {
            isNeed = YES;
        }
    }
    return isNeed;
}

- (BOOL)isNeedConfigImage:(UIImage *)originalImage renderRect:(CGRect)aRect{
    BOOL isNeed = [self isNeedDpiConvert:originalImage renderRect:aRect];
    if (!isNeed) {
        isNeed = [self imageIsNeedCompress];
    }
    return isNeed;
}

/// Note: return image for PDF report using.
- (UIImage *)configImageForReport:(UIImage *)originalImage withRect:(CGRect)aRect{
    UIImage *dpiImg = [self dpiConvertImage:originalImage renderRect:aRect];
    UIImage *convertImg = nil;
    if (dpiImg) {
        convertImg = [self compressImage:dpiImg];
    }else{
        convertImg = [self compressImage:originalImage];
    }
    return convertImg;
}

/// Do some dpi compressing and convert work if needed.
- (UIImage *)dpiConvertImage:(UIImage *)originalImage renderRect:(CGRect)aRect{
    if (imageDpi>0 && (imageDpi>= (kBase72Dpi + 1) || imageDpi<= (kBase72Dpi -1))) {
        UIImage *dipConvertImg = nil;
        CGSize orignalImageSize = originalImage.size;
        CGSize convertSize = CGSizeMake(ceilf(aRect.size.width * imageDpi/kBase72Dpi),
                                        ceilf(aRect.size.height * imageDpi/kBase72Dpi));
        if (convertSize.width < orignalImageSize.width) {
            UIGraphicsBeginImageContext(convertSize);
            [originalImage drawInRect:CGRectMake(0, 0, convertSize.width, convertSize.height)];
            dipConvertImg = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return dipConvertImg;
        }else{
            return originalImage;
        }
    }else{
        return originalImage;
    }
}

/// Compress image with compress reate parameter.
- (UIImage *)compressImage:(UIImage *)originalImage{
    NSData *imageData = nil;
    UIImage *convertedImg = nil;
    if (imageCompressRate >0 && imageCompressRate <1) {
        imageData = UIImageJPEGRepresentation(originalImage, imageCompressRate);
        convertedImg = [UIImage imageWithData:imageData];
    }else{
        convertedImg = originalImage;
    }
    return convertedImg;
}


+ (void)freeInstance{
    if (configManager) {
        FreeObj(configManager);
    }
}

@end
