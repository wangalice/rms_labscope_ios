//
//  CZColumnFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZColumnFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"


@implementation CZColumnFormat

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        if (self.valueDic) {
            self.key = [self.valueDic valueForKey:kColumnComponentArray];
            _isAssigned = [CZDictionaryTool isHaveAssigned:self.key];
            if (!_isAssigned) {
                self.key = [CZDictionaryTool extractKey:self.key];
            }
        }
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    
    [super templateValidate];
    BOOL isSuc =[self templateBaseFormat];
    BOOL isTitleSuc = [self titleFormatValidate];
    if (isSuc) {
        isSuc = isTitleSuc;
    }
    return isSuc;
}


- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL valueIsRight = [self valueIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    BOOL requiredIsRight = [self requiredIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        valueIsRight && canBreakIsRight && requiredIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!valueIsRight) {
            [self valueValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
        if (!requiredIsRight) {
            [self requiredValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
         aError.errorCode = CZReportGeneratorErrorColumnOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
         aError.errorCode = CZReportGeneratorErrorColumnFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorColumnTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)valueValidate{
    if ([self valueIsExist]) {
        if (![self valueIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnValueError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnValueError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnValueKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorColumnValueKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorColumnCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)requiredValidate{
    if ([self requiredIsExist]) {
        if (![self requiredIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnRequiredError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnRequiredError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnRequiredKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorColumnRequiredKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}



#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }
    }else{
        isSuc = NO;
    }
    [self titleDataValueValidate:data];
    return isSuc;
}

- (BOOL)assignValue:(NSDictionary *)dataDic{
    BOOL isSuc = YES;
    if (_isAssigned) {
        return isSuc;
    }
    id value = [dataDic valueForKey:self.key];
    if (value == nil) {
        if ([self valueIsRequired]){
            
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorColumnValueNotExist;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            
            isSuc = NO;
        }
        _canRender = NO;

    }else{
        [self.valueDic setObject:value forKey:kColumnComponentArray];
        [self.elementDic setObject:self.valueDic forKey:kValue];
        _isAssigned = YES;
        CZLog(@"the assingned column element is %@",self.elementDic);
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    
    isSuc = NO;
    
    id obj = [self.valueDic valueForKey:kColumnComponentArray];
    BOOL valueExist = obj != nil;
    if ([obj isKindOfClass:[NSArray class]]) {  // version 0
        isSuc = [self isSingleValueValid:obj];
    } else if ([obj isKindOfClass:[NSDictionary class]]) {
        id version = obj[kVersion];
        if ([@1 isEqual:version]) {  // version 1
            id valuesArray  = obj[kValues];
            if ([valuesArray isKindOfClass:[NSArray class]] &&
                [valuesArray count] > 0) {
                BOOL allValid = YES;
                for (id dict in valuesArray) {
                    BOOL singleDataValid = NO;
                    
                    if ([dict isKindOfClass:[NSDictionary class]]) {
                        id value = [dict objectForKey:kValue];
                        if ([self isSingleValueValid:value]) {
                            singleDataValid = YES;
                        }
                    }
                    
                    if (!singleDataValid) {
                        allValid = NO;
                        break;
                    }
                }
                
                isSuc = allValid;
            }
        }
    }
    
    if (!isSuc) {
        if ([self valueIsRequired]){
            if (valueExist) {
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnValueTypeNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kColumnElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorColumnValueTypeNotMatch;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }else{
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorColumnValueNotExist];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kColumnElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorColumnValueNotExist;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }
            
            isSuc = NO;
        } else {
            isSuc = YES;
        }
        _canRender = NO;
    }

    return isSuc;
}

- (BOOL)isSingleValueValid:(id)value {
    BOOL isSuc = NO;
    
    NSString *type = [NSString stringWithFormat:@"%@",kArrayType];
    if ([value isKindOfClass:[self convertType2Class:type]]) {
        assert([value isKindOfClass:[NSArray class]]);
        NSError *error = nil;
        isSuc = [self cheackArraySubElementIsDic:value formatError:&error];
        if (!isSuc) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:error.code];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = error.code;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }
    return isSuc;
}

#pragma mark -
#pragma mark title validate
/**
 * Title is optional ,its value can be hard code in title dictionary, or be setted by parameter like $**$.
 * This is just implemented in ImageElement for bakeup.
 * If title need validate, firstly, we should call titleFormatValidate, then call
 * titleDataValueValidate.
 * Currently, title is optional ,it is not be validated,but can be assigned.
 */
- (BOOL)titleFormatValidate{
    BOOL isSuc = YES;
    id titleDic = [self.elementDic valueForKey:kTitle];
    if (!titleDic) {
        return  isSuc;
    }
    if (titleDic && [titleDic isKindOfClass:[NSDictionary class]]) {
        id titleTextObj = [titleDic valueForKey:kTitleText];
        if (!(titleTextObj && [titleTextObj isKindOfClass:[NSString class]])) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleTextDefinedError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleTextDefinedError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleNotDictionary];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kColumnElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTemplateElementTitleNotDictionary;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        isSuc = NO;
    }
    
    return isSuc;
}


- (BOOL)titleDataValueValidate:(NSDictionary *)aData{
    BOOL isSuc = NO;
    id titleDic = [self.elementDic valueForKey:kTitle];
    if (!titleDic) {
        return  isSuc = YES;
    }
    if (![titleDic isKindOfClass:[NSDictionary class]]) {
        return isSuc;
    }
    id titleTextObj = [titleDic valueForKey:kTitleText];
    if (titleTextObj && [titleTextObj isKindOfClass:[NSString class]]){
        
        NSString *titleKey = nil;
        BOOL isTitleAssigned = [CZDictionaryTool isHaveAssigned:titleTextObj];
        if (!isTitleAssigned) {
            titleKey = [CZDictionaryTool extractKey:titleTextObj];
            isSuc = [self assignTitleValue:aData withKey:titleKey];
        }else{
            isSuc = YES;
        }
    }
    return isSuc;
}

- (BOOL)assignTitleValue:(NSDictionary *)aData withKey:(NSString *)titleKey{
    BOOL isSuc = YES;
    if (titleKey && aData) {
        id value = [aData valueForKey:titleKey];
        if (value == nil) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueNotExist;
            //[_errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }else if (![value isKindOfClass:[NSString class]]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kColumnElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueTypeError;
            //[_errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }else{
            NSMutableDictionary *titleDic =[NSMutableDictionary dictionaryWithDictionary: [self.elementDic valueForKey:kTitle]];
            NSDictionary *formatDic = [titleDic valueForKey:kTitleFormat];
            
            NSUInteger maxLimitaiton = 1000000000;
            NSUInteger minLimitation = 1;
            NSUInteger textLength = [(NSString*)value length];
            
            if (formatDic && [formatDic isKindOfClass:[NSDictionary class]]) {
                NSNumber *maxLength = [formatDic valueForKey:kMaxLength];
                NSNumber *minLength = [formatDic valueForKey:kMinLength];
                
                if (maxLength && [maxLength isKindOfClass:[NSNumber class]]) {
                    maxLimitaiton = [maxLength unsignedIntegerValue];
                }else{
                    maxLimitaiton = 1000000000;
                }
                if (minLength && [minLength isKindOfClass:[NSNumber class]]) {
                    minLimitation = [minLength unsignedIntegerValue];
                }else{
                    minLimitation = 1;
                }
            }
            if (textLength <= maxLimitaiton && textLength >= minLimitation) {
                isSuc = YES;
                [titleDic setValue:value forKey:kTitleText];
                [self.elementDic setValue:titleDic forKey:kTitle];
            }else{
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleLengthNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kColumnElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTemplateElementTitleLengthNotMatch;
                //[_errorObjArray addObject:aError];
                FreeObj(aError);
                isSuc = NO;
            }
        }
    }
    if (!isSuc) {
        [self.elementDic setValue:nil forKey:kTitle];
    }
    return isSuc;
}

#pragma mark - override CZFormatBase

- (NSString *)elementType {
    return kColumnElement;
}

- (void)dealloc{
    [super dealloc];
}

@end
