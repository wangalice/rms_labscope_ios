//
//  CZFormatBase.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFormatBase.h"
#import "CZDictionaryTool.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZLineFormat.h"
#import "CZPieFormat.h"
#import "CZColumnFormat.h"
#import "CZTableFormat.h"
#import "CZTextFormat.h"
#import "CZPageInfoFormat.h"
#define kStringType         @"NSString"
#define kArrayType          @"NSArray"
#define kDictionaryType     @"NSDcitonary"
#define kNumberType         @"NSNumber"

@interface CZFormatBase ()

@property (nonatomic, retain, readwrite) NSMutableDictionary *elementDic;
@property (nonatomic, retain, readwrite) NSString *key;

@end

@implementation CZFormatBase
@synthesize isAssigned = _isAssigned;
@synthesize isValueRequired = _isValueRequired;
@synthesize key = _key;
@synthesize elementDic = _elementDic;
@synthesize valueDic = _valueDic;
@synthesize canRender = _canRender;
@synthesize errorObjArray = _errorObjArray;
@synthesize subDomain = _subDomain;
@synthesize sequenceOrder = _sequenceOrder;

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super init];
    if (self) {
        _errorObjArray = [[NSMutableArray alloc]initWithCapacity:5];
        _elementDic = [contentDic mutableDeepCopy];
        NSDictionary *value = [_elementDic valueForKey:kValue];
        if ([value isKindOfClass:[NSDictionary class]]) {
            _valueDic = [value mutableDeepCopy];
        }
        _isAssigned = NO;
        _isValueRequired = YES;
        _canRender  = YES;
    }
    
    return self;
}

#pragma mark -
#pragma mark data validate

- (Class)convertType2Class:(NSString *)type{
    if ([type isEqualToString:kStringType]) {
        return [NSString class];
    }else if ([type isEqualToString:kArrayType]){
        return [NSArray class];
    }else if ([type isEqualToString:kDictionaryType]){
        return [NSDictionary class];
    }else if ([type isEqualToString:kNumberType]){
        return [NSNumber class];
    }
    return nil;
}


- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL  validateIsSuc = YES;
    [_errorObjArray removeAllObjects];
    return validateIsSuc;
}

- (BOOL)assignValue:(NSDictionary *)dataDic{
    assert([dataDic isKindOfClass:[NSDictionary class]]);
    BOOL isSuc = YES;
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = YES;
    return isSuc;
}

- (BOOL)valueIsRequired{
    BOOL isRequired = YES;
    id boolenValue = [_elementDic valueForKey:kReuiredKey];
    if (boolenValue) {
        isRequired = [boolenValue boolValue];
    }
    return isRequired;
}

- (BOOL)cheackArraySubElementIsDic:(NSArray *)dicArray formatError:(NSError **)error{
    BOOL isSuc = YES;
    if (dicArray && [dicArray count]>0) {
        for (id aObj in dicArray) {
            if (![aObj isKindOfClass:[NSDictionary class]]) {
                NSError* curError = [self dataArraySubElementTypeError];
                if (error && curError) {
                    *error = curError;
                }
                isSuc = NO;
                if (![self valueIsRequired]) {
                    isSuc = YES;
                    _canRender = NO;
                }
                return isSuc;
            }
        }
        /// new added for the pie or column just one item,but the largestSegmentShow is NO.
        if ([dicArray count] == 1 && _canRender) {
            if (_valueDic) {
                NSNumber *booleanObj =  [_valueDic valueForKey:kIsLargestSegmentShow];
                if (booleanObj) {
                    BOOL isShow = [booleanObj boolValue];
                    if (!isShow && ([self isKindOfClass:[CZPieFormat class]] || [self isKindOfClass:[CZColumnFormat class]])) {
                        _canRender = NO;
                    }
                }
            }
        }
    }else{
        NSError* curError = [self dataArrayNoContent];
        if (error && curError) {
            *error = curError;
        }
        isSuc = NO;
        if (![self valueIsRequired]) {
            isSuc = YES;
            _canRender = NO;
        }
        return isSuc;
    }
    return isSuc;
}

- (NSError *)dataArraySubElementTypeError{
    if ([self isKindOfClass:[CZTableFormat class]]) {
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTableValueArraySubelementTypeNotMatch];
        return curError;
    }else if ([self isKindOfClass:[CZPieFormat class]]){
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorPieValueArraySubelementTypeNotMatch];
        return curError;
        
    }else if ([self isKindOfClass:[CZColumnFormat class]]){
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorColumnValueArraySubelementTypeNotMatch];
        return curError;
        
    }
    return nil;
}

- (NSError *)dataArrayNoContent{
    if ([self isKindOfClass:[CZTableFormat class]]) {
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTableValueArrayNoContent];
        return curError;
        
    }else if ([self isKindOfClass:[CZPieFormat class]]){
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorPieValueArrayNoContent];
        return curError;
        
    }else if ([self isKindOfClass:[CZColumnFormat class]]){
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorColumnValueArrayNoContent];
        return curError;
        
    }
    return nil;
}



#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    BOOL  validateIsSuc = YES;
    [_errorObjArray removeAllObjects];
    return validateIsSuc;
}

- (BOOL)templateBaseFormat{
    BOOL isSuc = YES;
    return isSuc;
}

- (BOOL)subKeysFormat:(NSError **)aError{
    BOOL isSuc = YES;
    id frameObj = [_elementDic valueForKey:kFrame];
    id orderObj = [_elementDic valueForKey:kRenderOrder];
    id typeObj = [_elementDic valueForKey:kElementType];
    id valueObj = [_elementDic valueForKey:kValue];
    id canbreakObj = [_elementDic valueForKey:kCanBreak];
    id requiredObj = [_elementDic valueForKey:kReuiredKey];
    
    /// sepcial for the line element
    if ([self isKindOfClass:[CZLineFormat class]] || [self isKindOfClass:[CZPageInfoFormat class]]) {
        valueObj = [NSNumber numberWithInt:1];
        requiredObj = [NSNumber numberWithInt:1];
    }
    if (frameObj && orderObj && typeObj && valueObj && canbreakObj && requiredObj) {
        isSuc = YES;
    }else{
        isSuc = NO;
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTemplateKeyMissingError];
        if (aError) {
            *aError = curError;
        }
    }
    return isSuc;
}




- (BOOL)frameIsExist{
    BOOL isRight = YES;
    NSDictionary *frameDic = [_elementDic valueForKey:kFrame];
    if (!frameDic) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)frameIsRight{
    BOOL isRight = NO;
    NSDictionary *frameDic = [_elementDic valueForKey:kFrame];
    if (!frameDic || ![frameDic isKindOfClass:[NSDictionary class]]) {
        isRight = NO;
        return isRight;
    }
    NSArray *keyArray = [frameDic allKeys];
    if ([keyArray count] ==4) {
        id xValue = [frameDic valueForKey:@"x"];
        id yValue = [frameDic valueForKey:@"y"];
        id wideValue = [frameDic valueForKey:@"width"];
        id heightValue = [frameDic valueForKey:@"height"];
        if (xValue && yValue && wideValue && heightValue) {
            if ([xValue isKindOfClass:[NSNumber class]] &&
                [yValue isKindOfClass:[NSNumber class]] &&
                [wideValue isKindOfClass:[NSNumber class]] &&
                [heightValue isKindOfClass:[NSNumber class]]) {
                isRight = YES;
                
                if((strcmp([xValue objCType], @encode(char))) == 0){
                    isRight = NO;
                    return isRight;
                }
                if((strcmp([yValue objCType], @encode(char))) == 0){
                    isRight = NO;
                    return isRight;
                }
                if((strcmp([wideValue objCType], @encode(char))) == 0){
                    isRight = NO;
                    return isRight;
                }
                if((strcmp([heightValue objCType], @encode(char))) == 0){
                    isRight = NO;
                    return isRight;
                }
            }
        }
    }
    if (isRight) {
        CGRect frame = [CZDictionaryTool dictionay2Frame:frameDic];
        if (frame.size.height <0 || frame.size.width<0) {
            isRight = NO;
            return isRight;
        }
    }
    return isRight;
}

- (BOOL)OrderIsExist{
    BOOL isRight = YES;
    id order = [_elementDic valueForKey:kRenderOrder];
    if (!order) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)OrderIsRight{
    BOOL isRight = YES;
    id order = [_elementDic valueForKey:kRenderOrder];
    if (!order) {
        isRight = NO;
        return isRight;
    }
    if (![order isKindOfClass:[NSNumber class]]) {
        isRight = NO;
        return isRight;
    }
    if((strcmp([order objCType], @encode(int))) != 0
       && strcmp([order objCType], @encode(long long)) != 0) {
        isRight = NO;
        return isRight;
    }
    
    if ([order integerValue]<0) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)elementTypeIsExist{
    BOOL isRight = YES;
    id typeObj = [_elementDic valueForKey:kElementType];
    if (!typeObj) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)elementTypeIsRight{
    BOOL isRight = YES;
    id typeObj = [_elementDic valueForKey:kElementType];
    if (!typeObj) {
        isRight = NO;
        return isRight;
    }
    if (![typeObj isKindOfClass:[NSString class]]) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)valueIsExist{
    BOOL isRight = YES;
    id typeObj = [_elementDic valueForKey:kValue];
    if (!typeObj && ![self isKindOfClass:[CZLineFormat class]]
        && ![self isKindOfClass:[CZPageInfoFormat class]]) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)valueIsRight{
    BOOL isRight = NO;
    id valueObj = [_elementDic valueForKey:kValue];
    if (valueObj && [valueObj isKindOfClass:[NSDictionary class]]) {
        isRight = YES;
    }
    if ([self isKindOfClass:[CZLineFormat class]]|| [self isKindOfClass:[CZPageInfoFormat class]]) {
        isRight = YES;
    }
    return isRight;
}

- (BOOL)canbreakIsExist{
    BOOL isRight = YES;
    id typeObj = [_elementDic valueForKey:kCanBreak];
    if (!typeObj) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)canbreakIsRight{
    BOOL isRight = NO;
    if (![self isKindOfClass:[CZTableFormat class]]
        && ![self isKindOfClass:[CZTextFormat class]]
        ) {
        [_elementDic setValue:[NSNumber numberWithBool:NO] forKey:kCanBreak];
        isRight = YES;
        return isRight;
    }
    id canbreakObj = [_elementDic valueForKey:kCanBreak];

    if (canbreakObj && [canbreakObj isKindOfClass:[NSNumber class]]) {
        isRight = YES;
        if((strcmp([canbreakObj objCType], @encode(char))) != 0) {
            isRight = NO;
            return isRight;
        }
    }
    return isRight;
}

- (BOOL)requiredIsExist{
    BOOL isRight = YES;
    id typeObj = [_elementDic valueForKey:kReuiredKey];
    if (!typeObj && ![self isKindOfClass:[CZLineFormat class]]
        && ![self isKindOfClass:[CZPageInfoFormat class]]) {
        isRight = NO;
        return isRight;
    }
    return isRight;
}

- (BOOL)requiredIsRight{
    BOOL isRight = NO;
    id requiredObj = [_elementDic valueForKey:kReuiredKey];
    if (requiredObj && [requiredObj isKindOfClass:[NSNumber class]]) {
        isRight = YES;
        if((strcmp([requiredObj objCType], @encode(char))) != 0) {
            isRight = NO;
            return isRight;
        }
    }
    return isRight;
}



- (void)setRelativeFrameDic:(NSDictionary *)frameDic{
    if (_elementDic) {
        [_elementDic setValue:frameDic forKey:kFrame];
    }
}

- (void)printContent{
    CZLog(@"the class is %@ content is %@",[self debugDescription],self.elementDic);
}

- (NSString *)elementType {
    return nil;
}

- (void)dealloc{
    FreeObj(_errorObjArray);
    FreeObj(_elementDic);
    FreeObj(_valueDic);
    FreeObj(_key);
    _keyTypeDes = nil;
    [super dealloc];
}
@end


@interface CZTitleValidator ()

@property (nonatomic, retain) CZFormatBase *format;

@end

@implementation CZTitleValidator

- (id)initWithFormat:(CZFormatBase *)format primaryKey:(NSString *)primaryKey valueKey:(NSString *)valueKey {
    self = [super init];
    if (self) {
        _format = [format retain];
        _primaryKey = [primaryKey copy];
        _valueKey = [valueKey copy];
    }
    
    return self;
}

- (void)dealloc {
    [_format release];
    [_primaryKey release];
    [_valueKey release];
    [super dealloc];
}

/**
 * Title is optional ,its value can be hard code in title dictionary, or be setted by parameter like $**$.
 * This is just implemented in ImageElement for bakeup.
 * If title need validate, firstly, we should call titleFormatValidate, then call
 * titleDataValueValidate.
 * Currently, title is optional ,it is not be validated,but can be assigned.
 */

- (BOOL)formatValidate {
    BOOL isSuc = YES;
    id titleDic = [self.format.elementDic valueForKey:self.primaryKey];
    if (!titleDic) {
        return isSuc;
    }
    
    if (titleDic && [titleDic isKindOfClass:[NSDictionary class]]) {
        id titleTextObj = [titleDic valueForKey:self.valueKey];
        if (!(titleTextObj && [titleTextObj isKindOfClass:[NSString class]])) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleTextDefinedError];
            CZError *aError = [[CZError alloc] initWithDomain:self.format.subDomain
                                                        order:self.format.sequenceOrder
                                             errorElementType:[self.format elementType]];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleTextDefinedError;
            [self.format.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }
    } else {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleNotDictionary];
        CZError *aError = [[CZError alloc] initWithDomain:self.format.subDomain
                                                    order:self.format.sequenceOrder
                                         errorElementType:[self.format elementType]];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTemplateElementTitleNotDictionary;
        [self.format.errorObjArray addObject:aError];
        FreeObj(aError);
        isSuc = NO;
    }
    
    return isSuc;
}

- (BOOL)dataValueValidate:(NSDictionary *)aData {
    BOOL isSuc = NO;
    id titleDic = [self.format.elementDic valueForKey:self.primaryKey];
    
    if (!titleDic) {
        return  isSuc = YES;
    }
    
    if (![titleDic isKindOfClass:[NSDictionary class]]) {
        return isSuc;
    }
    
    id titleTextObj = [titleDic valueForKey:self.valueKey];
    if (titleTextObj && [titleTextObj isKindOfClass:[NSString class]]) {
        NSString *titleKey = nil;
        BOOL isTitleAssigned = [CZDictionaryTool isHaveAssigned:titleTextObj];
        if (!isTitleAssigned) {
            titleKey = [CZDictionaryTool extractKey:titleTextObj];
            isSuc = [self assignValue:aData withKey:titleKey];
        } else {
            isSuc = YES;
        }
    }
    return isSuc;
}

- (BOOL)assignValue:(NSDictionary *)aData withKey:(NSString *)titleKey {
    BOOL isSuc = YES;
    if (titleKey && aData) {
        id value = [aData valueForKey:titleKey];
        if (value == nil) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:self.format.subDomain
                                                        order:self.format.sequenceOrder
                                             errorElementType:[self.format elementType]];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueNotExist;
            //[self.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        } else if (![value isKindOfClass:[NSString class]]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:self.format.subDomain
                                                        order:self.format.sequenceOrder
                                             errorElementType:[self.format elementType]];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueTypeError;
            //[self.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        } else {
            NSMutableDictionary *titleDic =[NSMutableDictionary dictionaryWithDictionary: [self.format.elementDic valueForKey:self.primaryKey]];
            NSDictionary *formatDic = [titleDic valueForKey:kTitleFormat];
            
            NSUInteger maxLimitaiton = 1000000000;
            NSUInteger minLimitation = 1;
            NSUInteger textLength = [(NSString*)value length];
            
            if (formatDic && [formatDic isKindOfClass:[NSDictionary class]]) {
                NSNumber *maxLength = [formatDic valueForKey:kMaxLength];
                NSNumber *minLength = [formatDic valueForKey:kMinLength];
                
                if (maxLength && [maxLength isKindOfClass:[NSNumber class]]) {
                    maxLimitaiton = [maxLength unsignedIntegerValue];
                }else{
                    maxLimitaiton = 1000000000;
                }
                if (minLength && [minLength isKindOfClass:[NSNumber class]]) {
                    minLimitation = [minLength unsignedIntegerValue];
                }else{
                    minLimitation = 1;
                }
            }
            if (textLength <= maxLimitaiton && textLength >= minLimitation) {
                isSuc = YES;
                [titleDic setValue:value forKey:self.valueKey];
                [self.format.elementDic setValue:titleDic forKey:self.primaryKey];
            } else {
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleLengthNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:self.format.subDomain
                                                            order:self.format.sequenceOrder
                                                 errorElementType:[self.format elementType]];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTemplateElementTitleLengthNotMatch;
                FreeObj(aError);
                isSuc = NO;
            }
        }
    }
    
    if (!isSuc) {
        [self.format.elementDic setValue:nil forKey:kTitle];
    }
    return isSuc;
}


@end
