//
//  CZFormatBase+InSubclassEye.h
//  ReportSDK
//
//  Created by Ralph Jin on 1/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZFormatBase.h"

@interface CZFormatBase (InSubclassEye)

@property (nonatomic, retain, readwrite) NSMutableDictionary *elementDic;
@property (nonatomic, retain, readwrite) NSString *key;

@end
