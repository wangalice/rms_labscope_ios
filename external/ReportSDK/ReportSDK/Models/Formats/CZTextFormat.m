//
//  CZTextFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTextFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"

#define kSpace @" "

@implementation CZTextFormat

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        if (self.valueDic) {
            self.key = [self.valueDic valueForKey:kTextValue];
            if (!self.key || [self.key length]<1) {
                if (![self isValueRequired]) {
                    self.key = kSpace;
                    [self.valueDic setObject:self.key forKey:kTextValue];
                }
            }
            self.isAssigned = [CZDictionaryTool isHaveAssigned:self.key];
            if (!self.isAssigned) {
                self.key = [CZDictionaryTool extractKey:self.key];
            }
        }
    }
    return self;
}

#pragma mark -
#pragma mark template validate


- (BOOL)templateValidate{
    [super templateValidate];
     BOOL isSuc = [self templateBaseFormat];
    return isSuc;
}

- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL valueIsRight = [self valueIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    BOOL requiredIsRight = [self requiredIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        valueIsRight && canBreakIsRight && requiredIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
           [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!valueIsRight) {
            [self valueValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
        if (!requiredIsRight) {
            [self requiredValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)valueValidate{
    if ([self valueIsExist]) {
        if (![self valueIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextValueError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextValueKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)requiredValidate{
    if ([self requiredIsExist]) {
        if (![self requiredIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextRequiredError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextRequiredError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextRequiredKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTextElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTextRequiredKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}



#pragma mark -
#pragma mark data validate


- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }
    }else{
        isSuc = NO;
    }
    return isSuc;
}

- (BOOL)assignValue:(NSDictionary *)dataDic{
    BOOL isSuc = YES;
    if (_isAssigned) {
        return isSuc;
    }
    id value = [dataDic valueForKey:self.key];
    if (value == nil) {
        if ([self valueIsRequired]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTextElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTextValueNotExist;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }else{
            /// add for track bug 261,if no data,should show space word.
            [self.valueDic setObject:@" " forKey:kTextValue];
            [self.elementDic setObject:self.valueDic forKey:kValue];
            _isAssigned = YES;
        }
       // _canRender = NO;

    }else{
        [self.valueDic setObject:value forKey:kTextValue];
        [self.elementDic setObject:self.valueDic forKey:kValue];
        _isAssigned = YES;
        CZLog(@"the assingned text element is %@", self.elementDic);
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    id value = [self.valueDic valueForKey:kTextValue];

    NSDictionary *formatDic = [self.valueDic valueForKey:kValueFormat];

    NSString *type = [NSString stringWithFormat:@"%@",kStringType];
    if ([value isKindOfClass:[self convertType2Class:type]]) {
        isSuc = YES;
        NSUInteger maxLimitaiton = 1000000000;
        NSUInteger minLimitation = 1;
        NSInteger textLength = [(NSString*)value length];
        
        if (formatDic && [formatDic isKindOfClass:[NSDictionary class]]) {
            NSNumber *maxLength = [formatDic valueForKey:kMaxLength];
            NSNumber *minLength = [formatDic valueForKey:kMinLength];
            
            if (maxLength && [maxLength isKindOfClass:[NSNumber class]]) {
                maxLimitaiton = [maxLength unsignedIntegerValue];
            }else{
                maxLimitaiton = 1000000000;
            }
            if (minLength && [minLength isKindOfClass:[NSNumber class]]) {
                minLimitation = [minLength unsignedIntegerValue];
            }else{
                minLimitation = 1;
            }
        }
        
        if (textLength <= maxLimitaiton && textLength >= minLimitation) {
            isSuc = YES;
        }else{
            if ([self valueIsRequired]){
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextLengthNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kTextElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTextLengthNotMatch;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
                
                isSuc = NO;
            }
            _canRender = NO;
        }
    }else{
        if ([self valueIsRequired]) {
            if (value == nil) {
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueNotExist];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kTextElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                 aError.errorCode = CZReportGeneratorErrorTextValueNotExist;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }else{
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueTypeNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kTextElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTextValueTypeNotMatch;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }
            isSuc = NO;
        }
        _canRender = NO;
    }
    return isSuc;
}

#pragma mark - override CZFormatBase

- (NSString *)elementType {
    return kTextElement;
}

- (void)dealloc{
    [super dealloc];
}

@end
