//
//  CZReportConfigManager.h
//  ReportSDK
//
//  Created by Johnny on 9/2/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZReportConfigManager : NSObject

@property (nonatomic, assign) CGFloat imageCompressRate;
@property (nonatomic, assign) CGFloat imageDpi;


+ (CZReportConfigManager*)sharedInstance;

/// used for free the single instance
+ (void)freeInstance;

- (UIImage *)configImageForReport:(UIImage *)originalImage withRect:(CGRect)aRect;

- (BOOL)imageIsNeedCompress;

- (BOOL)isNeedDpiConvert:(UIImage *)originalImage renderRect:(CGRect)aRect;

- (BOOL)isNeedConfigImage:(UIImage *)originalImage renderRect:(CGRect)aRect;

@end
