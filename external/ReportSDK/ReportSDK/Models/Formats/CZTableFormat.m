//
//  CZTableFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZDictionaryTool.h"
#import "CZTableFilter.h"

#define kTableBreakMarginValue      0.75
#define kHermesText                 @"Hermes"

@implementation CZTableFormat

@synthesize filterTable;
@synthesize isHasFilter;

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        if (self.valueDic) {
            self.key = [self.valueDic valueForKey:kTableValue];
            _isAssigned = [CZDictionaryTool isHaveAssigned:self.key];
            if (!_isAssigned) {
                self.key = [CZDictionaryTool extractKey:self.key];
            }
        }
        isHasFilter = NO;
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    [super templateValidate];
    
    BOOL isSuc = [self templateBaseFormat];

    BOOL subKeyFormat = [self subSpecialKeysFormat];
    if (!subKeyFormat) {
        isSuc = NO;
    }
    BOOL isTitleSuc = [self titleFormatValidate];
    if (isSuc) {
        isSuc = isTitleSuc;
    }
    BOOL isCaptionSuc = [self captionFormatValidate];
    if (isSuc) {
        isSuc = isCaptionSuc;
    }
    return isSuc;
}

- (BOOL)subSpecialKeysFormat{
    BOOL isSuc = NO;
    id columnsObj = [self.elementDic valueForKey:kTableColumns];
    if (columnsObj && [columnsObj isKindOfClass:[NSArray class]]
                   && [columnsObj count]>0) {
        if ([self tableColumnKeyFormat]) {
           isSuc = YES; 
        }
    }else{
        isSuc = NO;
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableColumnSubKeysMissing];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableColumnSubKeysMissing;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
    return isSuc;
}

- (BOOL)tableColumnKeyFormat{
    BOOL isSuc = NO;
    NSArray* tableColumns = [self.elementDic valueForKey:kTableColumns];
    NSArray *keyArray = [[tableColumns objectAtIndex:0] allKeys];
    if ([keyArray count]>3) {
        BOOL isTypeOk = [keyArray containsObject:kElementType];
        BOOL isrenderOrderOk = [keyArray containsObject:kRenderOrder];
        BOOL isColumnTitleOk = [keyArray containsObject:kTableColumnTitle];
        BOOL isColumnIdOk = [keyArray containsObject:kTableColumnId];
        if (isTypeOk && isrenderOrderOk && isColumnIdOk && isColumnTitleOk) {
            isSuc = YES;
        }
    }
    if(!isSuc){
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableColumnDefinitionError];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableColumnDefinitionError;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        
    }
    return isSuc;
}


- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL valueIsRight = [self valueIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    BOOL requiredIsRight = [self requiredIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        valueIsRight && canBreakIsRight && requiredIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!valueIsRight) {
            [self valueValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
        if (!requiredIsRight) {
            [self requiredValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)valueValidate{
    if ([self valueIsExist]) {
        if (![self valueIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableValueError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableValueError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableValueKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableValueKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)requiredValidate{
    if ([self requiredIsExist]) {
        if (![self requiredIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableRequiredError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableRequiredError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableRequiredKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableRequiredKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }else{
            assignSuc = [self columnsValidate];
            if (!assignSuc) {
                isSuc = NO;
            }
        }
    }else{
        isSuc = NO;
    }
    [self titleDataValueValidate:data];
    [self captionDataValueValidate:data];
    
    if (isSuc && _canRender) {
        [self generateFilterTable];
    }
    return isSuc;
}


- (BOOL)assignValue:(NSDictionary *)dataDic{
    BOOL isSuc = YES;
    if (_isAssigned) {
        return isSuc;
    }
    id value = [dataDic valueForKey:self.key];
    if (value == nil) {
        if ([self valueIsRequired]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableValueNotExist;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            
            isSuc = NO;
        }
        _canRender = NO;
    }else{
        [self.valueDic setObject:value forKey:kTableValue];
        [self.elementDic setObject:self.valueDic forKey:kValue];
        _isAssigned = YES;
        CZLog(@"the assingned column element is %@",self.elementDic);
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    if (!_canRender) {
        return isSuc;
    }
    
    isSuc = NO;

    id obj = [self.valueDic valueForKey:kTableValue];
    if ([obj isKindOfClass:[NSArray class]]) {  // version 0
        isSuc = [self isSingleValueValid:obj];
    } else if ([obj isKindOfClass:[NSDictionary class]]) {
        id version = obj[kVersion];
        if ([@1 isEqual:version]) {  // version 1
            id valuesArray  = obj[kValues];
            if ([valuesArray isKindOfClass:[NSArray class]] &&
                [valuesArray count] > 0) {
                BOOL allValid = YES;
                for (id dict in valuesArray) {
                    BOOL singleDataValid = NO;
                    
                    if ([dict isKindOfClass:[NSDictionary class]]) {
                        id value = [dict objectForKey:kValue];
                        if ([self isSingleValueValid:value]) {
                            singleDataValid = YES;
                        }
                    }
                    
                    if (!singleDataValid) {
                        allValid = NO;
                        break;
                    }
                }
                
                isSuc = allValid;
            }
        }
    }
    
    if (!isSuc) {
        if ([self valueIsRequired]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableValueTypeNotMatch];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTableValueTypeNotMatch;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        } else {
            isSuc = YES;
        }
        _canRender = NO;
    }
    
    return isSuc;
}

- (BOOL)isSingleValueValid:(id)value {
    BOOL isSuc = NO;
    NSString *type = [NSString stringWithFormat:@"%@",kArrayType];
    if ([value isKindOfClass:[self convertType2Class:type]]) {
        assert([value isKindOfClass:[NSArray class]]);
        NSError *error = nil;
        isSuc = [self cheackArraySubElementIsDic:value formatError:&error];
        if (!isSuc) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:error.code];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kTableElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = error.code;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }
    
    return isSuc;
}

- (BOOL)columnsValidate{
    BOOL isSuc = YES;
    if (!_canRender) {
        return isSuc;
    }
    NSArray* tableColumns = [self.elementDic valueForKey:kTableColumns];
    
    if ([tableColumns count]>0) {
        NSArray *keyArray = [[tableColumns objectAtIndex:0] allKeys];
        NSInteger keyNum = [keyArray count];
        if (keyNum >0) {
            for (NSDictionary *columnDic in tableColumns) {
                NSArray *keyArrayEvery = [columnDic allKeys];
                for (NSString *key in keyArray) {
                    if ([key isEqualToString:kFilter]) {
                        isHasFilter = YES;
                        continue;
                    }
                    BOOL isContain = [keyArrayEvery containsObject:key];
                    if (!isContain) {
                        isSuc = NO;
                        break;
                    }
                }
                if ([keyArrayEvery containsObject:kFilter]) {
                    isHasFilter = YES;
                }
            }
        }else{
            isSuc = NO;
        }
    }else{
        isSuc = NO;
    }
    if(!isSuc){
        _canRender = NO;
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTableColumnDefinitionError];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTableColumnDefinitionError;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
    BOOL isTablePageBreakOk = [self tablePageBreakValidate];

    if(isSuc){
        isSuc = isTablePageBreakOk;
    }
    return isSuc;
}


- (BOOL)tablePageBreakValidate{
     ///this use used to validate the table is larger than one page
    BOOL isSuc = YES;
    
     CGFloat cellHeight = 0.0;
     /// begin to get the font and font size.
     NSDictionary *fontDic = [self.elementDic valueForKey:kFont];
     fontDic = [CZDictionaryTool validateFontDic:fontDic];
     if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
     fontDic = [CZDictionaryTool defaultFontDic];
     }
     
     CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
     if (renderFontSize <1) {
     renderFontSize = kDefaultTextFontSize;
     }
     NSString *fontName = [fontDic valueForKey:kFontName];
     UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
     if (!font) {
     font = [UIFont systemFontOfSize:renderFontSize];
     }
     /// end to get font and font size.
     cellHeight = [CZDictionaryTool textSize:kHermesText useFont:font].height + kTableRowFontMargin * 2;
     
     if (cellHeight >0) {
     NSArray *value = [self.valueDic valueForKey:kTableValue];
     CGFloat totalHeight = [value count]*cellHeight;
         if (totalHeight == 0) {
         isSuc = NO;
         }else {
             BOOL canBreak = [[self.elementDic valueForKey:kCanBreak] boolValue];
             CGFloat pageBodyHt = [[[NSUserDefaults standardUserDefaults] valueForKey:kPageBodyHeight] floatValue];
             assert( pageBodyHt > 0.f);
             if (!canBreak && totalHeight > pageBodyHt * kTableBreakMarginValue) {
             isSuc = NO;
             }
         }
     }else{
         isSuc = NO;
     }
    if (!isSuc) {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateCanbreakSettingError];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kTableElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTemplateCanbreakSettingError;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);

    }
    return isSuc;
}

#pragma mark -
#pragma mark title validate

- (BOOL)titleFormatValidate {
    CZTitleValidator *validator = [[CZTitleValidator alloc] initWithFormat:self primaryKey:kTitle valueKey:kTitleText];
    BOOL result = [validator formatValidate];
    [validator release];
    return result;
}

- (BOOL)titleDataValueValidate:(NSDictionary *)aData {
    CZTitleValidator *validator = [[CZTitleValidator alloc] initWithFormat:self primaryKey:kTitle valueKey:kTitleText];
    BOOL result = [validator dataValueValidate:aData];
    [validator release];
    return result;
}

#pragma mark - caption validate

- (BOOL)captionFormatValidate {
    CZTitleValidator *validator = [[CZTitleValidator alloc] initWithFormat:self primaryKey:kCaption valueKey:kCaptionText];
    BOOL result = [validator formatValidate];
    [validator release];
    return result;
}

- (BOOL)captionDataValueValidate:(NSDictionary *)aData {
    CZTitleValidator *validator = [[CZTitleValidator alloc] initWithFormat:self primaryKey:kCaption valueKey:kCaptionText];
    BOOL result = [validator dataValueValidate:aData];
    [validator release];
    return result;
}

#pragma mark - table filter

- (void)generateFilterTable{
    CZTableFilter *filter = [[CZTableFilter alloc]initWithContent:self.elementDic];
    CZTableFormat *filterTableFormat = [filter filterTable];
    FreeObj(filter);
    self.filterTable = filterTableFormat;
}

#pragma mark - override CZFormatBase

- (NSString *)elementType {
    return kTableElement;
}

- (void)dealloc{
    FreeObj(filterTable);
    [super dealloc];
}

@end
