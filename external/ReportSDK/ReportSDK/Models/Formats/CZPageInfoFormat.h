//
//  CZPageInfoFormat.h
//  ReportSDK
//
//  Created by Johnny on 8/30/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFormatBase.h"

@interface CZPageInfoFormat : CZFormatBase

/// init method,inherited from CZFormatBase.
- (id)initWithContent:(NSDictionary *)contentDic;

@end
