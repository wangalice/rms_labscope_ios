//
//  CZFormatBase.h
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZReportGeneratorErrorHandler.h"
#import "CZError.h"

@interface CZFormatBase : NSObject{
  @private
    NSString                *_key;
    NSString                *_keyTypeDes;
    NSMutableDictionary     *_elementDic;
    NSMutableDictionary     *_valueDic;
    
    NSMutableArray         *_errorObjArray;

  @protected
    BOOL                    _isValueRequired;
    BOOL                    _isAssigned;
    BOOL                    _canRender;
    
    CZErrorSubDomainType    _subDomain;
    NSInteger               _sequenceOrder;
}

@property (nonatomic, retain, readonly) NSMutableDictionary *elementDic;
@property (nonatomic, retain, readonly) NSMutableArray      *errorObjArray;
@property (nonatomic, retain, readonly) NSString            *key;
@property (nonatomic, retain, readonly) NSMutableDictionary *valueDic;
@property (nonatomic,assign)   BOOL                 isValueRequired;
@property (nonatomic,assign)   BOOL                 isAssigned;
@property (nonatomic,assign)   BOOL                 canRender;

/// indicate when error happend,the error's sub-domain.
@property (nonatomic,assign)   CZErrorSubDomainType subDomain;
/// indicate the element location 
@property (nonatomic,assign)   NSInteger            sequenceOrder;



/// construct method.
- (id)initWithContent:(NSDictionary *)contentDic;

/// convinent method,conver the class description string to class.
- (Class)convertType2Class:(NSString *)type;

/** The followed methods is used for template elements validate
 *  validate the frame,order,element,assiged value,required value is right.
 */
/// template validate.
- (BOOL)templateValidate;

/// validate the frame setting is right
- (BOOL)frameIsRight;

- (BOOL)frameIsExist;

/// validate the order setting is right
- (BOOL)OrderIsRight;

- (BOOL)OrderIsExist;

/// validate the element type is right.
- (BOOL)elementTypeIsRight;

- (BOOL)elementTypeIsExist;

/// validate the value type is right.
- (BOOL)valueIsRight;

- (BOOL)valueIsExist;

/// validate the canbreak setting is right.
- (BOOL)canbreakIsRight;

- (BOOL)canbreakIsExist;

/// validate the required setting is right.
- (BOOL)requiredIsRight;

- (BOOL)requiredIsExist;




/// this method is inherited by the sub class,is used to format the order,frame,element type...
- (BOOL)templateBaseFormat;

/// validate the template element required key is Integral.
- (BOOL)subKeysFormat:(NSError **)aError;


/** The followed methods is used for data validate,data validate mainly have three steps: 
 *  1. assign the value.
 *  2. validate the value is rightful,such as data vlaue type,sub element type,value range.
 *  3. judge the value is optional or required.
 */
- (BOOL)dataValidate:(NSDictionary *)data;

/// assign the data value to the template element,return boolean value,and get error message.
- (BOOL)assignValue:(NSDictionary *)dataDic;

/// validate the assigned value is right.
- (BOOL)valueFormat;

/// get the value is required attribute.
- (BOOL)valueIsRequired;

- (BOOL)cheackArraySubElementIsDic:(NSArray *)dicArray formatError:(NSError **)error;

- (void)setRelativeFrameDic:(NSDictionary *)frameDic;

/// used for debug.
- (void)printContent;

- (NSString *)elementType;

@end

@interface CZTitleValidator : NSObject

- (id)initWithFormat:(CZFormatBase *)format
          primaryKey:(NSString *)primaryKey
            valueKey:(NSString *)valueKey;

@property (nonatomic, copy, readonly) NSString *primaryKey;
@property (nonatomic, copy, readonly) NSString *valueKey;

- (BOOL)formatValidate;
- (BOOL)dataValueValidate:(NSDictionary *)aData;

@end
