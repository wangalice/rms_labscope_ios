//
//  CZFooterFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFooterFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZLineFormat.h"
#import "CZTextFormat.h"
#import "CZPageInfoFormat.h"

@implementation CZFooterFormat

@synthesize footerElementFormats = _footerElementFormats;

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        NSMutableDictionary *subElementsDic = [self.elementDic mutableDeepCopy];
//      NSArray *footerElementDicArray = [self.elementDic valueForKey:kFooterElements];
         NSArray *footerElementDicArray = [subElementsDic valueForKey:kFooterElements];
//        assert([footerElementDicArray count]>0);
        _footerElementFormats = [[NSMutableArray alloc]initWithCapacity:5];
        NSInteger sequence = 0;
        for (NSDictionary *elementDic in footerElementDicArray) {
            sequence += 1;
            NSString *elmentType = [elementDic valueForKey:kElementType];
            if ([elmentType isEqualToString:kTextElement]) {
                CZTextFormat *footerComponent = [[CZTextFormat alloc]initWithContent:elementDic];
                footerComponent.sequenceOrder += sequence;
                footerComponent.subDomain = CZReportGeneratorFooterDomain;
                [_footerElementFormats addObject:footerComponent];
                FreeObj(footerComponent);
            }else if ([elmentType isEqualToString:kLineElement]){
                CZLineFormat *footerComponent = [[CZLineFormat alloc]initWithContent:elementDic];
                footerComponent.sequenceOrder += sequence;
                footerComponent.subDomain = CZReportGeneratorFooterDomain;
                [_footerElementFormats addObject:footerComponent];
                FreeObj(footerComponent);
            }else if ([elmentType isEqualToString:kPageInfoElement]){
                CZPageInfoFormat *footerComponent = [[CZPageInfoFormat alloc]initWithContent:elementDic];
                footerComponent.sequenceOrder += sequence;
                footerComponent.subDomain = CZReportGeneratorFooterDomain;
                [_footerElementFormats addObject:footerComponent];
                FreeObj(footerComponent);
            }
        }
        
        [subElementsDic release];
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    [super templateValidate];
    BOOL isSuc = [self definitionValidate];
    if (_footerElementFormats && [_footerElementFormats count]>0) {
        for (CZFormatBase *formatObj in _footerElementFormats) {
            BOOL subIsSuc = YES;
            subIsSuc = [formatObj templateValidate];
            if (!subIsSuc) {
                isSuc = NO;
                [self.errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    return isSuc;
}


/// The report footer just have type,frame,elements.
- (BOOL)definitionValidate{
    BOOL isSuc = NO;
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    if (frameIsRight && typeIsRight) {
        isSuc = [self valueValidate];
    }else{
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self elementTypeValidate];
        }

        [self valueValidate];
    }
    return isSuc;
}



- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorFooterRootFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportFooter];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorFooterRootFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorFooterRootFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportFooter];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorFooterRootFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)elementTypeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorFooterRootTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportFooter];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorFooterRootTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorFooterRootTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportFooter];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorFooterRootTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}



- (BOOL)valueValidate{
    BOOL isSuc = NO;
    id valueObj = [self.elementDic valueForKey:kFooterElements];
    if (!valueObj) {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                              CZReportGeneratorErrorFooterRootElementsArrayKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportFooter];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorFooterRootElementsArrayKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        
        return isSuc;
    }else{
        BOOL isValueMath = NO;
        BOOL isArray = [valueObj isKindOfClass:[NSArray class]];
        if (isArray) {
            NSArray *arrayObj =(NSArray*) valueObj;
            if ([arrayObj count]>0) {
                for (id element in arrayObj) {
                    if (![element isKindOfClass:[NSDictionary class]]) {
                        isValueMath = NO;
                        break;
                    }else{
                        isValueMath = YES;
                    }
                }
            }
        }
        isSuc = isValueMath;
        if (!isValueMath) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorFooterRootElementsArrayError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportFooter];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorFooterRootElementsArrayError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            return isSuc;
        }
    }
    return isSuc;
}


- (BOOL)subKeysFormat:(NSError **)aError{
    BOOL isSuc = YES;
    id frameObj = [self.elementDic valueForKey:kFrame];
    id typeObj = [self.elementDic valueForKey:kElementType];
    id valueObj = [self.elementDic valueForKey:kFooterElements];
    id pageInfo = [self.elementDic valueForKey:kPageInfo];
    
    if (frameObj && typeObj && valueObj &&pageInfo) {
        BOOL isArray = [valueObj isKindOfClass:[NSArray class]];
        BOOL isHasObjects = NO;
        if (isArray) {
            isHasObjects = [(NSArray *)valueObj count]>0 ? YES:NO;
        }
        if ([self frameIsRight] && [self elementTypeIsRight]
            && isArray && isHasObjects ) {
            isSuc = YES;
        }else{
            isSuc = NO;
            NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTemplateKeyGeneralError];
            if (aError) {
                *aError = curError;
            }
        }
    }else{
        isSuc = NO;
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTemplateKeyGeneralError];
        if (aError) {
            *aError = curError;
        }
    }
    return isSuc;
}

#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super templateValidate];
    if (_footerElementFormats && [_footerElementFormats count]>0) {
        for (CZFormatBase *formatObj in _footerElementFormats) {
            BOOL bodySubFormatSuc =YES;
            bodySubFormatSuc = [formatObj dataValidate:data];
            if (!bodySubFormatSuc) {
                isSuc = NO;
                [self.errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    if (isSuc) {
        [self internalAssignValue];
    }
    return isSuc;
}

- (BOOL)internalAssignValue{
    BOOL isSuc = YES;
    NSMutableArray *elementDicArray = [[NSMutableArray alloc] initWithCapacity:3];
    if ([_footerElementFormats count]>0) {
        NSUInteger elmentCount = [_footerElementFormats count];
        
        for (NSUInteger i = 0; i < elmentCount; i++) {
            CZFormatBase *format = [_footerElementFormats objectAtIndex:i];
            if ([format isKindOfClass:[CZTextFormat class]]) {
                [elementDicArray addObject:format.elementDic];
            }else if ([format isKindOfClass:[CZLineFormat class]]){
                [elementDicArray addObject:format.elementDic];
            }else if ([format isKindOfClass:[CZPageInfoFormat class]]){
                [elementDicArray addObject:format.elementDic];
            }
        }
    }
    CZLog(@"the footer dic is %@",self.elementDic);
    NSMutableDictionary *headerDic = [self.elementDic mutableDeepCopy];
    [headerDic setObject:elementDicArray forKey:kFooterElements];
    CZLog(@"the footer dic is %@",headerDic);
    [elementDicArray release];
    
    self.elementDic = headerDic;
    
    [headerDic release];
    
    return isSuc;
}
 
- (void)FilterErrorElements{
    NSMutableIndexSet *sets = [[NSMutableIndexSet alloc]init];
    if ([_footerElementFormats count]>0) {
        NSInteger elmentCount = [_footerElementFormats count];
        for (NSUInteger i = 0;i<elmentCount; i++) {
            CZFormatBase *format = [_footerElementFormats objectAtIndex:i];
            if ([format isKindOfClass:[CZTextFormat class]]) {
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }else if ([format isKindOfClass:[CZLineFormat class]]){
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }else if ([format isKindOfClass:[CZPageInfoFormat class]]){
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }
        }
    }
    if([sets count]>0){
        [_footerElementFormats removeObjectsAtIndexes:sets];
    }
    [sets release];
    return;
}

- (void)dealloc{
    FreeObj(_footerElementFormats);
    [super dealloc];
}

@end
