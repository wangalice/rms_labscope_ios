//
//  CZHeaderFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZHeaderFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZLineFormat.h"
#import "CZImageFormat.h"
#import "CZTextFormat.h"

@implementation CZHeaderFormat

@synthesize headerElementFormats = _headerElementFormats;

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        NSArray *headerElementDicArray = [self.elementDic valueForKey:kHeaderElements];
        //assert([headerElementDicArray count]>0);
        _headerElementFormats = [[NSMutableArray alloc]initWithCapacity:5];
        NSInteger sequence = 0;

        for (NSDictionary *elementDic in headerElementDicArray) {
            sequence += 1;
            NSString *elmentType = [elementDic valueForKey:kElementType];
            if ([elmentType isEqualToString:kTextElement]) {
                CZTextFormat *headerComponent = [[CZTextFormat alloc]initWithContent:elementDic];
                headerComponent.sequenceOrder += sequence;
                headerComponent.subDomain = CZReportGeneratorHeaderDomain;
                [_headerElementFormats addObject:headerComponent];
                FreeObj(headerComponent);
            }else if ([elmentType isEqualToString:kImageElement]){
                CZImageFormat *headerComponent = [[CZImageFormat alloc]initWithContent:elementDic];
                headerComponent.sequenceOrder += sequence;
                headerComponent.subDomain = CZReportGeneratorHeaderDomain;
                [_headerElementFormats addObject:headerComponent];
                FreeObj(headerComponent);
            }else if ([elmentType isEqualToString:kLineElement]){
                CZLineFormat *headerComponent = [[CZLineFormat alloc]initWithContent:elementDic];
                headerComponent.sequenceOrder += sequence;
                headerComponent.subDomain = CZReportGeneratorHeaderDomain;
                [_headerElementFormats addObject:headerComponent];
                FreeObj(headerComponent);
            }

        }
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    [super templateValidate];
    BOOL isSuc = [self definitionValidate];
    if (_headerElementFormats && [_headerElementFormats count]>0){
        for (CZFormatBase *formatObj in _headerElementFormats) {
            BOOL subIsSuc = YES;
            subIsSuc = [formatObj templateValidate];
            if (!subIsSuc) {
                isSuc = NO;
                [self.errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    return isSuc;
}

/// The report header just have type,frame,elements.
- (BOOL)definitionValidate{
    BOOL isSuc = NO;
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    if (frameIsRight && typeIsRight) {
        isSuc = [self valueValidate];
    }else{
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self elementTypeValidate];
        }
        [self valueValidate];
    }
    return isSuc;
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorHeaderRootFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportHeader];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderRootFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderRootFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportHeader];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderRootFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)elementTypeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorHeaderRootTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportHeader];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderRootTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderRootTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportHeader];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderRootTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (BOOL)valueValidate{
    BOOL isSuc = NO;
    id valueObj = [self.elementDic valueForKey:kHeaderElements];
    if (!valueObj) {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                              CZReportGeneratorErrorHeaderRootElementsArrayKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kReportHeader];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderRootElementsArrayKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        return isSuc;
    }else{
        BOOL isValueMath = NO;
        BOOL isArray = [valueObj isKindOfClass:[NSArray class]];
        if (isArray) {
            NSArray *arrayObj =(NSArray*) valueObj;
            if ([arrayObj count]>0) {
                for (id element in arrayObj) {
                    if (![element isKindOfClass:[NSDictionary class]]) {
                        isValueMath = NO;
                        break;
                    }else{
                        isValueMath = YES;
                    }
                }
            }
        }
        isSuc = isValueMath;
        if (!isValueMath) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorHeaderRootElementsArrayError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kReportHeader];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderRootElementsArrayError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            return isSuc;
        }
    }
    return isSuc;
}


- (BOOL)subKeysFormat:(NSError **)aError{
    BOOL isSuc = YES;
    id frameObj = [self.elementDic valueForKey:kFrame];
    id typeObj = [self.elementDic valueForKey:kElementType];
    id valueObj = [self.elementDic valueForKey:kHeaderElements];
    if (frameObj && typeObj && valueObj) {
        BOOL isArray = [valueObj isKindOfClass:[NSArray class]];
        BOOL isHasObjects = NO;
        if (isArray) {
            isHasObjects = [(NSArray *)valueObj count]>0 ? YES:NO;
        }
        if ([self frameIsRight] && [self elementTypeIsRight]
            && isArray && isHasObjects ) {
            isSuc = YES;
        }else{
            isSuc = NO;
            NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTemplateKeyGeneralError];
            if (aError) {
                *aError = curError;
            }
        }
    }else{
        isSuc = NO;
        NSError* curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorTemplateKeyGeneralError];
        if (aError) {
            *aError = curError;
        }
    }
    return isSuc;
}

#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super templateValidate];
    if (_headerElementFormats && [_headerElementFormats count]>0) {
        for (CZFormatBase *formatObj in _headerElementFormats) {
            BOOL bodySubFormatSuc =YES;
            bodySubFormatSuc = [formatObj dataValidate:data];
            if (!bodySubFormatSuc) {
                isSuc = NO;
                [self.errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    if (isSuc) {
        [self internalAssignValue];
    }
    return isSuc;
}

- (BOOL)internalAssignValue{
    BOOL isSuc = YES;
    NSMutableArray *elementDicArray = [[NSMutableArray alloc] initWithCapacity:3];
    if ([_headerElementFormats count]>0) {
        NSUInteger elmentCount = [_headerElementFormats count];
        
        for (NSUInteger i = 0; i < elmentCount; i++) {
            CZFormatBase *format = [_headerElementFormats objectAtIndex:i];
            if ([format isKindOfClass:[CZImageFormat class]]) {
                [elementDicArray addObject:format.elementDic];
            }else if ([format isKindOfClass:[CZTextFormat class]]){
                [elementDicArray addObject:format.elementDic];
            }else if ([format isKindOfClass:[CZLineFormat class]]){
                [elementDicArray addObject:format.elementDic];
            }
        }
    }
    CZLog(@"the header dic is %@",self.elementDic);
    NSMutableDictionary *headerDic = [self.elementDic mutableDeepCopy];
    [headerDic setObject:elementDicArray forKey:kHeaderElements];
    CZLog(@"the header dic is %@",headerDic);
    [elementDicArray release];
    
    self.elementDic = headerDic;
    [headerDic release];
    
    return isSuc;
}

- (void)FilterErrorElements{
    NSMutableIndexSet *sets = [[NSMutableIndexSet alloc]init];
    
    if ([_headerElementFormats count]>0) {
        NSUInteger elmentCount = [_headerElementFormats count];
        for (NSUInteger i = 0;i<elmentCount; i++) {
            CZFormatBase *format = [_headerElementFormats objectAtIndex:i];
            if ([format isKindOfClass:[CZImageFormat class]]) {
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }else if ([format isKindOfClass:[CZTextFormat class]]){
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }else if ([format isKindOfClass:[CZLineFormat class]]){
                if(!format.canRender){
                    [sets addIndex:i];
                }
            }
        }
    }
    if([sets count]>0){
        [_headerElementFormats removeObjectsAtIndexes:sets];
    }
    [sets release];
    return;
        
}

- (void)dealloc{
    FreeObj(_headerElementFormats);
    [super dealloc];
}

@end
