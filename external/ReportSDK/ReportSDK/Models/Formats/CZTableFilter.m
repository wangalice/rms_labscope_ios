//
//  CZTableFilter.m
//  ReportSDK
//
//  Created by Johnny on 8/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableFilter.h"
#import "CZTableFormat.h"
#import "CZGlobal.h"
#import "NSDictionary+MutableDeepCopy.h"

#define kID @"ID"

@interface CZTableFilter (){
  @private
    NSMutableDictionary *_parentTableDic;
    BOOL                 _isFilterd;
}

@end

@implementation CZTableFilter

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super init];
    if (self) {
        _isFilterd = NO;
        _parentTableDic = [contentDic mutableDeepCopy];
    }
    return self;
}

- (CZTableFormat *)filterTable{
      NSArray* tableColumns = [_parentTableDic valueForKey:kTableColumns];
    NSDictionary *valueDic = [_parentTableDic valueForKey:kValue];
    assert(valueDic);
    id tableValue = [valueDic valueForKey:kTableValue];
    if (!tableValue) {
        return nil;
    }
    
    BOOL contentIsNotEmpty = NO;

    if ([tableValue isKindOfClass:[NSArray class]]) {
        for (NSDictionary *columnDic in tableColumns) {
            
            NSString *filterValue = [columnDic valueForKey:kFilter];
            NSString *filterKey = [columnDic valueForKey:kTableColumnId];
            
            if ([self filterFormat:columnDic]) {
                tableValue = [self filterArrayWithArray:tableValue filterKey:filterKey filterValue:filterValue];
            }
        }
        
        contentIsNotEmpty = [tableValue count] > 0;
        
    } else if ([tableValue isKindOfClass:[NSDictionary class]]) {
        id version = tableValue[kVersion];
        if ([@1 isEqual:version]) {
            NSMutableArray *tableSubValues = tableValue[kValues];
            
            for (NSDictionary *columnDic in tableColumns) {
                
                NSString *filterValue = [columnDic valueForKey:kFilter];
                NSString *filterKey = [columnDic valueForKey:kTableColumnId];
                
                if ([self filterFormat:columnDic]) {
                    for (NSDictionary *oneTableGroup in tableSubValues) {
                        NSArray *tempArray = [self filterArrayWithArray:oneTableGroup[kValue] filterKey:filterKey filterValue:filterValue];
                        if (tempArray) {
                            [oneTableGroup setValue:tempArray forKey:kValue];
                        }
                    }
                }
            }

            for (NSInteger i = [tableSubValues count] - 1; i >= 0; i--) {
                NSDictionary *oneTableGroup = [tableSubValues objectAtIndex:i];
                NSArray* value = oneTableGroup[kValue];
                if ([value count] == 0) {
                    [tableSubValues removeObjectAtIndex:i];
                }
            }
            
            contentIsNotEmpty = [tableSubValues count] > 0;
        }
    } else {
        return nil;
    }
    
    if (!_isFilterd) {
        return nil;
    }else{
        if (contentIsNotEmpty) {
            NSMutableDictionary *valueDic = [NSMutableDictionary dictionaryWithCapacity:2];
            [valueDic setValue:tableValue forKey:kTableValue];
            [_parentTableDic setValue:valueDic forKey:kValue];
            
            NSMutableDictionary *frameDic = [_parentTableDic valueForKey:kFrame];
            /// because parent table is not shown again,so filter table use the parent frame.
            //[frameDic setValue:[NSNumber numberWithFloat:0.f] forKey:@"y"];
            [_parentTableDic setValue:frameDic forKey:kFrame];
            
            CZTableFormat *table = [[CZTableFormat alloc]initWithContent:_parentTableDic];
            FreeObj(_parentTableDic);
            return [table autorelease];
        }
    }
    return nil;
}

- (BOOL)filterFormat:(NSDictionary*)columnDic{
    BOOL isSuc = NO;
    NSString *filterValue = [columnDic valueForKey:kFilter];
    NSString *filterKey = [columnDic valueForKey:kTableColumnId];
    BOOL isValueSuc = NO;
    BOOL isKeySuc = NO;
    if (filterValue && [filterValue isKindOfClass:[NSString class]]) {
        if ([filterValue length]>0) {
            isValueSuc = YES;
        }
    }
    if (filterKey && [filterKey isKindOfClass:[NSString class]]) {
        if ([filterKey length]>0) {
            isKeySuc = YES;
        }
    }
    if (isKeySuc && isValueSuc) {
        isSuc = YES;
    }
    
    return isSuc;
}

- (NSArray *)filterArrayWithArray:(NSArray *)originalArray
                            filterKey:(NSString*)filterKey
                          filterValue:(NSString *)filterValue{
    if (filterKey && [filterKey length]>0 &&
        filterValue && [filterValue length]>0) {
        _isFilterd = YES;
        NSString *key = filterKey;
        NSMutableArray *filterArray = [[NSMutableArray alloc]initWithCapacity:5];
        for (NSUInteger i =0;i<[originalArray count];i++) {
            NSDictionary *dic = [originalArray objectAtIndex:i];
            NSString *dicValue = [dic valueForKey:key];
            if (dicValue && [dicValue isKindOfClass:[NSString class]]) {
                if ([dicValue isEqualToString:filterValue]) {
                    [filterArray addObject:dic];
                }
            }
        }
    return [filterArray autorelease];
    }
    return nil;
}

- (NSString *)formatExpressKey:(NSString *)orignalKey{
    if ([orignalKey length]>2) {
        NSString *lastTwoStr = [orignalKey substringFromIndex:[orignalKey length]-2];
        if ([lastTwoStr isEqualToString:kID]) {
            return orignalKey;
        }
    }
    return [orignalKey stringByAppendingString:kID];
}

- (void)dealloc{
    [_parentTableDic release];
    [super dealloc];
}

@end
