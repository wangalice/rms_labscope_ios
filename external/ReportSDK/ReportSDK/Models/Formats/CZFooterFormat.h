//
//  CZFooterFormat.h
//  ReportSDK
//
//  Created by Johnny on 3/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFormatBase.h"

@interface CZFooterFormat : CZFormatBase{
    NSMutableArray      *_footerElementFormats;
}

@property(nonatomic,readonly) NSMutableArray *footerElementFormats;

/// filter the elements that can not be rendered.
- (void)FilterErrorElements;

@end
