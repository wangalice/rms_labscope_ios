//
//  CZTableFilter.h
//  ReportSDK
//
//  Created by Johnny on 8/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZTableFormat.h"

@interface CZTableFilter : NSObject

- (id)initWithContent:(NSDictionary *)contentDic;

- (CZTableFormat *)filterTable;

@end
