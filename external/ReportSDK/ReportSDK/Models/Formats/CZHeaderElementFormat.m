//
//  CZHeaderElementFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZHeaderElementFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"

#define kSpace @" "


@implementation CZHeaderElementFormat

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        if (self.valueDic) {
            self.key = [self.valueDic valueForKey:kHeaderElementValue];
            if (!self.key || [self.key length]<1) {
                if (![self isValueRequired]) {
                    self.key = kSpace;
                    [self.valueDic setObject:self.key forKey:kHeaderElementValue];
                }
            }
            _isAssigned = [CZDictionaryTool isHaveAssigned:self.key];
            if (!_isAssigned) {
                self.key = [CZDictionaryTool extractKey:self.key];
            }
        }
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    [super templateValidate];
    BOOL isSuc = [self templateBaseFormat];

    return isSuc;
}

- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL valueIsRight = [self valueIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    BOOL requiredIsRight = [self requiredIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        valueIsRight && canBreakIsRight && requiredIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!valueIsRight) {
            [self valueValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
        if (!requiredIsRight) {
            [self requiredValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)valueValidate{
    if ([self valueIsExist]) {
        if (![self valueIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementValueError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementValueError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementValueKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementValueKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)requiredValidate{
    if ([self requiredIsExist]) {
        if (![self requiredIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementRequiredError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementRequiredError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementRequiredKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kHeaderElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorHeaderElementRequiredKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }
    }else{
        isSuc = NO;
    }
    return isSuc;
}

- (BOOL)assignValue:(NSDictionary *)dataDic{
    BOOL isSuc = YES;
    if (_isAssigned) {
        return isSuc;
    }
    id value = [dataDic valueForKey:self.key];
    if (value == nil) {
         if ([self valueIsRequired]){
             NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementValueNotExist];
             CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                         order:_sequenceOrder
                                              errorElementType:kHeaderElement];
             [aError.errorDetailMsgArray addObject:errorMsg];
             aError.errorCode = CZReportGeneratorErrorHeaderElementValueNotExist;
             [self.errorObjArray addObject:aError];
             FreeObj(aError);
             isSuc = NO;
         }else{
             /// add for track bug 261,if no data,should show space word.
             [self.valueDic setObject:@" " forKey:kHeaderElementValue];
             [self.elementDic setObject:self.valueDic forKey:kValue];
             _isAssigned = YES;
         }
        //_canRender = NO;
    }else{
        [self.valueDic setObject:value forKey:kHeaderElementValue];
        [self.elementDic setObject:self.valueDic forKey:kValue];
        _isAssigned = YES;
        CZLog(@"the assingned header element is %@",self.elementDic);
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    id value = [self.valueDic valueForKey:kHeaderElementValue];
    
    NSDictionary *formatDic = [self.valueDic valueForKey:kValueFormat];
//    NSString *type = [formatDic valueForKey:kValueType];
     NSString *type = [NSString stringWithFormat:@"%@",kStringType];
    if ([value isKindOfClass:[self convertType2Class:type]]) {
        isSuc = YES;
        NSUInteger maxLimitaiton = 0;
        NSUInteger minLimitation = 0;
        NSUInteger textLength = [(NSString*)value length];
        NSNumber *maxLength = [formatDic valueForKey:kMaxLength];
        NSNumber *minLength = [formatDic valueForKey:kMinLength];
        
        if (maxLength) {
            maxLimitaiton = [maxLength unsignedIntegerValue];
        }else{
            maxLimitaiton = 1000000000;
        }
        if (minLength) {
            minLimitation = [minLength unsignedIntegerValue];
        }else{
            minLimitation = 1;
        }
        if (textLength <= maxLimitaiton && textLength >= minLimitation) {
            isSuc = YES;
        }else{
            if ([self valueIsRequired]){
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextLengthNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kHeaderElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTextLengthNotMatch;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
                isSuc = NO;
            }
            _canRender = NO;
        }

    }else{
        if ([self valueIsRequired]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorHeaderElementValueTypeNotMatch];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kHeaderElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorHeaderElementValueTypeNotMatch;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            
            isSuc = NO;
        }
        _canRender = NO;
    }
    return isSuc;
}

- (void)dealloc{
    [super dealloc];
}

@end
