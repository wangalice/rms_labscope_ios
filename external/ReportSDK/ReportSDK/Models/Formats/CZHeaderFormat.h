//
//  CZHeaderFormat.h
//  ReportSDK
//
//  Created by Johnny on 3/13/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFormatBase.h"

@interface CZHeaderFormat : CZFormatBase{
    NSMutableArray      *_headerElementFormats;
}

@property (nonatomic,readonly) NSMutableArray   *headerElementFormats;

/// filter the elements that can not be rendered.
- (void)FilterErrorElements;

@end
