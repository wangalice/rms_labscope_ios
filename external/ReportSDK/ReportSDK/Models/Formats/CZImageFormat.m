//
//  CZImageFormat.m
//  ReportSDK
//
//  Created by Johnny on 3/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZImageFormat.h"
#import "CZFormatBase+InSubclassEye.h"
#import "CZDictionaryTool.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZBase64Data.h"
#import "NSDictionary+MutableDeepCopy.h"

@implementation CZImageFormat

- (id)initWithContent:(NSDictionary *)contentDic{
    self = [super initWithContent:contentDic];
    if (self) {
        if (self.valueDic) {
            self.key = [self.valueDic valueForKey:kImageFile];
            _isAssigned = [CZDictionaryTool isHaveAssigned:self.key];
            if (!_isAssigned) {
                self.key = [CZDictionaryTool extractKey:self.key];
            }
        }
    }
    return self;
}

#pragma mark -
#pragma mark template validate

- (BOOL)templateValidate{
    [super templateValidate];
    BOOL isSuc = [self templateBaseFormat];

    BOOL isTitleSuc = [self titleFormatValidate];
    if (isSuc) {
        isSuc = isTitleSuc;
    }
    return isSuc;
}



- (BOOL)templateBaseFormat{
    BOOL isSuc = NO;
    BOOL orderIsRight = [self OrderIsRight];
    BOOL frameIsRight = [self frameIsRight];
    BOOL typeIsRight = [self elementTypeIsRight];
    BOOL valueIsRight = [self valueIsRight];
    BOOL canBreakIsRight = [self canbreakIsRight];
    BOOL requiredIsRight = [self requiredIsRight];
    if (orderIsRight && frameIsRight && typeIsRight &&
        valueIsRight && canBreakIsRight && requiredIsRight) {
        isSuc = YES;
    }else{
        if (!orderIsRight) {
            [self orderValidate];
        }
        if (!frameIsRight) {
            [self frameValidate];
        }
        if (!typeIsRight) {
            [self typeValidate];
        }
        if (!valueIsRight) {
            [self valueValidate];
        }
        if (!canBreakIsRight) {
            [self canbreakValidate];
        }
        if (!requiredIsRight) {
            [self requiredValidate];
        }
    }
    return isSuc;
}

- (void)orderValidate{
    if ([self OrderIsExist]) {
        if (![self OrderIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageOrderError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageOrderError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageOrderKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageOrderKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)frameValidate{
    if ([self frameIsExist]) {
        if (![self frameIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageFrameError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageFrameError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageFrameKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageFrameKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)typeValidate{
    if ([self elementTypeIsExist]) {
        if (![self elementTypeIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageTypeError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageTypeKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageTypeKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)valueValidate{
    if ([self valueIsExist]) {
        if (![self valueIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageValueError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageValueError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageValueKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageValueKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}

- (void)canbreakValidate{
    if ([self canbreakIsExist]) {
        if (![self canbreakIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageCanbreakError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageCanbreakError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageCanbreakKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageCanbreakKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}


- (void)requiredValidate{
    if ([self requiredIsExist]) {
        if (![self requiredIsRight]) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageRequiredError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorImageRequiredError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageRequiredKeyNotExist];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageRequiredKeyNotExist;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
    }
}



#pragma mark -
#pragma mark data validate

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = [super dataValidate:data];
    BOOL assignSuc = [self assignValue:data];
    if (assignSuc) {
        assignSuc = [self valueFormat];
        if (!assignSuc) {
            isSuc = NO;
        }else{
            if (_canRender && [self isValueRequired]) {
                if (![self imageContentValidate]){
                    isSuc = NO;
                }
            }
        }
    }else{
        isSuc = NO;
    }
    return isSuc;
}

- (BOOL)assignValue:(NSDictionary *)dataDic{
    BOOL isSuc = YES;
    if (_isAssigned) {
        return isSuc;
    }
    id value = nil;
    if (![self.key isKindOfClass:[NSString class]]) {
        value = nil;
    }else{
        value = [dataDic valueForKey:self.key];
    }
   
    NSString *defaulImageValue = [self.valueDic valueForKey:kDefaultImageValue];
    if (!value && !defaulImageValue) {
        /// new added for defaultImage value
    if ([self valueIsRequired] ) {
         NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageValueNotExist];
         CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                     order:_sequenceOrder
                                          errorElementType:kImageElement];
         [aError.errorDetailMsgArray addObject:errorMsg];
         aError.errorCode = CZReportGeneratorErrorImageValueNotExist;
         [self.errorObjArray addObject:aError];
         FreeObj(aError);
         isSuc = NO;
     }
    _canRender = NO;
    }else{
        if (value) {
            [self.valueDic setObject:value forKey:kImageFile];
        }else{
            [self.valueDic setObject:defaulImageValue forKey:kImageFile];
        }
        
        [self.elementDic setObject:self.valueDic forKey:kValue];
        _isAssigned = YES;
        CZLog(@"the assingned image element is %@",self.elementDic);
    }
    return isSuc;
}

- (BOOL)valueFormat{
    BOOL isSuc = [super valueFormat];
    if (!isSuc) {
        return isSuc;
    }
    id value = [self.valueDic valueForKey:kImageFile];
    
    NSString *type = [NSString stringWithFormat:@"%@",kStringType];
    Class stringClass = [self convertType2Class:type];
    if ([value isKindOfClass:stringClass]) {
        isSuc = YES;
    } else if ([value isKindOfClass:[NSDictionary class]]) {
        id version = value[kVersion];
        if ([@1 isEqual:version]) {  // version 1
            id valuesArray = value[kValues];
            if ([valuesArray isKindOfClass:[NSArray class]] &&
                [valuesArray count] > 0) {
                BOOL allValid = YES;
                for (id dict in valuesArray) {
                    BOOL singleDataValid = NO;
                    
                    if ([self isSingleValueValid:dict]) {
                        singleDataValid = YES;
                    }
                    
                    if (!singleDataValid) {
                        allValid = NO;
                        break;
                    }
                }
                
                isSuc = allValid;
            }
        }
    }
             
    if (!isSuc) {
        if ([self valueIsRequired]) {
            if (value) {
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageValueTypeNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kImageElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorImageValueTypeNotMatch;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }else{
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageValueNotExist];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kImageElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorImageValueNotExist;
                [self.errorObjArray addObject:aError];
                FreeObj(aError);
            }
            isSuc = NO;
        }
        _canRender = NO;
    }
    return isSuc;
}

- (BOOL)isSingleValueValid:(id)value {
    if ([value isKindOfClass:[NSDictionary class]]) {
        id caption = [value objectForKey:kImageCaption];
        if (caption && ![caption isKindOfClass:[NSString class]]) {
            return NO;
        }
        
        id imageFile = [value objectForKey:kValue];
        if(!(imageFile && [imageFile isKindOfClass:[NSString class]])) {
            return NO;
        }
        
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)imageContentValidate{
    id value = [self.valueDic valueForKey:kImageFile];
    if ([value isKindOfClass:[NSString class]]) {  // data format version 0
        return [self imageFilePathValidate:value fromValueDict:self.valueDic];
    } else if ([value isKindOfClass:[NSDictionary class]]) {  // data format version 1
        NSMutableDictionary *mutableValue = [value mutableDeepCopy];
        self.valueDic[kImageFile] = mutableValue;
        [mutableValue release];
        
        for (NSMutableDictionary *value in [mutableValue objectForKey:kValues]) {
            assert([value isKindOfClass:[NSMutableDictionary class]]);
            
            NSString *subValue = [value valueForKey:kValue];
            if (![self imageFilePathValidate:subValue fromValueDict:value]) {
                return NO;
            }
        }
        return YES;
    }
    return NO;
}


- (BOOL)imageFilePathValidate:(NSString *)imageFile fromValueDict:(NSMutableDictionary *)valueDict {
    BOOL isContentValid = NO;
    if (imageFile && [imageFile length]>0) {
        @autoreleasepool {
            UIImage *imageData = [UIImage imageWithContentsOfFile:imageFile];
            if (imageData && [imageData size].height>0 && [imageData size].width>0){
                isContentValid = YES;
            }else{
                /**
                 * In this branch, indicate the given value is not image file path,so we consider it 
                 * as base64 encoded data string, but if there are no data provide ,we try to use default image value from key 
                 * @"defaultValue"
                 */
                NSData *imageBase64DecodeData = [CZBase64Data dataFromBase64String:imageFile];
                if (imageBase64DecodeData) {
                    imageData = [UIImage imageWithData:imageBase64DecodeData];
                    if (imageData && [imageData size].height>0 && [imageData size].width>0){
                        isContentValid = YES;
                        [self.elementDic setValue:[NSNumber numberWithBool:YES] forKey:kIsUseImageBase64Data];
                    }
                }else{
                    NSString *defaulImageValue = [valueDict valueForKey:kDefaultImageValue];
                    if (defaulImageValue) {
                        if ([defaulImageValue isKindOfClass:[NSString class]] && [defaulImageValue length]>0) {
                           imageBase64DecodeData = [CZBase64Data dataFromBase64String:defaulImageValue];
                            if (imageBase64DecodeData) {
                                imageData = [UIImage imageWithData:imageBase64DecodeData];
                                if (imageData && [imageData size].height>0 && [imageData size].width>0){
                                    isContentValid = YES;
                                    
                                    [valueDict setValue:defaulImageValue forKey:kImageFile];
                                    [self.elementDic setObject:valueDict forKey:kValue];
                                    [self.elementDic setValue:[NSNumber numberWithBool:YES] forKey:kIsUseImageBase64Data];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /// Need to do... use defautDat for imageElement when did not get valid image from image file
    if (!isContentValid && [self valueIsRequired]) {
        
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorImageContentIsEmpty];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorImageContentIsEmpty;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        
    }else{
        if (!isContentValid) {
            _canRender = NO;
            isContentValid = YES;
        }
    }
    return isContentValid;
}


#pragma mark -
#pragma mark title validate
/**
 * Title is optional ,its value can be hard code in title dictionary, or be setted by parameter like $**$.
 * This is just implemented in ImageElement for bakeup.
 * If title need validate, firstly, we should call titleFormatValidate, then call 
 * titleDataValueValidate.
 * Currently, title is optional ,it is not be validated,but can be assigned.
 */

- (BOOL)titleFormatValidate{
    BOOL isSuc = YES;
    id titleDic = [self.elementDic valueForKey:kTitle];
    if (!titleDic) {
        return  isSuc;
    }
    if (titleDic && [titleDic isKindOfClass:[NSDictionary class]]) {
        id titleTextObj = [titleDic valueForKey:kTitleText];
        if (!(titleTextObj && [titleTextObj isKindOfClass:[NSString class]])) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleTextDefinedError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleTextDefinedError;
            [self.errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }
    }else{
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleNotDictionary];
        CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                    order:_sequenceOrder
                                         errorElementType:kImageElement];
        [aError.errorDetailMsgArray addObject:errorMsg];
        aError.errorCode = CZReportGeneratorErrorTemplateElementTitleNotDictionary;
        [self.errorObjArray addObject:aError];
        FreeObj(aError);
        isSuc = NO;
    }
    
    return isSuc;
}

- (BOOL)titleDataValueValidate:(NSDictionary *)aData{
    BOOL isSuc = NO;
    id titleDic = [self.elementDic valueForKey:kTitle];
    if (!titleDic) {
        return  isSuc = YES;
    }
    if (![titleDic isKindOfClass:[NSDictionary class]]) {
        return isSuc;
    }
    id titleTextObj = [titleDic valueForKey:kTitleText];
    if (titleTextObj && [titleTextObj isKindOfClass:[NSString class]]){
        
        NSString *titleKey = nil;
        BOOL isTitleAssigned = [CZDictionaryTool isHaveAssigned:titleTextObj];
        if (!isTitleAssigned) {
            titleKey = [CZDictionaryTool extractKey:titleTextObj];
            isSuc = [self assignTitleValue:aData withKey:titleKey];
        }else{
            isSuc = YES;
        }
    }
    return isSuc;
}


- (BOOL)assignTitleValue:(NSDictionary *)aData withKey:(NSString *)titleKey{
    BOOL isSuc = YES;
    if (titleKey && aData) {
        id value = [aData valueForKey:titleKey];
        if (value == nil) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueNotExist];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueNotExist;
            //[_errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }else if (![value isKindOfClass:[NSString class]]){
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleValueTypeError];
            CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                        order:_sequenceOrder
                                             errorElementType:kImageElement];
            [aError.errorDetailMsgArray addObject:errorMsg];
            aError.errorCode = CZReportGeneratorErrorTemplateElementTitleValueTypeError;
            //[_errorObjArray addObject:aError];
            FreeObj(aError);
            isSuc = NO;
        }else{
            NSMutableDictionary *titleDic =[NSMutableDictionary dictionaryWithDictionary: [self.elementDic valueForKey:kTitle]];
            NSDictionary *formatDic = [titleDic valueForKey:kTitleFormat];
            
            NSUInteger maxLimitaiton = 1000000000;
            NSUInteger minLimitation = 1;
            NSUInteger textLength = [(NSString*)value length];
            
            if (formatDic && [formatDic isKindOfClass:[NSDictionary class]]) {
                NSNumber *maxLength = [formatDic valueForKey:kMaxLength];
                NSNumber *minLength = [formatDic valueForKey:kMinLength];
                
                if (maxLength && [maxLength isKindOfClass:[NSNumber class]]) {
                    maxLimitaiton = [maxLength unsignedIntegerValue];
                }else{
                    maxLimitaiton = 1000000000;
                }
                if (minLength && [minLength isKindOfClass:[NSNumber class]]) {
                    minLimitation = [minLength unsignedIntegerValue];
                }else{
                    minLimitation = 1;
                }
            }
            if (textLength <= maxLimitaiton && textLength >= minLimitation) {
                isSuc = YES;
                [titleDic setValue:value forKey:kTitleText];
                [self.elementDic setValue:titleDic forKey:kTitle];
            }else{
                NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTemplateElementTitleLengthNotMatch];
                CZError *aError = [[CZError alloc] initWithDomain:_subDomain
                                                            order:_sequenceOrder
                                                 errorElementType:kImageElement];
                [aError.errorDetailMsgArray addObject:errorMsg];
                aError.errorCode = CZReportGeneratorErrorTemplateElementTitleLengthNotMatch;
                //[_errorObjArray addObject:aError];
                FreeObj(aError);
                isSuc = NO;
            }
        }
    }
    if (!isSuc) {
        [self.elementDic setValue:nil forKey:kTitle];
    }
    return isSuc;
}

#pragma mark - override CZFormatBase

- (NSString *)elementType {
    return kImageElement;
}

- (void)dealloc{
    [super dealloc];
}

@end
