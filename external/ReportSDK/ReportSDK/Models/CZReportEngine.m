//
//  CZReportEngine.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportEngine.h"
#import "CZReportFactory.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZReportData.h"
#import "CZSystemTemplate.h"
#import "CZDictionaryTool.h"
#import "CZRTFTool.h"
#import "CZRTFPreviewConfig.h"

#import "CZReportConfigManager.h"
@interface CZReportEngine()<CZRenderDelegate>{
    CZReportFactory         *_factory;
    CZReportData            *_reportData;
    CZSystemTemplate        *_systemTemplate;
    BOOL                    _validateIsSuc;
    BOOL                    _isValidated;
    
    
    ReportEngineReportType  _reportType;
    NSString                *_renderFileDestination;
    NSDictionary            *_dataDic;
    NSDictionary            *_templateDic;
    
    CGFloat                 _imageCompressRate;
    CGFloat                 _imageDpi;
}
- (void)generatePDF;
- (void)generateRTF;
- (BOOL)systemValidate:(NSError **)aError;
- (BOOL)fileIsExist:(NSString *)filePath;
- (void)PrepareData:(BOOL)isReport;
@end

@implementation CZReportEngine

@synthesize dataDic = _dataDic;
@synthesize templateDic = _templateDic;
@synthesize delegate;

#pragma mark - External interface Methods

- (id)initEngine:(NSDictionary*)data reportTemplate:(NSDictionary*)systemplate{
    if (self = [super init]) {
        _validateIsSuc = NO;
        _isValidated = NO;
        _reportData = [[CZReportData alloc] initWithDictionary:data];
        self.dataDic = data;
        _templateDic = systemplate;
        [_templateDic retain];
        _systemTemplate = [[CZSystemTemplate alloc]initWithDictionary:systemplate];
        _factory = [[CZReportFactory alloc] initWithData:_dataDic
                                          systemTemplate:_systemTemplate];
        _factory.delegate = self;
    }
    return self;
}

- (BOOL)isCancelled {
    return _factory.isCancelled;
}

- (void)cancel {
    _factory.cancelled = YES;
    _factory.userCancelled = YES;
}

/** asynchronous method;
 *  Before export report,the template and data must be validated.
 */
- (void)exportReport:(NSString*)fileName
          reportType:(ReportEngineReportType)type
   imageCompressRate:(CGFloat)rate
              useDpi:(CGFloat)dpi{

    assert((fileName != nil) && [fileName length]>0);
    _imageCompressRate = rate;
    _imageDpi = dpi;
    
    FreeObj(_renderFileDestination);
    _renderFileDestination = fileName;
    [_renderFileDestination retain];
     NSError* error = nil;
     BOOL systemIsSuc = [self systemValidate:&error];
    if (!systemIsSuc) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(generateDidFailWithError:)]) {
            [self.delegate generateDidFailWithError:(error)];
        }
        return;
    }
    if (_validateIsSuc && _isValidated) {
       // before we generate the pdf ,first should ensure the data and template have been validated.
        [CZReportConfigManager sharedInstance].imageCompressRate = _imageCompressRate;
        [CZReportConfigManager sharedInstance].imageDpi = _imageDpi;
        switch (type) {
            case ReportEngineReportTypePDF:{
                [self generatePDF];
            }
                break;
            case ReportEngineReportTypeRTF:{
                [self generateRTF];
            }
                break;
            case ReportEngineReportTypeRTFD:{
                [self generateRTFD];
            }
                break;
            default:
                break;
        }
    }else{
        if (!_isValidated) {
             error = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorDataAndTemplateNotValidated];
        }else{
            error = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorDataAndTemplateValidateFailed];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(generateDidFailWithError:)]) {
            [self.delegate generateDidFailWithError:(error)];
        }
    }
}

/** validate have four steps. Firstly should prepare the data; secondly, validate the resource,like if report engine has data or template; Thirdly, system validate; lastly, the data and template content validate.
 */
- (BOOL)validateReport:(NSError **)error isReport:(BOOL)isForReport{
    BOOL isSuc = YES;
    _isValidated = YES;
    [self PrepareData:isForReport];
    isSuc = [self resourceValidate:error];
    if (!isSuc) {
        return isSuc;
    }
    isSuc = [self systemValidate:error];
    if (!isSuc) {
        return isSuc;
    }
    isSuc = [_factory dataAndTemplateValidate:error];
    if (!isSuc) {
        assert( error != nil);
    }
    if (isSuc) {
        _validateIsSuc = YES;
    }
    return isSuc;
}

/**
 * This method is just for validation
 * Prepare the data ,if prepare for report,should only use the paremeter passed data,if prepare for
 * thumbnail,firstly use the parementer passed data,then use the data from report template.
 */
- (void)PrepareData:(BOOL)isReport{
    if (isReport) {
        FreeObj(_dataDic);
        _dataDic = _reportData.originalDataDic;
        [_dataDic retain];
    }else{
        NSDictionary *reportData = [_templateDic valueForKey:kDataFromTemplate];
        if (reportData && [reportData isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary *newData = [[NSMutableDictionary alloc]initWithDictionary:reportData];
            if (_reportData.originalDataDic &&
                [_reportData.originalDataDic isKindOfClass:[NSDictionary class]]) {
                NSArray *keys = [_reportData.originalDataDic allKeys];
                if ([keys count]>0) {
                    for (NSString *key in keys) {
                        id value = [_reportData.originalDataDic valueForKey:key];
                        [newData setValue:value forKey:key];
                    }
                }
            }
            FreeObj(_dataDic);
            _dataDic = [[NSDictionary alloc]initWithDictionary:newData];
            FreeObj(newData);
        }
    }
    FreeObj(_factory);
    _factory = [[CZReportFactory alloc] initWithData:_dataDic
                                      systemTemplate:_systemTemplate];
    _factory.delegate = self;
}

- (BOOL)resourceValidate:(NSError **)error{
    BOOL isSuc = YES;
    if(!_dataDic || !_templateDic){
        NSError * curError = [CZReportGeneratorErrorHandler createErrorWithCode:
                              CZReportGeneratorErrorResourceMissing];
        if (error) {
            *error = curError;
        }
        isSuc = NO;
    }
    return isSuc;
}

#pragma mark -  generate Thumbnails Methods

- (NSArray *)generateThumbnails:(NSRange)thumbnailRange
                  thumbnailSize:(CGSize)aSize{
    NSInteger startIndex = thumbnailRange.location + 1;
    NSInteger endIndex = thumbnailRange.location + thumbnailRange.length +1;
    if (endIndex < startIndex) {
        return nil;
    }
    
    BOOL isNeedGeneratePDF = YES;
    BOOL isGeneratePDFSuc = NO;
    NSString *pdfThumnailFile = nil;
    if ([self fileIsExist:_renderFileDestination] && [_renderFileDestination hasSuffix:@".pdf"]) {
        isNeedGeneratePDF = NO;
    }
    if (isNeedGeneratePDF) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        pdfThumnailFile = [documentsDirectory stringByAppendingPathComponent:@"thumbnail.pdf"];
        isGeneratePDFSuc =  [_factory generatePDFThumbnail:pdfThumnailFile aRange:thumbnailRange];
        if (!isGeneratePDFSuc) {
            return nil;
        }
    }else{
        pdfThumnailFile = [NSString stringWithFormat:@"%@",_renderFileDestination];
    }
    
    CFStringRef path;
    CFURLRef url;
    CGPDFDocumentRef document;
    path = CFStringCreateWithCString(NULL, [pdfThumnailFile UTF8String], kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, NO);
    CFRelease(path);
    document = CGPDFDocumentCreateWithURL(url);
    CFRelease(url);
    NSInteger totalPages = CGPDFDocumentGetNumberOfPages(document);
    if (startIndex <= totalPages) {
        NSMutableArray *mutableArray = [[NSMutableArray alloc]initWithCapacity:3];
        if (endIndex >= totalPages) {
           endIndex = totalPages;
        }
        for (NSUInteger i = startIndex;i <= endIndex; i++) {
            UIImage *thumb = [self imageFromPDFDocRef:document pageIndex:i size:aSize];
            [mutableArray addObject:thumb];
            /// Just for debug.
            //[CZRTFTool convertImage2ImageFile:thumb];
        }
        CFRelease(document);
        if (isGeneratePDFSuc) {
            NSFileManager *fileManger = [NSFileManager defaultManager];
            [fileManger removeItemAtPath:pdfThumnailFile error:nil];
        }
        return [mutableArray autorelease];
    }
    if (document) {
        CFRelease(document); 
    }
   
    return nil;
}


- (UIImage *)imageFromPDFDocRef:(CGPDFDocumentRef)documentRef
                       pageIndex:(NSUInteger)index
                            size:(CGSize)aSize{
    CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, index);
    CGRect pageRect = CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
    
    CGFloat scaleX = aSize.width / pageRect.size.width;
    CGFloat scaleY = aSize.height / pageRect.size.height;
    if (scaleX < scaleY) {
        scaleY = scaleX;
        aSize.height = floorf(pageRect.size.height * scaleX + 0.5f);
    } else {
        scaleX = scaleY;
        aSize.width = floorf(pageRect.size.width * scaleY + 0.5f);
    }
    
    UIGraphicsBeginImageContext(aSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0, aSize.height);
    CGContextScaleCTM(context, scaleX, -scaleY);
    CGContextDrawPDFPage (context, pageRef);
    CGContextTranslateCTM(context, 0, 0);
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
    
}

#pragma mark -  setting Methods

- (void)setDataDic:(NSDictionary *)dataDic{
    [dataDic retain];
    FreeObj(_dataDic);
    _dataDic = dataDic;
    
    FreeObj(_reportData);
    _reportData = [[CZReportData alloc] initWithDictionary:dataDic];
    
    _validateIsSuc = NO;
    _isValidated = NO;
}

/** for the template is readonly.
- (void)setTemplateDic:(NSDictionary *)templateDic{
    [templateDic retain];
    if (_templateDic) {
     [_templateDic release];
    }
    _templateDic = templateDic;
    _isValidated = NO;
    _validateIsSuc = NO;

}
*/

#pragma mark -  validate Methods

- (BOOL)systemValidate:(NSError **)aError{
    BOOL isSystemSuc = YES;
    
    CGFloat leftSpace = [CZDictionaryTool getDiskTotalSpace];
    if (leftSpace <5) {
        isSystemSuc = NO;
        NSError * curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorMemoryNotEnough];
        if (aError) {
            *aError = curError;
        }
    return isSystemSuc;
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (_renderFileDestination) {
        BOOL isFilePathValid = NO;
        
        BOOL isPdf = [_renderFileDestination hasSuffix:@".pdf"];
        BOOL isRtf = [_renderFileDestination hasSuffix:@".rtf"];
        BOOL isRtfd = [_renderFileDestination hasSuffix:@".rtfd"];

        
        NSInteger filePathTotalLength = [_renderFileDestination length];
        NSString *dir = nil;
        if (isPdf || isRtf || isRtfd) {
            dir = [_renderFileDestination substringToIndex:filePathTotalLength-4];
            if (dir) {
             isFilePathValid =  [fileManager createDirectoryAtPath:dir
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:nil];
                if (isFilePathValid) {
                    isFilePathValid =  [fileManager removeItemAtPath:dir error:nil];
                }
            }
        }else{
            isFilePathValid = NO;
        }
        if (!isFilePathValid) {
            NSError * curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorExportFileCreateFailed];
            if (aError) {
                *aError = curError;
            }
            isSystemSuc = isFilePathValid;
        }
    }
    return isSystemSuc;
}

- (NSArray *)scanTemplate{
    if (_factory && _factory.systemTemplate) {
        return [_factory.systemTemplate needValueElements];
    }
    return nil;
}

#pragma mark - Private Methods

- (void)generatePDF{
    if (_factory) {
        //BOOL intergrateOk = [_factory integrateDataAndTemplate];
        /// before generate,the data and template have been integrated.
        _factory.reportDestination = _renderFileDestination;
        [_factory generatePDFReport];
        
    }
}

- (void)generateRTF{
    if (_factory) {
        _factory.reportDestination = _renderFileDestination;
        [_factory generateRTFReport];
    }
}

- (void)generateRTFD{
    if (_factory) {
        _factory.reportDestination = _renderFileDestination;
        [_factory generateRTFDReport];
    }
}

- (BOOL)fileIsExist:(NSString *)filePath{
    BOOL isExist = NO;
    if (filePath && [filePath length] > 0) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
      isExist =  [fileManager fileExistsAtPath:filePath];
    }
    return isExist;
}

#pragma mark - CZRenderDelegate Methods

- (void)notifyRenderProgress:(ReportEngineReportType)type
           currentPercentage:(CGFloat)percentage
                 destination:(NSString *) filePath{
        if (self.delegate && [self.delegate respondsToSelector:@selector(generateProgress:currentPercentage:destination:)]) {
            [self.delegate generateProgress:type
                          currentPercentage:percentage
                                destination:filePath];
             }
        if (percentage >= 1) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(generateDidFinishDestination:)]) {
                [self.delegate generateDidFinishDestination:filePath];
            }
        }
}

- (void)notifyRenderFailWithError:(NSError *)error{
    CZLog(@"the notifyRenderFailWithError  notified filepath is %@",error.localizedDescription);
    _factory.cancelled = YES;
    _factory.userCancelled = ([error code] == CZReportGeneratorErrorUserCanceled);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(generateDidFailWithError:)]) {
        [self.delegate generateDidFailWithError:error];
    }
}

-(void)dealloc{
    self.delegate = nil;
    FreeObj(_dataDic);
    FreeObj(_templateDic);
    FreeObj(_reportData);
    FreeObj(_systemTemplate);
    FreeObj(_factory);
    FreeObj(_renderFileDestination);
    [CZRTFPreviewConfig freeInstance];
    [super dealloc];
}

@end
