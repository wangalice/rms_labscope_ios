//
//  CZRTFPage.h
//  ReportSDK
//
//  Created by Johnny on 9/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZPDFPage.h"


@interface CZRTFPage: CZPDFPage

/// render the pdf page to pdf context.
- (void)drawPageIntoStream:(NSOutputStream *)stream;

@end
