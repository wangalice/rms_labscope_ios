//
//  CZReportFactory.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportFactory.h"
#import "CZFormatBase.h"
#import "CZRTFPreviewConfig.h"

@interface CZReportFactory () {
    NSDictionary                    *_reportData;
    CZSystemTemplate                *_systemTemplate;
    NSDictionary                    *_renderContent;
    CZContentRenderManager          *_renderManager;
}
@end

@implementation CZReportFactory

- (id)initWithData:(NSDictionary *)data systemTemplate:(CZSystemTemplate *)sysTemplate {
    self = [super init];
    if (self) {
        _reportData = data;
        [_reportData retain];
        self.systemTemplate = sysTemplate;
        
        [self addObserver:self forKeyPath:@"cancelled" options:0 context:NULL];
    }
    return self;
}

- (BOOL)dataAndTemplateValidate:(NSError **)aError {
    BOOL isSuc = YES;
    BOOL templateIsSuc = [_systemTemplate templateValidate];
    if (!templateIsSuc) {
        isSuc = NO;
        NSMutableArray *errorDicArray = [[NSMutableArray alloc]initWithCapacity:4];
        NSMutableDictionary *errorInfo = [[NSMutableDictionary alloc] initWithCapacity:6];
        for (CZError *errorObj in _systemTemplate.errorObjArray) {
            NSDictionary *errorDic = errorObj.dicInfo;
            [errorDicArray addObject:errorDic];
        }
        [errorInfo setValue:errorDicArray forKey:kErrorDetails];
        [errorDicArray release];
        if (aError) {
            NSString *errorDes = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorReportTemplateError];
            [errorInfo setValue:errorDes forKey:kErrorDes];
            NSError *curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorReportTemplateError
                                                                          userInfo:errorInfo];
            *aError = curError;
        }
        [errorInfo release];
    } else {
        BOOL dataIsSuc = [_systemTemplate dataValidate:_reportData];
        if (!dataIsSuc) {
            isSuc = NO;
            NSMutableArray *errorDicArray = [[NSMutableArray alloc]initWithCapacity:4];
            NSMutableDictionary *errorInfo = [[NSMutableDictionary alloc] initWithCapacity:6];
            for (CZError *errorObj in _systemTemplate.errorObjArray) {
                NSDictionary *errorDic = errorObj.dicInfo;
                [errorDicArray addObject:errorDic];
            }
            [errorInfo setValue:errorDicArray forKey:kErrorDetails];
            [errorDicArray release];
            if (aError) {
                NSString *errorDes = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorReportDataError];
                [errorInfo setValue:errorDes forKey:kErrorDes];
                NSError *curError = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorReportDataError
                                                                              userInfo:errorInfo];
                *aError = curError;
            }
            [errorInfo release];
        }
    }
    return isSuc;
}

- (void)generatePDFReport {
    [self canclePreGenerateAction];
    
    _renderManager = [[CZPDFContentRenderManger alloc] initWithRenderContent:_systemTemplate.intergrateDic];
    _renderManager.reportDestination = self.reportDestination;
    _renderManager.delegate = self.delegate;
    
    [_renderManager startGenerate];

    if (self.isCancelled) {
        [[NSFileManager defaultManager] removeItemAtPath:self.reportDestination error:NULL];
        
        if (self.isUserCancelled) {
            [_renderManager notifyError:CZReportGeneratorErrorUserCanceled];
        }
    }
    
    FreeObj(_renderManager);
    
    NSLog(@"is finished");
}

- (BOOL)generatePDFThumbnail:(NSString *)tempPath aRange:(NSRange)range {
    [self canclePreGenerateAction];
    
    CZPDFContentRenderManger *pdfRenderManager = [[CZPDFContentRenderManger alloc] initWithRenderContent:_systemTemplate.intergrateDic];
    _renderManager = pdfRenderManager;
    _renderManager.reportDestination = tempPath;
    _renderManager.delegate = nil;
    BOOL isSuc = [pdfRenderManager generatePDF:tempPath pageRange:range];
    
    if (self.isCancelled) {
        [[NSFileManager defaultManager] removeItemAtPath:tempPath error:NULL];
        
        if (self.isUserCancelled) {
            [_renderManager notifyError:CZReportGeneratorErrorUserCanceled];
        }
    }
    
    FreeObj(_renderManager);
    return isSuc;
}


- (void)generateRTFReport {
    [self canclePreGenerateAction];
    
    [[CZRTFPreviewConfig sharedInstance] setFileDestination:self.reportDestination];
    [[CZRTFPreviewConfig sharedInstance] configForRTF];
    
    _renderManager = [[CZRTFContentRenderManager alloc] initWithRenderContent:_systemTemplate.intergrateDic];
    _renderManager.reportDestination = self.reportDestination;
    _renderManager.delegate = self.delegate;
    
    [_renderManager startGenerate];
    
    if (self.isCancelled) {
        [[NSFileManager defaultManager] removeItemAtPath:self.reportDestination error:NULL];
        
        if (self.isUserCancelled) {
            [_renderManager notifyError:CZReportGeneratorErrorUserCanceled];
        }
    }
    
    FreeObj(_renderManager);
}

- (void)generateRTFDReport {
    [self canclePreGenerateAction];
    
    /** followed code is not used currently, because RTFD can not show header and footer.
    [[CZRTFPreviewConfig sharedInstance] setFileDestination:self.reportDestination];
    [[CZRTFPreviewConfig sharedInstance] configForRTFD];
    
    _reportType = ReportEngineReportTypeRTFD;
    FreeObj(_RTFRenderManager);
    _RTFRenderManager = [[CZRTFContentRenderManager alloc] initWithRenderContent:_systemTemplate.intergrateDic];
    _RTFRenderManager.reportDestination = self.reportDestination;
    _RTFRenderManager.delegate = self.delegate;
    _RTFRenderManager.notifyThread = [NSThread currentThread];
    _generatorThread = [[NSThread alloc] initWithTarget:_RTFRenderManager
                                               selector:@selector(startGenerate)
                                                 object:nil];
    [_generatorThread start];
    
    if (![[NSThread currentThread] isMainThread]) {
        while (self.isRun) {
            CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0, YES);
        }
    }
    */
}

- (void)canclePreGenerateAction {
    _renderManager.cancelled = YES;
    _renderManager.delegate = nil;
    FreeObj(_renderManager);
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self && [keyPath isEqualToString:@"cancelled"]) {
        if (self.isCancelled) {
            _renderManager.cancelled = YES;
        }
    }
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"cancelled"];
    [self canclePreGenerateAction];
    
    self.delegate = nil;
    FreeObj(_reportData);
    FreeObj(_systemTemplate);
    FreeObj(_renderManager);
    FreeObj(_renderContent);
    FreeObj(_reportDestination);
    [super dealloc];
}

@end
