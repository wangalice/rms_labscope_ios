//
//  CZContentRenderManager.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZContentRenderManager.h"
#import "CZDictionaryTool.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZReportGeneratorErrorHandler.h"

@implementation CZContentRenderManager

@synthesize renderContent = _renderContent;
@synthesize delegate;
@synthesize reportDestination = _reportDestination;

- (id)initWithRenderContent:(NSDictionary *)aContentDic{
    self = [super init];
    if (self) {
        _renderContent = [aContentDic mutableDeepCopy];
        _totalPages = 0;
        _currentPageIndex = 0;
    }
    return self;
}

-(void)startGenerate{
    currentProgress = 0.f;
}

- (void)notifyError:(NSInteger)errorCode {
    NSError *error = nil;
    error = [CZReportGeneratorErrorHandler createErrorWithCode:errorCode];
    if (self.delegate && [self.delegate respondsToSelector:@selector(notifyRenderFailWithError:)]) {
        [self.delegate notifyRenderFailWithError:error];
    }
}

- (void)dealloc{
    FreeObj(_renderContent);
    FreeObj(_reportDestination);
    [super dealloc];
}
@end
