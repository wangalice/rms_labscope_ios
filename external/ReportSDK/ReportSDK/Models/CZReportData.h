//
//  CZReportData.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZReportData : NSObject{
    NSDictionary    *_originalDataDic;
}

@property(nonatomic,retain) NSDictionary *originalDataDic;

-(id)initWithDictionary:(NSDictionary *)aDictionary;


@end
