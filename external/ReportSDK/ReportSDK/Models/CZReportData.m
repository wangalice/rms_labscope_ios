//
//  CZReportData.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportData.h"

@implementation CZReportData

@synthesize originalDataDic = _originalDataDic;

-(id)initWithDictionary:(NSDictionary *)aDictionary{
    self = [super init];
    if (self) {
        self.originalDataDic = aDictionary;
        return self;
    }
    return self;
}

- (void)dealloc{
    FreeObj(_originalDataDic);
    [super dealloc];
}

@end
