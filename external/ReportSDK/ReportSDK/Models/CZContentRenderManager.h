//
//  CZContentRenderManager.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZTypedefine.h"

@protocol CZRenderDelegate <NSObject>

- (void)notifyRenderFailWithError:(NSError *)error;

- (void)notifyRenderProgress:(ReportEngineReportType)type
           currentPercentage:(CGFloat)percentage
                 destination:(NSString *)filePath;

@end

@interface CZContentRenderManager : NSObject {
    NSDictionary                *_renderContent;
    NSString                    *_reportDestination;
    CGSize                      _pageSize;
    NSUInteger                   _totalPages;
    NSUInteger                   _currentPageIndex;
    CGFloat                      currentProgress;
}

@property(nonatomic, retain) NSDictionary               *renderContent;
@property(nonatomic, assign) id<CZRenderDelegate>       delegate;
@property(nonatomic, retain) NSString                   *reportDestination;
@property(atomic, getter=isCancelled) BOOL              cancelled;
/**
 *methods  traversal all dic elements, compute pdf pages ,base on element type
 *and element render type compute size ...
 */

/// init method.
- (id)initWithRenderContent:(NSDictionary *)aContentDic;

/// start generate the RTF or PDF content,this method is called in sub thread.
-(void)startGenerate;

- (void)notifyError:(NSInteger)errorCode;

@end
