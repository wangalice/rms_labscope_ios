//
//  CZRenderBase.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZRenderBase : NSObject{
    CGContextRef    _currentContext;
}

- (id)initWithConfig:(NSDictionary *)configDic;
@end
