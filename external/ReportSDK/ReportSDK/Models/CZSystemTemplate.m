//
//  CZSystemTemplate.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSystemTemplate.h"
#import "CZHeaderFormat.h"
#import "CZPieFormat.h"
#import "CZColumnFormat.h"
#import "CZTableFormat.h"
#import "CZTextFormat.h"
#import "CZLineFormat.h"
#import "CZImageFormat.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZFooterFormat.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZDictionaryTool.h"
#import "CZError.h"
#import "CZKeyRegistManager.h"

#define kWidth  @"width"
#define kHeight @"height"

@interface CZSystemTemplate (){
    CZHeaderFormat *_headerFormat;
    NSMutableArray *_bodyFormats;
    CZFooterFormat *_footerFormat;
}

@end

@implementation CZSystemTemplate

@synthesize originalTemplate = _originalTemplate;
@synthesize intergrateDic = _intergrateDic;
@synthesize errorObjArray = _errorObjArray;

-(id)initWithDictionary:(NSDictionary *)aDictionary{
    self = [super init];
    if (self) {
        self.originalTemplate = [NSMutableDictionary dictionaryWithDictionary:aDictionary];
        _errorObjArray = [[NSMutableArray alloc]initWithCapacity:5];
    }
    return self;
}

- (BOOL)systemValidate{
    BOOL isSuc = YES;
    return isSuc;
}

- (BOOL)templateValidate{
    BOOL isSuc = YES;
    [_errorObjArray removeAllObjects];
    BOOL isGenerateTemplateSuc = [self generateFormats];
    if (!isGenerateTemplateSuc) {
        isSuc = NO;
        return isSuc;
    }
    if (![self templateMetadataValidate]) {
        isSuc = NO;
    }
    if (_bodyFormats && [_bodyFormats count]>0) {
        for (CZFormatBase *formatObj in _bodyFormats) {
            BOOL subIsSuc = YES;
            subIsSuc = [formatObj templateValidate];
            if (!subIsSuc) {
                isSuc = NO;
                [_errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    if (_headerFormat) {
        BOOL isHeaderValidateSuc = [_headerFormat templateValidate];
        if (!isHeaderValidateSuc) {
            isSuc = NO;
            [_errorObjArray addObjectsFromArray:_headerFormat.errorObjArray];
        }
    }
    if (_footerFormat) {
        BOOL isHeaderValidateSuc = [_footerFormat templateValidate];
        if (!isHeaderValidateSuc) {
            isSuc = NO;
            [_errorObjArray addObjectsFromArray:_footerFormat.errorObjArray];
        }
    }
    BOOL isDup =[self keyDuplicateValidate];
    if (!isDup) {
        isSuc = isDup;
    }
    return isSuc;
}

- (BOOL)keyDuplicateValidate{
    BOOL isSuc = YES;
    NSArray *dicArray = [self needValueElements];
    CZError *aError = [[CZKeyRegistManager sharedInstance] keyDuplicateValidate:dicArray];
    if (aError) {
        isSuc = NO;
        [_errorObjArray addObject:aError];
    }
    return isSuc;
    
}

- (BOOL)templateMetadataValidate{
    BOOL isSuc = NO;
    NSDictionary *metadataDic = [_originalTemplate valueForKey:kReportTemplateMetadata];
    if (metadataDic && [metadataDic isKindOfClass:[NSDictionary class]]) {
        id dateObj = [metadataDic valueForKey:kReportDate];
        id nameObj = [metadataDic valueForKey:kReportName];
        id typeObj = [metadataDic valueForKey:kElementType];
        id pageSizeObj = [metadataDic valueForKey:kPageSize];
        if (dateObj && nameObj && typeObj && pageSizeObj) {
            if (pageSizeObj && [pageSizeObj isKindOfClass:[NSDictionary class]]) {
                id widthObj = [pageSizeObj valueForKey:kWidth];
                id heightObj = [pageSizeObj valueForKey:kHeight];
                
                CGFloat width = 0.f;
                CGFloat height = 0.f;
                if ([widthObj isKindOfClass:[NSNumber class]]
                    && [heightObj isKindOfClass:[NSNumber class]]) {
                     width = [[pageSizeObj valueForKey:kWidth]floatValue];
                     height = [[pageSizeObj valueForKey:kHeight]floatValue];
                }
                if (width * height >0) {
                    isSuc = YES;
                }
            }
        }
    }else{
        isSuc = NO;
    }
    if (!isSuc) {
        if (metadataDic) {
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateMetadataError];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorMetadataDomain
                                                       order:-1
                                            errorElementType:kReportTemplateMetadata];
            [error.errorDetailMsgArray addObject:errorMsg];
            error.errorCode = CZReportGeneratorErrorTemplateMetadataError;
            [_errorObjArray addObject:error];
            FreeObj(error);
        }else{
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateMetadataMissing];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorMetadataDomain
                                                       order:-1
                                            errorElementType:kReportTemplateMetadata];
            [error.errorDetailMsgArray addObject:errorMsg];
            error.errorCode = CZReportGeneratorErrorTemplateMetadataMissing;
            [_errorObjArray addObject:error];
            FreeObj(error);
        }

    }
    return isSuc;
}

- (BOOL)dataValidate:(NSDictionary *)data{
    BOOL isSuc = YES;
    [_errorObjArray removeAllObjects];
    
    if (_bodyFormats && [_bodyFormats count]>0) {
        for (CZFormatBase *formatObj in _bodyFormats) {
            BOOL bodySubFormatSuc =YES;
            bodySubFormatSuc = [formatObj dataValidate:data];
            if (!bodySubFormatSuc) {
                isSuc = NO;
                [_errorObjArray addObjectsFromArray:formatObj.errorObjArray];
            }
        }
    }
    if (_headerFormat) {
        BOOL isHeaderDataValidte = [_headerFormat dataValidate:data];
        if (!isHeaderDataValidte) {
            isSuc = NO;
            [_errorObjArray addObjectsFromArray:_headerFormat.errorObjArray];
        }
    }
    if (_footerFormat) {
        BOOL isHeaderDataValidte = [_footerFormat dataValidate:data];
        if (!isHeaderDataValidte) {
            isSuc = NO;
            [_errorObjArray addObjectsFromArray:_footerFormat.errorObjArray];
        }
    }
    /// new added for avoiding intergrate the data every time when generate pdf.
    if (isSuc) {
        isSuc = [self intergrateDataInternal];
    }
    return isSuc;
}

-(BOOL)generateFormats{
    BOOL isSuc = YES;
    FreeObj(_headerFormat);
    FreeObj(_footerFormat);
    FreeObj(_bodyFormats);
    
    if (_originalTemplate) {
        BOOL isHeaderSuc = [self headerFormatsAndValidate];
        BOOL isBodySuc = [self bodyFormatsAndValidate];
        BOOL isFooterSuc = [self footerFormatsAndValidate];
        if (isHeaderSuc && isBodySuc && isFooterSuc) {
            isSuc = YES;
        }else{
            isSuc = NO;
        }
    }
    [self setPageBodyHeight];
    return isSuc;
}


- (BOOL)bodyFormatsAndValidate{
    BOOL isSuc = YES;
    NSDictionary *bodyDic = [_originalTemplate valueForKey:kReportBody];
    _bodyFormats = [[NSMutableArray alloc]initWithCapacity:5];
    if (bodyDic) {
        if (![bodyDic isKindOfClass:[NSDictionary class]]) {
            isSuc = NO;
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateElementContentNotDictionary];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                       order:-1
                                            errorElementType:kReportBody];
            [error.errorDetailMsgArray addObject:errorMsg];
            [_errorObjArray addObject:error];
            FreeObj(error);
            return isSuc;
        }
        NSArray *bodyElements = [bodyDic valueForKey:kBodyElements];
        if ([bodyElements isKindOfClass:[NSArray class]] && [bodyElements count]>0) {
            NSInteger sequence = 0;
            for (NSDictionary *dicObj in bodyElements) {
                sequence += 1;
                if (![dicObj isKindOfClass:[NSDictionary class]]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementContentNotDictionary];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                               order:sequence
                                                    errorElementType:kReportBody];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementContentNotDictionary;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                NSString *elementType = [dicObj valueForKey:kElementType];
                //assert(elementType != nil && [elementType length]>0);
                if (elementType == nil) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeMissing];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                               order:sequence
                                                    errorElementType:kReportBody];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeMissing;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                if (![elementType isKindOfClass:[NSString class]]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeNotMatch];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                               order:sequence
                                                    errorElementType:kReportBody];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeNotMatch;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                if ([elementType isKindOfClass:[NSString class]]&&[elementType length]<1) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeMissing];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                               order:sequence
                                                    errorElementType:kReportBody];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeMissing;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                
                
                
                if ([elementType isEqualToString:kLineElement]) {
                    CZLineFormat *lineFormat = [[CZLineFormat alloc] initWithContent:dicObj];
                    lineFormat.sequenceOrder = sequence;
                    lineFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:lineFormat];
                    FreeObj(lineFormat);
                    
                }else if ([elementType isEqualToString:kPieElement]){
                    CZPieFormat *pieFormat = [[CZPieFormat alloc] initWithContent:dicObj];
                    pieFormat.sequenceOrder = sequence;
                    pieFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:pieFormat];
                    FreeObj(pieFormat);
                    
                }else if ([elementType isEqualToString:kColumnElement]){
                    CZColumnFormat *columnFormat = [[CZColumnFormat alloc] initWithContent:dicObj];
                    columnFormat.sequenceOrder = sequence;
                    columnFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:columnFormat];
                    FreeObj(columnFormat);
                    
                }else if ([elementType isEqualToString:kTableElement]){
                    CZTableFormat *tableFormat = [[CZTableFormat alloc]initWithContent:dicObj];
                    tableFormat.sequenceOrder = sequence;
                    tableFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:tableFormat];
                    FreeObj(tableFormat);
                }else if ([elementType isEqualToString:kImageElement]){
                    CZImageFormat *imageFormat = [[CZImageFormat alloc]initWithContent:dicObj];
                    imageFormat.sequenceOrder = sequence;
                    imageFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:imageFormat];
                    FreeObj(imageFormat);
                }else if ([elementType isEqualToString:kTextElement]){
                    CZTextFormat *textFormat = [[CZTextFormat alloc]initWithContent:dicObj];
                    textFormat.sequenceOrder = sequence;
                    textFormat.subDomain = CZReportGeneratorBodyDomain;
                    [_bodyFormats addObject:textFormat];
                    FreeObj(textFormat);
                }
            }
        }else{
            isSuc = NO;
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateNoBodyContent];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                       order:-1
                                            errorElementType:kReportBody];
            [error.errorDetailMsgArray addObject:errorMsg];
            error.errorCode = CZReportGeneratorErrorTemplateNoBodyContent;
            [_errorObjArray addObject:error];
            FreeObj(error);
            return isSuc;
        }
    }else{
        isSuc = NO;
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                              CZReportGeneratorErrorTemplateNoBodyContent];
        CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                   order:-1
                                        errorElementType:kReportBody];
        [error.errorDetailMsgArray addObject:errorMsg];
        error.errorCode = CZReportGeneratorErrorTemplateNoBodyContent;
        [_errorObjArray addObject:error];
        FreeObj(error);
        return isSuc;
    }
    return isSuc;
}

- (BOOL)headerFormatsAndValidate{
    BOOL isSuc = YES;
    NSDictionary *headerDic = [_originalTemplate valueForKey:kReportHeader];

    if (headerDic) {
        if (![headerDic isKindOfClass:[NSDictionary class]]) {
            isSuc = NO;
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateElementContentNotDictionary];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorHeaderDomain
                                                       order:-1
                                            errorElementType:kReportHeader];
            [error.errorDetailMsgArray addObject:errorMsg];
            [_errorObjArray addObject:error];
            FreeObj(error);
            return isSuc;
        }
        NSArray *headerElementDicArray = [headerDic valueForKey:kHeaderElements];
        if (headerElementDicArray && [headerElementDicArray count]>0) {
            NSInteger sequence = 0;
            for (NSDictionary *dicObj in headerElementDicArray) {
                sequence += 1;
                if (![dicObj isKindOfClass:[NSDictionary class]]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementContentNotDictionary];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorHeaderDomain
                                                               order:sequence
                                                    errorElementType:kReportHeader];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                NSString *elementType = [dicObj valueForKey:kElementType];
                //assert(elementType != nil && [elementType length]>0);
                if (elementType && ![elementType isKindOfClass:[NSString class]]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeNotMatch];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorHeaderDomain
                                                               order:sequence
                                                    errorElementType:kReportHeader];
                    [error.errorDetailMsgArray addObject:errorMsg];
                     error.errorCode = CZReportGeneratorErrorTemplateElementTypeNotMatch;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                
                if (elementType == nil || [elementType length]<1) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeMissing];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorHeaderDomain
                                                               order:sequence
                                                    errorElementType:kReportHeader];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeMissing;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                if (![elementType isEqualToString:kTextElement] &&
                    ![elementType isEqualToString:kImageElement] &&
                    ![elementType isEqualToString:kLineElement]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementForbidInHeader];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorHeaderDomain
                                                               order:sequence
                                                    errorElementType:elementType];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementForbidInHeader;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
            }
        if (!isSuc) {
            return isSuc;
        }
        }
        
        CZHeaderFormat *headerFormate = [[CZHeaderFormat alloc]initWithContent:headerDic];
        _headerFormat = headerFormate;
        _headerFormat.subDomain = CZReportGeneratorHeaderDomain;
        _headerFormat.sequenceOrder = -1;
    }
    return isSuc;
}

- (BOOL)footerFormatsAndValidate{
    BOOL isSuc = YES;
    NSDictionary *footerDic = [_originalTemplate valueForKey:kReportFooter];
    
    if (footerDic) {
        if (![footerDic isKindOfClass:[NSDictionary class]]) {
            isSuc = NO;
            NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                  CZReportGeneratorErrorTemplateElementContentNotDictionary];
            CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorFooterDomain
                                                       order:-1
                                            errorElementType:kReportFooter];
            [error.errorDetailMsgArray addObject:errorMsg];
            error.errorCode = CZReportGeneratorErrorTemplateElementContentNotDictionary;
            [_errorObjArray addObject:error];
            FreeObj(error);
            return isSuc;
        }
        
        NSArray *footerElementDicArray = [footerDic valueForKey:kFooterElements];
        if (footerElementDicArray && [footerElementDicArray count]>0) {
            NSInteger sequence = 0;
            for (NSDictionary *dicObj in footerElementDicArray) {
                sequence += 1;
                if (![dicObj isKindOfClass:[NSDictionary class]]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementContentNotDictionary];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorFooterDomain
                                                               order:sequence
                                                    errorElementType:kReportFooter];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementContentNotDictionary;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                NSString *elementType = [dicObj valueForKey:kElementType];
                //assert(elementType != nil && [elementType length]>0);
                 if (elementType && ![elementType isKindOfClass:[NSString class]])  {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeNotMatch];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorFooterDomain
                                                               order:sequence
                                                    errorElementType:kReportFooter];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeNotMatch;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }

                
                if (elementType == nil || [elementType length]<1) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementTypeMissing];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorFooterDomain
                                                               order:sequence
                                                    errorElementType:kReportFooter];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementTypeMissing;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                
                if (![elementType isEqualToString:kTextElement] &&
                    ![elementType isEqualToString:kLineElement] &&
                    ![elementType isEqualToString:kPageInfoElement]) {
                    isSuc = NO;
                    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                                          CZReportGeneratorErrorTemplateElementForbidInFooter];
                    CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorFooterDomain
                                                               order:sequence
                                                    errorElementType:elementType];
                    [error.errorDetailMsgArray addObject:errorMsg];
                    error.errorCode = CZReportGeneratorErrorTemplateElementForbidInFooter;
                    [_errorObjArray addObject:error];
                    FreeObj(error);
                    continue;
                }
                
            }
            if (!isSuc) {
                return isSuc;
            }
        }
        
        CZFooterFormat *footerFormate = [[CZFooterFormat alloc]initWithContent:footerDic];
        _footerFormat = footerFormate;
        _footerFormat.subDomain = CZReportGeneratorFooterDomain;
        _footerFormat.sequenceOrder = -1;
    }
    return isSuc;
}


/** Return the dictionary array that has parameters.Special for footer text,we should add a
     key for indicating text is used in footer part,this can be used in TemplateManagment Project.
**/

- (NSArray *)needValueElements{
    NSMutableArray *elements = [[[NSMutableArray alloc]initWithCapacity:5] autorelease];
     if ([self generateFormats]) {
         if (_headerFormat && [_headerFormat.headerElementFormats count]>0) {
             for (CZFormatBase *formatObj in _headerFormat.headerElementFormats) {
                 if (!formatObj.isAssigned) {
                     [elements addObject:formatObj.elementDic];
                 }
             }
         }
         if (_footerFormat && [_footerFormat.footerElementFormats count]>0) {
             for (CZFormatBase *formatObj in _footerFormat.footerElementFormats) {
                 if (!formatObj.isAssigned) {
                     NSMutableDictionary *footerTextDic = [[NSMutableDictionary alloc]
                                                           initWithDictionary:formatObj.elementDic];
                     [footerTextDic setValue:[NSNumber numberWithBool:YES]
                                      forKey:kIsInFooter];
                     [elements addObject:footerTextDic];
                     FreeObj(footerTextDic);
                 }
             }
         }
         if (_bodyFormats && [_bodyFormats count]>0) {
             for (CZFormatBase *formatObj in _bodyFormats) {
                 if (!formatObj.isAssigned) {
                     [elements addObject:formatObj.elementDic];
                 }
             }
         }
    }
    return elements;
}


- (void)setPageBodyHeight{
    CGFloat pageBodeHeight = 0.0;
    CGRect headerRct = CGRectZero;
    CGRect footerRct = CGRectZero;
    
    CGSize pageSize = CGSizeZero;
    NSDictionary *meadtateDic = [_originalTemplate valueForKey:kReportTemplateMetadata];
    if (meadtateDic && [meadtateDic isKindOfClass:[NSDictionary class]]) {
        NSDictionary *sizeDic = [meadtateDic valueForKey:kPageSize];
        if (sizeDic && [sizeDic isKindOfClass:[NSDictionary class]]) {
            pageSize = [CZDictionaryTool dictionay2Size:sizeDic];
        }
    }
    if (pageSize.width * pageSize.height < (kPageWidth * kPageHeight)) {
        pageSize = CGSizeMake(kPageWidth, kPageHeight);
    }
    
    if (_headerFormat) {
        headerRct = [CZDictionaryTool dictionay2Frame:[_headerFormat.elementDic valueForKey:kFrame]];
    }
    if (_footerFormat) {
        footerRct = [CZDictionaryTool dictionay2Frame:[_footerFormat.elementDic valueForKey:kFrame]];
    }
    pageBodeHeight = pageSize.height - headerRct.size.height - footerRct.size.height;
    [[NSUserDefaults standardUserDefaults] setValue:@(pageBodeHeight)
                                             forKey:kPageBodyHeight];
    
    [[NSUserDefaults standardUserDefaults] setValue:@(pageSize.width)
                                             forKey:kPageBodyWidth];
    [[NSUserDefaults standardUserDefaults]synchronize];
}


- (void)FilterErrorElements{
    if (_headerFormat) {
        [_headerFormat FilterErrorElements];
    }
    if (_footerFormat) {
        [_footerFormat FilterErrorElements];
    }
    if (!_bodyFormats || [_bodyFormats count]<1) {
        return;
    }
    NSMutableIndexSet *sets = [[NSMutableIndexSet alloc]init];
    if ([_bodyFormats count]>0) {
        NSUInteger elmentCount = [_bodyFormats count];
        for (NSUInteger i = 0;i<elmentCount; i++) {
            CZFormatBase *format = [_bodyFormats objectAtIndex:i];
         if(!format.canRender){
             [sets addIndex:i];
             if (i+1 < elmentCount) {
                 CZFormatBase *nextFormat = [_bodyFormats objectAtIndex:i+1];
                 NSDictionary *preFrame = [format.elementDic valueForKey:kFrame];
                 NSDictionary *curFrame = [nextFormat.elementDic valueForKey:kFrame];
                 CGFloat preX = [[preFrame valueForKey:@"x"] floatValue];
                 CGFloat curX = [[curFrame valueForKey:@"x"] floatValue];
                 CGFloat useX = preX + curX;
                 NSMutableDictionary *newFrameDic = [[NSMutableDictionary alloc]initWithDictionary:curFrame];
                 [newFrameDic setValue:@(useX) forKey:@"x"];
                 [nextFormat setRelativeFrameDic:newFrameDic];
                 [newFrameDic release];
                 newFrameDic = nil;
             }
         }
        }
    }
    if([sets count]>0){
        [_bodyFormats removeObjectsAtIndexes:sets];
    }
    [sets release];
    return;
}

- (BOOL)intergrateDataInternal{
    BOOL isSuc = YES;
    [self FilterErrorElements];
    
    if ([_bodyFormats count] < 1) {
        NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:
                              CZReportGeneratorErrorTemplateNoBodyContent];
        CZError *error = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                   order:-1
                                        errorElementType:kReportBody];
        [error.errorDetailMsgArray addObject:errorMsg];
        error.errorCode = CZReportGeneratorErrorTemplateNoBodyContent;
        [_errorObjArray addObject:error];
        [error release];
        isSuc = NO;
        return isSuc;
    }
    
    CZLog(@"the original template is %@",_originalTemplate);
    NSMutableDictionary *beforeTempalte = [_originalTemplate mutableDeepCopy];
    NSMutableDictionary *bodyDic = [_originalTemplate valueForKey:kReportBody];
    
    NSMutableDictionary *newBodyDic = [NSMutableDictionary dictionaryWithDictionary:bodyDic];
    NSMutableArray *bodyDicArray = [[NSMutableArray alloc]initWithCapacity:5];
    for (CZFormatBase *formatObj in _bodyFormats) {
        if ([formatObj isKindOfClass:[CZTableFormat class]]) {
            CZTableFormat *parentTable = (CZTableFormat *)formatObj;
            if (parentTable.isHasFilter) {
                CZTableFormat *filterTable = parentTable.filterTable;
                if (filterTable) {
                    [bodyDicArray addObject:filterTable.elementDic];
                }
            }else{
                [bodyDicArray addObject:formatObj.elementDic];
            }
            
        }else{
            [bodyDicArray addObject:formatObj.elementDic];
        }
    }
    [newBodyDic setObject:bodyDicArray forKey:kBodyElements];
    [bodyDicArray release];
    CZLog(@"the new body dic is %@",newBodyDic);
    [beforeTempalte setObject:newBodyDic forKey:kReportBody];
    
    if (_headerFormat) {
        ///note: the header element dictionary should alread be assigned
        if (isSuc) {
            [beforeTempalte setObject:_headerFormat.elementDic forKey:kReportHeader];
        }
        //CZLog(@"the header dic is %@",_headerFormat.elementDic);
    }
    if (_footerFormat) {
        ///note: the footer element dictionary should alread be assigned
        if (isSuc) {
            [beforeTempalte setObject:_footerFormat.elementDic forKey:kReportFooter];
        }
        //CZLog(@"the footer dic is %@",_footerFormat.elementDic);
    }
    self.intergrateDic = beforeTempalte;
    CZLog(@"the intergrate dic is %@",beforeTempalte);
    [beforeTempalte release];
    return isSuc;
}

-(NSMutableArray *)errorObjArray{
    if (_errorObjArray && [_errorObjArray count]>0) {
        for (NSUInteger i = 0; i<[_errorObjArray count]; i++) {
            CZError * curError = [_errorObjArray objectAtIndex:i];
            NSRange removeRange = NSMakeRange(i+1, [_errorObjArray count]-(i+1));
            [ _errorObjArray removeObject:curError inRange:removeRange];
        }
    }
    return _errorObjArray;
}

- (void)dealloc{
    FreeObj(_intergrateDic);
    FreeObj(_originalTemplate);
    FreeObj(_footerFormat);
    FreeObj(_headerFormat);
    FreeObj(_bodyFormats);
    FreeObj(_errorObjArray);
    [super dealloc];
}

@end
