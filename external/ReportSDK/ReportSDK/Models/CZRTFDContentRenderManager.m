//
//  CZRTFContentRenderManager.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFDContentRenderManager.h"
#import "CZPieRender.h"
#import "CZDictionaryTool.h"
#import "CZGlobal.h"
#import "CZDictionaryTool.h"
#import "CZImageRender.h"
#import "CZColumnRender.h"
#import "CZColumnItem.h"
#import "CZPieRender.h"
#import "CZLineRender.h"
#import "CZTableRender.h"
#import "CZPDFPage.h"
#import "CZDictionaryTool.h"
#import "CZTableSeparater.h"
#import "CZTextSeparater.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFPreviewConfig.h"

#define kParserWeight 117
#define kRenderWeight 2885
#define kWriteBufferWeight 329
#define kWriteToFileWeight 50

#define kImageRenderWeightPercent 10
#define kNoImageRenderWeightPercent 1


@interface CZRTFDContentRenderManager (){
    CZHeaderRender  *_headerRender;
    CZFooterRender  *_footerRender;
    NSMutableArray  *_renders;
    NSMutableData   *_RTFDataBuffer;
}

- (void)appendStringToRTFBuffer:(NSString *)aString;

/**
 * @brief resort the element order,avoid the order is same.At the same time make the x Value is absolute,y Value is relative.
 */
- (void)reSortOrder;

@end

@implementation CZRTFDContentRenderManager


- (id)initWithRenderContent:(NSDictionary *)aContentDic{
    self = [super initWithRenderContent:aContentDic];
    if (self) {
        _renders = [[NSMutableArray alloc]initWithCapacity:5];
        _RTFDataBuffer = [[NSMutableData alloc]initWithCapacity:1024];
        CZLogSize(_pageSize);
    }
    return self;
}

- (void)updateGeneratePercentage:(NSNumber *)percent{
    CGFloat addPercent = [percent floatValue];
    currentProgress += addPercent;
    CGFloat percentage = currentProgress/(kParserWeight + kWriteBufferWeight +
                                          kRenderWeight + kWriteToFileWeight);
    if (percentage >1.f) {
        percentage = 1.f;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(notifyRenderProgress:currentPercentage:destination:)]) {
        ReportEngineReportType type = [[CZRTFPreviewConfig sharedInstance] isForRTF] == YES? ReportEngineReportTypeRTF:ReportEngineReportTypeRTFD;
        
        [self.delegate notifyRenderProgress:type
                          currentPercentage:percentage
                                destination:_reportDestination];
    }
    
}

- (void)notifyGeneratePercentage:(CGFloat)percent{
    NSNumber *percentNum = [NSNumber numberWithFloat:percent];
    [self performSelector:@selector(updateGeneratePercentage:)
                 onThread:_notifyThread withObject:percentNum
            waitUntilDone:YES];
}

- (void)notifyErrorWithCode:(NSNumber *)errorCode{
    NSInteger intCode = [errorCode integerValue];
    NSError *renderError = nil;
    renderError = [CZReportGeneratorErrorHandler createErrorWithCode:intCode];
    if (self.delegate && [self.delegate respondsToSelector:@selector(notifyRenderFailWithError:)]) {
        [self.delegate notifyRenderFailWithError:renderError];
    }
}

- (void)notifyError:(NSInteger)errorCode{
    NSNumber *errorNum = [NSNumber numberWithInteger:errorCode];
    [self performSelector:@selector(notifyErrorWithCode:)
                 onThread:_notifyThread
               withObject:errorNum
            waitUntilDone:YES];
}


-(void)startGenerate{
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [super startGenerate];
    [[CZRTFFontColorRegistTable sharedInstance] clearFonts];
    /// first begin paser the content.
    [self paserContent];
    /// convert element to RTF grammar string.
    [self renderPages];
    /// write the elements grammar string to buffer.
    [self writeToBuffer];
    /// write buffer to file.
    [self writeToFile];

    [pool release];
    
}

- (void)writeToFile{
    if (_RTFDataBuffer && [_RTFDataBuffer length]>0 &&
        ![[NSThread currentThread] isCancelled]) {
     // BOOL isWriteSuc = [_RTFDataBuffer writeToFile:_reportDestination atomically:YES];
        @synchronized(self) {
            NSError *error = nil;
            BOOL isWriteSuc = NO;
            if (![[CZRTFPreviewConfig sharedInstance] isForRTF]) {
                CZLog(@"This is write for RTFD!");
                NSString *tempRTF = [[CZRTFPreviewConfig sharedInstance] subRTFFilePath];
                if (tempRTF && [tempRTF length]>0) {
                    isWriteSuc = [_RTFDataBuffer writeToFile:tempRTF
                                                     options:NSDataWritingAtomic
                                                       error:&error];
                }
            }else{
                if (_reportDestination && [_reportDestination length]>0) {
                    isWriteSuc = [_RTFDataBuffer writeToFile:_reportDestination
                                                     options:NSDataWritingAtomic
                                                       error:&error];
                }
            }
            
            if (isWriteSuc) {
                [self notifyGeneratePercentage:kWriteToFileWeight];
            }else{
                [self notifyError:CZReportGeneratorErrorWriteRTFToFileUnexpected];
            }
        }
    }
}


- (void)writeToBuffer{
    /// write the rtf version, language,character map
    [self appendStringToRTFBuffer:@"{\\rtf1\\ansi\\ansicpg936\\deff0\\deflang1033\\deflangfe2052"];
    
    /**add some RTF paper and header, footer layout information, we can set the paper size,page 
     * size,heaer margin, and some other layout information here.
     */
    [self appendStringToRTFBuffer:PaperLayOut];
    [self appendStringToRTFBuffer:kRTFHeaderHeight];
    
    /// write the rtf font table
    [self appendStringToRTFBuffer:[[CZRTFFontColorRegistTable sharedInstance] fontTableRTFDes]];
    
    /// write the rtf color table
    [self appendStringToRTFBuffer:[[CZRTFFontColorRegistTable sharedInstance] fontColorRTFDes]];

    /// write the header rtf grammar.
    [self appendHeaderToRTFBuffer];
    
    /// write the footer rtf grammar.
    [self appendFooterToRTFBuffer];
    
    /// write the rtf elements.
    [self appendElementsToRTFBuffer];
    
    //[self appendStringToRTFBuffer:@"}"];
    [self appendEndSpaceToRTFBuffer];

}

- (void)paserContent{
    [self notifyGeneratePercentage:0];
    [self paserReportHeader];
    [self paserReportFooter];
    [self paserReportBody];
    [self notifyGeneratePercentage:kParserWeight];
}

- (void)renderPages{
    CGFloat headerCount = 0;
    CGFloat footerCount = 0;
    CGFloat bodyCount = 0;
    if (_headerRender) {
        headerCount = [_headerRender.headerElements count];
    }
    if (_footerRender) {
        footerCount = [_footerRender.footerElements count];
    }
    for (CZRenderBase *renderObj in _renders) {
        if([[NSThread currentThread] isCancelled]){
            [NSThread exit];
        }
        if ([renderObj isKindOfClass:[CZImageRender class]] ||
            [renderObj isKindOfClass:[CZPieRender class]]||
            [renderObj isKindOfClass:[CZColumnItem class]]) {
            bodyCount += kImageRenderWeightPercent;
        }else{
            bodyCount += kNoImageRenderWeightPercent;
            
           
        }
    }
    [[CZRTFFontColorRegistTable sharedInstance] setPreRenderObj:nil];
    CGFloat totalCount = headerCount  + footerCount  + bodyCount;
    if (_headerRender && ![[NSThread currentThread] isCancelled]) {
        [_headerRender rtfRender];
        [self notifyGeneratePercentage:kRenderWeight * headerCount/totalCount];
    }
    if (_footerRender && ![[NSThread currentThread] isCancelled]) {
        [_footerRender rtfRender];
        [self notifyGeneratePercentage:kRenderWeight * footerCount/totalCount];
    }
    if (_renders && [_renders count]>0 && ![[NSThread currentThread] isCancelled]) {
        //[_renders makeObjectsPerformSelector:@selector(rtfRender)];
        for (NSInteger i =0; i< [_renders count]; i++) {
            if([[NSThread currentThread] isCancelled]){
                [NSThread exit];
            }
            CZRenderBase *renderObj = [_renders objectAtIndex:i];
            [renderObj rtfRender];
            [[CZRTFFontColorRegistTable sharedInstance] setPreRenderObj:nil];
            if ([renderObj isKindOfClass:[CZImageRender class]] ||
                [renderObj isKindOfClass:[CZPieRender class]]||
                [renderObj isKindOfClass:[CZColumnItem class]]) {
            [self notifyGeneratePercentage:kRenderWeight * kImageRenderWeightPercent/totalCount];
            }else{
                [self notifyGeneratePercentage:kRenderWeight * kNoImageRenderWeightPercent/totalCount];
                
                if ([renderObj isKindOfClass:[CZTextRender class]]) {
                    [[CZRTFFontColorRegistTable sharedInstance] setPreRenderObj:renderObj];
                }
            }
        }
    }
}

- (void)appendElementsToRTFBuffer{
    if (_renders && [_renders count]>0 && ![[NSThread currentThread] isCancelled]) {
        CGFloat writeBufferWeight = [self adjustWriteBufferWeight];
        for (NSInteger i =0; i< [_renders count]; i++) {
            CZRenderBase *renderObj = [_renders objectAtIndex:i];
            [self appendStringToRTFBuffer:renderObj.RTFAttributeStr];
            [self notifyGeneratePercentage:(ceilf(writeBufferWeight/(CGFloat)([_renders count] + 1)))];
        }
    }
}

- (void)appendEndSpaceToRTFBuffer{
    CGFloat writeBufferWeight = [self adjustWriteBufferWeight];
    [self appendStringToRTFBuffer:@"}"];
    [self notifyGeneratePercentage:writeBufferWeight];
}

- (CGFloat)adjustWriteBufferWeight{
    CGFloat weight = (kParserWeight + kWriteBufferWeight +
                      kRenderWeight - currentProgress);
    //assert(weight >= 0);
    if (weight <0) {
        weight = 0.f;
    }
    return weight;
}

- (void)appendHeaderToRTFBuffer{
    if (_headerRender) {
        [self appendStringToRTFBuffer:_headerRender.RTFAttributeStr];
    }
}

- (void)appendFooterToRTFBuffer{
    if (_footerRender) {
        [self appendStringToRTFBuffer:_footerRender.RTFAttributeStr];
    }
}

- (void)paserReportHeader{
    NSDictionary *reportHeaderDic = [_renderContent valueForKey:kReportHeader];
    if (reportHeaderDic) {
        CGRect frame = [CZDictionaryTool dictionay2Frame:[reportHeaderDic valueForKey:kFrame]];
        _headerRender = [[CZHeaderRender alloc]initWithContent:reportHeaderDic frame:frame];
    }
}

- (void)paserReportFooter{
    NSDictionary *reportFooterDic = [_renderContent valueForKey:kReportFooter];
    if (reportFooterDic) {
        CGRect frame = [CZDictionaryTool dictionay2Frame:[reportFooterDic valueForKey:kFrame]];
        _footerRender = [[CZFooterRender alloc]initWithContent:reportFooterDic frame:frame];
    }
}

- (void)paserReportBody{
    NSDictionary *reportBodyDic = [_renderContent valueForKey:kReportBody];
    if (!reportBodyDic) {
        return ;
    }
    NSArray *bodyElements = [reportBodyDic valueForKey:kBodyElements];
    assert([bodyElements count]>0);
    for (NSDictionary *dicObj in bodyElements) {
        NSString *elementType = [dicObj valueForKey:kElementType];
        assert(elementType != nil && [elementType length]>0);
        CGRect frame = [CZDictionaryTool dictionay2Frame:[dicObj valueForKey:kFrame]];
        
        if ([elementType isEqualToString:kLineElement]) {
            CZLineRender *lineRender = [[CZLineRender alloc] initWithContent:dicObj frame:frame];
            [_renders addObject:lineRender];
            [lineRender release];
            lineRender = nil;
            
        }else if ([elementType isEqualToString:kPieElement]){
            CZPieRender *pieRender = [[CZPieRender alloc]initWithContent:dicObj frame:frame];
            [_renders addObject:pieRender];
            [pieRender release];
            pieRender = nil;
            
        }else if ([elementType isEqualToString:kColumnElement]){
            CZColumnRender *columnRender = [[CZColumnRender alloc]initWithContent:dicObj frame:frame];
            [_renders addObject:columnRender];
            [columnRender release];
            columnRender = nil;
            
        }else if ([elementType isEqualToString:kTableElement]){
            CZTableRender *tableRender = [[CZTableRender alloc]initWithContent:dicObj frame:frame];
            [_renders addObject:tableRender];
            [tableRender release];
            tableRender = nil;
            
        }else if ([elementType isEqualToString:kImageElement]){
            CZImageRender *imageRender = [[CZImageRender alloc]initWithContent:dicObj frame:frame];
            [_renders addObject:imageRender];
            [imageRender release];
            
        }else if ([elementType isEqualToString:kTextElement]){
            CZTextRender *textRender = [[CZTextRender alloc]initWithContent:dicObj frame:frame];
            [_renders addObject:textRender];
            [textRender release];
            textRender = nil;
        }
    }
    [self reSortOrder];
}


- (void)reSortOrder{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_renders sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    CGFloat xValue = 0.f;
    for (NSInteger i = 0; i< [_renders count]; i++) {
        CZRenderBase *renderObj = [_renders objectAtIndex:i];
        if (i == 0) {
            CGRect rederRct = renderObj.renderFrame;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
            xValue = renderObj.renderFrame.origin.x;

        }else{
            xValue = xValue + renderObj.renderFrame.origin.x;
            CGRect rederRct = renderObj.renderFrame;
            rederRct = CGRectMake(xValue, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            renderObj.renderFrame = rederRct;
            CZLogRect(rederRct);
        }
        renderObj.renderOrder = renderObj.renderOrder + i*2;
    }
}

/// note: if the string is Chinese ,should be converted!
- (void)appendStringToRTFBuffer:(NSString *)aString{
    if([[NSThread currentThread] isCancelled]){
        [NSThread exit];
    }
    if (aString && [aString length]>0) {
        //CZLog(@"the append string is %@",aString);
        [_RTFDataBuffer appendData:[aString dataUsingEncoding:NSASCIIStringEncoding]];
    }
}

- (void)dealloc{
    self.delegate = nil;
    FreeObj(_renders);
    FreeObj(_RTFDataBuffer);
    FreeObj(_headerRender);
    FreeObj(_footerRender);
    [super dealloc];
}

@end
