//
//  CZRenderBase.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

@implementation CZRenderBase

- (id)initWithConfig:(NSDictionary *)configDic{
    self = [super init];
    if (self) {
        _currentContext = UIGraphicsGetCurrentContext();
    }
    return self;
}

@end
