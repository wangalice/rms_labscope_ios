//
//  CZRenderBase.m
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"

@implementation CZRenderBase

@synthesize renderContent = _renderContent;
@synthesize renderFrame = _renderFrame;
@synthesize relativeFrame = _relativeFrame;
@synthesize renderOrder = _renderOrder;
@synthesize backgroundColor = _backgroundColor;
@synthesize canBreak = _canBreak;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super init];
    if (self) {
        self.renderContent = contentDic;
        _renderFrame = aFrame;
        NSDictionary *relativeFrameDic = [contentDic valueForKey:kFrame];
        _relativeFrame = [CZDictionaryTool dictionay2Frame:relativeFrameDic];
        CZLogRect(_relativeFrame);
        CZLog(@"compare the relative and reder frame");
        _renderOrder = [[self.renderContent valueForKey:kRenderOrder] integerValue];
    }
    return self;
}

- (CGSize)preRenderLayout{
    return CGSizeZero;
}

/// render the graphic to current context,all render element should call this method for rendering.
- (void)render{
    CZLog(@"render begin of class %@",[self debugDescription]);
    if (self.renderFrame.size.height == 0 || self.renderFrame.size.width == 0) {
         CZLogRect(self.renderFrame);
         CZLog(@"render end of class %@",[self debugDescription]);
        return;
    }
    _currentContext = UIGraphicsGetCurrentContext();
    [self preRenderLayout];
    //render background
    if (_backgroundColor) {
    UIGraphicsPushContext(_currentContext);
    CGContextSetRGBFillColor(_currentContext, 1.0f, 1.0f, 0.40f, 0.2f);  // white color
    CGContextFillRect(_currentContext, self.renderFrame);
    CZLogRect(_renderFrame);
    UIGraphicsPopContext();
    _currentContext = UIGraphicsGetCurrentContext();

    }
    CZLog(@"render end of class %@",[self debugDescription]);
    return;
}

- (void)registerRTFColor:(CZRTFFontColorRegistTable *)colorTable {
    // empty function; sub-Class can override.
}

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    if (stream != _RTFOutputStream) {
        if (_RTFOutputStream) {
            [_RTFOutputStream release];
        }
        _RTFOutputStream = [stream retain];
    }
}

- (void)printOrder{
    CZLog(@"render order is %ld",(long)self.renderOrder);
}

-(void)dealloc{
    FreeObj(_renderContent);
    FreeObj(_backgroundColor);
    FreeObj(_RTFOutputStream);
    [super dealloc];
}

@end
