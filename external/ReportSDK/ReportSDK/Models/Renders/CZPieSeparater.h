//
//  CZPieSeparater.h
//  ReportSDK
//
//  Created by Ralph Jin on 2/18/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRenderSeperater.h"
#import "CZPieRender.h"

@interface CZPieSeparater : NSObject <CZRenderSeperater>

/** initialized method used the separate the render with multi pie data to
 * several renders that render one pie data.
 */
- (id)initWithOriginalRender:(CZPieRender *)originalRender;

@end
