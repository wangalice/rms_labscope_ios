//
//  CZImageSeperator.m
//  ReportSDK
//
//  Created by Ralph Jin on 1/16/14.
//  Copyright (c) 2014 Bleum. All rights reserved.
//

#import "CZImageSeparator.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZImageRender.h"

@interface CZImageSeparater ()

@property (nonatomic, retain) NSMutableArray *subImageRenders;
@property (nonatomic, retain, readonly) CZImageRender *originalImageRender;

@end

@implementation CZImageSeparater

- (id)initWithImageRender:(CZImageRender *)imageRender {
    self = [super init];
    if (self) {
        _originalImageRender = [imageRender retain];
        
        _subImageRenders = [[NSMutableArray alloc] initWithCapacity:3];
    }
    return self;
}

- (void)dealloc {
    [_originalImageRender release];
    [_subImageRenders release];
    
    [super dealloc];
}

- (void)separateMultiDataRender {
    
    NSString *keyPath = [NSString stringWithFormat:@"%@.%@.%@", kValue, kImageFile, kValues];
    
    NSUInteger i = 0;
    for (NSDictionary *image in self.originalImageRender.images) {
        NSMutableDictionary *content = [self.originalImageRender.renderContent mutableDeepCopy];
        [content setValue:@[image] forKeyPath:keyPath];
        
        CZImageRender *imageRender = [self generateImageRender:content first:(i == 0)];
        [content release];
        
        [self.subImageRenders addObject:imageRender];
        i++;
    }
}

- (NSArray *)subRenders {
    return _subImageRenders;
}

#pragma mark - private methods

- (CZImageRender *)generateImageRender:(NSDictionary *)content first:(BOOL)first {
    CZImageRender *newImageRender = [[CZImageRender alloc] initWithContent:content
                                                                     frame:_originalImageRender.relativeFrame];
    if (!first) {
        CGRect frame = newImageRender.relativeFrame;
        frame.origin = CGPointZero;
        newImageRender.relativeFrame = frame;
    }
    
    newImageRender.renderMode = kCZImageRenderModePendding;
    newImageRender.renderOrder = self.originalImageRender.renderOrder;

    return [newImageRender autorelease];
    
}

@end
