//
//  CZTableColumn.h
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZTableColumn : NSObject{
    NSDictionary    *_tableColumnDic;
    NSMutableArray  *_tableColumnCells;
    
    NSInteger       _order;
    NSString        *_title;
    NSString        *_tableColumnID;
//    CGRect          _frame;
    BOOL            _isNum;
}

@property (nonatomic,retain)   NSDictionary    *tableColumnDic;
@property (nonatomic,retain)   NSMutableArray  *tableColumnCells;
@property (nonatomic)          NSInteger       order;
@property (nonatomic,retain)   NSString        *title;
@property (nonatomic,retain)   NSString        *tableColumnID;
//@property (nonatomic)          CGRect          frame;
@property (nonatomic)          NSInteger       cellNum;
@property (nonatomic)          BOOL            isNum;
@property (nonatomic, assign, readonly) NSUInteger    precision;  // max precision of column

/// init method.
- (id)initWithDic:(NSDictionary *)tableColumnDic;

/// add the cells to column
- (void)addColumnCells:(NSArray *)tableContentArray withFontDic:(NSDictionary *)fontDic;

/// sort the cell order by ascending.
- (void)ascendingSortCellOrder;

/// set column order.
- (void)setCellDrawColumnOrder:(NSInteger) aOrder;

/// return array of UIColor, which table column will use.
- (NSArray *)useColors;

@end
