//
//  CZTableEvaluate.m
//  ReportSDK
//
//  Created by Johnny on 3/1/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableEvaluate.h"
#import "NSOutputStream+String.h"
#import "CZTableColumnCellObj.h"
#import "CZDictionaryTool.h"
#import "CZNumberTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFTool.h"

@implementation CZTableEvaluate
@synthesize columnNum;
@synthesize evaluteValue = _evaluteValue;
@synthesize tableColumn = _tableColumn;
@synthesize evaluteTitle = _evaluteTitle;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        NSString* aDicType = [contentDic valueForKey:kElementType];
        assert([aDicType isEqualToString:kTableEvaluationElement]);
        self.columnNum = [[self.renderContent valueForKey:kColumnNum] integerValue];
        NSString* dicTitle = [self.renderContent valueForKey:kEvaluationTitle];
        _evaluteTitle = [NSString stringWithFormat:@"%@",dicTitle];
    
        NSString *dicMethod = [self.renderContent valueForKey:kEvaluationMethod];
        _method =[NSString stringWithFormat:@"%@",dicMethod];
    
        CGSize aSize = [self preRenderLayout];
        //[self performSelectorOnMainThread:@selector(preRenderLayout) withObject:nil waitUntilDone:YES];
        //CGSize aSize = _realSize;
        self.renderFrame = CGRectMake(_renderFrame.origin.x,
                                      _renderFrame.origin.y,
                                      aSize.width,
                                      aSize.height);
        //self.backgroundColor = [UIColor colorWithRed:0.2 green:0.6 blue:0.8 alpha:0.85];
    }
    return self;
}

- (CGSize)preRenderLayout{
   [super preRenderLayout];
    
    NSDictionary *fontDic = [self.renderContent valueForKey:kFont];
    if (!fontDic) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }    CGSize aSize  = CGSizeZero;

    if (_evaluteTitle && [_evaluteTitle length]>0) {
        /*
        aSize = [_evaluteTitle sizeWithFont:font
                                   forWidth:self.renderFrame.size.width
                              lineBreakMode:NSLineBreakByWordWrapping];
         */
        aSize = [CZDictionaryTool textSize:_evaluteTitle
                                   useFont:font
                            constraintSize:CGSizeMake(self.renderFrame.size.width, 0)];
        
    }

    return aSize;
}

- (void)setTableColumn:(CZTableColumn *)tableColumn{
    [tableColumn retain];
    if (_tableColumn) {
        FreeObj(_tableColumn);
    }
    _tableColumn = tableColumn;
    assert(_tableColumn.cellNum>0);
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_floatContent"
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                          count:1];
    [_tableColumn.tableColumnCells sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    //[_tableColumn.tableColumnCells makeObjectsPerformSelector:@selector(printValue)];
    if (_method) {
        if ([_method isEqualToString:kAVERAGE]) {
            double total = 0.f;
            int cellNum = 0;
            for (CZTableColumnCellObj *cellObj in _tableColumn.tableColumnCells) {
                if (cellObj.isNum) {
                    total = total + cellObj.floatContent;
                    cellNum ++;
                }
            }
            self.evaluteValue = (cellNum != 0) ? total / cellNum : 0;
            
        }else if([_method isEqualToString:kMAX]){
            for (CZTableColumnCellObj *cellObj in [_tableColumn.tableColumnCells reverseObjectEnumerator]) {
                if (cellObj.isNum) {
                    self.evaluteValue = cellObj.floatContent;
                    break;
                }
            }
        }else if ([_method isEqualToString:kMIN]){
            for (CZTableColumnCellObj *cellObj in _tableColumn.tableColumnCells) {
                if (cellObj.isNum) {
                    self.evaluteValue = cellObj.floatContent;
                    break;
                }
            }
        }else if ([_method isEqualToString:kSum]){
            CZTableColumnCellObj *minObj = [_tableColumn.tableColumnCells objectAtIndex:0];
             double total = 0.f;
            if (minObj.isNum) {
                for (NSUInteger i = 0; i< [_tableColumn.tableColumnCells count] -1; i++) {
                    CZTableColumnCellObj *Obj = [_tableColumn.tableColumnCells objectAtIndex:i];
                    if (Obj.isNum) {
                        total = total + Obj.floatContent;
                    }
                }
            }else{
                for (NSUInteger i = 1; i< [_tableColumn.tableColumnCells count]; i++) {
                    CZTableColumnCellObj *Obj = [_tableColumn.tableColumnCells objectAtIndex:i];
                    if (Obj.isNum) {
                        total = total + Obj.floatContent;
                    }
                }
            }
            self.evaluteValue =total;
        } else if ([_method isEqualToString:kSTDEV] || [_method isEqualToString:kSTDEVP]) {
            double sum = 0.f;
            NSUInteger count = 0;
            for (CZTableColumnCellObj *cellObj in _tableColumn.tableColumnCells) {
                if (cellObj.isNum) {
                    sum = sum + cellObj.floatContent;
                    count++;
                }
            }
            
            double deviation = 0;
            if (count > 1) {
                double average = sum / count;
                double divider = count - 1;
                if ([_method isEqualToString:kSTDEVP]) {
                    divider = count;
                }
                
                for (CZTableColumnCellObj *cellObj in _tableColumn.tableColumnCells) {
                    if (cellObj.isNum) {
                        double temp = (cellObj.floatContent - average);
                        deviation = deviation + temp * temp;
                    }
                }
                
                deviation = sqrt(deviation / divider);
            }
            
            self.evaluteValue = deviation;
        }
    }
}

- (void)setEvaluteValue:(double)evaluteValue{
     _evaluteValue = evaluteValue;
     //NSString *valueStr = [NSString stringWithFormat:@"%.3f",_evaluteValue];
     NSUInteger precision = _tableColumn ? _tableColumn.precision : 3U;
     NSString *valueStr = [CZNumberTool localizedStringFromFloat:_evaluteValue
                                                       precision:precision];
     
     
     NSString* dicTitle = [self.renderContent valueForKey:kEvaluationTitle];
     _evaluteTitle = [NSString stringWithFormat:@"%@",dicTitle];
     _evaluteTitle = [_evaluteTitle stringByReplacingOccurrencesOfString:kReplaceValue
                                                              withString:valueStr];
     CZLog(@"the evalute title i %@",_evaluteTitle);
}

- (void)render{
    [super render];
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];

    CGFloat r = [[color valueForKey:kRedColor] floatValue];
    CGFloat g = [[color valueForKey:kGreenColor] floatValue];
    CGFloat b = [[color valueForKey:kBlueColor] floatValue];
    CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
    
    CGContextSetRGBFillColor(_currentContext, r, g, b, a);
    
    CZLog(@"the evalute title is %@",_evaluteTitle);
    CZLogRect(self.renderFrame);
    

    /// begin: new added for evaluate title is too long
    CGSize lengthSize = [CZDictionaryTool textSize:_evaluteTitle useFont:font];
    NSInteger lineNum = ceilf(_renderFrame.size.height/lengthSize.height);
    if (lineNum >= 3) {
        lineNum = lineNum -1;
    }
    if (lengthSize.width > _renderFrame.size.width *lineNum) {
        NSInteger showLength = ceilf([_evaluteTitle length] * _renderFrame.size.width * lineNum /lengthSize.width);
        if (showLength >5) {
            NSRange aRange = NSMakeRange(0, showLength-5);
            NSString *textStr = [_evaluteTitle substringWithRange:aRange];
            _evaluteTitle = [textStr stringByAppendingString:@"..."];
        }
    }
    /// end: new added for evaluate title is too long
    
    [_evaluteTitle drawInRect:self.renderFrame
                     withFont:font
                lineBreakMode:NSLineBreakByCharWrapping
                    alignment:NSTextAlignmentLeft];
}

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *evaluateDic = self.renderContent;
    
    NSDictionary *fontDic = [evaluateDic valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    
    /// begin: new added for evaluate title is too long
    CGSize lengthSize = [CZDictionaryTool textSize:_evaluteTitle useFont:font];
    NSInteger lineNum = ceilf(_renderFrame.size.height/lengthSize.height);
    if (lineNum >= 3) {
        lineNum = lineNum -1;
    }
    if (lengthSize.width > _renderFrame.size.width *lineNum) {
        NSInteger showLength = ceilf([_evaluteTitle length] * _renderFrame.size.width * lineNum /lengthSize.width);
        if (showLength >5) {
            NSRange aRange = NSMakeRange(0, showLength-5);
            NSString *textStr = [_evaluteTitle substringWithRange:aRange];
            _evaluteTitle = [textStr stringByAppendingString:@"..."];
        }
    }
    /// end: new added for evaluate title is too long
    
    
    
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
 
    NSString *textAlignment = kAlignmentLeft;
    NSString *evaluateOffset = nil;
    if ([textAlignment isEqualToString:@"\\ql"]) {
        evaluateOffset = textAlignment;
    }
    
    if (evaluateOffset) {
        [_RTFOutputStream writeString:evaluateOffset];
    }
    
    int32_t lineSpacing = ceilf(self.renderFrame.size.height * kPixelToTwips);
    [_RTFOutputStream writeString:[NSString stringWithFormat:@"\\sl-%d", lineSpacing]]; // line spacing 0
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:self.evaluteTitle];
    [_RTFOutputStream writeString:unicodeStr];
    [_RTFOutputStream writeString:@"\\par"];
    [_RTFOutputStream writeString:@"\\pard"];
    
}

- (void)dealloc{
    _evaluteTitle = nil;
    FreeObj(_tableColumn);
    [super dealloc];
}

@end
