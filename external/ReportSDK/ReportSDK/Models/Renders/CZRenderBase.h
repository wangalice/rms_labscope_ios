//
//  CZRenderBase.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSOutputStream;
@class CZRTFFontColorRegistTable;

@interface CZRenderBase : NSObject{
    CGContextRef    _currentContext;
    NSDictionary    *_renderContent;
    
    /// the render frame can be chanaged any time
    CGRect          _renderFrame;
    
    /// the relative frame should not be changed ,except separate the PDF page 
    CGRect          _relativeFrame;
    NSInteger       _renderOrder;
    UIColor         *_backgroundColor;
    BOOL            _canBreak;
    NSOutputStream  *_RTFOutputStream;
}

@property(nonatomic,retain) NSDictionary           *renderContent;
@property(nonatomic)        CGRect                 renderFrame;
@property(nonatomic)        CGRect                 relativeFrame;

@property(nonatomic)        NSInteger              renderOrder;
@property(nonatomic,retain) UIColor                *backgroundColor;
@property(nonatomic)        BOOL                   canBreak;

///Init method,every render element inited by element content dictionary and a rect.
- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame;

/**
  This method is base method, and do some layout work and compute the real frame of the elemnt.
   every render element should call this method when it begin to render.
 */
- (CGSize)preRenderLayout;

/// this is the main rendering function for PDF,render the content to CGGContextRef 
- (void)render;

/// register color in RTF color table before renderering.
- (void)registerRTFColor:(CZRTFFontColorRegistTable *)colorTable;
/// this is the main rendering function for RTF,organize the content use RTF grammar.
- (void)rtfRenderIntoStream:(NSOutputStream *)stream;

/// just used for debug.
- (void)printOrder;

@end
