//
//  CZTextRender.h
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

@interface CZTextRender : CZRenderBase{
    NSString    *_text;
    
    /// used in header
    BOOL         _isInHeader;
}

@property(nonatomic, retain) NSString  *text;
@property(nonatomic, assign) float   totalHeight;
@property(nonatomic, assign) NSInteger textSubOrder;

- (id)initWithHeaderContent:(NSDictionary *)contentDic frame:(CGRect)aFrame;

@end
