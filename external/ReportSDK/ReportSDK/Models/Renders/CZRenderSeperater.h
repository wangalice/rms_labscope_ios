//
//  CZRenderSeperater.h
//  ReportSDK
//
//  Created by Ralph Jin on 2/19/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZRenderBase.h"

@protocol CZRenderSeperater <NSObject>

@property (nonatomic, retain, readonly) NSArray *subRenders;

- (void)separateMultiDataRender;

@end
