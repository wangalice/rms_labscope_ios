//
//  CZPageInfoRender.h
//  ReportSDK
//
//  Created by Johnny on 8/30/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

@interface CZPageInfoRender : CZRenderBase

@property (nonatomic,assign)        NSUInteger          totalPageNum;
@property (nonatomic,assign)        NSUInteger          curPageNum;

@end
