//
//  CZFooterRender.m
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFooterRender.h"
#import "NSOutputStream+String.h"
#import "CZTextRender.h"
#import "CZDictionaryTool.h"
#import "CZLineRender.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFTool.h"
#import "CZPageInfoRender.h"
#define kPageInfo @"pageInfo"
#define kIsPageInfoVisible @"isPageInfoVisible"

@implementation CZFooterRender

@synthesize curPageNum;
@synthesize totalPageNum;
@synthesize footerElements = _footerElements;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
//      self.backgroundColor = [UIColor colorWithRed:0.2 green:0.5 blue:0.8 alpha:0.2];
        NSArray *footerElementDicArray = [self.renderContent valueForKey:kFooterElements];
        assert([footerElementDicArray count]>0);
        _footerElements = [[NSMutableArray alloc]initWithCapacity:5];
        for (NSDictionary *elementDic in footerElementDicArray) {
            NSDictionary *componentFrame = [elementDic valueForKey:kFrame];
            CGRect componentRct = [CZDictionaryTool dictionay2Frame:componentFrame];
            NSString *componentType = [elementDic valueForKey:kElementType];
            CZRenderBase *footerComponent = nil;
            if ([componentType isEqualToString:kTextElement]) {
                footerComponent = [[CZTextRender alloc]initWithHeaderContent:elementDic
                                                                       frame:componentRct];
            }else if ([componentType isEqualToString:kLineElement]){
                footerComponent = [[CZLineRender alloc]initWithContent:elementDic
                                                                 frame:componentRct];
                CZLineRender *lineComponent = (CZLineRender *)footerComponent;
                lineComponent.isInBody = NO;
                
            }else if ([componentType isEqualToString:kPageInfoElement]){
                footerComponent = [[CZPageInfoRender alloc]initWithContent:elementDic
                                                                 frame:componentRct];
            }
            if(footerComponent){
                [_footerElements addObject:footerComponent];
            }
            FreeObj(footerComponent);
        }
        NSDictionary *pageInfoDic = [self.renderContent valueForKey:kPageInfo];
        isPageInfoVisible = [[pageInfoDic valueForKey:kIsPageInfoVisible] boolValue];
        _isResort = NO;
    }
    return self;
}

/// covert the relative frame to absolute frame,and remove the element that is not contained in the header.
- (void)reSortOrderForPDF{
    if ([_footerElements count]<1 || _isResort) {
        return;
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_footerElements sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    CGFloat xValue = 0.f;
    CGFloat yValue = 0.f;
    for (NSUInteger i = 0; i< [_footerElements count]; i++) {
        CZRenderBase *renderObj = [_footerElements objectAtIndex:i];
        if (i == 0) {
            CGRect componentRct = renderObj.renderFrame;
            componentRct = CGRectMake(componentRct.origin.x + self.renderFrame.origin.x,
                                      componentRct.origin.y + self.renderFrame.origin.y,
                                      componentRct.size.width, componentRct.size.height);
            CGRect rederRct = componentRct;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
            xValue = renderObj.renderFrame.origin.x;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
        }else{
            xValue = xValue + renderObj.relativeFrame.origin.x;
            yValue = yValue + renderObj.relativeFrame.origin.y;
            CGRect rederRct = renderObj.renderFrame;
            rederRct = CGRectMake(xValue, yValue, rederRct.size.width, rederRct.size.height);
            renderObj.renderFrame = rederRct;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;

            CZLogRect(rederRct);
        }
        renderObj.renderOrder = renderObj.renderOrder + i;
        
        CGRect rederRct = renderObj.renderFrame;
        CGPoint rightBottomPoint = CGPointMake(renderObj.renderFrame.origin.x +rederRct.size.width,
                                               renderObj.renderFrame.origin.y + rederRct.size.height);
        
        if (!CGRectContainsPoint(_renderFrame, renderObj.renderFrame.origin) && !CGRectContainsPoint(_renderFrame, rightBottomPoint)) {
            [_footerElements removeObject:renderObj];
        }
    }
    _isResort = YES;
    
}


- (void)reSortOrderForRTF{
    if ([_footerElements count]<1 || _isResort) {
        return;
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_footerElements sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    CGFloat xValue = 0.f;
    CGFloat yValue = 0.f;
    for (NSUInteger i = 0; i< [_footerElements count]; i++) {
        CZRenderBase *renderObj = [_footerElements objectAtIndex:i];
        if (i == 0) {
            CGRect componentRct = renderObj.renderFrame;
            componentRct = CGRectMake(componentRct.origin.x + self.renderFrame.origin.x,
                                      componentRct.origin.y + self.renderFrame.origin.y,
                                      componentRct.size.width, componentRct.size.height);
            CGRect rederRct = componentRct;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
            xValue = renderObj.renderFrame.origin.x;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
        }else{
            xValue = xValue + renderObj.relativeFrame.origin.x;
            yValue = yValue + renderObj.relativeFrame.origin.y;
            CGRect rederRct = renderObj.renderFrame;
            rederRct = CGRectMake(xValue, yValue, rederRct.size.width, rederRct.size.height);
            renderObj.renderFrame = rederRct;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
            
            CZLogRect(rederRct);
        }
        renderObj.renderOrder = renderObj.renderOrder + i;
        
        CGRect rederRct = renderObj.renderFrame;
        CGPoint rightBottomPoint = CGPointMake(renderObj.renderFrame.origin.x +rederRct.size.width,
                                               renderObj.renderFrame.origin.y + rederRct.size.height);
        
        if (!CGRectContainsPoint(_renderFrame, renderObj.renderFrame.origin) && !CGRectContainsPoint(_renderFrame, rightBottomPoint)) {
            [_footerElements removeObject:renderObj];
        }
    }
    _isResort = YES;
}

- (void)render{
    [super render];
    [self reSortOrderForPDF];
    if (_footerElements) {
        for (CZRenderBase *renderObj in _footerElements) {
            if ([renderObj isKindOfClass:[CZPageInfoRender class]]) {
                CZPageInfoRender *infoRender = (CZPageInfoRender *)renderObj;
                infoRender.totalPageNum = self.totalPageNum;
                infoRender.curPageNum = self.curPageNum;
            }
            [renderObj render];
        }
    }
    CZLog(@"render end of class %@",[self debugDescription]);
}


#pragma mark -
#pragma mark RTF render methods

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    [self reSortOrderForRTF];
    if (!_footerElements || [_footerElements count]<1) {
        return;
    }

    [_RTFOutputStream writeString:@"{\\footer\\viewkind4\\viewscale100\\uc1\\pard\\f0\\fs20"];
    
    for (NSUInteger i = 0; i< [_footerElements count]; i++) {
        CZRenderBase *renderObj = [_footerElements objectAtIndex:i];
        CGRect currentRenderRct = renderObj.renderFrame;
        CGFloat offsetX = currentRenderRct.origin.x < 0 ? 0:currentRenderRct.origin.x ;
        renderObj.renderFrame = CGRectMake(offsetX,
                                           currentRenderRct.origin.y,
                                           currentRenderRct.size.width ,
                                           currentRenderRct.size.height);
        [renderObj rtfRenderIntoStream:stream];
    }

    
    [_RTFOutputStream writeString:@"}"];
    
}

- (void)dealloc{
    FreeObj(_footerElements);
    [super dealloc];
}

@end
