//
//  CZPieRender.h
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

/**
 * @brief CZPieRender.
 * @note  this class limit that: 1. the pie render part ,require 
 *        the rect.wide > rect.heigt,if notauto adjust the rect.heigt (rect.heigt = rect.wide*2/3)
 */

@interface CZPieRender : CZRenderBase

@property(nonatomic) BOOL sameColorText;

@property (nonatomic, assign, readonly) BOOL hasMultiData;

@end
