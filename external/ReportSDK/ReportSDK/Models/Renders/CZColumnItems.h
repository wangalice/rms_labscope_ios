//
//  CZColumnItems.h
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZColumnItem.h"

@interface CZColumnItems : NSObject{
	//CZColumnItem array
	NSMutableArray	*_items;
}

@property(nonatomic, retain) NSMutableArray *items;

- (void)addData:(CZColumnItem *)value;
- (CZColumnItem *)objectAtIndex:(NSUInteger)index;
- (NSUInteger)count;

@end