//
//  CZLineRender.h
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

@interface CZLineRender : CZRenderBase

@property (nonatomic,assign) BOOL isInBody;

@end
