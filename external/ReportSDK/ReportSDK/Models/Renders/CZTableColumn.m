//
//  CZTableColumn.m
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableColumn.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"
#import "CZTableColumnCellObj.h"
#import "CZNumberTool.h"

#define kColumnCellElements     @"columnCellElements"
#define kSortOrderAttribute     @"order"

@interface CZTableColumn ()

@end

@implementation CZTableColumn

@synthesize tableColumnCells = _tableColumnCells;
@synthesize tableColumnDic = _tableColumnDic;
@synthesize order = _order;
@synthesize title = _title;
@synthesize tableColumnID = _tableColumnID;
//@synthesize frame = _frame;
@synthesize cellNum;
@synthesize isNum = _isNum;

- (id)initWithDic:(NSDictionary *)tableColumnDic{
    self = [super init];
    if (self) {
        self.tableColumnDic = tableColumnDic;
        NSString *type = [tableColumnDic valueForKey:kElementType];
        assert([type isEqualToString:kTableColumnElement]);
        self.order = [[tableColumnDic valueForKey:kOrder]integerValue];
        self.title = [tableColumnDic valueForKey:kTableColumnTitle];
        self.tableColumnID = [tableColumnDic valueForKey:kTableColumnId];
//        NSDictionary *frameDic = [tableColumnDic valueForKey:kFrame];
//        self.frame = [CZDictionaryTool dictionay2Frame:frameDic];

        NSArray *cellDicArray = [tableColumnDic valueForKey:kColumnCellElements];
        //assert([cellDicArray count]>0);

        _tableColumnCells = [[NSMutableArray alloc]initWithCapacity:5];

        if (cellDicArray && [cellDicArray count]>0) {
            for (NSDictionary *cellDic in cellDicArray) {
                CZTableColumnCellObj *cellObj = [[CZTableColumnCellObj alloc]initWithDic:cellDic];
                cellObj.columnOrder = self.order;
                [_tableColumnCells addObject:cellObj];
                FreeObj(cellObj);
            }
        }
        if (self.title) {
            CZTableColumnCellObj *cellObj = [[CZTableColumnCellObj alloc]initWithTitle:self.title];
            cellObj.columnOrder = self.order;
            cellObj.isTitle = YES;
            cellObj.order = 0;
            [_tableColumnCells addObject:cellObj];
            FreeObj(cellObj);
        }else{
            CZTableColumnCellObj *cellObj = [[CZTableColumnCellObj alloc]initWithTitle:kTitle];
            cellObj.columnOrder = self.order;
            cellObj.order = 0;
            cellObj.isTitle = YES;
            [_tableColumnCells addObject:cellObj];
            FreeObj(cellObj);
        }
    }
    return self;
}

- (void)addColumnCells:(NSArray *)tableContentArray withFontDic:(NSDictionary *)fontDic{
    
    if (_tableColumnCells && [_tableColumnCells count]>0) {
        CZTableColumnCellObj *cellObj = [_tableColumnCells objectAtIndex:0];
        cellObj.fontDictionary = fontDic;
    }
    NSInteger i = 0;
    NSUInteger maxPrecision = 0;
    NSString *precisionKey = [self.tableColumnID stringByAppendingString:@".precision"];
    NSString *colorKey = [self.tableColumnID stringByAppendingString:@".backgroundColor"];
    
    for (NSDictionary *columnCellObj in tableContentArray) {
        i +=1;
        NSString *cellText = [columnCellObj valueForKey:self.tableColumnID];
        id cellContent = [columnCellObj valueForKey:self.tableColumnID];
        if ([cellContent isKindOfClass:[NSNumber class]]) {
            self.isNum = YES;
            cellText = nil;
            
            NSUInteger precision = 3U;
            
            id precisionObj = [columnCellObj valueForKey:precisionKey];
            if ([precisionObj isKindOfClass:[NSNumber class]]) {
                precision = [(NSNumber *)precisionObj unsignedIntegerValue];
            }
            
            maxPrecision = MAX(precision, maxPrecision);
            
            /**
            cellText = [NSString stringWithFormat:@"%@",[cellContent stringValue]];
            
            ///begin: added special for the rtfd review
            if ([cellText length] == 8 && [cellText rangeOfString:@"."].length>0) {
                cellText = [NSString stringWithFormat:@"%@0",cellText];
            }
            ///end: added special for the rtfd review
            */
            /// Followed is used for localization.
            cellText = [CZNumberTool localizedStringFromNumber:cellContent precision:precision];
        }
        CZTableColumnCellObj *cellObj = [[CZTableColumnCellObj alloc] initWithTitle:cellText];
        
        NSUInteger rgba = 0;
        NSNumber *backgroudColor = [columnCellObj valueForKey:colorKey];
        if ([backgroudColor isKindOfClass:[NSNumber class]]) {
            rgba = [backgroudColor unsignedIntegerValue];
            float components[4];
            for (int i = 3; i >= 0; i--) {
                components[i] = (rgba & 0Xff) / 255.0;
                rgba >>= 8;
            }
            cellObj.backgroundColor = [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:components[3]];
        }
        
        cellObj.fontDictionary = fontDic;
        cellText = nil;
        if ([cellContent isKindOfClass:[NSNumber class]]){
            cellObj.floatContent = [cellContent doubleValue];
        }
        cellObj.order = i;
        cellObj.columnOrder = self.order;
        [self.tableColumnCells addObject:cellObj];
        FreeObj(cellObj);
    }
    
    _precision = maxPrecision;
}

- (void)ascendingSortCellOrder{
    /// TO DO ... sort
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kSortOrderAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_tableColumnCells sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    for (NSUInteger i = 0; i< [_tableColumnCells count]; i++) {
        CZTableColumnCellObj *obj = [_tableColumnCells objectAtIndex:i];
        obj.drawOrder.lineOrder = i;
    }
}

- (void)setCellDrawColumnOrder:(NSInteger) aOrder{
    for (NSUInteger i = 0; i< [_tableColumnCells count]; i++) {
        CZTableColumnCellObj *obj = [_tableColumnCells objectAtIndex:i];
        obj.drawOrder.columnOrder = aOrder;
    }
}

- (NSInteger)cellNum{
     cellNum = 0;
     if (_tableColumnCells) {
         cellNum = [_tableColumnCells count];
     }
     return cellNum;
}

- (NSArray *)useColors {
    NSMutableArray *useColors = [NSMutableArray array];
    for (CZTableColumnCellObj *cell in _tableColumnCells) {
        if (cell.backgroundColor) {
            [useColors addObject:cell.backgroundColor];
        }
    }
    
    return useColors;
}

- (void)dealloc{
    FreeObj(_tableColumnCells);
    FreeObj(_tableColumnDic);
    FreeObj(_title);
    FreeObj(_tableColumnID);
    [super dealloc];
}

@end
