//
//  CZTableRender.h
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

#define kTableMarginWidth   5
#define kTableMarginHeight  5

@interface CZTableRender : CZRenderBase

@property (nonatomic, readonly) NSInteger columnNumber;
@property (nonatomic, readonly) NSInteger lineNumber;

@property (nonatomic, readonly) float tableCellHeight;
@property (nonatomic, readonly) float tableTitleHeight;
@property (nonatomic, readonly) float tableEvaluateHeight;
@property (nonatomic, readonly) float tableCaptionHeight;

@property (nonatomic, assign) NSInteger tableSubOrder;
@property (nonatomic, retain, readonly) NSArray *evaluates;

@property (nonatomic, assign, readonly) BOOL hasMultiData;

/// used in CZTableSeparater class,set the last table the evaluations.
- (void)setEvaluatesContent:(NSArray *)evaluates;

/// used in CZTableSeparater class,hide the last table evaluations.
- (void)hideTableEvaluate;

@end
