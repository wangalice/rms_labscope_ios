//
//  CZColumnRender.m
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZColumnRender.h"
#import <QuartzCore/QuartzCore.h>
#import "NSOutputStream+String.h"
#import "CZColumnLable.h"
#import "CZColumnItem.h"
#import "CZColumnItems.h"
#import "CZRTFTool.h"
#import "CZRTFPreviewConfig.h"
#import "CZDictionaryTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZNumberTool.h"

#define kSegmentMaxNum                  6
#define kColumnItemTitleMaxWidth        68.f

static const int MARGIN_LEFT = 50;
//Y axis to the left margin
static const int MARGIN_BOTTOM = 70;		///bottom margin
static const int MARGIN_TOP = 20;			///top margin
static const int SHOW_SCALE_NUM = 10;		///the grade number
static const int BARCELL=2;				///column margin
static const int FONTSIZE=8;             ///font size
static NSUInteger	MAXGROUP=1;


@interface CZColumnRender() {
  @private
	///background color
	UIColor *backcolor;
    
	///the show data array
	CZColumnItems *groupData;
    
	NSMutableArray *groupColor;
    
    NSString *title;  // assign
	
	///max，mini, colomn wide,
	///maxScaleValue　every line data value
	///maxScaleHeight every line pixel value
	///sideWidth　column interval
	///columnWidth column wide
	///maxValue,minValue the allowed max data value and minimum data value
	double			  maxValue,minValue;
	double			  columnWidth,sideWidth,maxScaleValue,maxScaleHeight;
    BOOL              _isShowBiggestData;
}

@property(nonatomic, retain) CZColumnItems *groupData;
@property(nonatomic, copy) NSString *unitString;

-(void)drawColumn:(CGContextRef)context rect:(CGRect)_rect;
-(void)drawScale:(CGContextRef)context rect:(CGRect)_rect;
-(void)calcScales:(CGRect)_rect;
-(void)addGroupColor:(int) rgb;
-(UIColor *)getGroupColor:(NSUInteger)groupIndex;
-(bool)checkLabel:(NSMutableArray*)_arr;

@end

@implementation CZColumnRender

@synthesize groupData;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        _canBreak = NO;
        NSDictionary *value = [contentDic valueForKey:kValue];
        
        _isShowBiggestData = YES;
        id largeShowId = [value valueForKey:kIsLargestSegmentShow];
        if (largeShowId && [largeShowId isKindOfClass:[NSNumber class]]) {
            if((strcmp([largeShowId objCType], @encode(char))) == 0) {
                _isShowBiggestData = [largeShowId boolValue];
            }
        }
        
        // read column title
        NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
        if ([pieTitle isKindOfClass:[NSDictionary class]]) {
            title =[pieTitle valueForKey:kTitleText];
            if (![title isKindOfClass:[NSString class]]) {
                title = nil;
            }
        }
        
        id obj = [value valueForKey:kColumnComponentArray];
        if ([obj isKindOfClass:[NSArray class]]) {  // data format version 0
            [self parseItemsFromArray:(NSArray *)obj];
            [self bigSegmentFilter];
        } else if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *complexValue = (NSDictionary *)obj;
            id version = complexValue[kVersion];
            if ([@1 isEqual:version]) {  // data format version 1
                NSArray *valuesArray = [complexValue valueForKey:kValues];
                assert([valuesArray count]);
                
                if ([valuesArray count] == 1) {
                    NSArray *componentArray = [valuesArray[0] valueForKey:kValue];
                    [self parseItemsFromArray:componentArray];
                    
                    // parse unit string
                    id unitString = [valuesArray[0] valueForKey:kUnit];
                    if ([unitString isKindOfClass:[NSString class]]) {
                        _unitString = [unitString copy];
                    }
                    
                    [self bigSegmentFilter];
                } else {  // multi-data column, which need seperate during render
                    _hasMultiData = YES;
                }
            } else {
                [self release];
                self = nil;
            }
        }
        
        // initialize default color
        [self addGroupColor:0x04BE00];
        [self addGroupColor:0xFF0000];
        [self addGroupColor:0xFF00FF];
        
       //self.backgroundColor = [UIColor colorWithRed:0.2 green:0.3 blue:0.9 alpha:0.8];
    }
    return self;
}

- (void)bigSegmentFilter{
    assert([self.groupData.items count]>0);

    NSMutableArray *sortedArray = nil;
    
    if (!_isShowBiggestData || [self.groupData.items count] > kSegmentMaxNum) {  // sort items
        sortedArray = [[NSMutableArray alloc]initWithArray:self.groupData.items];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kCurrentValue
                                                                       ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                              count:1];
        [sortedArray sortUsingDescriptors:sortDescriptors];
        FreeObj(sortDescriptor);
        FreeObj(sortDescriptors);
    }
    
    if (!_isShowBiggestData) {
        [self.groupData.items removeObject:[sortedArray lastObject]];
        [sortedArray removeLastObject];
    }
    
    /// the following code is just for the situation:there are too many data
    if ([self.groupData.items count] > kSegmentMaxNum) {
        CGFloat otherValue = 0.f;
        
        NSUInteger removeCount = [self.groupData.items count] - kSegmentMaxNum + 1;

        for (NSUInteger i = 0; i < removeCount; i++) {
            CZColumnItem *leftComponent = [sortedArray objectAtIndex:i];
            otherValue += leftComponent.currentValue;
            [groupData.items removeObject:leftComponent];
        }

        CZColumnItem *otherComponent = [[CZColumnItem alloc] initWithDouble:otherValue];
        otherComponent.lable = @"Others";
        [groupData.items addObject:otherComponent];
        FreeObj(otherComponent);
    }
    
    [sortedArray release];
}

- (CGSize)preRenderLayout{
    [super preRenderLayout];
    CGSize renderSize = self.renderFrame.size;
    return renderSize;
}

#pragma mark -
#pragma mark PDF render methods

- (void)render{
    [self preRenderLayout];
    [super render];
    [self drawRect:self.renderFrame];
    CZLog(@"render end of class %@",[self debugDescription]);
    return;
}


-(void)addGroupColor:(int) rgb{
	int r=(rgb/65536);
	int g=((rgb%65536)/256);
	int b=rgb%256;
	
	float r1=r/255.0;
	float g1=g/255.0;
	float b1=b/255.0;
	float a1=1;
	
	UIColor *_color=[[UIColor alloc] initWithRed:r1 green:g1 blue:b1 alpha:a1];
	if (groupColor){
		[groupColor addObject:_color];
	}
	else{
		groupColor=[[NSMutableArray alloc]init];
		[groupColor addObject:_color];
	}
    [_color release];
    _color = nil;
}

- (UIColor *)getGroupColor:(NSUInteger)groupIndex {
	if (groupColor) {
        if (groupColor.count) {
            groupIndex = groupIndex % groupColor.count;  // rewind group index
			return [groupColor objectAtIndex:groupIndex];
		} else {
			return [UIColor greenColor];
		}
	} else {
		return [UIColor cyanColor];
	}
}

-(void)drawRect:(CGRect)_rect{

	//compute scale graduation
	[self calcScales:_rect];
	//draw scale graduation
	[self drawScale:_currentContext rect:_rect];
	//draw column
	[self drawColumn:_currentContext rect:_rect];
	//draw title
    [self drawColumnTitle];
}

- (void)drawColumnTitle{
    if (title) {
        NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
        
        NSDictionary *fontDic = [pieTitle valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic ||![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat r = [[color valueForKey:kRedColor] floatValue];
        CGFloat g = [[color valueForKey:kGreenColor] floatValue];
        CGFloat b = [[color valueForKey:kBlueColor] floatValue];
        CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
        //NSInteger titleWidth=[_title sizeWithFont:font].width;
        CGFloat titleWidth = [CZDictionaryTool textSize:title useFont:font].width;
        CGFloat titleheight = ceil([title sizeWithFont:font].height);
        
        CGFloat baseX = self.renderFrame.origin.x;
        CGFloat baseY = self.renderFrame.origin.y;
        CGFloat yMargin = self.renderFrame.size.height/15 * 0.5;
        
        CGRect titleRct = CGRectMake(baseX + (_renderFrame.size.width - titleWidth)/2,
                                     baseY + yMargin,
                                     titleWidth,
                                     titleheight);
        [title drawInRect:titleRct withFont:font
             lineBreakMode:NSLineBreakByWordWrapping
                 alignment:NSTextAlignmentLeft];
    }

}

/// draw the scale to the context
-(void)drawScale:(CGContextRef)context rect:(CGRect)_rect{
    
    CGFloat yMargin = self.renderFrame.size.height/15;
    CGFloat baseX = self.renderFrame.origin.x;
    CGFloat baseY = self.renderFrame.origin.y + yMargin;
    
    
	UIFont *_font=[UIFont systemFontOfSize:FONTSIZE];
	CGPoint points[3];
	points[0] = CGPointMake(MARGIN_LEFT + baseX, MARGIN_TOP + baseY);
	points[1] = CGPointMake(MARGIN_LEFT + baseX, _rect.size.height - MARGIN_BOTTOM + 1+ baseY);
	points[2] = CGPointMake(_rect.size.width+ baseX , _rect.size.height - MARGIN_BOTTOM + 1+ baseY);
	CGContextSetAllowsAntialiasing(context, NO);
	CGContextAddLines(context, points, 3);
	CGContextSetLineWidth(context, 1);
	CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);

    if (self.unitString) {
        CGRect renderRect;
        renderRect.size = [self.unitString sizeWithFont:_font];
        renderRect.origin.x = MARGIN_LEFT + baseX - 5 - renderRect.size.width;
        renderRect.origin.y = MARGIN_TOP + baseY - renderRect.size.height / 2;
        renderRect = CGRectIntegral(renderRect);

        [self.unitString drawInRect:renderRect withFont:_font];
    }
    
	for(int i=0;i<SHOW_SCALE_NUM + 1; i++){
		maxScaleHeight = (_rect.size.height - MARGIN_BOTTOM-MARGIN_TOP) * ( i ) / (SHOW_SCALE_NUM + 1);
		double vScal = ceil(1.0 * maxScaleValue / (SHOW_SCALE_NUM ) * (i ));
		const CGFloat y = (_rect.size.height - MARGIN_BOTTOM) - maxScaleHeight;
		
		///NSString *scaleStr = [NSString stringWithFormat:@"%0.f",vScal];
        NSString *scaleStr = [CZNumberTool localizedStringFromFloat:vScal precision:1U];
        
        
		// the text wide and height in the Y axis
		int w=ceil([scaleStr sizeWithFont:_font].width);
		int h=ceil([scaleStr sizeWithFont:_font].height);
		
        [scaleStr drawInRect:CGRectMake(MARGIN_LEFT - 5 - w + baseX,
                                        y - h/2 + baseY, w, h)
                                        withFont:_font];
        scaleStr = nil;
		points[0] = CGPointMake(MARGIN_LEFT + baseX, y + baseY);
		points[1] = CGPointMake(MARGIN_LEFT - 3 + baseX, y + baseY);
		CGContextSetLineDash(context, 0, NULL, 0);
		CGContextAddLines(context, points, 2);
		CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
		
		CGContextDrawPath(context, kCGPathStroke);
		
		
		points[0] = CGPointMake(MARGIN_LEFT + baseX, y + baseY);
		points[1] = CGPointMake(_rect.size.width + baseX , y + baseY);
		const CGFloat partren[] = {2,3};
		CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:.7 green:.7 blue:.7 alpha:1].CGColor);
		
		CGContextSetLineDash(context, 0,partren , 2);
		CGContextAddLines(context, points, 2);
		CGContextDrawPath(context, kCGPathStroke);
		
	}
	CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
	CGContextDrawPath(context, kCGPathStroke);
	CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetLineDash(context, 0, NULL, 0);
}


/// draw the column to the context
-(void)drawColumn:(CGContextRef)context rect:(CGRect)_rect{
    CGFloat yMargin = self.renderFrame.size.height/15;
    CGFloat baseX = self.renderFrame.origin.x;
    CGFloat baseY = self.renderFrame.origin.y + yMargin;

	NSMutableArray	*_labels=[[NSMutableArray alloc] init];
	int baseGroundY = _rect.size.height - MARGIN_BOTTOM;
    const int baseGroundX = MARGIN_LEFT;
	UIFont *_font=[UIFont systemFontOfSize:FONTSIZE + 1];
	
	for (int i=0; i<[groupData count]; i++){
		CZColumnItem *_item=[groupData objectAtIndex:i];
		for (int j=0; j<[_item count]; j++){
            UIColor *columnColor = _item.color;
            if (columnColor == nil) {
                columnColor = [self getGroupColor:j];
            }
            
			CGContextSetFillColorWithColor(context, columnColor.CGColor);
			CGContextSetStrokeColorWithColor(context, columnColor.CGColor);
			
			double v=[_item objectAtIndex:j];
			
			int columnHeight = v / maxScaleValue * maxScaleHeight ;
			
			//draw front face
			int offset=ceil(BARCELL/2);								
			offset=offset+ceil((columnWidth+sideWidth)*MAXGROUP/2);	
			offset=offset+i*BARCELL;								
			offset=offset+i*(columnWidth +sideWidth)*MAXGROUP;		
            
			offset=offset+(columnWidth +sideWidth)* j;				
			offset=offset+j;
			
			CGContextSetFillColorWithColor(context, columnColor.CGColor);
			CGContextAddRect(context, CGRectMake( offset+ baseGroundX + baseX
												 , baseGroundY - columnHeight + baseY
												 , columnWidth
												 , columnHeight));
			
			CGContextDrawPath(context, kCGPathFill);
			
			NSLog(@"i=%d,j=%d,x=%d",i,j,offset+ baseGroundX);
			
			if(columnHeight < 10){
				continue;
			}
		}
		// calculate the position of text
		NSString *scaleStr = _item.lable;
		if (scaleStr){
			// Y axis ,the text wide and height
			int offset=ceil(BARCELL/2);								
			offset=offset+ceil((columnWidth+sideWidth)*MAXGROUP);	
			offset=offset+i*BARCELL;								
			offset=offset+i*(columnWidth +sideWidth)*MAXGROUP;		
			
			CZColumnLable	*_label=[[CZColumnLable alloc] init];
			_label.x=offset+baseGroundX;
			_label.y=baseGroundY+2;
			_label.label=scaleStr;
            CGSize textSize = [scaleStr sizeWithFont:_font];
            textSize.width = ceil(textSize.width);
            textSize.height = ceil(textSize.height);
			_label.width = textSize.width;
			_label.height = textSize.height;
			[_labels addObject:_label];
            [_label release];
            _label = nil;
		}
	}
	//draw column title
	bool isc=[self checkLabel:_labels];
	if (_labels.count>0){
		CGContextSetFillColorWithColor(context,[UIColor blackColor].CGColor);
		CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
        const CGFloat kMaxTextWidthLong = columnWidth * 2 + sideWidth * 3;
        const CGFloat kMaxTextWidthShort = columnWidth + sideWidth * 2;
		for (int i=0; i<_labels.count; i++){
			CZColumnLable *_lab=[_labels objectAtIndex:i];
			
			int32_t x=_lab.x-_lab.width/2  + baseX;
			int32_t y= _lab.y + baseY;
			if(!isc){
				if (i%2==1){
					y=y+_lab.height+1;
				}
                _lab.width = kMaxTextWidthLong;
			} else {
                _lab.width = kMaxTextWidthShort;
            }

            int32_t fwidth = _lab.width;
            int32_t fheight = _lab.height;
            
            [_lab.label drawInRect:CGRectMake(x, y, fwidth, fheight)
                          withFont:_font
                     lineBreakMode:NSLineBreakByTruncatingTail];
		}
	}
    [_labels release];
    _labels = nil;
}

-(bool)checkLabel:(NSMutableArray*)_arr{
	if (_arr.count>0){
		for (int i=0;i<_arr.count-1; i++){
			CZColumnLable *_lab1=[_arr objectAtIndex:i];
			CZColumnLable *_lab2=[_arr objectAtIndex:i+1];
			if (_lab1.x+_lab1.width>=_lab2.x){
				return false;
			}
		}
		return true;
	}
	return false;
}

// compute max,minimum,and the num of column
-(void)calcScales:(CGRect)_rect{
	NSUInteger columnCount = 0;
	NSUInteger groupNum=[groupData count];
	for (NSUInteger i=0; i<groupNum; i++){
		NSUInteger gNum =0;
		CZColumnItem *_item=[groupData objectAtIndex:i];
		for (int j=0; j<[_item count]; j++){
			double v=[_item objectAtIndex:j];
			if(maxValue<v) maxValue = v;
			if(minValue>v) minValue = v;
			gNum++;
		}
		columnCount++;
		if (gNum>MAXGROUP)
			MAXGROUP=gNum;
	}
	//maxScaleValue = ((int)ceil(maxValue) + (SHOW_SCALE_NUM - (int)ceil(maxValue) % SHOW_SCALE_NUM));
    maxScaleValue = ((double)ceil(maxValue) + SHOW_SCALE_NUM);
	columnWidth = (_rect.size.width - MARGIN_LEFT * 1) / (columnCount + 1);
    
//	BARCELL=columnWidth *0.2;
    if (MAXGROUP!=0) {
    columnWidth=(columnWidth-BARCELL)/MAXGROUP;
    }
	sideWidth = columnWidth *.2;
	columnWidth *= .8;					
}

#pragma mark -
#pragma mark RTF render methods

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    BOOL isRTF = [[CZRTFPreviewConfig sharedInstance] isForRTF];
    if (isRTF) {
        [self renderRTFContent];
    }else{
        [self renderRTFDContent];
    }
}

- (void)renderRTFContent{ @autoreleasepool{
    /// convert the render content to image,and convert the image data to ASIC code
    
    NSString *imageRTFStr = nil;
    /// followed is generate the column image rtf content.
    /// Note: the title is in top of image.
    NSString *textBoxPre = [CZRTFTool textBoxPre];
    NSString *textBoxEnd = [CZRTFTool textBoxEnd:_renderFrame];
    [_RTFOutputStream writeString:textBoxPre];
    [self generateRTFColumnTitle];
    UIImage *image = [self generateRTFImage];
    if (!image) {
        [_RTFOutputStream writeString:textBoxEnd];
        return;
    }

    /** if use JPEG compressing,should use the "\\jpegblip" format word.
     *  if use PNG compressing,should use the the "\\pngblip" format word.
     */
    NSData *imageData = UIImageJPEGRepresentation(image,kJpegCompressPercent);
    //NSData *imageData = UIImagePNGRepresentation(image);
    imageRTFStr = [CZRTFTool convertImageData2String:imageData];
    int32_t rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,rtfWide,rtfHeight];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
    //[_RTFOutputStream writeString:@"\\pngblip "];
    [_RTFOutputStream writeString:@"\\jpegblip "];
    [_RTFOutputStream writeString:imageRTFStr];
    [_RTFOutputStream writeString:@"}"];
    [_RTFOutputStream writeString:kLineBreak];
    
    [_RTFOutputStream writeString:textBoxEnd];
    
    return;
}}

- (void)generateRTFColumnTitle{
    if (!title) {
        return;
    }
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
    
    NSDictionary *fontDic = [pieTitle valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
    
    CZLogRect(_renderFrame);
    
//    [_RTFOutputStream writeString:@"\\par"];
//    [_RTFOutputStream writeString:@"\\pard"];
   
    
    ///begin new added for set the location default is center.
//    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:title];
    [_RTFOutputStream writeString:unicodeStr];
    //CZLog(@"The text rtf string is %@",_RTFAttributeStr);
    
    title = nil;
    
    /// reduce the area of title
    int fheight=ceil([title sizeWithFont:font].height);
    _renderFrame = CGRectMake(_renderFrame.origin.x,
                              _renderFrame.origin.y,
                              _renderFrame.size.width,
                              _renderFrame.size.height - fheight);
    

}

#pragma mark -
#pragma mark RTFD render methods

- (void)renderRTFDContent{ @autoreleasepool {
    /// convert the render content to image,and convert the image data to ASIC code
    NSString *frameStr = [CZRTFTool offsetFrame2RTFString:_renderFrame];
    
    NSString *imageRTFStr = nil;
    UIImage *image = [self generateRTFImage];
    if (!image) {
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image,kJpegCompressPercent);
    //NSData *imageData = UIImagePNGRepresentation(image);
    imageRTFStr = [[CZRTFPreviewConfig sharedInstance] convertImageData2ImageFile:imageData];
    
    int32_t rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{{\\NeXTGraphic %@ \\width%d \\height%d \\noorient}%@}",imageRTFStr,rtfWide,rtfHeight,[CZRTFTool getNSStringUnicode:@"¨"]];
    
    [_RTFOutputStream writeString:frameStr];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
    [_RTFOutputStream writeString:kLineBreak];
}}


- (UIImage *)generateRTFImage{
    UIGraphicsBeginImageContext(_renderFrame.size);
    _renderFrame = CGRectMake(0, 0, _renderFrame.size.width, _renderFrame.size.height);
    [self render];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void) dealloc{
    FreeObj(backcolor);
    FreeObj(groupData);
    FreeObj(groupColor);
    FreeObj(_unitString);
	[super dealloc];
}

#pragma mark - Privates

- (void)parseItemsFromArray:(NSArray *)componentArray {
    CZColumnItems *items = [[CZColumnItems alloc] init];
    for (NSUInteger i = 0; i < [componentArray count]; i++) {
        NSDictionary *columnItemDic = [componentArray objectAtIndex:i];
        NSNumber *value = [columnItemDic valueForKey:kComponentValue];
        NSString *componentTitle = [columnItemDic valueForKey:kComponentTitle];
        CZColumnItem *item = [[CZColumnItem alloc] initWithDouble:[value floatValue]];
        item.lable = componentTitle;
        
        id color = [columnItemDic valueForKey:kColor];
        if (color) {
            color = [CZDictionaryTool validateColor:color];
            
            CGFloat r = [[color valueForKey:kRedColor] floatValue];
            CGFloat g = [[color valueForKey:kGreenColor] floatValue];
            CGFloat b = [[color valueForKey:kBlueColor] floatValue];
            CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
            item.color = [UIColor colorWithRed:r green:g blue:b alpha:a];
        }
        
        [items addData:item];
        [item release];
    }
    
    self.groupData = items;
    [items release];
}

@end

