//
//  CZPieComponent.m
//  ReportSDK
//
//  Created by Johnny on 2/6/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPieComponent.h"
#import "CZDictionaryTool.h"

@implementation CZPieComponent
@synthesize value = _value, title = _title, colour = _colour;
@synthesize startDegrees = _startDegrees, endDegrees = _endDegrees;


- (id)initWithPicComponentDic:(NSDictionary *)componentDic{
    self = [super init];
    if (self) {
        self.title = [componentDic valueForKey:kComponentTitle];
        self.value = [[componentDic valueForKey:kComponentValue] floatValue];
        
        id color = [componentDic valueForKey:kColor];
        if (color) {
            color = [CZDictionaryTool validateColor:color];
            
            CGFloat r = [[color valueForKey:kRedColor] floatValue];
            CGFloat g = [[color valueForKey:kGreenColor] floatValue];
            CGFloat b = [[color valueForKey:kBlueColor] floatValue];
            self.colour = [UIColor colorWithRed:r green:g blue:b alpha:1.0];
        }

        id additionals = [componentDic valueForKey:kComponentAdditionalValues];
        if ([additionals isKindOfClass:[NSArray class]]) {
            _additionalValues = [additionals retain];
        }
    }
    return self;
}

- (id)initWithTitle:(NSString *)title value:(CGFloat)percent{
    self = [super init];
    if (self) {
        self.title = title;
        self.value = percent;
    }
    return self;
}


- (void)dealloc{
    FreeObj(_colour);
    FreeObj(_title);
    FreeObj(_additionalValues);
    [super dealloc];
}

@end


