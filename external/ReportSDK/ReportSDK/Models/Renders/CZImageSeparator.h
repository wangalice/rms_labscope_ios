//
//  CZImageSeperator.h
//  ReportSDK
//
//  Created by Ralph Jin on 1/16/14.
//  Copyright (c) 2014 Bleum. All rights reserved.
//

#import "CZRenderSeperater.h"

@class CZPDFPage;
@class CZImageRender;

@interface CZImageSeparater : NSObject <CZRenderSeperater>


/** initialized method used the separate the image render with multi-images to
 * several image renders that render one image.
 */
- (id)initWithImageRender:(CZImageRender *)imageRender;

@end