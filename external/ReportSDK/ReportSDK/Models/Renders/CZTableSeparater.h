//
//  CZTableSeparater.h
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderSeperater.h"
#import "CZTableRender.h"
#import "CZPDFPage.h"

@interface CZTableSeparater : NSObject <CZRenderSeperater> {
    
    CZPDFPage       *_containedPage;
    CZTableRender   *_originalTableRender;
    NSMutableArray  *_subTableRenders;
    BOOL             _isAdjust;
}
@property (nonatomic,retain)    NSMutableArray  *subTableRenders;
@property (nonatomic,retain)       CZPDFPage       *containedPage;
@property (nonatomic,retain)       CZTableRender   *originalTableRender;

/** If YES, the first seperated table render goes to the next page. otherwise, it stays in the current page. */
@property (nonatomic,readonly)     BOOL            isAdjust;

/** initialized method used the separate the big table to sever small table 
 *that can be rendered in one page.
 */
- (id)initWithTableRender:(CZTableRender *)tableRender page:(CZPDFPage *)pdfPage;

/// separate the big table to small table.
- (void)separateTable;

@end
