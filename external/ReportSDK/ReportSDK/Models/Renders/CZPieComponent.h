//
//  CZPieComponent.h
//  ReportSDK
//
//  Created by Johnny on 2/6/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PCColorBlue [UIColor colorWithRed:0.0 green:153/255.0 blue:204/255.0 alpha:1.0]
#define PCColorGreen [UIColor colorWithRed:153/255.0 green:204/255.0 blue:51/255.0 alpha:1.0]
#define PCColorOrange [UIColor colorWithRed:1.0 green:153/255.0 blue:51/255.0 alpha:1.0]
#define PCColorRed [UIColor colorWithRed:1.0 green:51/255.0 blue:51/255.0 alpha:1.0]
#define PCColorYellow [UIColor colorWithRed:1.0 green:220/255.0 blue:0.0 alpha:1.0]
#define PCColorDefault [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]

@interface CZPieComponent : NSObject{
    CGFloat     _value, _startDegrees, _endDegrees;
    NSString    *_title;
    UIColor     *_colour;
}

@property (nonatomic, assign) CGFloat value, startDegrees, endDegrees;
@property (nonatomic, retain) UIColor *colour;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSArray *additionalValues;

/// init method.
- (id)initWithPicComponentDic:(NSDictionary *)componentDic;

- (id)initWithTitle:(NSString *)title value:(CGFloat)percent;
@end