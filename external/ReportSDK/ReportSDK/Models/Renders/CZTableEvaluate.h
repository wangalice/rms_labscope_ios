//
//  CZTableEvaluate.h
//  ReportSDK
//
//  Created by Johnny on 3/1/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"
#import "CZTableColumn.h"

@interface CZTableEvaluate : CZRenderBase {
    NSString            *_evaluteTitle;
    NSInteger           _columnNum;
    NSString            *_method;
    double              _evaluteValue;
    
    CZTableColumn       *_tableColumn;
}

/// columnNun should begin with 1.
@property (nonatomic)           NSInteger      columnNum;
@property (nonatomic, readonly) NSString       *evaluteTitle;
@property (nonatomic)           double         evaluteValue;
@property (nonatomic, retain)   CZTableColumn  *tableColumn;

@end
