//
//  CZHeaderRender.m
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZHeaderRender.h"
#import "NSOutputStream+String.h"
#import "CZTextRender.h"
#import "CZDictionaryTool.h"
#import "CZImageRender.h"
#import "CZRTFTool.h"

@implementation CZHeaderRender

@synthesize headerElements = _headerElements;

/// all the headerElement frame is relative with the header frame.
- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        NSArray *headerElementDicArray = [self.renderContent valueForKey:kHeaderElements];
        assert([headerElementDicArray count]>0);
        _headerElements = [[NSMutableArray alloc]initWithCapacity:5];
        for (NSDictionary *elementDic in headerElementDicArray) {
            NSDictionary *componentFrame = [elementDic valueForKey:kFrame];
            CGRect componentRct = [CZDictionaryTool dictionay2Frame:componentFrame];
            NSString *elmentType = [elementDic valueForKey:kElementType];
            if ([elmentType isEqualToString:kTextElement]) {
                CZTextRender *headerComponent = [[CZTextRender alloc]initWithHeaderContent:elementDic
                                                                                     frame:componentRct];
                [_headerElements addObject:headerComponent];
                FreeObj(headerComponent);
            }else if ([elmentType isEqualToString:kImageElement]){
                CZImageRender *headerComponent = [[CZImageRender alloc]initWithContent:elementDic
                                                                                 frame:componentRct];
                headerComponent.isInHeader = YES;
                [_headerElements addObject:headerComponent];
                FreeObj(headerComponent);
            }else if ([elmentType isEqualToString:kLineElement]){
                CZLineRender* headerComponent = [[CZLineRender alloc]initWithContent:elementDic
                                                                               frame:componentRct];
                headerComponent.isInBody = NO;
                [_headerElements addObject:headerComponent];
                FreeObj(headerComponent);
            }
        }
        _isResort = NO;
    }
//  self.backgroundColor = [UIColor colorWithRed:0.2 green:0.5 blue:0.8 alpha:0.5];
    return self;
}

/// covert the relative frame to absolute frame,and remove the element that is not contained in the header.
- (void)reSortOrderForPDF{
    if ([_headerElements count]<1 || _isResort) {
        return;
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                          count:1];
    [_headerElements sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    CGFloat xValue = 0.f;
    CGFloat yValue = 0.f;
    for (NSUInteger i = 0; i< [_headerElements count]; i++) {
        CZRenderBase *renderObj = [_headerElements objectAtIndex:i];
        if (i == 0) {
            CGRect componentRct = renderObj.renderFrame;
            componentRct = CGRectMake(componentRct.origin.x + self.renderFrame.origin.x,
                                      componentRct.origin.y + self.renderFrame.origin.y,
                                      componentRct.size.width, componentRct.size.height);
            CGRect rederRct = componentRct;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
            xValue = renderObj.renderFrame.origin.x;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
        }else{
            xValue = xValue + renderObj.relativeFrame.origin.x;
            yValue = yValue + renderObj.relativeFrame.origin.y;
            CGRect rederRct = renderObj.renderFrame;
            rederRct = CGRectMake(xValue, yValue, rederRct.size.width, rederRct.size.height);
            renderObj.renderFrame = rederRct;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;

            CZLogRect(rederRct);
        }
        renderObj.renderOrder = renderObj.renderOrder + i;
        
        CGRect rederRct = renderObj.renderFrame;
        CGPoint rightBottomPoint = CGPointMake(renderObj.renderFrame.origin.x +rederRct.size.width,
                                               renderObj.renderFrame.origin.y + rederRct.size.height);
        
        if (!CGRectContainsPoint(_renderFrame, renderObj.renderFrame.origin)
                                 && !CGRectContainsPoint(_renderFrame, rightBottomPoint)) {
            [_headerElements removeObject:renderObj];
        }
    }
    _isResort = YES;
    
}

- (CGSize)preRenderLayout{
    CGSize renderContentSize = CGSizeZero;
    renderContentSize = self.renderFrame.size;
    return renderContentSize;
}

- (void)render{
    [super render];
    [self reSortOrderForPDF];
    if (_headerElements) {
        [_headerElements makeObjectsPerformSelector:@selector(render)];
    }
    CZLog(@"render end of class %@",[self debugDescription]);
    return;
}

#pragma mark -
#pragma mark RTF render methods

- (void)reSortOrderForRTF{
    if ([_headerElements count]<1 || _isResort) {
        return;
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                          count:1];
    [_headerElements sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    CGFloat xValue = 0.f;
    CGFloat yValue = 0.f;
    for (NSUInteger i = 0; i< [_headerElements count]; i++) {
        CZRenderBase *renderObj = [_headerElements objectAtIndex:i];
        if (i == 0) {
            CGRect componentRct = renderObj.renderFrame;
            componentRct = CGRectMake(componentRct.origin.x + self.renderFrame.origin.x,
                                      componentRct.origin.y + self.renderFrame.origin.y,
                                      componentRct.size.width, componentRct.size.height);
            CGRect rederRct = componentRct;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
            xValue = renderObj.renderFrame.origin.x;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
        }else{
            xValue = xValue + renderObj.relativeFrame.origin.x;
            yValue = yValue + renderObj.relativeFrame.origin.y;
            CGRect rederRct = renderObj.renderFrame;
            rederRct = CGRectMake(xValue, yValue, rederRct.size.width, rederRct.size.height);
            renderObj.renderFrame = rederRct;
            yValue = renderObj.renderFrame.origin.y + rederRct.size.height;
            
            CZLogRect(rederRct);
        }
        renderObj.renderOrder = renderObj.renderOrder + i;
        
        CGRect rederRct = renderObj.renderFrame;
        CGPoint rightBottomPoint = CGPointMake(renderObj.renderFrame.origin.x +rederRct.size.width,
                                               renderObj.renderFrame.origin.y + rederRct.size.height);
        
        if (!CGRectContainsPoint(_renderFrame, renderObj.renderFrame.origin)
            && !CGRectContainsPoint(_renderFrame, rightBottomPoint)) {
            [_headerElements removeObject:renderObj];
        }
    }
    _isResort = YES;
}

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    [self reSortOrderForRTF];

    if (!_headerElements || [_headerElements count]<1) {
        return;
    }
    
    [_RTFOutputStream writeString:@"{\\header\\viewkind4\\viewscale100\\uc1\\pard\\f0\\fs20"];
    for (NSUInteger i = 0; i< [_headerElements count]; i++) {
        CZRenderBase *renderObj = [_headerElements objectAtIndex:i];
        CGRect currentRenderRct = renderObj.renderFrame;
        CZLogRect(currentRenderRct);
        CGFloat offsetX = currentRenderRct.origin.x < 0 ? 0:currentRenderRct.origin.x;
        renderObj.renderFrame = CGRectMake(offsetX,
                                           currentRenderRct.origin.y,
                                           currentRenderRct.size.width,
                                           currentRenderRct.size.height);
        [renderObj rtfRenderIntoStream:_RTFOutputStream];
        if ([renderObj isKindOfClass:[CZLineRender class]]) {
            /// in footer,line should be linebreak.
            //[_RTFOutputStream writeString:kLineBreak];
            /// in footer,line should be linebreak.
        }
    }
    [_RTFOutputStream writeString:@"}"];
    
}

- (void)dealloc{
    FreeObj(_headerElements);
    [super dealloc];
}

@end
