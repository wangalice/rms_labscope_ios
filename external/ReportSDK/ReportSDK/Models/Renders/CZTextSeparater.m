//
//  CZTextSeparater.m
//  ReportSDK
//
//  Created by Johnny on 3/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTextSeparater.h"
#import <CoreText/CoreText.h>
#import "CZDictionaryTool.h"
#import "CZPDFPage.h"
#import "CZTextRender.h"

@interface CZTextSeparater() {
    CZPDFPage *_containedPage;
    CZTextRender *_originalTextRender;
    NSMutableArray *_subTextRenders;
}

@property (nonatomic, retain) CZPDFPage *containedPage;
@property (nonatomic, retain) CZTextRender *originalTextRender;

@end

@implementation CZTextSeparater

@synthesize containedPage = _containedPage;
@synthesize originalTextRender = _originalTextRender;
@synthesize subTextRenders = _subTextRenders;

- (id)initWithTextRender:(CZTextRender *)textRender page:(CZPDFPage *)pdfPage {
    self = [super init];
    if (self) {
        self.containedPage = pdfPage;
        self.originalTextRender = textRender;
        assert(_originalTextRender.canBreak == YES);
        _subTextRenders = [[NSMutableArray alloc] initWithCapacity:3];
    }
    return self;
}

- (void)separateText {
    [_subTextRenders removeAllObjects];
    _allInNextPages = NO;
    
    const CGFloat pageHeight = _containedPage.pageBodyFrame.size.height;
    const CGFloat width = _originalTextRender.renderFrame.size.width;

    // create type setter
    UIFont *font = [self originalTextRendersFont];
    NSUInteger pageIndex = self.containedPage.pageIndex;
    NSString *text = _originalTextRender.text;
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:text
                                                                     attributes:@{NSFontAttributeName: font}];
    
    CTTypesetterRef typesetter = CTTypesetterCreateWithAttributedString((CFAttributedStringRef)attrString);
    [attrString release];
    
    // initialize values
    CGFloat pageTop = _containedPage.pageBodyFrame.origin.y;
    CGFloat pageValidHeight = floorf(pageTop + pageHeight - _originalTextRender.renderFrame.origin.y);
    CGFloat lastRenderBottom = _originalTextRender.renderFrame.origin.y - _originalTextRender.relativeFrame.origin.y;
    CFIndex start = 0, paragraphStart = 0;
    NSUInteger subRenderOrder = 0;
    CGFloat lastParagraphHeight = 0;
    
    while (paragraphStart < text.length) {
        CFIndex count = CTTypesetterSuggestLineBreak(typesetter, start, width);
        NSRange paragraphTextRange = NSMakeRange(paragraphStart, start + count - paragraphStart);
        NSString *paragraphText = [_originalTextRender.text substringWithRange:paragraphTextRange];
        CGFloat paragraphheight = [paragraphText sizeWithFont:font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)].height;
        paragraphheight = ceil(paragraphheight);
        
        if (paragraphheight > pageValidHeight || (start + count) >= text.length) {
            if (paragraphheight <= pageValidHeight) {
                start = text.length;
                lastParagraphHeight = paragraphheight;
            }
            
            if (start != paragraphStart) {  // add sub text render
                paragraphTextRange = NSMakeRange(paragraphStart, start - paragraphStart);
                paragraphText = [_originalTextRender.text substringWithRange:paragraphTextRange];
                NSDictionary *originalContent = _originalTextRender.renderContent;
                NSDictionary *value = [originalContent valueForKey:kValue];
                NSMutableDictionary *paragraphDic = [NSMutableDictionary dictionaryWithDictionary:originalContent];
                NSMutableDictionary *paragraphValue = [NSMutableDictionary dictionaryWithDictionary:value];
                [paragraphValue setValue:paragraphText forKey:kTextValue];
                [paragraphDic setObject:paragraphValue forKey:kValue];
                
                CGRect renderFrame = _originalTextRender.renderFrame;
                CGRect relativeFrame = _originalTextRender.relativeFrame;
                renderFrame.size.height = lastParagraphHeight;
                if (pageIndex != self.containedPage.pageIndex) {
                    CGFloat offsetY = pageTop - lastRenderBottom;
                    relativeFrame.origin.y = offsetY;
                    renderFrame.origin.y = pageTop;
                }
                if (subRenderOrder != 0) {
                    relativeFrame.origin.x = 0;
                }
                
                CZTextRender *paragraphTextRender = [[CZTextRender alloc] initWithContent:paragraphDic frame:renderFrame];
                
                renderFrame = paragraphTextRender.renderFrame;
                if (CGRectGetMaxY(renderFrame) > (pageTop + pageHeight)) {  // in case the text render is too big, reduce its size to page height.
                    renderFrame.size.height = pageTop + pageHeight - renderFrame.origin.y;
                    paragraphTextRender.renderFrame = renderFrame;
                }
                
                paragraphTextRender.relativeFrame = relativeFrame;
                paragraphTextRender.canBreak = NO;
                paragraphTextRender.renderOrder = _originalTextRender.renderOrder;
                paragraphTextRender.textSubOrder = subRenderOrder;
                subRenderOrder++;
                
                [_subTextRenders addObject:paragraphTextRender];
                lastRenderBottom = CGRectGetMaxY(paragraphTextRender.renderFrame);
                [paragraphTextRender release];

                paragraphStart = start;
            } else {
                _allInNextPages = YES;
            }
            
            // go to next page
            pageIndex++;
            pageValidHeight = self.containedPage.pageBodyFrame.size.height;
            pageTop += pageHeight;
        }
        
        lastParagraphHeight = paragraphheight;
        start += count;
    }
    
    CFRelease(typesetter);
}

- (NSArray *)subTextRenders {
    return _subTextRenders;
}

- (UIFont *)originalTextRendersFont {
    NSDictionary *renderContent = _originalTextRender.renderContent;
    NSDictionary *fontDict = [renderContent valueForKey:kFont];
    fontDict = [CZDictionaryTool validateFontDic:fontDict];
    if (!fontDict) {
        fontDict = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDict valueForKey:kFontSize] floatValue];
    if (renderFontSize < kMinTextFontSize) {
        renderFontSize = kMinTextFontSize;
    } else if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    
    NSString *fontName = [fontDict valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }

    return font;
}

- (void)dealloc {
    FreeObj(_containedPage);
    FreeObj(_originalTextRender);
    FreeObj(_subTextRenders);
    [super dealloc];
}

@end
