//
//  CZTableSeparater.m
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//


#import "CZTableSeparater.h"
#import "NSDictionary+MutableDeepCopy.h"

#define kSeparateMargin     5
#define kTableSubOrder      @"tableSubOrder"

@implementation CZTableSeparater

@synthesize containedPage = _containedPage;
@synthesize originalTableRender = _originalTableRender;
@synthesize subTableRenders = _subTableRenders;
@synthesize isAdjust = _isAdjust;

- (id)initWithTableRender:(CZTableRender *)tableRender page:(CZPDFPage *)pdfPage{
    self = [super init];
    if (self) {
        self.containedPage = pdfPage;
        self.originalTableRender = tableRender;
        _isAdjust = NO;
        _subTableRenders = [[NSMutableArray alloc]initWithCapacity:3];
    }
    return self;
}

/**
 * Divide the big table to small table,when the table is cross the page or the table is bigger than the pdf page size.
 */
- (void)separateTable {
    const CGFloat originalEvaluatesHeight = [_originalTableRender.evaluates count] > 0 ? _originalTableRender.tableEvaluateHeight : 0;
    const CGFloat pageHeight = _containedPage.pageBodyFrame.size.height;
    
    NSDictionary *originalContent = _originalTableRender.renderContent;
    NSDictionary *originalValue = [originalContent valueForKey:kValue];
    NSArray *originalTableValue = [originalValue valueForKey:kTableValue];
    NSUInteger remainingRows = [originalTableValue count];
    NSUInteger contentOffset = 0;
    
    // flags that indicate if current iterator need show title, caption or evaluation.
    BOOL hasTitle = _originalTableRender.tableTitleHeight > 0;
    BOOL hasCaption = NO;
    BOOL hasEvaluation = NO;
    
    NSUInteger subOrder = 1;
    NSUInteger pageIndex = self.containedPage.pageIndex;
    CGFloat pageTop = _containedPage.pageBodyFrame.origin.y;
    
    CGFloat lastRenderBottom = _originalTableRender.renderFrame.origin.y - _originalTableRender.relativeFrame.origin.y;
    while (!hasCaption) {
        CGFloat pageValidHeight;
        if (pageIndex == self.containedPage.pageIndex) {
            pageValidHeight = floorf(pageTop + pageHeight - _originalTableRender.renderFrame.origin.y - kTableMarginHeight * 2);
        } else {
            pageValidHeight = self.containedPage.pageBodyFrame.size.height - kTableMarginHeight * 2;
        }
        
        if (hasTitle) {
            pageValidHeight -= _originalTableRender.tableTitleHeight;
        }
        
        if (pageValidHeight < 1) {
            pageValidHeight = 1;
        }
        
        NSUInteger pageValidRows = floorf((pageValidHeight - 1) / _originalTableRender.tableCellHeight);
        if (pageValidRows >= 1) {
            pageValidRows--; // minus first row of table
        }
        
        if (pageValidRows >= remainingRows) {
            pageValidRows = remainingRows;
            
            pageValidHeight -= originalEvaluatesHeight;
            if (pageValidHeight < 1) {
                pageValidHeight = 1;
            }
            
            NSUInteger pageValidRowsConsiderEvaluations = floorf((pageValidHeight - 1) / _originalTableRender.tableCellHeight);
            if (pageValidRowsConsiderEvaluations >= 1) {
                pageValidRowsConsiderEvaluations--; // minus first row of table
            }
            
            if (pageValidRowsConsiderEvaluations >= remainingRows) {
                hasEvaluation = YES;
                
                pageValidHeight -= _originalTableRender.tableCaptionHeight;
                if (pageValidHeight < 1) {
                    pageValidHeight = 1;
                }
                
                NSUInteger pageValidRowsConsiderCaption = floorf((pageValidHeight - 1) / _originalTableRender.tableCellHeight);
                if (pageValidRowsConsiderCaption >= 1) {
                    pageValidRowsConsiderCaption--; // minus first row of table
                }
                if (pageValidRowsConsiderCaption >= remainingRows) {
                    hasCaption = YES;
                }
            }
        }
        
        if (!hasCaption && pageValidRows == remainingRows && pageValidRows > 0) {
            pageValidRows--;  // move last row to next page and show with evaluations and caption together.
            hasEvaluation = NO;
            pageValidHeight -= _originalTableRender.tableCellHeight;
        }
        
        if (pageValidRows > 0) {
            remainingRows -= pageValidRows;
            NSArray *pageArray = [originalTableValue subarrayWithRange:NSMakeRange(contentOffset, pageValidRows)];
            contentOffset += pageValidRows;
            
            // create render content for seperated render
            NSMutableDictionary *pageContent = [NSMutableDictionary dictionaryWithDictionary:originalContent];
            NSMutableDictionary *pageValue = [NSMutableDictionary dictionaryWithDictionary:originalValue];
            [pageValue setObject:pageArray forKey:kTableValue];
            [pageContent setObject:pageValue forKey:kValue];
            if (hasTitle) {
                hasTitle = NO;
            } else {
                [pageContent removeObjectForKey:kTitle];
            }
            
            if (!hasCaption) {
                [pageContent removeObjectForKey:kCaption];
            }
            
            CGRect pageTableRct = _originalTableRender.renderFrame;
            pageTableRct.size.height = pageValidHeight;
            if (pageIndex != self.containedPage.pageIndex) {
                pageTableRct.origin.y = pageTop;
            }
            
            // create seperated render
            CZTableRender * tableRender = [[CZTableRender alloc] initWithContent:pageContent
                                                                           frame:pageTableRct];
            tableRender.renderOrder = _originalTableRender.renderOrder;
            tableRender.tableSubOrder = subOrder;
            subOrder ++;
            tableRender.canBreak = NO;
            
            if ([_originalTableRender.evaluates count] > 0) {
                if (hasEvaluation) {
                    [tableRender setEvaluatesContent:_originalTableRender.evaluates];
                } else {
                    [tableRender hideTableEvaluate];
                }
            }
            
            if (pageIndex == self.containedPage.pageIndex) {
                CGRect relativeRect = tableRender.relativeFrame;
                relativeRect.origin.y = _originalTableRender.relativeFrame.origin.y;
                tableRender.relativeFrame = relativeRect;
            } else {
                CGRect relativeRect = tableRender.relativeFrame;
                CGFloat offsetY = pageTop - lastRenderBottom;
                relativeRect.origin.y = offsetY;
                tableRender.relativeFrame = relativeRect;
            }
            
            [_subTableRenders addObject:tableRender];
            [tableRender release];
            
            lastRenderBottom = CGRectGetMaxY(tableRender.renderFrame);
        } else {
            if (pageIndex == self.containedPage.pageIndex) {
                _isAdjust = YES;
            } else {
                break;  // whole page is too small to hold table
            }
        }
        
        // go to next page
        pageTop += _containedPage.pageBodyFrame.size.height;
        pageIndex++;
    }
}

- (void)separateMultiDataRender {
    if (!self.originalTableRender.hasMultiData) {
        return;
    }
    
    NSString *srcKeyPath = [NSString stringWithFormat:@"%@.%@.%@", kValue, kTableValue, kValues];
    NSString *dstKeyPath = [NSString stringWithFormat:@"%@.%@", kValue, kTableValue];
    NSArray *subValues = [[self.originalTableRender.renderContent valueForKeyPath:srcKeyPath] retain];
    
    NSUInteger i = 0;
    for (id subValue in subValues) {
        id value = [subValue objectForKey:kValue];
        
        NSMutableDictionary *content = [self.originalTableRender.renderContent mutableDeepCopy];
        [content setValue:value forKeyPath:dstKeyPath];
        if (i != 0) {
            [content setValue:@(kSeparateMargin) forKeyPath:@"frame.y"];
        }
        
        BOOL isFirst = (i == 0);
        BOOL isLast = (i == subValues.count - 1);
        
        if (isFirst && isLast) {
            // do nothing
        } else if (isFirst) {
            [content removeObjectForKey:kCaption];
        } else if (isLast) {
            [content removeObjectForKey:kTitle];
        } else {
            [content removeObjectForKey:kTitle];
            [content removeObjectForKey:kCaption];
        }
        
        CZTableRender *tableRender = [self generateSubRender:content first:(i == 0)];
        [content release];
        
        [self.subTableRenders addObject:tableRender];
        i++;
    }
    
    [subValues release];
}

- (NSArray *)subRenders {
    return _subTableRenders;
}

- (NSMutableArray *)subTableRenders {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kTableSubOrder ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_subTableRenders sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    return _subTableRenders;
}

- (void)dealloc{
    FreeObj(_containedPage);
    FreeObj(_originalTableRender);
    FreeObj(_subTableRenders);
    [super dealloc];
}

#pragma mark - Privates

- (CZTableRender *)generateSubRender:(NSDictionary *)content first:(BOOL)first {
    CZTableRender *newSubRender = [[CZTableRender alloc] initWithContent:content
                                                                   frame:self.originalTableRender.relativeFrame];
    if (!first) {
        CGRect frame = newSubRender.relativeFrame;
        frame.origin = CGPointMake(0, kSeparateMargin);
        newSubRender.relativeFrame = frame;
    }
    
    newSubRender.renderOrder = self.originalTableRender.renderOrder;
    
    return [newSubRender autorelease];
    
}

@end
