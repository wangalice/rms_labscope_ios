//
//  CZLineRender.m
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZLineRender.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"
#import "CZRTFTool.h"

@implementation CZLineRender

@synthesize isInBody;

- (CGSize)preRenderLayout{
    return _renderFrame.size;
}

- (void)render{
    [super render];
    [self preRenderLayout];
    NSDictionary *color = [_renderContent valueForKey:kColor];
    if (!color || ![color isKindOfClass:[NSDictionary class]]) {
        color = [CZDictionaryTool defaultColor];
    }
    CGFloat r = [[color valueForKey:kRedColor] floatValue];
    CGFloat g = [[color valueForKey:kGreenColor] floatValue];
    CGFloat b = [[color valueForKey:kBlueColor] floatValue];
    CGFloat a = [[color valueForKey:kAlphaColor] floatValue];

    CGContextSetRGBFillColor(_currentContext, r, g, b, a);
    CGFloat lineHeight = _renderFrame.size.height;
    
    
    CGContextSetLineWidth(_currentContext, lineHeight);
    
    CGContextSetStrokeColorWithColor(_currentContext, [UIColor colorWithRed:r green:g blue:b alpha:a].CGColor);
    
    CGPoint startPoint = _renderFrame.origin;
    CGPoint endPoint = CGPointMake(_renderFrame.origin.x+_renderFrame.size.width, _renderFrame.origin.y);
    
    CGContextBeginPath(_currentContext);
    CGContextMoveToPoint(_currentContext, startPoint.x, startPoint.y);
    CGContextAddLineToPoint(_currentContext, endPoint.x, endPoint.y);
    
    CGContextClosePath(_currentContext);
    CGContextDrawPath(_currentContext, kCGPathFillStroke);
    CZLog(@"render end of class %@",[self debugDescription]);
    return;
}

#pragma mark -
#pragma mark RTF render methods

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    NSDictionary *color = [_renderContent valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];
    
    CGFloat r = [[color valueForKey:kRedColor] floatValue];
    CGFloat g = [[color valueForKey:kGreenColor] floatValue];
    CGFloat b = [[color valueForKey:kBlueColor] floatValue];
    int32_t dpRed = 255 * r;
    int32_t dpGreen = 255 * g;
    int32_t dpBlue = 255 * b;
    int32_t fillColor = (dpRed | dpGreen << 8 | dpBlue << 16);
    
    _renderFrame.origin.x = MAX(_renderFrame.origin.x, 0);
    _renderFrame.origin.y = MAX(_renderFrame.origin.y, 0);
    
    int32_t xTwips = (int32_t)_renderFrame.origin.x * kPixelToTwips * kPdfToRTFScale;
    int32_t yTwips = ceilf(_renderFrame.origin.y * kPixelToTwips * kPdfToRTFScale);
    int32_t widthTwips = ceilf(_renderFrame.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t heightTwips = ceilf(_renderFrame.size.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *shpleftStr = [NSString stringWithFormat:@"\\shpleft%d",xTwips];
    NSString *shptopStr = [NSString stringWithFormat:@"\\shptop%d",yTwips];
    NSString *shprightStr = [NSString stringWithFormat:@"\\shpright%d",widthTwips + xTwips];
    NSString *shpbottomStr = [NSString stringWithFormat:@"\\shpbottom%d",heightTwips + yTwips];
    
    if (self.isInBody) {
       [_RTFOutputStream writeString:@"{\\pard{\\shp\\shpbxcolumn\\shpbymargin"];
    }else{
       [_RTFOutputStream writeString:@"{\\pard{\\shp\\shpbxcolumn\\shpbypage"];
    }
    
    [_RTFOutputStream writeString:shpleftStr];
    [_RTFOutputStream writeString:shptopStr];
    [_RTFOutputStream writeString:shprightStr];
    [_RTFOutputStream writeString:shpbottomStr];
    
    [_RTFOutputStream writeString:[NSString stringWithFormat:@"{\\*\\shpinst{\\sp{\\sn fillColor}{\\sv %d}}{\\sp{\\sn ShapeType}{\\sv 1}}}", fillColor]];
    
    [_RTFOutputStream writeString:@"}\\par}"];
    
    [_RTFOutputStream writeString:kLineBreak];
    
    return;
}

- (void)dealloc{
    [super dealloc];
}

@end
