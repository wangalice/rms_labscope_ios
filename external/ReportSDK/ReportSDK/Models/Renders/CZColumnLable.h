//
//  CZColumnLable.h
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CZColumnLable : NSObject{
	int32_t			x;
	int32_t			y;
	int32_t			width;		///used wide
	int32_t			height;		///used height
	NSString            *label;		///show title
}

@property(nonatomic, retain) NSString *label;
@property(nonatomic, assign) int32_t x;
@property(nonatomic, assign) int32_t y;
@property(nonatomic, assign) int32_t width;
@property(nonatomic, assign) int32_t height;

@end