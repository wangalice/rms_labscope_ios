//
//  CZColumnItem.m
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZColumnItem.h"

@implementation CZColumnItem

@synthesize lable;
@synthesize currentValue;

-(id)init{
	self=[super init];
    if (self) {
      values=[[NSMutableArray alloc] init];
    }
	return self;
}

-(id)initWithDouble:(double)value{
	self=[super init];
    if (self) {
        currentValue = value;
        values=[[NSMutableArray alloc] init];
        [self addData:value];
    }
	return self;
}


-(void)addData:(double)value{
	NSNumber *obj=[[NSNumber alloc] initWithDouble:value];
	[values addObject:obj];
    [obj release];
    obj = nil;
}

-(void)removeAt:(int)index{
	if(index<values.count){
		[values removeObjectAtIndex:index];
	}
}

-(double)objectAtIndex:(int)index{
	if(index<values.count){
		return [[values objectAtIndex:index] doubleValue];
	}
	else {
		return 0;
	}
}

-(NSUInteger)count{
	if (values){
		return values.count;
	}
	else {
		return 0;
	}
}

-(void)dealloc{
    FreeObj(values);
    FreeObj(lable);
    FreeObj(_color);
    [super dealloc];
}


@end
