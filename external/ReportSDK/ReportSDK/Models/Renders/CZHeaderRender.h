//
//  CZHeaderRender.h
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"
#import "CZLineRender.h"
#import "CZTextRender.h"

@interface CZHeaderRender : CZRenderBase{
    /// _headerElement contain all the sub element of header
    NSMutableArray      *_headerElements;
    BOOL                 _isResort;
}

@property (nonatomic, readonly) NSMutableArray *headerElements;


@end
