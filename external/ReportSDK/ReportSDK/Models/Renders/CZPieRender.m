//
//  CZPieRender.m
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPieRender.h"
#import "NSOutputStream+String.h"
#import "CZTextRender.h"
#import "CZPieComponent.h"
#import "CZDictionaryTool.h"
#import "CZRTFTool.h"
#import "CZRTFPreviewConfig.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZNumberTool.h"

#define kTitlePieMargin 1
#define kLableTopMargin 15
#define kArrowHeadLength 6
#define kArrowHeadWidth 4
#define kCircleRectMargin 15

#define kTITLEHEIGHT            24
#define kTITLEBOTTOMMARGIN      3
#define kTITLEIMAGIMARGIN       3

#define kSegmentMaxNum          6
#define kRTFRenderWidthScale    1.0

static const CGFloat kLabelFontSize = 9;  // font size
static const CGFloat kColorPatchSize = 8;

@interface CZPieRender () {
    
    NSMutableArray  *_pieComponents;
    
    CGFloat          _diameter;
    BOOL             _sameColorText;
    
    NSString         *_title;  // assign
    
    BOOL              _isShowBiggestData;
    
    CGFloat           _biggestDataValue;
}

@property (nonatomic, copy) NSString *unitString;

- (void)bigSegmentFilter;

- (UIColor *)getColorFromString:(NSString *)colorDes;

- (UIColor *)getColorFromTemplate:(NSUInteger) aIndex;

@end

@implementation CZPieRender
@synthesize sameColorText = _sameColorText;       

+ (UIColor *)randomColor {
    CGFloat srandValueR = arc4random() % 256;
    CGFloat r = srandValueR / 255;
    
    CGFloat srandValueG = arc4random() % 256;
    CGFloat g = srandValueG / 255;
    
    CGFloat srandValueB = arc4random() % 256;
    CGFloat b = srandValueB / 255;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}

/**
 * pieData structure: key      value      valueType
 *
 * pie component predifined colors
 + (UIColor *)redColor;        // 1.0, 0.0, 0.0 RGB
 + (UIColor *)greenColor;      // 0.0, 1.0, 0.0 RGB
 + (UIColor *)blueColor;       // 0.0, 0.0, 1.0 RGB
 + (UIColor *)cyanColor;       // 0.0, 1.0, 1.0 RGB
 + (UIColor *)yellowColor;     // 1.0, 1.0, 0.0 RGB
 + (UIColor *)magentaColor;    // 1.0, 0.0, 1.0 RGB
 + (UIColor *)orangeColor;     // 1.0, 0.5, 0.0 RGB
 + (UIColor *)purpleColor;     // 0.5, 0.0, 0.5 RGB
 + (UIColor *)brownColor;      // 0.6, 0.4, 0.2 RGB
 */

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        CZLogRect(aFrame);
        _canBreak = NO;
        NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
        if ([pieTitle isKindOfClass:[NSDictionary class]]) {
            _title =[pieTitle valueForKey:kTitleText];
            if (![_title isKindOfClass:[NSString class]]) {
                _title = nil;
            }
        }
       
        _isShowBiggestData = YES;
        NSDictionary *pieValue = [self.renderContent valueForKey:kValue];
        id largeShowId = [pieValue valueForKey:kIsLargestSegmentShow];
        if (largeShowId && [largeShowId isKindOfClass:[NSNumber class]]) {
            if((strcmp([largeShowId objCType], @encode(char))) == 0) {
              _isShowBiggestData = [largeShowId boolValue];  
            }
        }
        _biggestDataValue = 0.f;
        
        id obj = [pieValue valueForKey:kPieComponentArray];
        if ([obj isKindOfClass:[NSArray class]]) { // data format version 0
            [self parseComponentsFromArray:obj];
            [self bigSegmentFilter];
            [self sortComponentsAndAssignColor];
        } else if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *complexValue = (NSDictionary *)obj;
            id version = complexValue[kVersion];
            if ([@1 isEqual:version]) {  // data format version 0
                NSArray *valuesArray = complexValue[kValues];
                if (valuesArray.count == 1) {
                    NSArray *componentsDicArray = [valuesArray[0] valueForKey:kValue];
                    [self parseComponentsFromArray:componentsDicArray];
                    
                    id unitString = [valuesArray[0] valueForKey:kUnit];
                    if ([unitString isKindOfClass:[NSString class]]) {
                        _unitString = [unitString copy];
                    }
                    
                    [self bigSegmentFilter];
                    [self sortComponentsAndAssignColor];
                } else {  // multi-data source, which need seperate during render
                    _hasMultiData = YES;
                }
            } else {  // error data format version number
                [self release];
                self = nil;
            }
        }
    }
    return self;
}

- (void)sortComponentsAndAssignColor{
    if (!_pieComponents && [_pieComponents count]<1) {
        return;
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kValue
                                                                   ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                          count:1];
    [_pieComponents sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);

    for (NSUInteger i = 0; i< [_pieComponents count]; i++) {
        CZPieComponent *component = [_pieComponents objectAtIndex:i];
        if (component.colour == nil) {
            component.colour = [self getColorFromTemplate:i];
        }
        
        if (component.colour == nil) {
            component.colour = [CZPieRender randomColor];
        }
    }
}

- (UIColor *)getColorFromTemplate:(NSUInteger) aIndex{
    UIColor *templateColor = nil;
    NSArray *pieColors = [_renderContent valueForKey:kPieColors];
    if ([pieColors isKindOfClass:[NSArray class]]) {
        if (pieColors && [pieColors count]>0 && aIndex < [pieColors count]) {
            NSString *colorDes = [pieColors objectAtIndex:aIndex];
            if ([colorDes isKindOfClass:[NSString class]]) {
                templateColor = [self getColorFromString:colorDes];
            }
        }
    }
    return templateColor;
}

- (UIColor *)getColorFromString:(NSString *)colorDes{
    if ([colorDes isEqualToString:kRed]) {
        return [UIColor redColor];
    }else if([colorDes isEqualToString:kBlue]){
        return [UIColor blueColor];
    }else if([colorDes isEqualToString:kGreen]){
        return [UIColor greenColor];
    }else if ([colorDes isEqualToString:kCyan]){
        return [UIColor cyanColor];
    }else if ([colorDes isEqualToString:kYellow]){
        return [UIColor yellowColor];
    }else if ([colorDes isEqualToString:kMagenta]){
        return [UIColor magentaColor];
    }else if ([colorDes isEqualToString:kOrange]){
        return [UIColor orangeColor];
    }else if ([colorDes isEqualToString:kPurple]){
        return [UIColor purpleColor];
    }else if ([colorDes isEqualToString:kBrown]){
        return [UIColor brownColor];
    }
    return nil;
}

- (void)bigSegmentFilter{
    assert([_pieComponents count]>0);
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kValue
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_pieComponents sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    _biggestDataValue = 0.f;
    if (!_isShowBiggestData) {
        CZPieComponent *biggestComponent = [_pieComponents objectAtIndex:[_pieComponents count]-1];
        _biggestDataValue = biggestComponent.value;
        [_pieComponents removeObjectAtIndex:[_pieComponents count]-1];
    }
    
    /// the following code is just for the situation:there are too many data
    if ([_pieComponents count] > kSegmentMaxNum) {
        CGFloat otherValue = 0.f;
        NSMutableIndexSet *indexes = [[NSMutableIndexSet alloc]init];
        for (NSUInteger i = kSegmentMaxNum - 1; i<[_pieComponents count]; i++) {
            CZPieComponent *leftComponent = [_pieComponents objectAtIndex:i];
            otherValue += leftComponent.value;
            [indexes addIndex:i];
        }
        [_pieComponents removeObjectsAtIndexes:indexes];
        FreeObj(indexes);
        CZPieComponent *otherComponent = [[CZPieComponent alloc]initWithTitle:@"Others" value:otherValue];
        [_pieComponents addObject:otherComponent];
        FreeObj(otherComponent);
    }
}


- (CGSize)preRenderLayout{
    CGSize renderSize = [super preRenderLayout];
    
    CGSize titleRenderSize = [self titleSize];

    CZLogSize(titleRenderSize);
    CGRect circleRect = CGRectMake(_renderFrame.origin.x,
                                   _renderFrame.origin.y + titleRenderSize.height
                                   + kTitlePieMargin,
                                   _renderFrame.size.width,
                                   _renderFrame.size.height -
                                   titleRenderSize.height - kTitlePieMargin);
    circleRect = CGRectMake(circleRect.origin.x, circleRect.origin.y, circleRect.size.width, circleRect.size.width*2/3);
    CZLogRect(circleRect);

    renderSize = CGSizeMake(renderSize.width, renderSize.height + circleRect.size.height + kTitlePieMargin);
    CZLogRect(circleRect);

    return renderSize;
}

/// keep left render
- (CGSize)titleSize{
    CGSize titleRenderSize = CGSizeZero;
    if (_title) {
        NSDictionary *imageTitle = [self.renderContent valueForKey:kTitle];
        
        NSDictionary *fontDic = [imageTitle valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];

        CGFloat r = [[color valueForKey:kRedColor] floatValue];
        CGFloat g = [[color valueForKey:kGreenColor] floatValue];
        CGFloat b = [[color valueForKey:kBlueColor] floatValue];
        CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
        //NSInteger titleWidth=[_title sizeWithFont:font].width;
        CGFloat titleWidth = [CZDictionaryTool textSize:_title useFont:font].width;

        titleRenderSize = CGSizeMake(titleWidth, kTITLEHEIGHT);
    }
    return titleRenderSize;
}


- (void)render{
    [super render];
    
    /// save and then clip
    CGRect clipRct = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y, _renderFrame.size.width * kRTFRenderWidthScale, _renderFrame.size.height);
    CGContextSaveGState(_currentContext);
    CGContextClipToRect(_currentContext, clipRct);
    
    [self renderPDFPieChart];
 
    [self renderPDFPieTitle];
    
    /// compare with begin CGContextSaveGState and clip
    CGContextRestoreGState(_currentContext);
    CZLog(@"render end of class %@",[self debugDescription]);

    return;
}

- (void)renderPDFPieChart {
    if ([_pieComponents count] == 0) {
        return;
    }
    
    const CGFloat kCircleRectRatio = 0.6;
    
    CGSize titleRenderSize = [self titleSize];
    CGRect circleRect = CGRectMake(_renderFrame.origin.x,
                                   _renderFrame.origin.y+ titleRenderSize.height+ kTitlePieMargin,
                                   _renderFrame.size.width,
                                   _renderFrame.size.height- titleRenderSize.height - kTitlePieMargin);
    if (circleRect.size.width * kCircleRectRatio < circleRect.size.height) {
        circleRect = CGRectMake(circleRect.origin.x, circleRect.origin.y, circleRect.size.width, circleRect.size.width * kCircleRectRatio);
    }
    CGRect rect = circleRect;
    
    CZLogRect(rect);
    _diameter = MIN(rect.size.width, rect.size.height) - 2*kCircleRectMargin;
    float x = _renderFrame.origin.x;
    float y =_renderFrame.origin.y + titleRenderSize.height+ kTitlePieMargin +(rect.size.height - _diameter)/2;
    
    float gap = 1;
    float inner_radius = _diameter/2;
    float origin_x = x + _diameter/2;
    float origin_y = y + _diameter/2;

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kValue
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_pieComponents sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    
    
    float total = 0;
    for (CZPieComponent *component in _pieComponents){
        total += component.value;
    }
    
    UIGraphicsPushContext(_currentContext);
    CGContextSetRGBFillColor(_currentContext, 1.0f, 1.0f, 1.0f, 1.0f);  // white color
    CGContextFillEllipseInRect(_currentContext, CGRectMake(x, y, _diameter, _diameter));  // a white filled circle with a diameter of 100 pixels, centered in (60, 60)
    UIGraphicsPopContext();
    
    // draw pies
    float nextStartDeg = 0;
    float endDeg = 0;
    NSMutableArray *tmpComponents = [NSMutableArray array];
    int last_insert = -1;
    for (int i=0; i<[_pieComponents count]; i++){
        CZPieComponent *component  = [_pieComponents objectAtIndex:i];
        float perc = [component value] / total;
        endDeg = nextStartDeg + perc * 360;
        
        CGContextSetFillColorWithColor(_currentContext, [component.colour CGColor]);
        CGContextMoveToPoint(_currentContext, origin_x, origin_y);
        CGContextAddArc(_currentContext, origin_x, origin_y, inner_radius, (nextStartDeg - 90) * M_PI / 180.0, (endDeg - 90) * M_PI / 180.0, 0);
        CGContextClosePath(_currentContext);
        CGContextFillPath(_currentContext);
        
        CGContextSetRGBStrokeColor(_currentContext, 1, 1, 1, 1);
        CGContextSetLineWidth(_currentContext, gap);
        CGContextMoveToPoint(_currentContext, origin_x, origin_y);
        CGContextAddArc(_currentContext, origin_x, origin_y, inner_radius, (nextStartDeg - 90) * M_PI / 180.0, (endDeg - 90) * M_PI / 180.0, 0);
        CGContextClosePath(_currentContext);
        CGContextStrokePath(_currentContext);
        
        [component setStartDegrees:nextStartDeg];
        [component setEndDegrees:endDeg];
        
        if (nextStartDeg < 180) {
            [tmpComponents addObject:component];
        } else {
            if (last_insert == -1) {
                last_insert = i;
                [tmpComponents addObject:component];
            } else {
                [tmpComponents insertObject:component atIndex:last_insert];
            }
        }
        
        nextStartDeg = endDeg;
    }
    
    // draw labels
    // label stuff
    CGFloat text_y =_renderFrame.origin.y+ titleRenderSize.height+ kTitlePieMargin + kLableTopMargin;
    const CGFloat text_x = x + _diameter + 10 + kColorPatchSize + 5;
    const CGFloat textMaxWidth = CGRectGetMaxX(_renderFrame) - text_x;
    
    CGFloat leftValue = 0.f;
    NSUInteger i = 0;
    CGContextSetRGBStrokeColor(_currentContext, 1, 1, 1, 1);
    CGContextSetLineWidth(_currentContext, 1);
    
    UIFont *piePercentageFont = [UIFont systemFontOfSize:kLabelFontSize];
    UIFont *pieBoldFont = [UIFont boldSystemFontOfSize:kLabelFontSize];
    
    for (CZPieComponent *component in tmpComponents) {
        // render label at right
        // display percentage label
        if (self.sameColorText) {
            CGContextSetFillColorWithColor(_currentContext, [component.colour CGColor]);
        } else {
            CGContextSetRGBFillColor(_currentContext, 0.1f, 0.1f, 0.1f, 1.0f);
        }
        
        CGFloat percentageValue = component.value / (total + _biggestDataValue) * 100;
        if (i == [tmpComponents count] - 1 && _biggestDataValue < 0.00001) {
            percentageValue = 100 - leftValue;
        }
        
        percentageValue = roundf(percentageValue * 10) * 0.1;
        leftValue += percentageValue;
        CZLog(@"the percent float value is %.1f", percentageValue);

        ///The following is use localized number.
        NSString *componentValue;
        if (self.unitString == nil) {
            NSString *percentageValueStr = [CZNumberTool localizedStringFromFloat:percentageValue precision:1U];
            componentValue = [NSString stringWithFormat:@"%@%%", percentageValueStr];
        } else {
            NSString *valueStr = [CZNumberTool localizedStringFromFloat:component.value precision:1U];
            componentValue = [NSString stringWithFormat:@"%@%@", valueStr, self.unitString];
        }
        CGSize componentValueSize = [CZDictionaryTool textSize:componentValue useFont:piePercentageFont];
        
        NSString *componentTitle = [NSString stringWithFormat:@"%@ ", component.title];
        CGSize componentTitleSize = [CZDictionaryTool textSize:componentTitle useFont:pieBoldFont];
        componentTitleSize.width = MIN(componentTitleSize.width, textMaxWidth - componentValueSize.width);
        [componentTitle drawAtPoint:CGPointMake(text_x, text_y)
                           forWidth:componentTitleSize.width
                           withFont:pieBoldFont
                      lineBreakMode:NSLineBreakByTruncatingMiddle];

        [componentValue drawAtPoint:CGPointMake(text_x + componentTitleSize.width, text_y) withFont:piePercentageFont];
        
        CGSize additionalTextSize = CGSizeZero;
        if (component.additionalValues.count) {
            componentValue = @"";
            
            for (id additionalValue in component.additionalValues) {
                NSString *additionalValueString;
                if ([additionalValue isKindOfClass:[NSString class]]) {
                    additionalValueString = additionalValue;
                } else {
                    additionalValueString = [additionalValue description];
                }
                
                if (additionalValueString) {
                    if ([componentValue length]) {
                        componentValue = [componentValue stringByAppendingString:@"\n"];
                    }
                    componentValue = [componentValue stringByAppendingString:additionalValueString];
                }
            }

            additionalTextSize = [CZDictionaryTool textSize:componentValue
                                                    useFont:piePercentageFont
                                             constraintSize:CGSizeMake(textMaxWidth, 100)];
            
            CGRect percFrame = CGRectMake(text_x, text_y + componentTitleSize.height, additionalTextSize.width, additionalTextSize.height);
            [componentValue drawInRect:percFrame withFont:piePercentageFont];
        }
        
        // draw color patch
        CGFloat colorPatchX = x + _diameter + 10;
        CGFloat colorPatchY = text_y + (componentTitleSize.height - kColorPatchSize) / 2;
        CGRect colorPatchRect = CGRectMake(colorPatchX, colorPatchY, kColorPatchSize, kColorPatchSize);
        CGContextSetFillColorWithColor(_currentContext, [component.colour CGColor]);
        CGContextFillRect(_currentContext, colorPatchRect);
        
        text_y += additionalTextSize.height + componentTitleSize.height + 2;
        i++;
    }
}

- (void)renderPDFPieTitle{
    if (_title) {
        NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
        
        NSDictionary *fontDic = [pieTitle valueForKey:kFont];
        if (!fontDic ||![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat r = [[color valueForKey:kRedColor] floatValue];
        CGFloat g = [[color valueForKey:kGreenColor] floatValue];
        CGFloat b = [[color valueForKey:kBlueColor] floatValue];
        CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
        //NSInteger titleWidth=[_title sizeWithFont:font].width;
        CGFloat titleWidth = [CZDictionaryTool textSize:_title useFont:font].width;
        
        CGRect titleRct = CGRectMake(self.renderFrame.origin.x + (_renderFrame.size.width - titleWidth)/2,
                                     _renderFrame.origin.y, titleWidth, kTITLEHEIGHT);
        [_title drawInRect:titleRct withFont:font
             lineBreakMode:NSLineBreakByWordWrapping
                 alignment:NSTextAlignmentLeft];
    }

}

#pragma mark -
#pragma mark RTF render methods

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    BOOL isRTF = [[CZRTFPreviewConfig sharedInstance] isForRTF];
    
    if (isRTF) {
        [self renderRTFContent];
    }else{
        [self renderRTFDContent];
    }
    return;
}


- (void)renderRTFContent{@autoreleasepool{
    /// convert the render content to image,and convert the image data to ASIC code
    NSString *imageRTFStr = nil;
    //[_RTFOutputStream writeString:frameStr];
    NSString *textBoxPre = [CZRTFTool textBoxPre];
    NSString *textBoxEnd = [CZRTFTool textBoxEnd:_renderFrame];
    [_RTFOutputStream writeString:textBoxPre];
    
    [self generateRTFPieTitle];
    
    /// followed is generate the pie image rtf content.
    /// Note: the title is in top of image.
    UIImage *image = [self generateRTFImage];
    if (!image) {
        [_RTFOutputStream writeString:textBoxEnd];
        return;
    }
    /** 
        if use JPEG compressing,should use the "\\jpegblip" format word.
     *  if use PNG compressing,should use the the "\\pngblip" format word.
     */
    NSData *imageData = UIImageJPEGRepresentation(image,kJpegCompressPercent);
    //NSData *imageData = UIImagePNGRepresentation(image);
    imageRTFStr = [CZRTFTool convertImageData2String:imageData];
    
    int32_t rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,rtfWide,rtfHeight];
    
    /**
     begin new added for set the location default is center.(cause the element 
     interval 1 linebreak the template defined.
     */
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
    [_RTFOutputStream writeString:@"\\jpegblip "];
    [_RTFOutputStream writeString:imageRTFStr];
    [_RTFOutputStream writeString:@"}"];
    [_RTFOutputStream writeString:kLineBreak];
    
    [_RTFOutputStream writeString:textBoxEnd];
}}

    /** 
       make the width scaled 5/4 ,special for RTF and RTFD render,because the component title infomation can not be clipped.
     */
- (UIImage *)generateRTFImage{
    UIGraphicsBeginImageContext(CGSizeMake(_renderFrame.size.width *kRTFRenderWidthScale, _renderFrame.size.height));
    _renderFrame = CGRectMake(0, 0, _renderFrame.size.width*kRTFRenderWidthScale, _renderFrame.size.height);
    [self render];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)generateRTFPieTitle{
    if (!_title) {
        return;
    }
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *pieTitle = [self.renderContent valueForKey:kTitle];
    
    NSDictionary *fontDic = [pieTitle valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance]readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
    
    CZLogRect(_renderFrame);

    
//    [_RTFOutputStream writeString:@"\\par"];
//    [_RTFOutputStream writeString:@"\\pard"];
   
    ///begin new added for set the location default is center.
//    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_title];
    [_RTFOutputStream writeString:unicodeStr];
    //CZLog(@"The text rtf string is %@",_RTFAttributeStr);
    
    _title = nil;
}


#pragma mark -
#pragma mark RTFD render methods

- (void)renderRTFDContent{@autoreleasepool{
    /// convert the render content to image,and convert the image data to ASIC code
    NSString *frameStr = [CZRTFTool offsetFrame2RTFString:_renderFrame];
    
    NSString *imageRTFStr = nil;
    UIImage *image = [self generateRTFImage];
    if (!image) {
        return;
    }
  
    NSData *imageData = UIImageJPEGRepresentation(image,kJpegCompressPercent);
    //NSData *imageData = UIImagePNGRepresentation(image);
    imageRTFStr = [[CZRTFPreviewConfig sharedInstance] convertImageData2ImageFile:imageData];
    
    int32_t rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
    int32_t rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{{\\NeXTGraphic %@ \\width%d \\height%d \\noorient}%@}",imageRTFStr,rtfWide,rtfHeight,[CZRTFTool getNSStringUnicode:@"¨"]];
    
    [_RTFOutputStream writeString:frameStr];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
    [_RTFOutputStream writeString:kLineBreak];
}}


- (void)dealloc{
    FreeObj(_unitString);
    FreeObj(_pieComponents);
    [super dealloc];
}

#pragma mark - Privates

- (void)parseComponentsFromArray:(NSArray *)componentsDicArray {
    if ([componentsDicArray count]) {
        [_pieComponents release];
        _pieComponents = [[NSMutableArray alloc] initWithCapacity:componentsDicArray.count];
        
        NSUInteger i = 0;
        for (NSDictionary *componentDic in componentsDicArray) {
            CZPieComponent *component = [[CZPieComponent alloc] initWithPicComponentDic:componentDic];
            if (component.colour == nil) {
                component.colour = [self getColorFromTemplate:i];
            }
            
            if (component.colour == nil) {
                component.colour = [CZPieRender randomColor];
            }
            
            [_pieComponents addObject:component];
            [component release];
            
            i++;
        }
    }
}
@end
