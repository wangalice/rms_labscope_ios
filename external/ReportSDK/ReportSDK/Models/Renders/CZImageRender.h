//
//  CZImageRender.h
//  ReportSDK
//
//  Created by Johnny on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"

typedef NS_ENUM(NSUInteger, CZImageRenderMode) {
    kCZImageRenderModeNormal,
    kCZImageRenderModeTile,
    kCZImageRenderModeMultipage,
    
    kCZImageRenderModePendding  // Do not use this mode, it is a temporal mode for layout
};

@interface CZImageRender : CZRenderBase

/** Image to render; the string is a image file full path or
 *  base64 encoded string of image (JPG or PNG) data.
 */
@property(nonatomic, copy, readonly) NSString *imagePath;

/** array of the complex image objects in NSDictionary. the image schema is like,
 * @{ "value": @"file path",
 *    "imageCaption": @"Caption"}
 */
@property(nonatomic, retain, readonly) NSArray *images;

@property(nonatomic, assign) BOOL isInHeader;

@property(nonatomic, assign) CZImageRenderMode renderMode;

@end
