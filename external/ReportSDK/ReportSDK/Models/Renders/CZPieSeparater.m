//
//  CZPieSeparater.m
//  ReportSDK
//
//  Created by Ralph Jin on 2/18/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZPieSeparater.h"
#import "NSDictionary+MutableDeepCopy.h"

@interface CZPieSeparater () {
    CZPieRender *_originalRender;
}

@property (nonatomic, retain) NSMutableArray *subPieRenders;

@end

@implementation CZPieSeparater

- (id)initWithOriginalRender:(CZPieRender *)originalRender {
    self = [super init];
    if (self) {
        _originalRender = [originalRender retain];
        _subPieRenders = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_originalRender release];
    [_subPieRenders release];
    
    [super dealloc];
}

- (void)separateMultiDataRender {
    if (!_originalRender.hasMultiData) {
        return;
    }
    
    NSString *keyPath = [NSString stringWithFormat:@"%@.%@.%@", kValue, kPieComponentArray, kValues];
    NSArray *subValues = [[_originalRender.renderContent valueForKeyPath:keyPath] retain];
    
    NSUInteger i = 0;
    for (id subValue in subValues) {
        NSMutableDictionary *content = [_originalRender.renderContent mutableDeepCopy];
        [content setValue:@[subValue] forKeyPath:keyPath];
        
        CZPieRender *columnRender = [self generateSubRender:content first:(i == 0)];
        [content release];
        
        [self.subPieRenders addObject:columnRender];
        i++;
    }
    
    [subValues release];
}

- (NSArray *)subRenders {
    return _subPieRenders;
}

#pragma mark - private methods

- (CZPieRender *)generateSubRender:(NSDictionary *)content first:(BOOL)first {
    CZPieRender *newSubRender = [[CZPieRender alloc] initWithContent:content
                                                                     frame:_originalRender.relativeFrame];
    if (!first) {
        CGRect frame = newSubRender.relativeFrame;
        frame.origin = CGPointZero;
        newSubRender.relativeFrame = frame;
    }
    
    newSubRender.renderOrder = _originalRender.renderOrder;
    
    return [newSubRender autorelease];
    
}
@end
