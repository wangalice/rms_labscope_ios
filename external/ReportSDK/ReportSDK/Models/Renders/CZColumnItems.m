//
//  CZColumnItems.m
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZColumnItems.h"

@implementation CZColumnItems
@synthesize items = _items;

- (id)init{
	self=[super init];
	_items=[[NSMutableArray alloc] init];
	return self;
}

- (void)addData:(CZColumnItem *)value{
	[_items addObject:value];
}

- (CZColumnItem *)objectAtIndex:(NSUInteger)index{
	if(index<_items.count){
		return [_items objectAtIndex:index];
	}
	else {
		return nil;
	}
}

- (NSUInteger)count{
	if (_items){
		return _items.count;
	}
	else {
		return 0;
	}
}

-(void)dealloc{
    [_items release];
    _items = nil;
    [super dealloc];
}
@end
