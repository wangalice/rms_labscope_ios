//
//  CZColumnItem.h
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZColumnItem : NSObject{
@private
    ///every column title
	NSString		*lable;
    
    ///NSNumber type array,store the value that should be shown
	NSMutableArray	*values;
}

@property(nonatomic, retain)        NSString		*lable;
@property(nonatomic, readonly)      float           currentValue;
@property(nonatomic, retain)        UIColor         *color;

- (id)initWithDouble:(double)value;

- (void)addData:(double)value;
- (void)removeAt:(int)index;
- (double)objectAtIndex:(int)index;
- (NSUInteger)count;

@end
