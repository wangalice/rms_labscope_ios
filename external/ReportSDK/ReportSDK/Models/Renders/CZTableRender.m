//
//  CZTableRender.m
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableRender.h"
#import "NSOutputStream+String.h"
#import "CZTableColumn.h"
#import "CZTableColumnCellObj.h"
#import "CZDictionaryTool.h"
#import "CZTableEvaluate.h"
#import "CZRTFTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFPreviewConfig.h"

#define kRelativeSubEelementMargin  5
#define kSortOrderAttribute         @"_order"
#define kIsTableVisible             @"isVisible"

#define kHermesText                 @"Hermes"
#define kTableTitleMaxFontSize      24
#define kTableCaptionMinFontSize    6
#define kTableCaptionMaxFontSize    kTableTitleMaxFontSize
#define kTableCaptionTopMargin      8

@interface CZTableRender (){
    
    /// contain the all tableColumn object0
    NSMutableArray  *_tableColumns;
    
    /// _tableCells contain all the tableColumnCell object in the tableColumns
    NSMutableArray  *_tableCells;
    NSString        *_title;
    NSMutableArray  *_evaluates;
    NSString        *_caption;
    float         _evaluatesHeight;
    BOOL            _isShowTable;
}

@property (nonatomic, readwrite) float tableCellHeight;
@property (nonatomic, readwrite) float tableCaptionHeight;
@property (nonatomic, readwrite) float tableEvaluateHeight;
@property (nonatomic, retain) NSMutableArray *evaluates;

- (void)sortTableColumnByOrder;

@end

@implementation CZTableRender

@synthesize columnNumber;
@synthesize lineNumber;
@synthesize tableCellHeight;
@synthesize tableEvaluateHeight;
@synthesize tableCaptionHeight;
@synthesize tableSubOrder;
@synthesize evaluates = _evaluates;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        CZLogRect(aFrame);
        
        _canBreak = [[_renderContent valueForKey:kCanBreak] boolValue];
        
        NSDictionary *title = [self.renderContent valueForKey:kTitle];
        if ([title isKindOfClass:[NSDictionary class]]) {
            _title = [title valueForKey:kTitleText];
            if ([_title isKindOfClass:[NSString class]]) {
                _title = [_title copy];
            } else {
                _title = nil;
            }
        }
        
        NSDictionary *caption = [self.renderContent valueForKey:kCaption];
        if ([caption isKindOfClass:[NSDictionary class]]) {
            _caption = [caption valueForKey:kCaptionText];
            if ([_caption isKindOfClass:[NSString class]]) {
                _caption = [_caption copy];
            } else {
                _caption = nil;
            }
        }
        
        NSDictionary *rootValue = [self.renderContent valueForKey:kValue];
        id complexValue = [rootValue valueForKey:kTableValue];
        if ([complexValue isKindOfClass:[NSArray class]]) {  // data format version 0
            [self initWithOneContentArray:complexValue];
        } else if ([complexValue isKindOfClass:[NSDictionary class]]) {
            id version = [complexValue objectForKey:kVersion];
            if ([@1 isEqual:version]) {  // data format version 1
                NSArray *values = [complexValue objectForKey:kValues];
                
                if (values.count) {
                    _hasMultiData = YES;
                }
            }  // implement future version here
        }
    }
    return self;
}

#pragma mark - privates

- (void)initWithOneContentArray:(NSArray *)tableContentDicArray {
    assert([tableContentDicArray count]>0);
    
    NSArray* tableColumns = [self.renderContent valueForKey:kTableColumns];
    assert([tableColumns count]>0);
    
    _tableColumns = [[NSMutableArray alloc]initWithCapacity:5];
    _tableCells = [[NSMutableArray alloc]initWithCapacity:20];
    
    
    CGFloat cellHeight = 0.0;
    /// begin to get the font and font size.
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize < 1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    /// end to get font and font size.
    cellHeight = [CZDictionaryTool textSize:kHermesText useFont:font].height + kTableRowFontMargin * 2;
    for (NSDictionary *tableColumnDic in tableColumns) {
        CZTableColumn *tableColum = [[CZTableColumn alloc]initWithDic:tableColumnDic];
        self.tableCellHeight = cellHeight;
        if (tableColum.tableColumnID) {
            [tableColum addColumnCells:tableContentDicArray withFontDic:fontDic];
        }
        assert([tableColum.tableColumnCells count]>0);
        
        [_tableColumns addObject:tableColum];
        [_tableCells addObjectsFromArray:tableColum.tableColumnCells];
        FreeObj(tableColum);
    }
    
    CGFloat renderFrameHeight = cellHeight * ([tableContentDicArray count] + 1) + kTableMarginHeight * 2;
    if (_title && [_title length]>0) {
        renderFrameHeight = renderFrameHeight + self.tableTitleHeight;
    }
    
    self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y, _renderFrame.size.width, renderFrameHeight);
    CZLogRect(self.renderFrame);
    
    [self createEvaluates];
    CGSize evaluationSize = [self computeEvaluatesSize];
    _evaluatesHeight = evaluationSize.height;
    self.tableEvaluateHeight = _evaluatesHeight;
    self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y,
                                  _renderFrame.size.width, _renderFrame.size.height + _evaluatesHeight);
    CZLogRect(self.renderFrame);
    
    if ([_caption length]) {
        NSDictionary *tableCaption = [self.renderContent valueForKey:kCaption];
        NSDictionary *tableCaptionFontDic = [tableCaption valueForKey:kFont];
        CGFloat renderFontSize = [[tableCaptionFontDic valueForKey:kFontSize] floatValue];
        renderFontSize = MAX(kTableCaptionMinFontSize, MIN(renderFontSize, kTableCaptionMaxFontSize));
        
        NSString *fontName = [tableCaptionFontDic valueForKey:kFontName];
        UIFont *captionFont = fontName ? [UIFont fontWithName:fontName size:renderFontSize] : nil;
        if (!captionFont) {
            captionFont = [UIFont systemFontOfSize:renderFontSize];
        }
        
        CGSize captionTextSize = [CZDictionaryTool textSize:_caption
                                                    useFont:captionFont
                                             constraintSize:CGSizeMake(self.renderFrame.size.width - kTableMarginWidth * 2, 1e4)];
        
        self.tableCaptionHeight = ceilf(captionTextSize.height) + kTableCaptionTopMargin;
        self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y,
                                      _renderFrame.size.width, _renderFrame.size.height + self.tableCaptionHeight);
    }
    
    //_isShowTable = [[_renderContent valueForKey:kIsTableVisible] boolValue];
    _isShowTable = YES;
    id showTableId = [_renderContent valueForKey:kIsTableVisible];
    if (showTableId && [showTableId isKindOfClass:[NSNumber class]]) {
        if((strcmp([showTableId objCType], @encode(char))) == 0) {
            _isShowTable = [showTableId boolValue];
        }
    }
    
    
    if (!_isShowTable) {
        self.canBreak = NO;
        if ([_evaluates count]>0) {
            CGSize aSize = [self computeHideTableEvaluatesSize];
            self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y, aSize.width, aSize.height);
            CZLogRect(self.renderFrame);
            
        }else{
            self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y,
                                          0, 0);
        }
    }
}

#pragma mark - common methods

- (float)tableTitleHeight {
    return (_title == nil) ? 0 : kTableTitleHeight;
}

/// this method must be called after creating the columns.
- (void)createEvaluates{
    NSArray *evaluationDicArray = [self.renderContent valueForKey:kDataEvaluation];
    CGRect evRct = CGRectMake(_renderFrame.origin.x + kTableMarginWidth, _renderFrame.origin.y + _renderFrame.size.height, _renderFrame.size.width - kTableMarginWidth*2, 0);
    CZLogRect(evRct);
    _evaluates = [[NSMutableArray alloc]initWithCapacity:3];
    
    /// firt should sort the columns ,make them in the right order
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kSortOrderAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_tableColumns sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    if (![evaluationDicArray isKindOfClass:[NSArray class]]){
        return;
    }
        
    if (evaluationDicArray && [evaluationDicArray count]){
        for (NSDictionary *aDic in evaluationDicArray) {
            if (![aDic isKindOfClass:[NSDictionary class]]) {
                continue;
            }
            
            NSString* aDicType = [aDic valueForKey:kElementType];
            assert([aDicType isEqualToString:kTableEvaluationElement]);
            CZTableEvaluate *aEvaluate = [[CZTableEvaluate alloc]initWithContent:aDic frame:evRct];
            
            if ([_tableColumns count]> aEvaluate.columnNum-1) {
                aEvaluate.tableColumn = [_tableColumns objectAtIndex:aEvaluate.columnNum-1];
                [_evaluates addObject:aEvaluate];
               }
             FreeObj(aEvaluate);
        }
    }
}

- (void)setEvaluatesContent:(NSArray *)evaluates{
    if ([evaluates count]>0) {
        assert(evaluates.count == _evaluates.count);
        for (NSUInteger i = 0; i< [_evaluates count]; i++) {
            CZTableEvaluate *curEvaluate = [_evaluates objectAtIndex:i];
            CZTableEvaluate *originalEvaluate = [evaluates objectAtIndex:i];
            curEvaluate.tableColumn = originalEvaluate.tableColumn;
        }
    }
}

- (void)hideTableEvaluate{
    if (_evaluates && [_evaluates count]>0) {
        [_evaluates removeAllObjects];
        self.renderFrame = CGRectMake(_renderFrame.origin.x, _renderFrame.origin.y,
                                      _renderFrame.size.width, _renderFrame.size.height - _evaluatesHeight);
        _evaluatesHeight = 0.f;
    }
}

- (void)setRenderFrame:(CGRect)renderFrame{
    CGFloat offsetX = renderFrame.origin.x - _renderFrame.origin.x;
    CGFloat offsetY = renderFrame.origin.y - _renderFrame.origin.y;
    _renderFrame = renderFrame;
    if (_evaluates && [_evaluates count]>0){
        for (CZTableEvaluate *aObj in _evaluates) {
            aObj.renderFrame = CGRectMake(aObj.renderFrame.origin.x + offsetX,
                                          aObj.renderFrame.origin.y + offsetY,
                                          _renderFrame.size.width,
                                          aObj.renderFrame.size.height);
        }
    }
}

- (CGSize)computeEvaluatesSize{
    CGSize evSize = CGSizeZero;
    CGFloat totalHeight = 0.f;
    if (_evaluates && [_evaluates count]>0) {
        for (NSUInteger i = 0; i<[_evaluates count]; i++) {
            CZTableEvaluate *aEvaluate = [_evaluates objectAtIndex:i];
            CGRect beforeRct = aEvaluate.renderFrame;
            CZLogRect(beforeRct);
            aEvaluate.renderFrame = CGRectMake(beforeRct.origin.x, beforeRct.origin.y + totalHeight,
                                               beforeRct.size.width, beforeRct.size.height);
            totalHeight += aEvaluate.renderFrame.size.height;
        }
    }
    evSize = CGSizeMake(0.0, totalHeight);
    return evSize;
}

/// This method must called after computeEvaluatesSize method.
- (CGSize)computeHideTableEvaluatesSize{
    assert([_evaluates count]>0);
    CGFloat upOffset = _renderFrame.size.height - _evaluatesHeight;
    //    if (_title && [_title length]>0) {
    //        upOffset = _renderFrame.size.height - _evaluatesHeight - kTableTitleHeight;
    //    }
    for (CZTableEvaluate *aObj in _evaluates) {
        CGRect beforeEvaluateRct = aObj.renderFrame;
        aObj.renderFrame = CGRectMake(beforeEvaluateRct.origin.x, beforeEvaluateRct.origin.y - upOffset,
                                      beforeEvaluateRct.size.width, beforeEvaluateRct.size.height);
    }
    //    if (_title && [_title length]>0) {
    //        return CGSizeMake(_renderFrame.size.width, _evaluatesHeight + kTableTitleHeight);
    //    }
    return CGSizeMake(_renderFrame.size.width, _evaluatesHeight);
}

- (void)setTableSubOrder:(NSInteger)aSubOrder{
     tableSubOrder = aSubOrder;
     _renderOrder = _renderOrder + tableSubOrder;
     CZLog(@"the render order is %ld",(long)_renderOrder);
     if (aSubOrder > 0) {
         _relativeFrame = CGRectMake(0,
                                     kRelativeSubEelementMargin,
                                     _renderFrame.size.width,
                                     _renderFrame.size.height);
         CZLogRect(_relativeFrame);
     }
}

- (CGSize)preRenderLayout{
    [self layoutTableCells];
   CGSize aSize = self.renderFrame.size;
    return aSize;
}

- (void)render{
    [super render];
    if (self.renderFrame.size.height == 0 || self.renderFrame.size.width == 0){
        return;
    }
    /// render content
    if (!_isShowTable) {
        /**
        if (_title) {
            NSDictionary *tableTitle = [self.renderContent valueForKey:kTitle];
            
            NSDictionary *fontDic = [tableTitle valueForKey:@"font"];
            CGFloat renderFontSize = [[fontDic valueForKey:@"size"] floatValue];
            if (renderFontSize<1) {
                renderFontSize = kDefaultTextFontSize;
            }
            UIFont *font = [UIFont systemFontOfSize:renderFontSize];
            
            NSDictionary *color = [fontDic valueForKey:@"color"];
            CGFloat r = [[color valueForKey:@"r"] floatValue];
            CGFloat g = [[color valueForKey:@"g"] floatValue];
            CGFloat b = [[color valueForKey:@"b"] floatValue];
            CGFloat a = [[color valueForKey:@"a"] floatValue];
            
            CGContextSetRGBFillColor(_currentContext, r, g, b, a);
            NSInteger titleWidth=[_title sizeWithFont:font].width;
            CGRect titleRct = CGRectMake(self.renderFrame.origin.x + (_renderFrame.size.width - titleWidth)/2,
                                         _renderFrame.origin.y, titleWidth, kTableTitleHeight);
            [_title drawInRect:titleRct withFont:font lineBreakMode:UILineBreakModeWordWrap alignment:NSTextAlignmentLeft];
        }
        */
        if(_evaluates && [_evaluates count]>0){
            [_evaluates makeObjectsPerformSelector:@selector(render)];
        }
        return;
    }
    
    /// render the table content
    CGFloat baseX = self.renderFrame.origin.x + kTableMarginWidth;
    CGFloat baseY = self.renderFrame.origin.y + self.tableTitleHeight;
    NSInteger columnNum = self.columnNumber;
    NSInteger lineNum =  self.lineNumber;
    assert(columnNum>0 && lineNum>0);
    
    /// begin render 
    CGContextRef  context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, NO);
    CGContextSetLineWidth(context, 1.0);
    CGFloat width = self.renderFrame.size.width - kTableMarginWidth * 2;
    CGFloat height = self.tableCellHeight * lineNum;
    CGFloat cellHeight = self.tableCellHeight;
    CGFloat cellWidth = ceilf(width/columnNum);

    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGRect box = CGRectMake(baseX, baseY + kTableMarginHeight, width, height);
    CGContextAddRect(context, box);
    for(int j = 1; j < lineNum; j++) {
        /// draw horizontal line
        CGContextMoveToPoint(context, CGRectGetMinX(box), box.origin.y + cellHeight * j);
        CGContextAddLineToPoint(context, CGRectGetMaxX(box), box.origin.y + cellHeight * j);
    }
    
    for(int j = 1; j < columnNum; j++) {
        // draw vertical line
        CGContextMoveToPoint(context, box.origin.x + cellWidth * j, CGRectGetMinY(box));
        CGContextAddLineToPoint(context, box.origin.x + cellWidth * j, CGRectGetMaxY(box));
    }
    CGContextStrokePath(context);
    
    if (_tableCells && [_tableCells count]>0) {
        [_tableCells makeObjectsPerformSelector:@selector(drawToContenxt)];
    }
    baseY += kTableMarginHeight + height;

    if (_title) {
        NSDictionary *tableTitle = [self.renderContent valueForKey:kTitle];
        
        NSDictionary *fontDic = [tableTitle valueForKey:kFont];
        
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kTableTitleMaxFontSize) {
            renderFontSize = kTableTitleMaxFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        CGFloat r = [[color valueForKey:@"r"] floatValue];
        CGFloat g = [[color valueForKey:@"g"] floatValue];
        CGFloat b = [[color valueForKey:@"b"] floatValue];
        CGFloat a = [[color valueForKey:@"a"] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
        //CGFloat titleWidth=[_title sizeWithFont:font].width;
        CGFloat titleWidth = [CZDictionaryTool textSize:_title useFont:font].width;

        CGRect titleRct = CGRectMake(self.renderFrame.origin.x + (_renderFrame.size.width - titleWidth)/2,
                                     _renderFrame.origin.y, titleWidth, self.tableTitleHeight);
        [_title drawInRect:titleRct withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentLeft];
    }

    if(_evaluates && [_evaluates count]>0){
        [_evaluates makeObjectsPerformSelector:@selector(render)];
        baseY += self.tableEvaluateHeight;
    }
    
    if (_caption) {
        NSDictionary *tableCaption = [self.renderContent valueForKey:kCaption];
        
        NSDictionary *fontDic = [tableCaption valueForKey:kFont];
        
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        renderFontSize = MAX(kTableCaptionMinFontSize, MIN(renderFontSize, kTableCaptionMaxFontSize));

        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = fontName ? [UIFont fontWithName:fontName size:renderFontSize] : nil;
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        
        NSDictionary *colorDict = [fontDic valueForKey:kColor];
        UIColor *color = [CZDictionaryTool dictionay2Color:colorDict];
        CGContextSetFillColorWithColor(context, [color CGColor]);
        
        CGRect captionRect = CGRectMake(self.renderFrame.origin.x + kTableMarginWidth, baseY + kTableCaptionTopMargin,
                                        self.renderFrame.size.width - kTableMarginWidth * 2, self.tableCaptionHeight);

        [_caption drawInRect:captionRect
                    withFont:font
               lineBreakMode:NSLineBreakByWordWrapping
                   alignment:NSTextAlignmentLeft];
        baseY += self.tableCaptionHeight;
    }
    
    CZLog(@"render end of class %@",[self debugDescription]);
}

/// sort the table column and tablecolumn cell element ,default by asending.
- (void)sortTableColumnByOrder{
    if ([_tableColumns count]>0) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_order" ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
        [_tableColumns sortUsingDescriptors:sortDescriptors];
        FreeObj(sortDescriptor);
        FreeObj(sortDescriptors);
        /// sort the cell order ,and sort the draw line order
        [_tableColumns makeObjectsPerformSelector:@selector(ascendingSortCellOrder)];
        
        /// sort the draw column order
        for (NSUInteger i = 0; i<[_tableColumns count]; i++) {
            CZTableColumn *tableColum = [_tableColumns objectAtIndex:i];
            [tableColum setCellDrawColumnOrder:i];
        }
    }
}

- (void)layoutTableCells{
    [self sortTableColumnByOrder];
    CGFloat baseX = self.renderFrame.origin.x + kTableMarginWidth;
    CGFloat baseY = self.renderFrame.origin.y + self.tableTitleHeight;
    NSInteger columnNum = self.columnNumber;
    NSInteger lineNum =  self.lineNumber;
    assert(columnNum>0 && lineNum>0);
    
    /// begin render
    CGContextRef  context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, NO);
    CGContextSetLineWidth(context, 1.0);
    float width = self.renderFrame.size.width - kTableMarginWidth*2 ;
    float cellHeight = self.tableCellHeight;
    CGFloat cellWidth = ceilf(width/columnNum);
    
    CGFloat pointX = 0.f;
    CGFloat pointY = 0.f;
    [_tableCells removeAllObjects];
    for (NSUInteger i=0; i<[_tableColumns count]; i++) {
        pointX =baseX + i*cellWidth;
        CZTableColumn *columObj = [_tableColumns objectAtIndex:i];
        for (NSInteger j=0; j<columObj.cellNum; j++) {
            pointY =baseY + kTableMarginHeight + j*cellHeight;
            CZTableColumnCellObj *cell = [columObj.tableColumnCells objectAtIndex:j];
            if (cell) {
                cell.x = pointX;
                cell.y = pointY;
                cell.width = cellWidth;
                cell.height = cellHeight;
                CZLogRect(cell.frame);
            }
        }
        [_tableCells addObjectsFromArray:columObj.tableColumnCells];
    }
}


- (NSInteger)columnNumber{
    NSInteger num = 0;
    if (_tableColumns) {
        num = [_tableColumns count];
    }
    return num;
}

- (NSInteger)lineNumber{
    NSInteger num = 0;
    if (_tableCells && _tableColumns) {
        if ([_tableColumns count]>0) {
            num = [_tableCells count]/[_tableColumns count];
        }
    }
    return num;
}

#pragma mark -
#pragma mark RTF render methods

- (void)generateRTFTableTitle{
    if (!_title) {
        return;
    }
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *tableTitle = [self.renderContent valueForKey:kTitle];
    
    NSDictionary *fontDic = [tableTitle valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];

    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kTableTitleMaxFontSize) {
        renderFontSize = kTableTitleMaxFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
    
    CZLogRect(_renderFrame);
    /**
    NSString *textAlignment = [CZRTFTool xOffset2TextRTFString:_renderFrame.origin.x];
    NSString *titleOffset = nil;
    if ([textAlignment isEqualToString:@"\\ql"]) {
        CGFloat titleOffsetValue = (_renderFrame.size.width - [self titleSize].width)*0.5;
        titleOffset = [CZRTFTool xOffset2RTFString:titleOffsetValue];
    }else if ([textAlignment isEqualToString:@"\\qr"]){
        CGFloat pageWidth = [[[NSUserDefaults standardUserDefaults]valueForKey:kPageBodyWidth] floatValue];
        CGFloat titleOffsetValue = pageWidth - _renderFrame.size.width
                                             + (_renderFrame.size.width
                                             - [self titleSize].width)*0.5;
        titleOffset = [CZRTFTool xOffset2RTFString:titleOffsetValue];
        
    }else{
        titleOffset = textAlignment;
    }
    */
     NSString* titleOffset = kAlignmentCenter;
    
    [_RTFOutputStream writeString:@"\\pard"];
    if (titleOffset) {
        [_RTFOutputStream writeString:titleOffset];
     }
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_title];
    [_RTFOutputStream writeString:unicodeStr];
    //CZLog(@"The text rtf string is %@",_RTFAttributeStr);
    
    [_RTFOutputStream writeString:@"\\par"];
}

- (void)generateRTFTableCaption {
    if (!_caption) {
        return;
    }

    NSDictionary *tableCaption = [self.renderContent valueForKey:kCaption];
    
    NSDictionary *fontDic = [tableCaption valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize < 1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kTableTitleMaxFontSize) {
        renderFontSize = kTableTitleMaxFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance] registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];

    [_RTFOutputStream writeString:@"\\pard"];
    [_RTFOutputStream writeString:kAlignmentLeft];
    NSString *paragraphTopSpace = [NSString stringWithFormat:@"\\sb%d", kTableCaptionTopMargin * kPixelToTwips / 2];
    [_RTFOutputStream writeString:paragraphTopSpace];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:@" "];
    NSString *unicodeStr = [CZRTFTool getNSStringUnicode:_caption];
    [_RTFOutputStream writeString:unicodeStr];
}

- (void)generateRTFTableContent{
    if (!_tableColumns || [_tableColumns count]<1) {
        return;
    }
    [self sortTableColumnByOrder];
    NSInteger columnNum = self.columnNumber;
    NSInteger lineNum =  self.lineNumber;
    assert(columnNum>0 && lineNum>0);
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:[UIColor blackColor]];

    /// begin render
    CGFloat width = self.renderFrame.size.width - kTableMarginWidth*2 ;
    CGFloat cellWidth = ceilf(width/columnNum);
    
    CZTableColumn *columObj = [_tableColumns objectAtIndex:0];
    [_RTFOutputStream writeString:kAlignmentCenter];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    for (NSInteger j=0; j<columObj.cellNum; j++){
        NSMutableArray *tableRowArray = [[NSMutableArray alloc]initWithCapacity:5];
        for (NSUInteger i=0; i<[_tableColumns count]; i++){
            CZTableColumn *columObj = [_tableColumns objectAtIndex:i];
            CZTableColumnCellObj *cell = [columObj.tableColumnCells objectAtIndex:j];
            [tableRowArray addObject:cell];
        }
        [self generateRTFTableRow:tableRowArray colomnWidth:cellWidth];

        [tableRowArray release];
        tableRowArray = nil;
    }
    
    if (columObj.cellNum > 0 && _evaluates.count == 0 && _caption == nil) {
        // add separator between table and table.
        [_RTFOutputStream writeString:@"\\pard{\\par}"];
    }
}


- (void)generateRTFTableRow:(NSArray *)rowItems colomnWidth:(CGFloat)width{
    assert(width>0);
    assert([rowItems count]>0);
    //NSString *tableAlignment = [CZRTFTool xOffset2TableRTFString:_renderFrame.origin.x];
    
    /// begin to get the font and font size.
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    /// end to get font and font size.
    CGSize textSize = [CZDictionaryTool textSize:kHermesText useFont:font];
    int32_t rowHeight = textSize.height + kTableRowFontMargin * 2;
    rowHeight = MIN(self.tableCellHeight, rowHeight);
    int32_t twipRowHeight = rowHeight * kPixelToTwips;
       

    [_RTFOutputStream writeString:[NSString stringWithFormat:@"\\trowd\\trqc\\trkeep\\trgaph144\\trrh%d",-twipRowHeight]];
    
    CGFloat twipsWide = width * kPixelToTwips * kPdfToRTFScale ;
    
    NSString *cellBorder = @"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs";

    for (NSUInteger i = 0; i<[rowItems count]; i++) {
        CZTableColumnCellObj *cell = [rowItems objectAtIndex:i];
        NSString *cellRightOffsetStr = nil;
        if (cell.backgroundColor) {
            NSString *cbString = [[CZRTFFontColorRegistTable sharedInstance] readBackgoundColorFromColorTable:cell.backgroundColor];
            cellRightOffsetStr = [NSString stringWithFormat:@"\\%@\\cellx%d", cbString, (int32_t)(twipsWide *(i+1))];
        } else {
            cellRightOffsetStr = [NSString stringWithFormat:@"\\cellx%d", (int32_t)(twipsWide *(i+1))];
        }
        [_RTFOutputStream writeString:cellBorder];
        [_RTFOutputStream writeString:cellRightOffsetStr];
    }
    for (NSUInteger i = 0; i<[rowItems count]; i++) {
        CZTableColumnCellObj *cell = [rowItems objectAtIndex:i];
        CGSize textSize = [CZDictionaryTool textSize:cell.cellContent useFont:font];
        CGFloat shrinkRatio = 1.0;
        if (textSize.width > width) {
            shrinkRatio = (width - 4) / textSize.width;  // 4 is margin around text box
            shrinkRatio = MAX(kTableCellTextShrinkMin, shrinkRatio);
        }
        [self generateRTFTableCell:cell shrinkRatio:shrinkRatio];
    }
    [_RTFOutputStream writeString:@"\\row"];
}


- (void)generateRTFTableCell:(CZTableColumnCellObj *)cell shrinkRatio:(CGFloat)shrinkRatio {
    NSString *cellStr = cell.cellContent;
    if (cellStr == nil || [cellStr length]<1) {
        cellStr = @" ";
    }
    
    /// begin to get the font and font size.
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    NSDictionary *colorDic = [fontDic valueForKey:kColor];
    colorDic = [CZDictionaryTool validateColor:colorDic];
    
    UIColor *color = [CZDictionaryTool dictionay2Color:colorDic];
    
    if (cell.isTitle) {
        [_RTFOutputStream writeString:@"\\b"];
    }else{
        [_RTFOutputStream writeString:@"\\b0"];
    }
    /// end to get font and font size.
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:color];
    
    if (cell.backgroundColor) {
        CGFloat red, green, blue, alpha;
        [cell.backgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
        CGFloat Y = 0.299 * red + 0.587 * green + 0.114 * blue;
        UIColor *fontColor = Y > 0.5 ? [UIColor blackColor] : [UIColor whiteColor];
        cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:fontColor];
    }
    
    [_RTFOutputStream writeString:kAlignmentCenter];
    
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize * shrinkRatio];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:cellStr];
    [_RTFOutputStream writeString:unicodeStr];
    [_RTFOutputStream writeString:@"\\intbl\\cell"];
}

- (void)generateRTFEvaluations {
    if ([_evaluates count]) {
        [_RTFOutputStream writeString:@"\\pard"];
        for (CZTableEvaluate *item in _evaluates) {
            [self generateRTFEvaluationItem:item];
        }
    }
}

- (void)generateRTFEvaluationItem:(CZTableEvaluate *)evaluateItem{
    if (!evaluateItem || evaluateItem.evaluteTitle == nil) {
        return;
    }
    [evaluateItem rtfRenderIntoStream:_RTFOutputStream];
}

- (void)registerRTFColor:(CZRTFFontColorRegistTable *)colorTable {
    for (CZTableColumn *tableColumn in _tableColumns) {
        for (UIColor *color in [tableColumn useColors]) {
            [colorTable registColorToColorTable:color];
        }
    }
}

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    
    BOOL isRTF = [[CZRTFPreviewConfig sharedInstance] isForRTF];
    if (isRTF) {
        [self renderRTFContent];
    } else {
        [self renderRTFDContent];
    }
    return;
}

- (void)renderRTFContent {
    CGRect box = CGRectInset(_renderFrame, kTableMarginWidth, 0);

    NSString *textBoxPre = [CZRTFTool textBoxPre];
    NSString *textBoxEnd = [CZRTFTool textBoxEnd:box];
    
    [_RTFOutputStream writeString:textBoxPre];
    
    if (!_isShowTable) {
        [self generateRTFEvaluations];
         [_RTFOutputStream writeString:textBoxEnd];
        return;
    }
    
    [self generateRTFTableTitle];
    [self generateRTFTableContent];
    [self generateRTFEvaluations];
    [self generateRTFTableCaption];
    
    [_RTFOutputStream writeString:textBoxEnd];
}


#pragma mark -
#pragma mark RTFD render methods


- (void)renderRTFDContent{
    NSString *frameStr = [CZRTFTool offsetFrame2RTFString:_renderFrame];
    [_RTFOutputStream writeString:frameStr];
    
    if (!_isShowTable) {
        [self generateRTFEvaluations];
        return;
    }
    [self generateRTFDTableTitle];
    [self generateRTFDTableContent];
    [self generateRTFEvaluations];
    return;
}

- (void)generateRTFDTableContent{
    if (!_tableColumns || [_tableColumns count]<1) {
        return;
    }
    [self sortTableColumnByOrder];
    NSInteger columnNum = self.columnNumber;
    NSInteger lineNum =  self.lineNumber;
    assert(columnNum>0 && lineNum>0);
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:[UIColor blackColor]];
    /// begin render
    CGFloat width = self.renderFrame.size.width - kTableMarginWidth*2 ;
    CGFloat cellWidth = ceilf(width/columnNum);
    
    CZTableColumn *columObj = [_tableColumns objectAtIndex:0];
    //[_tableColumns makeObjectsPerformSelector:@selector(FormatCellsForRTFD)];
    [_RTFOutputStream writeString:@"\\par"];
    
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    for (NSInteger j=0; j<columObj.cellNum; j++){
        NSMutableArray *tableRowArray = [[NSMutableArray alloc]initWithCapacity:5];
        for (NSUInteger i=0; i<[_tableColumns count]; i++){
            CZTableColumn *columObj = [_tableColumns objectAtIndex:i];
            CZTableColumnCellObj *cell = [columObj.tableColumnCells objectAtIndex:j];
            [tableRowArray addObject:cell];
        }
        [self generateRTFDTableRow:tableRowArray colomnWidth:cellWidth];
        if (j == columObj.cellNum -1) {
            [_RTFOutputStream writeString:@"\\lastrow"];
            [_RTFOutputStream writeString:@"\\row"];
        } else {
            [_RTFOutputStream writeString:@"\\row"];
        }
        
        [tableRowArray release];
        tableRowArray = nil;
    }
    if (_evaluates && [_evaluates count]>0) {
        [_RTFOutputStream writeString:@"\\pard"];
        
    } else {
        [_RTFOutputStream writeString:@"\\pard\\par"];
    }
}

- (void)generateRTFDTableTitle{
    if (!_title) {
        return;
    }
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *tableTitle = [self.renderContent valueForKey:kTitle];
    
    NSDictionary *fontDic = [tableTitle valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];

    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kTableTitleMaxFontSize) {
        renderFontSize = kTableTitleMaxFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:25];
    
    CZLogRect(_renderFrame);
    /**
    NSString *textAlignment = [CZRTFTool xOffset2TextRTFString:_renderFrame.origin.x];
    NSString *titleOffset = nil;
    if ([textAlignment isEqualToString:@"\\ql"]) {
        CGFloat titleOffsetValue = (_renderFrame.size.width - [self titleSize].width)*0.5;
        titleOffset = [CZRTFTool xOffset2RTFString:titleOffsetValue];
    }else if ([textAlignment isEqualToString:@"\\qr"]){
        CGFloat pageWidth = [[[NSUserDefaults standardUserDefaults]valueForKey:kPageBodyWidth] floatValue];
        CGFloat titleOffsetValue = pageWidth - _renderFrame.size.width
        + (_renderFrame.size.width
           - [self titleSize].width)*0.5;
        titleOffset = [CZRTFTool xOffset2RTFString:titleOffsetValue];
        
    }else{
        titleOffset = textAlignment;
    }
    */
    NSString* titleOffset = kAlignmentCenter;

    [_RTFOutputStream writeString:@"\\par"];
    [_RTFOutputStream writeString:@"\\pard"];
    if (titleOffset) {
        [_RTFOutputStream writeString:titleOffset];
    }
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_title];
    [_RTFOutputStream writeString:unicodeStr];
    //CZLog(@"The text rtf string is %@",_RTFAttributeStr);
}



    /**
     *   new changed for preview the RTFD
     *    NSString *cellBorder = @"\\clvertalc \\clshdrawnil \\clbrdrt\\brdrs\\brdrw20\\brdrcf2 \\clbrdrl\\brdrs\\brdrw20\\brdrcf2 \\clbrdrb\\brdrs\\brdrw20\\brdrcf2 \\clbrdrr\\brdrs\\brdrw20\\brdrcf2 \\clpadl100 \\clpadr100 \\gaph"; used to define the table column cell boder.
     */
- (void)generateRTFDTableRow:(NSArray *)rowItems colomnWidth:(CGFloat)width{
    assert(width>0);
    assert([rowItems count]>0);
    
    /** not suggest make rtfd table font be defined in the template, because when font in table is too big or small, the rtfd preview is too ugly.
      */
     
    /// begin to get the font and font size.
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    /// end to get font and font size.
    
    //UIFont *font = [UIFont systemFontOfSize:kDefaultTextFontSize];
    
    CGSize textSize = [CZDictionaryTool textSize:kHermesText useFont:font];
    int32_t rowHeight = textSize.height + kTableRowFontMargin * 2;
    int32_t twipRowHeight = rowHeight * kPixelToTwips;
    
    
    NSString *tableAlignment = @"\\trqc";
    [_RTFOutputStream writeString:[NSString stringWithFormat:@"\\trowd%@\\trkeep\\trgaph144\\trrh%d",tableAlignment,twipRowHeight]];
    
    //CGFloat twipsWide = width * kPixelToTwips * kPdfToRTFScale ;
    CGFloat twipsWide = width * kPixelToTwips * 1 ;
    NSString *cellBorder = @"\\clvertalc \\clshdrawnil \\clbrdrt\\brdrs\\brdrw20 \\clbrdrl\\brdrs\\brdrw20 \\clbrdrb\\brdrs\\brdrw20 \\clbrdrr\\brdrs\\brdrw20 \\clpadl100 \\clpadr100 \\gaph";
    //NSString *cellBorder = @"\\clbrdrt\\brdrs\\clbrdrl\\brdrs\\clbrdrb\\brdrs\\clbrdrr\\brdrs";
    for (NSUInteger i = 0; i<[rowItems count]; i++) {
        NSString *cellRightOffsetStr = [NSString stringWithFormat:@"\\cellx%d",(int32_t)(twipsWide *(i+1))];
        [_RTFOutputStream writeString:cellBorder];
        [_RTFOutputStream writeString:cellRightOffsetStr];
    }
    for (NSUInteger i = 0; i<[rowItems count]; i++) {
        CZTableColumnCellObj *cell = [rowItems objectAtIndex:i];
        [self generateRTFDTableCell:cell];
    }
}

- (void)generateRTFDTableCell:(CZTableColumnCellObj *)cell{
    NSString *cellStr = cell.cellContent;
    if (cellStr == nil || [cellStr length]<1) {
        cellStr = @" ";
    }
    
    /// begin to get the font and font size.
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    /// end to get font and font size.
    
    if (cell.isTitle) {
        [_RTFOutputStream writeString:@"\\b"];
    } else {
        [_RTFOutputStream writeString:@"\\b0"];
    }
    
    
    ///new added  begin :special for the charator of "µm²"
    if ([cellStr isEqualToString:@"µm²"]) {
        UIFont *swissFont = [UIFont fontWithName:kSwissFont size:renderFontSize];
        NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:swissFont];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fString];
    }else{
        //UIFont* normalFont = [UIFont systemFontOfSize:renderFontSize];
        NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fString];
    }
    ///new added  end :special for the charator of "µm²"
    /// just for RTFD use
    NSString *tempCellStr = [NSString stringWithFormat:@"%@                                ",cellStr];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:tempCellStr];
    //[_RTFOutputStream writeString:@"\\fs25"];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    
    [_RTFOutputStream writeString:unicodeStr];
    [_RTFOutputStream writeString:@"\\intbl\\cell"];
}

- (void)dealloc{
    FreeObj(_tableCells);
    FreeObj(_tableColumns);
    FreeObj(_evaluates);
    [_title release];
    [_caption release];
    [super dealloc];
    
}

@end
