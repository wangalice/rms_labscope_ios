//
//  CZFooterRender.h
//  ReportSDK
//
//  Created by Johnny on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderBase.h"


@interface CZFooterRender : CZRenderBase{
    NSMutableArray      *_footerElements;
    BOOL                 isPageInfoVisible;
    BOOL                 _isResort;

}

@property (nonatomic,retain) NSMutableArray     *footerElements;
@property (nonatomic)        NSUInteger          totalPageNum;
@property (nonatomic)        NSUInteger          curPageNum;

@end
