//
//  CZImageRender.m
//  ReportSDK
//
//  Created by Johnny on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZImageRender.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"
#import "CZRTFTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZConvertImageToString.h"
#import "CZRTFPreviewConfig.h"
#import "CZBase64Data.h"
#import "CZReportConfigManager.h"

static const CGFloat kImageTitleHeight = 50;
static const CGFloat kImageTitleMargin = 3;
static const CGFloat kImageBottomOffset = kImageTitleHeight;
static const CGFloat kImageMarginPercent = 5;

@interface CZImageRender() {
    NSString    *_title;
    BOOL        isUseBase64Data;
    BOOL        _noLineBreak;
}

@property(nonatomic, copy, readwrite) NSString *imagePath;  // override readonly attribute
@property(nonatomic, retain, readwrite) NSArray *images;  // override readonly attribute

@end

@implementation CZImageRender

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame {
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        NSDictionary *value = [contentDic valueForKey:kValue];
        id imageValue = [value valueForKey:kImageFile];
        if ([imageValue isKindOfClass:[NSString class]]) {  // data format version 0
            _imagePath = [imageValue copy];
        } else if ([imageValue isKindOfClass:[NSDictionary class]]) {
            id version = [imageValue objectForKey:kVersion];
            if ([@1 isEqual:version]) {
                NSDictionary *tempDict = (NSDictionary *)imageValue;
                _images = [tempDict[kValues] retain];

                id renderMode = imageValue[kImageRenderMode];
                if ([kImageRenderModeTile isEqual:renderMode]) {
                    self.renderMode = kCZImageRenderModeTile;
                } else if ([kImageRenderModeMultipage isEqual:renderMode]) {
                    self.renderMode = kCZImageRenderModeMultipage;
                }
            } // implement next verison here
        }

        if (_imagePath == nil && self.images.count) {
            NSDictionary *subImage = self.images[0];
            _imagePath = [subImage[kValue] copy];
            
            NSNumber *isUse = [subImage valueForKey:kIsUseImageBase64Data];
            if ([isUse boolValue]) {
                isUseBase64Data = YES;
            }
            
            _title = [subImage[kImageCaption] copy];
        } else {
            NSNumber *isUse = [_renderContent valueForKey:kIsUseImageBase64Data];
            if ([isUse boolValue]) {
                isUseBase64Data = YES;
            }
            
            if (!isUseBase64Data) {
                // use image file name as default
                _title = [[[_imagePath lastPathComponent] stringByDeletingPathExtension] copy];
            } else {
                _title = [[contentDic valueForKeyPath:@"title.titleText"] copy];
            }
        }
    }
//    self.backgroundColor = [UIColor greenColor];
    return self;
}

- (void)dealloc {
    [_images release];
    [_imagePath release];
    [_title release];
    [super dealloc];
}

- (BOOL)canBreak {
    return NO;
}

- (CGSize)preRenderLayout{
    /// The image path is absolute path.
    if (self.imagePath == nil) {
        return CGSizeZero;
    }

    CGSize imageSize;
    @autoreleasepool {
        UIImage *image = [UIImage imageWithContentsOfFile:self.imagePath];
        if (isUseBase64Data) {
            NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
            image = [UIImage imageWithData:imageData];
            if (!image) {
                return CGSizeZero;
            }
        }
        imageSize = image.size;
    }
    CGFloat yScale = 0.0;
    if ([self shouldShowsTitle]) {
     yScale =  (self.renderFrame.size.height - kImageBottomOffset)/imageSize.height;   
    }else{
        yScale =self.renderFrame.size.height/imageSize.height;
    }
    CGFloat xScale = self.renderFrame.size.width/imageSize.width;
    CGSize renderSize;
    if (yScale>=xScale) {
        renderSize = CGSizeMake(imageSize.width * xScale, imageSize.height * xScale);
    }else{
        renderSize = CGSizeMake(imageSize.width * yScale, imageSize.height * yScale);
    }
    return renderSize;
}

- (CGSize)titleSize{
    CGSize titleRenderSize = CGSizeZero;
    if (_title) {
        NSDictionary *imageTitle = [self.renderContent valueForKey:kTitle];
        
        NSDictionary *fontDic = [imageTitle valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];

        CGFloat r = [[color valueForKey:kRedColor] floatValue];
        CGFloat g = [[color valueForKey:kGreenColor] floatValue];
        CGFloat b = [[color valueForKey:kBlueColor] floatValue];
        CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
        //CGFloat titleWidth=[_title sizeWithFont:font].width;
        CGFloat titleWidth = [CZDictionaryTool textSize:_title useFont:font].width;

        titleRenderSize = CGSizeMake(titleWidth, kTableTitleHeight);
    }
    return titleRenderSize;
}

- (void)render {
  @autoreleasepool {
    if (self.renderMode == kCZImageRenderModeTile &&
        !self.isInHeader &&
        self.images.count > 1) {
        [self tileImageInRect:_renderFrame renderBlock:^(CZImageRender *subRender) {
            @autoreleasepool {
                [subRender render];
            }
        }];
    } else {
        [super render];
        
        CGSize renderSize = [self preRenderLayout];
        
        CGFloat offsetX = (_renderFrame.size.width - renderSize.width)/2;
        CGFloat offsetY;
        if ([self shouldShowsTitle]) {
            offsetY = (_renderFrame.size.height - kImageBottomOffset - renderSize.height)/2;
        } else {
            offsetY = (_renderFrame.size.height - renderSize.height)/2;
        }
        
        CGRect renderRect = CGRectMake(_renderFrame.origin.x + offsetX,
                                       _renderFrame.origin.y + offsetY,
                                       renderSize.width,
                                       renderSize.height);
        
        
        UIImage *useImage = nil;
        
        /// The image path is absolute path.
        if (!isUseBase64Data) {
            //[[UIImage imageWithContentsOfFile:self.imagePath]drawInRect:renderRect];
            UIImage *originalImage = [UIImage imageWithContentsOfFile:self.imagePath];
            useImage = [[CZReportConfigManager sharedInstance] configImageForReport:originalImage
                                                                           withRect:renderRect];
        } else {
            /**
             /// The image path contain the base64 image string.
             NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
             UIImage *image = [UIImage imageWithData:imageData];
             if (image) {
             [image drawInRect:renderRect];
             }
             */
            NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
            UIImage *originalImage = [UIImage imageWithData:imageData];
            useImage = [[CZReportConfigManager sharedInstance] configImageForReport:originalImage
                                                                           withRect:renderRect];
        }
        
        if (useImage) {
            [useImage drawInRect:renderRect];
        }
        
        if ([self shouldShowsTitle]) {
            NSDictionary *imageTitle = [self.renderContent valueForKey:kTitle];
            
            NSDictionary *fontDic = [imageTitle valueForKey:kFont];
            fontDic = [CZDictionaryTool validateFontDic:fontDic];
            if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
                fontDic = [CZDictionaryTool defaultFontDic];
            }
            CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
            if (renderFontSize <1) {
                renderFontSize = kDefaultTextFontSize;
            }
            if (renderFontSize > kMaxTextFontSize) {
                renderFontSize = kMaxTextFontSize;
            }
            NSString *fontName = [fontDic valueForKey:kFontName];
            UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
            if (!font) {
                font = [UIFont systemFontOfSize:renderFontSize];
            }
            NSDictionary *color = [fontDic valueForKey:kColor];
            color = [CZDictionaryTool validateColor:color];
            
            CGFloat r = [[color valueForKey:kRedColor] floatValue];
            CGFloat g = [[color valueForKey:kGreenColor] floatValue];
            CGFloat b = [[color valueForKey:kBlueColor] floatValue];
            CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
            
            CGContextSetRGBFillColor(_currentContext, r, g, b, a);
            CGRect titleRct = CGRectMake(self.renderFrame.origin.x,
                                         _renderFrame.origin.y + offsetY + renderSize.height + kImageTitleMargin,
                                         _renderFrame.size.width,
                                         kImageTitleHeight);
            
            [_title drawInRect:titleRct withFont:font
                 lineBreakMode:NSLineBreakByTruncatingMiddle
                     alignment:NSTextAlignmentCenter];
        }
    }

    CZLog(@"render end of class %@",[self debugDescription]);

    return;
  }
}

#pragma mark - private

+ (CGSize)gridSizeInRect:(CGRect)renderRect byImageCount:(NSUInteger)imageCount {
    // calculate grid size, max size and min size
    CGFloat gridSize, maxSize, minSize;
    if (renderRect.size.width > renderRect.size.height) {
        minSize = renderRect.size.height;
        maxSize = renderRect.size.width;
    } else {
        minSize = renderRect.size.width;
        maxSize = renderRect.size.height;
    }
    gridSize = minSize;
    
    // calculate grid size that fit all the images.
    // For easiness, here we as if in portait mode.
    NSUInteger row = 1, col = floor(maxSize / gridSize);
    NSUInteger gridCount = row * col;
    CGFloat nextGridWidth = floor(minSize / 2);
    CGFloat nextGridHeight = floor(maxSize / (col + 1));
    while (gridCount < imageCount) {
        if (nextGridWidth > nextGridHeight) {
            row ++;
            gridSize = nextGridWidth;
            nextGridWidth = floor(minSize / (row + 1));
            
            NSUInteger tempColumn = floor(maxSize / gridSize);
            gridCount = row * tempColumn;
        } else {
            col ++;
            gridSize = nextGridHeight;
            nextGridHeight = floor(maxSize / (col + 1));
            
            NSUInteger tempRow = floor(minSize / gridSize);
            gridCount = tempRow * col;
        }
    };
    
    // calculate actrual col and row, then grid size
    CGSize theGridSize;
    theGridSize.width = renderRect.size.width / floor(renderRect.size.width / gridSize);
    theGridSize.height = renderRect.size.height / floor(renderRect.size.height / gridSize);
    
    return theGridSize;
}

- (void)tileImageInRect:(CGRect)renderRect renderBlock:(void (^)(CZImageRender *render))block {
    CGSize gridSize = [CZImageRender gridSizeInRect:renderRect byImageCount:self.images.count];
    
    // render images in tiles
    CGFloat x = 0, y = 0;
    
    CZImageRender *subImageRender = [[CZImageRender alloc] initWithContent:self.renderContent frame:renderRect];
    subImageRender.images = nil;
    subImageRender.renderMode = kCZImageRenderModeNormal;

    CGFloat gridMargin = floor(MIN(gridSize.width, gridSize.height) * kImageMarginPercent * 0.01);
    
    NSUInteger row = 0;
    for (NSDictionary *image in self.images) {
        NSString *filePath = image[kValue];
        CGRect gridRect = CGRectMake(x + renderRect.origin.x, y + renderRect.origin.y, gridSize.width, gridSize.height);
        
        CGRect tempRect = CGRectInset(gridRect, gridMargin, gridMargin);
        subImageRender.renderFrame = tempRect;
        tempRect.origin = CGPointZero;
        subImageRender.relativeFrame = tempRect;
        subImageRender.imagePath = filePath;
        
        NSNumber *isUse = [image valueForKey:kIsUseImageBase64Data];
        if ([isUse boolValue]) {
            subImageRender->isUseBase64Data = YES;
        }
        
        id title = [image valueForKey:kImageCaption];
        if ([title isKindOfClass:[NSString class] ]) {
            subImageRender->_title = [title copy];
        } else {
            FreeObj(subImageRender->_title);
        }
        
        subImageRender->_noLineBreak = row != 0;  // each row, only one has line break
        
        if (block) {
            block(subImageRender);
        }
        
        x += gridSize.width;
        row++;
        if (x + gridSize.width > renderRect.size.width) {
            y += gridSize.height;
            x = 0;
            row = 0;
        }
    }
    
    [subImageRender release];
}

- (BOOL)shouldShowsTitle {
    return _title && !self.isInHeader;
}

#pragma mark -
#pragma mark RTF render methods

/// The following code is used for generate hight quality image in RTF file
- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    if (self.renderMode == kCZImageRenderModeTile &&
        !self.isInHeader &&
        self.images.count > 1) {
        [self tileImageInRect:_renderFrame renderBlock:^(CZImageRender *subRender) {
            @autoreleasepool {
                [subRender rtfRenderIntoStream:_RTFOutputStream];
            }
        }];
    } else {
        BOOL isRTF = [[CZRTFPreviewConfig sharedInstance] isForRTF];
        
        if (isRTF) {
            if (!self.isInHeader) {
                [self renderRTFContent];
            }else{
                [self renderRTFContentInHeader];
            }
        }else{
            [self renderRTFDContent];
        }
    }
}

- (BOOL)isUseOriginalImage{
    BOOL isUse = YES;
    UIImage *image = nil;
    CGSize goalSize = [self preRenderLayout];
    CGRect renderRct = CGRectMake(0, 0, goalSize.width, goalSize.height);
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
        if (!image) {
            return isUse;
        }
        BOOL isNeedConfig = [[CZReportConfigManager sharedInstance] isNeedConfigImage:image
                                                                           renderRect:renderRct];
        if (isNeedConfig) {
            isUse = NO;
        }
        image = nil;
        
    }else{
        image = [[UIImage alloc]initWithContentsOfFile:self.imagePath];
        if (!image) {
             return isUse;
        }
        BOOL isNeedConfig = [[CZReportConfigManager sharedInstance] isNeedConfigImage:image
                                                                           renderRect:renderRct];
        if (isNeedConfig) {
            isUse = NO;
        }
        FreeObj(image);
    }
    return isUse;
}

#pragma mark -
#pragma mark RTF render header image methods
/**
 Special for image logo show in header,if image show in header,the layout like 
 linebreak and aligment is not same as it show in report body.
 */

- (void)renderOriginalheaderImage{
    NSString *imageRTFStr = nil;
    UIImage *image = nil;
    int32_t rtfWide = 0;
    int32_t rtfHeight =0;
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
        if (image == nil) {
            return;
        }
        rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
        image = nil;
        
    }else{
        image = [[UIImage alloc]initWithContentsOfFile:self.imagePath];
        if (image == nil) {
            return;
        }
        rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
        FreeObj(image);
    }

    if (!isUseBase64Data) {
        NSError *readError = nil;
        NSData *imageData = [NSData dataWithContentsOfFile:self.imagePath
                                                   options:NSDataReadingMappedIfSafe
                                                     error:&readError];
        assert(readError == nil);
        imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
        [[CZConvertImageToString sharedInstance] deleteTempFile];
    }else{
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
        [[CZConvertImageToString sharedInstance] deleteTempFile];
    }
    
    CGSize goalSize = [self preRenderLayout];
    int32_t goalWide = ceilf(goalSize.width * kPixelToTwips * kPdfToRTFScale);
    int32_t goalHeight = ceilf(goalSize.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,goalWide,goalHeight];

    
    [_RTFOutputStream writeString:[CZRTFTool textBoxPreInHeaderOrFooter]];
    
    
    [_RTFOutputStream writeString:imageFormatStr];
    [_RTFOutputStream writeString:@"\\pngblip "];
    if (imageRTFStr) {
        [_RTFOutputStream writeString:imageRTFStr];
    }
    [_RTFOutputStream writeString:@"}"];
}

- (void)renderConfigHeaderImage{
  @autoreleasepool {
    NSString *imageRTFStr = nil;
    UIImage *image = nil;
    UIImage *convertImage = nil;
    int32_t rtfWide = 0;
    int32_t rtfHeight =0;
    CGRect renderRct = CGRectMake(0, 0, [self preRenderLayout].width,
                                  [self preRenderLayout].height);
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
        convertImage = [[CZReportConfigManager sharedInstance]configImageForReport:image
                                                                          withRect:renderRct];
        if (convertImage == nil){
            return;
        }
        rtfWide = ceilf(convertImage.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(convertImage.size.height * kPixelToTwips * kPdfToRTFScale);
        image = nil;
    }else{
        image = [[UIImage alloc]initWithContentsOfFile:self.imagePath];
        convertImage = [[CZReportConfigManager sharedInstance]configImageForReport:image
                                                                          withRect:renderRct];
        if (convertImage == nil){
            FreeObj(image);
            return;
        }
        rtfWide = ceilf(convertImage.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(convertImage.size.height * kPixelToTwips * kPdfToRTFScale);
        FreeObj(image);
    }
    
    {
        NSError *readError = nil;
        //NSData *imageData = UIImagePNGRepresentation(convertImage);
        NSData *imageData = UIImageJPEGRepresentation(convertImage, 1.);
        assert(readError == nil);
        imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
        [[CZConvertImageToString sharedInstance] deleteTempFile];
    }
    
    CGSize goalSize = [self preRenderLayout];
    int32_t goalWide = ceilf(goalSize.width * kPixelToTwips * kPdfToRTFScale);
    int32_t goalHeight = ceilf(goalSize.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,goalWide,goalHeight];
    
    [_RTFOutputStream writeString:[CZRTFTool textBoxPreInHeaderOrFooter]];
    
    [_RTFOutputStream writeString:imageFormatStr];
    [_RTFOutputStream writeString:@"\\jpegblip "];
    //[_RTFOutputStream writeString:@"\\pngblip "];
    if (imageRTFStr) {
        [_RTFOutputStream writeString:imageRTFStr];
    }
    [_RTFOutputStream writeString:@"}"];
  }
}



- (void)renderRTFContentInHeader{ @autoreleasepool {
    /// convert the render content to image,and convert the image data to ASIC code
    if ([self isUseOriginalImage]) {
        [self renderOriginalheaderImage];
    }else{
        [self renderConfigHeaderImage];
    }
    [self generateRTFImageTitle];
    return;
}}

#pragma mark -
#pragma mark RTF render report body image methods

- (void)renderOriginalBodyImage{
    NSString *imageRTFStr = nil;
    
    UIImage *image = nil;
    int32_t rtfWide = 0;
    int32_t rtfHeight = 0;
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
        if (image == nil) {
            return;
        }
        rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
        image = nil;
    }else{
        image = [[UIImage alloc]initWithContentsOfFile:self.imagePath];
        if (image == nil) {
            return;
        }
        rtfWide = ceilf(image.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(image.size.height * kPixelToTwips * kPdfToRTFScale);
        FreeObj(image);
    }
    
    {
        if (!isUseBase64Data) {
            //NSData *imageData = [[NSData  alloc] initWithContentsOfFile:self.imagePath];
            //imageRTFStr = [CZRTFTool convertImageData2String:imageData];
            NSError *readError = nil;
            NSData *imageData = [NSData dataWithContentsOfFile:self.imagePath
                                                       options:NSDataReadingMappedIfSafe
                                                         error:&readError];
            assert(readError == nil);
            imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
            //FreeObj(imageData);
            [[CZConvertImageToString sharedInstance] deleteTempFile];
        }else{
            /// The image path contain the base64 image string.
            NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
            imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
            [[CZConvertImageToString sharedInstance] deleteTempFile];
        }
    }
    
    CGSize goalSize = [self preRenderLayout];
    int32_t goalWide = ceilf(goalSize.width * kPixelToTwips * kPdfToRTFScale);
    int32_t goalHeight = ceilf(goalSize.height * kPixelToTwips * kPdfToRTFScale);

    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,goalWide,goalHeight];
    
    NSString *textBoxPre = [CZRTFTool textBoxPre];
    [_RTFOutputStream writeString:textBoxPre];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
    
    ///[_RTFOutputStream writeString:@"\\jpegblip "];
    [_RTFOutputStream writeString:@"\\pngblip "];
    if (imageRTFStr) {
        [_RTFOutputStream writeString:imageRTFStr];
    }
    [_RTFOutputStream writeString:@"}"];
}

- (void)renderConfigBodyImage{
  @autoreleasepool {
    NSString *imageRTFStr = nil;
    
    UIImage *image = nil;
    UIImage *convertImage = nil;
    int32_t rtfWide = 0;
    int32_t rtfHeight =0;
    CGRect renderRct = CGRectMake(0,
                                  0,
                                  [self preRenderLayout].width,
                                  [self preRenderLayout].height);
    
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
        convertImage = [[CZReportConfigManager sharedInstance]configImageForReport:image
                                                                          withRect:renderRct];
        if (!convertImage) {
            return;
        }
        rtfWide = ceilf(convertImage.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(convertImage.size.height * kPixelToTwips * kPdfToRTFScale);
        image = nil;
    }else{
        image = [[UIImage alloc]initWithContentsOfFile:self.imagePath];
        convertImage = [[CZReportConfigManager sharedInstance]configImageForReport:image
                                                                          withRect:renderRct];
        if (!convertImage) {
            FreeObj(image);
            return;
        }
        rtfWide = ceilf(convertImage.size.width * kPixelToTwips * kPdfToRTFScale);
        rtfHeight = ceilf(convertImage.size.height * kPixelToTwips * kPdfToRTFScale);
        FreeObj(image);
    }
                      
    {
        NSData *imageData = UIImageJPEGRepresentation(convertImage, 1.0);
        imageRTFStr = [[CZConvertImageToString sharedInstance]convertImageData2String:imageData];
        [[CZConvertImageToString sharedInstance] deleteTempFile];
    }
    
    CGSize goalSize = [self preRenderLayout];
    int32_t goalWide = ceilf(goalSize.width * kPixelToTwips * kPdfToRTFScale);
    int32_t goalHeight = ceilf(goalSize.height * kPixelToTwips * kPdfToRTFScale);

    NSString *imageFormatStr = [NSString stringWithFormat:@"{\\pict\\wmetafile8\\picw%d\\pich%d\\picwgoal%d\\pichgoal%d",rtfWide,rtfHeight,goalWide,goalHeight];
    
    NSString *textBoxPre = [CZRTFTool textBoxPre];
    [_RTFOutputStream writeString:textBoxPre];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];


    [_RTFOutputStream writeString:@"\\jpegblip "];
    //[_RTFOutputStream writeString:@"\\pngblip "];
    if (imageRTFStr) {
        [_RTFOutputStream writeString:imageRTFStr];
    }
    [_RTFOutputStream writeString:@"}"];
  }
}

- (void)renderRTFContent{@autoreleasepool {
    /// convert the render content to image,and convert the image data to ASIC code
    if ([self isUseOriginalImage]) {
        [self renderOriginalBodyImage];
    }else{
        [self renderConfigBodyImage];
    }
    [self generateRTFImageTitle];
    return;
}}

    /** if want to preview the rtf ,we should follow the apple's standary of generate .rtfd file.
     eg: {{\NeXTGraphic 6_96491_7639eb5ee0f884c.png \width14120 \height15160 \noorient
     }¨}
     */
- (UIImage *)generateRTFDImage{
    CGSize imageRTFSize = _renderFrame.size;
    if ([self shouldShowsTitle]) {
        imageRTFSize = CGSizeMake(imageRTFSize.width, imageRTFSize.height + kImageTitleHeight);
    }
    UIGraphicsBeginImageContext(imageRTFSize);
    _renderFrame = CGRectMake(0, 0, _renderFrame.size.width, _renderFrame.size.height);
    [self render];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)renderRTFDContent{@autoreleasepool{
    
    /// convert the render content to image,and convert the image data to ASIC code
    NSString *frameStr = [CZRTFTool offsetFrame2RTFString:_renderFrame];
    NSString *fileName = nil;
    {
        /** this code is to use original image to produce image data
        NSData *imageData = [self generateRTFDImageData];
        fileName = [[CZRTFPreviewConfig sharedInstance] convertImageData2ImageFile:imageData];
        if (fileName == nil) {
            return;
        }
         **/
        /// Use pdf image context to generate image,and this image contain the title .
        UIImage *image = [self generateRTFDImage];
        if (!image) {
            return;
        }
        NSData *imageData = UIImageJPEGRepresentation(image,kJpegCompressPercent);
        fileName = [[CZRTFPreviewConfig sharedInstance] convertImageData2ImageFile:imageData];
        if (fileName == nil) {
            return;
        }
    }
    CGSize goalSize = [self preRenderLayout];
    int32_t goalWide = ceilf(goalSize.width * kPixelToTwips * kPdfToRTFScale);
    int32_t goalHeight = ceilf(goalSize.height * kPixelToTwips * kPdfToRTFScale);
    
    NSString *imageFormatStr = [NSString stringWithFormat:@"{{\\NeXTGraphic %@ \\width%d \\height%d \\noorient}%@}",fileName,goalWide/5,goalHeight/5,[CZRTFTool getNSStringUnicode:@"¨"]];
    
    [_RTFOutputStream writeString:frameStr];
    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:imageFormatStr];
}}


- (UIImage *)generateRTFImage{
    UIImage *image = [UIImage imageWithContentsOfFile:self.imagePath];
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
    }
    return image;
}

- (NSData *)generateRTFDImageData{
    UIImage *image = [UIImage imageWithContentsOfFile:self.imagePath];
    if (isUseBase64Data) {
        /// The image path contain the base64 image string.
        NSData *imageData = [CZBase64Data dataFromBase64String:self.imagePath];
        image = [UIImage imageWithData:imageData];
    }
    CGSize scaleSize = [self preRenderLayout];
    UIGraphicsBeginImageContext(scaleSize);
    [image drawInRect:CGRectMake(0, 0, scaleSize.width, scaleSize.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return UIImagePNGRepresentation(scaledImage);
    
}

- (void)generateRTFImageTitle{
    if (![self shouldShowsTitle]) {
     NSString *textBoxEnd = [CZRTFTool textBoxEnd:_renderFrame hasLineBreak:!_noLineBreak];
     [_RTFOutputStream writeString:textBoxEnd];
        return;
    }
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    NSDictionary *tableTitle = [self.renderContent valueForKey:kTitle];
    
    NSDictionary *fontDic = [tableTitle valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];

    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
    NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
    
    NSString *fString = [[CZRTFFontColorRegistTable sharedInstance] registFont2FontTable:font];
    NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];

    
    ///begin new added for set the location default is center.
    [_RTFOutputStream writeString:kLineBreak];
    [_RTFOutputStream writeString:kAlignmentCenter];
    ///begin new added for set the location default is center.
    
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:cfString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fString];
    [_RTFOutputStream writeString:@"\\"];
    [_RTFOutputStream writeString:fsString];
    [_RTFOutputStream writeString:kAlignmentCenter];
    [_RTFOutputStream writeString:@" "];
    NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_title];
    [_RTFOutputStream writeString:unicodeStr];
    [_RTFOutputStream writeString:kLineBreak];

    NSString *textBoxEnd = [CZRTFTool textBoxEnd:_renderFrame hasLineBreak:!_noLineBreak];
    [_RTFOutputStream writeString:textBoxEnd];
}





@end
