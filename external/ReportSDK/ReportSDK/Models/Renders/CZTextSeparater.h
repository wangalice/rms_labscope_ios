//
//  CZTextSeparater.h
//  ReportSDK
//
//  Created by Johnny on 3/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZPDFPage;
@class CZTextRender;

@interface CZTextSeparater : NSObject

@property (nonatomic, retain, readonly) NSArray *subTextRenders;

/** YES, if all the subrenders are not in |containedPage|. */
@property (nonatomic, assign, readonly) BOOL allInNextPages;

/// initialized method used the separate the big text to sever small text that can be rendered in one page .
- (id)initWithTextRender:(CZTextRender *)textRender page:(CZPDFPage *)pdfPage;

- (void)separateText;

@end
