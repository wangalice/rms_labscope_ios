//
//  CZTextRender.m
//  ReportSDK
//
//  Created by Johnny on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTextRender.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFTool.h"
/// this marging is in every text bottom.

@implementation CZTextRender
@synthesize text = _text;
@synthesize totalHeight;
@synthesize textSubOrder;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        _canBreak = [[_renderContent valueForKey:kCanBreak]boolValue];

        _isInHeader = NO;
        NSDictionary *value = [self.renderContent valueForKey:kValue];
        self.text = [value valueForKey:kTextValue];
        NSDictionary *fontDic = [_renderContent valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize<1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        CGSize renderContentSize = [CZDictionaryTool textSize:self.text
                                                      useFont:font
                                               constraintSize:CGSizeMake(_renderFrame.size.width,CGFLOAT_MAX)];
        CGRect renderRct = _renderFrame;
        self.renderFrame = CGRectMake(renderRct.origin.x,
                                      renderRct.origin.y,
                                       renderContentSize.width,
                                      renderContentSize.height);
        self.totalHeight = renderContentSize.height;
    }
    return self;
}

/// This function is used to generate text render when textElement is in header.
- (id)initWithHeaderContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        _isInHeader = YES;
        NSDictionary *value = [self.renderContent valueForKey:kValue];
        self.text = [value valueForKey:kTextValue];
    }
    return self;
}

- (void)setTextSubOrder:(NSInteger)aSubOrder{
    textSubOrder = aSubOrder;
    _renderOrder = _renderOrder + aSubOrder;
}

- (CGSize)preRenderLayout{
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize<1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    CZLogRect(_renderFrame);
    CZLog(@"the header element text is %@",_text);
    
    
    CGSize renderContentSize =[CZDictionaryTool textSize:self.text
                                                 useFont:font
                                          constraintSize:self.renderFrame.size];
    
    if (!_isInHeader) {
        if (renderContentSize.width > _renderFrame.size.width) {
            NSInteger showLength = ceilf([self.text length] * _renderFrame.size.width/renderContentSize.width);
            if (showLength >3) {
                NSRange aRange = NSMakeRange(0, showLength-3);
                NSString *textStr = [self.text substringWithRange:aRange];
                textStr = [textStr stringByAppendingString:@"..."];
                self.text = textStr;
            }
        }
        CZLogSize(renderContentSize);
        CZLogRect(self.renderFrame);
        return renderContentSize;
    }else{
        
        CGSize lengthSize = [CZDictionaryTool textSize:self.text useFont:font];
        NSInteger lineNum = ceilf(_renderFrame.size.height/lengthSize.height);
        if (lineNum >= 3) {
            lineNum = lineNum -1;
        }
        if (lengthSize.width > _renderFrame.size.width *lineNum) {
            NSInteger showLength = ceilf([self.text length] * _renderFrame.size.width * lineNum /lengthSize.width);
            if (showLength >5) {
                NSRange aRange = NSMakeRange(0, showLength-5);
                NSString *textStr = [self.text substringWithRange:aRange];
                textStr = [textStr stringByAppendingString:@"..."];
                self.text = textStr;
            }
        }
        CZLogSize(renderContentSize);
        return renderContentSize;
 
    }

}

- (void)render{
    [self preRenderLayout];
    [super render];
    if (!_text || [_text length]<1) {
        return;
    }
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    
    NSDictionary *color = [fontDic valueForKey:kColor];
    color = [CZDictionaryTool validateColor:color];

    CGFloat r = [[color valueForKey:kRedColor] floatValue];
    CGFloat g = [[color valueForKey:kGreenColor] floatValue];
    CGFloat b = [[color valueForKey:kBlueColor] floatValue];
    CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
    
    CGContextSetRGBFillColor(_currentContext, r, g, b, a);
    assert([self.text length]>0);
    
    
    // special the memory leak! This is the system leak.
    /*
    [self.text drawInRect:self.renderFrame
                 withFont:font
            lineBreakMode:NSLineBreakByWordWrapping
                alignment:NSTextAlignmentLeft];
    CZLog(@"render end of class %@",[self debugDescription]);
    */
    
    [self.text drawInRect:self.renderFrame
                 withFont:font
            lineBreakMode:NSLineBreakByCharWrapping
                alignment:NSTextAlignmentLeft];
    
    return;
}

#pragma mark -
#pragma mark RTF render methods

/** sample of rtf text render
    {\rtf1\ansi\deff0
    {\colortbl;\red0\green0\blue0;\red255\green0\blue0;}
    This line is the default color\line
    \cf2
    This line is red\line
    \cf1
    This line is the default color
    }
    Note that any space before the \ symbol is included in the text of the document, 
    so exclude them. To include the \ symbol in your document you use \\
**/

- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    // \pard\cf1\lang2052\f0\fs20 Hellow\cf0\par
    if (!_isInHeader) {
        NSDictionary *fontDic = [_renderContent valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
        NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
        
        NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
        NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
        
        NSString *offSetStr = [CZRTFTool textBoxLayOutFrameString:_renderFrame];
        
        [_RTFOutputStream writeString:@"\\par"];
        [_RTFOutputStream writeString:@"{\\shp\\shpbxcolumn\\shpbymargin{\\shptxt\\plain\\ql"];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:cfString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fsString];
        [_RTFOutputStream writeString:@" "];
        //[_RTFOutputStream writeString:offSetStr];
        NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_text];
        [_RTFOutputStream writeString:unicodeStr];
        
        ///new added for text box end editting.
        [_RTFOutputStream writeString:@" "];
        
        [_RTFOutputStream writeString:@"}"];
        [_RTFOutputStream writeString:offSetStr];
        [_RTFOutputStream writeString:@"}"];
        
        //CZLog(@"The text rtf string is %@",_RTFAttributeStr);

    }else{
        /// The following code is from CZHeaderComponentRender, and just used for render header.
        
        NSDictionary *fontDic = [_renderContent valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont systemFontOfSize:renderFontSize];
        }
        UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
        NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
        
        NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
        NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
        
        NSString *offSetStr = [CZRTFTool textBoxLayOutFrameString:_renderFrame];
        
        [_RTFOutputStream writeString:@"\\pard"];
        [_RTFOutputStream writeString:@"{\\shp\\shpbxcolumn\\shpbypage{\\shptxt\\plain\\ql"];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:cfString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fsString];
        [_RTFOutputStream writeString:@" "];
        //[_RTFOutputStream writeString:offSetStr];
        NSString* unicodeStr = [CZRTFTool getNSStringUnicode:_text];
        [_RTFOutputStream writeString:unicodeStr];
        
        ///new added for text box end editting.
        [_RTFOutputStream writeString:@" "];
        
        [_RTFOutputStream writeString:@"}"];
        [_RTFOutputStream writeString:offSetStr];
        [_RTFOutputStream writeString:@"}"];

    }
  }


- (void)dealloc{
    FreeObj(_text);
    [super dealloc];
}

@end
