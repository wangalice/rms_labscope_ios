//
//  CZTableColumnCellObj.m
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZTableColumnCellObj.h"
#import "CZDictionaryTool.h"

const CGFloat kTableCellTextShrinkMin = 0.5;

@implementation CZDrawOrder

@synthesize columnOrder;
@synthesize lineOrder;

- (void)dealloc{
    [super dealloc];
}
@end

@implementation CZTableColumnCellObj
@synthesize cellContent;
@synthesize order;
@synthesize columnOrder;
@synthesize drawOrder = _drawOrder;
@synthesize floatContent = _floatContent;
@synthesize isNum = _isNum;
@synthesize fontDictionary;
@synthesize isTitle;

- (id)initWithDic:(NSDictionary *)tableColumnCellDic{
    self = [super init];
    if (self) {
        NSString *elementType = [tableColumnCellDic valueForKey:kElementType];
        assert([elementType isEqualToString:kTableColumnCellElement]);
        NSDictionary *value = [tableColumnCellDic valueForKey:kValue];
        NSString *valueContent = [value valueForKey:kTableColumnCellValue];
        self.cellContent = valueContent;
        self.order = [[tableColumnCellDic valueForKey:kOrder] integerValue];
        NSDictionary *frameDic = [tableColumnCellDic valueForKey:kFrame];
        self.frame = [CZDictionaryTool dictionay2Frame:frameDic];
        
        _drawOrder = [[CZDrawOrder alloc]init];
        _drawOrder.columnOrder = 0;
        _drawOrder.lineOrder = 0;
        _isNum = NO;
    }
    return self;
}

- (id)initWithTitle:(NSString *)title{
    self = [super init];
    if (self) {
        self.cellContent = title;
        self.order = 0;
        isTitle = NO;
        _drawOrder = [[CZDrawOrder alloc]init];
        _drawOrder.columnOrder = 0;
        _drawOrder.lineOrder = 0;
    }
    return self;
}

- (CGRect)frame {
    return CGRectMake(_x, _y, _width, _height);
}

- (void)setFrame:(CGRect)frame {
    _x = frame.origin.x;
    _y = frame.origin.y;
    _width = frame.size.width;
    _height = frame.size.height;
}

- (void)setFloatContent:(double)floatContent{
    _floatContent = floatContent;
    _isNum = YES;
}

/// this method can not be used without the contenxt
- (void) drawToContenxt{
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
   // UIFont *font = [UIFont systemFontOfSize:8.0];
    /// begin to get the font and font size.
    NSDictionary *fontDic = self.fontDictionary;
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize <1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont systemFontOfSize:renderFontSize];
    }
    if (isTitle) {
        font = [UIFont boldSystemFontOfSize:renderFontSize];
    }
    /// end to get font and font size.
    
    /// compute the content lenght 
    //CGSize lengthSize = [self.cellContent sizeWithFont:font];
    CGSize textSize = [CZDictionaryTool textSize:self.cellContent useFont:font];

    if (textSize.width > self.width) {
        CGFloat shrinkRatio = (self.width - 4) / textSize.width;  // 4 is margin around text box
        shrinkRatio = MAX(kTableCellTextShrinkMin, shrinkRatio);
        font = [UIFont fontWithName:font.fontName size:renderFontSize * shrinkRatio];
    }
    
    CGRect frame = CGRectMake(_x, _y, _width, _height);
    
    if (self.backgroundColor) {
        CGFloat red, green, blue, alpha;
        [self.backgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
        CGFloat Y = 0.299 * red + 0.587 * green + 0.114 * blue;
        UIColor *fontColor = Y > 0.5 ? [UIColor blackColor] : [UIColor whiteColor];
        
        CGRect backgroundFrame = CGRectInset(frame, 0.5, 0.5);
        CGContextSetFillColorWithColor(currentContext, self.backgroundColor.CGColor);
        CGContextFillRect(currentContext, backgroundFrame);
        
        CGContextSetFillColorWithColor(currentContext, fontColor.CGColor);
    }
    
    [self.cellContent drawInRect:frame withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
}

- (void)dealloc{
    FreeObj(cellContent);
    FreeObj(_drawOrder);
    FreeObj(fontDictionary);
    FreeObj(_backgroundColor);
    [super dealloc];
}

@end
