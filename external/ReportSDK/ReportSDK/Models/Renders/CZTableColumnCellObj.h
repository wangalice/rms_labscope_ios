//
//  CZTableColumnCellObj.h
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const CGFloat kTableCellTextShrinkMin;

@interface CZDrawOrder : NSObject
@property (nonatomic, assign) NSInteger columnOrder;
@property (nonatomic, assign) NSInteger lineOrder;
@end


@interface CZTableColumnCellObj : NSObject{
    NSInteger   order;
    NSInteger   columnOrder;
    NSString    *cellContent;
    double      _floatContent;
    CZDrawOrder *_drawOrder;
    BOOL         _isNum;
}

@property (nonatomic, retain)      NSString    *cellContent;
@property (nonatomic)              double      floatContent;
@property (nonatomic)              NSInteger   order;
@property (nonatomic)              NSInteger   columnOrder;
@property (nonatomic)              CGRect      frame;
@property (nonatomic)              float      x;
@property (nonatomic)              float      y;
@property (nonatomic)              float      width;
@property (nonatomic)              float      height;
@property (nonatomic, retain)      CZDrawOrder *drawOrder;
@property (nonatomic, readonly)    BOOL        isNum;
@property (nonatomic)              BOOL        isTitle;
@property (nonatomic, retain)      UIColor     *backgroundColor;

@property (nonatomic, retain)      NSDictionary *fontDictionary;

- (id)initWithDic:(NSDictionary *)tableColumnCellDic;

- (id)initWithTitle:(NSString *)title;

- (void)drawToContenxt;

@end
