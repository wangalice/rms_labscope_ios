//
//  CZPageInfoRender.m
//  ReportSDK
//
//  Created by Johnny on 8/30/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPageInfoRender.h"
#import "NSOutputStream+String.h"
#import "CZDictionaryTool.h"
#import "CZRTFTool.h"
#import "CZRTFFontColorRegistTable.h"

#import "CZDictionaryTool.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFTool.h"

#define kPageInfo @"pageInfo"
#define kIsPageInfoVisible @"isPageInfoVisible"


@interface CZPageInfoRender (){
    BOOL isPageInfoVisible;
}

@end

@implementation CZPageInfoRender

@synthesize totalPageNum;
@synthesize curPageNum;

- (id)initWithContent:(NSDictionary *)contentDic frame:(CGRect)aFrame{
    self = [super initWithContent:contentDic frame:aFrame];
    if (self) {
        _canBreak = [[_renderContent valueForKey:kCanBreak]boolValue];
        isPageInfoVisible = [[_renderContent valueForKey:kIsPageInfoVisible] boolValue];
    }
    return self;
}

- (CGSize)preRenderLayout{
    NSDictionary *fontDic = [_renderContent valueForKey:kFont];
    fontDic = [CZDictionaryTool validateFontDic:fontDic];
    if (!fontDic) {
        fontDic = [CZDictionaryTool defaultFontDic];
    }
    CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
    if (renderFontSize<1) {
        renderFontSize = kDefaultTextFontSize;
    }
    if (renderFontSize > kMaxTextFontSize) {
        renderFontSize = kMaxTextFontSize;
    }
    NSString *pageInfoText = [NSString stringWithFormat:@"%lu / %lu",(unsigned long)self.curPageNum,(unsigned long)self.totalPageNum];
    NSString *fontName = [fontDic valueForKey:kFontName];
    UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
    if (!font) {
        font = [UIFont fontWithName:@"HelveticaNeue" size:renderFontSize];
    }
    CZLogRect(_renderFrame);
    
    
    CGSize renderContentSize =[CZDictionaryTool textSize:pageInfoText
                                                 useFont:font
                                          constraintSize:self.renderFrame.size];

    CZLogSize(renderContentSize);
    return renderContentSize;
}

- (void)render{
    [super render];
    if (isPageInfoVisible) {
        NSDictionary *pageInfoDic =_renderContent;
        NSString *pageInfoText = [NSString stringWithFormat:@"%lu / %lu",(unsigned long)self.curPageNum,(unsigned long)self.totalPageNum];
        
        NSDictionary *fontDic = [pageInfoDic valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont fontWithName:@"HelveticaNeue" size:renderFontSize];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat r = [[color valueForKey:kRedColor] floatValue];
        CGFloat g = [[color valueForKey:kGreenColor] floatValue];
        CGFloat b = [[color valueForKey:kBlueColor] floatValue];
        CGFloat a = [[color valueForKey:kAlphaColor] floatValue];
        
        CGContextSetRGBFillColor(_currentContext, r, g, b, a);
                
        [pageInfoText drawInRect:_renderFrame
                        withFont:font
                   lineBreakMode:NSLineBreakByWordWrapping
                       alignment:NSTextAlignmentLeft];
    }
}


- (void)rtfRenderIntoStream:(NSOutputStream *)stream {
    [super rtfRenderIntoStream:stream];
    if (isPageInfoVisible) {

        NSDictionary *fontDic = [_renderContent valueForKey:kFont];
        fontDic = [CZDictionaryTool validateFontDic:fontDic];
        if (!fontDic || ![fontDic isKindOfClass:[NSDictionary class]]) {
            fontDic = [CZDictionaryTool defaultFontDic];
        }
        NSDictionary *color = [fontDic valueForKey:kColor];
        color = [CZDictionaryTool validateColor:color];
        
        CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
        if (renderFontSize <1) {
            renderFontSize = kDefaultTextFontSize;
        }
        if (renderFontSize > kMaxTextFontSize) {
            renderFontSize = kMaxTextFontSize;
        }
        NSString *fontName = [fontDic valueForKey:kFontName];
        UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
        if (!font) {
            font = [UIFont fontWithName:@"HelveticaNeue" size:renderFontSize];
        }
        UIColor *textColor = [CZDictionaryTool dictionay2Color:color];
        NSString *cfString = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:textColor];
        
        NSString *fString = [[CZRTFFontColorRegistTable sharedInstance]registFont2FontTable:font];
        NSString *fsString = [CZRTFTool fontSize2RTFString:renderFontSize];
        
        
        NSString *offSetStr = [CZRTFTool textBoxLayOutFrameString:_renderFrame];
        
        [_RTFOutputStream writeString:@"\\pard"];
        [_RTFOutputStream writeString:@" {\\shp\\shpbxcolumn\\shpbypage{\\shptxt\\plain\\ql"];
        
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:cfString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fString];
        [_RTFOutputStream writeString:@"\\"];
        [_RTFOutputStream writeString:fsString];
        [_RTFOutputStream writeString:@" "];
        NSString* unicodeStr = [NSString stringWithFormat:@"%@",@"{\\insrsid2691151 }{\\field{\\*\\fldinst {\\insrsid2691151  PAGE }}{\\fldrslt {\\insrsid2691151 1}}}{\\insrsid2691151  / }{\\field{\\*\\fldinst {\\insrsid2691151  NUMPAGES }}{\\fldrslt {\\insrsid11226526 2}}}{\\insrsid2691151 \\pard }"];
        [_RTFOutputStream writeString:unicodeStr];
        
        
        [_RTFOutputStream writeString:@"}"];
        [_RTFOutputStream writeString:offSetStr];
        [_RTFOutputStream writeString:@"}"];
        //CZLog(@"The text rtf string is %@",_RTFAttributeStr);
        
    }
}

- (void)dealloc{
    [super dealloc];
}

@end
