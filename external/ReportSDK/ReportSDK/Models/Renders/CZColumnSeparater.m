//
//  CZColumnSeparater.m
//  ReportSDK
//
//  Created by Ralph Jin on 2/18/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZColumnSeparater.h"
#import "NSDictionary+MutableDeepCopy.h"
#import "CZColumnRender.h"

@interface CZColumnSeparater ()

@property (nonatomic, retain) NSMutableArray *subColumnRenders;
@property (nonatomic, retain, readonly) CZColumnRender *originalRender;

@end

@implementation CZColumnSeparater

- (id)initWithOriginalRender:(CZColumnRender *)originalRender {
    self = [super init];
    if (self) {
        _originalRender = [originalRender retain];
        
        _subColumnRenders = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_originalRender release];
    [_subColumnRenders release];
    
    [super dealloc];
}

- (void)separateMultiDataRender {
    if (!self.originalRender.hasMultiData) {
        return;
    }
    
    NSString *keyPath = [NSString stringWithFormat:@"%@.%@.%@", kValue, kColumnComponentArray, kValues];
    NSArray *subValues = [[self.originalRender.renderContent valueForKeyPath:keyPath] retain];
    
    NSUInteger i = 0;
    for (id subValue in subValues) {
        NSMutableDictionary *content = [self.originalRender.renderContent mutableDeepCopy];
        [content setValue:@[subValue] forKeyPath:keyPath];
        
        CZColumnRender *columnRender = [self generateSubRender:content first:(i == 0)];
        [content release];
        
        [self.subColumnRenders addObject:columnRender];
        i++;
    }
    
    [subValues release];
}

- (NSArray *)subRenders {
    return _subColumnRenders;
}

#pragma mark - private methods

- (CZColumnRender *)generateSubRender:(NSDictionary *)content first:(BOOL)first {
    CZColumnRender *newSubRender = [[CZColumnRender alloc] initWithContent:content
                                                                     frame:_originalRender.relativeFrame];
    if (!first) {
        CGRect frame = newSubRender.relativeFrame;
        frame.origin = CGPointZero;
        newSubRender.relativeFrame = frame;
    }
    
    newSubRender.renderOrder = _originalRender.renderOrder;
    
    return [newSubRender autorelease];
    
}

@end
