//
//  CZColumnSeparater.h
//  ReportSDK
//
//  Created by Ralph Jin on 2/18/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRenderSeperater.h"

@class CZColumnRender;

@interface CZColumnSeparater : NSObject <CZRenderSeperater>

/** initialized method used the separate the render with multi column data to
 * several renders that render one column data.
 */
- (id)initWithOriginalRender:(CZColumnRender *)originalRender;

@end
