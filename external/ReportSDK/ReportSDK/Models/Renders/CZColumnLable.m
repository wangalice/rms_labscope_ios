//
//  CZColumnLable.m
//  ReportSDK
//
//  Created by Johnny on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZColumnLable.h"

@implementation CZColumnLable
@synthesize label;
@synthesize x;
@synthesize y;
@synthesize width;
@synthesize height;

- (void)dealloc {
    [label release];
    [super dealloc];
}

@end
