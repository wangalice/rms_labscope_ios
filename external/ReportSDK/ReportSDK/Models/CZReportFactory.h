//
//  CZReportFactory.h
//  ReportSDK
//
//  Created by Johnny on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZReportData.h"
#import "CZSystemTemplate.h"
#import "CZPDFContentRenderManger.h"
#import "CZRTFContentRenderManager.h"
#import "CZTypedefine.h"
#import "CZReportEngine.h"

@interface CZReportFactory : NSObject

@property (nonatomic, retain)   CZSystemTemplate        *systemTemplate;
@property (nonatomic, retain)   NSString                *reportDestination;
@property (nonatomic, assign)   id<CZRenderDelegate>    delegate;
@property (atomic, assign, getter=isCancelled) BOOL     cancelled;
@property (atomic, assign, getter=isUserCancelled) BOOL userCancelled;  // if cancel is caused by user interaction or by internal error.

- (id)initWithData:(NSDictionary *)data systemTemplate:(CZSystemTemplate *)sysTemplate;

/// validate the template and data.
- (BOOL)dataAndTemplateValidate:(NSError **)aError;

/// use PDF content manage generate PDF report.
- (void)generatePDFReport;

/// use RTF content manage generate RTF report.
- (void)generateRTFReport;

/// use RTF content manage generate RTFD report.
- (void)generateRTFDReport;

///  generate the PDF for thumbnail,if PDF not exist,return boolean value indicate the generate is successful.
- (BOOL)generatePDFThumbnail:(NSString *)tempPath aRange:(NSRange)range;

@end
