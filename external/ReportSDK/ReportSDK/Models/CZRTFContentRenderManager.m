//
//  CZRTFContentRenderManager.m
//  ReportSDK
//
//  Created by Johnny on 9/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFContentRenderManager.h"

#import "NSOutputStream+String.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZPieRender.h"
#import "CZDictionaryTool.h"
#import "CZGlobal.h"
#import "CZDictionaryTool.h"
#import "CZImageRender.h"
#import "CZImageSeparator.h"
#import "CZColumnRender.h"
#import "CZColumnSeparater.h"
#import "CZPieRender.h"
#import "CZPieSeparater.h"
#import "CZLineRender.h"
#import "CZTableRender.h"
#import "CZRTFPage.h"
#import "CZDictionaryTool.h"
#import "CZTableSeparater.h"
#import "CZTextSeparater.h"
#import "CZRTFPage.h"
#import "CZHeaderRender.h"
#import "CZFooterRender.h"
#import "CZReportGeneratorErrorHandler.h"
#import "CZRTFFontColorRegistTable.h"
#import "CZRTFPreviewConfig.h"

#define kImageRenderWeightPercent 10.f
#define kNoImageRenderWeightPercent 1.f

@interface CZRTFContentRenderManager(){
    CZHeaderRender  *_headerRender;
    CZFooterRender  *_footerRender;
    NSMutableArray  *_renders;
    CGRect          _bodyFrame;
    NSMutableArray  *_RTFPages;
    
    CGFloat          _separatePageOffset;
    NSUInteger        _currentRenderIndex;
}

@property (nonatomic, retain) NSOutputStream *RTFStream;

- (void)paserContent;
- (void)paserReportHeader;
- (void)paserReportFooter;
- (void)paserReportBody;

@end

@implementation CZRTFContentRenderManager

- (id)initWithRenderContent:(NSDictionary *)aContentDic{
    self = [super initWithRenderContent:aContentDic];
    if (self) {
        NSDictionary *templateProfile = [aContentDic valueForKey:kReportTemplateMetadata];
        NSDictionary *pageSizeDic = [templateProfile valueForKey:kPageSize];
        _pageSize = [CZDictionaryTool dictionay2Size:pageSizeDic];
        _renders = [[NSMutableArray alloc]initWithCapacity:5];
        _RTFPages = [[NSMutableArray alloc]initWithCapacity:3];

        _separatePageOffset = 0.f;
        _bodyFrame = CGRectNull;
        CZLogSize(_pageSize);
    }
    return self;
}



- (void)updateGeneratePercentage:(NSNumber *)percent finished:(BOOL)finished {
    CGFloat percentage = [percent floatValue];
    if (percentage > 1.f) {
        percentage = 1.f;
    }
    
    if (finished) {
        percentage = 1.f;
    } else {
        if (percentage > 0.99) {
            percentage = 0.99;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(notifyRenderProgress:currentPercentage:destination:)]) {
        [self.delegate notifyRenderProgress:ReportEngineReportTypePDF
                          currentPercentage:percentage
                                destination:_reportDestination];
    }
    
}

- (void)notifyGeneratePercentage:(CGFloat)percent finished:(BOOL)finished {
    [self updateGeneratePercentage:@(percent) finished:finished];
}

#pragma mark -  generate PDF FILE Methods

- (void)startGenerate {
    @autoreleasepool {
        [super startGenerate];
        
        NSString *filePath = nil;
        if (![[CZRTFPreviewConfig sharedInstance] isForRTF]) {
            CZLog(@"This is write for RTFD!");
            filePath = [[CZRTFPreviewConfig sharedInstance] subRTFFilePath];
        } else {
            filePath = _reportDestination;
        }
        
        if (filePath.length > 0) {
            NSOutputStream *stream = [[NSOutputStream alloc] initToFileAtPath:filePath append:NO];
            [stream open];
            self.RTFStream = stream;
            [stream release];
        }
        
        if (self.RTFStream == nil) {
            [self notifyError:CZReportGeneratorErrorWriteRTFToFileUnexpected];
            return;
        }
        
        [self paserContent];
        [self separatePageAndLayout];
        [self registerColor];
        [self render];
        
        [self.RTFStream close];
        self.RTFStream = nil;
        
        [self notifyGeneratePercentage:1.f finished:YES];
    }
}

- (void)registerColor {
    NSArray *renderLists = @[_renders, _headerRender.headerElements, _footerRender.footerElements];
    for (NSArray *renderList in renderLists) {
        for (CZRenderBase *render in renderList) {
            [render registerRTFColor:[CZRTFFontColorRegistTable sharedInstance]];
        }
    }
}

- (void)render {
    CZLog(@"%@",_reportDestination);
    assert([self.reportDestination length]>0);
    
    // Pre-load font and color information from the dictionary to register into
    // RTF font table and color table before actual rendering.
    NSArray *renderLists = @[_renders, _headerRender.headerElements, _footerRender.footerElements];
    for (NSArray *renderList in renderLists) {
        for (CZRenderBase *render in renderList) {
            NSDictionary *dict = render.renderContent;
            if ([render isKindOfClass:[CZImageRender class]] ||
                [render isKindOfClass:[CZPieRender class]] ||
                [render isKindOfClass:[CZColumnRender class]]) {
                dict = [dict valueForKey:@"title"];
            }
            
            if (dict == nil) {
                continue;
            }
            
            NSDictionary *fontDic = [dict valueForKey:kFont];
            fontDic = [CZDictionaryTool validateFontDic:fontDic];
            if (!fontDic ||![fontDic isKindOfClass:[NSDictionary class]]) {
                fontDic = [CZDictionaryTool defaultFontDic];
            }
            CGFloat renderFontSize = [[fontDic valueForKey:kFontSize] floatValue];
            if (renderFontSize < 1) {
                renderFontSize = kDefaultTextFontSize;
            }
            if (renderFontSize > kMaxTextFontSize) {
                renderFontSize = kMaxTextFontSize;
            }
            NSString *fontName = [fontDic valueForKey:kFontName];
            UIFont *font = [UIFont fontWithName:fontName size:renderFontSize];
            if (!font) {
                font = [UIFont systemFontOfSize:renderFontSize];
            }
            NSDictionary *colorDic = [fontDic valueForKey:kColor];
            colorDic = [CZDictionaryTool validateColor:colorDic];
            
            UIColor *color = [CZDictionaryTool dictionay2Color:colorDic];
            
            [[CZRTFFontColorRegistTable sharedInstance] registFont2FontTable:font];
            [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:color];
        }
    }
    
    /// write the rtf version, language,character map
    [self appendFileAttributesToRTFStream];
    
    /**add some RTF paper and header, footer layout information, we can set the paper size,page
     * size,heaer margin, and some other layout information here.
     */
    [self appendLayoutToRTFStream];
    
    /// write the rtf font table
    [self appendStringToRTFStream:[[CZRTFFontColorRegistTable sharedInstance] fontTableRTFDes]];
    
    /// write the rtf color table
    [self appendStringToRTFStream:[[CZRTFFontColorRegistTable sharedInstance] fontColorRTFDes]];
    
    [[CZRTFFontColorRegistTable sharedInstance] setPreRenderObj:nil];
    
    if (_headerRender && !self.isCancelled) {
        [_headerRender rtfRenderIntoStream:self.RTFStream];
    }
    
    if (_footerRender && !self.isCancelled) {
        [_footerRender rtfRenderIntoStream:self.RTFStream];
    }
    
    if (_RTFPages) {
        _currentPageIndex = 0;
        _totalPages = [_RTFPages count];
        for (CZRTFPage *aPage in _RTFPages) {
            if (self.isCancelled) {
                break;
            }
            _currentPageIndex = _currentPageIndex + 1;
            @autoreleasepool {
                [aPage drawPageIntoStream:self.RTFStream];
            }
            [self notifyGeneratePercentage:(_currentPageIndex / (float)_totalPages) finished:NO];
        }
    }
    
    [self appendEndSpaceToRTFStream];
}

- (void)appendFileAttributesToRTFStream {
    [self appendStringToRTFStream:@"{\\rtf1\\fbidis\\ansi\\deff0"];
}

- (void)appendLayoutToRTFStream {
    CGFloat headerHeight = 0.f;
    CGFloat footerHeight = 0.f;
    if (_headerRender) {
        headerHeight = CGRectGetMaxY(_headerRender.renderFrame);
    }
    if (_footerRender) {
        footerHeight = _pageSize.height - _footerRender.renderFrame.origin.y;
        footerHeight = MAX(0, footerHeight);
    }
    
    CGFloat bodyHeight = _pageSize.height - headerHeight - footerHeight;
    assert(bodyHeight >0);
    
    int32_t footerTwipsHeight = ceilf(footerHeight* kPixelToTwips * kPdfToRTFScale);
    int32_t headerTwipsHeight = ceilf(headerHeight* kPixelToTwips * kPdfToRTFScale);
    
    int32_t paperHeight =ceilf( _pageSize.height * kPixelToTwips * kPdfToRTFScale);
    int32_t paperWidth =ceilf( _pageSize.width * kPixelToTwips * kPdfToRTFScale);
    
    NSString *layoutStr = [NSString stringWithFormat:@"\\deftab%d\\paperw%d\\paperh%d\\margl0\\margr0\\margt%d\\margb%d\\footery0\\headery0",kTabDefaultTwipWidth,paperWidth,paperHeight,headerTwipsHeight,footerTwipsHeight];
    
    [self appendStringToRTFStream:layoutStr];
}

/// note: if the string is Chinese ,should be converted!
- (void)appendStringToRTFStream:(NSString *)aString {
    if (aString && [aString length]>0) {
        //CZLog(@"the append string is %@",aString);
        [_RTFStream writeString:aString];
    }
}

- (void)appendEndSpaceToRTFStream{
    [self appendStringToRTFStream:@"}"];
}

- (void)paserContent{
    [self notifyGeneratePercentage:0.f finished:NO];
    [self paserReportHeader];
    [self paserReportFooter];
    [self paserReportBody];
}

- (void)paserReportHeader{
    NSDictionary *reportHeaderDic = [_renderContent valueForKey:kReportHeader];
    if (reportHeaderDic) {
        CGRect frame = [CZDictionaryTool dictionay2Frame:[reportHeaderDic valueForKey:kFrame]];
        _headerRender = [[CZHeaderRender alloc]initWithContent:reportHeaderDic
                                                         frame:frame];
    }
}

- (void)paserReportFooter{
    NSDictionary *reportFooterDic = [_renderContent valueForKey:kReportFooter];
    if (reportFooterDic) {
        CGRect frame = [CZDictionaryTool dictionay2Frame:[reportFooterDic valueForKey:kFrame]];
        _footerRender = [[CZFooterRender alloc]initWithContent:reportFooterDic
                                                         frame:frame];
    }
}


- (void)paserReportBody{
    NSDictionary *reportBodyDic = [_renderContent valueForKey:kReportBody];
    if (!reportBodyDic) {
        return ;
    }
    
    NSArray *bodyElements = [reportBodyDic valueForKey:kBodyElements];
    assert([bodyElements count]>0);
    for (NSDictionary *dicObj in bodyElements) {
        
        NSString *elementType = [dicObj valueForKey:kElementType];
        assert(elementType != nil && [elementType length]>0);
        CGRect frame = [CZDictionaryTool dictionay2Frame:[dicObj valueForKey:kFrame]];
        
        if ([elementType isEqualToString:kLineElement]) {
            CZLineRender *lineRender = [[CZLineRender alloc] initWithContent:dicObj
                                                                       frame:frame];
            lineRender.isInBody = YES;
            [_renders addObject:lineRender];
            [lineRender release];
            lineRender = nil;
            
        }else if ([elementType isEqualToString:kPieElement]){
            CZPieRender *pieRender = [[CZPieRender alloc]initWithContent:dicObj
                                                                   frame:frame];
            [_renders addObject:pieRender];
            [pieRender release];
            pieRender = nil;
            
        }else if ([elementType isEqualToString:kColumnElement]){
            CZColumnRender *columnRender = [[CZColumnRender alloc]initWithContent:dicObj
                                                                            frame:frame];
            [_renders addObject:columnRender];
            [columnRender release];
            columnRender = nil;
            
        }else if ([elementType isEqualToString:kTableElement]){
            CZTableRender *tableRender = [[CZTableRender alloc]initWithContent:dicObj
                                                                         frame:frame];
            [_renders addObject:tableRender];
            [tableRender release];
            tableRender = nil;
            
        }else if ([elementType isEqualToString:kImageElement]){
            CZImageRender *imageRender = [[CZImageRender alloc]initWithContent:dicObj
                                                                         frame:frame];
            [_renders addObject:imageRender];
            [imageRender release];
            
        }else if ([elementType isEqualToString:kTextElement]){
            CZTextRender *textRender = [[CZTextRender alloc]initWithContent:dicObj
                                                                      frame:frame];
            [_renders addObject:textRender];
            [textRender release];
            textRender = nil;
        }
    }
    [self reSortOrder];
}

- (void)reSortOrder{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor count:1];
    [_renders sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    for (NSUInteger i = 0; i< [_renders count]; i++) {
        CZRenderBase *renderObj = [_renders objectAtIndex:i];
        if (i == 0) {
            CGRect rederRct = renderObj.renderFrame;
            if (rederRct.origin.x<0) {
                rederRct = CGRectMake(1, rederRct.origin.y, rederRct.size.width, rederRct.size.height);
            }
            if (rederRct.origin.y <0) {
                rederRct = CGRectMake(rederRct.origin.x, 1, rederRct.size.width, rederRct.size.height);
            }
            renderObj.renderFrame = rederRct;
        }
        renderObj.renderOrder = renderObj.renderOrder + i*2;
    }
}

// TODO: similar as CZPDFContentRenderManger preLayout, can extract to base class
- (void)preLayout {
    _bodyFrame.origin = CGPointZero;
    _bodyFrame.size = _pageSize;
    
    if (_headerRender) {
        _bodyFrame.origin.x = MAX(_bodyFrame.origin.x, _headerRender.renderFrame.origin.x);
        _bodyFrame.size.width = MIN(_bodyFrame.size.width, _headerRender.renderFrame.size.width);
        _bodyFrame.origin.y = CGRectGetMaxY(_headerRender.renderFrame);
        _bodyFrame.size.height -= _bodyFrame.origin.y;
    }
    if (_footerRender) {
        _bodyFrame.origin.x = MAX(_bodyFrame.origin.x, _footerRender.renderFrame.origin.x);
        _bodyFrame.size.width = MIN(_bodyFrame.size.width, _footerRender.renderFrame.size.width);
        _bodyFrame.size.height = _footerRender.renderFrame.origin.y - _bodyFrame.origin.y;
    }
    
    // before layout, seperate all the multipage mode image renderers
    NSArray *tempArray = [_renders copy];
    for (CZRenderBase *renderObj in tempArray) {
        id <CZRenderSeperater> separator = nil;
        // separate image render which mode is |kCZImageRenderModeMultipage|
        if ([renderObj isKindOfClass:[CZImageRender class]]) {
            CZImageRender *imageRender = (CZImageRender *)renderObj;
            if (imageRender.renderMode == kCZImageRenderModeMultipage && !imageRender.isInHeader) {
                separator = [[CZImageSeparater alloc] initWithImageRender:imageRender];
                [self breakMultiDataRender:imageRender withSeparator:separator];
            }
        } else if ([renderObj isKindOfClass:[CZColumnRender class]]) {
            CZColumnRender *columnRender = (CZColumnRender *)renderObj;
            separator = [[CZColumnSeparater alloc] initWithOriginalRender:columnRender];
            [self breakMultiDataRender:columnRender withSeparator:separator];
        } else if ([renderObj isKindOfClass:[CZPieRender class]]) {
            CZPieRender *pieRender = (CZPieRender *)renderObj;
            separator = [[CZPieSeparater alloc] initWithOriginalRender:pieRender];
            [self breakMultiDataRender:pieRender withSeparator:separator];
        } else if ([renderObj isKindOfClass:[CZTableRender class]]) {
            CZTableRender *tableRender = (CZTableRender *)renderObj;
            separator = [[CZTableSeparater alloc] initWithTableRender:tableRender page:nil];
            [self breakMultiDataRender:tableRender withSeparator:separator];
        }
        
        [separator release];
    }
    
    [tempArray release];
}

// TODO: similar as CZPDFContentRenderManger renderObj:toWholePage:, can extract to base class
/** reset render's frame to whole page.
 *
 * @param renderObj, the renderer to reset.
 * @param aPage, the page to reference
 * @param previousRect, previous renderer object's render frame.
 */
- (void)expandRender:(CZRenderBase *)renderObj toWholePage:(CZRTFPage *)aPage previousRenderRect:(CGRect)previousRect {
    if ([renderObj isKindOfClass:[CZImageRender class]]) {
        // make image render use whole page, if image mode is ...
        CZImageRender *imageRender = (CZImageRender *)renderObj;
        if (imageRender.renderMode == kCZImageRenderModePendding ||
            imageRender.renderMode == kCZImageRenderModeTile) {
            
            CGRect tempFrame = imageRender.relativeFrame;
            tempFrame.origin.x = -previousRect.origin.x;
            tempFrame.size = aPage.pageBodyFrame.size;
            tempFrame.size.height -= 2;  // height just can't be same as page height
            
            imageRender.relativeFrame = tempFrame;
            imageRender.renderFrame = tempFrame;
            
            if (imageRender.renderMode == kCZImageRenderModePendding) {
                imageRender.renderMode = kCZImageRenderModeNormal;
            }
        }
    }
}

- (void)separatePageAndLayout{
    [self preLayout];
    
    _currentRenderIndex = 0;
    CGSize pageBodySize = _bodyFrame.size;
    
    NSInteger pageIndex = 0;
    CGFloat   pointY = 0.f;
    NSUInteger emptyPageCount = 0;
    
    do {
        CZLog(@"currrent render index is %lu",(unsigned long)_currentRenderIndex);
        CGRect pageBodyFrame = CGRectMake(0, pointY + pageIndex*pageBodySize.height, pageBodySize.width, pageBodySize.height);
        CZRTFPage *page = [[CZRTFPage alloc] initWithBodyFrame:pageBodyFrame];
        CZRTFPage *separatedPage = [self addRendersToPage:page];
        [page release];
        separatedPage.pageIndex = pageIndex + 1;
        
        // set offset of the body part
        separatedPage.pageBodyOffset = _bodyFrame.origin;
        
        if (separatedPage.renders.count == 0) {
            emptyPageCount++;
            if (emptyPageCount >= 3) {
                [self notifyError:CZReportGeneratorErrorPageNumberUnexpected];
                break;
            }
        } else {
            emptyPageCount = 0;
        }
        
        [_RTFPages addObject:separatedPage];
        
        pageIndex = pageIndex + 1;
        CZLog(@"currrent render count is %lu",(unsigned long)[_renders count]);
        
        if (pageIndex > 100) {
            [self notifyError:CZReportGeneratorErrorPageNumberUnexpected];
            break;
        }
        
        if (self.isCancelled) {
            break;
        }
    } while (_currentRenderIndex < [_renders count]);
    
    for (CZRTFPage *aPdfPage in _RTFPages) {
        aPdfPage.totalPageNum = [_RTFPages count];
    }
}

- (CZRTFPage *)addRendersToPage:(CZRTFPage *)aPage {
    CZRTFPage *page = [[CZRTFPage alloc] initWithBodyFrame:aPage.pageBodyFrame];
    page.pageHeader = _headerRender;
    page.pageFooter = _footerRender;
    [page autorelease];
    NSInteger renderFromIndex = _currentRenderIndex;
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRenderOrderSortAttribute
                                                                   ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:&sortDescriptor
                                                          count:1];
    [_renders sortUsingDescriptors:sortDescriptors];
    FreeObj(sortDescriptor);
    FreeObj(sortDescriptors);
    // TO DO ... separate some pages ...
    CGRect renderRct = CGRectZero;
    for (CZRenderBase *renderObj in _renders) {
        if (self.isCancelled) {
            break;
        }
        
        [self expandRender:renderObj toWholePage:aPage previousRenderRect:renderRct];
        
        CGRect renderFrame = renderObj.renderFrame;
        CGRect relativeFrame = renderObj.relativeFrame;
        CZLogRect(renderFrame);
        
        CGFloat yOffset = relativeFrame.origin.y + renderRct.size.height;
        if (yOffset < 0) {
            yOffset = 0;
        }
        
        renderObj.renderFrame = CGRectMake(relativeFrame.origin.x + renderRct.origin.x,
                                           yOffset + renderRct.origin.y,
                                           renderFrame.size.width,
                                           renderFrame.size.height);
        CZLogRect(renderObj.renderFrame);
        renderRct = renderObj.renderFrame;
    }
    CGRect pageBodyFrame = aPage.pageBodyFrame;
    for (NSUInteger i = renderFromIndex; i< [_renders count]; i++) {
        if (self.isCancelled) {
            break;
        }
        CZRenderBase *renderObj = [_renders objectAtIndex:i];
        CGRect renderFrame = renderObj.renderFrame;
        
        BOOL isPartlyContain = CGRectContainsPoint(pageBodyFrame,  CGPointMake(5,renderFrame.origin.y));
        if (isPartlyContain) {
            BOOL isContain = CGRectContainsRect(pageBodyFrame, renderFrame);
            if (pageBodyFrame.origin.y <= renderFrame.origin.y &&
                pageBodyFrame.origin.y + pageBodyFrame.size.height >=
                renderFrame.origin.y + renderFrame.size.height) {
                isContain = YES;
            }
            if (isContain) {
                [page.renders addObject:renderObj];
                _currentRenderIndex = i+1;
            }else{
                /// compute the offset of render elements that can not be breaked.
                if (!renderObj.canBreak) {
                    CGFloat offsetY = page.pageBodyFrame.origin.y+pageBodyFrame.size.height-renderObj.renderFrame.origin.y + 1; /// 5 is the margin of every page
                    CGRect beforeRelativeRct = renderObj.relativeFrame;
                    renderObj.relativeFrame = CGRectMake(beforeRelativeRct.origin.x,
                                                         beforeRelativeRct.origin.y + offsetY,
                                                         beforeRelativeRct.size.width,
                                                         beforeRelativeRct.size.height);
                    _currentRenderIndex = i;
                    
                    if (renderObj.renderFrame.size.height > page.pageBodyFrame.size.height) {
                        [self notifyError:CZReportGeneratorErrorTemplateCanbreakSettingError];
                    }
                    break;
                }else{
                    CZRenderBase *aContainObj = [self breakBigRenders:renderObj
                                                                 page:page];
                    
                    if (aContainObj) {
                        [page.renders addObject:aContainObj];
                        _currentRenderIndex = i + 1;
                    }else{
                        _currentRenderIndex = i;
                    }
                    break;
                }
            }
        }else{
            _currentRenderIndex = i;
            break;
        }
    }
    return page;
}

- (CZRenderBase *)breakBigRenders:(CZRenderBase *)aBigRender page:(CZRTFPage *)aPage{
    CZRenderBase * smallRender = nil;
    if ([aBigRender isKindOfClass:[CZTableRender class]]) {
        smallRender =[self breakBigTable:(CZTableRender*)aBigRender PDFPage:aPage];
    }else if ([aBigRender isKindOfClass:[CZTextRender class]]){
        smallRender =[self breakBigText:(CZTextRender*)aBigRender PDFPage:aPage];
    }
    return smallRender;
}

- (CZRenderBase *)breakBigTable:(CZTableRender *)table PDFPage:(CZRTFPage *)aPage{
    CZTableSeparater *separater = [[CZTableSeparater alloc]initWithTableRender:table
                                                                          page:aPage];
    [separater separateTable];
    
    NSInteger tableIndex = [_renders indexOfObject:table];
    if ([separater.subTableRenders count]>1 && (tableIndex < [_renders count]-1)) {
        for (NSUInteger i = tableIndex +1; i<[_renders count]; i++) {
            CZRenderBase *renderObj = [_renders objectAtIndex:i];
            CZLog(@"the befor order is %ld",(long)(renderObj.renderOrder));
            NSInteger order = renderObj.renderOrder + [separater.subTableRenders count];
            renderObj.renderOrder =  order;
            CZLog(@"the after order is %ld",(long)(renderObj.renderOrder));
        }
    }
    if ([separater.subTableRenders count] == 1) {
        [_renders replaceObjectAtIndex:tableIndex withObject:[separater.subTableRenders objectAtIndex:0]];
    }else{
        [_renders addObjectsFromArray:separater.subTableRenders];
        [_renders removeObject:table];
    }
    
    if ([separater.subTableRenders count] == 1 || separater.isAdjust) {
        [separater release];
        return nil;
    }
    CZTableRender *prePageTable = [separater.subTableRenders objectAtIndex:0];
    [prePageTable retain];
    [prePageTable autorelease];
    [separater release];
    
    return prePageTable;
}

- (CZRenderBase *)breakBigText:(CZTextRender *)text PDFPage:(CZRTFPage *)aPage{
    CZTextSeparater *separater = [[CZTextSeparater alloc]initWithTextRender:text
                                                                       page:aPage];
    [separater separateText];
    //[separater.subTextRenders makeObjectsPerformSelector:@selector(printOrder)];
    
    NSInteger textIndex = [_renders indexOfObject:text];
    if ([separater.subTextRenders count]>1 && (textIndex < [_renders count]-1)) {
        for (NSUInteger i = textIndex +1; i<[_renders count]; i++) {
            CZRenderBase *renderObj = [_renders objectAtIndex:i];
            CZLog(@"the befor order is %ld",(long)renderObj.renderOrder);
            NSInteger order = renderObj.renderOrder + [separater.subTextRenders count];
            renderObj.renderOrder =  order;
            CZLog(@"the after order is %ld",(long)renderObj.renderOrder);
        }
    }
    if ([separater.subTextRenders count] == 1) {
        [_renders replaceObjectAtIndex:textIndex withObject:[separater.subTextRenders objectAtIndex:0]];
    }else{
        [_renders addObjectsFromArray:separater.subTextRenders];
        //[separater.subTextRenders makeObjectsPerformSelector:@selector(printOrder)];
        [_renders removeObject:text];
    }
    
    if ([separater.subTextRenders count] == 1 || separater.allInNextPages) {
        [separater release];
        return nil;
    }
    
    CZTextRender *prePageText = [separater.subTextRenders objectAtIndex:0];
    [prePageText retain];
    [prePageText autorelease];
    [separater release];
    return prePageText;
}


// TODO: similar as CZPDFContentRenderManger breakMutliDataRender:withSeparator:, can extract to base class
- (BOOL)breakMultiDataRender:(CZRenderBase *)multiDataRender withSeparator:(id<CZRenderSeperater>)separator {
    [separator separateMultiDataRender];
    
    NSUInteger index = [_renders indexOfObject:multiDataRender];
    NSUInteger subColCount = [separator.subRenders count];
    if (subColCount == 1) {
        CZRenderBase *newRener = [separator.subRenders objectAtIndex:0];
        newRener.renderOrder = multiDataRender.renderOrder;
        [_renders replaceObjectAtIndex:index withObject:[separator.subRenders objectAtIndex:0]];
    } else if (subColCount > 1) {  // add |serparater.subRenders| to _renders and adjust render order
        NSUInteger renderOrder = multiDataRender.renderOrder;
        for (CZRenderBase *renderObj in separator.subRenders) {
            renderObj.renderOrder = renderOrder;
            renderOrder++;
        }
        
        renderOrder = multiDataRender.renderOrder;
        for (CZRenderBase *renderObj in _renders) {
            if (renderObj.renderOrder >= renderOrder) {
                renderObj.renderOrder = renderObj.renderOrder + subColCount - 1;
            }
        }
        
        [_renders addObjectsFromArray:separator.subRenders];
        [_renders removeObject:multiDataRender];
    }
    
    return (subColCount > 1);
}

- (void)dealloc{
    self.delegate = nil;
    FreeObj(_renders);
    FreeObj(_RTFPages);
    FreeObj(_footerRender);
    FreeObj(_headerRender);
    FreeObj(_RTFStream);
    [super dealloc];
}

@end
