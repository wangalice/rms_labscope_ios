//
//  CZReportFactoryTests.h
//  ReportSDK
//
//  Created by Johnny on 3/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZReportFactory.h"
#import "CZReportData.h"
#import "CZSystemTemplate.h"

@interface CZReportFactoryTests : XCTestCase{
    
    CZReportFactory     *_factory;
    CZReportFactory     *_RTFFactory;

}

@end
