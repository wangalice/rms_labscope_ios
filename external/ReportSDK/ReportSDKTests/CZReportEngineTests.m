//
//  CZReportEngineTests.m
//  ReportSDK
//
//  Created by Johnny on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportEngineTests.h"
#import "CZRTFPreviewConfig.h"

#define kReportDir @"/Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/F8948F24-77C6-4A8F-83A3-351277B7CC76/Documents/"



@implementation CZReportEngineTests

- (void)setUp{
    [super setUp];
    // Set-up code here.
    NSString *templatePath = [[NSBundle bundleForClass:[CZReportEngineTests class]] pathForResource:@"reportTemplateSampleOriginal_nodata" ofType:@"plist"];
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportEngineTests class]]
                                   pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *reportData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    NSDictionary *reportTemplate = [NSDictionary dictionaryWithContentsOfFile:templatePath];
    _reportEngine = [[CZReportEngine alloc]initEngine:reportData
                                       reportTemplate:reportTemplate];
    _reportEngine.dataDic = reportData;
    _reportEngine.delegate = nil;
}

- (void)tearDown{
    
    [_reportEngine release];
    [super tearDown];
}

- (void)testAttributes{
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportEngineTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *reportData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    [_reportEngine setDataDic:reportData];
    
    NSDictionary *template = [_reportEngine templateDic];
    XCTAssertTrue([[template allKeys] count]>0,@"read template form engine failed.");

}

- (void)testGenerateRTF{
    /** For the UT does not have its own file desitination ,so use the demo file path.
     *  /Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/2839C78F-4292-484B-B8FF-1F30863E3508/Documents/CZDemo.pdf
     */
    NSString *fileName = @"UtDemo.rtf";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *rtfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    rtfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,fileName];
    
    NSError *aError = nil;
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportEngineTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *reportData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    _reportEngine.dataDic = reportData;
    [_reportEngine validateReport:&aError isReport:YES];

    [_reportEngine exportReport:rtfFileName reportType:ReportEngineReportTypeRTF imageCompressRate:0.5 useDpi:200];
    BOOL isTrue = aError == nil ? YES:NO;
    XCTAssertTrue(isTrue,@"report generate error");
}

- (void)testGenerateRTFD{
    /** For the UT does not have its own file desitination ,so use the demo file path.
     *  /Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/2839C78F-4292-484B-B8FF-1F30863E3508/Documents/CZDemo.pdf
     */
    NSString *fileName = @"UtDemo.rtfd";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *rtfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    rtfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,fileName];
    
    NSError *aError = nil;
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportEngineTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *reportData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    _reportEngine.dataDic = reportData;
    [_reportEngine validateReport:&aError isReport:YES];
    
    [_reportEngine exportReport:rtfFileName reportType:ReportEngineReportTypeRTFD imageCompressRate:0.5 useDpi:200];
    BOOL isTrue = aError == nil ? YES:NO;
    XCTAssertTrue(isTrue,@"report generate error");
}

- (void)testGeneratePDF{
    /** For the UT does not have its own file desitination ,so use the demo file path.
     *  /Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/2839C78F-4292-484B-B8FF-1F30863E3508/Documents/CZDemo.pdf
     */
    NSString *fileName = @"UtDemo.pdf";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    pdfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,fileName];
    
    
    NSError *aError = nil;
    [_reportEngine validateReport:&aError isReport:YES];
    [_reportEngine exportReport:pdfFileName reportType:ReportEngineReportTypePDF imageCompressRate:0.5 useDpi:200];
    BOOL isTrue = aError == nil ? YES:NO;
    XCTAssertTrue(isTrue,@"report generate error");

}

- (void)testScanTemplate{
    NSString *fileName = @"UtDemo.pdf";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    pdfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,fileName];
    
    
    NSError *aError = nil;
    [_reportEngine validateReport:&aError isReport:YES];
    BOOL isTrue = aError == nil ? YES:NO;
    XCTAssertTrue(isTrue,@"report validate error");
    
    NSArray *elements = [_reportEngine scanTemplate];
    XCTAssertTrue([elements count]>0, @"scan template generate error.");
}





@end
