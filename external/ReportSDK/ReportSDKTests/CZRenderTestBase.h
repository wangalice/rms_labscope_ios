//
//  CZRenderTestBase.h
//  ReportSDK
//
//  Created by Johnny on 3/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>

#import "CZReportFactory.h"

@interface CZRenderTestBase : XCTestCase{
    CZReportFactory * _factory;
    NSMutableDictionary *_content ;
}

@end
