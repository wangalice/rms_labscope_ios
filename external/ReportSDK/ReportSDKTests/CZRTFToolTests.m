//
//  CZRTFToolTests.m
//  ReportSDK
//
//  Created by Johnny on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRTFToolTests.h"
#import "CZRTFTool.h"
@implementation CZRTFToolTests


- (void)setUp{
    [super setUp];
}

- (void)testCZRTFToolClass{
    CGRect aOffsetRct = CGRectMake(20, 15, 0, 0);
    NSString *rtfStr = [CZRTFTool offsetFrame2RTFString:aOffsetRct];
    XCTAssertTrue([rtfStr length]>0,@"convert frame to rtf string failed.");
    
    
    NSString *xRtfStr = [CZRTFTool xOffset2RTFString:50 useFont:[UIFont systemFontOfSize:18]];
    XCTAssertTrue([xRtfStr length]>0,@"convert x value to rtf string failed.");

    
    NSString *xTableRtfStr = [CZRTFTool xOffset2TableRTFString:60];
    XCTAssertTrue([xTableRtfStr length]>0,@"convert x table value to rtf string failed.");
    
    NSString *xTextRtfStr = [CZRTFTool xOffset2TextRTFString:80];
    XCTAssertTrue([xTextRtfStr length]>0,@"convert x text value to rtf string failed.");
    
    NSString *imagePath = [[NSBundle bundleForClass:[CZRTFToolTests class]] pathForResource:@"RtfTest" ofType:@"png"];
    NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
    NSString *imageStr = [CZRTFTool convertImageData2String:imageData];
    XCTAssertTrue([imageStr length]>0,@"convert image to rtf string failed.");

    
}

- (void)tearDown{
    // Tear-down code here.
    [super tearDown];
}

@end
