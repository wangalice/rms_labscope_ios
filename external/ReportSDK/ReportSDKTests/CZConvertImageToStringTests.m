//
//  CZConvertImageToStringTests.m
//  ReportSDK
//
//  Created by Johnny on 5/28/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZConvertImageToStringTests.h"
#import "CZConvertImageToString.h"

@implementation CZConvertImageToStringTests



- (void)setUp{
    [super setUp];
}

- (void)testExample{
    NSString *dataPath = [[NSBundle bundleForClass:[CZConvertImageToStringTests class]]
                          pathForResource:@"Untitled" ofType:@"png"];
    NSData *imageData = [NSData dataWithContentsOfFile:dataPath];
    NSString *imageStr = [[CZConvertImageToString sharedInstance] convertImageData2String:imageData];
    [[CZConvertImageToString sharedInstance] deleteTempFile];
     XCTAssertTrue([imageStr length]>0,@"convert image data to string failed");
    
}

- (void)tearDown{
    // Tear-down code here.
    [super tearDown];
}

@end
