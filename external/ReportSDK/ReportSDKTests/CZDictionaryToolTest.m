//
//  CZDictionaryToolTest.m
//  ReportSDK
//
//  Created by Johnny on 3/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZDictionaryToolTest.h"

@implementation CZDictionaryToolTest


- (void)setUp{
    [super setUp];
}

- (void)testExample{
    NSString *value1 = @"value1";
    NSString *value2 = @"value2";
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:value1,@"key1",value2,@"key2", nil];
    NSMutableDictionary *copyDic = [dic mutableDeepCopy];
    XCTAssertTrue([[copyDic allValues] count] == 2,@"deep copy is ok");
}


- (void)tearDown{
    // Tear-down code here.
    [super tearDown];
}
@end
