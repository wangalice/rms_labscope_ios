//
//  CZErrorTests.m
//  ReportSDK
//
//  Created by Johnny on 7/23/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZErrorTests.h"
#import "CZError.h"
#import "CZReportGeneratorErrorHandler.h"

@implementation CZErrorTests

- (void)setUp{
    [super setUp];
}

- (void)testCZErrorClass{
    NSString *errorMsg = [CZReportGeneratorErrorHandler getErrorMessage:CZReportGeneratorErrorTextValueNotExist];
    CZError *aError = [[CZError alloc] initWithDomain:CZReportGeneratorBodyDomain
                                                order:0
                                     errorElementType:kTextElement];
    [aError.errorDetailMsgArray addObject:errorMsg];
    aError.errorCode = CZReportGeneratorErrorTextValueNotExist;
    
    XCTAssertTrue([[[aError dicInfo] allKeys] count]>0,@"Get error information dic failed.");
    NSString *errorStringDes = [[aError dicInfo] description];
    XCTAssertTrue([errorStringDes length]>0,@"convert error dictionary to string failed.");

}

- (void)tearDown{
    // Tear-down code here.
    [super tearDown];
}



@end
