//
//  RTFFontColorRegistTableTests.m
//  ReportSDK
//
//  Created by Johnny on 4/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "RTFFontColorRegistTableTests.h"
#import "CZRTFFontColorRegistTable.h"

@implementation RTFFontColorRegistTableTests


- (void)setUp{
    [super setUp];
}

- (void)testExample{
    
    NSString *colorRtfDes1 = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:[UIColor redColor]];
    NSString *colorRtfDes2 = [[CZRTFFontColorRegistTable sharedInstance] readTextColorFromColorTable:[UIColor brownColor]];
    NSString *rtfColorTableDes = [[CZRTFFontColorRegistTable sharedInstance] fontColorRTFDes];
    XCTAssertTrue([colorRtfDes1 length]>0,@"color regist failed.");
    XCTAssertTrue([colorRtfDes2 length]>0,@"color regist failed.");
    XCTAssertTrue([rtfColorTableDes length]>0,@"color regist failed.");
}

- (void)tearDown{
    // Tear-down code here.
    [super tearDown];
}


@end
