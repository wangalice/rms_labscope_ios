//
//  CZReportFactoryTests.m
//  ReportSDK
//
//  Created by Johnny on 3/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZReportFactoryTests.h"
#import "CZReportFactory.h"
#import "CZReportData.h"
#import "CZSystemTemplate.h"
#import "CZError.h"
#import "CZRTFPreviewConfig.h"


#define kReportDir @"/Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/F8948F24-77C6-4A8F-83A3-351277B7CC76/Documents/"

@implementation CZReportFactoryTests


- (void)setUp{
    [super setUp];
    NSString *templatePath = [[NSBundle bundleForClass:[CZReportFactoryTests class]] pathForResource:@"reportTemplateSampleOriginal_nodata" ofType:@"plist"];
    
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportFactoryTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    CZSystemTemplate * template = [[CZSystemTemplate alloc]initWithDictionary:
                                   [NSDictionary dictionaryWithContentsOfFile:templatePath]];
    _factory = [[CZReportFactory alloc] initWithData:data systemTemplate:template];
    [data release];
    [template release];
}


- (void)testExample{
   
}

- (void)testFormatDataAndTemplate{
    }

- (void)testCZReportDataClass{
   
    
}

- (void)testgeneratePDFReport{
    
    NSError *newError = nil;
    [_factory dataAndTemplateValidate:&newError];
    _factory.reportDestination = [NSString stringWithFormat:@"%@%@",kReportDir,@"UTDemo.pdf"];

    _factory.delegate = nil;

    [_factory generatePDFReport];
    
}

- (void)testgenerateRTFReport{
    
    NSString *templatePath = [[NSBundle bundleForClass:[CZReportFactoryTests class]] pathForResource:@"reportTemplateSampleOriginal_nodata" ofType:@"plist"];
    
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportFactoryTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    CZSystemTemplate * template = [[CZSystemTemplate alloc]initWithDictionary:
                                   [NSDictionary dictionaryWithContentsOfFile:templatePath]];
    FreeObj(_RTFFactory);
    _RTFFactory = [[CZReportFactory alloc] initWithData:data systemTemplate:template];
    [data release];
    [template release];
    
    NSError *aError = nil;

    [_RTFFactory dataAndTemplateValidate:&aError];

    _RTFFactory.delegate = nil;

#ifdef CanUseLog
    NSDictionary  *content = [[NSDictionary alloc] initWithDictionary:_factory.systemTemplate.intergrateDic];
    CZLog(@"the merge dic content is %@",content);
#endif
    //assert([[content allKeys]count]>0);
    [[CZRTFPreviewConfig sharedInstance] setFileDestination:_RTFFactory.reportDestination];

    [[CZRTFPreviewConfig sharedInstance]configForRTF];

    [_RTFFactory generateRTFReport];
    
}

- (void)testgenerateRTFDReport{
    
    NSString *templatePath = [[NSBundle bundleForClass:[CZReportFactoryTests class]] pathForResource:@"reportTemplateSampleOriginal_nodata" ofType:@"plist"];
    
    NSString *dataPath = [[NSBundle bundleForClass:[CZReportFactoryTests class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    CZSystemTemplate * template = [[CZSystemTemplate alloc]initWithDictionary:
                                   [NSDictionary dictionaryWithContentsOfFile:templatePath]];
    FreeObj(_RTFFactory);
    _RTFFactory = [[CZReportFactory alloc] initWithData:data systemTemplate:template];
    [data release];
    [template release];
    
    NSError *aError = nil;
    
    [_RTFFactory dataAndTemplateValidate:&aError];
    _RTFFactory.reportDestination = [NSString stringWithFormat:@"%@%@",kReportDir,@"UTDemo.rtfd"];
    
    _RTFFactory.delegate = nil;

#ifdef CanUseLog
    NSDictionary  *content = [[NSDictionary alloc] initWithDictionary:_factory.systemTemplate.intergrateDic];
    CZLog(@"the merge dic content is %@",content);
#endif

    [[CZRTFPreviewConfig sharedInstance] setFileDestination:_RTFFactory.reportDestination];

    [[CZRTFPreviewConfig sharedInstance]configForRTFD];
    [_RTFFactory generateRTFDReport];
     
    
}

- (void)tearDown{
    // Tear-down code here.
    if (_factory) {
        [_factory release];
    }
    if (_RTFFactory) {
        [_RTFFactory release];
    }
    [super tearDown];
}

@end
