//
//  CZRenderTestBase.m
//  ReportSDK
//
//  Created by Johnny on 3/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZRenderTestBase.h"
#import "CZReportFactory.h"
#import "CZReportData.h"
#import "CZSystemTemplate.h"
#import "CZContentRenderManager.h"
#import "CZPDFContentRenderManger.h"
#import "CZRTFContentRenderManager.h"

#import "CZLineRender.h"
#import "CZTableRender.h"
#import "CZImageRender.h"
#import "CZColumnRender.h"
#import "CZPieRender.h"
#import "CZDictionaryTool.h"
#import "CZError.h"
#import "CZRTFPreviewConfig.h"

@implementation CZRenderTestBase

- (void)setUp{
    [super setUp];
    NSString *templatePath = [[NSBundle bundleForClass:[CZRenderTestBase class]] pathForResource:@"reportTemplateSampleOriginal_nodata" ofType:@"plist"];
    
    NSString *dataPath = [[NSBundle bundleForClass:[CZRenderTestBase class]]
                          pathForResource:@"templateData" ofType:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:dataPath];
    CZSystemTemplate * template = [[CZSystemTemplate alloc]initWithDictionary:
                                   [NSDictionary dictionaryWithContentsOfFile:templatePath]];
    _factory = [[CZReportFactory alloc] initWithData:data systemTemplate:template];
    [_factory.systemTemplate generateFormats];
    NSError *aError = nil;
    [_factory dataAndTemplateValidate:&aError];
    XCTAssertTrue(aError == nil,@"data validate failed");
    _content = [[NSMutableDictionary alloc] initWithDictionary:_factory.systemTemplate.intergrateDic];
    CZLog(@"the merge dic content is %@",_content);
    [data release];
    [template release];
}

#define kReportDir @"/Users/johnny/Library/Application Support/iPhone Simulator/6.0/Applications/BD4202EB-A3F5-4A98-9EE6-EF16EA64F948/Documents/"

- (void)testCZPDFContentRenderMangerClass{
    CZPDFContentRenderManger *pdfManger = [[CZPDFContentRenderManger alloc]initWithRenderContent:_content];
    NSString  *pdfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,@"UTDemo.pdf"];
    pdfManger.reportDestination = pdfFileName;
    [pdfManger startGenerate];
}

- (void)testCZRTFContentRenderManagerClass{
    CZRTFContentRenderManager *rtfManger = [[CZRTFContentRenderManager alloc]initWithRenderContent:_content];
    NSString  *pdfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,@"UTDemo.rtf"];

    
    rtfManger.reportDestination = pdfFileName;
    [[CZRTFPreviewConfig sharedInstance] setFileDestination:pdfFileName];
    
    [[CZRTFPreviewConfig sharedInstance]configForRTF];
    [rtfManger startGenerate];
}

- (void)testCZRTFDContentRenderManagerClass{
    CZRTFContentRenderManager *rtfManger = [[CZRTFContentRenderManager alloc]initWithRenderContent:_content];
    NSString  *pdfFileName = [NSString stringWithFormat:@"%@%@",kReportDir,@"UTDemo.rtfd"];
    
    
    rtfManger.reportDestination = pdfFileName;
    [[CZRTFPreviewConfig sharedInstance] setFileDestination:pdfFileName];
    
    [[CZRTFPreviewConfig sharedInstance]configForRTFD];
    [rtfManger startGenerate];
}



- (void)testRTFRender{
    NSDictionary *reportBodyDic = [_content valueForKey:kReportBody];
    XCTAssertTrue(reportBodyDic != nil,@"Get report body content failed");
    
    NSArray *bodyElements = [reportBodyDic valueForKey:kBodyElements];
    XCTAssertTrue([bodyElements count]>0,@"Get report body array failed");

    
    for (NSDictionary *dicObj in bodyElements) {
        NSString *elementType = [dicObj valueForKey:kElementType];
        assert(elementType != nil && [elementType length]>0);
        CGRect frame = [CZDictionaryTool dictionay2Frame:[dicObj valueForKey:kFrame]];
        
        if ([elementType isEqualToString:kLineElement]) {
            CZLineRender *lineRender = [[CZLineRender alloc] initWithContent:dicObj frame:frame];
            NSOutputStream *stream = [[NSOutputStream alloc] initToMemory];
            [stream open];
            [lineRender rtfRenderIntoStream:stream];
            [stream close];
            
            id memory = [stream propertyForKey:NSStreamDataWrittenToMemoryStreamKey];
            XCTAssertTrue(memory != nil, @"lineRender rtf render failed");
            FreeObj(lineRender);
            FreeObj(stream);
        }else if ([elementType isEqualToString:kTableElement]) {
//             CZTableRender *tableRender = [[CZTableRender alloc]initWithContent:dicObj frame:frame];
//            
//            [tableRender rtfRender];
//            STAssertTrue([tableRender.RTFAttributeStr length]>0,@"tableRender rtf render failed");
//            FreeObj(tableRender);
        }else if ([elementType isEqualToString:kImageElement]) {
//             CZImageRender *imageRender = [[CZImageRender alloc]initWithContent:dicObj frame:frame];
//            
//            [imageRender rtfRender];
//            STAssertTrue([imageRender.RTFAttributeStr length]>0,@"imageRender rtf render failed");
//            FreeObj(imageRender);
        }else if ([elementType isEqualToString:kPieElement]) {
            CZPieRender *pieRender = [[CZPieRender alloc]initWithContent:dicObj frame:frame];
            
            NSOutputStream *stream = [[NSOutputStream alloc] initToMemory];
            [stream open];
            [pieRender rtfRenderIntoStream:stream];
            [stream close];
            
            id memory = [stream propertyForKey:NSStreamDataWrittenToMemoryStreamKey];
            XCTAssertTrue(memory != nil, @"pieRender rtf render failed");
            FreeObj(pieRender);
        }
        else if ([elementType isEqualToString:kPieElement]) {
            CZColumnRender *columnRender = [[CZColumnRender alloc]initWithContent:dicObj frame:frame];
            
            NSOutputStream *stream = [[NSOutputStream alloc] initToMemory];
            [stream open];
            [columnRender rtfRenderIntoStream:stream];
            [stream close];
            
            id memory = [stream propertyForKey:NSStreamDataWrittenToMemoryStreamKey];
            XCTAssertTrue(memory != nil, @"columnRender rtf render failed");
            FreeObj(columnRender);
        }

    }
}



- (void)tearDown{
    // Tear-down code here.
    [_factory release];
    [_content release];
    [super tearDown];
}

@end
