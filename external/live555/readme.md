readme.md

# How to build live555 for iOS

* Copy original source code of live555 into src directory. After that, make sure ./src/BasicUsageEnvironment, ./src/groupsock, ./src/livMedia, ./src/UsageEnvironmnet exist.

* To fix a crash bug, modify code in MultiframedRTPSource.cpp; change destructor code as follow:

```
#include <stack>

...

BufferedPacket::~BufferedPacket() {
    // >>>>>>> modified code by Hermes
    std::stack<BufferedPacket *> packetReleaseStack;
    BufferedPacket *parent = this;
    BufferedPacket *child = fNextPacket;
    while (child) {
        packetReleaseStack.push(child);
        parent->fNextPacket = NULL;
        parent = child;
        child = parent->fNextPacket;
    }
    
    //    if (packetReleaseStack.size() > 2000) {
    //        printf("BufferedPacket depth is %d \n", packetReleaseStack.size());
    //    }

    while (!packetReleaseStack.empty()) {
        BufferedPacket *top = packetReleaseStack.top();
        packetReleaseStack.pop();
        
        delete top;
    }
    // <<<<<<< modified code by Hermes
    
    //  delete fNextPacket;
    delete[] fBuf;
}
```

* To fix another bug about getting local IP address, modify code in GroupsockHelper.cpp, add code below under “static netAddressBits ourAddress = 0;”:
   
```
#include <arpa/inet.h>
#include <ifaddrs.h>

...

    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    
    // Retrieve the current interfaces - returns 0 on success
    int success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if (temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if (strcmp(temp_addr->ifa_name, "en0") == 0) {
                    char *hostname = inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr);
                    
                    if (hostname != NULL) {
                        // Try to resolve "hostname" to an IP address:
                        NetAddressList addresses(hostname);
                        NetAddressList::Iterator iter(addresses);
                        NetAddress const* address;
                        
                        // Take the first address that's not bad:
                        netAddressBits addr = 0;
                        while ((address = iter.nextAddress()) != NULL) {
                            netAddressBits a = *(netAddressBits*)(address->data());
                            if (!badAddressForUs(a)) {
                                addr = a;
                                break;
                            }
                        }
                        
                        ourAddress = addr;
                        break;
                    }
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
```

* To avoid LIVE555 crashes itself in some edge cases, comment out the “abort();” statements in UsageEnvironment.cpp

* Open live555.xcodeproject, archive once for armv7 and armv7s (iOS devices), then build for i386 (iOS Simulator); combine these two static libraries into one using “lipo”

## Advanced settings

The project is based on LIVE555 version 20130831, consider adjust following settings:

* Set preprocess macros `BSD=1 HAVE_SOCKADDR_LEN=1 SOCKLEN_T=socklen_t HAVE_SOCKADDR_LEN=1 _LARGEFILE_SOURCE=1 _FILE_OFFSET_BITS=64`
* Add or remove source files, if new files are added or deprecated files are removed.
