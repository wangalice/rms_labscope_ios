mkdir _build

make clean

./genMakefiles iphoneos-armv7
make
cp ./BasicUsageEnvironment/libBasicUsageEnvironment.a _build/b1.a
cp ./groupsock/libgroupsock.a _build/g1.a
cp ./liveMedia/libliveMedia.a _build/l1.a
cp ./UsageEnvironment/libUsageEnvironment.a _build/u1.a

make clean

./genMakefiles iphoneos-armv7s
make
cp ./BasicUsageEnvironment/libBasicUsageEnvironment.a _build/b2.a
cp ./groupsock/libgroupsock.a _build/g2.a
cp ./liveMedia/libliveMedia.a _build/l2.a
cp ./UsageEnvironment/libUsageEnvironment.a _build/u2.a

make clean

./genMakefiles iphone-simulator
make
cp ./BasicUsageEnvironment/libBasicUsageEnvironment.a _build/b3.a
cp ./groupsock/libgroupsock.a _build/g3.a
cp ./liveMedia/libliveMedia.a _build/l3.a
cp ./UsageEnvironment/libUsageEnvironment.a _build/u3.a

make clean

cd _build

lipo -create b1.a b2.a -output b4.a
lipo -create g1.a g2.a -output g4.a
lipo -create l1.a l2.a -output l4.a
lipo -create u1.a u2.a -output u4.a

lipo -create b3.a b4.a -output libBasicUsageEnvironment.a
lipo -create g3.a g4.a -output libgroupsock.a
lipo -create l3.a l4.a -output libliveMedia.a
lipo -create u3.a u4.a -output libUsageEnvironment.a

rm b1.a
rm b2.a
rm b3.a
rm b4.a
rm g1.a
rm g2.a
rm g3.a
rm g4.a
rm l1.a
rm l2.a
rm l3.a
rm l4.a
rm u1.a
rm u2.a
rm u3.a
rm u4.a

cd ..
