#include "libplatform/impl.h"

namespace mp4v2 { namespace platform { namespace io {
    
    ///////////////////////////////////////////////////////////////////////////////
    
    class StandardFileProvider : public FileProvider {
    public:
        StandardFileProvider();
        
        bool open(std::string name, Mode mode);
        bool seek(Size pos );
        bool read(void* buffer, Size size, Size& nin, Size maxChunkSize);
        bool write(const void* buffer, Size size, Size& nout, Size maxChunkSize);
        bool close();
        
    private:
        bool _seekg;
        bool _seekp;
        
        std::fstream _fstream;
        
        std::string _name;
        void *_buffer;
        Size _offsetg;
        Size _offsetp;
        Size _bufferLength;
    };
    
    ///////////////////////////////////////////////////////////////////////////////
    
    StandardFileProvider::StandardFileProvider()
    : _seekg(false), _seekp(false) {
        _bufferLength = MemoryFileMap::instance()->getMaxBufferLength();
    }
    
    bool StandardFileProvider::open(std::string name, Mode mode) {
        ios::openmode om = ios::binary;
        switch (mode) {
            case MODE_UNDEFINED:
            case MODE_READ:
            default:
                om |= ios::in;
                _seekg = true;
                _seekp = false;
                break;
                
            case MODE_MODIFY:
                om |= ios::in | ios::out;
                _seekg = true;
                _seekp = true;
                break;
                
            case MODE_CREATE:
                om |= ios::in | ios::out | ios::trunc;
                _seekg = true;
                _seekp = true;
                break;
        }
        
         _name = name;
        _offsetg = 0;
        _offsetp = 0;
        
        bool ret = MemoryFileMap::instance()->lockBufferForFile(_name.c_str(), &_buffer);
        if (!ret) {
            _buffer = NULL;
            _fstream.open(name.c_str(), om);
            return _fstream.fail();
        }
        
        return false;
    }
    
    bool StandardFileProvider::seek(Size pos) {
        if (_buffer == NULL) {
            if( _seekg )
                _fstream.seekg(pos, ios::beg);
            if( _seekp )
                _fstream.seekp(pos, ios::beg);
            return _fstream.fail();
        } else {
            if (pos < 0 || pos >= _bufferLength) {
                return true;
            }
            
            if (_seekg)
                _offsetg = pos;
            if (_seekp)
                _offsetp = pos;
            return false;
        }
    }
    
    bool StandardFileProvider::read(void *buffer, Size size, Size &nin, Size maxChunkSize) {
        if (_buffer == NULL) {
            _fstream.read((char*)buffer, size);
            if (_fstream.fail()) {
                return true;
            }
            nin = _fstream.gcount();
            return false;
        } else {
            if (_offsetg + size > _bufferLength) {
                return true;
            }
            
            memcpy((char *)buffer, (char *)_buffer + _offsetg, size);
            nin = size;
            _offsetg += size;
            return false;
        }
    }
    
    bool StandardFileProvider::write(const void *buffer, Size size, Size &nout, Size maxChunkSize) {
        if (_buffer == NULL) {
            _fstream.write((const char*)buffer, size);
            if (_fstream.fail()) {
                return true;
            }
            nout = size;
            return false;
        } else {
            if (_offsetp + size > _bufferLength) {
                return true;
            }
            
            memcpy((char *)_buffer + _offsetp, (char *)buffer, size);
            nout = size;
            _offsetp += size;
            return false;
        }
    }
    
    bool StandardFileProvider::close() {
        if (_buffer == NULL) {
            _fstream.close();
            return _fstream.fail();
        } else {
            // Unlock the memory file map after the memory file is closed.
            MemoryFileMap::instance()->unlockBufferForFile(_name.c_str());
            _buffer = NULL;
            return false;
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    
    FileProvider& FileProvider::standard() {
        return *new StandardFileProvider();
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    
}}} // namespace mp4v2::platform::io
