#include "libplatform/impl.h"

namespace mp4v2 { namespace platform { namespace io {

MemoryFileMap *MemoryFileMap::_instance = NULL;

pthread_mutex_t MemoryFileMap::_mutex = PTHREAD_MUTEX_INITIALIZER;

MemoryFileMap *MemoryFileMap::instance() {
    if (!_instance) {
        // Only allow one instance of class to be generated.
        _instance = new MemoryFileMap();
    }
    return _instance;
}

int64_t MemoryFileMap::getMaxBufferLength() {
    return _maxLength;
}

void MemoryFileMap::setMaxBufferLength(int64_t maxLength) {
    _maxLength = maxLength;
}

bool MemoryFileMap::lockBufferForFile(const char *filePath, void **pBuffer) {
    pthread_mutex_lock(&_mutex);
    
    std::string key(filePath);
    
    std::map<std::string, void *>::iterator itBuffer;
    itBuffer = _bufferMap.find(key);
    
    std::map<std::string, pthread_mutex_t>::iterator itLock;
    itLock = _lockMap.find(key);
    
    bool result = true;
    if (itBuffer == _bufferMap.end()) {
        result = false;
    } else {
        if (itLock != _lockMap.end()) {
            pthread_mutex_lock(&itLock->second);
        }
        
        *pBuffer = itBuffer->second;
    }
    
    pthread_mutex_unlock(&_mutex);
    
    return result;
}

void MemoryFileMap::unlockBufferForFile(const char *filePath) {
    pthread_mutex_lock(&_mutex);
    
    std::string key(filePath);
    std::map<std::string, pthread_mutex_t>::iterator itLock;
    itLock = _lockMap.find(key);
    
    if (itLock != _lockMap.end()) {
        pthread_mutex_unlock(&itLock->second);
    }
    
    pthread_mutex_unlock(&_mutex);
}

void MemoryFileMap::setBufferForFile(const char *filePath, void *buffer) {
    pthread_mutex_lock(&_mutex);
    
    std::string key(filePath);
    
    std::map<std::string, pthread_mutex_t>::iterator itLock;
    itLock = _lockMap.find(key);
    
    if (itLock != _lockMap.end()) {
        pthread_mutex_lock(&itLock->second);
    }
    
    if (buffer) {
        _bufferMap[key] = buffer;
    } else {
        _bufferMap.erase(key);
    }
    
    if (itLock != _lockMap.end()) {
        pthread_mutex_unlock(&itLock->second);
    }
    
    if (buffer == NULL) {
        _lockMap.erase(key);
    } else if (itLock == _lockMap.end()) {
        pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
        _lockMap[key] = mutex;
    }
    
    pthread_mutex_unlock(&_mutex);
}

}}} // namespace mp4v2::platform::io
