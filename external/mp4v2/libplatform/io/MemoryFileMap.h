#ifndef MP4V2_PLATFORM_IO_MEMORYFILEMAP_H
#define MP4V2_PLATFORM_IO_MEMORYFILEMAP_H

#include <map>
#include <string>

namespace mp4v2 { namespace platform { namespace io {

class MP4V2_EXPORT MemoryFileMap {
public:
    static MemoryFileMap *instance();
    
    int64_t getMaxBufferLength();
    void setMaxBufferLength(int64_t maxLength);
    
    void lockFileMap();
    void unlockFileMap();
    
    bool lockBufferForFile(const char *filePath, void **pBuffer);
    void unlockBufferForFile(const char *filePath);
    void setBufferForFile(const char *filePath, void *buffer);
    
private:
    MemoryFileMap() {
        _maxLength = 0;
    }
    
    MemoryFileMap(MemoryFileMap const&) {};
    MemoryFileMap& operator=(MemoryFileMap const&) {};
    
    static MemoryFileMap *_instance;
    static pthread_mutex_t _mutex;
    
    std::map<std::string, void *> _bufferMap;
    std::map<std::string, pthread_mutex_t> _lockMap;
    int64_t _maxLength;
};

}}} // namespace mp4v2::platform::io

#endif /* defined(__mp4v2_ios__MemoryFileMap__) */
