//////////////////////////////////////////////////////////////////////////////////////////////////
//
//  JFRWebSocket.m
//
//  Created by Austin and Dalton Cherry on on 5/13/14.
//  Copyright (c) 2014-2017 Austin Cherry.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#import "JFRWebSocket.h"

// Re-enable this when you need to debug the websocket flow
//#define ENABLE_LOG

static inline void JFRFastLog(NSString *format, ...)  {
#ifdef ENABLE_LOG
    __block va_list arg_list;
    va_start (arg_list, format);
    
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:arg_list];
    
    va_end(arg_list);
    
    NSLog(@"[JFRWebSocket] %@", formattedString);
#else
    //Do nothing
#endif
}

//get the opCode from the packet
typedef NS_ENUM(NSUInteger, JFROpCode) {
    JFROpCodeContinueFrame = 0x0,
    JFROpCodeTextFrame = 0x1,
    JFROpCodeBinaryFrame = 0x2,
    //3-7 are reserved.
    JFROpCodeConnectionClose = 0x8,
    JFROpCodePing = 0x9,
    JFROpCodePong = 0xA,
    //B-F reserved.
};

typedef NS_ENUM(NSUInteger, JFRCloseCode) {
    JFRCloseCodeNormal                 = 1000,
    JFRCloseCodeGoingAway              = 1001,
    JFRCloseCodeProtocolError          = 1002,
    JFRCloseCodeProtocolUnhandledType  = 1003,
    // 1004 reserved.
    JFRCloseCodeNoStatusReceived       = 1005,
    //1006 reserved.
    JFRCloseCodeEncoding               = 1007,
    JFRCloseCodePolicyViolated         = 1008,
    JFRCloseCodeMessageTooBig          = 1009
};

typedef NS_ENUM(NSUInteger, JFRInternalErrorCode) {
    // 0-999 WebSocket status codes not used
    JFROutputStreamWriteError  = 1
};

#define kJFRInternalHTTPStatusWebSocket 101
static const NSTimeInterval connectingRequestTimeoutInterval = 30;

//holds the responses in our read stack to properly process messages
@interface JFRResponse : NSObject

@property(nonatomic, assign)BOOL isFin;
@property(nonatomic, assign)JFROpCode code;
@property(nonatomic, assign)NSInteger bytesLeft;
@property(nonatomic, assign)NSInteger frameCount;
@property(nonatomic, strong)NSMutableData *buffer;

@end

@interface JFRRunLoopThread : NSThread

@property (nonatomic, strong, readonly) NSRunLoop *runLoop;

+ (instancetype)sharedThread;

@end

@interface JFRWebSocket ()<NSStreamDelegate>{
    // Use this to retain ourSelf
    __strong JFRWebSocket *_selfRetain;
}

@property(nonatomic, strong, nonnull) NSURL *url;
@property(nonatomic, strong, null_unspecified) NSInputStream *inputStream;
@property(nonatomic, strong, null_unspecified) NSOutputStream *outputStream;
@property(nonatomic, strong, nonnull) NSMutableArray *readStack;
@property(nonatomic, strong, nonnull) NSMutableArray *inputQueue;
@property(nonatomic, strong, nullable) NSData *fragBuffer;
@property(nonatomic, strong, nullable) NSMutableDictionary *headers;
@property(nonatomic, strong, nullable) NSArray *optProtocols;
@property(nonatomic, assign)BOOL didDisconnect;
@property(nonatomic, assign)BOOL certValidated;
// Distingush the WebSocket connecting/connecting accomplished status;
@property (nonatomic, assign) BOOL isConnecting;
@property (nonatomic, assign) BOOL isConnected;
// Delegate callback queue;
@property(nonatomic, strong, nullable)dispatch_queue_t delegateDispatchQueue;
@property(nonatomic, strong, nullable)dispatch_queue_t workQueue;
@property(nonatomic, assign) BOOL closeWhenFinishWriting;

@end

//Constant Header Values.
NS_ASSUME_NONNULL_BEGIN
static NSString *const headerWSUpgradeName     = @"Upgrade";
static NSString *const headerWSUpgradeValue    = @"websocket";
static NSString *const headerWSHostName        = @"Host";
static NSString *const headerWSConnectionName  = @"Connection";
static NSString *const headerWSConnectionValue = @"Upgrade";
static NSString *const headerWSProtocolName    = @"Sec-WebSocket-Protocol";
static NSString *const headerWSVersionName     = @"Sec-Websocket-Version";
static NSString *const headerWSVersionValue    = @"13";
static NSString *const headerWSKeyName         = @"Sec-WebSocket-Key";
static NSString *const headerOriginName        = @"Origin";
static NSString *const headerWSAcceptName      = @"Sec-WebSocket-Accept";
NS_ASSUME_NONNULL_END

//Class Constants
static char CRLFBytes[] = {'\r', '\n', '\r', '\n'};
static int BUFFER_MAX = 4096;

// This get the correct bits out by masking the bytes of the buffer.
static const uint8_t JFRFinMask             = 0x80;
static const uint8_t JFROpCodeMask          = 0x0F;
static const uint8_t JFRRSVMask             = 0x70;
static const uint8_t JFRMaskMask            = 0x80;
static const uint8_t JFRPayloadLenMask      = 0x7F;
static const size_t  JFRMaxFrameSize        = 32;

@implementation JFRWebSocket

//Default initializer
- (instancetype)initWithURL:(NSURL *)url protocols:(NSArray*)protocols {
    return [self initWithURL:url
                   protocols:protocols
               callbackQueue:dispatch_get_main_queue()];
}

- (instancetype)initWithURL:(NSURL *)url protocols:(NSArray *)protocols callbackQueue:(dispatch_queue_t)callbackQueue {
    if (self = [super init]) {
        _certValidated = NO;
        _voipEnabled = NO;
        _selfSignedSSL = NO;
        _closeWhenFinishWriting = NO;
        _delegateDispatchQueue = callbackQueue;
        
        _workQueue = dispatch_queue_create("com.Jetfire.WebSocket.Handle", DISPATCH_QUEUE_SERIAL);
        // Going to set a specific on the queue so we can validate we're on the work queue
        dispatch_queue_set_specific(_workQueue, (__bridge void *)self, (__bridge void *)_workQueue, NULL);

        _url = url;
        _optProtocols = protocols;
        _readStack = [NSMutableArray array];
        _inputQueue = [NSMutableArray array];
        _isConnecting = NO;
        _didDisconnect = NO;
    }
    return self;
}

// Exposed method for connecting to URL provided in init method.
- (void)connect {
   
    if (self.isConnecting) {
        JFRFastLog(@"Cannot call connect on JFRWebSocket more than once");
        return;
    }
    
    _selfRetain = self;
    
    [self openConnection];
   
    // Add connecting request timeout logic
    __weak typeof(self) weakSelf = self;
    dispatch_time_t  popupTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(connectingRequestTimeoutInterval * NSEC_PER_SEC));
    dispatch_after(popupTime, dispatch_get_main_queue(), ^{
        if (weakSelf.isConnecting) {
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorTimedOut userInfo:@{NSLocalizedDescriptionKey:@"The WebSocket connection request time out"}];
            [weakSelf failedWithError:error];
        }
    });
}

- (void)disconnect {
    JFRFastLog(@"disconnect");
    if (self.isConnecting) {
        __weak typeof(self) weakSelf = self;
        dispatch_async(self.workQueue, ^{
            [weakSelf closeConnection];
        });
        return;
    }
    [self writeError:JFRCloseCodeNormal];
}

- (void)writeString:(NSString*)string {
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        if ([string isKindOfClass:[NSString class]]) {
            [weakSelf dequeueWrite:[string dataUsingEncoding:NSUTF8StringEncoding]
                      withCode:JFROpCodeTextFrame];
        }
    });
}

- (void)writePing:(NSData*)data {
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        [weakSelf dequeueWrite:data withCode:JFROpCodePing];
    });
}

- (void)writePong:(NSData *)data {
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        [weakSelf dequeueWrite:data withCode:JFROpCodePong];
    });
}

- (void)writeData:(NSData*)data {
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        [weakSelf dequeueWrite:data withCode:JFROpCodeBinaryFrame];
    });
}

- (void)addHeader:(NSString*)value forKey:(NSString*)key {
    if(!self.headers) {
        self.headers = [[NSMutableDictionary alloc] init];
    }
    [self.headers setObject:value forKey:key];
}

#pragma mark - Close Methods
- (void)closeWithProtocolErrorMessage:(NSString *)message {
    JFRFastLog(@"closeWithProtocolErrorMessage: %@", message);
    __weak typeof(self) weakSelf = self;
    [self performDelegateBlock:^{
        //send close with error code
        [weakSelf writeError:JFRCloseCodeProtocolError];
        dispatch_async(weakSelf.workQueue, ^{
            [weakSelf closeConnection];
        });
    }];
}

- (void)failedWithError:(NSError *)error {
    JFRFastLog(@"failedWithError: %@", error);
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        if (!weakSelf.didDisconnect) {
            [weakSelf performDelegateBlock:^{
                if([weakSelf.delegate respondsToSelector:@selector(websocketDidDisconnect:error:)]) {
                    [weakSelf.delegate websocketDidDisconnect:weakSelf error:error];
                }
            }];
            
            // Set state to initial state
            weakSelf.didDisconnect = YES;
            weakSelf.isConnecting = NO;
            weakSelf.isConnected = NO;
            
            [weakSelf closeConnection];
        }
    });
}

- (void)closeConnection {
    JFRFastLog(@"closeConnection");
    
    self.closeWhenFinishWriting = YES;
    
    [self assertOnWorkQueue];
    
    // TODO: we should add a logic when the output stream is still writing data;
    @synchronized (self) {
        [self.outputStream close];
        [self.inputStream close];
        
        [self.inputStream removeFromRunLoop:[JFRRunLoopThread sharedThread].runLoop
                                    forMode:NSDefaultRunLoopMode];
        [self.outputStream removeFromRunLoop:[JFRRunLoopThread sharedThread].runLoop
                                     forMode:NSDefaultRunLoopMode];
    }
    _selfRetain = nil;
}

#pragma mark - connect's internal supporting methods

- (NSString *)origin {
    NSString *scheme = [_url.scheme lowercaseString];
    
    if ([scheme isEqualToString:@"wss"]) {
        scheme = @"https";
    } else if ([scheme isEqualToString:@"ws"]) {
        scheme = @"http";
    }
    
    if (_url.port) {
        return [NSString stringWithFormat:@"%@://%@:%@/", scheme, _url.host, _url.port];
    } else {
        return [NSString stringWithFormat:@"%@://%@/", scheme, _url.host];
    }
}

//Uses CoreFoundation to build a HTTP request to send over TCP stream.
- (void)didConnect {
    JFRFastLog(@"didConnect");
    CFURLRef url = CFURLCreateWithString(kCFAllocatorDefault, (CFStringRef)self.url.absoluteString, NULL);
    CFStringRef requestMethod = CFSTR("GET");
    CFHTTPMessageRef urlRequest = CFHTTPMessageCreateRequest(kCFAllocatorDefault,
                                                             requestMethod,
                                                             url,
                                                             kCFHTTPVersion1_1);
    CFRelease(url);

    NSNumber *port = _url.port;
    if (!port) {
        if([self.url.scheme isEqualToString:@"wss"] || [self.url.scheme isEqualToString:@"https"]){
            port = @(443);
        } else {
            port = @(80);
        }
    }
    NSString *protocols = nil;
    if([self.optProtocols count] > 0) {
        protocols = [self.optProtocols componentsJoinedByString:@","];
    }
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerWSHostName,
                                     (__bridge CFStringRef)[NSString stringWithFormat:@"%@:%@",self.url.host,port]);
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerWSVersionName,
                                     (__bridge CFStringRef)headerWSVersionValue);
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerWSKeyName,
                                     (__bridge CFStringRef)[self generateWebSocketKey]);
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerWSUpgradeName,
                                     (__bridge CFStringRef)headerWSUpgradeValue);
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerWSConnectionName,
                                     (__bridge CFStringRef)headerWSConnectionValue);
    if (protocols.length > 0) {
        CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                         (__bridge CFStringRef)headerWSProtocolName,
                                         (__bridge CFStringRef)protocols);
    }
   
    CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                     (__bridge CFStringRef)headerOriginName,
                                     (__bridge CFStringRef)[self origin]);
    
    for(NSString *key in [self.headers allKeys]) {
        CFHTTPMessageSetHeaderFieldValue(urlRequest,
                                         (__bridge CFStringRef)key,
                                         (__bridge CFStringRef)self.headers[key]);
    }
    
    NSData *serializedRequest = (__bridge_transfer NSData *)(CFHTTPMessageCopySerializedMessage(urlRequest));

    JFRFastLog(@"urlRequest = \"%@\"", urlRequest);
    JFRFastLog(@"%s:Request Header:%@",__func__,[[NSString alloc] initWithBytes:[serializedRequest bytes]
                                                                         length:[serializedRequest length]
                                                                       encoding:NSUTF8StringEncoding]);
    CFRelease(urlRequest);
    
    size_t dataLen = [serializedRequest length];
    NSInteger bytesWritten = [self.outputStream write:[serializedRequest bytes] maxLength:dataLen];
    
    NSError *error = [self errorWithDetail:@"OutputStream write failed" code:JFROutputStreamWriteError];
    if (bytesWritten == -1) {
        __weak typeof(self) weakSelf = self;
        dispatch_async(self.workQueue, ^{
            [weakSelf failedWithError:error];
        });
    }
}

//Random String of 16 lowercase chars, SHA1 and base64 encoded.
- (NSString*)generateWebSocketKey {
    NSInteger seed = 16;
    NSMutableString *string = [NSMutableString stringWithCapacity:seed];
    for (int i = 0; i < seed; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    return [[string dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
}

//Sets up our reader/writer for the TCP stream.
- (void)openConnection {

    JFRFastLog(@"openConnection");
    
    @autoreleasepool{
        self.isConnecting = YES;
        NSNumber *port = _url.port;
        if (!port) {
            if([self.url.scheme isEqualToString:@"wss"] || [self.url.scheme isEqualToString:@"https"]){
                port = @(443);
            } else {
                port = @(80);
            }
        }
        
        CFReadStreamRef readStream = NULL;
        CFWriteStreamRef writeStream = NULL;
        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)self.url.host, [port intValue], &readStream, &writeStream);
        
        self.inputStream = (__bridge_transfer NSInputStream *)readStream;
        self.inputStream.delegate = self;
        self.outputStream = (__bridge_transfer NSOutputStream *)writeStream;
        self.outputStream.delegate = self;
        if([self.url.scheme isEqualToString:@"wss"] || [self.url.scheme isEqualToString:@"https"]) {
            [self.inputStream setProperty:NSStreamSocketSecurityLevelNegotiatedSSL forKey:NSStreamSocketSecurityLevelKey];
            [self.outputStream setProperty:NSStreamSocketSecurityLevelNegotiatedSSL forKey:NSStreamSocketSecurityLevelKey];
        } else {
            self.certValidated = YES; //not a https session, so no need to check SSL pinning
        }
        if(self.voipEnabled) {
            [self.inputStream setProperty:NSStreamNetworkServiceTypeVoIP forKey:NSStreamNetworkServiceType];
            [self.outputStream setProperty:NSStreamNetworkServiceTypeVoIP forKey:NSStreamNetworkServiceType];
        }
        if(self.selfSignedSSL) {
            NSString *chain = (__bridge_transfer NSString *)kCFStreamSSLValidatesCertificateChain;
            NSString *peerName = (__bridge_transfer NSString *)kCFStreamSSLValidatesCertificateChain;
            NSString *key = (__bridge_transfer NSString *)kCFStreamPropertySSLSettings;
            NSDictionary *settings = @{chain: [[NSNumber alloc] initWithBool:NO],
                                       peerName: [NSNull null]};
            [self.inputStream setProperty:settings forKey:key];
            [self.outputStream setProperty:settings forKey:key];
        }
        
        [self.inputStream open];
        [self.outputStream open];

        [self.inputStream scheduleInRunLoop:[JFRRunLoopThread sharedThread].runLoop
                                    forMode:NSDefaultRunLoopMode];
        [self.outputStream scheduleInRunLoop:[JFRRunLoopThread sharedThread].runLoop
                                     forMode:NSDefaultRunLoopMode];
        
    }
}

// Check current queue is on the serial queue
- (void)assertOnWorkQueue {
    assert(dispatch_get_specific((__bridge void *)self) == (__bridge void *)_workQueue);
}

- (void)performDelegateBlock:(dispatch_block_t)block {
    assert(_delegateDispatchQueue);
    dispatch_async(_delegateDispatchQueue, block);
}

#pragma mark - NSStreamDelegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    if(self.security && !self.certValidated && (eventCode == NSStreamEventHasBytesAvailable || eventCode == NSStreamEventHasSpaceAvailable)) {
        SecTrustRef trust = (__bridge SecTrustRef)([aStream propertyForKey:(__bridge_transfer NSString *)kCFStreamPropertySSLPeerTrust]);
        NSString *domain = [aStream propertyForKey:(__bridge_transfer NSString *)kCFStreamSSLPeerName];
        if([self.security isValid:trust domain:domain]) {
            self.certValidated = YES;
        } else {
            __weak typeof(self) weakSelf = self;
            dispatch_async(self.workQueue, ^{
                [weakSelf failedWithError:[weakSelf errorWithDetail:@"Invalid SSL certificate"
                                                               code:JFROutputStreamWriteError]];
            });
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        switch (eventCode) {
            case NSStreamEventOpenCompleted: {
                JFRFastLog(@"NSStreamEventOpenCompleted, %@", aStream);
                if (weakSelf.inputStream == aStream && weakSelf.isConnecting) {
                    [weakSelf didConnect];
                }
            }
                break;
                
            case NSStreamEventHasBytesAvailable: {
                JFRFastLog(@"NSStreamEventHasBytesAvailable, %@", aStream);
                
                if(aStream == self.inputStream) {
                    [weakSelf processInputStream];
                }
            }
                break;
                
            case NSStreamEventHasSpaceAvailable: {
                JFRFastLog(@"NSStreamEventHasSpaceAvailable, %@", aStream);
            }
                break;
                
            case NSStreamEventErrorOccurred: {
                JFRFastLog(@"NSStreamEventErrorOccurred, %@", aStream);
                
                [weakSelf failedWithError:aStream.streamError];
            }
                break;
                
            case NSStreamEventEndEncountered: {
                JFRFastLog(@"NSStreamEventEndEncountered, %@", aStream);
                
                [weakSelf failedWithError:aStream.streamError];
            }
                break;
                
            default: {
                JFRFastLog(@"Default: %@", aStream);
            }
                break;
        }
    });
}

#pragma mark - Stream Processing Methods
/*
 From RFC 6455: Base frame control
 
     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-------+-+-------------+-------------------------------+
     |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
     |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
     |N|V|V|V|       |S|             |   (if payload len==126/127)   |
     | |1|2|3|       |K|             |                               |
     +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
     |     Extended payload length continued, if payload len == 127  |
     + - - - - - - - - - - - - - - - +-------------------------------+
     |                               |Masking-key, if MASK set to 1  |
     +-------------------------------+-------------------------------+
     | Masking-key (continued)       |          Payload Data         |
     +-------------------------------- - - - - - - - - - - - - - - - +
     :                     Payload Data continued ...                :
     + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
     |                     Payload Data continued ...                |
     +---------------------------------------------------------------+
 */
- (void)processInputStream {
    @autoreleasepool {
        uint8_t buffer[BUFFER_MAX];
        NSInteger length = [self.inputStream read:buffer maxLength:BUFFER_MAX];
        if(length > 0) {
            if(!self.isConnected) {
                CFIndex responseStatusCode;
                BOOL status = [self processHTTP:buffer length:length responseStatusCode:&responseStatusCode];
                //log the responseStatus code and buffer
                if (length < BUFFER_MAX) {
                    buffer[length] = 0x00;
                } else {
                    buffer[BUFFER_MAX - 1] = 0x00;
                }
                
                JFRFastLog(@"processInputStream response (%ld) = \"%s\"", responseStatusCode, buffer);
                
                if(status == NO) {
                    __weak typeof(self) weakSelf = self;
                    dispatch_async(self.workQueue, ^{
                        [weakSelf failedWithError:[weakSelf errorWithDetail:@"Invalid HTTP upgrade"
                                                               code:JFROutputStreamWriteError
                                                           userInfo:@{@"HTTPResponseStatusCode":@(responseStatusCode)}]];
                    });
                }
            } else {
                BOOL process = (_inputQueue.count == 0);
                [self.inputQueue addObject:[NSData dataWithBytes:buffer length:length]];
                if(process) {
                    [self dequeueInput];
                }
            }
        }
    }
}

- (void)dequeueInput {
    if(self.inputQueue.count > 0) {
        NSData *data = [self.inputQueue objectAtIndex:0];
        NSData *work = data;
        if(self.fragBuffer) {
            NSMutableData *combine = [NSMutableData dataWithData:self.fragBuffer];
            [combine appendData:data];
            work = combine;
            self.fragBuffer = nil;
        }
        [self processRawMessage:(uint8_t*)work.bytes length:work.length];
        [self.inputQueue removeObject:data];
        [self dequeueInput];
    }
}

//Finds the HTTP Packet in the TCP stream, by looking for the CRLF.
- (BOOL)processHTTP:(uint8_t*)buffer length:(NSInteger)bufferLen responseStatusCode:(CFIndex*)responseStatusCode {
    int k = 0;
    NSInteger totalSize = 0;
    for(int i = 0; i < bufferLen; i++) {
        if(buffer[i] == CRLFBytes[k]) {
            k++;
            if(k == 3) {
                totalSize = i + 1;
                break;
            }
        } else {
            k = 0;
        }
    }
    if(totalSize > 0) {
        BOOL status = [self validateResponse:buffer length:totalSize responseStatusCode:responseStatusCode];
        if (status == YES) {
            self.isConnected = YES;
            self.isConnecting = NO;
            __weak typeof(self) weakSelf = self;
            [self performDelegateBlock:^{
                if([weakSelf.delegate respondsToSelector:@selector(websocketDidConnect:)]) {
                    [weakSelf.delegate websocketDidConnect:weakSelf];
                }
            }];
            totalSize += 1; //skip the last \n
            NSInteger  restSize = bufferLen-totalSize;
            if(restSize > 0) {
                [self processRawMessage:(buffer+totalSize) length:restSize];
            }
        }
        return status;
    }
    return NO;
}

//Validate the HTTP is a 101, as per the RFC spec.
- (BOOL)validateResponse:(uint8_t *)buffer length:(NSInteger)bufferLen responseStatusCode:(CFIndex*)responseStatusCode {
    CFHTTPMessageRef response = CFHTTPMessageCreateEmpty(kCFAllocatorDefault, NO);
    CFHTTPMessageAppendBytes(response, buffer, bufferLen);
    *responseStatusCode = CFHTTPMessageGetResponseStatusCode(response);
    BOOL status = ((*responseStatusCode) == kJFRInternalHTTPStatusWebSocket)?(YES):(NO);
    if(status == NO) {
        CFRelease(response);
        return NO;
    }
    NSDictionary *headers = (__bridge_transfer NSDictionary *)(CFHTTPMessageCopyAllHeaderFields(response));
    NSString *acceptKey = headers[headerWSAcceptName];
    CFRelease(response);
    if(acceptKey.length > 0) {
        return YES;
    }
    return NO;
}

-(void)processRawMessage:(uint8_t*)buffer length:(NSInteger)bufferLen {
    JFRResponse *response = [self.readStack lastObject];
    if(response && bufferLen < 2) {
        self.fragBuffer = [NSData dataWithBytes:buffer length:bufferLen];
        return;
    }
    if(response.bytesLeft > 0) {
        NSInteger len = response.bytesLeft;
        NSInteger extra =  bufferLen - response.bytesLeft;
        if(response.bytesLeft > bufferLen) {
            len = bufferLen;
            extra = 0;
        }
        response.bytesLeft -= len;
        [response.buffer appendData:[NSData dataWithBytes:buffer length:len]];
        [self processResponse:response];
        NSInteger offset = bufferLen - extra;
        if(extra > 0) {
            [self processExtra:(buffer+offset) length:extra];
        }
        return;
    } else {
        if(bufferLen < 2) { // we need at least 2 bytes for the header
            self.fragBuffer = [NSData dataWithBytes:buffer length:bufferLen];
            return;
        }
        BOOL isFin = (JFRFinMask & buffer[0]);
        uint8_t receivedOpcode = (JFROpCodeMask & buffer[0]);
        BOOL isMasked = (JFRMaskMask & buffer[1]);
        uint8_t payloadLen = (JFRPayloadLenMask & buffer[1]);
        NSInteger offset = 2; //how many bytes do we need to skip for the header
        if((isMasked  || (JFRRSVMask & buffer[0])) && receivedOpcode != JFROpCodePong) {
            [self closeWithProtocolErrorMessage:@"masked and rsv data is not currently supported"];
            return;
        }
        BOOL isControlFrame = (receivedOpcode == JFROpCodeConnectionClose || receivedOpcode == JFROpCodePing);
        if(!isControlFrame && (receivedOpcode != JFROpCodeBinaryFrame && receivedOpcode != JFROpCodeContinueFrame && receivedOpcode != JFROpCodeTextFrame && receivedOpcode != JFROpCodePong)) {
            [self closeWithProtocolErrorMessage:[NSString stringWithFormat:@"unknown opcode: 0x%x",receivedOpcode]];
            return;
        }
        if(isControlFrame && !isFin) {
            [self closeWithProtocolErrorMessage:@"control frames can't be fragmented"];
            return;
        }
        if(receivedOpcode == JFROpCodeConnectionClose) {
            //the server disconnected us
            uint16_t code = JFRCloseCodeNormal;
            if(payloadLen == 1) {
                code = JFRCloseCodeProtocolError;
            }
            else if(payloadLen > 1) {
                code = CFSwapInt16BigToHost(*(uint16_t *)(buffer+offset) );
                if(code < 1000 || (code > 1003 && code < 1007) || (code > 1011 && code < 3000)) {
                    code = JFRCloseCodeProtocolError;
                }
                offset += 2;
            }
            
            if(payloadLen > 2) {
                NSInteger len = payloadLen-2;
                if(len > 0) {
                    NSData *data = [NSData dataWithBytes:(buffer+offset) length:len];
                    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if(!str) {
                        code = JFRCloseCodeProtocolError;
                    }
                }
            }
            [self writeError:code];
            __weak typeof(self) weakSelf = self;
            dispatch_async(self.workQueue, ^{
                [weakSelf closeConnection];
            });
            return;
        }
        if(isControlFrame && payloadLen > 125) {
            [self writeError:JFRCloseCodeProtocolError];
            return;
        }
        NSInteger dataLength = payloadLen;
        if(payloadLen == 127) {
            dataLength = (NSInteger)CFSwapInt64BigToHost(*(uint64_t *)(buffer+offset));
            offset += sizeof(uint64_t);
        } else if(payloadLen == 126) {
            dataLength = CFSwapInt16BigToHost(*(uint16_t *)(buffer+offset) );
            offset += sizeof(uint16_t);
        }
        if(bufferLen < offset) { // we cannot process this yet, nead more header data
            self.fragBuffer = [NSData dataWithBytes:buffer length:bufferLen];
            return;
        }
        NSInteger len = dataLength;
        if(dataLength > (bufferLen-offset) || (bufferLen - offset) < dataLength) {
            len = bufferLen-offset;
        }
        NSData *data = nil;
        if(len < 0) {
            len = 0;
            data = [NSData data];
        } else {
            data = [NSData dataWithBytes:(buffer+offset) length:len];
        }
        if(receivedOpcode == JFROpCodePong) {
            NSInteger step = (offset+len);
            NSInteger extra = bufferLen-step;
            if(extra > 0) {
                [self processRawMessage:(buffer+step) length:extra];
            }
            [self _handlePong:response.buffer];
            return;
        }
        JFRResponse *response = [self.readStack lastObject];
        if(isControlFrame) {
            response = nil; //don't append pings
        }
        if(!isFin && receivedOpcode == JFROpCodeContinueFrame && !response) {
            [self closeWithProtocolErrorMessage:@"continue frame before a binary or text frame"];
            return;
        }
        BOOL isNew = NO;
        if(!response) {
            if(receivedOpcode == JFROpCodeContinueFrame) {
                [self closeWithProtocolErrorMessage:@"first frame can't be a continue frame"];
                return;
            }
            isNew = YES;
            response = [JFRResponse new];
            response.code = receivedOpcode;
            response.bytesLeft = dataLength;
            response.buffer = [NSMutableData dataWithData:data];
        } else {
            if(receivedOpcode == JFROpCodeContinueFrame) {
                response.bytesLeft = dataLength;
            } else {
                [self closeWithProtocolErrorMessage:@"second and beyond of fragment message must be a continue frame"];
                return;
            }
            [response.buffer appendData:data];
        }
        response.bytesLeft -= len;
        response.frameCount++;
        response.isFin = isFin;
        if(isNew) {
            [self.readStack addObject:response];
        }
        [self processResponse:response];
        
        NSInteger step = (offset+len);
        NSInteger extra = bufferLen-step;
        if(extra > 0) {
            [self processExtra:(buffer+step) length:extra];
        }
    }
    
}

- (void)processExtra:(uint8_t*)buffer length:(NSInteger)bufferLen {
    if(bufferLen < 2) {
        self.fragBuffer = [NSData dataWithBytes:buffer length:bufferLen];
    } else {
        [self processRawMessage:buffer length:bufferLen];
    }
}

- (BOOL)processResponse:(JFRResponse*)response {
    @autoreleasepool{
        if(response.isFin && response.bytesLeft <= 0) {
            NSData *data = response.buffer;
            if(response.code == JFROpCodePing) {
                //response with pong code
                [self _handlePing:response.buffer];
            } else if(response.code == JFROpCodeTextFrame) {
                NSString *str = [[NSString alloc] initWithData:response.buffer encoding:NSUTF8StringEncoding];
                if(!str) {
                    [self writeError:JFRCloseCodeEncoding];
                    return NO;
                }
                __weak typeof(self) weakSelf = self;
                [self performDelegateBlock:^{
                    if([weakSelf.delegate respondsToSelector:@selector(websocket:didReceiveMessage:)]) {
                        [weakSelf.delegate websocket:weakSelf didReceiveMessage:str];
                    }
                }];
            } else if(response.code == JFROpCodeBinaryFrame) {
                __weak typeof(self) weakSelf = self;
                [self performDelegateBlock:^{
                    if([weakSelf.delegate respondsToSelector:@selector(websocket:didReceiveData:)]) {
                        [weakSelf.delegate websocket:weakSelf didReceiveData:data];
                    }
                }];
            }
            [self.readStack removeLastObject];
            return YES;
        }
        return NO;
    }
}

-(void)dequeueWrite:(NSData*)data withCode:(JFROpCode)code {
    JFRFastLog(@"%s:data:%@,code:%lu",__func__, data, (unsigned long)code);
    
    [self assertOnWorkQueue];
    
    if(!self.isConnected || self.closeWhenFinishWriting) {
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        if(!weakSelf || !weakSelf.isConnected) {
            return;
        }
        typeof(weakSelf) strongSelf = weakSelf;
        uint64_t offset = 2; //how many bytes do we need to skip for the header
        uint8_t *bytes = (uint8_t*)[data bytes];
        uint64_t dataLength = data.length;
        NSMutableData *frame = [[NSMutableData alloc] initWithLength:(NSInteger)(dataLength + JFRMaxFrameSize)];
        uint8_t *buffer = (uint8_t*)[frame mutableBytes];
        buffer[0] = JFRFinMask | code;
        if(dataLength < 126) {
            buffer[1] |= dataLength;
        } else if(dataLength <= UINT16_MAX) {
            buffer[1] |= 126;
            *((uint16_t *)(buffer + offset)) = CFSwapInt16BigToHost((uint16_t)dataLength);
            offset += sizeof(uint16_t);
        } else {
            buffer[1] |= 127;
            *((uint64_t *)(buffer + offset)) = CFSwapInt64BigToHost((uint64_t)dataLength);
            offset += sizeof(uint64_t);
        }
        BOOL isMask = YES;
        if(isMask) {
            buffer[1] |= JFRMaskMask;
            uint8_t *mask_key = (buffer + offset);
            (void)SecRandomCopyBytes(kSecRandomDefault, sizeof(uint32_t), (uint8_t *)mask_key);
            offset += sizeof(uint32_t);
            
            for (size_t i = 0; i < dataLength; i++) {
                buffer[offset] = bytes[i] ^ mask_key[i % sizeof(uint32_t)];
                offset += 1;
            }
        } else {
            for(size_t i = 0; i < dataLength; i++) {
                buffer[offset] = bytes[i];
                offset += 1;
            }
        }
        uint64_t total = 0;
        while (true) {
            if(!strongSelf.isConnected || !strongSelf.outputStream) {
                break;
            }
            NSInteger len = [strongSelf.outputStream write:([frame bytes]+total) maxLength:(NSInteger)(offset-total)];
            if(len < 0 || len == NSNotFound) {
                NSError *error = strongSelf.outputStream.streamError;
                if(!error) {
                    error = [strongSelf errorWithDetail:@"output stream error during write" code:JFROutputStreamWriteError];
                }
                dispatch_async(strongSelf.workQueue, ^{
                    [strongSelf failedWithError:error];
                });
                break;
            } else {
                total += len;
            }
            if(total >= offset) {
                break;
            }
        }
    });
}

- (NSError*)errorWithDetail:(NSString*)detail code:(NSInteger)code {
    return [self errorWithDetail:detail code:code userInfo:nil];
}

- (NSError*)errorWithDetail:(NSString*)detail code:(NSInteger)code userInfo:(NSDictionary *)userInfo {
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    details[NSLocalizedDescriptionKey] = detail;
    if (userInfo) {
        [details addEntriesFromDictionary:userInfo];
    }
    return [[NSError alloc] initWithDomain:@"JFRWebSocket" code:code userInfo:details];
}

- (void)writeError:(uint16_t)code {
    uint16_t buffer[1];
    buffer[0] = CFSwapInt16BigToHost(code);
    NSData *data = [NSData dataWithBytes:buffer length:sizeof(uint16_t)];
    __weak typeof(self) weakSelf = self;
    dispatch_async(self.workQueue, ^{
        [weakSelf dequeueWrite:data
                      withCode:JFROpCodeConnectionClose];
    });
}


- (void)_handlePong:(NSData *)data {
    //response with pong code
    __weak typeof(self) weakSelf = self;
    [self performDelegateBlock:^{
        if([weakSelf.delegate respondsToSelector:@selector(websocket:didReceivePongData:)]) {
            [weakSelf.delegate websocket:weakSelf didReceivePongData:data];
        }
    }];
}

- (void)_handlePing:(NSData *)data {
    __weak typeof(self) weakSelf = self;
    [self performDelegateBlock:^{
        // Base on the ping/pong principle, we need to send pong data on the callback method
        [weakSelf writePong:data];
        if([weakSelf.delegate respondsToSelector:@selector(websocket:didReceivePingData:)]) {
            [weakSelf.delegate websocket:weakSelf didReceivePingData:data];
        }
    }];

}

- (void)dealloc {
    JFRFastLog(@"dealloc");
    
    _outputStream.delegate = nil;
    _inputStream.delegate = nil;
    
    [_outputStream close];
    [_inputStream close];

    if (_workQueue) {
        _workQueue = NULL;
    }
    
    if (_delegateDispatchQueue) {
        _delegateDispatchQueue = NULL;
    }
}

@end

@implementation JFRResponse

@end

@interface JFRRunLoopThread ()

@property (nonatomic, strong) NSRunLoop *runLoop;
@property (nonatomic, strong) dispatch_group_t waitGroup;

@end


@implementation JFRRunLoopThread

+ (instancetype)sharedThread {
    static JFRRunLoopThread *thread;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        thread = [[JFRRunLoopThread alloc] init];
        thread.name = @"com.zeiss.Jetfire.NetWorkThread";
        [thread start];
    });
    return thread;
}

- (instancetype)init {
    if (self = [super init]) {
        _waitGroup = dispatch_group_create();
        dispatch_group_enter(_waitGroup);
    }
    return self;
}

- (void)main {
    @autoreleasepool {
        _runLoop = [NSRunLoop currentRunLoop];
        dispatch_group_leave(_waitGroup);
        
        // Add an empty run loop source to prevent runloop from spinning.
        CFRunLoopSourceContext sourceCtx = {
            .version = 0,
            .info = NULL,
            .retain = NULL,
            .release = NULL,
            .copyDescription = NULL,
            .equal = NULL,
            .hash = NULL,
            .schedule = NULL,
            .cancel = NULL,
            .perform = NULL
        };
        CFRunLoopSourceRef source = CFRunLoopSourceCreate(NULL, 0, &sourceCtx);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), source, kCFRunLoopDefaultMode);
        CFRelease(source);
        
        while ([_runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]) {
            JFRFastLog(@"-------------------------------");
            JFRFastLog(@"Runloop Source: %@", _runLoop);
            JFRFastLog(@"-------------------------------");
        }
        assert(NO);
    }
}

- (NSRunLoop *)runLoop {
    dispatch_group_wait(_waitGroup, DISPATCH_TIME_FOREVER);
    return _runLoop;
}

@end
