//
//  WNFSSDK.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZWNFSManager.h"

@interface WNFSSDK : NSObject

+ (CZWNFSManager*) sharedInstance;
@end
