//
//  CZWNFSManager.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZWNFSManager.h"
#import "CZSMBAdapter.h"
#import "CZSMBTaskParameter.h"
#import "CZSMBTaskOperation.h"
#import "CZSMBErrorHandler.h"
#import "CZSMBTaskResult.h"
#import "CZNetworkUtilities.h"

@interface CZWNFSManager()<CZWNFSManagerDelegate> {
    CZSMBAdapter *adapter;
    NSOperationQueue *operationQueue;
    NSOperationQueue *operationPingQueue;
    NSMutableDictionary *targetMap;
    NSMutableDictionary *threadMap;
}

@end

@implementation CZWNFSManager

static CZWNFSManager *wnfsManager = nil;

#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    static CZWNFSManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CZWNFSManager alloc] init];
    });
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (wnfsManager == nil) {
			wnfsManager = [super allocWithZone:zone];
			return wnfsManager;
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)init {
    self = [super init];
    
    if (self != nil) {
        operationQueue = [[NSOperationQueue alloc] init];
        operationPingQueue = [[NSOperationQueue alloc] init];
        operationPingQueue.maxConcurrentOperationCount = 2;
        ///We can only allow one operation excute in one time.
        operationQueue.maxConcurrentOperationCount = 2;
        threadMap = [[NSMutableDictionary alloc] init];
        targetMap = [[NSMutableDictionary alloc] init];
    }
	return self;
}

- (void)addObserverForOperation:(CZSMBTaskOperation*) task delegate:(id)delegate {
    [task addObserver:self
           forKeyPath:kSMBTaskKeyPath
              options:NSKeyValueObservingOptionNew
              context:NULL];
    if (task.taskInfo.taskType == CZSMBTaskTypeSendFile
        ||task.taskInfo.taskType == CZSMBTaskTypeGetFile) {
        [task addObserver:self
               forKeyPath:kSMBTaskProgressKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    }
    
    if (task.taskInfo.taskType == CZSMBTaskTypeListDirectory) {
        [task addObserver:self
               forKeyPath:kSMBTaskStatusKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    }

    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:task];
        targetMap[key] = delegate;
        threadMap[key] = [NSThread currentThread];
    }

    if (task.taskInfo.taskType == CZSMBTaskTypeServerAvailable) {
        [operationPingQueue addOperation:task];
    }else {
        task.queue = operationQueue;
        [operationQueue addOperation:task];
    }
}

- (void)removeObserverForOperation:(CZSMBTaskOperation*) task {
    [task removeObserver:self
              forKeyPath:kSMBTaskKeyPath];
    if (task.taskInfo.taskType == CZSMBTaskTypeSendFile
        ||task.taskInfo.taskType == CZSMBTaskTypeGetFile) {
        [task removeObserver:self
                  forKeyPath:kSMBTaskProgressKeyPath];
    }
    if (task.taskInfo.taskType == CZSMBTaskTypeListDirectory) {
        [task removeObserver:self
                  forKeyPath:kSMBTaskStatusKeyPath];
    }
}

#pragma mark -
#pragma mark WNFS Synchronous APIs

- (void)setAccountInformation:(NSString*) domain userName:(NSString*) aName password:(NSString*) aPassword {
    [[self sharedSMBAdapterAndCreateIfNil] setAccountInformation:domain
                                          userName:aName
                                          password:aPassword];
}

- (id)sharedSMBAdapter {
    @synchronized(self) {
        return adapter;
    }
}

- (id)sharedSMBAdapterAndCreateIfNil {
    @synchronized(self) {
        if (adapter == nil) {
            adapter = [[CZSMBAdapter alloc] init];
        }
        return adapter;
    }
}

#pragma mark -
#pragma mark WNFS Asynchronous APIs

- (void)isServerReachable:(NSString*) server timeout:(float) timeout delegate:(id<CZWNFSManagerDelegate>)delegate{
    CZSMBTaskParameter *parameters = [[CZSMBTaskParameter alloc] init];
    parameters.taskType = CZSMBTaskTypeServerAvailable;
    parameters.serverPath = server;
    parameters.timeout = timeout;
    CZSMBTaskOperation *taskOperation = [[CZSMBTaskOperation alloc] init];
    taskOperation.adapter = [self sharedSMBAdapterAndCreateIfNil];
    taskOperation.taskInfo = parameters;
    [self addObserverForOperation:taskOperation
                         delegate:delegate];
}

- (void)getFileFrom:(NSString*) serverPath local:(NSString*) localFilePath overwriting:(BOOL) isOverwriting delegate:(id<CZWNFSManagerDelegate>)delegate{
    CZSMBTaskParameter *parameters = [[CZSMBTaskParameter alloc] init];
    parameters.taskType = CZSMBTaskTypeGetFile;
    parameters.serverPath = serverPath;
    parameters.localFilePath = localFilePath;
    parameters.isOverwriting = isOverwriting;
    if ([delegate respondsToSelector:@selector(attributesOfFile:)]) {
        parameters.serverFileAttributes = [delegate attributesOfFile:serverPath];
    }
    CZSMBTaskOperation *taskOperation = [[CZSMBTaskOperation alloc] init];
    taskOperation.adapter = [self sharedSMBAdapterAndCreateIfNil];
    taskOperation.taskInfo = parameters;
    [self addObserverForOperation:taskOperation
                         delegate:delegate];
}

- (void)sendFileTo:(NSString*) serverPath local:(NSString*) localFilePath overwriting:(BOOL) isOverwriting delegate:(id<CZWNFSManagerDelegate>)delegate{
    CZSMBTaskParameter *parameters = [[CZSMBTaskParameter alloc] init];
    parameters.taskType = CZSMBTaskTypeSendFile;
    parameters.serverPath = serverPath;
    parameters.localFilePath = localFilePath;
    parameters.isOverwriting = isOverwriting;
    CZSMBTaskOperation *taskOperation = [[CZSMBTaskOperation alloc] init];
    taskOperation.adapter = [self sharedSMBAdapterAndCreateIfNil];
    taskOperation.taskInfo = parameters;
    [self addObserverForOperation:taskOperation
                         delegate:delegate];
}

- (void)listItemsFrom:(NSString*) serverPath mode:(CZSMBListItemsMode) aMode delegate:(id<CZWNFSManagerDelegate>)delegate {
    CZSMBTaskParameter *parameters = [[CZSMBTaskParameter alloc] init];
    parameters.taskType = CZSMBTaskTypeListDirectory;
    parameters.listItemsMode = aMode;
    parameters.serverPath = serverPath;
    CZSMBTaskOperation *taskOperation = [[CZSMBTaskOperation alloc] init];
    taskOperation.adapter = [self sharedSMBAdapterAndCreateIfNil];
    taskOperation.taskInfo = parameters;
    [taskOperation setQueuePriority:NSOperationQueuePriorityNormal];
    [self addObserverForOperation:taskOperation
                         delegate:delegate];
}

- (void)listItemsFrom:(NSString*) serverPath delegate:(id<CZWNFSManagerDelegate>)delegate{
    [self listItemsFrom:serverPath
                   mode:CZSMBListItemsModeAll
               delegate:delegate];
}

- (void)deleteItemFrom:(NSString*) serverPath delegate:(id<CZWNFSManagerDelegate>)delegate {
    [self deleteItemFrom:serverPath
                   force:NO
                delegate:delegate];
}

- (void)deleteItemFrom:(NSString*) serverPath force:(BOOL) isForce delegate:(id<CZWNFSManagerDelegate>)delegate{
    CZSMBTaskParameter *parameters = [[CZSMBTaskParameter alloc] init];
    parameters.taskType = CZSMBTaskTypeDeleteItem;
    parameters.serverPath = serverPath;
    parameters.isForce = isForce;
    CZSMBTaskOperation *taskOperation = [[CZSMBTaskOperation alloc] init];
    taskOperation.adapter = [self sharedSMBAdapterAndCreateIfNil];
    taskOperation.taskInfo = parameters;
    [taskOperation setQueuePriority:NSOperationQueuePriorityNormal];
    [self addObserverForOperation:taskOperation
                         delegate:delegate];
}

- (void)cancelAllOperation {
    [operationQueue cancelAllOperations];
    [operationPingQueue cancelAllOperations];
}

- (void)releaseAdapter {
    @synchronized(self) {
        if (adapter) {
            [self cancelAllOperation];
            adapter = nil;
        }
    }
}

#pragma mark - Delegate Process

- (void)operationCancelled:(CZSMBTaskOperation *)operation {
    id<CZWNFSManagerDelegate> target;
    CZSMBTaskOperation * taskOperation = operation;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:taskOperation];
        target = targetMap[key];
        assert(target != nil);
        [targetMap removeObjectForKey:key];
    }
    
    switch (operation.taskInfo.taskType) {
        case CZSMBTaskTypeGetFile: {
            if ([target respondsToSelector:@selector(dataTransferDidFailWithError:)]) {
                NSError *error = [CZSMBErrorHandler createErrorWithCode:kErrorCodeOperationCancelled
                                                                   path:operation.taskInfo.localFilePath];
                [target dataTransferDidFailWithError:error];
            }
            break;
        }
        case CZSMBTaskTypeSendFile: {
            if ([target respondsToSelector:@selector(dataTransferDidFailWithError:)]) {
                NSError *error = [CZSMBErrorHandler createErrorWithCode:kErrorCodeOperationCancelled
                                                                   path:operation.taskInfo.serverPath];
                [target dataTransferDidFailWithError:error];
            }
        }
            break;
        case CZSMBTaskTypeListDirectory:
        case CZSMBTaskTypeDeleteItem: {
            if ([target respondsToSelector:@selector(operateDidFailWithError:)]) {
                NSError *error = [CZSMBErrorHandler createErrorWithCode:kErrorCodeOperationCancelled
                                                                   path:operation.taskInfo.serverPath];
                [target operateDidFailWithError:error];
            }
        }
            break;
        case CZSMBTaskTypeServerAvailable: {
            if ([target respondsToSelector:@selector(reachingDidFailWithError:)]) {
                NSError *error = [CZSMBErrorHandler createErrorWithCode:kErrorCodeOperationCancelled
                                                                   path:operation.taskInfo.serverPath];
                [target reachingDidFailWithError:error];
            }
        }
            break;
        default:
            assert(NO);
            break;
    }
    
}

- (void)operationDone:(CZSMBTaskOperation *)operation {
    id<CZWNFSManagerDelegate> target;
    CZSMBTaskOperation * taskOperation = operation;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:taskOperation];
        target = targetMap[key];
        [targetMap removeObjectForKey:key];
    }
    
    switch (operation.taskInfo.taskType) {
        case CZSMBTaskTypeGetFile: {
            if (![CZSMBErrorHandler isError:taskOperation.error]) {
                if ([target respondsToSelector:@selector(dataTransferDidFinishDestination:)]) {
                    [target dataTransferDidFinishDestination:taskOperation.taskInfo.localFilePath];
                }
            }else {
                if ([target respondsToSelector:@selector(dataTransferDidFailWithError:)]) {
                    NSError *error = [CZSMBErrorHandler createErrorWithCode:taskOperation.error
                                                                       path:taskOperation.taskInfo.localFilePath];
                    [target dataTransferDidFailWithError:error];
                }
            }
        }
            break;
        case CZSMBTaskTypeSendFile: {
            if (![CZSMBErrorHandler isError:taskOperation.error]) {
                if ([target respondsToSelector:@selector(dataTransferDidFinishDestination:)]) {
                    [target dataTransferDidFinishDestination:taskOperation.taskInfo.serverPath];
                }
            }else {
                if ([target respondsToSelector:@selector(dataTransferDidFailWithError:)]) {
                    NSError *error = [CZSMBErrorHandler createErrorWithCode:taskOperation.error
                                                                       path:taskOperation.taskInfo.serverPath];
                    [target dataTransferDidFailWithError:error];
                }
            }
        }
            break;
        case CZSMBTaskTypeListDirectory: {
            if (![CZSMBErrorHandler isError:taskOperation.error]) {
                if ([target respondsToSelector:@selector(operateDidFinishItem:type:)]) {
                    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                          taskOperation.resultInfo.directory,
                                          taskOperation.taskInfo.serverPath,
                                          nil];
                    [target operateDidFinishItem:dict
                                            type:CZOperateTypeListDirectory];
                }
            }else {
                if ([target respondsToSelector:@selector(operateDidFailWithError:)]) {
                    NSError *error = [CZSMBErrorHandler createErrorWithCode:taskOperation.error
                                                                       path:taskOperation.taskInfo.serverPath];
                    [target operateDidFailWithError:error];
                }
            }
        }
            break;
        case CZSMBTaskTypeDeleteItem: {
            if (![CZSMBErrorHandler isError:taskOperation.error]) {
                if ([target respondsToSelector:@selector(operateDidFinishItem:type:)]) {
                    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                          taskOperation.taskInfo.serverPath,
                                          taskOperation.taskInfo.serverPath,
                                          nil];
                    [target operateDidFinishItem:dict
                                            type:CZOperateTypeDeleteItem];
                }
            }else {
                if ([target respondsToSelector:@selector(operateDidFailWithError:)]) {
                    NSError *error = [CZSMBErrorHandler createErrorWithCode:taskOperation.error
                                                                       path:taskOperation.taskInfo.serverPath];
                    [target operateDidFailWithError:error];
                }
            }
        }
            break;
        case CZSMBTaskTypeServerAvailable: {
            if ([CZSMBErrorHandler isError:taskOperation.error]) {
                
                if ([target respondsToSelector:@selector(reachingDidFailWithError:)]) {
                    NSError *error = [CZSMBErrorHandler createErrorWithCode:taskOperation.error
                                                                       path:taskOperation.taskInfo.serverPath];
                    [target reachingDidFailWithError:error];
                }
            }else {
                if ([target respondsToSelector:@selector(reachingDidSuccess:)]) {
                    [target reachingDidSuccess:taskOperation.taskInfo.serverPath];
                }
            }
        }
            break;
        default:
            assert(NO);
            break;
    }

}

- (void)operationProgress:(CZSMBTaskOperation *)operation {
    id<CZWNFSManagerDelegate> target;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:operation];
        target = targetMap[key];
    }
    
    switch (operation.taskInfo.taskType) {
        case CZSMBTaskTypeGetFile: {
            if ([target respondsToSelector:@selector(dataTransferProgress:delivered:total:destination:)]) {
                [target dataTransferProgress:CZTransferDirectionReceiving
                                   delivered:operation.delivered
                                       total:operation.total
                                 destination:operation.taskInfo.localFilePath];
            }
        }
            break;
        case CZSMBTaskTypeSendFile: {
            if ([target respondsToSelector:@selector(dataTransferProgress:delivered:total:destination:)]) {
                [target dataTransferProgress:CZTransferDirectionSending
                                   delivered:operation.delivered
                                       total:operation.total
                                 destination:operation.taskInfo.serverPath];
            }
        }
            break;
        default:
            break;
    }
}

- (void)operationStatus:(CZSMBTaskOperation *)operation {
    id<CZWNFSManagerDelegate> target;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:operation];
        target = targetMap[key];
    }
    
    switch (operation.taskInfo.taskType) {
        case CZSMBTaskTypeListDirectory: {
            if ([target respondsToSelector:@selector(operateDidChangeStatus:type:)]) {
                [target operateDidChangeStatus:[NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInteger:operation.itemsCount]
                                                                           forKey:@"Count"]
                                          type:CZSMBTaskTypeListDirectory];
            }
        }
            break;
        default:
            break;
    }
}

- (void)observeValueForStatusOperation:(id)object{
    CZSMBTaskOperation * operation = (CZSMBTaskOperation *) object;
    NSThread *thread = nil;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:operation];
        thread = threadMap[key];
    }
    if (operation.isCancelled) {
        ///do nothing
    }else {
        ///update status
        if (thread != nil) {
            [self performSelector:@selector(operationStatus:)
                         onThread:thread
                       withObject:operation
                    waitUntilDone:NO];
        }else {
            [self performSelectorOnMainThread:@selector(operationStatus:)
                                   withObject:operation
                                waitUntilDone:NO];
        }
    }
}

- (void)observeValueForProgressOperation:(id)object{
    CZSMBTaskOperation * operation = (CZSMBTaskOperation *) object;
    NSThread *thread = nil;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:operation];
        thread = threadMap[key];
    }
    if (operation.isCancelled) {
        ///do nothing;
    }else {
        ///update progress
        if (thread != nil) {
            [self performSelector:@selector(operationProgress:)
                         onThread:thread
                       withObject:operation
                    waitUntilDone:NO];
        }else {
            [self performSelectorOnMainThread:@selector(operationProgress:)
                                   withObject:operation
                                waitUntilDone:NO];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    assert([object isKindOfClass:[CZSMBTaskOperation class]]);
    if ([keyPath isEqualToString:kSMBTaskProgressKeyPath]) {
        [self observeValueForProgressOperation:object];
        return;
    }
    if ([keyPath isEqualToString:kSMBTaskStatusKeyPath]) {
        [self observeValueForStatusOperation:object];
        return;
    }
    
    CZSMBTaskOperation * operation = (CZSMBTaskOperation *) object;
    [self removeObserverForOperation:operation];
    NSThread *thread = nil;
    @synchronized (self) {
        NSValue *key = [NSValue valueWithNonretainedObject:operation];
        thread = threadMap[key];
        [threadMap removeObjectForKey:key];
    }

    SEL sel;
    if ([operation isCancelled]) {
        sel = @selector(operationCancelled:);
    }else {
        sel = @selector(operationDone:);
    }
    
    if (thread != nil) {
        [self performSelector:sel
                     onThread:thread
                   withObject:operation
                waitUntilDone:NO];
    }else {
        [self performSelectorOnMainThread:sel
                               withObject:thread
                            waitUntilDone:NO];
    }
}

@end
