//
//  CZSMBAdapter.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBAdapter.h"
#import "CZGlobal.h"
#import "CZSMBConfiguration.h"
#import "CZFileDescriptor.h"
#import "CZFileAttribute.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBConnection.h"
#import "CZDiskInformation.h"
#import "CZSMBPath.h"
#import "CZSMBConsistents.h"

#define CZSMBConnectRetryCount 1

@interface CZSMBAdapter() <NSStreamDelegate> {
    CZSMBDirectTCPTransport *   directTCPTransport;
}

@end

@implementation CZSMBAdapter

#pragma mark - Class Lifecycle

+ (BOOL)isString:(NSString *)string equalToString:(NSString *)anotherString {
    if (string.length == 0) {
        return anotherString.length == 0;
    } else {
        return [string isEqualToString:anotherString];
    }
}

- (id)init {
    if (self = [super init]) {
        directTCPTransport = [[CZSMBDirectTCPTransport alloc] init];
    }
    return self;
}

- (id)initWithAccountInformation:(NSString *)domain userName:(NSString *)aName password:(NSString *)aPassword {
    if (self = [self init]) {
        [self setAccountInformation:domain
                           userName:aName
                           password:aPassword];
    }
    return self;
}

- (void)dealloc {
    [directTCPTransport disconnectFromShare];
    [directTCPTransport stopThread];
}
#pragma mark - Internal Method

- (BOOL)isNeedDisconnectHost:(NSString*) host share:(NSString*) share user:(NSString*) userName password:(NSString*) password{
    if (![CZSMBAdapter isString:host equalToString:directTCPTransport.serverName]
        || ![CZSMBAdapter isString:share equalToString:directTCPTransport.shareName]
        || ![CZSMBAdapter isString:userName equalToString:directTCPTransport.userName]
        || ![CZSMBAdapter isString:password equalToString:directTCPTransport.password]) {
        return YES;
    }
    return NO;
}

- (BOOL)connectToShare:(CZSMBPath *)smbPath error:(CZSMBErrorCode *)error {
    CZSMBErrorCode tempError = kErrorCodeNone;
    CZSMBErrorCode firstTryError = kErrorCodeNone;
    
    directTCPTransport.tryConnectCount = 0;
    while (directTCPTransport.tryConnectCount < CZSMBConnectRetryCount) {
        tempError = [directTCPTransport connectToShare];
        if ([CZSMBErrorHandler isError:tempError] &&
            tempError != kErrorCodeServerIncompatible) {
            if (directTCPTransport.tryConnectCount == 0) {
                //record the first try connect error
                firstTryError = tempError;
            }
            ///let's try again
            [directTCPTransport disconnectFromShare];
            directTCPTransport.serverName = smbPath.host;
            directTCPTransport.tryConnectCount++;
        } else {
            break;
        }
    }
    
    CZSMBErrorCode errorCode = 0;
    if (directTCPTransport.tryConnectCount >= CZSMBConnectRetryCount &&
        tempError == kSTATUS_INVALID_PARAMETER) {
        //if the server use ntlm and failed to connect, the last error code is not valid
        //because the last connect uses ntlmv2.
        //if the server use ntlmv2 and failed to connect in the last try because the return is
        //invalid parameter, it's very unlikely so we will fall back to the error code of the first
        //connect try as well.
        errorCode = firstTryError;
    } else {
        errorCode = tempError;
    }
    
    if (error) {
        *error = errorCode;
    }
    
    return ![CZSMBErrorHandler isError:errorCode];
}

#pragma mark - APIs

- (void)setAccountInformation:(NSString *)domain userName:(NSString *)aName password:(NSString *)aPassword {
    self.userName = nil;
    self.userName = aName;
    self.password = nil;
    self.password = aPassword;
    self.domainName = nil;
    self.domainName = domain;
}

- (NSArray *)enumServerFrom:(NSString *) hostPath error:(CZSMBErrorCode*)error {
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:hostPath];
    if (smbPath.isValidPath == NO) {
        *error = kErrorCodeServerPathError;
        return nil;
    }
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
    
    if (![self connectToShare:smbPath error:error]) {
        return nil;
    }
    
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    ///named pipe
    *error = [directTCPTransport enumServer:itemsArray];
    
    if ([CZSMBErrorHandler isError:*error]) {
        return nil;
    }
    return itemsArray;
}

- (NSArray *)listDirectory:(NSString *)directoryPath error:(CZSMBErrorCode*)error {
    return [self _listDirectory:directoryPath
                          error:error];
}

- (NSArray *)_listDirectory:(NSString *)directoryPath error:(CZSMBErrorCode*)error {
    
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:directoryPath];
    if (smbPath.isValidPath == NO) {
        *error = kErrorCodeServerPathError;
        return nil;
    }
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
    
    [directTCPTransport reconnectToTree];
    
    if (![self connectToShare:smbPath error:error]) {
        return nil;
    }
    
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    ///named pipe
    if ([smbPath.fileName isEqualToString:@"IPC$"]) {
        *error = [directTCPTransport enumShareWithRPC:itemsArray];
        if ([itemsArray count] == 0) {
            *error = [directTCPTransport enumShare:itemsArray];
        }
    }else {
        *error = [directTCPTransport listDirectory:smbPath.relativePath
                                                to:itemsArray];
    }
    if ([CZSMBErrorHandler isError:*error]) {
        return nil;
    }
    return itemsArray;
}

- (CZFileDescriptor*)getAttribute:(NSString*) filePath error:(CZSMBErrorCode*)error {
    return [self _getAttribute:filePath
                         error:error];
}

- (CZFileDescriptor*)_getAttribute:(NSString*) filePath error:(CZSMBErrorCode*)error {
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:filePath];
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
    
    if (![self connectToShare:smbPath error:error]) {
        return nil;
    }
    
    CZFileDescriptor * fileDescriptor = [[CZFileDescriptor alloc] init];
    fileDescriptor.fileName = smbPath.fileName;
    fileDescriptor.filePath = smbPath.relativePath;
    *error = [directTCPTransport queryFileInfo:fileDescriptor];
    if ([CZSMBErrorHandler isError:*error]) {
        return nil;
    }
    return fileDescriptor;
}

- (CZSMBErrorCode)setAttribute:(CZFileDescriptor*) fileDescriptor {
    if (fileDescriptor == nil || fileDescriptor.FID == 0) {
        return kErrorCodeFileDescriptorError;
    }
    CZSMBErrorCode error = [directTCPTransport setFileInfo:fileDescriptor];
    return error;
}

- (CZSMBErrorCode)getDiskInformation:(CZDiskInformation*) diskInfo {
    if (diskInfo == nil) {
        return kErrorCodeArgumentInvalid;
    }
    CZSMBErrorCode error = [directTCPTransport queryFileSystemInfo:diskInfo];
    return error;
}

- (CZFileDescriptor*)openFile:(NSString*) filePath options:(CZSMBFileOpenFlag)mask error:(CZSMBErrorCode*)error {
    return [self _openFile:filePath
                   options:mask
                     error:error];
}

- (CZFileDescriptor*)_openFile:(NSString*) filePath options:(CZSMBFileOpenFlag)mask error:(CZSMBErrorCode*)error {
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:filePath];
    if (smbPath.isValidFile == NO) {
        *error = kErrorCodeFilePathError;
        return nil;
    }
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
        
    if (![self connectToShare:smbPath error:error]) {
        return nil;
    }
    
    CZFileDescriptor * fileDescriptor = [[CZFileDescriptor alloc] init];
    fileDescriptor.fileName = smbPath.fileName;
    fileDescriptor.filePath = smbPath.relativePath;
    fileDescriptor.openFlag = mask;
    *error = [directTCPTransport openFile:fileDescriptor];
    if ([CZSMBErrorHandler isError:*error]) {
        return nil;
    }
    return fileDescriptor;
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor byLength:(uint32_t)length data:(NSData **)dataBuffer {
    return [self readFile:fileDescriptor byLength:length preload:NO data:dataBuffer];
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor byLength:(uint32_t)length preload:(BOOL)isPreload data:(NSData **)dataBuffer {
    if (NULL == dataBuffer) {
        return 0;
    }
    
    CZSMBErrorCode error = [self _readFile:fileDescriptor byLength:length preload:isPreload];
    if ([CZSMBErrorHandler isError:error] == NO) {
        *dataBuffer = fileDescriptor.dataPtr;
        return fileDescriptor.readBytes;
    }
    return error;
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor buffer:(void *)dataBuffer size:(uint32_t)bufferSize {
    return [self readFile:fileDescriptor
                   buffer:dataBuffer
                     size:bufferSize
                  preload:NO];
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor buffer:(void *)dataBuffer size:(uint32_t)bufferSize preload:(BOOL)isPreload {
    if (NULL == dataBuffer || bufferSize == 0) {
        return 0;
    }
    
    CZSMBErrorCode error = [self _readFile:fileDescriptor byLength:bufferSize preload:isPreload];
    if (error == kErrorCodeNone) {
        if (fileDescriptor.dataPtr.length >= fileDescriptor.readBytes) {
            memcpy(dataBuffer, [fileDescriptor.dataPtr bytes], fileDescriptor.readBytes);
            return fileDescriptor.readBytes;
        } else {
            return 0;
        }
    }
    
    if (![CZSMBErrorHandler isError:error]) {
        error = 0;
    }
    return error;
}

- (CZSMBErrorCode)_readFile:(CZFileDescriptor *)fileDescriptor byLength:(uint32_t)length preload:(BOOL)isPreload {
    if (fileDescriptor == nil || fileDescriptor.FID == 0) {
        return kErrorCodeFileDescriptorError;
    }
    if (fileDescriptor.fileName == nil) {
        return kErrorCodeFileDescriptorError;
    }
    
    fileDescriptor.readBytes = length;
    CZSMBErrorCode error;
    if (isPreload) {
        error = [directTCPTransport readFile:fileDescriptor preload:isPreload];
    } else {
        error = [directTCPTransport readFile:fileDescriptor];
    }
    
    return error;
}

- (CZSMBErrorCode)writeFile:(CZFileDescriptor*)fileDescriptor buffer:(void *) inputBuffer size:(uint16_t)bufferSize {
    return [self _writeFile:fileDescriptor
                     buffer:inputBuffer
                       size:bufferSize];
}

- (CZSMBErrorCode)_writeFile:(CZFileDescriptor*)fileDescriptor buffer:(void *) inputBuffer size:(uint16_t)bufferSize {
    if (fileDescriptor == nil || fileDescriptor.FID == 0) {
        return kErrorCodeFileDescriptorError;
    }
    if (fileDescriptor.fileName == nil) {
        return kErrorCodeFileDescriptorError;
    }
    fileDescriptor.dataPtr = [[NSData alloc] initWithBytesNoCopy:inputBuffer length:bufferSize freeWhenDone:NO];
    fileDescriptor.writeBytes = bufferSize;
    CZSMBErrorCode error = [directTCPTransport writeFile:fileDescriptor];
    if ([CZSMBErrorHandler isError:error] == NO) {
        return fileDescriptor.writeBytes;
    }
    return error;
}

- (CZSMBErrorCode)seekFile:(CZFileDescriptor*)fileDescriptor offset:(int64_t)fileOffset mode:(CZSMBFileSeekMode)seekMode {
    int64_t offset = fileDescriptor.offset;
    switch (seekMode) {
        case CZSMBFileSeekModeSet:
            offset = fileOffset;
            break;
        case CZSMBFileSeekModeCur:
            offset += fileOffset;
            break;
        case CZSMBFileSeekModeEnd:
            offset = fileDescriptor.endOfFile;
            offset -= fileOffset;
            break;
        default:
            assert(NO);
            break;
    }
    
    if (offset < 0 || offset > fileDescriptor.endOfFile) {
        return kErrorCodeFileSeekOutofRange;
    }
    
    fileDescriptor.offset = offset;
    return kErrorCodeNone;
}

- (CZSMBErrorCode)deleteFile:(NSString *) filePath {
    CZFileDescriptor *fileDescriptor = nil;
    @try {
        CZSMBErrorCode error = kErrorCodeNone;
        fileDescriptor = [self openFile:filePath options:kOpenFlagDelete|kOpenFlagWriteOnly error:&error];
        if (error != kErrorCodeNone) {
            return error;
        }
    } @finally {
        if (fileDescriptor) {
            CZSMBErrorCode error = [self closeFile:fileDescriptor];
            if (error != kErrorCodeNone) {
                return error;
            }
        }
    }
    
    return [self _deleteFile:filePath];
}

- (CZSMBErrorCode)_deleteFile:(NSString *) filePath {
    CZSMBErrorCode error = kErrorCodeNone;
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:filePath];
    if (smbPath.isValidFile == NO) {
        return kErrorCodeServerPathError;
    }
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
    
    if (![self connectToShare:smbPath error:&error]) {
        return error;
    }
    
    error = [directTCPTransport deleteFile:[NSString stringWithFormat:@"%@%@",
                                            smbPath.relativePath,
                                            smbPath.fileName]];
    return error;
}

- (CZSMBErrorCode)deleteDirectory:(NSString *)directoryPath {
    return [self _deleteDirectory:directoryPath];
}

- (CZSMBErrorCode)_deleteDirectory:(NSString *)directoryPath {
    CZSMBErrorCode error = kErrorCodeNone;
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:directoryPath];
    if (smbPath.isValidPath == NO) {
        return kErrorCodeServerPathError;
    }
    if ([self isNeedDisconnectHost:smbPath.host
                             share:smbPath.share
                              user:self.userName
                          password:self.password]) {
        [directTCPTransport disconnectFromShare];
    }
    directTCPTransport.serverName = smbPath.host;
    directTCPTransport.shareName = smbPath.share;
    directTCPTransport.userName = self.userName;
    directTCPTransport.password = self.password;
    directTCPTransport.domainName = self.domainName;
    
    if (![self connectToShare:smbPath error:&error]) {
        return error;
    }
    
    error = [directTCPTransport deleteDirectory:smbPath.relativePath];
    return error;
}

- (CZSMBErrorCode)closeFile:(CZFileDescriptor*)fileDescriptor {
    return [self _closeFile:fileDescriptor];
}

- (CZSMBErrorCode)_closeFile:(CZFileDescriptor*)fileDescriptor {
    if (fileDescriptor == nil || fileDescriptor.FID == 0) {
        return kErrorCodeFileDescriptorError;
    }
    CZSMBErrorCode error = [directTCPTransport closeFile:fileDescriptor];
    return error;
}

- (CZSMBErrorCode)renameFile:(NSString *)oldFilePath newName:(NSString *)newFilePath {
    if (oldFilePath == nil || newFilePath == nil) {
        return kErrorCodeFileNameInvalid;
    }
    
    CZSMBPath * oldSMBPath = [[CZSMBPath alloc] initWithSMBString:oldFilePath];
    CZSMBPath * newSMBPath = [[CZSMBPath alloc] initWithSMBString:newFilePath];
    
    NSString *oldStr = [NSString stringWithFormat:@"%@%@", oldSMBPath.relativePath, oldSMBPath.fileName];
    NSString *newStr = [NSString stringWithFormat:@"%@%@", newSMBPath.relativePath, newSMBPath.fileName];
    
    CZSMBErrorCode error = [directTCPTransport rename:oldStr
                                              newName:newStr];
    return error;
}
@end
