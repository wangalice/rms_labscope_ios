//
//  CZSMBConnection.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/6/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBErrorHandler.h"

#define kDirectTCPTransportPacketHeader     (4)
#define kMaxPacketReciveOnce                4*1024//4k
#define kMaxBuffer                          0x20000//this buffer need big than kMaxTransDataBlockSize
#define kSMBPrereadMultiplexID              0x1000
/**
 *  max allow 100*0.1 = 10 second delay when packet sending once
 */
#define kConnectWaitInterval        0.02

#define kPacketSendTry              100
#define kPacketSendTryInterval      0.1

#define kDataReciveTry              300
#define kDataReciveTryInterval      0.1

typedef NS_ENUM(NSUInteger, CZSMBAuthenticationType)
{
    CZSMBAuthenticationTypePlaintext = 0,
    CZSMBAuthenticationTypeLM = 1,
    CZSMBAuthenticationTypeNTLM = 2,
};

@protocol CZSMBConnectionDelegate;
@class CZSMBDirectTCPTransport;
@interface CZSMBConnection : NSObject

@property (nonatomic,weak)    CZSMBDirectTCPTransport *   tcpTransport;

@property (nonatomic,strong)    NSString *                  dialectString;
@property (nonatomic,strong)    NSData *                    serverChallenge;
@property (nonatomic,copy)      NSString *                  primaryDomain;

@property (nonatomic,readonly)  BOOL                        isHostConnected;
@property (nonatomic,readwrite) BOOL                        isNegotiated;
@property (nonatomic,readwrite) BOOL                        isAuthenticated;
@property (nonatomic,readwrite) BOOL                        isShareConnected;
@property (nonatomic,readonly)  BOOL                        isExtendedSecurity;
@property (nonatomic,readwrite) BOOL                        isShareLevelAccessControl;
@property (nonatomic,readwrite) BOOL                        isServerChallengeResponse;
@property (nonatomic,readwrite) BOOL                        isSigningMandatory;
@property (nonatomic,readonly)  BOOL                        isUnicodeSupported;
@property (nonatomic,readonly)  BOOL                        isNTDialectSupported;
@property (nonatomic,readonly)  BOOL                        isTimeout;

/**
 * Maximum Buffer size, in bytes, of the server buffer for receiving SMB messages. This value accounts for the size of the largest SMB message that the client can send to the server, measured from the start of the SMB header to the end of the packet. This value does not account for any underlying transport-layer packet headers, and thus does not account for the size of the complete network packet.
 */
@property (nonatomic,readwrite) uint32_t                    serverMaxBufferSize;
@property (nonatomic,readwrite) uint32_t                    serverCapabilities;
// Minutes from UTC; signed
@property (nonatomic,readwrite) int32_t                     serverTimeZone;

@property (nonatomic,readwrite) uint16_t                     treeID;
@property (nonatomic,readwrite) uint16_t                     processID;
@property (nonatomic,readwrite) uint16_t                     userID;
@property (nonatomic,readwrite) uint16_t                     maxMpxCount;
@property (nonatomic,readwrite) uint16_t                     maxAllowMpxCount;

@property (nonatomic,readwrite) uint32_t                    sessionKey;

@property (nonatomic,readwrite) CZSMBAuthenticationType     authenticationType;
@property (nonatomic,strong)    NSData *                    authenticateToken;


- (CZSMBErrorCode)startConnectHost;
- (void)disconnectHost;
- (void)setDelegate:(id<CZSMBConnectionDelegate>) delegate;
- (void)clearDelegate;

- (void)releaseMultiplexID;
- (void)releasePreloadMultiplexID;
- (uint16_t)getMultiplexID;
- (uint16_t)getPreloadMultiplexID;

/**
 *  Send the bytes from the specified buffer to the server.
 *  When using Direct TCP as the SMB transport, the implementer MUST prepend a 4-byte Direct TCP transport packet header to each SMB message. This transport header MUST be formatted as a byte of zero (8 zero bits) followed by 3 bytes that indicate the length of the SMB message that is encapsulated.
 *  @return The packet has sended or not.
 */
- (BOOL)sendDirectTCPPacket:(NSData*) packetData;

@end

@protocol CZSMBConnectionDelegate <NSObject>
- (void)connectionDidRecivedPacket:(NSData*) data;
- (void)connectionDidRecivedError:(CZSMBErrorCode) code;
@end
