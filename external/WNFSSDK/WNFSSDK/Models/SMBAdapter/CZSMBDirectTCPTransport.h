//
//  CZSMBDirectTCPTransport.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBErrorHandler.h"


@class CZSMBConnection,CZFileDescriptor,CZDiskInformation;
@interface CZSMBDirectTCPTransport : NSObject

@property (nonatomic,strong)    NSString *                  serverName;
@property (nonatomic,strong)    NSString *                  serverIP;
@property (nonatomic,strong)    NSString *                  shareName;
@property (nonatomic,strong)    NSString *                  domainName;
@property (nonatomic,strong)    NSString *                  userName;
@property (nonatomic,strong)    NSString *                  password;
@property (nonatomic, assign)   NSUInteger                  tryConnectCount;

- (CZSMBErrorCode)connectToShare;

- (CZSMBErrorCode)reconnectToTree;
- (CZSMBErrorCode)listDirectory:(NSString*) directory to:(NSMutableArray*) array;
- (CZSMBErrorCode)enumServer:(NSMutableArray*) array;
- (CZSMBErrorCode)enumShare:(NSMutableArray*) array;
- (CZSMBErrorCode)enumShareWithRPC:(NSMutableArray*) array;
- (CZSMBErrorCode)queryFileInfo:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)setFileInfo:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)queryFileSystemInfo:(CZDiskInformation*) theDiskInfo;
- (CZSMBErrorCode)openFile:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor preload:(BOOL) isPreload;
- (CZSMBErrorCode)writeFile:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)deleteFile:(NSString*) fileName;
- (CZSMBErrorCode)deleteDirectory:(NSString*) path;
- (CZSMBErrorCode)closeFile:(CZFileDescriptor*) fileDescriptor;
- (CZSMBErrorCode)rename:(NSString*) oldFileName newName:(NSString*) fileName;
- (void)disconnectFromShare;

- (void)stopThread;
@end
