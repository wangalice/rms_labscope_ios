//
//  CZSMBEvents.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"
#import "CZGlobal.h"
#import "CZSMBConsistents.h"
#import "CZSMBErrorHandler.h"
#import "CZSMBConnection.h"

#define kEventKeyPath                       @"state"

@interface CZSMBEvents() {
@private
    NSTimer *eventTimeoutTimer;
}

@end

@implementation CZSMBEvents

- (id)init {
    self = [super init];
    if (self) {
        [self addObserver:self
               forKeyPath:@"state"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"state"];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (self == object && [keyPath isEqualToString:@"state"]) {
        id<CZSMBEventsObserver> observer = self.observer;
        if ([observer respondsToSelector:@selector(smbEventsStateChanged:)]) {
            [observer smbEventsStateChanged:self];
        }
    }
}

- (void)setTimeout {
    if ([eventTimeoutTimer isValid]) {
        [eventTimeoutTimer invalidate];
    }
    eventTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kSMBEventTimeOut
                                                         target:self
                                                       selector:@selector(eventDidTimeout:)
                                                       userInfo:nil
                                                        repeats:NO];
    
    [[NSRunLoop currentRunLoop] addTimer:eventTimeoutTimer
                                 forMode:NSRunLoopCommonModes];
}

- (void)clearTimeout {
    if (eventTimeoutTimer) {
        if ([eventTimeoutTimer isValid]) {
          [eventTimeoutTimer invalidate];  
        }
        eventTimeoutTimer = nil;
    }
}

- (void)eventDidTimeout:(NSTimer*) theTimer {
    if (eventTimeoutTimer) {
        if ([eventTimeoutTimer isValid]) {
            [eventTimeoutTimer invalidate];
        }
        eventTimeoutTimer = nil;
    }
    self.errorCode = kErrorCodeServerNoResponse;
    [self setState:CZSMBEventsStateError];
}

- (void)dataError:(NSNumber*) code {
    [self clearTimeout];
    self.errorCode = [code integerValue];
    [self setState:CZSMBEventsStateError];
}
@end
