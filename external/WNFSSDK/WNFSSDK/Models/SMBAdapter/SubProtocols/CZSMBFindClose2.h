//
//  CZSMBFindClose2.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"

@interface CZSMBFindClose2 : CZSMBEvents<CZSMBEventsDelegate>

@property (nonatomic,readwrite) uint16_t              searchID;
@end
