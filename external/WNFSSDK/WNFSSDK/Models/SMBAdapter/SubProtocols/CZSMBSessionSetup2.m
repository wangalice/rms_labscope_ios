//
//  CZSMBSessionSetup2.m
//  Matscope
//
//  Created by Mike Wang on 12/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSMBSessionSetup2.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZLANManagerAuthentication.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"

static uint8_t reqMsg[11] = {
    0x04, 0xff, 0x00, 0xb1, 0x00, 0x08, 0x00, 0x01,
    0x00, 0x27, 0x00
};

@implementation CZSMBSessionSetup2

- (int)constructAccountBlockByUserName:(NSString*) aName domain:(NSString*) aDomain toByte:(uint8_t*) block {
    NSString *nativeOS = kSMBClientOS;
    NSString *nativeLanMan = kSMBClientName;
    
    ///null pad, ensure that the AccountName string is aligned on a 16-bit boundary.
    int index = 1;
    
    [aName getCString:(char*)block+index
            maxLength:kMaxDataBlockSize-index
             encoding:kUnicodeEncoding];
    
    ///plus \0 length
    index+=aName.length*2+2;
    
    NSString * domain = aDomain;
    if (domain == nil || [domain isEqualToString:@""]) {
        domain = @"?";
    }
    
    [domain getCString:(char*)block+index
             maxLength:kMaxDataBlockSize-index
              encoding:kUnicodeEncoding];
    index+=domain.length*2+2;
    
    [nativeOS getCString:(char*)block+index
               maxLength:kMaxDataBlockSize-index
                encoding:kUnicodeEncoding];
    index+=nativeOS.length*2+2;
    
    [nativeLanMan getCString:(char*)block+index
                   maxLength:kMaxDataBlockSize-index
                    encoding:kUnicodeEncoding];
    index+=nativeLanMan.length*2+2;
    
    return index;
}

- (NSUInteger)constructDataBlockByUnicodePassword:(NSString *) password userName:(NSString *) aName domain:(NSString *) aDomain challenge:(const uint8_t *)challenge challengeLength:(NSUInteger)challengeLength targetDomain:(NSString *)targetDomain toByte:(uint8_t *) block {

    NSUInteger index = 0;
    uint8_t response[128];
    CZLANManagerAuthentication *lanMan = [[CZLANManagerAuthentication alloc] init];
    
    const uint8_t clientChallenge[8] = {
        arc4random() % 16, arc4random() % 16,
        arc4random() % 16, arc4random() % 16,
        arc4random() % 16, arc4random() % 16,
        arc4random() % 16, arc4random() % 16
    };
    
    memset(response, 0, 128);
    
    
    UInt16 targetDomainLength = [targetDomain length] * 2;
    UInt16 targetInfoLength = targetDomainLength + 2 + 2;
    uint8_t targetInfo[targetInfoLength];
    memset(targetInfo, 0, targetInfoLength);
    
    
    uint8_t targetDomainName[targetDomainLength + 2];
    memset(targetDomainName, 0, targetDomainLength + 2);
    [targetDomain getCString:(char*)targetDomainName maxLength:targetDomainLength + 2 encoding:kUnicodeEncoding];
    const uint8_t targetDomainHeader[2] = {0x02, 0x00};
    
    memcpy(targetInfo, targetDomainHeader, 2);
    memcpy(targetInfo + 2, &targetDomainLength, 2);
    memcpy(targetInfo + 4, targetDomainName, targetDomainLength);
    
    
    NSUInteger length = [lanMan encryptPassword:password
                                         domain:aDomain
                                           user:aName
                                  lmv2Challenge:challenge
                            lmv2ChallengeLength:challengeLength
                                      challenge:clientChallenge
                                     targetInfo:targetInfo
                               targetInfoLength:targetInfoLength
                                     toResponse:response];
    memcpy((char*)block+index, response, length);
    index = length;
    
    return index;
}

- (CZSMBRequestPacket *)newRequestData:(CZSMBConnection *)connection {
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_SESSION_SETUP_ANDX];
    [reqPacket setCommonFlag2:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    //This field MUST be either the command code for the next SMB command in the packet or 0xFF.
    //reqPacket.andXCommand = kSMB_COM_TREE_CONNECT_ANDX;
    reqPacket.andXCommand = 0xFF;
    ///A reserved field. This MUST be set to 0x00 when this request is sent.
    reqPacket.andXReserved = 0;
    /**
     * This field MUST be set to the offset in bytes from the start of the SMB Header to the start of the WordCount field in the next SMB command in this packet. This field is valid only if the AndXCommand field is not set to 0xFF. If AndXCommand is 0xFF, this field MUST be ignored by the server.
     */
    reqPacket.andXOffset = 0;
    
    reqPacket.maxBufferSize = connection.serverMaxBufferSize;
    /**
     * The maximum number of pending requests supported by the client. This value MUST be less than or equal to the MaxMpxCount field value provided by the server in the SMB_COM_NEGOTIATE Response.
     */
    reqPacket.maxMpxCount = connection.maxMpxCount;
    
    /**
     * The number of this VC (virtual circuit) between the client and the server. This field SHOULD be set to a value of 0x0000 for the first virtual circuit between the client and the server and it SHOULD be set to a unique nonzero value for each additional virtual circuit.
     */
    reqPacket.vcNumber = 0;
    reqPacket.sessionKey = connection.sessionKey;
    /** Referenced in  http://www.ubiqx.org/cifs/SMB.html#SMB.6.3.2
     *
     * Basically, though, if Extended Security has been negotiated then the Lengths field is a single uint16_t, known as SecurityBlobLength in the SNIA doc. If Extended Security is not in use then there will be two uint16_t fields identified by the excessively long names:
     
     CaseInsensitivePasswordLength and
     CaseSensitivePasswordLength.
     */
    if (connection.isExtendedSecurity) {
        reqPacket.securityBlobLength = 0;//TO DO: need to set the real number
        ///Words (24 bytes) = 24/2 words
        [reqPacket setParameterCount:24/2];
    }else {
        ///if we choose LM or NTLM, here is the hash of response length. (LM/NTLM is case insensitive)
        if (connection.isUnicodeSupported) {
            reqPacket.caseInsensitivePasswordLength = 24;
            reqPacket.caseSensitivePasswordLength = 72;///Called UnicodePasswordLen in MS-CIFS.pdf
        }else {
            reqPacket.caseInsensitivePasswordLength = 24;
            reqPacket.caseSensitivePasswordLength = 0;///Called UnicodePasswordLen in MS-CIFS.pdf
        }
        
        ///Words (26 bytes) = 26/2 words
        [reqPacket setParameterCount:26/2];
    }
    reqPacket.reserved = 0;
    ///If set, indicates that the server supports the 32-bit NT_STATUS error codes.
    uint32_t capabilities = kCAP_STATUS32;
    if (connection.isExtendedSecurity) {
        ///Set to indicate that Extended Security exchanges are supported.
        capabilities |= kCAP_EXTENDED_SECURITY;
    }
    /**
     * The client supports SMB_COM_READ_RAW and SMB_COM_WRITE_RAW requests. Raw mode is not supported over connectionless transports. Remember that we already have choose TCP/IP.
     
     More:
     Windows Server 2008, Windows Server 2008 R2, and Windows Server 2012 do not support SMB_COM_READ_RAW or SMB_COM_WRITE_RAW and disconnect the client by closing the underlying transport connection if either command is received from the client.
     */
    //    capabilities |= kCAP_RAW_MODE;
    ///The client supports UTF-16LE Unicode strings.
    if (connection.isUnicodeSupported) {
        capabilities |= kCAP_UNICODE;
        capabilities |= kCAP_NT_SMBS;
    }
    
    reqPacket.capabilities = capabilities;
    
    ///Construct data block
    uint8_t dataBlock[kMaxDataBlockSize];
    memset(dataBlock, 0, kMaxDataBlockSize);
    ///SecurityBlob field MUST be the authentication token sent to the server.
    uint16_t length;
    if (connection.isUnicodeSupported) {
        NSString *targetDomain = nil;
        if (connection.primaryDomain && [connection.primaryDomain isEqualToString:connection.tcpTransport.domainName]) {
            targetDomain = connection.primaryDomain;
        } else {
            targetDomain = connection.tcpTransport.serverName;
        }
        
        length = [self constructDataBlockByUnicodePassword:connection.tcpTransport.password
                                                  userName:connection.tcpTransport.userName
                                                    domain:connection.tcpTransport.domainName
                                                 challenge:[connection.serverChallenge bytes]
                                           challengeLength:connection.serverChallenge.length
                                              targetDomain:targetDomain
                                                    toByte:dataBlock+24];
        reqPacket.caseSensitivePasswordLength = length;
        
        length += [self constructAccountBlockByUserName:connection.tcpTransport.userName
                                                 domain:connection.tcpTransport.domainName
                                                 toByte:dataBlock+24+length];
        length += 24;
    }else {
        
        length = [super constructDataBlockByOEMPassword:connection.tcpTransport.password
                                              userName:connection.tcpTransport.userName
                                                domain:connection.tcpTransport.domainName
                                             challenge:[connection.serverChallenge bytes]
                                                toByte:dataBlock];
    }
    
    [reqPacket setDataBlock:dataBlock byteCount:length];
    
    //    reqPacket.andXOffset = [reqPacket.packetData length];
    
    return reqPacket;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (connection.isAuthenticated) {
        [self setState:CZSMBEventsStateSuccess];
        return;
    }
    
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [self newRequestData:connection];
    
    uint8_t byte[kMaxDataBlockSize];
    memset(byte, 0, kMaxDataBlockSize);
    NSString * share = connection.tcpTransport.shareName;
    NSString * service = kServiceFileSystem;
    if ([share length] > 4) {
        NSString * str = [share substringFromIndex:[share length]-4];
        if ([str isEqualToString:@"IPC$"]) {
            service = kServiceNamedPipe;
        }
    }
    
    uint16_t index = 0;
    if (connection.isShareLevelAccessControl) {
        ///Compute hash response here.
        uint8_t response[24];
        CZLANManagerAuthentication *lanMan = [[CZLANManagerAuthentication alloc] init];
        [lanMan encryptPassword:connection.tcpTransport.password
                  ntlmChallenge:[connection.serverChallenge bytes]
                     toResponse:response];
        memcpy((char*)byte+index, response, 24);
        index = 24;
        ///pad
        index += 1;
        
    }else {
        ///The Password is the null padding byte, so, let's move to next field.
        index = 1;
    }
    
    ///Set share path
    if (connection.isUnicodeSupported) {
        [share getCString:(char*)byte+index
                maxLength:kMaxDataBlockSize-index
                 encoding:kUnicodeEncoding];
        index+=([share length]+1)*2;
    }else {
        [share getCString:(char*)byte+index
                maxLength:kMaxDataBlockSize-index
                 encoding:NSASCIIStringEncoding];
        index+=[share length]+1;
    }
    [service getCString:(char*)byte+index
              maxLength:kMaxDataBlockSize-index
               encoding:NSASCIIStringEncoding];
    index+=[service length]+1;
    
    reqMsg[3] = [reqPacket.packetData length] + 11 + index;
    //[reqPacket appendBytes:reqMsg length:11];
    //[reqPacket appendBytes:byte length:index];
    
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
    
}

@end
