//
//  CZSMBNegotiate.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNegotiate.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBSession.h"
#import "CZSMBErrorHandler.h"

@interface CZSMBNegotiate() {
}

@end

@implementation CZSMBNegotiate
@synthesize parameters = _parameters;

- (int)eventFlag {
    return kEventFlagNegotiate;
}

- (void)startRequest:(CZSMBConnection*) connection {
    if (connection.isNegotiated) {
        [self setState:CZSMBEventsStateSuccess];
        return;
    }
    
    self.connection = connection;
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_NEGOTIATE];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    [reqPacket setParameters:0 wordCount:0];
    //Set dialect into data block
    uint8_t dialect[12] = kDialectString;
    assert(strlen(kDialectString)<13);
    [reqPacket setDataBlock:dialect byteCount:12];
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

#pragma mark - Properties Methods

- (u_char)securityMode {
    return *((const u_char*)self.parameters+kSMBResponseParametersSecurityMode);
}

- (uint16_t)maxMpxCount {
    uint16_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersMaxMpxCount, sizeof(value));
    return value;
}

- (uint16_t)maxNumberVCs {
    uint16_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersMaxNumberVcs, sizeof(value));
    return value;
}

- (uint32_t)maxBufferSize {
    uint32_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersMaxBufferSize, sizeof(value));
    return value;
}

- (uint32_t)maxRawSize {
    uint32_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersMaxRawSize, sizeof(value));
    return value;
}

- (uint32_t)sessionKey {
    uint32_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersSessionKey, sizeof(value));
    return value;
}

- (uint32_t)capabilities {
    uint32_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersCapabilities, sizeof(value));
    return value;
}

- (int16_t)serverTimeZone {
    uint16_t value;
    memcpy(&value, self.parameters+kSMBResponseParametersServerTimeZone, sizeof(value));
    return value;
}

- (u_char)encryptionKeyLength {
    return *((const u_char *)self.parameters+kSMBResponseParametersEncryptionKeyLength);
}

- (uint16_t)byteCount {
    return *((const u_char *)self.parameters+kSMBResponseParametersByteCountLength);
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    
    int status = *respPacket.getSMBStatus;

    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
        self.connection.isNegotiated = YES;
        CZLog(@"Negotiat success");
    }else {
        self.connection.isNegotiated = NO;
        CZPrintError(status);
        [self setState:CZSMBEventsStateError];
        return;
    }
    int length = [respPacket getParameters:NULL];
    ///We have selected the NT LAN Manager dialect, then WordCount MUST be 0x11.
    assert(length == kSMBResponseParametersLength);
    
    [respPacket getParametersNoCopy:&_parameters];
    
    if (*[respPacket getSMBCommand] != kSMB_COM_NEGOTIATE) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    assert(self.encryptionKeyLength == 0 || self.encryptionKeyLength == 8);
    self.connection.serverMaxBufferSize = self.maxBufferSize;
    uint16_t maxMpxCount = MAX(self.maxMpxCount, 1);
    self.connection.maxMpxCount = maxMpxCount;
    self.connection.sessionKey = self.sessionKey;
    u_char securityMode = self.securityMode;
    
    self.connection.isShareLevelAccessControl = !(securityMode & kNEGOTIATE_USER_SECURITY);
    self.connection.isServerChallengeResponse = ((securityMode & kNEGOTIATE_ENCRYPT_PASSWORDS) &&
                                                 !(securityMode & kNEGOTIATE_SECURITY_SIGNATURES_ENABLED));
    if (securityMode & kNEGOTIATE_SECURITY_SIGNATURES_REQUIRED) {
        self.connection.isSigningMandatory = YES;
    } else {
        self.connection.isSigningMandatory = NO;
    }
    
    self.connection.serverCapabilities = self.capabilities;
    ///if response has the CAP_EXTENDED_SECURITY flag set (which indicates that the server supports extended security), then the client MUST construct an SMB_COM_SESSION_SETUP_ANDX request, as specified in section 2.2.4.6.1 of MS-SMB.pdf.
    assert(self.connection.isExtendedSecurity == NO);
    if (self.encryptionKeyLength == 8) {
        ///Set server challenge
        NSData *byts = NULL;
        [respPacket getDataBlockNoCopy:&byts];
        NSData *challenge = [byts subdataWithRange:NSMakeRange(0, self.encryptionKeyLength)];
        self.connection.serverChallenge = challenge;
        
        if (self.byteCount > self.encryptionKeyLength) {
            NSInteger additionalAttributeLength = self.byteCount - self.encryptionKeyLength;
            NSData *primaryDomainBuff = [byts subdataWithRange:NSMakeRange(self.encryptionKeyLength, additionalAttributeLength)];
            NSString *primaryDomainName = [[NSString alloc] initWithData:primaryDomainBuff encoding:kUnicodeEncoding];
            CZLog(@"Domain = %@", primaryDomainName);
            
            NSString *tempString = [[NSString alloc] initWithUTF8String:[primaryDomainName UTF8String]];
            self.connection.primaryDomain = tempString;
        }
    }
    
    maxMpxCount = MIN(maxMpxCount, kMaxMultiplexCount);
    self.connection.maxAllowMpxCount = maxMpxCount;
//    CZLog(@"Max VC number:%d",self.maxNumberVCs);
    CZLog(@"Max Mpx count:%d",self.maxMpxCount);

    if (self.capabilities&kCAP_DFS) {
        CZLog(@"DFS");
    }

    self.connection.processID = *respPacket.getSMBPIDLow;
    self.connection.treeID = *respPacket.getSMBTID;
    self.connection.userID = *respPacket.getSMBUID;
    self.connection.serverTimeZone = self.serverTimeZone;
    
    // TODO: use time zone to calculate time
    CZLog(@"time zone: %d", self.serverTimeZone);

    if (self.connection.isSigningMandatory) {
        self.connection.isNegotiated = NO;
        self.errorCode = kErrorCodeServerIncompatible;
        [self setState:CZSMBEventsStateError];
    } else {
        [self setState:CZSMBEventsStateSuccess];
    }
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}

@end
