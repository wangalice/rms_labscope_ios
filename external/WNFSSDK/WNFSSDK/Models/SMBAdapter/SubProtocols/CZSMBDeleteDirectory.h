//
//  CZSMBDeleteDirectory.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"

@interface CZSMBDeleteDirectory : CZSMBEvents<CZSMBEventsDelegate> 

@property (nonatomic,strong) NSString *             directory;
@end
