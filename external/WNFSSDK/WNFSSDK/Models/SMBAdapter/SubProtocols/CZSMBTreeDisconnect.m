//
//  CZSMBTreeDisconnect.m
//  WNFSSDK
//
//  Created by Ralph Jin on 6/17/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZSMBTreeDisconnect.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"

@implementation CZSMBTreeDisconnect

- (int)eventFlag {
    return kEventFlagTreeDisconnect;
}

- (void)startRequest:(CZSMBConnection *)connection {
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_TREE_DISCONNECT];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];

    uint8_t byte[3];
    memset(byte, 0, 3);
    [reqPacket setDataBlock:byte byteCount:3];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

- (void)dataParse:(NSData *)data {
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;

    uint32_t status = *respPacket.getSMBStatus;
    if (![CZSMBErrorHandler isError:status]) {
        CZLog(@"TreeConnect success");
        self.connection.isShareConnected = NO;
    } else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }

    self.connection.treeID = 0;
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode)code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end

