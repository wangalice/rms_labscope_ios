//
//  CZSMBTransaction2.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTransaction2.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZSMBErrorHandler.h"
#import "CZFileDescriptor.h"

@implementation CZSMBTransaction2


- (int)eventFlag {
    if (self.subCommand == nil) {
        return kEventFlagTransaction2;
    }
    
    switch ([self.subCommand commandFlag]) {
        case kTRANS2_FIND_FIRST2:
            return kEventFlagTrans2FindFirst2;
        case kTRANS2_FIND_NEXT2:
            return kEventFlagTrans2FindNext2;
        case kTRANS2_QUERY_PATH_INFO:
            return kEventFlagTrans2QueryPathInfo;
        case kTRANS2_QUERY_FS_INFO:
            return kEventFlagTrans2QueryFSInfo;
        case kTRANS2_SET_FILE_INFO:
            return kEventFlagTrans2SetFileInfo;
        default:
            break;
    }
    return kEventFlagTransaction2;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.subCommand == nil) {
        self.errorCode = kErrorCodeSubCommandInvalid;
        [self setState:CZSMBEventsStateError];
    }
    
    if ([CZSMBErrorHandler isError:[self.subCommand validCommand]]) {
        self.errorCode = [self.subCommand validCommand];
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_TRANSACTION2];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    [self setParametersFor:reqPacket];
    int primarySize = kSMBRequestParametersSetup + sizeof(uint16_t);
    primarySize = primarySize/2+primarySize%2;
    [reqPacket setParameterCount:primarySize];
    
    [self.subCommand setRequestPacket:reqPacket];
    
    ///set parameter offset
    uint16_t offset = [self.subCommand parameterOffset];
    [reqPacket setParameterWithUint16:&offset
                               offset:kSMBRequestParametersParameterOffset];
    
    offset = [self.subCommand dataOffset];
    ///set data offset
    [reqPacket setParameterWithUint16:&offset
                               offset:kSMBRequestParametersDataOffset];
    
    uint16_t dataCount = [self.subCommand dataCount];
    [reqPacket setParameterWithUint16:&dataCount
                               offset:kSMBRequestParametersDataCount];
    
    uint16_t tdataCount = dataCount;
    [reqPacket setParameterWithUint16:&tdataCount
                               offset:kSMBRequestParametersTotalDataCount];

    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

#pragma mark - Internal Methods

- (void)setParametersFor:(CZSMBRequestPacket*) reqPacket{
/**
 * The value of Words.SetupCount plus 14 (0x0E). This value represents the total number of SMB parameter words and MUST be greater than or equal to 14 (0x0E).
 */
    NSUInteger paracount = [self.subCommand parameterCount];
    uint16_t totalParameterCount = paracount;
    [reqPacket setParameterWithUint16:&totalParameterCount
                     offset:kSMBRequestParametersTotalParameterCount];
    
/**
 * MaxParameterCount (2 bytes): The maximum number of parameter bytes that the client will accept in the transaction reply. The server MUST NOT return more than this number of parameter bytes.
 */
    uint16_t maxParameterCount = kMaxAllowParametersBytes;
    [reqPacket setParameterWithUint16:&maxParameterCount
                               offset:kSMBRequestParametersMaxParameterCount];
/**
 * MaxDataCount (2 bytes): The maximum number of data bytes that the client will accept in the transaction reply. The server MUST NOT return more than this number of data bytes.
 */
    uint16_t maxDataCount = self.connection.serverMaxBufferSize - 0x80;
    [reqPacket setParameterWithUint16:&maxDataCount
                               offset:kSMBRequestParametersMaxDataCount];
/**
 * MaxSetupCount (1 byte): The maximum number of setup bytes that the client will accept in the transaction reply. The server MUST NOT return more than this number of setup bytes.
 */
    u_char maxSetupCount = 0xFF;
    [reqPacket setParameterWithUint8:&maxSetupCount
                              offset:kSMBRequestParametersMaxSetupCount];
/**
 * Reserved1 (1 byte): A padding byte. This field MUST be zero. Existing CIFS implementations MAY combine this field with MaxSetupCount to form a USHORT. If MaxSetupCount is defined as a USHORT, the high order byte MUST be 0x00. 
 */
    u_char reserved1 = 0x00;
    [reqPacket setParameterWithUint8:&reserved1
                              offset:kSMBRequestParametersReserved1];
/**
 * Flags (2 bytes): A set of bit flags that alter the behavior of the requested operation. Unused bit fields MUST be set to zero by the client sending the request, and MUST be ignored by the server receiving the request. The client MAY set either or both of the following bit flags:
 
 DISCONNECT_TID 0x0001
 If set, following the completion of the operation the server MUST disconnect the tree connect associated with the tree identifier (TID) field received in the SMB Header (section 2.2.3.1) of this request. The client SHOULD NOT send a subsequent SMB_COM_TREE_DISCONNECT for this tree connect.
 ￼
 NO_RESPONSE 0x0002
 This is a one-way transaction. The server MUST attempt to complete the transaction, but SHOULD NOT send a response to the client.<65>
 */
    ///Unused bit fields MUST be set to zero
    uint16_t flag = 0x0000;
    [reqPacket setParameterWithUint16:&flag
                               offset:kSMBRequestParametersFlags];
/**
 * Timeout (4 bytes): The number of milliseconds that the server waits for completion of the transaction before generating a time-out. A value of 0x00000000 indicates that the operation is not blocked.
 */
    uint32_t timeout = 0x00000000;
    [reqPacket setParameterWithUint32:&timeout
                               offset:kSMBRequestParametersTimeout];
/**
 * Reserved2 (2 bytes): Reserved. This field MUST be 0x0000 in the client request. The server MUST ignore the contents of this field.
 */
    uint16_t reserved2 = 0x0000;
    [reqPacket setParameterWithUint16:&reserved2
                               offset:kSMBRequestParametersReserved2];
/**
 * ParameterCount (2 bytes): The number of transaction parameter bytes being sent in this SMB message. If the transaction fits within a single SMB_COM_TRANSACTION2 request, then this value MUST be equal to TotalParameterCount. Otherwise, the sum of the ParameterCount values in the primary and secondary transaction request messages MUST be equal to the smallest TotalParameterCount value reported to the server. If the value of this field is less than the value of TotalParameterCount, then at least one SMB_COM_TRANSACTION2_SECONDARY message MUST be used to transfer the remaining parameter bytes. The ParameterCount field MUST be used to determine the number of transaction parameter bytes contained within the SMB_COM_TRANSACTION2 message.
 */
    uint16_t parameterCount = totalParameterCount;
    [reqPacket setParameterWithUint16:&parameterCount
                               offset:kSMBRequestParametersParameterCount];
/**
 * ParameterOffset (2 bytes): The offset, in bytes, from the start of the SMB_Header to the transaction parameter bytes. This MUST be the number of bytes from the start of the SMB message to the start of the SMB_Data.Bytes.Parameters field. Server implementations MUST use this value to locate the transaction parameter block within the SMB message. If ParameterCount is zero, the client/server MAY set this field to zero.
 */
    uint16_t parameterOffset = 0;///set this value in later
    [reqPacket setParameterWithUint16:&parameterOffset
                               offset:kSMBRequestParametersParameterOffset];
/**
 * DataCount (2 bytes): The number of transaction data bytes being sent in this SMB message. If the transaction fits within a single SMB_COM_TRANSACTION2 request, then this value MUST be equal to TotalDataCount. Otherwise, the sum of the DataCount values in the primary and secondary transaction request messages MUST be equal to the smallest TotalDataCount value reported to the server. If the value of this field is less than the value of TotalDataCount, then at least one SMB_COM_TRANSACTION2_SECONDARY message MUST be used to transfer the remaining data bytes.
 */
    uint16_t dataCount = 0;
    [reqPacket setParameterWithUint16:&dataCount
                               offset:kSMBRequestParametersDataCount];
/**
 * DataOffset (2 bytes): The offset, in bytes, from the start of the SMB Header (section 2.2.3.1) to the transaction data bytes. This MUST be the number of bytes from the start of the SMB message to the start of the SMB_Data.Bytes.Data field. Server implementations MUST use this value to locate the transaction data block within the SMB message. If DataCount is zero, the client/server MAY set this field to zero.
 */
    uint16_t dataOffset = 0;///set this value in later
    [reqPacket setParameterWithUint16:&dataOffset
                               offset:kSMBRequestParametersDataOffset];
    
/**
 * SetupCount (1 byte): The number of setup words that are included in the transaction request.
 */
    u_char setupCount = 1;
    [reqPacket setParameterWithUint8:&setupCount
                              offset:kSMBRequestParametersSetupCount];
/**
 * Reserved3 (1 byte): A padding byte. This field MUST be 0x00. Existing CIFS implementations MAY combine this field with SetupCount to form a USHORT. If SetupCount is defined as a USHORT, the high order byte MUST be0x00.
 */
    u_char reserved3 = 0x00;
    [reqPacket setParameterWithUint8:&reserved3
                              offset:kSMBRequestParametersReserved3];
/**
 * Setup (variable): An array of two-byte words that provide transaction context to the server. The size and content of the array are specific to individual subcommands.SMB_COM_TRANSACTION2 messages MAY exceed the maximum size of a single SMB message (as determined by the value of the MaxBufferSize session parameter). If this is the case, then the client MUST use one or more SMB_COM_TRANSACTION2_SECONDARY messages to transfer transaction Data and Parameter bytes that did not fit in the initial message.
 */
    uint16_t setupCommand = [self.subCommand commandFlag];
    [reqPacket setParameterWithUint16:&setupCommand
                               offset:kSMBRequestParametersSetup];
    
}

- (uint16_t)getTotalParameterCountFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersTotalParameterCount, sizeof(value));
    return value;
}

- (uint16_t)getParameterCountFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersParameterCount, sizeof(value));
    return value;
}

- (uint16_t)getTotalDataCountFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersTotalDataCount, sizeof(value));
    return value;
}

- (uint16_t)getDataCountFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersDataCount, sizeof(value));
    return value;
}

- (uint16_t)getDataOffsetFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersDataOffset, sizeof(value));
    return value;
}

- (uint16_t)getParametersOffsetFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersParameterOffset, sizeof(value));
    return value;
}


- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_TRANSACTION2) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    ///This value represents the total number of SMB parameter words and MUST be greater than or equal to 10 (0x0A).
    const void *parameter = NULL;
    int parameterLength = [respPacket getParametersNoCopy:&parameter];
//    assert(parameterLength >= 10);
    if (parameterLength < 10) {
        CZPrintError(status);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }

//    assert([self getTotalParameterCountFrom:parameter] == [self getParameterCountFrom:parameter]);
    
    uint32_t totalDataCount = [self getTotalDataCountFrom:parameter];
    uint16_t dataCount = [self getDataCountFrom:parameter];
    assert(totalDataCount == dataCount);
    int32_t offset = [self getParametersOffsetFrom:parameter];
    
    ///compute the sub parameters offset in datablock
    offset = offset - kSMBHeaderLength - kSMBMinimumDataBlockLength - kSMBMinimumParameterLength - parameterLength*2;
    if (offset < 0) {
        CZPrintError(status);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    NSData *dataBlock = NULL;
    [respPacket getDataBlockNoCopy:&dataBlock];
    
    ///The data offset is from the start of the SMB message to the start of the SMB_Data.Bytes.Data field.
    uint16_t dataOffset = [self getDataOffsetFrom:parameter];
    uint16_t parameterHeaderLength = kSMBHeaderLength + kSMBMinimumDataBlockLength + kSMBMinimumParameterLength + parameterLength*2;
    
    NSInteger dataRemainLength = ([dataBlock length] >= offset) ? (dataBlock.length - offset) : 0;
    
    if (dataOffset > parameterHeaderLength) {
        dataOffset -= parameterHeaderLength;
    }

    if (dataOffset >= offset) {
        assert(dataBlock.length >= dataOffset);
        [self.subCommand setResponseData:[dataBlock subdataWithRange:NSMakeRange(offset, dataRemainLength)]
                           subDataOffset:dataOffset-offset];
    }
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}

@end
