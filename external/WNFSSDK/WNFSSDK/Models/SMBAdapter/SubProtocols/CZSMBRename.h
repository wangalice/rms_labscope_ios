//
//  CZSMBRename.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"

@interface CZSMBRename : CZSMBEvents<CZSMBEventsDelegate>

@property (nonatomic,strong) NSString *             fileName;
@property (nonatomic,strong) NSString *             oldFileName;
@property (nonatomic,readwrite) uint16_t              searchAttributes;
@end
