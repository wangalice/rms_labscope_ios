//
//  CZSMBDelete.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"

@interface CZSMBDelete : CZSMBEvents<CZSMBEventsDelegate> 

@property (nonatomic,strong) NSString *             fileName;
@property (nonatomic,readwrite) uint16_t              searchAttributes;
@end
