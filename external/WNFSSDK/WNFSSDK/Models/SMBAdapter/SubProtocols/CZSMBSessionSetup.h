//
//  CZSMBSessionSetup.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBEvents.h"
#import "CZSMBErrorHandler.h"

#define kSMBParametersAndXCommand           (0)
#define kSMBParametersAndXReserved          (1)
#define kSMBParametersAndXOffset            (2)
/**
 * A 16-bit field. The two lowest-order bits have been defined:
 
 SMB_SETUP_GUEST    If clear (0), the user successfully authenticated and is logged in.
 if set (1), authentication failed but the server has granted guest access. The user is logged in as Guest.
 
 SMB_SETUP_USE_LANMAN_KEY   If clear, the NTLM user session key will be used for message signing (if enabled).
 If set, the LM session key will be used for message signing.
 */
#define kSMBParametersAction                (4)

#define kSMB_SETUP_GUEST                    (0x0001)
#define kSMB_SETUP_USE_LANMAN_KEY           (0x0002)

@class CZSMBRequestPacket;

@interface CZSMBSessionSetup : CZSMBEvents<CZSMBEventsDelegate>
- (int)constructDataBlockByOEMPassword:(NSString*) password userName:(NSString*) aName domain:(NSString*) aDomain challenge:(const uint8_t*)challenge toByte:(uint8_t*) block;
@end
