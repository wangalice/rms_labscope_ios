//
//  CZSMBEvents.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBEventsDelegate.h"
#import "CZSMBErrorHandler.h"
#import "CZSMBConfiguration.h"

/**
 *  We only have to implements the "NT LM 0.12" dialect
 */
#define kDialectString                      "\x02NT LM 0.12"

#define kEventFlagNegotiate                 (0x001)
#define kEventFlagSessionSetup              (0x002)
#define kEventFlagTreeConnect               (0x004)
#define kEventFlagTransaction               (0x008)
#define kEventFlagTransaction2              (0x010)
#define kEventFlagTrans2FindFirst2          (0x020)
#define kEventFlagTrans2FindNext2           (0x040)
#define kEventFlagTrans2QueryPathInfo       (0x080)
#define kEventFlagNTCreate                  (0x100)
#define kEventFlagRead                      (0x200)
#define kEventFlagWrite                     (0x400)
#define kEventFlagDelete                    (0x800)
#define kEventFlagDeleteDirectory           (0x1000)
#define kEventFlagClose                     (0x2000)
#define kEventFlagFindClose2                (0x4000)
#define kEventFlagRename                    (0x8000)
#define kEventFlagTrans2QueryFSInfo         (0x10000)
#define kEventFlagTrans2SetFileInfo         (0x20000)
#define kEventFlagTransactionEnumShare      (0x40000)
#define kEventFlagTransactionEnumServer     (0x80000)
#define kEventFlagTransactionEnumShareRPC   (0x100000)
#define kEventFlagKeepLoop                  (0x200000)
#define kEventFlagTreeDisconnect            (0x400000)

typedef NS_ENUM(NSUInteger, CZSMBEventsState)
{
    CZSMBEventsStateError = -1,
    CZSMBEventsStateInit = 0,
    CZSMBEventsStateSuccess = 1,
};

@class CZSMBEvents;

@protocol CZSMBEventsObserver <NSObject>
@optional
- (void)smbEventsStateChanged:(CZSMBEvents *)smbEvents;
@end

@interface CZSMBEvents : NSObject

@property (nonatomic,readwrite) BOOL                 isResponse;
@property (nonatomic,readwrite) CZSMBEventsState     state;
@property (nonatomic,readwrite) CZSMBErrorCode       errorCode;
@property (nonatomic,strong)    CZSMBConnection      *connection;
@property (nonatomic,readwrite) uint16_t             multiplexID;
@property (atomic, weak) id<CZSMBEventsObserver>     observer;

- (void)setTimeout;
- (void)clearTimeout;
@end

