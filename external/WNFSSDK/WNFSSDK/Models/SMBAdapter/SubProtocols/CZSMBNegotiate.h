//
//  CZSMBNegotiate.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "CZSMBEvents.h"


///  uint16_t DialectIndex;        /* Selected dialect index    */
#define kSMBResponseParametersDialectIndex			(0)
///  uchar  SecurityMode;        /* Server security flags     */
#define kSMBResponseParametersSecurityMode			(2)
///  uint16_t MaxMpxCount;         /* Maximum Multiplex Count   */
#define kSMBResponseParametersMaxMpxCount			(3)
///  uint16_t MaxNumberVCs;        /* Maximum Virtual Circuits  */
#define kSMBResponseParametersMaxNumberVcs			(5)
///  uint32_t  MaxBufferSize;       /* Maximum SMB message size  */
#define kSMBResponseParametersMaxBufferSize         (7)
///  uint32_t  MaxRawSize;          /* Obsolete                  */
#define kSMBResponseParametersMaxRawSize			(11)
///  uint32_t  SessionKey;          /* Unique session ID         */
#define kSMBResponseParametersSessionKey			(15)
///  uint32_t  Capabilities;        /* Server capabilities flags */
#define kSMBResponseParametersCapabilities			(19)
///  uint32_t  SystemTimeLow;       /* Server time; low bytes    */
#define kSMBResponseParametersSystemTimeLow         (23)
///  uint32_t  SystemTimeHigh;      /* Server time; high bytes   */
#define kSMBResponseParametersSystemTimeHigh		(27)
///  uint16_t  ServerTimeZone;      /* Minutes from UTC; signed  */
#define kSMBResponseParametersServerTimeZone		(31)
///  uint8_t  EncryptionKeyLength; /* 0 or 8                    */
#define kSMBResponseParametersEncryptionKeyLength	(33)
///  uint8_t  EncryptionKeyLength; /* 0 or 8                    */
#define kSMBResponseParametersByteCountLength	    (34)
///  uint8_t WordCount;              /* Always 17 for this struct */
#define kSMBResponseParametersLength                (0x11)

/// security mode
#define kNEGOTIATE_USER_SECURITY (0x01)
#define kNEGOTIATE_ENCRYPT_PASSWORDS (0x02)
#define kNEGOTIATE_SECURITY_SIGNATURES_ENABLED (0x04)
#define kNEGOTIATE_SECURITY_SIGNATURES_REQUIRED (0x08)

@interface CZSMBNegotiate : CZSMBEvents<CZSMBEventsDelegate>

@property (nonatomic,readonly) const void   *parameters;
@property (nonatomic,readonly) u_char       securityMode;
@property (nonatomic,readonly) uint16_t      maxMpxCount;
@property (nonatomic,readonly) uint16_t      maxNumberVCs;
@property (nonatomic,readonly) uint32_t     maxBufferSize;
@property (nonatomic,readonly) uint32_t     maxRawSize;
@property (nonatomic,readonly) uint32_t     sessionKey;
@property (nonatomic,readonly) uint32_t     capabilities;
@property (nonatomic,readonly) uint32_t     systemTimeLow;
@property (nonatomic,readonly) uint32_t     systemTimeHigh;
@property (nonatomic,readonly) int16_t      serverTimeZone;
@property (nonatomic,readonly) u_char       encryptionKeyLength;
@property (nonatomic,readonly) uint16_t      byteCount;
@end
