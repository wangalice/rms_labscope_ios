//
//  CZSMBWrite.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBWrite.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZFileDescriptor.h"

#define kSMBRequestParametersFID                            (4)
#define kSMBRequestParametersOffset                         (6)
#define kSMBRequestParametersWriteMode                      (14)
#define kSMBRequestParametersRemaining                      (16)
#define kSMBRequestParametersDataLengthHigh                 (18)
#define kSMBRequestParametersDataLength                     (20)
#define kSMBRequestParametersDataOffset                     (22)
#define kSMBRequestParametersOffsetHigh                     (24)

@implementation CZSMBWrite

- (int)eventFlag {
    return kEventFlagWrite;
}

- (void)startRequest:(CZSMBConnection *)connection {
    self.fileDescriptor.writeBytes = MIN(self.fileDescriptor.writeBytes, self.fileDescriptor.dataPtr.length);
    
    if (self.fileDescriptor.dataPtr == NULL
        || self.fileDescriptor.FID < 1
        || self.fileDescriptor.writeBytes < 1) {
        self.errorCode = kErrorCodeFileDescriptorError;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_WRITE_ANDX];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    reqPacket.andXCommand = 0xFF;
    uint16_t fid = self.fileDescriptor.FID;
    uint32_t offset = self.fileDescriptor.offset&0xFFFFFFFF;
    [reqPacket setParameterWithUint16:&fid
                               offset:kSMBRequestParametersFID];
    
    [reqPacket setParameterWithUint32:&offset
                               offset:kSMBRequestParametersOffset];
    
    uint16_t writeCountHigh = 0;
    [reqPacket setParameterWithUint16:&writeCountHigh
                               offset:kSMBRequestParametersDataLengthHigh];
    uint16_t maxWriteCount;
    if (connection.serverCapabilities&kCAP_LARGE_WRITEX) {
        maxWriteCount = MIN(kMaxWriteDataBlockSize, self.fileDescriptor.writeBytes);
    }else {
        maxWriteCount = MIN(connection.serverMaxBufferSize,self.fileDescriptor.writeBytes);
    }
    [reqPacket setParameterWithUint16:&maxWriteCount
                               offset:kSMBRequestParametersDataLength];
    
    uint16_t dataOffset = kSMBHeaderLength + kSMBMinimumDataBlockLength + kSMBMinimumParameterLength;
    ///This field MUST be either 0x0C or 0x0E.
    if (connection.serverCapabilities&kCAP_LARGE_WRITEX) {
        uint32_t offsetHigh = (self.fileDescriptor.offset&0xFFFFFFFF00000000) >> 32;
        [reqPacket setParameterWithUint32:&offsetHigh
                                  offset:kSMBRequestParametersOffsetHigh];
        [reqPacket setParameterCount:0x0E];
        dataOffset += (0x0E*2);
    }else {
        [reqPacket setParameterCount:0x0C];
        dataOffset += (0x0C*2);
    }
    ///pad byte
    dataOffset += 1;
    [reqPacket setParameterWithUint16:&dataOffset
                               offset:kSMBRequestParametersDataOffset];
    ///The value of SMB_Data.ByteCount is equal to 1 + SMB_Parameters.Words.DataLength. The additional 1 byte is to account for the SMB_Data.Bytes.Pad byte.
    size_t byteCount = maxWriteCount+1;
    uint8_t * dataBlock = malloc(byteCount);
    memcpy(dataBlock+1, [self.fileDescriptor.dataPtr bytes], maxWriteCount);
    [reqPacket setDataBlock:dataBlock
                  byteCount:byteCount];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
    free(dataBlock);
}


- (uint16_t)getWriteCountFrom:(const void *)parameter {
    uint16_t value;
    memcpy(&value, parameter+4, sizeof(value));
    return value;
}


- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_WRITE_ANDX) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    const void *parameter = NULL;
    u_char parameterLength = [respPacket getParametersNoCopy:&parameter];
    assert(parameter != NULL);
    if (parameterLength == 0) {
        self.fileDescriptor.writeBytes = 0;
    }else {
        self.fileDescriptor.writeBytes = [self getWriteCountFrom:parameter];
    }
    self.fileDescriptor.endOfFile += self.fileDescriptor.writeBytes;
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}

@end
