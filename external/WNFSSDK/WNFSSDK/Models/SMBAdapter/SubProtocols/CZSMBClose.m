//
//  CZSMBClose.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBClose.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZFileDescriptor.h"

@interface CZSMBClose() {
    
}

@end

@implementation CZSMBClose


- (int)eventFlag {
    return kEventFlagClose;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.fileDescriptor.FID < 1) {
        self.errorCode = kErrorCodeFileDescriptorError;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_CLOSE];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    uint16_t fid = self.fileDescriptor.FID;
    [reqPacket setParameterWithUint16:&fid
                               offset:0];
    
    ///This field MUST be 0x03.
    [reqPacket setParameterCount:0x03];
    [reqPacket setDataBlock:NULL
                  byteCount:0];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}


- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_CLOSE) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
