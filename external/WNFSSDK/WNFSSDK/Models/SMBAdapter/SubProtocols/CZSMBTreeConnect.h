//
//  CZSMBTreeConnect.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBEvents.h"

#define kSMBParametersFlags                 (4)
#define kSMBParametersPasswordLength        (6)

/**
 * OptionalSupport (2 bytes): A 16-bit field. The following OptionalSupport field flags are defined. Any combination of the following flags MUST be supported. All undefined values are considered reserved. The server SHOULD set them to 0, and the client MUST ignore them.
 */
#define kSMBParametersOptionalSupport       (4)

#define kSMB_SUPPORT_SEARCH_BITS            (0x0001)
#define kSMB_SHARE_IS_IN_DFS                (0x0002)
@interface CZSMBTreeConnect : CZSMBEvents<CZSMBEventsDelegate>

@end
