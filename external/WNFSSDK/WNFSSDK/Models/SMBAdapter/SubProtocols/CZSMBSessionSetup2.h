//
//  CZSMBSessionSetup2.h
//  Matscope
//
//  Created by Mike Wang on 12/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSMBEvents.h"
#import "CZSMBSessionSetup.h"

@interface CZSMBSessionSetup2 : CZSMBSessionSetup<CZSMBEventsDelegate>
- (CZSMBRequestPacket *)newRequestData:(CZSMBConnection *)connection;
@end
