//
//  CZSMBRead.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBRead.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZFileDescriptor.h"


#define kSMBRequestParametersFID                            (4)
#define kSMBRequestParametersOffset                         (6)
#define kSMBRequestParametersMaxCountOfBytesToReturn        (10)
#define kSMBRequestParametersMinCountOfBytesToReturn        (12)
#define kSMBRequestParametersTimeoutOrMaxCountHigh          (14)
#define kSMBRequestParametersRemaining                      (18)
#define kSMBRequestParametersOffsetHigh                     (20)

#define kSMBResponseParametersAvailable                     (4)
#define kSMBResponseParametersDataCompactionMode            (6)
#define kSMBResponseParametersReserved1                     (8)
#define kSMBResponseParametersDataLength                    (10)
#define kSMBResponseParametersDataOffset                    (12)
#define kSMBResponseParametersDataLengthHigh                (14)
#define kSMBResponseParametersReserved2                     (16)

@interface CZSMBRead() {
    
}
@end

@implementation CZSMBRead


- (int)eventFlag {
    return kEventFlagRead;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.fileDescriptor.FID < 1
        || self.fileDescriptor.readBytes < 1) {
        self.errorCode = kErrorCodeFileDescriptorError;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_READ_ANDX];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    reqPacket.andXCommand = 0xFF;
    uint16_t fid = self.fileDescriptor.FID;
    uint32_t offset = self.fileDescriptor.offset&0xFFFFFFFF;
    [reqPacket setParameterWithUint16:&fid
                               offset:kSMBRequestParametersFID];
    
/**
 * Offset (4 bytes): If WordCount is 0x0A, this field represents a 32-bit offset, measured in bytes, of where the read MUST start relative to the beginning of the file. If WordCount is 0x0C, this field represents the lower 32 bits of a 64-bit offset.
 */
    [reqPacket setParameterWithUint32:&offset
                               offset:kSMBRequestParametersOffset];
    
    uint16_t maxCountLow;
    uint32_t maxCountHigh = 0;
    if (connection.serverCapabilities&kCAP_LARGE_READX) {
        uint64_t maxCount = MIN(kMaxReadDataBlockSize, self.fileDescriptor.readBytes);
        maxCountLow = maxCount&0xFFFF;
        maxCountHigh = maxCount&0xFFFF0000;
    }else {
        maxCountLow = MIN(connection.serverMaxBufferSize,self.fileDescriptor.readBytes)&0xFFFF;
        maxCountHigh = 0;
    }
    [reqPacket setParameterWithUint16:&maxCountLow
                               offset:kSMBRequestParametersMaxCountOfBytesToReturn];
    
    [reqPacket setParameterWithUint16:&maxCountLow
                               offset:kSMBRequestParametersMinCountOfBytesToReturn];
    
    [reqPacket setParameterWithUint32:&maxCountHigh
                               offset:kSMBRequestParametersTimeoutOrMaxCountHigh];
    
    if (connection.serverCapabilities&kCAP_LARGE_READX) {
        uint32_t offsetHigh = (self.fileDescriptor.offset&0xFFFFFFFF00000000) >> 32;
        [reqPacket setParameterWithUint32:&offsetHigh
                                  offset:kSMBRequestParametersOffsetHigh];
        [reqPacket setParameterCount:0x0C];
    }else {
        [reqPacket setParameterCount:0x0A];
    }
    [reqPacket setDataBlock:NULL
                  byteCount:0];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

- (uint32_t)getDataLengthFrom:(const void*) parameter {
    uint32_t dataLength = 0;
    uint16_t lengthLow;
    memcpy(&lengthLow, parameter+kSMBResponseParametersDataLength, sizeof(lengthLow));
    
    if (self.fileDescriptor.readBytes&0xFFFF0000) {
        uint16_t lengthHigh;
        memcpy(&lengthHigh, parameter+kSMBResponseParametersDataLengthHigh, sizeof(lengthHigh));
        dataLength = lengthHigh;
        dataLength = dataLength << 16;
        dataLength |= lengthLow;
    } else {
        dataLength = lengthLow;
    }
    return dataLength;
}

- (uint16_t)getDataOffsetFrom:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBResponseParametersDataOffset, sizeof(value));
    return value;
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    if (self.fileDescriptor == nil) {
        self.errorCode = kErrorCodeConnectTimeout;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_READ_ANDX) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    if (status != kErrorCodeNone) {
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    const void *parameter = NULL;
    u_char parameterLength = [respPacket getParametersNoCopy:&parameter];
    assert(parameter != NULL);
    uint32_t dataLength = [self getDataLengthFrom:parameter];
    uint16_t dataOffset = [self getDataOffsetFrom:parameter];

    if (dataLength > 0) {
        NSData *dataBlock = NULL;
        [respPacket getDataBlockNoCopy:&dataBlock];
        uint16_t dataBlockLength = [dataBlock length];
        NSAssert(dataBlock, @"empty block");
        NSAssert(dataBlockLength >= dataLength, @"wrong block length");
        dataOffset = dataOffset - kSMBHeaderLength - kSMBMinimumDataBlockLength - kSMBMinimumParameterLength - parameterLength*2;
        self.fileDescriptor.dataPtr = [dataBlock subdataWithRange:NSMakeRange(dataOffset, dataLength)];
        if ([self.delegate respondsToSelector:@selector(read:didReceiveData:)]) {
            [self.delegate read:self didReceiveData:self.fileDescriptor.dataPtr];
        }
    }

    self.fileDescriptor.readBytes = dataLength;
    self.isResponse = YES;
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    self.isResponse = YES;
    [self setState:CZSMBEventsStateError];
}
@end
