//
//  CZSMBTrans2QueryPathInfo.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileInformation.h"
#import "CZSMBSubCommandDelegate.h"

@class CZSMBRequestPacket,CZSMBResponsePacket,CZFileDescriptor;
@interface CZSMBTrans2QueryPathInfo : CZSMBFileInformation<CZSMBSubCommandDelegate>

@property (nonatomic,readonly) NSString *           fileName;
@property (nonatomic,strong) CZFileDescriptor *     fileDescriptor;
@end
