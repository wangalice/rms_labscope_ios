//
//  CZSMBTransTransactNmpipe.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 8/27/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTransTransactNmpipe.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"

@interface CZSMBTransTransactNmpipe() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

static const uint8_t reqMsg1[72] = {
    0x05, 0x00, 0x0b, 0x03, 0x10, 0x00, 0x00, 0x00,
    0x48, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
    0xb8, 0x10, 0xb8, 0x10, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00,
    0xc8, 0x4f, 0x32, 0x4b, 0x70, 0x16, 0xd3, 0x01,
    0x12, 0x78, 0x5a, 0x47, 0xbf, 0x6e, 0xe1, 0x88,
    0x03, 0x00, 0x00, 0x00, 0x04, 0x5d, 0x88, 0x8a,
    0xeb, 0x1c, 0xc9, 0x11, 0x9f, 0xe8, 0x08, 0x00,
    0x2b, 0x10, 0x48, 0x60, 0x02, 0x00, 0x00, 0x00
};

static const uint8_t reqMsg2[100] = {
    0x05, 0x00, 0x00, 0x03, 0x10, 0x00, 0x00, 0x00,
    0x64, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00,
    0x3f, 0xfe, 0x9f, 0x7e, 0x0f, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00,
    ///server name
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ///end
    0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
    0x3f, 0xfe, 0x9f, 0x7e, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0x00, 0x00, 0x00, 0x00
};

/*static const uint8_t reqMsg3[132] = {
    0x05, 0x00, 0x00, 0x03, 0x10, 0x00, 0x00, 0x00,
    0x84, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00,
    0x3f, 0xfe, 0x9f, 0x7e, 0x0f, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00,
    ///server name
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ///end
    0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
    0x3f, 0xfe, 0x9f, 0x7e, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0x00, 0x00, 0x00, 0x00
};*/

@implementation CZSMBTransTransactNmpipe


- (uint16_t)commandFlag {
    return kTRANS_TRANSACT_NMPIPE;
}

- (int)validCommand {
    if (self.FID == 0) {
        return kErrorCodeServerPathError;
    }
    return kErrorCodeNone;
}

- (NSUInteger)parameterCount {
    return 0;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    if (self.isSec) {
        return 0x64;
    }
    return 0x48;
}

- (u_char)setupCount {
    return 2;
}

- (uint32_t)setup2 {
    uint32_t setup = self.FID << 16;
    setup |= kTRANS_TRANSACT_NMPIPE;
    return setup;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    const size_t maxBytes = 0xE0;
    uint8_t * byte = malloc(maxBytes);
    memset(byte, 0, maxBytes);
    
    ///Name
    uint16_t index = 1;
    NSString * pipeName = @"\\PIPE\\";
    
    if (self.isUnicodeSupported) {
        [pipeName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:kUnicodeEncoding];
        index += ([pipeName length] + 1)*2;
    }else {
        [pipeName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:NSASCIIStringEncoding];
        index += [pipeName length] + 1;
    }

    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
//    index += 1;
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
//    int padding = parameterOffset%4;
//    parameterOffset += 4 - padding;
//    index += padding;
    
    if (self.isSec) {
        uint8_t req[100];
        memcpy(req, reqMsg2, 100);
        [self.server getCString:(char *)req+40
                      maxLength:32
                       encoding:NSASCIIStringEncoding];
        memcpy(byte + index, req, 100);
        index += 100;
    }else {
        memcpy(byte + index, reqMsg1, 72);
        index += 72;
    }
    
    [requestPacket setDataBlock:byte
                      byteCount:index];
    
    dataOffset = parameterOffset;
    
    free(byte);
    
}

- (void)setResponseData:(NSData *)response subDataOffset:(uint16_t)responseDataOffset {
    if (self.isSec) {
        if (response.length < 46) {
            return;
        }
        
        const uint8_t * const responseData = [response bytes];
        
        // start parse
        uint16_t pktLen;
        memcpy(&pktLen, responseData + 8, sizeof(pktLen));
        if (pktLen < 0x80) {
            return;
        }
        
        int index = 44;
        uint16_t shareCount;
        memcpy(&shareCount, responseData + index, sizeof(shareCount));
        
        const uint8_t * const responseDataEnd = responseData + response.length;
        
        index = 48 + shareCount * 12;
        const uint8_t *shareInfo = responseData + responseDataOffset + index;
        NSMutableArray *shareArray = [[NSMutableArray alloc] initWithCapacity:shareCount];
        int offset = 0;
        int i = 0;
        while (shareCount > [shareArray count]) {
            if ((shareInfo + offset + 2) >= responseDataEnd) {
                break;
            }
            uint16_t strLength;
            memcpy(&strLength, shareInfo + offset, sizeof(strLength));
            strLength *= 2;
            
            offset += 12;
            
            if (strLength > 2) {
                if ((shareInfo  + offset + strLength - 2) >= responseDataEnd) {
                    break;
                }
                NSString * shareName = [[NSString alloc] initWithBytes:(char*)shareInfo + offset
                                                                length:strLength - 2
                                                              encoding:kUnicodeEncoding];
                
                if (i % 2 == 0) {
                    [shareArray addObject:shareName];
                }
                
            }
            
            offset += strLength;
            int padding = strLength % 4;
            if (padding > 0) {
                offset += 4 - padding;
            }
            
            ++i;
        }
        
        _shareList = shareArray;
    }
}

@end
