//
//  CZSMBSubCommandDelegate.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/11/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZSMBRequestPacket,CZSMBResponsePacket,CZSMBConnection;
@protocol CZSMBSubCommandDelegate <NSObject>
///return error code
- (int)validCommand;
- (uint16_t) commandFlag;
- (void)setRequestPacket:(CZSMBRequestPacket *)requestPacket;
- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t)offset;
- (NSUInteger)parameterCount;
- (uint16_t)parameterOffset;
- (uint16_t)dataOffset;
- (uint16_t)dataCount;
- (u_char)setupCount;
@optional
- (uint16_t)setup;
- (uint32_t)setup2;
@end
