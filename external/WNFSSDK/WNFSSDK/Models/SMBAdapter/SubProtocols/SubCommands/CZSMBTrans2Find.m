//
//  CZSMBTrans2Find.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTrans2Find.h"
#import "CZFileDescriptor.h"

@implementation CZSMBTrans2Find

- (void)getSMBItemsFrom:(NSData *)dataBlock count:(uint16_t)count to:(NSMutableArray *)fileArray {
    uint32_t index = 0;
    NSData *data = dataBlock;
    for (uint16_t i = 0; i < count; i++) {
        CZFileDescriptor *file = [[CZFileDescriptor alloc] init];
        file.filePath = self.searchPath;
        index = [self getSMBInfoFrom:data
                                  to:file];
        if (index > data.length) {
            break;
        }
        
        data = [data subdataWithRange:NSMakeRange(index, data.length - index)];
        
        if ([file.fileName length] >= CZFileDescripttorMaxFileNameSize) {
            continue;
        }

        [fileArray addObject:file];
    }
    //the empty folder will include two items, they are "." and "..".
    if (fileArray && [fileArray count] > 2) {
        self.lastFileName = ((CZFileDescriptor*)[fileArray lastObject]).fileName;
    }
}
@end
