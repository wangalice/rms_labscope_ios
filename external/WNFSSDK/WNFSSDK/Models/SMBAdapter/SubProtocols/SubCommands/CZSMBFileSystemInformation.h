//
//  CZSMBFileSystemInformation.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZDiskInformation;

@interface CZSMBFileSystemInformation : NSObject

@property (nonatomic, strong) CZDiskInformation *diskInformation;

- (void)getSMBFileSystemSizeInformation:(const void *)dataBlock;

@end
