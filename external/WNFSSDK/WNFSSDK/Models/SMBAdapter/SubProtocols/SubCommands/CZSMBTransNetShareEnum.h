//
//  CZSMBTransNetShareEnum.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 8/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBSubCommandDelegate.h"

@class CZFileDescriptor;
@interface CZSMBTransNetShareEnum : NSObject<CZSMBSubCommandDelegate>

@property (nonatomic,readwrite) BOOL            isUnicodeSupported;
@property (nonatomic,readonly) NSArray *        shareList;
@end
