//
//  CZSMBTrans2FindFirst2.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZGlobal.h"
#import "CZSMBTrans2FindFirst2.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"

///sub parameters
#define kSMBResponseSubParametersSID                (0)
#define kSMBResponseSubParametersSearchCount        (2)
#define kSMBResponseSubParametersEndOfSearch        (4)

@interface CZSMBTrans2FindFirst2() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}
@end

@implementation CZSMBTrans2FindFirst2
@synthesize searchID = _searchID;
@synthesize isEndOfSearch = _isEndOfSearch;
@synthesize itemsList = _itemsList;


- (uint16_t)commandFlag {
    return kTRANS2_FIND_FIRST2;
}

- (int)validCommand {
    if (self.searchPath == nil || [self.searchPath length] < 1) {
        return kErrorCodeServerPathError;
    }
    return kErrorCodeNone;
}

- (NSUInteger)parameterCount {
    /// plus *
    NSUInteger count = [self.searchPath length]+1;
    if (self.isUnicodeSupported) {
        count *= 2;
    }
    return 0xC + count + 2;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    return 0;
}

- (u_char)setupCount {
    return 1;
}

- (uint16_t)getSearchCountFrom:(const void*) dataBlock {
    uint16_t value;
    memcpy(&value, dataBlock+kSMBResponseSubParametersSearchCount, sizeof(value));
    return value;
}

- (uint16_t)getSIDFrom:(const void*) dataBlock {
    uint16_t value;
    memcpy(&value, dataBlock+kSMBResponseSubParametersSID, sizeof(value));
    return value;
}

- (uint16_t)getEndOfSearchFrom:(const void*) dataBlock {
    uint16_t value;
    memcpy(&value, dataBlock+kSMBResponseSubParametersEndOfSearch, sizeof(value));
    return value;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    size_t maxBytes = [self.searchPath length];
    if (self.isUnicodeSupported) {
        maxBytes *= 2;
    }
    ///add additional buffer;
    maxBytes += 0x80;
    uint8_t * byte = malloc(maxBytes);
    memset(byte, 0, maxBytes);
    /**
     * Name (variable): This field is present but not used in SMB_COM_TRANSACTION2 requests. If Unicode support has been negotiated, then this field MUST be aligned to a 16-bit boundary and MUST consist of two terminating null characters. If Unicode support has not been negotiated this field will contain only one terminating null character. The Name field MUST be the first field in this section.
     */
    ///one terminating null character, so, let's move to next field.
    uint16_t index = 1;
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     
     */
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    int padding = parameterOffset%4;
    parameterOffset += 4 - padding;
    index += padding;
    
    ///start set subcommand right here.
    uint16_t attributes = self.searchAttributes;
    memcpy(byte+index, &attributes, 2);
    index +=2;
    
    uint16_t searchCount = self.searchCount;
    memcpy(byte+index, &searchCount, 2);
    index +=2;
    
    uint16_t flags = kSMB_FIND_CONTINUE;
    memcpy(byte+index, &flags, 2);
    index +=2;
    /**
     * InformationLevel (2 bytes): This field contains an information level code, which determines the information contained in the response. The list of valid information level codes is specified in section 2.2.2.3.1. A client that has not negotiated long names support MUST request only SMB_INFO_STANDARD. If a client that has not negotiated long names support requests an InformationLevel other than SMB_INFO_STANDARD, the server MUST return a status of STATUS_INVALID_PARAMETER (ERRDOS/ERRinvalidparam).
     */
    uint16_t infoLevel = self.isNTDialectSupported ? kSMB_FIND_FILE_BOTH_DIRECTORY_INFO : kSMB_INFO_STANDARD;
    memcpy(byte+index, &infoLevel, 2);
    index +=2;
    
    /**
     * SearchStorageType (4 bytes): This field specifies whether the search is for directories or for files. This field MUST be one of two values:
     
     FILE_DIRECTORY_FILE     0x00000001
     FILE_NON_DIRECTORY_FILE 0x00000040
     */
    uint32_t storageType = 0x0;
    memcpy(byte+index, &storageType, 4);
    index +=4;
    
    NSString *sPath = [NSString stringWithFormat:@"%@*",self.searchPath];
    if (self.isUnicodeSupported) {
        [sPath getCString:(char*)byte+index
                          maxLength:kMaxDataBlockSize-index
                           encoding:kUnicodeEncoding];
        index += ([sPath length] + 1)*2;
    }else {
        [sPath getCString:(char*)byte+index
                          maxLength:kMaxDataBlockSize-index
                           encoding:NSASCIIStringEncoding];
        index += [sPath length] + 1;
    }
    /**
     * Pad2 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header. This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    index += 4 - index%4;
    [requestPacket setDataBlock:byte byteCount:index];
    
    dataOffset = [requestPacket.packetData length];
    free(byte);
}

- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t)offset {
    uint16_t resultCount = 0;
    if (responseData.length >= (kSMBResponseSubParametersEndOfSearch + 2)) {
        _searchID = [self getSIDFrom:[responseData bytes]];
        uint16_t eos = [self getEndOfSearchFrom:[responseData bytes]];
        _isEndOfSearch = (eos != 0);
        resultCount = [self getSearchCountFrom:[responseData bytes]];
    } else {
        return;
    }
    
    if (offset < responseData.length) {
        NSMutableArray * fileArray = [[NSMutableArray alloc] initWithCapacity:resultCount];
        
        NSData *subData = [responseData subdataWithRange:NSMakeRange(offset, responseData.length - offset)];
        [self getSMBItemsFrom:subData
                        count:resultCount
                           to:fileArray];

        _itemsList = fileArray;
    }
}
@end
