//
//  CZSMBTrans2QueryFSInfo.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTrans2QueryFSInfo.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"

@interface CZSMBTrans2QueryFSInfo() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

@implementation CZSMBTrans2QueryFSInfo

- (uint16_t)commandFlag {
    return kTRANS2_QUERY_FS_INFO;
}

- (u_char)setupCount {
    return 1;
}

- (int)validCommand {
    return kErrorCodeNone;
}

- (NSUInteger)parameterCount {
    return 2;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    return 0;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{

    uint8_t byte[0xFF];
    memset(byte, 0, sizeof(byte));
    
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    uint16_t index = 1;
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    /**
     * This field contains an information level code, which determines the information contained in the response..
     */
    uint16_t fsInfoLevel = kSMB_QUERY_FS_SIZE_INFO;
    memcpy(byte+index, &fsInfoLevel, 2);
    index +=2;
    
    /**
     * Pad2 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header. This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    index += 4 - index%4;
    [requestPacket setDataBlock:byte byteCount:index];
    
    dataOffset = [requestPacket.packetData length];
}

- (void)setResponseData:(NSData *) responseData subDataOffset:(uint16_t) offset{
    [self getSMBFileSystemSizeInformation:[responseData bytes] + offset];
}
@end
