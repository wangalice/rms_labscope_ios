//
//  CZSMBTrans2FindFirst2.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBTrans2Find.h"
#import "CZSMBSubCommandDelegate.h"

@class CZSMBRequestPacket,CZSMBResponsePacket;
@interface CZSMBTrans2FindFirst2 : CZSMBTrans2Find <CZSMBSubCommandDelegate>

@property (nonatomic,readonly) uint16_t               searchID;
@property (nonatomic,readonly) NSArray *            itemsList;

@end
