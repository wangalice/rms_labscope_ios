//
//  CZSMBTrans2QueryPathInfo.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTrans2QueryPathInfo.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"

@interface CZSMBTrans2QueryPathInfo() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

@implementation CZSMBTrans2QueryPathInfo
@synthesize fileDescriptor = _fileDescriptor;
@synthesize fileName = _fileName;


- (uint16_t)commandFlag {
    return kTRANS2_QUERY_PATH_INFO;
}

- (u_char)setupCount {
    return 1;
}

- (int)validCommand {
    if (self.fileName == nil || [self.fileName length] < 1) {
        return kErrorCodeServerPathError;
    }
    return kErrorCodeNone;
}

- (NSUInteger)parameterCount {
    return 6+[self.fileName length]*2;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    return 0;
}

- (NSString*) fileName {
    if (self.fileDescriptor == nil) {
        return nil;
    }
    if (_fileName == nil) {
        if (self.fileDescriptor.fileName) {
            _fileName = [[NSString stringWithFormat:@"%@%@",self.fileDescriptor.filePath,self.fileDescriptor.fileName] copy];
        }else {
            _fileName = [self.fileDescriptor.filePath copy];
        }
        
    }
    
    return _fileName;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    size_t maxBytes = [self.fileName length];
    if (self.isUnicodeSupported) {
        maxBytes *= 2;
    }
    ///add additional buffer;
    maxBytes += 0x80;
    uint8_t * byte = malloc(maxBytes);
    memset(byte, 0, maxBytes);
    
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    uint16_t index = 1;
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    /**
     * InformationLevel (2 bytes): This field contains an information level code, which determines the information contained in the response. The list of valid information level codes is specified in section 2.2.2.3.1. A client that has not negotiated long names support MUST request only SMB_INFO_STANDARD. If a client that has not negotiated long names support requests an InformationLevel other than SMB_INFO_STANDARD, the server MUST return a status of STATUS_INVALID_PARAMETER (ERRDOS/ERRinvalidparam).
     */
    uint16_t infoLevel = self.isNTDialectSupported ? kSMB_QUERY_FILE_BASIC_INFO : kSMB_INFO_STANDARD;;
    memcpy(byte+index, &infoLevel, 2);
    index +=2;
    
    ///ULONG      Reserved;
    uint32_t reserved = 0x0;
    memcpy(byte+index, &reserved, 4);
    index +=4;
    
    if (self.isUnicodeSupported) {
        [self.fileName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:kUnicodeEncoding];
        index += ([self.fileName length] + 1)*2;
    }else {
        [self.fileName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:NSASCIIStringEncoding];
        index += [self.fileName length] + 1;
    }
    /**
     * Pad2 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header. This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    index += 4 - index%4;
    [requestPacket setDataBlock:byte byteCount:index];

    dataOffset = [requestPacket.packetData length];
    free(byte);
}

- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t) offset{
    if (responseData.length >= offset) {
        CZFileDescriptor *file = self.fileDescriptor;
        NSData *subData = [responseData subdataWithRange:NSMakeRange(offset, responseData.length - offset)];
        [self getSMBShortInfoFrom:subData
                           to:file];
    }
}
@end
