//
//  CZSMBFileSystemInformation.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileSystemInformation.h"
#import "CZDiskInformation.h"

@implementation CZSMBFileSystemInformation


- (void)getSMBFileSystemSizeInformation:(const void *)dataBlock {
    int offset = 0;
    ///TotalAllocationUnits: (8 bytes): This field contains the total number of allocation units assigned to the volume.
    uint64_t totalAllocation;
    memcpy(&totalAllocation, dataBlock + offset, sizeof(totalAllocation));
    offset += 8;
    ///TotalFreeAllocationUnits: (8 bytes): This field contains the total number of unallocated or free allocation units for the volume.
    uint64_t totalFreeAllocation;
    memcpy(&totalFreeAllocation, dataBlock + offset, sizeof(totalFreeAllocation));
    offset += 8;
    ///SectorsPerAllocationUnit: (4 bytes): This field contains the number of sectors per allocation unit.
    uint32_t sectorsPerUnit;
    memcpy(&sectorsPerUnit, dataBlock + offset, sizeof(sectorsPerUnit));
    offset += 4;
    ///BytesPerSector: (4 bytes): This field contains the bytes per sector.
    uint32_t bytesPerSector;
    memcpy(&bytesPerSector, dataBlock + offset, sizeof(bytesPerSector));
    
    uint64_t bytesPerUnit = sectorsPerUnit * bytesPerSector;

    self.diskInformation.totalAllocationSize = totalAllocation*bytesPerUnit;
    self.diskInformation.totalFreeAllocationSize = totalFreeAllocation * bytesPerUnit;
}
@end
