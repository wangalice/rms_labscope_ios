//
//  CZSMBFileInformation.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileInformation.h"
#import <wchar.h>
#import "CZFileDescriptor.h"
#import "CZSMBConsistents.h"

@implementation CZSMBFileInformation

- (uint32_t)getSMBInfoFrom:(NSData *)dataBlock to:(CZFileDescriptor *)fileDescriptor {
    if (self.isNTDialectSupported) {
        return [self getSMBFileAndDirectoryInfoFrom:dataBlock to:fileDescriptor hasFileName:YES];
    } else {
        return [self getSMBStandardInfoFrom:dataBlock to:fileDescriptor hasFileName:YES];
    }
}

- (uint32_t)getSMBShortInfoFrom:(NSData *)dataBlock to:(CZFileDescriptor *)fileDescriptor {
    if (self.isNTDialectSupported) {
        return [self getSMBFileAndDirectoryInfoFrom:dataBlock to:fileDescriptor hasFileName:NO];
    } else {
        return [self getSMBStandardInfoFrom:dataBlock to:fileDescriptor hasFileName:NO];
    }
}


- (NSDate*)getDatetimeFrom:(const void *)dataBlock {
    uint16_t date;
    uint16_t time;
    memcpy(&date, dataBlock, sizeof(date));
    memcpy(&time, dataBlock + 2, sizeof(time));
    
    NSDateComponents * dateComponets = [[NSDateComponents alloc] init];
    ///The year. Add 1980 to the resulting value to return the actual year.
    dateComponets.year = (date&0xFE00) >> 9;
    dateComponets.year += 1980;
    ///The month. Values range from 1 to 12.
    dateComponets.month = (date&0x01E0) >> 5;
    ///The date. Values range from 1 to 31.
    dateComponets.day = date&0x001F;
    ///The hours. Values range from 0 to 23.
    dateComponets.hour = (time&0xF800) >> 11;
    ///The minutes. Values range from 0 to 59.
    dateComponets.minute = (time&0x07E0) >> 5;
    ///The seconds. Values MUST represent two-second increments.
    dateComponets.second = time&0x001F;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateTime = [calendar dateFromComponents:dateComponets];
    
    // GMT time = datetime + serverTimeZone
    // Local time = GMT time + systemTimeZone
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:dateTime] + self.serverTimeZone * 60;
    return [dateTime dateByAddingTimeInterval:timeZoneOffset];}

- (NSDate *)get64BitDatetimeFrom:(const void *)dataBlock {
    int64_t seconds;
    memcpy(&seconds, dataBlock, sizeof(seconds));
    seconds /= 1000 * 1000 * 10; // convert to second
    seconds -= 11644473600LL; // convert from 1601-01-01 started to 1970-01-01 started
    if (seconds < 0) {
        seconds = 0;
    }
    return [NSDate dateWithTimeIntervalSince1970:seconds];
}

/**
 *
 SMB_INFO_STANDARD[SearchCount]
 {
     SMB_DATE            CreationDate;
     SMB_TIME            CreationTime;
     SMB_DATE            LastAccessDate;
     SMB_TIME            LastAccessTime;
     SMB_DATE            LastWriteDate;
     SMB_TIME            LastWriteTime;
     ULONG               FileDataSize;
     ULONG               AllocationSize;
     SMB_FILE_ATTRIBUTES Attributes;
     UCHAR               FileNameLength;
     SMB_STRING          FileName;
 }
 if hasFileName is NO, see SMB_INFO_STANDARD structure at http://msdn.microsoft.com/en-us/library/ff470155.aspx
 */
- (uint32_t)getSMBStandardInfoFrom:(NSData *)data to:(CZFileDescriptor*) fileDescriptor hasFileName:(BOOL)hasFileName {
    const uint8_t *dataBlock = [data bytes];
    uint32_t offset = 0;
    ///SMB_DATE            CreationDatetime;
    fileDescriptor.createDatetime = [self getDatetimeFrom:dataBlock + offset];
    offset += 4;
    ///SMB_DATE            LastAccessDatetime;
    fileDescriptor.lastAccessDatetime = [self getDatetimeFrom:dataBlock + offset];
    offset += 4;
    ///SMB_DATE            LastWriteDatetime;
    fileDescriptor.lastWriteDatetime = [self getDatetimeFrom:dataBlock + offset];
    offset += 4;
    ///ULONG               FileDataSize;
    uint32_t value32;
    memcpy(&value32, dataBlock + offset, sizeof(value32));
    fileDescriptor.fileDataSize = value32;
    offset += 4;
    ///ULONG               AllocationSize;
    memcpy(&value32, dataBlock + offset, sizeof(value32));
    fileDescriptor.allocationSize = value32;
    offset += 4;
    ///SMB_FILE_ATTRIBUTES Attributes;
    uint16_t value16;
    memcpy(&value16, dataBlock + offset, sizeof(value16));
    fileDescriptor.attributes = value16;
    offset += 2;
    
    if (hasFileName) {
        ///UCHAR               FileNameLength;
        u_char namelength = *(dataBlock + offset);
        offset += 1;
        ///SMB_STRING          FileName;
        NSString *name;
        if (self.isUnicodeSupported) {
            ///pad is here if unicode.
            offset++;
            const uint16_t *wstrName = (const uint16_t *)(dataBlock + offset);
            NSUInteger wstrNameLength = 0;
            while (*(wstrName + wstrNameLength)) {
                wstrNameLength++;
            }
            
            name = [[NSString alloc] initWithBytes:dataBlock + offset
                                            length:wstrNameLength * 2
                                        encoding:kUnicodeEncoding];
            offset += ([name length] + 1)*2;
            assert(wstrNameLength == [name length]);
        }else {
            name = [[NSString alloc] initWithBytes:dataBlock + offset
                                            length:(NSUInteger)namelength
                                          encoding:NSASCIIStringEncoding];
            offset += [name length] + 1;
            assert(namelength == [name length]);
        }
        
        fileDescriptor.fileName = name;
    }
    return offset;
}


/**
 * if hasFileName is YES, see SMB_FIND_FILE_BOTH_DIRECTORY_INFO structure at http://msdn.microsoft.com/en-us/library/Cc246290.aspx
 * else see SMB_QUERY_FILE_BASIC_INFO at http://msdn.microsoft.com/en-us/library/ff469952.aspx
 */
- (uint32_t)getSMBFileAndDirectoryInfoFrom:(NSData *)data to:(CZFileDescriptor *)fileDescriptor hasFileName:(BOOL)hasFileName {
    size_t offset = 0;
    const void *dataBlock = [data bytes];
    
    uint32_t entryOffset = 0;
    if (hasFileName) {
        memcpy(&entryOffset, dataBlock + offset, sizeof(uint32_t));
        offset += 4;
    
        offset += 4;  // skip file Index
    }
    
    fileDescriptor.createDatetime = [self get64BitDatetimeFrom:dataBlock + offset];
    offset += 8;
    
    fileDescriptor.lastAccessDatetime = [self get64BitDatetimeFrom:dataBlock + offset];
    offset += 8;
    
    fileDescriptor.lastWriteDatetime = [self get64BitDatetimeFrom:dataBlock + offset];
    offset += 8;
    
    offset += 8;  // skip lastChangeTime
    
    if (hasFileName) {
        uint64_t value64;
        memcpy(&value64, dataBlock + offset, sizeof(value64));
        fileDescriptor.fileDataSize = value64;
        offset += 8;
        
        memcpy(&value64, dataBlock + offset, sizeof(value64));
        fileDescriptor.allocationSize = value64;
        offset += 8;
        
        uint16_t value16;
        memcpy(&value16, dataBlock + offset, sizeof(value16));
        fileDescriptor.attributes = value16;
        offset += 4;  // skip extend attribute
        
        uint32_t namelength;
        memcpy(&namelength, dataBlock + offset, sizeof(namelength));
        offset += 4;
        
        offset += (4 + 1 + 1 + 24);  // skip EASize(4), short name length(1), reserve(1), short name (24)
        
        NSString *filename = [[NSString alloc] initWithBytes:dataBlock + offset
                                                      length:namelength
                                                    encoding:kUnicodeEncoding];
        fileDescriptor.fileName = filename;
    } else {
        uint16_t value16;
        memcpy(&value16, dataBlock + offset, sizeof(value16));
        fileDescriptor.attributes = value16;
        offset += 4;  // skip extend attribute
    }
    
    return entryOffset;
}
@end
