//
//  CZSMBTrans2FindNext2.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZGlobal.h"
#import "CZSMBTrans2FindNext2.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"


#define kSMBResponseSubParametersSearchCount        (0)
#define kSMBResponseSubParametersEndOfSearch        (2)

@interface CZSMBTrans2FindNext2() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

@implementation CZSMBTrans2FindNext2
@synthesize isEndOfSearch = _isEndOfSearch;
@synthesize itemsList = _itemsList;


- (uint16_t)commandFlag {
    return kTRANS2_FIND_NEXT2;
}

- (u_char)setupCount {
    return 1;
}

/*
 http://msdn.microsoft.com/en-us/library/ff470270.aspx
 The value 0xFFFF MUST NOT be used as a valid SID. All other possible values for SID, including zero (0x0000), are valid. The value 0xFFFF is reserved.
 */
- (int)validCommand {
    if (self.searchID == 0xFFFF) {
        return kErrorCodeSearchIDInvalid;
    }
    return kErrorCodeNone;
}

- (NSUInteger)parameterCount {
    NSUInteger count = [self.fileName length];
    if (self.isUnicodeSupported) {
        count *= 2;
    }
    return 0xE + count;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    return 0;
}

- (uint16_t)getSearchCountFrom:(const void*) dataBlock {
    uint16_t value;
    memcpy(&value, dataBlock+kSMBResponseSubParametersSearchCount, sizeof(value));
    return value;
}

- (uint16_t)getEndOfSearchFrom:(const void*) dataBlock {
    uint16_t value;
    memcpy(&value, dataBlock+kSMBResponseSubParametersEndOfSearch, sizeof(value));
    return value;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    uint8_t byte[kMaxDataBlockSize];
    memset(byte, 0, kMaxDataBlockSize);
    /**
     * Name (variable): This field is present but not used in SMB_COM_TRANSACTION2 requests. If Unicode support has been negotiated, then this field MUST be aligned to a 16-bit boundary and MUST consist of two terminating null characters. If Unicode support has not been negotiated this field will contain only one terminating null character. The Name field MUST be the first field in this section.
     */
    ///one terminating null character, so, let's move to next field.
    uint16_t index = 1;
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    int padding = parameterOffset%4;
    parameterOffset += 4 - padding;
    index += padding;
    
    ///start set search ID right here.
    uint16_t sid = self.searchID;
    memcpy(byte+index, &sid, 2);
    index +=2;
    
    uint16_t searchCount = self.searchCount;
    memcpy(byte+index, &searchCount, 2);
    index +=2;
    /**
     * InformationLevel (2 bytes): This field contains an information level code, which determines the information contained in the response. The list of valid information level codes is specified in section 2.2.2.3.1. A client that has not negotiated long names support MUST request only SMB_INFO_STANDARD. If a client that has not negotiated long names support requests an InformationLevel other than SMB_INFO_STANDARD, the server MUST return a status of STATUS_INVALID_PARAMETER (ERRDOS/ERRinvalidparam).
     */
    uint16_t infoLevel = self.isNTDialectSupported ? kSMB_FIND_FILE_BOTH_DIRECTORY_INFO : kSMB_INFO_STANDARD;
    memcpy(byte+index, &infoLevel, 2);
    index +=2;
    
    ///ULONG      ResumeKey;
    uint32_t resumeKey = 0x0;
    memcpy(byte+index, &resumeKey, 4);
    index +=4;
    
    ///Flags
    uint16_t flags = kSMB_FIND_CONTINUE;
    memcpy(byte+index, &flags, 2);
    index +=2;
    
    if (self.isUnicodeSupported) {
        [self.fileName getCString:(char*)byte+index
                        maxLength:kMaxDataBlockSize-index
                         encoding:kUnicodeEncoding];
        index += ([self.fileName length] + 1)*2;
    }else {
        [self.fileName getCString:(char*)byte+index
                        maxLength:kMaxDataBlockSize-index
                         encoding:NSASCIIStringEncoding];
        index += [self.fileName length] + 1;
    }
    /**
     * Pad2 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header. This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    index += 4 - index%4;
    [requestPacket setDataBlock:byte byteCount:index];
    
    dataOffset = [requestPacket.packetData length];
}

- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t) offset{
    uint16_t eos = [self getEndOfSearchFrom:[responseData bytes]];
    _isEndOfSearch = (eos != 0);
    uint16_t resultCount = [self getSearchCountFrom:[responseData bytes]];
    
    NSMutableArray * fileArray = [[NSMutableArray alloc] initWithCapacity:resultCount];

    if (offset < responseData.length) {
        NSData *subData = [responseData subdataWithRange:NSMakeRange(offset, responseData.length - offset)];
        [self getSMBItemsFrom:subData
                        count:resultCount
                           to:fileArray];
        _itemsList = fileArray;
    }
}

@end
