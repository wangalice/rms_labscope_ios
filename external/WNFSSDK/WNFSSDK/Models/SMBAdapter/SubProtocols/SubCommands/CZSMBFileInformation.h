//
//  CZSMBFileInformation.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZFileDescriptor;

@interface CZSMBFileInformation : NSObject

@property (nonatomic, readwrite) BOOL                isUnicodeSupported;
@property (nonatomic, readwrite) BOOL                isNTDialectSupported;
/// Minutes from UTC; signed
@property (nonatomic, readwrite) int32_t             serverTimeZone;

- (uint32_t)getSMBInfoFrom:(NSData *)dataBlock to:(CZFileDescriptor *)fileDescriptor;

// get information in short mode, without file name string
- (uint32_t)getSMBShortInfoFrom:(NSData *)dataBlock to:(CZFileDescriptor *)fileDescriptor;
@end
