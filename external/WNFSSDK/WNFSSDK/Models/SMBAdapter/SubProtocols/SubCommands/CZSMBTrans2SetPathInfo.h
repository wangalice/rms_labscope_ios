//
//  CZSMBTrans2SetPathInfo.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/20/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileInformation.h"
#import "CZSMBSubCommandDelegate.h"

@class CZFileDescriptor;
@interface CZSMBTrans2SetPathInfo : CZSMBFileInformation<CZSMBSubCommandDelegate>

@property (nonatomic, readonly) NSString *           fileName;
@property (nonatomic, strong) CZFileDescriptor *     fileDescriptor;
@end
