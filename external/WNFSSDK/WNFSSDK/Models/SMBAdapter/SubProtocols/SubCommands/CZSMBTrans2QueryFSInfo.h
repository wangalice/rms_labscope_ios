//
//  CZSMBTrans2QueryFSInfo.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileSystemInformation.h"
#import "CZSMBSubCommandDelegate.h"

@interface CZSMBTrans2QueryFSInfo : CZSMBFileSystemInformation<CZSMBSubCommandDelegate>

@end
