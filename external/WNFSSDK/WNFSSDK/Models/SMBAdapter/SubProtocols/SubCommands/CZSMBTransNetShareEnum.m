//
//  CZSMBTransNetShareEnum.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 8/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTransNetShareEnum.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"

@interface CZSMBTransNetShareEnum() {
@private
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

@implementation CZSMBTransNetShareEnum

- (uint16_t)commandFlag {
    return 0;
}

- (int)validCommand {

    return kErrorCodeNone;
}

- (u_char)setupCount {
    return 0;
}

- (NSUInteger)parameterCount {
    return (0x13);
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    return 0;
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    const size_t maxBytes = 0x80;
    uint8_t * byte = malloc(maxBytes);
    memset(byte, 0, maxBytes);
    
    ///Name
    uint16_t index = 1;
    NSString * pipeName = @"\\PIPE\\LANMAN";
    
    if (self.isUnicodeSupported) {
        [pipeName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:kUnicodeEncoding];
        index += ([pipeName length] + 1)*2;
    }else {
        [pipeName getCString:(char*)byte+index
                        maxLength:maxBytes-index
                         encoding:NSASCIIStringEncoding];
        index += [pipeName length] + 1;
    }
    
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
//    index += 1;
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    int padding = parameterOffset%4;
    parameterOffset += 4 - padding;
    index += padding;
    
    ///(..WrLeh.B13BWz..)
    const uint8_t magicByts[19] = {
        0x00, 0x00,
        0x57, 0x72,
        0x4C, 0x65,
        0x68, 0x00,
        0x42, 0x31,
        0x33, 0x42,
        0x57, 0x7A,
        0x00, 0x01,
        0x00, 0x00,
        0x10};
    
    memcpy(byte + index, magicByts, 19);
    index += 19;
    
    [requestPacket setDataBlock:byte
                      byteCount:index];
    
    dataOffset = [requestPacket.packetData length];
    
    free(byte);
    
}

- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t) offset{
    /// parse NetShareInfo1 Data Structure
    /// ignore the first 32 bits
    const int index = 4;
    uint16_t shareCount;
    memcpy(&shareCount, [responseData bytes]+index, sizeof(shareCount));
    
    /// assume the share count will not big than 100
    if (shareCount > 100) {
        return;
    }
    const char *shareInfo = [responseData bytes] + offset;
    NSMutableArray * shareArray = [[NSMutableArray alloc] initWithCapacity:shareCount];
    for (uint16_t i = 0; i < shareCount; i++) {
        NSString * shareName = [NSString stringWithCString:(shareInfo + i*20)
                                                  encoding:NSASCIIStringEncoding];
        
        [shareArray addObject:shareName];
    }
    
    _shareList = shareArray;
}

@end
