//
//  CZSMBTransTransactNmpipe.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 8/27/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileInformation.h"
#import "CZSMBSubCommandDelegate.h"

@class CZFileDescriptor;
@interface CZSMBTransTransactNmpipe : NSObject<CZSMBSubCommandDelegate>

@property (nonatomic,readwrite) uint16_t          FID;
@property (nonatomic,strong)    NSString *      server;
@property (nonatomic,readwrite) BOOL            isUnicodeSupported;
@property (nonatomic,readwrite) BOOL            isSec;
@property (nonatomic,readonly) NSArray *        shareList;
@end
