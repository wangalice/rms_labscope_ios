//
//  CZSMBTrans2SetFileInfo.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFileInformation.h"
#import "CZSMBSubCommandDelegate.h"

@class CZFileDescriptor;
@interface CZSMBTrans2SetFileInfo : CZSMBFileInformation<CZSMBSubCommandDelegate>

@property (nonatomic,strong) CZFileDescriptor *     fileDescriptor;
@end
