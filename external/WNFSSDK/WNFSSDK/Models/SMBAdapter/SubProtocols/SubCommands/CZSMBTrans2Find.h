//
//  CZSMBTrans2Find.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/12/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBFileInformation.h"

@interface CZSMBTrans2Find : CZSMBFileInformation

@property (nonatomic,readwrite) uint16_t              searchCount;
@property (nonatomic,strong) NSString *             searchPath;

@property (nonatomic,strong) NSString *             lastFileName;
@property (nonatomic,readwrite) uint16_t              searchAttributes;
/**
 * This field MUST be 'NO' (0x0000) if the search can be continued using the TRANS2_FIND_NEXT2 transaction. This field MUST be 'YES' if this response is the last and the find has reached the end of the search results.
 */
@property (nonatomic,readonly) BOOL                 isEndOfSearch;

- (void)getSMBItemsFrom:(NSData *)dataBlock count:(uint16_t)count to:(NSMutableArray *)fileArray;
@end
