//
//  CZSMBTransNetServerEnum2.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 9/6/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBSubCommandDelegate.h"

@class CZFileDescriptor;
@interface CZSMBTransNetServerEnum2 : NSObject<CZSMBSubCommandDelegate>

@property (nonatomic, readwrite) BOOL            isUnicodeSupported;
@property (nonatomic, readonly) NSArray *        serverList;
@end
