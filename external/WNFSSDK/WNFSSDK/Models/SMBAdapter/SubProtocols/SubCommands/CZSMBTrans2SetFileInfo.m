//
//  CZSMBTrans2SetFileInfo.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTrans2SetFileInfo.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZFileDescriptor.h"

@interface CZSMBTrans2SetFileInfo() {
    uint16_t  parameterOffset;
    uint16_t  dataOffset;
}

@end

@implementation CZSMBTrans2SetFileInfo

- (uint16_t)commandFlag {
    return kTRANS2_SET_FILE_INFO;
}

- (int)validCommand {
    if (self.fileDescriptor == nil) {
        return kErrorCodeServerPathError;
    }
    return kErrorCodeNone;
}

- (u_char)setupCount {
    return 1;
}

- (NSUInteger)parameterCount {
    return 6;
}

- (uint16_t)parameterOffset {
    return parameterOffset;
}

- (uint16_t)dataOffset {
    return dataOffset;
}

- (uint16_t)dataCount {
    ///Length of SMB_SET_FILE_BASIC_INFO
    return 40;
}

/**
 * number of seconds from 1 Jan. 1601 00:00 to 1 Jan 1970 00:00 UTC
 */
#define EPOCH_DIFF 11644473600000l

- (uint64_t)fileTimeFromDate:(NSDate *)date {
    if (date) {
        return (([date timeIntervalSince1970] * 1000) + EPOCH_DIFF) * 10000;
    } else {
        return 0;
    }
}

- (void)setRequestPacket:(CZSMBRequestPacket*) requestPacket{
    const size_t maxBytes = 0x80;
    uint8_t * byte = malloc(maxBytes);
    memset(byte, 0, maxBytes);
    
    /**
     * Pad1 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header (section 2.2.3.1). This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    uint16_t index = 1;
    ///set parameters offset
    ///number 2 is the datablock USHORT size.
    parameterOffset = [requestPacket.packetData length]+index+2;
    int padding = parameterOffset%4;
    parameterOffset += 4 - padding;
    index += padding;
    /**
     * FID (2 bytes): This field MUST contain a valid FID returned from a previously successful SMB open command.
     */
    uint16_t fid = self.fileDescriptor.FID;
    memcpy(byte+index, &fid, 2);
    index +=2;
    /**
     * InformationLevel (2 bytes): This field contains an information level code, which determines the information contained in the response. The list of valid information level codes is specified in section 2.2.2.3.1. A client that has not negotiated long names support MUST request only SMB_INFO_STANDARD. If a client that has not negotiated long names support requests an InformationLevel other than SMB_INFO_STANDARD, the server MUST return a status of STATUS_INVALID_PARAMETER (ERRDOS/ERRinvalidparam).
     */
    uint16_t infoLevel = kSMB_SET_FILE_BASIC_INFO;
    memcpy(byte+index, &infoLevel, 2);
    index +=2;
    
    ///USHORT      Reserved;
    uint16_t reserved = 0x0;
    memcpy(byte+index, &reserved, 2);
    index +=2;
    
    /**
     * Pad2 (variable): This field SHOULD be used as an array of padding bytes to align the following field to a 4-byte boundary relative to the start of the SMB Header. This constraint can cause this field to be a zero-length field. This field SHOULD be set to zero by the client/server and MUST be ignored by the server/client.
     */
    int pad = 4 - index%4;
    index += pad;
    //    [requestPacket setDataBlock:byte
    //                      byteCount:index];
    
    
    //    SMB_SET_FILE_BASIC_INFO
    //    {
    //        FILETIME    CreationTime              8 Bytes
    //        FILETIME    LastAccessTime            8 Bytes
    //        FILETIME    LastWriteTime             8 Bytes
    //        FILETIME    ChangeTime                8 Bytes
    //        SMB_EXT_FILE_ATTR   ExtFileAttributes 4 Bytes
    //        ULONG       Reserved                  4 Bytes
    //    }
    uint64_t createTime = [self fileTimeFromDate:self.fileDescriptor.createDatetime];
    memcpy(byte + index, &createTime, sizeof(createTime));
    index += 8;
    
    uint64_t lastAccessTime = [self fileTimeFromDate:self.fileDescriptor.lastAccessDatetime];
    memcpy(byte + index, &lastAccessTime, sizeof(lastAccessTime));
    index += 8;
    
    uint64_t lastWriteTime = [self fileTimeFromDate:self.fileDescriptor.lastWriteDatetime];
    memcpy(byte + index, &lastWriteTime, sizeof(lastWriteTime));
    index += sizeof(uint64_t) * 2; //skip change time
    uint32_t attr = self.fileDescriptor.attributes;
    memcpy(byte + index, &attr, 4);
    index += 4;
    index += 4;
    [requestPacket setDataBlock:byte
                      byteCount:index];
    
    /// Decrease length of SMB_SET_FILE_BASIC_INFO
    dataOffset = [requestPacket.packetData length] - 40;
    
    free(byte);
}

- (void)setResponseData:(NSData *)responseData subDataOffset:(uint16_t) offset{
    
}

@end
