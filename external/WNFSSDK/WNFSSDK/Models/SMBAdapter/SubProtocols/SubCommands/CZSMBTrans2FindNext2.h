//
//  CZSMBTrans2FindNext2.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBTrans2Find.h"
#import "CZSMBSubCommandDelegate.h"

@class CZSMBRequestPacket,CZSMBResponsePacket;
@interface CZSMBTrans2FindNext2 : CZSMBTrans2Find <CZSMBSubCommandDelegate> {
    
}

@property (nonatomic,readwrite) uint16_t              searchID;
@property (nonatomic,strong) NSString *             fileName;
@property (nonatomic,readonly) NSArray *            itemsList;
@end
