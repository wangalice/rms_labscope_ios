//
//  CZSMBNtCreate.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNtCreate.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZFileDescriptor.h"
#import "CZSMBAdapter.h"

#undef kSMBRequestParametersReserved
#define kSMBRequestParametersReserved               (4)
#define kSMBRequestParametersNameLength             (5)
#define kSMBRequestParametersFlags                  (7)
#define kSMBRequestParametersRootDirectoryFID       (11)
#define kSMBRequestParametersDesiredAccess          (15)
#define kSMBRequestParametersAllocationSize         (19)
#define kSMBRequestParametersExtFileAttributes      (27)
#define kSMBRequestParametersShareAccess            (31)
#define kSMBRequestParametersCreateDisposition      (35)
#define kSMBRequestParametersCreateOptions          (39)
#define kSMBRequestParametersImpersonationLevel     (43)
#define kSMBRequestParametersSecurityFlags          (47)

@interface CZSMBNtCreate() {
    
}

@end

@implementation CZSMBNtCreate


- (int)eventFlag {
    return kEventFlagNTCreate;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.fileDescriptor == nil) {
        self.errorCode = kErrorCodeFileDescriptorError;
        [self setState:CZSMBEventsStateError];
    }
    
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_NT_CREATE_ANDX];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
/**
 * AndXCommand (1 byte): The command code for the next SMB command in the packet. This value MUST be set to 0xFF if there are no additional SMB commands in the client request packet.
 */
    reqPacket.andXCommand = 0xFF;
    
    uint16_t nameLength = self.fileDescriptor.fileName.length+self.fileDescriptor.filePath.length+1;
    if (connection.isUnicodeSupported) {
        nameLength *= 2;
    }
    [reqPacket setParameterWithUint16:&nameLength
                               offset:kSMBRequestParametersNameLength];
/**
 * Flags (4 bytes): A 32-bit field containing a set of flags that modify the client request. Unused bit fields SHOULD be set to 0 when sent and MUST be ignored on receipt.
 */
    uint32_t flags = 0;
    if (self.fileDescriptor.openFlag&kOpenFlagExclusiveLock) {
        flags |= kNT_CREATE_REQUEST_OPLOCK;
    }
    [reqPacket setParameterWithUint32:&flags
                              offset:kSMBRequestParametersFlags];
/**
 * DesiredAccess (4 bytes): A 32-bit field of flags that indicate standard, specific, and generic access rights. These rights are used in access-control entries (ACEs) and are the primary means of specifying the requested or granted access to an object. If this value is 0x00000000, it represents a request to query the attributes without accessing the file.
 */
    uint32_t desiredAccess = 0;
    if (self.fileDescriptor.openFlag&kOpenFlagReadOnly) {
        desiredAccess |= kFILE_READ_DATA;
        desiredAccess |= kGENERIC_READ;
    }
    if (self.fileDescriptor.openFlag&kOpenFlagWriteOnly) {
        desiredAccess |= kFILE_WRITE_DATA;
    }
    if (self.fileDescriptor.openFlag&kOpenFlagAppend) {
        desiredAccess |= kFILE_APPEND_DATA;
    }
    if (self.fileDescriptor.openFlag&kOpenFlagTruncate) {
        desiredAccess |= kGENERIC_WRITE;
    }
    if (self.fileDescriptor.openFlag&kOpenFlagWriteAttributes) {
        desiredAccess |= kFILE_WRITE_ATTRIBUTES;
    }
    if (self.fileDescriptor.openFlag & kOpenFlagDelete) {
        desiredAccess |= kDELETE;
        desiredAccess |= kFILE_READ_ATTRIBUTES;
    }

    [reqPacket setParameterWithUint32:&desiredAccess
                              offset:kSMBRequestParametersDesiredAccess];
    
/**
 * AllocationSize (8 bytes): The client MUST set this value to the initial allocation size of the file in bytes. The server MUST ignore this field if this request is to open an existing file. This field MUST be used only if the file is created or overwritten. The value MUST be set to 0x0000000000000000 in all other cases. This does not apply to directory-related requests. This is the number of bytes to be allocated, represented as a 64-bit integer value.
 */

/**
 * ShareAccess (4 bytes): A 32-bit field that specifies how the file SHOULD be shared with other processes. The names in the table below are provided for reference use only. If ShareAccess values of FILE_SHARE_READ, FILE_SHARE_WRITE, or FILE_SHARE_DELETE are set for a printer file or a named pipe, the server SHOULD ignore these values. The value MUST be FILE_SHARE_NONE or some combination of the other values.
 */
    uint32_t shareAccess = kFILE_SHARE_READ|kFILE_SHARE_WRITE|kFILE_SHARE_DELETE;
    if (self.fileDescriptor.openFlag&kOpenFlagExclusiveLock) {
        shareAccess = kFILE_SHARE_NONE;
    }
    [reqPacket setParameterWithUint32:&shareAccess
                              offset:kSMBRequestParametersShareAccess];
    
/**
 * CreateDisposition (4 bytes): A 32-bit value that represents the action to take if the file already exists or if the file is a new file and does not already exist.
 */
    uint32_t createDisp = kFILE_SUPERSEDE;
    if (self.fileDescriptor.openFlag&kOpenFlagReadOnly) {
        createDisp |= kFILE_OPEN;
    }
    if (self.fileDescriptor.openFlag&kOpenFlagWriteAttributes) {
        createDisp |= kFILE_OPEN;
    }
    if ((self.fileDescriptor.openFlag&kOpenFlagCreate)) {
        createDisp |= kFILE_OPEN_IF;
    }
    if ((self.fileDescriptor.openFlag&kOpenFlagExists)) {
        createDisp = kFILE_SUPERSEDE;
        createDisp |= kFILE_CREATE;
    }
    if ((self.fileDescriptor.openFlag&kOpenFlagTruncate)) {
        createDisp |= kFILE_OVERWRITE_IF;
    }
    if ((self.fileDescriptor.openFlag&kOpenFlagDelete)) {
        createDisp |= kFILE_OPEN;
    }
    [reqPacket setParameterWithUint32:&createDisp
                              offset:kSMBRequestParametersCreateDisposition];

    uint32_t createOption = 0;
    if (!self.fileDescriptor.isDirectory) {
        createOption |= kFILE_NON_DIRECTORY_FILE;
    }
    if (self.fileDescriptor.openFlag & kOpenFlagReadOnly) {
        createOption |= kFILE_OPEN_NO_RECALL;
    }
    if (self.fileDescriptor.openFlag & kOpenFlagWriteOnly) {
        createOption |= kFILE_SEQUENTIAL_ONLY;
    }
    [reqPacket setParameterWithUint32:&createOption
                               offset:kSMBRequestParametersCreateOptions];
    
/**
 * ImpersonationLevel (4 bytes): This field specifies the impersonation level requested by the application that is issuing the create request, and MUST contain one of the following values. The server MUST validate this field, but otherwise ignore it.
 */
    uint32_t impeLevel = kSEC_IMPERSONATE;
    [reqPacket setParameterWithUint32:&impeLevel
                              offset:kSMBRequestParametersImpersonationLevel];
    
/**
 * WordCount (1 byte): This field MUST be 0x18.
    48 bytes, 24 Words.
 */
    [reqPacket setParameterCount:0x18];
    NSString * filePath = [NSString stringWithFormat:@"%@%@",
                           self.fileDescriptor.filePath,
                           self.fileDescriptor.fileName];
    ///null pad
    size_t index = 1, totalLength = 0;
    uint8_t *byt = NULL;
    if (connection.isUnicodeSupported) {
        totalLength = ([filePath length] + 1)*2;
        byt = malloc(totalLength + index);
        memset(byt, 0, totalLength + index);
        [filePath getCString:(char*)byt + index
                   maxLength:totalLength - index
                    encoding:kUnicodeEncoding];
        index += totalLength;
        
    }else {
        totalLength = [filePath length] + 1;
        byt = malloc(totalLength);
        memset(byt, 0, totalLength);
        [filePath getCString:(char*)byt
                   maxLength:totalLength
                    encoding:NSASCIIStringEncoding];
        index = totalLength;
    }

    [reqPacket setDataBlock:byt
                  byteCount:index];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
    free(byt);
}

- (uint16_t)getFIdFrom:(const void *) parameter {
    uint16_t value;
    memcpy(&value, parameter+5, sizeof(value));
    return value;
}

- (uint8_t)getOpLockFrom:(const void *) parameter {
    return *((const uint8_t *)((const char*)parameter+4));
}

- (uint64_t)getEndOfFileFrom:(const void *) parameter {
    uint64_t value;
    memcpy(&value, parameter+55, sizeof(value));
    return value;
}

- (uint64_t)getAllocationSizeFrom:(const void*) parameter {
    uint64_t value;
    memcpy(&value, parameter+47, sizeof(value));
    return value;
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_NT_CREATE_ANDX) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    const void *parameter = NULL;
    [respPacket getParametersNoCopy:&parameter];
    assert(parameter != NULL);
    self.fileDescriptor.FID = [self getFIdFrom:parameter];
    self.fileDescriptor.opLockLevel = [self getOpLockFrom:parameter];
    self.fileDescriptor.allocationSize = [self getAllocationSizeFrom:parameter];
    self.fileDescriptor.endOfFile = [self getEndOfFileFrom:parameter];
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
