//
//  CZSMBTreeConnect.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTreeConnect.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZLANManagerAuthentication.h"


@interface CZSMBTreeConnect() {
    
}

@end

@implementation CZSMBTreeConnect

- (int)eventFlag {
    return kEventFlagTreeConnect;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (connection.isShareConnected) {
        [self setState:CZSMBEventsStateSuccess];
        return;
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_TREE_CONNECT_ANDX];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    ///This field MUST be either the command code for the next SMB command in the packet or 0xFF.
    reqPacket.andXCommand = 0xFF;
    ///A reserved field. This MUST be set to 0x00 when this request is sent.
    reqPacket.andXReserved = 0;
    /**
     * This field MUST be set to the offset in bytes from the start of the SMB Header to the start of the WordCount field in the next SMB command in this packet. This field is valid only if the AndXCommand field is not set to 0xFF. If AndXCommand is 0xFF, this field MUST be ignored by the server.
     */
    reqPacket.andXOffset = 0;
/** Flags
 * A 16-bit field used to modify the SMB_COM_TREE_CONNECT_ANDX Request (section 2.2.4.55.1). The client MUST set reserved values to 0, and the server MUST ignore them.
 */
    uint16_t flag = 0;
    [reqPacket setParameter:&flag
                     offset:kSMBParametersFlags
                     length:sizeof(uint16_t)];
/**
 * The SMB_Parameters.Bytes.PasswordLength MUST be the full length of the Password field. If the Password is the null padding byte, the password length is 1.
 */
    uint16_t passwordLength = 1;
    if (connection.isShareLevelAccessControl) {
        passwordLength = 24;///The hash compute length.
    }
    [reqPacket setParameter:&passwordLength
                     offset:kSMBParametersPasswordLength
                     length:sizeof(uint16_t)];
    ///The value of this field MUST be 0x04.
    [reqPacket setParameterCount:4];
/**
 * Now, time to set data block.
     ByteCount       = variable (An array of bytes)
     Password        = ""
     Path            = "\\Jimmy\document"
     Service         = "A:"  (Disk Share)
 */
    uint8_t byte[kMaxDataBlockSize];
    memset(byte, 0, kMaxDataBlockSize);
    NSString * share = connection.tcpTransport.shareName;
    NSString * service = kServiceFileSystem;
    if ([share length] > 4) {
        NSString * str = [share substringFromIndex:[share length]-4];
        if ([str isEqualToString:@"IPC$"]) {
            service = kServiceNamedPipe;
        }
    }
    
    uint16_t index = 0;
    if (connection.isShareLevelAccessControl) {
        ///Compute hash response here.
        uint8_t response[24];
        CZLANManagerAuthentication *lanMan = [[CZLANManagerAuthentication alloc] init];
        [lanMan encryptPassword:connection.tcpTransport.password
                  ntlmChallenge:[connection.serverChallenge bytes]
                     toResponse:response];
        memcpy((char*)byte+index, response, 24);
        index = 24;
        ///pad
        index += 1;
        
    }else {
        ///The Password is the null padding byte, so, let's move to next field.
        index = 1;
    }
    
    ///Set share path
    if (connection.isUnicodeSupported) {
        [share getCString:(char*)byte+index
                maxLength:kMaxDataBlockSize-index
                 encoding:kUnicodeEncoding];
        index+=([share length]+1)*2;
    }else {
        [share getCString:(char*)byte+index
                maxLength:kMaxDataBlockSize-index
                 encoding:NSASCIIStringEncoding];
        index+=[share length]+1;
    }
    [service getCString:(char*)byte+index
              maxLength:kMaxDataBlockSize-index
               encoding:NSASCIIStringEncoding];
    index+=[service length]+1;
    
    [reqPacket setDataBlock:byte byteCount:index];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

- (uint16_t)getOptionalFromParemeters:(const void*) parameter {
    uint16_t value;
    memcpy(&value, parameter+kSMBParametersOptionalSupport, sizeof(value));
    return value;
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_TREE_CONNECT_ANDX) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    CZLog(@"UID:%d and TID:%d",*respPacket.getSMBUID,*respPacket.getSMBTID);
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
        CZLog(@"TreeConnect success");
        self.connection.isShareConnected = YES;
    }else {
        self.connection.isShareConnected = NO;
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }

    self.connection.treeID = *respPacket.getSMBTID;
    
    const void *parameter = NULL;
    [respPacket getParametersNoCopy:&parameter];
    assert(parameter != NULL);
    uint16_t optional = [self getOptionalFromParemeters:parameter];
    ///If set, this share is managed by DFS, however we don't care this flag.
    if (optional&kSMB_SHARE_IS_IN_DFS) {
        CZLog(@"Share is managed by DFS.");
    }
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
