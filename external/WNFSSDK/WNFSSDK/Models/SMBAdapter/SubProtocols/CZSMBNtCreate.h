//
//  CZSMBNtCreate.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

/**
 * This command was introduced in the NT LAN Manager dialect.
 This command is used to create and open a new file, or to open an existing file, or to open and truncate an existing file to zero length, or to create a directory, or to create a connection to a named pipe. The FID returned MAY be used in subsequent requests.
 The message includes the pathname of the file, directory, or named pipe, and RootDirectoryFID (see following) that the client attempts to create or open. If the message is successful, the server response MUST include a FID value identifying the opened resource. The client MUST supply the FID in subsequent operations on the resource. The client MUST have write permission on the resource parent directory to create a new file or directory, or write permissions on the file itself to truncate the file.
 */

#import "CZSMBEvents.h"

///If set, the client requests an exclusive OpLock.
#define kNT_CREATE_REQUEST_OPLOCK               (0x00000002)
///If set, the client requests an exclusive batch OpLock.
#define kNT_CREATE_REQUEST_OPBATCH              (0x00000004)
///If set, the client indicates that the parent directory of the target is to be opened.
#define kNT_CREATE_OPEN_TARGET_DIR              (0x00000008)
///If set, then the client is requesting extended information in the response.
#define kNT_CREATE_REQUEST_EXTENDED_RESPONSE    (0x00000010)

///Indicates the right to read data from the file.
#define kFILE_READ_DATA                         (0x00000001)
///Indicates the right to write data into the file beyond the end of the file.
#define kFILE_WRITE_DATA                        (0x00000002)
///Indicates the right to append data to the file beyond the end of the file only.
#define kFILE_APPEND_DATA                       (0x00000004)
///Indicates the right to read the extended attributes (EAs) of the file.
#define kFILE_READ_EA                           (0x00000008)
///Indicates the right to write or change the extended attributes (EAs) of the file.
#define kFILE_WRITE_EA                          (0x00000010)
///Indicates the right to read the attributes of the file.
#define kFILE_READ_ATTRIBUTES                   (0x00000080)
///Indicates the right to change the attributes of the file.
#define kFILE_WRITE_ATTRIBUTES                  (0x00000100)
///Indicates the right to delete or to rename the file.
#define kDELETE                                 (0x00010000)
///Indicates the right to read the security descriptor of the file.
#define kREAD_CONTROL                           (0x00020000)
///Indicates a request for the following combination of access flags listed previously in this table: FILE_WRITE_DATA, FILE_APPEND_DATA, SYNCHRONIZE, FILE_WRITE_ATTRIBUTES, and FILE_WRITE_EA.
#define kGENERIC_WRITE                          (0x40000000)
///Indicates a request for the following combination of access flags listed previously in this table: FILE_READ_DATA, FILE_READ_ATTRIBUTES, FILE_READ_EA, and SYNCHRONIZE.
#define kGENERIC_READ                           (0x80000000)

///(No bits set.)Prevents the file from being shared.
#define kFILE_SHARE_NONE                        (0x00000000)
///Other open operations can be performed on the file for read access.
#define kFILE_SHARE_READ                        (0x00000001)
///Other open operations can be performed on the file for write access.
#define kFILE_SHARE_WRITE                       (0x00000002)
//Other open operations can be performed on the file for delete access.
#define kFILE_SHARE_DELETE                      (0x00000004)

///(No bits set.)If the file already exists, it SHOULD be superseded (overwritten). If it does not already exist, then it SHOULD be created.
#define kFILE_SUPERSEDE                         (0x00000000)

///If the file already exists, it SHOULD be opened rather than created. If the file does not already exist, the operation MUST fail.
#define kFILE_OPEN                              (0x00000001)

///If the file already exists, the operation MUST fail. If the file does not already exist, it SHOULD be created.
#define kFILE_CREATE                            (0x00000002)

///If the file already exists, it SHOULD be opened. If the file does not already exist, then it SHOULD be created. This value is equivalent to (FILE_OPEN | FILE_CREATE).
#define kFILE_OPEN_IF                           (0x00000003)

///If the file already exists, it SHOULD be opened and truncated. If the file does not already exist, the operation MUST fail. The client MUST open the file with at least GENERIC_WRITE access for the command to succeed.
#define kFILE_OVERWRITE                         (0x00000004)

///If the file already exists, it SHOULD be opened and truncated. If the file does not already exist, it SHOULD be created. The client MUST open the file with at least GENERIC_WRITE access.
#define kFILE_OVERWRITE_IF                      (0x00000005)

///The application-requested impersonation level is Anonymous.
#define kSEC_ANONYMOUS                          (0x00000000)

///The application-requested impersonation level is Identification.
#define kSEC_IDENTIFY                           (0x00000001)

///The application-requested impersonation level is Impersonation.
#define kSEC_IMPERSONATE                        (0x00000002)

/// This option indicates that access to the file can be sequential. The server can use this information to influence its caching and read-ahead strategy for this file. The file MAY in fact be accessed randomly, but the server can optimize its caching and read-ahead policy for sequential access.
#define kFILE_SEQUENTIAL_ONLY                   (0x00000004)

/// If the file being opened is a directory, the server MUST fail the request with STATUS_FILE_IS_A_DIRECTORY in the Status field of the SMB Header in the server response.
#define kFILE_NON_DIRECTORY_FILE                (0x00000040)

/// In a hierarchical storage management environment, this option requests that the file SHOULD NOT be recalled from tertiary storage such as tape. A file recall can take up to several minutes in a hierarchical storage management environment. The clients can specify this option to avoid such delays.
#define kFILE_OPEN_NO_RECALL                    (0x00400000)

//UCHAR AndXCommand;
//UCHAR AndXReserved;
//USHORT AndXOffset;
//UCHAR OpLockLevel;
//USHORT FID;   5
//ULONG CreateDisposition;  7
//FILETIME CreateTime;      11
//FILETIME LastAccessTime;  19
//FILETIME LastWriteTime;   27
//FILETIME LastChangeTime;  35
//SMB_EXT_FILE_ATTR ExtFileAttributes;  43
//LARGE_INTEGER AllocationSize;     47
//LARGE_INTEGER EndOfFile;          55
//USHORT ResourceType;


@class CZFileDescriptor;
@interface CZSMBNtCreate : CZSMBEvents<CZSMBEventsDelegate>

@property (nonatomic,strong) CZFileDescriptor *             fileDescriptor;
@end
