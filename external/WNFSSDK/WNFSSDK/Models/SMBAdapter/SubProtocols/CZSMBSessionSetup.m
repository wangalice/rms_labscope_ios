//
//  CZSMBSessionSetup.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBSessionSetup.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZLANManagerAuthentication.h"
#import "CZSMBSession.h"

@interface CZSMBSessionSetup() {
    CZSMBEventsState    _state;
}

@end

@implementation CZSMBSessionSetup

- (int)eventFlag {
    return kEventFlagSessionSetup;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (connection.isAuthenticated) {
        [self setState:CZSMBEventsStateSuccess];
        return;
    }
    
    self.connection = connection;
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_SESSION_SETUP_ANDX];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    ///This field MUST be either the command code for the next SMB command in the packet or 0xFF.
    reqPacket.andXCommand = 0xFF;
    ///A reserved field. This MUST be set to 0x00 when this request is sent.
    reqPacket.andXReserved = 0;
/**
 * This field MUST be set to the offset in bytes from the start of the SMB Header to the start of the WordCount field in the next SMB command in this packet. This field is valid only if the AndXCommand field is not set to 0xFF. If AndXCommand is 0xFF, this field MUST be ignored by the server.
 */
    reqPacket.andXOffset = 0;
    reqPacket.maxBufferSize = connection.serverMaxBufferSize;
/**
 * The maximum number of pending requests supported by the client. This value MUST be less than or equal to the MaxMpxCount field value provided by the server in the SMB_COM_NEGOTIATE Response.
 */
    reqPacket.maxMpxCount = connection.maxMpxCount;
    
/**
 * The number of this VC (virtual circuit) between the client and the server. This field SHOULD be set to a value of 0x0000 for the first virtual circuit between the client and the server and it SHOULD be set to a unique nonzero value for each additional virtual circuit.
 */
    reqPacket.vcNumber = 0;
    reqPacket.sessionKey = connection.sessionKey;
/** Referenced in  http://www.ubiqx.org/cifs/SMB.html#SMB.6.3.2
 *
 * Basically, though, if Extended Security has been negotiated then the Lengths field is a single uint16_t, known as SecurityBlobLength in the SNIA doc. If Extended Security is not in use then there will be two uint16_t fields identified by the excessively long names:
 
    CaseInsensitivePasswordLength and
    CaseSensitivePasswordLength.
 */
    if (connection.isExtendedSecurity) {
        reqPacket.securityBlobLength = 0;//TO DO: need to set the real number
        ///Words (24 bytes) = 24/2 words
        [reqPacket setParameterCount:24/2];
    }else {
        ///if we choose LM or NTLM, here is the hash of response length. (LM/NTLM is case insensitive)
        if (connection.isUnicodeSupported) {
            reqPacket.caseInsensitivePasswordLength = 24;
            reqPacket.caseSensitivePasswordLength = 24;///Called UnicodePasswordLen in MS-CIFS.pdf
        }else {
            reqPacket.caseInsensitivePasswordLength = 24;
            reqPacket.caseSensitivePasswordLength = 0;///Called UnicodePasswordLen in MS-CIFS.pdf
        }
        
        ///Words (26 bytes) = 26/2 words
        [reqPacket setParameterCount:26/2];
    }
    reqPacket.reserved = 0;
    ///If set, indicates that the server supports the 32-bit NT_STATUS error codes.
    uint32_t capabilities = kCAP_STATUS32;

/**
 * The client supports SMB_COM_READ_RAW and SMB_COM_WRITE_RAW requests. Raw mode is not supported over connectionless transports. Remember that we already have choose TCP/IP.
 
 More:
 Windows Server 2008, Windows Server 2008 R2, and Windows Server 2012 do not support SMB_COM_READ_RAW or SMB_COM_WRITE_RAW and disconnect the client by closing the underlying transport connection if either command is received from the client.
 */
//    capabilities |= kCAP_RAW_MODE;
    ///The client supports UTF-16LE Unicode strings.
    if (connection.isUnicodeSupported) {
        capabilities |= kCAP_UNICODE;
        capabilities |= kCAP_NT_SMBS;
    }
    reqPacket.capabilities = capabilities;
    
    ///Construct data block
    uint8_t dataBlock[kMaxDataBlockSize];
    memset(dataBlock, 0, kMaxDataBlockSize);
    ///SecurityBlob field MUST be the authentication token sent to the server.
    uint16_t length;
    if (connection.isUnicodeSupported) {
        length = [self constructDataBlockByUnicodePassword:connection.tcpTransport.password
                                                  userName:connection.tcpTransport.userName
                                                    domain:connection.tcpTransport.domainName
                                                 challenge:[connection.serverChallenge bytes]
                                                    toByte:dataBlock];
    }else {
        length = [self constructDataBlockByOEMPassword:connection.tcpTransport.password
                                              userName:connection.tcpTransport.userName
                                                domain:connection.tcpTransport.domainName
                                             challenge:[connection.serverChallenge bytes]
                                                toByte:dataBlock];
    }
    [reqPacket setDataBlock:dataBlock byteCount:length];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

- (int)constructDataBlockByOEMPassword:(NSString *) password userName:(NSString *) aName domain:(NSString *) aDomain challenge:(const uint8_t *)challenge toByte:(uint8_t *) block{
    /**
     * A string that represents the native operating system of the SMB client. If SMB_FLAGS2_UNICODE is set in the Flags2 field of the SMB header of the request, then the name string MUST be a NULL-terminated array of 16-bit Unicode characters. Otherwise, the name string MUST be a NULL-terminated array of OEM characters. If the name string consists of Unicode characters, then this field MUST be aligned to start on a 2-byte boundary from the start of the SMB header.
     */
    NSString *nativeOS = kSMBClientOS;
    /**
     * A string that represents the native LAN manager type of the client. If SMB_FLAGS2_UNICODE is set in the Flags2 field of the SMB header of the request, then the name string MUST be a NULL-terminated array of 16-bit Unicode characters. Otherwise, the name string MUST be a NULL-terminated array of OEM characters. If the name string consists of Unicode characters, then this field MUST be aligned to start on a 2-byte boundary from the start of the SMB header.
     */
    NSString *nativeLanMan = kSMBClientName;
    
    int index = 0;
    uint8_t response[24];
    CZLANManagerAuthentication *lanMan = [[CZLANManagerAuthentication alloc] init];
    [lanMan encryptPassword:password
              ntlmChallenge:challenge
                 toResponse:response];
    memcpy(block, response, 24);
    index = 24;
    
    [[aName uppercaseString] getCString:(char*)block+index
                              maxLength:kMaxDataBlockSize-index
                               encoding:NSASCIIStringEncoding];

    ///plus \0 length
    index+=aName.length+1;
    
    NSString * domain = aDomain;
    if (domain == nil || [domain isEqualToString:@""]) {
        domain = @"?";
    }
    
    [domain getCString:(char*)block+index
             maxLength:kMaxDataBlockSize-index
              encoding:NSASCIIStringEncoding];
    index+=domain.length+1;
    
    [nativeOS getCString:(char*)block+index
               maxLength:kMaxDataBlockSize-index
                encoding:NSASCIIStringEncoding];
    index+=nativeOS.length+1;
    
    [nativeLanMan getCString:(char*)block+index
                   maxLength:kMaxDataBlockSize-index
                    encoding:NSASCIIStringEncoding];
    index+=nativeLanMan.length+1;
    
    return index;
}

- (int)constructDataBlockByUnicodePassword:(NSString*) password userName:(NSString*) aName domain:(NSString*) aDomain challenge:(const uint8_t*)challenge toByte:(uint8_t*) block{
    NSString *nativeOS = @"";
    NSString *nativeLanMan = @"";
    
    int index = 0;
    uint8_t response[24];
    CZLANManagerAuthentication *lanMan = [[CZLANManagerAuthentication alloc] init];
    [lanMan encryptPassword:password
              ntlmChallenge:challenge
                 toResponse:response];
    memcpy((char*)block+index, response, 24);
    index = 24;
    
    memcpy((char*)block+index, response, 24);
    index += 24;
    
    ///null pad, ensure that the AccountName string is aligned on a 16-bit boundary.
    index += 1;
    
    [[aName uppercaseString] getCString:(char*)block+index
                              maxLength:kMaxDataBlockSize-index
                               encoding:kUnicodeEncoding];
    
    ///plus \0 length
    index+=aName.length*2+2;
    
    NSString * domain = aDomain;
    if (domain == nil) {
        domain = @"";
    }
    
    [domain getCString:(char*)block+index
             maxLength:kMaxDataBlockSize-index
              encoding:kUnicodeEncoding];
    index+=domain.length*2+2;
    
    [nativeOS getCString:(char*)block+index
               maxLength:kMaxDataBlockSize-index
                encoding:kUnicodeEncoding];
    index+=nativeOS.length*2+2;
    
    [nativeLanMan getCString:(char*)block+index
                   maxLength:kMaxDataBlockSize-index
                    encoding:kUnicodeEncoding];
    index+=nativeLanMan.length*2+2;
    
    return index;
}

- (uint16_t)getActionFromParemeters:(const void*) parameter {
    if (parameter == NULL) {
        return 0;
    }
    return *((const char*)parameter+kSMBParametersAction);
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_SESSION_SETUP_ANDX) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    ///The content of TID/UID fields has not yet been determined, let's move on.
    if ([CZSMBErrorHandler getNTStatusLevel:status] == CZSMBNTStatusLevelNone) {
        CZLog(@"SessionSetup success");
        self.connection.isAuthenticated = YES;
    }else {
        CZLog(@"authenticate failure");
        [self.connection disconnectHost];
        self.connection.isAuthenticated = NO;
        self.connection.isNegotiated = NO;
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    const void *parameter = NULL;
    int length = [respPacket getParametersNoCopy:&parameter];
    ///Only 3 words in this response.
//    assert(length==3);
//    assert(parameter != NULL);
    if (parameter == NULL && length != 3) {
        [self.connection disconnectHost];
        self.connection.isAuthenticated = NO;
        self.connection.isNegotiated = NO;
        CZPrintError(status);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint16_t action = [self getActionFromParemeters:parameter];
    if (action&kSMB_SETUP_GUEST) {
        ///The user is logged in as Guest.
        CZLog(@"Logged in as guest");
    }else {
        CZLog(@"The user successfully authenticated and is logged in");
    }
    
/**
 * Notes:This flag has addtional specification in MS-SMB.pdf
    This bit is not used with extended security and MUST be clear.
 */
    if (action&kSMB_SETUP_USE_LANMAN_KEY) {
        CZLog(@"The LM session key will be used for message signing");
    }else {
        CZLog(@"The NTLM user session key will be used for message signing (if enabled)");
    }
/**
 * We have login server now, user id must be assigned by server.
 */ 
    self.connection.userID = *respPacket.getSMBUID;
    self.connection.treeID = *respPacket.getSMBTID;
    self.connection.processID = *respPacket.getSMBPIDLow;
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
