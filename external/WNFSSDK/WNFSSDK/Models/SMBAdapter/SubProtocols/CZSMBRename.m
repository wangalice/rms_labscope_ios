//
//  CZSMBRename.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/10/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBRename.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"

@implementation CZSMBRename


- (int)eventFlag {
    return kEventFlagRename;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (_oldFileName == nil || [_oldFileName length] < 1) {
        self.errorCode = kErrorCodeFileNameInvalid;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_RENAME];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    uint16_t attributes = self.searchAttributes;
    [reqPacket setParameterWithUint16:&attributes
                               offset:0];
    
    ///This field MUST be 0x01.
    [reqPacket setParameterCount:0x01];
    
    /// Plus two null-terminated
    size_t byteCount = _oldFileName.length + _fileName.length + 2;
    if (connection.isUnicodeSupported) {
        byteCount *= 2;
    }
    /// 2 byte for BufferFormat
    byteCount += 2;
    /// add buffers for data align
    byteCount += 4;
    uint8_t *dataBlock = malloc(byteCount);
    memset(dataBlock, 0, byteCount);
    int index = 1;
    ///BufferFormat (1 byte): This field MUST be 0x04.
    memset(dataBlock, 0x04, 1);
    if (connection.isUnicodeSupported) {
        [_oldFileName getCString:(char*)dataBlock+index
                       maxLength:byteCount-index
                        encoding:kUnicodeEncoding];
        index += (_oldFileName.length + 1) *2;
    }else {
        [_oldFileName getCString:(char*)dataBlock+index
                       maxLength:byteCount-index
                        encoding:NSASCIIStringEncoding];
        index += _oldFileName.length + 1;
    }
    
    index += (4 - index%4);
    ///BufferFormat (1 byte): This field MUST be 0x04.
    memset(dataBlock+index, 0x04, 1);
    index ++;
    
    if (connection.isUnicodeSupported) {
        [_fileName getCString:(char*)dataBlock+index
                    maxLength:byteCount-index
                     encoding:kUnicodeEncoding];
        index += (_fileName.length + 1) *2;
    }else {
        [_fileName getCString:(char*)dataBlock+index
                    maxLength:byteCount-index
                     encoding:NSASCIIStringEncoding];
        index += (_fileName.length + 1) *2;
    }
    
    [reqPacket setDataBlock:dataBlock
                  byteCount:index];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
    free(dataBlock);
}


- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_RENAME) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
