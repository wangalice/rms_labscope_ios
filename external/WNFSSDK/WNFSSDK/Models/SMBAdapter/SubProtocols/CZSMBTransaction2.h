//
//  CZSMBTransaction2.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

/**
 * This transaction is used to begin a search for file(s) within a directory or for a directory. The search can be continued if necessary with the TRANS2_FIND_NEXT2 command. There are several levels of information that can be queried for the returned files or directories. The information level is specified in the InformationLevel field of the Trans2_Parameters (see following), and each information level has a unique response format.
 */

#import "CZSMBEvents.h"
#import "CZSMBSubCommandDelegate.h"

#define kSMBRequestParametersTotalParameterCount    (0)
#define kSMBRequestParametersTotalDataCount			(2)
#define kSMBRequestParametersMaxParameterCount      (4)
#define kSMBRequestParametersMaxDataCount			(6)
#define kSMBRequestParametersMaxSetupCount          (8)
#define kSMBRequestParametersReserved1              (9)
#define kSMBRequestParametersFlags                  (10)
#define kSMBRequestParametersTimeout                (12)
#define kSMBRequestParametersReserved2              (16)
#define kSMBRequestParametersParameterCount         (18)
#define kSMBRequestParametersParameterOffset        (20)
#define kSMBRequestParametersDataCount              (22)
#define kSMBRequestParametersDataOffset             (24)
#define kSMBRequestParametersSetupCount             (26)
#define kSMBRequestParametersReserved3              (27)
#define kSMBRequestParametersSetup                  (28)

///primary parameter
#define kSMBResponseParametersTotalParameterCount   (0)
#define kSMBResponseParametersTotalDataCount        (2)
#define kSMBResponseParametersParameterCount        (6)
#define kSMBResponseParametersParameterOffset       (8)
#define kSMBResponseParametersDataCount             (12)
#define kSMBResponseParametersDataOffset            (14)


@interface CZSMBTransaction2 : CZSMBEvents<CZSMBEventsDelegate>

@property (nonatomic,strong) id<CZSMBSubCommandDelegate>    subCommand;

@end
