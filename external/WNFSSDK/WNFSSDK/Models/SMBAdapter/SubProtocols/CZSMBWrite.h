//
//  CZSMBWrite.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBEvents.h"

@class CZFileDescriptor;

@interface CZSMBWrite : CZSMBEvents<CZSMBEventsDelegate>
@property (nonatomic, strong) CZFileDescriptor *fileDescriptor;
@end
