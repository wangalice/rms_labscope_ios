//
//  CZSMBEventsDelegate.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZSMBConnection;
@protocol CZSMBEventsDelegate <NSObject>
@required
- (int) eventFlag;
- (void)startRequest:(CZSMBConnection*) connection;
- (void)dataParse:(NSData*) data;
@end
