//
//  CZSMBFindClose2.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBFindClose2.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"

@interface CZSMBFindClose2() {
    
}

@end

@implementation CZSMBFindClose2
- (int)eventFlag {
    return kEventFlagFindClose2;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.searchID == 0xFFFF) {
        self.errorCode = kErrorCodeSearchIDInvalid;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_FIND_CLOSE2];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    uint16_t sID = self.searchID;
    [reqPacket setParameterWithUint16:&sID
                               offset:0];
    
    [reqPacket setParameterCount:0x01];
    [reqPacket setDataBlock:NULL
                  byteCount:0];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
}

- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_FIND_CLOSE2) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
//        CZLog(@"SMB_COM_FIND_CLOSE2 success");
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
