//
//  CZSMBDelete.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBDelete.h"
#import "CZGlobal.h"
#import "CZSMBRequestPacket.h"
#import "CZSMBResponsePacket.h"
#import "CZSMBConnection.h"
#import "CZSMBDirectTCPTransport.h"

@interface CZSMBDelete() {
    
}

@end

@implementation CZSMBDelete


- (int)eventFlag {
    return kEventFlagDelete;
}

- (void)startRequest:(CZSMBConnection *)connection {
    if (self.fileName == nil || [self.fileName length] < 1) {
        self.errorCode = kErrorCodeFileNameInvalid;
        [self setState:CZSMBEventsStateError];
    }
    self.connection = connection;
    
    CZSMBRequestPacket *reqPacket = [[CZSMBRequestPacket alloc] init];
    [reqPacket setSMBCommand:kSMB_COM_DELETE];
    [reqPacket setCommonFlag:connection];
    [reqPacket setSMBMID:self.multiplexID];
    
    uint16_t attributes = self.searchAttributes;
    [reqPacket setParameterWithUint16:&attributes
                               offset:0];
    
    ///This field MUST be 0x01.
    [reqPacket setParameterCount:0x01];
    
    /// Plus a null-terminated
    size_t byteCount = self.fileName.length + 1;
    if (connection.isUnicodeSupported) {
        byteCount *= 2;
    }
    /// 1 byte is BufferFormat
    byteCount += 1;
    uint8_t *dataBlock = malloc(byteCount);
    memset(dataBlock, 0, byteCount);
    ///BufferFormat (1 byte): This field MUST be 0x04.
    memset(dataBlock, 0x04, 1);
    
    if (connection.isUnicodeSupported) {
        [self.fileName getCString:(char*)dataBlock+1
                        maxLength:byteCount-1
                         encoding:kUnicodeEncoding];
    }else {
        [self.fileName getCString:(char*)dataBlock+1
                        maxLength:byteCount-1
                         encoding:NSASCIIStringEncoding];
    }
    
    [reqPacket setDataBlock:dataBlock
                  byteCount:byteCount];
    
    if (![connection sendDirectTCPPacket:reqPacket.packetData]) {
        self.errorCode = kErrorCodeHostUnreachable;
        [self setState:CZSMBEventsStateError];
    }else {
        [self setTimeout];
    }
    free(dataBlock);
}


- (void)dataParse:(NSData*) data{
    [self clearTimeout];
    
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    if (*[respPacket getSMBCommand] != kSMB_COM_DELETE) {
        CZLog(@"unexpected command:%x",*[respPacket getSMBCommand]);
        self.errorCode = kErrorCodePacketError;
        [self setState:CZSMBEventsStateError];
        return;
    }
    uint32_t status = *respPacket.getSMBStatus;
    if ([CZSMBErrorHandler getNTStatusLevel:status] != CZSMBNTStatusLevelError) {
    }else {
        CZPrintError(status);
        self.errorCode = status;
        [self setState:CZSMBEventsStateError];
        return;
    }
    
    [self setState:CZSMBEventsStateSuccess];
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    [self clearTimeout];
    self.errorCode = code;
    [self setState:CZSMBEventsStateError];
}
@end
