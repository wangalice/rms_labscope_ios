//
//  CZSMBTreeDisconnect.h
//  WNFSSDK
//
//  Created by Ralph Jin on 6/17/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZSMBEvents.h"

@interface CZSMBTreeDisconnect : CZSMBEvents<CZSMBEventsDelegate>

@end
