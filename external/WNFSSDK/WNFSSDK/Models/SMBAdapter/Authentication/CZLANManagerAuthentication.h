//
//  CZLANManagerAuthentication.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZLANManagerAuthentication : NSObject

/**
 * LM/NTLM computational formula.
 *
 *  NT-Hash=MD4(UTF-16-LE(password))
 *  C = 8-byte server challenge, random
 *  K1 | K2 | K3 = NT-Hash | 5-bytes-0
 *  R1 = DES(K1,C) | DES(K2,C) | DES(K3,C)
 *
 *  K1 | K2 | K3 = LM-Hash | 5-bytes-0
 *  R2 = DES(K1,C) | DES(K2,C) | DES(K3,C)
 *  response = R1 | R2
 */

/**
 * Generates the response in the LM challenge/response security.
 *
 *  LM-Hash=DESeach(DOSCHARSET(UPPERCASE(password)), "KGS!@#$%")
 *  C = 8-byte server challenge, random
 *  K1 | K2 | K3 = LM-hash | 5-bytes-0
 *  R2 = DES(K1,C) | DES(K2,C) | DES(K3,C)
 *  response = R2
 */
- (BOOL)encryptPassword:(NSString *)secret lmChallenge:(const uint8_t *)challenge toResponse:(uint8_t *)response;

/**
 * Generates the response in the NTLM v1 challenge/response security.
 *
 *  NT-Hash=MD4(UTF-16-LE(password))
 *  C = 8-byte server challenge, random
 *  K1 | K2 | K3 = NT-Hash | 5-bytes-0
 *  R1 = DES(K1,C) | DES(K2,C) | DES(K3,C)
 *  response = R1
 */
- (BOOL)encryptPassword:(NSString *)secret ntlmChallenge:(const uint8_t *)challenge toResponse:(uint8_t *)response;

/**
 * NTLM v2 computational formula.
 *
 *  SC = 8-byte server challenge, random
 *  CC = 8-byte client challenge, random
 *  CC* = (X, time, CC, domain name)
 *  v2-Hash = HMAC-MD5(NT-Hash, user name, domain name)
 *  LMv2 = HMAC-MD5(v2-Hash, SC, CC)
 *  NTv2 = HMAC-MD5(v2-Hash, SC, CC*)
 *  response = LMv2 | CC | NTv2 | CC*
 */

/**
 * Generates the response in the LMv2 challenge/response security.
 *
 *  SC = 8-byte server challenge, random
 *  CC = 8-byte client challenge, random
 *  v2-Hash = HMAC-MD5(NT-Hash, user name, domain name)
 *  LMv2 = HMAC-MD5(v2-Hash, SC, CC)
 */
- (NSUInteger)encryptPassword:(NSString *)secret
                 domain:(NSString *)domain
                   user:(NSString *)userName
          lmv2Challenge:(const uint8_t*)serverChallenge
    lmv2ChallengeLength:(NSUInteger)lmv2ChallengeLength
              challenge:(const uint8_t *)clientChallenge
             targetInfo:(const uint8_t *)targetInfo
       targetInfoLength:(NSUInteger)targetInfoLength
             toResponse:(uint8_t *) response;

@end
