//
//  CZLANManagerAuthentication.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/7/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZLANManagerAuthentication.h"
#import "CZSMBConsistents.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation CZLANManagerAuthentication

/**
 * Calculates MD4 hash.
 */
- (void)MD4:(void*)data output:(uint8_t*)bytes length:(CC_LONG)length {
    CC_MD4_CTX ctx;
    CC_MD4_Init(&ctx);
    CC_MD4_Update(&ctx, data, (uint32_t)length);
    CC_MD4_Final(bytes, &ctx);
}

/**
 * Calculates the number of bits set (1) in a byte.
 */
- (int)computeParity:(uint8_t)secret {
	int num_bits_set = 0;
	for (int i=0; i<8; i++) {
		num_bits_set += (secret >> i & 0x01);
	}
	return num_bits_set % 2 == 1;
}

/**
 * Expands a 7-byte DES key to 8-byte.
 */
- (void)convert7Byte:(uint8_t*)inKey to8Byte:(uint8_t*)outKey {
	uint8_t window[2] = {0,0};
	
	uint8_t tmp;
	for (int i=0; i<8; i++) {
		if (i==0) {
			window[0] = inKey[0];
		}
		else if (i==7) {
			window[0] = 0;
			window[1] = inKey[6];
		}
		else {
			window[0] = inKey[i];
			window[1] = inKey[i-1];
		}
		
		tmp = 0xFE & (*((uint16_t*)window) >> i);
		outKey[i] = (tmp & 0xFE) + (1 - [self computeParity:tmp]);
	}
}

/**
 * The lmHash size should big than secret. eg:lmHash[8] secret[7];
 */
- (BOOL)generateLMHash:(uint8_t*)lmHash forSecret:(uint8_t*) secret secretLength:(int) secretLength encrypt:(const uint8_t*) encrypt encryptLength:(int) encryptLength{
    if (lmHash == NULL) {
        return NO;
    }
    BOOL result = YES;
    uint8_t keypairExpanded[8];
    uint8_t lmHashBuffer[8];

	CCCryptorStatus status;
    size_t movedBytes = 0;
    
    //At last twice encrypt
    int len = secretLength;
    int max = MAX(len,14);
    int count = max/7;
    if ((max%7) > 0) {
        count++;
    }
    
    for (int i = 0; i < count; i++) {
        //Expand 7-byte DES keys to 8-byte keys.
        [self convert7Byte:secret+i*7 to8Byte:keypairExpanded];
        
        status = CCCrypt(kCCEncrypt,
                         kCCAlgorithmDES,
                         kCCOptionPKCS7Padding,
                         keypairExpanded,
                         kCCKeySizeDES,
                         NULL,
                         encrypt,
                         encryptLength,
                         lmHashBuffer,
                         16,
                         &movedBytes);
        
        if (status != kCCSuccess) {
            result = NO;
            break;
        }
        memcpy(lmHash+i*8, lmHashBuffer, 8);
    }
    
    return result;
}

- (BOOL)encryptPassword:(NSString*) secret lmChallenge:(const uint8_t*) challenge toResponse:(uint8_t*) response {
    uint8_t lmHashCompressed[21];
	uint8_t lmHashExpanded[24];
	
    uint8_t secretCompressed[14];
    memset(lmHashCompressed, 0, 21);
    memset(lmHashExpanded, 0, 24);
	memset(secretCompressed, 0, 14);
    if (secret == nil) {
        return NO;
    }
	//This password is null-padded to 14 bytes.
    NSString *_secret = [secret uppercaseString];
    if (_secret.length > 13) {
        _secret = [secret substringToIndex:13];
    }
    [_secret getCString:(char*)secretCompressed maxLength:14 encoding:NSASCIIStringEncoding];
    // Encrypt with KGS!@#$%, which could possibly mean Key of Glen and Steve and then the combination of Shift + 12345.
    char *encryptString = "KGS!@#$%";
	// Generate LM-Hash for secret
    if (![self generateLMHash:lmHashCompressed
                    forSecret:secretCompressed
                 secretLength:14
                      encrypt:(uint8_t*)encryptString
                encryptLength:8]) {
        return NO;
    }
    memset(&lmHashCompressed[16], 0, 5);
    if (![self generateLMHash:lmHashExpanded
                    forSecret:lmHashCompressed
                 secretLength:21
                      encrypt:challenge
                encryptLength:8]) {
        return NO;
    }
	
    memcpy(response, lmHashExpanded, 24);
    return YES;
}

- (BOOL)encryptPassword:(NSString*) secret ntlmChallenge:(const uint8_t*) challenge toResponse:(uint8_t*) response {
    uint8_t lmHashCompressed[21];
	uint8_t lmHashExpanded[24];
    memset(lmHashCompressed, 0, 21);
    memset(lmHashExpanded, 0, 24);
    [self computeNTOWFv1With:secret toHash:lmHashCompressed];

    memset(lmHashCompressed + 16, 0, 5);
    
    if (![self generateLMHash:lmHashExpanded
                    forSecret:lmHashCompressed
                 secretLength:21
                      encrypt:challenge
                encryptLength:8]) {
        return NO;
    }
    memcpy(response, lmHashExpanded, 24);
    return YES;
}

- (void)computeNTOWFv1With:(NSString*) password toHash:(uint8_t*) hash16{
    uint8_t lmHash[16];
    uint8_t pwdBuffer[128];
    assert([password length]<128);
    [password getCString:(char*)pwdBuffer maxLength:128 encoding:kUnicodeEncoding];
    //Note that the string terminator is not counted in the KeySize. That is common behavior for NTLM and NTLMv2 challenge/response when working with Unicode strings.
    [self MD4:pwdBuffer output:lmHash length:(CC_LONG)[password length]*2];
    assert(CC_MD4_DIGEST_LENGTH == 16);
    memcpy(hash16, lmHash, CC_MD4_DIGEST_LENGTH);
}

- (void)computeNTOWFv2With:(NSString*) userName password:(NSString*) password domain:(NSString*)domain toHash:(uint8_t*) hash16{
    uint8_t lmHash[16];
    uint8_t secretBuffer[256];
    assert([password length]<256);
    [password getCString:(char*)secretBuffer maxLength:256 encoding:kUnicodeEncoding];
    [self MD4:secretBuffer output:lmHash length:(CC_LONG)[password length]*2];
    
    uint8_t userNameBytes[128];
    uint8_t domainBytes[128];
    
    [userName getCString:(char*)userNameBytes maxLength:128 encoding:kUnicodeEncoding];
    [domain getCString:(char*)domainBytes maxLength:128 encoding:kUnicodeEncoding];
    CCHmacContext    ctx;
    uint8_t macHash[CC_MD5_DIGEST_LENGTH];
    memset(macHash, 0, CC_MD5_DIGEST_LENGTH);
    CCHmacInit(&ctx, kCCHmacAlgMD5, lmHash, 16);
    CCHmacUpdate(&ctx, userNameBytes, [userName length]*2);
    CCHmacUpdate(&ctx, domainBytes, [domain length]*2);
    CCHmacFinal( &ctx, macHash);
    assert(CC_MD5_DIGEST_LENGTH == 16);
    memcpy(hash16, macHash, CC_MD5_DIGEST_LENGTH);
}

- (void)computeNTLM2With:(uint8_t*) ntowfv1 challenge:(uint8_t*) serverChallenge andChallenge:(uint8_t*) clientChallange toResponse:(uint8_t*) response {
    uint8_t lmHash8[8];
    uint8_t lmHash16[16];
    uint8_t lmHashCompressed[21];
    uint8_t lmHashExpanded[24];
    
    CC_MD5_CTX ctx;
    CC_MD5_Init(&ctx);
    CC_MD5_Update(&ctx, serverChallenge, (CC_LONG)strlen((char*)serverChallenge));
    CC_MD5_Update(&ctx, clientChallange, (CC_LONG)strlen((char*)clientChallange));
    CC_MD5_Final(lmHash16, &ctx);
    
    memcpy(lmHash8, lmHash16, 8);
    memcpy(lmHashCompressed, ntowfv1, 16);
    memset(lmHashCompressed + 16, 0, 5);
    [self generateLMHash:lmHashExpanded
               forSecret:lmHashCompressed
            secretLength:21
                 encrypt:lmHash8
           encryptLength:8];
    memcpy(response, lmHashExpanded, 24);
}

/**
 * Calculates the HMAC-MD5 hash.
 */
- (BOOL)encryptKey:(uint8_t*)key size:(int)keySize withData:(const uint8_t *)data size:(int)dataSize toHash:(uint8_t *)hash16{
    size_t dataLength = strlen((char*)data);
    assert(dataLength <= 64);
    CCHmacContext    ctx;
    uint8_t macHash[CC_MD5_DIGEST_LENGTH];
    memset(macHash, 0, CC_MD5_DIGEST_LENGTH);
    CCHmacInit(&ctx, kCCHmacAlgMD5, key, strlen((char*)key));
    CCHmacUpdate(&ctx, data, dataLength);
    CCHmacFinal( &ctx, macHash);
    memcpy(hash16, macHash, CC_MD5_DIGEST_LENGTH);
    return YES;
}

- (NSUInteger)encryptPassword:(NSString *)secret
                 domain:(NSString *)domain
                   user:(NSString *)userName
          lmv2Challenge:(const uint8_t *)serverChallenge
    lmv2ChallengeLength:(NSUInteger)lmv2ChallengeLength
              challenge:(const uint8_t *)clientChallenge
             targetInfo:(const uint8_t *)targetInfo
       targetInfoLength:(NSUInteger) targetInfoLength
             toResponse:(uint8_t *) response {
    uint8_t lmHash[16];
    [self computeNTOWFv1With:secret toHash:lmHash];
    uint8_t userNameBytes[128];
    uint8_t domainBytes[128];
    
    [[userName uppercaseString] getCString:(char*)userNameBytes maxLength:128 encoding:kUnicodeEncoding];
    [domain getCString:(char*)domainBytes maxLength:128 encoding:kUnicodeEncoding];
    
    CCHmacContext    ctx,ctx1;
    uint8_t macHash[CC_MD5_DIGEST_LENGTH];
    uint8_t macHash1[CC_MD5_DIGEST_LENGTH];
    memset(macHash, 0, CC_MD5_DIGEST_LENGTH);
    memset(macHash1, 0, CC_MD5_DIGEST_LENGTH);
    CCHmacInit(&ctx, kCCHmacAlgMD5, lmHash, 16);
    CCHmacUpdate(&ctx, userNameBytes, [userName length]*2);
    CCHmacUpdate(&ctx, domainBytes, [domain length]*2);
    CCHmacFinal(&ctx, macHash);
    
    const uint32_t blobSignatureLength = 4;
    const uint32_t reserveMsgLength = 4;
    const uint32_t unknownMsgLength = 4;
    const uint32_t timeStampMsgLength = 8;
    const uint32_t clientChallengelength = 8;
    
    const NSUInteger clientDataLength = blobSignatureLength + reserveMsgLength + timeStampMsgLength + clientChallengelength + unknownMsgLength + targetInfoLength + 2*unknownMsgLength;
    
    uint8_t clientData[clientDataLength];
    memset(clientData, 0, clientDataLength);
    
    int index = 0;
    //header message
    const uint8_t reqMsg1[8] = {
        0x01, 0x01, 0x0, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    
    memcpy(clientData+index, reqMsg1, 8);
    index += 8;
    
    
    NSDate *nowDate = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    long long time = [nowDate timeIntervalSince1970];
    time *= 1000; // milliseconds
    time += 11644473600000l; // milliseconds from January 1, 1601 -> epoch.
    time *= 10000; // tenths of a microsecond.
    
    
    memcpy(clientData+index, &time, sizeof(long long));
    index += sizeof(long long);
    
    memcpy(clientData+index, clientChallenge, clientChallengelength);
    index += clientChallengelength;
    
    const uint8_t reqMsg2[4] = {
        0x00, 0x00, 0x00, 0x00
    };
    memcpy(clientData+index, reqMsg2, unknownMsgLength);
    index += unknownMsgLength;
    
    memcpy(clientData+index, targetInfo, targetInfoLength);
    index += targetInfoLength;
    
    memcpy(clientData+index, reqMsg2, unknownMsgLength);
    index += unknownMsgLength;
    
    memcpy(clientData+index, reqMsg2, unknownMsgLength);
    
    CCHmacInit(&ctx1, kCCHmacAlgMD5, macHash, CC_MD5_DIGEST_LENGTH);
    CCHmacUpdate(&ctx1, serverChallenge, lmv2ChallengeLength);
    CCHmacUpdate(&ctx1, clientData, clientDataLength);
    CCHmacFinal( &ctx1, macHash1);
    
    memcpy(response, macHash1, CC_MD5_DIGEST_LENGTH);
    memcpy(response + CC_MD5_DIGEST_LENGTH, clientData, clientDataLength);
    
    return clientDataLength+CC_MD5_DIGEST_LENGTH;
}
@end
