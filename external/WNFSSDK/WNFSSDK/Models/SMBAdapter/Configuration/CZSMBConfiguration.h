//
//  SMBConfiguration.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#ifndef WNFSSDK_SMBConfiguration_h
#define WNFSSDK_SMBConfiguration_h

/**
 * The windows PC/server shared port, which use in host directing mode.
 */
#define kDirectHostPortNumber       445

/**
 * Define the SMB connecting timeout, unit is seconds.
 */
#define kSMBConnectingTimeOut       10.0f

/**
 * Define the SMB packet transfer timeout, unit is seconds.
 */
#define kSMBPacketTransferTimeOut       60.0f

/**
 * Define the SMB packet timeout (interval between SMB request and SMB response), unit is seconds.
 */
#define kSMBEventTimeOut            15.0f

#endif
