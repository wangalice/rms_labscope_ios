//
//  CZFileSystemSize.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZDiskInformation.h"

@implementation CZDiskInformation

- (id)initWithTotalAllocationSize:(uint64_t)totalAllocationSize
          totalFreeAllocationSize:(uint64_t)totalFreeAllocationSize {
    self = [super init];
    if (self) {
        _totalAllocationSize = totalAllocationSize;
        _totalFreeAllocationSize = totalFreeAllocationSize;
    }
    return self;
}

@end
