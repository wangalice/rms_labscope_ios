//
//  CZSMBRequestPacket.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZServerMessagePacket.h"

//  uint16_t AndXCommand;
#define kSMBRequestParametersAndXCommand			(0)
//  uchar  AndXReserved;
#define kSMBRequestParametersAndXReserved			(1)
//  uint16_t AndXOffset;
#define kSMBRequestParametersAndXOffset             (2)
//  uint16_t MaxBufferSize;
#define kSMBRequestParametersMaxBufferSize			(4)
//  uint16_t  MaxMpxCount;
#define kSMBRequestParametersMaxMpxCount            (6)
//  uint16_t  VcNumber;
#define kSMBRequestParametersVcNumber               (8)
//  ulong  SessionKey;          /* Unique session ID         */
#define kSMBRequestParametersSessionKey             (10)
//if Extended Security has been negotiated then the Lengths field is a single uint16_t, known as SecurityBlobLength in the MS-SMB.pdf, This value MUST specify the length in bytes of the variable-length SecurityBlob field that is contained within the request.
#define kSMBRequestParametersSecurityBlobLength     (14)
#define kSMBRequestParametersCaseInsensitivePasswordLength  (14)
#define kSMBRequestParametersCaseSensitivePasswordLength    (16)
//  ulong
#define kSMBRequestParametersReserved               (16)
//OEMPasswordLen (2 bytes): The length, in bytes, of the contents of the SMB_Data.OEMPassword field.
//UnicodePasswordLen (2 bytes): The length, in bytes, of the contents of the SMB_Data.UnicodePassword field.
//  ulong  Capabilities;        /* Server capabilities flags */
#define kSMBRequestParametersCapabilities			(20)

@class CZSMBConnection;
@interface CZSMBRequestPacket : CZServerMessagePacket

@property (nonatomic,readwrite) u_char       andXCommand;
@property (nonatomic,readwrite) u_char       andXReserved;
@property (nonatomic,readwrite) uint16_t      andXOffset;
@property (nonatomic,readwrite) uint16_t      maxBufferSize;
@property (nonatomic,readwrite) uint16_t      maxMpxCount;
@property (nonatomic,readwrite) uint16_t      vcNumber;
@property (nonatomic,readwrite) uint32_t     sessionKey;
@property (nonatomic,readwrite) uint32_t     reserved;
@property (nonatomic,readwrite) uint32_t     capabilities;
@property (nonatomic,readwrite) uint16_t      securityBlobLength;
@property (nonatomic,readwrite) uint16_t      caseInsensitivePasswordLength;
@property (nonatomic,readwrite) uint16_t      caseSensitivePasswordLength;

- (void)setCommonFlag:(CZSMBConnection *) connection;
- (void)setCommonFlag2:(CZSMBConnection *) connection;
@end
