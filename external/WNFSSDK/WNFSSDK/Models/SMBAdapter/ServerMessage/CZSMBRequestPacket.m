//
//  CZSMBRequestPacket.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBRequestPacket.h"
#import "CZSMBConnection.h"

@interface CZSMBRequestPacket() {
    int    bufferOffset;
}

@end

@implementation CZSMBRequestPacket

- (id)init {
    if (self  = [super init]) {
        [self performSelector:@selector(packetInit)];
    }
    return self;
}

#pragma mark - Properties Methods

- (void)setAndXCommand:(u_char)andXCommand {
    [self setParameter:&andXCommand
                offset:kSMBRequestParametersAndXCommand
                length:sizeof(u_char)];
}

- (void)setAndXReserved:(u_char)andXReserved {
    [self setParameter:&andXReserved
                offset:kSMBRequestParametersAndXReserved
                length:sizeof(u_char)];
}

- (void)setAndXOffset:(uint16_t)andXOffset {
    [self setParameter:&andXOffset
                offset:kSMBRequestParametersAndXOffset
                length:sizeof(uint16_t)];
}

//- (void)setSecurityMode:(u_char)securityMode {
//    [self setParameter:&securityMode
//                offset:kSMBRequestParametersSecurityMode
//                length:sizeof(u_char)];
//}

- (void)setMaxMpxCount:(uint16_t)maxMpxCount {
    [self setParameter:&maxMpxCount
                offset:kSMBRequestParametersMaxMpxCount
                length:sizeof(uint16_t)];
}

- (void)setVcNumber:(uint16_t)vcNumber {
    [self setParameter:&vcNumber
                offset:kSMBRequestParametersVcNumber
                length:sizeof(uint16_t)];
}

- (void)setMaxBufferSize:(uint16_t)maxBufferSize {
    [self setParameter:&maxBufferSize
                offset:kSMBRequestParametersMaxBufferSize
                length:sizeof(uint16_t)];
}

- (void)setSessionKey:(uint32_t)sessionKey {
    [self setParameter:&sessionKey
                offset:kSMBRequestParametersSessionKey
                length:sizeof(uint32_t)];
}

- (void)setSecurityBlobLength:(uint16_t)securityBlobLength {
    bufferOffset = 0;
    [self setParameter:&securityBlobLength
                offset:kSMBRequestParametersSecurityBlobLength
                length:sizeof(uint16_t)];
}

- (void)setCaseInsensitivePasswordLength:(uint16_t)caseInsensitivePasswordLength {
    bufferOffset = sizeof(uint16_t);
    [self setParameter:&caseInsensitivePasswordLength
                offset:kSMBRequestParametersCaseInsensitivePasswordLength
                length:sizeof(uint16_t)];
}

- (void)setCaseSensitivePasswordLength:(uint16_t)caseSensitivePasswordLength {
    bufferOffset = sizeof(uint16_t);
    [self setParameter:&caseSensitivePasswordLength
                offset:kSMBRequestParametersCaseSensitivePasswordLength
                length:sizeof(uint16_t)];
}

- (void)setReserved:(uint32_t)reserved {
    [self setParameter:&reserved
                offset:kSMBRequestParametersReserved+bufferOffset
                length:sizeof(uint32_t)];
}

- (void)setCapabilities:(uint32_t)capabilities {
    [self setParameter:&capabilities
                offset:kSMBRequestParametersCapabilities+bufferOffset
                length:sizeof(uint32_t)];
}

- (void)setCommonFlag:(CZSMBConnection*) connection {
    uint16_t flag2 = kSMB_FLAGS2_LONG_NAMES;
    flag2 |= kSMB_FLAGS2_NT_STATUS;
    if (connection.isUnicodeSupported) {
        flag2 |= kSMB_FLAGS2_UNICODE;
    }
    if (connection.serverCapabilities&kCAP_LARGE_READX) {
        flag2 |= kCAP_LARGE_READX;
    }
    if (connection.serverCapabilities&kCAP_LARGE_WRITEX) {
        flag2 |= kCAP_LARGE_WRITEX;
    }
    [self setSMBFlags2:flag2];
    [self setSMBTID:connection.treeID];
    [self setSMBUID:connection.userID];
    [self setSMBPIDLow:connection.processID];
}

- (void)setCommonFlag2:(CZSMBConnection *) connection {
    uint16_t flag2 = kSMB_FLAGS2_LONG_NAMES;
    flag2 |= kSMB_FLAGS2_NT_STATUS;
    if (connection.isUnicodeSupported) {
        flag2 |= kSMB_FLAGS2_UNICODE;
    }
    if (connection.serverCapabilities&kCAP_LARGE_READX) {
        flag2 |= kCAP_LARGE_READX;
    }
    if (connection.serverCapabilities&kCAP_LARGE_WRITEX) {
        flag2 |= kCAP_LARGE_WRITEX;
    }
    [self setSMBFlags:0x18];
    [self setSMBFlags2:flag2];
    [self setSMBTID:connection.treeID];
    [self setSMBUID:connection.userID];
    [self setSMBPIDLow:connection.processID];
}

@end
