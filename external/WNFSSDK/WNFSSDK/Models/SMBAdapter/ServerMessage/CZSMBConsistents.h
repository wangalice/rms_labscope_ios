//
//  CZSMBConsistents.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#ifndef WNFSSDK_CZSMBConsistents_h
#define WNFSSDK_CZSMBConsistents_h

#define kSMBHeaderLength                    32
#define kSMBMinimumParameterLength          1
#define kSMBMinimumDataBlockLength          2

#define kListDirectoryStatusNotification    @"LIST_DIRECTORY_STATUS"

#define kSMBClientOS                        @"iOS"
#define kSMBClientName                      @"Hermes"
#define kUnicodeEncoding                    NSUTF16LittleEndianStringEncoding
#define kMaxMultiplexCount                  5
#define kMaxDataBlockSize                   (512)
#define kMaxAllowParametersBytes            0xFF
#define kMaxTransDataBlockSize              (0xFFF0)
#define kMaxReadDataBlockSize               (0xFFF0)//~63k (not include header size, with header size will be 64K)
#define kMaxWriteDataBlockSize              (0x7C00)//~31k (need consider pad bytes, not include header size, with header size will be 32K)
#define kMaxSearchItemsInDirectory          200
#define kServiceFileSystem                  @"A:"
#define kServiceNamedPipe                   @"IPC"
#define kServiceWildcard                    @"?????"

#define kSMBHeaderProtocolIndex             (0)
#define kSMBHeaderCommandIndex              (4)
#define kSMBHeaderStatusIndex               (5)
#define kSMBHeaderFlagsIndex                (9)
#define kSMBHeaderFlags2Index               (10)
#define kSMBHeaderPIDHightIndex             (12)
#define kSMBHeaderSecurityFeaturesIndex     (14)
#define kSMBHeaderReservedIndex             (22)
#define kSMBHeaderTIDIndex                  (24)
#define kSMBHeaderPIDLowIndex               (26)
#define kSMBHeaderUIDIndex                  (28)
#define kSMBHeaderMIDIndex                  (30)

#pragma mark Commands

#define kSMB_COM_NEGOTIATE                  (0x72)
#define kSMB_COM_SESSION_SETUP_ANDX         (0x73)
#define kSMB_COM_LOGOFF_ANDX                (0x74)
#define kSMB_COM_TREE_CONNECT_ANDX          (0x75)
#define kSMB_COM_TREE_DISCONNECT            (0x71)
#define kSMB_COM_TRANSACTION                (0x25)
#define kSMB_COM_TRANSACTION2               (0x32)
#define kSMB_COM_TRANSACTION2_SECONDARY     (0x33)
#define kSMB_COM_NT_CREATE_ANDX             (0xA2)
#define kSMB_COM_OPEN_ANDX                  (0x2D)
#define kSMB_COM_READ_ANDX                  (0x2E)
#define kSMB_COM_WRITE_ANDX                 (0x2F)
#define kSMB_COM_CLOSE                      (0x04)
#define kSMB_COM_DELETE                     (0x06)
#define kSMB_COM_DELETE_DIRECTORY           (0x01)
#define kSMB_COM_NONE                       (0xFF)
#define kSMB_COM_FIND_CLOSE2                (0x34)
#define kSMB_COM_RENAME                     (0x07)

#pragma mark Subcommands
#define kTRANS2_FIND_FIRST2                 (0x0001)
#define kTRANS2_FIND_NEXT2                  (0x0002)
#define kTRANS2_QUERY_FS_INFO               (0x0003)
#define kTRANS2_QUERY_PATH_INFO             (0x0005)
#define kTRANS2_SET_PATH_INFO               (0x0006)
#define kTRANS2_SET_FILE_INFO               (0x0008)

#define kTRANS_TRANSACT_NMPIPE              (0x0026)
#define kTRANS_TRANSACT_ENUM_SERVER         (0x0001)


#pragma mark File attribute
///Normal file.
#define kSMB_FILE_ATTRIBUTE_NORMAL          (0x0000)
///Read-only file.
#define kSMB_FILE_ATTRIBUTE_READONLY        (0x0001)
///Hidden file.
#define kSMB_FILE_ATTRIBUTE_HIDDEN          (0x0002)
///System file.
#define kSMB_FILE_ATTRIBUTE_SYSTEM          (0x0004)
///Volume Label.
#define kSMB_FILE_ATTRIBUTE_VOLUME          (0x0008)
///Directory file.
#define kSMB_FILE_ATTRIBUTE_DIRECTORY       (0x0010)
///File changed since last archive.
#define kSMB_FILE_ATTRIBUTE_ARCHIVE         (0x0020)
///Search for Read-only files.
#define kSMB_SEARCH_ATTRIBUTE_READONLY      (0x0100)
///Search for Hidden files.
#define kSMB_SEARCH_ATTRIBUTE_HIDDEN        (0x0200)
///Search for System files.
#define kSMB_SEARCH_ATTRIBUTE_SYSTEM        (0x0400)
///Search for Directory files.
#define kSMB_SEARCH_ATTRIBUTE_DIRECTORY     (0x1000)
///Search for files that have changed since they were last archived.
#define kSMB_SEARCH_ATTRIBUTE_ARCHIVE       (0x2000)


#define kSMB_FIND_CONTINUE                  (0x0000)
///Close the search after this request.
#define kSMB_FIND_CLOSE_AFTER_REQUEST       (0x0001)
///Close search when end of search is reached.
#define kSMB_FIND_CLOSE_AT_EOS              (0x0002)
///Return resume keys for each entry found.
#define kSMB_FIND_RETURN_RESUME_KEYS        (0x0004)
///Continue search from previous ending place.
#define kSMB_FIND_CONTINUE_FROM_LAST        (0x0008)
///Find with backup intent.
#define kSMB_FIND_WITH_BACKUP_INTENT        (0x0010)

#pragma mark Information level Codes

///Return creation, access, and last write timestamps, size and file attributes along with the file name.
#define kSMB_INFO_STANDARD                  (0x0001)
///Return the SMB_INFO_STANDARD data along with the size of a file's extended attributes (EAs).
#define kSMB_INFO_QUERY_EA_SIZE             (0x0002)
#define kSMB_SET_FILE_BASIC_INFO            (0x0101)
#define kSMB_QUERY_FILE_BASIC_INFO          (0x0101)
#define kSMB_FIND_FILE_BOTH_DIRECTORY_INFO  (0x0104)

#pragma mark QUERY_FS Information Level Codes
#define kSMB_QUERY_FS_SIZE_INFO             (0x0103)

#pragma mark Flags

#define kSMB_FLAGS_REPLY                    (0x80)


#pragma mark Flags2

#define kSMB_FLAGS2_LONG_NAMES              (0x0001)
#define kSMB_FLAGS2_IS_LONG_NAME            (0x0040)
#define kSMB_FLAGS2_EAS                     (0x0002)
#define kSMB_FLAGS2_SMB_SECURITY_SIGNATURE  (0x0004)
#define kSMB_FLAGS2_NT_STATUS               (0x4000)
#define kSMB_FLAGS2_UNICODE                 (0x8000)


#pragma mark SecurityFeatures

#pragma mark Capabilities

#define kCAP_RAW_MODE                       (0x00000001)
#define kCAP_MPX_MODE                       (0x00000002)

/**
 * The server supports UTF-16LE Unicode strings.
 */
#define kCAP_UNICODE                        (0x00000004)

/**
 * The server supports large files with 64-bit offsets.
 */
#define kCAP_LARGE_FILES                    (0x00000008)

/**
 * The server supports SMB commands particular to the NT LAN Manager dialect.
 */
#define kCAP_NT_SMBS                        (0x00000010)
#define kCAP_RPC_REMOTE_APIS                (0x00000020)

/**
 * The server is capable of responding with 32-bit status codes in the Status field of the SMB header. CAP_STATUS32 can also be referred to as CAP_NT_STATUS.
 */
#define kCAP_STATUS32                       (0x00000040)
#define kCAP_LEVEL_II_OPLOCKS               (0x00000080)

/**
 * The server supports the SMB_COM_LOCK_AND_READ command requests.
 */
#define kCAP_LOCK_AND_READ                  (0x00000100)

/**
 * The server supports the TRANS2_FIND_FIRST2, TRANS2_FIND_NEXT2, and FIND_CLOSE2 command requests. This bit SHOULD<31> be set if CAP_NT_SMBS is set.
 */
#define kCAP_NT_FIND                        (0x00000200)
#define kCAP_DFS                            (0x00001000)
#define kCAP_INFOLEVEL_PASSTHROUGH          (0x00002000)
/**
 * The server supports large read operations. This capability affects the maximum size, in bytes, of the server buffer for sending an SMB_COM_READ_ANDX response to the client. When this capability is set by the server (and set by the client in the SMB_COM_SESSION_SETUP_ANDX request), then the maximum server buffer size for sending data can exceed the MaxBufferSize field. Therefore, the server can send a single SMB_COM_READ_ANDX response to the client up to an implementation-specific default size.
 */
#define kCAP_LARGE_READX                    (0x00004000)

//Extensions
#define kCAP_LARGE_WRITEX                   (0x00008000)
#define kCAP_LWIO                           (0x00010000)

/**
 * Windows-based clients and servers do not support CAP_UNIX; therefore, this capability is never set.
 */
#define kCAP_UNIX                           (0x00800000)
#define kCAP_COMPRESSED_DATA                (0x40000000)
#define kCAP_DYNAMIC_REAUTH                 (0x20000000)

/**
 * The server supports extended security for authentication, and used in conjunction with the SMB_FLAGS2_EXTENDED_SECURITY SMB_Header.Flags2 flag.
 */
#define kCAP_EXTENDED_SECURITY              (0x80000000)



#endif
