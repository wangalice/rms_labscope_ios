//
//  CZServerMessagePacket.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZServerMessagePacket.h"

@interface CZServerMessagePacket() {
    int                 dataBlockIndex;
}

- (void)setSMBProtocol;
@end

@implementation CZServerMessagePacket
@synthesize packetData = _packetData;


- (void)packetInit {
    _packetData = [[NSMutableData alloc] initWithCapacity:kSMBHeaderLength+kSMBMinimumParameterLength+kSMBMinimumDataBlockLength];
    [self setSMBProtocol];
    dataBlockIndex = kSMBHeaderLength+kSMBMinimumParameterLength;
}

- (void)appendBytes:(const void *)bytes length:(NSUInteger)length {
    [_packetData appendBytes:bytes length:length];
}

- (void)replaceBytesInRange:(NSRange)range withBytes:(const void *) replacementBytes length:(NSUInteger)replacementLength {
    if ([_packetData length] <= range.location) {
        NSUInteger padding = range.location - [_packetData length];
        [_packetData increaseLengthBy:padding];
        [_packetData appendBytes:replacementBytes length:replacementLength];
    }else {
        [_packetData replaceBytesInRange:range
                               withBytes:replacementBytes
                                  length:replacementLength];
    }
}

- (void)setSMBProtocol {
    if ([_packetData length] < 1) {
        [_packetData increaseLengthBy:kSMBHeaderLength];
    }
    NSRange range = NSMakeRange(kSMBHeaderProtocolIndex, 4);
    const uint8_t byt[4] = {0xFF,'S','M','B'};
    [self replaceBytesInRange:range withBytes:byt length:4];
}

- (void)setSMBCommand:(u_char) command {
    NSRange range = NSMakeRange(kSMBHeaderCommandIndex, 1);
    [self replaceBytesInRange:range withBytes:&command length:1];
}

- (void)setSMBStatus:(uint32_t) status {
    NSRange range = NSMakeRange(kSMBHeaderStatusIndex, 4);
    [self replaceBytesInRange:range withBytes:&status length:4];
}

- (void)setSMBFlags:(u_char) flag {
    NSRange range = NSMakeRange(kSMBHeaderFlagsIndex, 1);
    [self replaceBytesInRange:range withBytes:&flag length:1];
}

- (void)setSMBFlags2:(uint16_t) flags {
    NSRange range = NSMakeRange(kSMBHeaderFlags2Index, 2);
    [self replaceBytesInRange:range withBytes:&flags length:2];
}

- (void)setSMBPIDHigh:(uint16_t) pid {
    NSRange range = NSMakeRange(kSMBHeaderPIDHightIndex, 2);
    [self replaceBytesInRange:range withBytes:&pid length:2];
}

- (void)setSMBSecurityFeatures {
    NSRange range = NSMakeRange(kSMBHeaderSecurityFeaturesIndex, 8);
    [_packetData resetBytesInRange:range];
}

- (void)setSMBReserved {
    NSRange range = NSMakeRange(kSMBHeaderReservedIndex, 2);
    [_packetData resetBytesInRange:range];
}

- (void)setSMBTID:(uint16_t) tid {
    NSRange range = NSMakeRange(kSMBHeaderTIDIndex, 2);
    [self replaceBytesInRange:range withBytes:&tid length:2];
}

- (void)setSMBPIDLow:(uint16_t) pidLow {
    NSRange range = NSMakeRange(kSMBHeaderPIDLowIndex, 2);
    [self replaceBytesInRange:range withBytes:&pidLow length:2];
}

- (void)setSMBUID:(uint16_t) uid {
    NSRange range = NSMakeRange(kSMBHeaderUIDIndex, 2);
    [self replaceBytesInRange:range withBytes:&uid length:2];
}

- (void)setSMBMID:(uint16_t) mid {
    NSRange range = NSMakeRange(kSMBHeaderMIDIndex, 2);
    [self replaceBytesInRange:range withBytes:&mid length:2];
}

- (void)setParameters:(const uint16_t*) wordValue wordCount:(u_char) count {
    NSRange range = NSMakeRange(kSMBHeaderLength, 1);
    [self replaceBytesInRange:range withBytes:&count length:1];
    range = NSMakeRange(kSMBHeaderLength+1, 2);
    for (u_char i = 0;i < count;i++) {
        [self replaceBytesInRange:range withBytes:&wordValue[i] length:sizeof(uint16_t)];
    }
    dataBlockIndex = kSMBHeaderLength;
    dataBlockIndex += 1;
    //word count = x2 bytes
    dataBlockIndex += count*2;
}

- (void)setParameter:(const void *)byte offset:(NSUInteger)offset length:(NSUInteger)byteLength {
    NSRange range = NSMakeRange(kSMBHeaderLength+1+offset, byteLength);
    [self replaceBytesInRange:range withBytes:byte length:byteLength];
}

- (void)setParameterWithUint8:(const uint8_t *)chr offset:(NSUInteger)offset {
    [self setParameter:chr
                offset:offset
                length:1];
}

- (void)setParameterWithUint16:(const uint16_t *)shor offset:(NSUInteger)offset {
    [self setParameter:shor
                offset:offset
                length:2];
}

- (void)setParameterWithUint32:(const uint32_t *)lon offset:(NSUInteger)offset {
    [self setParameter:lon
                offset:offset
                length:4];
}

- (void)setParameterWithUint64:(const uint64_t*)uin offset:(NSUInteger)offset {
    [self setParameter:uin
                offset:offset
                length:8];
}

- (void)setParameterCount:(u_char) count {
    NSRange range = NSMakeRange(kSMBHeaderLength, 1);
    [self replaceBytesInRange:range withBytes:&count length:1];
    dataBlockIndex = kSMBHeaderLength;
    dataBlockIndex += 1;
    //convert word count to byte
    dataBlockIndex += count*2;
}

- (void)setDataBlock:(const uint8_t*) bytes byteCount:(uint16_t) count {
    dataBlockIndex = MAX(dataBlockIndex, kSMBHeaderLength+kSMBMinimumParameterLength);
    NSRange range = NSMakeRange(dataBlockIndex, 2);
    [self replaceBytesInRange:range withBytes:&count length:2];
    range = NSMakeRange(dataBlockIndex+2, count);
    [self replaceBytesInRange:range withBytes:bytes length:count];
}

- (const u_char*)getSMBCommand {
    return (const u_char*)([_packetData bytes]+kSMBHeaderCommandIndex);
}

- (const uint32_t*)getSMBStatus {
    return (const uint32_t*)([_packetData bytes]+kSMBHeaderStatusIndex);
}

- (const u_char*)getSMBFlags {
    return (const u_char*)([_packetData bytes]+kSMBHeaderFlagsIndex);
}

- (const uint16_t*)getSMBFlags2 {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderFlags2Index);
}

- (const uint16_t*)getSMBPIDHigh {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderPIDHightIndex);
}

- (NSData*)getSMBSecurityFeatures {
    return nil;
}

- (const uint16_t*)getSMBTID {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderTIDIndex);
}

- (const uint16_t*)getSMBPIDLow {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderPIDLowIndex);
}

- (const uint16_t*)getSMBUID {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderUIDIndex);
}

- (const uint16_t*)getSMBMID {
    return (const uint16_t*)([_packetData bytes]+kSMBHeaderMIDIndex);
}

- (u_char)getParameters:(void*) wordValue {
    u_char wordCount = *((const u_char*)([_packetData bytes]+kSMBHeaderLength));
    if (wordValue == NULL) {
        return wordCount;
    }
    memcpy(wordValue, [_packetData bytes]+kSMBHeaderLength+1, wordCount*2);
    return wordCount;
}

- (u_char)getParametersNoCopy:(const void**) wordValue {
    u_char wordCount = *((const u_char*)([_packetData bytes]+kSMBHeaderLength));
    *wordValue = ((const u_char *)[_packetData bytes])+kSMBHeaderLength+1;
    return wordCount;
}

- (void)getDataBlockNoCopy:(NSData **) bytes {
    if (bytes == NULL) {
        return;
    }
    
    ///Word count convert to byte count and plus u_char length
    int index = [self getParameters:NULL] * 2 + 1;
    index += kSMBHeaderLength;
    uint16_t byteCount;
    memcpy(&byteCount, [_packetData bytes]+index, sizeof(byteCount));
    
    NSRange subDataRange = NSMakeRange(index + 2, byteCount);
    if (subDataRange.location + subDataRange.length > _packetData.length) {
        subDataRange.length = _packetData.length - subDataRange.location;
    }

    *bytes = [_packetData subdataWithRange:subDataRange];
}
@end
