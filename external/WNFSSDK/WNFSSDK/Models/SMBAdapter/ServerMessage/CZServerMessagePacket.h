//
//  CZServerMessagePacket.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CZSMBConsistents.h"

@interface CZServerMessagePacket : NSObject {
    
}
@property (nonatomic,readwrite, strong) NSMutableData *   packetData;

- (void)appendBytes:(const void *)bytes length:(NSUInteger)length;
- (void)replaceBytesInRange:(NSRange)range withBytes:(const void *)replacementBytes length:(NSUInteger)replacementLength;

- (void)setSMBProtocol;
- (void)setSMBCommand:(u_char) command;
- (void)setSMBStatus:(uint32_t) status;
- (void)setSMBFlags:(u_char) flag;
- (void)setSMBFlags2:(uint16_t) flags;
- (void)setSMBPIDHigh:(uint16_t) pidHigh;
- (void)setSMBSecurityFeatures;
- (void)setSMBReserved;// MUST be 0x0000
- (void)setSMBTID:(uint16_t) tid;
- (void)setSMBPIDLow:(uint16_t) pidLow;
- (void)setSMBUID:(uint16_t) uid;
- (void)setSMBMID:(uint16_t) mid;

/**
 *  At minimum, the WordCount field of the SMB Parameters block MUST be included. The remainder of the SMB_Parameters block MUST be two times WordCount bytes in length. If WordCount is 0x00, then zero parameter bytes MUST be included in the SMB_Parameters block.
 *
 */
- (void)setParameters:(const uint16_t*) wordValue wordCount:(u_char) count;

- (void)setParameter:(const void *)byte offset:(NSUInteger)offset length:(NSUInteger)byteLength;
- (void)setParameterWithUint8:(const uint8_t *)chr offset:(NSUInteger)offset;
- (void)setParameterWithUint16:(const uint16_t *)shor offset:(NSUInteger)offset;
- (void)setParameterWithUint32:(const uint32_t *)lon offset:(NSUInteger)offset;
- (void)setParameterWithUint64:(const uint64_t *)uin offset:(NSUInteger)offset;

- (void)setParameterCount:(u_char) count;
/**
 *  At minimum, the ByteCount field of the SMB_Data block MUST be included. The remainder of the SMB_Data block MUST be ByteCount bytes in length. If ByteCount is 0x0000, then zero data bytes MUST be included in the SMB_Data block.
 */
- (void)setDataBlock:(const uint8_t*) bytes byteCount:(uint16_t) count;

- (const u_char*)getSMBCommand;
- (const uint32_t*)getSMBStatus;
- (const u_char*)getSMBFlags;
- (const uint16_t*)getSMBFlags2;
- (const uint16_t*)getSMBPIDHigh;
- (NSData*)getSMBSecurityFeatures;
- (const uint16_t*)getSMBTID;
- (const uint16_t*)getSMBPIDLow;
- (const uint16_t*)getSMBUID;
- (const uint16_t*)getSMBMID;

/**
 *  Get the parameters buffer from SMB packet.
 *  @return The number of words(2 bytes) in parameters.
 */
- (u_char)getParameters:(void*) wordValue;

- (u_char)getParametersNoCopy:(const void**) wordValue;

- (void)getDataBlockNoCopy:(NSData **)bytes;
@end
