//
//  CZSMBConnection.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/6/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBConnection.h"
#import "CZSMBConfiguration.h"
#import "CZGlobal.h"
#import "CZSMBDirectTCPTransport.h"
#import "CZSMBSession.h"
#import "CZSMBNetworkStream.h"
#import "CZSMBConsistents.h"

@interface CZSMBConnection() <NSStreamDelegate> {
@private
    NSTimer *               connectingTimeoutTimer;
    BOOL                    isTimeout;
    uint32_t               writeCount;
    uint32_t               readCount;
    uint16_t                 multiplexValue;
    uint16_t                 preloadMultiplexValue;
    uint16_t                 multiplexCount;
    uint16_t                 preloadMultiplexCount;
    BOOL                    isBufferReset;
    CZSMBNetworkStream *    networkStream;
    
//    NSTimeInterval          timerInterval;
}

@end

@implementation CZSMBConnection
@synthesize maxMpxCount = _maxMpxCount;

#pragma mark - Class Lifecycle

- (id)init {
    if (self  = [super init]) {
        networkStream = [[CZSMBNetworkStream alloc] init];
        multiplexValue = 0;
        preloadMultiplexValue = 0x1000;
        preloadMultiplexCount = 0;
        multiplexCount = 0;
    }
    return self;
}

- (void)dealloc {
    [networkStream stopThread];
}

#pragma mark - Public Methods

- (BOOL)isExtendedSecurity {
    return (BOOL)(self.serverCapabilities & kCAP_EXTENDED_SECURITY);
}

- (BOOL)isUnicodeSupported {
    return (BOOL)(self.serverCapabilities & kCAP_UNICODE);
}

- (BOOL)isNTDialectSupported {
    return (BOOL)(self.serverCapabilities & kCAP_NT_SMBS);
}

-(BOOL)isTimeout {
    return [networkStream isNetworkTimeOut];
}

- (BOOL)isHostConnected {
    return networkStream.isHostConnected;
}

- (CZSMBErrorCode)startConnectHost {
    if (self.isHostConnected) {
        return kErrorCodeNone;
    }
    
    [self disconnectHost];
    
    return [networkStream connect:self.tcpTransport.serverName];
}

- (void)disconnectHost {
    networkStream.delegate = nil;
    [networkStream disconnectHost];
    self.isNegotiated = NO;
    self.isAuthenticated = NO;
    self.isShareConnected = NO;
    self.userID = 0;
    self.treeID = 0;
    self.processID = 0;
    self.maxMpxCount = 0;
    self.maxAllowMpxCount = 0;
    multiplexValue = 0;
    preloadMultiplexValue = 0x1000;
    preloadMultiplexCount = 0;
    multiplexCount = 0;
}

- (void)setDelegate:(id) delegate {
    if (networkStream) {
        networkStream.delegate = delegate;
    }
}

- (void)clearDelegate {
    if (networkStream) {
        networkStream.delegate = nil;
    }
}

- (void)releaseMultiplexID {
    @synchronized(self) {
        if (multiplexCount > 0) {
            multiplexCount --;
        }
    }
}

- (void)releasePreloadMultiplexID {
    @synchronized(self) {
        if (preloadMultiplexCount > 0) {
            preloadMultiplexCount --;
        }
    }
}

- (uint16_t)getMultiplexID {
    @synchronized(self) {
        multiplexCount ++;
        if (multiplexCount > self.maxMpxCount) {
            CZLog(@"---multiplex count %d",multiplexCount);
            multiplexCount --;
            return 0;
        }
        multiplexValue++;
        /// This number should less than kSMBPrereadMultiplexID
        if (multiplexValue > 0xFFF) {
            CZLog(@"multiplexValue %d > 0xFFF", multiplexValue);
            multiplexValue = 1;
        }

        return multiplexValue;
    }
}

- (uint16_t)getPreloadMultiplexID {
    @synchronized(self) {
        preloadMultiplexCount ++;
        ///5 is temporary number.
        if (preloadMultiplexCount > 4) {
            CZLog(@"---multiplex count %d",preloadMultiplexCount);
            preloadMultiplexCount --;
            return 0;
        }
        preloadMultiplexValue++;
        /// This number should big than kSMBPrereadMultiplexID
        if (preloadMultiplexValue > 0xFFF0) {
            preloadMultiplexValue = 0x1000;
        }
//        CZLog(@"---- multiplex ID:%d",preloadMultiplexValue);
        return preloadMultiplexValue;
    }
}

#pragma mark - Internal Methods



- (BOOL)sendDirectTCPPacket:(NSData*) packetData {
    BOOL result = NO;
    if (!self.isHostConnected) {
        return result;
    }
    
    uint32_t header = CFSwapInt32HostToBig((uint32_t)[packetData length]);
    NSMutableData * dataHeader = [[NSMutableData alloc] initWithBytes:&header
                                                 length:4];
    [dataHeader appendData:packetData];
    [networkStream sendData:dataHeader];
    return YES;
}

@end
