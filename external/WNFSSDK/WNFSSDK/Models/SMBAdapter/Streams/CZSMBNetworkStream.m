//
//  CZSMBNetworkStream.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNetworkStream.h"
#import "CZGlobal.h"
#import "CZSMBConnection.h"
#import "CZSMBConfiguration.h"

@interface CZSMBNetworkStream() <NSStreamDelegate> {
    NSTimer *           connectingTimeoutTimer;
    NSMutableData *     readyToSendData;
    NSMutableData *     recivedData;
//    NSTimeInterval      timerInterval;
    
    int                 noResponseCount;
    BOOL                isTimeout;
}

@property (nonatomic,strong) NSOutputStream *   outputStream;
@property (nonatomic,strong) NSInputStream *    inputStream;
@property (atomic, strong) NSThread *networkRunloopThread;

@end

@implementation CZSMBNetworkStream
@synthesize isHostConnected = _isHostConnected;
@synthesize networkRunloopThread = networkRunloopThread;

#pragma mark - Class Lifecycle

- (id)init {
    if (self  = [super init]) {
        readyToSendData = [[NSMutableData alloc] init];
        recivedData = [[NSMutableData alloc] init];
        networkRunloopThread = [[NSThread alloc] initWithTarget:self
                                                       selector:@selector(networkRunLoopThreadEntry)
                                                         object:nil];
        [networkRunloopThread start];
    }
    return self;
}

- (void)dealloc {
    [_outputStream close];
    [_inputStream close];
}

#pragma mark - Public Methods

- (BOOL)isNetworkTimeOut {
    return isTimeout;
}

- (int)connect:(NSString*) host {
    __strong NSThread *thread = self.networkRunloopThread;
    if (thread == nil) {
        return NO;
    }
    
    [self performSelector:@selector(_connect:)
                 onThread:thread
               withObject:host
            waitUntilDone:YES];
    return self.isHostConnected;
}

- (int)_connect:(NSString*) host {
    if (self.isHostConnected) {
        return kErrorCodeNone;
    }
    
    [self disconnectHost];
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, kDirectHostPortNumber, &readStream, &writeStream);
    
    if (!(readStream||writeStream)) {
        return kErrorCodeStreamFailure;
    }
    
    self.outputStream = (__bridge NSOutputStream *)writeStream;
    if (writeStream) {
        CFRelease(writeStream);
    }

    [self.outputStream setDelegate:self];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [self.outputStream open];
    
    self.inputStream = (__bridge NSInputStream *)readStream;
    if (readStream) {
        CFRelease(readStream);
    }
    
    [self.inputStream setDelegate:self];
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [self.inputStream open];
    
    [self startTimeoutCounting:kSMBConnectingTimeOut];
    
    while ((!self.isHostConnected)&&(!isTimeout)) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kConnectWaitInterval]];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    return kErrorCodeNone;
}

- (void)disconnectHost {
    __strong NSThread *thread = self.networkRunloopThread;
    if (thread) {
        [self performSelector:@selector(_disconnectHost)
                     onThread:thread
                   withObject:nil
                waitUntilDone:YES];
    }
}

- (void)stopThread {
    [self.networkRunloopThread cancel];
    [self stopTimeoutCounting];
}

- (void)_disconnectHost {
    noResponseCount = 0;
    [readyToSendData setLength:0];
    [recivedData setLength:0];
    _isHostConnected = NO;
    if (self.inputStream) {
        self.inputStream.delegate = nil;
        [self.inputStream close];
        [self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    if (self.outputStream) {
        self.outputStream.delegate = nil;
        [self.outputStream close];
        [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    self.inputStream = nil;
    self.outputStream = nil;
}

- (void)sendData:(NSData*) data {
    __strong NSThread *thread = self.networkRunloopThread;
    if (thread) {
        [self performSelector:@selector(_sendData:)
                     onThread:thread
                   withObject:data
                waitUntilDone:NO];
    }
}

- (void)_sendData:(NSData*) data {
//    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
//    double interval = fabs(timerInterval - now)*1000;
//    if (interval < 10.0) {
//        [NSThread sleepForTimeInterval:0.01];
//    }
//    timerInterval = now;
    [readyToSendData appendData:data];

//    while (noResponseCount > 2) {
//        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
//    }
    if (self.outputStream.hasSpaceAvailable) {
//        noResponseCount ++;
        [self checkReadyData];
    }
}

- (void)checkReadyData {
    
    @synchronized(self) {
        if ([readyToSendData length] < 1) {
            return;
        }
        
        NSInteger sendCount = 0;
        sendCount = [self.outputStream write:[readyToSendData bytes]
                                   maxLength:[readyToSendData length]];
        if (sendCount < 0) {
            CZLog(@"outputStream write data error, send count:%ld. error:%@",(long)sendCount, [self.outputStream streamError]);
            return;
        }
        NSRange range = NSMakeRange(0, sendCount);
        assert(sendCount <= [readyToSendData length]);
        [readyToSendData replaceBytesInRange:range
                                   withBytes:NULL
                                      length:0];
    }

}

- (void)streamHasBytesAvailable:(NSInputStream*) aStream {
    ///8*1024
    uint8_t buffer[8192];
    do {
        NSInteger bytesRead = [(NSInputStream*)aStream read:buffer
                                                  maxLength:sizeof(buffer)];
        if (bytesRead > 0) {
            [recivedData appendBytes:buffer
                              length:(NSUInteger)bytesRead];
        }
    } while (aStream.hasBytesAvailable);
    
    /// let's try most 10 times for filter
    for (int i = 0; i < 10; i++) {
        
        if ([self packetFilter] == NO) {
            break;
        }
    }
}

- (BOOL)packetFilter {
    
    NSUInteger recivedDataLength = [recivedData length];
    BOOL ret = YES;
    if (recivedDataLength < kDirectTCPTransportPacketHeader) {
        return NO;
    }
    
    NSUInteger dataLength = recivedDataLength - kDirectTCPTransportPacketHeader;
    NSUInteger packetLength = 0;
    const uint8_t *buffer = (const uint8_t *)[recivedData bytes];
    for (NSUInteger i = kDirectTCPTransportPacketHeader; i < dataLength; i++) {
        if (buffer[i] == 0xff
            && buffer[i+1] == 'S'
            && buffer[i+2] == 'M'
            && buffer[i+3] == 'B') {
            
            if (i >= kDirectTCPTransportPacketHeader) {
                ///Server returned this value in the first 4 bytes of the stream.
                int32_t value;
                memcpy(&value, (buffer + i - 4), sizeof(value));
                packetLength = CFSwapInt32BigToHost(value);
//                        CZLog(@"packet filter - get packet length:%d",packetLength);
                ///split packet to array
                if ((recivedDataLength - i) >= packetLength) {
                    [self deliverPacket:buffer + i
                                 length:packetLength];
                    ///reset recived data
                    NSUInteger rangeLength = packetLength + kDirectTCPTransportPacketHeader;
                    assert(rangeLength <= recivedDataLength);
                    NSRange range = NSMakeRange(0, rangeLength);
                    [recivedData replaceBytesInRange:range
                                           withBytes:NULL
                                              length:0];
                }
                ret = YES;
                break;
            }
            
            ret = NO;
        }
    }
    
    if (ret == NO) {
        CZLog(@"None of packet in buffer.(length:%lu)",(unsigned long)recivedDataLength);
    }
    return ret;
}

- (void)deliverPacket:(const uint8_t *)byts length:(NSUInteger)length{
//    noResponseCount -= 2;
    NSData * data = [[NSData alloc] initWithBytes:byts
                                           length:length];
    if (self.delegate && [self.delegate respondsToSelector:@selector(connectionDidRecivedPacket:)]) {
        [self.delegate connectionDidRecivedPacket:data];
    }
}

#pragma mark - Internal Methods

- (void)networkRunLoopThreadEntry {
    assert(![NSThread isMainThread]);
    while (![self.networkRunloopThread isCancelled]) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }

        [NSThread sleepForTimeInterval:0.01];
    }

    self.networkRunloopThread = nil;
    
    [NSThread exit];
}

- (void)startTimeoutCounting:(float)timeout {
//    CZLog(@"Start timeout counting.");
    isTimeout = NO;
    if ([connectingTimeoutTimer isValid]) {
        [connectingTimeoutTimer invalidate];
    }
    connectingTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                              target:self
                                                            selector:@selector(connectingDidTimeout:)
                                                            userInfo:nil
                                                             repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:connectingTimeoutTimer
                                 forMode:NSRunLoopCommonModes];
}

- (void)stopTimeoutCounting {
    if (connectingTimeoutTimer) {
        if ([connectingTimeoutTimer isValid]) {
          [connectingTimeoutTimer invalidate];  
        }
        connectingTimeoutTimer = nil;
    }
}

- (void)reStartTimeoutCounting:(float)timeout {
    [self stopTimeoutCounting];
    [self startTimeoutCounting:timeout];
}

- (void)connectingDidTimeout:(NSTimer*) theTimer {
    CZLog(@"Server connecting did timeout.");
    if (connectingTimeoutTimer) {
        if ([connectingTimeoutTimer isValid]) {
            [connectingTimeoutTimer invalidate];
        }
        connectingTimeoutTimer = nil;
    }
    isTimeout = YES;
    [self disconnectHost];
}

- (void)connectingIsError:(CZSMBErrorCode) theCode{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(connectionDidRecivedError:)]) {
        [self.delegate connectionDidRecivedError:theCode];
    }
}

#pragma mark - Stream Delegate Callback
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent) eventCode {
#pragma unused(aStream)
    switch (eventCode) {
        case NSStreamEventOpenCompleted: {
            [self reStartTimeoutCounting:kSMBPacketTransferTimeOut];
            _isHostConnected = YES;
        } break;
        case NSStreamEventHasBytesAvailable: {
            assert(aStream==self.inputStream);
            [self reStartTimeoutCounting:kSMBPacketTransferTimeOut];
            [self streamHasBytesAvailable:(NSInputStream*)aStream];
        } break;
        case NSStreamEventHasSpaceAvailable: {
            assert(aStream==self.outputStream);
            [self reStartTimeoutCounting:kSMBPacketTransferTimeOut];
            [self checkReadyData];
        } break;
        case NSStreamEventErrorOccurred: {
            [self stopTimeoutCounting];
            [self disconnectHost];
            CZLog(@"NSStreamEventErrorOccurred:%@", [aStream streamError]);
            [self connectingIsError:kErrorCodeStreamFailure];
            isTimeout = YES;
        } break;
        case NSStreamEventEndEncountered: {
            [self stopTimeoutCounting];
            CZLog(@"NSStreamEventEndEncountered:%@",[aStream streamError]);
            [self disconnectHost];
            [self connectingIsError:kErrorCodeServerNoResponse];
            isTimeout = YES;
        } break;
        default: {
            assert(NO);
        } break;
    }
}
@end
