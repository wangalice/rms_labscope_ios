//
//  CZSMBNetworkStream.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol CZSMBConnectionDelegate;

@interface CZSMBNetworkStream : NSObject

@property (nonatomic, weak) id<CZSMBConnectionDelegate> delegate;
@property (nonatomic, readonly) BOOL isHostConnected;

- (int)connect:(NSString*) host;
- (void)disconnectHost;
- (void)stopThread;

- (void)sendData:(NSData*) data;
- (BOOL)isNetworkTimeOut;
@end
