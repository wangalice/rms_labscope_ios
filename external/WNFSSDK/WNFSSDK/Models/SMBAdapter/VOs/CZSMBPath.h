//
//  CZSMBPath.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZSMBPath : NSObject

- (id)initWithSMBString:(NSString*) smbString;

- (BOOL)isIPAddress;

@property (weak, nonatomic,readonly) NSString *   host;
@property (weak, nonatomic,readonly) NSString *   share;
@property (weak, nonatomic,readonly) NSString *   relativePath;
@property (weak, nonatomic,readonly) NSString *   fileName;
@property (nonatomic,readonly) BOOL         isValidFile;
@property (nonatomic,readonly) BOOL         isValidPath;
@end
