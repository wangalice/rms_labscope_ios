//
//  CZFileDescriptor.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZFileDescriptor.h"
#import "CZSMBConsistents.h"

@implementation CZFileDescriptor


- (BOOL)isDirectory {
    if (self.attributes&kSMB_FILE_ATTRIBUTE_DIRECTORY) {
        return YES;
    }
    return NO;
}

- (NSString*)slashFilePath {
    if (self.filePath == nil) {
        return nil;
    }
    return [self.filePath stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
}

@end
