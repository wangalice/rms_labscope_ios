//
//  CZSMBTreeConnectTable.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTreeConnectTable.h"

@interface CZSMBTreeConnectTable() {
    NSMutableDictionary *   treeDict;
}

@end

@implementation CZSMBTreeConnectTable

- (id)init {
    if (self = [super init]) {
        treeDict = [[NSMutableDictionary alloc] initWithCapacity:8];
    }
    return self;
}


- (NSUInteger)count {
    return [treeDict count];
}

- (uint16_t)getTreeIDBy:(uint16_t) userID {
    NSNumber *numb = [treeDict valueForKey:[NSString stringWithFormat:@"%u",userID]];
    return [numb unsignedShortValue];
}

- (void)setTreeID:(uint16_t) treeID withUID:(uint16_t) userID {
    [treeDict setValue:[NSNumber numberWithUnsignedShort:treeID]
                forKey:[NSString stringWithFormat:@"%u",userID]];
}
@end
