//
//  CZSMBPath.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBPath.h"
#import "CZSMBErrorHandler.h"
#import "CZSMBConsistents.h"
#import "CZSMBNameServiceQuery.h"

@interface CZSMBPath() <CZSMBNameQueryDelegate> {
    NSURL *     url;
    NSString *  ipAddr;
    NSString *  serverPathString;
}

@end

@implementation CZSMBPath

- (id)init {
    if (self = [super init]) {
        //We are not allow user invoke "init" directly.
        NSAssert(url, @"Please use -(id)initWithSMBString to create CZSMBPath.");
    }
    return self;
}


- (id)initWithSMBString:(NSString*) smbString {
    if (self = [super init]) {
        url = [[NSURL alloc] initWithString:
               [smbString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        serverPathString = smbString;
    }
    
    if ([self isIPAddress] == NO && self.host) {
        CZSMBNameServiceQuery *nameQuery = [CZSMBNameServiceQuery sharedInstance];
        [nameQuery doNameQuery:self.host
                      delegate:self];
    }
    return self;
}

#pragma mark - Name Query Delegate

- (void)queryHadResponse:(NSString*) anIP {
    ipAddr = [anIP copy];
}

#pragma mark - Methods
- (NSString*)host {
    if (ipAddr) {
        return ipAddr;
    }
    if (url == nil) {
        return nil;
    }
    NSString *hostString = [url host];
    NSArray *fields = [serverPathString componentsSeparatedByString:@"/"];
    if (fields.count > 3) {
        hostString = fields[2];
    }
    
    if (hostString == nil) {
        return nil;
    }
    return hostString;
}

- (NSString*)share {
    if (url == nil || self.host == nil) {
        return nil;
    }
    NSArray *array = [url pathComponents];
    if (array.count < 2) {
        return nil;
    }
    
    ///Return share name like this: \\server\share
    NSMutableString *tmp = [[NSMutableString alloc] initWithString:@"\\\\"];
    [tmp appendString:self.host];
    [tmp appendString:@"\\"];
    if ([[array objectAtIndex:0] isEqualToString:@"/"]) {
        [tmp appendString:[array objectAtIndex:1]];
    }
    return tmp;
}

- (NSString*)relativePath {
    if (url==nil || self.host == nil) {
        return nil;
    }
    NSArray *array = [url pathComponents];
    if (array.count < 3) {
        return @"\\";
    }
    
    ///Return relativePath like this: "\path\"
    NSMutableString *tmp = [[NSMutableString alloc] initWithString:@"\\"];
    NSUInteger loopCount = array.count;
    
    if (self.fileName) {
        loopCount--;
    }
    
    for (NSUInteger i = 2; i < loopCount; i++) {
        NSString *component = [array objectAtIndex:i];
        [tmp appendString:component];
        [tmp appendString:@"\\"];
    }
    NSString *relativeStr = [tmp stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //return [tmp stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];;
    if ([tmp length]>0 && [relativeStr length]<1) {
        return tmp;
    }
    return relativeStr;

}

- (NSString*)fileName {
    if (url == nil) {
        return nil;
    }
    NSString * urlString = [url absoluteString];
    NSString * last = [urlString substringFromIndex:[urlString length] - 1];
    if ([last isEqualToString:@"/"]) {
        return nil;
    }
    return [url lastPathComponent];
}

- (BOOL)isValidPath {
    if (url==nil || self.host==nil || self.share==nil) {
        return NO;
    }
    return YES;
}

- (BOOL)isIPAddress {
    if (url==nil || self.host == nil) {
        return NO;
    }
    
    return [self isIPAddress:self.host];
}

- (BOOL)isIPAddress:(NSString*) ip {
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
                                  options:0
                                  error:nil];
    
    NSUInteger matches = [regex numberOfMatchesInString:ip
                                                options:0
                                                  range:NSMakeRange(0, [ip length])];
    if (matches == 1) {
        return YES;
    }
    return NO;
}

- (BOOL)isValidFile {
    if ([self isValidPath] == NO) {
        return NO;
    }
    NSArray *array = [url pathComponents];
    if (array.count < 3) {
        return NO;
    }
    NSString *fileName = self.fileName;
    if (fileName == nil) {
        return NO;
    }
    if ([fileName isEqualToString:@"/"]||[fileName isEqualToString:@"\\"]) {
        return NO;
    }
    
    return YES;
}
@end
