//
//  CZSMBTreeConnectTable.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZSMBTreeConnectTable : NSObject

@property (nonatomic,readonly) NSUInteger count;

- (uint16_t)getTreeIDBy:(uint16_t)userID;
- (void)setTreeID:(uint16_t)treeID withUID:(uint16_t)userID;
@end
