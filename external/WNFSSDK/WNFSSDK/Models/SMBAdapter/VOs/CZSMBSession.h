//
//  CZSMBSession.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZSMBSession : NSObject

@property (nonatomic,strong)    NSString *                  userID;
@property (nonatomic,readwrite) int32_t                   authenticationState;
@property (nonatomic,readwrite) uint32_t                    key;
@property (nonatomic,readwrite) int32_t                   keyState;
@property (nonatomic,readwrite) float                       timeoutValue;
@end
