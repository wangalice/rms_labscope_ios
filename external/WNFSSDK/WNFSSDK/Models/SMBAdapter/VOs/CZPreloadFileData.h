//
//  CZPreloadFileData.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/28/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBRead.h"

@class CZFileDescriptor;

@interface CZPreloadFileData : NSObject <CZSMBReadDelegate>
@property (nonatomic,readwrite) BOOL                        isRecived;
@property (nonatomic,readwrite) BOOL                        isExpired;
@property (nonatomic,strong)    CZFileDescriptor *          fileDescriptor;
@property (nonatomic,strong)    CZSMBRead *                 smbPreload;
@property (nonatomic,strong)    NSData *                      buffer;
@property (nonatomic,readwrite) uint16_t                      multiplexID;
@end
