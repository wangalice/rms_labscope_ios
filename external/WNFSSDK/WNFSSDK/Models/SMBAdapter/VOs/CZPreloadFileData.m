//
//  CZPreloadFileData.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/28/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZPreloadFileData.h"
#import "CZFileDescriptor.h"

@implementation CZPreloadFileData

- (void)read:(CZSMBRead *)read didReceiveData:(NSData *)data{
    if (read == self.smbPreload) {
        self.buffer = data;
    }
}
@end
