//
//  CZSMBNameServiceQuery.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

@protocol CZSMBNameQueryDelegate;
@interface CZSMBNameServiceQuery : NSObject <GCDAsyncUdpSocketDelegate> {
}

+ (id)sharedInstance;

- (void)doNameQuery:(NSString*) hostName delegate:(id<CZSMBNameQueryDelegate>) delegate;

@end

@protocol CZSMBNameQueryDelegate <NSObject>
- (void)queryHadResponse:(NSString*) ipAddr;
@end
