//
//  CZSMBNameServiceRequest.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNameServicePacket.h"

@interface CZSMBNameServiceRequest : CZSMBNameServicePacket

@property (nonatomic,strong) NSString * hostName;

- (NSData*)getData;
@end
