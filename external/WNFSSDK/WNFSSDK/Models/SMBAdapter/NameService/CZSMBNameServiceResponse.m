//
//  CZSMBNameServiceResponse.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNameServiceResponse.h"

@interface CZSMBNameServiceResponse() {
    NSData * responseData;
}

@end

@implementation CZSMBNameServiceResponse

- (NSString*)hostName {
    if (responseData == nil) {
        return nil;
    }
    
    if ([responseData length] < 50) {
        return nil;
    }
    
    uint8_t body[32] = {0};
    const uint8_t * response = (const uint8_t*)[responseData bytes];
    memcpy(body, response + 13, 32);
    NSString * hostName = [self l1Decode:[NSData dataWithBytes:body
                                                        length:32]];
    
    return hostName;
}


- (id)initWithData:(NSData*) data {
    if (self = [super init]) {
        responseData = data;
    }
    return self;
}
@end
