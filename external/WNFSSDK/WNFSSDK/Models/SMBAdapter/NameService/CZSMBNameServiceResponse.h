//
//  CZSMBNameServiceResponse.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNameServicePacket.h"

@interface CZSMBNameServiceResponse : CZSMBNameServicePacket

@property (weak, nonatomic,readonly) NSString * hostName;

- (id)initWithData:(NSData*) data;
@end
