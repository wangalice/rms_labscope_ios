//
//  CZSMBNameServicePacket.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZSMBNameServicePacket : NSObject

- (NSData*)l1Encode:(NSString*) hostName;
- (NSString*)l1Decode:(NSData*) packet;
- (NSData*)l2Encode:(NSData*) l1Data;
@end
