//
//  CZSMBNameServicePacket.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBNameServicePacket.h"

@implementation CZSMBNameServicePacket

- (NSData*)l1Encode:(NSString*) hostName {
    if (hostName == nil) {
        return nil;
    }
    if ([hostName length] > 128) {
        return nil;
    }
    
    char name[33] = {'\0'};
    char encoded[33] = {'\0'};
    [hostName getCString:name
               maxLength:33
                encoding:NSASCIIStringEncoding];
    
    NSUInteger count = 16;
    count = MIN([hostName length], count);
    for (NSUInteger i = 0; i < count; i++) {
        encoded[i*2] = 0x41 + ((name[i] & 0xF0) >> 4);
        encoded[i*2+1] = 0x41 + (name[i] & 0xF);
    }
    
    while( (count*2) < 32 )
    {
        encoded[count*2] = 0x43;
        encoded[count*2+1] = 0x41;
        count++;
    }
    
    NSData * data = [NSData dataWithBytes:encoded
                                   length:32];
    return data;
}

- (NSData*)l2Encode:(NSData*) l1Data {
    if (l1Data == nil) {
        return nil;
    }
    
    
    return nil;
}

- (NSString*)l1Decode:(NSData*) packet {
    if ([packet length] > 32) {
        return nil;
    }
    
    uint8_t encoded[32] = {0};
    uint8_t decode[17] = {0};
    memcpy(encoded, [packet bytes], 32);

    for( int i = 0; i < 16; i++ ) {
        decode[i] = ((encoded[i*2]&0xFF) - 0x41) << 4;
        decode[i] |= ((encoded[i*2+1]&0xFF) - 0x41) & 0xF;
    }
    
    for (int j = 0; j < 17; j++) {
        if (decode[j] == ' ') {
            decode[j] = '\0';
        }
    }
    return [NSString stringWithCString:(char*)decode
                              encoding:NSASCIIStringEncoding];
}
@end
