//
//  CZSMBNameServiceQuery.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 7/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#define kBroadcastAddress       @"255.255.255.255"
#define kNetBIOSServicePort     137

#import "CZSMBNameServiceQuery.h"
#import "CZGlobal.h"
#import "CZSMBNameServiceRequest.h"
#import "CZSMBNameServiceResponse.h"

@interface CZSMBNameServiceQuery() {
    long                        tag;
    id<CZSMBNameQueryDelegate>  delegate;
    GCDAsyncUdpSocket           *udpSocket;
    NSString                    *hostName;
    NSMutableDictionary         *nameCache;
}

@end

@implementation CZSMBNameServiceQuery

static CZSMBNameServiceQuery *nameQuery = nil;

#pragma mark -
#pragma mark Singleton Pattern

+ (id)sharedInstance {
    @synchronized(self) {
		if (nameQuery == nil)
			nameQuery = [[self alloc] init];
	}
    
    return nameQuery;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (nameQuery == nil) {
			nameQuery = [super allocWithZone:zone];
			return nameQuery;
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)init {
    self = [super init];
    
    if (self != nil) {
        nameCache = [[NSMutableDictionary alloc] init];
    }
	return self;
}

#pragma mark - Methods

- (void)doNameQuery:(NSString*) aHostName delegate:(id<CZSMBNameQueryDelegate>) aDelegate{
    NSString *ipAddr = nil;
    if (nameCache) {
        ipAddr = [nameCache valueForKey:aHostName];
    }
    if (ipAddr) {
        [aDelegate queryHadResponse:ipAddr];
        return;
    }
    
    delegate = aDelegate;
    
    hostName = aHostName;
    
    CZSMBNameServiceRequest * nameRequest = [[CZSMBNameServiceRequest alloc] init];
    nameRequest.hostName = aHostName;
    [self sendNBNRequest:nameRequest];
}

- (BOOL)sendNBNRequest:(CZSMBNameServiceRequest*) request{
    if (udpSocket == nil) {
        udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                      delegateQueue:dispatch_get_main_queue()];
        NSError *error = nil;
        if (![udpSocket bindToPort:0 error:&error]) {
            return NO;
        }
        if (![udpSocket beginReceiving:&error]) {
            return NO;
        }
        if (![udpSocket enableBroadcast:YES error:&error]) {
            return NO;
        }
    }
    
    [udpSocket sendData:[request getData]
                 toHost:kBroadcastAddress
                   port:kNetBIOSServicePort
            withTimeout:-1
                    tag:tag];
    tag ++;
    return YES;
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext {
	CZLog(@"RECV:%@ data length:%lu",data,(unsigned long)[data length]);
    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    if (host && hostName) {
        CZSMBNameServiceResponse * response = [[CZSMBNameServiceResponse alloc] initWithData:data];
        [nameCache setValue:host
                     forKey:response.hostName];
        
    }
    if (delegate && [delegate respondsToSelector:@selector(queryHadResponse:)]) {
        [delegate queryHadResponse:host];
    }
    
    udpSocket = nil;
}


@end
