//
//  CZSMBTransportTask.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 5/28/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBErrorHandler.h"

typedef NS_ENUM(NSUInteger, CZSMBTCPTransportStatus) {
    CZSMBTCPTransportStatusDone = 0,
    CZSMBTCPTransportStatusInit = 1,
    CZSMBTCPTransportStatusNegotiate = 2,
    CZSMBTCPTransportStatusSessionSetup = 3,
    CZSMBTCPTransportStatusTreeConnect = 4,
    CZSMBTCPTransportStatusTRANS2FindFirst2 = 5,
    CZSMBTCPTransportStatusTRANS2FindNext2 = 6,
    CZSMBTCPTransportStatusTRANS2QueryPathInfo2 = 7,
    CZSMBTCPTransportStatusTRANS2QueryFSInfo = 8,
    CZSMBTCPTransportStatusTRANS2SetFileInfo = 9,
    CZSMBTCPTransportStatusFindClose2 = 10,
    CZSMBTCPTransportStatusNtCreate = 11,
    CZSMBTCPTransportStatusRead = 12,
    CZSMBTCPTransportStatusWrite = 13,
    CZSMBTCPTransportStatusClose = 14,
    CZSMBTCPTransportStatusDelete = 15,
    CZSMBTCPTransportStatusDeleteFolder = 16,
    CZSMBTCPTransportStatusRename = 17,
    CZSMBTCPTransportStatusTransEnumShare = 18,
    CZSMBTCPTransportStatusTransEnumServer = 19,
};

@class CZFileDescriptor;
@interface CZSMBTransportTask : NSObject

@property (nonatomic,readwrite) int32_t                     taskID;
@property (nonatomic,readwrite) uint16_t                     multiplexID;
@property (nonatomic,readwrite) uint16_t                     searchID;
@property (nonatomic,strong)    NSString *                  searchPath;
@property (nonatomic,strong)    NSMutableArray *            fileDirectory;
@property (nonatomic,strong)    id                          arguments;
@property (nonatomic,strong)    id                          arguments1;
@property (nonatomic,strong)    id                          event;
@property (nonatomic,weak)      NSThread *                  thread;
@property (nonatomic,readwrite) CZSMBErrorCode              errorCode;
@property (nonatomic,readwrite) CZSMBTCPTransportStatus     status;
@property (nonatomic,readwrite) BOOL                        isPreload;
@property (nonatomic,readwrite) BOOL                        isSec;
@end
