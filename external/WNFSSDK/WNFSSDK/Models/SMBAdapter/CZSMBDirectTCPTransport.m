//
//  CZSMBDirectTCPTransport.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBDirectTCPTransport.h"
#import "CZGlobal.h"
#import "CZSMBConnection.h"
#import "CZSMBNegotiate.h"
#import "CZSMBSessionSetup.h"
#import "CZSMBSessionSetup2.h"
#import "CZSMBTreeConnect.h"
#import "CZSMBTreeDisconnect.h"
#import "CZSMBTransaction2.h"
#import "CZSMBTrans2FindFirst2.h"
#import "CZSMBTrans2FindNext2.h"
#import "CZSMBTrans2QueryPathInfo.h"
#import "CZSMBTrans2SetFileInfo.h"
#import "CZSMBTrans2QueryFSInfo.h"
#import "CZSMBFindClose2.h"
#import "CZSMBNtCreate.h"
#import "CZSMBRead.h"
#import "CZSMBWrite.h"
#import "CZSMBDelete.h"
#import "CZSMBDeleteDirectory.h"
#import "CZSMBRename.h"
#import "CZSMBClose.h"
#import "CZOperationLock.h"
#import "CZSMBErrorHandler.h"
#import "CZSMBConsistents.h"
#import "CZSMBErrorHandler.h"
#import "CZFileDescriptor.h"
#import "CZSMBTransportTask.h"
#import "CZSMBResponsePacket.h"
#import "CZPreloadFileData.h"
#import "CZSMBTransaction.h"
#import "CZSMBTransTransactNmpipe.h"
#import "CZSMBTransNetShareEnum.h"
#import "CZSMBTransNetServerEnum2.h"

#define kCacheSize  8


@interface CZSMBDirectTCPTransport()<CZSMBConnectionDelegate,CZSMBEventsObserver,NSCacheDelegate> {
@private
    CZSMBConnection *       connection;    
    NSMutableDictionary *   multiplexDict;
    NSCache *               preLoadCache;
    NSMutableDictionary *   preLoadDict;
    NSThread *              processThread;
}

@property (atomic, strong) NSThread *processThread;

@end

@implementation CZSMBDirectTCPTransport

@synthesize processThread = processThread;

#pragma mark - Class Lifecycle

- (id)init {
    if (self = [super init]) {
        connection = [[CZSMBConnection alloc] init];
        multiplexDict = [[NSMutableDictionary alloc] init];
        preLoadDict = [[NSMutableDictionary alloc] init];
        preLoadCache = [[NSCache alloc] init];
        processThread = [[NSThread alloc] initWithTarget:self
                                                selector:@selector(processRunLoopThreadEntry)
                                                  object:nil];
        [processThread start];
        
        [preLoadCache setDelegate:self];
        [preLoadCache setCountLimit:kCacheSize];
        connection.tcpTransport = self;
    }
    return self;
}

#pragma mark - Internal Methods

- (void)processRunLoopThreadEntry {
    assert(![NSThread isMainThread]);
    
    while (![self.processThread isCancelled]) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
        
        [NSThread sleepForTimeInterval:0.01];
    }

    self.processThread = nil;

    [NSThread exit];
}

- (void)addObserverForEvents:(CZSMBEvents *) event with:(CZSMBTransportTask*) task {
    [event setMultiplexID:task.multiplexID];
    event.observer = self;
    [connection setDelegate:self];
}

- (void)removeObserverForEvents:(CZSMBEvents *) event {
    event.observer = nil;
}

- (void)negotiate:(CZSMBTransportTask*) task {
    CZSMBNegotiate * smbNegotiate = [[CZSMBNegotiate alloc] init];
    task.event = smbNegotiate;
    [self addObserverForEvents:smbNegotiate
                          with:task];
    [smbNegotiate startRequest:connection];
}

- (void)sessionSetup:(CZSMBTransportTask*) task {
    CZSMBSessionSetup * smbSessionSetup = [[CZSMBSessionSetup alloc] init];
    task.event = smbSessionSetup;
    [self addObserverForEvents:smbSessionSetup
                          with:task];
    [smbSessionSetup startRequest:connection];
}

- (void)sessionSetup2:(CZSMBTransportTask *) task {
    CZSMBSessionSetup2 *smbSessionSetup2 = [[CZSMBSessionSetup2 alloc] init];
    task.event = smbSessionSetup2;
    [self addObserverForEvents:smbSessionSetup2
                          with:task];
    [smbSessionSetup2 startRequest:connection];
}

- (void)treeConnect:(CZSMBTransportTask*) task {
    CZSMBTreeConnect * smbTreeConnect = [[CZSMBTreeConnect alloc] init];
    task.event = smbTreeConnect;
    [self addObserverForEvents:smbTreeConnect
                          with:task];
    [smbTreeConnect startRequest:connection];
}

- (void)treeDisconnect:(CZSMBTransportTask*) task {
    CZSMBTreeDisconnect * smbTreeDisconnect = [[CZSMBTreeDisconnect alloc] init];
    task.event = smbTreeDisconnect;
    [self addObserverForEvents:smbTreeDisconnect
                          with:task];
    [smbTreeDisconnect startRequest:connection];
}

- (void)transactionEnumServer:(CZSMBTransportTask*) task {
    CZSMBTransNetServerEnum2* smbNetServer = [[CZSMBTransNetServerEnum2 alloc] init];
    smbNetServer.isUnicodeSupported = connection.isUnicodeSupported;
    
    CZSMBTransaction * smbTrans = [[CZSMBTransaction alloc] init];
    task.event = smbTrans;
    [self addObserverForEvents:smbTrans
                          with:task];
    smbTrans.subCommand = smbNetServer;
    [smbTrans startRequest:connection];
}

- (void)transactionEnumShare:(CZSMBTransportTask*) task {
    CZSMBTransNetShareEnum* smbNetShare = [[CZSMBTransNetShareEnum alloc] init];
    smbNetShare.isUnicodeSupported = connection.isUnicodeSupported;
    
    CZSMBTransaction * smbTrans = [[CZSMBTransaction alloc] init];
    task.event = smbTrans;
    [self addObserverForEvents:smbTrans
                          with:task];
    smbTrans.subCommand = smbNetShare;
    [smbTrans startRequest:connection];
}

- (void)transactionEnumShareWithRPC:(CZSMBTransportTask*) task {
    CZSMBTransTransactNmpipe* smbNmpipe = [[CZSMBTransTransactNmpipe alloc] init];
    CZFileDescriptor * fileDesc = task.arguments;
    smbNmpipe.isSec = task.isSec;
    smbNmpipe.FID = fileDesc.FID;
    smbNmpipe.isUnicodeSupported = connection.isUnicodeSupported;
    NSString *server;
    if (self.serverIP) {
        server = [[NSString alloc] initWithFormat:@"\\\\%@",self.serverIP];
    }else {
        server = [[NSString alloc] initWithFormat:@"\\\\%@",self.serverName];
    }
    smbNmpipe.server = server;
    
    CZSMBTransaction * smbTrans = [[CZSMBTransaction alloc] init];
    task.event = smbTrans;
    [self addObserverForEvents:smbTrans
                          with:task];
    smbTrans.subCommand = smbNmpipe;
    [smbTrans startRequest:connection];
}

- (void)transaction2FindFirst2:(CZSMBTransportTask*) task{
    CZSMBTrans2FindFirst2* smbFindFirst2 = [[CZSMBTrans2FindFirst2 alloc] init];
    smbFindFirst2.serverTimeZone = connection.serverTimeZone;
    smbFindFirst2.searchPath = task.arguments;
    smbFindFirst2.searchAttributes = kSMB_FILE_ATTRIBUTE_DIRECTORY|kSMB_FILE_ATTRIBUTE_SYSTEM|kSMB_FILE_ATTRIBUTE_ARCHIVE;
    smbFindFirst2.searchCount = kMaxSearchItemsInDirectory;
    smbFindFirst2.isUnicodeSupported = connection.isUnicodeSupported;
    smbFindFirst2.isNTDialectSupported = connection.isNTDialectSupported;
    
    CZSMBTransaction2 * smbTrans2 = [[CZSMBTransaction2 alloc] init];
    task.event = smbTrans2;
    smbTrans2.subCommand = smbFindFirst2;
    [self addObserverForEvents:smbTrans2
                          with:task];
    [smbTrans2 startRequest:connection];
}

- (void)transaction2FindNext2:(CZSMBTransportTask*) task {
    CZSMBTrans2FindNext2* smbFindNext2 = [[CZSMBTrans2FindNext2 alloc] init];
    smbFindNext2.serverTimeZone = connection.serverTimeZone;
    smbFindNext2.searchID = task.searchID;
    smbFindNext2.searchAttributes = kSMB_FILE_ATTRIBUTE_DIRECTORY|kSMB_FILE_ATTRIBUTE_SYSTEM|kSMB_FILE_ATTRIBUTE_ARCHIVE;
    smbFindNext2.searchCount = kMaxSearchItemsInDirectory;
    smbFindNext2.searchPath = task.searchPath;
    smbFindNext2.fileName = task.arguments;
    smbFindNext2.isUnicodeSupported = connection.isUnicodeSupported;
    smbFindNext2.isNTDialectSupported = connection.isNTDialectSupported;

    CZSMBTransaction2 * smbTrans2 = [[CZSMBTransaction2 alloc] init];
    task.event = smbTrans2;
    [self addObserverForEvents:smbTrans2
                          with:task];
    smbTrans2.subCommand = nil;
    smbTrans2.subCommand = smbFindNext2;
    [smbTrans2 startRequest:connection];
}

- (void)transaction2QueryPathInfo:(CZSMBTransportTask*) task {
    CZSMBTrans2QueryPathInfo* smbQueryInfo = [[CZSMBTrans2QueryPathInfo alloc] init];
    smbQueryInfo.serverTimeZone = connection.serverTimeZone;
    smbQueryInfo.fileDescriptor = task.arguments;
    smbQueryInfo.isUnicodeSupported = connection.isUnicodeSupported;
    smbQueryInfo.isNTDialectSupported = connection.isNTDialectSupported;
    
    CZSMBTransaction2 * smbTrans2 = [[CZSMBTransaction2 alloc] init];
    task.event = smbTrans2;
    [self addObserverForEvents:smbTrans2
                          with:task];
    smbTrans2.subCommand = smbQueryInfo;
    [smbTrans2 startRequest:connection];
}

- (void)transaction2SetFileInfo:(CZSMBTransportTask*) task {
    CZSMBTrans2SetFileInfo* smbSetInfo = [[CZSMBTrans2SetFileInfo alloc] init];
    smbSetInfo.serverTimeZone = connection.serverTimeZone;
    smbSetInfo.fileDescriptor = task.arguments;
    smbSetInfo.isUnicodeSupported = connection.isUnicodeSupported;
    smbSetInfo.isNTDialectSupported = connection.isNTDialectSupported;

    CZSMBTransaction2 * smbTrans2 = [[CZSMBTransaction2 alloc] init];
    task.event = smbTrans2;
    [self addObserverForEvents:smbTrans2
                          with:task];
    smbTrans2.subCommand = smbSetInfo;
    [smbTrans2 startRequest:connection];
}

- (void)transaction2QueryFSInfo:(CZSMBTransportTask*) task {
    CZSMBTrans2QueryFSInfo* smbQueryFSInfo = [[CZSMBTrans2QueryFSInfo alloc] init];
    smbQueryFSInfo.diskInformation = task.arguments;
    CZSMBTransaction2 * smbTrans2 = [[CZSMBTransaction2 alloc] init];
    task.event = smbTrans2;
    [self addObserverForEvents:smbTrans2
                          with:task];
    smbTrans2.subCommand = smbQueryFSInfo;
    [smbTrans2 startRequest:connection];
}

- (void)findClose2:(CZSMBTransportTask*) task {
    CZSMBFindClose2 * smbFindClose = [[CZSMBFindClose2 alloc] init];
    task.event = smbFindClose;
    [self addObserverForEvents:smbFindClose
                          with:task];
    NSNumber *sID = task.arguments;
    smbFindClose.searchID = [sID unsignedShortValue];
    [smbFindClose startRequest:connection];
}

- (void)ntCreate:(CZSMBTransportTask*) task {
    CZSMBNtCreate * smbCreate = [[CZSMBNtCreate alloc] init];
    task.event = smbCreate;
    CZFileDescriptor* fileDescriptor = task.arguments;
    [self addObserverForEvents:smbCreate
                          with:task];
    smbCreate.fileDescriptor = fileDescriptor;
    [smbCreate startRequest:connection];
}

- (void)read:(CZSMBTransportTask*) task {
    CZSMBRead * smbRead = [[CZSMBRead alloc] init];
    task.event = smbRead;
    CZFileDescriptor* fileDescriptor = task.arguments;
    [self addObserverForEvents:smbRead
                          with:task];
    smbRead.fileDescriptor = fileDescriptor;
    [smbRead startRequest:connection];
    
    if (task.isPreload == NO) {
        return;
    }
    ///pre load mechanism
    uint64_t offset = fileDescriptor.offset;
    for (int i = 1; i < 2; i++) {
        
        uint16_t mID = [connection getPreloadMultiplexID];
        if (mID == 0) {
            continue;
        }
        [self preread:fileDescriptor
               offset:offset
            multiplex:mID];
        offset += kMaxReadDataBlockSize;
    }
    
}

- (void)preread:(CZFileDescriptor*)fileDescriptor offset:(uint64_t) dataOffset multiplex:(uint16_t) mID{
    CZPreloadFileData * preloadFile = [[CZPreloadFileData alloc] init];
    CZSMBRead * smbPreread = [[CZSMBRead alloc] init];
    smbPreread.delegate = preloadFile;
    preloadFile.smbPreload = smbPreread;
    preloadFile.multiplexID = mID;
    CZFileDescriptor *fileDesc = [[CZFileDescriptor alloc] init];
    smbPreread.fileDescriptor = fileDesc;
    preloadFile.isRecived = NO;
    fileDesc.readBytes = fileDescriptor.readBytes;
    fileDesc.FID = fileDescriptor.FID;
    fileDesc.offset = dataOffset;
    smbPreread.observer = self;
    @synchronized(self) {
        [preLoadDict setObject:[NSNumber numberWithUnsignedShort:mID]
                        forKey:[NSNumber numberWithLongLong:fileDesc.offset]];
    }
    [preLoadCache setObject:preloadFile
                     forKey:[NSNumber numberWithUnsignedShort:mID]];
    
    smbPreread.multiplexID = mID;
    [smbPreread startRequest:connection];
    
}

- (void)write:(CZSMBTransportTask*) task {
    CZSMBWrite * smbWrite = [[CZSMBWrite alloc] init];
    task.event = smbWrite;
    CZFileDescriptor* fileDescriptor = task.arguments;
    [self addObserverForEvents:smbWrite
                          with:task];
    smbWrite.fileDescriptor = fileDescriptor;
    [smbWrite startRequest:connection];
}

- (void)delete:(CZSMBTransportTask*) task {
    CZSMBDelete * smbDelete = [[CZSMBDelete alloc] init];
    task.event = smbDelete;
    NSString * fileName = task.arguments;
    [self addObserverForEvents:smbDelete
                          with:task];
    smbDelete.fileName = fileName;
    smbDelete.searchAttributes = kSMB_FILE_ATTRIBUTE_HIDDEN|
    kSMB_FILE_ATTRIBUTE_SYSTEM|
    kSMB_FILE_ATTRIBUTE_ARCHIVE;
    [smbDelete startRequest:connection];
}

- (void)deleteFolder:(CZSMBTransportTask*) task {
    CZSMBDeleteDirectory * smbDeleteDirectory = [[CZSMBDeleteDirectory alloc] init];
    task.event = smbDeleteDirectory;
    NSString * folderName = task.arguments;
    [self addObserverForEvents:smbDeleteDirectory
                          with:task];
    smbDeleteDirectory.directory = folderName;
    [smbDeleteDirectory startRequest:connection];
}

- (void)rename:(CZSMBTransportTask*) task {
    CZSMBRename * smbRename = [[CZSMBRename alloc] init];
    task.event = smbRename;
    NSString * fileName = task.arguments;
    [self addObserverForEvents:smbRename
                          with:task];
    smbRename.fileName = task.arguments1;
    smbRename.oldFileName = fileName;
    smbRename.searchAttributes = 0;
    [smbRename startRequest:connection];
}

- (void)close:(CZSMBTransportTask*) task {
    CZSMBClose * smbClose = [[CZSMBClose alloc] init];
    task.event = smbClose;
    CZFileDescriptor* fileDescriptor = task.arguments;
    [self addObserverForEvents:smbClose
                          with:task];
    smbClose.fileDescriptor = fileDescriptor;
    [smbClose startRequest:connection];
}

- (void)doTask:(CZSMBTransportTask*) task {
    if (task.status == CZSMBTCPTransportStatusDone) {
        return;
    }
    
    if (task.taskID&kEventFlagNegotiate) {
        [self negotiate:task];
        return;
    }
    if (task.taskID&kEventFlagSessionSetup) {
        [self sessionSetup:task];
        return;
    }
    if (task.taskID&kEventFlagTreeDisconnect) {
        [self treeDisconnect:task];
        return;
    }
    if (task.taskID&kEventFlagTreeConnect) {
        [self treeConnect:task];
        return;
    }
    if (task.taskID&kEventFlagTrans2FindFirst2) {
        [self transaction2FindFirst2:task];
        return;
    }
    if (task.taskID&kEventFlagTrans2FindNext2) {
        [self transaction2FindNext2:task];
        return;
    }
    if (task.taskID&kEventFlagTrans2QueryPathInfo) {
        [self transaction2QueryPathInfo:task];
        return;
    }
    if (task.taskID&kEventFlagTrans2SetFileInfo) {
        [self transaction2SetFileInfo:task];
        return;
    }
    if (task.taskID&kEventFlagTrans2QueryFSInfo) {
        [self transaction2QueryFSInfo:task];
        return;
    }
    if (task.taskID&kEventFlagFindClose2) {
        [self findClose2:task];
        return;
    }
    if (task.taskID&kEventFlagNTCreate) {
        [self ntCreate:task];
        return;
    }
    if (task.taskID&kEventFlagTransactionEnumServer) {
        [self transactionEnumServer:task];
        return;
    }
    if (task.taskID&kEventFlagTransactionEnumShare) {
        [self transactionEnumShare:task];
        return;
    }
    if (task.taskID&kEventFlagTransactionEnumShareRPC) {
        [self transactionEnumShareWithRPC:task];
        return;
    }
    if (task.taskID&kEventFlagRead) {
        [self read:task];
        return;
    }
    if (task.taskID&kEventFlagWrite) {
        [self write:task];
        return;
    }
    if (task.taskID&kEventFlagDelete) {
        [self delete:task];
        return;
    }
    if (task.taskID&kEventFlagDeleteDirectory) {
        [self deleteFolder:task];
        return;
    }
    if (task.taskID&kEventFlagClose) {
        [self close:task];
        return;
    }
    if (task.taskID&kEventFlagRename) {
        [self rename:task];
        return;
    }
}

#pragma mark - Public Methods

- (void)waitForTaskStatus:(CZSMBTransportTask *)transTask {
    while (transTask.status > CZSMBTCPTransportStatusDone) {
        @autoreleasepool {
            __strong NSThread *thread = transTask.thread;
            if (thread == nil || [thread isCancelled]) {
                break;
            }
            
            @synchronized(self) {
                if (![multiplexDict objectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]]) {
                    transTask.status = CZSMBTCPTransportStatusDone;
                    break;
                }
                if(connection.isTimeout) {
                    break;
                }
            }
            
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval]];
        }
        
        [NSThread sleepForTimeInterval:0.01];
    }
}

- (CZSMBErrorCode)connectToShare {
    CZSMBErrorCode code = kErrorCodeNone;

    code = [connection startConnectHost];
    if ([CZSMBErrorHandler isError:code]) {
        return code;
    }
    if (connection.isShareConnected) {
        return kErrorCodeNone;
    }
    
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = code;
    transTask.taskID = kEventFlagNegotiate;
    transTask.taskID |= kEventFlagSessionSetup;
    transTask.taskID |= kEventFlagTreeConnect;
    ///we don't care about the multiplex in this time.
    transTask.multiplexID = 0;
    transTask.thread = [NSThread currentThread];
    
    @synchronized(self) {
        NSNumber *key = nil;
        do {
            @autoreleasepool {
                key = [NSNumber numberWithUnsignedShort:transTask.multiplexID];
                if (![multiplexDict objectForKey:key]) {
                    break;
                }
                transTask.multiplexID--;
            }
        } while (TRUE);
        
        [multiplexDict setObject:transTask
                          forKey:key];
        
      
    }
    
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    code = transTask.errorCode;
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    if ([CZSMBErrorHandler isError:code] == NO) {
        connection.maxMpxCount = connection.maxAllowMpxCount;
        self.tryConnectCount = 0;
    }
    return code;
}

- (CZSMBErrorCode)reconnectToTree {
    CZSMBErrorCode code = kErrorCodeNone;
    if (!connection.isShareConnected) {  // tree is not connected, do not need to reconnect
        return kErrorCodeNone;
    }
    
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = code;
    transTask.taskID = kEventFlagTreeConnect | kEventFlagTreeDisconnect;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)listDirectory:(NSString*) directory to:(NSMutableArray*) array{
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.arguments = directory;
    transTask.taskID = kEventFlagTrans2FindFirst2;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    transTask.fileDirectory = array;
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];

    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    
    return code;
}

- (CZSMBErrorCode)enumServer:(NSMutableArray*) array{
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagTransactionEnumServer;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    transTask.fileDirectory = array;
    
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    
    return code;
}

- (CZSMBErrorCode)enumShare:(NSMutableArray*) array{
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagTransactionEnumShare;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    transTask.fileDirectory = array;
    
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    
    return code;
}

- (CZSMBErrorCode)enumShareWithRPC:(NSMutableArray*) array{
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagNTCreate|kEventFlagTransactionEnumShareRPC;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    transTask.fileDirectory = array;
    CZFileDescriptor *fileDesc = [[CZFileDescriptor alloc] init];
    transTask.arguments = fileDesc;
    fileDesc.fileName = @"\\srvsvc";
    fileDesc.filePath = @"";
    fileDesc.attributes = 0x80;
    
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    
    return code;
}

- (CZSMBErrorCode)queryFileSystemInfo:(CZDiskInformation*) theDiskInfo {
    
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagTrans2QueryFSInfo;
    transTask.arguments = theDiskInfo;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    uint16_t key = transTask.multiplexID;
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:key]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)queryFileInfo:(CZFileDescriptor*) fileDescriptor {

    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagTrans2QueryPathInfo;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    uint16_t key = transTask.multiplexID;
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:key]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)setFileInfo:(CZFileDescriptor*) fileDescriptor {
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagTrans2SetFileInfo;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    uint16_t key = transTask.multiplexID;
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:key]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)openFile:(CZFileDescriptor*) fileDescriptor {
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagNTCreate;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor preload:(BOOL) isPreload{
    if (isPreload == NO) {
        return [self readFile:fileDescriptor];
    }
    
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    
    @synchronized(self) {
        uint16_t mID = [[preLoadDict objectForKey:[NSNumber numberWithLongLong:fileDescriptor.offset]] unsignedShortValue];
        if (mID) {
            CZPreloadFileData * preload = [preLoadCache objectForKey:[NSNumber numberWithUnsignedShort:mID]];
            if (preload) {
                if (preload.fileDescriptor.FID == fileDescriptor.FID &&
                    preload.isRecived) {
                    fileDescriptor.dataPtr = [preload.buffer subdataWithRange:NSMakeRange(0, preload.fileDescriptor.readBytes)];
                    fileDescriptor.readBytes = preload.fileDescriptor.readBytes;
                    [preLoadDict removeObjectForKey:[NSNumber numberWithLongLong:fileDescriptor.offset]];
                    uint16_t mID = [connection getPreloadMultiplexID];
                    if (mID > 0) {
                        uint64_t offset = [[[preLoadDict allKeys] lastObject] unsignedLongLongValue] + kMaxReadDataBlockSize;
                        [self preread:fileDescriptor
                               offset:offset
                            multiplex:mID];
                    }
                    
                    CZLog(@"preLoadDict length:%lu",(unsigned long)[preLoadDict count]);
                    return 0;
                }else {
                    preload.isExpired = YES;
                }
            }
        }
    }
    
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagRead;
    transTask.isPreload = YES;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
        [preLoadDict removeObjectForKey:[NSNumber numberWithLongLong:fileDescriptor.offset]];
    }
    
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)readFile:(CZFileDescriptor*) fileDescriptor {
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }

    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagRead;
    transTask.isPreload = NO;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
        [preLoadDict removeObjectForKey:[NSNumber numberWithLongLong:fileDescriptor.offset]];
    }
    
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)writeFile:(CZFileDescriptor*) fileDescriptor {
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagWrite;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)deleteFile:(NSString*) fileName {
    
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagDelete;
    transTask.arguments = fileName;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)deleteDirectory:(NSString*) path {

    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagDeleteDirectory;
    transTask.arguments = path;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)rename:(NSString*) oldFileName newName:(NSString*) fileName{
    
    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagRename;
    transTask.arguments = oldFileName;
    transTask.arguments1 = fileName;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];
    
    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (CZSMBErrorCode)closeFile:(CZFileDescriptor*) fileDescriptor {

    if (connection.isHostConnected == NO) {
        return kErrorCodeHostUnreachable;
    }
    CZSMBTransportTask * transTask = [[CZSMBTransportTask alloc] init];
    transTask.status = CZSMBTCPTransportStatusInit;
    transTask.errorCode = kErrorCodeNone;
    transTask.taskID = kEventFlagClose;
    transTask.arguments = fileDescriptor;
    transTask.multiplexID = [connection getMultiplexID];
    transTask.thread = [NSThread currentThread];
    ///suspend this function until we get a available multiplex ID.
    while (transTask.multiplexID == 0) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval1]];
            transTask.multiplexID = [connection getMultiplexID];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
    
    @synchronized(self) {
        
        [multiplexDict setObject:transTask
                          forKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [self doTask:transTask];
    
    [self waitForTaskStatus:transTask];

    @synchronized(self) {
        [multiplexDict removeObjectForKey:[NSNumber numberWithUnsignedShort:transTask.multiplexID]];
    }
    [connection releaseMultiplexID];
    CZSMBErrorCode code = transTask.errorCode;
    return code;
}

- (void)disconnectFromShare {
    [connection disconnectHost];
}

- (void)stopThread {
    [self.processThread cancel];
}

#pragma mark - SMBConnection Delegate
- (void)connectionDidRecivedPacket:(NSData*) data {
    CZSMBResponsePacket *respPacket = [[CZSMBResponsePacket alloc] init];
    respPacket.packetData = (NSMutableData*)data;
    uint16_t mID = *[respPacket getSMBMID];
//    u_char command = *[respPacket getSMBCommand];
    CZSMBTransportTask * task = nil;

    if (mID >= kSMBPrereadMultiplexID) {
//        CZLog(@"recive preload response");
        
        CZPreloadFileData * preload = nil;
        preload = [preLoadCache objectForKey:[NSNumber numberWithUnsignedShort:mID]];
        @synchronized(self) {
            __strong NSThread *thread = self.processThread;
            if (preload && (preload.isExpired == NO) && thread) {
                [preload.smbPreload performSelector:@selector(dataParse:)
                                           onThread:thread
                                         withObject:data
                                      waitUntilDone:NO];
            }
        }


        [connection releasePreloadMultiplexID];
        return;
    }

    @synchronized(self) {
        task = [multiplexDict objectForKey:[NSNumber numberWithUnsignedShort:mID]];
    }
    __strong NSThread *thread = task.thread;
    if (thread == nil) {
        thread = self.processThread;
    }
    
    if (thread) {
        [task.event performSelector:@selector(dataParse:)
                           onThread:thread
                         withObject:data
                      waitUntilDone:NO];
    }
    CZLog(@"multiplex ID:%d, dict count:%lu",mID,(unsigned long)[multiplexDict count]);
}

- (void)connectionDidRecivedError:(CZSMBErrorCode) code {
    @synchronized(self) {
        
        NSEnumerator *enumerator = [multiplexDict objectEnumerator];
        CZSMBTransportTask * task;
        while ((task = [enumerator nextObject])) {
            __strong NSThread *thread = task.thread;
            if (thread == nil) {
                thread = self.processThread;
            }
            if (thread) {
                [task.event performSelector:@selector(dataError:)
                                   onThread:thread
                                 withObject:[NSNumber numberWithUnsignedInteger:code]
                              waitUntilDone:NO];
            }
        }
    }
}

#pragma mark - Status Update Process

- (void)updateItemStatus:(NSArray*) items {
    NSNotificationCenter * notificationCenter = [NSNotificationCenter defaultCenter];
    NSUInteger itemCount = [items count];
    /// remove "." and ".." items.
    if (itemCount > 2) {
        itemCount -=2;
    }
    [notificationCenter postNotificationName:kListDirectoryStatusNotification
                                      object:[NSNumber numberWithUnsignedInteger:itemCount]];
}

#pragma mark - CZSMBEventsObserver

- (void)smbEventsStateChanged:(CZSMBEvents *)smbEvents {
    CZSMBEvents<CZSMBEventsDelegate> *event = nil;
    
    if ([smbEvents conformsToProtocol:@protocol(CZSMBEventsDelegate)]) {
        event = (CZSMBEvents<CZSMBEventsDelegate> *)smbEvents;
    } else {
        return;
    }

    CZSMBTransportTask * task = nil;
    
    if (event.multiplexID >= kSMBPrereadMultiplexID) {
        if (event.state == CZSMBEventsStateSuccess) {
            CZPreloadFileData * preload = [preLoadCache objectForKey:[NSNumber numberWithUnsignedShort:event.multiplexID]];
            @synchronized(self) {
                preload.isRecived = YES;
            }
        }
        return;
    }
    
    [self removeObserverForEvents:event];
    
    @synchronized(self) {
        task = [multiplexDict objectForKey:[NSNumber numberWithUnsignedShort:event.multiplexID]];
    }
    if (task == nil) {
        return;
    }
    
    BOOL suppressFindNextError = NO;
    if (event.state != CZSMBEventsStateSuccess && [event eventFlag] == kEventFlagTrans2FindNext2) {
        event.state = CZSMBEventsStateSuccess;
        suppressFindNextError = YES;
        task.errorCode = kErrorCodeNone;
    }
    
    id parameter = nil;
    if ([event eventFlag] == kEventFlagNegotiate) {
        task.status = CZSMBTCPTransportStatusNegotiate;
    }else if ([event eventFlag] == kEventFlagSessionSetup) {
        task.status = CZSMBTCPTransportStatusSessionSetup;
    }else if ([event eventFlag] == kEventFlagTreeConnect) {
        task.status = CZSMBTCPTransportStatusTreeConnect;
    }else if ([event eventFlag] == kEventFlagTrans2FindFirst2) {
        task.status = CZSMBTCPTransportStatusTRANS2FindFirst2;
        CZSMBTrans2FindFirst2* findFirst2 = (CZSMBTrans2FindFirst2*)[((CZSMBTransaction2*)event) subCommand];
        if (findFirst2.isEndOfSearch == NO) {
            task.taskID |= kEventFlagTrans2FindNext2;
            task.searchID = findFirst2.searchID;
            task.searchPath = findFirst2.searchPath;
            parameter = findFirst2.lastFileName;
        }else {
            task.taskID |= kEventFlagFindClose2;
            parameter = [[NSNumber alloc] initWithUnsignedShort:findFirst2.searchID];
        }
        [task.fileDirectory removeAllObjects];
        [task.fileDirectory addObjectsFromArray:findFirst2.itemsList];
        [self updateItemStatus:task.fileDirectory];
    }else if ([event eventFlag] == kEventFlagTrans2FindNext2) {
        task.status = CZSMBTCPTransportStatusTRANS2FindNext2;
        CZSMBTrans2FindNext2* findNext2 = (CZSMBTrans2FindNext2*)[(CZSMBTransaction2*)event subCommand];
        if (findNext2.isEndOfSearch == NO && suppressFindNextError == NO) {
            task.taskID |= kEventFlagTrans2FindNext2;
            task.taskID |= kEventFlagKeepLoop;
            task.searchID = findNext2.searchID;
            task.searchPath = findNext2.searchPath;
            parameter = findNext2.lastFileName;
        }else {
            task.taskID = kEventFlagTrans2FindNext2;
            task.taskID |= kEventFlagFindClose2;
            parameter = [[NSNumber alloc] initWithUnsignedShort:findNext2.searchID];
        }
        [task.fileDirectory addObjectsFromArray:findNext2.itemsList];
        [self updateItemStatus:task.fileDirectory];
    }else if ([event eventFlag] == kEventFlagFindClose2) {
        task.status = CZSMBTCPTransportStatusFindClose2;
    }else if ([event eventFlag] == kEventFlagTrans2QueryPathInfo) {
        task.status = CZSMBTCPTransportStatusTRANS2QueryPathInfo2;
    }else if ([event eventFlag] == kEventFlagNTCreate) {
        task.status = CZSMBTCPTransportStatusNtCreate;
//        CZSMBNtCreate* ntCreate = (CZSMBNtCreate*)event;
//        task.arguments1 =
    }else if ([event eventFlag] == kEventFlagRead) {
        task.status = CZSMBTCPTransportStatusRead;
    }else if ([event eventFlag] == kEventFlagWrite) {
        task.status = CZSMBTCPTransportStatusWrite;
    }else if ([event eventFlag] == kEventFlagClose) {
        task.status = CZSMBTCPTransportStatusClose;
    }else if ([event eventFlag] == kEventFlagDelete) {
        task.status = CZSMBTCPTransportStatusDelete;
    }else if ([event eventFlag] == kEventFlagDeleteDirectory) {
        task.status = CZSMBTCPTransportStatusDeleteFolder;
    }else if ([event eventFlag] == kEventFlagRename) {
        task.status = CZSMBTCPTransportStatusRename;
    }else if ([event eventFlag] == kEventFlagTrans2QueryFSInfo) {
        task.status = CZSMBTCPTransportStatusTRANS2QueryFSInfo;
    }else if ([event eventFlag] == kEventFlagTrans2SetFileInfo) {
        task.status = CZSMBTCPTransportStatusTRANS2SetFileInfo;
    }else if ([event eventFlag] ==  kEventFlagTransactionEnumShare) {
        CZSMBTransNetShareEnum* netShare = (CZSMBTransNetShareEnum*)[(CZSMBTransaction*)event subCommand];
        task.status = CZSMBTCPTransportStatusTransEnumShare;
        [task.fileDirectory removeAllObjects];
        [task.fileDirectory addObjectsFromArray:netShare.shareList];
    }else if ([event eventFlag] ==  kEventFlagTransactionEnumServer) {
        CZSMBTransNetServerEnum2* netServer = (CZSMBTransNetServerEnum2*)[(CZSMBTransaction*)event subCommand];
        task.status = CZSMBTCPTransportStatusTransEnumServer;
        [task.fileDirectory removeAllObjects];
        [task.fileDirectory addObjectsFromArray:netServer.serverList];
    }else if ([event eventFlag] ==  kEventFlagTransactionEnumShareRPC) {
        CZSMBTransTransactNmpipe* nmpipe = (CZSMBTransTransactNmpipe*)[(CZSMBTransaction*)event subCommand];
        task.status = CZSMBTCPTransportStatusTransEnumShare;
        if (nmpipe.isSec == NO) {
            task.isSec = YES;
            task.taskID |= kEventFlagKeepLoop;
        }else {
            task.taskID = kEventFlagTransactionEnumShareRPC;
            [task.fileDirectory removeAllObjects];
            [task.fileDirectory addObjectsFromArray:nmpipe.shareList];
        }
    }
    
    if (event.state != CZSMBEventsStateSuccess) {///unsuccess
        task.errorCode = event.errorCode;
        task.status = CZSMBTCPTransportStatusDone;
    }else {///success
        if ((task.taskID&kEventFlagKeepLoop) == 0) {
            task.taskID ^= [event eventFlag];
        }
        if (task.taskID == 0) {
            task.status = CZSMBTCPTransportStatusDone;
        }else {
            if (parameter != nil) {
                task.arguments = parameter;
            }
            [self doTask:task];
        }
    }
    
}

- (void)cache:(NSCache *)cache willEvictObject:(id)obj {
    CZPreloadFileData * fileData = obj;
    @synchronized(self) {
        fileData.isExpired = YES;
    }
    if (fileData.smbPreload) {
        [self removeObserverForEvents:fileData.smbPreload];
    }
}
@end
