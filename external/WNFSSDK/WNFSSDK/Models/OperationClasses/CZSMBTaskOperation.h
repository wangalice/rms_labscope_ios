//
//  CZSMBTaskOperation.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZBasicOperation.h"
#import "CZSMBErrorHandler.h"

@class CZSMBAdapter,CZSMBTaskParameter,CZSMBTaskResult;
@interface CZSMBTaskOperation : CZBasicOperation

@property (nonatomic,strong)    CZSMBAdapter *          adapter;
@property (nonatomic,strong)    CZSMBTaskParameter *    taskInfo;
@property (nonatomic,readonly)  CZSMBTaskResult *       resultInfo;
@property (nonatomic,readonly)  CZSMBErrorCode          error;

@property (nonatomic,readwrite) int                     progress;

@property (nonatomic,readwrite) NSUInteger              itemsCount;

@property (nonatomic,readwrite) uint64_t               delivered;
@property (nonatomic,readwrite) uint64_t               total;

- (id)initWithAdapter:(CZSMBAdapter*) adapter parameter:(CZSMBTaskParameter*) info;
@end
