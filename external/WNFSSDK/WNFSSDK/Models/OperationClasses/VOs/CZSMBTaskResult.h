//
//  CZSMBTaskResult.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZSMBTaskResult : NSObject

@property (nonatomic,strong) NSArray *         directory;
@end
