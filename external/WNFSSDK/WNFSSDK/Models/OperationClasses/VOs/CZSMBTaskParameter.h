//
//  CZSMBTaskParameter.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/26/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZSMBTaskType)
{
    CZSMBTaskTypeGetFile = 0,
    CZSMBTaskTypeSendFile = 1,
    CZSMBTaskTypeListDirectory = 2,
    CZSMBTaskTypeDeleteItem = 3,
    CZSMBTaskTypeServerAvailable = 4
};

@interface CZSMBTaskParameter : NSObject

@property (nonatomic,readwrite) CZSMBTaskType       taskType;
@property (nonatomic,readwrite) BOOL                isOverwriting;
@property (nonatomic,readwrite) BOOL                isForce;
@property (nonatomic,readwrite) NSUInteger          listItemsMode;
@property (nonatomic,strong) NSString *             serverPath;
@property (nonatomic,strong) NSString *             localFilePath;
@property (nonatomic,strong) NSDictionary *         serverFileAttributes;
@property (nonatomic,readwrite) NSTimeInterval      timeout;

@end
