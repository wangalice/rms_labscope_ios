//
//  CZSMBTaskOperation.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBTaskOperation.h"
#import "CZGlobal.h"
#import "CZSMBAdapter.h"
#import "CZSMBTaskParameter.h"
#import "CZSMBTaskResult.h"
#import "CZFileDescriptor.h"
#import "CZSMBConsistents.h"
#import "CZSMBPath.h"
#import "CZSMBConfiguration.h"
#import "CZNetworkUtilities.h"
#import "CZDiskInformation.h"

@interface CZSMBTaskOperation() <NSStreamDelegate> {
    NSInputStream *    inputStream;
}

- (void)updateProgressWithDataDelivered:(uint64_t) delivered total:(uint64_t) totalBytes;
@end

@implementation CZSMBTaskOperation
@synthesize resultInfo=_resultInfo;
@synthesize error=_error;


- (id)initWithAdapter:(CZSMBAdapter*) adapter parameter:(CZSMBTaskParameter*) info {
    if (self = [super init]) {
        self.taskInfo = info;
        self.adapter = adapter;
    }
    return self;
}

- (void)entry {
    assert(![NSThread isMainThread]);

    switch (self.taskInfo.taskType) {
        case CZSMBTaskTypeListDirectory:
            [self listDirectory];
            break;
        case CZSMBTaskTypeGetFile:
            [self getFileFromServer];
            break;
        case CZSMBTaskTypeSendFile:
            [self sendFileToServer];
            break;
        case CZSMBTaskTypeDeleteItem:
            [self deleteItem];
            break;
        case CZSMBTaskTypeServerAvailable:
            [self isServerAvailable];
            break;
        default:
            assert(NO);
            break;
    }
    
}

- (void)listDirectory {
    CZSMBErrorCode code = kErrorCodeNone;
    if (_resultInfo == nil) {
        _resultInfo = [[CZSMBTaskResult alloc] init];
    }
    NSNotificationCenter * notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(updateListDirectoryItemCount:)
                               name:kListDirectoryStatusNotification
                             object:nil];
    
    
    if (self.queue) {
        self.queue.maxConcurrentOperationCount = 1;
    }
    _resultInfo.directory = [self.adapter listDirectory:self.taskInfo.serverPath
                                            error:&code];
    
    [notificationCenter removeObserver:self
                                  name:kListDirectoryStatusNotification
                                object:nil];
    _error = code;
}

- (void)updateListDirectoryItemCount:(NSNotification *) notification {
    self.itemsCount = [notification.object unsignedIntegerValue];
}

- (void)updateProgressWithDataDelivered:(uint64_t) delivered total:(uint64_t) totalBytes {

    [self willChangeValueForKey:kSMBTaskProgressKeyPath];
    self.delivered = delivered;
    self.total = totalBytes;
    [self didChangeValueForKey:kSMBTaskProgressKeyPath];
}

- (void)getFileFromServer {
    BOOL isOverwriting = NO;
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr isWritableFileAtPath:[self.taskInfo.localFilePath stringByDeletingLastPathComponent]] == NO) {
        _error = kErrorCodeLocalFileWriteError;
        return;
    }
    
    isOverwriting = [fileMgr fileExistsAtPath:self.taskInfo.localFilePath];
    if (!self.taskInfo.isOverwriting && isOverwriting) {
        _error = kErrorCodeLocalFileExist;
        return;
    }
    
    CZSMBErrorCode code = kErrorCodeNone;
    
    CZFileDescriptor *fileDesc = [self.adapter openFile:self.taskInfo.serverPath
                                                options:kOpenFlagReadOnly
                                                  error:&code];
    
    if ([CZSMBErrorHandler isError:code]) {
        _error = code;
        return;
    }
    
    if (self.queue) {
        self.queue.maxConcurrentOperationCount = 2;
    }
    
    const uint64_t kFreeSpaceLowerLimit = 200 * 1024 * 1024; // leave 200MB for iOS system

    uint64_t fileSize = fileDesc.endOfFile;
    uint64_t requireDiskSpace = fileSize + kFreeSpaceLowerLimit;
    if ([[self class] freeSpaceInBytesForPath:self.taskInfo.localFilePath] < requireDiskSpace) {
        _error = kSTATUS_DISK_FULL;
        return;
    }
    
    NSString *uniqueFileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingPathExtension:@"tmp"];
    NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:uniqueFileName];

    NSOutputStream *fileStream = [NSOutputStream outputStreamToFileAtPath:tempFilePath
                                                                   append:NO];
    [fileStream open];
    
    uint64_t bytesWrittenOffset = 0, bytesWritten = 0, totalBytes = fileSize;
    NSData *buffer = NULL;
    do { @autoreleasepool {
        code = [self.adapter seekFile:fileDesc
                               offset:bytesWrittenOffset
                                 mode:CZSMBFileSeekModeSet];
        
        if ([CZSMBErrorHandler isError:code]) {
            break;
        }
        
        assert(fileDesc);
        code = [self.adapter readFile:fileDesc
                             byLength:kMaxReadDataBlockSize
                              preload:YES
                                 data:&buffer];
        
        if ([CZSMBErrorHandler isError:code]) {
            break;
        }
        
        bytesWritten = code;

        if ([self writeBytesToStream:fileStream
                               bytes:[buffer bytes]
                              length:bytesWritten] == NO) {
            code = kErrorCodeLocalFileWriteError;
            break;
        }

        bytesWrittenOffset += bytesWritten;
        [self updateProgressWithDataDelivered:bytesWrittenOffset
                                        total:totalBytes];
    } } while ((bytesWrittenOffset < totalBytes) && (self.isCancelled == NO));
    
    [fileStream close];
    
    if (bytesWrittenOffset == totalBytes) {
        if (isOverwriting) {
            [[NSFileManager defaultManager] removeItemAtPath:self.taskInfo.localFilePath error:NULL];
        }
        [[NSFileManager defaultManager] moveItemAtPath:tempFilePath toPath:self.taskInfo.localFilePath error:NULL];
    } else {
        [[NSFileManager defaultManager] removeItemAtPath:tempFilePath error:NULL];
    }
    
    [self.adapter closeFile:fileDesc];
    
    _error = code;
    
    NSDictionary *fileAttributes = self.taskInfo.serverFileAttributes;
    
    // fetch file attribute, retry for up to three times
    for (NSUInteger i = 0; i < 3 && fileAttributes == nil; ++i) {
        fileDesc = [self.adapter getAttribute:self.taskInfo.serverPath
                                        error:&code];
        
        if ([CZSMBErrorHandler isError:code] == NO) {
            NSDate *createTime = fileDesc.createDatetime;
            if (!createTime) {
                createTime = [NSDate date];
            }
            
            NSDate *modifyTime = fileDesc.lastWriteDatetime;
            if (!modifyTime) {
                modifyTime = [NSDate date];
            }
            
            fileAttributes = @{NSFileCreationDate: createTime,
                               NSFileModificationDate: modifyTime};
        } else {
            CZLog(@"Failed to get attribute from remote file.");
            sleep(1);
        }
    }
    
    if (fileAttributes) {
        BOOL isAttributeSuccess = [fileMgr setAttributes:fileAttributes
                                            ofItemAtPath:self.taskInfo.localFilePath
                                                   error:nil];
        if (isAttributeSuccess == NO) {
            CZLog(@"Failed to set attribute on local file.");
        }
    }
}

- (void)sendFileToServer {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:self.taskInfo.localFilePath] == NO) {
        _error = kErrorCodeLocalFileDoesNotExist;
        return;
    }

    CZSMBErrorCode code = kErrorCodeNone;
    CZSMBFileOpenFlag options;
    if (self.taskInfo.isOverwriting) {
        options = kOpenFlagTruncate|kOpenFlagWriteOnly|kOpenFlagExclusiveLock;
    }else {
        options = kOpenFlagExists|kOpenFlagWriteOnly|kOpenFlagExclusiveLock;
    }
    
    NSError *sysError = NULL;
    NSDictionary *dictAttributes = [fileMgr attributesOfItemAtPath:self.taskInfo.localFilePath
                                                             error:&sysError];
    if (dictAttributes == nil) {
        _error = kErrorCodeLocalFileReadFailed;
        return;
    }
    
    const uint64_t totalSize = [[dictAttributes valueForKey:NSFileSize] unsignedLongLongValue];
    CZDiskInformation *diskInfo = [[CZDiskInformation alloc] init];
    code = [self.adapter getDiskInformation:diskInfo];
    if (![CZSMBErrorHandler isError:code]) {
        if (diskInfo.totalFreeAllocationSize < totalSize) {
            _error = kSTATUS_DISK_FULL;
            return;
        }
    }
    
    CZFileDescriptor *fileDesc = [self.adapter openFile:self.taskInfo.serverPath
                                                options:options
                                                  error:&code];
    
    if ([CZSMBErrorHandler isError:code]) {
        _error = code;
        return;
    }
    
    if (self.queue) {
        self.queue.maxConcurrentOperationCount = 2;
    }
    
    uint8_t buffer[kMaxWriteDataBlockSize];
    
    NSInputStream *fileStream = [NSInputStream inputStreamWithFileAtPath:self.taskInfo.localFilePath];
    if (fileStream == nil) {
        _error = kErrorCodeLocalFileStreamError;
        return;
    }
    [fileStream open];
    
    NSInteger bytesNeedWritten = 0;
    uint64_t bytesWrittenOffset = 0;
    
    while ((bytesNeedWritten = [fileStream read:buffer
                                      maxLength:kMaxWriteDataBlockSize]) > 0 && (self.isCancelled == NO)) {
        
        code = [self.adapter seekFile:fileDesc
                               offset:bytesWrittenOffset
                                 mode:CZSMBFileSeekModeSet];
        
        if ([CZSMBErrorHandler isError:code]) {
            break;
        }
        
        code = [self.adapter writeFile:fileDesc
                                buffer:buffer
                                  size:bytesNeedWritten];
        
        if ([CZSMBErrorHandler isError:code]) {
            break;
        }
        
        bytesNeedWritten = code;
        code = 0;
        
        bytesWrittenOffset += bytesNeedWritten;
        
        if (fileStream.hasBytesAvailable == NO) {
            break;
        }
        
        [self updateProgressWithDataDelivered:bytesWrittenOffset
                                        total:totalSize];
    }
    
    [fileStream close];
    
    fileDesc.createDatetime = dictAttributes[NSFileCreationDate];
    fileDesc.lastWriteDatetime = dictAttributes[NSFileModificationDate];
    [self.adapter setAttribute:fileDesc];
    
    [self.adapter closeFile:fileDesc];
    if (bytesWrittenOffset < totalSize || code == kSTATUS_DISK_FULL) {
        [self.adapter deleteFile:self.taskInfo.serverPath];
    }
    
    if ([CZSMBErrorHandler isError:code]) {
        _error = code;
        return;
    }
    
    if (bytesNeedWritten < 0) {
        _error = kErrorCodeLocalFileReadFailed;
        return;
    }
}

- (BOOL)writeBytesToStream:(NSOutputStream*)stream bytes:(const uint8_t *)buffer length:(uint64_t)length {
    BOOL ret = YES;
    
    uint64_t bytesWrittenSoFar = 0;
    NSInteger bytesWritten = 0;
    while (bytesWrittenSoFar < length) {
        bytesWritten = [stream write:buffer + bytesWrittenSoFar
                           maxLength:length - bytesWrittenSoFar];
        if (bytesWritten < 0) {
            ret = NO;
            break;
        }
        bytesWrittenSoFar += bytesWritten;
    }
    
    return ret;
}

- (CZSMBErrorCode)removeReadonlyAttribute:(NSString*) fileName {
    CZSMBErrorCode error = kErrorCodeNone;
    uint16_t attribute = kSMB_FILE_ATTRIBUTE_NORMAL;
    CZFileDescriptor *fileDesc = [self.adapter getAttribute:fileName
                                                       error:&error];
    if ([CZSMBErrorHandler isError:error] == NO) {
        attribute = fileDesc.attributes^kSMB_FILE_ATTRIBUTE_READONLY;
    }
    
    return [self removeReadonlyAttribute:fileName
                                attriute:attribute];
}

- (CZSMBErrorCode)removeReadonlyAttribute:(NSString*) fileName attriute:(uint16_t) attribute {
    CZSMBErrorCode error = kErrorCodeNone;
    CZFileDescriptor *fileDesc = [self.adapter openFile:fileName
                                                options:kOpenFlagReadOnly|kOpenFlagWriteAttributes
                                                  error:&error];
    
    if ([CZSMBErrorHandler isError:error]) {
        return error;
    }
    fileDesc.attributes = attribute;
    error = [self.adapter setAttribute:fileDesc];
    if ([CZSMBErrorHandler isError:error]) {
        return error;
    }
    error = [self.adapter closeFile:fileDesc];
    return error;
}

- (void)deleteItem {
    _error = kErrorCodeNone;
    
    if (self.queue) {
        self.queue.maxConcurrentOperationCount = 1;
    }
    
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:self.taskInfo.serverPath];
    if (smbPath.isValidFile) {
        _error = [self.adapter deleteFile:self.taskInfo.serverPath];
        if (_error == kSTATUS_CANNOT_DELETE && self.taskInfo.isForce) {
            _error = [self removeReadonlyAttribute:self.taskInfo.serverPath];
            _error = [self.adapter deleteFile:self.taskInfo.serverPath];
        }
    }else if (smbPath.isValidPath) {
        _error = [self removeDirectory:[smbPath.relativePath stringByReplacingOccurrencesOfString:@"\\"
                                                                                       withString:@"/"]
                                    at:[smbPath.share stringByReplacingOccurrencesOfString:@"\\"
                                                                                withString:@"/"]];
    }else {
        _error = kErrorCodeServerPathError;
        return;
    }
}

- (CZSMBErrorCode)removeDirectory:(NSString*) directory at:(NSString*) sharePath{
    NSMutableArray *stackArray = [[NSMutableArray alloc] init];
    [stackArray addObject:[NSString stringWithFormat:@"%@%@",sharePath,directory]];
    CZSMBErrorCode error = kErrorCodeNone;
    
    while ([stackArray count] > 0) {
        NSString *path = [stackArray lastObject];
        
        ///skip the error.
        error = kErrorCodeNone;
        ///for performance considering, let's remove all files first.
        [self.adapter deleteFile:[NSString stringWithFormat:@"%@*",
                                  path]];
        NSArray *itemsArray = [self.adapter listDirectory:path
                                                    error:&error];
        if ([CZSMBErrorHandler isError:error]) {
            break;
        }
        ///if less 3 items, that means this folder is empty.
        if ([itemsArray count] < 3) {
            error = [self.adapter deleteDirectory:path];
            if ([CZSMBErrorHandler isError:error]) {
                [stackArray removeAllObjects];
                break;
            }else {
                [stackArray removeLastObject];
                continue;
            }
        }
        for (CZFileDescriptor * file in itemsArray) {
            if ([file.fileName isEqualToString:@"."] || [file.fileName isEqualToString:@".."]) {
                continue;
            }
            if (file.isDirectory) {
                [stackArray addObject:[NSString stringWithFormat:@"%@%@%@/",
                                       sharePath,
                                       file.slashFilePath,
                                       file.fileName]];
            }else {
                NSString * filePath = [NSString stringWithFormat:@"%@%@%@",
                                       sharePath,
                                       file.slashFilePath,
                                       file.fileName];
                ///set read-only file's attribute
                if ((file.attributes&kSMB_FILE_ATTRIBUTE_READONLY) && self.taskInfo.isForce) {
                    CZLog(@"Find read-only file:%@",file.fileName);
                    [self removeReadonlyAttribute:filePath
                                         attriute:file.attributes^kSMB_FILE_ATTRIBUTE_READONLY];
                }
                error = [self.adapter deleteFile:filePath];
                
                if ([CZSMBErrorHandler isError:error]) {
                    ///delete file failures. so, clear all in stack, and break;
                    [stackArray removeAllObjects];
                    break;
                }
            }
        }
    }
    
    return error;
}

- (void)isServerAvailable {
    _error = kErrorCodeHostUnreachable;
    
    if (self.queue) {
        self.queue.maxConcurrentOperationCount = 2;
    }
    
    if ([CZNetworkUtilities isNetworkEnable] == NO) {
        _error = kErrorCodeHostUnreachable;
        return;
    }
    CFReadStreamRef readStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)self.taskInfo.serverPath, kDirectHostPortNumber, &readStream, NULL);
    
    if (!readStream) {
        _error = kErrorCodeStreamFailure;
        return;
    }
    
    inputStream = (__bridge NSInputStream *)readStream;
    [inputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSRunLoopCommonModes];
    [inputStream open];
    
    @autoreleasepool {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:self.taskInfo.timeout]];
    }
    [self endStream];
}

- (void)endStream {
    if (inputStream) {
        [inputStream close];
        [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                               forMode:NSRunLoopCommonModes];
    }
}

#pragma mark - private

+ (uint64_t)freeSpaceInBytesForPath:(NSString *)filePath {
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSString *parentFolder = [filePath stringByDeletingLastPathComponent];
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:parentFolder error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        CZLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

#pragma mark - Stream Delegate Callback
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent) eventCode {
#pragma unused(aStream)
    switch (eventCode) {
        case NSStreamEventOpenCompleted:
        case NSStreamEventHasBytesAvailable:
        case NSStreamEventHasSpaceAvailable: {
            [self endStream];
            _error = kErrorCodeNone;
        } break;
        case NSStreamEventErrorOccurred:
        case NSStreamEventEndEncountered: {
            [self endStream];
            _error = kErrorCodeHostUnreachable;
        } break;
        default: {
            assert(NO);
        } break;
    }
}
@end
