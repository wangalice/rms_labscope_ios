//
//  CZBasicOperation.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZBasicOperation.h"

@implementation CZBasicOperation

- (void)entry {
    ///override this method in subclass
}


- (void)main {
    @autoreleasepool {
        [self entry];
    }
}

@end
