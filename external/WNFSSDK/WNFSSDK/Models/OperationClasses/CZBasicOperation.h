//
//  CZBasicOperation.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSMBTaskKeyPath                             @"isFinished"
#define kSMBTaskProgressKeyPath                     @"progress"
#define kSMBTaskStatusKeyPath                       @"itemsCount"

@interface CZBasicOperation : NSOperation

@property (nonatomic,strong) NSOperationQueue *     queue;
@end
