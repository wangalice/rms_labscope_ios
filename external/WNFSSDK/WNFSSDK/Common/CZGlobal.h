//
//  Global.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/5/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#ifndef WNFSSDK_Global_h
#define WNFSSDK_Global_h

#ifndef CZLog
#ifdef  DEBUG
#define CZLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#else
#define CZLog(...)
#endif
#endif

#endif
