//
//  CZOperationLock.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kloopConditionInterval      0.01f
#define kloopConditionInterval1     0.1f

@interface CZOperationLock : NSObject

+ (void)lockWith:(int*) value1 lessThan:(int*) value2;
+ (void)lockWith:(int*) value1 lessThanConstant:(int) value2;
+ (void)lockWith:(int*) value1 moreThanConstant:(int) value2;
@end
