//
//  CZSMBErrorHandler.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBErrorHandler.h"
#import "CZGlobal.h"


#define CZLocalizedString(key, comment) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"CZSMBErrorMessage"]



#define kSMBErrorDomain     @"com.carlzeiss.Hermens.CIFS"


@implementation CZSMBErrorHandler

/**
 *  NTSTATUS encoding are 32 bit values layed out as follows:
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | | | | | | | | | | |1| | | | | | | | | |2| | | | | | | | | |3| |
 * |0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|7|8|9|0|1|
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |Sev|C|R| Facility              | Code                          |
 * +-------+-----------------------+-------------------------------+
 *
 *      Sev - is the severity code
 *
 *          00 - Success
 *          01 - Informational
 *          10 - Warning
 *          11 - Error
 *
 *      C - is the Customer code flag
 *
 *      R - is a reserved bit
 *
 *  More information please reference to MS-ERREF.pdf
 */

+ (CZSMBNTStatusLevel)getNTStatusLevel:(CZNTStatusCode) code {
    if (code&0xC0000000) {
        return CZSMBNTStatusLevelError;
    }
    
    if (code&0x80000000) {
        return CZSMBNTStatusLevelWarning;
    }
    
    if (code&0x40000000) {
        return CZSMBNTStatusLevelInformational;
    }
    
    return CZSMBNTStatusLevelNone;
}

+ (BOOL)isError:(CZSMBErrorCode) code {
    if (code&0xE0000000) {
        return YES;
    }
    return NO;
}

+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code description :(NSString *)desc {
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : desc };
    
    return [NSError errorWithDomain:kSMBErrorDomain
                               code:code
                           userInfo:userInfo];
}

+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code description :(NSString *)desc path:(NSString*)filePath {
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : desc ,NSFilePathErrorKey : filePath};
    
    return [NSError errorWithDomain:kSMBErrorDomain
                               code:code
                           userInfo:userInfo];
}

+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code {
    return [self createErrorWithCode:code
                         description:[self getErrorMessage:code]];
}

+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code path:(NSString*)filePath{
    return [self createErrorWithCode:code
                         description:[self getErrorMessage:code]
                                path:filePath];
}

+ (NSString*)getErrorMessage:(CZSMBErrorCode) code {
    if (code == kErrorCodeNone || ![self isError:code]) {
        return @"N/A";
    }
    ///Customer Code
    switch (code) {
        case kErrorCodeHostUnreachable:
            return CZLocalizedString(@"ErrorCodeHostUnreachable", @"The host can not reachable.");
        case kErrorCodeConnectTimeout:
            return CZLocalizedString(@"ErrorCodeConnectTimeout", @"The host connect timeout.");
        case kErrorCodeFilePathError:
            return CZLocalizedString(@"ErrorCodeFilePathError", @"The file path is invalid.");
        case kErrorCodeServerNoResponse:
            return CZLocalizedString(@"ErrorCodeServerNoResponse", @"The packet receive timed out.");
        case kErrorCodeInvalidParameter:
            return CZLocalizedString(@"ErrorCodeInvalidParameter", @"Parameters invalid.");
        case kErrorCodeServerPathError:
            return CZLocalizedString(@"ErrorCodeServerPathError", @"The server path is invalid.");
        case kErrorCodeStreamFailure:
            return CZLocalizedString(@"ErrorCodeStreamFailure", @"The network stream failure.");
        case kErrorCodeLogonFailure:
            return CZLocalizedString(@"ErrorCodeLogonFailure", @"User logon failure.");
        case kErrorCodeLocalPathError:
            return CZLocalizedString(@"ErrorCodeLocalPathError", @"The local path is invalid.");
        case kErrorCodePacketError:
            return CZLocalizedString(@"ErrorCodePacketError", @"The packet of server response is invalid.");
        case kErrorCodeFileDescriptorError:
            return CZLocalizedString(@"ErrorCodeFileDescriptorError", @"The file descriptor is invalid.");
        case kErrorCodeLocalFileWriteError:
            return CZLocalizedString(@"ErrorCodeLocalFileWriteError", @"File write operation error, please make sure you have write privileges for the file at path.");
        case kErrorCodeOperationCancelled:
            return CZLocalizedString(@"ErrorCodeOperationCancelled", @"Current operation has cancelled.");
        case kErrorCodeLocalFileExist:
            return CZLocalizedString(@"ErrorCodeLocalFileExist", @"The local file already exists.");
        case kErrorCodeLocalFileDoesNotExist:
            return CZLocalizedString(@"ErrorCodeLocalFileDoesNotExist", @"The local file does not exist.");
        case kErrorCodeLocalFileReadFailed:
            return CZLocalizedString(@"ErrorCodeLocalFileReadFailed", @"Local file operation failed.");
        case kErrorCodeFileSeekOutofRange:
            return CZLocalizedString(@"ErrorCodeFileSeekOutofRange", @"The file seek offset is out of range.");
        case kErrorCodeLocalFileStreamError:
            return CZLocalizedString(@"ErrorCodeLocalFileStreamError", @"The local file specified by path doesn’t exist or is unreadable.");
        case kErrorCodeFileNameInvalid:
            return CZLocalizedString(@"ErrorCodeFileNameInvalid", @"The file name is invalid.");
        case kErrorCodeDirectoryInvalid:
            return CZLocalizedString(@"ErrorCodeDirectoryInvalid", @"The directory is invalid.");
        case kErrorCodeSubCommandInvalid:
            return CZLocalizedString(@"ErrorCodeSubCommandInvalid", @"The sub command of transaction2 is invalid.");
        case kErrorCodeSearchIDInvalid:
            return CZLocalizedString(@"ErrorCodeSearchIDInvalid", @"The search ID is invalid in FIND_NEXT2 command.");
        case kErrorCodeArgumentInvalid:
            return CZLocalizedString(@"ErrorCodeArgumentInvalid", @"The argument is invalid.");
        case kErrorCodeConnectClose:
            return CZLocalizedString(@"ErrorCodeConnectClose", @"The connecting has closed.");
        case kErrorCodeServerIncompatible:
            return CZLocalizedString(@"ErrorCodeServerIncompatible", @"The App is not compatible to the server.");
        default:
            break;
    }
    
    ///NT Code
    switch (code) {
        case kSTATUS_ACCOUNT_LOCKED_OUT:
            return CZLocalizedString(@"STATUS_ACCOUNT_LOCKED_OUT", @"The user account has been automatically locked because too many invalid logon attempts or password change attempts have been requested.");
        case kSTATUS_LOGON_FAILURE:
            return CZLocalizedString(@"STATUS_LOGON_FAILURE", @"The attempted logon is invalid. This is either due to a bad username or authentication information.");
        case kSTATUS_ACCESS_DENIED:
            return CZLocalizedString(@"STATUS_ACCESS_DENIED", @"Access denied.");
        case kSTATUS_BUFFER_OVERFLOW:
            return CZLocalizedString(@"STATUS_BUFFER_OVERFLOW", @"The number of bytes read from the named pipe exceeds the MaxDataCount field in the client request.");
        case kSTATUS_PASSWORD_EXPIRED:
            return CZLocalizedString(@"STATUS_PASSWORD_EXPIRED",@"The user account password has expired.");
        case kSTATUS_SMB_BAD_TID:
            return CZLocalizedString(@"STATUS_SMB_BAD_TID",@"The TID specified in the request is invalid.");
        case kSTATUS_SMB_BAD_UID:
            return CZLocalizedString(@"STATUS_SMB_BAD_UID",@"The UID supplied is not known to the session, or the user identified by the UID does not have sufficient privileges.");
        case kSTATUS_NO_SUCH_FILE:
            return CZLocalizedString(@"STATUS_NO_SUCH_FILE",@"The file does not exist.");
        case kSTATUS_NO_SUCH_DEVICE:
            return CZLocalizedString(@"STATUS_NO_SUCH_DEVICE",@"A device that does not exist was specified.");
        case kSTATUS_OBJECT_NAME_NOT_FOUND:
            return CZLocalizedString(@"STATUS_OBJECT_NAME_NOT_FOUND",@"The file can not be opened or the user does not have permission to access.");
        case kSTATUS_INVALID_PARAMETER:
            return CZLocalizedString(@"STATUS_INVALID_PARAMETER",@"One of the extended attributes had an invalid Flag bit value.");
        case kSTATUS_OBJECT_PATH_NOT_FOUND:
            return CZLocalizedString(@"STATUS_OBJECT_PATH_NOT_FOUND",@"The path does not exist.");
        case kSTATUS_BAD_NETWORK_NAME:
            return CZLocalizedString(@"STATUS_BAD_NETWORK_NAME",@"The specified share name cannot be found on the remote server.");
        case kSTATUS_BAD_DEVICE_TYPE:
            return CZLocalizedString(@"STATUS_BAD_DEVICE_TYPE",@"The specified device type (LPT, for example) conflicts with the actual device type on the remote resource.");
        case kSTATUS_PASSWORD_MUST_CHANGE:
            return CZLocalizedString(@"STATUS_PASSWORD_MUST_CHANGE",@"The user password must be changed before logging on the first time.");
        case kSTATUS_DISK_FULL:
            return CZLocalizedString(@"STATUS_DISK_FULL",@"An operation failed because the disk was full.");
        case kSTATUS_OBJECT_NAME_COLLISION:
            return CZLocalizedString(@"STATUS_OBJECT_NAME_COLLISION",@"The object name already exists.");
        case kSTATUS_DIRECTORY_NOT_EMPTY:
            return CZLocalizedString(@"STATUS_DIRECTORY_NOT_EMPTY",@"The directory trying to be deleted is not empty.");
        case kSTATUS_CANNOT_DELETE:
            return CZLocalizedString(@"STATUS_CANNOT_DELETE",@"This item cannot be deleted.");
        case kSTATUS_SHARING_VIOLATION:
            return CZLocalizedString(@"STATUS_SHARING_VIOLATION",@"Sharing violation.");
        case kSTATUS_OBJECT_NAME_INVALID:
            return CZLocalizedString(@"STATUS_OBJECT_NAME_INVALID",@"Object Name invalid.");
        case kSTATUS_DELETE_PENDING:
            return CZLocalizedString(@"STATUS_DELETE_PENDING",@"A non-close operation has been requested of a file object that has a delete pending.");
        case kSTATUS_LOGON_TYPE_NOT_GRANTED:
            return CZLocalizedString(@"STATUS_LOGON_TYPE_NOT_GRANTED",@"A user has requested a type of logon that has not been granted. An administrator has control over who may logon interactively and through the network.");
        case kSTATUS_INVALID_HANDLE:
            return CZLocalizedString(@"STATUS_INVALID_HANDLE",@"An invalid HANDLE was specified.");
        case kSTATUS_WRONG_PASSWORD:
            return CZLocalizedString(@"STATUS_WRONG_PASSWORD",@"When trying to update a password, this return status indicates that the value provided as the current password is not correct.");
        case kSTATUS_NO_LOGON_SERVERS:
            return CZLocalizedString(@"STATUS_NO_LOGON_SERVERS",@"No logon servers are currently available to service the logon request.");
        case kSTATUS_ACCOUNT_RESTRICTIONS:
            return CZLocalizedString(@"STATUS_ACCOUNT_RESTRICTIONS",@"Indicates a referenced user name and authentication information are valid, but some user account restriction has prevented successful authentication (such as time-of-day restrictions).");
        default:
            break;
    }
    
    return [NSString stringWithFormat:@"Unexpected error:%lx",(unsigned long)code];
}

+ (void)printServerError:(CZSMBErrorCode) code {
    CZLog(@"%@ Error code:%lx",[self getErrorMessage:code],(unsigned long)code);
}
@end
