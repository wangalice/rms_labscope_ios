//
//  CZOperationLock.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/22/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZOperationLock.h"

@implementation CZOperationLock

+ (void)lockWith:(int*) value1 lessThan:(int*) value2 {
    while (*value1 < *value2) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval]];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
}

+ (void)lockWith:(int*) value1 lessThanConstant:(int) value2 {
    while (*value1 < value2) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval]];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
}

+ (void)lockWith:(int*) value1 moreThanConstant:(int) value2 {
    while (*value1 > value2) {
        @autoreleasepool {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:kloopConditionInterval]];
        }
        [NSThread sleepForTimeInterval:0.01];
    }
}
@end
