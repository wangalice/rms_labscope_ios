//
//  CZNetworkUtilities.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface CZNetworkUtilities : NSObject

+ (BOOL)isIPAddress:(NSString*) ip;
+ (BOOL)isNetworkEnable;
+ (NSString *)getIPAddress;
+ (NSArray *)hostNameFrom:(NSString *)address;
@end
