//
//  CZWNFSManager.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZWNFSManagerDelegate.h"

typedef NS_ENUM(NSUInteger, CZSMBListItemsMode)
{
    CZSMBListItemsModeAll = 0,
    CZSMBListItemsModeOnlyFile = 1,
};

@class CZSMBAdapter;
@interface CZWNFSManager : NSObject

/**
 * @brief WNFSManager's shared instance.
 * @return Returns the WNFSManager’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;


- (id)sharedSMBAdapter;
- (id)sharedSMBAdapterAndCreateIfNil;
/**
 * @brief Indicate the server is reachable or not.
 * @param server IP address or host name.
 * @param timeout Specifying the maximum amount of time allowed to perform the server connection, in seconds.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error.
 */
- (void)isServerReachable:(NSString*) server timeout:(float) timeout delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Set the user account information.
 * @param domain Domain of the user account in Windows network.
 * @param aName User name of the account.
 * @param aPassword User password of the account.
 */
- (void)setAccountInformation:(NSString*) domain userName:(NSString*) aName password:(NSString*) aPassword;

/**
 * @brief Using to copy a file from the windows server to local.
 * @param serverPath The share path on the windows PC/Server. This path should including share file name.
 * @param localFilePath The local file path on iOS, this path should including file name component.
 * @param isOverwriting This value using to prevent overwriting an existing file on local.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error.
 */
- (void)getFileFrom:(NSString*) serverPath local:(NSString*) localFilePath overwriting:(BOOL) isOverwriting delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Using to send a local file to the windows shared.
 * @param serverPath The share path on the windows PC/Server. This path should including share file name.
 * @param localFilePath The local file path on iOS, this path should including file name component.
 * @param isOverwriting This value using to prevent overwriting an existing file on remote.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error. 
 */
- (void)sendFileTo:(NSString*) serverPath local:(NSString*) localFilePath overwriting:(BOOL) isOverwriting delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Using to list all items for a windows shared directory.
 * @param serverPath The share path on the windows PC/Server. This path must be directory.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error.
 */
- (void)listItemsFrom:(NSString*) serverPath delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Using to list all items for a windows shared directory.
 * @param serverPath The share path on the windows PC/Server. This path must be directory.
 * @param mode The value indicate that when we list items, the resulte should include directory or not.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error.
 */
- (void)listItemsFrom:(NSString*) serverPath mode:(CZSMBListItemsMode) aMode delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Using to delete a file/directory at windows shared. if the server path is directory, this operation will delete all sub items without warning.
 * @param serverPath The share path on the windows PC/Server. This path can be a directory or file name.
 * @param force This parameter indicate that operation will force delete all times include Read-Only.
 * @param delegate The class provides a delegate method that allows the higher level modules can reciver data and error.
 */
- (void)deleteItemFrom:(NSString*) serverPath force:(BOOL) isForce delegate:(id<CZWNFSManagerDelegate>)delegate;

- (void)deleteItemFrom:(NSString*) serverPath delegate:(id<CZWNFSManagerDelegate>)delegate;

/**
 * @brief Using to cancel all operation in task queue.
 */
- (void)cancelAllOperation;

/**
 * @brief Using to relese adapter, this method will close network connection and cancel all operation in task queue.
 */
- (void)releaseAdapter;

@end
