//
//  CZSMBAdapter.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZSMBErrorHandler.h"

/**
 * Open for reading only
 */
#define kOpenFlagReadOnly               0x0001
/**
 * Open for writing only
 */
#define kOpenFlagWriteOnly              0x0002
/**
 * Open for reading and writing
 */
#define kOpenFlagReadWrite              kOpenFlagReadOnly|kOpenFlagWriteOnly
/**
 * Create file if it does not exist, If the file already exists, then open it.
 */
#define kOpenFlagCreate                 0x0008
/**
 * Append on each write
 */
#define kOpenFlagAppend                 0x0010
/**
 * Truncate the file size to 0
 */
#define kOpenFlagTruncate               0x0020
/**
 * Return error, If kOpenFlagCreate and file exists.
 */
#define kOpenFlagExists                 0x0040
/**
 * Atomically obtain a shared lock
 */
#define kOpenFlagShareLock              0x0080
/**
 * Atomically obtain an exclusive lock
 */
#define kOpenFlagExclusiveLock          0x0100

#define kOpenFlagWriteAttributes        0x0200

#define kOpenFlagDelete                 0x0400

typedef uint32_t CZSMBFileOpenFlag;

typedef NS_ENUM(NSUInteger, CZSMBFileSeekMode)
{
    CZSMBFileSeekModeSet = 0,
    CZSMBFileSeekModeCur = 1,
    CZSMBFileSeekModeEnd = 2,
};

typedef NS_ENUM(NSUInteger, CZSMBFileAttribute)
{
    CZSMBFileAttributeNormal = 0,
    CZSMBFileAttributeReadOnly = 1,
};

#define kFileAttributeNormal            0x0000
#define kFileAttributeReadOnly          0x0001

@class CZFileDescriptor, CZDiskInformation;

@interface CZSMBAdapter : NSObject

@property (nonatomic,copy)    NSString *                  serverName;
@property (nonatomic,copy)    NSString *                  serverIP;
@property (nonatomic,copy)    NSString *                  shareName;
@property (nonatomic,copy)    NSString *                  domainName;
@property (nonatomic,copy)    NSString *                  userName;
@property (nonatomic,copy)    NSString *                  password;

/**
 * Perform initialization with user account information. The subsequent operation will rely on the account information.
 
 @return The instance of CZSMBAdapter.
 */
- (id)initWithAccountInformation:(NSString *) domain userName:(NSString *) aName password:(NSString *) aPassword;
/**
 * Change the account information after initialization.
 */
- (void)setAccountInformation:(NSString *) domain userName:(NSString *) aName password:(NSString *) aPassword;
/**
 * Performs a shallow search of the specified directory and returns the paths of any contained but hidden, system items.
 */
- (NSArray *)listDirectory:(NSString *)directoryPath error:(CZSMBErrorCode*)error;

- (NSArray *)enumServerFrom:(NSString *) hostPath error:(CZSMBErrorCode*)error;
- (CZFileDescriptor*)getAttribute:(NSString *) filePath error:(CZSMBErrorCode*)error;
- (CZSMBErrorCode)setAttribute:(CZFileDescriptor *) fileDescriptor;
- (CZFileDescriptor*)openFile:(NSString*) filePath options:(CZSMBFileOpenFlag)mask error:(CZSMBErrorCode *)error;
- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor buffer:(void *)dataBuffer size:(uint32_t)bufferSize;
- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor buffer:(void *)dataBuffer size:(uint32_t)bufferSize preload:(BOOL)isPreload;
- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor byLength:(uint32_t)size data:(NSData **)dataBuffer;
- (CZSMBErrorCode)readFile:(CZFileDescriptor *)fileDescriptor byLength:(uint32_t)size preload:(BOOL)isPreload data:(NSData **)dataBuffer;
- (CZSMBErrorCode)writeFile:(CZFileDescriptor *)fileDescriptor buffer:(void *)inputBuffer size:(uint16_t)bufferSize;
- (CZSMBErrorCode)seekFile:(CZFileDescriptor *)fileDescriptor offset:(int64_t)fileOffset mode:(CZSMBFileSeekMode) seekMode;
- (CZSMBErrorCode)deleteFile:(NSString *)filePath;
- (CZSMBErrorCode)deleteDirectory:(NSString *)directoryPath;
- (CZSMBErrorCode)closeFile:(CZFileDescriptor *)fileDescriptor;
- (CZSMBErrorCode)renameFile:(NSString *)oldFileName newName:(NSString *)newFileName;
- (CZSMBErrorCode)getDiskInformation:(CZDiskInformation *)diskInfo;

@end
