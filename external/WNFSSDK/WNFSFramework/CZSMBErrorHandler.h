//
//  CZSMBErrorHandler.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/21/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef CZPrintError
#ifdef  DEBUG
#define CZPrintError(A) [CZSMBErrorHandler printServerError:A]
#else
#define CZPrintError(...)
#endif
#endif


#pragma mark NT Error Codes

/**
 * The user account has been automatically locked because too many invalid logon attempts or password change attempts have been requested.
 */
#define kSTATUS_ACCOUNT_LOCKED_OUT          (0xC0000234)
/**
 * The attempted logon is invalid. This is either due to a bad username or authentication information.
 */
#define kSTATUS_LOGON_FAILURE               (0xC000006D)
#define kSTATUS_INVALID_SMB                 (0x00010002)
#define kSTATUS_INSUFF_SERVER_RESOURCES     (0xC0000205)
#define kSTATUS_TOO_MANY_SESSIONS           (0xC00000CE)

/**
 * The user account password has expired.
 */
#define kSTATUS_PASSWORD_EXPIRED            (0xC0000071)
/**
 * The TID specified in the request is invalid.
 */
#define kSTATUS_SMB_BAD_TID                 (0x00050002)
/**
 * The UID supplied is not known to the session, or the user identified by the UID does not have sufficient privileges.
 */
#define kSTATUS_SMB_BAD_UID                 (0x005B0002)

/**
 * The named file was not found.
 */
#define kSTATUS_NO_SUCH_FILE                (0xC000000F)

/**
 * The file path syntax is invalid.
 */
#define kSTATUS_OBJECT_PATH_SYNTAX_BAD      (0xC000003B)
/**
 * Access denied.
 */
#define kSTATUS_ACCESS_DENIED               (0xC0000022)
/**
 * Sharing violation.
 */
#define kSTATUS_SHARING_VIOLATION           (0xC0000043)
/**
 * The size of the extended attribute list is not correct. Check the EaErrorOffset field for address of the SMB_GEA structure at which the error was detected.
 */
#define kSTATUS_UNSUCCESSFUL                (0xC0000001)
/**
 * One of the extended attributes had an invalid Flag bit value.
 */
#define kSTATUS_INVALID_PARAMETER           (0xC000000D)
/**
 * Invalid SMB. Not enough parameter bytes were sent.
 */
#define kSTATUS_INVALID_SMB                 (0x00010002)
/**
 * The number of bytes read from the named pipe exceeds the MaxDataCount field in the client request.
 */
#define kSTATUS_BUFFER_OVERFLOW             (0x80000005)

/**
 * The named file was not found.
 */
#define kSTATUS_NO_SUCH_FILE                (0xC000000F)
#define kSTATUS_NO_SUCH_DEVICE              (0xC000000E)
#define kSTATUS_OBJECT_NAME_NOT_FOUND       (0xC0000034)

/**
 * The path does not exist.
 */
#define kSTATUS_OBJECT_PATH_NOT_FOUND       (0xC000003A)

/**
 * The specified share name cannot be found on the remote server.
 */
#define kSTATUS_BAD_NETWORK_NAME            (0xC00000CC)

/**
 * The specified device type (LPT, for example) conflicts with the actual device type on the remote resource.
 */
#define kSTATUS_BAD_DEVICE_TYPE             (0xC00000CB)

/**
 * The user password must be changed before logging on the first time.
 */
#define kSTATUS_PASSWORD_MUST_CHANGE        (0xC0000224)

/**
 * An operation failed because the disk was full.
 */
#define kSTATUS_DISK_FULL                   (0xC000007F)

/**
 * The object name already exists.
 */
#define kSTATUS_OBJECT_NAME_COLLISION       (0xC0000035)

/**
 * Indicates that the directory trying to be deleted is not empty.
 */
#define kSTATUS_DIRECTORY_NOT_EMPTY         (0xC0000101)

/**
 * The directory is in use.
 */
#define kSTATUS_CANNOT_DELETE               (0xC0000121)

/**
 * Sharing violation.
 */
#define kSTATUS_SHARING_VIOLATION           (0xC0000043)

/**
 * Object Name invalid.
 */
#define kSTATUS_OBJECT_NAME_INVALID         (0xC0000033)

/**
 * A non-close operation has been requested of a file object that has a delete pending.
 */
#define kSTATUS_DELETE_PENDING              (0xC0000056)

/**
 * A user has requested a type of logon (for example, interactive or network) that has not been granted. An administrator has control over who may logon interactively and through the network.
 */
#define kSTATUS_LOGON_TYPE_NOT_GRANTED      (0xC000015B)

/**
 * An invalid HANDLE was specified.
 */
#define kSTATUS_INVALID_HANDLE              (0xC0000008)

/**
 * When trying to update a password, this return status indicates that the value provided as the current password is not correct.
 */
#define kSTATUS_WRONG_PASSWORD              (0xC000006A)

/**
 * No logon servers are currently available to service the logon request.
 */
#define kSTATUS_NO_LOGON_SERVERS              (0xC000005E)

/**
 * Indicates a referenced user name and authentication information are valid, but some user account restriction has prevented successful authentication (such as time-of-day restrictions).
 */
#define kSTATUS_ACCOUNT_RESTRICTIONS          (0xC000006E)

#pragma mark Customer Error Codes

#define kErrorCodeNone                      (0x00000000)
#define kErrorCodeCustomer                  (0x20000000)
#define kErrorCodeUnexpected                (kErrorCodeCustomer|0x01)
#define kErrorCodeHostUnreachable           (kErrorCodeCustomer|0x02)
#define kErrorCodeConnectTimeout            (kErrorCodeCustomer|0x03)
#define kErrorCodeLocalPathError            (kErrorCodeCustomer|0x04)
#define kErrorCodeServerPathError           (kErrorCodeCustomer|0x05)
#define kErrorCodeFilePathError             (kErrorCodeCustomer|0x06)
#define kErrorCodeOperateTimeout            (kErrorCodeCustomer|0x07)
#define kErrorCodeLogonFailure              (kErrorCodeCustomer|0x08)
#define kErrorCodeStreamFailure             (kErrorCodeCustomer|0x09)
#define kErrorCodeServerNoResponse          (kErrorCodeCustomer|0x10)
#define kErrorCodeOperationCancelled        (kErrorCodeCustomer|0x11)
#define kErrorCodePacketError               (kErrorCodeCustomer|0x12)
#define kErrorCodeInvalidParameter          (kErrorCodeCustomer|0x13)
#define kErrorCodeFileDescriptorError       (kErrorCodeCustomer|0x14)
#define kErrorCodeLocalFileWriteError       (kErrorCodeCustomer|0x15)
#define kErrorCodeLocalFileExist            (kErrorCodeCustomer|0x16)
#define kErrorCodeLocalFileDoesNotExist     (kErrorCodeCustomer|0x17)
#define kErrorCodeLocalFileReadFailed       (kErrorCodeCustomer|0x18)
#define kErrorCodeFileSeekOutofRange        (kErrorCodeCustomer|0x19)
#define kErrorCodeLocalFileStreamError      (kErrorCodeCustomer|0x20)
#define kErrorCodeFileNameInvalid           (kErrorCodeCustomer|0x21)
#define kErrorCodeDirectoryInvalid          (kErrorCodeCustomer|0x22)
#define kErrorCodeSubCommandInvalid         (kErrorCodeCustomer|0x23)
#define kErrorCodeSearchIDInvalid           (kErrorCodeCustomer|0x24)
#define kErrorCodeArgumentInvalid           (kErrorCodeCustomer|0x25)
#define kErrorCodeConnectClose              (kErrorCodeCustomer|0x26)
#define kErrorCodeServerIncompatible        (kErrorCodeCustomer|0x27)

typedef NSUInteger CZSMBErrorCode;

typedef int32_t CZNTStatusCode;

typedef NS_ENUM(NSUInteger, CZSMBNTStatusLevel)
{
    CZSMBNTStatusLevelNone = 0,
    CZSMBNTStatusLevelError = 1,
    CZSMBNTStatusLevelWarning = 2,
    CZSMBNTStatusLevelInformational = 4,
};

@interface CZSMBErrorHandler : NSObject

+ (CZSMBNTStatusLevel)getNTStatusLevel:(CZNTStatusCode) code;
+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code description:(NSString*) desc;
+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code;
+ (NSError*)createErrorWithCode:(CZSMBErrorCode) code path:(NSString*)filePath;
+ (BOOL)isError:(CZSMBErrorCode) code;
+ (void)printServerError:(CZSMBErrorCode) code;
+ (NSString*)getErrorMessage:(CZSMBErrorCode) code;
@end
