//
//  CZWNFSManagerDelegate.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/25/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZOperateType)
{
    CZOperateTypeListDirectory = 0,
    CZOperateTypeDeleteItem = 1,
};

typedef NS_ENUM(NSUInteger, CZTransferDirection)
{
    CZTransferDirectionSending = 0,
    CZTransferDirectionReceiving = 1,
};

@protocol CZWNFSManagerDelegate <NSObject>
@optional
/**
 * @brief Using to receive the notification from listItemsFrom/deleteItemFrom.
 * @param item This value encapsulating the result for listItemsFrom/deleteItemFrom.
 * @param type CZOperateTypeListDirectory or CZOperateTypeDeleteItem.
 */
- (void)operateDidFinishItem:(NSDictionary*) item type:(CZOperateType) type;
/**
 * @brief Using to receive the notification from getFileFrom/sendFileTo.
 * @param error This value is type of NSError.
 */
- (void)operateDidFailWithError:(NSError*) error;
/**
 * @brief Using to receive the notification from listItemsFrom.
 * @param status This value encapsulating the status of operate.
 * @param type CZOperateTypeListDirectory or CZOperateTypeDeleteItem (Only support listDirectory status update for now).
 */
- (void)operateDidChangeStatus:(NSDictionary*) status type:(CZOperateType) type;
/**
 * @brief Using to receive the notification from getFileFrom/sendFileTo.
 * @param direction CZTransferDirectionSending or CZTransferDirectionReceiving.
 * @param bytes The number of bytes already delivered.
 * @param totalBytes The total bytes of the file.
 * @param path The destination path.
 */
- (void)dataTransferProgress:(CZTransferDirection) direction delivered:(uint64_t) bytes total:(uint64_t) totalBytes destination:(NSString *) path;

/**
 * @brief Using to receive the notification from getFileFrom/sendFileTo when task finished.
 * @param path The destination path.
 */
- (void)dataTransferDidFinishDestination:(NSString*) path;

/**
 * @brief Using to receive the notification from getFileFrom/sendFileTo when error occured.
 * @param error This value is type of NSError.
 */
- (void)dataTransferDidFailWithError:(NSError*)error;

/**
 * @brief Using to receive the notification from isServerReachable.
 * @param server This is destination path.
 */
- (void)reachingDidSuccess:(NSString*)server;

/**
 * @brief Using to receive the notification from isServerReachable when error occured.
 * @param error This value is type of NSError.
 */
- (void)reachingDidFailWithError:(NSError*)error;

/**
 * @brief WNFS manager query file attributes from delegate, when copy file from SMB server.
 * Usually delegate already cachees some file infos, so that WNFS manager doesn't need to query it from server any more.
 * @return file attributes. return nil, if delegate can't provide it.
 */
- (NSDictionary *)attributesOfFile:(NSString *)filePath;

@end
