//
//  CZFileSystemSize.h
//  WNFSSDK
//
//  Created by Jimmy Kong on 6/18/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZDiskInformation : NSObject

/**
 * This field contains the total size of the filesystem allocation unit, in bytes.
 */
@property (nonatomic,readwrite) uint64_t          totalAllocationSize;
/**
 * This field contains the total size of the filesystem free unit, in bytes.
 */
@property (nonatomic,readwrite) uint64_t          totalFreeAllocationSize;;
@end
