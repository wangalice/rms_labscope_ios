//
//  CZFileDescriptor.h
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CZFileDescripttorMaxFileNameSize 254

@interface CZFileDescriptor : NSObject
/**
 * This field contains the date when the file was created.
 */
@property (nonatomic,strong) NSDate *           createDatetime;

/**
 * This field contains the datetime when the file was last accessed.
 */
@property (nonatomic,strong) NSDate *           lastAccessDatetime;

/**
 * This field contains the datetime when data was last written to the file.
 */
@property (nonatomic,strong) NSDate *           lastWriteDatetime;

/**
 * This field contains the file size, in filesystem allocation units.
 */
@property (nonatomic,readwrite) uint64_t          fileDataSize;
/**
 * This field contains the size of the filesystem allocation unit, in bytes.
 */
@property (nonatomic,readwrite) uint64_t          allocationSize;
/**
 * This field contains the file attributes.
 */
@property (nonatomic,readwrite) uint16_t         attributes;
/**
 * This field contains the name of the file.
 */
@property (nonatomic,strong) NSString *         fileName;
/**
 * This field contains the path of the file,  backslash. eg:(\path\file\)
 */
@property (nonatomic,strong) NSString *         filePath;

/**
 * This field contains the path of the file, slash version. eg:(/path/file/)
 */
@property (weak, nonatomic,readonly) NSString *slashFilePath;

#pragma mark - File Operation Properties

@property (nonatomic,readwrite) uint32_t       openFlag;

#pragma mark - SMB Properties

@property (nonatomic,readwrite) uint16_t       FID;

@property (nonatomic,readwrite) uint8_t        opLockLevel;

@property (nonatomic,readwrite) uint32_t       createDisposition;

@property (nonatomic,readwrite) uint64_t       endOfFile;

@property (nonatomic,readwrite) uint64_t       offset;

@property (nonatomic,strong) NSData *          dataPtr;

@property (nonatomic,readwrite) uint32_t       readBytes;

@property (nonatomic,readwrite) uint16_t       writeBytes;

- (BOOL)isDirectory;
@end
