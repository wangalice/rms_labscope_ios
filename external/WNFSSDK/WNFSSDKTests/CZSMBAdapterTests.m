//
//  CZSMBAdapterTests.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBAdapterTests.h"
#import "CZSMBAdapter.h"
#import "CZSMBErrorHandler.h"
#import "CZFileDescriptor.h"
#import "CZDiskInformation.h"

@interface CZSMBAdapterTests() {
    CZSMBAdapter *adapter;
}

@end

@implementation CZSMBAdapterTests

- (void)setUp
{
    [super setUp];
    // Set-up code here.
    adapter = [[CZSMBAdapter alloc] initWithAccountInformation:@"cn"
                                                      userName:@"tim.pan"
                                                      password:@"Qwert,2014"];
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void)testSMBAdapter {
    return;
    [self serverEnum];
    return;
    [self setAttribute];
//    return;
    [self listDirectory];
    [self getDiskInformation];
//    return;
//    [self multiFileRead];
//    return;
//    [adapter renameFile:@"news.txt"
//                newName:@"news1sdf78.txt"];
//    [adapter renameFile:@"123news.txt"
//                newName:@"5455.txt"];
//    return;
    [self openFileAndRead:@"//10.102.37.52/Camel_7/test.png"];
//    return;
    [self getAttribute];
    
    [self listDirectory];
    [self deleteFile];
//    return;
//    [self openFileAndRead:@"//10.102.37.52/Camel_7/test.png"];
    
//    [self listDirectory];

    
//    [self openFileAndWrite];
    
}

- (void)serverEnum {
    CZSMBErrorCode code = 0;
    NSArray * arry = [adapter enumServerFrom:@"//BCD-PC-013/IPC$" error:&code];
    NSLog(@"%@",arry);
}

- (void)setAttribute {
    CZSMBErrorCode code = 0;
    CZFileDescriptor *fileDesc = [adapter openFile:@"//10.102.37.52/Camel_7/test.png"
                                           options:kOpenFlagReadOnly|kOpenFlagWriteAttributes
                                             error:&code];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
    fileDesc.attributes = 0;
    code = [adapter setAttribute:fileDesc];
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
    
    code = [adapter closeFile:fileDesc];
    
}

- (void)getDiskInformation {
    CZDiskInformation * diskInfo = [[CZDiskInformation alloc] init];
    CZSMBErrorCode code = [adapter getDiskInformation:diskInfo];
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
    NSLog(@"Disk total size:%.2f GB",diskInfo.totalAllocationSize/1024.0f/1024.0f/1024.0f);
    NSLog(@"Disk free size:%.2f GB",diskInfo.totalFreeAllocationSize/1024.0f/1024.0f/1024.0f);
}

- (void)multiFileRead {
    [NSThread detachNewThreadSelector:@selector(openFileAndRead:)
                             toTarget:self
                           withObject:@"//10.102.37.52/Camel_7/test.png"];
    [NSThread sleepForTimeInterval:1.0f];
    [NSThread detachNewThreadSelector:@selector(openFileAndRead:)
                             toTarget:self
                           withObject:@"//10.102.37.52/Camel_7/test.png"];
    
    @autoreleasepool {
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:10.0f]];
    }
}

- (void)getAttribute {
    CZSMBErrorCode code;
    CZFileDescriptor *fileDesc = [adapter getAttribute:@"//10.102.37.52/Camel_7/bleum.m4v"
                                                 error:&code];
    NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    NSLog(@"%@",fileDesc);
}

- (void)listDirectory {
    CZSMBErrorCode code;
    id dict = [adapter listDirectory:@"//10.102.37.52/Camel_7/" error:&code];
    
    NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    NSLog(@"%@",dict);
}

- (void)openFileAndRead:(NSString*) fileName {
    
    CZSMBErrorCode code;
    CZFileDescriptor *fileDesc = [adapter openFile:fileName
                                           options:kOpenFlagReadOnly
                                             error:&code];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
    
    XCTAssertNotNil(fileDesc, @"FileDescriptor should not be nil.");
    uint8_t buffer[0xFFFF];
    for (int  i = 0; i < 40; i++) {
        NSLog(@"----read-----thread:%lu",(unsigned long)[[NSThread currentThread] hash]);
        code = [adapter readFile:fileDesc
                          buffer:buffer
                            size:0xFFFF];
        
        NSLog(@"----read end-----thread:%lu",(unsigned long)[[NSThread currentThread] hash]);
        if ([CZSMBErrorHandler isError:code]) {
            NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
        }
        fileDesc.offset += code;
    }
    
    code  = [adapter closeFile:fileDesc];
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
}

- (void)openFileAndWrite {
    CZSMBErrorCode code;
    CZFileDescriptor *fileDesc = [adapter openFile:@"//10.102.37.54/Camel_xp/testWrite.txt"
                                           options:kOpenFlagCreate|kOpenFlagReadWrite
                                             error:&code];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
    
    if (fileDesc.endOfFile == 0) {
         NSLog(@"error: file size:%llu bytes",fileDesc.endOfFile);
    }
    NSLog(@"file size:%llu bytes",fileDesc.endOfFile);
    
    XCTAssertNotNil(fileDesc, @"FileDescriptor should not be nil.");
    const uint16_t kBufferSize = 0xFF00;
    uint8_t buffer[kBufferSize];
    memset(buffer, 0, kBufferSize);
    
    NSString *stringTest = @"This part of the book will cover the basics of SMB, enumerate and describe some of the SMB message types (commands), discuss protocol dialects, give some details on authentication, and provide a few examples. That should be enough to help you develop a working knowledge of the protocol, a working SMB client, and possibly a simple server.";
    
    [stringTest getCString:(char*)buffer
                 maxLength:kBufferSize
                  encoding:NSUnicodeStringEncoding];
    
    code = [adapter writeFile:fileDesc
                       buffer:buffer
                         size:kBufferSize];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }else {
        NSLog(@"have write %lu bytes to file",(unsigned long)code);
    }
    
    code  = [adapter closeFile:fileDesc];
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
}

- (void)deleteFile {
    CZSMBErrorCode code = [adapter deleteFile:@"//10.102.37.54/Camel_xp/test.png"];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
}

- (void)deleteDirectory {
    CZSMBErrorCode code = [adapter deleteDirectory:@"//10.102.37.52/Camel_7/test1/sdf/"];
    
    if ([CZSMBErrorHandler isError:code]) {
        NSLog(@"%@",[CZSMBErrorHandler getErrorMessage:code]);
    }
}
@end
