//
//  CZSMBHostNameTests.m
//  WNFSSDK
//
//  Created by Jimmy Kong on 8/2/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBHostNameTests.h"
#import "CZSMBNameServicePacket.h"
#import "CZSMBNameServiceQuery.h"
#import "CZWNFSManager.h"
#import "CZWNFSManagerDelegate.h"

@interface CZSMBHostNameTests()<CZSMBNameQueryDelegate> {
    CZSMBNameServicePacket * namePacket;
    CZWNFSManager * manager;
}

@end

@implementation CZSMBHostNameTests

- (void)setUp
{
    [super setUp];
    // Set-up code here.
    namePacket = [[CZSMBNameServicePacket alloc] init];
    
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void)testNameEncode {
    NSData * encodedData = [namePacket l1Encode:@"BCD-PC-013"];
    XCTAssertTrue([encodedData length] == 32, @"The data length should equal to 32.");
    uint8_t bytes[32] = {'\0'};
    [encodedData getBytes:bytes
                   length:32];
    XCTAssertTrue(bytes[0] == 0x45, @"");
    XCTAssertTrue(bytes[1] == 0x43, @"");
    XCTAssertTrue(bytes[2] == 0x45, @"");
    XCTAssertTrue(bytes[3] == 0x44, @"");
    XCTAssertTrue(bytes[28] == 0x43, @"");
    XCTAssertTrue(bytes[29] == 0x41, @"");
    
    
}

- (void)testNameQuery {
    CZSMBNameServiceQuery * serviceQuery = [CZSMBNameServiceQuery sharedInstance];
    [serviceQuery doNameQuery:@"BCD-PC-013"
                     delegate:self];
    
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:3.0]];
}

- (void)queryHadResponse:(NSString*) ipAddr {
    NSLog(@"service response IP:%@",ipAddr);
}
@end
