//
//  CZLANManagerAuthenticationTests.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 2/16/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZLANManagerAuthenticationTests.h"
#import "CZLANManagerAuthentication.h"

@interface CZLANManagerAuthenticationTests() {
    CZLANManagerAuthentication *lanManager;
}

@end

@implementation CZLANManagerAuthenticationTests

- (void)setUp
{
    [super setUp];
    // Set-up code here.
    lanManager = [[CZLANManagerAuthentication alloc] init];
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void)testExample {
    [self lanManEncryptTesting];
    [self ntLMEncryptTesting];
}

- (void)lanManEncryptTesting
{
    BOOL result = NO;
    const uint8_t challenge[8] = {'y','M','d','\x9a','\xb4','R','\xb9','D'};
    uint8_t response[24];
    memset(response, 0, 24);
    result = [lanManager encryptPassword:@"12345678"
                             lmChallenge:challenge
                              toResponse:(uint8_t*) response];
    XCTAssertTrue(result, @"LM encrypt error");
    XCTAssertTrue(response[0]==0x96, @"");
    XCTAssertTrue(response[1]==0x05, @"");
    XCTAssertTrue(response[2]==0x51, @"");
    XCTAssertTrue(response[3]==0x29, @"");
    XCTAssertTrue(response[4]==0xb6, @"");
    XCTAssertTrue(response[5]==0x1b, @"");
    XCTAssertTrue(response[6]==0x0d, @"");
    XCTAssertTrue(response[7]==0xb5, @"");
    XCTAssertTrue(response[8]==0x93, @"");
    XCTAssertTrue(response[9]==0xd3, @"");
    XCTAssertTrue(response[10]==0x25, @"");
    XCTAssertTrue(response[11]==0x32, @"");
    XCTAssertTrue(response[12]==0x2f, @"");
    XCTAssertTrue(response[13]==0xa5, @"");
    XCTAssertTrue(response[14]==0x4d, @"");
    XCTAssertTrue(response[15]==0x78, @"");
    XCTAssertTrue(response[16]==0xa6, @"");
    XCTAssertTrue(response[17]==0x35, @"");
    XCTAssertTrue(response[18]==0x19, @"");
    XCTAssertTrue(response[19]==0x94, @"");
    XCTAssertTrue(response[20]==0xc4, @"");
    XCTAssertTrue(response[21]==0x04, @"");
    XCTAssertTrue(response[22]==0x1e, @"");
    XCTAssertTrue(response[23]==0x62, @"");
}

- (void)ntLMEncryptTesting
{
    BOOL result = NO;
    const uint8_t challenge[8] = {0x31, 0x67, 0x07, 0x24, 0x7E, 0xE0, 0x9C, 0x26};
    uint8_t response[24];
    memset(response, 0, 24);
    result = [lanManager encryptPassword:@"qwert,2001"
                           ntlmChallenge:challenge
                              toResponse:(uint8_t*) response];
    XCTAssertTrue(result, @"NTLM encrypt error");
    XCTAssertTrue(response[0]==0x7d, @"");
    XCTAssertTrue(response[1]==0x0f, @"");
    XCTAssertTrue(response[2]==0x70, @"");
    XCTAssertTrue(response[3]==0x67, @"");
    XCTAssertTrue(response[4]==0x43, @"");
    XCTAssertTrue(response[5]==0xa6, @"");
    XCTAssertTrue(response[6]==0xbb, @"");
    XCTAssertTrue(response[7]==0x4a, @"");
    XCTAssertTrue(response[8]==0xea, @"");
    XCTAssertTrue(response[9]==0x24, @"");
    XCTAssertTrue(response[10]==0x58, @"");
    XCTAssertTrue(response[11]==0x3b, @"");
    XCTAssertTrue(response[12]==0xed, @"");
    XCTAssertTrue(response[13]==0xd4, @"");
    XCTAssertTrue(response[14]==0xf9, @"");
    XCTAssertTrue(response[15]==0xbc, @"");
    XCTAssertTrue(response[16]==0xc3, @"");
    XCTAssertTrue(response[17]==0xe2, @"");
    XCTAssertTrue(response[18]==0xc3, @"");
    XCTAssertTrue(response[19]==0xd2, @"");
    XCTAssertTrue(response[20]==0x55, @"");
    XCTAssertTrue(response[21]==0xf3, @"");
    XCTAssertTrue(response[22]==0x3d, @"");
    XCTAssertTrue(response[23]==0xc2, @"");
}
@end
