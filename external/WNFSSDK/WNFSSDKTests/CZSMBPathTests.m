//
//  CZSMBPathTests.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 3/8/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZSMBPathTests.h"
#import "CZSMBPath.h"

@interface CZSMBPathTests() {
    
}

@end

@implementation CZSMBPathTests

- (void)setUp
{
    [super setUp];
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void)testSMBPath {
    [self pathTesting];
    [self fileTesting];
    [self otherTesting];
}

- (void)fileTesting {
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/go"];
    XCTAssertTrue([smbPath isValidFile], @"File path should valid.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//"];
    XCTAssertFalse([smbPath isValidFile], @"File path should error.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@""];
    XCTAssertFalse([smbPath isValidFile], @"File path should error.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"   "];
    XCTAssertFalse([smbPath isValidFile], @"File path should error.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"10.0.0.0/share/go.txt"];
    XCTAssertFalse([smbPath isValidFile], @"File path should error.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/go.txt"];
    XCTAssertTrue([smbPath isValidFile], @"File path should valid.");
    XCTAssertTrue([[smbPath fileName] isEqualToString:@"go.txt"], @"File name should be go.txt.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/中文/笔记本/go.txt"];
    XCTAssertTrue([smbPath isValidFile], @"File path should valid.");
    XCTAssertTrue([[smbPath fileName] isEqualToString:@"go.txt"], @"File name should be go.txt.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\中文\\笔记本\\"], @"Relative path should be \\中文\\笔记本\\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share\\fdl"];
    XCTAssertFalse([smbPath isValidFile], @"File path should error.");
}

- (void)pathTesting {
    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/go"];
    XCTAssertTrue([smbPath isValidPath], @"Path should valid.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\"], @"Path should be \\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/dd/eee/"];
    XCTAssertTrue([smbPath isValidPath], @"Path should valid.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\dd\\eee\\"], @"Path should be \\dd\\eee\\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/中文/笔记本/"];
    XCTAssertTrue([smbPath isValidPath], @"Path should valid.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\中文\\笔记本\\"], @"Path should be \\中文\\笔记本\\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/中文/笔记本/d的/"];
    XCTAssertTrue([smbPath isValidPath], @"Path should valid.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\中文\\笔记本\\d的\\"], @"Path should be \\中文\\笔记本\\d的\\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/go.txt"];
    XCTAssertTrue([smbPath isValidPath], @"Path should valid.");
    XCTAssertTrue([[smbPath relativePath] isEqualToString:@"\\"], @"Path should be \\.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/"];
    XCTAssertFalse([smbPath isValidPath], @"File path should error.");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/"];
    XCTAssertFalse([smbPath isValidPath], @"File path should error.");
}

- (void)otherTesting {
//    CZSMBPath * smbPath = [[CZSMBPath alloc] initWithSMBString:@"//BCD-PC-013/share/go.txt"];
//    STAssertNotNil(smbPath.relativePath, @"Relative path should not nil");
//    STAssertTrue([smbPath.host isEqualToString:@"BCD-PC-013"], @"The host name should be BCD-PC-013.");
    
    CZSMBPath *smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/go.txt"];
    XCTAssertNotNil(smbPath.relativePath, @"Relative path should not nil");
    XCTAssertTrue([smbPath.relativePath isEqualToString:@"\\"], @"Relative path should be \\");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/window/dos/go.txt"];
    XCTAssertNotNil(smbPath.relativePath, @"Relative path should not nil");
    XCTAssertTrue([smbPath.relativePath isEqualToString:@"\\window\\dos\\"], @"Relative path should valid");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/window/dos///go.txt"];
    XCTAssertNotNil(smbPath.relativePath, @"Relative path should not nil");
    XCTAssertTrue([smbPath.relativePath isEqualToString:@"\\window\\dos\\"], @"Relative path should valid");
    
    smbPath = [[CZSMBPath alloc] initWithSMBString:@"//10.0.0.0/share/window/dos///go.txt"];
    XCTAssertNotNil(smbPath.fileName, @"File name should not nil");
    XCTAssertTrue([smbPath.fileName isEqualToString:@"go.txt"], @"File name should valid");
}

@end
