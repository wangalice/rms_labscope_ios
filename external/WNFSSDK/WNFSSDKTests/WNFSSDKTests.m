//
//  WNFSSDKTests.m
//  WNFSSDKTests
//
//  Created by BCD-MAC-004 on 2/4/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "WNFSSDKTests.h"
#import "WNFSSDK.h"
#import "CZFileDescriptor.h"


@interface WNFSSDKTests()<CZWNFSManagerDelegate> {
    CZWNFSManager * manager;
    int             delegateCount;
}

@end
@implementation WNFSSDKTests

- (void)setUp
{
    [super setUp];
    // Set-up code here.
    manager = [WNFSSDK sharedInstance];
    XCTAssertNotNil(manager, @"Could not create CZWNFSManager");
    [manager setAccountInformation:@"cn"
                          userName:@"tim.pan"
                          password:@"Qwert,2014"];
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void)testWNFSManager
{

//    return;
//    return;
    delegateCount = 1;
//    [self window7Ping];
//    [self windows7List1000Items];
    [self windows7ListItem];
//    [self windowXpGetFile];
//    [self windows2000ListItem];
//    [self window7DeleteItem];
//    return;
    
//    [self window7DeleteItem];
    while (delegateCount > 0) {
        [self keepLoop:0.5];
    }
    return;
    delegateCount = 12;
    [self window7DeleteItem];
    [self windowXpGetFile];
    [self window7WriteFile];
    [self windows7ListItem];
    [self windowXpListItem];
    [self windowXpListItemUnicode];
    [self window2008ListItem];
    [self operationCancel];
    [self swithDirectory];
    [self swithHost];
    [self errorIP];
    while (delegateCount > 0) {
        [self keepLoop:0.5];
    }
    
    
}

- (void)operationCancel {
    [manager cancelAllOperation];
}

- (void)window7Ping {
    [manager isServerReachable:@"10.102.37.52"
                       timeout:5.0f
                      delegate:self];
}

- (void)windows7List1000Items {
    [manager listItemsFrom:@"//10.102.37.52/Camel_7/more than 1000/" delegate:self];
}

- (void)windows7ListItem {
    [manager listItemsFrom:@"//BCD-VM-003/IPC$" delegate:self];
}

- (void)windows2000ListItem {
    [manager listItemsFrom:@"//10.102.37.73/Camel_2000/" delegate:self];
}

- (void)windowXpGetFile {
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,
                                                                            NSUserDomainMask, YES ) objectAtIndex:0];
    
    NSString * localFile = [documentsDirectoryPath stringByAppendingString:@"/test.png"];
    
    [manager getFileFrom:@"//BCD-PC-013/Camel_xp/test.png"
                   local:localFile
             overwriting:YES
                delegate:self];
}

- (void)windowXpWriteFile {
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,
                                                                            NSUserDomainMask, YES ) objectAtIndex:0];
    
    NSString * localFile = [[documentsDirectoryPath stringByDeletingLastPathComponent] stringByAppendingString:@"/test.png"];
    [manager sendFileTo:@"//10.102.37.66/Camel_xp/test_file.png"
                  local:localFile
            overwriting:YES
               delegate:self];
}

- (void)window7WriteFile {
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory,
                                                                            NSUserDomainMask, YES ) objectAtIndex:0];
    
    NSString * localFile = [[documentsDirectoryPath stringByDeletingLastPathComponent] stringByAppendingString:@"/test.png"];
    [manager sendFileTo:@"//10.102.37.52/Camel_7/test_file.png"
                  local:localFile
            overwriting:YES
               delegate:self];
}

- (void)window8DeleteItem {
    [manager setAccountInformation:nil
                          userName:@"camel"
                          password:@"123456"];
    [manager deleteItemFrom:@"//10.102.37.64/Camel_8/df/"
                      force:YES
                   delegate:self];
}

- (void)window7DeleteItem {
    [manager deleteItemFrom:@"//10.102.37.52/Camel_7/test.png"
                      force:YES
                   delegate:self];
}

- (void)windowXpListItem {
    [manager listItemsFrom:@"//10.102.37.78/IPC$" delegate:self];
}

- (void)windowXpListItemUnicode {
    [manager listItemsFrom:@"//10.102.37.66/Camel_xp/中文字符/" delegate:self];
}

- (void)window2008ListItem {
    [manager listItemsFrom:@"//10.102.37.53/Camel_2008/" delegate:self];
}

- (void)swithDirectory
{   
    [manager listItemsFrom:@"//10.102.37.54/Camel_xp/test1" delegate:self];
    [self keepLoop:2.0f];
    [manager listItemsFrom:@"//10.102.37.54/Camel_xp/test1" delegate:self];
    
}

- (void)swithHost
{
    [manager listItemsFrom:@"//10.102.37.54/Camel_xp/" delegate:self];
    [self keepLoop:2.0f];
    [manager listItemsFrom:@"//10.102.37.52/Camel_7/file" delegate:self];
    
}

- (void)errorIP
{
    [manager listItemsFrom:@"//102.102.36.1670/Camel_xp/" delegate:self];
    
}

- (void)keepLoop:(float) time {
    @autoreleasepool {
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:time]];
    }
}

#pragma mark - Delegate

- (void)reachingDidSuccess:(NSString*)server {
    delegateCount -- ;
}

- (void)reachingDidFailWithError:(NSError*)error {
    delegateCount -- ;
}

- (void)operateDidChangeStatus:(NSDictionary *)status type:(CZOperateType)type {
    NSLog(@"%@",status);
}

- (void)operateDidFinishItem:(NSDictionary*) item type:(CZOperateType) type {
    if (type == CZOperateTypeDeleteItem) {
        
    }else {
        NSLog(@"success reciving items:%lu", (unsigned long)[[[item allValues] objectAtIndex:0] count]);
    }
    delegateCount -- ;
}

- (void)operateDidFailWithError:(NSError*) error {
    NSLog(@"list items error:%@",error);
    delegateCount -- ;
}
- (void)dataTransferDidFinishDestination:(NSString*) path {
    NSLog(@"%@",path);
    delegateCount -- ;
}
- (void)dataTransferDidFailWithError:(NSError*)error {
    NSLog(@"%@",error);
    delegateCount -- ;
}
- (void)dataTransferProgress:(CZTransferDirection)direction delivered:(uint64_t) bytes total:(uint64_t) totalBytes destination:( NSString*) path {
    NSLog(@"delivered:%llu,total:%llu",bytes,totalBytes);
}
@end
