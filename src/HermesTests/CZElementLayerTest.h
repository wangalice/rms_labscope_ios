//
//  CZElementLayerTest.h
//  HermesTests
//
//  Created by Ralph Jin on 1/25/13.
//  Copyright (c) 2013 Ralph Jin. All rights reserved.
//

#import <XCTest/XCTest.h>

@class CZElementLayer;
@class CZElementRectangle;

@interface CZElementLayerTest : XCTestCase

@property (nonatomic, retain) CZElementLayer *layer;
@property (nonatomic, retain) CZElementRectangle *rect1;
@property (nonatomic, retain) CZElementRectangle *rect2;

@end
