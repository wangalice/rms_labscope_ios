//
//  CZNodeRectangleTest.m
//  Hermes
//
//  Created by Ralph Jin on 1/25/13.
//  Copyright (c) 2013 Ralph Jin. All rights reserved.
//

#import "CZNodeRectangleTest.h"
#import "CZNodeRectangle.h"

@implementation CZNodeRectangleTest

- (void)setUp {
    [super setUp];
    
    _rect = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(10.0f, 20.0f, 60.0f, 40.0f)];
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    [_rect release];
    
    [super tearDown];
}

- (void)testInit {
    XCTAssertEqual(self.rect.x, 10.0f, @"expect rect.x = 10.");
    XCTAssertEqual(self.rect.y, 20.0f, @"expect rect.y = 20.");
    XCTAssertEqual(self.rect.width, 60.0f, @"expect rect.x = 10.");
    XCTAssertEqual(self.rect.height, 40.0f, @"expect rect.y = 20.");
}

- (void)testObserveSelf {
    CGPathRef path = self.rect.relativePath;
    path = CGPathRetain(path);
    XCTAssertTrue(path != NULL, @"path should not be nil");
    
    self.rect.x = 30;
    CGPathRef changedPath = self.rect.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.rect.y = 30;
    changedPath = self.rect.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.rect.width = 120;
    changedPath = self.rect.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.rect.height = 80;
    changedPath = self.rect.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
}

- (void)testHittest {
    CGFloat tolerance = 3.0f;
    CGPoint topLeft = {.x = 8, .y = 18};
    XCTAssertTrue([self.rect hitTest:topLeft tolerance:tolerance], @"top left point should hit on");
    
    CGPoint bottomRight = {.x = 72, .y = 62};
    XCTAssertTrue([self.rect hitTest:bottomRight tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint topRight = {.x = 72, .y = 18};
    XCTAssertTrue([self.rect hitTest:topRight tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint bottomLeft = {.x = 8, .y = 62};
    XCTAssertTrue([self.rect hitTest:bottomLeft tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint outsidePoint = {.x = 5, .y = 15};
    XCTAssertFalse([self.rect hitTest:outsidePoint tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsidePoint2 = {.x = 115, .y = 115};
    XCTAssertFalse([self.rect hitTest:outsidePoint2 tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint centerPoint = {.x = 40, .y = 40};
    XCTAssertFalse([self.rect hitTest:centerPoint tolerance:tolerance], @"center point shouldn't hit on middle point");
    
    self.rect.fillColor = kCZColorRed;
    XCTAssertTrue([self.rect hitTest:centerPoint tolerance:tolerance], @"center point should hit on middle point");
}

- (void)testCreateLayer {
    CALayer *layer = [self.rect newLayer];
    XCTAssertNotNil(layer, @"layer shall not be nil.");
    
    CGRect expectFrame = CGRectMake(10, 20, 60, 40);
    CGRect expectBounds = CGRectMake(0, 0, 60, 40);
    XCTAssertTrue(CGRectEqualToRect(expectBounds, layer.bounds), @"bounds shall be (0, 0, 60, 40)");
    XCTAssertTrue(CGRectEqualToRect(expectFrame, layer.frame), @"frame shall be (10, 20, 60, 40)");
    
    [layer release];
}

- (void)testHitTestRotated {
    CGFloat tolerance = 3.0f;
    
    CZNodeRectangle *rotatedRect = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(100.0f, 100.0f, 100.0f, 100.0f)];
    rotatedRect.rotateAngle = M_PI_2;
    
    CGPoint topLeft = {.x = 100, .y = 100};
    XCTAssertTrue([rotatedRect hitTest:topLeft tolerance:tolerance], @"top left point should hit on");
    
    CGPoint bottomRight = {.x = 200, .y = 200};
    XCTAssertTrue([rotatedRect hitTest:bottomRight tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint topRight = {.x = 200, .y = 100};
    XCTAssertTrue([rotatedRect hitTest:topRight tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint bottomLeft = {.x = 100, .y = 200};
    XCTAssertTrue([rotatedRect hitTest:bottomLeft tolerance:tolerance], @"bottom right point should hit on");
    
    CGPoint outsidePoint = {.x = 95, .y = 95};
    XCTAssertFalse([rotatedRect hitTest:outsidePoint tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsidePoint2 = {.x = 215, .y = 215};
    XCTAssertFalse([rotatedRect hitTest:outsidePoint2 tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint centerPoint = {.x = 150, .y = 150};
    XCTAssertFalse([rotatedRect hitTest:centerPoint tolerance:tolerance], @"center point shouldn't hit on middle point");
    
    rotatedRect.fillColor = kCZColorRed;
    XCTAssertTrue([rotatedRect hitTest:centerPoint tolerance:tolerance], @"center point should hit on middle point");
    
    [rotatedRect release];
}

- (void)testRatatedTransform {
    CZNodeRectangle *rotatedRect = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(-50.0f, -50.0f, 100.0f, 100.0f)];
    rotatedRect.rotateAngle = M_PI_2;
    
    CGPoint p = {.x = 0, .y = 0}; // point in |rotatedRect|'s own coordinate, top left
    CGPoint respect = {.x = 50, .y = -50};  // point in parent or global coodinate, top right
    
    CGAffineTransform t = [rotatedRect transformSelf];
    p = CGPointApplyAffineTransform(p, t);
    
    XCTAssertEqual(p.x, respect.x, @"");
    XCTAssertEqual(p.y, respect.y, @"");
    
    
    [rotatedRect release];
}

- (void)testFrame {
    CGRect frame = [_rect frame];
    XCTAssertTrue(CGRectEqualToRect(frame, CGRectMake(10, 20, 60, 40)), @"");
    
    _rect.rotateAngle = M_PI_2;
    frame = [_rect frame];
    XCTAssertEqualWithAccuracy(frame.size.width, 40.0f, 1e-5, @"");
    XCTAssertEqualWithAccuracy(frame.size.height, 60.0f, 1e-5, @"");
}

- (void)testCascadeBoundary {
    CZNodeRectangle *subRect = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(60, 40, 40, 60)];
    [_rect addSubNode:subRect];
    
    CGRect frame = [subRect frame];
    XCTAssertTrue(CGRectEqualToRect(frame, CGRectMake(60, 40, 40, 60)), @"");
    
    frame = [_rect frame];
    XCTAssertTrue(CGRectEqualToRect(frame, CGRectMake(10, 20, 100, 100)), @"");
    
    subRect.rotateAngle = M_PI_2;
    frame = [_rect frame];
    XCTAssertTrue(CGRectEqualToRect(frame, CGRectMake(10, 20, 110, 90)), @"");
    
    [subRect release];
}


@end
