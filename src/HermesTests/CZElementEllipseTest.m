//
//  CZElementEllipseTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

// TODO: delete in sprint 4

#import "CZElementEllipseTest.h"

@implementation CZElementEllipseTest

- (void)setUp {
    [super setUp];
    
    _ellipse = [[CZElementEllipse alloc] initWithCenter:CGPointMake(100, 100) radius:CGSizeMake(5, 5)];
}

- (void)tearDown {
    // Tear-down code here.
    [_ellipse release];
    
    [super tearDown];
}

- (void)testNewLayer {
    CALayer* layer = [_ellipse newLayer];
    STAssertNotNil(layer, @"Layer shall not be nil.");
    [layer release];
}

- (void)testShapeMemo {
    NSDictionary *memo = [_ellipse newShapeMemo];
    STAssertNotNil([memo objectForKey:@"CGRect"], @"The memo shall contain a CGRect value");
    STAssertNotNil([memo objectForKey:@"rotateAngle"], @"The memo shall contain a rotate angle value");
    [memo release];
}

- (void)testRestoreFromMemo {
    CGFloat x = 1, y = 2, width = 6, height = 4;
    CGRect ellipse = CGRectMake(x, y, width, height);
    NSValue *value = [NSValue valueWithCGRect:ellipse];
    NSNumber *rotateAngle = [NSNumber numberWithFloat:0.5f];
    NSDictionary *memo = [NSDictionary dictionaryWithObjectsAndKeys:value, @"CGRect", rotateAngle, @"rotateAngle", nil];
    [_ellipse restoreFromShapeMemo:memo];
    
    STAssertTrue(CGPointEqualToPoint(ellipse.origin, _ellipse.center), @"");
    STAssertTrue(CGSizeEqualToSize(ellipse.size, _ellipse.radiuses), @"");
    STAssertEqualsWithAccuracy(0.5f, _ellipse.rotateAngle, 1e-5, @"Rotate angle shall be remembered.");
}

- (void)testFeature {
    NSUInteger count = [_ellipse featureCount];
    STAssertTrue(count != 0U, @"Feature count shall not be 0 for a ellipse.");
    
    for (NSUInteger i = 0; i < count; ++i) {
        STAssertNotNil([_ellipse featureAtIndex:i], @"The feature shall not be nil.");
    }
}

@end
