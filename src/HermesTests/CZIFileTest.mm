//
//  CZIFileTest.m
//  Hermes
//
//  Created by Mike Wang on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZIFileTest.h"
#import <UIKit/UIKit.h>
#import "CZIFile.h"
#import "CZAppDelegate.h"
#import <Common/CZCommonUtils.h>
#import "NSData+CZIMetaData.h"
#import "NSData+CZImageAnalysisData.h"
#import "UIImage+CZIImage.h"
#import "pugixml.hpp"
#import "CZAnalyzer2dPersister.h"
#include <string>

using namespace std;
using namespace pugi;

@implementation CZIFileTest

- (void)tearDown {
    
    [_filePath release];
    [super tearDown];
}

//TODO
/*
This is similar with a method in my image processing unit test. Maybe it'd be a good idea to create a helper class to reuse codes. We can do this when we have time later.
 */

- (CZIImageRef*)getRGBAsFromImage:(UIImage*)image
{
    CZIImageRef* result = new CZIImageRef();
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    result->_rawData = rawData;
    result->_compression = kCZIImageCompressionUncompressed;
    result->_height = height;
    result->_width = width;
    result->_size = height * width * 4;
    result->_pixType = kBGR32;
    
    return result;
}


- (void)testFileReadImage
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"graphite_50x_01.czi"
                                  ofType:nil
                             inDirectory:@"Assets"];
    XCTAssertNotNil(self.filePath, @"graphite_50x_01.czi is missing!");
    
    NSString* path = [CZAppDelegate get].documentPath;
    CZLogv(@"path = %@", path);
    CZIFile file ([_filePath UTF8String]);
    int err = file.open(CZIFile::kOpen);
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    CZIImageRef srcImage;
    err = file.readImage(&srcImage);
    XCTAssertFalse(err != 0, @" file failed to read image");

    CGBitmapInfo bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
    CFDataRef dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, srcImage._rawData, srcImage._size, kCFAllocatorNull);

    CGDataProviderRef provider = CGDataProviderCreateWithCFData(dataRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    CGImageRef cgImage = CGImageCreate(srcImage._width,
                                       srcImage._height,
                                       8,
                                       24,
                                       srcImage._width*3,
                                       colorSpace,
                                       bitmapInfo,
                                       provider,
                                       NULL,
                                       NO,
                                       kCGRenderingIntentDefault);

    CGColorSpaceRelease(colorSpace);
    UIImage *outImage = [UIImage imageWithCGImage:cgImage];
    NSData* png_data = UIImagePNGRepresentation(outImage);
    [png_data writeToFile:[path stringByAppendingString:@"test.png"] atomically:YES];
    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    CFRelease(dataRef);
}

- (void)testFileReadMetaData
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"line_withMetaData.czi"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    NSData* metaData = [[NSData alloc] initMetaDataWithContentsOfCZIFile:self.filePath];
    XCTAssertFalse(metaData.length == 0 || metaData.bytes == nil, @" file failed to create meta data");
    
    NSString *metaDataString = [[NSString alloc] initWithData:metaData encoding:NSASCIIStringEncoding];
    [metaData release];
    CZLogv(@"%@", metaDataString);
    [metaDataString release];
}

- (void)testFileReadThumbData
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"line.czi"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    UIImage* thumb = [[UIImage alloc] initThumbWithContentsOfCZIFile:self.filePath];
    XCTAssertFalse(thumb == nil, @" file failed to create thumb data");
    
    NSData *thumbData = UIImageJPEGRepresentation(thumb, 1.0);
    NSString *thumbPath = [[self.filePath stringByDeletingPathExtension] stringByAppendingPathExtension:@".jpg"];
    [thumbData writeToFile:thumbPath atomically:YES];
    
    [thumb release];
}

-(void)testFileWriteImage
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"sample.png"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    UIImage* sampleImage = [UIImage imageWithContentsOfFile:self.filePath];
    CZIImageRef* outputImage = [self getRGBAsFromImage: sampleImage];
    XCTAssertFalse(outputImage == nil, @" file failed to create image");
    
    self.filePath = [[_filePath stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"sample.czi"];
    CZIFile file ([_filePath UTF8String]);
    int err = file.open(CZIFile::kCreate);
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    err = file.writeImage(outputImage);
    XCTAssertFalse(err != 0, @" file failed to open image");
 
    err = file.save();
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    delete outputImage;
}

struct xml_string_writer: pugi::xml_writer {
    std::string result;
    
    virtual void write(const void* data, size_t size) {
        result += std::string(static_cast<const char*>(data), size);
    }
};

string node_to_string(xml_node node) {
    xml_string_writer writer;
    node.print(writer);
    
    return writer.result;
}

-(void)testFileWriteImageWithMetaData
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"line.png"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    NSString* metaDataPath = [bundle pathForResource:@"line.xml"
                                              ofType:nil
                                         inDirectory:@"Assets"];
    
    UIImage* sampleImage = [UIImage imageWithContentsOfFile:self.filePath];
    CZIImageRef* outputImage = [self getRGBAsFromImage: sampleImage];
    XCTAssertFalse(outputImage == nil, @" file failed to create image");
    
    self.filePath = [[_filePath stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"line_metadata.czi"];
    CZIFile file ([_filePath UTF8String]);
    int err = file.open(CZIFile::kCreate);
    XCTAssertFalse(err != 0, @" file failed to create file");
    
    err = file.writeImage(outputImage);
    XCTAssertFalse(err != 0, @" file failed to write image");
    
    //Write metadata
    xml_document doc;
    
    xml_parse_result result = doc.load_file([metaDataPath UTF8String]);
    XCTAssertFalse(result.status != pugi::status_ok, @"failed to load metadata");
    
    string metaData = node_to_string(doc.document_element());
    cout << metaData;
    
    err = file.writeMetaData(metaData.c_str(), metaData.length()-1);
    XCTAssertFalse(err != 0, @" file failed to write meta data");
    
    err = file.save();
    XCTAssertFalse(err != 0, @" file failed to save file");
    
    delete outputImage;
}

-(void)testFileWriteImageWithThumbnail
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"line.png"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    UIImage* sampleImage = [UIImage imageWithContentsOfFile:self.filePath];
    CZIImageRef* outputImage = [self getRGBAsFromImage: sampleImage];
    XCTAssertFalse(outputImage == nil, @" file failed to create image");
    
    self.filePath = [[_filePath stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"line_thumbnail.czi"];
    CZIFile file ([_filePath UTF8String]);
    int err = file.open(CZIFile::kCreate);
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    err = file.writeImage(outputImage);
    XCTAssertFalse(err != 0, @" file failed to write image");
    
    CGSize thumbSize;
    thumbSize.width = 160;
    thumbSize.height = 120;
    UIImage *thumbImage = [CZCommonUtils scaleImage:sampleImage intoSize:thumbSize];
    NSData* thumbData = UIImageJPEGRepresentation(thumbImage, 1.0);
    err = file.writeThumb((const unsigned char*)thumbData.bytes, (uint64_t)thumbData.length);
    XCTAssertFalse(err != 0, @" file failed to write thumbnail");
    
    err = file.save();
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    delete outputImage; 
}

-(void)testFileWriteImageWithThumbnailAndImageAnalysis
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"Tutor6 with demo contours.czi"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    BOOL isGray;
    UIImage* sampleImage = [[UIImage alloc] initImageWithContentsOfCZIFile:self.filePath
                                                                      gray:&isGray];
    CZIImageRef* outputImage = [self getRGBAsFromImage: sampleImage];
    XCTAssertFalse(outputImage == nil, @" file failed to create image");
    
    NSData* metaData = [[NSData alloc] initMetaDataWithContentsOfCZIFile:self.filePath];
    XCTAssertFalse(metaData.length == 0 || metaData.bytes == nil, @" file failed to create meta data");
    NSString *metaDataString = [[NSString alloc] initWithData:metaData encoding:NSASCIIStringEncoding];
    [metaData release];
    
    UIImage* thumb = [[UIImage alloc] initThumbWithContentsOfCZIFile:self.filePath];
    XCTAssertFalse(thumb == nil, @" file failed to create thumb data");
    
    self.filePath = [[_filePath stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"Tutor6 with demo contours_imageAnalysis.czi"];
    CZIFile file ([_filePath UTF8String]);
    int err = file.open(CZIFile::kCreate);
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    err = file.writeImage(outputImage);
    XCTAssertFalse(err != 0, @" file failed to write image");
    
    NSData* thumbData = UIImageJPEGRepresentation(thumb, 1.0);
    err = file.writeThumb((const unsigned char*)thumbData.bytes, (uint64_t)thumbData.length);
    XCTAssertFalse(err != 0, @" file failed to write thumbnail");
    
    err = file.writeMetaData([metaDataString cStringUsingEncoding:NSASCIIStringEncoding], [metaDataString length]);
    XCTAssertFalse(err != 0, @" file failed to write meta data");
    
    CZAnalyzer2dPersister *pers = [[CZAnalyzer2dPersister alloc] init];
    NSData *imageAnalysisData = [pers setAnalyzer2dResultTest];
    err = file.writeImageAnalysisData((const unsigned char*)imageAnalysisData.bytes, (uint64_t)imageAnalysisData.length);
    XCTAssertFalse(err != 0, @" file failed to write image analysis");
    
    [pers release];
    
    err = file.save();
    XCTAssertFalse(err != 0, @" file failed to open image");
    
    [sampleImage release];
    [metaDataString release];
    [thumb release];
    delete outputImage;
}

- (void)testFileReadImageAnalysisData
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZIFileTest class]];
    self.filePath = [bundle pathForResource:@"Tutor6 with demo contours.czi"
                                     ofType:nil
                                inDirectory:@"Assets"];
    
    NSData* imageAnalysisData = [[NSData alloc] initImageAnalysisDataWithContentsOfCZIFile:self.filePath];
    XCTAssertFalse(imageAnalysisData == nil, @" file failed to create thumb data");
    
    [imageAnalysisData release];
}

@end
