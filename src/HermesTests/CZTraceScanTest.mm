//
//  CZTraceScanTest.mm
//  Matscope
//
//  Created by Ralph Jin on 2/25/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "CZTraceScanner.h"
#import "CZRegion2D.h"
#import "CZScanLine.h"

@interface CZTraceScanTest : XCTestCase {
    CZRegion2D *rgn;
}

@end

@implementation CZTraceScanTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    rgn = [[CZRegion2D alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [rgn release];
    
    [super tearDown];
}

- (void)testScanSquare
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:20 height:20] autorelease];
    
    for (int y = 5; y < 15; y ++) {
        [image addChordXStart:5 xEnd:15 y:y];
    }
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    XCTAssertTrue([rgn area] == 100, @"");
}

- (void)testCrackCount {
    [rgn addDirection:0];
    XCTAssertEqual([rgn crackCount], 1UL);
    XCTAssertEqual([rgn crackCountPacked], 1UL);
    
    [rgn addDirection:1];
    XCTAssertEqual([rgn crackCount], 2UL);
    XCTAssertEqual([rgn crackCountPacked], 1UL);
    
    for (int i = 0; i < 8; i++) {
        [rgn addDirection:2];
    }
    
    XCTAssertEqual([rgn crackCount], 10UL);
    XCTAssertEqual([rgn crackCountPacked], 2UL);
}

- (void)testScanSquareAtBottomRight
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:20 height:20] autorelease];
    
    for (int y = 10; y < 20; y ++) {
        [image addChordXStart:10 xEnd:20 y:y];
    }
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    XCTAssertTrue([rgn area] == 100, @"");
}

- (void)testScanTwoSquaresSlope
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:40 height:40] autorelease];
    
    for (int y = 5; y < 15; y ++) {
        [image addChordXStart:5 xEnd:15 y:y];
    }
    
    for (int y = 15; y < 25; y ++) {
        [image addChordXStart:15 xEnd:25 y:y];
    }
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
}

- (void)testScanTwoSquaresSlope2
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:2048 height:1536] autorelease];
    
    [image addChordXStart:1126 xEnd:1128 y:209];[image addChordXStart:1131 xEnd:1132 y:209];
    [image addChordXStart:1125 xEnd:1134 y:210];
    [image addChordXStart:1125 xEnd:1134 y:211];
    [image addChordXStart:1124 xEnd:1134 y:212];
    [image addChordXStart:1124 xEnd:1125 y:213];[image addChordXStart:1127 xEnd:1134 y:213];
    [image addChordXStart:1124 xEnd:1125 y:214];[image addChordXStart:1130 xEnd:1134 y:214];
    [image addChordXStart:1125 xEnd:1127 y:215];[image addChordXStart:1132 xEnd:1134 y:215];
    [image addChordXStart:1126 xEnd:1134 y:216];
    [image addChordXStart:1131 xEnd:1133 y:217];
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
}

- (void)testScanHole
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:20 height:20] autorelease];
    
    for (int y = 2; y < 4; y ++) {
        [image addChordXStart:2 xEnd:18 y:y];
    }
    
    for (int y = 4; y < 16; y ++) {
        [image addChordXStart:2 xEnd:4 y:y];
        [image addChordXStart:16 xEnd:18 y:y];
    }
    
    for (int y = 16; y < 18; y ++) {
        [image addChordXStart:2 xEnd:18 y:y];
    }
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    
    XCTAssertTrue([rgn subRegionCount] == 1, @"");
    
    XCTAssertTrue([rgn area] == (16 * 16 - 12 * 12), @"");
}

- (void)testScanHShape
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:15 height:15] autorelease];
    
    for (int y = 0; y < 5; y ++) {
        [image addChordXStart:0 xEnd:5 y:y];
        [image addChordXStart:10 xEnd:15 y:y];
    }
    
    for (int y = 5; y < 10; y ++) {
        [image addChordXStart:0 xEnd:15 y:y];
    }
    
    for (int y = 10; y < 15; y ++) {
        [image addChordXStart:0 xEnd:5 y:y];
        [image addChordXStart:10 xEnd:15 y:y];
    }
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    XCTAssertTrue([rgn subRegionCount] == 0, @"");
    XCTAssertTrue([rgn area] == 175, @"");
}

- (void)testScanSmallHole
{
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:3 height:3] autorelease];
    [image addChordXStart:1 xEnd:2 y:0];
    [image addChordXStart:0 xEnd:1 y:1];
    [image addChordXStart:2 xEnd:3 y:1];
    [image addChordXStart:1 xEnd:2 y:2];
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    
    XCTAssertTrue([rgn subRegionCount] == 1, @"");
    XCTAssertTrue([rgn area] == 4, @"");
}

- (void)testScanMassHoles
{
    const int yCount = 100;
    const int xCount = 100;
    
    const int width = 20 * xCount + 10;
    const int height = 20 * yCount + 10;

    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:width height:height] autorelease];
    
    for (int y = 0; y < 10; y++) {
        [image addChordXStart:0 xEnd:width y:y];
    }
    
    for (int j = 0, yOffset = 10; j < yCount; j++, yOffset += 20) {
        for (int y = yOffset; y < yOffset + 10; y ++) {
            for (int i = 0, x = 0; i <= xCount; i++, x += 20) {
                [image addChordXStart:x xEnd:x + 10 y:y];
            }
        }
        
        for (int y = yOffset + 10; y < yOffset + 20; y++) {
            [image addChordXStart:0 xEnd:width y:y];
        }
    }

    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    XCTAssertTrue([rgn subRegionCount] == yCount * xCount, @"");
}

- (void)testComplexRegion {   // test region with hole, and hole is 8 directions connected.
    CZScanLine* image = [[[CZScanLine alloc] initWithWidth:10 height:10] autorelease];
    
    [image addChordXStart:0 xEnd:7 y:0];
    [image addChordXStart:0 xEnd:2 y:1];[image addChordXStart:4 xEnd:5 y:1];[image addChordXStart:6 xEnd:7 y:1];
    [image addChordXStart:0 xEnd:1 y:2];[image addChordXStart:2 xEnd:3 y:2];[image addChordXStart:5 xEnd:7 y:2];
    [image addChordXStart:0 xEnd:1 y:3];[image addChordXStart:5 xEnd:7 y:3];
    [image addChordXStart:0 xEnd:7 y:4];
    
    CZTraceScanner scanner;
    bool ok = scanner.ScanRegions(image, rgn);
    
    XCTAssertTrue(ok, @"");
    XCTAssertTrue([rgn subRegionCount] == 2, @"");
    XCTAssertTrue([rgn area] == 25, @"");
}

#pragma mark - CZScanLine
- (void)assertChords:(CZScanLine *)chords from:(int)from end:(int)end {
    XCTAssertFalse([chords getPointX:from - 1 y:0], @"");
    XCTAssertFalse([chords getPointX:end y:0], @"");
    XCTAssertFalse([chords getPointX:end + 1 y:0], @"");
    XCTAssertTrue([chords getPointX:from y:0], @"");
    XCTAssertTrue([chords getPointX:(from + end) / 2 y:0], @"");
    XCTAssertTrue([chords getPointX:end - 1 y:0], @"");
}

- (void)testAddPoint {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:10 height:1];
    [chords addChordXStart:0 xEnd:1 y:0];
    
    XCTAssertTrue([chords getPointX:0 y:0], @"");
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    
    [chords addChordXStart:2 xEnd:3 y:0];
    
    XCTAssertTrue([chords getPointX:2 y:0], @"");
//    STAssertEquals(chords->GetChordCount(0), 2UL, @"");
    
    [chords addChordXStart:1 xEnd:2 y:0];
    XCTAssertTrue([chords getPointX:1 y:0], @"");
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    [self assertChords:chords from:0 end:3];
    
    [chords release];
}

- (void)testAddChord {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:10 height:1];
    [chords addChordXStart:0 xEnd:10 y:0];
    
    [self assertChords:chords from:0 end:10];
    
    [chords release];
}

- (void)testChordMergeTwoSegment {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:30 xEnd:40 y:0];
    [chords addChordXStart:60 xEnd:80 y:0];
    [chords addChordXStart:90 xEnd:100 y:0];
    [chords addChordXStart:0 xEnd:10 y:0];
    
    [self assertChords:chords from:0 end:10];
    [self assertChords:chords from:30 end:40];
    [self assertChords:chords from:60 end:80];
    [self assertChords:chords from:90 end:100];
//    STAssertEquals(chords->GetChordCount(0), 4UL, @"");
    
    [chords addChordXStart:10 xEnd:11 y:0];
    [self assertChords:chords from:0 end:11];
//    STAssertEquals(chords->GetChordCount(0), 4UL, @"");
    
    [chords addChordXStart:35 xEnd:85 y:0];
    [self assertChords:chords from:0 end:11];
    [self assertChords:chords from:30 end:85];
    [self assertChords:chords from:90 end:100];
//    STAssertEquals(chords->GetChordCount(0), 3UL, @"");
    
    [chords release];
}

- (void)testChordMergeLeft {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];

    [chords addChordXStart:5 xEnd:10 y:0];

    [self assertChords:chords from:5 end:20];
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    
    [chords release];
}

- (void)testChordMergeFarLeft {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];
    
    [chords addChordXStart:0 xEnd:5 y:0];
    
    [self assertChords:chords from:0 end:5];
    [self assertChords:chords from:10 end:20];
//    STAssertEquals(chords->GetChordCount(0), 2UL, @"");
    
    [chords release];
}

- (void)donttestChordMergeInset {  // not support yet
   CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];
    [chords addChordXStart:11 xEnd:19 y:0];
    
    [self assertChords:chords from:10 end:20];
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    
    [chords release];
}

- (void)donttestChordMergeCover {  // not support yet
   CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];
    [chords addChordXStart:5 xEnd:25 y:0];

    [self assertChords:chords from:5 end:25];
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    
    [chords release];
}

- (void)testChordMergeRight {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];
    [chords addChordXStart:20 xEnd:25 y:0];

    [self assertChords:chords from:10 end:25];
//    STAssertEquals(chords->GetChordCount(0), 1UL, @"");
    
    [chords release];
}

- (void)testChordMergeFarRight {
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:100 height:1];
    
    [chords addChordXStart:10 xEnd:20 y:0];
    [chords addChordXStart:30 xEnd:40 y:0];
    
    [self assertChords:chords from:10 end:20];
    [self assertChords:chords from:30 end:40];
//    STAssertEquals(chords->GetChordCount(0), 2UL, @"");
    
    [chords release];
}

@end
