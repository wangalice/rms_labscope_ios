//
//  CZFileManagerTest.m
//  Hermes
//
//  Created by Mike Wang on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileManagerTest.h"
#import "CZFileHandler.h"
#import "CZAppDelegate.h"

#import "CZAnalyzer2dPersister.h"

@interface CZFileManagerTest()
{
    CZFileHandler _fileManager;
}

@end
@implementation CZFileManagerTest

- (void)setUp {
    [super setUp];
    
    NSBundle *bundle = [NSBundle bundleForClass:[CZFileManagerTest class]];
    self.filePath = [bundle pathForResource:@"line.czi"
                                      ofType:nil
                                 inDirectory:@"Assets"];
}

- (void)tearDown {
   
    [_filePath release];
    [super tearDown];
}

- (void)testFileOpenClose
{
    int err = _fileManager.openFile([_filePath UTF8String], kRDONLY);
    XCTAssertFalse(err < 0, @"Open file failed");
    err = _fileManager.closeFile();
    XCTAssertFalse(err < 0, @"Open file failed");
}

- (void)testFileRead
{
    unsigned char data[10];
    int dataLength = 10;
    
    int err = _fileManager.openFile([_filePath UTF8String], kRDONLY);
    XCTAssertFalse(err < 0, @"Open file failed");
     
    int readLength = _fileManager.readFile(data, dataLength);
    XCTAssertEqual(readLength, dataLength, @"file read success");
    
    err = _fileManager.closeFile();
    XCTAssertFalse(err < 0, @"Open file failed");
}

- (void)testFileSeek
{   
    int pos = 0;
    int offset = 10;
    int testPos = 0;
    
    int err = _fileManager.openFile([_filePath UTF8String], kRDONLY);
    XCTAssertFalse(err < 0, @"Open file failed");

    pos = _fileManager.seekFile(offset, kCurrent);
    XCTAssertEqual(pos, offset, @"file seek current success");
    
    testPos = 30;
    pos = _fileManager.seekFile(testPos, kSet);
    XCTAssertEqual(pos, testPos, @"file seek set success");
    
    offset = 10;
    pos = _fileManager.seekFile(offset, kCurrent);
    XCTAssertEqual(pos, testPos+10, @"file seek current success");
    
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSDictionary *dic = [defaultManager attributesOfItemAtPath:_filePath error:nil];
    
    pos = _fileManager.seekFile(0, kEnd);
    NSNumber *fileSize = [dic valueForKey:NSFileSize];
    XCTAssertEqual(pos, [fileSize intValue], @"file seek end success");
    
    err = _fileManager.closeFile();
    XCTAssertFalse(err < 0, @"Open file failed");
}

- (void)testFileWrite
{
    self.filePath = [[_filePath stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"test.data"];
    
    int err = _fileManager.openFile([_filePath UTF8String], kWRONLY);
    XCTAssertFalse(err < 0, @"Open file failed");
    
    unsigned char data[10] = "test.data";
    
    _fileManager.writeFile(data, 10);
    err = _fileManager.closeFile();
    XCTAssertFalse(err < 0, @"Open file failed");
}


@end
