//
//  CZCameraBrowserTest.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCameraBrowserTest.h"
#import "CZCameraBrowser.h"

@implementation CZCameraBrowserTest

#pragma mark -
#pragma mark Test Cases

/**
 * Test whether the class is correctly implemented as a singleton.
 */
- (void)testIsSingleton {
    CZCameraBrowser *sharedInstance = [CZCameraBrowser sharedInstance];
    
    CZCameraBrowser *wrongUsage1 = [[CZCameraBrowser alloc] init];
    CZCameraBrowser *wrongUsage2 = [[CZCameraBrowser alloc] init];
    
    XCTAssertTrue(wrongUsage1 == wrongUsage2, @"The returned instance from 'alloc' should be always the same.");
    XCTAssertTrue(wrongUsage1 == sharedInstance, @"The returned instance from 'alloc' should be the shared instance.");
    
    CZCameraBrowser *wrongUsage3 = [sharedInstance copy];
    
    XCTAssertTrue(wrongUsage3 == sharedInstance, @"The returned instance from 'copy' should be the shared instance.");
    
    NSUInteger originalRetainCount = [[CZCameraBrowser sharedInstance] retainCount];
    
    [[CZCameraBrowser sharedInstance] release];
    NSUInteger newRetainCount1 = [[CZCameraBrowser sharedInstance] retainCount];
    
    XCTAssertEqual(originalRetainCount, newRetainCount1, @"'retainCount' should not be changed after 'release'.");
    
    [[CZCameraBrowser sharedInstance] retain];
    NSUInteger newRetainCount2 = [[CZCameraBrowser sharedInstance] retainCount];
    
    XCTAssertEqual(originalRetainCount, newRetainCount2, @"'retainCount' should not be changed after 'retain'.");
}

@end
