//
//  CZSelectToolTest.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSelectToolTest.h"
#import <UIKit/UIKit.h>
#import <OCMock/OCMock.h>
#import "CZUnitTestUtility.h"
#import "CZDocManager.h"
#import "CZElementLayer.h"
#import "CZElementRectangle.h"
#import "CZSelectTool.h"
#import "CZAnnotationHandler.h"

@implementation CZSelectToolTest

- (void)setUp {
    [super setUp];
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    document = [[CZDocManager alloc] initWithImage:[CZUnitTestUtility testImage]];
    selectTool = [[CZSelectTool alloc] initWithDocManager:document view:view];
}

- (void)tearDown {
    [selectTool release];
    [document release];
    [view release];
    
    [super tearDown];
}

- (void)testCreateRectangle {
    CGRect rect = CGRectMake(400, 400, 100, 100);
    [selectTool createElementByClass:[CZElementRectangle class] inRect:rect];
    
    XCTAssertEqual([document.elementLayer elementCount], 1U, @"One rectangle shall be created.");
    XCTAssertNotNil([selectTool selectedElement], @"The new created rectangle shall be seleted.");
    
    // test deselect
    [selectTool tap:CGPointMake(0, 0)];
    XCTAssertNil([selectTool selectedElement], @"The new created rectangle shall be seleted.");
}

- (void)testDelegate {
    CZElement *elem1 = [[CZElementRectangle alloc] initWithRect:CGRectMake(0, 0, 1, 1)];
    CZElement *elem2 = [[CZElementRectangle alloc] initWithRect:CGRectMake(2, 2, 1, 1)];
    
    [document.elementLayer addElement:elem1];
    [document.elementLayer addElement:elem2];
    
    id mock = [OCMockObject mockForProtocol:@protocol(CZSelectToolDelegate)];
    [[mock expect] selectTool:selectTool didSelectElement:elem1];
    [[mock expect] selectTool:selectTool willDeselectElement:elem1];
    [[mock expect] selectToolDidDeselectElement:selectTool];
    [[mock expect] selectTool:selectTool didSelectElement:elem2];

    selectTool.delegate = mock;
    [selectTool selectElement:elem1];
    [selectTool deselect];
    [selectTool selectElement:elem2];
    
    [mock verify];
    
    // Verify that there's no call for deselect when select another element, to avoid flicking UI
    id mock2 = [OCMockObject niceMockForProtocol:@protocol(CZSelectToolDelegate)];
    [[mock2 reject] selectTool:selectTool willDeselectElement:elem2];
    
    selectTool.delegate = mock2;
    [selectTool selectElement:elem1];
    [mock2 verify];
    
    [elem1 release];
    [elem2 release];
}

@end
