//
//  CZUndoActionActionStub.m
//  HermesTests
//
//  Created by Ralph Jin on 1/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZUndoableActionStub.h"
#import "CZDeallocateChecking.h"

@implementation CZUndoableActionStub


- (id)init {
    self = [super init];
    if (self) {
        _checker = nil;
        _memoryCosted = 1U;
    }
    return self;
}

- (id)initWithChecker:(id<CZDeallocateChecking>) anChecker {
    self = [super init];
    if (self) {
        _checker = [anChecker retain];
        _memoryCosted = 1U;
    }
    return self;
}

- (void)dealloc {
    [_checker didDeallocated];
    [_checker release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return _memoryCosted;
}

- (void)do {
}

- (void)redo {
}

- (void)undo {
}

- (CZUndoActionType)type {
    return CZUndoActionTypeUnknown;
}

@end