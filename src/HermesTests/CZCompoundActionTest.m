//
//  CZCompoundActionTest.m
//  HermesTests
//
//  Created by Ralph Jin on 1/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCompoundActionTest.h"
#import <OCMock/OCMock.h>
#import "CZCompoundAction.h"
#import "CZDeallocateChecking.h"
#import "CZUndoableActionStub.h"


@implementation CZCompoundActionTest

- (void)testUndo {
    id mock = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[mock expect] undo];
    CZCompoundAction *compoundAction = [CZCompoundAction new];
    [compoundAction addAction:mock];
    [compoundAction undo];
    
    [mock verify];
    
    [compoundAction release];
}

- (void)testRedo {
    id mock = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[mock expect] redo];
    CZCompoundAction *compoundAction = [CZCompoundAction new];
    [compoundAction addAction:mock];
    [compoundAction redo];
    
    [mock verify];
    
    [compoundAction release];
}

- (void)testDealloc {
    id mock = [OCMockObject mockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock expect] didDeallocated];
    
    CZUndoableActionStub *theAction = [[CZUndoableActionStub alloc] initWithChecker:mock];
    CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
    [compoundAction addAction:theAction];
    [theAction release];
    [compoundAction release];
    
    [mock verify];
}

- (void)testCostMemory {
    id action1 = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[[action1 stub] andReturnValue:OCMOCK_VALUE((NSUInteger) {1})] costMemory];
    id action2 = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[[action2 stub] andReturnValue:OCMOCK_VALUE((NSUInteger) {1})] costMemory];
    
    CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
    [compoundAction addAction:action1];
    [compoundAction addAction:action2];
    
    XCTAssertEqual(2U, [compoundAction costMemory], @"total cost shall be 1+1=2.");
    
    [compoundAction release];
}

// TODO: add ut for order -(void)

@end
