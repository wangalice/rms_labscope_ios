//
//  CZNodeArcTest.m
//  Matscope
//
//  Created by Ralph Jin on 11/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZNodeArc.h"

@interface CZNodeArcTest : XCTestCase

@property (nonatomic, retain) CZNodeArc *arc;

@end

@implementation CZNodeArcTest

- (void)setUp {
    [super setUp];

    // Set-up code here.
    _arc = [[CZNodeArc alloc] init];
    _arc.cx = 0.0;
    _arc.cy = 0.0;
    _arc.radius = 100.0;
    _arc.startAngle = 0.0;
    _arc.endAngle = M_PI;
}

- (void)tearDown {
    // Tear-down code here.
    [_arc release];
    
    [super tearDown];
}

- (void)testInit {
    XCTAssertEqual(self.arc.cx, 0.0f, @"");
    XCTAssertEqual(self.arc.cy, 0.0f, @"");
    XCTAssertEqual(self.arc.radius, 100.0f, @"");
    XCTAssertEqual(self.arc.startAngle, 0.0f, @"");
    XCTAssertEqual(self.arc.endAngle, (CGFloat)M_PI, @"");
}

- (void)testObserveSelf {
    CGPathRef path = self.arc.relativePath;
    path = CGPathRetain(path);
    XCTAssertTrue(path != NULL, @"path should not be nil");
    
    self.arc.cx = 30.0;
    CGPathRef changedPath = self.arc.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.arc.cy = 30.0;
    changedPath = self.arc.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.arc.radius = 120.0;
    changedPath = self.arc.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.arc.startAngle = M_PI;
    changedPath = self.arc.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.arc.endAngle = 0.0;
    changedPath = self.arc.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
}

- (void)testHittest {
    CGFloat tolerance = 3.0f;
    CGPoint p1 = {.x = 100, .y = 0};
    XCTAssertTrue([self.arc hitTest:p1 tolerance:tolerance], @"start point should hit on");
    
    CGPoint p2 = {.x = -100, .y = 0};
    XCTAssertTrue([self.arc hitTest:p2 tolerance:tolerance], @"end point should hit on");
    
    CGPoint p3 = {.x = 0, .y = 100};
    XCTAssertTrue([self.arc hitTest:p3 tolerance:tolerance], @"middle point should hit on");
    
    CGPoint centerPoint = {.x = 0, .y = 0};
    XCTAssertFalse([self.arc hitTest:centerPoint tolerance:tolerance], @"center point shouldn't hit on middle point");
}

- (void)testCreateLayer {
    CALayer *layer = [self.arc newLayer];
    XCTAssertNotNil(layer, @"layer shall not be nil.");
    
    CGRect expectFrame = CGRectMake(-100, 0, 200, 100);
    CGRect expectBounds = CGRectMake(0, 0, 200, 100);
    
    CGRect resultFrame = layer.frame;
    CGRect resultBounds = layer.bounds;
    const CGFloat accuracy = 1e-4;
    XCTAssertEqualWithAccuracy(expectBounds.origin.x, resultBounds.origin.x, accuracy, @"bounds shall be (0, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectBounds.origin.y, resultBounds.origin.y, accuracy, @"bounds shall be (0, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectBounds.size.width, resultBounds.size.width, accuracy, @"bounds shall be (0, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectBounds.size.height, resultBounds.size.height, accuracy, @"bounds shall be (0, 0, 200, 100)");
    
    XCTAssertEqualWithAccuracy(expectFrame.origin.x, resultFrame.origin.x, accuracy, @"frame shall be (-100, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectFrame.origin.x, resultFrame.origin.x, accuracy, @"frame shall be (-100, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectFrame.size.width, resultFrame.size.width, accuracy, @"frame shall be (-100, 0, 200, 100)");
    XCTAssertEqualWithAccuracy(expectFrame.size.height, resultFrame.size.height, accuracy, @"frame shall be (-100, 0, 200, 100)");
    
    [layer release];
}

@end
