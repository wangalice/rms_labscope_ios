//
//  CZElementRectangleTest.m
//  HermesTests
//
//  Created by Ralph Jin on 1/25/13.
//  Copyright (c) 2013 Ralph Jin. All rights reserved.
//

#import "CZElementRectangleTest.h"
#import "CZElementRectangle.h"

@implementation CZElementRectangleTest

- (void)setUp {
    [super setUp];
    
    _rect = [[CZElementRectangle alloc] initWithRect:CGRectMake(3, 7, 10, 11)];
}

- (void)tearDown {
    // Tear-down code here.
    [_rect release];
    
    [super tearDown];
}

- (void)testProperties {
    CGRect cgRect = _rect.rect;
    XCTAssertEqual(cgRect.origin.x, 3.0f, @"rect's x shall be 3.");
    XCTAssertEqual(cgRect.origin.y, 7.0f, @"rect's y shall be 7.");
    XCTAssertEqual(cgRect.size.width, 10.0f, @"rect's width shall be 10.");
    XCTAssertEqual(cgRect.size.height, 11.0f, @"rect's y shall be 11.");
}

- (void)testNewLayer {
    CALayer *layer = [_rect newLayer];
    XCTAssertNotNil(layer, @"");
    [layer release];
}

- (void)testHitTest {
    CGFloat t = 3.0f;
    CGPoint topLeft = {.x = 3, .y = 7};
    XCTAssertTrue([_rect hitTest:topLeft tolerance:t], @"Hit on top left shall be hit on.");
}

- (void)testShapeMemo {
    NSDictionary *memo = [_rect newShapeMemo];
    XCTAssertNotNil([memo objectForKey:@"CGRect"], @"CZElementRectangle's memo shall be a CGRect value");
    [memo release];
}

- (void)testRestoreFromMemo {
    CGFloat x = 1, y = 2, width = 3, height = 4;
    CGRect rect = CGRectMake(x, y, width, height);
    NSDictionary *value = @{@"x":@1, @"y": @2, @"w": @3, @"h": @4};
    NSNumber *rotateAngle = [NSNumber numberWithFloat:0.5f];
    NSDictionary *memo = [NSDictionary dictionaryWithObjectsAndKeys:value, @"CGRect", rotateAngle, @"rotateAngle", nil];
    [_rect restoreFromShapeMemo:memo];
    
    XCTAssertTrue(CGRectEqualToRect(rect, _rect.rect), @"");
    XCTAssertEqualWithAccuracy(0.5f, _rect.rotateAngle, 1e-5, @"rotate angle shall be remembered.");
}

- (void)testFeature {
    NSUInteger count = _rect.features.count;
    XCTAssertTrue(count > 0U, @"");
}
@end
