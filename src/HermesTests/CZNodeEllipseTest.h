//
//  CZNodeEllipseTest.h
//  Hermes
//
//  Created by Ralph Jin on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZNodeEllipse.h"

@interface CZNodeEllipseTest : XCTestCase

@property (nonatomic, retain) CZNodeEllipse *ellipse;

@end
