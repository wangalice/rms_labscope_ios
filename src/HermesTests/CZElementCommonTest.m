//
//  CZElementCommonTest.m
//  Matscope
//
//  Created by Ralph Jin on 3/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZElementCommon.h"
#include "CZCubicBezier.h"

@interface CZElementCommonTest : XCTestCase

@end

@implementation CZElementCommonTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIntergalE {
    CZMathFunctionBlock f = ^(double x) {
        return 1.0 / x;
    };
    
    int lenw = 128;
    double w[128 + 1], result, err;
    ClenshawCurtisInit(lenw, w);
    
    double wCopy[128 + 1];
    
    double expectResult = 2.302585092994046;  // ln10
    double expectError[15] = {1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8,
        1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14, 1e-15};
    
    for (int i = 0; i < 15; i++) {
        result = 0.0;
        memcpy(wCopy, w, sizeof(double) * (lenw + 1));
        ClenshawCurtis(f, 1.0, 10.0, expectError[i], lenw, wCopy, &result, &err);
        XCTAssertEqualWithAccuracy(expectResult, result, expectError[i], @"");
    }
}


- (void)testLengthOfCubicBezier{
    // quater of a circle with radius 2
    CGFloat ratio = 0.551915024494;

    CGPoint p[4];
    p[0] = CGPointMake(0, 2);
    p[1] = CGPointMake(2 * ratio, 2);
    p[2] = CGPointMake(2, 2 * ratio);
    p[3] = CGPointMake(2, 0);
    
    double result = [CZElementCommon lengthOfCubicBezierSegments:p pointsCount:4];
    
    // No accuracy any more, since circle to bezier is an approximation.
    XCTAssertEqualWithAccuracy(M_PI, result, 1e-3, @"");
}

- (void)testAreaOfCubicBezierQuaterCircle {
    // quater of a circle with radius 2
    const CGFloat ratio = 0.551915024494;  // bezier to circle approximation ratio
    
    CGPoint p[4];
    p[0] = CGPointMake(0, 2);
    p[1] = CGPointMake(2 * ratio, 2);
    p[2] = CGPointMake(2, 2 * ratio);
    p[3] = CGPointMake(2, 0);
    
    double result = [CZElementCommon areaOfCubicBezierSegments:p pointsCount:4];
    
    // No accuracy any more, since circle to bezier is an approximation.
    XCTAssertEqualWithAccuracy(M_PI, result, 1e-3, @"");
}

- (void)testAreaOfCubicBezierCircle {
    // a circle with radius 1
    const CGFloat ratio = 0.551915024494;  // bezier to circle approximation ratio
    
    CGPoint p[13];
    p[0] = CGPointMake(0, 1);
    p[1] = CGPointMake(ratio, 1);
    p[2] = CGPointMake(1, ratio);
    p[3] = CGPointMake(1, 0);
    
    p[4] = CGPointMake(1, -ratio);
    p[5] = CGPointMake(ratio, -1);
    p[6] = CGPointMake(0, -1);
    
    p[7] = CGPointMake(-ratio, -1);
    p[8] = CGPointMake(-1, -ratio);
    p[9] = CGPointMake(-1, 0);
    
    p[10] = CGPointMake(-1, ratio);
    p[11] = CGPointMake(-ratio, 1);
    p[12] = CGPointMake(0, 1);
    
    double result = [CZElementCommon areaOfCubicBezierSegments:p pointsCount:13];
    
    // No accuracy any more, since circle to bezier is an approximation.
    XCTAssertEqualWithAccuracy(M_PI, result, 1e-3, @"");
}

- (void)testAreaOfCubicBezier2 {
    // y = x ^ 3 [0, 1]
    CGPoint p[4];
    p[0] = CGPointMake(0, 0);
    p[1] = CGPointMake(1.0 / 3.0, 0);
    p[2] = CGPointMake(2.0 / 3.0, 0);
    p[3] = CGPointMake(1, 1);
    
    double result = [CZElementCommon areaOfCubicBezierSegments:p pointsCount:4];
    
    // No accuracy any more, since circle to bezier is an approximation.
    XCTAssertEqualWithAccuracy(0.25, result, 1e-15, @"");
}

- (void)testRombergPi {
    CZMathFunctionBlock f = ^(double x) {
        return 4.0 / (1.0 + x * x);
    };
    
    int lenw = 128;
    double w[128 + 1], result, err;
    ClenshawCurtisInit(lenw, w);
    
    double wCopy[128 + 1];
    
    double expectResult = M_PI;
    double expectError[15] = {1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8,
        1e-9, 1e-10, 1e-11, 1e-12, 1e-13, 1e-14, 1e-15};
    
    for (int i = 0; i < 15; i++) {
        result = 0.0;
        memcpy(wCopy, w, sizeof(double) * (lenw + 1));
        ClenshawCurtis(f, 0.0, 1.0, expectError[i], lenw, wCopy, &result, &err);
        XCTAssertEqualWithAccuracy(expectResult, result, expectError[i], @"");
    }
}

- (void)testBezierSelfIntersect {
    CubicBezier b;
    b.p0 = CGPointMake(1, 0);
    b.p1 = CGPointMake(-2, 1);
    b.p2 = CGPointMake(4, 1);
    b.p3 = CGPointMake(-1, 0);

    double t[2];
    int length = CubicBezierFindSelfIntersections(&b, t);
    XCTAssertEqual(length, 2, @"The bezier shall be self-intersected.");
    
    b.p1 = CGPointMake(4, 1);
    b.p2 = CGPointMake(-2, 1);
    
    length = CubicBezierFindSelfIntersections(&b, t);
    XCTAssertEqual(length, 0, @"The bezier shall be not self-intersected.");
    
    b.p1 = CGPointMake(-1, 1);  // special case when t0 = t1 = 0.5
    b.p2 = CGPointMake(1, 1);
    length = CubicBezierFindSelfIntersections(&b, t);
    XCTAssertEqual(length, 0, @"The bezier shall be not self-intersected.");
    
    b.p0 = CGPointMake(0, 0);
    b.p1 = CGPointMake(1, 1);
    b.p2 = CGPointMake(3, 2);
    b.p3 = CGPointMake(2, 3);
    length = CubicBezierFindSelfIntersections(&b, t);
    XCTAssertEqual(length, 0, @"The bezier shall be not self-intersected.");
}

- (void)testSpecialBezier {
    CubicBezier b;
    b.p0 = CGPointMake(0, 0);
    b.p1 = CGPointMake(1, 1);
    b.p2 = CGPointMake(3, 3);
    b.p3 = CGPointMake(2, 2);
    
    double t[2];
    int length = CubicBezierFindSelfIntersections(&b, t);
    XCTAssertEqual(length, 0, @"The bezier shall be not self-intersected.");
    
    double area = [CZElementCommon areaOfCubicBezierSegments:&b.p0 pointsCount:4];
    XCTAssertEqualWithAccuracy(2, area, 1e-15);
}

@end
