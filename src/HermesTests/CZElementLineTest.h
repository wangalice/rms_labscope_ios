//
//  CZElementLineTest.h
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZElementLine.h"

@interface CZElementLineTest : XCTestCase

@property (nonatomic, retain) CZElementLine *line;

@end
