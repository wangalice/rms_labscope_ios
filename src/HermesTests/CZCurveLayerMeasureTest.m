//
//  CZCurveLayerMeasureTest.m
//  Matscope
//
//  Created by Ralph Jin on 1/5/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZCurveLayerMeasure.h"
#import "CZNodePolyline.h"

@interface CZCurveLayerMeasureTest : XCTestCase

@end

@implementation CZCurveLayerMeasureTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMeasureStraightLayer {
    CZNodePolyline *edge1 = [[CZNodePolyline alloc] init];
    [edge1 appendPoint:CGPointMake(10, 0)];
    [edge1 appendPoint:CGPointMake(10, 100)];
    [edge1 appendPoint:CGPointMake(10, 200)];
    
    CZNodePolyline *edge2 = [[CZNodePolyline alloc] init];
    [edge2 appendPoint:CGPointMake(20, 0)];
    [edge2 appendPoint:CGPointMake(20, 100)];
    [edge2 appendPoint:CGPointMake(20, 200)];
    
    CZCurveLayerMeasure *measure = [[CZCurveLayerMeasure alloc] init];
    measure.searchCount = 10;
    [measure measureLayerInEdge:edge1 anotherEdge:edge2];
    [edge1 release];
    [edge2 release];
    
    XCTAssert(([measure.result count] == 2 * measure.searchCount), @"measure success");
    
    [measure release];
}

@end
