//
//  CZUnitTestUtility.m
//  Hermes
//
//  Created by Halley Gu on 2/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZUnitTestUtility.h"

@implementation CZUnitTestUtility

+ (UIImage *)testImage {
    // Must get image path from test bundle and use "imageWithContentsOfFile:"
    // to initialize the UIImage object. "imageNamed:" won't work here.
    NSBundle *bundle = [NSBundle bundleForClass:[CZUnitTestUtility class]];
    NSString *imagePath = [bundle pathForResource:@"sample.png"
                                           ofType:nil
                                      inDirectory:@"Assets"];
    return [UIImage imageWithContentsOfFile:imagePath];
}

@end
