//
//  CZFileNameGeneratorTest.m
//  Hermes
//
//  Created by Ralph Jin on 5/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameGeneratorTest.h"
#import "CZFileNameGenerator.h"

@implementation CZFileNameGeneratorTest

- (void)testSearchAutoNumber {
    NSString *sampleFile = @"Snap_001_Mik_Ralph.czi";
    NSString *expressionFormat = [NSRegularExpression escapedPatternForString:@"%@Snap_%@_Mik_Ralph.czi%@"];
    NSString *expression = [NSString stringWithFormat:expressionFormat, @"^", @"(\\d{3,})", @"$"];
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];

    NSTextCheckingResult *match = [regex firstMatchInString:sampleFile
                                                    options:0
                                                      range:NSMakeRange(0, [sampleFile length])];
    if (match) {
        NSRange matchRange = [match rangeAtIndex:1];
        NSString *matchString = [sampleFile substringWithRange:matchRange];
        XCTAssertEqualObjects(@"001", matchString, @"");
    }
}

- (void)testGenerateFileName {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"3"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeDate
                                                    value:@"0"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeVariableText
                                                    value:@"Operator"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.operatorName = @"Ralph";
    generator.microscope = @"Apollo";
    generator.extension = @"czi";
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    [components setMonth:5];
    [components setYear:2013];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [gregorian dateFromComponents:components];
    [components release];
    generator.captureDate = date;
    
    NSArray *existFiles = @[@"Snap_001_2013-05-01_Ralph.czi",
                            @"Snap_1001_2013-05-01_Ralph.jpg",   // mix extension shall not affect result
                            @"xSnap_1099_2013-05-01_Ralph.czi",  // other format file
                            @"Snap_1099_2013-05-01_Ralphi.czi"   // other format file
                            ];

    // test
    NSString *newFileName = [generator uniqueFileNameFromNames:existFiles];
    XCTAssertEqualObjects(@"Snap_1002_2013-05-01_Ralph.czi", newFileName, @"");
    
    [nameTemplate release];
}

- (void)testDefaultAutoNumber {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                            value:@"Iron"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];

    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:@[@"Snap_Iron.czi"]];
    XCTAssertEqualObjects(@"Snap_Iron(02).czi", newFileName, @"");
    
    // test 2
    newFileName = [generator uniqueFileNameFromNames:@[@"otherfile.czi"]];
    XCTAssertEqualObjects(@"Snap_Iron.czi", newFileName, @"");

    // test 3
    newFileName = [generator uniqueFileNameFromNames:@[@"Snap_Iron.czi", @"Snap_Iron(09).czi"]];
    XCTAssertEqualObjects(@"Snap_Iron(10).czi", newFileName, @"");    
}

- (void)testDefaultAutoNumber2 {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"SnapIron(02)"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];
    
    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:@[@"SnapIron(02).czi"]];
    XCTAssertEqualObjects(@"SnapIron(02)(02).czi", newFileName, @"");
}

- (void)testSampleFileName {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap.Iron"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"3"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    [nameTemplate release];
    
    // test 1
    NSString *sampleFileName = [generator sampleFileName];
    XCTAssertEqualObjects(@"Snap.Iron_###", sampleFileName, @"");
}

- (void)testNumberField1 {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"1"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];
    
    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:@[@"Snap_01.czi", @"Snap_001.czi"]];
    XCTAssertEqualObjects(@"Snap_2.czi", newFileName, @"");
}

- (void)testNumberField2 {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"2"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];
    
    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:@[@"Snap_001.czi", @"Snap_1.czi"]];
    XCTAssertEqualObjects(@"Snap_02.czi", newFileName, @"");
}

- (void)testNumberField3 {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"3"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];
    
    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:@[@"Snap_01.czi", @"Snap_1.czi"]];
    XCTAssertEqualObjects(@"Snap_002.czi", newFileName, @"");
}

- (void)testSpecialCharacter {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"%{#%@"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"3"];
    [nameTemplate addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    [nameTemplate release];
    
    // test 1
    NSString *newFileName = [generator uniqueFileNameFromNames:nil];
    XCTAssertEqualObjects(@"%{#%@_001.czi", newFileName, @"");
}

- (void)testEmptyField {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@""];
    [nameTemplate addField:field];
    [field release];
    
    // test 1
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    NSString *newFileName = [generator uniqueFileNameFromNames:nil];
    XCTAssertEqualObjects(@"(01).czi", newFileName, @"");
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Begin"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@""];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"End"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@""];
    [nameTemplate addField:field];
    [field release];

    // test 2
    newFileName = [generator uniqueFileNameFromNames:nil];
    XCTAssertEqualObjects(@"Begin_End.czi", newFileName, @"");
    
    [nameTemplate release];
    [generator release];
}

- (void)testEmptyField2 {
    // init
    CZFileNameTemplate *nameTemplate = [[CZFileNameTemplate alloc] init];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Begin"];
    [nameTemplate addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@""];
    [nameTemplate addField:field];
    [field release];
    
    // test
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:nameTemplate];
    generator.extension = @"czi";
    NSString *newFileName = [generator uniqueFileNameFromNames:nil];
    XCTAssertEqualObjects(@"Begin.czi", newFileName, @"");
    
    newFileName = [generator uniqueFileNameFromNames:@[@"Begin.czi"]];
    XCTAssertEqualObjects(@"Begin(02).czi", newFileName, @"");
}

@end
