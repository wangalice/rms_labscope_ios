//
//  CZUndoableActionStub.h
//  HermesTests
//
//  Created by Ralph Jin on 1/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZUndoAction.h"

@protocol CZDeallocateChecking;

@interface CZUndoableActionStub : NSObject<CZUndoAction>

@property (nonatomic, readonly, retain) id<CZDeallocateChecking> checker;
@property (nonatomic) NSUInteger memoryCosted; // default is 1

- (id)initWithChecker:(id<CZDeallocateChecking>) anChecker;

@end
