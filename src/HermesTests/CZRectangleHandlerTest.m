//
//  CZRectangleHandlerTest.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRectangleHandlerTest.h"
#import "CZRectangleHandler.h"
#import "CZElementRectangle.h"

@implementation CZRectangleHandlerTest


- (void)testHitTest {
    CZElementRectangle *rect = [[CZElementRectangle alloc] initWithRect:CGRectMake(0, 50, 100, 150)];
    CZRectangleHandler *handler = [[CZRectangleHandler alloc] init];
    handler.target = rect;
    [handler updateWithZoom:1.0f];
    
    XCTAssertNotNil([handler representLayer], @"|updateWithZoom| shall create represent layer.");
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    
    CGPoint p;
    p.x = 100 / screenScale;
    p.y = 200 / screenScale;
    XCTAssertEqual(CZAnnotationHandlerPartResizeBottomRight, [[handler hitTest:p] integerValue], @"It shall hit the right bottom resize handler part.");
    
    p.x = 1000 / screenScale;
    p.y = 1000 / screenScale;
    NSString *hitPart = [handler hitTest:p];
    XCTAssertTrue([hitPart length] == 0U, @"It shall hit nothing.");
    
    [rect release];
    [handler release];
}

- (void)testPositionOfPart {
    CZElementRectangle *rect = [[CZElementRectangle alloc] initWithRect:CGRectMake(0, 50, 100, 150)];
    CZRectangleHandler *handler = [[CZRectangleHandler alloc] init];
    handler.target = rect;
    
    XCTAssertNoThrow([handler positionOfPart:-1], @"Handler should not trow error, when input value.");
    
    [rect release];
    [handler release];
}

@end
