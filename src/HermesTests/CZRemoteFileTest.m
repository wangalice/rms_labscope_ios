//
//  CZRemoteFileTest.m
//  Matscope
//
//  Created by Mike Wang on 3/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <WNFSFramework/CZFileDescriptor.h>
#import <WNFSFramework/CZWNFSManager.h>
#import <WNFSFramework/CZWNFSManagerDelegate.h>
#import <WNFSFramework/CZSMBAdapter.h>
#import "UIImage+CZIImage.h"

#define CZ_REMOTE_FILE_TEST 0

#if CZ_REMOTE_FILE_TEST

#define kCZRemoteServerIPRouter @"192.168.1.21"
#define kCZRemoteServerIPXP @"192.168.1.101"
#define kCZRemoteServerIPWIN7 @"192.168.1.102"
#define kCZRemoteServerIPWIN8 @"192.168.1.103"
#define kCZRemoteServerIP2003Server @"192.168.1.104"
#define kCZRemoteServerIP2005Server @"192.168.1.105"

#define kCZRemoteSharePath @"hermes/test"
#define kCZRemoteTestUserName @"hermes"
#define kCZRemoteTestUserPassword @"hermes"
#define kCZRemoteTestDomain @""
#define kCZRemoteTestSample1CZI @"sample.czi"
#define kCZRemoteSampleFilesCount 10

typedef enum {
    kCZRemoteFileTestStateListFiles,
    kCZRemoteFileTestStateUploadFiles,
    kCZRemoteFileTestStateDownloadFiles,
    kCZRemoteFileTestStateGetThumbnails,
    kCZRemoteFileTestStateDeleteFiles,
    kCZRemoteFileTestStateFinished,
    kCZRemoteFileTestStateTimeOut
} CZRemoteFileTestState;

@interface CZRemoteFileTest : XCTestCase <CZWNFSManagerDelegate> {
    CZWNFSManager * _manager;
}
@property (nonatomic, retain) NSString *sampleFilePath;
@property (nonatomic, retain) NSString *cachePath;
@property (nonatomic, retain) NSMutableArray *testFilesList;
@property (nonatomic, retain) NSString *remoteSharePath;
@property (nonatomic, assign) CZRemoteFileTestState testState;
@end

@implementation CZRemoteFileTest

- (void)setUp {
    [super setUp];
    BOOL isSuccess = [self setupSampleFilesTest:kCZRemoteTestSample1CZI];
    XCTAssertFalse(isSuccess != YES, @"setupSampleFilesTest failed");
    
    _manager = [CZWNFSManager sharedInstance];
    XCTAssertFalse(_manager == nil, @"Could not create CZWNFSManager");
    [_manager setAccountInformation:kCZRemoteTestDomain
                           userName:kCZRemoteTestUserName
                           password:kCZRemoteTestUserPassword];
}

- (void)tearDown {
    [self tearDownSampleFilesTest];
    [_sampleFilePath release];
    [_cachePath release];
    [_testFilesList release];
    [_remoteSharePath release];
    
    [super tearDown];
}

- (NSArray *)remoteServersList {
    return @[
//             kCZRemoteServerIPXP,
//             kCZRemoteServerIPWIN7,
//             kCZRemoteServerIPWIN8,
//             kCZRemoteServerIP2003Server,
//             kCZRemoteServerIP2005Server,
             kCZRemoteServerIPRouter];
}

- (NSString *)cachePath {
    if (!_cachePath) {
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        _cachePath = [[NSString stringWithFormat:@"%@/", path] retain];
    }
    return _cachePath;
}

- (NSMutableArray *)testFilesList {
    if (!_testFilesList) {
        _testFilesList = [[NSMutableArray alloc] init];
    }
    
    return _testFilesList;
}

- (void)waitForDone {
    while (self.testState != kCZRemoteFileTestStateFinished &&
           self.testState != kCZRemoteFileTestStateTimeOut) {
        @autoreleasepool {
            [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
        }
    }
}

/********************************************************************************************
How to setup testSampleFiles:
1. Create a sample file under Supporting Files/Assets
2. Define test servers' ip address for the following
     kCZRemoteServerIPRouter @"192.168.1.21"
     kCZRemoteServerIPXP @"192.168.1.101"
     kCZRemoteServerIPWIN7 @"192.168.1.102"
     kCZRemoteServerIPWIN8 @"192.168.1.103"
     kCZRemoteServerIP2003Server @"192.168.1.104"
     kCZRemoteServerIP2005Server @"192.168.1.105"
 
2. Define test server environment
     kCZRemoteSharePath @"hermes/test"
     kCZRemoteTestUserName @"hermes"
     kCZRemoteTestUserPassword @"hermes"
     kCZRemoteTestDomain @""
     kCZRemoteTestSample1CZI @"sample.czi"
     kCZRemoteSampleFilesCount 10
 
3. Include/Exclude test servers
     comment or uncomment the test servers in remoteServersList
 
Test Description:
1. copy the sample file from Assets to Cache directory by kCZRemoteSampleFilesCount times
2. delete all test files from remote server directory
3. upload all sample files to remote server
4. get thumbnails from remote server
5. download sample files from remote server
6. delete all sample files from remote server
*************************************************************************************************/

- (void)testSampleFiles {
    for (NSString *remoteServerIP in [self remoteServersList]) {
        self.remoteSharePath = [NSString stringWithFormat:@"//%@/%@", remoteServerIP, kCZRemoteSharePath];
        
        [self listDirectoryFromRemotePath:self.remoteSharePath];
        [self waitForDone];
        
        [self deleteRemoteSampleFiles];
        [self waitForDone];
        
        [self updateLocalCacheDirectory];
        [self uploadSampleFilesToRemotePath:self.remoteSharePath];
        [self waitForDone];
        
        [self getThumbnailsFromRemotePath:self.remoteSharePath];
        
        [self listDirectoryFromRemotePath:self.remoteSharePath];
        [self waitForDone];

        [self downloadSampleFilesFromRemotePath:self.remoteSharePath];
        [self waitForDone];
        
        [self deleteRemoteSampleFiles];
        [self waitForDone];
    }
}

- (BOOL)setupSampleFilesTest:(NSString *)sampleFileName {
    NSBundle *bundle = [NSBundle bundleForClass:[CZRemoteFileTest class]];
    self.sampleFilePath = [bundle pathForResource:sampleFileName
                                           ofType:nil
                                      inDirectory:@"Assets"];
    NSString *fileExtension = [sampleFileName pathExtension];
    NSString *fileName = [sampleFileName stringByDeletingPathExtension];
    NSError *error = nil;
    
    [self tearDownSampleFilesTest];
    for (int i = 0; i < kCZRemoteSampleFilesCount; i++) {
        NSString *destinationFile = [NSString stringWithFormat:@"%@%@%d.%@", self.cachePath, fileName, i, fileExtension];
        [[NSFileManager defaultManager] copyItemAtPath:self.sampleFilePath
                                                toPath:destinationFile
                                                 error:&error];
        if (error) {
            CZLog(@"Failed to copy file (%@): %@", destinationFile, error.localizedDescription);
            return NO;
        }
        
        CZFileDescriptor *fileDescriptor = [[CZFileDescriptor alloc] init];
        fileDescriptor.fileName = [destinationFile lastPathComponent];
        fileDescriptor.filePath = destinationFile;
        [self.testFilesList addObject:fileDescriptor];
    }
    
    return YES;
}

- (void)tearDownSampleFilesTest {
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSArray *cacheContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:nil];
    for (NSString *cacheContent in cacheContents) {
        NSString *cacheItem = [NSString stringWithFormat:@"%@/%@", cachePath, cacheContent];
        
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:cacheItem error:&error];
        
        if (error) {
            CZLog(@"Failed to delete cache file %@: %@", cacheContent, [error localizedDescription]);
        }
    }
}

- (void)updateLocalCacheDirectory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:self.cachePath
                                                         error:NULL];
    int fileIndex = 0;
    [self.testFilesList removeAllObjects];
    for (NSString *sampleFileName in contents) {
        @autoreleasepool {
            NSString *destinationFile = [NSString stringWithFormat:@"%@%@", self.cachePath, sampleFileName];
            
            CZFileDescriptor *fileDescriptor = [[CZFileDescriptor alloc] init];
            fileDescriptor.fileName = [destinationFile lastPathComponent];
            fileDescriptor.filePath = destinationFile;
            [self.testFilesList addObject:fileDescriptor];
        }
    }
}

- (void)uploadSampleFilesToRemotePath:(NSString *)remotePath {
    self.testState = kCZRemoteFileTestStateUploadFiles;
    
    if (self.testFilesList.count <= 0) {
        self.testState = kCZRemoteFileTestStateFinished;
        CZLog(@"uploadSampleFilesToServer (%@) is finished", remotePath);
        return;
    }
    
    CZFileDescriptor *sourceTestFileDescriptor = self.testFilesList[0];
    NSString *sourceTestFilePath = [sourceTestFileDescriptor filePath];
    NSString *testFileName = [sourceTestFilePath lastPathComponent];
    NSString *destinationTestFilePath = [NSString stringWithFormat:@"%@/%@", remotePath, testFileName];
    
    [_manager sendFileTo:destinationTestFilePath
                   local:sourceTestFilePath
             overwriting:YES
                delegate:self];
}

- (void)getThumbnailsFromRemotePath:(NSString *)remotePath {
    [self listDirectoryFromRemotePath:self.remoteSharePath];
    [self waitForDone];
    
    self.testState = kCZRemoteFileTestStateGetThumbnails;
    
    if (self.testFilesList.count <= 0) {
        self.testState = kCZRemoteFileTestStateFinished;
        CZLog(@"getThumbnailsFromRemotePath (%@) is finished", remotePath);
        return;
    }
    
    for (CZFileDescriptor *fileDescriptor in self.testFilesList) {
        UIImage *thumbnail = [[UIImage alloc] initThumbWithContentsOfCZIFile:fileDescriptor.filePath];
        if (!thumbnail || thumbnail.size.width == 0 || thumbnail.size.height == 0) {
            CZLog(@"getThumbnailsFromRemotePath is failed for %@", fileDescriptor.filePath);
        }
    }
    
    self.testState = kCZRemoteFileTestStateFinished;
    CZLog(@"getThumbnailsFromRemotePath (%@) is finished", remotePath);
}

- (void)listDirectoryFromRemotePath:(NSString *)remotePath {
    self.testState = kCZRemoteFileTestStateListFiles;
    [self.testFilesList removeAllObjects];
    [_manager listItemsFrom:[remotePath stringByAppendingString:@"/"] delegate:self];
}

- (void)downloadSampleFilesFromRemotePath:(NSString *)remotePath {
    self.testState = kCZRemoteFileTestStateDownloadFiles;
    
    if (self.testFilesList.count <= 0) {
        self.testState = kCZRemoteFileTestStateFinished;
        CZLog(@"uploadSampleFilesToServer (%@) is finished", remotePath);
        return;
    }
    
    CZFileDescriptor *sourceTestFileDescriptor = self.testFilesList[0];
    NSString *sourceTestFilePath = [sourceTestFileDescriptor filePath];
    NSString *testFileName = [sourceTestFilePath lastPathComponent];
    NSString *destinationTestFilePath = [NSString stringWithFormat:@"%@%@", self.cachePath, testFileName];
    
    [_manager getFileFrom:sourceTestFilePath
                    local:destinationTestFilePath
              overwriting:TRUE
                 delegate:self];
}

- (void)deleteRemoteSampleFiles {
    self.testState = kCZRemoteFileTestStateDeleteFiles;
    
    if (self.testFilesList.count <= 0) {
        self.testState = kCZRemoteFileTestStateFinished;
        CZLog(@"deleteRemoteSampleFiles is finished");
        return;
    }
    
    for (CZFileDescriptor *fileDescriptor in self.testFilesList) {
        [_manager deleteItemFrom:fileDescriptor.filePath
                           force:YES
                        delegate:self];
    }
}

#pragma mark - CZWNFSManagerDelegate
- (void)operateDidChangeStatus:(NSDictionary *)status
                          type:(CZOperateType)type {
    NSLog(@"%@",status);
}

- (void)operateDidFinishItem:(NSDictionary*)item
                        type:(CZOperateType)type {
    XCTAssertFalse ((self.testState != kCZRemoteFileTestStateDeleteFiles && self.testState != kCZRemoteFileTestStateListFiles), @"test in the wrong state");
    
    if (type == CZOperateTypeListDirectory) {
        NSArray *array = [[item allValues] objectAtIndex:0];
        for (CZFileDescriptor *fileDesc in array) {
            if ([fileDesc.fileName isEqualToString:@"."] ||
                [fileDesc.fileName isEqualToString:@".."]) {
                continue;
            }

            fileDesc.filePath = [NSString stringWithFormat:@"%@/%@",
                                 self.remoteSharePath, fileDesc.fileName];
            [self.testFilesList addObject:fileDesc];
            CZLog(@"%@", fileDesc.filePath);
        }
        CZLog(@"CZOperateTypeListDirectory is finished with file count:%d",[self.testFilesList count]);
        self.testState = kCZRemoteFileTestStateFinished;
    } else {
        if (self.testState == kCZRemoteFileTestStateDeleteFiles) {
            XCTAssertFalse ((self.testFilesList.count <= 0), @"CZRemoteFileTestStateDeleteFiles is in the wrong state");
            
            NSString *destinationFile = [[item allValues] objectAtIndex:0];
            [self.testFilesList removeObjectAtIndex:0];
            
            CZLog(@"Delete (%@) is success", destinationFile);
            
            if (self.testFilesList.count <= 0) {
                self.testState = kCZRemoteFileTestStateFinished;
                CZLog(@"deleteRemoteSampleFiles is finished");
            }
        }
    }
}

- (void)operateDidFailWithError:(NSError*)error {
    XCTAssertFalse ((self.testState != kCZRemoteFileTestStateDeleteFiles && self.testState != kCZRemoteFileTestStateListFiles), @"test in the wrong state");
    
    if (self.testState == kCZRemoteFileTestStateListFiles) {
        CZLog(@"list items error: NSFilePath = %@, NSLocalizedDesciption = %@", error.userInfo[@"NSFilePath"], error.localizedDescription);
    } else {
        CZLog(@"delete item error: NSFilePath = %@, NSLocalizedDesciption = %@", error.userInfo[@"NSFilePath"], error.localizedDescription);
    }
    
    self.testState = kCZRemoteFileTestStateFinished;
}

- (void)dataTransferDidFinishDestination:(NSString*)path {
    NSString *destinationFile = path;
    [self.testFilesList removeObjectAtIndex:0];
    if (self.testState == kCZRemoteFileTestStateUploadFiles) {
        CZLog(@"Test destinatione file (%@) finish to upload", destinationFile);
        [self uploadSampleFilesToRemotePath:self.remoteSharePath];
    } else if (self.testState == kCZRemoteFileTestStateDownloadFiles) {
        CZLog(@"Test destinatione file (%@) finish to download", destinationFile);
        [self downloadSampleFilesFromRemotePath:self.remoteSharePath];
    } else {
        CZLog(@"Test destinatione file (%@) finish in the wrong state (%d)", destinationFile, self.testState);
        XCTAssertFalse ((self.testState != kCZRemoteFileTestStateUploadFiles && self.testState != kCZRemoteFileTestStateDownloadFiles), @"test in the wrong state");
    }
}

- (void)dataTransferDidFailWithError:(NSError*)error {
    NSString *destinationFile = error.userInfo[@"NSFilePath"];
    [self.testFilesList removeObjectAtIndex:0];
    
    if (self.testState == kCZRemoteFileTestStateUploadFiles) {
        CZLog(@"Test destinatione file (%@) failed to upload: %@", destinationFile, error.localizedDescription);
        [self uploadSampleFilesToRemotePath:self.remoteSharePath];
    } else if (self.testState == kCZRemoteFileTestStateDownloadFiles) {
        CZLog(@"Test destinatione file (%@) failed to download: %@", destinationFile, error.localizedDescription);
        [self downloadSampleFilesFromRemotePath:self.remoteSharePath];
    } else {
        CZLog(@"Test destinatione file (%@) failed in the wrong state (%d): %@", destinationFile, self.testState, error.localizedDescription);
        XCTAssertFalse ((self.testState != kCZRemoteFileTestStateUploadFiles && self.testState != kCZRemoteFileTestStateDownloadFiles), @"test in the wrong state");
    }
}

- (void)dataTransferProgress:(CZTransferDirection) direction
                   delivered:(u_int32_t) bytes
                       total:(u_int32_t) totalBytes {
}


@end

#endif  // CZ_REMOTE_FILE_TEST
