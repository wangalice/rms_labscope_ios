//
//  CZSelectToolTest.h
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>

@class UIView;
@class CZDocManager;
@class CZSelectTool;

@interface CZSelectToolTest : XCTestCase {
    UIView *view;
    CZDocManager *document;
    CZSelectTool *selectTool;
}

@end
