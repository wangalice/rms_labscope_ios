//
//  CZFileNameTemplateTest.m
//  Hermes
//
//  Created by Ralph Jin on 5/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameTemplateTest.h"
#import "CZFileNameTemplate.h"

@implementation CZFileNameTemplateTest

- (void)testReadWrite {
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"testReadWrite.plist"];
    
    CZFileNameTemplate *template = [[CZFileNameTemplate alloc] init];
    
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:@"Snap"];
    [template addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeAutoNumber
                                                    value:@"3"];
    [template addField:field];
    [field release];
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeVariableText
                                                    value:@"Operator"];
    [template addField:field];
    [field release];
    
    XCTAssertTrue([template writeToFile:filePath], @"It shall return YES.");
    [template release];
    
    template = [[CZFileNameTemplate alloc] initWithContentsOfFile:filePath];
    XCTAssertNotNil(template, @"It shall not be nil.");
    XCTAssertEqual(3U, [template fieldCount], @"It shall have 3 fields");
    
    field = [template fieldAtIndex:1];
    XCTAssertNotNil(field, @"Field shall not be nil.");
    XCTAssertEqual([field type], kCZFileNameTemplateFieldTypeAutoNumber, @"");
    XCTAssertEqualObjects([field value], @"3", @"");
}

- (void)testNilFormat {
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"testNilFormat.plist"];
    CZFileNameTemplateField *field = nil;
    
    field = [[CZFileNameTemplateField alloc] initWithType:kCZFileNameTemplateFieldTypeFreeText
                                                    value:nil];
    XCTAssertNil([field value], @"");
    
    CZFileNameTemplate *template = [[CZFileNameTemplate alloc] init];
    [template addField:field];
    
    XCTAssertTrue([template writeToFile:filePath], @"It shall return YES.");
    [template release];
    
    template = [[CZFileNameTemplate alloc] initWithContentsOfFile:filePath];
    XCTAssertNotNil(template, @"It shall not be nil.");
    
    field = [template fieldAtIndex:0];
    XCTAssertNotNil(field, @"Field shall not be nil.");
    XCTAssertNil([field value], @"");
}

@end
