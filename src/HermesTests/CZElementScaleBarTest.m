//
//  CZElementScaleBarTest.m
//  Hermes
//
//  Created by Ralph Jin on 2/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementScaleBarTest.h"
#import "CZElementScaleBar.h"

@implementation CZElementScaleBarTest

- (void)testTruncateValue {
    const float epsilon = 1e-5f;
    
    XCTAssertEqualWithAccuracy(0.0f, [CZElementScaleBar truncateValue:NAN], epsilon, @"");
    XCTAssertEqualWithAccuracy(0.0f, [CZElementScaleBar truncateValue:INFINITY], epsilon, @"");
    XCTAssertEqualWithAccuracy(0.0f, [CZElementScaleBar truncateValue:0.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(0.10f, [CZElementScaleBar truncateValue:0.09f], epsilon, @"");
    XCTAssertEqualWithAccuracy(1.0f, [CZElementScaleBar truncateValue:1.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(2000.0f, [CZElementScaleBar truncateValue:1800.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(2.0f, [CZElementScaleBar truncateValue:3.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(5.0f, [CZElementScaleBar truncateValue:4.5f], epsilon, @"");
    XCTAssertEqualWithAccuracy(5.0f, [CZElementScaleBar truncateValue:5.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(5.0f, [CZElementScaleBar truncateValue:7.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(10.0f, [CZElementScaleBar truncateValue:9.0f], epsilon, @"");
    XCTAssertEqualWithAccuracy(10.0f, [CZElementScaleBar truncateValue:10.0f], epsilon, @"");
}

@end
