//
//  CZNodeTextTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeTextTest.h"

@implementation CZNodeTextTest

- (void)setUp {
    [super setUp];
    
    _text = [[CZNodeText alloc] initWithFrame:CGRectMake(10.0f, 20.0f, 60.0f, 40.0f)];
    _text.string = @"Test string";
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    [_text release];
    
    [super tearDown];
}

- (void)testCreateLayer {
    CALayer *layer = [self.text newLayer];
    XCTAssertNotNil(layer, @"layer shall not be nil.");
    
    const CGFloat zoomScale = ([[UIScreen mainScreen] scale] == 2.0) ? 1.0 : 2.0;
    
    CGRect expectFrame = CGRectMake(10, 20, 60, 40);
    CGRect expectBounds = CGRectMake(0, 0, 60 * zoomScale, 40 * zoomScale);
    
    XCTAssertTrue(CGRectEqualToRect(expectBounds, layer.bounds), @"bounds shall be (0, 0, 60, 40)");
    XCTAssertTrue(CGRectEqualToRect(expectFrame, layer.frame), @"frame shall be (10, 20, 60, 40)");
    
    [layer release];
}

- (void)testBoundary {
    CGRect expectFrame = CGRectMake(10, 20, 60, 40);
    CGRect expectBounds = CGRectMake(0, 0, 60, 40);
    XCTAssertTrue(CGRectEqualToRect(expectBounds, [_text boundary]), @"bounds shall be (0, 0, 60, 40)");
    XCTAssertTrue(CGRectEqualToRect(expectFrame, [_text frame]), @"frame shall be (10, 20, 60, 40)");
}

@end
