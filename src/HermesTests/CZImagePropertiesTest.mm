//
//  CZImagePropertiesTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImagePropertiesTest.h"
#import "CZImageProperties+CZIMetadata.h"
#import "CZUnitTestUtility.h"
#include "pugixml.hpp"

@implementation CZImagePropertiesTest


- (void)testSaveLoad {
    CZImageProperties *properties = [[CZImageProperties alloc] init];
    properties.scaling = 1.5;
    properties.exposureTime = 0.066666667;
    properties.microscopeManufacturer = @"Carl Zeiss";
    properties.microscopeModel = @"Apollo";
    properties.originalDateTime = [NSDate date];
    properties.objectiveModel = @"Aplan20x";
    
    UIImage *image = [CZUnitTestUtility testImage];
        
    NSData* combineData = [properties JPEGRepresentationOfImage:image compression:0.5];
    
    XCTAssertNotNil(combineData, @"");
    
    CZImageProperties *properties2 = [[CZImageProperties alloc] init];
    [properties2 readMetadataFromImageRepresentation:combineData];
    
    XCTAssertEqualObjects(properties2.microscopeManufacturer, properties.microscopeManufacturer, @"");
    XCTAssertEqualObjects(properties2.microscopeModel, properties.microscopeModel, @"");
    XCTAssertEqualObjects(properties2.objectiveModel, properties.objectiveModel, @"");
    
    NSTimeInterval timeDiff = [properties2.originalDateTime timeIntervalSinceDate:properties.originalDateTime];
    XCTAssertTrue(fabs(timeDiff) < 1, @"");
    
    XCTAssertEqualWithAccuracy(properties2.scaling, properties.scaling, 1e-5, @"");
    XCTAssertEqualWithAccuracy(properties2.exposureTime, properties.exposureTime, 1e-5, @"");
        
    [properties release];
}

- (void)testSaveLoadEmptyProperties {
    CZImageProperties *properties = [[CZImageProperties alloc] init];
    UIImage* image = [CZUnitTestUtility testImage];
    NSData* combineData = [properties JPEGRepresentationOfImage:image compression:0.5];
    XCTAssertNotNil(combineData, @"");
    
    CZImageProperties *properties2 = [[CZImageProperties alloc] init];
    XCTAssertNoThrow([properties2 readMetadataFromImageRepresentation:combineData], @"");
    
    XCTAssertNil(properties2.microscopeManufacturer, @"");
    XCTAssertNil(properties2.microscopeModel, @"");
    XCTAssertNil(properties2.objectiveModel, @"");
    XCTAssertNil(properties2.originalDateTime, @"");
    
    XCTAssertTrue(properties2.scaling <= 0, @"It shall be invalid scaling.");
    XCTAssertTrue(properties2.exposureTime <= 0, @"It shall be invalid value.");
    
    [properties release];
}

- (void)testImportWithXMLData {
    NSBundle *bundle = [NSBundle bundleForClass:[CZImagePropertiesTest class]];
    NSString *filePath = [bundle pathForResource:@"annotationAllIn1.xml"
                                          ofType:nil
                                     inDirectory:@"Assets"];
    NSData* metaData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    pugi::xml_document doc;
    XCTAssertTrue(doc.load_buffer(metaData.bytes, metaData.length), @"");
    
    CZImageProperties *properties = [[CZImageProperties alloc] init];
    [properties readFromCZIMetadataXML:&doc];
    
    XCTAssertEqualWithAccuracy(0.5f, properties.scaling, 1e-5f, @"");
    XCTAssertNotNil(properties.microscopeManufacturer, @"");
    XCTAssertNotNil(properties.microscopeModel, @"");
    XCTAssertNotNil(properties.originalDateTime, @"");
    XCTAssertNotNil(properties.objectiveModel, @"");
    XCTAssertEqualWithAccuracy(0.004f, properties.exposureTime, 1e-5f, @"");
}
@end
