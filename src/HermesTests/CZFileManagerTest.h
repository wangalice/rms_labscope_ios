//
//  CZFileManagerTest.h
//  Hermes
//
//  Created by Mike Wang on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>


@interface CZFileManagerTest : XCTestCase

@property(nonatomic, retain) NSString *filePath;
    
@end
