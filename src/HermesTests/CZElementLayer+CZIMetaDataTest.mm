//
//  CZElementLayer+CZIMetaDataTest.mm
//  Hermes
//
//  Created by Ralph Jin on 4/19/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayer+CZIMetaDataTest.h"

#include "pugixml.hpp"
#import "CZElementLayer+CZIMetaData.h"
#import "CZElementRectangle.h"

@implementation CZElementLayerCZIMetaDataTest

- (void)testFileReadAnnotations
{
    NSBundle *bundle = [NSBundle bundleForClass:[CZElementLayerCZIMetaDataTest class]];
    NSString *filePath = [bundle pathForResource:@"annotationAllIn1.xml"
                                          ofType:nil
                                     inDirectory:@"Assets"];
    NSData *metaData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    pugi::xml_document doc;
    XCTAssertTrue(doc.load_buffer(metaData.bytes, metaData.length), @"");

    CZElementLayer *layer = [[CZElementLayer alloc] initWithMetaData:&doc imageSize:CGSizeMake(1024, 768)];
    XCTAssertNotNil(layer, @"");
    XCTAssertEqual([layer elementCount], 8U, @"annotationAllIn1.xml shall contain 8 elements.");
    
    CZElement *rectangle = [layer elementAtIndex:1U];
    XCTAssertNotNil(rectangle, @"The 2nd annotation in layer shall be a rectangle.");
    XCTAssertFalse([rectangle rotateAngle] == 0, @"");
    
    [layer release];
}


@end
