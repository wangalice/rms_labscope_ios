//
//  CZElementLineTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLineTest.h"

@implementation CZElementLineTest

- (void)setUp {
    [super setUp];
    
    _line = [[CZElementLine alloc] initWithPoint:CGPointMake(100, 100) anotherPoint:CGPointMake(200, 0)];
}

- (void)tearDown {
    // Tear-down code here.
    [_line release];
    
    [super tearDown];
}

- (void)testInit {
    CZElementLine *line1 = [[CZElementLine alloc]init];
    XCTAssertNotNil(line1, @"Default init shall be invalid.");
    [line1 release];
}

- (void)testProperties {
    XCTAssertEqual(_line.p1.x, 100.0f, @"");
    XCTAssertEqual(_line.p1.y, 100.0f, @"");
    XCTAssertEqual(_line.p2.x, 200.0f, @"");
    XCTAssertEqual(_line.p2.y, 0.0f, @"");
}

- (void)testNewLayer {
    CALayer *layer = [_line newLayer];
    XCTAssertNotNil(layer, @"");
    [layer release];
}

- (void)testHitTest {
    CGFloat t = 3.0f;
    XCTAssertFalse([_line hitTest:CGPointMake(94, 104) tolerance:t], @"It shall not be hit on.");
    XCTAssertTrue([_line hitTest:CGPointMake(98, 102) tolerance:t], @"It shall be hit on.");
    XCTAssertTrue([_line hitTest:CGPointMake(100, 100) tolerance:t], @"It shall be hit on.");
    XCTAssertTrue([_line hitTest:CGPointMake(150, 50) tolerance:t], @"It shall be hit on.");
    XCTAssertTrue([_line hitTest:CGPointMake(200, 0) tolerance:t], @"It shall be hit on.");
    XCTAssertTrue([_line hitTest:CGPointMake(202, -2) tolerance:t], @"It shall be hit on.");
    XCTAssertFalse([_line hitTest:CGPointMake(204, -4) tolerance:t], @"It shall not be hit on.");
}

- (void)testShapeMemo {
    NSDictionary *memo = [_line newShapeMemo];
    NSArray *pointArray = [memo objectForKey:@"CGPointArray"];
    XCTAssertNotNil(pointArray, @"CZElementLine's memo shall be a point array");
    XCTAssertEqual([pointArray count], 4U, @"It shall contain two points inside the point array.");
    [memo release];
}

- (void)testRestoreMemo {
    CGPoint p1 = CGPointMake(0, 0);
    CGPoint p2 = CGPointMake(2, 2);
    
    NSArray *pointArray = @[@0, @0, @2, @2];
    NSDictionary *memo = [NSDictionary dictionaryWithObjectsAndKeys:pointArray, @"CGPointArray", nil];
    
    [_line restoreFromShapeMemo:memo];
    XCTAssertTrue(CGPointEqualToPoint(_line.p1, p1), @"");
    XCTAssertTrue(CGPointEqualToPoint(_line.p2, p2), @"");
}

- (void)testApplyOffset {
    CGSize offset = CGSizeMake(100, 100);
    [_line applyOffset:offset];
    
    XCTAssertEqual(_line.p1.x, 200.0f, @"");
    XCTAssertEqual(_line.p1.y, 200.0f, @"");
    XCTAssertEqual(_line.p2.x, 300.0f, @"");
    XCTAssertEqual(_line.p2.y, 100.0f, @"");
}

- (void)testBoundary {
    CGRect boundary = [_line boundary];
    CGRect expect = CGRectMake(100, 0, 100, 100);
    XCTAssertTrue(CGRectEqualToRect(boundary, expect), @"");
}

@end
