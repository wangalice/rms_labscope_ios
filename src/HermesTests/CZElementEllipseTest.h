//
//  CZElementEllipseTest.h
//  Hermes
//
//  Created by Ralph Jin on 3/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

// TODO: delete in sprint 4

#import <SenTestingKit/SenTestingKit.h>
#import "CZElementEllipse.h"

@interface CZElementEllipseTest : SenTestCase

@property (nonatomic, readonly) CZElementEllipse *ellipse;

@end
