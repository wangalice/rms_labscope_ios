//
//  CZNodeEllipseTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeEllipseTest.h"

@implementation CZNodeEllipseTest

- (void)setUp {
    [super setUp];
    
    _ellipse = [[CZNodeEllipse alloc] initWithCenterX:40 centerY:50 radiusX:10 radiusY:15];
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    [_ellipse release];
    
    [super tearDown];
}

- (void)testInit {
    XCTAssertEqual(self.ellipse.cx, 40.0f, @"expect rect.x = 10.");
    XCTAssertEqual(self.ellipse.cy, 50.0f, @"expect rect.y = 20.");
    XCTAssertEqual(self.ellipse.rx, 10.0f, @"expect rect.x = 10.");
    XCTAssertEqual(self.ellipse.ry, 15.0f, @"expect rect.y = 20.");
}

- (void)testObserveSelf {
    CGPathRef path = self.ellipse.relativePath;
    path = CGPathRetain(path);
    XCTAssertTrue(path != NULL, @"path should not be nil");
    
    self.ellipse.cx = 30;
    CGPathRef changedPath = self.ellipse.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.ellipse.cy = 30;
    changedPath = self.ellipse.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.ellipse.rx = 120;
    changedPath = self.ellipse.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.ellipse.ry = 80;
    changedPath = self.ellipse.relativePath;
    XCTAssertTrue(path != changedPath, @"2 paths should not be same.");
    CGPathRelease(path);
}

- (void)testHittest {
    CGFloat tolerance = 3.0f;
    CGPoint top = {.x = 40, .y = 35};
    XCTAssertTrue([self.ellipse hitTest:top tolerance:tolerance], @"top point should hit on");
    
    CGPoint bottom = {.x = 40, .y = 65};
    XCTAssertTrue([self.ellipse hitTest:bottom tolerance:tolerance], @"bottom point should hit on");
    
    CGPoint right = {.x = 50, .y = 50};
    XCTAssertTrue([self.ellipse hitTest:right tolerance:tolerance], @"right point should hit on");
    
    CGPoint left = {.x = 30, .y = 50};
    XCTAssertTrue([self.ellipse hitTest:left tolerance:tolerance], @"left point should hit on");
    
    CGPoint centerPoint = {.x = 40, .y = 50};
    XCTAssertFalse([self.ellipse hitTest:centerPoint tolerance:tolerance], @"center point shouldn't hit on middle point");
    
    CGPoint outsidePoint = {.x = 5, .y = 15};
    XCTAssertFalse([self.ellipse hitTest:outsidePoint tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsidePoint2 = {.x = 115, .y = 115};
    XCTAssertFalse([self.ellipse hitTest:outsidePoint2 tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint topLeft = {.x = 30, .y = 35};
    XCTAssertFalse([self.ellipse hitTest:topLeft tolerance:tolerance], @"top left shouldn't hit on");
 
    self.ellipse.fillColor = kCZColorRed;
    XCTAssertTrue([self.ellipse hitTest:centerPoint tolerance:tolerance], @"center point should hit on middle point");
}

- (void)testCreateLayer {
    CALayer *layer = [self.ellipse newLayer];
    XCTAssertNotNil(layer, @"layer shall not be nil.");
    
    CGRect expectFrame = CGRectMake(30, 35, 20, 30);
    CGRect expectBounds = CGRectMake(0, 0, 20, 30);
    XCTAssertTrue(CGRectEqualToRect(expectBounds, layer.bounds), @"bounds shall be (0, 0, 60, 40)");
    XCTAssertTrue(CGRectEqualToRect(expectFrame, layer.frame), @"frame shall be (10, 20, 60, 40)");
    
    [layer release];
}

- (void)testHittestRotated {
    self.ellipse.rotateAngle = M_PI_2;
    
    CGFloat tolerance = 3.0f;
    CGPoint top = {.x = 40, .y = 40};
    XCTAssertTrue([self.ellipse hitTest:top tolerance:tolerance], @"top point should hit on");
    
    CGPoint bottom = {.x = 40, .y = 60};
    XCTAssertTrue([self.ellipse hitTest:bottom tolerance:tolerance], @"bottom point should hit on");
    
    CGPoint right = {.x = 25, .y = 50};
    XCTAssertTrue([self.ellipse hitTest:right tolerance:tolerance], @"right point should hit on");
    
    CGPoint left = {.x = 55, .y = 50};
    XCTAssertTrue([self.ellipse hitTest:left tolerance:tolerance], @"left point should hit on");
    
    CGPoint centerPoint = {.x = 40, .y = 50};
    XCTAssertFalse([self.ellipse hitTest:centerPoint tolerance:tolerance], @"center point shouldn't hit on middle point");
    
    CGPoint outsideTop = {.x = 40, .y = 30};
    XCTAssertFalse([self.ellipse hitTest:outsideTop tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsideBottom = {.x = 40, .y = 70};
    XCTAssertFalse([self.ellipse hitTest:outsideBottom tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsideLeft = {.x = 20, .y = 50};
    XCTAssertFalse([self.ellipse hitTest:outsideLeft tolerance:tolerance], @"outside point shouldn't hit on");
    
    CGPoint outsideRight = {.x = 60, .y = 50};
    XCTAssertFalse([self.ellipse hitTest:outsideRight tolerance:tolerance], @"outside point shouldn't hit on");
    
    self.ellipse.fillColor = kCZColorRed;
    XCTAssertTrue([self.ellipse hitTest:centerPoint tolerance:tolerance], @"center point should hit on middle point");
}

@end
