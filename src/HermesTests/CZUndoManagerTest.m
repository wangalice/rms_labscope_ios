//
//  CZUndoManagerTest.m
//  HermesTests
//
//  Created by Ralph Jin on 1/22/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZUndoManagerTest.h"
#import <OCMock/OCMock.h>
#import "CZUndoManager.h"
#import "CZUndoableActionStub.h"
#import "CZDeallocateChecking.h"

@implementation CZUndoManagerTest

- (void)setUp {
    [super setUp];
    
    _undoManager = [[CZUndoManager alloc] init];
}

- (void)tearDown {
    [_undoManager release];
    
    [super tearDown];
}

- (void)testPushAction {
    id mock = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[[mock stub] andReturnValue:OCMOCK_VALUE((NSUInteger) {1})] costMemory];
    
    [[mock expect] do];
    [[mock expect] undo];
    [[mock expect] redo];
        
    [self.undoManager pushAction:mock];
    XCTAssertTrue([self.undoManager canUndo], @"It can undo.");
    XCTAssertFalse([self.undoManager canRedo], @"It can not redo.");
    
    [self.undoManager undo];
    XCTAssertFalse([self.undoManager canUndo], @"It can not undo.");
    XCTAssertTrue([self.undoManager canRedo], @"It can redo.");
    
    [self.undoManager redo];
    XCTAssertTrue([self.undoManager canUndo], @"It can undo.");
    XCTAssertFalse([self.undoManager canRedo], @"It can not redo.");
    
    [mock verify];
}

- (void)testPushActionNoRedo {
    id mock = [OCMockObject mockForProtocol:@protocol(CZUndoAction)];
    [[[mock stub] andReturnValue:OCMOCK_VALUE((NSUInteger) {1})] costMemory];
    
    [[mock expect] do];
    [[mock expect] undo];
    [[mock expect] redo];
    
    [self.undoManager pushAction:mock];
    XCTAssertTrue([self.undoManager canUndo], @"It can undo.");
    XCTAssertFalse([self.undoManager canRedo], @"It can not redo.");
    
    [self.undoManager undo];
    XCTAssertFalse([self.undoManager canUndo], @"It can not undo.");
    XCTAssertTrue([self.undoManager canRedo], @"It can redo.");
    
    [self.undoManager redo];
    XCTAssertTrue([self.undoManager canUndo], @"It can undo.");
    XCTAssertFalse([self.undoManager canRedo], @"It can not redo.");
    
    [mock verify];
}

- (void)testMaxMemorySize {
    CZUndoManager *anUndoManager = [[CZUndoManager alloc] init];
    
    id mock = [OCMockObject mockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock expect] didDeallocated];
    
    anUndoManager.maxMemorySize = 1;
    
    CZUndoableActionStub *action1 = [[CZUndoableActionStub alloc] initWithChecker:mock];
    [anUndoManager pushAction:action1];
    [action1 release];
    
    id mock2 = [OCMockObject niceMockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock2 reject] didDeallocated];
    
    CZUndoableActionStub *action2 = [[CZUndoableActionStub alloc] initWithChecker:mock2];
    [anUndoManager pushAction:action2];
    [action2 release];
    
    [mock verify];
    [mock2 verify];
    
    // don't release the manager, otherwise all the actions in the manager will be deallocated.
    // [anUndoManger release];
}

- (void)testAtLeaseOneStepUndo {
    CZUndoManager *anUndoManager = [[CZUndoManager alloc] init];
    
    id mock1 = [OCMockObject niceMockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock1 reject] didDeallocated];
    
    CZUndoableActionStub *stub1 = [[CZUndoableActionStub alloc] initWithChecker:mock1];
    stub1.memoryCosted = 100U;
    
    anUndoManager.maxMemorySize = 1;
    [anUndoManager pushAction:stub1];
    [stub1 release];
    
    [mock1 verify];
    
    // don't release the manager, otherwise all the actions in the manager will be deallocated.
    // [anUndoManger release];
}

- (void)testRedoActionIsDeallocated {
    id deallocatedMock = [OCMockObject mockForProtocol:@protocol(CZDeallocateChecking)];
    [[deallocatedMock expect] didDeallocated];
    
    CZUndoableActionStub *stub1 = [[CZUndoableActionStub alloc] initWithChecker:deallocatedMock];
    [self.undoManager pushAction:stub1];
    [stub1 release];
    [self.undoManager undo];

    id mockAction = [OCMockObject niceMockForProtocol:@protocol(CZUndoAction)];
    [self.undoManager pushAction:mockAction];
    
    [deallocatedMock verify];
}

- (void)testClear {    
    // add action 1
    id mock = [OCMockObject mockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock expect] didDeallocated];
    CZUndoableActionStub *action = [[CZUndoableActionStub alloc] initWithChecker:mock];
    [self.undoManager pushAction:action];
    [action release];
    
    // add action 2
    id mock2 = [OCMockObject mockForProtocol:@protocol(CZDeallocateChecking)];
    [[mock2 expect] didDeallocated];
    CZUndoableActionStub *action2 = [[CZUndoableActionStub alloc] initWithChecker:mock2];
    [self.undoManager pushAction:action2];
    [action2 release];
    
    [self.undoManager removeAllActions];
    XCTAssertFalse([self.undoManager canUndo], @"It can not undo.");
    XCTAssertFalse([self.undoManager canRedo], @"It can not redo.");
    
    [mock verify];
    [mock2 verify];
}

- (void)testPushMultiActions {
    id mock = [OCMockObject niceMockForProtocol:@protocol(CZUndoAction)];
    id mock2 = [OCMockObject niceMockForProtocol:@protocol(CZUndoAction)];
    
    [self.undoManager pushAction:mock];
    [self.undoManager pushAction:mock2];
    
    XCTAssertTrue([self.undoManager canUndo], @"");
    
    [self.undoManager undo];
    XCTAssertFalse([self.undoManager canUndo], @"NSUndoManager auto combine 2 actions together.");
    
    [self.undoManager redo];
    XCTAssertFalse([self.undoManager canRedo], @"NSUndoManager auto combine 2 actions together.");
}

@end
