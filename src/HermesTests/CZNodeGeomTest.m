//
//  CZNodeGeomTest.m
//  Matscope
//
//  Created by Ralph Jin on 7/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZNodeGeom.h"

@interface CZNodeGeomTest : XCTestCase

@end

@implementation CZNodeGeomTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNoralizeAngle {
    CGFloat r = -M_PI;
    r = CZNormalizeAngle(r);
    XCTAssertEqualWithAccuracy(r, M_PI, 1e-6);
    
    r = -M_PI * 7;
    r = CZNormalizeAngle(r);
    XCTAssertEqualWithAccuracy(r, M_PI, 1e-6);
    
    r = M_PI * 2;
    r = CZNormalizeAngle(r);
    XCTAssertEqualWithAccuracy(r, 0, 1e-6);
    
    r = -M_PI_2;
    r = CZNormalizeAngle(r);
    XCTAssertEqualWithAccuracy(r, M_PI_2 * 3, 1e-6);
}

- (void)testCircleInsectedRectangle {
    CGRect rect = CGRectMake(10, 10, 10, 10);
    CGFloat r = 10;
    
    CGPoint center = CGPointMake(2, 2); // top left
    bool result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertFalse(result);
    
    center = CGPointMake(5, 5); // top left
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertTrue(result);
    
    center = CGPointMake(2, 28); // top right
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertFalse(result);
    
    center = CGPointMake(25, 5); // top right
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertTrue(result);
    
    center = CGPointMake(28, 28); // bottom right
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertFalse(result);
    
    center = CGPointMake(25, 25); // bottom right
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertTrue(result);
    
    center = CGPointMake(2, 28); // bottom left
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertFalse(result);
    
    center = CGPointMake(5, 25); // bottom right
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertTrue(result);
    
    center = CGPointMake(0, 15);  // left border in
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertTrue(result);
    
    center = CGPointMake(30, 15);  // right border out
    result = CGRectIntersectsCircle(rect, center, r);
    XCTAssertFalse(result);
}

@end
