//
//  CZNodeTextTest.h
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZNodeText.h"

@interface CZNodeTextTest : XCTestCase

@property (nonatomic, retain) CZNodeText *text;

@end
