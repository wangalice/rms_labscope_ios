//
//  CZNodeLineTest.m
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeLineTest.h"
#import "CZNodeLine.h"

@implementation CZNodeLineTest


- (void)setUp {
    [super setUp];
    
    self.p1 = (CGPoint) {.x = 10, .y = 10};
    self.p2 = (CGPoint) {.x = 110, .y = 110};
    self.p3 = (CGPoint) {.x = 120, .y = 120};
    _line = [[CZNodeLine alloc] initWithPoint:self.p1 anotherPoint:self.p2];
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    [_line release];
    
    [super tearDown];
}

- (void)testInit {
    XCTAssertTrue(CGPointEqualToPoint(self.line.p1, self.p1), @"First point should be (10,10).");
    XCTAssertTrue(CGPointEqualToPoint(self.line.p2, self.p2), @"Second point should be (100,110).");
}

- (void)testObserveSelf {
    CGPathRef path = self.line.relativePath;
    path = CGPathRetain(path);
    XCTAssertTrue(path != NULL, @"path should not be nil");
    
    self.line.p1 = self.p3;
    CGPathRef changedPath = self.line.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
    path = CGPathRetain(changedPath);
    
    self.line.p2 = self.p1;
    changedPath = self.line.relativePath;
    XCTAssertTrue(path != changedPath, @"2 pathes should not be same.");
    CGPathRelease(path);
}

- (void)testHittest {
    CGFloat tolerance = 3.0f;
    XCTAssertTrue([self.line hitTest:self.p1 tolerance:tolerance], @"start point should hit on");
    XCTAssertTrue([self.line hitTest:self.p2 tolerance:tolerance], @"end point should hit on");
    
    CGPoint middlePoint = {.x = 20, .y = 22};
    XCTAssertTrue([self.line hitTest:middlePoint tolerance:tolerance], @"should hit on");
    
    CGPoint missPoint = {.x = 20, .y = 30};
    XCTAssertFalse([self.line hitTest:missPoint tolerance:tolerance], @"shouldn't hit on");
    
    CGPoint outsidePoint = {.x = 5, .y = 5};
    XCTAssertFalse([self.line hitTest:outsidePoint tolerance:tolerance], @"shouldn't hit on");
    
    CGPoint outsidePoint2 = {.x = 115, .y = 115};
    XCTAssertFalse([self.line hitTest:outsidePoint2 tolerance:tolerance], @"shouldn't hit on");
}

- (void)testCreateLayer {
    CALayer *layer = [self.line newLayer];
    XCTAssertNotNil(layer, @"layer shall not be nil.");
    
    CGRect expectFrame = CGRectMake(10, 10, 100, 100);
    CGRect expectBounds = CGRectMake(0, 0, 100, 100);
    XCTAssertTrue(CGRectEqualToRect(expectBounds, layer.bounds), @"");
    XCTAssertTrue(CGRectEqualToRect(expectFrame, layer.frame), @"");
    
    [layer release];
}
@end
