//
//  CZDeallocateChecking.h
//  HermesTests
//
//  Created by Ralph Jin on 1/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CZDeallocateChecking<NSObject>

- (void)didDeallocated;

@end
