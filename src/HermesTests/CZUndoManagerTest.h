//
//  CZUndoManagerTest.h
//  HermesTests
//
//  Created by Ralph Jin on 1/22/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>

@class CZUndoManager;

@interface CZUndoManagerTest : XCTestCase

@property (nonatomic, retain) CZUndoManager *undoManager;

@end
