//
//  CZElementRectangleTest.h
//  HermesTests
//
//  Created by Ralph Jin on 1/25/13.
//  Copyright (c) 2013 Ralph Jin. All rights reserved.
//

#import <XCTest/XCTest.h>

@class CZElementRectangle;

@interface CZElementRectangleTest : XCTestCase

@property (nonatomic, readonly) CZElementRectangle *rect;
@end
