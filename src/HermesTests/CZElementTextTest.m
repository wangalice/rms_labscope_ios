//
//  CZElementTextTest.m
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementTextTest.h"
//#import <QuartzCore/QuartzCore.h>

@implementation CZElementTextTest

- (void)setUp {
    [super setUp];
    
    _bound = CGRectMake(4, 8, 10, 12);
    _text = [[CZElementText alloc] initWithFrame:_bound];
}

- (void)tearDown {
    // Tear-down code here.
    [_text release];
    
    [super tearDown];
}

- (void)testProperties {
    CGRect cgRect = _text.frame;
    XCTAssertTrue(CGRectEqualToRect(cgRect, _bound), @"");
}

- (void)testNewLayer {
    CALayer *layer = [_text newLayer];
    XCTAssertNotNil(layer, @"");
    [layer release];
}

- (void)testHitTest {
    CGFloat t = 3.0f;
    CGPoint topLeft = _bound.origin;
    XCTAssertTrue([_text hitTest:topLeft tolerance:t], @"Hit on top left shall be hit on.");
}

- (void)testShapeMemo {
    NSDictionary *memo = [_text newShapeMemo];
    XCTAssertNotNil([memo objectForKey:@"CGRect"], @"CZElementRectangle's memo shall be a CGRect value");
    XCTAssertNotNil([memo objectForKey:@"rotateAngle"], @"CZElementRectangle's memo shall be a CGRect value");
    [memo release];
}

- (void)testRestoreFromMemo {
    CGRect rect = CGRectMake(1, 2, 3, 4);
    NSDictionary *value = @{@"x":@1, @"y": @2, @"w": @3, @"h": @4};
    NSNumber *rotateAngle = [NSNumber numberWithFloat:0.5f];
    NSDictionary *memo = [NSDictionary dictionaryWithObjectsAndKeys:value, @"CGRect", rotateAngle, @"rotateAngle", nil];
    [_text restoreFromShapeMemo:memo];
    
    XCTAssertTrue(CGRectEqualToRect(rect, _text.frame), @"");
    XCTAssertEqualWithAccuracy(0.5f, _text.rotateAngle, 1e-5, @"rotate angle shall be remembered.");
}



@end
