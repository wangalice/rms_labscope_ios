//
//  CZElementTextTest.h
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CZElementText.h"

@interface CZElementTextTest : XCTestCase {
    CGRect _bound;
}

@property (nonatomic, retain) CZElementText *text;

@end
