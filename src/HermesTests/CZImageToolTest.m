//
//  CZImageToolTest.m
//  Hermes
//
//  Created by Halley Gu on 2/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageToolTest.h"
#import "CZUnitTestUtility.h"
#import "CZImageTool.h"
#import "CZDocManager.h"

@interface CZImageToolTest () {
    CZDocManager *_docManager;
    CZImageTool *_imageTool;
}

+ (NSArray *)pixelArrayFromImage:(UIImage *)image;

@end

@implementation CZImageToolTest

- (void)setUp {
    [super setUp];
    
    _docManager = [[CZDocManager alloc] initWithImage:[CZUnitTestUtility testImage]];
    _imageTool = [[CZImageTool alloc] initWithDocManager:_docManager];
    
    [[_imageTool imageView] setFrame:CGRectMake(0, 0, 500, 500)];
}

- (void)tearDown {
    [_imageTool release];
    [_docManager release];
    
    [super tearDown];
}

#pragma mark -
#pragma mark Test Cases

/**
 * Test whether the image rotation methods work as expected.
 */
- (void)testRotation {
    UIImage *source = [[_docManager image] retain];
    
    XCTAssertNotNil(source, @"The test sample image should not be nil.");
    
    NSArray *pixels = [CZImageToolTest pixelArrayFromImage:source];
    CGSize size = [source size];
    
    [_docManager setImage:source];
    [_imageTool rotateClockwise];
    [_imageTool apply];
    UIImage *imageCW = [_docManager image];
    
    XCTAssertNotNil(imageCW, @"The result image should not be nil.");
    
    NSArray *pixelsCW = [CZImageToolTest pixelArrayFromImage:imageCW];
    
    for (NSUInteger x = 0; x < size.width; ++x) {
        for (NSUInteger y = 0; y < size.height; ++y) {
            NSUInteger index1 = y * size.width + x;
            UIColor *color1 = [pixels objectAtIndex:index1];
            
            NSUInteger index2 = x * size.width + size.height - y - 1;
            UIColor *color2 = [pixelsCW objectAtIndex:index2];
            
            XCTAssertEqualObjects(color1, color2, @"The two pixels should be consistent.");
        }
    }
    
    [_docManager setImage:source];
    [_imageTool rotateCounterClockwise];
    [_imageTool apply];
    UIImage *imageCCW = [_docManager image];
    
    XCTAssertNotNil(imageCCW, @"The result image should not be nil.");
    
    NSArray *pixelsCCW = [CZImageToolTest pixelArrayFromImage:imageCCW];
    
    for (NSUInteger x = 0; x < size.width; ++x) {
        for (NSUInteger y = 0; y < size.height; ++y) {
            NSUInteger index1 = y * size.width + x;
            UIColor *color1 = [pixels objectAtIndex:index1];
            
            NSUInteger index2 = (size.width - x - 1) * size.width + y;
            UIColor *color2 = [pixelsCCW objectAtIndex:index2];
            
            XCTAssertEqualObjects(color1, color2, @"The two pixels should be consistent.");
        }
    }
    
    [source release];
}

/**
 * Test whether the image flipping methods work as expected.
 */
- (void)testFlipping {
    UIImage *source = [[_docManager image] retain];
    
    XCTAssertNotNil(source, @"The test sample image should not be nil.");
    
    NSArray *pixels = [CZImageToolTest pixelArrayFromImage:source];
    CGSize size = [source size];
    
    [_docManager setImage:source];
    [_imageTool flipHorizontally];
    [_imageTool apply];
    UIImage *imageFH = [_docManager image];
    
    XCTAssertNotNil(imageFH, @"The result image should not be nil.");
    
    NSArray *pixelsFH = [CZImageToolTest pixelArrayFromImage:imageFH];
    
    for (NSUInteger x = 0; x < size.width; ++x) {
        for (NSUInteger y = 0; y < size.height; ++y) {
            NSUInteger index1 = y * size.width + x;
            UIColor *color1 = [pixels objectAtIndex:index1];
            
            NSUInteger index2 = y * size.width + size.width - x - 1;
            UIColor *color2 = [pixelsFH objectAtIndex:index2];
            
            XCTAssertEqualObjects(color1, color2, @"The two pixels should be consistent.");
        }
    }
    
    [_docManager setImage:source];
    [_imageTool flipVertically];
    [_imageTool apply];
    UIImage *imageFV = [_docManager image];
    
    XCTAssertNotNil(imageFV, @"The result image should not be nil.");
    
    NSArray *pixelsFV = [CZImageToolTest pixelArrayFromImage:imageFV];
    
    for (NSUInteger x = 0; x < size.width; ++x) {
        for (NSUInteger y = 0; y < size.height; ++y) {
            NSUInteger index1 = y * size.width + x;
            UIColor *color1 = [pixels objectAtIndex:index1];
            
            NSUInteger index2 = (size.height - y - 1) * size.width + x;
            UIColor *color2 = [pixelsFV objectAtIndex:index2];
            
            XCTAssertEqualObjects(color1, color2, @"The two pixels should be consistent.");
        }
    }
    
    [source release];
}

/**
 * Test whether the brightness adjustment method works as expected.
 */
- (void)testBrightness {
    UIImage *source = [[_docManager image] retain];
    
    XCTAssertNotNil(source, @"The test sample image should not be nil.");
    
    NSArray *pixelsSource = [CZImageToolTest pixelArrayFromImage:source];
    
    // Test 1: Apply brightness = 0 to the test image. The expected result is
    // that the image stays unchanged.
    
    [_docManager setImage:source];
    [_imageTool setBrightness:0.0];
    [_imageTool apply];
    UIImage *resultTest1 = [_docManager image];
    
    XCTAssertNotNil(resultTest1, @"The result image should not be nil.");
    
    NSArray *pixelsTest1 = [CZImageToolTest pixelArrayFromImage:resultTest1];
    
    for (NSUInteger index = 0; index < [pixelsSource count]; ++index) {
        UIColor *colorSource = [pixelsSource objectAtIndex:index];
        UIColor *colorTest1 = [pixelsTest1 objectAtIndex:index];
        
        XCTAssertEqualObjects(colorSource, colorTest1, @"The two pixels should be consistent.");
    }
    
    // Colors to compare with.
    UIColor *white = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    UIColor *black = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    
    // Test 2: Apply brightness = 100 to the test image. The expected result is
    // that the whole image becomes white.
    [_docManager setImage:source];
    [_imageTool setBrightness:100.0];
    [_imageTool apply];
    UIImage *resultTest2 = [_docManager image];
    
    XCTAssertNotNil(resultTest2, @"The result image should not be nil.");
    
    NSArray *pixelsTest2 = [CZImageToolTest pixelArrayFromImage:resultTest2];
    
    for (NSUInteger index = 0; index < [pixelsTest2 count]; ++index) {
        UIColor *colorTest2 = [pixelsTest2 objectAtIndex:index];
        
        XCTAssertEqualObjects(colorTest2, white, @"The pixel should be white.");
    }
    
    // Test 3: Apply brightness = -100 to the test image. The expected result is
    // that the whole image becomes black.
    [_docManager setImage:source];
    [_imageTool setBrightness:-100.0];
    [_imageTool apply];
    UIImage *resultTest3 = [_docManager image];
    
    XCTAssertNotNil(resultTest3, @"The result image should not be nil.");
    
    NSArray *pixelsTest3 = [CZImageToolTest pixelArrayFromImage:resultTest3];
    
    for (NSUInteger index = 0; index < [pixelsTest3 count]; ++index) {
        UIColor *colorTest3 = [pixelsTest3 objectAtIndex:index];
        
        XCTAssertEqualObjects(colorTest3, black, @"The pixel should be black.");
    }
    
    [source release];
}

/**
 * Test whether the contrast adjustment method works as expected.
 */
- (void)testContrast {
    const CGFloat epsilon = 0.00001f;
    
    UIImage *source = [[_docManager image] retain];
    
    XCTAssertNotNil(source, @"The test sample image should not be nil.");
    
    NSArray *pixelsSource = [CZImageToolTest pixelArrayFromImage:source];
    
    // Test 1: Apply contrast = 0 to the test image. The expected result is that
    // the image stays unchanged.
    [_docManager setImage:source];
    [_imageTool setContrast:0.0];
    [_imageTool apply];
    UIImage *resultTest1 = [_docManager image];
    
    XCTAssertNotNil(resultTest1, @"The result image should not be nil.");
    
    NSArray *pixelsTest1 = [CZImageToolTest pixelArrayFromImage:resultTest1];
    
    for (NSUInteger index = 0; index < [pixelsSource count]; ++index) {
        UIColor *colorSource = [pixelsSource objectAtIndex:index];
        UIColor *colorTest1 = [pixelsTest1 objectAtIndex:index];
        
        XCTAssertEqualObjects(colorSource, colorTest1, @"The two pixels should be consistent.");
    }
    
    // Test 2: Apply contrast = -100 to the test image. The expected result is
    // that the whole image becomes grayscale.
    [_docManager setImage:source];
    [_imageTool setContrast:-100.0];
    [_imageTool apply];
    UIImage *resultTest2 = [_docManager image];
    
    XCTAssertNotNil(resultTest2, @"The result image should not be nil.");
    
    NSArray *pixelsTest2 = [CZImageToolTest pixelArrayFromImage:resultTest2];
    
    for (NSUInteger index = 0; index < [pixelsTest2 count]; ++index) {
        UIColor *colorTest2 = [pixelsTest2 objectAtIndex:index];
        
        CGFloat red, green, blue, alpha;
        BOOL successful = [colorTest2 getRed:&red green:&green blue:&blue alpha:&alpha];
        
        XCTAssertTrue(successful, @"The color space should be RGBA.");
        
        XCTAssertEqualWithAccuracy(red, green, epsilon, @"The pixel should be gray.");
        XCTAssertEqualWithAccuracy(red, blue, epsilon, @"The pixel should be gray.");
    }
    
    [source release];
}

/**
 * Test whether the color intensity adjustment method works as expected.
 */
- (void)testColorIntensity {
    const CGFloat epsilon = 0.00001f;
    
    UIImage *source = [[_docManager image] retain];
    
    XCTAssertNotNil(source, @"The test sample image should not be nil.");
    
    NSArray *pixelsSource = [CZImageToolTest pixelArrayFromImage:source];
    
    // Test 1: Apply saturation = 0 to the test image. The expected result is
    // that the image stays unchanged.
    [_docManager setImage:source];
    [_imageTool setColorIntensity:0.0];
    [_imageTool apply];
    UIImage *resultTest1 = [_docManager image];
    
    XCTAssertNotNil(resultTest1, @"The result image should not be nil.");
    
    NSArray *pixelsTest1 = [CZImageToolTest pixelArrayFromImage:resultTest1];
    
    for (NSUInteger index = 0; index < [pixelsSource count]; ++index) {
        UIColor *colorSource = [pixelsSource objectAtIndex:index];
        UIColor *colorTest1 = [pixelsTest1 objectAtIndex:index];
        
        XCTAssertEqualObjects(colorSource, colorTest1, @"The two pixels should be consistent.");
    }
    
    // Test 2: Apply saturation = 100 to the test image. The expected result is
    // that the whole image becomes grayscale.
    [_docManager setImage:source];
    [_imageTool setColorIntensity:-100.0];
    [_imageTool apply];
    UIImage *resultTest2 = [_docManager image];
    
    XCTAssertNotNil(resultTest2, @"The result image should not be nil.");
    
    NSArray *pixelsTest2 = [CZImageToolTest pixelArrayFromImage:resultTest2];
    
    for (NSUInteger index = 0; index < [pixelsTest2 count]; ++index) {
        UIColor *colorTest2 = [pixelsTest2 objectAtIndex:index];
        
        CGFloat red, green, blue, alpha;
        BOOL successful = [colorTest2 getRed:&red green:&green blue:&blue alpha:&alpha];
        
        XCTAssertTrue(successful, @"The color space should be RGBA.");
        
        XCTAssertEqualWithAccuracy(red, green, epsilon, @"The pixel should be gray.");
        XCTAssertEqualWithAccuracy(red, blue, epsilon, @"The pixel should be gray.");
    }
    
    [source release];
}

#pragma mark -
#pragma mark Helper Methods

/**
 * Gets pixel array from a certain image.
 * Modified based on the code from http://stackoverflow.com/a/1262893/1241690
 *
 * @param image The image need to extract pixels from
 */
+ (NSArray *)pixelArrayFromImage:(UIImage *)image {
    NSMutableArray *result = [NSMutableArray array];
    
    // First get the image into data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * bytesPerPixel,
                                                     sizeof(unsigned char));
    
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now the rawData contains the image data in the RGBA8888 pixel format
    for (int i = 0 ; i < width * height ; ++i) {
        int byteIndex = i * bytesPerPixel;
        CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        
        UIColor *acolor = [UIColor colorWithRed:red
                                          green:green
                                           blue:blue
                                          alpha:alpha];
        [result addObject:acolor];
    }
    
    free(rawData);
    
    return result;
}

@end
