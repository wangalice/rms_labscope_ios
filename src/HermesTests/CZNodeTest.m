//
//  CZNodeTest.m
//  HermesTests
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeTest.h"
#import "CZNode.h"

@implementation CZNodeTest

- (void)setUp {
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testPutInParentNode {
    CZNode *node = [[CZNode alloc] init];
    CZNode *group1 = [[CZNode alloc] init];
    CZNode *group2 = [[CZNode alloc] init];
    
    // put node in group1
    [group1 addSubNode:node];
    XCTAssertFalse([group1 addSubNode:node], @"It can't put a node inside a group twice.");
    XCTAssertEqual(1U, [group1 subNodeCount], @"And there shall be 1 node in the group.");
    
    XCTAssertEqual(group1, node.parentNode, @"The node's parent should exactly be group1.");
    
    // move node to another group.
    [group2 addSubNode:node];
    
    XCTAssertEqual(0U, [group1 subNodeCount], @"group1 should be empty.");
    XCTAssertEqual(group2, node.parentNode, @"The node's parent should exactly be group2.");
    XCTAssertEqual(1U, [group2 subNodeCount], @"And there shall be 1 node in the group2.");
    
    [node release];
    [group1 release];
    [group2 release];
}

- (void)testAddAndRemove {
    CZNode *node = [[CZNode alloc] init];
    CZNode *parent = [[CZNode alloc] init];
    XCTAssertEqual(0U, [parent subNodeCount], @"Parent node should be empty.");
    
    BOOL ok = [parent addSubNode:node];
    XCTAssertTrue(ok, @"Put a node into another node.");
    XCTAssertEqual(1U, [parent subNodeCount], @"Parent node should has 1 node now.");
    
    XCTAssertTrue([node removeFromParent], @"Remove a node from it's parent.");
    XCTAssertNil(node.parentNode, @"The node should no longer has parent.");
    XCTAssertEqual(0U, [parent subNodeCount], @"Parent node should has 1 node now.");
    
    [node release];
    [parent release];
}

- (void)testIllegalAddSubNode {
    CZNode *node = [[CZNode alloc] init];
    CZNode *parent = [[CZNode alloc] init];
    
    XCTAssertFalse([node addSubNode:node], @"Sub node can't add itself.");
    
    [parent addSubNode:node];
    XCTAssertFalse([node addSubNode:parent], @"Sub node can't add its parent node.");
    
    [node release];
    [parent release];
}

@end
