//
//  CZDragToolTest.m
//  Hermes
//
//  Created by Ralph Jin on 2/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDragToolTest.h"
#import "CZUnitTestUtility.h"
#import "CZDragTool.h"
#import "CZDocManager.h"
#import "CZElementLayer.h"
#import "CZElementRectangle.h"

@implementation CZDragToolTest

- (void)testSelectAnother {
    CZDocManager *document = [[CZDocManager alloc] initWithImage:[CZUnitTestUtility testImage]];
    CZDragTool *dragTool = [[CZDragTool alloc] initWithDocManager:document];
    dragTool.zoomOfImage = 1.0f;
    CZElementRectangle *rect = [[CZElementRectangle alloc] initWithRect:CGRectMake(0, 0, 100, 100)];
    [document.elementLayer addElement:rect];
    [rect release];
    
    BOOL handled = [dragTool touchBegin:CGPointMake(10, 10)];
    XCTAssertTrue(handled, @"drag shall begin.");
    
    [dragTool touchMoved:CGPointMake(60, 50)];
    XCTAssertEqual(50.0f, rect.rect.origin.x, @"");
    XCTAssertEqual(40.0f, rect.rect.origin.y, @"");

    [dragTool release];
    [document release];
}

@end
