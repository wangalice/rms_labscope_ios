//
//  GemMeasurerClrTest.mm
//  Hermes
//
//  Created by Ralph Jin on 8/19/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "GemMeasurerClrTest.h"
#import "GemMeasurerClr.h"
#import "GemChords.h"

using namespace GemMeasurerClr;

@implementation GemMeasurerClrTest

- (void)testTriangle {
    GemMeasurer *measurer = new GemMeasurer;
    
    int pointsCount = 5;
	double *pRectPointsCoordinates = new double[pointsCount * 2];
	pRectPointsCoordinates[0] = 150.4;
	pRectPointsCoordinates[1] = 150.7;
    
	pRectPointsCoordinates[2] = 300.5;
	pRectPointsCoordinates[3] = 200.6;
    
	pRectPointsCoordinates[4] = 150.3;
	pRectPointsCoordinates[5] = 250.5;
    
	pRectPointsCoordinates[6] = 100.2;
	pRectPointsCoordinates[7] = 200.9;
    
	pRectPointsCoordinates[8] = 150.4;
	pRectPointsCoordinates[9] = 150.7;
	
	measurer->SetRegion(pRectPointsCoordinates, pointsCount);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    XCTAssertTrue(area > 0, @"");
    XCTAssertTrue(perimeter > 0, @"");
    
    delete[] pRectPointsCoordinates;
    delete measurer;
}

- (void)testCircle {
    GemMeasurer *measurer = new GemMeasurer;
    
    double centerX = 101.9;
	double centerY = 202.4;
	double radius = 101.5;
	
	double* pPolygonPoints;
	long count = GemMeasurer::ConvertCircleToPolygon(centerX, centerY, radius, &pPolygonPoints);
	measurer->SetRegion(pPolygonPoints, count);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    delete[] pPolygonPoints;
    
    double expectArea = M_PI * radius * radius;
    double expectPerimeter = M_PI * radius * 2;
    XCTAssertEqualWithAccuracy(area, expectArea, 300, @"");
    XCTAssertEqualWithAccuracy(perimeter, expectPerimeter, 100, @"");
    
    delete measurer;
}

- (void)testLargeCircle {
    GemMeasurer *measurer = new GemMeasurer;
    
    double centerX = 0;
	double centerY = 0;
	double radius = 300000.5;
	
	double* pPolygonPoints;
	long count = GemMeasurer::ConvertCircleToPolygon(centerX, centerY, radius, &pPolygonPoints);
	measurer->SetRegion(pPolygonPoints, count);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    delete[] pPolygonPoints;
    
    XCTAssertTrue(area > 0.0, @"");
    XCTAssertTrue(perimeter > 0.0, @"");
    
    delete measurer;
}

- (void)testSqaure {
    GemMeasurer *measurer = new GemMeasurer;
    
    double width = 100;
    double left = 0;
    
    int pointsCount = 5;
	double *pRectPointsCoordinates = new double[pointsCount * 2];
	pRectPointsCoordinates[0] = left;
	pRectPointsCoordinates[1] = left;
    
	pRectPointsCoordinates[2] = width;
	pRectPointsCoordinates[3] = left;
    
	pRectPointsCoordinates[4] = width;
	pRectPointsCoordinates[5] = width;
    
	pRectPointsCoordinates[6] = left;
	pRectPointsCoordinates[7] = width;
    
	pRectPointsCoordinates[8] = pRectPointsCoordinates[0];
	pRectPointsCoordinates[9] = pRectPointsCoordinates[1];
	
	measurer->SetRegion(pRectPointsCoordinates, pointsCount);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    XCTAssertEqualWithAccuracy(area, width * width, 210, @"");
    XCTAssertEqualWithAccuracy(perimeter, width * 4, 3, @"");
    
    delete[] pRectPointsCoordinates;
    delete measurer;
}

- (void)testLargeSqaure {
    GemMeasurer *measurer = new GemMeasurer;
    
    double width = 30000;
    double left = 0;
    
    int pointsCount = 5;
	double *pRectPointsCoordinates = new double[pointsCount * 2];
	pRectPointsCoordinates[0] = left;
	pRectPointsCoordinates[1] = left;
    
	pRectPointsCoordinates[2] = width;
	pRectPointsCoordinates[3] = left;
    
	pRectPointsCoordinates[4] = width;
	pRectPointsCoordinates[5] = width;
    
	pRectPointsCoordinates[6] = left;
	pRectPointsCoordinates[7] = width;
    
	pRectPointsCoordinates[8] = pRectPointsCoordinates[0];
	pRectPointsCoordinates[9] = pRectPointsCoordinates[1];
	
	measurer->SetRegion(pRectPointsCoordinates, pointsCount);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    XCTAssertTrue(area > 0, @"");
    XCTAssertTrue(perimeter > 0, @"");
    
    delete[] pRectPointsCoordinates;
    delete measurer;
}

- (void)testThinPolygon {
    GemMeasurer *measurer = new GemMeasurer;
    
    int pointsCount = 5;
	double *pRectPointsCoordinates = new double[pointsCount * 2];
	pRectPointsCoordinates[0] = 0;
	pRectPointsCoordinates[1] = 0;
    
	pRectPointsCoordinates[2] = 30;
	pRectPointsCoordinates[3] = 0;
    
	pRectPointsCoordinates[4] = 20;
	pRectPointsCoordinates[5] = 0;
    
	pRectPointsCoordinates[6] = 40;
	pRectPointsCoordinates[7] = 0;
    
	pRectPointsCoordinates[8] = pRectPointsCoordinates[0];
	pRectPointsCoordinates[9] = pRectPointsCoordinates[1];
	
	measurer->SetRegion(pRectPointsCoordinates, pointsCount);
	double area = measurer->Area();
	double perimeter = measurer->Perimeter();
    
    XCTAssertTrue(area >= 0.0, @"");
    XCTAssertTrue(perimeter >= 0.0, @"");
    
    delete[] pRectPointsCoordinates;
    delete measurer;
}

- (void)assertChords:(GemChords *)chords from:(int)from end:(int)end {
    XCTAssertFalse(chords->GetPoint(from - 1, 0), @"");
    XCTAssertFalse(chords->GetPoint(end, 0), @"");
    XCTAssertTrue(chords->GetPoint(from, 0), @"");
    XCTAssertTrue(chords->GetPoint((from + end) / 2, 0), @"");
    XCTAssertTrue(chords->GetPoint(end - 1, 0), @"");
}

- (void)testAddPoint {
    GemChords *chords = new GemChords(10, 1);
    chords->AddPoint(0, 0);
    
    XCTAssertTrue(chords->GetPoint(0, 0), @"");
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    
    chords->AddPoint(2, 0);
    
    XCTAssertTrue(chords->GetPoint(2, 0), @"");
    XCTAssertEqual(chords->GetChordCount(0), 2UL, @"");
    
    chords->AddPoint(1, 0);
    XCTAssertTrue(chords->GetPoint(1, 0), @"");
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    [self assertChords:chords from:0 end:3];
    
    delete chords;
}

- (void)testAddChord {
    GemChords *chords = new GemChords(10, 1);
    chords->AddChord(0, 10, 0);
    
    [self assertChords:chords from:0 end:10];
    
    delete chords;
}

- (void)testChordMergeTwoSegment {
    GemChords *chords = new GemChords(100, 1);

    chords->AddChord(30, 40, 0);
    chords->AddChord(60, 80, 0);
    chords->AddChord(90, 100, 0);
    chords->AddChord(0, 10, 0);

    [self assertChords:chords from:0 end:10];
    [self assertChords:chords from:30 end:40];
    [self assertChords:chords from:60 end:80];
    [self assertChords:chords from:90 end:100];
    XCTAssertEqual(chords->GetChordCount(0), 4UL, @"");
    
    chords->AddPoint(10, 0);
    [self assertChords:chords from:0 end:11];
    XCTAssertEqual(chords->GetChordCount(0), 4UL, @"");

    chords->AddChord(35, 85, 0);
    [self assertChords:chords from:0 end:11];
    [self assertChords:chords from:30 end:85];
    [self assertChords:chords from:90 end:100];
    XCTAssertEqual(chords->GetChordCount(0), 3UL, @"");
    
    delete chords;
}

- (void)testChordMergeLeft {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    
    chords->AddChord(5, 10, 0);
    [self assertChords:chords from:5 end:20];
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    
    delete chords;
}

- (void)testChordMergeFarLeft {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    
    chords->AddChord(0, 5, 0);

    [self assertChords:chords from:0 end:5];
    [self assertChords:chords from:10 end:20];
    XCTAssertEqual(chords->GetChordCount(0), 2UL, @"");
    
    delete chords;
}

- (void)testChordMergeInset {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    
    chords->AddChord(11, 19, 0);
    [self assertChords:chords from:10 end:20];
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    
    delete chords;
}

- (void)testChordMergeCover {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    
    chords->AddChord(5, 25, 0);
    [self assertChords:chords from:5 end:25];
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    
    delete chords;
}

- (void)testChordMergeRight {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    
    chords->AddChord(20, 25, 0);
    [self assertChords:chords from:10 end:25];
    XCTAssertEqual(chords->GetChordCount(0), 1UL, @"");
    
    delete chords;
}

- (void)testChordMergeFarRight {
    GemChords *chords = new GemChords(100, 1);
    
    chords->AddChord(10, 20, 0);
    chords->AddChord(30, 40, 0);
    
    [self assertChords:chords from:10 end:20];
    [self assertChords:chords from:30 end:40];
    XCTAssertEqual(chords->GetChordCount(0), 2UL, @"");
    
    delete chords;
}


- (void)testChordFindLeftX {
    GemChords *chords = new GemChords(100, 1);
    
    XCTAssertEqual(-1L, chords->FindLeftXOnChord(50, 0, true), @"");
    XCTAssertEqual(50L, chords->FindLeftXOnChord(50, 0, false), @"");
    
    chords->AddChord(10, 20, 0);
    chords->AddChord(30, 40, 0);
    
    // test 1
    XCTAssertEqual(-1L, chords->FindLeftXOnChord(9, 0, true), @"");
    XCTAssertEqual(10L, chords->FindLeftXOnChord(10, 0, true), @"");
    XCTAssertEqual(15L, chords->FindLeftXOnChord(15, 0, true), @"");
    XCTAssertEqual(19L, chords->FindLeftXOnChord(19, 0, true), @"");
    
    XCTAssertEqual(19L, chords->FindLeftXOnChord(20, 0, true), @"");
    XCTAssertEqual(19L, chords->FindLeftXOnChord(25, 0, true), @"");
    XCTAssertEqual(19L, chords->FindLeftXOnChord(29, 0, true), @"");
    
    XCTAssertEqual(30L, chords->FindLeftXOnChord(30, 0, true), @"");
    XCTAssertEqual(35L, chords->FindLeftXOnChord(35, 0, true), @"");
    XCTAssertEqual(39L, chords->FindLeftXOnChord(39, 0, true), @"");
    
    XCTAssertEqual(39L, chords->FindLeftXOnChord(40, 0, true), @"");
    XCTAssertEqual(39L, chords->FindLeftXOnChord(50, 0, true), @"");
    
    // test 0
    XCTAssertEqual(9L, chords->FindLeftXOnChord(9, 0, false), @"");
    XCTAssertEqual(9L, chords->FindLeftXOnChord(10, 0, false), @"");
    XCTAssertEqual(9L, chords->FindLeftXOnChord(15, 0, false), @"");
    XCTAssertEqual(9L, chords->FindLeftXOnChord(19, 0, false), @"");
    
    XCTAssertEqual(20L, chords->FindLeftXOnChord(20, 0, false), @"");
    XCTAssertEqual(25L, chords->FindLeftXOnChord(25, 0, false), @"");
    XCTAssertEqual(29L, chords->FindLeftXOnChord(29, 0, false), @"");
    
    XCTAssertEqual(29L, chords->FindLeftXOnChord(30, 0, false), @"");
    XCTAssertEqual(29L, chords->FindLeftXOnChord(35, 0, false), @"");
    XCTAssertEqual(29L, chords->FindLeftXOnChord(39, 0, false), @"");
    
    XCTAssertEqual(40L, chords->FindLeftXOnChord(40, 0, false), @"");
    XCTAssertEqual(50L, chords->FindLeftXOnChord(50, 0, false), @"");
    
    delete chords;
    
    chords = new GemChords(100, 1);
    chords->AddChord(0, 100, 0);
    XCTAssertEqual(50L, chords->FindLeftXOnChord(50, 0, true), @"");
    XCTAssertEqual(-1L, chords->FindLeftXOnChord(50, 0, false), @"");
}

- (void)testPackedCracks {
    PackedCracks cracks;
    XCTAssertTrue(cracks.begin() == cracks.end(), @"");
    XCTAssertEqual(0UL, cracks.size(), @"");
    
    cracks.push_back(CRACK_R);
    cracks.push_back(CRACK_U);
    XCTAssertEqual(2UL, cracks.size(), @"");
    cracks.push_back(CRACK_L);
    cracks.push_back(CRACK_D);
    XCTAssertEqual(4UL, cracks.size(), @"");
    
    PackedCracks::iterator b = cracks.begin();
    PackedCracks::iterator e = cracks.end();
    
    XCTAssertTrue(b != e, @"");
    
    XCTAssertEqual(*b, (PackedCracks::type_t)CRACK_R, @"");
    ++b;
    
    XCTAssertEqual(*b, (PackedCracks::type_t)CRACK_U, @"");
    ++b;
    
    XCTAssertEqual(*b, (PackedCracks::type_t)CRACK_L, @"");
    ++b;
    
    XCTAssertEqual(*b, (PackedCracks::type_t)CRACK_D, @"");
    ++b;
    
    XCTAssertEqual(b, e, @"");
}

- (void)testPackedCracksIteratorEnd {
    int counts [] = {0, 3, 4, 6, 8, 4096*4, 4096*4 + 7, 4096 * 8};
    for (int j = 0; j < 7; ++j) {
        int count = counts[j];
        PackedCracks cracks;
        for (int i = 0; i < count; i ++) {
            cracks.push_back(0);
        }
        
        PackedCracks::Iterator it = cracks.begin();
        for (int i = 0; i < count; i ++) {
            ++it;
        }
        
        XCTAssertEqual(it, cracks.end(), @"");
    }
}

- (void)testPackedCracksClear {
    PackedCracks cracks;
    cracks.clear();
    
    XCTAssertEqual(cracks.size(), 0UL, @"");
    XCTAssertTrue(cracks.begin() == cracks.end(), @"");
    
    for (int i = 0; i < 100; i++) {
        cracks.push_back(0);
    }
    cracks.clear();
    
    XCTAssertEqual(cracks.size(), 0UL, @"");
    XCTAssertTrue(cracks.begin() == cracks.end(), @"");
}

@end
