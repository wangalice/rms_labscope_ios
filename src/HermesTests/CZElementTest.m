//
//  CZElementTest.m
//  HermesTests
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementTest.h"
#import "CZElement.h"
#import "CZElementLayer.h"

@implementation CZElementTest

- (void)testPutInParent {
    // Test put int a node into a parent node.
    CZElement *element = [[CZElement alloc] init];
    CZElementLayer *layer1 = [[CZElementLayer alloc] init];
    CZElementLayer *layer2 = [[CZElementLayer alloc] init];
    
    [layer1 addElement:element];
    
    XCTAssertEqual(layer1, element.parent, @"The element's parent should exactly be layer1.");
    
    // Move element to another layer.
    [layer2 addElement:element];
    
    XCTAssertEqual(0U, [layer1 elementCount], @"layer1 should be empty");
    XCTAssertEqual(layer2, element.parent, @"The element's parent should exactly be layer2, now.");
}

@end
