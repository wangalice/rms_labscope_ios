//
//  CZNodeLineTest.h
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <QuartzCore/QuartzCore.h>

@class CZNodeLine;

@interface CZNodeLineTest : XCTestCase

@property(nonatomic) CGPoint p1;
@property(nonatomic) CGPoint p2;
@property(nonatomic) CGPoint p3;

@property(nonatomic, retain) CZNodeLine *line;

@end
