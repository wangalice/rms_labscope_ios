//
//  CZUnitTestUtility.h
//  Hermes
//
//  Created by Halley Gu on 2/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZUnitTestUtility : NSObject

+ (UIImage *)testImage;

@end
