//
//  CZAppDelegate.m
//  Hermes
//
//  Created by Mike Wang on 1/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAppDelegate.h"
#import "CZFileCommonMarco.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import "CZReportTemplateFilesManager.h"

@interface CZAppDelegate () <UINavigationControllerDelegate>

@property (nonatomic, strong) CZNavigationController *navigationController;
@property (nonatomic, strong) CZMultitaskViewController *multitaskViewController;

@end

@implementation CZAppDelegate

@synthesize window;

+ (CZAppDelegate *)sharedDelegate {
    return (CZAppDelegate *)UIApplication.sharedApplication.delegate;
}

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [CZCrashHandler installExceptionHandler];
    
    [[CZWifiNotifier sharedInstance] startNotifier];
    
    [self initialConfiguration];
    
    [self registerDefaults];
    
    CZElementUnitStyle unitStyle = [[CZDefaultSettings sharedInstance] unitStyle];
    [CZElement setUnitStyle:unitStyle];
    
    self.multitaskViewController = [[CZMultitaskViewController alloc] init];
    self.navigationController = [[CZNavigationController alloc] initWithRootViewController:self.multitaskViewController];
    self.navigationController.delegate = self;
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    window.rootViewController = self.navigationController;
    [window makeKeyAndVisible];
    self.window = window;
    
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    CZLogv(@"app did become active");
    CZLogv(@"sand box path:%@", NSHomeDirectory());
    
    [[CZMNABrowser sharedInstance] beginBrowsing];
    
    static BOOL firstLaunch = YES;
    if (firstLaunch) {
        firstLaunch = NO;
    } else {
#if DCM_REFACTORED
        if ([CZDCMManager defaultManager].hasConnectedToDCMServer || [CZDCMManager defaultManager].state != CMConnectStateDisConnect) {
            //TODO:SG Add DCM Server Service connection logic.
        } else {
#endif
            if (![[CZCameraBrowser sharedInstance] isBrowsing]) {
                [[CZCameraBrowser sharedInstance] beginBrowsing];
            }
#if MIC_REFACTORED
            [self.tabBar checkMultiMicroscopesMode];
#endif
            if ([self.multitaskViewController.multitaskManager.primaryTask.identifier isEqualToString:CZTaskIdentifierMicroscopes]) {
                [[CZCameraBrowser sharedInstance] setThumbnailRefreshingEnabled:YES];
            }
#if DCM_REFACTORED
        }
#endif
    }
    
#if GLOBAL_SETTINGS_REFACTORED
    // check information screen enable/disable setting
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    CZInformationScreenOption infoScreenOption = [[CZDefaultSettings sharedInstance] infoScreenOption];
    if (infoScreenOption == CZInformationScreenOptionDisableAll) {
        for (NSString *key in kHideInfoScreenKeyArray) {
            [userDefaults setBool:YES forKey:key];
        }
        [[CZHelpOverlayView current] dismiss];
    } else if (infoScreenOption == CZInformationScreenOptionEnableAll) {
        for (NSString *key in kHideInfoScreenKeyArray) {
            [userDefaults setBool:NO forKey:key];
        }
    }
    
    if (self.tabBar.activatedControllerIndex == kCZFileViewController) {
        CZFileViewController *fileViewController = _tabBar.tabViewControllers[kCZFileViewController];
        [fileViewController updateUIStatus];
    }
    
    CZImageViewController *imageViewController = _tabBar.tabViewControllers[kCZImageViewController];
    [imageViewController updateAfterUnitChanged];
    [imageViewController updateAfterFileOverwriteOptionChanged];
    
    // update virtual camera appearance
    CZMicroscopeViewController *microscopeViewController = _tabBar.tabViewControllers[kCZMicroscopeController];
    [microscopeViewController updateVirtualCameraAppearance];
    [microscopeViewController updatePlusCameraAppearance];
    [microscopeViewController refreshNotFoundLabel];
#endif
}

- (void)applicationWillResignActive:(UIApplication *)application {
    CZLogv(@"app will resign active");
    
    [[CZMNABrowser sharedInstance] stopBrowsing];
    
    [[CZCameraBrowser sharedInstance] setThumbnailRefreshingEnabled:NO];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[CZWifiNotifier sharedInstance] stopNotifier];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
#if LIVE_REFACTORED
    [self waitTimeLapse];
#endif
    
#if GLOBAL_SETTINGS_REFACTORED
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if ([[CZDefaultSettings sharedInstance] enableLog]) {
        fclose(_logFileHandler);
    }
#endif
    
    [MagicalRecord cleanUp];
    
#if DCM_REFACTORED
    if ([CZDCMManager defaultManager].isCMServerConnected) {
        [[CZDCMManager defaultManager] sendLogoutCommandToClientService];
        [[CZDCMManager defaultManager] disconnectToCMServer];
        [[CZDCMCameraManager sharedInstance] dcmStopBrowsing];
    }
#endif
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
#if DCM_REFACTORED
    //FIXME:SG Add DCM disconnect logic
    if ([CZDCMManager defaultManager].isCMServerConnected) {
        [[CZDCMCameraManager sharedInstance] dcmStopBrowsing];
        [[CZDCMManager defaultManager] setShouldSyncMonitorStates:YES];
    }
#endif
    
    CZLiveTask *liveTask = self.multitaskViewController.multitaskManager.liveTask;
    CZCamera *currentCamera = liveTask.currentCamera;
    // NOTICE: set delegate to nil, ignore coming video frames,
    // so that no UI refreshing after app enter background.
    currentCamera.delegate = nil;
    [currentCamera stop];
    
    __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    void(^expireHandler)() = ^{
        if (bgTask != UIBackgroundTaskInvalid) {
            [application endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    };
    bgTask = [application beginBackgroundTaskWithName:@"StopCameraBrowsing" expirationHandler:expireHandler];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[CZCameraBrowser sharedInstance] stopBrowsing];
        if (bgTask != UIBackgroundTaskInvalid) {
            [application endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    });
    
    [[NSUserDefaults standardUserDefaults] synchronize];

#if LIVE_REFACTORED
    [self waitTimeLapse];
#endif
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Force to update wifi name
    [[CZWifiNotifier sharedInstance] postNotification];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
#if DCM_REFACTORED
    //FIXME: Add DCM re-connect logic
    if ([[CZDCMManager defaultManager] isNeedSyncMonitorStates]) {
        [[CZDCMManager defaultManager] reconnectToCMServer];
        [[CZDCMCameraManager sharedInstance] dcmBeginBrowsing];
    }
    
    //Refresh 5th Tab
    if (_tabBar.activatedControllerIndex == kCZCMServiceControlController) {
        CZCMServiceControlViewController * serviceControlController = _tabBar.tabViewControllers[kCZCMServiceControlController];
        [serviceControlController reloadWebPage];
    }
#endif
    
    // reconnect camera in live view
    CZLiveTask *liveTask = self.multitaskViewController.multitaskManager.liveTask;
    CZCamera *camera = liveTask.currentCamera;
    [liveTask setCurrentCamera:nil resetExclusiveLock:YES];
    [liveTask setCurrentCamera:camera resetExclusiveLock:YES];
    
#if LIVE_REFACTORED
    //FIXME: SG when application state changed from background, we need to update the element layer to ignore element scale chaged.
    [liveViewController updateElementLayerLogicalScaling];
    
    [_tabBar showFullScreen:NO]; // Requirement, dismiss fullscreen mode when re-enter app.
#endif
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
#if FILE_REFACTOR
    if (_tabBar) {
        return [_tabBar supportedInterfaceOrientations];
    } else {
        return UIInterfaceOrientationMaskLandscape;
    }
#endif
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark - Private methods

- (void)initialConfiguration {
    // Setup Core Data backend with MagicalRecord.
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"HermesData.sqlite"];
    // Copy the report templates to documents direcotry
    [CZReportTemplateFilesManager copyDefaultTemplateFiles:@"czrtj" useLocal:YES];
}

- (void)registerDefaults {
    NSDictionary *defaults = @{
        kGlobalModelVersion : @(kGlobalModelVersionNumber),
        kMicroscopeModelVersion : @(kMicroscopeModelVersionNumber),
        kFactoryResetOnNextLaunch : @NO,
        kShowLaserPointer : @NO,
        kOperatorName : @"",
        kStayInLiveTabAfterSnapping : @NO,
        kCZShortcutMode : @"0",
        kShowVirtualMicroscopes: @"0",
        kEnableSingleMicroscopeMode : @NO,
        kDefaultColor : @"0",
        kDefaultBackGroundColor : @"0",
        kDefaultSize : @"2",
        kDefaultScaleBarCorner : @"0",
        kServerURL : @"192.168.0.100",
        kServerSharePath : @"Network share/Folder/Some sub-folder",
        kDefaultFileFormat : @"0",
        kOverwriteImageOnSave : @"0",
        kMeasurementUnit : @"4",
        kMeasurementEnabled : @YES,
        kMeasurementColorCodingDisabled : @NO,
        kTimelapseVideoPalybackRate:@"1",
        kTimelapseVideoTimeInterval:@"1",
        kShareMail : @YES,
        kShareSocial : @YES,
        kEnableLog : @NO,
        kDisableMacAddressFilter : @NO,
        kFirstTimeCopyDemoFiles : @YES,
        kCZLocalFileFilterKey : [NSString stringWithFormat:@"%lu", (unsigned long)CZFileFilterTypeAll],
        kCZRemoteFileFilterKey : [NSString stringWithFormat:@"%lu", (unsigned long)CZFileFilterTypeAll],
        kNumOfCamerasPerPage : @"6",
        kEnableMNALevelSimulator : @NO,
        kMNALevel : @"MNALevelNone",
        kEnableNonReleasedMicroscopes: @NO,
        kDefaultGrainsStandard: @0,
        kDefaultGrainsPattern: @0,
        kCZFilesDisplayViewMode: @"0",
        kDefaultLocalThumbnailGridViewZoomLevel : @"1.0",
        kDefaultRemoteThumbnailGridViewZoomLevel: @"1.0",
        kManuallyAddMicroscopes: @NO,
        kFixedSharedFolder: @YES,
        kDigitalClassroomHost : @"",
        kEnableMacroSnap : @YES
    };
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (viewController == self.multitaskViewController) {
        [navigationController setNavigationBarHidden:YES animated:animated];
    } else {
        [navigationController setNavigationBarHidden:NO animated:animated];
    }
}

@end
