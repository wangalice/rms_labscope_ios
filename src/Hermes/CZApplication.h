//
//  CZApplication.h
//  Hermes
//
//  Created by Li, Junlin on 3/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZApplication : UIApplication

@property (class, nonatomic, readonly) CZApplication *sharedApplication;

@property (nonatomic, readonly, strong) UIEvent *latestEvent;
@property (nonatomic, readonly, strong) NSDate  *latestEventSendDate;

@end
