//
//  CZImageEditingViewController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZImageViewEditingDelegate.h"

@class CZDocManager, CZImagePageViewController, CZSelectTool;
@protocol CZImageEditingControl;

@protocol CZImageEditingViewControllerDataSource <NSObject>
@required
- (CZDocManager *)editingImageDocument;

- (UIView *)editingImageView;

/** the image view to editing. */
- (UIView *)imageView;

@optional

/** the parent scroll view of image view.*/
- (UIScrollView *)imageScrollView;

/** in case that image view is not inside a scroll view, use this to fetch zoom scale. */
- (CGFloat)imageViewZoomScale;

- (id<CZMagnifyDelegate>)magnifyDelegate;

@end

@protocol CZImageEditingViewControllerDelegate <NSObject>

@required
/** @return actrual mode after switching. */
- (NSUInteger)viewControllerBeginCropping:(UIViewController *)viewController;

@optional

- (void)viewControllerDidBeginEditing:(UIViewController *)viewController;
- (void)viewControllerDidEndEditing:(UIViewController *)viewController;
- (void)viewControllerDidModifyDocManager:(UIViewController *)viewController;

/**
 * @param rect, rect in image view's coordinate.
 */
- (void)scrollRectToVisible:(CGRect)rect;

@end

/** Abstract class of image editing view controller. */
@interface CZImageEditingViewController : UIViewController <CZSelectToolDelegate, CZImageViewEditingDelegate>

@property (nonatomic, weak) id<CZImageEditingViewControllerDelegate> delegate;
@property (nonatomic, weak) id<CZImageEditingViewControllerDataSource> dataSource;
@property (nonatomic, readonly, strong) UIView *editingImageView;

// helper functions
- (CZDocManager *)editingImageDocument;

- (UIView *)imageView;
- (UIScrollView *)imageScrollView;

- (CGFloat)imageViewZoomScale;

- (id<CZMagnifyDelegate>)magnifyDelegate;

/** Sub-class can override this and it's safe not to call super.*/
- (void)updateAfterUnitChanged;

@end
