//
//  CZAnnotationConfigPanelViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnnotationMarco.h"
#import <CZAnnotationKit/CZColor.h>

NS_ASSUME_NONNULL_BEGIN

@class CZElement;
@interface CZAnnotationConfigPanelViewModel : NSObject

@property (nonatomic, assign, getter=isMeasurementShowControlAvailable) BOOL measurementShowControlAvailable;
@property (nonatomic, assign, getter=isBackgroundColorControlAvailable) BOOL backgroundColorControlAvailable;
@property (nonatomic, assign, getter=isRectangleScalingControlAvailable) BOOL rectangleScalingControlAvailable;
@property (nonatomic, assign, getter=isDefaultColorAvailable) BOOL defaultColorAvailable;

- (void)updateSelectionElement:(CZElement *)selectionElement;

- (CZColor)defaultColorAfterChangingBackgroundColor:(CZColor)backgroundColor
                                        originalForegroundColor:(CZColor)originalForegroundColor;

@end

NS_ASSUME_NONNULL_END
