//
//  CZElement+Info.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZAnnotationKit/CZAnnotationKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZElement (Info)

- (NSString *)elementTypeName;

- (BOOL)isMeasurementInfoAvailable;

@end

NS_ASSUME_NONNULL_END
