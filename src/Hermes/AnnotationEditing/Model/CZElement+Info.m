//
//  CZElement+Info.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZElement+Info.h"

@implementation CZElement (Info)

- (NSString *)elementTypeName {
    if ([self isKindOfClass:[CZElementAngle class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTANGLE");
    } else if ([self isKindOfClass:[CZElementArrow class]]) {
        return L(@"ARROW");
    } else if ([self isKindOfClass:[CZElementCircle class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTCIRCLE");
    } else if ([self isKindOfClass:[CZElementCount class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTCOUNT");
    } else if ([self isKindOfClass:[CZElementDisconnectedAngle class]]) {
        return L(@"DISCONNECTED_ANGLE");
    } else if ([self isKindOfClass:[CZElementCalipers class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTCALIPERS");
    } else if ([self isKindOfClass:[CZElementLine class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTLINE");
    } else if ([self isKindOfClass:[CZElementMultiCalipers class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTMULTICALIPERS");
    } else if ([self isKindOfClass:[CZElementPolygon class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTPOLYGON");
    } else if ([self isKindOfClass:[CZElementPolyline class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTPOLYLINE");
    } else if ([self isKindOfClass:[CZElementRectangle class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTRECTANGLE");
    } else if ([self isKindOfClass:[CZElementScaleBar class]]) {
        return L(@"SCALEBAR");
    } else if ([self isKindOfClass:[CZElementSpline class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTSPLINE");
    } else if ([self isKindOfClass:[CZElementSplineContour class]]) {
        return L(@"ANNOTATION_TYPE_CZELEMENTSPLINECONTOUR");
    } else if ([self isKindOfClass:[CZElementText class]]) {
        return L(@"TEXT");
    }
    
    return nil;
}

- (BOOL)isMeasurementInfoAvailable {
    if ([self isKindOfClass:[CZElementAngle class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementCircle class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementDisconnectedAngle class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementDistance class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementLine class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementMultiCalipers class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementPolygon class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementPolyline class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementRectangle class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementSpline class]]) {
        return YES;
    } else if ([self isKindOfClass:[CZElementSplineContour class]]) {
        return YES;
    }
    
    return NO;
}

@end
