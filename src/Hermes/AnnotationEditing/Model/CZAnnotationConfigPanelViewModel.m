//
//  CZAnnotationConfigPanelViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationConfigPanelViewModel.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZElement+Info.h"

@interface CZAnnotationConfigPanelViewModel ()

@property (nonatomic, readonly, strong) CZElement *selectionElement;

@end

@implementation CZAnnotationConfigPanelViewModel

- (instancetype)init {
    if (self = [super init]) {
        _selectionElement = nil;
    }
    return self;
}

- (void)dealloc {
    _selectionElement = nil;
}

#pragma mark - Public method

- (BOOL)isMeasurementShowControlAvailable {
    return [self.selectionElement isMeasurementInfoAvailable];
}

- (BOOL)isBackgroundColorControlAvailable {
    return YES;
}

- (BOOL)isRectangleScalingControlAvailable {
    if ([self.selectionElement isKindOfClass:[CZElementRectangle class]]) {
        return YES;
    }
    return NO;
}

- (BOOL)isDefaultColorAvailable {
    return YES;
}

- (void)updateSelectionElement:(CZElement *)selectionElement {
    _selectionElement = selectionElement;
}

- (CZColor)defaultColorAfterChangingBackgroundColor:(CZColor)backgroundColor originalForegroundColor:(CZColor)originalForegroundColor {
    CZColor foregroundColor = kCZColorRed;

    if (CZColorEqualToColor(originalForegroundColor, backgroundColor)) {
        if (CZColorEqualToColor(backgroundColor, kCZColorRed)) {
            foregroundColor = kCZColorBlue;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorBlue)) {
            foregroundColor = kCZColorGreen;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorWhite)) {
            foregroundColor = kCZColorBlack;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorGreen)) {
            foregroundColor = kCZColorBlack;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorBlack)) {
            foregroundColor = kCZColorRed;
        }
    } else {
        if (CZColorEqualToColor(backgroundColor, kCZColorWhite) && CZColorEqualToColor(originalForegroundColor, kCZColorYellow)) {
            foregroundColor = kCZColorBlack;
        } else {
            foregroundColor = originalForegroundColor;
        }
    }
    
    return foregroundColor;
}

@end
