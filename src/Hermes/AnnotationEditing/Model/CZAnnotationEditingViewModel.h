//
//  CZAnnotationEditingViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/17.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnnotationMarco.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZAnnotationEditingViewModel : NSObject

- (CZAnnotationConfigPanelControllerPosition)configPanelControllerPosition;

@end

NS_ASSUME_NONNULL_END
