//
//  CZAnnotationEditingViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/17.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationEditingViewModel.h"
#import <CZAnnotationKit/CZAnnotationKit.h>

@interface CZAnnotationEditingViewModel ()

@property (nonatomic, strong) CZElement *selectionElement;

@end

@implementation CZAnnotationEditingViewModel

- (CZAnnotationConfigPanelControllerPosition)configPanelControllerPosition {
    CZAnnotationConfigPanelControllerPosition position =  CZAnnotationConfigPanelControllerPositionDefault;
    
    CGRect selectionElementFrame = [self.selectionElement.primaryNode frame];
    CGRect annotationScreenFrame = CGRectMake(0, kUIStatusBarHeight, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds) - 64.0);

    BOOL horizontalOverlapped = fabs(CGRectGetMidX(selectionElementFrame) - CGRectGetMidX(annotationScreenFrame)) >= (CGRectGetWidth(annotationScreenFrame) - CZAnnotationConfigPanelWidth) / 2.0;
    BOOL verticalOverlapped = fabs(CGRectGetMidY(selectionElementFrame) - CGRectGetMidY(annotationScreenFrame)) >= (CGRectGetHeight(annotationScreenFrame) - CZAnnotationConfigPanelHeight) / 2.0;
    
    BOOL positiveLeft = CGRectGetMidX(selectionElementFrame) >= CGRectGetMidX(annotationScreenFrame);
    BOOL positiveBottom = CGRectGetMidY(selectionElementFrame) >= CGRectGetMidY(annotationScreenFrame);
    
    if ((horizontalOverlapped && verticalOverlapped) == NO){
        if (positiveLeft) {
            position = positiveBottom ? CZAnnotationConfigPanelControllerPositionBottomRight : CZAnnotationConfigPanelControllerPositionBottomLeft;
        } else {
            position = CZAnnotationConfigPanelControllerPositionBottomLeft;
        }
    }
    
    return position;
}

@end
