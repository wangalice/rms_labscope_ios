//
//  CZAnnotationEditingViewController.h
//  Matscope
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageEditingViewController.h"
#import "CZAnnotationMarco.h"

NS_ASSUME_NONNULL_BEGIN

@class CZSelectTool;
@interface CZAnnotationEditingViewController : CZImageEditingViewController

- (void)annotationButtonPressed:(CZAnnotationButtonID)buttonID on:(BOOL)on;

@end

NS_ASSUME_NONNULL_END
