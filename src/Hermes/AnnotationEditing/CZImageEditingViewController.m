//
//  CZImageEditingViewController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageEditingViewController.h"

/* Inner class, translucent view which means this view doesn't handle UI touch, but it's children do*/
@interface CZTranslucentView : UIView

@end

@implementation CZTranslucentView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView * view in [self subviews]) {
        if (!view.isHidden && view.userInteractionEnabled && [view pointInside:[self convertPoint:point toView:view] withEvent:event]) {
            return YES;
        }
    }
    return NO;
}

@end

@implementation CZImageEditingViewController

- (void)loadView {
    CGRect frame = self.parentViewController.view.frame;
    frame.origin = CGPointZero;
    
    CZTranslucentView *translucentView = [[CZTranslucentView alloc] initWithFrame:frame];
    translucentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view = translucentView;
    
    if ([self.delegate respondsToSelector:@selector(viewControllerDidBeginEditing:)]) {
        [self.delegate viewControllerDidBeginEditing:self];
    }
}

- (void)removeFromParentViewController {
    [super removeFromParentViewController];
    
    if ([self.delegate respondsToSelector:@selector(viewControllerDidEndEditing:)]) {
        [self.delegate viewControllerDidEndEditing:self];
    }
}

- (CZDocManager *)editingImageDocument {
    if ([self.dataSource respondsToSelector:@selector(editingImageDocument)]) {
        return [self.dataSource editingImageDocument];
    } else {
        return nil;
    }
}

- (UIView *)editingImageView {
    if ([self.dataSource respondsToSelector:@selector(editingImageView)]) {
        return [self.dataSource editingImageView];
    } else {
        return nil;
    }
}

- (UIView *)imageView {
    if ([self.dataSource respondsToSelector:@selector(imageView)]) {
        return [self.dataSource imageView];
    } else {
        return nil;
    }
}

- (UIScrollView *)imageScrollView {
    if ([self.dataSource respondsToSelector:@selector(imageScrollView)]) {
        return [self.dataSource imageScrollView];
    } else {
        return nil;
    }
}

- (CGFloat)imageViewZoomScale {
    UIScrollView *scrollView = self.imageScrollView;
    if (scrollView) {
        return scrollView.zoomScale;
    } else {
        if ([self.dataSource respondsToSelector:@selector(imageViewZoomScale)]) {
            return [self.dataSource imageViewZoomScale];
        } else {
            return 1;
        }
    }
}

- (id<CZMagnifyDelegate>)magnifyDelegate {
    if ([self.dataSource respondsToSelector:@selector(magnifyDelegate)]) {
        return [self.dataSource magnifyDelegate];
    } else {
        return nil;
    }
}

@end
