//
//  CZAnnotationControlPanelController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationConfigPanelController.h"
#import "CZAnnotationColorPickerView.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZElement+Info.h"
#import "CZAnnotationConfigPanelViewModel.h"
#import "CZAnnotationConfigPanelPresentationController.h"

@interface CZAnnotationConfigPanelController () <
UIViewControllerTransitioningDelegate
>
@property (nonatomic, readonly, strong) UILabel *showMeasurementLabel;
@property (nonatomic, readonly, strong) UILabel *defaultColorLabel;
@property (nonatomic, readonly, strong) UILabel *defaultBackgroundColorLabel;
@property (nonatomic, readonly, strong) UILabel *backgroundTransparencyLabel;
@property (nonatomic, readonly, strong) UILabel *defaultSizeLabel;
@property (nonatomic, readonly, strong) UILabel *keepAspectLabel;
@property (nonatomic, readonly, strong) CZAnnotationConfigPanelViewModel *viewModel;
@property (nonatomic, readonly, assign) CZAnnotationConfigPanelControllerPosition position;

@end

@implementation CZAnnotationConfigPanelController

@synthesize showMeasurementLabel = _showMeasurementLabel;
@synthesize defaultColorLabel = _defaultColorLabel;
@synthesize defaultBackgroundColorLabel = _defaultBackgroundColorLabel;
@synthesize backgroundTransparencyLabel = _backgroundTransparencyLabel;
@synthesize defaultSizeLabel = _defaultSizeLabel;
@synthesize keepAspectLabel = _keepAspectLabel;
@synthesize annotationTypeLabel = _annotationTypeLabel;
@synthesize deleteButton = _deleteButton;
@synthesize measurementSwitch = _measurementSwitch;
@synthesize backgroundTransparencySwitch = _backgroundTransparencySwitch;
@synthesize keepAspectSwitch = _keepAspectSwitch;
@synthesize defaultSizeSwitch = _defaultSizeSwitch;
@synthesize defaultBackgroundColorPickerView = _defaultBackgroundColorPickerView;
@synthesize defaultColorPickerView = _defaultColorPickerView;
@synthesize position = _position;

- (instancetype)initWithPosition:(CZAnnotationConfigPanelControllerPosition)position {
    if (self = [super init]) {
        _viewModel = [[CZAnnotationConfigPanelViewModel alloc] init];
        _position = position;
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

- (void)dealloc {
    _annotationTypeLabel = nil;
    _deleteButton = nil;
    _showMeasurementLabel = nil;
    _measurementSwitch = nil;
    _defaultColorLabel = nil;
    _defaultColorPickerView = nil;
    _defaultBackgroundColorLabel = nil;
    _defaultBackgroundColorPickerView = nil;
    _backgroundTransparencyLabel = nil;
    _backgroundTransparencySwitch = nil;
    _keepAspectLabel = nil;
    _keepAspectSwitch = nil;
    _defaultSizeLabel = nil;
    _defaultSizeSwitch = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs20]);
    self.view.layer.cornerRadius = 8.0f;
    [self.view addSubview:self.annotationTypeLabel];
    [self.view addSubview:self.deleteButton];
    [self.view addSubview:self.showMeasurementLabel];
    [self.view addSubview:self.measurementSwitch];
    [self.view addSubview:self.defaultColorLabel];
    [self.view addSubview:self.defaultColorPickerView];
    [self.view addSubview:self.defaultBackgroundColorLabel];
    [self.view addSubview:self.defaultBackgroundColorPickerView];
    [self.view addSubview:self.backgroundTransparencyLabel];
    [self.view addSubview:self.backgroundTransparencySwitch];
    [self.view addSubview:self.defaultSizeLabel];
    [self.view addSubview:self.defaultSizeSwitch];

    //auto layout
    [self addAutoLayoutContraints];
    [self updateControlsWithElement:[self selectionElement]];
}

- (void)addAutoLayoutContraints {
    
    CGFloat kHorizonalMargin = 32.0f;
    CGFloat kVerticalMargin = 27.0f;
    CGFloat kVerticalControlsMargin = 16.0f;
    CGFloat kHorizonalControlsMargin = 16.0f;
    
    self.annotationTypeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.showMeasurementLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.measurementSwitch.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultColorLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultColorPickerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultBackgroundColorLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultBackgroundColorPickerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.backgroundTransparencyLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.backgroundTransparencySwitch.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultSizeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.defaultSizeSwitch.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.annotationTypeLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:kHorizonalMargin].active = YES;
    [self.annotationTypeLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:kVerticalMargin].active = YES;
    [self.annotationTypeLabel.heightAnchor constraintEqualToConstant:20.0f].active = YES;
    [self.annotationTypeLabel.rightAnchor constraintEqualToAnchor:self.deleteButton.leftAnchor constant:-kHorizonalControlsMargin].active = YES;
    
    [self.deleteButton.centerYAnchor constraintEqualToAnchor:self.annotationTypeLabel.centerYAnchor].active = YES;
    [self.deleteButton.widthAnchor constraintEqualToConstant:44.0f].active = YES;
    [self.deleteButton.heightAnchor constraintEqualToConstant:44.0f].active = YES;
    [self.deleteButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    
    [self.showMeasurementLabel.leftAnchor constraintEqualToAnchor:self.annotationTypeLabel.leftAnchor].active = YES;
    [self.showMeasurementLabel.topAnchor constraintEqualToAnchor:self.annotationTypeLabel.bottomAnchor constant:kVerticalControlsMargin].active = YES;
    [self.showMeasurementLabel.heightAnchor constraintEqualToConstant:20.0f * 2].active = YES;
    [self.showMeasurementLabel.widthAnchor constraintLessThanOrEqualToConstant:96.0f].active = YES;
    
    [self.measurementSwitch.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    [self.measurementSwitch.centerYAnchor constraintEqualToAnchor:self.showMeasurementLabel.centerYAnchor].active = YES;
    
    [self.defaultColorLabel.leftAnchor constraintEqualToAnchor:self.showMeasurementLabel.leftAnchor].active = YES;
    [self.defaultColorLabel.topAnchor constraintEqualToAnchor:self.showMeasurementLabel.bottomAnchor constant:kVerticalControlsMargin].active = YES;
    [self.defaultColorLabel.heightAnchor constraintEqualToConstant:20.0f * 2].active = YES;
    [self.defaultColorLabel.widthAnchor constraintLessThanOrEqualToConstant:96.0f].active = YES;
    
    [self.defaultColorPickerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    [self.defaultColorPickerView.centerYAnchor constraintEqualToAnchor:self.defaultColorLabel.centerYAnchor].active = YES;
    
    [self.backgroundTransparencyLabel.leftAnchor constraintEqualToAnchor:self.defaultColorLabel.leftAnchor].active = YES;
    [self.backgroundTransparencyLabel.topAnchor constraintEqualToAnchor:self.defaultColorLabel.bottomAnchor constant:kVerticalControlsMargin].active = YES;
    [self.backgroundTransparencyLabel.heightAnchor constraintEqualToConstant:20.0f * 2].active = YES;
    [self.backgroundTransparencyLabel.widthAnchor constraintLessThanOrEqualToConstant:96.0f].active = YES;

    [self.backgroundTransparencySwitch.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    [self.backgroundTransparencySwitch.centerYAnchor constraintEqualToAnchor:self.backgroundTransparencyLabel.centerYAnchor].active = YES;
    
    [self.defaultBackgroundColorLabel.leftAnchor constraintEqualToAnchor:self.backgroundTransparencyLabel.leftAnchor].active = YES;
    [self.defaultBackgroundColorLabel.topAnchor constraintEqualToAnchor:self.backgroundTransparencyLabel.bottomAnchor constant:kVerticalControlsMargin].active = YES;
    [self.defaultBackgroundColorLabel.heightAnchor constraintEqualToConstant:20.0f * 2].active = YES;
    [self.defaultBackgroundColorLabel.widthAnchor constraintLessThanOrEqualToConstant:96.0f].active = YES;

    [self.defaultBackgroundColorPickerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    [self.defaultBackgroundColorPickerView.centerYAnchor constraintEqualToAnchor:self.defaultBackgroundColorLabel.centerYAnchor].active = YES;
    
    [self.defaultSizeLabel.leftAnchor constraintEqualToAnchor:self.defaultBackgroundColorLabel.leftAnchor].active = YES;
    [self.defaultSizeLabel.topAnchor constraintEqualToAnchor:self.defaultBackgroundColorLabel.bottomAnchor constant:kVerticalControlsMargin].active = YES;
    [self.defaultSizeLabel.heightAnchor constraintEqualToConstant:20.0f * 2].active = YES;
    [self.defaultSizeLabel.widthAnchor constraintLessThanOrEqualToConstant:96.0f].active = YES;
    
    [self.defaultSizeSwitch.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kHorizonalMargin].active = YES;
    [self.defaultSizeSwitch.centerYAnchor constraintEqualToAnchor:self.defaultSizeLabel.centerYAnchor].active = YES;
    [self.defaultSizeSwitch.heightAnchor constraintEqualToConstant:48.0f].active = YES;
    [self.defaultSizeSwitch.widthAnchor constraintEqualToConstant:240.0f].active = YES;
}

#pragma mark - Private method

- (CZElement *)selectionElement {
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(selectionElement)]) {
        return [self.dataSource selectionElement];
    }
    return nil;
}

- (void)updateControlsWithElement:(CZElement *)element {
    [self.viewModel updateSelectionElement:element];
    
    self.defaultColorLabel.enabled = self.viewModel.isDefaultColorAvailable;
    self.defaultColorPickerView.enabled = self.viewModel.isDefaultColorAvailable;
    self.defaultBackgroundColorPickerView.enabled = self.viewModel.isBackgroundColorControlAvailable;
    self.defaultBackgroundColorLabel.enabled = self.viewModel.isBackgroundColorControlAvailable;
    self.showMeasurementLabel.enabled = self.viewModel.isMeasurementShowControlAvailable;
    self.measurementSwitch.enabled = self.viewModel.isMeasurementShowControlAvailable;
    
    if (element) {
        self.annotationTypeLabel.text = [element elementTypeName];
        self.defaultSizeSwitch.selectedIndex = element.size;
        self.measurementSwitch.on = !element.measurementHidden && self.viewModel.isMeasurementShowControlAvailable;
        self.defaultColorPickerView.selectedColor = element.strokeColor;
        BOOL isBackgroundTransparent = [self selectionElement].isMeasurementInfoAvailable ?  CZColorEqualToColor(element.measurementBackgroundColor, kCZColorTransparent) : CZColorEqualToColor(element.fillColor, kCZColorTransparent);
        if (isBackgroundTransparent == NO) {
            self.defaultBackgroundColorPickerView.selectedColor = [self selectionElement].isMeasurementInfoAvailable ? element.measurementBackgroundColor : element.fillColor;
        }
        self.defaultBackgroundColorPickerView.enabled = !isBackgroundTransparent;
        self.backgroundTransparencySwitch.on = isBackgroundTransparent;
    }
}

- (CGRect)sourceRectWithPostion:(CZAnnotationConfigPanelControllerPosition)position {
    CGFloat x = 0;
    CGFloat y = 0;
    
    switch (position) {
        case CZAnnotationConfigPanelControllerPositionBottomLeft:
        {
            x = CZANnotationConfigPanelHorizontalMargin;
            y = CGRectGetHeight([UIScreen mainScreen].bounds) - 64.0f - CZANnotationConfigPanelVerticalMargin - CZAnnotationConfigPanelHeight;
        }
            break;
        case CZAnnotationConfigPanelControllerPositionTopLeft: {
            x = CZANnotationConfigPanelHorizontalMargin;
            y = kUISystemStatusBarHeight + CZANnotationConfigPanelVerticalMargin;
        }
            break;
        case CZAnnotationConfigPanelControllerPositionTopRight: {
            x = CGRectGetWidth([UIScreen mainScreen].bounds) - CZANnotationConfigPanelHorizontalMargin - CZAnnotationConfigPanelWidth;
            y = kUISystemStatusBarHeight + CZANnotationConfigPanelVerticalMargin;
        }
            break;
        case CZAnnotationConfigPanelControllerPositionBottomRight: {
            x = CGRectGetWidth([UIScreen mainScreen].bounds) - CZANnotationConfigPanelHorizontalMargin - CZAnnotationConfigPanelWidth;
            y = CGRectGetHeight([UIScreen mainScreen].bounds) - 64.0f - CZANnotationConfigPanelVerticalMargin - CZAnnotationConfigPanelHeight;
        }
            break;
    }
    
    return CGRectMake(x, y, CZAnnotationConfigPanelWidth, CZAnnotationConfigPanelHeight);
}


- (CGRect)sourceRect {
    return [self sourceRectWithPostion:self.position];
}

#pragma mark - Action

- (void)annotationDeleteAction:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelControllerDidDeleteAnnotation:)]) {
        [self.delegate annotationConfigPanelControllerDidDeleteAnnotation:self];
    }
}

- (void)annotationSizeSwitchAction:(CZToggleButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didSelectSize:)]) {
        [self.delegate annotationConfigPanelController:self didSelectSize:sender.selectedIndex];
    }
}

- (void)measurementSwitchEvent:(CZSwitch *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didChangeMeasurementVisiable:)]) {
        [self.delegate annotationConfigPanelController:self didChangeMeasurementVisiable:sender.isOn];
    }
}

- (void)backgroundTransparencySwitchEvent:(CZSwitch *)sender {
    self.defaultBackgroundColorPickerView.enabled = !sender.isOn;
    if (sender.isOn) {
        self.defaultBackgroundColorPickerView.selectedColorIndex = CZAnnotationColorPickerViewNoSelectionColor;
        if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didSelectBackgroundColor:)]) {
            [self.delegate annotationConfigPanelController:self didSelectBackgroundColor:kCZColorTransparent];
        }
    } else {
        self.defaultBackgroundColorPickerView.selectedColor = kCZColorWhite;
        if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didSelectBackgroundColor:)]) {
            [self.delegate annotationConfigPanelController:self didSelectBackgroundColor:kCZColorWhite];
        }
    }
}

- (void)rectangleScalingSwitchEvent:(CZSwitch *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:keepAspect:)]) {
        [self.delegate annotationConfigPanelController:self keepAspect:sender.isOn];
    }
}

- (void)defaultColorPickerViewAction:(CZAnnotationColorPickerView *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didSelectColor:)]) {
        [self.delegate annotationConfigPanelController:self didSelectColor:sender.selectedColor];
    }
}

- (void)bakcgroundColorPickerViewAction:(CZAnnotationColorPickerView *)sender {
    if (self.backgroundTransparencySwitch.isOn) {
        return;
    }
    
    CZColor defaultColor = [self.viewModel defaultColorAfterChangingBackgroundColor:sender.selectedColor originalForegroundColor:self.defaultColorPickerView.selectedColor];
    self.defaultColorPickerView.selectedColor = defaultColor;
    [self.defaultColorPickerView sendActionsForControlEvents:UIControlEventValueChanged];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationConfigPanelController:didSelectBackgroundColor:)]) {
        [self.delegate annotationConfigPanelController:self didSelectBackgroundColor:sender.selectedColor];
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    CZAnnotationConfigPanelPresentationController *presentationController = [[CZAnnotationConfigPanelPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    presentationController.sourceRect = [self sourceRect];
    return presentationController;
}

#pragma mark - Getter

- (UILabel *)annotationTypeLabel {
    if (_annotationTypeLabel == nil) {
        _annotationTypeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _annotationTypeLabel.font = [UIFont cz_subtitle1];
        _annotationTypeLabel.textColor = [UIColor cz_gs10];
    }
    return _annotationTypeLabel;
}

- (UILabel *)showMeasurementLabel {
    if (_showMeasurementLabel == nil) {
        _showMeasurementLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _showMeasurementLabel.font = [UIFont cz_label1];
        _showMeasurementLabel.textColor = [UIColor cz_gs80];
        _showMeasurementLabel.text = L(@"MEASUREMENT_ENABLED");
        _showMeasurementLabel.numberOfLines = 0;
    }
    return _showMeasurementLabel;
}

- (UILabel *)defaultColorLabel {
    if (_defaultColorLabel == nil) {
        _defaultColorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _defaultColorLabel.font = [UIFont cz_label1];
        _defaultColorLabel.textColor = [UIColor cz_gs80];
        _defaultColorLabel.text = L(@"DEFAULT_COLOR");
        _defaultColorLabel.numberOfLines = 0;
    }
    return _defaultColorLabel;
}

- (UILabel *)defaultBackgroundColorLabel {
    if (_defaultBackgroundColorLabel == nil) {
        _defaultBackgroundColorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _defaultBackgroundColorLabel.font = [UIFont cz_label1];
        _defaultBackgroundColorLabel.textColor = [UIColor cz_gs80];
        _defaultBackgroundColorLabel.text = L(@"DEFAULT_BACKGROUND_COLOR");
        _defaultBackgroundColorLabel.numberOfLines = 0;
    }
    return _defaultBackgroundColorLabel;
}

- (UILabel *)defaultSizeLabel {
    if (_defaultSizeLabel == nil) {
        _defaultSizeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _defaultSizeLabel.font = [UIFont cz_label1];
        _defaultSizeLabel.textColor = [UIColor cz_gs80];
        _defaultSizeLabel.text = L(@"DEFAULT_SIZE");
        _defaultSizeLabel.numberOfLines = 0;
    }
    return _defaultSizeLabel;
}

- (UILabel *)backgroundTransparencyLabel {
    if (_backgroundTransparencyLabel == nil) {
        _backgroundTransparencyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _backgroundTransparencyLabel.font = [UIFont cz_label1];
        _backgroundTransparencyLabel.textColor = [UIColor cz_gs80];
        _backgroundTransparencyLabel.text = L(@"DEFAULT_BACKGROUND_COLOR_CLEAR");
        _backgroundTransparencyLabel.numberOfLines = 0;
    }
    return _backgroundTransparencyLabel;
}

- (UILabel *)keepAspectLabel {
    if (_keepAspectLabel == nil) {
        _keepAspectLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _keepAspectLabel.font = [UIFont cz_label1];
        _keepAspectLabel.textColor = [UIColor cz_gs80];
        _keepAspectLabel.text = L(@"KEEP_ASPECT");
        _keepAspectLabel.numberOfLines = 0;
    }
    return _keepAspectLabel;
}

- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
        [_deleteButton addTarget:self action:@selector(annotationDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}

- (CZSwitch *)measurementSwitch {
    if (_measurementSwitch == nil) {
        _measurementSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_measurementSwitch setLevel:CZControlEmphasisActivePrimary];
        [_measurementSwitch addTarget:self action:@selector(measurementSwitchEvent:) forControlEvents:UIControlEventValueChanged];
    }
    return _measurementSwitch;
}


- (CZSwitch *)backgroundTransparencySwitch {
    if (_backgroundTransparencySwitch == nil) {
        _backgroundTransparencySwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_backgroundTransparencySwitch setLevel:CZControlEmphasisActivePrimary];
        [_backgroundTransparencySwitch addTarget:self action:@selector(backgroundTransparencySwitchEvent:) forControlEvents:UIControlEventValueChanged];
    }
    return _backgroundTransparencySwitch;
}

- (CZSwitch *)keepAspectSwitch {
    if (_keepAspectSwitch == nil) {
        _keepAspectSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_keepAspectSwitch setLevel:CZControlEmphasisActivePrimary];
        [_keepAspectSwitch addTarget:self action:@selector(rectangleScalingSwitchEvent:) forControlEvents:UIControlEventValueChanged];
    }
    return _keepAspectSwitch;
}

- (CZToggleButton *)defaultSizeSwitch {
    if (_defaultSizeSwitch == nil) {
        _defaultSizeSwitch = [[CZToggleButton alloc] initWithItems:@[L(@"SIZE_XS"),
                                                                     L(@"SIZE_S"),
                                                                     L(@"SIZE_M"),
                                                                     L(@"SIZE_L"),
                                                                     L(@"SIZE_XL")]];
        [_defaultSizeSwitch setTitleTextAttributes:@{NSFontAttributeName:[UIFont cz_subtitle2]} forState:UIControlStateNormal];
        [_defaultSizeSwitch addTarget:self action:@selector(annotationSizeSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _defaultSizeSwitch;
}

- (CZAnnotationColorPickerView *)defaultBackgroundColorPickerView {
    if (_defaultBackgroundColorPickerView == nil) {
        _defaultBackgroundColorPickerView = [[CZAnnotationColorPickerView alloc] initWithColors:kCZColorValueRed, kCZColorValueBlue, kCZColorValueGreen, kCZColorValueWhite, kCZColorValueBlack, nil];
        [_defaultBackgroundColorPickerView addTarget:self action:@selector(bakcgroundColorPickerViewAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _defaultBackgroundColorPickerView;
}

- (CZAnnotationColorPickerView *)defaultColorPickerView {
    if (_defaultColorPickerView == nil) {
        _defaultColorPickerView = [[CZAnnotationColorPickerView alloc] initWithColors:kCZColorValueRed, kCZColorValueBlue, kCZColorValueGreen, kCZColorValueYellow, kCZColorValueBlack, nil];
        [_defaultColorPickerView addTarget:self action:@selector(defaultColorPickerViewAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _defaultColorPickerView;
}

@end
