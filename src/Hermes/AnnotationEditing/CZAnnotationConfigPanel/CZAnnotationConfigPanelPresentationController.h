//
//  CZAnnotationConfigPanelPresentationController.h
//  Hermes
//
//  Created by Carl Zeiss on 2019/6/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZAnnotationConfigPanelPresentationController : UIPresentationController

@property (nonatomic, assign) CGRect sourceRect;

@end

NS_ASSUME_NONNULL_END
