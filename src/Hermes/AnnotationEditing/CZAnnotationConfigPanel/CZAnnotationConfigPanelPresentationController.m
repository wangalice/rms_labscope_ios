//
//  CZAnnotationConfigPanelPresentationController.m
//  Hermes
//
//  Created by Carl Zeiss on 2019/6/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationConfigPanelPresentationController.h"

@interface CZAnnotationConfigPanelPresentationController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong)  UITapGestureRecognizer *tapGestureRec;

@end

@implementation CZAnnotationConfigPanelPresentationController

- (CGRect)frameOfPresentedViewInContainerView {
    return _sourceRect;
}

- (void)presentationTransitionWillBegin {
    [self.containerView addGestureRecognizer:self.tapGestureRec];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.containerView removeGestureRecognizer:self.tapGestureRec];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint location = [touch locationInView:self.containerView];
    return !CGRectContainsPoint(_sourceRect, location);
}

#pragma mark - Getters
- (UITapGestureRecognizer *)tapGestureRec {
    if (!_tapGestureRec) {
        _tapGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(dismiss)];
        _tapGestureRec.delegate = self;
    }
    return _tapGestureRec;
}

#pragma mark - Action
- (void)dismiss {
    if (self.presentedViewController) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
