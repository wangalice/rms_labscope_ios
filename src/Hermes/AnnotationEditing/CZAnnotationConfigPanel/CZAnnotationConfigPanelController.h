//
//  CZAnnotationControlPanelController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZAnnotationMarco.h"

NS_ASSUME_NONNULL_BEGIN

/** Annotion operation pannel: change color, measurement, size. etc. */

@class CZElement, CZAnnotationConfigPanelController, CZAnnotationColorPickerView;

@protocol CZAnnotationControlPanelControllerDelegate <NSObject>

@optional
- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectColor:(CZColor)color;
- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectSize:(CZElementSize)size;
- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectBackgroundColor:(CZColor)backgroundColor;
- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didChangeMeasurementVisiable:(BOOL)visiable;
- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController keepAspect:(BOOL)keepAspect;
- (void)annotationConfigPanelControllerDidDeleteAnnotation:(CZAnnotationConfigPanelController *)annotationConfigPanelController;

@end

@protocol CZAnnotationControlPanelControllerDataSource <NSObject>

@required
- (CZElement *)selectionElement;

@end

@interface CZAnnotationConfigPanelController : UIViewController

@property (nonatomic, weak) id<CZAnnotationControlPanelControllerDataSource> dataSource;
@property (nonatomic, weak) id<CZAnnotationControlPanelControllerDelegate> delegate;

@property (nonatomic, readonly, strong) UILabel *annotationTypeLabel;
@property (nonatomic, readonly, strong) UIButton *deleteButton;
@property (nonatomic, readonly, strong) CZSwitch *measurementSwitch;
@property (nonatomic, readonly, strong) CZSwitch *backgroundTransparencySwitch;
@property (nonatomic, readonly, strong) CZSwitch *keepAspectSwitch;
@property (nonatomic, readonly, strong) CZToggleButton *defaultSizeSwitch;
@property (nonatomic, readonly, strong) CZAnnotationColorPickerView *defaultBackgroundColorPickerView;
@property (nonatomic, readonly, strong) CZAnnotationColorPickerView *defaultColorPickerView;

- (instancetype)initWithPosition:(CZAnnotationConfigPanelControllerPosition)position;

@end

NS_ASSUME_NONNULL_END
