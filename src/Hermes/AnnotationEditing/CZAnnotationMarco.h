//
//  CZAnnotationMarco.h
//  Labscope
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#ifndef CZAnnotationMarco_h
#define CZAnnotationMarco_h

typedef NS_ENUM(NSInteger, CZAnnotationButtonID) {
    CZAnnotationButtonUnknown = -1,
    CZAnnotationButtonLine,
    CZAnnotationButtonSquare,
    CZAnnotationButtonCircle,
    CZAnnotationButtonPolygon,
    CZAnnotationButtonPolyline,
    CZAnnotationButtonSpline,
    CZAnnotationButtonSplineContour,
    CZAnnotationButtonCount,
    CZAnnotationButtonArrow,
    CZAnnotationButtonAngle,
    CZAnnotationButtonDisconnectedAngle,
    CZAnnotationButtonText,
    CZAnnotationButtonScaleBar,
    CZAnnotationButtonCalipers,
    CZAnnotationButtonPCBXDirection,
    CZAnnotationButtonPCBYDirection,
    CZAnnotationButtonMultiCalipers
};

typedef NS_ENUM(NSUInteger, CZEditingMode) {
    CZEditingModeAnnotationBasic = 0,
    CZEditingModePCB = 1,
    CZEditingModeMultiphase = 2,
    CZEditingModeParticles = 3,
    CZEditingModeGrainSize = 4,
    CZEditingModeCurveLayer = 5,
    CZEditingModeImageProcessing = 10
};

typedef NS_ENUM(NSUInteger, CZAnnotationConfigPanelControllerPosition) {
    CZAnnotationConfigPanelControllerPositionDefault = 0,
    CZAnnotationConfigPanelControllerPositionTopLeft = 1,
    CZAnnotationConfigPanelControllerPositionTopRight = 2,
    CZAnnotationConfigPanelControllerPositionBottomLeft = CZAnnotationConfigPanelControllerPositionDefault,
    CZAnnotationConfigPanelControllerPositionBottomRight = 3
};

static CGFloat const CZAnnotationConfigPanelWidth = 416.0f;
static CGFloat const CZAnnotationConfigPanelHeight = 350.0f;
static CGFloat const CZANnotationConfigPanelHorizontalMargin = 16.0f;
static CGFloat const CZANnotationConfigPanelVerticalMargin = 16.0f;

#endif /* CZAnnotationMarco_h */
