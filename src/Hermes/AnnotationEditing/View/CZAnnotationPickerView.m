//
//  CZAnnotationPickerView.m
//  Hermes
//
//  Created by Li, Junlin on 2/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationPickerView.h"

@interface CZAnnotationPickerView ()

@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, strong) UIButton *firstButton;
@property (nonatomic, readonly, strong) UIButton *lastButton;
@property (nonatomic, readonly, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CZToggleButton *toggleButton;
@property (nonatomic, assign) CZAnnotationButtonID selectedAnnotionButtonID;
@property (nonatomic, strong) NSMutableArray <NSNumber *> *annotationButtonItems;

@end

@implementation CZAnnotationPickerView

@synthesize stackView = _stackView;
@synthesize firstButton = _firstButton;
@synthesize lastButton = _lastButton;
@synthesize scrollView = _scrollView;
@synthesize toggleButton = _toggleButton;

- (instancetype)initWithDataSource:(id)dataSouce {
    if (self = [super init]) {
        self.dataSource = dataSouce;
        _annotationButtonItems = [NSMutableArray array];
        self.toggleButton = [[CZToggleButton alloc] initWithItems:[self annotationIconItems]];
        [self.toggleButton addTarget:self action:@selector(selectAnnotationAction:) forControlEvents:UIControlEventValueChanged];
        self.toggleButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.stackView];
        [self.stackView addArrangedSubview:self.firstButton];
        [self.stackView addArrangedSubview:self.scrollView];
        [self.stackView addArrangedSubview:self.lastButton];
        [self.scrollView addSubview:self.toggleButton];
        
        [self.stackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
        [self.stackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
        [self.stackView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [self.stackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
        
        [self.toggleButton.leadingAnchor constraintEqualToAnchor:self.scrollView.leadingAnchor].active = YES;
        [self.toggleButton.trailingAnchor constraintEqualToAnchor:self.scrollView.trailingAnchor].active = YES;
        [self.toggleButton.topAnchor constraintEqualToAnchor:self.scrollView.topAnchor].active = YES;
        [self.toggleButton.bottomAnchor constraintEqualToAnchor:self.scrollView.bottomAnchor].active = YES;
        [self.toggleButton.heightAnchor constraintEqualToAnchor:self.scrollView.heightAnchor].active = YES;
        [self.toggleButton.widthAnchor constraintEqualToConstant:64.0 * 15].active = YES;

        self.lastButton.enabled = YES;
        self.firstButton.enabled = NO;

        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - Public method

- (void)deselectAnnotationButton {
    self.selectedAnnotionButtonID = CZAnnotationButtonUnknown;
    if (self.toggleButton.selectedIndex != CZToggleButtonNoSelectedButton) {
        self.toggleButton.selectedIndex = CZToggleButtonNoSelectedButton;
    }
}

#pragma mark - Private method

- (NSArray <CZIcon *> *)annotationIconItems {
    NSMutableArray *array = [NSMutableArray array];
    for (NSNumber *number in self.annotationButtonItems) {
        CZIcon *icon = [CZIcon iconWithAnnotationButtonID:[number integerValue]];
        if (icon != nil) {
            [array addObject:icon];
        }
    }
    return array;
}

#pragma mark - Getter

- (NSMutableArray<NSNumber *> *)annotationButtonItems {
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(annotationButtonsOfAnnotationPickerView:)]) {
        NSArray <NSNumber *> * array = [self.dataSource annotationButtonsOfAnnotationPickerView:self];
        self.annotationButtonItems = [NSMutableArray arrayWithArray:array];
        return _annotationButtonItems;
    }
    return nil;
}

#pragma mark - Views

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.backgroundColor = [UIColor clearColor];
        _stackView.axis = UILayoutConstraintAxisHorizontal;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.spacing = 8.0;
    }
    return _stackView;
}

- (UIButton *)firstButton {
    if (_firstButton == nil) {
        _firstButton = [[UIButton alloc] init];
        [_firstButton cz_setImageWithIcon:[CZIcon iconNamed:@"previous-page"]];
        [_firstButton addTarget:self action:@selector(firstButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [_firstButton.widthAnchor constraintEqualToConstant:32.0].active = YES;
    }
    return _firstButton;
}

- (UIButton *)lastButton {
    if (_lastButton == nil) {
        _lastButton = [[UIButton alloc] init];
        [_lastButton cz_setImageWithIcon:[CZIcon iconNamed:@"next-page"]];
        [_lastButton addTarget:self action:@selector(lastButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [_lastButton.widthAnchor constraintEqualToConstant:32.0].active = YES;
    }
    return _lastButton;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.scrollEnabled = NO;
        _scrollView.layer.borderColor = [[UIColor cz_gs120] CGColor];
        _scrollView.layer.borderWidth = 1.0;
        _scrollView.layer.cornerRadius = 3.0;
        _scrollView.layer.masksToBounds = YES;
        [_scrollView.widthAnchor constraintLessThanOrEqualToConstant:64.0 * 9.5].active = YES;
    }
    return _scrollView;
}

#pragma mark - Actions

- (void)firstButtonAction:(UIButton *)sender {
    [self.scrollView setContentOffset:CGPointZero animated:YES];

    sender.enabled = NO;
    self.lastButton.enabled = YES;
}

- (void)lastButtonAction:(UIButton *)sender {
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width - self.scrollView.bounds.size.width, 0.0) animated:YES];

    sender.enabled = NO;
    self.firstButton.enabled = YES;
}

- (void)selectAnnotationAction:(CZToggleButton *)sender {
    NSParameterAssert(sender.selectedIndex < self.annotationButtonItems.count);
    
    CZAnnotationButtonID buttonID = [self.annotationButtonItems[sender.selectedIndex] integerValue];
    if (self.delegate && [self.delegate respondsToSelector:@selector(annotationPickerView:didSelectAnnotationID:)]) {
        [self.delegate annotationPickerView:self didSelectAnnotationID:buttonID];
    }
}

@end

@implementation CZIcon (CZAnnotationButton)

+ (CZIcon *)iconWithAnnotationButtonID:(CZAnnotationButtonID)annotationButtonID {
    switch (annotationButtonID) {
        case CZAnnotationButtonUnknown: {
            return nil;
        }
            break;
        case CZAnnotationButtonLine: {
            return [CZIcon iconNamed:@"annotation-line"];
        }
            break;
        case CZAnnotationButtonSquare: {
            return [CZIcon iconNamed:@"annotation-rectangle"];
        }
            break;
        case CZAnnotationButtonCircle: {
            return [CZIcon iconNamed:@"annotation-circle"];
        }
            break;
        case CZAnnotationButtonPolygon: {
            return [CZIcon iconNamed:@"annotation-polygon"];
        }
            break;
        case CZAnnotationButtonPolyline: {
            return [CZIcon iconNamed:@"annotation-polyline"];
        }
            break;
        case CZAnnotationButtonSpline: {
            return [CZIcon iconNamed:@"annotation-spline"];
        }
            break;
        case CZAnnotationButtonSplineContour: {
            return [CZIcon iconNamed:@"annotation-spline-contour"];
        }
            break;
        case CZAnnotationButtonCount: {
            return [CZIcon iconNamed:@"annotation-count"];
        }
            break;
        case CZAnnotationButtonArrow: {
            return [CZIcon iconNamed:@"annotation-arrow"];
        }
            break;
        case CZAnnotationButtonAngle: {
            return [CZIcon iconNamed:@"annotation-angle"];
        }
            break;
        case CZAnnotationButtonDisconnectedAngle: {
            return [CZIcon iconNamed:@"annotation-disconnected-angle"];
        }
            break;
        case CZAnnotationButtonText: {
            return [CZIcon iconNamed:@"annotation-text"];
        }
            break;
        case CZAnnotationButtonScaleBar: {
            return [CZIcon iconNamed:@"annotation-scale-bar"];
        }
            break;
        case CZAnnotationButtonCalipers: {
            return [CZIcon iconNamed:@"annotation-calipers"];
        }
            break;
        case CZAnnotationButtonMultiCalipers: {
            return [CZIcon iconNamed:@"annotation-multi-calipers"];
        }
            break;
        default: {
            return nil;
        }
            break;
    }
    return nil;
}

@end
