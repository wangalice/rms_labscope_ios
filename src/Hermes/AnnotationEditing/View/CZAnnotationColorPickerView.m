//
//  CZAnnotationColorPickerView.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationColorPickerView.h"

@interface CZAnnotationColorPickerView ()

@property (nonatomic, readonly, copy) NSArray<NSValue *> *colors;
@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, copy) NSArray<UIButton *> *colorButtons;

@end

@implementation CZAnnotationColorPickerView

- (instancetype)initWithColors:(NSValue *)color, ... {
    self = [super init];
    if (self) {
        NSMutableArray<NSValue *> *colors = [NSMutableArray array];
        NSMutableArray<UIButton *> *colorButtons = [NSMutableArray array];
        
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.axis = UILayoutConstraintAxisHorizontal;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.spacing = 16.0;
        [self addSubview:_stackView];
        
        [_stackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
        [_stackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
        [_stackView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [_stackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
        
        va_list args;
        va_start(args, color);
        
        NSValue *arg = color;
        while (arg) {
            [colors addObject:arg];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.widthAnchor constraintEqualToConstant:32.0].active = YES;
            [button.heightAnchor constraintEqualToConstant:32.0].active = YES;
            button.backgroundColor = UIColorFromCZColor(arg.CZColorValue);
            UIImage *checkmarkImage;
            if (CZColorIsLight(arg.CZColorValue)) {
                CZIconRenderingOptions *options = [CZIconRenderingOptions automaticRenderingOptionsWithTintColor:[UIColor cz_gs120]];
                checkmarkImage = [[CZIcon iconNamed:@"checkmark"] imageWithOptions:options];
            } else {
                CZIconRenderingOptions *options = [CZIconRenderingOptions automaticRenderingOptionsWithTintColor:[UIColor cz_gs10]];
                checkmarkImage = [[CZIcon iconNamed:@"checkmark"] imageWithOptions:options];
            }
            [button setImage:checkmarkImage forState:UIControlStateSelected];
            [button addTarget:self action:@selector(colorButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [_stackView addArrangedSubview:button];
            [colorButtons addObject:button];
            
            arg = va_arg(args, NSValue *);
        }
        
        va_end(args);
        
        _colors = [colors copy];
        _colorButtons = [colorButtons copy];
    }
    return self;
}

#pragma mark - Public

- (void)setSelectedColor:(CZColor)selectedColor {
    [self.colors enumerateObjectsUsingBlock:^(NSValue *color, NSUInteger index, BOOL *stop) {
        if (CZColorEqualToColor(color.CZColorValue, selectedColor)) {
            _selectedColor = selectedColor;
            _selectedColorIndex = index;
            [self updateColorButtonSelection];
            *stop = YES;
        }
    }];
}

- (void)setSelectedColorIndex:(NSUInteger)selectedColorIndex {
    if (selectedColorIndex == CZAnnotationColorPickerViewNoSelectionColor) {
        if (_selectedColorIndex < self.colors.count) {
            UIButton *selectedColorButton = self.colorButtons[_selectedColorIndex];
            selectedColorButton.selected = NO;
            _selectedColor = kCZColorTransparent;
        }
    } else if (selectedColorIndex < self.colors.count) {
        _selectedColor = self.colors[selectedColorIndex].CZColorValue;
        _selectedColorIndex = selectedColorIndex;
        [self updateColorButtonSelection];
    }
}

#pragma mark - Private

- (void)colorButtonAction:(UIButton *)colorButton {
    NSUInteger index = [self.colorButtons indexOfObject:colorButton];
    _selectedColor = self.colors[index].CZColorValue;
    _selectedColorIndex = index;
    
    [self updateColorButtonSelection];
    
    if (self.selectionHandler) {
        self.selectionHandler(self.selectedColor, self.selectedColorIndex);
    }
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void)updateColorButtonSelection {
    [self.colorButtons enumerateObjectsUsingBlock:^(UIButton *colorButton, NSUInteger index, BOOL *stop) {
        colorButton.selected = (index == self.selectedColorIndex);
    }];
}

@end
