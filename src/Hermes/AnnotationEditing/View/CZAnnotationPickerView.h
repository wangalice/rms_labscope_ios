//
//  CZAnnotationPickerView.h
//  Hermes
//
//  Created by Li, Junlin on 2/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZAnnotationMarco.h"

@class CZAnnotationPickerView;

@protocol CZAnnotationPickerViewDelegate <NSObject>

@optional
- (void)annotationPickerView:(CZAnnotationPickerView *)annotationPickerView
       didSelectAnnotationID:(CZAnnotationButtonID)selectedAnnotionButtonID;

@end

@protocol CZAnnotationPickerViewDataSource <NSObject>

- (NSArray <NSNumber *> *)annotationButtonsOfAnnotationPickerView:(CZAnnotationPickerView *)annotationPickerView;

@end

@interface CZAnnotationPickerView : UIView

@property (nonatomic, readonly, assign) CZAnnotationButtonID selectedAnnotionButtonID;
@property (nonatomic, weak) id <CZAnnotationPickerViewDelegate> delegate;
@property (nonatomic, weak) id <CZAnnotationPickerViewDataSource> dataSource;

- (instancetype)initWithDataSource:(id)dataSouce;

- (void)deselectAnnotationButton;

@end

@interface CZIcon (CZAnnotationButton)

+ (CZIcon *)iconWithAnnotationButtonID:(CZAnnotationButtonID)annotationButtonID;

@end
