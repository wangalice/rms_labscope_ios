//
//  CZAnnotationColorPickerView.h
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

enum {
    CZAnnotationColorPickerViewNoSelectionColor = NSUIntegerMax
};

/**
 You can choose either UIControlEventValueChanged or selectionHandler block for control event.
 */
@interface CZAnnotationColorPickerView : UIControl

@property (nonatomic, assign) CZColor selectedColor;
@property (nonatomic, assign) NSUInteger selectedColorIndex;
@property (nonatomic, copy) void (^selectionHandler)(CZColor color, NSUInteger index);

- (instancetype)initWithColors:(NSValue *)colorValues, ... NS_REQUIRES_NIL_TERMINATION;

@end
