//
//  CZAnnotationEditingBaseViewControllerSubclass.h
//  Matscope
//
//  Created by Ralph Jin on 4/17/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationEditingViewController.h"
#import "CZImageViewEditingDelegate.h"

@interface CZAnnotationEditingViewController (ForSubclassEyesOnly) <
CZSelectToolDelegate,
CZElementLayerDelegate,
CZImageViewEditingDelegate
>

@property (nonatomic, readonly, strong) CZSelectTool *selectTool;

- (void)appDidEnterBackground:(NSNotification *)notification;

- (BOOL)canUndo;
- (BOOL)canRedo;

- (void)redoAction;
- (void)undoAction;

- (void)annotationMeasurementDoneAction:(id)sender;

@end
