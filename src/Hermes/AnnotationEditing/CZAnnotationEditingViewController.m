//
//  CZAnnotationEditingViewController.m
//  Matscope
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationEditingViewController.h"
#import "CZAnnotationMarco.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZAnnotationEditingViewControllerSubclass.h"
#import "CZToastManager.h"
#import "CZAnnotationConfigPanelController.h"
#import "CZPopoverController.h"
#import "CZAnnotationEditingViewModel.h"
#import "CZElement+Info.h"

@interface CZAnnotationEditingViewController ()<
CZAnnotationControlPanelControllerDelegate,
CZAnnotationControlPanelControllerDataSource
>

@property (nonatomic, readonly, strong) UILabel *countingLabel;
@property (nonatomic, strong) CZSelectTool *selectTool;
@property (nonatomic, strong) NSArray *scaleBarCornerButtons;
@property (nonatomic, strong) UIView *deleteAllView;
@property (nonatomic, strong) CZButton *annotationSettingButton;
@property (nonatomic, strong) CZAnnotationConfigPanelController *annotationConfigPanelController;

@end

@implementation CZAnnotationEditingViewController

@synthesize countingLabel = _countingLabel;

- (id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}

- (CZDocManager *)docManager {
    return self.editingImageDocument;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // create scale bar corner button
    if (![CZCommonUtils isRunningOnIPhone]) {
        CGRect cornerButtonRect = CGRectZero;
        cornerButtonRect.size = CGSizeMake(64.0f, 48.0f);
        
        UIButton *cornerButtons[4];
        
        for (int i = 0; i < 4; ++i) {
            UIButton *cornerButton = [[UIButton alloc] initWithFrame:cornerButtonRect];
            cornerButtons[i] = cornerButton;
            [cornerButton cz_setImageWithIcon:[CZIcon iconNamed:@"annotation-scale-bar"]];
            [cornerButton addTarget:self action:@selector(selectScaleBarPositionAction:) forControlEvents:UIControlEventTouchUpInside];
            cornerButton.alpha = 0.8f;
            cornerButton.contentMode = UIViewContentModeCenter;
            cornerButton.layer.cornerRadius = 5;
            cornerButton.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
            cornerButton.hidden = YES;
            [self.view addSubview:cornerButton];
        }
        
        _scaleBarCornerButtons = [NSArray arrayWithObjects:cornerButtons count:4];
    }
    
    [self.view addSubview:self.annotationSettingButton];
    
    // initialize model
    CZDocManager *docManager = self.editingImageDocument;
    UIView *imageView = self.imageView;
    CGFloat zoomScale = self.imageViewZoomScale;
    
    // create select tool
    CZSelectTool *selectTool = [[CZSelectTool alloc] initWithDocManager:docManager view:imageView];
    self.selectTool = selectTool;
    [selectTool setMagnifyDelegate:self.magnifyDelegate];
    selectTool.delegate = self;
    [selectTool setZoomOfView:zoomScale];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.selectTool cancelJob];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    CGFloat horizontalMargin = 16.0f;
    CGFloat verticalMargin = 8.0f;
    CGRect pageFrame = self.view.frame;
    pageFrame.origin.y = kUISystemStatusBarHeight;
    
    if (![CZCommonUtils isRunningOnIPhone]) {
        CGRect scrollViewVisbleFrame = pageFrame;
        scrollViewVisbleFrame = CGRectInset(scrollViewVisbleFrame, 2, 2);  // leave a margin
        CGSize scaleBarImageSize = ((UIButton *)_scaleBarCornerButtons[0]).frame.size;
        
        ((UIButton *)_scaleBarCornerButtons[0]).frame = CGRectMake(scrollViewVisbleFrame.origin.x + horizontalMargin,
                                                                   scrollViewVisbleFrame.origin.y ,
                                                                   scaleBarImageSize.width,
                                                                   scaleBarImageSize.height);
        
        ((UIButton *)_scaleBarCornerButtons[1]).frame = CGRectMake(CGRectGetMaxX(scrollViewVisbleFrame) - scaleBarImageSize.width - horizontalMargin,
                                                                   scrollViewVisbleFrame.origin.y,
                                                                   scaleBarImageSize.width,
                                                                   scaleBarImageSize.height);
        
        
        ((UIButton *)_scaleBarCornerButtons[2]).frame = CGRectMake(scrollViewVisbleFrame.origin.x + horizontalMargin,
                                                                   CGRectGetMaxY(scrollViewVisbleFrame) - scaleBarImageSize.height - verticalMargin,
                                                                   scaleBarImageSize.width,
                                                                   scaleBarImageSize.height);
        
        ((UIButton *)_scaleBarCornerButtons[3]).frame = CGRectMake(CGRectGetMaxX(scrollViewVisbleFrame) - scaleBarImageSize.width - horizontalMargin,
                                                                   CGRectGetMaxY(scrollViewVisbleFrame) - scaleBarImageSize.height - verticalMargin,
                                                                   scaleBarImageSize.width,
                                                                   scaleBarImageSize.height);
    }
    self.annotationSettingButton.frame = CGRectMake(CGRectGetMidX(pageFrame) - 64.0f / 2, CGRectGetMaxY(pageFrame) - 48.0f - 8.0f, 64.0f, 48.0f);
}

#pragma mark - Public method

- (void)annotationButtonPressed:(CZAnnotationButtonID)buttonID on:(BOOL)on {
    if (on) {
        if (buttonID == CZAnnotationButtonPolygon ||
            buttonID == CZAnnotationButtonCount ||
            buttonID == CZAnnotationButtonSpline ||
            buttonID == CZAnnotationButtonSplineContour) {
            [_selectTool cancelJob];
            [_selectTool setCreationMode:kCZSelectToolCreationModeNormal];
        }
    } else {
        [_selectTool cancelJob];
        
        CZDocManager *docManager = self.editingImageDocument;
        UIView *imageView = self.imageView;
        UIScrollView *scrollView = self.imageScrollView;
        
        CGSize imageSize = docManager.elementLayer.frame.size;
        CGRect rectInImage;
        if (scrollView) { // image tab
            CGRect rectInView = scrollView.bounds;
            rectInView = [scrollView convertRect:rectInView toView:imageView];
            rectInImage = [CZCommonUtils viewRectToImageRect:rectInView];
        } else {  // live tab
            CGRect rectInView = imageView.frame;
            rectInView.origin = CGPointZero;
            CGFloat scale = self.imageViewZoomScale * 2;
            rectInImage.origin = CGPointZero;
            rectInImage.size.width = rectInView.size.width * scale;
            rectInImage.size.height = rectInView.size.height * scale;
        }
        
        if (rectInImage.size.width > imageSize.width) {
            rectInImage.size.width = imageSize.width;
            if (rectInImage.origin.x < 0) {
                rectInImage.origin.x = 0;
            }
        }
        
        if (rectInImage.size.height > imageSize.height) {
            rectInImage.size.height = imageSize.height;
            if (rectInImage.origin.y < 0) {
                rectInImage.origin.y = 0;
            }
        }
        
        rectInImage.size.width /= 3.0;
        rectInImage.size.height /= 3.0;
        rectInImage.origin.x += rectInImage.size.width;
        rectInImage.origin.y += rectInImage.size.height;
        
        rectInImage = CGRectIntegral(rectInImage);
        
        if (buttonID == CZAnnotationButtonLine) {
            [_selectTool createElementByClass:[CZElementLine class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonSquare) {
            [_selectTool createElementByClass:[CZElementRectangle class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonCircle) {
            [_selectTool createElementByClass:[CZElementCircle class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonPolygon) {
            [_selectTool setCreationMode:kCZSelectToolCreationModePolygon];
        } else if (buttonID == CZAnnotationButtonPolyline) {
            [_selectTool setCreationMode:kCZSelectToolCreationModePolyline];
        } else if (buttonID == CZAnnotationButtonCount) {
            [_selectTool setCreationMode:kCZSelectToolCreationModeCount];
        } else if (buttonID == CZAnnotationButtonSpline) {
            [_selectTool setCreationMode:kCZSelectToolCreationModeSpline];
        } else if (buttonID == CZAnnotationButtonSplineContour) {
            [_selectTool setCreationMode:kCZSelectToolCreationModeSplineContour];
        } else if (buttonID == CZAnnotationButtonArrow) {
            [_selectTool createElementByClass:[CZElementArrow class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonText) {
            [_selectTool createElementByClass:[CZElementText class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonAngle) {
            [_selectTool createElementByClass:[CZElementAngle class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonDisconnectedAngle) {
            [_selectTool createElementByClass:[CZElementDisconnectedAngle class] inRect:rectInImage];
        } else if (buttonID == CZAnnotationButtonCalipers) {
            [_selectTool setCreationMode:kCZSelectToolCreationModeCalipers];
        } else if (buttonID == CZAnnotationButtonMultiCalipers) {
            [_selectTool setCreationMode:kCZSelectToolCreationModeMultiCalipers];
        } else if (buttonID == CZAnnotationButtonScaleBar) {
            CZElement *scaleBar = nil;
            for (NSUInteger i = 0; i < docManager.elementLayer.elementCount; ++i) {
                CZElement *element = [docManager.elementLayer elementAtIndex:i];
                if ([element isMemberOfClass:[CZElementScaleBar class]]) {
                    scaleBar = element;
                    break;
                }
            }
            
            if (scaleBar) {
                [_selectTool selectElement:scaleBar];
            } else {
                [_selectTool createElementByClass:[CZElementScaleBar class] inRect:CGRectZero];
            }
        } else if (buttonID == CZAnnotationButtonPCBXDirection ||
                   buttonID == CZAnnotationButtonPCBYDirection) {
            BOOL horizontal = buttonID == CZAnnotationButtonPCBYDirection;
            CZElement *lastElement = [_selectTool createElementByClass:[CZElementLine class] inRect:rectInImage horizontal:horizontal];
            
            if ([lastElement isMemberOfClass:[CZElementLine class]]) {
                CZElementLine *lineElement = (CZElementLine *)lastElement;
                [lineElement notifyWillChange];
                lineElement.measurementNameHidden = YES;
                [lineElement notifyDidChange];
            }
        }
    }
}

- (void)updateCornerButtonsByElement:(CZElement *)element {
    if ([element isMemberOfClass:[CZElementScaleBar class]]) {
        [self showCornerButtonsWithScaleBar:(CZElementScaleBar *)element];
    } else {
        for (UIButton *button in self.scaleBarCornerButtons) {
            button.hidden = YES;
        }
    }
}

- (CZSelectToolCreationMode)toolMode {
    return _selectTool.creationMode;
}

- (void)doneAction:(id)sender {
    [_selectTool cancelJob];
}

#pragma mark - Private methods

- (void)showCornerButtonsButPosition:(CZScaleBarPosition)position {
    int i = 0;
    //scaleBar Corner button hidden logic
    for (UIButton *button in self.scaleBarCornerButtons) {
        button.hidden = (position == i);
        i++;
    }
}

- (void)showCornerButtonsWithScaleBar:(CZElementScaleBar *)scaleBar {
    //Calucate scaleBar position
    CZScaleBarPosition position = [scaleBar calucationScaleBarPositionWithTolerance:scaleBar.tolerance];
    [self showCornerButtonsButPosition:position];
}

- (void)setScaleBarPosition:(CZScaleBarPosition)position {
    if ([_selectTool.selectedElement isMemberOfClass:[CZElementScaleBar class]]) {
        CZElementScaleBar *scaleBar = (CZElementScaleBar *)_selectTool.selectedElement;
        CZScaleBarSetCornerAction *action = [[CZScaleBarSetCornerAction alloc] initWithScaleBar:scaleBar newPosition:position];
        
        [self.docManager pushAction:action];
        
        CGRect rect = scaleBar.boundary;
        rect = CGRectInset(rect, -kDefaultScaleBarMargin, -kDefaultScaleBarMargin);
        rect = [CZCommonUtils imageRectToViewRect:rect];
        
        if ([self.delegate respondsToSelector:@selector(scrollRectToVisible:)]) {
            [self.delegate scrollRectToVisible:rect];
        }
    }
}

- (CZAnnotationConfigPanelControllerPosition)configPaneloControllerPasition {
    CZAnnotationConfigPanelControllerPosition position =  CZAnnotationConfigPanelControllerPositionDefault;

    CGRect selectionElementFrame = self.selectedElement.boundary;
    CGRect annotationScreenFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    CGFloat screenPanelDistance = CGRectGetWidth(annotationScreenFrame) / 2.00f - CZAnnotationConfigPanelWidth - CZANnotationConfigPanelHorizontalMargin;
    
    CGFloat newCoordinateCenterX = CGRectGetMidX(annotationScreenFrame);
    
    BOOL horizontalOverlapped = fabs(CGRectGetMinX(selectionElementFrame) - newCoordinateCenterX) >= screenPanelDistance || fabs(CGRectGetMaxX(selectionElementFrame) - newCoordinateCenterX) >= screenPanelDistance;
    BOOL positiveLeft = (newCoordinateCenterX - CGRectGetMinX(selectionElementFrame) + newCoordinateCenterX - CGRectGetMaxX(selectionElementFrame)) >= 0;
    
    if (horizontalOverlapped){
        position = positiveLeft ? CZAnnotationConfigPanelControllerPositionBottomRight : CZAnnotationConfigPanelControllerPositionBottomLeft;
    } else {
        position = CZAnnotationConfigPanelControllerPositionBottomLeft;
    }
    
    CZLogv(@"CZAnnotation config panel: %lu, [positionLeft: %d, horizontalOverlapped: %d]", (unsigned long)position, positiveLeft, horizontalOverlapped);
    
    return position;
}

#pragma mark - Action

- (void)undoButtonAction:(id)sender {
    [_selectTool cancelJob];
    
    CZDocManager *docManager = self.editingImageDocument;
    if ([docManager canUndo]) {
        [docManager undo];
    }
}

- (void)redoButtonAction:(id)sender {
    [_selectTool cancelJob];
    
    CZDocManager *docManager = self.editingImageDocument;
    if ([docManager canRedo]) {
        [docManager redo];
    }
}

- (void)cancelAnnotationJob {
    [self.selectTool cancelJob];
}

- (void)deleteAnnotationAction {
    [self.selectTool deleteSelected];
}

- (void)deleteAllAnnotationAction {
    if ([self.docManager.elementLayer elementCount] > 0) {
        [self.selectTool deselect];
        
        CZDeleteAllElementAction *action = [[CZDeleteAllElementAction alloc] init];
        action.layer = self.docManager.elementLayer;
        [self.docManager pushAction:action];
    }
}

- (void)selectScaleBarPositionAction:(id)sender {
    CZScaleBarPosition position;
    if (sender == _scaleBarCornerButtons[0]) {
        position = kCZScaleBarPositionTopLeft;
    } else if (sender == _scaleBarCornerButtons[1]) {
        position = kCZScaleBarPositionTopRight;
    } else if (sender == _scaleBarCornerButtons[2]) {
        position = kCZScaleBarPositionBottomLeft;
    } else {
        position = kCZScaleBarPositionBottomRight;
    }
    [self setScaleBarPosition:position];
}

- (void)annotationSettingAction:(CZButton *)sender{
    if (self.annotationConfigPanelController) {
        self.annotationConfigPanelController = nil;
    }
    
    CZAnnotationConfigPanelController *annotationConfigPanelController = [[CZAnnotationConfigPanelController alloc] initWithPosition:[self configPaneloControllerPasition]];
    annotationConfigPanelController.delegate = self;
    annotationConfigPanelController.dataSource = self;
    self.annotationConfigPanelController = annotationConfigPanelController;
    [self presentViewController:self.annotationConfigPanelController animated:YES completion:nil];
}

#pragma mark - Setter

- (void)setAnnotationColor:(CZColor)color {
    CZChangeElementColorAction *action =
    [[CZChangeElementColorAction alloc] initWithElement:_selectTool.selectedElement
                                               newColor:color];
    [self.docManager pushAction:action];
}

- (void)setAnnotationFillColor:(CZColor)fillColor {
    CZChangeElementFillColorAction *action =
    [[CZChangeElementFillColorAction alloc] initWithElement:_selectTool.selectedElement
                                             newStrokeColor:_selectTool.selectedElement.strokeColor
                                               newFillColor:fillColor];
    [self.docManager pushAction:action];
}

- (void)setAnnotationStokeColor:(CZColor)strokeColor fillColor:(CZColor)fillColor {
    CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
    CZChangeElementColorAction *action =
    [[CZChangeElementColorAction alloc] initWithElement:_selectTool.selectedElement
                                               newColor:strokeColor];
    [compoundAction addAction:action];
    
    CZChangeElementFillColorAction *fillColorAction =
    [[CZChangeElementFillColorAction alloc] initWithElement:_selectTool.selectedElement
                                             newStrokeColor:strokeColor
                                               newFillColor:fillColor];
    [compoundAction addAction:fillColorAction];
    
    [self.docManager pushAction:compoundAction];
}

- (void)setAnnotationLineWidth:(CGFloat)lineWidth {
    CZChangeElementSizeAction *action =
    [[CZChangeElementSizeAction alloc] initWithElement:_selectTool.selectedElement
                                               newSize:lineWidth];
    [self.docManager pushAction:action];
}

- (void)setAnnotationKeepAspect:(BOOL)keepAspect {
    _selectTool.selectedElement.keepAspect = keepAspect;
}

- (void)setAnnotationShowMeasurement:(BOOL)show {
    CZToggleMeasurementAction *action =
    [[CZToggleMeasurementAction alloc] initWithElement:_selectTool.selectedElement];
    if ([CZCommonUtils isRunningOnIPhone]) {
        _selectTool.selectedElement.measurementHidden = show;
    }
    [self.docManager pushAction:action];
}

- (void)setAnnotationMeasurementBackgroundColor:(CZColor)measurementColor {
    CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
    CZChangeMeasurementBackgroundAction *action = [[CZChangeMeasurementBackgroundAction alloc] initWithElement:_selectTool.selectedElement
                                                                                 newMeasurementBackgroundColor:measurementColor];
    [compoundAction addAction:action];

    [self.docManager pushAction:compoundAction];
}

#pragma mark - Getter

- (UILabel *)countingLabel {
    if (_countingLabel == nil) {
        _countingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _countingLabel.backgroundColor = [UIColor clearColor];
        _countingLabel.textColor = kDefaultLabelColor;
        _countingLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:144];
        _countingLabel.textAlignment = NSTextAlignmentCenter;
        _countingLabel.alpha = 0.0;
        _countingLabel.hidden = NO;
        _countingLabel.layer.cornerRadius = 3;
        _countingLabel.userInteractionEnabled = NO;
        UIView *rootView = self.view;
        [rootView addSubview:_countingLabel];
        [rootView bringSubviewToFront:_countingLabel];
        
        _countingLabel.translatesAutoresizingMaskIntoConstraints = 0;
        
        [rootView addConstraint:[NSLayoutConstraint constraintWithItem:_countingLabel
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:rootView
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1.0f
                                                              constant:0.0f]];
        
        [rootView addConstraint:[NSLayoutConstraint constraintWithItem:_countingLabel
                                                             attribute:NSLayoutAttributeCenterY
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:rootView
                                                             attribute:NSLayoutAttributeCenterY
                                                            multiplier:1.0f
                                                              constant:0.0f]];
    }
    
    return _countingLabel;
}

- (CZButton *)annotationSettingButton {
    if (_annotationSettingButton == nil) {
        _annotationSettingButton =  [CZButton buttonWithLevel:CZControlEmphasisDefault];
        _annotationSettingButton.hidden = YES;
        [_annotationSettingButton cz_setImageWithIcon:[CZIcon iconNamed:@"annotation-settings"]];
        [_annotationSettingButton addTarget:self action:@selector(annotationSettingAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _annotationSettingButton;
}

@end

@implementation CZAnnotationEditingViewController (ForSubclassEyesOnly)

- (void)appDidEnterBackground:(NSNotification *)notification {
    [self cancelAnnotationJob];
}

- (BOOL)canRedo {
    return [self.editingImageDocument canRedo];
}

- (BOOL)canUndo {
    return [self.editingImageDocument canUndo];
}

- (void)undoAction {
    if ([_selectTool canCancelJob]) {
        [_selectTool cancelJob];
    }
    
    if ([self.docManager canUndo]) {
        [self.docManager undo];
    }
}

- (void)redoAction {
    if ([_selectTool canCancelJob]) {
        [_selectTool cancelJob];
    }
    
    if ([self.docManager canRedo]) {
        [self.docManager redo];
    }
}

- (void)annotationMeasurementDoneAction:(id)sender {
    
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    if ([_selectTool respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [_selectTool layer:layer didRemoveElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {
    if ([_selectTool respondsToSelector:@selector(layer:didChangeElement:)]) {
        [_selectTool layer:layer didChangeElement:element];
    }
    
    if (_selectTool.selectedElement == element) {
        if ([element isKindOfClass:[CZElementScaleBar class]]) {
            [self updateCornerButtonsByElement:element];
        }
    }
}

#pragma mark - CZImageViewEditingDelegate methods

- (void)imageViewDocManagerModified:(UIView *)imageView {
    if ([self.delegate respondsToSelector:@selector(viewControllerDidModifyDocManager:)]) {
        [self.delegate viewControllerDidModifyDocManager:self];
    }
}

- (void)imageView:(UIView *)imageView isChangingZoom:(CGFloat)zoomOfView {
    [self.selectTool setZoomOfView:zoomOfView];
}

- (void)imageView:(UIView *)imageView didChangeToZoom:(CGFloat)zoomOfView {
    [self.selectTool setZoomOfView:zoomOfView];
}

- (CZShouldBeginTouchState)imageView:(UIView *)imageView shouldBeginTouch:(UITouch *)touch {
    CGPoint pointInView = [touch locationInView:imageView];
    return [self.selectTool shouldBeginTouch:pointInView];
}

- (BOOL)imageView:(UIView *)imageView handleTapGesture:(UIGestureRecognizer *)recognizer {
    CGPoint pointInView = [recognizer locationInView:imageView];
    return [_selectTool tap:pointInView];
}

- (BOOL)imageView:(UIView *)imageView handlePanGesture:(UIGestureRecognizer *)recognizer {
    BOOL handled = NO;
    CGPoint pointInView = [recognizer locationInView:imageView];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        handled = [_selectTool touchBegin:pointInView];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        handled = [_selectTool touchMoved:pointInView];
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        handled = [_selectTool touchEnd:pointInView];
    } else {
        handled = [_selectTool touchCancelled:pointInView];
    }
    return handled;
}

- (void)imageView:(UIView *)imageView handleTouchGesture:(UIGestureRecognizer *)gestureRecognizer {
    NSUInteger touchCounts = [gestureRecognizer numberOfTouches];
    if (touchCounts <= 0) {
        return;
    }
    
    CGPoint pointInView, pointInView2;
    if (touchCounts > 0) {
        pointInView = [gestureRecognizer locationOfTouch:0 inView:imageView];
    }
    if (touchCounts > 1) {
        pointInView2 = [gestureRecognizer locationOfTouch:1 inView:imageView];
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [_selectTool touchBegin:pointInView];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (touchCounts >= 2) {
            [_selectTool twoTouchsMoved:pointInView p2:pointInView2];
        } else {
            [_selectTool touchMoved:pointInView];
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [_selectTool touchEnd:pointInView];
    } else {
        [_selectTool touchCancelled:pointInView];
    }
}

- (BOOL)imageView:(UIView *)imageView shouldHandleDoubleTapGesture:(UIGestureRecognizer *)recognizer {
    return (_selectTool.creationMode == kCZSelectToolCreationModeNormal);
}

- (BOOL)imageView:(UIView *)imageView handleDoubleTapGesture:(UIGestureRecognizer *)recognizer {
    CGPoint pointInView = [recognizer locationInView:imageView];
    BOOL handled = [_selectTool doubleTap:pointInView];
    return handled;
}

- (CZElement *)selectedElement {
    return self.selectTool.selectedElement;
}

- (CZElement *)temporarilyHiddenElement {
    return self.selectTool.temporarilyHiddenElement;
}

#pragma mark - CZSelectToolDelgate methods

- (void)selectTool:(CZSelectTool *)selectTool didSelectElement:(CZElement *)element {
    if ([element isMemberOfClass:[CZElementScaleBar class]]) {
        CGRect rect = element.boundary;
        rect = CGRectInset(rect, -kDefaultScaleBarMargin, -kDefaultScaleBarMargin);
        rect = [CZCommonUtils imageRectToViewRect:rect];
        
        if ([self.delegate respondsToSelector:@selector(scrollRectToVisible:)]) {
            [self.delegate scrollRectToVisible:rect];
        }
    }
    self.annotationSettingButton.hidden = NO;
    [self updateCornerButtonsByElement:selectTool.selectedElement];
}

- (void)selectTool:(CZSelectTool *)selectTool willDeselectElement:(CZElement *)element {
    self.annotationSettingButton.hidden = YES;
}

- (void)selectTool:(CZSelectTool *)selectTool waitsCaliperPoint:(NSUInteger)pointIndex {
    if (pointIndex == 0) {
        [[CZToastManager sharedManager] dismissAllToastMesseges];
        [[CZToastManager sharedManager] showToastMessage:L(@"IMAGE_CALIPER_HINT_1") identifier:L(@"IMAGE_CALIPER_HINT_1") dismissAfter:0 sourceRect:CGRectZero animated:YES];
    } else if (pointIndex == 1) {
        [[CZToastManager sharedManager] dismissAllToastMesseges];
        [[CZToastManager sharedManager] showToastMessage:L(@"IMAGE_CALIPER_HINT_2") identifier:L(@"IMAGE_CALIPER_HINT_2") dismissAfter:0 sourceRect:CGRectZero animated:YES];
    } else if (pointIndex > 1) {
        [[CZToastManager sharedManager] dismissAllToastMesseges];
        switch (selectTool.creationMode) {
            case kCZSelectToolCreationModeCalipers:
                [[CZToastManager sharedManager] showToastMessage:L(@"IMAGE_CALIPER_HINT_SINGULAR_3") identifier:L(@"IMAGE_CALIPER_HINT_SINGULAR_3") dismissAfter:0 sourceRect:CGRectZero animated:YES];
                break;
            case kCZSelectToolCreationModeMultiCalipers:
                [[CZToastManager sharedManager] showToastMessage:L(@"IMAGE_CALIPER_HINT_PLURAL_3") identifier:L(@"IMAGE_CALIPER_HINT_PLURAL_3") dismissAfter:0 sourceRect:CGRectZero animated:YES];
                break;
            default:
                NSAssert(NO, @"Unknown creation mode");
                break;
        }
    }
}

- (void)selectToolDidFinishCaliper:(CZSelectTool *)selectTool {
    [[CZToastManager sharedManager] dismissAllToastMesseges];
}

- (void)selectToolDidDeselectElement:(CZSelectTool *)selectTool {
    [self updateCornerButtonsByElement:selectTool.selectedElement];
}

- (void)selectToolDidCancelJob:(CZSelectTool *)selectTool {
    
}

- (void)selectToolDidBeginEdting:(CZSelectTool *)selectTool {
    
}

- (void)selectToolDidEndEdting:(CZSelectTool *)selectTool {
    CZLogv(@"End typing mode.");
    
}

- (void)selectToolDidChangeMode:(CZSelectTool *)selectTool {
    if (selectTool.creationMode == kCZSelectToolCreationModeCalipers ||
        selectTool.creationMode == kCZSelectToolCreationModeMultiCalipers) {
        [self selectTool:selectTool waitsCaliperPoint:0];
    }
}

- (void)selectTool:(CZSelectTool *)selectTool didCountTo:(NSUInteger)count {
    UILabel *countingLabel = self.countingLabel;
    [self.view bringSubviewToFront:countingLabel];
    NSString *countingString = [NSString stringWithFormat:@"%lu", (unsigned long)count];
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:countingString
                                                                         attributes:@{NSStrokeWidthAttributeName: @-3,
                                                                                      NSStrokeColorAttributeName: [UIColor blackColor],
                                                                                      NSForegroundColorAttributeName: [UIColor whiteColor]}];
    countingLabel.attributedText = attributedText;
    [countingLabel sizeToFit];
    
    // cancel previous animation
    [UIView setAnimationBeginsFromCurrentState:YES];
    countingLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
    countingLabel.alpha = 1.0;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         countingLabel.transform = CGAffineTransformIdentity;
                         countingLabel.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [UIView animateWithDuration:1.0
                                                   delay:1.0
                                                 options:UIViewAnimationOptionCurveEaseInOut
                                              animations:^ {
                                                  countingLabel.alpha = 0.0;
                                                  countingLabel.transform = CGAffineTransformIdentity;
                                              }
                                              completion:NULL];
                         }
                     }];
}

- (void)selectToolDidFinishCounting:(CZSelectTool *)selectTool {
    UILabel *countingLabel = self.countingLabel;
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{}
                     completion:^(BOOL finished) {
                         if (finished) {
                             countingLabel.alpha = 0.0;
                         }
                     }];
}

#pragma mark - CZAnnotationConfigPanelControllerDataSource

- (nonnull CZElement *)selectionElement {
    return self.selectTool.selectedElement;
}

#pragma mark - CZAnnotationConfigPanelControllerDelegate

- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectColor:(CZColor)color {
    [self setAnnotationColor:color];
    
    if (![CZCommonUtils isRunningOnIPhone]) {
        CZColor defaultColor = [[CZDefaultSettings sharedInstance] color];
        if (!CZColorEqualToColor(defaultColor, color)) {
            CZUserInteractionTracker *tracker = [CZUserInteractionTracker defaultTracker];
            NSString *stokeColor = NSStringFromColor(color);
            NSString *fillColor = @"";
            NSString *sizeString = @"";
            NSDictionary *parameters = @{@"stroke_color"  : stokeColor,
                                         @"fill_color" : fillColor,
                                         @"size" : sizeString};
            [tracker logEventWithCategory:kCZUIActionEventCategory
                                    event:@"annotation_setting_button_pressed"
                                     time:[NSDate date]
                               parameters:parameters
                         postNotification:YES];
        }
    }

}

- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectSize:(CZElementSize)size {
    [self setAnnotationLineWidth:size];

    if (![CZCommonUtils isRunningOnIPhone]) {
        CZElementSize defaultSize = [[CZDefaultSettings sharedInstance] elementSize];
        if (defaultSize != size) {
            CZUserInteractionTracker *tracker = [CZUserInteractionTracker defaultTracker];
            NSString *stokeColor = @"";
            NSString *fillColor = @"";
            NSString *sizeString = [NSString stringWithFormat:@"%lu", (unsigned long)size];
            
            NSDictionary *parameters = @{@"stroke_color": stokeColor, @"fill_color": fillColor, @"size": sizeString};
            [tracker logEventWithCategory:kCZUIActionEventCategory
                                    event:@"annotation_setting_button_pressed"
                                     time:[NSDate date]
                               parameters:parameters
                         postNotification:YES];
        }
    }
}

- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didSelectBackgroundColor:(CZColor)backgroundColor {
    if ([self.selectionElement isMeasurementInfoAvailable]) {
        [self setAnnotationMeasurementBackgroundColor:backgroundColor];
    } else {
        [self setAnnotationFillColor:backgroundColor];
    }
    
    if (![CZCommonUtils isRunningOnIPhone]) {
        CZColor defaultBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        if (!CZColorEqualToColor(defaultBackgroundColor, backgroundColor)) {
            CZUserInteractionTracker *tracker = [CZUserInteractionTracker defaultTracker];
            NSString *fillColorString = NSStringFromColor(backgroundColor);
            NSString *sizeString = @"";
            NSString *strokeColor = @"";
            NSDictionary *parameters = @{@"stroke_color": strokeColor, @"fill_color": fillColorString, @"size": sizeString};
            [tracker logEventWithCategory:kCZUIActionEventCategory
                                    event:@"annotation_setting_button_pressed"
                                     time:[NSDate date]
                               parameters:parameters
                         postNotification:YES];
        }
    }
}

- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController didChangeMeasurementVisiable:(BOOL)visiable {
    [self setAnnotationShowMeasurement:visiable];
}

- (void)annotationConfigPanelController:(CZAnnotationConfigPanelController *)annotationConfigPanelController keepAspect:(BOOL)keepAspect {
       [self setAnnotationKeepAspect:keepAspect];
}

- (void)annotationConfigPanelControllerDidDeleteAnnotation:(CZAnnotationConfigPanelController *)annotationConfigPanelController {
    [self deleteAnnotationAction];
    [self.annotationConfigPanelController dismissViewControllerAnimated:YES completion:nil];
    
}

@end
