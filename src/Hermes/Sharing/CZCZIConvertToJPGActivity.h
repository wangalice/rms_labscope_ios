//
//  CZCZIConvertToJPGActivity.h
//  Labscope
//
//  Created by Ralph Jin on 4/8/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const CZActivityTypeCZIConvertToJPG;
extern NSString * const CZActivityTypeCZIConvertToTIF;

/// abstract class
@interface CZCZIConvertActivity : UIActivity

@end

@interface CZCZIConvertToJPGActivity : CZCZIConvertActivity

@end

@interface CZCZIConvertToTIFActivity : CZCZIConvertActivity

@end
