//
//  CZOpenInOtherAppActivity.h
//  Matscope
//
//  Created by Ralph Jin on 5/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const CZActivityTypeOpenInOtherApp;

@protocol CZOpenInOtherAppActivityDelegate;

@interface CZOpenInOtherAppActivity : UIActivity

@property (nonatomic, assign) id<CZOpenInOtherAppActivityDelegate> delegate;

- (void)preparePresentFromRect:(CGRect)rect inView:(UIView *)view;
- (void)preparePresentFromBarButtonItem:(UIBarButtonItem *)barButtonItem;

/** @return NO if there are no applications that can open the item. */
- (BOOL)present;
- (void)dismiss;

@end

@protocol CZOpenInOtherAppActivityDelegate <NSObject>
@optional
- (void)openInOtherAppDidFinished:(CZOpenInOtherAppActivity *)activity;
@end
