//
//  CZShareFileItem.m
//  Hermes
//
//  Created by Mike Wang on 7/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZShareFileItem.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZMailActivity.h"
#import "CZSaveToCameraRollActivity.h"
#import "CZCZIConvertToJPGActivity.h"
#import "CZOpenInOtherAppActivity.h"

#define kMaxImageSizeForExporting 2560

@interface CZShareFileItem ()

@property (nonatomic, retain) NSURL *fileURL;
@property (nonatomic, assign) uint64_t allocatedSize;
@property (nonatomic, assign) BOOL allocatedOnDisk;

+ (uint64_t)fileSizeFromURL:(NSURL *)fileURL;

- (id)initWithFilePath:(NSString *)path;

@end

// PDF item class

@interface CZShareFileItemPDF : CZShareFileItem
@end

// Image item class

@interface CZShareFileItemImage : CZShareFileItem

@property (nonatomic, retain) NSURL *tempFileURL;

+ (NSURL *)createTemporaryJPEGFileFromImageFile:(NSURL *)fileUrl
                                    forActivity:(NSString *)activityType
                                          error:(BOOL *)error;

@end

// MP4 item class

@interface CZShareFileItemMP4 : CZShareFileItem
@end

// NSData item class

@interface CZShareFileItemData : CZShareFileItem

- (id)initWithData:(NSData *)data andName:(NSString *)fileName;

@property (nonatomic, retain) NSData *data;

@end

@implementation CZShareFileItem

+ (CZShareFileItem *)itemWithFilePath:(NSString *)path {
    CZFileFormat type = [CZCommonUtils fileFormatForExtension:path.pathExtension];
    CZShareFileItem *item = nil;
    switch (type) {
        case kCZFileFormatCZI:
        case kCZFileFormatPNG:
        case kCZFileFormatJPEG:
        case kCZFileFormatTIF:
            item = [[CZShareFileItemImage alloc] initWithFilePath:path];
            break;
        case kCZFileFormatPDF:
            item = [[CZShareFileItemPDF alloc] initWithFilePath:path];
            break;
        case kCZFileFormatMP4:
            item = [[CZShareFileItemMP4 alloc] initWithFilePath:path];
            break;
        default:
            item = [[CZShareFileItem alloc] initWithFilePath:path];
            break;
    }
    return item;
}

+ (CZShareFileItem *)itemWithData:(NSData *)data andName:(NSString *)fileName {
    return [[CZShareFileItemData alloc] initWithData:data andName:fileName];
}

+ (uint64_t)fileSizeFromURL:(NSURL *)fileURL {
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSDictionary *dic = [defaultManager attributesOfItemAtPath:[fileURL relativePath] error:nil];
    NSNumber *fileSize = [dic valueForKey:NSFileSize];
    return [fileSize unsignedLongLongValue];
}

- (id)initWithFilePath:(NSString *)path {
    id placeholdItem;
    
    NSUInteger width = kCZShareItemImageCodeOther;
    CZFileFormat format = [CZCommonUtils fileFormatForExtension:[path pathExtension]];
    if (format == kCZFileFormatCZI) {
        width = kCZShareItemImageCodeCZI;
        placeholdItem = [CZCommonUtils blankImageWithSize:CGSizeMake(width, 1)];
    } else {
        placeholdItem = [NSURL fileURLWithPath:path];
    }
    
    self = [super initWithPlaceholderItem:placeholdItem];
    
    if (self) {
        _fileURL =[NSURL fileURLWithPath:path];
    }
    
    return self;
}

- (NSArray *)includedActivityTypes {
    return @[UIActivityTypeAirDrop, CZActivityTypeMail];
}

- (id)item {
    if ([self.delegate respondsToSelector:@selector(shouldExportShareFileItem:activityType:)]) {
        BOOL shouldExport = [self.delegate shouldExportShareFileItem:self activityType:self.activityType];
        if (!shouldExport) {
            return [NSURL fileURLWithPath:@""];
        }
    }

    id item = [self createItemForActivityType:self.activityType];
    
    if ([item isKindOfClass:[NSURL class]]) {
        self.allocatedSize = [CZShareFileItem fileSizeFromURL:(NSURL *)item];
        self.allocatedOnDisk = YES;
    } else if ([item isKindOfClass:[NSData class]]) {
        NSData *data = (NSData *)item;
        self.allocatedSize = [data length];
        self.allocatedOnDisk = NO;
    } else {
        self.allocatedSize = 0;
        self.allocatedOnDisk = NO;
    }

    if ([self.delegate respondsToSelector:@selector(shareFileItemDidCreated:activityType:success:)]) {
        [self.delegate shareFileItemDidCreated:self activityType:self.activityType success:(item != nil)];
    }

    if ([self.delegate respondsToSelector:@selector(shouldExportShareFileItem:activityType:)]) {
        BOOL shouldExport = [self.delegate shouldExportShareFileItem:self activityType:self.activityType];
        if (!shouldExport) {
            return [NSURL fileURLWithPath:@""];
        }
    }

    return item;
}

- (id)createItemForActivityType:(NSString *)activityType {
    return self.fileURL;
}

@end

@implementation CZShareFileItemImage

+ (NSURL *)createTemporaryJPEGFileFromImageFile:(NSURL *)fileUrl
                                    forActivity:(NSString *)activityType
                                          error:(BOOL *)error {
    // Don't need to create temporary file for mail sending.
    if ([activityType isEqualToString:CZActivityTypeMail] ||
        [activityType isEqualToString:CZActivityTypeOpenInOtherApp] ||
        [activityType isEqualToString:CZActivityTypeCZIConvertToJPG] ||
        [activityType isEqualToString:CZActivityTypeCZIConvertToTIF]) {
        if (error) {
            *error = NO;
        }
        return nil;
    }
    
    CZFileFormat format = [CZCommonUtils fileFormatForExtension:[fileUrl pathExtension]];
    
    BOOL isPostActivity = NO;
    CGSize targetSize = CGSizeZero;
    
    if ([activityType isEqualToString:UIActivityTypePostToFacebook] ||
        [activityType isEqualToString:UIActivityTypePostToWeibo]) {
        isPostActivity = YES;
        targetSize = CGSizeMake(1920, 1920);
    } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
        isPostActivity = YES;
        targetSize = CGSizeMake(1024, 2048);
    }
    
    // No need to save as temp file if not sharing to SNS and not CZI format.
    if (format != kCZFileFormatCZI) {
        if (!isPostActivity) {
            if (error) {
                *error = NO;
            }
            return nil;
        }
        
        // Optimization: no need to generate temporary file if the file is
        // already smaller than the limitation of SNS site.
        CGSize imageSize = [CZCommonUtils imageSizeFromFileURL:fileUrl];
        if (imageSize.width < targetSize.width && imageSize.height < targetSize.height) {
            if (error) {
                *error = NO;
            }
            return nil;
        }
    }
    
    NSString *tempFilePath = nil;
    
    @autoreleasepool {
        NSString *filePath = fileUrl.relativePath;
        
        CZDocManager *docManager = [CZDocManager newDocManagerFromFilePath:filePath];
        if (docManager == nil) {
            if (error) {
                *error = YES;
            }
            return nil;
        }
        
        UIImage *image = [docManager imageWithBurninAnnotations];
        if (image == nil) {
            if (error) {
                *error = YES;
            }
            return nil;
        }
        
        // Resize if target size was set.
        UIImage *resized = image;
        if (!CGSizeEqualToSize(targetSize, CGSizeZero)) {
            resized = [CZCommonUtils scaleImage:image intoSize:targetSize];
            
            // Re-calculate the scaling value according to old and new resolution.
            float newScaling = docManager.imageProperties.scaling * image.size.width / targetSize.width;
            [docManager.imageProperties updateScaling:newScaling];
        }
        if (resized == nil) {
            if (error) {
                *error = YES;
            }
            return nil;
        }
        
        NSData *imageData = [docManager.imageProperties JPEGRepresentationOfImage:resized
                                                              compression:1.0];
        
        if (imageData == nil) {
            if (error) {
                *error = YES;
            }
            return nil;
        }
        
        // get file name and replace with jpg extension
        NSString *fileName = [[filePath lastPathComponent] stringByDeletingPathExtension];
        fileName = [fileName stringByAppendingPathExtension:@"jpg"];
        
        // generate an temporary file path
        tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        
        // write to the temporary file
        BOOL success = [imageData writeToFile:tempFilePath atomically:YES];
        if (!success) {
            if (error) {
                *error = YES;
            }
            return nil;
        }
    }
    
    NSURL *result = [NSURL fileURLWithPath:tempFilePath];
    
    if (error) {
        *error = NO;
    }
    return result;
}

- (void)dealloc {
    if (_tempFileURL) {
        [[NSFileManager defaultManager] removeItemAtURL:_tempFileURL error:nil];
    }
}

- (NSArray *)includedActivityTypes {
    return @[UIActivityTypePostToFacebook,
                       UIActivityTypePostToTwitter,
                       UIActivityTypePostToWeibo,
                       UIActivityTypePrint,
                       UIActivityTypeAirDrop,
                       CZActivityTypeMail,
                       CZActivityTypeSaveToCameraRoll,
                       CZActivityTypeOpenInOtherApp,
                       UIActivityTypePostToFlickr];
}

- (id)createItemForActivityType:(NSString *)activityType {
    BOOL error;
    NSURL *tempFileUrl = [CZShareFileItemImage createTemporaryJPEGFileFromImageFile:self.fileURL
                                                                        forActivity:activityType
                                                                              error:&error];
    if (tempFileUrl != nil) {
        self.tempFileURL = tempFileUrl;
        return self.tempFileURL;
    } else {
        if (error) {
            return nil;
        } else {
            return self.fileURL;
        }
    }
}

@end

@implementation CZShareFileItemPDF

- (NSArray *)includedActivityTypes {
    return @[UIActivityTypePrint,
             UIActivityTypeAirDrop,
             CZActivityTypeMail];
}

@end

@implementation CZShareFileItemMP4

- (NSArray *)includedActivityTypes {
    return @[UIActivityTypeAirDrop,
             CZActivityTypeMail,
             CZActivityTypeSaveToCameraRoll,
             UIActivityTypePostToFlickr];
}

@end

@implementation CZShareFileItemData

- (id)initWithData:(NSData *)data andName:(NSString *)fileName {
    self = [super initWithFilePath:fileName];
    if (self) {
        _data = data;
    }
    return self;
}

- (NSArray *)includedActivityTypes {
    return @[UIActivityTypePostToFacebook,
             UIActivityTypePostToTwitter,
             UIActivityTypePostToWeibo,
             UIActivityTypePrint,
             UIActivityTypeAirDrop,
             CZActivityTypeMail,
             CZActivityTypeSaveToCameraRoll,
             UIActivityTypePostToFlickr];
}

- (id)createItemForActivityType:(NSString *)activityType {
    return self.data;
}

@end

@implementation CZShareSpecialTextItem

- (id)item {
    NSString *productNameKey = @"ABOUT_PRODUCT_BIO_NAME";
    
    if ([self.activityType isEqualToString:UIActivityTypePostToFacebook] ||
        [self.activityType isEqualToString:UIActivityTypePostToTwitter] ||
        [self.activityType isEqualToString:UIActivityTypePostToWeibo] ||
        [self.activityType isEqualToString:UIActivityTypePostToFlickr]) {
        return [NSString stringWithFormat:L(@"SHARE_MESSAGE_FOOTER"), L(productNameKey)];
    } else {
        return @"";
    }
}

@end
