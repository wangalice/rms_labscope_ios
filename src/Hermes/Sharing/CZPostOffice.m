//
//  CZPostOffice.m
//  Hermes
//
//  Created by Halley Gu on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZPostOffice.h"
#import <Photos/Photos.h>
#import <MessageUI/MessageUI.h>
#import "CZUIDefinition.h"
#import "CZShareFileItem.h"
#import "MBProgressHUD+Hide.h"
#import "CZAppDelegate.h"
#import "CZAlertController.h"
#import "CZShareFileItem.h"
#import "CZMailActivity.h"
#import "CZSaveToCameraRollActivity.h"
#import "CZCZIConvertToJPGActivity.h"
#import "CZOpenInOtherAppActivity.h"
#import "CZPopoverController.h"

#define kDefaultSharingPannelContentSize CGSizeMake(805.0/2.0, 900.0/2.0)

@interface CZPostOffice () <
CZPopoverControllerDelegate,
CZShareItemDelegate,
CZOpenInOtherAppActivityDelegate
>
{
    BOOL _stopExporting;
    NSUInteger _numberOfAlertShown;
    NSUInteger _processedCount;
}

@property (nonatomic, strong) NSMutableArray *shareItems;
@property (nonatomic, assign) uint64_t totalAllocatedSharedItemSize;
@property (nonatomic, assign) uint64_t memoryAllocatedSharedItemSize;
@property (nonatomic, assign) uint64_t freeSpaceBeforeSharing;  // free disk space before sharing
@property (nonatomic, strong) CZPopoverController *popover;
@property (atomic, strong) MBProgressHUD *sharingHUD;
@property (atomic, assign) BOOL needsHUD;
@property (nonatomic, assign, readwrite) BOOL failed;

@property (nonatomic, retain) CZOpenInOtherAppActivity *openInOtherAppActivity;
@property (nonatomic, retain) CZMailActivity *mailActivity;

@end

@implementation CZPostOffice

- (id)init {
    self = [super init];
    if (self) {
        _shareItems = [[NSMutableArray alloc] init];
        
        // By default, social network options are hidden.
        _socialNetworksEnabled = NO;
    }
    return self;
}

- (void)dealloc {
    _openInOtherAppActivity.delegate = nil;
}

- (void)presentActivityViewControllerFromBarButtonItem:(CZBarButtonItem *)item {
    [self dismissPopovers];  // cancel unfinished presenting, maybe.
    
    UIActivityViewController *activityView = [self newActivityControllerFromShareItems];
    [self.openInOtherAppActivity preparePresentFromBarButtonItem:item];
    
    if (!activityView) {
        self.openInOtherAppActivity.delegate = nil;
        self.openInOtherAppActivity = nil;
        self.mailActivity = nil;
        return;
    }
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:activityView];
    popover.delegate = self;
    popover.backgroundColor = [UIColor whiteColor];

    [popover presentFromBarButtonItem:item animated:YES completion:nil];
    self.popover = popover;
}

- (void)presentActivityViewControllerFromToolbarItem:(CZToolbarItem *)item {
    [self dismissPopovers];  // cancel unfinished presenting, maybe.
    
    UIActivityViewController *activityView = [self newActivityControllerFromShareItems];
    [self.openInOtherAppActivity preparePresentFromRect:item.view.bounds inView:item.view];
    
    if (!activityView) {
        self.openInOtherAppActivity.delegate = nil;
        self.openInOtherAppActivity = nil;
        self.mailActivity = nil;
        return;
    }
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:activityView];
    popover.delegate = self;
    popover.backgroundColor = [UIColor whiteColor];
    
    [popover presentFromToolbarItem:item animated:YES completion:nil];
    self.popover = popover;
}

- (void)addShareItemFromPath:(NSString *)path {
    CZShareFileItem *item = [CZShareFileItem itemWithFilePath:path];
    if (item) {
        [item setDelegate:self];
        [self.shareItems addObject:item];
    }
}

- (void)addShareItemFromData:(NSData *)data withName:(NSString *)fileName {
    CZShareFileItem *item = [CZShareFileItem itemWithData:data andName:fileName];
    if (item) {
        [item setDelegate:self];
        [self.shareItems addObject:item];
    }
}

- (BOOL)canDismiss {
    return _popover || _openInOtherAppActivity || _mailActivity;
}

- (void)dismiss {
    [self reset];
    [self dismissPopovers];
}

- (void)reset {
    [self.shareItems removeAllObjects];
    _totalAllocatedSharedItemSize = 0;
    _memoryAllocatedSharedItemSize = 0;
    _freeSpaceBeforeSharing = [CZCommonUtils freeSpaceInBytes];
    _processedCount = 0;
    _stopExporting = NO;
    _failed = NO;
    _numberOfAlertShown = 0;
}

- (BOOL)hasShareItem {
    return self.shareItems.count > 0;
}

#pragma mark - CZShareItemDelegate methods

- (void)shareFileItemDidCreated:(CZShareFileItem *)shareFileItem activityType:(NSString *)activityType success:(BOOL)success {
    if (!success) {
        self.failed = YES;
        _stopExporting = YES;  // stop exporting if any error happens
        
        NSURL *fileURL = shareFileItem.fileURL;
        NSString *filePath = fileURL.relativePath;
        NSString *message = [[NSString alloc] initWithFormat:L(@"ERROR_SHARE_IMAGE_FORMAT"), [filePath lastPathComponent]];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:message level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        });
    }
    
    _totalAllocatedSharedItemSize += shareFileItem.allocatedSize;
    if (!shareFileItem.allocatedOnDisk) {
        _memoryAllocatedSharedItemSize += shareFileItem.allocatedSize;
    }
    _processedCount++;
    if (_processedCount >= self.shareItems.count) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self hideBusyIndicator];
        });
    }
}

- (BOOL)shouldExportShareFileItem:(CZShareFileItem *)shareFileItem activityType:(NSString *)activityType {
    if (_stopExporting) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self hideBusyIndicator];
        });
        
        return NO;
    }
    
    const uint64_t kShareItemMaxImageSizeForPrintAndExport = 200 * 1024 * 1024;  // 200 MB
    
    if ([activityType isEqualToString:CZActivityTypeMail]) {
        const NSUInteger kMaxTotalSize = 25 * 1024 * 1024; // 25 MB
        
        if (_totalAllocatedSharedItemSize > kMaxTotalSize) {
            _stopExporting = YES;
            self.mailActivity.lastError = L(@"ERROR_MAIL_EXCEED_SIZE_LIMIT");
        } else if (![MFMailComposeViewController canSendMail]) {
            _stopExporting = YES;
            self.mailActivity.lastError = L(@"ERROR_NO_MAIL_ACCOUNT");
        }
    } else if ([activityType isEqualToString:CZActivityTypeSaveToCameraRoll]) {
        if (_memoryAllocatedSharedItemSize >= kShareItemMaxImageSizeForPrintAndExport) {
            _stopExporting = YES;
            // TODO: so far, this case will not happen, but if it do happen in future,
            //   it should show error message that saving > 200ms images to camera roll
            //   at once costs too much memory for iPad's low physical memory, please
            //   export them separately.
        }
    } else if ([activityType isEqualToString:UIActivityTypePrint]) {
        if (_totalAllocatedSharedItemSize >= kShareItemMaxImageSizeForPrintAndExport) {
            _stopExporting = YES;
            [self showErrorAlertAsync:L(@"ERROR_PRINT_EXPORT_EXCEED_SIZE_LIMIT")];
        }
    }
    
    if (_stopExporting) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self hideBusyIndicator];
        });
        
        return NO;
    }
    
    if (_processedCount < self.shareItems.count) {
        [self showBusyIndicator];
    }

    return YES;
}

#pragma mark - CZPopoverControllerDelegate

- (void)popoverControllerDidDismiss:(CZPopoverController *)popoverController {
    [self reset];
    if (self.popover == popoverController) {
        self.popover.delegate = nil;
        self.popover = nil;
    }
}

- (void)popoverControllerWillDismiss:(CZPopoverController *)popoverController {
    if ([self.delegate respondsToSelector:@selector(postOfficeDidDismissActivityController:)]) {
        [self.delegate postOfficeDidDismissActivityController:self];
    }
}

#pragma mark - CZOpenInOtherAppActivityDelegate

- (void)openInOtherAppDidFinished:(CZOpenInOtherAppActivity *)activity {
    if (activity == self.openInOtherAppActivity) {
        self.openInOtherAppActivity.delegate = nil;
        self.openInOtherAppActivity = nil;
        
        if ([_delegate respondsToSelector:@selector(postOfficeDidDismissActivityController:)]) {
            [_delegate postOfficeDidDismissActivityController:self];
        }
    }
}

#pragma mark - Private methods

- (NSMutableSet <UIActivityType>*)execludeTypeSet {
    if (self.shareItems.count <= 0) {
        return nil;
    }
    
    NSMutableSet *includedTypeSet = [[NSMutableSet alloc] initWithCapacity:10];
    
    for (id item in self.shareItems) {
        NSArray *supportedTypes = nil;
        
        CZShareFileItem *shareFileItem = (CZShareFileItem *)item;
        supportedTypes = [shareFileItem includedActivityTypes];
        
        if (includedTypeSet.count == 0) {
            [includedTypeSet addObjectsFromArray:supportedTypes];
        } else {
            NSSet *tempSet = [[NSSet alloc] initWithArray:supportedTypes];
            [includedTypeSet intersectSet:tempSet];
        }
    }
    
    __block NSMutableSet *excludeTypeSet = [NSMutableSet setWithObjects:
                                            UIActivityTypePostToFacebook,
                                            UIActivityTypePostToTwitter,
                                            UIActivityTypePostToWeibo,
                                            UIActivityTypeMessage,
                                            UIActivityTypeMail,
                                            UIActivityTypePrint,
                                            UIActivityTypeAirDrop,
                                            UIActivityTypeCopyToPasteboard,
                                            UIActivityTypeAssignToContact,
                                            UIActivityTypeSaveToCameraRoll,
                                            CZActivityTypeMail,
                                            CZActivityTypeSaveToCameraRoll, nil];
    
    // exclude what we don't support
    [excludeTypeSet addObjectsFromArray:@[UIActivityTypeAddToReadingList,  // iOS 7 avaiable
                                          UIActivityTypePostToVimeo,
                                          UIActivityTypePostToTencentWeibo
                                          ]];
    //FIXME: That will lead crash happen here. If you want to available it, please reference
    //App extensions: https://developer.apple.com/library/content/documentation/General/Conceptual/ExtensibilityPG/index.html#//apple_ref/doc/uid/TP40014214-CH20-SW1 &  https://developer.apple.com/documentation/uikit/uiactivityviewcontroller
    if (@available(iOS 11.0,*)) {
        [excludeTypeSet addObjectsFromArray:@[UIActivityTypeMarkupAsPDF]];
    }
    
    [excludeTypeSet minusSet:includedTypeSet];
    
    // If user closed the permission for the app to access the camera roll,
    // exclude the option "Save To Camera Roll".
    PHAuthorizationStatus authorizationStatus =  [PHPhotoLibrary authorizationStatus];
    if (authorizationStatus == PHAuthorizationStatusDenied || authorizationStatus == PHAuthorizationStatusRestricted) {
        [excludeTypeSet addObject:UIActivityTypeSaveToCameraRoll];
        [excludeTypeSet addObject:CZActivityTypeSaveToCameraRoll];
    }
    
    if (![[CZDefaultSettings sharedInstance] shareToMailEnabled]) {
        [excludeTypeSet addObject:UIActivityTypeMail];
        [excludeTypeSet addObject:CZActivityTypeMail];
    }
    
    if (!self.convertToJPGEnabled) {
        [excludeTypeSet addObject:CZActivityTypeCZIConvertToJPG];
        [excludeTypeSet addObject:CZActivityTypeCZIConvertToTIF];
    }
    
    // If social network sharing is disabled for this post office instance,
    // do not show the options to the user.
    if (!self.socialNetworksEnabled) {
        [excludeTypeSet addObject:UIActivityTypePostToFacebook];
        [excludeTypeSet addObject:UIActivityTypePostToTwitter];
        [excludeTypeSet addObject:UIActivityTypePostToWeibo];
        [excludeTypeSet addObject:UIActivityTypePostToFlickr];
    } else {
        // If user doesn't want to share to a certain destination, do not show
        // the option to him.
        if (![[CZDefaultSettings sharedInstance] shareToScailNetworkEnabled]) {
            [excludeTypeSet addObject:UIActivityTypePostToFacebook];
            [excludeTypeSet addObject:UIActivityTypePostToTwitter];
            [excludeTypeSet addObject:UIActivityTypePostToWeibo];
            [excludeTypeSet addObject:UIActivityTypePostToFlickr];
        }
    }
    return excludeTypeSet;
}

- (UIActivityViewController *)newActivityControllerFromShareItems {
    NSMutableSet <UIActivityType> *exludeTypeSet = [self execludeTypeSet];
    // Add a special text item into the array for footer message when sharing
    // via Facebook, Twitter and Weibo.
    CZShareSpecialTextItem *footerText = [[CZShareSpecialTextItem alloc] initWithPlaceholderItem:@"Placeholder String"];
    [self.shareItems addObject:footerText];
    ++_processedCount; // Count this text as processed.
    
    self.needsHUD = YES;
    
    CZMailActivity *mailActivity = [[CZMailActivity alloc] init];
    CZSaveToCameraRollActivity *saveToCameraRollActivity = [[CZSaveToCameraRollActivity alloc] init];
    CZCZIConvertActivity *convertJPGActivity = [[CZCZIConvertToJPGActivity alloc] init];
    CZCZIConvertActivity *convertTIFActivity = [[CZCZIConvertToTIFActivity alloc] init];
    CZOpenInOtherAppActivity *openInOtherAppActivity = [[CZOpenInOtherAppActivity alloc] init];
    openInOtherAppActivity.delegate = self;
    
    self.mailActivity = mailActivity;
    self.openInOtherAppActivity.delegate = nil;
    self.openInOtherAppActivity = openInOtherAppActivity;
    
    NSArray *applicationActivities = @[mailActivity,
                                       saveToCameraRollActivity,
                                       convertJPGActivity,
                                       convertTIFActivity,
                                       openInOtherAppActivity];
    
    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:self.shareItems
                                                                               applicationActivities:applicationActivities];
    activityView.preferredContentSize = kDefaultSharingPannelContentSize;

    activityView.excludedActivityTypes = [exludeTypeSet allObjects];
    activityView.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        [self hideBusyIndicator];
        
        BOOL finished = YES;
        if ([activityType isEqualToString:CZActivityTypeOpenInOtherApp]) {
            finished = ![self.openInOtherAppActivity present];
            // TODO: if finished == YES, means no app can open it, maybe alert user.
        }
        
        self.mailActivity = nil;
        
        if (finished) {
            self.openInOtherAppActivity.delegate = nil;
            self.openInOtherAppActivity = nil;
            
            if ([_delegate respondsToSelector:@selector(postOfficeDidDismissActivityController:)]) {
                [_delegate postOfficeDidDismissActivityController:self];
            }
        }
        
        if (self.popover) {
            [self.popover dismissViewControllerAnimated:YES completion:nil];
            self.popover.delegate = nil;
            self.popover = nil;
        }
        
        if (!completed) {
            if ([activityType isEqualToString:CZActivityTypeSaveToCameraRoll]) {
                [self showErrorAlertAsync:L(@"ERROR_SAVE_TO_CAMERA_ROLL")];
            }
        }
    };
    
    activityView.view.backgroundColor = [UIColor whiteColor];
    return activityView;
}

- (void)showBusyIndicator {
    @synchronized (self) {
        if (self.sharingHUD || !self.needsHUD) {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            if (self.sharingHUD || !self.needsHUD) {
                return;
            }
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
            [hud setColor:kSpinnerBackgroundColor];
            [hud setLabelText:L(@"BUSY_INDICATOR_PLEASE_WAIT")];
            self.sharingHUD = hud;
        });
    }
}

- (void)showErrorAlertAsync:(NSString *)errorMessage {
    if (_numberOfAlertShown < 1) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self hideBusyIndicator];
            
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:errorMessage level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        });
        
        _numberOfAlertShown++;
    }
}

- (void)hideBusyIndicator {
    @synchronized (self) {
        self.needsHUD = NO;
        if (self.sharingHUD) {
            [self.sharingHUD dismiss:YES];
            self.sharingHUD = nil;
        }
    }
}

- (void)dismissPopovers {
    [self.mailActivity dismiss];
    self.mailActivity = nil;
    
    [self.openInOtherAppActivity dismiss];
    self.openInOtherAppActivity.delegate = nil;
    self.openInOtherAppActivity = nil;
    
    if (_popover) {
        [self.popover dismissViewControllerAnimated:YES completion:nil];
        self.popover.delegate = nil;
        self.popover = nil;
    }
}

@end
