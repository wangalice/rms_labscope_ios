//
//  CZOpenInOtherAppActivity.m
//  Matscope
//
//  Created by Ralph Jin on 5/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZOpenInOtherAppActivity.h"

NSString * const CZActivityTypeOpenInOtherApp = @"CZActivityTypeOpenInOtherApp";

@interface CZOpenInOtherAppActivity () <UIDocumentInteractionControllerDelegate>

@property (nonatomic, retain) UIDocumentInteractionController *interactionController;
@property (nonatomic, assign) UIView *view;
@property (nonatomic, assign) CGRect popOverRect;
@property (nonatomic, assign) UIBarButtonItem *barButtonItem;
@property (nonatomic, retain) NSTimer *timer;

@end

@implementation CZOpenInOtherAppActivity

- (void)preparePresentFromRect:(CGRect)rect inView:(UIView *)view {
    self.view = view;
    self.popOverRect = rect;
    self.barButtonItem = nil;
}

- (void)preparePresentFromBarButtonItem:(UIBarButtonItem *)barButtonItem {
    self.barButtonItem = barButtonItem;
    self.view = nil;
}

- (NSString *)activityType {
    return CZActivityTypeOpenInOtherApp;
}

- (NSString *)activityTitle {
    return L(@"OPEN_IN_OTHER_APP");
}

- (UIImage *)activityImage {
    return [UIImage imageNamed:A(@"other-activities")];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    BOOL canPerform = YES;
    
    NSUInteger imageCount = 0;
    for (id object in activityItems) {
        if ([object isKindOfClass:[UIImage class]]) {
            imageCount++;
        } else if ([object isKindOfClass:[NSURL class]]) {
            imageCount++;
        }
    }
    
    return canPerform && (imageCount == 1);
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSURL *fileURL = nil;
    for (id item in activityItems) {
        if ([item isKindOfClass:[NSURL class]]) {
            fileURL = item;
            break;
        }
    }
    
    if (fileURL) {
        self.interactionController.delegate = nil;
        self.interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
        self.interactionController.delegate = self;
    }
}

- (UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    [self activityDidFinish:YES];
}

- (BOOL)present {    
    BOOL success = NO;
    
    if (self.view) {
        success = [self.interactionController presentOpenInMenuFromRect:self.popOverRect
                                                                 inView:self.view
                                                               animated:YES];
    } else {
        success = [self.interactionController presentOpenInMenuFromBarButtonItem:self.barButtonItem
                                                                        animated:YES];
    }
    
    return success;
}

- (void)dismiss {
    [self.interactionController dismissMenuAnimated:NO];
}

- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller {
    if (_timer == nil) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5  // 0.5 second timeout
                                                      target:self
                                                    selector:@selector(handleTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application {
    [_timer invalidate];
    self.timer = nil;
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application {
    [self notifyFinished];
}

- (void)notifyFinished {
    self.interactionController.delegate = nil;
    self.interactionController = nil;

    if ([self.delegate respondsToSelector:@selector(openInOtherAppDidFinished:)]) {
        [self.delegate openInOtherAppDidFinished:self];
    }
}

- (void)handleTimer:(NSTimer *)timer {
    [_timer invalidate];
    self.timer = nil;
    
    [self notifyFinished];
}

@end
