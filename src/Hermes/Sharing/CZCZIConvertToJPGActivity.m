//
//  CZCZIConvertToJPGActivity.m
//  Labscope
//
//  Created by Ralph Jin on 4/8/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZCZIConvertToJPGActivity.h"
#import "CZShareFileItem.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZIKit/CZIKit.h>
#import <CZFileKit/CZFileKit.h>
#import "MBProgressHUD+Hide.h"
#import "CZFileOverwriteController.h"

#if DCM_REFACTORED
// DCM
#import "CZDCMManager.h"
#import "CZFileListManager.h"

#endif

NSString * const CZActivityTypeCZIConvertToJPG = @"CZActivityTypeCZIConvertToJPG";
NSString * const CZActivityTypeCZIConvertToTIF = @"CZActivityTypeCZIConvertToTIF";

@class CZCZIConvertToJPGViewController;

@protocol CZCZIConvertToJPGViewControllerDelegate <NSObject>
@required
- (NSString *)imageExtension;
- (void)saveImage:(UIImage *)image
              doc:(CZDocManager *)doc
           toFile:(NSString *)filePath;

@optional
- (void)convertViewController:(CZCZIConvertToJPGViewController *)viewController
                    didFinish:(BOOL)finish;

@end

@interface CZCZIConvertToJPGViewController : UIViewController

@property (nonatomic, retain) NSMutableArray *activityItems;
@property (nonatomic, retain, readonly) NSMutableArray *jpgFiles;

/* key: source file last path component, value: destination last path component.*/
@property (nonatomic, retain, readonly) NSMutableDictionary *convertMap;

@property (nonatomic, retain) MBProgressHUD *sharingHUD;
@property (atomic, assign) id<CZCZIConvertToJPGViewControllerDelegate> delegate;

@end

@implementation CZCZIConvertToJPGViewController

@synthesize jpgFiles = _jpgFiles;
@synthesize convertMap = _convertMap;

- (id)init {
    self = [super init];
    if (self) {
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    return self;
}

- (void)dealloc {
    self.delegate = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    // this view is a placeholder, hide its super view,
    // so that only sharingHUD is shown.
    self.view.superview.hidden = YES;
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    [hud setColor:kSpinnerBackgroundColor];
    self.sharingHUD = hud;
    
    [self.jpgFiles removeAllObjects];
    [self.convertMap removeAllObjects];
    
    for (NSURL *fromURL in self.activityItems) {
        NSString *fromFile = [fromURL lastPathComponent];
        NSString *toFile = [[fromFile stringByDeletingPathExtension] stringByAppendingPathExtension:[self.delegate imageExtension]];
        [self.convertMap setObject:toFile forKey:fromFile];
    }
    
    [self checkConfliction];
}

- (void)performActivity {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL processSuccess = YES;
        float totalCount = self.activityItems.count;
        int i = 0;
        NSUInteger count = self.activityItems.count;
        
        do {
            if (i >= count) {
                sleep(1); // delay for a while, otherwise UI will freeze.
                dispatch_async(dispatch_get_main_queue(), ^ {
                    if ([self.delegate respondsToSelector:@selector(convertViewController:didFinish:)]) {
                        [self.delegate convertViewController:self didFinish:processSuccess];
                    }
                    
                    [self.sharingHUD dismiss:YES];
                    self.sharingHUD = nil;
                });
                return;
            }
            
            NSURL *url = self.activityItems[i];
            NSString *file = [url lastPathComponent];
            
            float progress = i / totalCount;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.sharingHUD.progress = progress;
                NSString *label = [NSString stringWithFormat:L(@"BUSY_INDICATOR_CONVERTING"), [file lastPathComponent]];
                [self.sharingHUD setLabelText:label];
            });

            NSString *convertedFilePath = [self convertFile:file toFile:_convertMap[file]];
            
            if (convertedFilePath) {
                dispatch_async(dispatch_get_main_queue(), ^ {
                    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                    [notificationCenter postNotificationName:kCZFileUpdateNotification
                                                      object:nil
                                                    userInfo:@{kCZFileUpdateNotificationFileList: @[convertedFilePath]}];
                    
                    
                });
            }

            i++;
        } while (processSuccess);
    });
}

- (void)checkConfliction {
    if (self.activityItems.count == 0) {
        return;
    }
    
    NSString *folder = [self docPath];
    
    NSMutableArray *existFiles = [[NSMutableArray alloc] init];
    NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:folder];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        [existFiles addObject:file];
    }
    
    CZFileOverwriteController *fileOverwriteController = [[CZFileOverwriteController alloc] initWithSource:[self.convertMap allValues]
                                                                                               destination:existFiles];
    fileOverwriteController.overwriteYesBlock = ^(NSDictionary *overwriteFilesDic) {
        [self performActivity];
    };
    
    fileOverwriteController.overwriteNoBlock = ^(NSDictionary *overwriteFilesDic) {
        for (NSString *key in overwriteFilesDic.allKeys) {
            NSString *cziKey = [[key stringByDeletingPathExtension] stringByAppendingPathExtension:kFileExtensionCZI];
            self.convertMap[cziKey] = overwriteFilesDic[key];
        }
        
        [self performActivity];
    };
    
    if (![fileOverwriteController checkOverwriteFiles:NO] ) {
        [self performActivity];
    }
}

- (NSMutableArray *)jpgFiles {
    if (_jpgFiles == nil) {
        _jpgFiles = [[NSMutableArray alloc] init];
    }
    
    return _jpgFiles;
}

- (NSMutableDictionary *)convertMap {
    if (_convertMap == nil) {
        _convertMap = [[NSMutableDictionary alloc] init];
    }
    
    return _convertMap;
}

#pragma mark - Private methods

/** @return converted file path. */
- (NSString *)convertFile:(NSString *)file toFile:(NSString *)toFile {
    @autoreleasepool {
        NSString *docPath = [self docPath];
        NSString *fromFilePath = [docPath stringByAppendingPathComponent:file];
        NSString *toFilePath = [docPath stringByAppendingPathComponent:toFile];
        
        CZDocManager *doc = [CZDocManager newDocManagerFromFilePath:fromFilePath];
        if (doc) {
            UIImage *jpgImage = [doc imageWithBurninAnnotations];
            
            [self.delegate saveImage:jpgImage doc:doc toFile:toFilePath];
            
            [self.jpgFiles addObject:toFilePath];
            
            return toFilePath;
        } else {
            return nil;
        }
    }
}

- (NSString *)docPath {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
#if DCM_REFACTORED
    return [[CZDCMManager defaultManager] hasConnectedToDCMServer] ? [self documentPathForDCM] : path;
#else
    return path;
#endif
    
}

#if DCM_REFACTORED
- (NSString *)documentPathForDCM {
    NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
    return [documentPath stringByAppendingPathComponent:kLocalFolderForDCMFileStorage];
}
#endif


@end

@interface CZCZIConvertActivity () <CZCZIConvertToJPGViewControllerDelegate>

@property (nonatomic, retain) CZCZIConvertToJPGViewController *convertViewController;

@end

@implementation CZCZIConvertActivity

- (void)dealloc {
    if (_convertViewController.delegate == self) {
        _convertViewController.delegate = nil;
    }
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    BOOL canPerform = YES;
    for (id object in activityItems) {
        if ([object isKindOfClass:[NSURL class]]) {
            NSURL *url = (NSURL *)object;
            
            CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[url pathExtension]];
            if (fileFormat != kCZFileFormatCZI) {
                canPerform = NO;
                break;
            }
        } else if ([object isKindOfClass:[UIImage class]]) {
            CGSize size = [(UIImage *)object size];
            if (size.width != kCZShareItemImageCodeCZI) {
                canPerform = NO;
                break;
            }
        }
    }
    
    return canPerform;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (id item in activityItems) {
        if ([item isKindOfClass:[NSURL class]]) {
            CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[(NSURL *)item pathExtension]];
            if (fileFormat == kCZFileFormatCZI) {
                [tempArray addObject:item];
            }
        }
    }

    CZCZIConvertToJPGViewController *convertViewController = [[CZCZIConvertToJPGViewController alloc] init];
    self.convertViewController = convertViewController;
    convertViewController.delegate = self;
    convertViewController.activityItems = tempArray;
}

- (UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [rootVC presentViewController:self.convertViewController animated:YES completion:NULL];
}

- (void)convertViewController:(CZCZIConvertToJPGViewController *)viewController
                    didFinish:(BOOL)finish {
    if (self.convertViewController == viewController) {
        [self.convertViewController dismissViewControllerAnimated:YES completion:NULL];
        [self activityDidFinish:finish];
    }
}

- (NSString *)imageExtension {
    // sub-class should override this method
    return @"";
}

- (void)saveImage:(UIImage *)image
              doc:(CZDocManager *)doc
           toFile:(NSString *)filePath {
    // sub-class should override this method
}
@end

@implementation CZCZIConvertToJPGActivity

- (NSString *)activityType {
    return CZActivityTypeCZIConvertToJPG;
}

- (NSString *)activityTitle {
    return L(@"CONVERT_TO_JPG");
}

- (UIImage *)activityImage {
    return [UIImage imageNamed:A(@"czi-convert-to-jpg-activity")];
}

- (NSString *)imageExtension {
    return kFileExtensionJPEG;
}

- (void)saveImage:(UIImage *)image
              doc:(CZDocManager *)doc
           toFile:(NSString *)filePath {
    UIImage *jpgImage = image;
    if ([jpgImage czi_grayscale]) {  // convert to grayscale image
        jpgImage = [CZCommonUtils grayImageFromImage:jpgImage];
    }
    
    NSData *jpgImageData = [doc.imageProperties JPEGRepresentationOfImage:jpgImage compression:1.0];
    
    [jpgImageData writeToFile:filePath atomically:YES];
}

@end

@implementation CZCZIConvertToTIFActivity

- (NSString *)activityType {
    return CZActivityTypeCZIConvertToTIF;
}

- (NSString *)activityTitle {
    return L(@"CONVERT_TO_TIF");
}

- (UIImage *)activityImage {
    return [UIImage imageNamed:A(@"czi-convert-to-tif-activity")];
}

- (NSString *)imageExtension {
    return kFileExtensionTIF;
}

- (void)saveImage:(UIImage *)image
              doc:(CZDocManager *)doc
           toFile:(NSString *)filePath {
    UIImage *tifImage = image;
    if ([tifImage czi_grayscale]) {  // convert to grayscale image
        tifImage = [CZCommonUtils grayImageFromImage:tifImage];
    }
    [tifImage writeTifToFile:filePath
        usingImageProperties:doc.imageProperties];
}

@end
