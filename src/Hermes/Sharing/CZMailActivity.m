//
//  CZMailActivity.m
//  Hermes
//
//  Created by Halley Gu on 9/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMailActivity.h"
#import <MessageUI/MessageUI.h>
#import "CZAlertController.h"

NSString * const CZActivityTypeMail = @"CZActivityTypeMail";

@interface CZMailActivity () <MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) MFMailComposeViewController *mailController;

@end

@implementation CZMailActivity

+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}

- (NSString *)activityType {
    return CZActivityTypeMail;
}

- (NSString *)activityTitle {
    return L(@"SHARE_BY_MAIL");
}

- (UIImage *)activityImage {
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) {
        return [UIImage imageNamed:A(@"mail-activity8")];
    } else {
        return [UIImage imageNamed:A(@"mail-activity")];
    }
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    BOOL canPerform = NO;
    for (id object in activityItems) {
        if ([object isKindOfClass:[UIImage class]] ||
            [object isKindOfClass:[NSURL class]] ||
            [object isKindOfClass:[NSData class]] ) {
            canPerform = YES;
            break;
        }
    }
    
    return canPerform;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    if ([MFMailComposeViewController canSendMail] && activityItems.count > 0) {
        MFMailComposeViewController *composeController = [[MFMailComposeViewController alloc] init];
        [composeController setMailComposeDelegate:self];
        
        for (id object in activityItems) {
            @autoreleasepool {
                if ([object isKindOfClass:[NSURL class]]) {
                    NSURL *url = (NSURL *)object;
                    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
                    
                    NSString *mimeType = [CZCommonUtils mimeTypeStringForExtension:[url pathExtension]];
                    [composeController addAttachmentData:data
                                                mimeType:mimeType
                                                fileName:[url lastPathComponent]];
                    
                } else if ([object isKindOfClass:[NSData class]]) {
                    // Assume that NSData is JPEG image data since it's currently
                    // the only case in the app. If the assumption is not true any
                    // more, this should be revised then.
                    [composeController addAttachmentData:(NSData *)object
                                                mimeType:@"image/jpeg"
                                                fileName:@"attachment.jpg"];
                } else if ([object isKindOfClass:[UIImage class]]) {
                    NSData *data = UIImageJPEGRepresentation((UIImage *)object, 1.0);
                    [composeController addAttachmentData:data
                                                mimeType:@"image/jpeg"
                                                fileName:@"attachment.jpg"];
                }
            }
        }
        
        self.mailController = composeController;
    }
}

- (UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    void(^presentBlock)() = ^ {
        if (![self present]) {
            [self activityDidFinish:NO];
        }
    };
    
    if (self.lastError) {
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:self.lastError level:CZAlertLevelError];
        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            presentBlock();
        }];
        [alertController presentAnimated:YES completion:nil];
    } else {
        presentBlock();
    }
}

- (void)dismiss {
    if (self.mailController) {
        [self dismissMailControllerWithCompletion:nil];
        [self activityDidFinish:NO];
    }
}

#pragma mark - private methods

- (BOOL)present {
    if (self.mailController) {
        UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
        }
        
        [topController presentViewController:self.mailController
                                    animated:YES
                                  completion:NULL];
        
        return YES;
    } else {
        return NO;
    }
}

- (void)dismissMailControllerWithCompletion:(void (^)(void))completion {
    [self.mailController dismissViewControllerAnimated:YES completion:completion];
    self.mailController = nil;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    NSString *message = nil;
    switch (result) {
        case MFMailComposeResultSaved:
            message = NSLocalizedString(@"SUCCESS_MAIL_SAVED", nil);
            break;
        case MFMailComposeResultSent:
            message = NSLocalizedString(@"SUCCESS_MAIL_SENT", nil);
            break;
        case MFMailComposeResultFailed:
            message = [NSString stringWithFormat:@"%@\n\n%@",
                       NSLocalizedString(@"ERROR_MAIL_SENDING_FAILED", nil),
                       error.localizedFailureReason];
            break;
        default:
            break;
    }
    
    if (message) {
        [self dismissMailControllerWithCompletion:^{
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:message level:CZAlertLevelInfo];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        }];
        [self activityDidFinish:result != MFMailComposeResultFailed];
    } else {
        [self dismissMailControllerWithCompletion:nil];
        [self activityDidFinish:result != MFMailComposeResultFailed];
    }
}

@end
