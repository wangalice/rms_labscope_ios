//
//  CZShareFileItem.h
//  Hermes
//
//  Created by Mike Wang on 7/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZShareItemImageCode) {
    kCZShareItemImageCodeOther = 1,
    kCZShareItemImageCodeCZI = 2
};

@class CZShareFileItem;

@protocol CZShareItemDelegate <NSObject>

@optional
- (void)shareFileItemDidCreated:(CZShareFileItem *)shareFileItem activityType:(NSString *)activityType success:(BOOL)success;
- (BOOL)shouldExportShareFileItem:(CZShareFileItem *)shareFileItem activityType:(NSString *)activityType;

@end

@interface CZShareFileItem : UIActivityItemProvider

+ (CZShareFileItem *)itemWithFilePath:(NSString *)path;
+ (CZShareFileItem *)itemWithData:(NSData *)data andName:(NSString *)fileName;

@property (nonatomic, retain, readonly) NSURL *fileURL;

@property (nonatomic, assign) id<CZShareItemDelegate> delegate;
@property (nonatomic, assign, readonly) uint64_t allocatedSize;
@property (nonatomic, assign, readonly) BOOL allocatedOnDisk;  // YES, on disk; No, in memory.

- (NSArray *)includedActivityTypes;

@end

@interface CZShareSpecialTextItem : UIActivityItemProvider

@end
