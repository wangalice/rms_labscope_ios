//
//  CZSaveToCameraRollActivity.m
//  Hermes
//
//  Created by Ralph Jin on 9/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSaveToCameraRollActivity.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <Photos/Photos.h>
#import "MBProgressHUD+Hide.h"

const static NSUInteger kMaxRetryCount = 5;

NSString * const CZActivityTypeSaveToCameraRoll = @"CZActivityTypeSaveToCameraRoll";

#pragma mark - class CZSaveToCameraRollActivity

@interface CZSaveToCameraRollActivity () {
  @private
    BOOL _processSuccess;
    NSUInteger _retryCount;
}

@property (nonatomic, retain) NSMutableArray *activityItems;

@property (nonatomic, retain) MBProgressHUD *sharingHUD;

@end

@implementation CZSaveToCameraRollActivity

- (NSString *)activityType {
    return CZActivityTypeSaveToCameraRoll;
}

- (NSString *)activityTitle {
    return L(@"SAVE_TO_CAMERA_ROLL");
}

- (UIImage *)activityImage {
    return [UIImage imageNamed:A(@"save-to-camera-roll-activity")];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    BOOL canPerform = NO;
    for (id object in activityItems) {
        if ([object isKindOfClass:[UIImage class]] ||
            [object isKindOfClass:[NSURL class]] ||
            [object isKindOfClass:[NSData class]] ) {
            canPerform = YES;
            break;
        }
    }

    return canPerform;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:activityItems];
    self.activityItems = tempArray;
}

- (UIViewController *)activityViewController {
    return nil;
}

- (void)performActivity {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [hud setLabelText:L(@"BUSY_INDICATOR_EXPORTING")];
    [hud setColor:kSpinnerBackgroundColor];
    self.sharingHUD = hud;
    
    _processSuccess = YES;
    
    [self processNextItem];
}

#pragma mark - Private methods

- (void)processNextItem {
    @autoreleasepool {
        if (self.activityItems.count == 0) {
            [self.sharingHUD dismiss:YES];
            self.sharingHUD = nil;
            [self activityDidFinish:_processSuccess];
            return;
        }

        BOOL failed = YES;
        BOOL isSaving = NO;
        id object = [self.activityItems lastObject];

        do {
            if (![object isKindOfClass:[NSURL class]]) {
                break;
            }
            
            NSString *filePath = [(NSURL *)object relativePath];
            CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[filePath pathExtension]];

            if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
                if (filePath) {
                    [self saveImageWithFilePath:[NSURL fileURLWithPath:filePath]];
                    isSaving = YES;
                    failed = NO;
                    _retryCount = 0;
                }
            } else if (fileFormat == kCZFileFormatMP4) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(videoPath:didFinishSavingWithError:contextInfo:), NULL);
                    
                    isSaving = YES;
                    failed = NO;
                    _retryCount = 0;
                }
            }
            
            if (failed) {
                _processSuccess = NO;
            }
        } while (NO);
            
        [self.activityItems removeLastObject];
        
        if (!isSaving) {
            [self processNextItem];
        }
    }
}

- (void)saveImageWithFilePath:(NSURL *)fileURL {
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status != PHAuthorizationStatusAuthorized) {
            _processSuccess = NO;
            [self processNextItem];
            return;
        }
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [[PHAssetCreationRequest creationRequestForAsset] addResourceWithType:PHAssetResourceTypePhoto
                                                                          fileURL:fileURL
                                                                          options:nil];
        } completionHandler:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    _retryCount++;
                    if (error && _retryCount < kMaxRetryCount) {  // failed retry
                        usleep(2e+5);
                        [self saveImageWithFilePath:fileURL];
                        _retryCount++;
                    } else {
                        _processSuccess = NO;
                        [self processNextItem];
                    }
                } else {
                    [self processNextItem];
                }
            });
        }];
    }];
}

- (void)videoPath:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            _retryCount++;
            if (error && _retryCount < kMaxRetryCount) {  // failed retry
                usleep(2e+5);
                UISaveVideoAtPathToSavedPhotosAlbum(videoPath, self, @selector(videoPath:didFinishSavingWithError:contextInfo:), NULL);
            } else {
                _processSuccess = NO;
                [self processNextItem];
            }
        } else {
            [self processNextItem];
        }        
    });
}

@end
