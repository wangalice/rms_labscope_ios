//
//  CZPostOffice.h
//  Hermes
//
//  Created by Halley Gu on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZToolbarItem;
@class CZPostOffice;

@protocol CZPostOfficeDelegate <NSObject>

- (void)postOfficeDidDismissActivityController:(CZPostOffice *)postOffice;

@end

@interface CZPostOffice : NSObject

- (void)presentActivityViewControllerFromBarButtonItem:(CZBarButtonItem *)item;
- (void)presentActivityViewControllerFromToolbarItem:(CZToolbarItem *)item;

- (void)addShareItemFromPath:(NSString *)path;
- (void)addShareItemFromData:(NSData *)data withName:(NSString *)fileName;
- (void)reset;

- (BOOL)canDismiss;
- (void)dismiss;
- (BOOL)hasShareItem;

@property (nonatomic, assign) id<CZPostOfficeDelegate> delegate;

@property (nonatomic, assign) BOOL socialNetworksEnabled;

@property (nonatomic, assign) BOOL convertToJPGEnabled;

/* exporting success or failed. Only for judging if the file converting success or not.*/
@property (nonatomic, assign, readonly) BOOL failed;

@end
