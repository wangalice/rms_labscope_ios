//
//  CZSaveToCameraRollActivity.h
//  Hermes
//
//  Created by Ralph Jin on 9/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const CZActivityTypeSaveToCameraRoll;

@interface CZSaveToCameraRollActivity : UIActivity

@end
