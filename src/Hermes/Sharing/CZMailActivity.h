//
//  CZMailActivity.h
//  Hermes
//
//  Created by Halley Gu on 9/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const CZActivityTypeMail;

@interface CZMailActivity : UIActivity

@property (atomic, copy) NSString *lastError;

- (void)dismiss;

@end
