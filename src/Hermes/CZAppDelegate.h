//
//  CZAppDelegate.h
//  Hermes
//
//  Created by Mike Wang on 1/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZMultitaskViewController.h"

@interface CZAppDelegate : UIResponder <UIApplicationDelegate>

@property (class, nonatomic, readonly) CZAppDelegate *sharedDelegate;
@property (nonatomic, readonly, strong) CZMultitaskViewController *multitaskViewController;

@end
