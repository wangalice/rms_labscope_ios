//
//  main.m
//  Hermes
//
//  Created by Mike Wang on 1/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZApplication.h"
#import "CZAppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([CZApplication class]), NSStringFromClass([CZAppDelegate class]));
    }
}
