//
//  CZMultitaskBarPresentationController.h
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMultitaskBarPresentationController : UIPresentationController

@end
