//
//  CZMultitaskBarTaskCell.h
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMultitaskBarTaskCell : UICollectionViewCell

@property (nonatomic, readonly, strong) UIImageView *taskSnapshotView;

@end
