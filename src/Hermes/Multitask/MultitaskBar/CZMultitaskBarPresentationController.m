//
//  CZMultitaskBarPresentationController.m
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskBarPresentationController.h"

@interface CZMultitaskBarPresentationController () <UIGestureRecognizerDelegate>

@property (nonatomic, readonly, strong) UIView *shadowView;
@property (nonatomic, readonly, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation CZMultitaskBarPresentationController

@synthesize shadowView = _shadowView;
@synthesize tapGestureRecognizer = _tapGestureRecognizer;

- (CGRect)frameOfPresentedViewInContainerView {
    CGRect frame = self.containerView.frame;
    frame.size.height = 266.0;
    return frame;
}

- (void)presentationTransitionWillBegin {
    self.shadowView.frame = [self frameOfPresentedViewInContainerView];
    self.shadowView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.shadowView.bounds] CGPath];
    [self.containerView insertSubview:self.shadowView atIndex:0];
    
    self.shadowView.alpha = 0.0;
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.shadowView.alpha = 1.0;
    } completion:nil];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    if (!completed) {
        [self.shadowView removeFromSuperview];
    } else {
        [self.containerView addGestureRecognizer:self.tapGestureRecognizer];
    }
}

- (void)dismissalTransitionWillBegin {
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.shadowView.alpha = 0.0;
    } completion:nil];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.shadowView removeFromSuperview];
        [self.containerView removeGestureRecognizer:self.tapGestureRecognizer];
    }
}

#pragma mark - Shadow View

- (UIView *)shadowView {
    if (_shadowView == nil) {
        _shadowView = [[UIView alloc] init];
        _shadowView.clipsToBounds = NO;
        _shadowView.backgroundColor = [UIColor clearColor];
        _shadowView.layer.shadowColor = [[UIColor colorWithWhite:0.0 alpha:0.75] CGColor];
        _shadowView.layer.shadowOpacity = 1.0;
        _shadowView.layer.shadowRadius = 80.0;
        _shadowView.layer.shadowOffset = CGSizeMake(0.0, 10.0);
    }
    return _shadowView;
}

#pragma mark - Tap Gesture Recognizer

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (_tapGestureRecognizer == nil) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        _tapGestureRecognizer.delegate = self;
    }
    return _tapGestureRecognizer;
}

- (void)tapGestureRecognizerAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint locationInPresentedView = [touch locationInView:self.presentedView];
    if (CGRectContainsPoint(self.presentedView.bounds, locationInPresentedView)) {
        return NO;
    } else {
        return YES;
    }
}

@end
