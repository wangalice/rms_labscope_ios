//
//  CZMultitaskBarPresentationAnimator.m
//  Hermes
//
//  Created by Li, Junlin on 1/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskBarPresentationAnimator.h"

@implementation CZMultitaskBarPresentationAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *multitaskBarViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    CGRect finalFrame = [transitionContext finalFrameForViewController:multitaskBarViewController];
    CGRect initialFrame = finalFrame;
    initialFrame.origin.y -= initialFrame.size.height;
    
    multitaskBarViewController.view.frame = initialFrame;
    [transitionContext.containerView addSubview:multitaskBarViewController.view];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        multitaskBarViewController.view.frame = finalFrame;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:finished];
    }];
}

@end
