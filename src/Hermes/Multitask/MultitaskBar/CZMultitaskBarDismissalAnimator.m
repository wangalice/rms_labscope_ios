//
//  CZMultitaskBarDismissalAnimator.m
//  Hermes
//
//  Created by Li, Junlin on 1/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskBarDismissalAnimator.h"

@implementation CZMultitaskBarDismissalAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *multitaskBarViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    CGRect initialFrame = [transitionContext initialFrameForViewController:multitaskBarViewController];
    CGRect finalFrame = initialFrame;
    finalFrame.origin.y -= finalFrame.size.height;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        multitaskBarViewController.view.frame = finalFrame;
    } completion:^(BOOL finished) {
        [multitaskBarViewController.view removeFromSuperview];
        [transitionContext completeTransition:finished];
    }];
}

@end
