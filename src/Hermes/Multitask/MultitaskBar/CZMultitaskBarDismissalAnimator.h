//
//  CZMultitaskBarDismissalAnimator.h
//  Hermes
//
//  Created by Li, Junlin on 1/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMultitaskBarDismissalAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
