//
//  CZMultitaskBarViewController.h
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat CZMultitaskBarTaskCellWidth;
extern const CGFloat CZMultitaskBarTaskCellHeight;

@class CZTask;
@class CZMultitaskBarViewController;

@protocol CZMultitaskBarViewControllerDelegate <NSObject>

@optional
- (void)multitaskBarViewController:(CZMultitaskBarViewController *)multitaskBarViewController didSelectTask:(CZTask *)task;

@end

@interface CZMultitaskBarViewController : UIViewController

@property (nonatomic, weak) id<CZMultitaskBarViewControllerDelegate> delegate;

- (instancetype)initWithTasks:(NSArray<CZTask *> *)tasks;

@end
