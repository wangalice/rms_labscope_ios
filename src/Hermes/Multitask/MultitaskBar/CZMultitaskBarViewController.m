//
//  CZMultitaskBarViewController.m
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskBarViewController.h"
#import "CZMultitaskBarPresentationAnimator.h"
#import "CZMultitaskBarDismissalAnimator.h"
#import "CZMultitaskBarPresentationController.h"
#import "CZMultitaskBarTaskCell.h"
#import "CZTask.h"

const CGFloat CZMultitaskBarTaskCellWidth = 180.0;
const CGFloat CZMultitaskBarTaskCellHeight = 140.0;
const CGFloat CZMultitaskBarTaskCellInteritemSpacing = 24.0;

@interface CZMultitaskBarViewController () <UIViewControllerTransitioningDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *taskCollectionView;
@property (nonatomic, strong) UIButton *multitaskBarCollapseButton;

@property (nonatomic, copy) NSArray<CZTask *> *tasks;

@end

@implementation CZMultitaskBarViewController

- (instancetype)initWithTasks:(NSArray<CZTask *> *)tasks {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _tasks = [tasks copy];
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs110];
    
    [self.view addSubview:self.taskCollectionView];
    [self.view addSubview:self.multitaskBarCollapseButton];
    
    [self.taskCollectionView reloadData];
}

#pragma mark - Views

- (UICollectionView *)taskCollectionView {
    if (_taskCollectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        _taskCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0, 20.0 + 32.0, self.view.bounds.size.width, 140.0) collectionViewLayout:layout];
        _taskCollectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _taskCollectionView.backgroundColor = [UIColor clearColor];
        _taskCollectionView.dataSource = self;
        _taskCollectionView.delegate = self;
        [_taskCollectionView registerClass:[CZMultitaskBarTaskCell class] forCellWithReuseIdentifier:NSStringFromClass([CZMultitaskBarTaskCell class])];
    }
    return _taskCollectionView;
}

- (UIButton *)multitaskBarCollapseButton {
    if (_multitaskBarCollapseButton == nil) {
        _multitaskBarCollapseButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 44.0, 44.0)];
        _multitaskBarCollapseButton.center = CGPointMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height - 32.0 - 5.0);
        _multitaskBarCollapseButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [_multitaskBarCollapseButton cz_setImageWithIcon:[CZIcon iconNamed:@"multitask-bar-collapse"]];
        [_multitaskBarCollapseButton addTarget:self action:@selector(multitaskBarCollapseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _multitaskBarCollapseButton.isAccessibilityElement = YES;
        _multitaskBarCollapseButton.accessibilityIdentifier = @"MultitaskBarCollapseButton";
    }
    return _multitaskBarCollapseButton;
}

#pragma mark - Actions

- (void)multitaskBarCollapseButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[CZMultitaskBarPresentationAnimator alloc] init];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [[CZMultitaskBarDismissalAnimator alloc] init];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[CZMultitaskBarPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tasks.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZMultitaskBarTaskCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CZMultitaskBarTaskCell class]) forIndexPath:indexPath];
    cell.taskSnapshotView.image = self.tasks[indexPath.item].snapshot;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CZTask *task = self.tasks[indexPath.item];
    if (self.delegate && [self.delegate respondsToSelector:@selector(multitaskBarViewController:didSelectTask:)]) {
        [self.delegate multitaskBarViewController:self didSelectTask:task];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CZMultitaskBarTaskCellWidth, CZMultitaskBarTaskCellHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat contentWidth = (CZMultitaskBarTaskCellWidth * self.tasks.count) + (CZMultitaskBarTaskCellInteritemSpacing * (self.tasks.count - 1));
    CGFloat horizontalInset = (collectionView.bounds.size.width - contentWidth) / 2.0;
    horizontalInset = MAX(horizontalInset, 0.0);
    return UIEdgeInsetsMake(0.0, horizontalInset, 0.0, horizontalInset);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CZMultitaskBarTaskCellInteritemSpacing;
}

@end
