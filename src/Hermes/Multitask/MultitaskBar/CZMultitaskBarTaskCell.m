//
//  CZMultitaskBarTaskCell.m
//  Labscope
//
//  Created by Li, Junlin on 1/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskBarTaskCell.h"

@implementation CZMultitaskBarTaskCell

@synthesize taskSnapshotView = _taskSnapshotView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.taskSnapshotView];
    }
    return self;
}

- (UIImageView *)taskSnapshotView {
    if (_taskSnapshotView == nil) {
        _taskSnapshotView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _taskSnapshotView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _taskSnapshotView.backgroundColor = [UIColor cz_gs100];
        _taskSnapshotView.layer.borderWidth = 1.0;
        _taskSnapshotView.layer.borderColor = [[UIColor cz_gs50] CGColor];
        
        _taskSnapshotView.isAccessibilityElement = YES;
        _taskSnapshotView.accessibilityIdentifier = @"TaskSnapshotView";
    }
    return _taskSnapshotView;
}

@end
