//
//  CZTask.m
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"
#import "CZTaskPrivate.h"

NSString *const CZTaskIdentifierAnonymous = @"CZTaskIdentifierAnonymous";
CZTaskLaunchOptionsKey const CZTaskLaunchOptionsDelegateKey = @"CZTaskLaunchOptionsDelegateKey";

@interface CZTask ()

@property (nonatomic, readwrite, assign) CZTaskState state;
@property (nonatomic, readwrite, weak) id<CZTaskDelegate> delegate;

@end

@implementation CZTask

- (instancetype)init {
    return [self initWithIdentifier:CZTaskIdentifierAnonymous];
}

- (instancetype)initWithIdentifier:(CZTaskIdentifier)identifier {
    self = [super init];
    if (self) {
        _identifier = [identifier copy];
        _state = CZTaskStateTerminated;
    }
    return self;
}

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (self.delegate && [self.delegate respondsToSelector:@selector(task:didFinishLaunchingWithOptions:)]) {
        [self.delegate task:self didFinishLaunchingWithOptions:launchOptions];
    }
}

- (void)didEnterForeground {
    if (self.delegate && [self.delegate respondsToSelector:@selector(taskDidEnterForeground:)]) {
        [self.delegate taskDidEnterForeground:self];
    }
}

- (void)willEnterBackgroundWithCompletionHandler:(CZTaskEventCompletionHandler)completionHandler {
    if (self.delegate && [self.delegate respondsToSelector:@selector(taskWillEnterBackground:completionHandler:)]) {
        [self.delegate taskWillEnterBackground:self completionHandler:completionHandler];
    } else {
        if (completionHandler) {
            completionHandler(CZTaskEventAllow);
        }
    }
}

- (void)didEnterBackground {
    if (self.delegate && [self.delegate respondsToSelector:@selector(taskDidEnterBackground:)]) {
        [self.delegate taskDidEnterBackground:self];
    }
}

- (void)willTerminateWithCompletionHandler:(CZTaskEventCompletionHandler)completionHandler {
    if (self.delegate && [self.delegate respondsToSelector:@selector(taskWillTerminate:completionHandler:)]) {
        [self.delegate taskWillTerminate:self completionHandler:completionHandler];
    } else {
        if (completionHandler) {
            completionHandler(CZTaskEventAllow);
        }
    }
}

#pragma mark - Private

- (void)launchInForegroundWithOptions:(NSDictionary *)launchOptions {
    self.delegate = launchOptions[CZTaskLaunchOptionsDelegateKey];
    [self didFinishLaunchingWithOptions:launchOptions];
    self.state = CZTaskStateForeground;
    [self didEnterForeground];
}

- (void)launchInBackgroundWithOptions:(NSDictionary *)launchOptions {
    self.delegate = launchOptions[CZTaskLaunchOptionsDelegateKey];
    [self didFinishLaunchingWithOptions:launchOptions];
    self.state = CZTaskStateBackground;
    [self didEnterBackground];
}

- (void)enterForeground {
    self.state = CZTaskStateForeground;
    [self didEnterForeground];
}

- (void)enterBackground {
    self.state = CZTaskStateBackground;
    [self didEnterBackground];
}

- (void)terminate {
    [self willTerminateWithCompletionHandler:^(CZTaskEventDisposition disposition) {
        switch (disposition) {
            case CZTaskEventAllow:
                self.delegate = nil;
                self.state = CZTaskStateTerminated;
                break;
            case CZTaskEventDisallow:
                break;
        }
    }];
}

- (void)enterFullScreen {
    
}

- (void)exitFullScreen {
    
}

@end
