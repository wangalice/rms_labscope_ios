//
//  CZTaskPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"

@interface CZTask ()

@property (nonatomic, readwrite, weak) CZMultitaskManager *manager;

- (void)launchInForegroundWithOptions:(NSDictionary *)launchOptions;
- (void)launchInBackgroundWithOptions:(NSDictionary *)launchOptions;
- (void)enterForeground;
- (void)enterBackground;
- (void)terminate;

- (void)enterFullScreen;
- (void)exitFullScreen;

@end
