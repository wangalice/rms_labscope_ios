//
//  CZMultitaskViewController.h
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZMultitaskManager.h"
#import "CZMicroscopesTask.h"
#import "CZLiveTask.h"
#import "CZImageTask.h"
#import "CZFilesTask.h"
#import "CZToolbar.h"

extern NSString *const CZTaskIdentifierMicroscopes;
extern NSString *const CZTaskIdentifierLive;
extern NSString *const CZTaskIdentifierImage;
extern NSString *const CZTaskIdentifierFiles;

extern const CGFloat CZTopToolbarHeight;
extern const CGFloat CZMainToolbarHeight;
extern const CGFloat CZVirtualToolbarHeight;

typedef NS_ENUM(NSInteger, CZMultitaskViewControllerDisplayMode) {
    CZMultitaskViewControllerDisplayModePrimaryOnly,
    CZMultitaskViewControllerDisplayModePrimaryOnlyFullScreen,
    CZMultitaskViewControllerDisplayModeSplit
};

@interface CZMultitaskViewController : UIViewController <CZMultitaskManagerDelegate, CZToolbarResponder>

@property (nonatomic, readonly, strong) CZMultitaskManager *multitaskManager;

@property (nonatomic, assign) CZMultitaskViewControllerDisplayMode preferredDisplayMode;
@property (nonatomic, readonly, assign) CZMultitaskViewControllerDisplayMode displayMode;

@end

@interface UIViewController (CZMultitaskViewController)

@property (nonatomic, readonly, strong) CZMultitaskViewController *multitaskViewController;
@property (nonatomic, readonly, strong) CZToolbar *topToolbar;
@property (nonatomic, readonly, strong) CZToolbar *mainToolbar;
@property (nonatomic, readonly, strong) CZToolbar *virtualToolbar;

- (void)setNeedsMainToolbarItemsUpdate;
- (void)setNeedsVirtualToolbarItemsUpdate;

@end

@interface CZMultitaskManager (CZTaskSubclass)

@property (nonatomic, readonly, strong) CZMicroscopesTask *microscopesTask;
@property (nonatomic, readonly, strong) CZLiveTask *liveTask;
@property (nonatomic, readonly, strong) CZImageTask *imageTask;
@property (nonatomic, readonly, strong) CZFilesTask *filesTask;

- (void)switchToMicroscopesTask;
- (void)switchToLiveTask;
- (void)switchToImageTask;
- (void)switchToFilesTask;

@end
