//
//  CZMainToolbarDataSource.h
//  Hermes
//
//  Created by Li, Junlin on 1/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbar.h"

@protocol CZMainToolbarDataSource <NSObject>

@required
- (NSArray<CZToolbarItemIdentifier> *)mainToolbarPrimaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar;
- (NSArray<CZToolbarItemIdentifier> *)mainToolbarSecondaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar;
- (NSArray<CZToolbarItemIdentifier> *)mainToolbarCenterItemIdentifiers:(CZToolbar *)mainToolbar;

@end

@interface CZToolbar (CZMainToolbar)

- (void)setMainToolbarItemsFromDataSource:(id<CZMainToolbarDataSource>)dataSource;

@end
