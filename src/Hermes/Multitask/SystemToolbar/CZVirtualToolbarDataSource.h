//
//  CZVirtualToolbarDataSource.h
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbar.h"

@protocol CZVirtualToolbarDataSource <NSObject>

@required
- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarLeadingItemIdentifiers:(CZToolbar *)virtualToolbar;
- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarCenterItemIdentifiers:(CZToolbar *)virtualToolbar;
- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarTrailingItemIdentifiers:(CZToolbar *)virtualToolbar;

@end

@interface CZToolbar (CZVirtualToolbar)

- (void)setVirtualToolbarItemsFromDataSource:(id<CZVirtualToolbarDataSource>)dataSource;

@end
