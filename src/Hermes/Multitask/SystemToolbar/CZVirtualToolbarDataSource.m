//
//  CZVirtualToolbarDataSource.m
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZVirtualToolbarDataSource.h"

@implementation CZToolbar (CZVirtualToolbar)

- (void)setVirtualToolbarItemsFromDataSource:(id<CZVirtualToolbarDataSource>)dataSource {
    NSArray<CZToolbarItemIdentifier> *leadingItemIdentifiers = [dataSource virtualToolbarLeadingItemIdentifiers:self] ?: @[];
    NSArray<CZToolbarItem *> *leadingItems = [self makeItemsForItemIdentifiers:leadingItemIdentifiers];
    
    NSArray<CZToolbarItemIdentifier> *centerItemIdentifiers = [dataSource virtualToolbarCenterItemIdentifiers:self] ?: @[];
    NSArray<CZToolbarItem *> *centerItems = [self makeItemsForItemIdentifiers:centerItemIdentifiers];
    
    NSArray<CZToolbarItemIdentifier> *trailingItemIdentifiers = [dataSource virtualToolbarTrailingItemIdentifiers:self] ?: @[];
    NSArray<CZToolbarItem *> *trailingItems = [self makeItemsForItemIdentifiers:trailingItemIdentifiers];
    
    CGFloat leadingWidth = self.insets.left;
    for (CZToolbarItem *item in leadingItems) {
        leadingWidth += item.actualWidth + self.spacing;
    }
    
    CGFloat centerWidth = self.spacing;
    for (CZToolbarItem *item in centerItems) {
        centerWidth += item.actualWidth + self.spacing;
    }
    
    CZToolbarItem *fixedSpace = [CZToolbarItem fixedSpaceItemWithWidth:self.bounds.size.width / 2 - leadingWidth - centerWidth / 2];
    CZToolbarItem *flexibleSpace = [CZToolbarItem flexibleSpaceItem];
    
    NSMutableArray<CZToolbarItem *> *items = [NSMutableArray array];
    [items addObjectsFromArray:leadingItems];
    [items addObject:fixedSpace];
    [items addObjectsFromArray:centerItems];
    [items addObject:flexibleSpace];
    [items addObjectsFromArray:trailingItems];
    
    self.items = items;
}

@end
