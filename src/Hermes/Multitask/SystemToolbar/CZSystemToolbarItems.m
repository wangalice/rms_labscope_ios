//
//  CZSystemToolbarItems.m
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSystemToolbarItems.h"
#import "CZAnnotationPickerView.h"

CZToolbarItemIdentifier const CZToolbarItemIdentifierAbout = @"CZToolbarItemIdentifierAbout";
CZToolbarItemIdentifier const CZToolbarItemIdentifierAdd = @"CZToolbarItemIdentifierAdd";
CZToolbarItemIdentifier const CZToolbarItemIdentifierAnnotation = @"CZToolbarItemIdentifierAnnotation";
CZToolbarItemIdentifier const CZToolbarItemIdentifierAnnotationTools = @"CZToolbarItemIdentifierAnnotationTools";
CZToolbarItemIdentifier const CZToolbarItemIdentifierCameraSettings = @"CZToolbarItemIdentifierCameraSettings";
CZToolbarItemIdentifier const CZToolbarItemIdentifierCancel = @"CZToolbarItemIdentifierCancel";
CZToolbarItemIdentifier const CZToolbarItemIdentifierChannels = @"CZToolbarItemIdentifierChannels";
CZToolbarItemIdentifier const CZToolbarItemIdentifierConfirm = @"CZToolbarItemIdentifierConfirm";
CZToolbarItemIdentifier const CZToolbarItemIdentifierCopy = @"CZToolbarItemIdentifierCopy";
CZToolbarItemIdentifier const CZToolbarItemIdentifierCut = @"CZToolbarItemIdentifierCut";
CZToolbarItemIdentifier const CZToolbarItemIdentifierDelete = @"CZToolbarItemIdentifierDelete";
CZToolbarItemIdentifier const CZToolbarItemIdentifierDisplayCurve = @"CZToolbarItemIdentifierDisplayCurve";
CZToolbarItemIdentifier const CZToolbarItemIdentifierDrawingTube = @"CZToolbarItemIdentifierDrawingTube";
CZToolbarItemIdentifier const CZToolbarItemIdentifierExpandMultitaskBar = @"CZToolbarItemIdentifierExpandMultitaskBar";
CZToolbarItemIdentifier const CZToolbarItemIdentifierExport = @"CZToolbarItemIdentifierExport";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFileInfo = @"CZToolbarItemIdentifierFileInfo";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFilterSet = @"CZToolbarItemIdentifierFilterSet";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFluorescenceSnapMode = @"CZToolbarItemIdentifierFluorescenceSnapMode";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFocusIndicator = @"CZToolbarItemIdentifierFocusIndicator";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFreeSpace = @"CZToolbarItemIdentifierFreeSpace";
CZToolbarItemIdentifier const CZToolbarItemIdentifierGlobalSettings = @"CZToolbarItemIdentifierGlobalSettings";
CZToolbarItemIdentifier const CZToolbarItemIdentifierGraticuleOverlay = @"CZToolbarItemIdentifierGraticuleOverlay";
CZToolbarItemIdentifier const CZToolbarItemIdentifierHandsup = @"CZToolbarItemIdentifierHandsup";
CZToolbarItemIdentifier const CZToolbarItemIdentifierHistogram = @"CZToolbarItemIdentifierHistogram";
CZToolbarItemIdentifier const CZToolbarItemIdentifierImageProcessing = @"CZToolbarItemIdentifierImageProcessing";
CZToolbarItemIdentifier const CZToolbarItemIdentifierImageProcessingTools = @"CZToolbarItemIdentifierImageProcessingTools";
CZToolbarItemIdentifier const CZToolbarItemIdentifierImport = @"CZToolbarItemIdentifierImport";
CZToolbarItemIdentifier const CZToolbarItemIdentifierLaserPointer = @"CZToolbarItemIdentifierLaserPointer";
CZToolbarItemIdentifier const CZToolbarItemIdentifierLess = @"CZToolbarItemIdentifierLess";
CZToolbarItemIdentifier const CZToolbarItemIdentifierListGrid = @"CZToolbarItemIdentifierListGrid";
CZToolbarItemIdentifier const CZToolbarItemIdentifierLoginInfo = @"CZToolbarItemIdentifierLoginInfo";
CZToolbarItemIdentifier const CZToolbarItemIdentifierLogout = @"CZToolbarItemIdentifierLogout";
CZToolbarItemIdentifier const CZToolbarItemIdentifierMagnification = @"CZToolbarItemIdentifierMagnification";
CZToolbarItemIdentifier const CZToolbarItemIdentifierMicroscopeConfiguration = @"CZToolbarItemIdentifierMicroscopeConfiguration";
CZToolbarItemIdentifier const CZToolbarItemIdentifierMicroscopeZoom = @"CZToolbarItemIdentifierMicroscopeZoom";
CZToolbarItemIdentifier const CZToolbarItemIdentifierMore = @"CZToolbarItemIdentifierMore";
CZToolbarItemIdentifier const CZToolbarItemIdentifierObjective = @"CZToolbarItemIdentifierObjective";
CZToolbarItemIdentifier const CZToolbarItemIdentifierOverexposureIndicator = @"CZToolbarItemIdentifierOverexposureIndicator";
CZToolbarItemIdentifier const CZToolbarItemIdentifierRedo = @"CZToolbarItemIdentifierRedo";
CZToolbarItemIdentifier const CZToolbarItemIdentifierReport = @"CZToolbarItemIdentifierReport";
CZToolbarItemIdentifier const CZToolbarItemIdentifierSave = @"CZToolbarItemIdentifierSave";
CZToolbarItemIdentifier const CZToolbarItemIdentifierSaveAs = @"CZToolbarItemIdentifierSaveAs";
CZToolbarItemIdentifier const CZToolbarItemIdentifierSelectAll = @"CZToolbarItemIdentifierSelectAll";
CZToolbarItemIdentifier const CZToolbarItemIdentifierShare = @"CZToolbarItemIdentifierShare";
CZToolbarItemIdentifier const CZToolbarItemIdentifierSplitView = @"CZToolbarItemIdentifierSplitView";
CZToolbarItemIdentifier const CZToolbarItemIdentifierUndo = @"CZToolbarItemIdentifierUndo";
CZToolbarItemIdentifier const CZToolbarItemIdentifierWLANName = @"CZToolbarItemIdentifierWLANName";
CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomClickStop = @"CZToolbarItemIdentifierZoomClickStop";
CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomIn = @"CZToolbarItemIdentifierZoomIn";
CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomOut = @"CZToolbarItemIdentifierZoomOut";

@implementation UIViewController (CZToolbarDelegate)

- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier {
    static NSDictionary<CZToolbarItemIdentifier, NSString *> *makeItemSelectors = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        makeItemSelectors = @{
            CZToolbarItemIdentifierAbout                    : NSStringFromSelector(@selector(makeToolbarItemForAbout)),
            CZToolbarItemIdentifierAdd                      : NSStringFromSelector(@selector(makeToolbarItemForAdd)),
            CZToolbarItemIdentifierAnnotation               : NSStringFromSelector(@selector(makeToolbarItemForAnnotation)),
            CZToolbarItemIdentifierAnnotationTools          : NSStringFromSelector(@selector(makeToolbarItemForAnnotationTools)),
            CZToolbarItemIdentifierCameraSettings           : NSStringFromSelector(@selector(makeToolbarItemForCameraSettings)),
            CZToolbarItemIdentifierCancel                   : NSStringFromSelector(@selector(makeToolbarItemForCancel)),
            CZToolbarItemIdentifierChannels                 : NSStringFromSelector(@selector(makeToolbarItemForChannels)),
            CZToolbarItemIdentifierConfirm                  : NSStringFromSelector(@selector(makeToolbarItemForConfirm)),
            CZToolbarItemIdentifierCopy                     : NSStringFromSelector(@selector(makeToolbarItemForCopy)),
            CZToolbarItemIdentifierCut                      : NSStringFromSelector(@selector(makeToolbarItemForCut)),
            CZToolbarItemIdentifierDelete                   : NSStringFromSelector(@selector(makeToolbarItemForDelete)),
            CZToolbarItemIdentifierDisplayCurve             : NSStringFromSelector(@selector(makeToolbarItemForDisplayCurve)),
            CZToolbarItemIdentifierDrawingTube              : NSStringFromSelector(@selector(makeToolbarItemForDrawingTube)),
            CZToolbarItemIdentifierExpandMultitaskBar       : NSStringFromSelector(@selector(makeToolbarItemForExpandMultitaskBar)),
            CZToolbarItemIdentifierExport                   : NSStringFromSelector(@selector(makeToolbarItemForExport)),
            CZToolbarItemIdentifierFileInfo                 : NSStringFromSelector(@selector(makeToolbarItemForFileInfo)),
            CZToolbarItemIdentifierFilterSet                : NSStringFromSelector(@selector(makeToolbarItemForFilterSet)),
            CZToolbarItemIdentifierFluorescenceSnapMode     : NSStringFromSelector(@selector(makeToolbarItemForFluorescenceSnapMode)),
            CZToolbarItemIdentifierFocusIndicator           : NSStringFromSelector(@selector(makeToolbarItemForFocusIndicator)),
            CZToolbarItemIdentifierFreeSpace                : NSStringFromSelector(@selector(makeToolbarItemForFreeSpace)),
            CZToolbarItemIdentifierGlobalSettings           : NSStringFromSelector(@selector(makeToolbarItemForGlobalSettings)),
            CZToolbarItemIdentifierGraticuleOverlay         : NSStringFromSelector(@selector(makeToolbarItemForGraticuleOverlay)),
            CZToolbarItemIdentifierHandsup                  : NSStringFromSelector(@selector(makeToolbarItemForHandsup)),
            CZToolbarItemIdentifierHistogram                : NSStringFromSelector(@selector(makeToolbarItemForHistogram)),
            CZToolbarItemIdentifierImageProcessing          : NSStringFromSelector(@selector(makeToolbarItemForImageProcessing)),
            CZToolbarItemIdentifierImageProcessingTools     : NSStringFromSelector(@selector(makeToolbarItemForImageProcessingTools)),
            CZToolbarItemIdentifierImport                   : NSStringFromSelector(@selector(makeToolbarItemForImport)),
            CZToolbarItemIdentifierLaserPointer             : NSStringFromSelector(@selector(makeToolbarItemForLaserPointer)),
            CZToolbarItemIdentifierLess                     : NSStringFromSelector(@selector(makeToolbarItemForLess)),
            CZToolbarItemIdentifierListGrid                 : NSStringFromSelector(@selector(makeToolbarItemForListGrid)),
            CZToolbarItemIdentifierLoginInfo                : NSStringFromSelector(@selector(makeToolbarItemForLoginInfo)),
            CZToolbarItemIdentifierLogout                   : NSStringFromSelector(@selector(makeToolbarItemForLogout)),
            CZToolbarItemIdentifierMagnification            : NSStringFromSelector(@selector(makeToolbarItemForMagnification)),
            CZToolbarItemIdentifierMicroscopeConfiguration  : NSStringFromSelector(@selector(makeToolbarItemForMicroscopeConfiguration)),
            CZToolbarItemIdentifierMicroscopeZoom           : NSStringFromSelector(@selector(makeToolbarItemForMicroscopeZoom)),
            CZToolbarItemIdentifierMore                     : NSStringFromSelector(@selector(makeToolbarItemForMore)),
            CZToolbarItemIdentifierObjective                : NSStringFromSelector(@selector(makeToolbarItemForObjective)),
            CZToolbarItemIdentifierOverexposureIndicator    : NSStringFromSelector(@selector(makeToolbarItemForOverexposureIndicator)),
            CZToolbarItemIdentifierRedo                     : NSStringFromSelector(@selector(makeToolbarItemForRedo)),
            CZToolbarItemIdentifierReport                   : NSStringFromSelector(@selector(makeToolbarItemForReport)),
            CZToolbarItemIdentifierSave                     : NSStringFromSelector(@selector(makeToolbarItemForSave)),
            CZToolbarItemIdentifierSaveAs                   : NSStringFromSelector(@selector(makeToolbarItemForSaveAs)),
            CZToolbarItemIdentifierSelectAll                : NSStringFromSelector(@selector(makeToolbarItemForSelectAll)),
            CZToolbarItemIdentifierShare                    : NSStringFromSelector(@selector(makeToolbarItemForShare)),
            CZToolbarItemIdentifierSplitView                : NSStringFromSelector(@selector(makeToolbarItemForSplitView)),
            CZToolbarItemIdentifierUndo                     : NSStringFromSelector(@selector(makeToolbarItemForUndo)),
            CZToolbarItemIdentifierWLANName                 : NSStringFromSelector(@selector(makeToolbarItemForWLANName)),
            CZToolbarItemIdentifierZoomClickStop            : NSStringFromSelector(@selector(makeToolbarItemForZoomClickStop)),
            CZToolbarItemIdentifierZoomIn                   : NSStringFromSelector(@selector(makeToolbarItemForZoomIn)),
            CZToolbarItemIdentifierZoomOut                  : NSStringFromSelector(@selector(makeToolbarItemForZoomOut))
        };
    });
    
    SEL sel = NSSelectorFromString(makeItemSelectors[identifier]);
    NSAssert(sel != nil, @"No selector found for item identifier: %@.", identifier);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self performSelector:sel];
#pragma clang diagnostic pop
}

- (CZToolbarItem *)makeToolbarItemForAbout {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAbout];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"icon-placeholder"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForAdd {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAdd];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"plus"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForAnnotation {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAnnotation];
    CZToolbarItemAction *action = [CZToolbarItemAction pushActionWithItemIdentifiers:@[CZToolbarItemIdentifierAnnotationTools, CZToolbarItemIdentifierFlexibleSpace, CZToolbarItemIdentifierUndo, CZToolbarItemIdentifierRedo, CZToolbarItemIdentifierConfirm]];
    [item addAction:action];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"annotation"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForAnnotationTools {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierAnnotationTools];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForCameraSettings {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierCameraSettings];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"camera-settings"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForCancel {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierCancel];
    [item addAction:[CZToolbarItemAction popAction]];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"close"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForChannels {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierChannels];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForConfirm {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierConfirm];
    [item addAction:[CZToolbarItemAction popAction]];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"checkmark"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForCopy {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierCopy];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"copy-files"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForCut {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierCut];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"cut-files"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForDelete {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierDelete];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForDisplayCurve {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierDisplayCurve];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"icon-placeholder"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForDrawingTube {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierDrawingTube];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"drawing-tube"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForExpandMultitaskBar {
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, 48.0, 48.0) cornerRadius:24.0];
    path.lineWidth = 1.0;
    
    CZShapeImage *enabledImage = [CZShapeImage imageWithPath:path fillColor:[UIColor cz_gs110] strokeColor:[UIColor cz_gs120]];
    CZShapeImage *disabledImage = [CZShapeImage imageWithPath:path fillColor:[[UIColor cz_gs110] colorWithAlphaComponent:0.5] strokeColor:[[UIColor cz_gs120] colorWithAlphaComponent:0.5]];
    CZShapeImage *focusedImage = [CZShapeImage imageWithPath:path fillColor:[UIColor cz_gs110] strokeColor:[UIColor cz_gs80]];
    CZShapeImage *pressedImage = [CZShapeImage imageWithPath:path fillColor:[UIColor cz_gs120] strokeColor:[UIColor cz_gs120]];
    
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundImage:enabledImage.UIImage forState:UIControlStateNormal];
    [button setBackgroundImage:disabledImage.UIImage forState:UIControlStateDisabled];
    [button setBackgroundImage:focusedImage.UIImage forState:UIControlStateFocused];
    [button setBackgroundImage:pressedImage.UIImage forState:UIControlStateHighlighted | UIControlStateSelected];
    [button cz_setImageWithIcon:[CZIcon iconNamed:@"multitask-bar-expand"]];
    
    button.isAccessibilityElement = YES;
    button.accessibilityIdentifier = @"MultitaskBarExpandButton";
    
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierExpandMultitaskBar];
    item.view = button;
    item.width = 48.0;
    return item;
}

- (CZToolbarItem *)makeToolbarItemForExport {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierExport];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"upload"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForFileInfo {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierFileInfo];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"info"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForFilterSet {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierFilterSet];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForFluorescenceSnapMode {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierFluorescenceSnapMode];
    CZToggleButton *toggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"IMAGE_MULTICHANNEL_ONEKEY_SNAP"), L(@"IMAGE_MULTICHANNEL_MANUAL_SNAP")]];
    item.view = toggleButton;
    item.width = 222.0;
    item.edgeInsets = UIEdgeInsetsMake(16.0, 0.0, 0.0, 0.0);
    return item;
}

- (CZToolbarItem *)makeToolbarItemForFocusIndicator {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierFocusIndicator];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"focus-indicator"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForFreeSpace {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierFreeSpace];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForGlobalSettings {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierGlobalSettings];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"settings"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForGraticuleOverlay {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierGraticuleOverlay];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"overlay"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForHandsup {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierHistogram];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"handsup"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForHistogram {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierHistogram];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"histogram"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForImageProcessing {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierImageProcessing];
    CZToolbarItemAction *action = [CZToolbarItemAction pushActionWithItemIdentifiers:@[CZToolbarItemIdentifierImageProcessingTools, CZToolbarItemIdentifierFlexibleSpace, CZToolbarItemIdentifierCancel, CZToolbarItemIdentifierConfirm]];
    [item addAction:action];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"image-processing"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForImageProcessingTools {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierImageProcessingTools];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForImport {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierImport];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"download"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForLaserPointer {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierLaserPointer];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"laser-pointer"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForLess {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierLess];
    [item addAction:[CZToolbarItemAction dismissAction]];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"toolbar-less"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForListGrid {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierListGrid];
    CZToggleButton *toggleButton = [[CZToggleButton alloc] initWithItems:@[[CZIcon iconNamed:@"list-mode"], [CZIcon iconNamed:@"grid-mode"]]];
    item.view = toggleButton;
    return item;
}

- (CZToolbarItem *)makeToolbarItemForLoginInfo {
    CZToolbarItem *item = [CZToolbarItem labelItemWithIdentifier:CZToolbarItemIdentifierLoginInfo];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForLogout {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierLogout];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"logout"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForMagnification {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierMagnification];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForMicroscopeConfiguration {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierMicroscopeConfiguration];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"microscope-configuration"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForMicroscopeZoom {
    CZToolbarItem *item = [CZToolbarItem sliderItemWithIdentifier:CZToolbarItemIdentifierMicroscopeZoom];
    item.width = 192.0;
    item.slider.isAccessibilityElement = YES;
    item.slider.accessibilityIdentifier = @"MicroscopeScaleSlider";
    return item;
}

- (CZToolbarItem *)makeToolbarItemForMore {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierMore];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"toolbar-more"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForObjective {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierObjective];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForOverexposureIndicator {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierOverexposureIndicator];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"over-exposure"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForRedo {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierRedo];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"redo"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForReport {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierReport];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"report"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForSave {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierSave];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"save"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForSaveAs {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierSaveAs];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"save"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForSelectAll {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierSelectAll];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"select-all-files"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForShare {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierShare];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"share"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForSplitView {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierSplitView];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"split-view"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForUndo {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierUndo];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"undo"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForWLANName {
    CZToolbarItem *item = [CZToolbarItem labelItemWithIdentifier:CZToolbarItemIdentifierWLANName];
    item.width = 300.0;
    item.label.textAlignment = NSTextAlignmentCenter;
    item.label.isAccessibilityElement = YES;
    item.label.accessibilityIdentifier = @"WLANNameLabel";
    return item;
}

- (CZToolbarItem *)makeToolbarItemForZoomClickStop {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierZoomClickStop];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForZoomIn {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierZoomIn];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-in"]];
    return item;
}

- (CZToolbarItem *)makeToolbarItemForZoomOut {
    CZToolbarItem *item = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierZoomOut];
    [item.button cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-out"]];
    return item;
}

@end
