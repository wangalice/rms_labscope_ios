//
//  CZSystemToolbarItems.h
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZToolbar.h"

// All items can be used on top toolbar, main toolbar or virtual toolbar are defined here.
// Please keep the alphabetical order.
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierAbout;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierAdd;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierAnnotation;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierAnnotationTools;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierCameraSettings;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierCancel;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierChannels;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierConfirm;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierCopy;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierCut;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierDelete;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierDisplayCurve;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierDrawingTube;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierExpandMultitaskBar;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierExport;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFileInfo;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFilterSet;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFluorescenceSnapMode;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFocusIndicator;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFreeSpace;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierGlobalSettings;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierGraticuleOverlay;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierHandsup;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierHistogram;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierImageProcessing;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierImageProcessingTools;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierImport;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierLaserPointer;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierLess;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierListGrid;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierLoginInfo;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierLogout;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierMagnification;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierMicroscopeConfiguration;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierMicroscopeZoom;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierMore;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierObjective;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierOverexposureIndicator;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierRedo;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierReport;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierSave;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierSaveAs;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierSelectAll;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierShare;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierSplitView;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierUndo;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierWLANName;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomClickStop;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomIn;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierZoomOut;

@interface UIViewController (CZToolbarDelegate) <CZToolbarDelegate>

- (CZToolbarItem *)makeToolbarItemForAbout;
- (CZToolbarItem *)makeToolbarItemForAdd;
- (CZToolbarItem *)makeToolbarItemForAnnotation;
- (CZToolbarItem *)makeToolbarItemForAnnotationTools;
- (CZToolbarItem *)makeToolbarItemForCameraSettings;
- (CZToolbarItem *)makeToolbarItemForCancel;
- (CZToolbarItem *)makeToolbarItemForChannels;
- (CZToolbarItem *)makeToolbarItemForConfirm;
- (CZToolbarItem *)makeToolbarItemForCopy;
- (CZToolbarItem *)makeToolbarItemForCut;
- (CZToolbarItem *)makeToolbarItemForDelete;
- (CZToolbarItem *)makeToolbarItemForDisplayCurve;
- (CZToolbarItem *)makeToolbarItemForDrawingTube;
- (CZToolbarItem *)makeToolbarItemForExpandMultitaskBar;
- (CZToolbarItem *)makeToolbarItemForExport;
- (CZToolbarItem *)makeToolbarItemForFileInfo;
- (CZToolbarItem *)makeToolbarItemForFilterSet;
- (CZToolbarItem *)makeToolbarItemForFluorescenceSnapMode;
- (CZToolbarItem *)makeToolbarItemForFocusIndicator;
- (CZToolbarItem *)makeToolbarItemForFreeSpace;
- (CZToolbarItem *)makeToolbarItemForGlobalSettings;
- (CZToolbarItem *)makeToolbarItemForGraticuleOverlay;
- (CZToolbarItem *)makeToolbarItemForHandsup;
- (CZToolbarItem *)makeToolbarItemForHistogram;
- (CZToolbarItem *)makeToolbarItemForImageProcessing;
- (CZToolbarItem *)makeToolbarItemForImageProcessingTools;
- (CZToolbarItem *)makeToolbarItemForImport;
- (CZToolbarItem *)makeToolbarItemForLaserPointer;
- (CZToolbarItem *)makeToolbarItemForLess;
- (CZToolbarItem *)makeToolbarItemForListGrid;
- (CZToolbarItem *)makeToolbarItemForLoginInfo;
- (CZToolbarItem *)makeToolbarItemForLogout;
- (CZToolbarItem *)makeToolbarItemForMagnification;
- (CZToolbarItem *)makeToolbarItemForMicroscopeConfiguration;
- (CZToolbarItem *)makeToolbarItemForMicroscopeZoom;
- (CZToolbarItem *)makeToolbarItemForMore;
- (CZToolbarItem *)makeToolbarItemForObjective;
- (CZToolbarItem *)makeToolbarItemForOverexposureIndicator;
- (CZToolbarItem *)makeToolbarItemForRedo;
- (CZToolbarItem *)makeToolbarItemForReport;
- (CZToolbarItem *)makeToolbarItemForSave;
- (CZToolbarItem *)makeToolbarItemForSaveAs;
- (CZToolbarItem *)makeToolbarItemForSelectAll;
- (CZToolbarItem *)makeToolbarItemForShare;
- (CZToolbarItem *)makeToolbarItemForSplitView;
- (CZToolbarItem *)makeToolbarItemForUndo;
- (CZToolbarItem *)makeToolbarItemForWLANName;
- (CZToolbarItem *)makeToolbarItemForZoomClickStop;
- (CZToolbarItem *)makeToolbarItemForZoomIn;
- (CZToolbarItem *)makeToolbarItemForZoomOut;

@end
