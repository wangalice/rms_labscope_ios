//
//  CZMainToolbarDataSource.m
//  Hermes
//
//  Created by Li, Junlin on 1/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMainToolbarDataSource.h"
#import "CZSystemToolbarItems.h"

@implementation CZToolbar (CZMainToolbar)

- (void)setMainToolbarItemsFromDataSource:(id<CZMainToolbarDataSource>)dataSource {
    NSArray<CZToolbarItemIdentifier> *primaryLeadingItemIdentifiers = [dataSource mainToolbarPrimaryLeadingItemIdentifiers:self] ?: @[];
    NSArray<CZToolbarItem *> *primaryLeadingItems = [self makeItemsForItemIdentifiers:primaryLeadingItemIdentifiers];
    
    NSArray<CZToolbarItemIdentifier> *secondaryLeadingItemIdentifiers = [dataSource mainToolbarSecondaryLeadingItemIdentifiers:self] ?: @[];
    
    NSArray<CZToolbarItemIdentifier> *centerItemIdentifiers = [dataSource mainToolbarCenterItemIdentifiers:self] ?: @[];
    NSArray<CZToolbarItem *> *centerItems = [self makeItemsForItemIdentifiers:centerItemIdentifiers];
    
    NSArray<CZToolbarItemIdentifier> *originalTrailingItemIdentifiers = @[CZToolbarItemIdentifierHandsup, CZToolbarItemIdentifierLogout, CZToolbarItemIdentifierGlobalSettings];
    NSMutableArray<CZToolbarItemIdentifier> *trailingItemIdentifiers = [originalTrailingItemIdentifiers mutableCopy];
    if (secondaryLeadingItemIdentifiers.count > 0) {
        [trailingItemIdentifiers addObject:CZToolbarItemIdentifierMore];
    }
    NSArray<CZToolbarItem *> *trailingItems = [self makeItemsForItemIdentifiers:trailingItemIdentifiers];
    
    CGFloat leadingWidth = self.insets.left;
    for (CZToolbarItem *item in primaryLeadingItems) {
        leadingWidth += item.actualWidth + self.spacing;
    }
    
    CGFloat centerWidth = self.spacing;
    for (CZToolbarItem *item in centerItems) {
        centerWidth += item.actualWidth + self.spacing;
    }
    
    CZToolbarItem *fixedSpace = [CZToolbarItem fixedSpaceItemWithWidth:self.bounds.size.width / 2 - leadingWidth - centerWidth / 2];
    CZToolbarItem *flexibleSpace = [CZToolbarItem flexibleSpaceItem];
    
    NSMutableArray<CZToolbarItem *> *items = [NSMutableArray array];
    [items addObjectsFromArray:primaryLeadingItems];
    [items addObject:fixedSpace];
    [items addObjectsFromArray:centerItems];
    [items addObject:flexibleSpace];
    [items addObjectsFromArray:trailingItems];
    
    self.items = items;
    
    if (secondaryLeadingItemIdentifiers.count > 0) {
        NSMutableArray<CZToolbarItemIdentifier> *secondaryItemIdentifiers = [secondaryLeadingItemIdentifiers mutableCopy];
        [secondaryItemIdentifiers addObject:CZToolbarItemIdentifierFlexibleSpace];
        [secondaryItemIdentifiers addObjectsFromArray:originalTrailingItemIdentifiers];
        [secondaryItemIdentifiers addObject:CZToolbarItemIdentifierLess];
        
        CZToolbarItem *more = [self itemForIdentifier:CZToolbarItemIdentifierMore];
        CZToolbarItemAction *action = [CZToolbarItemAction presentActionWithItemIdentifiers:secondaryItemIdentifiers];
        [more addAction:action];
    }
}

@end
