//
//  CZMultitaskManager.m
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskManager.h"
#import "CZTaskPrivate.h"

@interface CZMultitaskManager ()

@property (nonatomic, strong) NSMutableSet<CZTask *> *allTasks;
@property (nonatomic, strong) NSArray<CZTask *> *rankedTasks;
@property (nonatomic, strong) NSMutableArray<CZTask *> *backgroundTaskQueue;
@property (nonatomic, readwrite, strong) CZTask *primaryTask;
@property (nonatomic, readwrite, strong) CZTask *secondaryTask;

@end

@implementation CZMultitaskManager

- (instancetype)init {
    return [self initWithDelegate:nil];
}

- (instancetype)initWithDelegate:(id<CZMultitaskManagerDelegate>)delegate {
    self = [super init];
    if (self) {
        _delegate = delegate;
        _allTasks = [NSMutableSet set];
        _backgroundTaskQueue = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}

#pragma mark - Register Tasks

- (CZTask *)registerClass:(Class)taskClass forTaskIdentifier:(CZTaskIdentifier)taskIdentifier {
    CZTask *task = [[taskClass alloc] initWithIdentifier:taskIdentifier];
    task.manager = self;
    [self.allTasks addObject:task];
    [self invalidateRankedTasks];
    return task;
}

- (void)invalidateRankedTasks {
    self.rankedTasks = nil;
}

- (NSArray<CZTask *> *)rankedTasks {
    if (_rankedTasks == nil) {
        _rankedTasks = [self.allTasks.allObjects sortedArrayUsingComparator:^NSComparisonResult(CZTask *task1, CZTask *task2) {
            NSInteger rank1 = [self.delegate multitaskManager:self rankForTask:task1];
            NSInteger rank2 = [self.delegate multitaskManager:self rankForTask:task2];
            return [@(rank1) compare:@(rank2)];
        }];
    }
    return _rankedTasks;
}

#pragma mark - Query Tasks

- (NSSet<CZTask *> *)registeredTasks {
    return [self.allTasks copy];
}

- (NSArray<CZTask *> *)launchingTasksButPrimary {
    NSMutableArray<CZTask *> *tasks = [NSMutableArray array];
    for (CZTask *task in self.rankedTasks) {
        if ((task.state == CZTaskStateForeground || task.state == CZTaskStateBackground) && task != self.primaryTask) {
            [tasks addObject:task];
        }
    }
    return [tasks copy];
}

- (CZTask *)taskWithIdentifier:(CZTaskIdentifier)taskIdentifier {
    for (CZTask *task in self.allTasks) {
        if ([task.identifier isEqualToString:taskIdentifier]) {
            return task;
        }
    }
    return nil;
}

#pragma mark - Manage Task State

- (void)launchTaskInForeground:(CZTask *)task {
    [self launchTaskInForeground:task completionHandler:nil];
}

- (void)launchTaskInForeground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    void (^nonnullCompletionHandler)(CZTaskEventDisposition disposition) = ^(CZTaskEventDisposition disposition) {
        if (completionHandler) {
            completionHandler(disposition);
        }
    };
    
    if (!task || task.state != CZTaskStateTerminated || ![self.allTasks containsObject:task]) {
        nonnullCompletionHandler(CZTaskEventDisallow);
        return;
    }
    
    void (^allow)(void) = ^{
        [self willSwitchFromPrimaryTask];
        
        [self.primaryTask enterBackground];
        [self enqueueBackgroundTask:self.primaryTask];
        
        NSDictionary *launchOptions = [self.delegate multitaskManager:self launchOptionsForTask:task];
        [task launchInForegroundWithOptions:launchOptions];
        self.primaryTask = task;
        
        [self didSwitchToPrimaryTask];
    };
    
    if (self.primaryTask) {
        [self.primaryTask willEnterBackgroundWithCompletionHandler:^(CZTaskEventDisposition disposition) {
            switch (disposition) {
                case CZTaskEventAllow:
                    allow();
                    nonnullCompletionHandler(CZTaskEventAllow);
                    break;
                case CZTaskEventDisallow:
                    nonnullCompletionHandler(CZTaskEventDisallow);
                    break;
            }
        }];
    } else {
        allow();
        nonnullCompletionHandler(CZTaskEventAllow);
    }
}

- (void)launchTaskInBackground:(CZTask *)task {
    [self launchTaskInBackground:task completionHandler:nil];
}

- (void)launchTaskInBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    void (^nonnullCompletionHandler)(CZTaskEventDisposition disposition) = ^(CZTaskEventDisposition disposition) {
        if (completionHandler) {
            completionHandler(disposition);
        }
    };
    
    if (!task || task.state != CZTaskStateTerminated || ![self.allTasks containsObject:task]) {
        nonnullCompletionHandler(CZTaskEventDisallow);
        return;
    }
    
    NSDictionary *launchOptions = [self.delegate multitaskManager:self launchOptionsForTask:task];
    [task launchInBackgroundWithOptions:launchOptions];
    [self enqueueBackgroundTask:task];
    
    nonnullCompletionHandler(CZTaskEventAllow);
}

- (void)terminateTask:(CZTask *)task {
    [self terminateTask:task completionHandler:nil];
}

- (void)terminateTask:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    void (^nonnullCompletionHandler)(CZTaskEventDisposition disposition) = ^(CZTaskEventDisposition disposition) {
        if (completionHandler) {
            completionHandler(disposition);
        }
    };
    
    if (!task || task.state == CZTaskStateTerminated || ![self.allTasks containsObject:task]) {
        nonnullCompletionHandler(CZTaskEventDisallow);
        return;
    }
    
    if (task == self.primaryTask) {
        void (^allow)(void) = ^{
            [self willSwitchFromPrimaryTask];
            
            [task terminate];
            
            CZTask *backgroundTask = [self dequeueBackgroundTask];
            [backgroundTask enterForeground];
            self.primaryTask = backgroundTask;
            
            [self didSwitchToPrimaryTask];
        };
        
        [task willTerminateWithCompletionHandler:^(CZTaskEventDisposition disposition) {
            switch (disposition) {
                case CZTaskEventAllow:
                    allow();
                    nonnullCompletionHandler(CZTaskEventAllow);
                    break;
                case CZTaskEventDisallow:
                    nonnullCompletionHandler(CZTaskEventDisallow);
                    break;
            }
        }];
    } else if (task.state == CZTaskStateBackground) {
        void (^allow)(void) = ^{
            [task terminate];
            [self.backgroundTaskQueue removeObject:task];
        };
        
        [task willTerminateWithCompletionHandler:^(CZTaskEventDisposition disposition) {
            switch (disposition) {
                case CZTaskEventAllow:
                    allow();
                    nonnullCompletionHandler(CZTaskEventAllow);
                    break;
                case CZTaskEventDisallow:
                    nonnullCompletionHandler(CZTaskEventDisallow);
                    break;
            }
        }];
    }
}

- (void)switchToTask:(CZTask *)task {
    [self switchToTask:task completionHandler:nil];
}

- (void)switchToTask:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    void (^nonnullCompletionHandler)(CZTaskEventDisposition disposition) = ^(CZTaskEventDisposition disposition) {
        if (completionHandler) {
            completionHandler(disposition);
        }
    };
    
    if (!task || ![self.allTasks containsObject:task] || task == self.primaryTask) {
        nonnullCompletionHandler(CZTaskEventDisallow);
        return;
    }
    
    switch (task.state) {
        case CZTaskStateForeground:
            nonnullCompletionHandler(CZTaskEventDisallow);
            return;
        case CZTaskStateBackground: {
            void (^allow)(void) = ^{
                [self willSwitchFromPrimaryTask];
                
                [self.primaryTask enterBackground];
                [self enqueueBackgroundTask:self.primaryTask];
                
                [self.backgroundTaskQueue removeObject:task];
                [task enterForeground];
                self.primaryTask = task;
                
                [self didSwitchToPrimaryTask];
            };
            
            if (self.primaryTask) {
                [self.primaryTask willEnterBackgroundWithCompletionHandler:^(CZTaskEventDisposition disposition) {
                    switch (disposition) {
                        case CZTaskEventAllow:
                            allow();
                            nonnullCompletionHandler(CZTaskEventAllow);
                            break;
                        case CZTaskEventDisallow:
                            nonnullCompletionHandler(CZTaskEventDisallow);
                            break;
                    }
                }];
            } else {
                allow();
                nonnullCompletionHandler(CZTaskEventAllow);
            }
            break;
        }
        case CZTaskStateTerminated:
            [self launchTaskInForeground:task completionHandler:completionHandler];
            break;
    }
}

- (BOOL)enqueueBackgroundTask:(CZTask *)task {
    if (!task || task.state != CZTaskStateBackground) {
        return NO;
    }
    
    [self.backgroundTaskQueue addObject:task];
    
    return YES;
}

- (CZTask *)dequeueBackgroundTask {
    CZTask *task = self.backgroundTaskQueue.firstObject;
    
    if (self.backgroundTaskQueue.count > 0) {
        [self.backgroundTaskQueue removeObjectAtIndex:0];
    }
    
    return task;
}

- (void)willSwitchFromPrimaryTask {
    if (self.delegate && [self.delegate respondsToSelector:@selector(multitaskManager:willSwitchFromTask:)]) {
        [self.delegate multitaskManager:self willSwitchFromTask:self.primaryTask];
    }
}

- (void)didSwitchToPrimaryTask {
    if (self.delegate && [self.delegate respondsToSelector:@selector(multitaskManager:didSwitchToTask:)]) {
        [self.delegate multitaskManager:self didSwitchToTask:self.primaryTask];
    }
}

#pragma mark - Notifications

- (void)applicationWillTerminate:(NSNotification *)note {
    for (CZTask *task in self.allTasks) {
        if (task.state != CZTaskStateTerminated) {
            [task terminate];
        }
    }
}

@end
