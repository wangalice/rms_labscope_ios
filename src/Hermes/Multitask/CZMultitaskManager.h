//
//  CZMultitaskManager.h
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZTask.h"

@class CZMultitaskManager;

@protocol CZMultitaskManagerDelegate <NSObject>

@required
- (NSInteger)multitaskManager:(CZMultitaskManager *)manager rankForTask:(CZTask *)task;
- (NSDictionary *)multitaskManager:(CZMultitaskManager *)manager launchOptionsForTask:(CZTask *)task;

@optional
- (void)multitaskManager:(CZMultitaskManager *)manager willSwitchFromTask:(CZTask *)task;
- (void)multitaskManager:(CZMultitaskManager *)manager didSwitchToTask:(CZTask *)task;
- (void)multitaskManager:(CZMultitaskManager *)manager didTerminateTask:(CZTask *)task;

@end

@interface CZMultitaskManager : NSObject

@property (nonatomic, readonly, weak) id<CZMultitaskManagerDelegate> delegate;
@property (nonatomic, readonly, copy) NSSet<__kindof CZTask *> *registeredTasks;
@property (nonatomic, readonly, strong) __kindof CZTask *primaryTask;
@property (nonatomic, readonly, strong) __kindof CZTask *secondaryTask;
@property (nonatomic, readonly, copy) NSArray<__kindof CZTask *> *launchingTasksButPrimary;

- (instancetype)initWithDelegate:(id<CZMultitaskManagerDelegate>)delegate NS_DESIGNATED_INITIALIZER;

- (__kindof CZTask *)registerClass:(Class)taskClass forTaskIdentifier:(CZTaskIdentifier)taskIdentifier;
- (__kindof CZTask *)taskWithIdentifier:(CZTaskIdentifier)taskIdentifier;

- (void)launchTaskInForeground:(CZTask *)task;
- (void)launchTaskInForeground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;

- (void)launchTaskInBackground:(CZTask *)task;
- (void)launchTaskInBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;

- (void)terminateTask:(CZTask *)task;
- (void)terminateTask:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;

- (void)switchToTask:(CZTask *)task;
- (void)switchToTask:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;

@end
