//
//  CZMultitaskViewController.m
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZMultitaskViewController.h"
#import "CZMultitaskBarViewController.h"
#import "CZGlobalSettingsViewController.h"
#import "CZMicroscopesViewController.h"
#import "CZLiveViewController.h"
#import "CZImageViewController.h"
#import "CZFilesViewController.h"
#import "CZMainToolbarDataSource.h"
#import "CZVirtualToolbarDataSource.h"

NSString *const CZTaskIdentifierMicroscopes = @"CZTaskIdentifierMicroscopes";
NSString *const CZTaskIdentifierLive        = @"CZTaskIdentifierLive";
NSString *const CZTaskIdentifierImage       = @"CZTaskIdentifierImage";
NSString *const CZTaskIdentifierFiles       = @"CZTaskIdentifierFiles";

const CGFloat CZTopToolbarHeight = 64.0;
const CGFloat CZMainToolbarHeight = 64.0;
const CGFloat CZVirtualToolbarHeight = 64.0;

@interface CZMultitaskViewController () <CZMultitaskBarViewControllerDelegate>

@property (nonatomic, readonly, strong) UIView *taskContainerView;
@property (nonatomic, readonly, strong) CZToolbar *topToolbar;
@property (nonatomic, readonly, strong) CZToolbar *mainToolbar;
@property (nonatomic, readonly, strong) CZToolbar *virtualToolbar;

@property (nonatomic, readonly, strong) NSMutableDictionary<CZTaskIdentifier, Class> *viewControllerClasses;
@property (nonatomic, readonly, strong) NSMutableDictionary<CZTaskIdentifier, UIViewController *> *viewControllers;

@end

@implementation CZMultitaskViewController

@synthesize taskContainerView = _taskContainerView;
@synthesize topToolbar = _topToolbar;
@synthesize mainToolbar = _mainToolbar;
@synthesize virtualToolbar = _virtualToolbar;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _multitaskManager = [[CZMultitaskManager alloc] initWithDelegate:self];
        _viewControllerClasses = [NSMutableDictionary dictionary];
        _viewControllers = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view addSubview:self.taskContainerView];
    [self.view addSubview:self.topToolbar];
    [self.view addSubview:self.mainToolbar];
    [self.view addSubview:self.virtualToolbar];
    
    [self registerViewControllerClass:[CZMicroscopesViewController class] taskClass:[CZMicroscopesTask class] forTaskIdentifier:CZTaskIdentifierMicroscopes];
    [self registerViewControllerClass:[CZLiveViewController class] taskClass:[CZLiveTask class] forTaskIdentifier:CZTaskIdentifierLive];
    [self registerViewControllerClass:[CZImageViewController class] taskClass:[CZImageTask class] forTaskIdentifier:CZTaskIdentifierImage];
    [self registerViewControllerClass:[CZFilesViewController class] taskClass:[CZFilesTask class] forTaskIdentifier:CZTaskIdentifierFiles];
    
    self.multitaskManager.microscopesTask.snapshot = [[CZIcon iconNamed:@"microscopes-task-placeholder"] image];
    self.multitaskManager.filesTask.snapshot = [[CZIcon iconNamed:@"files-task-placeholder"] image];
    [self.multitaskManager launchTaskInForeground:self.multitaskManager.microscopesTask];
    [self.multitaskManager launchTaskInBackground:self.multitaskManager.filesTask];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)registerViewControllerClass:(Class)viewControllerClass taskClass:(Class)taskClass forTaskIdentifier:(CZTaskIdentifier)taskIdentifier {
    [self.multitaskManager registerClass:taskClass forTaskIdentifier:taskIdentifier];
    self.viewControllerClasses[taskIdentifier] = viewControllerClass;
}

#pragma mark - Views

- (UIView *)taskContainerView {
    if (_taskContainerView == nil) {
        _taskContainerView = [[UIView alloc] initWithFrame:self.view.bounds];
        _taskContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _taskContainerView.backgroundColor = [UIColor clearColor];
    }
    return _taskContainerView;
}

- (CZToolbar *)topToolbar {
    if (_topToolbar == nil) {
        _topToolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, 20.0, self.view.bounds.size.width, CZTopToolbarHeight)];
        _topToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _topToolbar.backgroundColor = [UIColor clearColor];
        _topToolbar.delegate = self;
        [_topToolbar pushResponder:self];
        _topToolbar.itemIdentifiers = @[CZToolbarItemIdentifierFlexibleSpace, CZToolbarItemIdentifierExpandMultitaskBar, CZToolbarItemIdentifierFlexibleSpace];
        
        _topToolbar.isAccessibilityElement = YES;
        _topToolbar.accessibilityIdentifier = @"TopToolbar";
    }
    return _topToolbar;
}

- (CZToolbar *)mainToolbar {
    if (_mainToolbar == nil) {
        _mainToolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.bounds.size.height - CZMainToolbarHeight, self.view.bounds.size.width, CZMainToolbarHeight)];
        _mainToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _mainToolbar.backgroundColor = [UIColor cz_gs100];
        [_mainToolbar pushResponder:self];
        
        _mainToolbar.isAccessibilityElement = YES;
        _mainToolbar.accessibilityIdentifier = @"MainToolbar";
    }
    return _mainToolbar;
}

- (CZToolbar *)virtualToolbar {
    if (_virtualToolbar == nil) {
        _virtualToolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.bounds.size.height - CZMainToolbarHeight - CZVirtualToolbarHeight, self.view.bounds.size.width, CZVirtualToolbarHeight)];
        _virtualToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _virtualToolbar.backgroundColor = [UIColor clearColor];
        [_virtualToolbar pushResponder:self];
        
        _virtualToolbar.isAccessibilityElement = YES;
        _virtualToolbar.accessibilityIdentifier = @"VirtualToolbar";
    }
    return _virtualToolbar;
}

#pragma mark - CZMultitaskBarViewControllerDelegate

- (void)multitaskBarViewController:(CZMultitaskBarViewController *)multitaskBarViewController didSelectTask:(CZTask *)task {
    [self.multitaskManager switchToTask:task completionHandler:^(CZTaskEventDisposition disposition) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark - CZMultitaskManagerDelegate

- (NSInteger)multitaskManager:(CZMultitaskManager *)manager rankForTask:(CZTask *)task {
    NSArray<CZTaskIdentifier> *sortedIdentifiers = @[CZTaskIdentifierMicroscopes, CZTaskIdentifierLive, CZTaskIdentifierImage, CZTaskIdentifierFiles];
    return [sortedIdentifiers indexOfObject:task.identifier];
}

- (NSDictionary *)multitaskManager:(CZMultitaskManager *)manager launchOptionsForTask:(CZTask *)task {
    Class viewControllerClass = self.viewControllerClasses[task.identifier];
    UIViewController *viewController = [[viewControllerClass alloc] initWithTask:task];
    self.viewControllers[task.identifier] = viewController;
    return @{
        CZTaskLaunchOptionsDelegateKey: viewController
    };
}

- (void)multitaskManager:(CZMultitaskManager *)manager willSwitchFromTask:(CZTask *)task {
    if (task == nil) {
        return;
    }
    
    UIView *taskView = self.view;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(CZMultitaskBarTaskCellWidth, CZMultitaskBarTaskCellHeight), NO, 0.0);
    [taskView drawViewHierarchyInRect:CGRectMake(0.0, 0.0, CZMultitaskBarTaskCellWidth, CZMultitaskBarTaskCellHeight) afterScreenUpdates:NO];
    task.snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIViewController *taskViewController = task.delegate.taskViewController;
    [taskViewController willMoveToParentViewController:nil];
    [taskViewController.view removeFromSuperview];
    [taskViewController removeFromParentViewController];
    
    self.mainToolbar.delegate = nil;
    self.mainToolbar.items = nil;
    [self.mainToolbar popResponder];
    
    self.virtualToolbar.delegate = nil;
    self.virtualToolbar.items = nil;
    [self.virtualToolbar popResponder];
}

- (void)multitaskManager:(CZMultitaskManager *)manager didSwitchToTask:(CZTask *)task {
    if (task == nil) {
        return;
    }
    
    UIViewController *taskViewController = task.delegate.taskViewController;
    [self addChildViewController:taskViewController];
    taskViewController.view.frame = self.taskContainerView.bounds;
    [self.taskContainerView addSubview:taskViewController.view];
    [taskViewController didMoveToParentViewController:self];
    
    [taskViewController setNeedsMainToolbarItemsUpdate];
    [taskViewController setNeedsVirtualToolbarItemsUpdate];
}

- (void)multitaskManager:(CZMultitaskManager *)manager didTerminateTask:(CZTask *)task {
    [self.viewControllers removeObjectForKey:task.identifier];
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierExpandMultitaskBar]) {
        CZMultitaskBarViewController *multitaskBarViewController = [[CZMultitaskBarViewController alloc] initWithTasks:self.multitaskManager.launchingTasksButPrimary];
        multitaskBarViewController.delegate = self;
        [self presentViewController:multitaskBarViewController animated:YES completion:nil];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierGlobalSettings]) {
        CZGlobalSettingsViewController *globalSettingsViewController = [[CZGlobalSettingsViewController alloc] init];
        [self.navigationController pushViewController:globalSettingsViewController animated:YES];
        return YES;
    }
    
    return NO;
}

@end

@implementation UIViewController (CZMultitaskViewController)

- (CZMultitaskViewController *)multitaskViewController {
    UIViewController *viewController = self;
    while (viewController && ![viewController isKindOfClass:[CZMultitaskViewController class]]) {
        viewController = viewController.parentViewController;
    }
    return (CZMultitaskViewController *)viewController;
}

- (CZToolbar *)topToolbar {
    return self.multitaskViewController.topToolbar;
}

- (CZToolbar *)mainToolbar {
    return self.multitaskViewController.mainToolbar;
}

- (CZToolbar *)virtualToolbar {
    return self.multitaskViewController.virtualToolbar;
}

- (void)setNeedsMainToolbarItemsUpdate {
    NSAssert([self conformsToProtocol:@protocol(CZMainToolbarDataSource)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZMainToolbarDataSource)));
    NSAssert([self conformsToProtocol:@protocol(CZToolbarDelegate)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZToolbarDelegate)));
    NSAssert([self conformsToProtocol:@protocol(CZToolbarResponder)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZToolbarResponder)));
    
    self.mainToolbar.delegate = (id<CZToolbarDelegate>)self;
    [self.mainToolbar setMainToolbarItemsFromDataSource:(id<CZMainToolbarDataSource>)self];
    [self.mainToolbar pushResponder:(id<CZToolbarResponder>)self];
}

- (void)setNeedsVirtualToolbarItemsUpdate {
    NSAssert([self conformsToProtocol:@protocol(CZVirtualToolbarDataSource)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZVirtualToolbarDataSource)));
    NSAssert([self conformsToProtocol:@protocol(CZToolbarDelegate)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZToolbarDelegate)));
    NSAssert([self conformsToProtocol:@protocol(CZToolbarResponder)], @"%@ must conform to %@ protocol!", NSStringFromClass([self class]), NSStringFromProtocol(@protocol(CZToolbarResponder)));
    
    self.virtualToolbar.delegate = (id<CZToolbarDelegate>)self;
    [self.virtualToolbar setVirtualToolbarItemsFromDataSource:(id<CZVirtualToolbarDataSource>)self];
    [self.virtualToolbar pushResponder:(id<CZToolbarResponder>)self];
}

@end

@implementation CZMultitaskManager (IdentifierMask)

- (CZMicroscopesTask *)microscopesTask {
    return [self taskWithIdentifier:CZTaskIdentifierMicroscopes];
}

- (CZLiveTask *)liveTask {
    return [self taskWithIdentifier:CZTaskIdentifierLive];
}

- (CZImageTask *)imageTask {
    return [self taskWithIdentifier:CZTaskIdentifierImage];
}

- (CZFilesTask *)filesTask {
    return [self taskWithIdentifier:CZTaskIdentifierFiles];
}

- (void)switchToMicroscopesTask {
    [self switchToTask:self.microscopesTask];
}

- (void)switchToLiveTask {
    [self switchToTask:self.liveTask];
}

- (void)switchToImageTask {
    [self switchToTask:self.imageTask];
}

- (void)switchToFilesTask {
    [self switchToTask:self.filesTask];
}

@end
