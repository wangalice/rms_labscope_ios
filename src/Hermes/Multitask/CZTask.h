//
//  CZTask.h
//  Labscope
//
//  Created by Li, Junlin on 12/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CZTaskState) {
    CZTaskStateForeground,
    CZTaskStateBackground,
    CZTaskStateTerminated
};

typedef NS_ENUM(NSInteger, CZTaskEventDisposition) {
    CZTaskEventAllow,
    CZTaskEventDisallow
};

typedef NSString * CZTaskIdentifier;
typedef NSString * CZTaskLaunchOptionsKey;
typedef void (^CZTaskEventCompletionHandler)(CZTaskEventDisposition disposition);

extern CZTaskLaunchOptionsKey const CZTaskLaunchOptionsDelegateKey;

@class CZTask;
@class CZMultitaskManager;

@protocol CZTaskDelegate <NSObject>

@required
- (instancetype)initWithTask:(CZTask *)task;
@property (nonatomic, readonly) UIViewController *taskViewController;

@optional
- (void)task:(CZTask *)task didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (void)taskDidEnterForeground:(CZTask *)task;

- (void)taskWillEnterBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;
- (void)taskDidEnterBackground:(CZTask *)task;

- (void)taskWillTerminate:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler;

- (void)taskWillEnterFullScreen:(CZTask *)task;
- (void)taskDidEnterFullScreen:(CZTask *)task;
- (void)taskWillExitFullScreen:(CZTask *)task;
- (void)taskDidExitFullScreen:(CZTask *)task;

- (CZTask *)secondaryTaskForTask:(CZTask *)task;

- (void)task:(CZTask *)task willTransitionToSize:(CGSize)size;
- (void)task:(CZTask *)task didTransitionToSize:(CGSize)size;

@end

@interface CZTask : NSObject

@property (nonatomic, readonly, copy) CZTaskIdentifier identifier;
@property (nonatomic, readonly, assign) CZTaskState state;
@property (nonatomic, readwrite, strong) UIImage *snapshot;
@property (nonatomic, readonly, weak) id<CZTaskDelegate> delegate;
@property (nonatomic, readonly, weak) CZMultitaskManager *manager;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithIdentifier:(CZTaskIdentifier)identifier NS_DESIGNATED_INITIALIZER;

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (void)didEnterForeground;

- (void)willEnterBackgroundWithCompletionHandler:(CZTaskEventCompletionHandler)completionHandler;
- (void)didEnterBackground;

- (void)willTerminateWithCompletionHandler:(CZTaskEventCompletionHandler)completionHandler;

@end
