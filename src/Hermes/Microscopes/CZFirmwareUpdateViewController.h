//
//  CZFirmwareUpdateViewController.h
//  Matscope
//
//  Created by Halley Gu on 5/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFirmwareUpdateViewController;
@class CZCamera;

@protocol CZFirmwareUpdateViewControllerDelegate <NSObject>

- (void)firmwareUpdateViewControllerDidFinish:(CZFirmwareUpdateViewController *)controller;

@end

@interface CZFirmwareUpdateViewController : UIViewController

@property (nonatomic, assign) id<CZFirmwareUpdateViewControllerDelegate> delegate;
@property (nonatomic, retain) CZCamera *camera;

@end
