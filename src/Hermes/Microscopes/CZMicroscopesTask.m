//
//  CZMicroscopesTask.m
//  Labscope
//
//  Created by Li, Junlin on 12/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopesTask.h"
#import "CZMicroscopeTool.h"
#import "CZMicroscopeViewModelPrivate.h"
#import "CZMultitaskViewController.h"

#if DCM_REFACTORED
#import "CZCMLocalStorage.h"
#import "CZCMDevice.h"
#endif

#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@interface CZMicroscopesTask () <CZMicroscopeToolDelegate, CZCameraThumbnailDelegate>

@property (nonatomic, readwrite, copy) NSString *wlanName;
@property (nonatomic, readwrite, copy) NSString *loginInfo;
@property (nonatomic, readwrite, assign) BOOL isLoadingMicroscopes;
@property (nonatomic, readwrite, copy) NSArray<CZMicroscopeViewModel *> *sortedMicroscopeViewModels;
@property (nonatomic, readwrite, copy) NSString *noMicroscopesText;

@property (nonatomic, strong) CZMicroscopeTool *microscopeTool;
@property (nonatomic, strong) NSMutableDictionary<NSString *, CZMicroscopeViewModel *> *microscopeViewModels;
@property (nonatomic, strong) NSMutableSet<NSString *> *keysForVisibleCameras;

@property (nonatomic, strong) NSTimer *cameraBrowsingTimer;

@end

@implementation CZMicroscopesTask

@dynamic delegate;

- (instancetype)initWithIdentifier:(NSString *)identifier {
    self = [super initWithIdentifier:identifier];
    if (self) {
        _microscopeViewModels = [NSMutableDictionary dictionary];
        _keysForVisibleCameras = [NSMutableSet set];
        
        [self updateWLANName];
        [self updateDCMLoginInfo];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wifiChanged:) name:kCZWifiChangedNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [_cameraBrowsingTimer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCZWifiChangedNotification object:nil];
}

- (CZMicroscopeTool *)microscopeTool {
    if (_microscopeTool == nil) {
        _microscopeTool = [[CZMicroscopeTool alloc] initWithDelegate:self];
        NSString *defaultCameraMACAddress = [[CZDefaultSettings sharedInstance] defaultCameraMACAddress];
        if (defaultCameraMACAddress.length) {
            _microscopeTool.defaultMicroscopekey = defaultCameraMACAddress;
        }
#if DCM_REFACTORED
        if (![[CZDCMManager defaultManager] isCMServerConnected]) {
#endif
            [_microscopeTool loadManuallyAddCamerasList];
#if DCM_REFACTORED
        }
#endif
    }
    return _microscopeTool;
}

#pragma mark - Updaters

- (void)updateWLANName {
    if ([CZWifiNotifier copyOriginalSSID] == nil) {
        self.wlanName = [L(@"WLAN_TITLE") stringByAppendingString:[CZWifiNotifier copySSID]];
    } else {
        self.wlanName = [NSString stringWithFormat:L(@"CONNECTED_TO_WLAN"), [CZWifiNotifier copySSID]];
    }
    
    [self updateNoMicroscopesText];
}

- (void)updateDCMLoginInfo {
#ifdef DEBUG
#if DCM_REFACTORED
    NSString *ipAddress = [NSString stringWithFormat:@"%@", [CZCMLocalStorage shareInstance].clientServiceHost];
    NSString *account = [NSString stringWithFormat:@"%@", [CZDCMLoginStorage defaultStorage].userName];
    NSString *device = [NSString stringWithFormat:@"%@", [CZCMDevice deviceId]];
    NSString *name = [NSString stringWithFormat:@"%@", [CZCMDevice deviceName]];
    ipAddress = ipAddress.length > 8 ? [ipAddress substringWithRange:NSMakeRange(ipAddress.length - 8, 3)] : ipAddress;
    device = device.length > 4 ? [device substringWithRange:NSMakeRange(ipAddress.length - 4, 4)] : device;
    account = account.length > 10 ? [account substringWithRange:NSMakeRange(0, 10)] : account;
    name = name.length > 3 ? [name substringWithRange:NSMakeRange(name.length - 3, 3)] : name;
    
    NSString *infoText = [NSString stringWithFormat:@"Connected IP:%@, Account:%@, Device:%@, Name:%@", ipAddress, account, device, name];
    self.dcmLoginInfo = infoText;
#endif
#endif
}

- (void)updateSortedMicroscopeViewModels {
    NSArray<NSString *> *sortedKeys = self.microscopeTool.sortedMicroscopeKeys;
    
    NSMutableArray<CZMicroscopeViewModel *> *sortedMicroscopeViewModels = [NSMutableArray array];
    for (NSString *key in sortedKeys) {
        CZMicroscopeViewModel *microscopeViewModel = self.microscopeViewModels[key];
        if (microscopeViewModel) {
            [sortedMicroscopeViewModels addObject:microscopeViewModel];
        }
    }
    
    self.sortedMicroscopeViewModels = [sortedMicroscopeViewModels copy];
    
    [self updateNoMicroscopesText];
}

- (void)updateNoMicroscopesText {
    if (self.sortedMicroscopeViewModels.count > 0) {
        self.noMicroscopesText = nil;
    } else {
        if ([CZWifiNotifier copyOriginalSSID] == nil) {
            self.noMicroscopesText = L(@"MICROSCOPES_NOT_FOUND");
        } else {
            self.noMicroscopesText = [NSString stringWithFormat:L(@"MICROSCOPES_NOT_FOUND_IN_WLAN"), [CZWifiNotifier copySSID]];
        }
    }
}

#pragma mark - Setters

- (void)setWlanName:(NSString *)wlanName {
    _wlanName = [wlanName copy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopesTaskWLANNameDidChange:)]) {
        [self.delegate microscopesTaskWLANNameDidChange:self];
    }
}

- (void)setLoginInfo:(NSString *)loginInfo {
    _loginInfo = [loginInfo copy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopesTaskLoginInfoDidChange:)]) {
        [self.delegate microscopesTaskLoginInfoDidChange:self];
    }
}

- (void)setIsLoadingMicroscopes:(BOOL)isLoadingMicroscopes {
    _isLoadingMicroscopes = isLoadingMicroscopes;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopesTaskLoadingMicroscopesDidChange:)]) {
        [self.delegate microscopesTaskLoadingMicroscopesDidChange:self];
    }
}

- (void)setSortedMicroscopeViewModels:(NSArray<CZMicroscopeViewModel *> *)sortedMicroscopeViewModels {
    _sortedMicroscopeViewModels = [sortedMicroscopeViewModels copy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopesTaskSortedMicroscopeViewModelsDidChange:)]) {
        [self.delegate microscopesTaskSortedMicroscopeViewModelsDidChange:self];
    }
}

- (void)setNoMicroscopesText:(NSString *)noMicroscopesText {
    _noMicroscopesText = [noMicroscopesText copy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopesTaskNoMicroscopesTextDidChange:)]) {
        [self.delegate microscopesTaskNoMicroscopesTextDidChange:self];
    }
}

#pragma mark - Inputs

- (void)startMicroscopeBrowsing {
    [self.microscopeTool enableCameraBrowsing];
}

- (void)stopMicroscopeBrowsing {
    [self.microscopeTool disableCameraBrowsingRefreshThumbnail];
}

- (void)microscopeBecomeVisible:(CZMicroscopeViewModel *)microscopeViewModel {
    NSString *key = [self.microscopeViewModels allKeysForObject:microscopeViewModel].firstObject;
    if (key != nil) {
        [self.keysForVisibleCameras addObject:key];
    }
}

- (void)microscopeBecomeInvisible:(CZMicroscopeViewModel *)microscopeViewModel {
    NSString *key = [self.microscopeViewModels allKeysForObject:microscopeViewModel].firstObject;
    if (key != nil) {
        [self.keysForVisibleCameras removeObject:key];
    }
}

- (void)selectMicroscope:(CZMicroscopeViewModel *)microscopeViewModel {
    [self.microscopeTool microscopeSelected:microscopeViewModel.camera.macAddress forceSelect:NO];
}

- (void)removeMicroscope:(CZMicroscopeViewModel *)microscopeViewModel {
    NSString *removeMicroscopeKey = [self.microscopeViewModels allKeysForObject:microscopeViewModel].firstObject;
    if (removeMicroscopeKey == nil) {
        return;
    }
    
    CZMicroscopeModel *removeModel = self.microscopeTool.microscopeModels[removeMicroscopeKey];
    if (removeModel == nil) {
        return;
    }
    
    [self removeElementXmlFile:removeModel.cameraMACAddress];
    
    [removeModel removeSlotsFromDefaults];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:removeModel.cameraMACAddress];
    
    NSArray *settings = [[CZDefaultSettings sharedInstance] manuallyAddMicroscopesSettings];
    if (settings.count > 0) {
        [[CZDefaultSettings sharedInstance] removeManuallyMicroscopeSettingsAtMacAddress:removeModel.cameraMACAddress];
    }
    
    // remove shading correction reference file
    NSString *referenceImageFileName = [NSString stringWithFormat:@"%@.jpg", removeModel.cameraMACAddress];
    NSString *referenceImageFilePath = [[[NSFileManager defaultManager] cz_cachePath] stringByAppendingPathComponent:referenceImageFileName];
    [[NSFileManager defaultManager] removeItemAtPath:referenceImageFilePath error:NULL];
    
    [self.microscopeTool removeMicroscopeForKey:removeMicroscopeKey];
    [self.microscopeViewModels removeObjectForKey:removeMicroscopeKey];
    
    [self updateSortedMicroscopeViewModels];
}

- (void)prepareForConfiguringMicroscope:(CZMicroscopeViewModel *)microscopeViewModel withCompletionHandler:(void (^)(CZCamera *, CZMicroscopeModel *, BOOL))completionHandler {
    NSString *microscopeKey = [self.microscopeViewModels allKeysForObject:microscopeViewModel].firstObject;
    if (microscopeKey == nil) {
        return;
    }
    
    CZCamera *camera = self.microscopeTool.microscopeList[microscopeKey];
    if (camera == nil) {
        camera = [CZMicroscopeTool newPlaceholderCamera:microscopeKey cameraName:nil pin:nil];
    }
    
    if (camera == nil) {
        return;
    }
    
    CZMicroscopeStatus *status = self.microscopeTool.microscopeStatus[microscopeKey];
    
    CZMicroscopeModel *model = self.microscopeTool.microscopeModels[microscopeKey];
    
    if (completionHandler) {
        completionHandler(camera, model, status.isLost);
    }
}

- (void)applyConfigurationForCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel {
    NSString *microscopeKey = camera.macAddress;
    CZMicroscopeModel *modelCreated = [microscopeModel copy];
    self.microscopeTool.microscopeModels[microscopeKey] = modelCreated;
    
    //the current microscope just become from default to not default
    if ((self.microscopeTool.defaultMicroscopekey == microscopeKey) && (!modelCreated.isDefault)) {
        self.microscopeTool.defaultMicroscopekey = nil;
        [[CZDefaultSettings sharedInstance] setDefaultCameraMACAddress:nil];
    }
    
    if (modelCreated.isDefault) {
        [self.microscopeTool toggleDefaultModel:modelCreated];
        self.microscopeTool.defaultMicroscopekey = microscopeKey;
    }
    
    [self.microscopeTool updateDefaultModelAppearance];
    
    CZMicroscopeViewModel *microscope = self.microscopeViewModels[microscopeKey];
    microscope.microscopeName = modelCreated.name;
    microscope.locked = microscopeModel.pin.length > 0;
    
    camera = [self.microscopeTool cameraAtMicroscopeKey:microscopeKey];
    if (camera == nil) {
        camera = [CZMicroscopeTool newPlaceholderCamera:microscopeKey cameraName:nil pin:nil];
    }
    
    if (![modelCreated.name isEqualToString:camera.displayName]) {
        [camera setDisplayName:modelCreated.name andUpload:NO];
        if ([camera.macAddress hasPrefix:CZPlusCameraMACAddressPrefix]) {
            [[CZDefaultSettings sharedInstance] updateManuallyMicroscopeSettingsName:modelCreated.name atMacAddress:camera.macAddress];
        }
    }
    
    [camera setPIN:microscopeModel.pin andUpload:YES];
    [[CZCameraBrowser sharedInstance] pauseCameraRefreshing:camera duration:6];
    
#if MIC_REFACTORED
    //Update live tab title if it's currently selected
    if ([microscopeKey isEqualToString:self.microscopeTool.currentMicroscopekey]) {
        [self updateLiveTabWith:camera.displayName];
    }
    
    CZTabBarController *tabBar = [CZAppDelegate get].tabBar;
    [tabBar checkMultiMicroscopesMode];
    [tabBar autoReconnectCamerasInSingleMode];
#endif
}

- (void)refreshCameraAppearance {
    //VirtualMicroscope show configuration here.
    CZVirtualMicroscopesOption virtualMicroscopesOption = [[CZDefaultSettings sharedInstance] virtualMicroscopesOption];
    self.microscopeTool.virtualMicroscopesOption = virtualMicroscopesOption;
    
    // check if live view is playing missing camera.
    CZCamera *camera = self.manager.liveTask.currentCamera;
    if (camera.macAddress && self.microscopeTool.microscopeList[camera.macAddress] == nil) {
        CZCamera *placeholder = [CZMicroscopeTool newPlaceholderCamera:camera.macAddress cameraName:camera.displayName pin:camera.pin];
        [self.manager.liveTask setCurrentCamera:placeholder];
    }
}

- (void)updateVirtualCameraAppearance {
    if (self.microscopeTool.virtualMicroscopesOption == [[CZDefaultSettings sharedInstance] virtualMicroscopesOption]) {
        return;
    }
    [self refreshCameraAppearance];
}

- (void)updatePlusCameraAppearance {
#if DCM_REFACTORED
    // Add DCM configuration, when DCM service is available, manually add camera is not available
    if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
        self.microscopeTool.plusMicroscopeEnabled = NO;
    } else {
#endif
        BOOL plusMicroscopeEnabled = [[CZDefaultSettings sharedInstance] enableManuallyAddMicroscopes];
        if (self.microscopeTool.plusMicroscopeEnabled == plusMicroscopeEnabled) {
            return;
        }
        self.microscopeTool.plusMicroscopeEnabled = plusMicroscopeEnabled;
#if DCM_REFACTORED
    }
#endif
}

- (BOOL)validatePIN:(NSString *)pin forMicroscope:(CZMicroscopeViewModel *)microscopeViewModel remembersPIN:(BOOL)remembersPIN {
    NSString *key = [self.microscopeViewModels allKeysForObject:microscopeViewModel].firstObject;
    if (key == nil) {
        return NO;
    }
    
    CZMicroscopeModel *model = self.microscopeTool.microscopeModels[key];
    CZCamera *camera = self.microscopeTool.microscopeList[key];
    
    if (!remembersPIN) {
        model.cachedCameraPIN = @"";
    }
    
    if ([camera validatePIN:pin]) {
        if (remembersPIN) {
            model.cachedCameraPIN = pin;
        }
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)canCreatePlaceholderForManuallyAddCamera:(CZCamera *)camera {
    return [self.microscopeTool canCreatePlaceholderForManuallyAddCamera:camera];
}

- (void)createPlaceholderForManuallyAddCamera:(CZCamera *)camera {
    [self.microscopeTool createPlaceholderForManuallyAddCamera:camera];
}

#pragma mark - Notification

- (void)wifiChanged:(NSNotification *)note {
    [self updateWLANName];
    [self updateDCMLoginInfo];
}

#pragma mark - Timer

- (void)cameraBrowsingTimeout:(NSTimer *)timer {
    self.cameraBrowsingTimer = nil;
    self.isLoadingMicroscopes = NO;
}

#pragma mark - Private

- (void)removeElementXmlFile:(NSString *)cameraMacAddress {
    NSString *cachedPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *elementFile = nil;
    if (cameraMacAddress.length > 0) {
        cameraMacAddress = [cameraMacAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
        elementFile = [cachedPath stringByAppendingPathComponent:cameraMacAddress];
        elementFile = [elementFile stringByAppendingPathComponent:@"element.xml"];;
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:elementFile]) {
        [[NSFileManager defaultManager] removeItemAtPath:elementFile error:nil];
    }
}

#pragma mark - CZMicroscopeToolDelegate

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool setCamera:(CZCamera *)camera status:(BOOL)enabled {
    CZMicroscopeViewModel *microscopeViewModel = self.microscopeViewModels[camera.macAddress];
    if (microscopeViewModel == nil) {
        return;
    }
    
    if (microscopeViewModel.enabled != enabled) {
        microscopeViewModel.enabled = enabled;
        if (enabled) {
            microscopeViewModel.thumbnail = [camera thumbnail];
        }
    } else {
        if (!enabled) {
            microscopeViewModel.enabled = NO;
        }
    }
    
    [camera setThumbnailDelegate:self];
    
    microscopeViewModel.locked = [camera hasPIN];
    if (![microscopeViewModel.microscopeName isEqualToString:camera.displayName]) {
        if (![camera.macAddress hasPrefix:CZPlusCameraMACAddressPrefix]) {
            microscopeViewModel.microscopeName = camera.displayName;
        }
    }
}

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool didConnectToCamera:(NSString *)cameraMacAddress forceSwitch:(BOOL)forceSwitch {
    NSAssert(cameraMacAddress, @"camera MAC address can't be nil!");
    
#if DCM_REFACTORED
    // Do not response to the connect callback method when the DCM service is connect;
    if ([CZDCMManager defaultManager].shouldNotResponseCameraFoundWhileConnecting) {
        return;
    }
#endif
    
    CZMicroscopeViewModel *didConnectedMicroscopeViewModel = self.microscopeViewModels[cameraMacAddress];
    for (CZMicroscopeViewModel *microscopeViewModel in self.microscopeViewModels.allValues) {
        microscopeViewModel.isCurrentMicroscope = (microscopeViewModel == didConnectedMicroscopeViewModel);
    }
    
    CZCamera *camera = microscopeTool.microscopeList[cameraMacAddress];
    if (camera == nil) {
        camera = [CZMicroscopeTool newPlaceholderCamera:cameraMacAddress cameraName:nil pin:nil];
    }
    
    [self.manager.liveTask setCurrentCamera:camera resetExclusiveLock:YES];
    [self.manager switchToLiveTask];
    
#if LIVE_REFACTORED
    if (forceSwitch) {
        liveViewController.shouldValidateMNALevel = YES;
        [delegate.tabBar switchToTabAtIndex:kCZLiveViewController forceSwitch:YES];
    }
#endif
}

- (void)microscopeToolWillCreateMicroscopeButton:(CZMicroscopeTool *)microscopeTool {
#if DCM_REFACTORED
    // Clear the store camera list during user login
    if ([CZDCMManager defaultManager].shouldNotResponseCameraFoundWhileConnecting) {
        NSArray<NSString *> *exitKeys = [NSArray arrayWithArray:self.microscopeTool.microscopeKeys];
        for (NSString *key in exitKeys) {
            [self.microscopeTool removeMicroscopeForKey:key];
        }
    }
#endif
    
    NSArray<NSString *> *sortedKeys = self.microscopeTool.sortedMicroscopeKeys;
    for (NSString *microscopeKey in sortedKeys) {
        CZMicroscopeViewModel *microscopeViewModel = self.microscopeViewModels[microscopeKey];
        if (microscopeViewModel == nil) {
            CZCamera *camera = self.microscopeTool.microscopeList[microscopeKey];
            CZMicroscopeViewModel *microscopeViewModel = [[CZMicroscopeViewModel alloc] initWithCamera:camera];
            self.microscopeViewModels[microscopeKey] = microscopeViewModel;
            [camera setThumbnailDelegate:self];
        }
    }
    
    for (NSString *microscopeKey in self.microscopeViewModels.allKeys) {
        // Remove the virtual camera button if hide virtual camera
        NSUInteger index = [self.microscopeTool indexOfMicroscopeKeys:microscopeKey];
        if (index == NSNotFound) {
            [self.microscopeViewModels removeObjectForKey:microscopeKey];
        }
        
        // Refresh microscope UI
        CZMicroscopeViewModel *microscopeViewModel = self.microscopeViewModels[microscopeKey];
        CZCamera *camera = self.microscopeTool.microscopeList[microscopeKey];
        CZMicroscopeStatus *status = self.microscopeTool.microscopeStatus[microscopeKey];
        BOOL isLost = status.isLost;
        if (!isLost) {
            microscopeViewModel.thumbnail = camera.thumbnail;
        } else {
            microscopeViewModel.thumbnail = nil;
        }
        NSString *microscopeName = camera.displayName;
        if ([camera.macAddress hasPrefix:CZPlusCameraMACAddressPrefix]) {
            microscopeName = [microscopeName stringByAppendingFormat:@"%@%@%@", @"(", camera.ipAddress, @")"];
        }
        
        microscopeViewModel.microscopeName = microscopeName;
        microscopeViewModel.locked = [camera hasPIN];
    }
    
    [self updateSortedMicroscopeViewModels];
}

- (void)microscopeToolDidShowProgressHUD:(CZMicroscopeTool *)microscopeTool {
    self.isLoadingMicroscopes = YES;
    [self.cameraBrowsingTimer invalidate];
    self.cameraBrowsingTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(cameraBrowsingTimeout:) userInfo:nil repeats:NO];
}

- (void)microscopeToolDidDismissProgressHUD:(CZMicroscopeTool *)microscopeTool {
    if (self.cameraBrowsingTimer) {
        [self.cameraBrowsingTimer invalidate];
        self.cameraBrowsingTimer = nil;
        self.isLoadingMicroscopes = NO;
    }
}

- (BOOL)microscopeTool:(CZMicroscopeTool *)microscopeTool shouldRefreshThumbnailForCamera:(CZCamera *)camera {
    for (NSString *key in self.keysForVisibleCameras) {
        if ([key isEqualComparisionWithCaseInsensitive:camera.macAddress]) {
            return YES;
        }
    }
    return NO;
}

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool didLostCameras:(NSArray<CZCamera *> *)lostCameras {
#if MIC_REFACTORED
    [[CZAppDelegate get].tabBar autoReconnectCamerasInSingleMode];
#endif
}

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool didFindCameras:(NSArray<CZCamera *> *)newCameras {
#if MIC_REFACTORED
    [[CZAppDelegate get].tabBar reconnectCamerasAfterSleep];
#endif
}

#pragma mark - CZCameraThumbnailDelegate

- (void)camera:(CZCamera *)camera didUpdateThumbnail:(UIImage *)thumbnail {
    for (NSString *key in self.microscopeViewModels.allKeys) {
        if ([key isEqualComparisionWithCaseInsensitive:camera.macAddress]) {
            self.microscopeViewModels[key].thumbnail = thumbnail;
            break;
        }
    }
}

@end
