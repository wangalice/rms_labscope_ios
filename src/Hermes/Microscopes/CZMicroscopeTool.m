//
//  CZMicroscopeTool.m
//  Matscope iPhone
//
//  Created by Sherry Xu on 5/11/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeTool.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>

#if DCM_REFACTORED
#import "CZDCMManager.h"
#import "CZDCMManager+DefaultSettings.h"
#import "CZDCMCameraManager.h"
#endif

#define kUIThumbnailMaxWidthIPad 289

static NSString *const kVirtualCameraMacAddressPrefix = @"00:00:00:00:00:0";
static NSString *const kPlusCameraMacAddressPrefix = @"22:00:00:00:00:0";

@implementation CZMicroscopeStatus

@end

@interface CZMicroscopeTool () <CZCameraBrowserDelegate
#if DCM_REFACTORED
,CZDCMCameraManagerDelegate
#endif
>

@property (nonatomic, retain) NSMutableDictionary<NSString *, CZMicroscopeModel *> *mnaList;
@property (nonatomic, retain) NSMutableDictionary<NSString *, CZMicroscopeModel *> *modelsFromMNA;
@property (nonatomic, retain) NSMutableDictionary<NSString *, CZCamera *> *microscopeListM;

@property (nonatomic, retain) NSMutableArray<NSString *> *microscopeKeys; //all the cameras' mac addresses
@property (nonatomic, retain) NSMutableArray<NSString *> *virtualCameraKeys;

- (void)refreshCameraList:(NSArray<CZCamera *> *)cameras;
- (void)microscopeSelected:(NSString *)macAddress;
- (void)microscopeSelected:(NSString *)macAddress forceSelect:(BOOL)forceSelect;

@end

@implementation CZMicroscopeTool

- (id)initWithDelegate:(id <CZMicroscopeToolDelegate>)delegate {
    self = [super init];
    if (self) {
        _virtualCameraKeys = [[NSMutableArray alloc] init];
        _microscopeModels = [[NSMutableDictionary alloc] init];
        _microscopeStatus = [[NSMutableDictionary alloc] init];
        _mnaList = [[NSMutableDictionary alloc] init];
        _modelsFromMNA = [[NSMutableDictionary alloc] init];
        _delegate = delegate;
        
        //FIXME:SG CameraBrowser scan camera device first call here. Add DCM Server connection judgment logic
#if DCM_REFACTORED
        if ([CZDCMManager defaultManager].state != CMConnectStateDisConnect) {
            CGFloat screenScale = [[UIScreen mainScreen] scale];
            [[CZDCMCameraManager sharedInstance] setThumbnailSize:CGSizeMake(kUIThumbnailMaxWidthIPad * screenScale, kUIThumbnailMaxWidthIPad * screenScale)];
            [[CZDCMCameraManager sharedInstance] setDelegate:self];
        }
        else {
#endif
            if (![[CZCameraBrowser sharedInstance] isBrowsing]) {
                [self.delegate microscopeToolDidShowProgressHUD:self];
                [[CZCameraBrowser sharedInstance] beginBrowsing];
            }
            CGFloat screenScale = [[UIScreen mainScreen] scale];
            [[CZCameraBrowser sharedInstance] setThumbnailSize:CGSizeMake(kUIThumbnailMaxWidthIPad * screenScale, kUIThumbnailMaxWidthIPad * screenScale)];
            [[CZCameraBrowser sharedInstance] setDelegate:self];
#if DCM_REFACTORED
        }
#endif
        
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self selector:@selector(cameraBrowserDidFindCameras:) name:kCZCameraBrowserNotificationFound object:nil];
        [notificationCenter addObserver:self selector:@selector(cameraBrowserDidLoseCameras:) name:kCZCameraBrowserNotificationLost object:nil];
        [notificationCenter addObserver:self selector:@selector(cameraBrowserDidGetUpdatedDataFromCameras:) name:kCZCameraBrowserNotificationUpdate object:nil];
        [notificationCenter addObserver:self selector:@selector(cameraBrowserDidRemoveCameras:) name:kCZCameraBrowserNotificationRemoved object:nil];
        [notificationCenter addObserver:self selector:@selector(cameraBrowserDidReplaceCamera:) name:kCZCameraBrowserNotificationReplace object:nil];
    }
    return self;
}

- (void)dealloc {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self name:kCZCameraBrowserNotificationFound object:nil];
    [notificationCenter removeObserver:self name:kCZCameraBrowserNotificationLost object:nil];
    [notificationCenter removeObserver:self name:kCZCameraBrowserNotificationUpdate object:nil];
    [notificationCenter removeObserver:self name:kCZCameraBrowserNotificationRemoved object:nil];
    [notificationCenter removeObserver:self name:kCZCameraBrowserNotificationReplace object:nil];
    
    [_microscopeModels release];
    [_virtualCameraKeys release];
    [_microscopeKeys release];
    [_microscopeListM release];
    [_microscopeStatus release];
    [_defaultMicroscopekey release];
    [_currentMicroscopekey release];
    [_mnaList release];
    [_modelsFromMNA release];
    
    [super dealloc];
}

- (void)loadManuallyAddCamerasList {
    NSArray *microscopesSettings = [[CZDefaultSettings sharedInstance] manuallyAddMicroscopesSettings];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *settings in microscopesSettings) {
        NSString *macAddress = settings[kManuallyAddMicroscopeMacAddress];
        NSInteger index = [self indexOfMicroscopeKeys:macAddress];
        if (index == NSNotFound) {
            NSString *ipAddress = settings[kManuallyAddMicroscopeIPAddress];
            NSString *displayName = settings[kManuallyAddMicroscopeName];
            CZCamera *manuallyAddCamera = [[CZCamera alloc] init];
            [manuallyAddCamera setIpAddress:ipAddress];
            [manuallyAddCamera setMacAddress:macAddress];
            [manuallyAddCamera setDisplayName:displayName andUpload:NO];
            
            self.microscopeListM[macAddress] = manuallyAddCamera;
            
            [self checkAndCreateMicroscopeModelWithCamera:manuallyAddCamera];
            
            if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
                [self.delegate microscopeToolWillCreateMicroscopeButton:self];
            }
            [tempArray addObject:manuallyAddCamera];
        }
    }
    NSArray *manuallyAddCameras = [NSArray arrayWithArray:tempArray];
    [self disableLostCameras:manuallyAddCameras];
    [tempArray release];
}

- (BOOL)canCreatePlaceholderForManuallyAddCamera:(CZCamera *)camera {
    for (CZCamera *existingCamera in self.cameras) {
        if ([existingCamera isKindOfClass:[CZPlusCamera class]]) {
            continue;
        }
        if ([existingCamera.ipAddress isEqualToString:camera.ipAddress]) {
            return NO;
        }
    }
    return YES;
}

- (void)createPlaceholderForManuallyAddCamera:(CZCamera *)camera {
    NSString *microscopeKey = camera.macAddress;
    NSInteger index = [self indexOfMicroscopeKeys:microscopeKey];
    if (index == NSNotFound) {
        self.microscopeListM[microscopeKey] = camera;
        [self checkAndCreateMicroscopeModelWithCamera:camera];
        
        if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
            [self.delegate microscopeToolWillCreateMicroscopeButton:self];
        }
        [self disableLostCameras:@[camera]];
    }
}

- (void)setVirtualMicroscopesOption:(CZVirtualMicroscopesOption)virtualMicroscopesOption {
    _virtualMicroscopesOption = virtualMicroscopesOption;
    
    // Convert to bool
    NSArray<NSString *> *allCameraKeys = _microscopeListM.allKeys;
    [allCameraKeys retain];
    
    BOOL virtualMicroscopesEnabled = NO;
    if (_virtualMicroscopesOption == kCZNeverShowVirtualMicroscopes) {
        virtualMicroscopesEnabled = NO;
    } else if (_virtualMicroscopesOption == kCZAlwaysShowVirtualMicroscopes) {
        virtualMicroscopesEnabled = YES;
    } else if (_virtualMicroscopesOption == kCZAutomaticShowVirtualMicroscopes) {
        virtualMicroscopesEnabled = YES;
        for (NSString *cameraKey in allCameraKeys) {
            CZCamera *camera = _microscopeListM[cameraKey];
            if (![camera isKindOfClass:[CZLocalFileCamera class]]) {
                if (![camera isKindOfClass:[CZPlusCamera class]]) {
                    virtualMicroscopesEnabled = NO;
                    break;
                }
            }
        }
    }
    _virtualMicroscopesHidden = !virtualMicroscopesEnabled;
    [CZCameraBrowser sharedInstance].virtualCameraEnabled = virtualMicroscopesEnabled;
    
    if (!virtualMicroscopesEnabled) {
        // remove virtual camera from list
        BOOL removed = NO;
        for (NSString *cameraKey in allCameraKeys) {
            CZCamera *camera = _microscopeListM[cameraKey];
            if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
                [_microscopeListM removeObjectForKey:cameraKey];
                removed = YES;
            }
        }
        // refresh UI
        if (removed) {
            if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
                [self.delegate microscopeToolWillCreateMicroscopeButton:self];
            }
        }
    }
    [allCameraKeys release];
}

- (void)setPlusMicroscopeEnabled:(BOOL)plusMicroscopeEnabled {
    _plusMicroscopeEnabled = plusMicroscopeEnabled;
    [CZCameraBrowser sharedInstance].plusCameraEnabled = plusMicroscopeEnabled;
    
    NSArray<NSString *> *allCameraKeys = _microscopeListM.allKeys;
    [allCameraKeys retain];
    
    if (!plusMicroscopeEnabled) {
        // remove virtual camera from list
        BOOL removed = NO;
        for (NSString *cameraKey in allCameraKeys) {
            CZCamera *camera = _microscopeListM[cameraKey];
            if ([camera isKindOfClass:[CZPlusCamera class]]) {
                [_microscopeListM removeObjectForKey:cameraKey];
                removed = YES;
            }
        }
        // refresh UI
        if (removed) {
            if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
                [self.delegate microscopeToolWillCreateMicroscopeButton:self];
            }
        }
    }
    [allCameraKeys release];
}

- (void)enableCameraBrowsing {
    //FIXME:Camera browser scan camera device call here.
#if DCM_REFACTORED
    if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
        [[CZDCMCameraManager sharedInstance] setShouldThumbnailUpdateEnable:YES];
    } else {
#endif
        if (![[CZCameraBrowser sharedInstance] isBrowsing]) {
            [self.delegate microscopeToolDidShowProgressHUD:self];
            [[CZCameraBrowser sharedInstance] beginBrowsing];
        }
        
        [[CZCameraBrowser sharedInstance] setThumbnailRefreshingEnabled:YES];
#if DCM_REFACTORED
    }
#endif
}

- (void)disableCameraBrowsingRefreshThumbnail {
#if DCM_REFACTORED
    if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
        [[CZDCMCameraManager sharedInstance] setShouldThumbnailUpdateEnable:NO];
    } else {
#endif
        [[CZCameraBrowser sharedInstance] setThumbnailRefreshingEnabled:NO];
#if DCM_REFACTORED
    }
#endif
}

- (NSArray<NSString *> *)microscopeKeys {
    return self.microscopeListM.allKeys;
}

- (NSArray<NSString *> *)sortedMicroscopeKeys {
    if (_plusMicroscopeEnabled) {
        NSMutableArray *microscopeKeys = [NSMutableArray arrayWithArray:self.microscopeListM.allKeys];
        BOOL hasPlusCamera = NO;
        for (NSUInteger i = 0; i < microscopeKeys.count; i++) {
            NSString *microscopeKey = microscopeKeys[i];
            if ([microscopeKey hasPrefix:kPlusCameraMacAddressPrefix]) {
                [microscopeKeys exchangeObjectAtIndex:i withObjectAtIndex:microscopeKeys.count - 1];
                hasPlusCamera = YES;
                break;
            }
        }
        if (hasPlusCamera) {
            NSArray *subArray = [microscopeKeys subarrayWithRange:NSMakeRange(0, microscopeKeys.count - 1)];
            NSMutableArray *sortedMutableArray = [[subArray mutableCopy] autorelease];
            [sortedMutableArray sortUsingComparator:^NSComparisonResult(NSString *key1, NSString *key2) {
                CZCamera *c1 = self.microscopeListM[key1];
                CZCamera *c2 = self.microscopeListM[key2];
                return [c1.displayName caseInsensitiveCompare:c2.displayName];
            }];
            
            [sortedMutableArray addObject:microscopeKeys[microscopeKeys.count - 1]];
            NSArray *sortedArray = [NSArray arrayWithArray:sortedMutableArray];
            return sortedArray;
        } else {
            return [self.microscopeListM.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *key1, NSString *key2) {
                CZCamera *c1 = self.microscopeListM[key1];
                CZCamera *c2 = self.microscopeListM[key2];
                return [c1.displayName caseInsensitiveCompare:c2.displayName];
            }];
        }
    } else {
        return [self.microscopeListM.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *key1, NSString *key2) {
            CZCamera *c1 = self.microscopeListM[key1];
            CZCamera *c2 = self.microscopeListM[key2];
            return [c1.displayName caseInsensitiveCompare:c2.displayName];
        }];
    }
}

- (NSArray<CZCamera *> *)cameras {
    return self.microscopeListM.allValues;
}

- (NSDictionary<NSString *, CZCamera *> *)microscopeList {
    return [NSDictionary dictionaryWithDictionary:self.microscopeListM];
}

- (NSMutableDictionary<NSString *, CZCamera *> *)microscopeListM {
    if (_microscopeListM == nil) {
        _microscopeListM = [[NSMutableDictionary alloc] init];
    }
   
    return _microscopeListM;
}

- (NSUInteger)indexOfMicroscopeKeys:(NSString *)macAddress {
    return [self.microscopeKeys indexOfObject:macAddress];
}

- (void)microscopeSelected:(NSString *)macAddress {
    [self microscopeSelected:macAddress forceSelect:NO];
}

- (void)microscopeSelected:(NSString *)macAddress forceSelect:(BOOL)forceSelect {
    CZMicroscopeStatus *microscopeStatus = self.microscopeStatus[macAddress];
    if (!forceSelect && microscopeStatus.isLost && !microscopeStatus.isDefault) {
        return;
    }
    self.currentMicroscopekey = macAddress;
    if ([self.delegate respondsToSelector:@selector(microscopeTool:didConnectToCamera:forceSwitch:)]) {
        [self.delegate microscopeTool:self didConnectToCamera:self.currentMicroscopekey forceSwitch:YES];
    }
}

- (CZMicroscopeModel *)currentMicroscopeModel {
    if (self.currentMicroscopekey) {
        NSString *key = self.currentMicroscopekey;
        return self.microscopeModels[key];
    }
    return nil;
}

- (CZCamera *)cameraAtMicroscopeKey:(NSString *)key {
    if (key) {
        return self.microscopeListM[key];
    }
    return nil;
}

- (void)removeCameraForKey:(NSString *)key {
    [[CZCameraBrowser sharedInstance] removeCameraForKey:key];
    [_microscopeListM removeObjectForKey:key];
}

- (void)removeMicroscopeForKey:(NSString *)key {
    [_microscopeModels removeObjectForKey:key];
    [_microscopeStatus removeObjectForKey:key];
    [self removeCameraForKey:key];
}

- (CZMicroscopeModel *)microscopeModelAtMicroscopeKey:(NSString *)key {
    if (key) {
        return self.microscopeModels[key];
    }
    return nil;
}

- (BOOL)defaultMicroscopeSelected {
    if (self.defaultMicroscopekey) {
        [self microscopeSelected:self.defaultMicroscopekey forceSelect:YES];
        return TRUE;
    }
    return FALSE;
}

- (BOOL)currentMicroscopeSelected {
    if (self.currentMicroscopekey) {
        CZMicroscopeStatus *microscopeStatus = self.microscopeStatus[self.currentMicroscopekey];
        if (!microscopeStatus.isLost) {
            [self microscopeSelected:self.currentMicroscopekey forceSelect:NO];
            return TRUE;
        }
    }
    return FALSE;
}

- (BOOL)findActiveMicroscopeAndSelect {
    BOOL isActive = [self currentMicroscopeSelected];
    if (isActive) {
        return TRUE;
    }

    for (CZCamera *camera in self.cameras) {
        CZMicroscopeStatus *microscopeStatus = self.microscopeStatus[camera.macAddress];
        if (!microscopeStatus.isLost && camera && ![camera isKindOfClass:[CZLocalFileCamera class]]) {
            self.currentMicroscopekey = camera.macAddress;
            return [self currentMicroscopeSelected];
        }
    }

    return FALSE;
}

- (BOOL)assignCamera:(NSString *)key {
    if (key) {
        self.currentMicroscopekey = key;
        if ([self.delegate respondsToSelector:@selector(microscopeTool:didConnectToCamera:forceSwitch:)]) {
            [self.delegate microscopeTool:self didConnectToCamera:self.currentMicroscopekey forceSwitch:NO];
        }
        return TRUE;
    }
    return FALSE;
}

//Disable the default camera when Labscope connected to the DCM service;
- (BOOL)assignDefaultCamera {
    return [self assignCamera:self.defaultMicroscopekey];
}

- (BOOL)assignCurrentCamera {
    return [self assignCamera:self.currentMicroscopekey];
}

- (BOOL)findActiveMicroscopeAndAssign {
    BOOL isActive = [self assignCurrentCamera];
    if (isActive) {
        return TRUE;
    }
    
    for (CZCamera *camera in self.cameras) {
        CZMicroscopeStatus *microscopeStatus = self.microscopeStatus[camera.macAddress];
        if (!microscopeStatus.isLost && camera && ![camera isKindOfClass:[CZLocalFileCamera class]]) {
            self.currentMicroscopekey = camera.macAddress;
            return [self assignCurrentCamera];
        }
    }
    
    return FALSE;
}

- (void)updateDefaultModelAppearance {
    for (NSString *key in self.microscopeKeys) {
        CZMicroscopeModel *model = self.microscopeModels[key];
        CZMicroscopeStatus *status = self.microscopeStatus[key];
        status.isDefault = model.isDefault;
        //TODO:set button isdefault missing
    }
}

- (void)toggleDefaultModel:(CZMicroscopeModel *)selectedModel {
    NSArray *keys = [_microscopeModels allKeys];
    
    for (int i = 0; i < _microscopeModels.count; i++) {
        CZMicroscopeModel *model = _microscopeModels[keys[i]];
        if (model == selectedModel) {
            continue;
        }
        
        if (model.isDefault) {
            model.isDefault = FALSE;
            [model saveToDefaults];
        }
    }
    selectedModel.isDefault = TRUE;
}

- (void)disableLostCameras:(NSArray *)lostCameras {
    if (!lostCameras || lostCameras.count <= 0) {
        return;
    }
    
    for (CZCamera *camera in lostCameras) {
        [self.delegate microscopeTool:self setCamera:camera status:NO];
        [self refreshMicrosopeStatusWithCamera:camera isDefault:NO isLost:YES];
    }
}

- (void)reassignLostCamerasIfActive:(NSMutableArray *)newCameras {
    NSMutableArray *camerasToRemove = [[NSMutableArray alloc] init];
    
    for (CZCamera *camera in newCameras) {
        NSUInteger i = [self indexOfMicroscopeKeys:camera.macAddress];
        CZMicroscopeStatus *status = _microscopeStatus[camera.macAddress];
        if (i != NSNotFound && !status.isLost) {
            CZMicroscopeModel *model = self.microscopeModels[camera.macAddress];
            if (model == nil || [model isMemberOfClass:[CZMicroscopeModel class]]) {
                [self checkAndCreateMicroscopeModelWithCamera:camera];
            } else {
                [model updateDataFromCamera:camera];
            }
            [self.delegate microscopeTool:self setCamera:camera status:YES];
            [self refreshMicrosopeStatusWithCamera:camera isDefault:NO isLost:NO];
            
            [camerasToRemove addObject:camera];
        }
    }
    
    [newCameras removeObjectsInArray:camerasToRemove];
    [camerasToRemove release];
}

- (void)refreshCameraList:(NSArray *)cameras {
    if (!cameras || cameras.count <= 0) {
        return;
    }
    
    NSMutableArray *newCameras = [[NSMutableArray alloc] initWithArray:cameras];
    [self reassignLostCamerasIfActive:newCameras];
    
    for (CZCamera *camera in newCameras) {
        //TODO:button index is missing
        
        NSString *cameraKey = camera.macAddress;
        CZCamera *existCamera = _microscopeListM[cameraKey];
        if (camera != existCamera) {
            _microscopeListM[cameraKey] = camera;
        }
        
        if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
            [self.virtualCameraKeys addObject:cameraKey];
        }
        
        [self checkAndCreateMicroscopeModelWithCamera:camera];
        [self refreshMicrosopeStatusWithCamera:camera isDefault:NO isLost:NO];
    }
  
    [newCameras release];
    
    do {
        NSString *defaultCameraMACAddress = [CZDefaultSettings sharedInstance].defaultCameraMACAddress;
        if (defaultCameraMACAddress.length) {
            NSInteger index = [self indexOfMicroscopeKeys:defaultCameraMACAddress];
            if (index != NSNotFound) {
                self.defaultMicroscopekey = defaultCameraMACAddress;
                break;
            }
        }
       
        // Create placeholder camera in case default camera is not found.
        if (self.defaultMicroscopekey) {
            break;
        }

#if DCM_REFACTORED
        if ([[CZDCMManager defaultManager] enableMultiMicroscopes]) {
            break;
        }
#else
        if ([[CZDefaultSettings sharedInstance] enableMultiMicroscopes]) {
            break;
        }
#endif
        
        if (defaultCameraMACAddress.length == 0) {
            // Pick a camera as default camera in such edge case, that there is no default camera.
            for (CZCamera *camera in self.cameras) {
                if (![camera isKindOfClass:[CZLocalFileCamera class]]) {
                    defaultCameraMACAddress = camera.macAddress;
                    break;
                }
            }
            
            if (defaultCameraMACAddress.length) {
                [[CZDefaultSettings sharedInstance] setDefaultCameraMACAddress:defaultCameraMACAddress];
            }
        }

        if (defaultCameraMACAddress.length == 0) {
            break;
        }

        // If the default camera is virtual camera, try to reuse exist camera.
        if ([defaultCameraMACAddress hasPrefix:kVirtualCameraMacAddressPrefix]) {
            NSString *defaultMicroscopekey = self.defaultMicroscopekey;
            for (CZCamera *camera in cameras) {
                BOOL isEqualMACAddress = [camera.macAddress isEqualComparisionWithCaseInsensitive:defaultMicroscopekey];
                if (isEqualMACAddress) {
                    [self.delegate microscopeTool:self setCamera:camera status:YES];
                    [self refreshMicrosopeStatusWithCamera:camera isDefault:YES isLost:NO];
                    break;
                    
                }
            }
            break;
        }

        // If the default camera is a real camera, create a placeholder camera object for it.
        CZCamera *camera = [[self class] newPlaceholderCamera:defaultCameraMACAddress
                                                   cameraName:nil
                                                          pin:nil];
        if (camera == nil) {
            break;
        }
        // For restart the app, the default camera is lost
        NSUInteger i = [self indexOfMicroscopeKeys:camera.macAddress];
        if (i == NSNotFound) {
            _microscopeListM[defaultCameraMACAddress] = camera;
        }

        // Only update camera, if defaultMicroscopekey is already there.
        if (self.defaultMicroscopekey) {
            [self.delegate microscopeTool:self setCamera:camera status:NO];
            [self refreshMicrosopeStatusWithCamera:camera isDefault:YES isLost:YES];
            [camera release];
            break;
        }
        
        [self checkAndCreateMicroscopeModelWithCamera:camera];
        self.defaultMicroscopekey = camera.macAddress;

        [self.delegate microscopeTool:self setCamera:camera status:NO];
        [self refreshMicrosopeStatusWithCamera:camera isDefault:YES isLost:YES];
        
        [camera release];
    } while (false);
    
    [self updateDefaultModelAppearance];
}

- (CZMicroscopeStatus *)refreshMicrosopeStatusWithCamera:(CZCamera *)camera
                                               isDefault:(BOOL)defaultMicroscope
                                                  isLost:(BOOL)lost {
    CZMicroscopeStatus *microscopeStatus = self.microscopeStatus[camera.macAddress];
    if (microscopeStatus == nil) {
        microscopeStatus = [[CZMicroscopeStatus alloc] init];
    } else {
        [microscopeStatus retain];
    }
    microscopeStatus.isDefault = defaultMicroscope;
    microscopeStatus.isLock = [camera hasPIN];
    microscopeStatus.isLost = lost;
    
    NSString *key = camera.macAddress;
    [self.microscopeStatus setObject:microscopeStatus forKey:key];
    [microscopeStatus release];
    
    return microscopeStatus;
}

- (CZMicroscopeModel *)checkAndCreateMicroscopeModelWithCamera:(CZCamera *)camera {
    CZMicroscopeModel *model = self.microscopeModels[camera.macAddress];
    if (model && ![model isMemberOfClass:[CZMicroscopeModel class]]) { // If model exist and type is defined.
        return model;
    }
    
    CZMicroscopeModel *modelFromMNA = nil;
    for (CZMicroscopeModel *model in self.modelsFromMNA.allValues) {
        NSUInteger i = [self indexOfMicroscopeKeys:model.cameraMACAddress];
        if (i != NSNotFound) {
            modelFromMNA = model;
            break;
        }
    }
    CZMicroscopeModel *modelFromCamera = [CZMicroscopeModel newMicroscopeModelWithCamera:camera];
    if (modelFromCamera == nil) {
        return model;
    }
    
    [modelFromCamera autorelease];
    if (modelFromMNA) {
        modelFromCamera = [modelFromCamera updateMNAInfoWith:modelFromMNA];
    }
    
    if (self.microscopeModels[camera.macAddress] == nil) {
        [self.microscopeModels setObject:modelFromCamera forKey:camera.macAddress];
    } else if (![modelFromCamera isMemberOfClass:[CZMicroscopeModel class]]) {
        [self.microscopeModels setObject:modelFromCamera forKey:camera.macAddress];
    }
    
    if (modelFromCamera.isDefault) {
        self.defaultMicroscopekey = camera.macAddress;
    }
    return modelFromCamera;
}

#pragma mark - CZCameraBrowser notification handlers

- (void)cameraBrowserDidFindCameras:(NSNotification *)notification {
    NSArray *cameras = notification.userInfo[kCZCameraBrowserNotificationCameraList];
    
    if (self.virtualMicroscopesHidden) {  // filter out local file(virtual) camera
        
        NSMutableArray<CZCamera *> *validCameras = [[NSMutableArray alloc] init];
        for (CZCamera *camera in cameras) {
            if (![camera isKindOfClass:[CZLocalFileCamera class]]) {
                [validCameras addObject:camera];
            }
        }
        
        cameras = [NSArray arrayWithArray:validCameras];
        
        [validCameras release];
    }
    
    if (cameras.count) {
        [self refreshCameraList:cameras];
        [self.delegate microscopeToolDidDismissProgressHUD:self];
    }
    
    if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
        [self.delegate microscopeToolWillCreateMicroscopeButton:self];
    }

#if DCM_REFACTORED
    if (![[CZDCMManager defaultManager] enableMultiMicroscopes]) {  // if it's single mode
        CZCamera *camera = [self cameraAtMicroscopeKey:self.defaultMicroscopekey];
        CZMicroscopeStatus *status = _microscopeStatus[self.defaultMicroscopekey];
        if (status.isLost && camera) {
           [self.delegate microscopeTool:self setCamera:camera status:NO];
        }
    }
#else
    if (![[CZDefaultSettings sharedInstance] enableMultiMicroscopes]) {  // if it's single mode
        CZCamera *camera = [self cameraAtMicroscopeKey:self.defaultMicroscopekey];
        CZMicroscopeStatus *status = _microscopeStatus[self.defaultMicroscopekey];
        if (status.isLost && camera) {
            [self.delegate microscopeTool:self setCamera:camera status:NO];
        }
    }
#endif
    
    if ([self.delegate respondsToSelector:@selector(microscopeTool:didFindCameras:)]) {
        [self.delegate microscopeTool:self didFindCameras:cameras];
    }
}

- (void)cameraBrowserDidGetUpdatedDataFromCameras:(NSNotification *)notification {
    NSArray *cameras = notification.userInfo[kCZCameraBrowserNotificationCameraList];
    [self.delegate microscopeToolDidDismissProgressHUD:self];
    
    for (CZCamera *camera in cameras) {
        NSUInteger i = [self indexOfMicroscopeKeys:camera.macAddress];
        if (i != NSNotFound) {
            
            CZMicroscopeModel *model = self.microscopeModels[camera.macAddress];
            [model updateDataFromCamera:camera];
            
            [self.delegate microscopeTool:self setCamera:camera status:YES];
            [self refreshMicrosopeStatusWithCamera:camera isDefault:NO isLost:NO];
        }
    }
}

- (void)cameraBrowserDidLoseCameras:(NSNotification *)notification {
    NSArray *cameras = notification.userInfo[kCZCameraBrowserNotificationCameraList];
    
    for (CZCamera *camera in cameras) {
        CZLogv(@"Camera is lost (MAC: %@)", [camera macAddress]);
    }
    
    [self disableLostCameras:cameras];
    
    if ([self.delegate respondsToSelector:@selector(microscopeTool:didLostCameras:)]) {
        [self.delegate microscopeTool:self didLostCameras:cameras];
    }
}

- (void)cameraBrowserDidRemoveCameras:(NSNotification *)notification {
    NSArray *cameras = notification.userInfo[kCZCameraBrowserNotificationCameraList];
    for (CZCamera *camera in cameras) {
        CZLogv(@"Camera is unavailable (MAC: %@, Name:%@)",[camera macAddress],[camera displayName]);
        CZCamera *existCamera = _microscopeListM[camera.macAddress];
        if(existCamera) {
            [_microscopeListM removeObjectForKey:existCamera.macAddress];
        }
    }
    
    [self disableLostCameras:cameras];
    
    // refresh UI
    if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
        [self.delegate microscopeToolWillCreateMicroscopeButton:self];
    }
}

- (void)cameraBrowserDidReplaceCamera:(NSNotification *)notification {
    CZCamera *oldCamera = notification.userInfo[kCZCameraBrowserNotificationOldCamera];
    CZCamera *newCamera = notification.userInfo[kCZCameraBrowserNotificationNewCamera];
    
    CZCamera *existCamera = self.microscopeListM[oldCamera.macAddress];
    if (existCamera) {
        [self.microscopeListM removeObjectForKey:existCamera.macAddress];
    }
    [self refreshCameraList:@[newCamera]];
    
    CZMicroscopeModel *oldMicroscopeModel = self.microscopeModels[oldCamera.macAddress];
    if (oldMicroscopeModel) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [oldMicroscopeModel writeToDictionary:dictionary];
        [oldMicroscopeModel saveSlotsToDictionary:dictionary];
        CZMicroscopeModel *newMicroscopeModel = [CZMicroscopeModel newMicroscopeModelWithCamera:newCamera];
        [newMicroscopeModel readFromDictionary:dictionary];
        [newMicroscopeModel loadSlotsFromDictionary:dictionary];
        newMicroscopeModel.name = newCamera.displayName;
        newMicroscopeModel.cameraMACAddress = newCamera.macAddress;
        [newMicroscopeModel saveToDefaults];
        [newMicroscopeModel saveSlotsToDefaults];
        [self.microscopeModels removeObjectForKey:oldCamera.macAddress];
        [self.microscopeModels setObject:newMicroscopeModel forKey:newCamera.macAddress];
    }
    
    // refresh UI
    if ([self.delegate respondsToSelector:@selector(microscopeToolWillCreateMicroscopeButton:)]) {
        [self.delegate microscopeToolWillCreateMicroscopeButton:self];
    }
}

- (BOOL)cameraBrowserShouldRefreshThumbnailForCamera:(CZCamera *)camera {
    return [self.delegate microscopeTool:self shouldRefreshThumbnailForCamera:camera];
}

+ (CZCamera *)newPlaceholderCamera:(NSString *)cameraMacAddress
                        cameraName:(NSString *)name
                               pin:(NSString *)pin {
    CZCamera *camera = [[CZCamera alloc] init];
    [camera setMacAddress:cameraMacAddress];
    
    // Read useful data from local cache and set to placeholder camera
    CZMicroscopeModel *tempModel = [CZMicroscopeModel newMicroscopeModelWithCamera:camera];
    if (tempModel) {
        if (tempModel.type == kMicroscopeTypeDefault) {
           [camera setDisplayName:name andUpload:NO];
        } else {
           [camera setDisplayName:tempModel.name andUpload:NO];
        }
        if (tempModel.type == kMicroscopeTypeDefault) {
            [camera setPIN:camera.pin andUpload:NO];
        } else {
            [camera setPIN:tempModel.pin andUpload:NO];
        }

        [tempModel release];
    } else {
        [camera release];
        camera = nil;
    }
    
    return camera;
}

#pragma mark - CZDCMCameraManagerDelegate

- (BOOL)dcmCameraShouldRefreshThumbnailForCamera:(CZCamera *)camera {
    return [self.delegate microscopeTool:self shouldRefreshThumbnailForCamera:camera];
}

@end
