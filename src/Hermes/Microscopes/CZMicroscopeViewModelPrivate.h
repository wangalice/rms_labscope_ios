//
//  CZMicroscopeViewModelPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 4/22/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeViewModel.h"

@interface CZMicroscopeViewModel ()

@property (nonatomic, copy) NSString *microscopeName;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, assign, getter=isLocked) BOOL locked;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign, getter=isConfigurable) BOOL configurable;
@property (nonatomic, assign) BOOL isDefaultMicroscope;
@property (nonatomic, assign) BOOL isCurrentMicroscope;

@end
