//
//  CZKappaCameraFirmwareUpdateViewController.h
//  Matscope
//
//  Created by Sherry Xu on 12/2/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CZKappaCameraFirmwareUpdateViewController;

@protocol CZKappaCameraFirmwareUpdateViewControllerDelegate <NSObject>

- (void)kappaFirmwareUpdateViewControllerDidPressedOk:(CZKappaCameraFirmwareUpdateViewController *)controller;

@end

@interface CZKappaCameraFirmwareUpdateViewController : UIViewController

@property (nonatomic, assign) id<CZKappaCameraFirmwareUpdateViewControllerDelegate> delegate;

@end
