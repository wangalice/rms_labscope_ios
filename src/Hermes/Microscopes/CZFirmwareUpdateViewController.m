//
//  CZFirmwareUpdateViewController.m
//  Matscope
//
//  Created by Halley Gu on 5/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZFirmwareUpdateViewController.h"
#import "CZFunctionBar.h"

#import <CZCameraInterface/CZCameraInterface.h>

static const NSTimeInterval kFirmwareUpdateTimeout = 300;

@interface CZFirmwareUpdateViewController ()

@property (nonatomic, retain, readonly) UILabel *briefLabel;
@property (nonatomic, retain, readonly) UILabel *warningLabel;
@property (nonatomic, retain, readonly) UILabel *progressLabel;
@property (nonatomic, retain, readonly) UIActivityIndicatorView *progressIndicator;
@property (nonatomic, retain, readonly) CZZenStyleButton *updateButton;
@property (nonatomic, retain, readonly) CZZenStyleButton *notNowButton;
@property (nonatomic, retain, readonly) CZZenStyleButton *doneButton;
@property (nonatomic, retain) NSString *originalSSID;
@property (nonatomic, assign) BOOL wasCameraInAPMode;
@property (nonatomic, retain) NSTimer *timeoutTimer;

@end

@implementation CZFirmwareUpdateViewController

@synthesize briefLabel = _briefLabel;
@synthesize warningLabel = _warningLabel;
@synthesize progressLabel = _progressLabel;
@synthesize progressIndicator = _progressIndicator;
@synthesize updateButton = _updateButton;
@synthesize notNowButton = _notNowButton;
@synthesize doneButton = _doneButton;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_camera release];
    [_briefLabel release];
    [_warningLabel release];
    [_progressLabel release];
    [_progressIndicator release];
    [_updateButton release];
    [_notNowButton release];
    [_doneButton release];
    [_originalSSID release];
    
    [_timeoutTimer invalidate];
    [_timeoutTimer release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    self.title = L(@"MIC_CAMERA_FW_UPDATE_TITLE");
    self.view.backgroundColor = kDefaultBackGroundColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_briefLabel == nil) {
        [self.view addSubview:self.briefLabel];
    }
    if (_warningLabel == nil) {
        [self.view addSubview:self.warningLabel];
    }
    if (_progressLabel == nil) {
        [self.view addSubview:self.progressLabel];
    }
    if (_progressIndicator == nil) {
        [self.view addSubview:self.progressIndicator];
    }
    if (_updateButton == nil) {
        [self.view addSubview:self.updateButton];
    }
    if (_notNowButton == nil) {
        [self.view addSubview:self.notNowButton];
    }
    if (_doneButton == nil) {
        [self.view addSubview:self.doneButton];
    }
    
    self.progressIndicator.hidden = YES;
    self.progressLabel.hidden = YES;
    self.doneButton.hidden = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if ([self.delegate respondsToSelector:@selector(firmwareUpdateViewControllerDidFinish:)]) {
        [self.delegate firmwareUpdateViewControllerDidFinish:self];
    }
}

#pragma mark - Lazy-load objects

- (UILabel *)briefLabel {
    if (_briefLabel == nil) {
        _briefLabel = [[UILabel alloc] init];
        _briefLabel.backgroundColor = [UIColor clearColor];
        _briefLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        _briefLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _briefLabel.numberOfLines = 0;
        _briefLabel.text = L(@"MIC_CAMERA_FW_UPDATE_MESSAGE_1");
        _briefLabel.textColor = kDefaultLabelColor;
        
        CGRect frame = [_briefLabel textRectForBounds:CGRectMake(30, 30, self.view.frame.size.width - 60, CGFLOAT_MAX)
                               limitedToNumberOfLines:0];
        _briefLabel.frame = frame;
    }
    return _briefLabel;
}

- (UILabel *)warningLabel {
    if (_warningLabel == nil) {
        _warningLabel = [[UILabel alloc] init];
        _warningLabel.backgroundColor = [UIColor clearColor];
        _warningLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        _warningLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _warningLabel.numberOfLines = 0;
        NSString *msg = [NSString stringWithFormat:L(@"MIC_CAMERA_FW_UPDATE_MESSAGE_2"), [[UIApplication sharedApplication] cz_name]];
        _warningLabel.text = msg;
        _warningLabel.textColor = kDefaultLabelColor;
        
        CGRect frame = [_warningLabel textRectForBounds:CGRectMake(30, CGRectGetMaxY(self.briefLabel.frame) + 20,
                                                                   self.view.frame.size.width - 60, CGFLOAT_MAX)
                                 limitedToNumberOfLines:0];
        _warningLabel.frame = frame;
    }
    return _warningLabel;
}

- (UILabel *)progressLabel {
    if (_progressLabel == nil) {
        _progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, self.view.frame.size.height - 155,
                                                                   self.view.frame.size.width - 120, 70)];
        _progressLabel.backgroundColor = [UIColor clearColor];
        _progressLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        _progressLabel.numberOfLines = 3;
        _progressLabel.textColor = kDefaultLabelColor;
    }
    return _progressLabel;
}

- (UIActivityIndicatorView *)progressIndicator {
    if (_progressIndicator == nil) {
        UIActivityIndicatorViewStyle style = UIActivityIndicatorViewStyleWhite;
        _progressIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
        _progressIndicator.center = CGPointMake(40, self.view.frame.size.height - 120);
    }
    return _progressIndicator;
}

- (CZZenStyleButton *)updateButton {
    if (_updateButton == nil) {
        _updateButton = [[CZZenStyleButton alloc] initWithLabel:L(@"MIC_CAMERA_FW_UPDATE_START")];
        _updateButton.frame = CGRectMake(30, self.view.frame.size.height - 60,
                                         self.view.frame.size.width / 2 - 40, 40);
        _updateButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [_updateButton addTarget:self action:@selector(updateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _updateButton;
}

- (CZZenStyleButton *)notNowButton {
    if (_notNowButton == nil) {
        _notNowButton = [[CZZenStyleButton alloc] initWithLabel:L(@"MIC_CAMERA_FW_UPDATE_CANCEL")];
        _notNowButton.frame = CGRectMake(self.view.frame.size.width / 2 + 10, self.view.frame.size.height - 60,
                                         self.view.frame.size.width / 2 - 40, 40);
        [_notNowButton addTarget:self action:@selector(notNowButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _notNowButton;
}

- (CZZenStyleButton *)doneButton {
    if (_doneButton == nil) {
        _doneButton = [[CZZenStyleButton alloc] initWithLabel:L(@"DONE")];
        _doneButton.frame = CGRectMake(30, self.view.frame.size.height - 60,
                                       self.view.frame.size.width - 60, 40);
        [_doneButton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

#pragma mark - Event handlers

- (void)updateButtonAction:(id)sender {
    self.updateButton.enabled = NO;
    self.updateButton.alpha = 0.5;
    
    self.notNowButton.enabled = NO;
    self.notNowButton.alpha = 0.5;
    
    self.progressIndicator.hidden = NO;
    [self.progressIndicator startAnimating];
    self.progressLabel.hidden = NO;
    
    [self.timeoutTimer invalidate];
    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kFirmwareUpdateTimeout
                                                         target:self
                                                       selector:@selector(timeoutHandler:)
                                                       userInfo:nil
                                                        repeats:NO];
    
    if ([self.camera isKindOfClass:[CZWisionCamera class]]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cameraLostAfterFirstRestart:)
                                                     name:kCZCameraBrowserNotificationLost
                                                   object:nil];
        
        [self.camera restart];
        
        CZLogv(@"FW update: restart command sent.");
        
        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_STATUS_1");
    } else if ([self.camera isKindOfClass:[CZMoticCamera class]]) {
        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_STATUS_2");
        
        self.wasCameraInAPMode = NO;
        NSString *ssid = [CZWifiNotifier copySSID];
        self.originalSSID = [ssid autorelease];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(wifiChanged:)
                                                     name:kCZWifiChangedNotification
                                                   object:nil];
        
        CZLogv(@"FW update: start updating firmware.");
        [self.camera updateFirmware];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cameraLostAfterSecondRestart:)
                                                     name:kCZCameraBrowserNotificationLost
                                                   object:nil];
        
        CZLogv(@"FW update: end updating firmware, restarting.");
        
        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_STATUS_3");
    }
}

- (void)notNowButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)doneButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)wifiChanged:(NSNotification *)notification {
    if ([self.camera isKindOfClass:[CZMoticCamera class]]) {
        NSString *currentSSID = [CZWifiNotifier copySSID];
        
        if (![currentSSID isEqualToString:self.originalSSID]) {
            self.wasCameraInAPMode = YES;
        }
        
        [currentSSID release];
    }
}

- (void)cameraLostAfterFirstRestart:(NSNotification *)notification {
    if ([self isCorrectNotificationToHandle:notification]) {
        CZLogv(@"FW update: restarting before update.");
        
        [self.timeoutTimer invalidate];
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kFirmwareUpdateTimeout
                                                             target:self
                                                           selector:@selector(timeoutHandler:)
                                                           userInfo:nil
                                                            repeats:NO];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kCZCameraBrowserNotificationLost object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cameraFoundAfterFirstRestart:)
                                                     name:kCZCameraBrowserNotificationFound
                                                   object:nil];
    }
}

- (void)cameraFoundAfterFirstRestart:(NSNotification *)notification {
    if ([self isCorrectNotificationToHandle:notification]) {
        CZLogv(@"FW update: restarted before update.");
        
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kCZCameraBrowserNotificationFound object:nil];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            NSString *firmwareVersion = nil;
            // Wait until firmwareVersion is got, so we can be sure that camera is receiving requests.
            NSTimeInterval waitedSeconds = 0;
            while (firmwareVersion.length == 0 && waitedSeconds < kFirmwareUpdateTimeout) {
                sleep(2);
                waitedSeconds += 2;
                firmwareVersion = [self.camera firmwareVersion];
            }
            
            if (waitedSeconds >= kFirmwareUpdateTimeout) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self timeoutHandler:nil];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_STATUS_2");
                });
                
                CZLogv(@"FW update: start updating firmware.");
                [self.camera updateFirmware];
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self.timeoutTimer invalidate];
                    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kFirmwareUpdateTimeout
                                                                         target:self
                                                                       selector:@selector(timeoutHandler:)
                                                                       userInfo:nil
                                                                        repeats:NO];
                });
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(cameraLostAfterSecondRestart:)
                                                             name:kCZCameraBrowserNotificationLost
                                                           object:nil];
                
                CZLogv(@"FW update: end updating firmware, restart command sent.");
                [self.camera restart];
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_STATUS_3");
                });
            }
        });
    }
}

- (void)cameraLostAfterSecondRestart:(NSNotification *)notification {
    if ([self isCorrectNotificationToHandle:notification]) {
        CZLogv(@"FW update: restarting after update.");
        
        [self.timeoutTimer invalidate];
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kFirmwareUpdateTimeout
                                                             target:self
                                                           selector:@selector(timeoutHandler:)
                                                           userInfo:nil
                                                            repeats:NO];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kCZCameraBrowserNotificationLost object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cameraFoundAfterSecondRestart:)
                                                     name:kCZCameraBrowserNotificationFound
                                                   object:nil];
    }
}

- (void)cameraFoundAfterSecondRestart:(NSNotification *)notification {
    if ([self isCorrectNotificationToHandle:notification]) {
        CZLogv(@"FW update: restarted after update.");
        
        [self.timeoutTimer invalidate];
        self.timeoutTimer = nil;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kCZCameraBrowserNotificationFound object:nil];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            NSString *firmwareVersion = nil;
            // Wait until firmwareVersion is got, so we can be sure that camera is receiving requests.
            NSTimeInterval waitedSeconds = 0;
            while (firmwareVersion.length == 0 && waitedSeconds < kFirmwareUpdateTimeout) {
                sleep(2);
                waitedSeconds += 2;
                firmwareVersion = [self.camera firmwareVersion];
            }
            
            if (waitedSeconds >= kFirmwareUpdateTimeout) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self timeoutHandler:nil];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self.progressIndicator stopAnimating];
                    self.progressIndicator.hidden = YES;
                    
                    NSString *msg = [NSString stringWithFormat:L(@"MIC_CAMERA_FW_UPDATE_MESSAGE_2"), [[UIApplication sharedApplication] cz_name]];
                    self.warningLabel.text = msg;
                    
                    if ([self.camera isFirmwareUpdated]) {
                        CZLogv(@"FW update: updated successfully.");
                        
                        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_RESULT_1");
                        
                        self.updateButton.hidden = YES;
                        self.notNowButton.hidden = YES;
                        self.doneButton.hidden = NO;
                    } else {
                        CZLogv(@"FW update: failed to update.");
                        
                        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_RESULT_2");
                        
                        [self.updateButton setTitle:L(@"RETRY") forState:UIControlStateNormal];
                        self.updateButton.enabled = YES;
                        self.updateButton.alpha = 1.0;
                        
                        [self.notNowButton setTitle:L(@"CANCEL") forState:UIControlStateNormal];
                        self.notNowButton.enabled = YES;
                        self.notNowButton.alpha = 1.0;
                    }
                });
            }
        });
    }
}

- (void)timeoutHandler:(NSTimer *)timer {
    if ([self.camera isKindOfClass:[CZMoticCamera class]] && self.wasCameraInAPMode) {
        CZLogv(@"FW update: timeout after Wi-Fi changed.");
        
        self.warningLabel.text = [NSString stringWithFormat:L(@"MIC_CAMERA_FW_UPDATE_MESSAGE_3"), self.originalSSID];
        
        [self.notNowButton setTitle:L(@"CANCEL") forState:UIControlStateNormal];
        self.notNowButton.enabled = YES;
        self.notNowButton.alpha = 1.0;
        
        self.wasCameraInAPMode = NO;
    } else {
        CZLogv(@"FW update: timeout.");
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        NSString *msg = [NSString stringWithFormat:L(@"MIC_CAMERA_FW_UPDATE_MESSAGE_2"), [[UIApplication sharedApplication] cz_name]];
        self.warningLabel.text = msg;
        
        self.progressLabel.text = L(@"MIC_CAMERA_FW_UPDATE_RESULT_3");
        [self.progressIndicator stopAnimating];
        self.progressIndicator.hidden = YES;
        
        [self.updateButton setTitle:L(@"RETRY") forState:UIControlStateNormal];
        self.updateButton.enabled = YES;
        self.updateButton.alpha = 1.0;
        
        [self.notNowButton setTitle:L(@"CANCEL") forState:UIControlStateNormal];
        self.notNowButton.enabled = YES;
        self.notNowButton.alpha = 1.0;
        
        self.camera = nil;
    }
}

#pragma mark - Private methods

- (BOOL)isCorrectNotificationToHandle:(NSNotification *)notification {
    NSArray *cameras = notification.userInfo[kCZCameraBrowserNotificationCameraList];
    BOOL found = NO;
    for (CZCamera *camera in cameras) {
        BOOL isEqualMACAddress = [camera.macAddress isEqualComparisionWithCaseInsensitive:self.camera.macAddress];
        if (isEqualMACAddress) {
            found = YES;
            self.camera = camera; // Update camera instance
            break;
        }
    }
    return found;
}

@end
