//
//  CZMicroscopeViewModel.h
//  Hermes
//
//  Created by Li, Junlin on 1/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZCamera;
@class CZMicroscopeViewModel;

@protocol CZMicroscopeViewModelObserver <NSObject>

@optional
- (void)microscopeViewModelMicroscopeNameDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelThumbnailDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelLockedDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelEnabledDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelConfigurableDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelIsDefaultMicroscopeDidChange:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeViewModelIsCurrentMicroscopeDidChange:(CZMicroscopeViewModel *)microscopeViewModel;

@end

@interface CZMicroscopeViewModel : NSObject

@property (nonatomic, readonly, strong) CZCamera *camera;
@property (nonatomic, readonly, copy) NSString *microscopeName;
@property (nonatomic, readonly, strong) UIImage *thumbnail;
@property (nonatomic, readonly, assign, getter=isLocked) BOOL locked;
@property (nonatomic, readonly, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, readonly, assign, getter=isConfigurable) BOOL configurable;
@property (nonatomic, readonly, assign) BOOL isDefaultMicroscope;
@property (nonatomic, readonly, assign) BOOL isCurrentMicroscope;
@property (nonatomic, readonly, assign) BOOL isVirtualMicroscope;
@property (nonatomic, readonly, assign) BOOL isPlusMicroscope;

- (instancetype)initWithCamera:(CZCamera *)camera;

- (void)addObserver:(id<CZMicroscopeViewModelObserver>)observer;
- (void)removeObserver:(id<CZMicroscopeViewModelObserver>)observer;

@end
