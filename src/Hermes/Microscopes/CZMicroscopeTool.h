//
//  CZMicroscopeTool.h
//  Matscope iPhone
//
//  Created by Sherry Xu on 5/11/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZMicroscopeModel;
@class CZCamera;
@class CZMicroscopeTool;
@class CZMicroscopeStatus;

@protocol CZMicroscopeToolDelegate <NSObject>

@required

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool
             setCamera:(CZCamera *)camera
                status:(BOOL)enabled;
- (void)microscopeToolDidShowProgressHUD:(CZMicroscopeTool *)microscopeTool;
- (void)microscopeToolDidDismissProgressHUD:(CZMicroscopeTool *)microscopeTool;
- (BOOL)microscopeTool:(CZMicroscopeTool *)microscopeTool shouldRefreshThumbnailForCamera:(CZCamera *)camera;

@optional
- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool
    didConnectToCamera:(NSString *)cameraMacAddress
           forceSwitch:(BOOL)forceSwitch;
- (void)microscopeToolWillCreateMicroscopeButton:(CZMicroscopeTool *)microscopeTool;

// Fix bug 222511. We receive kCZCameraBrowserNotificationFound and kCZCameraBrowserNotificationLost notification both
// at class CZMicroscopeTool and CZMicroscopeViewController. iOS has no guarantee about notification receive order.
// However notification handler in CZMicroscopeTool should be called before that in CZMicroscopeViewController.
// Otherwise Labscope/Matscope could not reconnect to current camera if we stay at live tab. So we choose to receive
// notification at CZMicroscopeTool and use delegate to notify CZMicroscopeViewController.
- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool
         didFindCameras:(NSArray<CZCamera *> *)newCameras;

- (void)microscopeTool:(CZMicroscopeTool *)microscopeTool
        didLostCameras:(NSArray<CZCamera *> *)lostCameras;
@end

@interface CZMicroscopeTool : NSObject

@property (nonatomic, copy) NSString *defaultMicroscopekey;
@property (nonatomic, copy) NSString *currentMicroscopekey;

// TODO: hide details
@property (nonatomic, retain, readonly) NSMutableDictionary<NSString *, CZMicroscopeModel *> *microscopeModels;
@property (nonatomic, retain) NSMutableDictionary<NSString *, CZMicroscopeStatus *> *microscopeStatus;

@property (nonatomic, retain, readonly) NSDictionary<NSString *, CZCamera *> *microscopeList;
@property (nonatomic, retain, readonly) NSArray<NSString *> *microscopeKeys;
@property (nonatomic, retain, readonly) NSArray<NSString *> *sortedMicroscopeKeys;
@property (nonatomic, retain, readonly) NSArray<CZCamera *> *cameras;

@property (nonatomic, assign) id <CZMicroscopeToolDelegate> delegate;
@property (nonatomic, assign) CZVirtualMicroscopesOption virtualMicroscopesOption;
@property (nonatomic, assign) BOOL virtualMicroscopesHidden;
@property (nonatomic, assign) BOOL plusMicroscopeEnabled;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithDelegate:(id <CZMicroscopeToolDelegate>)delegate;

/** only clear camera list (microscopeList), let browser find again.*/
- (void)enableCameraBrowsing;
- (void)disableCameraBrowsingRefreshThumbnail;
- (BOOL)defaultMicroscopeSelected;
- (BOOL)currentMicroscopeSelected;
- (BOOL)assignCurrentCamera;
- (BOOL)assignDefaultCamera;
- (BOOL)assignCamera:(id)key;
- (BOOL)findActiveMicroscopeAndSelect;
- (BOOL)findActiveMicroscopeAndAssign;
- (void)toggleDefaultModel:(CZMicroscopeModel *)selectedModel;
- (void)updateDefaultModelAppearance;
- (CZMicroscopeModel *)currentMicroscopeModel;
- (CZMicroscopeModel *)microscopeModelAtMicroscopeKey:(NSString *)key;
- (CZCamera *)cameraAtMicroscopeKey:(NSString *)key;

- (void)removeCameraForKey:(NSString *)key;
- (void)removeMicroscopeForKey:(NSString *)key;

- (void)microscopeSelected:(NSString *)macAddress forceSelect:(BOOL)forceSelect;
- (NSUInteger)indexOfMicroscopeKeys:(NSString *)macAddress;

/** load exsiting manually add cameras into the microscope list.*/
- (void)loadManuallyAddCamerasList;
- (BOOL)canCreatePlaceholderForManuallyAddCamera:(CZCamera *)camera;
- (void)createPlaceholderForManuallyAddCamera:(CZCamera *)camera;

+ (CZCamera *)newPlaceholderCamera:(NSString *)cameraMacAddress
                        cameraName:(NSString *)name
                               pin:(NSString *)pin;

@end

@interface CZMicroscopeStatus : NSObject

@property (nonatomic, assign) BOOL isDefault;
@property (nonatomic, assign) BOOL isLock;
@property (nonatomic, assign) BOOL isLost;

@end

