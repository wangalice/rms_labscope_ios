//
//  CZMicroscopesViewController.h
//  Labscope
//
//  Created by Li, Junlin on 12/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZMicroscopesTask.h"
#import "CZSystemToolbar.h"

@interface CZMicroscopesViewController : UIViewController <CZTaskDelegate, CZMicroscopesTaskDelegate, CZMainToolbarDataSource, CZVirtualToolbarDataSource, CZToolbarDelegate, CZToolbarResponder>

@property (nonatomic, readonly, strong) CZMicroscopesTask *task;

@end
