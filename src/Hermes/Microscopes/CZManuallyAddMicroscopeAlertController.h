//
//  CZManuallyAddMicroscopeAlertController.h
//  Hermes
//
//  Created by Li, Junlin on 7/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZInputAlertController.h"

@class CZCamera;
@class CZManuallyAddMicroscopeAlertController;

@protocol CZManuallyAddMicroscopeAlertControllerDelegate <NSObject>

@optional
- (BOOL)manuallyAddMicroscopeAlertController:(CZManuallyAddMicroscopeAlertController *)manuallyAddMicroscopeAlertController canManuallyAddMicroscopeForCamera:(CZCamera *)camera;
- (void)manuallyAddMicroscopeAlertController:(CZManuallyAddMicroscopeAlertController *)manuallyAddMicroscopeAlertController didManuallyAddMicroscopeForCamera:(CZCamera *)camera;

@end

@interface CZManuallyAddMicroscopeAlertController : CZInputAlertController

@property (nonatomic, weak) id<CZManuallyAddMicroscopeAlertControllerDelegate> delegate;

@end
