//
//  CZMicroscopeViewModel.m
//  Hermes
//
//  Created by Li, Junlin on 1/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeViewModel.h"
#import "CZMicroscopeViewModelPrivate.h"
#import <CZCameraInterface/CZCameraInterface.h>

@interface CZMicroscopeViewModel ()

@property (nonatomic, strong) NSPointerArray *observers;

@end

@implementation CZMicroscopeViewModel

- (instancetype)initWithCamera:(CZCamera *)camera {
    self = [super init];
    if (self) {
        _camera = camera;
        
#if MIC_REFACTORED
        if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
            _hidden = YES;
        } else {
            _hidden = NO;
        }
#endif
        
        NSString *microscopeName = camera.displayName;
        if ([camera.macAddress hasPrefix:CZPlusCameraMACAddressPrefix]) {
            microscopeName = [microscopeName stringByAppendingFormat:@"%@%@%@", @"(", camera.ipAddress, @")"];
        }
        _microscopeName = [microscopeName copy];
        
        _locked = [camera hasPIN];
        
        _enabled = YES;
        
        if ([camera isKindOfClass:[CZRemoteStreamCamera class]] || [camera isKindOfClass:[CZPlusCamera class]]) {
            _configurable = NO;
        } else {
            _configurable = YES;
        }
        
        if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
            _isVirtualMicroscope = YES;
        } else {
            _isVirtualMicroscope = NO;
        }
        
        if ([camera.macAddress hasPrefix:@"22:00:00:00:00"]) {
            _isPlusMicroscope = YES;
        } else {
            _isPlusMicroscope = NO;
        }
        
        _observers = [NSPointerArray weakObjectsPointerArray];
    }
    return self;
}

- (void)setMicroscopeName:(NSString *)microscopeName {
    if ([_microscopeName isEqualToString:microscopeName]) {
        return;
    }
    
    _microscopeName = [microscopeName copy];
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelMicroscopeNameDidChange:)]) {
            [observer microscopeViewModelMicroscopeNameDidChange:self];
        }
    }
}

- (void)setThumbnail:(UIImage *)thumbnail {
    if (_thumbnail == thumbnail) {
        return;
    }
    
    _thumbnail = thumbnail;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelThumbnailDidChange:)]) {
            [observer microscopeViewModelThumbnailDidChange:self];
        }
    }
}

- (void)setLocked:(BOOL)locked {
    if (_locked == locked) {
        return;
    }
    
    _locked = locked;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelLockedDidChange:)]) {
            [observer microscopeViewModelLockedDidChange:self];
        }
    }
}

- (void)setEnabled:(BOOL)enabled {
    if (_enabled == enabled) {
        return;
    }
    
    _enabled = enabled;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelEnabledDidChange:)]) {
            [observer microscopeViewModelEnabledDidChange:self];
        }
    }
}

- (void)setConfigurable:(BOOL)configurable {
    if (_configurable == configurable) {
        return;
    }
    
    _configurable = configurable;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelConfigurableDidChange:)]) {
            [observer microscopeViewModelConfigurableDidChange:self];
        }
    }
}

- (void)setIsDefaultMicroscope:(BOOL)isDefaultMicroscope {
    if (_isDefaultMicroscope == isDefaultMicroscope) {
        return;
    }
    
    _isDefaultMicroscope = isDefaultMicroscope;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelIsDefaultMicroscopeDidChange:)]) {
            [observer microscopeViewModelIsDefaultMicroscopeDidChange:self];
        }
    }
}

- (void)setIsCurrentMicroscope:(BOOL)isCurrentMicroscope {
    if (_isCurrentMicroscope == isCurrentMicroscope) {
        return;
    }
    
    _isCurrentMicroscope = isCurrentMicroscope;
    
    for (id<CZMicroscopeViewModelObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(microscopeViewModelIsCurrentMicroscopeDidChange:)]) {
            [observer microscopeViewModelIsCurrentMicroscopeDidChange:self];
        }
    }
}

- (void)addObserver:(id<CZMicroscopeViewModelObserver>)observer {
    [self.observers addPointer:(void *)observer];
}

- (void)removeObserver:(id<CZMicroscopeViewModelObserver>)observer {
    [self.observers compact];
    for (NSInteger index = 0; index < self.observers.count; index++) {
        if ([self.observers pointerAtIndex:index] == (__bridge void *)observer) {
            [self.observers removePointerAtIndex:index];
            break;
        }
    }
}

@end
