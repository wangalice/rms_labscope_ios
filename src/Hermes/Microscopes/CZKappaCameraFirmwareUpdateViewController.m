//
//  CZKappaCameraFirmwareUpdateViewController.m
//  Matscope
//
//  Created by Sherry Xu on 12/2/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZKappaCameraFirmwareUpdateViewController.h"
#import "CZFunctionBar.h"

static const CGFloat kDefaultViewSize = 540;

@interface CZKappaCameraFirmwareUpdateViewController () <UIWebViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, retain) CZZenStyleButton *okButton;
@property (nonatomic, retain) UIGestureRecognizer *tapGesture;

@end

@implementation CZKappaCameraFirmwareUpdateViewController

- (void)dealloc {
    [_okButton release];
    
     _tapGesture.delegate = nil;
    [_tapGesture release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    CGRect rect = CGRectMake(0, 0, kDefaultViewSize, kDefaultViewSize);
   
    self.view.frame = rect;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.title = L(@"MIC_CAMERA_FW_UPDATE_TITLE");
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:rect];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webView.opaque = NO;
    webView.backgroundColor = kDefaultBackGroundColor;
    webView.delegate = self;
    webView.scrollView.scrollEnabled = NO;
    webView.scrollView.bounces = NO;
    [self.view addSubview:webView];
    
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:@"<html><head><style type=\"text/css\">body{font-family:HelveticaNeue; font-size:18px; color: rgb(203,206,212); margin-left:20px; margin-right:20px; margin-top:25px;} </style></head><body>"];
    [htmlString appendFormat:@"<tr><p>%@&nbsp<a href=\"https://bit.ly/zeiss1398\">https://bit.ly/zeiss1398</a></p></tr>", L(@"MIC_CAMERA_FW_UPDATE_MESSAGE")];
    [htmlString appendString:@"</body></html>"];

    [webView loadHTMLString:htmlString baseURL:nil];
    [htmlString release];
    [webView release];
    
    CZZenStyleButton *button = [[CZZenStyleButton alloc] initWithLabel:L(@"OK")];
    [button addTarget:self action:@selector(okButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    self.okButton = button;
    [button release];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.okButton.frame = CGRectMake(self.view.frame.size.width / 4 + 20, self.view.frame.size.height - 60, self.view.frame.size.width / 2 - 40, 40);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBehind:)];
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO;
    recognizer.delegate = self;
    [self.view.window addGestureRecognizer:recognizer];
    self.tapGesture = recognizer;
    [recognizer release];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view.window removeGestureRecognizer:self.tapGesture];

    [super viewWillDisappear:animated];
}

- (void)tapBehind:(UITapGestureRecognizer *)tapRecognizer {
    CGPoint location = [tapRecognizer locationInView:self.view];
    if (CGRectContainsPoint(self.view.frame, location)) {
        return;
    }
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)okButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
    if ([self.delegate respondsToSelector:@selector(kappaFirmwareUpdateViewControllerDidPressedOk:)]) {
        [self.delegate kappaFirmwareUpdateViewControllerDidPressedOk:self];
    }
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        NSURL* url = [inRequest URL];
        NSString *scheme = [url scheme];
        if ([scheme isEqualToString:@"http"] || [scheme isEqualToString:@"https"] || [scheme isEqualToString:@"mailto"]) {
            [[UIApplication sharedApplication] openURL:url];
        } else if ([scheme isEqualToString:@"applewebdata"]) {
            NSString *urlString = [url absoluteString];
            NSRange range = [urlString rangeOfString:@"http"];
            if (range.location != NSNotFound) {
                urlString = [urlString substringFromIndex:range.location];
                url = [NSURL URLWithString:urlString];
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        return NO;
    }
    
    return YES;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}

@end
