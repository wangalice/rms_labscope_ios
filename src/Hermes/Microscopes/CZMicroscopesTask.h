//
//  CZMicroscopesTask.h
//  Labscope
//
//  Created by Li, Junlin on 12/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"
#import "CZMicroscopeViewModel.h"

@class CZMicroscopesTask;
@class CZMicroscopeModel;

@protocol CZMicroscopesTaskDelegate <CZTaskDelegate>

@optional
- (void)microscopesTaskWLANNameDidChange:(CZMicroscopesTask *)task;
- (void)microscopesTaskLoginInfoDidChange:(CZMicroscopesTask *)task;
- (void)microscopesTaskLoadingMicroscopesDidChange:(CZMicroscopesTask *)task;
- (void)microscopesTaskSortedMicroscopeViewModelsDidChange:(CZMicroscopesTask *)task;
- (void)microscopesTaskNoMicroscopesTextDidChange:(CZMicroscopesTask *)task;

@end

@interface CZMicroscopesTask : CZTask

@property (nonatomic, readonly, weak) id<CZMicroscopesTaskDelegate> delegate;

- (void)startMicroscopeBrowsing;
- (void)stopMicroscopeBrowsing;
- (void)microscopeBecomeVisible:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)microscopeBecomeInvisible:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)selectMicroscope:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)removeMicroscope:(CZMicroscopeViewModel *)microscopeViewModel;
- (void)refreshCameraAppearance;
- (void)updateVirtualCameraAppearance;
- (void)updatePlusCameraAppearance;

- (void)prepareForConfiguringMicroscope:(CZMicroscopeViewModel *)microscopeViewModel withCompletionHandler:(void (^)(CZCamera *camera, CZMicroscopeModel *microscopeModel, BOOL isLost))completionHandler;
- (void)applyConfigurationForCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel;
- (BOOL)validatePIN:(NSString *)pin forMicroscope:(CZMicroscopeViewModel *)microscopeViewModel remembersPIN:(BOOL)remembersPIN;
- (BOOL)canCreatePlaceholderForManuallyAddCamera:(CZCamera *)camera;
- (void)createPlaceholderForManuallyAddCamera:(CZCamera *)camera;

@property (nonatomic, readonly, copy) NSString *wlanName;
@property (nonatomic, readonly, copy) NSString *loginInfo;
@property (nonatomic, readonly, assign) BOOL isLoadingMicroscopes;
@property (nonatomic, readonly, copy) NSArray<CZMicroscopeViewModel *> *sortedMicroscopeViewModels;
@property (nonatomic, readonly, copy) NSString *noMicroscopesText;

@end
