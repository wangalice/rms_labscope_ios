//
//  CZMicroscopesViewController.m
//  Labscope
//
//  Created by Li, Junlin on 12/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopesViewController.h"
#import "CZMultitaskViewController.h"
#import "CZMicroscopeCollectionViewLayout.h"
#import "CZMicroscopeCollectionViewCell.h"
#import "CZManuallyAddMicroscopeAlertController.h"
#import "CZFirmwareUpdateViewController.h"
#import "CZKappaCameraFirmwareUpdateViewController.h"
#import "CZMicroscopeConfigurationViewController.h"
#import "CZAlertController.h"
#import "CZInputAlertController.h"
#import "UIViewController+HUD.h"
#import "MBProgressHUD+Hide.h"

#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>

#define kNumOfPINInputTimesAllowed 3

NSString * const CZMicroscopeCollectionViewCellScaleKey = @"CZMicroscopeCollectionViewCellScaleKey";

@interface CZMicroscopesViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CZMicroscopeCollectionViewCellDelegate, CZManuallyAddMicroscopeAlertControllerDelegate, CZFirmwareUpdateViewControllerDelegate, CZKappaCameraFirmwareUpdateViewControllerDelegate, CZMicroscopeConfigurationViewControllerDelegate>

@property (nonatomic, strong) CZMicroscopeCollectionViewLayout *microscopeCollectionViewLayout;
@property (nonatomic, strong) UICollectionView *microscopeCollectionView;
@property (nonatomic, strong) UILabel *noMicroscopesLabel;

@property (nonatomic, copy) NSArray<CZMicroscopeViewModel *> *sortedMicroscopeViewModels;

@property (nonatomic, strong) CZCamera *configuredCamera;
@property (nonatomic, strong) CZMicroscopeModel *configuredMicroscopeModel;
@property (nonatomic, strong) CZMicroscopeViewModel *configuredMicroscopeViewModel;
@property (nonatomic, assign) BOOL isReadOnlyConfigurationMode;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, assign) int numOfPINInputTimes;
@property (nonatomic, strong) CZInputAlertController *inputPINAlertView;
@property (nonatomic, strong) NSTimer *inputPINAlertTimer;

@end

@implementation CZMicroscopesViewController

- (void)dealloc {
    [_inputPINAlertTimer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    
    self.microscopeCollectionViewLayout = [[CZMicroscopeCollectionViewLayout alloc] init];
    
    self.microscopeCollectionView = ({
        CGRect frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(20.0 + CZTopToolbarHeight, 0.0, CZMainToolbarHeight + CZVirtualToolbarHeight, 0.0));
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:self.microscopeCollectionViewLayout];
        collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        collectionView.backgroundColor = [UIColor clearColor];
        collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        collectionView.dataSource = self;
        collectionView.delegate = self;
        [collectionView registerClass:[CZMicroscopeCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([CZMicroscopeCollectionViewCell class])];
        
        if (@available(iOS 11.0, *)) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
        [self.view addSubview:collectionView];
        collectionView;
    });
    
    self.noMicroscopesLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.text = self.task.noMicroscopesText;
        label.font = [UIFont cz_h4];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor cz_gs50];
        label.numberOfLines = 1;
        label.hidden = YES;
        
        [self.view addSubview:label];
        
        [label.widthAnchor constraintEqualToAnchor:self.view.widthAnchor constant:-20.0].active = YES;
        [label.centerXAnchor constraintEqualToAnchor:self.microscopeCollectionView.centerXAnchor].active = YES;
        [label.centerYAnchor constraintEqualToAnchor:self.microscopeCollectionView.centerYAnchor].active = YES;
        
        label;
    });
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{CZMicroscopeCollectionViewCellScaleKey : @(self.microscopeCollectionViewLayout.defaultScale)}];
    
    float scale = [[NSUserDefaults standardUserDefaults] floatForKey:CZMicroscopeCollectionViewCellScaleKey];
    self.microscopeCollectionViewLayout.scale = scale;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.task updateVirtualCameraAppearance];
    [self.task updatePlusCameraAppearance];
    [[CZCameraBrowser sharedInstance] forceRefresh];
}

#pragma mark - Configure

- (void)configureMicroscope:(CZMicroscopeViewModel *)microscopeViewModel {
    @weakify(self);
    [self.task prepareForConfiguringMicroscope:microscopeViewModel withCompletionHandler:^(CZCamera *camera, CZMicroscopeModel *microscopeModel, BOOL isLost) {
        @strongify(self);
        self.configuredCamera = camera;
        self.configuredMicroscopeModel = microscopeModel;
        self.configuredMicroscopeViewModel = microscopeViewModel;
        self.isReadOnlyConfigurationMode = NO;
        
        if (isLost || [camera isMemberOfClass:[CZCamera class]]) {
            self.isReadOnlyConfigurationMode = YES;
        }
        
        if ([camera hasPIN]) {
            [self initInputPINAlertView];
            if (microscopeModel.cachedCameraPIN.length > 0) {
                CZTextField *textField = self.inputPINAlertView.textFields[0];
                textField.text = microscopeModel.cachedCameraPIN;
                
                CZCheckBox *checkBox = self.inputPINAlertView.checkBoxes[0];
                checkBox.selected = YES;
            }
            [self.inputPINAlertView presentAnimated:YES completion:nil];
        } else {
            [self microscopeConfigurationAction];
        }
    }];
}

- (void)microscopeConfigurationAction {
    CZCamera *currentCamera = self.configuredMicroscopeViewModel.camera;
    if (currentCamera == nil) {
        return;
    }
    
    if ([currentCamera isFirmwareUpdated] == NO) {
        UIViewController *content;
        if ([currentCamera isKindOfClass:[CZKappaCamera class]]) {
            CZKappaCameraFirmwareUpdateViewController *kappaFirmwareUpdateViewController = [[CZKappaCameraFirmwareUpdateViewController alloc] init];
            kappaFirmwareUpdateViewController.delegate = self;
            content = kappaFirmwareUpdateViewController;
        } else {
            CZFirmwareUpdateViewController *firmwareUpdateViewController = [[CZFirmwareUpdateViewController alloc] init];
            firmwareUpdateViewController.camera = currentCamera;
            firmwareUpdateViewController.delegate = self;
            content = firmwareUpdateViewController;
        }
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:content];
        navigation.modalPresentationStyle = UIModalPresentationFormSheet;
        navigation.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:navigation animated:YES completion:NULL];
        return;
    }
    
    [self pushMicroscopeConfigurationViewController];
}

- (void)pushMicroscopeConfigurationViewController {
    CZMicroscopeConfigurationViewController *microscopeConfigurationViewController = [[CZMicroscopeConfigurationViewController alloc] initWithCamera:self.configuredCamera microscopeModel:self.configuredMicroscopeModel isReadOnlyMode:self.isReadOnlyConfigurationMode];
    microscopeConfigurationViewController.delegate = self;
    
    [self showWaitingHUD:YES];
    
    [microscopeConfigurationViewController prepareForConfigurationWithCompletionHandler:^(BOOL prepared) {
        [self showWaitingHUD:NO];
        
        if (prepared == NO) {
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"LIVE_NO_CONTROL_ACCESS_MESSAGE") level:CZAlertLevelError];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
            [alert presentAnimated:YES completion:nil];
            return;
        }
        
        [self.navigationController pushViewController:microscopeConfigurationViewController animated:YES];
        
        self.configuredCamera = nil;
        self.configuredMicroscopeModel = nil;
        self.configuredMicroscopeViewModel = nil;
        self.isReadOnlyConfigurationMode = NO;
    }];
}

#pragma mark - Input PIN

- (void)initInputPINAlertView {
    CZInputAlertController *alert = [CZInputAlertController alertControllerWithTitle:nil message:L(@"MIC_CONFIG_LOCK_PIN_LABEL_2")];
    
    [alert addTextFieldWithLabel:L(@"MIC_CONFIG_INPUT_PIN_LABEL_1") configurationHandler:^(CZTextField *textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.secureTextEntry = YES;
    }];
    
    [alert addCheckBoxWithConfigurationHandler:^(CZCheckBox *checkBox) {
        NSString *title = [NSString stringWithFormat:L(@"MIC_CONFIG_LOCK_PIN_REMEMBER_LABEL"), [[UIApplication sharedApplication] cz_name]];
        [checkBox setTitle:title forState:UIControlStateNormal];
    }];
    
    @weakify(self);
    [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZInputAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self inputPINAlertViewAction];
    }];
    
    self.inputPINAlertView = alert;
}

- (void)inputPINAlertLoop {
    [self initInputPINAlertView];
    self.inputPINAlertView.message = L(@"MIC_CONFIG_LOCK_PIN_MISMATCH");
    [self.inputPINAlertView presentAnimated:YES completion:nil];
    [self.inputPINAlertTimer invalidate];
    self.inputPINAlertTimer = nil;
}

- (void)inputPINAlertViewAction {
    UITextField *textField = self.inputPINAlertView.textFields[0];
    
    CZCheckBox *checkBox = self.inputPINAlertView.checkBoxes[0];
    BOOL isPINRemembered = checkBox.isSelected;
    
    if ([self.task validatePIN:textField.text forMicroscope:self.configuredMicroscopeViewModel remembersPIN:isPINRemembered]) {
        [self.inputPINAlertTimer invalidate];
        self.inputPINAlertTimer = nil;
        
        [self microscopeConfigurationAction];
    } else {
        self.numOfPINInputTimes++;
        if (self.numOfPINInputTimes < kNumOfPINInputTimesAllowed) {
            if (!self.inputPINAlertTimer) {
                self.inputPINAlertTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                                           target:self
                                                                         selector:@selector(inputPINAlertLoop)
                                                                         userInfo:nil
                                                                          repeats:NO];
            }
        } else {
            self.numOfPINInputTimes = 0;
            [self.inputPINAlertTimer invalidate];
            self.inputPINAlertTimer = nil;
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"MIC_CONFIG_LOCK_PIN_FORGET_MESSAGE") level:CZAlertLevelInfo];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        }
    }
}

#pragma mark - Private

- (void)updateNoMicroscopesLabelVisibility {
    if ([[CZDefaultSettings sharedInstance] virtualMicroscopesOption] == kCZNeverShowVirtualMicroscopes) {
        BOOL show = (self.hud == nil && self.sortedMicroscopeViewModels.count == 0);
        self.noMicroscopesLabel.hidden = !show;
    } else {
        self.noMicroscopesLabel.hidden = YES;
    }
}

#pragma mark - CZTaskDelegate

- (instancetype)initWithTask:(CZMicroscopesTask *)task {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        NSAssert([task isKindOfClass:[CZMicroscopesTask class]], @"%@ is not a microscopes task", task);
        _task = task;
    }
    return self;
}

- (UIViewController *)taskViewController {
    return self;
}

- (void)taskDidEnterForeground:(CZTask *)task {
    [self.task startMicroscopeBrowsing];
}

- (void)taskDidEnterBackground:(CZTask *)task {
    [self.task stopMicroscopeBrowsing];
}

#pragma mark - CZMicroscopesTaskDelegate

- (void)microscopesTaskWLANNameDidChange:(CZMicroscopesTask *)task {
    CZToolbarItem *item = [self.virtualToolbar itemForIdentifier:CZToolbarItemIdentifierWLANName];
    item.label.text = task.wlanName;
}

- (void)microscopesTaskLoginInfoDidChange:(CZMicroscopesTask *)task {
    CZToolbarItem *item = [self.virtualToolbar itemForIdentifier:CZToolbarItemIdentifierLoginInfo];
    item.label.text = task.loginInfo;
}

- (void)microscopesTaskLoadingMicroscopesDidChange:(CZMicroscopesTask *)task {
    if (task.isLoadingMicroscopes) {
        if (self.hud == nil) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud setColor:kSpinnerBackgroundColor];
            [hud setLabelText:L(@"BUSY_INDICATOR_DISCOVERING")];
            self.hud = hud;
        }
        
        [self updateNoMicroscopesLabelVisibility];
    } else {
        if (self.hud) {
            [self.hud dismiss:YES];
            self.hud = nil;
        }
        
        [self updateNoMicroscopesLabelVisibility];
        [self.task refreshCameraAppearance];
        [self.task updatePlusCameraAppearance];
    }
}

- (void)microscopesTaskSortedMicroscopeViewModelsDidChange:(CZMicroscopesTask *)task {
    self.sortedMicroscopeViewModels = task.sortedMicroscopeViewModels;
    [self.microscopeCollectionView reloadData];
    [self updateNoMicroscopesLabelVisibility];
}

- (void)microscopesTaskNoMicroscopesTextDidChange:(CZMicroscopesTask *)task {
    self.noMicroscopesLabel.text = task.noMicroscopesText;
}

#pragma mark - CZMainToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarPrimaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarSecondaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarCenterItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

#pragma mark - CZVirtualToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarLeadingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return @[CZToolbarItemIdentifierLoginInfo];
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarCenterItemIdentifiers:(CZToolbar *)virtualToolbar {
    return @[CZToolbarItemIdentifierWLANName];
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarTrailingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return @[CZToolbarItemIdentifierMicroscopeZoom];
}

#pragma mark - CZToolbarDelegate

- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier {
    return [super toolbar:toolbar makeItemForIdentifier:identifier];
}

- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierMicroscopeZoom]) {
        item.slider.value = self.microscopeCollectionViewLayout.scale;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierWLANName]) {
        item.label.text = self.task.wlanName;
    }
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierMicroscopeZoom]) {
        self.microscopeCollectionViewLayout.scale = item.slider.value;
        [[NSUserDefaults standardUserDefaults] setFloat:item.slider.value forKey:CZMicroscopeCollectionViewCellScaleKey];
        return YES;
    }
    
    return NO;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.sortedMicroscopeViewModels.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZMicroscopeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CZMicroscopeCollectionViewCell class]) forIndexPath:indexPath];
    cell.delegate = self;
    cell.viewModel = self.sortedMicroscopeViewModels[indexPath.item];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item < self.sortedMicroscopeViewModels.count) {
        CZMicroscopeViewModel *microscopeViewModel = self.sortedMicroscopeViewModels[indexPath.item];
        [self.task microscopeBecomeVisible:microscopeViewModel];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item < self.sortedMicroscopeViewModels.count) {
        CZMicroscopeViewModel *microscopeViewModel = self.sortedMicroscopeViewModels[indexPath.item];
        [self.task microscopeBecomeInvisible:microscopeViewModel];
    }
}

#pragma mark - CZMicroscopeCollectionViewCellDelegate

- (void)microscopeCollectionViewCellDidTapThumbnail:(CZMicroscopeCollectionViewCell *)cell {
    NSString *originalCameraMACAddress = self.task.manager.liveTask.currentCamera.macAddress;
    
    CZMicroscopeViewModel *microscope = cell.viewModel;
    if (microscope.isPlusMicroscope) {
        CZManuallyAddMicroscopeAlertController *alert = [[CZManuallyAddMicroscopeAlertController alloc] init];
        alert.delegate = self;
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    [self.task selectMicroscope:microscope];
    
    CZCamera *camera = self.task.manager.liveTask.currentCamera;
    BOOL cameraChanged = ![camera.macAddress isEqualToString:originalCameraMACAddress];
    
    if (cameraChanged) {
        // Check whether resolution is 720p for Kappa camera. If it's larger,
        // such as 1080p, need to show an alert to user.
        if ([camera isKindOfClass:[CZKappaCamera class]]) {
            CZKappaCamera *kappaCamera = (CZKappaCamera *)camera;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                CZCameraControlLock *cameraControlLock = [[CZCameraControlLock alloc] initWithCamera:kappaCamera];
                if (cameraControlLock) {
                    uint32_t width = [kappaCamera readRegister:kGEVRegisterImageResolutionWidth];
                    
                    if (width > [kappaCamera liveResolution].width) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_UNSUPPORTED_RESOLUTION_MESSAGE") level:CZAlertLevelInfo];
                            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                            [alert presentAnimated:YES completion:nil];
                        });
                    }
                }
            });
        }
        // Check whether camera is running iOS 8 new streaming mode.
        // If true and current iOS version is less than 8, show an alert to user.
        else if ([camera isKindOfClass:[CZWisionCamera class]] && (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1)) {
            CZWisionCamera *wisionCamera = (CZWisionCamera *)camera;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if ([wisionCamera IFrameRatio] == kCZCameraIFrameRatioHigh) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:L(@"LIVE_STREAMING_MODE_WARNING") level:CZAlertLevelInfo];
                        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                        [alert presentAnimated:YES completion:nil];
                    });
                }
            });
        }
    }
}

- (void)microscopeCollectionViewCellDidTapConfigure:(CZMicroscopeCollectionViewCell *)cell {
    [self configureMicroscope:cell.viewModel];
}

- (void)microscopeCollectionViewCellDidTapRemove:(CZMicroscopeCollectionViewCell *)cell {
    CZMicroscopeViewModel *microscope = cell.viewModel;
    
    @weakify(self);
    CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"MICROSCOPES_REMOVE_MESSAGE") level:CZAlertLevelWarning];
    [alertController addActionWithTitle:L(@"NO") style:CZDialogActionStyleDestructive handler:NULL];
    [alertController addActionWithTitle:L(@"YES") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self.task removeMicroscope:microscope];
        [self.task refreshCameraAppearance];
        [self updateNoMicroscopesLabelVisibility];
    }];
    
    [alertController presentAnimated:YES completion:nil];
}

#pragma mark - CZManuallyAddMicroscopeAlertControllerDelegate

- (BOOL)manuallyAddMicroscopeAlertController:(CZManuallyAddMicroscopeAlertController *)manuallyAddMicroscopeAlertController canManuallyAddMicroscopeForCamera:(CZCamera *)camera {
    return [self.task canCreatePlaceholderForManuallyAddCamera:camera];
}

- (void)manuallyAddMicroscopeAlertController:(CZManuallyAddMicroscopeAlertController *)manuallyAddMicroscopeAlertController didManuallyAddMicroscopeForCamera:(CZCamera *)camera {
    [self.task createPlaceholderForManuallyAddCamera:camera];
    [self.task refreshCameraAppearance];
}

#pragma mark - CZFirmwareUpdateViewControllerDelegate

- (void)firmwareUpdateViewControllerDidFinish:(CZFirmwareUpdateViewController *)controller {
    if (controller.camera) {
        self.configuredCamera = controller.camera;
        if (controller.camera == nil) {
            self.isReadOnlyConfigurationMode = YES;
        }
        
        BOOL isFound = NO;
        
        //FIXME: Add the logic when the Labscope connected to the Digital Classroom server.
        NSArray *cameraList = nil;
#if DCM_REFACTORED
        if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
            cameraList = [[CZDCMCameraManager sharedInstance] cameraArray];
        } else {
#endif
            cameraList = [[CZCameraBrowser sharedInstance] cameras];
#if DCM_REFACTORED
        }
#endif
        
        for (CZCamera *camera in cameraList) {
            BOOL isEqualMACAddress = [camera.macAddress isEqualComparisionWithCaseInsensitive:controller.camera.macAddress];
            if ((isEqualMACAddress)) {
                isFound = YES;
                break;
            }
        }
        
        if (isFound) {
            [self pushMicroscopeConfigurationViewController];
        }
    }
}

#pragma mark - CZKappaCameraFirmwareUpdateViewControllerDelegate

- (void)kappaFirmwareUpdateViewControllerDidPressedOk:(CZKappaCameraFirmwareUpdateViewController *)controller {
    // TODO: Consider whether the camera is still alive.
    if (self.configuredCamera) {
        [self pushMicroscopeConfigurationViewController];
    }
}

#pragma mark - CZMicroscopeConfigurationViewControllerDelegate

- (void)microscopeConfigurationViewControllerDidSaveConfiguration:(CZMicroscopeConfigurationViewController *)microscopeConfigurationViewController {
    [self.task applyConfigurationForCamera:microscopeConfigurationViewController.camera microscopeModel:microscopeConfigurationViewController.microscopeModel];
}

@end
