//
//  CZMicroscopeCollectionViewCell.h
//  Hermes
//
//  Created by Li, Junlin on 1/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZMicroscopeViewModel.h"

@class CZMicroscopeCollectionViewCell;

extern const CGFloat CZMicroscopeCollectionViewCellThumbnailAspectRatio;
extern const CGFloat CZMicroscopeCollectionViewCellAdditionalHeight;

@protocol CZMicroscopeCollectionViewCellDelegate <NSObject>

@optional
- (void)microscopeCollectionViewCellDidTapThumbnail:(CZMicroscopeCollectionViewCell *)cell;
- (void)microscopeCollectionViewCellDidTapConfigure:(CZMicroscopeCollectionViewCell *)cell;
- (void)microscopeCollectionViewCellDidTapRemove:(CZMicroscopeCollectionViewCell *)cell;

@end

@interface CZMicroscopeCollectionViewCell : UICollectionViewCell <CZMicroscopeViewModelObserver>

@property (nonatomic, weak) id<CZMicroscopeCollectionViewCellDelegate> delegate;
@property (nonatomic, readonly, strong) UIImageView *thumbnailView;
@property (nonatomic, readonly, strong) UILabel *microscopeNameLabel;
@property (nonatomic, readonly, strong) UIButton *configureButton;
@property (nonatomic, readonly, strong) UIButton *removeButton;
@property (nonatomic, readonly, strong) UIImageView *defaultIcon;

@property (nonatomic, strong) CZMicroscopeViewModel *viewModel;

@end
