//
//  CZMicroscopeCollectionViewLayout.h
//  Hermes
//
//  Created by Li, Junlin on 1/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat CZMicroscopeCollectionViewCellThumbnailAspectRatio;

@interface CZMicroscopeCollectionViewLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, readonly, assign) CGFloat defaultScale;

@end
