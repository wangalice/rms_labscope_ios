//
//  CZManuallyAddMicroscopeAlertController.m
//  Hermes
//
//  Created by Li, Junlin on 7/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZManuallyAddMicroscopeAlertController.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZIPAddressValidator.h"
#import "CZMicroscopeNameValidator.h"

@interface CZManuallyAddMicroscopeAlertController () <CZTextFieldDelegate>

@property (nonatomic, readonly, strong) CZTextField *microscopeIPAddressTextField;
@property (nonatomic, readonly, strong) CZTextField *microscopeNameTextField;
@property (nonatomic, readonly, strong) CZDialogAction *okAction;

@property (nonatomic, readonly, strong) CZIPAddressValidator *ipAddressValidator;
@property (nonatomic, readonly, strong) CZMicroscopeNameValidator *microscopeNameValidator;

@end

@implementation CZManuallyAddMicroscopeAlertController

@synthesize ipAddressValidator = _ipAddressValidator;
@synthesize microscopeNameValidator = _microscopeNameValidator;

- (instancetype)init {
    self = [super initWithTitle:L(@"MIC_MANUALLY_ADD_MICROSCOPE_TITLE") message:L(@"MIC_MANUALLY_ADD_MICROSCOPE_LABEL")];
    if (self) {
        @weakify(self);
        
        [self addTextFieldWithLabel:L(@"MIC_MANUALLY_ADD_MICROSCOPE_IP") configurationHandler:^(CZTextField *textField) {
            @strongify(self);
            textField.delegate = self;
            textField.keyboardType = UIKeyboardTypeDecimalPad;
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.returnKeyType = UIReturnKeyNext;
            [textField addTarget:self action:@selector(textFieldAction:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        [self addTextFieldWithLabel:L(@"MIC_CONFIG_NAME") configurationHandler:^(CZTextField *textField) {
            @strongify(self);
            textField.delegate = self;
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldAction:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        [self addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        
        _okAction = [self addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZInputAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            [self okButtonAction];
        }];
        _okAction.enabled = NO;
        _okAction.disablesAutomaticDismissal = YES;
    }
    return self;
}

- (CZTextField *)microscopeIPAddressTextField {
    return self.textFields[0];
}

- (CZTextField *)microscopeNameTextField {
    return self.textFields[1];
}

- (CZIPAddressValidator *)ipAddressValidator {
    if (_ipAddressValidator == nil) {
        _ipAddressValidator = [[CZIPAddressValidator alloc] init];
    }
    return _ipAddressValidator;
}

- (CZMicroscopeNameValidator *)microscopeNameValidator {
    if (_microscopeNameValidator == nil) {
        _microscopeNameValidator = [[CZMicroscopeNameValidator alloc] initWithMaxSupportedNameLength:12];
    }
    return _microscopeNameValidator;
}

- (void)okButtonAction {
    NSString *ipAddress = self.microscopeIPAddressTextField.text;
    BOOL isValidIpAddress = [CZCommonUtils isIPAddress:ipAddress];
    
    if (!isValidIpAddress) {
        self.errorMessage = L(@"MIC_MANUALLY_ADD_MICROSCOPE_ALERT");
        return;
    }
    
    NSArray *ipFields = [ipAddress componentsSeparatedByString:@"."];
    if (ipFields.count == 4) {
        NSInteger beginIPNumber = [ipFields[0] integerValue];
        NSInteger endIPNumber = [ipFields[3] integerValue];
        if (beginIPNumber == 0 || beginIPNumber == 255 || endIPNumber == 0 || endIPNumber == 255) {
            self.errorMessage = L(@"MIC_MANUALLY_ADD_MICROSCOPE_ALERT");
            return;
        }
    }
    
    NSArray *microscopesSettings = [[CZDefaultSettings sharedInstance] manuallyAddMicroscopesSettings];
    int maxNum = 0;
    for (NSDictionary *settings in microscopesSettings) {
        NSString *macAddress = settings[kManuallyAddMicroscopeMacAddress];
        NSArray *fields = [macAddress componentsSeparatedByString:@":"];
        if (fields.count == 6) {
            NSString *hexString = fields[5];
            unsigned int outValue;
            NSScanner *scanner = [NSScanner scannerWithString:hexString];
            [scanner scanHexInt:&outValue];
            if (outValue > maxNum) {
                maxNum = outValue;
            }
        }
    }
    
    if (maxNum >= 255) {
        return;
    }
    
    NSString *fakeMacAddress = [[NSString alloc] initWithFormat:@"%@:%02x", CZPlusCameraMACAddressPrefix, (unsigned int)(maxNum + 1)];
    NSString *name = self.microscopeNameTextField.text ?: @"";
    
    CZCamera *camera = [[CZCamera alloc] init];
    [camera setIpAddress:ipAddress];
    [camera setMacAddress:fakeMacAddress];
    [camera setDisplayName:name andUpload:NO];
    
    BOOL canAdd = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(manuallyAddMicroscopeAlertController:canManuallyAddMicroscopeForCamera:)]) {
        canAdd = [self.delegate manuallyAddMicroscopeAlertController:self canManuallyAddMicroscopeForCamera:camera];
    }
    
    if (canAdd == NO) {
        self.errorMessage = L(@"MIC_MANUALLY_ADD_MICROSCOPE_ALERT1");
        return;
    }
    
    NSDictionary *settings = @{kManuallyAddMicroscopeName: name,
                               kManuallyAddMicroscopeMacAddress: fakeMacAddress,
                               kManuallyAddMicroscopeIPAddress: ipAddress};
    
    BOOL success = [[CZDefaultSettings sharedInstance] addManuallyMicroscopeSettingsToHistory:settings];
    
    if (!success) {
        self.errorMessage = L(@"MIC_MANUALLY_ADD_MICROSCOPE_ALERT1");
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(manuallyAddMicroscopeAlertController:didManuallyAddMicroscopeForCamera:)]) {
        [self.delegate manuallyAddMicroscopeAlertController:self didManuallyAddMicroscopeForCamera:camera];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldAction:(id)sender {
    self.errorMessage = nil;
    self.okAction.enabled = self.microscopeIPAddressTextField.text.length > 0 && self.microscopeNameTextField.text.length > 0;
}

#pragma mark - CZTextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField == self.microscopeIPAddressTextField) {
        [self.microscopeNameTextField becomeFirstResponder];
    } else if (textField == self.microscopeNameTextField) {
        [self.microscopeNameTextField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.microscopeNameTextField) {
        [self.microscopeNameValidator textFieldDidEndEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.microscopeNameTextField) {
        return [self.microscopeNameValidator textField:textField shouldChangeCharactersInRange:range replacementString:string];
    } else if (textField == self.microscopeIPAddressTextField) {
        return [self.ipAddressValidator textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

@end
