//
//  CZMicroscopeCollectionViewCell.m
//  Hermes
//
//  Created by Li, Junlin on 1/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeCollectionViewCell.h"
#import "CZMicroscopeCollectionViewLayout.h"

@implementation CZMicroscopeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor cz_gs100];
        self.contentView.layer.cornerRadius = 3.0;
        self.contentView.layer.masksToBounds = YES;
        self.contentView.layer.borderWidth = 1.0;
        self.contentView.layer.borderColor = [[UIColor cz_gs115] CGColor];
        
        _thumbnailView = ({
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.backgroundColor = [UIColor cz_gs110];
            imageView.userInteractionEnabled = YES;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            
            imageView.isAccessibilityElement = YES;
            imageView.accessibilityIdentifier = @"MicroscopeThumbnailView";
            
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(thumbnailTapGestureRecognizerAction:)];
            [imageView addGestureRecognizer:tapGestureRecognizer];
            
            [self.contentView addSubview:imageView];
            imageView;
        });
        
        _microscopeNameLabel = ({
            UILabel *label = [[UILabel alloc] init];
            label.font = [UIFont cz_h4];
            label.textColor = [UIColor cz_gs50];
            label.textAlignment = NSTextAlignmentLeft;
            label.numberOfLines = 1;
            
            label.isAccessibilityElement = YES;
            label.accessibilityIdentifier = @"MicroscopeNameLabel";
            
            [self.contentView addSubview:label];
            label;
        });
        
        _configureButton = ({
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button cz_setImageWithIcon:[CZIcon iconNamed:@"settings"]];
            [button addTarget:self action:@selector(configureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            button.isAccessibilityElement = YES;
            button.accessibilityIdentifier = @"MicroscopeConfigureButton";
            
            [self.contentView addSubview:button];
            button;
        });
        
        _removeButton = ({
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.hidden = YES;
            [button cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
            [button addTarget:self action:@selector(removeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:button];
            button;
        });

        _defaultIcon = ({
            UIImage *image = [UIImage imageNamed:A(@"green-check")];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.frame = CGRectMake(10.0, 10.0, image.size.width, image.size.height);
            imageView.hidden = YES;
            [self.contentView addSubview:imageView];
            imageView;
        });
    }
    return self;
}

- (void)dealloc {
    [_viewModel removeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat contentWidth = self.contentView.frame.size.width;
    CGFloat contentHeight = self.contentView.frame.size.height;
    CGFloat thumbnailHeight = contentWidth / CZMicroscopeCollectionViewCellThumbnailAspectRatio;
    CGFloat remainingHeight = contentHeight - thumbnailHeight;
    CGFloat configureButtonWidth = 56.0;
    
    self.thumbnailView.frame = CGRectMake(0.0, 0.0, contentWidth, thumbnailHeight);
    self.microscopeNameLabel.frame = CGRectMake(16.0, thumbnailHeight, contentWidth - configureButtonWidth - 16.0, remainingHeight);
    self.configureButton.frame = CGRectMake(contentWidth - configureButtonWidth, thumbnailHeight, configureButtonWidth, remainingHeight);
    self.removeButton.frame = self.configureButton.frame;
}

- (void)setViewModel:(CZMicroscopeViewModel *)viewModel {
    [_viewModel removeObserver:self];
    
    _viewModel = viewModel;
    
    [_viewModel addObserver:self];
    
    [self setThumbnail:viewModel.thumbnail isMicroscopeEnabled:viewModel.isEnabled isPlusMicroscope:viewModel.isPlusMicroscope];
    
    self.microscopeNameLabel.text = viewModel.microscopeName;
    self.configureButton.hidden = !viewModel.isConfigurable || viewModel.isPlusMicroscope || !viewModel.isEnabled;
    self.removeButton.hidden = viewModel.isEnabled;
    self.defaultIcon.hidden = !viewModel.isDefaultMicroscope;
    
    if (viewModel.isEnabled) {
#if MIC_REFACTORED
        self.alpha = 1.0;
        self.microscopeNameLabel.alpha = 1.0;
        self.microscopeNameLabel.backgroundColor = self.backgroundColor;
        self.configureButton.alpha = 1.0;
        self.thumbnailView.alpha = 1.0;
#endif
    } else {
#if MIC_REFACTORED
        self.alpha = 0.5;
        self.microscopeNameLabel.alpha = 0.5;
        self.microscopeNameLabel.backgroundColor = [UIColor clearColor];
        self.configureButton.alpha = 0.5;
        self.thumbnailView.alpha = 0.5;
#endif
    }
    
    if (viewModel.isLocked) {
        [self.configureButton cz_setImageWithIcon:[CZIcon iconNamed:@"settings-locked"]];
    } else {
        [self.configureButton cz_setImageWithIcon:[CZIcon iconNamed:@"settings"]];
    }
    
    if (viewModel.isCurrentMicroscope) {
        self.microscopeNameLabel.textColor = [UIColor cz_pb100];
    } else {
        self.microscopeNameLabel.textColor = [UIColor cz_gs50];
    }
}

- (void)setThumbnail:(UIImage *)thumbnail isMicroscopeEnabled:(BOOL)isMicroscopeEnabled isPlusMicroscope:(BOOL)isPlusMicroscope {
    if (isPlusMicroscope) {
        self.thumbnailView.image = [UIImage cz_imageWithIcon:[CZIcon iconNamed:@"plus"]];
        self.thumbnailView.contentMode = UIViewContentModeCenter;
    } else if (!isMicroscopeEnabled) {
        self.thumbnailView.image = [UIImage cz_imageWithIcon:[CZIcon iconNamed:@"microscope-thumbnail-lost"]];
        self.thumbnailView.contentMode = UIViewContentModeCenter;
    } else if (thumbnail == nil) {
        self.thumbnailView.image = [[CZIcon iconNamed:@"microscope-thumbnail-placeholder"] image];
        self.thumbnailView.contentMode = UIViewContentModeCenter;
    } else {
        self.thumbnailView.image = thumbnail;
        self.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    }
}

#pragma mark - Actions

- (void)thumbnailTapGestureRecognizerAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopeCollectionViewCellDidTapThumbnail:)]) {
        [self.delegate microscopeCollectionViewCellDidTapThumbnail:self];
    }
}

- (void)configureButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopeCollectionViewCellDidTapConfigure:)]) {
        [self.delegate microscopeCollectionViewCellDidTapConfigure:self];
    }
}

- (void)removeButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopeCollectionViewCellDidTapRemove:)]) {
        [self.delegate microscopeCollectionViewCellDidTapRemove:self];
    }
}

#pragma mark - CZMicroscopeViewModelObserver

- (void)microscopeViewModelMicroscopeNameDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    self.microscopeNameLabel.text = microscopeViewModel.microscopeName;
}

- (void)microscopeViewModelThumbnailDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    [self setThumbnail:microscopeViewModel.thumbnail isMicroscopeEnabled:microscopeViewModel.isEnabled isPlusMicroscope:microscopeViewModel.isPlusMicroscope];
}

- (void)microscopeViewModelLockedDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    if (microscopeViewModel.isLocked) {
        [self.configureButton cz_setImageWithIcon:[CZIcon iconNamed:@"settings-locked"]];
    } else {
        [self.configureButton cz_setImageWithIcon:[CZIcon iconNamed:@"settings"]];
    }
}

- (void)microscopeViewModelEnabledDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    [self setThumbnail:microscopeViewModel.thumbnail isMicroscopeEnabled:microscopeViewModel.isEnabled isPlusMicroscope:microscopeViewModel.isPlusMicroscope];
    
    self.configureButton.hidden = !microscopeViewModel.isConfigurable || microscopeViewModel.isPlusMicroscope || !microscopeViewModel.isEnabled;
    self.removeButton.hidden = microscopeViewModel.isEnabled;
    
    if (microscopeViewModel.isEnabled) {
#if MIC_REFACTORED
        self.alpha = 1.0;
        self.microscopeNameLabel.alpha = 1.0;
        self.microscopeNameLabel.backgroundColor = self.backgroundColor;
        self.configureButton.alpha = 1.0;
        self.thumbnailView.alpha = 1.0;
#endif
    } else {
#if MIC_REFACTORED
        self.alpha = 0.5;
        self.microscopeNameLabel.alpha = 0.5;
        self.microscopeNameLabel.backgroundColor = [UIColor clearColor];
        self.configureButton.alpha = 0.5;
        self.thumbnailView.alpha = 0.5;
#endif
    }
}

- (void)microscopeViewModelConfigurableDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    self.configureButton.hidden = !microscopeViewModel.isConfigurable || microscopeViewModel.isPlusMicroscope || !microscopeViewModel.isEnabled;
}

- (void)microscopeViewModelIsDefaultMicroscopeDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    self.defaultIcon.hidden = !microscopeViewModel.isDefaultMicroscope;
}

- (void)microscopeViewModelIsCurrentMicroscopeDidChange:(CZMicroscopeViewModel *)microscopeViewModel {
    if (microscopeViewModel.isCurrentMicroscope) {
        self.microscopeNameLabel.textColor = [UIColor cz_pb100];
    } else {
        self.microscopeNameLabel.textColor = [UIColor cz_gs50];
    }
}

@end
