//
//  CZMicroscopeCollectionViewLayout.m
//  Hermes
//
//  Created by Li, Junlin on 1/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeCollectionViewLayout.h"

const CGFloat CZMicroscopeCollectionViewInset = 32.0;
const CGFloat CZMicroscopeCollectionViewCellSpacing = 16.0;
const NSInteger CZMicroscopeCollectionViewCellMinimumCount = 3;
const NSInteger CZMicroscopeCollectionViewCellDefaultCount = 5;
const NSInteger CZMicroscopeCollectionViewCellMaximumCount = 7;
const CGFloat CZMicroscopeCollectionViewCellThumbnailAspectRatio = 16.0 / 9.0;
const CGFloat CZMicroscopeCollectionViewCellAdditionalHeight = 56.0;

@implementation CZMicroscopeCollectionViewLayout

- (instancetype)init {
    self = [super init];
    if (self) {
        _scale = self.defaultScale;
        
        self.minimumLineSpacing = CZMicroscopeCollectionViewCellSpacing;
        self.minimumInteritemSpacing = CZMicroscopeCollectionViewCellSpacing;
        self.sectionInset = UIEdgeInsetsMake(0.0, CZMicroscopeCollectionViewInset, 0.0, CZMicroscopeCollectionViewInset);
    }
    return self;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray<UICollectionViewLayoutAttributes *> *layoutAttributesForElements = [super layoutAttributesForElementsInRect:rect];
    
    CGFloat count = CZMicroscopeCollectionViewCellMaximumCount - (CZMicroscopeCollectionViewCellMaximumCount - CZMicroscopeCollectionViewCellMinimumCount) * self.scale;
    if ([self.collectionView numberOfItemsInSection:0] > count) {
        return layoutAttributesForElements;
    }
    
    UICollectionViewLayoutAttributes *previousLayoutAttributes = nil;
    for (UICollectionViewLayoutAttributes *layoutAttributes in layoutAttributesForElements) {
        if (previousLayoutAttributes != nil && CGRectGetMinX(layoutAttributes.frame) - CGRectGetMaxX(previousLayoutAttributes.frame) > self.minimumInteritemSpacing) {
            CGRect frame = layoutAttributes.frame;
            frame.origin.x = CGRectGetMaxX(previousLayoutAttributes.frame) + self.minimumInteritemSpacing;
            layoutAttributes.frame = frame;
        }
        previousLayoutAttributes = layoutAttributes;
    }
    
    return layoutAttributesForElements;
}

- (void)setScale:(CGFloat)scale {
    _scale = scale;
    _scale = MAX(_scale, 0.0);
    _scale = MIN(_scale, 1.0);
    
    CGFloat count = CZMicroscopeCollectionViewCellMaximumCount - (CZMicroscopeCollectionViewCellMaximumCount - CZMicroscopeCollectionViewCellMinimumCount) * self.scale;
    CGFloat width = (self.collectionView.bounds.size.width - CZMicroscopeCollectionViewInset * 2 - CZMicroscopeCollectionViewCellSpacing * (count - 1)) / count;
    CGFloat height = (width / CZMicroscopeCollectionViewCellThumbnailAspectRatio) + CZMicroscopeCollectionViewCellAdditionalHeight;
    self.itemSize = CGSizeMake(floorf(width), floorf(height));
    
    [self invalidateLayout];
}

- (CGFloat)defaultScale {
    return 1.0 * (CZMicroscopeCollectionViewCellMaximumCount - CZMicroscopeCollectionViewCellDefaultCount) / (CZMicroscopeCollectionViewCellMaximumCount - CZMicroscopeCollectionViewCellMinimumCount);
}

@end
