//
//  CZApplication.m
//  Hermes
//
//  Created by Li, Junlin on 3/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZApplication.h"

@interface CZApplication ()

@property (nonatomic, readwrite, strong) UIEvent *latestEvent;
@property (nonatomic, readwrite, strong) NSDate *latestEventSendDate;

@end

@implementation CZApplication

@dynamic sharedApplication;

- (void)sendEvent:(UIEvent *)event {
    self.latestEvent = event;
    self.latestEventSendDate = [NSDate date];
    [super sendEvent:event];
}

@end
