//
//  CZPopoverPresentationController.h
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZPopoverArrowDirection.h"

@interface CZPopoverPresentationController : UIPresentationController

@property (nonatomic, strong) UIView *sourceView;
@property (nonatomic, assign) CGRect sourceRect;

@property (nonatomic, assign) CZPopoverArrowDirection arrowDirection;

@property (nonatomic, copy) NSArray<UIView *> *passthroughViews;

@property (nonatomic, assign) UIEdgeInsets popoverLayoutMargins;

@property (nonatomic, assign) BOOL automaticallyAdjustsFrameWhenKeyboardAppears;

@end
