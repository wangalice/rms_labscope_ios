//
//  CZPopoverBackgroundView.m
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZPopoverBackgroundView.h"

@implementation CZPopoverBackgroundView

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CAShapeLayer *layer = (CAShapeLayer *)self.layer;
    layer.path = [[self pathForArrowDirection:self.arrowDirection] CGPath];
    layer.fillColor = self.color.CGColor;
    layer.strokeColor = self.borderColor.CGColor;
    layer.lineWidth = self.borderWidth;
}

- (UIBezierPath *)pathForArrowDirection:(CZPopoverArrowDirection)arrowDirection {
    switch (arrowDirection) {
        case CZPopoverArrowDirectionUp:
            return [self pathForArrowDirectionUp];
        case CZPopoverArrowDirectionDown:
            return [self pathForArrowDirectionDown];
        case CZPopoverArrowDirectionLeft:
            return [self pathForArrowDirectionLeft];
        case CZPopoverArrowDirectionRight:
            return [self pathForArrowDirectionRight];
    }
}

///               A
///      C        /\        J
///     ⌜---------  ---------⌝
///    D|        B  K        |I
///     |                    |
///     |                    |
///     |                    |
///    E|                    |H
///     ⌞--------------------⌟
///      F                  G
- (UIBezierPath *)pathForArrowDirectionUp {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sourceFrame = [self.sourceView convertRect:self.sourceRect toView:self];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint point;
    CGPoint center;
    
    // Move to A
    point.x = CGRectGetMidX(sourceFrame);
    point.y = 0.0;
    [path moveToPoint:point];
    
    // Add line from A to B
    point.x -= self.arrowBase / 2;
    point.y += self.arrowHeight;
    [path addLineToPoint:point];
    
    // Add line from B to C
    point.x = self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from C to D
    center.x = self.cornerRadius;
    center.y = self.arrowHeight + self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 * 3 endAngle:M_PI clockwise:NO];
    
    // Add line from D to E
    point.x = 0.0;
    point.y = height - self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from E to F
    center = point;
    center.x = self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI endAngle:M_PI_2 clockwise:NO];
    
    // Add line from F to G
    point.x = width - self.cornerRadius;
    point.y = height;
    [path addLineToPoint:point];
    
    // Add arc from G to H
    center = point;
    center.y -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 endAngle:0.0 clockwise:NO];
    
    // Add line from H to I
    point.x = width;
    point.y = self.arrowHeight + self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from I to J
    center = point;
    center.x -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:0.0 endAngle:M_PI_2 * 3 clockwise:NO];
    
    // Add line from J to K
    point.x = CGRectGetMidX(sourceFrame) + self.arrowBase / 2;
    point.y = self.arrowHeight;
    [path addLineToPoint:point];
    
    // Add line from K to A
    [path closePath];
    
    return path;
}

///      G                  F
///     ⌜--------------------⌝
///    H|                    |E
///     |                    |
///     |                    |
///     |                    |
///    I|        K  B        |D
///     ⌞---------  ---------⌟
///      J        \/        C
///               A
- (UIBezierPath *)pathForArrowDirectionDown {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sourceFrame = [self.sourceView convertRect:self.sourceRect toView:self];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint point;
    CGPoint center;
    
    // Move to A
    point.x = CGRectGetMidX(sourceFrame);
    point.y = height;
    [path moveToPoint:point];
    
    // Add line from A to B
    point.x += self.arrowBase / 2;
    point.y -= self.arrowHeight;
    [path addLineToPoint:point];
    
    // Add line from B to C
    point.x = width - self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from C to D
    center = point;
    center.y -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 endAngle:0.0 clockwise:NO];
    
    // Add line from D to E
    point.x = width;
    point.y = self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from E to F
    center = point;
    center.x -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:0.0 endAngle:M_PI_2 * 3 clockwise:NO];
    
    // Add line from F to G
    point.x = self.cornerRadius;
    point.y = 0.0;
    [path addLineToPoint:point];
    
    // Add arc from G to H
    center.x = self.cornerRadius;
    center.y = self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 * 3 endAngle:M_PI clockwise:NO];
    
    // Add line from H to I
    point.x = 0.0;
    point.y = height - self.arrowHeight - self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from I to J
    center.x = self.cornerRadius;
    center.y = height - self.arrowHeight - self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI endAngle:M_PI_2 clockwise:NO];
    
    // Add line from J to K
    point.x = CGRectGetMidX(sourceFrame) - self.arrowBase / 2;
    point.y = height - self.arrowHeight;
    [path addLineToPoint:point];
    
    // Add line from K to A
    [path closePath];
    
    return path;
}

///       I        H
///      ⌜----------⌝
///     J|          |G
///      |          |
///      |K         |
///    A<           |
///      |B         |
///      |          |
///     C|          |F
///      ⌞----------⌟
///       D        E
- (UIBezierPath *)pathForArrowDirectionLeft {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sourceFrame = [self.sourceView convertRect:self.sourceRect toView:self];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint point;
    CGPoint center;
    
    // Move to A
    point.x = 0.0;
    point.y = CGRectGetMidY(sourceFrame);
    [path moveToPoint:point];
    
    // Add line from A to B
    point.x += self.arrowHeight;
    point.y += self.arrowBase / 2;
    [path addLineToPoint:point];
    
    // Add line from B to C
    point.y = height - self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from C to D
    center = point;
    center.x += self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI endAngle:M_PI_2 clockwise:NO];
    
    // Add line from D to E
    point.x = width - self.cornerRadius;
    point.y = height;
    [path addLineToPoint:point];
    
    // Add arc from E to F
    center = point;
    center.y -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 endAngle:0.0 clockwise:NO];
    
    // Add line from F to G
    point.x = width;
    point.y = self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from G to H
    center = point;
    center.x -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:0.0 endAngle:M_PI_2 * 3 clockwise:NO];
    
    // Add line from H to I
    point.x = self.arrowHeight + self.cornerRadius;
    point.y = 0.0;
    [path addLineToPoint:point];
    
    // Add arc from I to J
    center = point;
    center.y = self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 * 3 endAngle:M_PI clockwise:NO];
    
    // Add line from J to K
    point.x = self.arrowHeight;
    point.y = CGRectGetMidY(sourceFrame) - self.arrowBase / 2;
    [path addLineToPoint:point];
    
    // Add line from K to A
    [path closePath];
    
    return path;
}

///       E        D
///      ⌜----------⌝
///     F|          |C
///      |          |
///      |         B|
///      |           >A
///      |         K|
///      |          |
///     G|          |J
///      ⌞----------⌟
///       H        I
- (UIBezierPath *)pathForArrowDirectionRight {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGRect sourceFrame = [self.sourceView convertRect:self.sourceRect toView:self];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint point;
    CGPoint center;
    
    // Move to A
    point.x = width;
    point.y = CGRectGetMidY(sourceFrame);
    [path moveToPoint:point];
    
    // Add line from A to B
    point.x -= self.arrowHeight;
    point.y -= self.arrowBase / 2;
    [path addLineToPoint:point];
    
    // Add line from B to C
    point.y = self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from C to D
    center = point;
    center.x -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:0.0 endAngle:M_PI_2 * 3 clockwise:NO];
    
    // Add line from D to E
    point.x = self.cornerRadius;
    point.y = 0.0;
    [path addLineToPoint:point];
    
    // Add arc from E to F
    center = point;
    center.y += self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 * 3 endAngle:M_PI clockwise:NO];
    
    // Add line from F to G
    point.x = 0.0;
    point.y = height - self.cornerRadius;
    [path addLineToPoint:point];
    
    // Add arc from G to H
    center = point;
    center.x += self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI endAngle:M_PI_2 clockwise:NO];
    
    // Add line from H to I
    point.x = width - self.arrowHeight - self.cornerRadius;
    point.y = height;
    [path addLineToPoint:point];
    
    // Add arc from I to J
    center = point;
    center.y -= self.cornerRadius;
    [path addArcWithCenter:center radius:self.cornerRadius startAngle:M_PI_2 endAngle:0.0 clockwise:NO];
    
    // Add line from J to K
    point.x = width - self.arrowHeight;
    point.y = CGRectGetMidY(sourceFrame) + self.arrowBase / 2;
    [path addLineToPoint:point];
    
    // Add line from K to A
    [path closePath];
    
    return path;
}

@end
