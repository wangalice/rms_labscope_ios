//
//  CZPopoverController.m
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZPopoverController.h"
#import "CZPopoverBackgroundView.h"
#import "CZPopoverPresentationController.h"
#import "CZToolbarItem.h"
#import "UIWindow+CZPresentedViewController.h"

const CGFloat CZPopoverDefaultLayoutMargin = 16.0;

@interface CZPopoverController () <UIViewControllerTransitioningDelegate>

@property (nonatomic, weak) CZBarButtonItem *barButtonItem;
@property (nonatomic, weak) CZToolbarItem *toolbarItem;

@end

@implementation CZPopoverController

- (instancetype)initWithContentViewController:(UIViewController *)contentViewController {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _contentViewController = contentViewController;
        
        _arrowBase = 21.0;
        _arrowHeight = 12.0;
        
        _backgroundColor = [UIColor cz_gs100];
        _borderColor = nil;
        _borderWidth = 0.0;
        _cornerRadius = 8.0;
        
        _popoverLayoutMargins = UIEdgeInsetsMake(CZPopoverDefaultLayoutMargin, CZPopoverDefaultLayoutMargin, CZPopoverDefaultLayoutMargin, CZPopoverDefaultLayoutMargin);
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.transitioningDelegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CZPopoverBackgroundView *backgroundView = [[CZPopoverBackgroundView alloc] initWithFrame:self.view.bounds];
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    backgroundView.sourceView = self.sourceView;
    backgroundView.sourceRect = self.sourceRect;
    backgroundView.arrowBase = self.arrowBase;
    backgroundView.arrowHeight = self.arrowHeight;
    backgroundView.arrowDirection = self.arrowDirection;
    backgroundView.color = self.backgroundColor;
    backgroundView.borderColor = self.borderColor;
    backgroundView.borderWidth = self.borderWidth;
    backgroundView.cornerRadius = self.cornerRadius;
    [self.view addSubview:backgroundView];
    
    [self addChildViewController:self.contentViewController];
    
    switch (self.arrowDirection) {
        case CZPopoverArrowDirectionUp:
            self.contentViewController.view.frame = UIEdgeInsetsInsetRect(backgroundView.bounds, UIEdgeInsetsMake(self.arrowHeight, 0.0, 0.0, 0.0));
            break;
        case CZPopoverArrowDirectionDown:
            self.contentViewController.view.frame = UIEdgeInsetsInsetRect(backgroundView.bounds, UIEdgeInsetsMake(0.0, 0.0, self.arrowHeight, 0.0));
            break;
        case CZPopoverArrowDirectionLeft:
            self.contentViewController.view.frame = UIEdgeInsetsInsetRect(backgroundView.bounds, UIEdgeInsetsMake(0.0, self.arrowHeight, 0.0, 0.0));
            break;
        case CZPopoverArrowDirectionRight:
            self.contentViewController.view.frame = UIEdgeInsetsInsetRect(backgroundView.bounds, UIEdgeInsetsMake(0.0, 0.0, 0.0, self.arrowHeight));
            break;
    }
    self.contentViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [backgroundView addSubview:self.contentViewController.view];
    
    [self.contentViewController didMoveToParentViewController:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.barButtonItem.button.selected = NO;
    self.toolbarItem.selected = NO;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(popoverControllerWillDismiss:)]) {
        [self.delegate popoverControllerWillDismiss:self];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.barButtonItem = nil;
    self.toolbarItem = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(popoverControllerDidDismiss:)]) {
        [self.delegate popoverControllerDidDismiss:self];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGFloat width = self.contentViewController.view.bounds.size.width;
    CGFloat height = self.contentViewController.view.bounds.size.height;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, width, height) cornerRadius:self.cornerRadius];
    
    // Border will extend to both inside and outside, so only subtract ONE borderWidth is sufficient
    CGFloat sx = (width - self.borderWidth) / width;
    CGFloat sy = (height - self.borderWidth) / height;
    CGAffineTransform t = CGAffineTransformMakeScale(sx, sy);
    t = CGAffineTransformTranslate(t, self.borderWidth / 2, self.borderWidth / 2);
    [path applyTransform:t];
    
    CAShapeLayer *mask = [CAShapeLayer layer];
    mask.path = path.CGPath;
    mask.fillColor = [[UIColor blackColor] CGColor];
    self.contentViewController.view.layer.mask = mask;
}

#pragma mark - Public

- (void)presentAnimated:(BOOL)animated completion:(void (^)(void))completion {
    UIViewController *frontmostPresentedViewController = UIApplication.sharedApplication.keyWindow.cz_frontmostPresentedViewController;
    [frontmostPresentedViewController presentViewController:self animated:animated completion:completion];
}

- (void)presentFromBarButtonItem:(CZBarButtonItem *)item animated:(BOOL)animated completion:(void (^)(void))completion {
    self.barButtonItem = item;
    self.barButtonItem.button.selected = YES;
    
    self.sourceView = self.barButtonItem.button;
    self.sourceRect = CGRectOffset(self.barButtonItem.button.bounds, 0.0, 12.0);
    self.arrowDirection = CZPopoverArrowDirectionUp;
    
    UIViewController *frontmostPresentedViewController = UIApplication.sharedApplication.keyWindow.cz_frontmostPresentedViewController;
    [frontmostPresentedViewController presentViewController:self animated:animated completion:completion];
}

- (void)presentFromToolbarItem:(CZToolbarItem *)item animated:(BOOL)animated completion:(void (^)(void))completion {
    self.toolbarItem = item;
    self.toolbarItem.selected = YES;
    
    self.sourceView = self.toolbarItem.view;
    self.sourceRect = CGRectOffset(self.toolbarItem.view.bounds, 0.0, -12.0);
    self.arrowDirection = CZPopoverArrowDirectionDown;
    
    UIViewController *frontmostPresentedViewController = UIApplication.sharedApplication.keyWindow.cz_frontmostPresentedViewController;
    [frontmostPresentedViewController presentViewController:self animated:animated completion:completion];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    NSAssert(self.sourceView != nil, @"Source view MUST not be nil.");
    
    presented.preferredContentSize = CGSizeMake(self.contentViewController.preferredContentSize.width, self.contentViewController.preferredContentSize.height + self.arrowHeight);
    
    CZPopoverPresentationController *popoverPresentationController = [[CZPopoverPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    popoverPresentationController.sourceView = self.sourceView;
    popoverPresentationController.sourceRect = self.sourceRect;
    popoverPresentationController.arrowDirection = self.arrowDirection;
    popoverPresentationController.passthroughViews = self.passthroughViews;
    popoverPresentationController.popoverLayoutMargins = self.popoverLayoutMargins;
    popoverPresentationController.automaticallyAdjustsFrameWhenKeyboardAppears = self.adjustsPopoverPositionWhenKeyboardAppears;
    return popoverPresentationController;
}

@end
