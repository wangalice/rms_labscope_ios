//
//  CZPopoverPresentationController.m
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZPopoverPresentationController.h"
#import "CZPopoverBackgroundView.h"

@interface CZPopoverPresentationController () <UIGestureRecognizerDelegate>

@property (nonatomic, readonly, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation CZPopoverPresentationController

@synthesize tapGestureRecognizer = _tapGestureRecognizer;

- (CGRect)frameOfPresentedViewInContainerView {
    CGRect sourceFrame = [self.sourceView convertRect:self.sourceRect toView:self.sourceView.window];
    CGSize contentSize = self.presentedViewController.preferredContentSize;
    CGSize containerSize = self.containerView.bounds.size;
    
    CGRect viewFrame = CGRectZero;
    viewFrame.size = contentSize;
    
    switch (self.arrowDirection) {
        case CZPopoverArrowDirectionUp:
            viewFrame.origin.x = CGRectGetMidX(sourceFrame) - contentSize.width / 2;
            viewFrame.origin.y = CGRectGetMaxY(sourceFrame);
            viewFrame.size.height = MIN(viewFrame.size.height, containerSize.height - CGRectGetMaxY(sourceFrame) - self.popoverLayoutMargins.bottom);
            break;
        case CZPopoverArrowDirectionDown:
            viewFrame.origin.x = CGRectGetMidX(sourceFrame) - contentSize.width / 2;
            viewFrame.origin.y = CGRectGetMinY(sourceFrame) - contentSize.height;
            viewFrame.size.height = MIN(viewFrame.size.height, CGRectGetMinY(sourceFrame) - self.popoverLayoutMargins.top);
            break;
        case CZPopoverArrowDirectionLeft:
            viewFrame.origin.x = CGRectGetMaxX(sourceFrame);
            viewFrame.origin.y = CGRectGetMidY(sourceFrame) - contentSize.height / 2;
            viewFrame.size.width = MIN(viewFrame.size.width, containerSize.width - CGRectGetMaxX(sourceFrame) - self.popoverLayoutMargins.right);
            break;
        case CZPopoverArrowDirectionRight:
            viewFrame.origin.x = CGRectGetMinX(sourceFrame) - contentSize.width;
            viewFrame.origin.y = CGRectGetMidY(sourceFrame) - contentSize.height / 2;
            viewFrame.size.width = MIN(viewFrame.size.width, CGRectGetMinX(sourceFrame) - self.popoverLayoutMargins.left);
            break;
    }
    
    
    CGFloat maxWidth = containerSize.width - self.popoverLayoutMargins.left - self.popoverLayoutMargins.right;
    CGFloat maxHeight = containerSize.height - self.popoverLayoutMargins.top - self.popoverLayoutMargins.bottom;
    
    if (CGRectGetWidth(viewFrame) > maxWidth) {
        viewFrame.size.width = maxWidth;
    }
    
    if (CGRectGetMinX(viewFrame) < self.popoverLayoutMargins.left) {
        viewFrame.origin.x = self.popoverLayoutMargins.left;
    }
    
    if (CGRectGetMaxX(viewFrame) > containerSize.width - self.popoverLayoutMargins.right) {
        viewFrame.origin.x = containerSize.width - self.popoverLayoutMargins.right - viewFrame.size.width;
    }
    
    if (CGRectGetHeight(viewFrame) > maxHeight) {
        viewFrame.size.height = maxHeight;
    }
    
    if (CGRectGetMinY(viewFrame) < self.popoverLayoutMargins.top) {
        viewFrame.origin.y = self.popoverLayoutMargins.top;
    }
    
    if (CGRectGetMaxY(viewFrame) > containerSize.height - self.popoverLayoutMargins.bottom) {
        viewFrame.origin.y = containerSize.height - self.popoverLayoutMargins.bottom - viewFrame.size.height;
    }
    
    return viewFrame;
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.containerView addGestureRecognizer:self.tapGestureRecognizer];
        
        if (self.automaticallyAdjustsFrameWhenKeyboardAppears) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        }
    }
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.containerView removeGestureRecognizer:self.tapGestureRecognizer];
        
        if (self.automaticallyAdjustsFrameWhenKeyboardAppears) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        }
    }
}

#pragma mark - Tap Gesture Recognizer

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (_tapGestureRecognizer == nil) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        _tapGestureRecognizer.delegate = self;
    }
    return _tapGestureRecognizer;
}

- (void)tapGestureRecognizerAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint locationInPresentedView = [touch locationInView:self.presentedView];
    if (CGRectContainsPoint(self.presentedView.bounds, locationInPresentedView)) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - Keyboard Notification

- (void)keyboardWillShow:(NSNotification *)note {
    CGRect frameEnd = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationOptions animationCurve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    
    [UIView animateWithDuration:animationDuration delay:0.0 options:animationCurve animations:^{
        CGRect frame = [self frameOfPresentedViewInContainerView];
        frame.origin.y = self.containerView.bounds.size.height - frameEnd.size.height - 4.0 - frame.size.height;
        self.presentedView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSTimeInterval animationDuration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationOptions animationCurve = [note.userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    
    [UIView animateWithDuration:animationDuration delay:0.0 options:animationCurve animations:^{
        self.presentedView.frame = [self frameOfPresentedViewInContainerView];
    } completion:^(BOOL finished) {
        
    }];
}

@end
