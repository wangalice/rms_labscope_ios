//
//  CZPopoverBackgroundView.h
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZPopoverArrowDirection.h"

@interface CZPopoverBackgroundView : UIView

@property (nonatomic, strong) UIView *sourceView;
@property (nonatomic, assign) CGRect sourceRect;

@property (nonatomic, assign) CGFloat arrowBase;
@property (nonatomic, assign) CGFloat arrowHeight;
@property (nonatomic, assign) CZPopoverArrowDirection arrowDirection;

@property (nonatomic, strong) UIColor *color;   // Background color
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, assign) CGFloat cornerRadius;

@end
