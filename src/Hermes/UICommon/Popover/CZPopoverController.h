//
//  CZPopoverController.h
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZPopoverArrowDirection.h"

extern const CGFloat CZPopoverDefaultLayoutMargin;

@class CZToolbarItem;
@class CZPopoverController;

@protocol CZPopoverControllerDelegate <NSObject>

@optional
- (void)popoverControllerWillDismiss:(CZPopoverController *)popoverController;
- (void)popoverControllerDidDismiss:(CZPopoverController *)popoverController;

@end

@interface CZPopoverController : UIViewController

@property (nonatomic, readonly, strong) __kindof UIViewController *contentViewController;
@property (nonatomic, weak) id<CZPopoverControllerDelegate> delegate;

@property (nonatomic, strong) UIView *sourceView;
@property (nonatomic, assign) CGRect sourceRect;

@property (nonatomic, assign) CGFloat arrowBase;                        ///< 21.0 as default
@property (nonatomic, assign) CGFloat arrowHeight;                      ///< 12.0 as default
@property (nonatomic, assign) CZPopoverArrowDirection arrowDirection;

@property (nonatomic, copy) NSArray<UIView *> *passthroughViews;        ///< Currently not be implemented

@property (nonatomic, strong) UIColor *backgroundColor;                 ///< gs100 as default
@property (nonatomic, strong) UIColor *borderColor;                     ///< nil as default
@property (nonatomic, assign) CGFloat borderWidth;                      ///< 0.0 as default
@property (nonatomic, assign) CGFloat cornerRadius;                     ///< 8.0 as default

@property (nonatomic, assign) UIEdgeInsets popoverLayoutMargins;        /// (16.0, 16.0, 16.0, 16.0) as default

@property (nonatomic, assign) BOOL adjustsPopoverPositionWhenKeyboardAppears;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithContentViewController:(UIViewController *)contentViewController NS_DESIGNATED_INITIALIZER;

- (void)presentAnimated:(BOOL)animated completion:(void (^)(void))completion;
- (void)presentFromBarButtonItem:(CZBarButtonItem *)item animated:(BOOL)animated completion:(void (^)(void))completion;
- (void)presentFromToolbarItem:(CZToolbarItem *)item animated:(BOOL)animated completion:(void (^)(void))completion;

@end
