//
//  CZPopoverArrowDirection.h
//  Labscope
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZPopoverArrowDirection) {
    CZPopoverArrowDirectionUp,
    CZPopoverArrowDirectionDown,
    CZPopoverArrowDirectionLeft,
    CZPopoverArrowDirectionRight
};
