//
//  CZLogPreviewViewController.h
//  Hermes
//
//  Created by Li, Junlin on 6/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZLogPreviewViewController : UIViewController

- (instancetype)initWithLogFile:(NSString *)path;

@end
