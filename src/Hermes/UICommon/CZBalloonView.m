//
//  CZBalloonView.m
//  Hermes
//
//  Created by Ralph Jin on 7/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZBalloonView.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZUIDefinition.h"

static const NSUInteger kMaxLineCount = 1000;
static const NSUInteger kArrowThickness = 14;
static const CGFloat kBooloonFontSize = 16;
static NSString * const kDefaultFontName = @"HelveticaNeue-BoldItalic";

@interface CZBalloonView ()

@property(nonatomic, retain) UILabel *textLabel;
@property(nonatomic, retain) UIImageView *arrowImageView;

@end

@implementation CZBalloonView

+ (CGSize)calculateSizeOfText:(NSString *)helpText thatFits:(CGSize)size {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    label.numberOfLines = kMaxLineCount;
    label.font = [UIFont fontWithName:kDefaultFontName size:kBooloonFontSize];
    label.text = helpText;
    size = [label sizeThatFits:size];
    
    [label release];
    return size;
}

- (id)initWithFrame:(CGRect)frame direction:(CZBalloonDirection)direction {
    self = [super initWithFrame:frame];
    if (self) {
        _direction = direction;
    }
    
    return self;
}

+ (CGPoint)calcFromPointOfRect:(CGRect)rect toPoint:(CGPoint)point {
    CGPoint fromPoint;
    
    CGFloat left = CGRectGetMinX(rect);
    CGFloat right = CGRectGetMaxX(rect);
    if (point.x < left) {
        fromPoint.x = left;
    } else if (point.x > right) {
        fromPoint.x = right;
    } else {
        fromPoint.x = CGRectGetMidX(rect);
    }
    
    CGFloat top = CGRectGetMinY(rect);
    CGFloat bottom = CGRectGetMaxY(rect);
    if (point.y < top) {
        fromPoint.y = top;
    } else if (point.y > bottom) {
        fromPoint.y = bottom;
    } else {
        fromPoint.y = CGRectGetMidY(rect);
    }
    
    return fromPoint;
}

- (void)dealloc {
    [_textLabel release];
    [_arrowImageView release];
    [super dealloc];
}

- (void)setHelpText:(NSString *)helpText {
    if (_textLabel == nil) {
        CGRect frame = self.frame;
        frame.origin = CGPointZero;
        
        UILabel *label = [[UILabel alloc] initWithFrame:self.frame];
        label.text = helpText;
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = kMaxLineCount;
        label.font = [UIFont fontWithName:kDefaultFontName size:kBooloonFontSize];
        [self addSubview:label];
        self.textLabel = label;
        [label release];
    } else {
        _textLabel.text = helpText;
    }
    
    CGPoint selfOrigin = self.frame.origin;
    CGRect frame = self.frame;
    frame.origin = CGPointZero;
    CGSize fitSize = [_textLabel sizeThatFits:frame.size];
    frame.size = fitSize;
    _textLabel.frame = frame;
    
    frame.origin = selfOrigin;
    self.frame = frame;
}

- (void)setArrowImage:(UIImage *)arrowImage {
    if (_arrowImageView == nil) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:arrowImage];
        self.arrowImageView = imageView;
        [self addSubview:imageView];
        [imageView release];
    } else {
        _arrowImageView.image = arrowImage;
    }
}

- (void)pointToView:(UIView *)view {
    if (CZBalloonDirectionNone == _direction) {
        [self.arrowImageView removeFromSuperview];
        self.arrowImageView = nil;
        return;
    }
    // calculate view bounds
    CGRect viewFrame = view.frame;
    viewFrame = [view.superview convertRect:viewFrame toView:self];

    // calculate help text bounds
    CGRect helpTextFrame;
    if (_textLabel) {
        helpTextFrame = [_textLabel convertRect:_textLabel.frame toView:self];
    } else {
        helpTextFrame = self.frame;
    }

    // calculate from point
    CGPoint center;
    center.x = CGRectGetMidX(viewFrame);
    center.y = CGRectGetMidY(viewFrame);
    CGPoint pointFromPoint = [CZBalloonView calcFromPointOfRect:helpTextFrame toPoint:center];
    
    if (_direction == CZBalloonDirectionLeft) {
        pointFromPoint.x = CGRectGetMinX(helpTextFrame);
    } else if (_direction == CZBalloonDirectionRight) {
        pointFromPoint.x = CGRectGetMaxX(helpTextFrame);
    } else if (_direction == CZBalloonDirectionUp) {
        pointFromPoint.y = CGRectGetMinY(helpTextFrame);
    } else if (_direction == CZBalloonDirectionDown) {
        pointFromPoint.y = CGRectGetMaxY(helpTextFrame);
    }
    
    // calculate to point
    CGPoint pointToPoint;
    if (_direction == CZBalloonDirectionLeft) {
        pointToPoint.x = CGRectGetMaxX(viewFrame);
        pointToPoint.y = CGRectGetMidY(viewFrame);
        if (pointFromPoint.x < pointToPoint.x) {
            pointFromPoint.x = CGRectGetMidX(helpTextFrame);
        }
    } else if (_direction == CZBalloonDirectionRight) {
        pointToPoint.x = CGRectGetMinX(viewFrame);
        pointToPoint.y = CGRectGetMidY(viewFrame);
        if (pointFromPoint.x > pointToPoint.x) {
            pointFromPoint.x = CGRectGetMidX(helpTextFrame);
        }
    } else if (_direction == CZBalloonDirectionUp) {
        pointToPoint.x = CGRectGetMidX(viewFrame);
        pointToPoint.y = CGRectGetMaxY(viewFrame);
        if (pointFromPoint.y < pointToPoint.y) {
            pointFromPoint.y = CGRectGetMidY(helpTextFrame);
        }
    } else if (_direction == CZBalloonDirectionDown) {
        pointToPoint.x = CGRectGetMidX(viewFrame);
        pointToPoint.y = CGRectGetMinY(viewFrame);
        if (pointFromPoint.y > pointToPoint.y) {
            pointFromPoint.y = CGRectGetMidY(helpTextFrame);
        }
    } else {
        pointToPoint = [CZBalloonView calcFromPointOfRect:viewFrame toPoint:pointFromPoint];
    }
    
    [self refreshArrowWithFromPoint:pointFromPoint toPoint:pointToPoint];
}

- (void)pointToPoint:(CGPoint)point {
    // calculate help text bounds
    CGRect helpTextFrame;
    if (_textLabel) {
        helpTextFrame = [_textLabel convertRect:_textLabel.frame toView:self];
    } else {
        helpTextFrame = self.frame;
    }
    
    CGPoint toPoint = [self.superview convertPoint:point toView:self];
    
    // calculate from point
    CGPoint fromPoint = [CZBalloonView calcFromPointOfRect:helpTextFrame toPoint:toPoint];
    
    [self refreshArrowWithFromPoint:fromPoint toPoint:toPoint];
}

- (void)refreshArrowWithFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint {
    if (_arrowImageView == nil) {
        CZLogv(@"No arrow image view, failed to point to view");
        return;
    }
    
    CGFloat dx = toPoint.x - fromPoint.x;
    CGFloat dy = toPoint.y - fromPoint.y;
    CGFloat length = sqrt(dx * dx + dy * dy);
    if (length < 1.0f) {
        length = 1.0f;
    }
    
    CGPoint center;
    center.x = (fromPoint.x + toPoint.x) / 2;
    center.y = (fromPoint.y + toPoint.y) / 2;
    
    const CGFloat imageHeight = kArrowThickness;
    _arrowImageView.transform = CGAffineTransformIdentity;
    _arrowImageView.frame = CGRectMake(center.x - length / 2, center.y - imageHeight / 2, length, imageHeight);
    _arrowImageView.bounds = CGRectMake(0, 0, length, imageHeight);

    _arrowImageView.transform = CGAffineTransformMakeRotationFromVector(dx, dy);
}

@end
