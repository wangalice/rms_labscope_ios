//
//  CZUppercaseLabel.m
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZUppercaseLabel.h"

@implementation CZUppercaseLabel

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.font = [UIFont cz_label2];
        self.textColor = [UIColor cz_gs80];
        self.highlightedTextColor = [UIColor cz_gs10];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.font = [UIFont cz_label2];
        self.textColor = [UIColor cz_gs80];
        self.highlightedTextColor = [UIColor cz_gs10];
    }
    return self;
}

- (void)setText:(NSString *)text {
    [super setText:text.uppercaseString];
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    NSMutableAttributedString *uppercaseAttributedText = [[NSMutableAttributedString alloc] initWithString:attributedText.string.uppercaseString];
    [attributedText enumerateAttributesInRange:NSMakeRange(0, attributedText.length) options:0 usingBlock:^(NSDictionary<NSAttributedStringKey,id> *attrs, NSRange range, BOOL *stop) {
        [uppercaseAttributedText addAttributes:attrs range:range];
    }];
    [super setAttributedText:uppercaseAttributedText];
}

@end
