//
//  UINavigationBar+CZHelpOverlayView.m
//  Hermes
//
//  Created by Ding Yu on 2018/8/8.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "UINavigationBar+CZHelpOverlayView.h"

@implementation UINavigationBar(CZHelpOverlayView)

- (UIControl *)findBarButtonItem:(UIBarButtonItem *)barButtonItem {
    // Find all buttons like control in UINavigationBar.
    // The controls are ascending sorted by its frame.origin.x
    NSArray<UIControl *> *controls = [[self class] buttonsForView:self];
    NSMutableArray *items = [NSMutableArray new];
    
    // Add all UIBarButtonItem, from left to right.
    // Also is ascending sorted by its frame.origin.x.
    [items addObjectsFromArray:self.topItem.leftBarButtonItems];
    [items addObjectsFromArray:[[self.topItem.rightBarButtonItems reverseObjectEnumerator] allObjects]];
    
    // Two array should have same size, directly mapping each other
    // The buttons[index] respresnts button for items[index].
    NSUInteger index = [items indexOfObject:barButtonItem];
    return index != NSNotFound ? controls[index] : nil;
}

+ (NSArray<UIControl *> *)buttonsForView:(UIView *)view {
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    [self buttonsForView:view buttons:buttons];
    
    if (@available(iOS 11, *)) {
        // Do nothing
    } else {
        [buttons sortUsingComparator:^NSComparisonResult(UIControl *obj1, UIControl *obj2) {
            if (obj1.frame.origin.x > obj2.frame.origin.x) {
                return NSOrderedDescending;
            } else if (obj1.frame.origin.x < obj2.frame.origin.x) {
                return NSOrderedAscending;
            }
            return NSOrderedSame;
        }];
    }
    return buttons;
}

+ (void)buttonsForView:(UIView *)view buttons:(NSMutableArray<UIView *> *)buttons {
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[UIControl class]]) {
            [buttons addObject:subview];
        } else {
            [self buttonsForView:subview buttons:buttons];
        }
    }
}

@end
