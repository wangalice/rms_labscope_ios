//
//  CZFileEntity+Icon.h
//  Hermes
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZFileKit/CZFileKit.h>

@interface CZFileEntity (Icon)

@property (nonatomic, readonly, strong) CZIcon *icon;

@end

@interface NSString (Icon)

@property (nonatomic, readonly, strong) CZIcon *icon;

@end
