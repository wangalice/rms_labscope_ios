//
//  CZGridElementView.h
//  Hermes
//
//  Created by Li, Junlin on 1/31/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZGridRow;
@class CZGridColumn;
@class CZGridViewLayoutAttributes;

@interface CZGridElementView : UIView

@property (nonatomic, readonly, copy) NSArray<CZGridRow *> *rows;
@property (nonatomic, readonly, copy) NSArray<CZGridColumn *> *columns;
@property (nonatomic, readonly, strong) UIView *contentView;
@property (nonatomic, readonly, strong) CZGridViewLayoutAttributes *layoutAttributes;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)initWithRows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns contentView:(UIView *)contentView layoutAttributes:(CZGridViewLayoutAttributes *)layoutAttributes NS_DESIGNATED_INITIALIZER;

@end
