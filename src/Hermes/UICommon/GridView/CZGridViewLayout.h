//
//  CZGridViewLayout.h
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZGridColumn.h"
#import "CZGridRow.h"

@interface CZGridViewLayoutAttributes : NSObject

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CZGridRowAlignment rowAlignment;
@property (nonatomic, assign) CZGridColumnAlignment columnAlignment;

@end
