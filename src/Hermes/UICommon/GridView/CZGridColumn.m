//
//  CZGridColumn.m
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridColumn.h"

@implementation CZGridColumn

- (instancetype)initWithWidth:(CGFloat)width alignment:(CZGridColumnAlignment)alignment {
    self = [super init];
    if (self) {
        _width = width;
        _alignment = alignment;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] alloc] initWithWidth:self.width alignment:self.alignment];
}

@end
