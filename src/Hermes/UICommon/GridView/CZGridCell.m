//
//  CZGridCell.m
//  Hermes
//
//  Created by Li, Junlin on 3/6/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridCell.h"
#import "CZGridCellPrivate.h"
#import "CZGridView.h"

@interface CZGridCell ()

@property (nonatomic, strong) NSMutableArray<UIView *> *mutableContentViews;

@end

@implementation CZGridCell

- (instancetype)initWithRow:(CZGridRow *)row column:(CZGridColumn *)column {
    self = [super init];
    if (self) {
        _row = row;
        _column = column;
        _mutableContentViews = [NSMutableArray array];
    }
    return self;
}


- (NSArray<UIView *> *)contentViews {
    return [self.mutableContentViews copy];
}

- (void)addContentView:(UIView *)contentView {
    [self.mutableContentViews addObject:contentView];
}

- (void)removeContentView:(UIView *)contentView {
    [self.mutableContentViews removeObject:contentView];
}

- (void)removeAllContentViews {
    [self.mutableContentViews removeAllObjects];
}

- (NSInteger)weightForAxis:(UILayoutConstraintAxis)axis {
    NSInteger rowIndex = [self.gridView indexOfRow:self.row];
    NSInteger columnIndex = [self.gridView indexOfColumn:self.column];
    switch (axis) {
        case UILayoutConstraintAxisHorizontal:
            return self.gridView.numberOfRows * columnIndex + rowIndex;
        case UILayoutConstraintAxisVertical:
            return self.gridView.numberOfColumns * rowIndex + columnIndex;
    }
}

@end
