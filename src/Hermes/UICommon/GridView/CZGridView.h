//
//  CZGridView.h
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZGridRow.h"
#import "CZGridColumn.h"
#import "CZGridCell.h"
#import "CZGridViewLayout.h"

@interface CZGridView : UIView

/** UILayoutConstraintAxisVertical as default value */
@property (nonatomic, assign) UILayoutConstraintAxis axis;
@property (nonatomic, readonly, assign) NSInteger numberOfRows;
@property (nonatomic, readonly, assign) NSInteger numberOfColumns;

/** rows and columns will be deep copied. */
- (instancetype)initWithRows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns;
- (instancetype)initWithFrame:(CGRect)frame rows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns;

- (CZGridRow *)rowAtIndex:(NSInteger)index;
- (NSArray<CZGridRow *> *)rowsAtRange:(NSRange)range;
- (NSInteger)indexOfRow:(CZGridRow *)row;
- (CZGridColumn *)columnAtIndex:(NSInteger)index;
- (NSArray<CZGridColumn *> *)columnsAtRange:(NSRange)range;
- (NSInteger)indexOfColumn:(CZGridColumn *)column;
- (CZGridCell *)cellAtRowIndex:(NSInteger)rowIndex columnIndex:(NSInteger)columnIndex;
- (NSArray<CZGridCell *> *)cellsForContentView:(UIView *)contentView;

- (void)addRow:(CZGridRow *)row;    // To be implemented
- (void)insertRow:(CZGridRow *)row atIndex:(NSInteger)index;    // To be implemented
- (void)moveRowAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex; // To be implemented
- (void)removeRowAtIndex:(NSInteger)index;

- (void)addColumn:(CZGridColumn *)column;   // To be implemented
- (void)insertColumn:(CZGridColumn *)column atIndex:(NSInteger)index;   // To be implemented
- (void)moveColumnAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex;  // To be implemented
- (void)removeColumnAtIndex:(NSInteger)index;   // To be implemented

@property (nonatomic, assign) CGFloat rowSpacing;
@property (nonatomic, assign) CGFloat columnSpacing;
- (void)setCustomSpacing:(CGFloat)spacing afterRowAtIndex:(NSUInteger)rowIndex;
- (void)setCustomSpacing:(CGFloat)spacing afterColumnAtIndex:(NSUInteger)columnIndex;

@property (nonatomic, readonly, copy) NSArray<__kindof UIView *> *contentViews;
- (void)addContentView:(UIView *)contentView;
- (void)addContentView:(UIView *)contentView configurationHandler:(void (^)(CZGridViewLayoutAttributes *layoutAttribute))configurationHandler;
- (void)addContentView:(UIView *)contentView atCell:(CZGridCell *)cell;
- (void)addContentView:(UIView *)contentView atRow:(CZGridRow *)row column:(CZGridColumn *)column;
- (void)addContentView:(UIView *)contentView atRowIndex:(NSUInteger)rowIndex columnIndex:(NSUInteger)columnIndex;
- (void)addContentView:(UIView *)contentView atRowIndex:(NSUInteger)rowIndex columnRange:(NSRange)columnRange;
- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnIndex:(NSUInteger)columnIndex;
- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnRange:(NSRange)columnRange;
- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnRange:(NSRange)columnRange configurationHandler:(void (^)(CZGridViewLayoutAttributes *layoutAttribute))configurationHandler;
- (void)removeContentView:(UIView *)contentView;
- (void)removeAllContentViews;

@end
