//
//  CZGridView.m
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridView.h"
#import "CZGridCellPrivate.h"
#import "CZGridElementView.h"

@interface CZGridView ()

@property (nonatomic, strong) NSMutableArray<CZGridRow *> *rows;
@property (nonatomic, strong) NSMutableArray<CZGridColumn *> *columns;
@property (nonatomic, strong) NSMutableArray<CZGridCell *> *cells;

@property (nonatomic, strong) NSMapTable<CZGridRow *, NSNumber *> *customRowSpacings;
@property (nonatomic, strong) NSMapTable<CZGridColumn *, NSNumber *> *customColumnSpacings;

@property (nonatomic, strong) NSMutableArray<CZGridElementView *> *elementViews;

@end

@implementation CZGridView

- (instancetype)initWithRows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns {
    return [self initWithFrame:CGRectZero rows:rows columns:columns];
}

- (instancetype)initWithFrame:(CGRect)frame rows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _axis = UILayoutConstraintAxisVertical;
        
        _rows = [[NSMutableArray alloc] initWithArray:rows copyItems:YES];
        _columns = [[NSMutableArray alloc] initWithArray:columns copyItems:YES];;
        _cells = [self makeCells];
        [self sortCells];
        
        _customRowSpacings = [NSMapTable strongToStrongObjectsMapTable];
        _customColumnSpacings = [NSMapTable strongToStrongObjectsMapTable];
        
        _elementViews = [NSMutableArray array];
    }
    return self;
}

- (CGSize)intrinsicContentSize {
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    
    for (NSUInteger rowIndex = 0; rowIndex < self.rows.count; rowIndex++) {
        CZGridRow *row = self.rows[rowIndex];
        height += row.height;
        if (rowIndex != self.rows.count - 1) {
            height += [self spacingAfterRow:row];
        }
    }
    
    for (NSUInteger columnIndex = 0; columnIndex < self.columns.count; columnIndex++) {
        CZGridColumn *column = self.columns[columnIndex];
        width += column.width;
        if (columnIndex != self.columns.count - 1) {
            width += [self spacingAfterColumn:column];
        }
    }
    
    return CGSizeMake(width, height);
}

- (void)setAxis:(UILayoutConstraintAxis)axis {
    _axis = axis;
    [self sortCells];
}

- (NSInteger)numberOfRows {
    return self.rows.count;
}

- (NSInteger)numberOfColumns {
    return self.columns.count;
}

#pragma mark - Find Rows, Columns, Cells

- (CZGridRow *)rowAtIndex:(NSInteger)index {
    if (index >= 0 && index < self.rows.count) {
        return self.rows[index];
    } else {
        return nil;
    }
}

- (NSArray<CZGridRow *> *)rowsAtRange:(NSRange)range {
    NSMutableArray<CZGridRow *> *rows = [NSMutableArray arrayWithCapacity:range.length];
    for (NSInteger index = range.location; index < NSMaxRange(range); index++) {
        CZGridRow *row = [self rowAtIndex:index];
        if (row) {
            [rows addObject:row];
        }
    }
    return [rows copy];
}

- (NSInteger)indexOfRow:(CZGridRow *)row {
    if (row) {
        return [self.rows indexOfObject:row];
    } else {
        return NSNotFound;
    }
}

- (CZGridColumn *)columnAtIndex:(NSInteger)index {
    if (index >= 0 && index < self.columns.count) {
        return self.columns[index];
    } else {
        return nil;
    }
}

- (NSArray<CZGridColumn *> *)columnsAtRange:(NSRange)range {
    NSMutableArray<CZGridColumn *> *columns = [NSMutableArray arrayWithCapacity:range.length];
    for (NSInteger index = range.location; index < NSMaxRange(range); index++) {
        CZGridColumn *column = [self columnAtIndex:index];
        if (column) {
            [columns addObject:column];
        }
    }
    return [columns copy];
}

- (NSInteger)indexOfColumn:(CZGridColumn *)column {
    if (column) {
        return [self.columns indexOfObject:column];
    } else {
        return NSNotFound;
    }
}

- (CZGridCell *)cellAtRowIndex:(NSInteger)rowIndex columnIndex:(NSInteger)columnIndex {
    CZGridRow *row = [self rowAtIndex:rowIndex];
    CZGridColumn *column = [self columnAtIndex:columnIndex];
    for (CZGridCell *cell in self.cells) {
        if (cell.column == column && cell.row == row) {
            return cell;
        }
    }
    return nil;
}

- (NSArray<CZGridCell *> *)cellsForContentView:(UIView *)contentView {
    NSMutableArray<CZGridCell *> *cells = [NSMutableArray array];
    for (CZGridCell *cell in self.cells) {
        if ([cell.contentViews containsObject:contentView]) {
            [cells addObject:cell];
        }
    }
    return [cells copy];
}

#pragma mark - Edit Rows

- (void)addRow:(CZGridRow *)row {
    
}

- (void)insertRow:(CZGridRow *)row atIndex:(NSInteger)index {
    
}

- (void)moveRowAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    
}

- (void)removeRowAtIndex:(NSInteger)index {
    if (index < 0 || index >= self.rows.count) {
        return;
    }
    
    CZGridRow *row = [self rowAtIndex:index];
    CGFloat rowHeight = row.height + [self spacingAfterRow:row];
    [self.rows removeObjectAtIndex:index];
    [self.customRowSpacings removeObjectForKey:row];
    
    NSMutableArray<CZGridCell *> *removedCells = [NSMutableArray arrayWithCapacity:self.columns.count];
    NSMutableArray<UIView *> *removedContentViews = [NSMutableArray array];
    for (CZGridCell *cell in self.cells) {
        if (cell.row == row) {
            [removedCells addObject:cell];
            [removedContentViews addObjectsFromArray:cell.contentViews];
        }
    }
    [self.cells removeObjectsInArray:removedCells];
    
    NSMutableArray<CZGridElementView *> *removedElementViews = [NSMutableArray array];
    for (CZGridElementView *elementView in self.elementViews) {
        if ([removedContentViews containsObject:elementView.contentView]) {
            [removedElementViews addObject:elementView];
        }
    }
    [removedElementViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.elementViews removeObjectsInArray:removedElementViews];
    
    for (CZGridElementView *elementView in self.elementViews) {
        NSInteger rowIndex = [self indexOfRow:elementView.rows.firstObject];
        if (rowIndex < index) {
            continue;
        }
        CGRect frame = elementView.frame;
        frame.origin.y -= rowHeight;
        elementView.frame = frame;
    }
}

#pragma mark - Edit Columns

- (void)addColumn:(CZGridColumn *)column {
    
}

- (void)insertColumn:(CZGridColumn *)column atIndex:(NSInteger)index {
    
}

- (void)moveColumnAtIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    
}

- (void)removeColumnAtIndex:(NSInteger)index {
    
}


#pragma mark - Set Custom Spacing

- (void)setCustomSpacing:(CGFloat)spacing afterRowAtIndex:(NSUInteger)rowIndex {
    CZGridRow *row = [self rowAtIndex:rowIndex];
    [self.customRowSpacings setObject:@(spacing) forKey:row];
}

- (void)setCustomSpacing:(CGFloat)spacing afterColumnAtIndex:(NSUInteger)columnIndex {
    CZGridColumn *column = [self columnAtIndex:columnIndex];
    [self.customColumnSpacings setObject:@(spacing) forKey:column];
}

#pragma mark - Manage Content Views

- (NSArray<UIView *> *)contentViews {
    NSMutableArray<UIView *> *contentViews = [NSMutableArray arrayWithCapacity:self.elementViews.count];
    for (CZGridElementView *elementView in self.elementViews) {
        [contentViews addObject:elementView.contentView];
    }
    return [contentViews copy];
}

- (void)addContentView:(UIView *)contentView {
    [self addContentView:contentView configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView configurationHandler:(void (^)(CZGridViewLayoutAttributes *))configurationHandler {
    CZGridCell *emptyCell = [self findEmptyCell];
    if (emptyCell) {
        [self addContentView:contentView atCell:emptyCell configurationHandler:configurationHandler];
    }
}

- (void)addContentView:(UIView *)contentView atCell:(CZGridCell *)cell {
    [self addContentView:contentView atRow:cell.row column:cell.column];
}

- (void)addContentView:(UIView *)contentView atCell:(CZGridCell *)cell configurationHandler:(void (^)(CZGridViewLayoutAttributes *))configurationHandler {
    [self addContentView:contentView atRow:cell.row column:cell.column configurationHandler:configurationHandler];
}

- (void)addContentView:(UIView *)contentView atRow:(CZGridRow *)row column:(CZGridColumn *)column {
    [self addContentView:contentView atRow:row column:column configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView atRow:(CZGridRow *)row column:(CZGridColumn *)column configurationHandler:(void (^)(CZGridViewLayoutAttributes *))configurationHandler {
    NSInteger rowIndex = [self indexOfRow:row];
    NSInteger columnIndex = [self indexOfColumn:column];
    if (rowIndex != NSNotFound && columnIndex != NSNotFound) {
        [self addContentView:contentView atRowRange:NSMakeRange(rowIndex, 1) columnRange:NSMakeRange(columnIndex, 1) configurationHandler:configurationHandler];
    }
}

- (void)addContentView:(UIView *)contentView atRowIndex:(NSUInteger)rowIndex columnIndex:(NSUInteger)columnIndex {
    [self addContentView:contentView atRowRange:NSMakeRange(rowIndex, 1) columnRange:NSMakeRange(columnIndex, 1) configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView atRowIndex:(NSUInteger)rowIndex columnRange:(NSRange)columnRange {
    [self addContentView:contentView atRowRange:NSMakeRange(rowIndex, 1) columnRange:columnRange configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnIndex:(NSUInteger)columnIndex {
    [self addContentView:contentView atRowRange:rowRange columnRange:NSMakeRange(columnIndex, 1) configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnRange:(NSRange)columnRange {
    [self addContentView:contentView atRowRange:rowRange columnRange:columnRange configurationHandler:nil];
}

- (void)addContentView:(UIView *)contentView atRowRange:(NSRange)rowRange columnRange:(NSRange)columnRange configurationHandler:(void (^)(CZGridViewLayoutAttributes *))configurationHandler {
    CZGridViewLayoutAttributes *layoutAttributes = [self layoutAttributesForElementAtColumnRange:columnRange rowRange:rowRange];
    if (configurationHandler) {
        configurationHandler(layoutAttributes);
    }
    
    NSArray<CZGridRow *> *rows = [self rowsAtRange:rowRange];
    NSArray<CZGridColumn *> *columns = [self columnsAtRange:columnRange];
    CZGridElementView *elementView = [[CZGridElementView alloc] initWithRows:rows columns:columns contentView:contentView layoutAttributes:layoutAttributes];
    [self addSubview:elementView];
    [self.elementViews addObject:elementView];
    
    [self enumerateCellsWithRowRange:rowRange columnRange:columnRange block:^(CZGridCell *cell) {
        [cell addContentView:contentView];
    }];
}

- (void)removeContentView:(UIView *)contentView {
    for (CZGridElementView *elementView in self.elementViews) {
        if (elementView.contentView == contentView) {
            [elementView removeFromSuperview];
            [self.elementViews removeObject:elementView];
            
            NSArray<CZGridCell *> *cells = [self cellsForContentView:contentView];
            for (CZGridCell *cell in cells) {
                [cell removeContentView:contentView];
            }
            
            break;
        }
    }
}

- (void)removeAllContentViews {
    [self.elementViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.elementViews removeAllObjects];
    
    for (CZGridCell *cell in self.cells) {
        [cell removeAllContentViews];
    }
}

#pragma mark - Private

- (NSMutableArray<CZGridCell *> *)makeCells {
    NSMutableArray<CZGridCell *> *cells = [NSMutableArray arrayWithCapacity:self.rows.count * self.columns.count];
    for (CZGridRow *row in self.rows) {
        for (CZGridColumn *column in self.columns) {
            CZGridCell *cell = [[CZGridCell alloc] initWithRow:row column:column];
            cell.gridView = self;
            [cells addObject:cell];
        }
    }
    return cells;
}

- (void)sortCells {
    [self.cells sortUsingComparator:^NSComparisonResult(CZGridCell *cell1, CZGridCell *cell2) {
        NSInteger weigth1 = [cell1 weightForAxis:self.axis];
        NSInteger weigth2 = [cell2 weightForAxis:self.axis];
        return [@(weigth1) compare:@(weigth2)];
    }];
}

- (CZGridCell *)findEmptyCell {
    for (CZGridCell *cell in self.cells) {
        if (cell.contentViews.count == 0) {
            return cell;
        }
    }
    return nil;
}

- (void)enumerateCellsWithRowRange:(NSRange)rowRange columnRange:(NSRange)columnRange block:(void (^)(CZGridCell *cell))block {
    for (NSInteger rowIndex = rowRange.location; rowIndex < NSMaxRange(rowRange); rowIndex++) {
        for (NSInteger columnIndex = columnRange.location; columnIndex < NSMaxRange(columnRange); columnIndex++) {
            CZGridCell *cell = [self cellAtRowIndex:rowIndex columnIndex:columnIndex];
            if (block) {
                block(cell);
            }
        }
    }
}

- (CZGridViewLayoutAttributes *)layoutAttributesForElementAtColumnRange:(NSRange)columnRange rowRange:(NSRange)rowRange {
    CZGridViewLayoutAttributes *layoutAttributes = [[CZGridViewLayoutAttributes alloc] init];
    
    CGRect frame = CGRectZero;
    
    for (NSInteger rowIndex = 0; rowIndex < NSMaxRange(rowRange); rowIndex++) {
        CZGridRow *row = [self rowAtIndex:rowIndex];
        CGFloat spacing = [self spacingAfterRow:row];
        if (rowIndex < rowRange.location) {
            frame.origin.y += self.rows[rowIndex].height + spacing;
        } else if (rowIndex == NSMaxRange(rowRange) - 1) {
            frame.size.height += self.rows[rowIndex].height;
        } else {
            frame.size.height += self.rows[rowIndex].height + spacing;
        }
    }
    
    for (NSInteger columnIndex = 0; columnIndex < NSMaxRange(columnRange); columnIndex++) {
        CZGridColumn *column = [self columnAtIndex:columnIndex];
        CGFloat spacing = [self spacingAfterColumn:column];
        if (columnIndex < columnRange.location) {
            frame.origin.x += self.columns[columnIndex].width + spacing;
        } else if (columnIndex == NSMaxRange(columnRange) - 1) {
            frame.size.width += self.columns[columnIndex].width;
        } else {
            frame.size.width += self.columns[columnIndex].width + spacing;
        }
    }
    
    layoutAttributes.frame = frame;
    layoutAttributes.rowAlignment = [self.rows subarrayWithRange:rowRange].firstObject.alignment;
    layoutAttributes.columnAlignment = [self.columns subarrayWithRange:columnRange].firstObject.alignment;
    
    return layoutAttributes;
}

- (CGFloat)spacingAfterRow:(CZGridRow *)row {
    NSNumber *customSpacing = [self.customRowSpacings objectForKey:row];
    if (customSpacing) {
        return customSpacing.floatValue;
    } else {
        return self.rowSpacing;
    }
}

- (CGFloat)spacingAfterColumn:(CZGridColumn *)column {
    NSNumber *customSpacing = [self.customColumnSpacings objectForKey:column];
    if (customSpacing) {
        return customSpacing.floatValue;
    } else {
        return self.columnSpacing;
    }
}

@end
