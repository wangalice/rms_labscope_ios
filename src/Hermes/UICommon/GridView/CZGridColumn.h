//
//  CZGridColumn.h
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CZGridColumnAlignment) {
    CZGridColumnAlignmentFill,
    CZGridColumnAlignmentLeading,
    CZGridColumnAlignmentCenter,
    CZGridColumnAlignmentTrailing
};

@interface CZGridColumn : NSObject <NSCopying>

@property (nonatomic, readonly, assign) CGFloat width;
@property (nonatomic, readonly, assign) CZGridColumnAlignment alignment;

@property (nonatomic, assign) CGFloat leadingMargin;    // To be implemented
@property (nonatomic, assign) CGFloat trailingMargin;   // To be implemented

@property (nonatomic, assign) CGFloat leadingPadding;   // To be implemented
@property (nonatomic, assign) CGFloat trailingPadding;  // To be implemented

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithWidth:(CGFloat)width alignment:(CZGridColumnAlignment)alignment NS_DESIGNATED_INITIALIZER;

@end
