//
//  CZGridRow.h
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CZGridRowAlignment) {
    CZGridRowAlignmentFill,
    CZGridRowAlignmentTop,
    CZGridRowAlignmentCenter,
    CZGridRowAlignmentBottom
};

@interface CZGridRow : NSObject <NSCopying>

@property (nonatomic, readonly, assign) CGFloat height;
@property (nonatomic, readonly, assign) CZGridRowAlignment alignment;

@property (nonatomic, assign) CGFloat topMargin;        // To be implemented
@property (nonatomic, assign) CGFloat bottomMargin;     // To be implemented

@property (nonatomic, assign) CGFloat topPadding;       // To be implemented
@property (nonatomic, assign) CGFloat bottomPadding;    // To be implemented

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithHeight:(CGFloat)height alignment:(CZGridRowAlignment)alignment NS_DESIGNATED_INITIALIZER;

@end
