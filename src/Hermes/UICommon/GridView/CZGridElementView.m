//
//  CZGridElementView.m
//  Hermes
//
//  Created by Li, Junlin on 1/31/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridElementView.h"
#import "CZGridColumn.h"
#import "CZGridRow.h"
#import "CZGridViewLayout.h"

@implementation CZGridElementView

- (instancetype)initWithRows:(NSArray<CZGridRow *> *)rows columns:(NSArray<CZGridColumn *> *)columns contentView:(UIView *)contentView layoutAttributes:(CZGridViewLayoutAttributes *)layoutAttributes {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _rows = rows;
        _columns = columns;
        
        _contentView = contentView;
        [_contentView addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:nil];
        [self addSubview:contentView];
        
        _layoutAttributes = layoutAttributes;
        [self applyLayoutAttributes];
    }
    return self;
}

- (void)dealloc {
    [_contentView removeObserver:self forKeyPath:@"hidden" context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey, id> *)change context:(void *)context {
    self.hidden = [change[NSKeyValueChangeNewKey] boolValue];
}

- (void)applyLayoutAttributes {
    self.frame = self.layoutAttributes.frame;
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    
    switch (self.layoutAttributes.columnAlignment) {
        case CZGridColumnAlignmentFill:
            [self.contentView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
            [self.contentView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
            break;
        case CZGridColumnAlignmentLeading:
            [self.contentView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
            break;
        case CZGridColumnAlignmentCenter:
            [self.contentView.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
            break;
        case CZGridColumnAlignmentTrailing:
            [self.contentView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
            break;
    }
    
    switch (self.layoutAttributes.rowAlignment) {
        case CZGridRowAlignmentFill:
            [self.contentView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
            [self.contentView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
            break;
        case CZGridRowAlignmentTop:
            [self.contentView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
            break;
        case CZGridRowAlignmentCenter:
            [self.contentView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
            break;
        case CZGridRowAlignmentBottom:
            [self.contentView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
            break;
    }
}

@end
