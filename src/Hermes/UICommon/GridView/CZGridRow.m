//
//  CZGridRow.m
//  Hermes
//
//  Created by Li, Junlin on 1/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridRow.h"

@implementation CZGridRow

- (instancetype)initWithHeight:(CGFloat)height alignment:(CZGridRowAlignment)alignment {
    self = [super init];
    if (self) {
        _height = height;
        _alignment = alignment;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] alloc] initWithHeight:self.height alignment:self.alignment];
}

@end
