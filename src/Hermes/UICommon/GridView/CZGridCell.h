//
//  CZGridCell.h
//  Hermes
//
//  Created by Li, Junlin on 3/6/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZGridRow;
@class CZGridColumn;

@interface CZGridCell : NSObject

@property (nonatomic, readonly, strong) CZGridRow *row;
@property (nonatomic, readonly, strong) CZGridColumn *column;
@property (nonatomic, readonly, copy) NSArray<UIView *> *contentViews;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRow:(CZGridRow *)row column:(CZGridColumn *)column NS_DESIGNATED_INITIALIZER;

@end
