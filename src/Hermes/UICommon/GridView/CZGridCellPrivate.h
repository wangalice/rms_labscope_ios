//
//  CZGridCellPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 4/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGridCell.h"

@class CZGridView;

@interface CZGridCell ()

@property (nonatomic, weak) CZGridView *gridView;

- (void)addContentView:(UIView *)contentView;
- (void)removeContentView:(UIView *)contentView;
- (void)removeAllContentViews;

- (NSInteger)weightForAxis:(UILayoutConstraintAxis)axis;

@end
