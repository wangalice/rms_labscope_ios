//
//  CZBulletedListView.m
//  Matscope
//
//  Created by Halley Gu on 3/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZBulletedListView.h"

@interface CZBulletedListView ()

@property (nonatomic, strong) UIStackView *verticalStackView;
@property (nonatomic, strong) NSMutableArray *bulletViews;
@property (nonatomic, strong) NSMutableArray *textViews;

@end

@implementation CZBulletedListView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _bullet = @"•";
        _font = [UIFont cz_body1];
        
        _verticalStackView = [[UIStackView alloc] init];
        _verticalStackView.translatesAutoresizingMaskIntoConstraints = NO;
        _verticalStackView.axis = UILayoutConstraintAxisVertical;
        _verticalStackView.distribution = UIStackViewDistributionFill;
        _verticalStackView.alignment = UIStackViewAlignmentFill;
        _verticalStackView.spacing = 22.0;
        [self addSubview:_verticalStackView];
        [_verticalStackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
        [_verticalStackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
        [_verticalStackView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [_verticalStackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
        
        _bulletViews = [NSMutableArray array];
        _textViews = [NSMutableArray array];
    }
    return self;
}

- (void)setContents:(NSArray *)contents {
    _contents = [contents copy];
    
    NSArray<UIView *> *arrangedSubviews = self.verticalStackView.arrangedSubviews;
    for (UIView *view in arrangedSubviews) {
        [self.verticalStackView removeArrangedSubview:view];
        [view removeFromSuperview];
    }
    [self.bulletViews removeAllObjects];
    [self.textViews removeAllObjects];
    
    for (id content in self.contents) {
        if (![content isKindOfClass:[NSString class]]) {
            continue;
        }
        
        UIStackView *horizontalStackView = [[UIStackView alloc] init];
        horizontalStackView.axis = UILayoutConstraintAxisHorizontal;
        horizontalStackView.distribution = UIStackViewDistributionFill;
        horizontalStackView.alignment = UIStackViewAlignmentTop;
        horizontalStackView.spacing = 9.0;
        
        UILabel *bulletView = [[UILabel alloc] init];
        bulletView.backgroundColor = [UIColor clearColor];
        bulletView.font = self.font;
        bulletView.numberOfLines = 1;
        bulletView.text = self.bullet;
        bulletView.textColor = [UIColor cz_gs50];
        [bulletView.widthAnchor constraintEqualToConstant:7.0].active = YES;
        [self.bulletViews addObject:bulletView];
        [horizontalStackView addArrangedSubview:bulletView];
        
        UILabel *textView = [[UILabel alloc] init];
        textView.backgroundColor = [UIColor clearColor];
        textView.font = self.font;
        textView.lineBreakMode = NSLineBreakByWordWrapping;
        textView.numberOfLines = 0;
        textView.text = (NSString *)content;
        textView.textColor = [UIColor cz_gs50];
        [self.textViews addObject:textView];
        [horizontalStackView addArrangedSubview:textView];
        
        [self.verticalStackView addArrangedSubview:horizontalStackView];
    }
}

- (void)setBullet:(NSString *)bullet {
    _bullet = [bullet copy];
    
    for (UILabel *bulletView in self.bulletViews) {
        bulletView.text = self.bullet;
    }
}

- (void)setFont:(UIFont *)font {
    _font = font;
    
    for (UILabel *bulletView in self.bulletViews) {
        bulletView.font = self.font;
    }
    for (UILabel *textView in self.textViews) {
        textView.font = self.font;
    }
}

@end
