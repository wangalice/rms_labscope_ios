//
//  CZDragButton.m
//  Hermes
//
//  Created by Ralph Jin on 5/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDragButton.h"

#define DRAG_THRESHOLD 10

@interface CZDragButton ()

@property (nonatomic, strong) UIImageView *dragIndicator;

@end

@implementation CZDragButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.multipleTouchEnabled = NO;
        self.exclusiveTouch = YES;
        self.dragEnabled = YES;
        
        self.dragIndicator = [[UIImageView alloc] init];
        self.dragIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        self.dragIndicator.hidden = !self.dragEnabled;
        [self.dragIndicator cz_setImageWithIcon:[CZIcon iconNamed:@"drag-indicator"]];
        [self addSubview:self.dragIndicator];
        
        [self.dragIndicator.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-4.0].active = YES;
        [self.dragIndicator.topAnchor constraintEqualToAnchor:self.topAnchor constant:4.0].active = YES;
    }
    return self;
}

- (void)setDragEnabled:(BOOL)dragEnabled {
    _dragEnabled = dragEnabled;
    self.dragIndicator.hidden = !self.dragEnabled;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    // store the location of the starting touch so we can decide when we've moved far enough to drag
    _touchLocation = [[touches anyObject] locationInView:self];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    if (!self.isDragEnabled) {
        return;
    }
    
    // we want to establish a minimum distance that the touch has to move before it counts as dragging,
    // so that the slight movement involved in a tap doesn't cause the frame to move.
    
    CGPoint newTouchLocation = [[touches anyObject] locationInView:self];
    
    // if we're already dragging, move our frame
    if (dragging) {
        float deltaX = newTouchLocation.x - _touchLocation.x;
        float deltaY = newTouchLocation.y - _touchLocation.y;
        [self moveByOffset:CGPointMake(deltaX, deltaY)];
    }

    // if we're not dragging yet, check if we've moved far enough from the initial point to start
    else if ([CZCommonUtils distanceBetweenPoints:_touchLocation Point:newTouchLocation] > DRAG_THRESHOLD) {
        _touchLocation = newTouchLocation;
        dragging = YES;
        
        if ([_delegate respondsToSelector:@selector(dragButtonStartedTracking:)]) {
            [_delegate dragButtonStartedTracking:self];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];

    if (dragging) {
        if ([_delegate respondsToSelector:@selector(dragButtonStoppedTracking:)]) {
            [_delegate dragButtonStoppedTracking:self];
        }
    
        [self goHome];
        dragging = NO;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    if (dragging) {
        if ([_delegate respondsToSelector:@selector(dragButtonStoppedTracking:)]) {
            [_delegate dragButtonStoppedTracking:self];
        }
        
        [self goHome];
        dragging = NO;
    }
}

- (void)goHome {
    // distance is in pixels
    CGFloat distanceFromHome = [CZCommonUtils distanceBetweenPoints:[self frame].origin Point:[self home].origin];
    // duration is in seconds, so each additional pixel adds only 1/1000th of a second.
    NSTimeInterval animationDuration = 0.1 + distanceFromHome * 0.001;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [self setFrame:[self home]];
    [UIView commitAnimations];
}

- (void)moveByOffset:(CGPoint)offset {
    CGRect frame = [self frame];
    frame.origin.x += offset.x;
    frame.origin.y += offset.y;
    [self setFrame:frame];
    if ([_delegate respondsToSelector:@selector(dragButtonMoved:)]) {
        [_delegate dragButtonMoved:self];
    }
}

@end


