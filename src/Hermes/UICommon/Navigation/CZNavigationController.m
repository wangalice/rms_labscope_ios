//
//  CZNavigationController.m
//  Hermes
//
//  Created by Li, Junlin on 2/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNavigationController.h"

@implementation CZNavigationController

@dynamic navigationBar;

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithNavigationBarClass:[CZNavigationBar class] toolbarClass:nil];
    if (self) {
        self.viewControllers = @[rootViewController];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.interactivePopGestureRecognizer.enabled = NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated  {
    [super pushViewController:viewController animated:animated];
    [self.navigationBar configureForViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    UIViewController *viewController = [super popViewControllerAnimated:animated];
    [self.navigationBar configureForViewController:self.topViewController animated:animated];
    return viewController;
}

- (NSArray<UIViewController *> *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated {
    NSArray<UIViewController *> *viewControllers = [super popToViewController:viewController animated:animated];
    [self.navigationBar configureForViewController:viewController animated:animated];
    return viewControllers;
}

- (NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated {
    NSArray<UIViewController *> *viewControllers = [super popToRootViewControllerAnimated:animated];
    [self.navigationBar configureForViewController:self.viewControllers.firstObject animated:animated];
    return viewControllers;
}

- (void)setViewControllers:(NSArray<UIViewController *> *)viewControllers animated:(BOOL)animated {
    [super setViewControllers:viewControllers animated:animated];
    [self.navigationBar configureForViewController:viewControllers.lastObject animated:animated];
}

@end
