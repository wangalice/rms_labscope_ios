//
//  CZBarButtonItemPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 7/16/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZBarButtonItem.h"

@interface CZBarButtonItem ()

@property (nonatomic, readwrite, weak) CZButton *button;

- (void)buttonAction:(CZButton *)button;

@end
