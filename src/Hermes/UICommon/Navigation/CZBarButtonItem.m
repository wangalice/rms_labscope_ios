//
//  CZBarButtonItem.m
//  Hermes
//
//  Created by Li, Junlin on 7/16/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZBarButtonItem.h"
#import "CZBarButtonItemPrivate.h"

@implementation CZBarButtonItem

- (instancetype)initWithIcon:(CZIcon *)icon target:(id)target action:(SEL)action {
    self = [super initWithTitle:nil style:UIBarButtonItemStylePlain target:target action:action];
    if (self) {
        _icon = icon;
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title target:(id)target action:(SEL)action {
    self = [super initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    if (self) {
    }
    return self;
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    self.button.enabled = enabled;
}

- (void)buttonAction:(CZButton *)button {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:self];
#pragma clang diagnostic pop
}

@end
