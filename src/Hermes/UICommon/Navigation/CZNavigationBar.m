//
//  CZNavigationBar.m
//  Hermes
//
//  Created by Li, Junlin on 7/16/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNavigationBar.h"
#import "CZBarButtonItem.h"
#import "CZBarButtonItemPrivate.h"

const CGFloat CZNavigationBarHeight = 64.0;
const CGFloat CZNavigationBarItemWidth = 64.0;
const CGFloat CZNavigationBarItemHeight = 32.0;

@interface CZNavigationBar ()

@property (nonatomic, readonly, strong) UIView *contentView;
@property (nonatomic, readonly, strong) UIButton *backButton;
@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UIStackView *rightItemsView;

@end

@implementation CZNavigationBar

@synthesize contentView = _contentView;
@synthesize backButton = _backButton;
@synthesize titleLabel = _titleLabel;
@synthesize rightItemsView = _rightItemsView;

+ (void)load {
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0.0, 0.0, 1.0, 1.0)];
    [[CZShapeImage imageWithPath:path fillColor:[UIColor cz_gs105]] setName:@"navigation-bar-background-image"];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIImage *backgroundImage = [[CZShapeImage imageNamed:@"navigation-bar-background-image"] UIImage];
        [self setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
        
        [self addSubview:self.contentView];
        [self.contentView addSubview:self.backButton];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.rightItemsView];
        
        [self.backButton.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:16.0].active = YES;
        [self.backButton.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [self.backButton.heightAnchor constraintEqualToConstant:32.0].active = YES;
        
        [self.titleLabel.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
        [self.titleLabel.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        
        [self.rightItemsView.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-16.0].active = YES;
        [self.rightItemsView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [self.rightItemsView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self bringSubviewToFront:self.contentView];
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize newSize = [super sizeThatFits:size];
    newSize.height = CZNavigationBarHeight;
    return newSize;
}

- (void)configureForViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController loadViewIfNeeded];
    
    [self.backButton removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.backButton addTarget:viewController action:@selector(backBarButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel.text = viewController.navigationItem.title;
    
    NSArray<UIView *> *arrangedSubviews = self.rightItemsView.arrangedSubviews;
    for (UIView *view in arrangedSubviews) {
        [self.rightItemsView removeArrangedSubview:view];
        [view removeFromSuperview];
    }
    for (CZBarButtonItem *item in viewController.navigationItem.rightBarButtonItems) {
        NSAssert([item isKindOfClass:[CZBarButtonItem class]], @"Please use CZBarButtonItem.");
        CZButton *button = [[CZButton alloc] init];
        button.enabled = item.enabled;
        [button.widthAnchor constraintEqualToConstant:CZNavigationBarItemWidth].active = YES;
        [button.heightAnchor constraintEqualToConstant:CZNavigationBarItemHeight].active = YES;
        [button setImage:[item.icon imageForControlWithSize:CZIconSizeSmall] forState:UIControlStateNormal];
        [button setTitle:item.title forState:UIControlStateNormal];
        [button addTarget:item action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        item.button = button;
        [self.rightItemsView addArrangedSubview:button];
    }
    
    if (animated) {
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = UINavigationControllerHideShowBarDuration;
        [self.contentView.layer addAnimation:transition forKey:@"fade"];
    }
}

#pragma mark - Views

- (UIView *)contentView {
    if (_contentView == nil) {
        _contentView = [[UIView alloc] initWithFrame:self.bounds];
        _contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _contentView.backgroundColor = [UIColor cz_gs100];
    }
    return _contentView;
}

- (UIButton *)backButton {
    if (_backButton == nil) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.translatesAutoresizingMaskIntoConstraints = NO;
        _backButton.isAccessibilityElement = YES;
        _backButton.accessibilityIdentifier = @"BackButton";
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.image = [[CZIcon iconNamed:@"navigation-bar-back-indicator"] image];
        [_backButton addSubview:imageView];
        [imageView.leadingAnchor constraintEqualToAnchor:_backButton.leadingAnchor constant:4.0].active = YES;
        [imageView.centerYAnchor constraintEqualToAnchor:_backButton.centerYAnchor].active = YES;
        
        UILabel *label = [[UILabel alloc] init];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.font = [UIFont cz_body1];
        label.text = L(@"BACK");
        label.textColor = [UIColor cz_gs20];
        [_backButton addSubview:label];
        [label.leadingAnchor constraintEqualToAnchor:_backButton.leadingAnchor constant:32.0].active = YES;
        [label.trailingAnchor constraintEqualToAnchor:_backButton.trailingAnchor constant:-19.0].active = YES;
        [label.centerYAnchor constraintEqualToAnchor:_backButton.centerYAnchor].active = YES;
    }
    return _backButton;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont cz_h2];
        _titleLabel.textColor = [UIColor cz_gs50];
    }
    return _titleLabel;
}

- (UIStackView *)rightItemsView {
    if (_rightItemsView == nil) {
        _rightItemsView = [[UIStackView alloc] init];
        _rightItemsView.translatesAutoresizingMaskIntoConstraints = NO;
        _rightItemsView.distribution = UIStackViewDistributionFill;
        _rightItemsView.alignment = UIStackViewAlignmentCenter;
        _rightItemsView.spacing = 8.0;
    }
    return _rightItemsView;
}

@end

@implementation UIViewController (Back)

- (void)backBarButtonItemAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
