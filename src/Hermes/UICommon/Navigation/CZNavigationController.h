//
//  CZNavigationController.h
//  Hermes
//
//  Created by Li, Junlin on 2/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZNavigationBar.h"
#import "CZBarButtonItem.h"

/// In the CZNavigationController implementation, we have override the pushViewController:animated: method to add a back bar button item for every pushed view controller, and after tap the back bar button, the category method backBarButtonItemAction: on UIViewController will be called. The default implementation of backBarButtonItemAction: will just pop the view controller, feel free to override this method in your own view controller subclass to provide custom implementation.
/// All navigation item properties should be set in view controller's viewDidLoad, if you change the properties afterward, you should call navigation bar's configureForViewController:animated: manually.
/// Each item added to navigation bar's right items should be an instance of CZBarButtonItem.
@interface CZNavigationController : UINavigationController

@property (nonatomic, readonly, strong) CZNavigationBar *navigationBar;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithNavigationBarClass:(Class)navigationBarClass toolbarClass:(Class)toolbarClass NS_UNAVAILABLE;
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController NS_DESIGNATED_INITIALIZER;

@end
