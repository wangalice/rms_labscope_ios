//
//  CZNavigationBar.h
//  Hermes
//
//  Created by Li, Junlin on 7/16/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZNavigationBar : UINavigationBar

- (void)configureForViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end

@interface UIViewController (Back)

- (void)backBarButtonItemAction:(id)sender;

@end
