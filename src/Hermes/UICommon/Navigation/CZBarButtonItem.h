//
//  CZBarButtonItem.h
//  Hermes
//
//  Created by Li, Junlin on 7/16/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZBarButtonItem : UIBarButtonItem

@property (nonatomic, readonly, weak) CZButton *button;
@property (nonatomic, readonly, strong) CZIcon *icon;

- (instancetype)initWithIcon:(CZIcon *)icon target:(id)target action:(SEL)action;
- (instancetype)initWithTitle:(NSString *)title target:(id)target action:(SEL)action;

@end
