//
//  CZButtonGroup.h
//  Hermes
//
//  Created by Ralph Jin on 7/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CZButtonGroupDelegate;

typedef NS_ENUM(NSUInteger, CZButtonGroupType) {
    CZButtonGroupTypeRadio,
    CZButtonGroupTypeCheck
};


@interface CZButtonGroup : UIView

@property (nonatomic, assign) id<CZButtonGroupDelegate> delegate;
@property (nonatomic, assign) NSUInteger sectionCount;
@property (nonatomic, assign) NSUInteger maxNumOfSelections;
@property (nonatomic, assign, getter = isDisableAll) BOOL disableAll;
@property (nonatomic, assign) CGFloat buttonHeight;

/**
 * @prama type, @see CZButtonGroupType
 * @prama options, option labels as NSString, UIImage array.
 */
- (id)initWithType:(CZButtonGroupType)type options:(NSArray *)options;

- (BOOL)isMultiSelection;

- (void)selectOptionAtIndex:(NSUInteger)index;
- (void)deselectOptionAtIndex:(NSUInteger)index;
- (void)clearSelection;
- (NSUInteger)selectionCount;
- (BOOL)isIndexSelected:(NSUInteger)index;
- (void)setEnabled:(BOOL)enabled atIndex:(NSUInteger)index;

- (UIButton *)buttonAtIndex:(NSUInteger)index;

@end

@protocol CZButtonGroupDelegate <NSObject>

@optional
- (void)buttonGroup:(CZButtonGroup *)buttonGroup didSelectIndex:(NSUInteger)index;
- (void)buttonGroup:(CZButtonGroup *)buttonGroup didDeselectIndex:(NSUInteger)index;
- (void)buttonGroupWillShowWarningMessage:(CZButtonGroup *)buttonGroup;

@end
