//
//  CZMagnifierView.h
//  Hermes
//
//  Created by Mike Wang on 2/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CZMagnifierViewMode) {
    CZMagnifierViewModeSinglePoint,
    CZMagnifierViewModeTwoPoints,
};

@interface CZMagnifierView : UIView

@property (nonatomic, retain, readonly) UIImageView *viewToMagnify;

// zoom scale of viewToMagnify, e.g. if image view is inside a scroll view,
// and scroll view's zoom scale shall set to this property.
@property (nonatomic, assign) CGFloat viewToMagnifyScale;

@property (nonatomic, assign) CGFloat magnifyFactor;  // default is 2.0
@property (nonatomic, assign) CGFloat minimumMagnifyFactor;  // default is 1.0;

@property (nonatomic, assign) CGPoint touchPoint;
@property (nonatomic, assign) CGPoint touchPoint2;
@property (nonatomic, assign) CZMagnifierViewMode mode;

/*! @return YES, if view repositioned; othersize NO.*/
- (BOOL)validateTouchPoint:(CGPoint)pt;
- (BOOL)validateTouchPoint:(CGPoint)pt point2:(CGPoint)pt2;

- (void)resetFrame;

@end