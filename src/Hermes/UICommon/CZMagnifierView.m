//
//  CZMagnifierView.h
//  Hermes
//
//  Created by Mike Wang on 2/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMagnifierView.h"
#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSUInteger, CZMagnifierViewPosition) {
    CZMagnifierViewPositionBottom,
    CZMagnifierViewPositionTop
};

@interface CZMagnifierView () {
  @private
    CZMagnifierViewPosition _position;
    UIImageView *_viewToMagnify;
}

@end

@implementation CZMagnifierView

- (id)initWithFrame:(CGRect)frame {
    CGFloat longSize = MAX(frame.size.width, frame.size.height);
    CGFloat width = longSize / 4.0;
    if (self = [super initWithFrame:CGRectMake(0, 0, width, width)]) {
        // Initialization code
		self.backgroundColor = [UIColor blackColor];
        self.center = CGPointMake(frame.origin.x + self.bounds.size.width / 2.0, frame.size.height - self.bounds.size.height / 2.0);
        _position = CZMagnifierViewPositionBottom;
        _viewToMagnifyScale = 1.0;
        _magnifyFactor = 2.0;
        _minimumMagnifyFactor = 1.0;
    }
    return self;
}

- (void)setTouchPoint:(CGPoint)pt {   
    _touchPoint = pt;
}

- (BOOL)validateTouchPoint:(CGPoint)pt {
    if (CGRectContainsPoint(self.bounds, pt)) {
        [self reposition];
        return YES;
    }
    
    return NO;
}

- (BOOL)validateTouchPoint:(CGPoint)pt point2:(CGPoint)pt2 {
    BOOL possibleReposition = YES;
    CGPoint another;
    if (CGRectContainsPoint(self.bounds, pt)) {
        another = pt2;
    } else if (CGRectContainsPoint(self.bounds, pt2)) {
        another = pt;
    } else {
        possibleReposition = NO;
    }
    
    if (possibleReposition) {
        CGRect candidateBounds = self.bounds;
        if (_position == CZMagnifierViewPositionTop) {
            candidateBounds.origin.y = self.superview.bounds.size.height - candidateBounds.size.height;
        } else {
            candidateBounds.origin.y = 0;
        }
        
        if (!CGRectContainsPoint(candidateBounds, another)) {
            [self reposition];
            return YES;
        } else {
            return NO;
        }
    }
    
    return NO;
}

- (void)resetFrame {
    CGRect superViewBounds = self.superview.bounds;
    
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
        CGFloat shortSize = MIN(superViewBounds.size.width, superViewBounds.size.height);
        
        CGRect frame = self.frame;
        frame.size.width = floor(shortSize / 2);
        frame.size.height = frame.size.width;
        frame.origin = CGPointMake(0, superViewBounds.size.height - frame.size.height);
        self.frame = frame;
        
        _position = CZMagnifierViewPositionBottom;
    } else {
        self.center = CGPointMake(self.center.x, superViewBounds.size.height - self.bounds.size.height / 2);
        _position = CZMagnifierViewPositionBottom;
    }
}

- (void)reposition {
    CGRect bounds = self.bounds;
    
    // if covers then flip to the other size
    if (_position == CZMagnifierViewPositionTop) {
        _position = CZMagnifierViewPositionBottom;
        
        self.center = CGPointMake(self.center.x, self.superview.bounds.size.height - bounds.size.height / 2);
    } else {
        _position = CZMagnifierViewPositionTop;
        self.center = CGPointMake(self.center.x, bounds.size.height / 2);
    }
}

- (void)drawRect:(CGRect)rect {
    const CGFloat frameWidth = self.frame.size.width;
    const CGFloat frameHeight = self.frame.size.height;

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (_mode == CZMagnifierViewModeSinglePoint) {
        [self drawMagnifyImageInRect:CGRectMake(0, 0, frameWidth, frameHeight) feferencePoint:_touchPoint];
    } else if (_mode == CZMagnifierViewModeTwoPoints) {
        CGFloat dx = fabs(_touchPoint.x - _touchPoint2.x);
        CGFloat dy = fabs(_touchPoint.y - _touchPoint2.y);
        
        if (dx >= dy) {
            CGPoint leftPt, rightPt;
            if (_touchPoint.x > _touchPoint2.x) {
                leftPt = _touchPoint2;
                rightPt = _touchPoint;
            } else {
                leftPt = _touchPoint;
                rightPt = _touchPoint2;
            }
            
            [self drawMagnifyImageInRect:CGRectMake(0, 0, frameWidth * 0.5, frameHeight) feferencePoint:leftPt];
            [self drawMagnifyImageInRect:CGRectMake(frameWidth * 0.5, 0, frameWidth * 0.5, frameHeight) feferencePoint:rightPt];
        } else {
            CGPoint topPt, bottomPt;
            if (_touchPoint.y > _touchPoint2.y) {
                topPt = _touchPoint2;
                bottomPt = _touchPoint;
            } else {
                topPt = _touchPoint;
                bottomPt = _touchPoint2;
            }
            
            [self drawMagnifyImageInRect:CGRectMake(0, 0, frameWidth, frameHeight * 0.5) feferencePoint:topPt];
            [self drawMagnifyImageInRect:CGRectMake(0, frameHeight * 0.5, frameWidth, frameHeight * 0.5) feferencePoint:bottomPt];
        }
        
        // draw separator line
        CGContextSaveGState(context);
        CGContextSetLineWidth(context, 4.0);
        //TODO: Advanced features. Pls check it.
        CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
        if (dx >= dy) {
            CGContextMoveToPoint(context, frameWidth * 0.5, 0);
            CGContextAddLineToPoint(context, frameWidth * 0.5, frameHeight);
        } else {
            CGContextMoveToPoint(context, 0, frameHeight * 0.5);
            CGContextAddLineToPoint(context, frameWidth, frameHeight * 0.5);
        }
        CGContextStrokePath(context);
        CGContextRestoreGState(context);
    }

    //TODO: Advanced features. Pls check it.
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    
    const CGFloat kBorderThickness = 5;
    CGContextSetLineWidth(context, kBorderThickness);
    CGContextSetAllowsAntialiasing(context, false);
    CGContextStrokeRect(context, CGRectInset(self.bounds, kBorderThickness * .5, kBorderThickness * .5));
}

- (void)dealloc {
    [_viewToMagnify release];
    [super dealloc];
}

- (UIImageView *)viewToMagnify {
    if (_viewToMagnify == nil) {
        _viewToMagnify = [[UIImageView alloc] init];
    }
    return _viewToMagnify;
}

# pragma mark - Private methods
- (void)drawMagnifyImageInRect:(CGRect)rect feferencePoint:(CGPoint)point {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat magnifyFactor;
    magnifyFactor = self.magnifyFactor * self.viewToMagnifyScale;
    magnifyFactor = MAX(self.minimumMagnifyFactor, magnifyFactor);
    
    CGContextSaveGState(context);
    CGContextClipToRect(context, rect);
    CGContextTranslateCTM(context, CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGContextScaleCTM(context, magnifyFactor, magnifyFactor);
    CGContextTranslateCTM(context, -point.x, -point.y);
    [self.viewToMagnify.layer renderInContext:context];
    CGContextRestoreGState(context);
    
    // draw magnify aim mark
    CGFloat magnifyAimLength = MIN(rect.size.width, rect.size.height);
    magnifyAimLength = floor(magnifyAimLength / 4.0);
    CGFloat halfBoundWidth = CGRectGetMidX(rect);
    CGFloat halfBoundHeight = CGRectGetMidY(rect);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    
    CGPathMoveToPoint(path, NULL, halfBoundWidth - magnifyAimLength, halfBoundHeight);
    CGPathAddLineToPoint(path, NULL, halfBoundWidth - 10.0, halfBoundHeight);
    
    CGPathMoveToPoint(path, NULL, halfBoundWidth + 10.0, halfBoundHeight);
    CGPathAddLineToPoint(path, NULL, halfBoundWidth+magnifyAimLength, halfBoundHeight);
    
    CGPathMoveToPoint(path, NULL, halfBoundWidth, halfBoundHeight - magnifyAimLength);
    CGPathAddLineToPoint(path, NULL, halfBoundWidth, halfBoundHeight - 10.0);
    
    CGPathMoveToPoint(path, NULL, halfBoundWidth, halfBoundHeight + 10.0);
    CGPathAddLineToPoint(path, NULL, halfBoundWidth, halfBoundHeight + magnifyAimLength);
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 0.0, 1.0);
    CGContextSetLineWidth(context, 2.0);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    CGPathRelease(path);
}

@end
