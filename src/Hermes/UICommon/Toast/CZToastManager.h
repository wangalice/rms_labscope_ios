//
//  CZToastManager.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/13.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZToastMessage.h"

extern CGFloat const kCZAnimationForever;

NS_ASSUME_NONNULL_BEGIN

@interface CZToastManager : NSObject

+ (instancetype)sharedManager;

- (void)registerIdenfiers:(NSArray <NSString *> *)identifers;

- (void)showToastMessage:(NSString *)message
              identifier:(NSString *)identifier
            dismissAfter:(NSTimeInterval)dissmissTimeInterval
              sourceRect:(CGRect)sourceRect
                animated:(BOOL)animated;

- (void)showToastMessage:(NSString *)message
              sourceRect:(CGRect)sourceRect;

- (void)dismissToastMessageWithIdentifier:(NSString *)identifer;

- (void)dismissToastMessagesWithIdentifiers:(NSArray <NSString *> *)identifiers;

- (void)dismissAllToastMesseges;

- (BOOL)isShowingToastMessage;

@end

NS_ASSUME_NONNULL_END
