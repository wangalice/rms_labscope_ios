//
//  CZToastMessage.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/13.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToastMessage.h"

static const NSTimeInterval kDefaultToastMessageAnimationDuration = 2.0;

@interface CZToastMessage ()

@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation CZToastMessage

@synthesize sourceRect = _sourceRect;

+ (instancetype)showToastMessage:(NSString *)message
                        fromRect:(CGRect)sourceRect
                        animated:(BOOL)animated {
    CZToastMessage *toast = [[CZToastMessage alloc] initWithMessage:message
                                                           fromRect:sourceRect
                                                           animated:animated];
    [toast showNotificationMessage];
    return toast;
}

- (instancetype)initWithMessage:(NSString *)mesage
                       fromRect:(CGRect)sourceRect
                       animated:(BOOL)animated {
    if (self = [super init]) {
        _message = [mesage copy];
        _sourceRect = sourceRect;
        _showAnimated = animated;
        UIView *rootView = [self rootView];
        [rootView addSubview:self.messageLabel];
    }
    return self;
}

- (void)dealloc {
    CZLogv(@"Works well");
}

#pragma mark - Public methods

- (void)dismissNotificationMessage {
    [UIView animateWithDuration:kDefaultToastMessageAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.messageLabel.hidden = YES;        
    } completion:^(BOOL finished) {
        [self.messageLabel removeFromSuperview];
        [self.messageLabel.layer removeAllAnimations];
        
    }];
}

#pragma mark - Private methods

- (UIView *)rootView {
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    UIViewController *topViewController = [rootViewController presentedViewController];
    if (topViewController == nil) {
        topViewController = rootViewController;
    }
    return topViewController.view;
}

- (void)showNotificationMessage {
    self.messageLabel.text = self.message;
    
    UIView *rootView = [self rootView];
    
    [rootView bringSubviewToFront:self.messageLabel];
    
    CGSize size = [self.messageLabel sizeThatFits:CGSizeMake(400, 1)];
    
    static const CGFloat kMargin = 6;
    size.width += kMargin * 2;
    size.height += kMargin * 2;
    
    if (size.width < 300) {
        size.width = 300;
    }
    CGRect frame = self.messageLabel.frame;
    frame.size = size;
    
    if (UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM()) {
        frame.origin.y = _sourceRect.origin.y - size.height;
    } else {
        frame.origin.y = _sourceRect.origin.y - size.height;
    }
    self.messageLabel.frame = frame;
    
    CGPoint centerPoint = self.messageLabel.center;
    centerPoint.x = rootView.bounds.size.width / 2.0;
    self.messageLabel.center = centerPoint;
    
    if (_showAnimated) {
        [UIView animateWithDuration:kDefaultToastMessageAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.messageLabel.hidden = NO;
        } completion:nil];
    } else {
        self.messageLabel.hidden = NO;
    }
}

#pragma mark - Getters

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _messageLabel.backgroundColor = [UIColor cz_gs100];
        _messageLabel.textColor = [UIColor cz_gs50];
        _messageLabel.font = [UIFont cz_caption];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.hidden = YES;
        _messageLabel.layer.cornerRadius = 4;
        _messageLabel.layer.borderColor = [UIColor cz_gs110].CGColor;
        _messageLabel.layer.borderWidth = 1.0f;
        _messageLabel.layer.masksToBounds = YES;
        _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _messageLabel.numberOfLines = 0;
    }
    return _messageLabel;
}

@end
