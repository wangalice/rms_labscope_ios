//
//  CZToastMessage.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/13.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZToastMessage : NSObject

@property (nonatomic, strong, readonly) UILabel *messageLabel;
@property (nonatomic, assign, readonly) CGRect sourceRect;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, assign) BOOL showAnimated;

/*! Show toastMessage. */
+ (instancetype)showToastMessage:(NSString *)message
                        fromRect:(CGRect)sourceRect
                        animated:(BOOL)animated;

/*! Hide toast message. */
- (void)dismissNotificationMessage;

@end

NS_ASSUME_NONNULL_END
