//
//  CZToastManager.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/13.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToastManager.h"

static CZToastManager *sharedManager;

CGFloat const kCZAnimationForever = -1.0;
static const NSTimeInterval kDefaultDissmissTimeInterval = 5.0;

@interface CZToastManager ()

@property (nonatomic, strong) NSMutableArray *identifiers;
@property (nonatomic, strong) NSMutableDictionary <NSString *, id> *toastDictionary;

@end

@implementation CZToastManager

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CZToastManager alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    if (self = [super init]) {
        _identifiers = [NSMutableArray array];
        _toastDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)dealloc {
    [_identifiers removeAllObjects];
    [_toastDictionary removeAllObjects];
    _identifiers = nil;
    _toastDictionary = nil;
}

- (void)registerIdenfiers:(NSArray <NSString *> *)identifers {
    [self.identifiers addObjectsFromArray:identifers];
}

- (void)showToastMessage:(NSString *)message
              identifier:(NSString *)identifier
           dismissAfter:(NSTimeInterval)dissmissTimeInterval
              sourceRect:(CGRect)sourceRect
                animated:(BOOL)animated {
    CZLogv(@"Show the toast with identifier: %@", identifier);
    if (![self.identifiers containsObject:identifier]) {
        [self.identifiers addObject:identifier];
    }
    if (self.toastDictionary[identifier] == nil) {
        CZToastMessage *toast = [CZToastMessage showToastMessage:message fromRect:sourceRect animated:animated];
        [self.toastDictionary setValue:toast forKey:identifier];
        if (dissmissTimeInterval != kCZAnimationForever) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(dissmissTimeInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [toast dismissNotificationMessage];
                [self.toastDictionary removeObjectForKey:identifier];
            });
        }
    }
}

- (void)showToastMessage:(NSString *)message sourceRect:(CGRect)sourceRect {
    [self showToastMessage:message identifier:message dismissAfter:kDefaultDissmissTimeInterval sourceRect:sourceRect animated:YES];
}

- (void)dismissToastMessageWithIdentifier:(NSString *)identifer {
    CZLogv(@"Dissmiss the toast with identifier: %@", identifer);
    CZToastMessage *toast = self.toastDictionary[identifer];
    if (toast) {
        [toast dismissNotificationMessage];
        [self.toastDictionary removeObjectForKey:identifer];
    }
}

- (void)dismissToastMessagesWithIdentifiers:(NSArray <NSString *> *)identifiers {
    for (NSString *identifier in identifiers) {
        CZToastMessage *toast = self.toastDictionary[identifier];
        if (toast) {
            [toast dismissNotificationMessage];
            [self.toastDictionary removeObjectForKey:identifier];
        }
    }
}

- (void)dismissAllToastMesseges {
    if ([self isShowingToastMessage]) {
        [self dismissToastMessagesWithIdentifiers:self.toastDictionary.allKeys];
    }
}

- (BOOL)isShowingToastMessage {
    if (self.toastDictionary.allValues.count) {
        return YES;
    }
    return NO;
}

@end
