//
//  CZFunctionBar.h
//  Hermes
//
//  Created by Mike Wang on 3/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kFunctionBarWidth 80

#ifdef CZButtonWithLabel
#define kFunctionButtonWidth 72
#define kFunctionButtonHeight 64
#else
#define kFunctionButtonWidth 64
#define kFunctionButtonHeight 56
#endif

#define kFunctionButtonXPadding ((kFunctionBarWidth - kFunctionButtonWidth) / 2.0)
#define kFunctionBarYPadding kFunctionButtonHeight
#define kFunctionButtonIconWidth 28
#define kFunctionButtonIconHeight 28
#define kFunctionButtonIconTop 0
#define kFunctionButtonSpacing 8
#define kFunctionButtonLabelSpacing 8
#define kFunctionButtonLabelTop kFunctionButtonIconHeight + kFunctionButtonLabelSpacing
#define kFunctionButtonLabelBottom 3
#define kFunctionButtonLabelPadding 10
#define kFunctionButtonIconPadding 3
#define kFunctionButtonIconTopInsect -20
#define kFunctionBarBottomPadding 17
#define kFunctionBarTopPadding 21

#define kToggleYesNoButtonWidth 90
#define kToggleYesNoButtonHeight 30

@class CZZoomLevel;

@interface CZFunctionButton : UIButton

- (id)initWithIcon:(UIImage *)icon
      selectedIcon:(UIImage *)selectedIcon
        background:(UIImage *)background
selectedBackground:(UIImage *)selectedBackground
             label:(NSString *)text;

- (id)initWithIcon:(UIImage *)icon
      selectedIcon:(UIImage *)selectedIcon
        background:(UIImage *)background
selectedBackground:(UIImage *)selectedBackground
             label:(NSString *)text
      includeLabel:(BOOL)includeLabel;

@property (nonatomic, assign, readonly) BOOL includeLabel;

@end

@interface CZHalfHeightFunctionButton : CZFunctionButton
@end

@interface CZDoneButton : CZFunctionButton
@end

@interface CZDoneAsButton : CZFunctionButton
@end

@interface CZColorSizeButton : CZFunctionButton
@end

@interface CZZenStyleButton : UIButton

- (id)initWithLabel:(NSString *)label;

@end

@interface CZDisplayButton : UIButton
@end

@interface CZConfigureButton : UIButton

@property (nonatomic, assign, getter = isConfigured) BOOL configured;
@property (nonatomic, retain) UIImage *configuredIcon;
@property (nonatomic, retain) UIImage *notConfiguredIcon;

- (id)initWithStatus:(BOOL)isConfigured;

@end

@interface CZFunctionView : UIView

@property (nonatomic, assign, getter = isHidden) BOOL hidden;

@end

@interface CZFunctionBar : UIView

@property (nonatomic, assign) CGFloat filledHeight;
@property (nonatomic, assign) CGFloat filledHeightFromBottom;
@property (nonatomic, assign) BOOL enable;

- (void)addButton:(UIButton *)button;
- (void)addButtonFromBottom:(UIButton *)button;
- (UIButton *)buttonAtIndex:(int)index;
- (NSUInteger)buttonCount;
- (void)selectButton:(UIButton *)buttonSelect;
- (void)disableAllAndSelect:(UIButton *)buttonSelect;

@end
