//
//  UINavigationBar+CZHelpOverlayView.h
//  Hermes
//
//  Created by Ding Yu on 2018/8/8.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UINavigationBar(CZHelpOverlayView)


/**
 see reference: https://shyngys.com/getting_the_frame_of_uibarbuttonitem
 An note about UINavigationBar in iOS 11
 https://gist.github.com/niw/569b49648fcab22124e1d12c195fe595
 
 Find UIControl in UINavigationBar which represents barButtonItem
 @param barButtonItem reference item to find.
 @return UIControl in UINavigationBar which represents barButtonItem
 */
- (UIControl *)findBarButtonItem:(UIBarButtonItem *)barButtonItem;
@end
