//
//  UIWindow+CZPresentedViewController.m
//  Hermes
//
//  Created by Li, Junlin on 7/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIWindow+CZPresentedViewController.h"

@implementation UIWindow (CZPresentedViewController)

- (UIViewController *)cz_frontmostPresentedViewController {
    UIViewController *viewController = self.rootViewController;
    NSAssert(viewController != nil, @"It's illegal that rootViewController is nil!");
    
    UIViewController *presentedViewController = nil;
    do {
        presentedViewController = viewController.presentedViewController;
        if (presentedViewController) {
            viewController = presentedViewController;
        } else {
            break;
        }
    } while (true);
    
    return viewController;
}

@end
