//
//  CZCameraView.m
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCameraView.h"
#import "CZUIDefinition.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "MBProgressHUD+Hide.h"
#import "CZAppDelegate.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZAlertController.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZVideoTool.h"

NSString * const CZCameraViewWillShowNotification = @"CZCameraViewWillShowNotification";
NSString * const CZCameraViewWillHideNotification = @"CZCameraViewWillHideNotification";

typedef struct _CZCameraModeData {
    CZCameraExposureMode exposureMode;
    NSUInteger gain;
    float exposureTime;
    BOOL isWhiteBalanceLocked;
    uint32_t autoLevel;  // Kappa only
} CZCameraModeData;

// instead of CZCameraView, let CZCameraModeController wait at sub-thread to restore camera mode.
// So that avoid CZCameraView dellocated in sub-thread, which might cause crash. UIView is not thread safe.
@interface CZCameraModeController : NSObject {
    BOOL _cameraModeChanged;
    CZCameraModeData _cameraModeData;
}

@end

@implementation CZCameraModeController

+ (dispatch_queue_t)sharedCameraModeQueue { // queue for protecting _cameraModeChanged and _cameraModeData
    static dispatch_queue_t cameraModeQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        cameraModeQueue = dispatch_queue_create("com.zeisscn.camera.mode", DISPATCH_QUEUE_SERIAL);
    });
    return cameraModeQueue;
}

- (id)init {
    self = [super init];
    if (self) {
        _cameraModeChanged = NO;
    }
    return self;
}

- (void)forceAutoCameraMode:(CZCamera *)camera {
    dispatch_async([CZCameraModeController sharedCameraModeQueue], ^(){
        CZCameraControlLock *controlLock = [[CZCameraControlLock alloc] initWithCamera:camera];
        if (controlLock) {
            // Read and save original camera settings.
            _cameraModeData.exposureMode = [camera exposureMode];
            _cameraModeData.exposureTime = [camera exposureTime];
            _cameraModeData.gain = [camera gain];
            _cameraModeData.isWhiteBalanceLocked = [camera isWhiteBalanceLocked];
            
            // Switch camera to automatic mode for exposure and white balance.
            [camera setExposureMode:kCZCameraExposureAutomatic];
            [camera setIsWhiteBalanceLocked:NO];
            
            if ([camera isKindOfClass:[CZKappaCamera class]]) {
                _cameraModeData.autoLevel = [(CZKappaCamera *)camera readRegister:kGEVRegisterAutomaticLevel];
                const uint32_t kAutoLevelThreshold = [(CZKappaCamera *)camera readDefaultValueForRegister:kGEVRegisterAutomaticLevel] * 4 / 5;  // 80% of default value
                if (_cameraModeData.autoLevel > kAutoLevelThreshold) {
                    [(CZKappaCamera *)camera writeRegisterAsync:kGEVRegisterAutomaticLevel withData:kAutoLevelThreshold];
                }
            }
            
            // Sleep for a while to wait for camera settings being saved.
            sleep(3);
            
            [controlLock release];
            _cameraModeChanged = YES;
        }
    });
}

- (void)restoreCameraMode:(CZCamera *)camera {
    NSAssert(camera, @"camera is nil!");
    
    dispatch_async([CZCameraModeController sharedCameraModeQueue], ^(){
        if (_cameraModeChanged) {
            // If camera mode was changed successfully, try to restore
            // the settings.
            CZCameraControlLock *controlLock = [[CZCameraControlLock alloc] initWithCamera:camera];
            if (controlLock) {
                if (_cameraModeData.exposureMode == kCZCameraExposureManual) {
                    [camera setExposureTime:_cameraModeData.exposureTime];
                }
                [camera setGain:_cameraModeData.gain];
                [camera setIsWhiteBalanceLocked:_cameraModeData.isWhiteBalanceLocked];
                [camera setExposureMode:_cameraModeData.exposureMode];
                
                if ([camera isKindOfClass:[CZKappaCamera class]]) {
                    [(CZKappaCamera *)camera writeRegisterAsync:kGEVRegisterAutomaticLevel
                                                       withData:100/*_cameraModeData.autoLevel*/];
                }
                
                // Sleep for a while to wait for camera settings being saved.
                sleep(3);
                
                [controlLock release];
            }
            _cameraModeChanged = NO;
        }
    });
}

@end

@interface CZCameraView () <CZCameraDelegate> {
    NSObject *_cameraLock;
    BOOL _isSnapping;
    NSUInteger _notifyCount;
}

@property (nonatomic, retain) MBProgressHUD *snappingHUD;
@property (nonatomic, retain) CZVideoTool *videoTool;
@property (nonatomic, retain) NSTimer *reconnectingTimer;
@property (nonatomic, assign) BOOL isReconnecting;
@property (nonatomic, retain) MBProgressHUD *reconnectingHUD;
@property (nonatomic, retain) NSTimer *cameraCheckTimer;
@property (nonatomic, retain, readonly) UILabel *messageLabel;
@property (nonatomic, retain) UIView *shutterAnimationView;
@property (nonatomic, copy) NSString *currentCameraName;
@property (nonatomic, retain) CZCameraModeController *cameraModeController;

@property (atomic, assign, readonly) BOOL isCameraRunning;  // return YES, if camera is playing or playing but paused.

@end

@implementation CZCameraView

@synthesize messageLabel = _messageLabel;

- (id)init {
    self = [super init];
    if (self) {
        [self setUpCameraView];
        _isSnapping = NO;
        _notifyCount = 0;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUpCameraView];
        _isSnapping = NO;
        _notifyCount = 0;
    }
    return self;
}

- (void)setUpCameraView {
    // install notification
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(appWillEnterForeground:)
               name:UIApplicationWillEnterForegroundNotification
             object:nil];
    [nc addObserver:self
           selector:@selector(appDidEnterBackground:)
               name:UIApplicationDidEnterBackgroundNotification
             object:nil];
    [nc addObserver:self
           selector:@selector(notifyCameraStopForSnapping:)
               name:CZCameraStreamingStopNotification
             object:nil];
    [nc postNotificationName:CZCameraViewWillShowNotification
                      object:self];
    
    self.backgroundColor = [UIColor cz_gs105];
    
    NSAssert(_videoTool == nil, @"videoTool already exist.");
    [_videoTool release];
    _videoTool = [[CZVideoTool alloc] init];
    _videoTool.magnifierEnabled = NO;
    [self addSubview:_videoTool.view];
    _videoTool.view.userInteractionEnabled = NO;
    
    _shutterAnimationView = [[UIView alloc] init];
    [_shutterAnimationView setBackgroundColor:[UIColor whiteColor]];
    [_shutterAnimationView setHidden:YES];
    [self addSubview:_shutterAnimationView];
    
    _cameraLock = [[NSObject alloc] init];
    _cameraModeController = [[CZCameraModeController alloc] init];
}

- (void)setCameraViewFrame:(CGRect)frame {
    self.frame = frame;
    self.videoTool.view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.shutterAnimationView.frame = self.bounds;
}

- (void)dealloc {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self
                  name:UIApplicationWillEnterForegroundNotification
                object:nil];
    [nc removeObserver:self
                  name:UIApplicationDidEnterBackgroundNotification
                object:nil];
    [nc removeObserver:self
                  name:CZCameraStreamingStopNotification
                object:nil];
    [nc postNotificationName:CZCameraViewWillHideNotification
                      object:self];
    
    self.videoTool.magnifierEnabled = NO;
    self.delegate = nil;
    
    [_camera release];
    [_shutterAnimationView release];
    [_snappingHUD release];
    [_videoTool release];
    [_reconnectingHUD release];
    [_cameraLock release];
    [_messageLabel release];
    [_currentCameraName release];
    [_cameraModeController release];
    [super dealloc];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _videoTool.needUpdateScaleBar = YES;
    [_videoTool refreshLayers];
}

- (void)setCamera:(CZCamera *)camera {
    @synchronized (_cameraLock) {
        if (camera == _camera) {
            return;
        }
        
        if (_camera) {
            [self stop];
            
            [_camera release];
            _camera = nil;
        }
        
        if (camera) {
            _videoTool.preferredLiveResolution = [camera liveResolution];
        }

        [_videoTool clear];
        
        CGFloat ratio = 1.0;
        if (camera) {
            ratio = camera.snapResolution.width / camera.liveResolution.width;
        }
        _videoTool.docManager.elementLayer.logicalScaling = ratio;
        
        if (camera) {
            _camera = [camera retain];
            self.currentCameraName = _camera.displayName;
        }
    }
}

- (void)enableAdvancedView:(BOOL)enable {
    self.videoTool.view.userInteractionEnabled = enable;
    self.videoTool.magnifierEnabled = enable;
}

- (void)setAnnotationHidden:(BOOL)hidden {
    self.videoTool.annotationHidden = hidden;
}

- (void)setScaleBarHidden:(BOOL)hidden {
    self.videoTool.scaleBarHidden = hidden;
}

- (void)stop {
    [_camera setDelegate:nil];
    [_camera endEventListening];
    [_camera stop];
    
    [self.reconnectingTimer invalidate];
    self.reconnectingTimer = nil;
    [self.cameraCheckTimer invalidate];
    self.cameraCheckTimer = nil;
}

- (void)play {
    if (_camera == nil) {
        return;
    }
    
    if (self.isCameraRunning) {  // if camera has not start playing yet.
        return;
    }

    [_camera setDelegate:self];
    [_camera play];
    [_camera resume];
    [_camera beginEventListening];
    
    _camera.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
    [self.reconnectingTimer invalidate];
    self.reconnectingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                              target:self
                                                            selector:@selector(streamReconnectingCheck:)
                                                            userInfo:nil
                                                             repeats:YES];
    
    // Check whether resolution is 720p for Kappa camera. If it's larger,
    // such as 1080p, need to show an alert to user.
    if ([_camera isKindOfClass:[CZKappaCamera class]]) {
        CZKappaCamera *kappaCamera = (CZKappaCamera *)_camera;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
            CZCameraControlLock *cameraControlLock = [[CZCameraControlLock alloc] initWithCamera:kappaCamera];
            if (cameraControlLock) {
                uint32_t width = [kappaCamera readRegister:kGEVRegisterImageResolutionWidth];
                [cameraControlLock release];
                
                if (width > [kappaCamera liveResolution].width) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_UNSUPPORTED_RESOLUTION_MESSAGE") level:CZAlertLevelInfo];
                        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
                        [alertController presentAnimated:YES completion:nil];
                    });
                }
            }
        });
    }
    
    const NSTimeInterval kCameraCheckInterval = 5.0;
    [self.cameraCheckTimer invalidate];
    self.cameraCheckTimer = [NSTimer scheduledTimerWithTimeInterval:kCameraCheckInterval
                                                             target:self
                                                           selector:@selector(cameraCheckTimerHandler:)
                                                           userInfo:nil
                                                            repeats:YES];
    
    // Manually trigger the camera reachability check to update HUD status.
    [self.cameraCheckTimer fire];
}

- (void)beginSnap {
    [self beginContinuousSnap:1];
}

- (void)beginContinuousSnap:(NSUInteger)frameCount {
    if (!_camera) {
        CZLogv(@"video is not ready for snapping or recording");
        return;
    }
    _isSnapping = YES;
    [self.snappingHUD setColor:kSpinnerBackgroundColor];
    [self.snappingHUD setLabelText:L(@"BUSY_INDICATOR_SNAPPING")];
    
    [_camera beginContinuousSnapping:frameCount];
    
    [_shutterAnimationView setAlpha:1.0];
    [_shutterAnimationView setHidden:NO];
    [UIView animateWithDuration:0.5
                     animations:^() {
                         [_shutterAnimationView setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [_shutterAnimationView setHidden:YES];
                     }];
}

- (void)beginFastSnap {
    if (!_camera) {
        CZLogv(@"video is not ready for snapping or recording");
        return;
    }
    _isSnapping = YES;
    [self.snappingHUD setColor:kSpinnerBackgroundColor];
    [self.snappingHUD setLabelText:L(@"BUSY_INDICATOR_SNAPPING")];
    
    [_camera beginSnappingFromVideoStream];
    
    [_shutterAnimationView setAlpha:1.0];
    [_shutterAnimationView setHidden:NO];
    [UIView animateWithDuration:0.5
                     animations:^() {
        [_shutterAnimationView setAlpha:0.0];
    }
                     completion:^(BOOL finished) {
        [_shutterAnimationView setHidden:YES];
    }];
}

- (void)pauseCamera {
    @synchronized (_cameraLock) {
        [self.camera pause];
    }
}

- (void)resumeCamera {
    @synchronized (_cameraLock) {
        [self.camera resume];
    }
}

- (void)forceAutoCameraMode {
    @synchronized (_cameraLock) {
        [_cameraModeController forceAutoCameraMode:self.camera];
    }
}

- (void)restoreCameraMode {
    @synchronized (_cameraLock) {
        [_cameraModeController restoreCameraMode:self.camera];
    }
}

#pragma mark - Private methods

- (void)cameraCheckTimerHandler:(NSTimer *)timer {
    BOOL reachable = YES;
    if (_camera == nil) {
        reachable = NO;
    } else {
        const NSTimeInterval kLostTime = -15.0;
        NSTimeInterval timeInterval = [[_camera lastActiveTime] timeIntervalSinceNow];
        if (timeInterval <= kLostTime) {
            reachable = NO;
        }
    }
    
    if (!self.isReconnecting && !reachable) {
        [_camera stop];
        self.isReconnecting = YES;
        
        if (self.reconnectingHUD == nil) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
            self.reconnectingHUD = hud;
            [hud setColor:kSpinnerBackgroundColor];
            
            // If the camera is lost, dismiss any snapping or camera control HUD.
            [self.snappingHUD dismiss:YES];
            self.snappingHUD = nil;
        }
        [self refreshReconnectingLabel];
    } else if (self.isReconnecting && reachable) {
        self.isReconnecting = NO;
        if (self.reconnectingHUD) {
            [_reconnectingHUD dismiss:YES];
            self.reconnectingHUD = nil;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(cameraView:didChangeReachability:)]) {
        [self.delegate cameraView:self didChangeReachability:reachable];
    }
}

- (void)refreshReconnectingLabel {
    if (self.reconnectingHUD) {
        NSString *labelText;
        if (self.currentCameraName) {
            labelText = [NSString stringWithFormat:L(@"BUSY_INDICATOR_RECONNECTING_TO"),
                         self.currentCameraName];
        } else {
            labelText = L(@"BUSY_INDICATOR_RECONNECTING");
        }
        
        NSString *wlanTitle = L(@"WLAN_TITLE");
        NSString *ssid = [CZWifiNotifier copySSID];
        
        NSString *detailsLabelText = [wlanTitle stringByAppendingString:ssid];
        
        [self.reconnectingHUD setLabelText:labelText];
        [self.reconnectingHUD setDetailsLabelText:detailsLabelText];
        [ssid release];
    }
}

- (void)streamReconnectingCheck:(NSTimer *)timer {
    if (self.camera && ![self.camera isPaused]) {
        NSTimeInterval interval = [self.camera.lastFrameTime timeIntervalSinceNow];
        if (interval < -3.0) {
            
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
                // If there was no new frame for at least 3 seconds.
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
                    [self.camera stop];
                    [self.camera play];
                });
            }
            
            // Set a fake frame refresh time to delay next retry, give a
            // chance to current stop and play.
            self.camera.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0.0];
        }
    }
}

- (MBProgressHUD *)snappingHUD {
    if (!_snappingHUD) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
        self.snappingHUD = hud;
    }
    
    return _snappingHUD;
}

- (void)beginMeasuring {
    [self beginMeasuringWithMeasurementHidden:NO];
}

- (void)beginMeasuringWithMeasurementHidden:(BOOL)hidden {
    [self.videoTool beginMeasuringWithMeasurementHidden:hidden];
}

- (float)measuredValue {
    return [self.videoTool measuredValue];
}

- (void)endMeasuring {
    [self.videoTool endMeasuring];
}

- (void)updateFOV:(float)fov {
    self.videoTool.fieldOfView = fov;
}

#pragma mark - Private methods

- (void)notifyCameraStopForSnapping:(NSNotification *)aNotification {
    id macAddress = [aNotification userInfo][@"macAddress"];
    if (![macAddress isKindOfClass:[NSString class]]) {
        return;
    }
    
    if (!self.isCameraRunning) {
        return;
    }
    
    if (_notifyCount > 0) {
        _notifyCount--;
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        if (_isSnapping) {  // snapping by myself, no need to show message
            _notifyCount++;
            return;
        }
        BOOL isEqualMacAddress = [self.camera.macAddress isEqualComparisionWithCaseInsensitive:macAddress];
        if (isEqualMacAddress) {
            if ([self.delegate respondsToSelector:@selector(cameraView:didChangeSnappingStatus:)]) {
                if (!_isSnapping && self.messageLabel.hidden) {
                    self.messageLabel.hidden = NO;
                }

                [self.delegate cameraView:self didChangeSnappingStatus:NO];
            }
        }
    });
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    [self play];
}

- (void)appDidEnterBackground:(NSNotification *)notification {
    [self stop];
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        messageLabel.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        messageLabel.textColor = kProgressLabelColor;
        messageLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.hidden = YES;
        messageLabel.layer.cornerRadius = 3;
        messageLabel.text = L(@"LIVE_CAMERA_BUSY_MESSAGE");
        UIView *rootView = self;
        [rootView addSubview:messageLabel];
        [rootView bringSubviewToFront:messageLabel];
        messageLabel.translatesAutoresizingMaskIntoConstraints = 0;
        
        CGRect frame = [messageLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, 32)
                             limitedToNumberOfLines:0];
        
        [rootView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:rootView
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1.0f
                                                              constant:0]];
        if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
            [rootView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:rootView
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0f
                                                                  constant:-40]];

        } else {
            [rootView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel
                                                                 attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:rootView
                                                                 attribute:NSLayoutAttributeBottom
                                                                multiplier:1.0f
                                                                  constant:-68]];
        }
        [rootView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:frame.size.width + 2 * 8]];
        [rootView addConstraint:[NSLayoutConstraint constraintWithItem:messageLabel
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0f
                                                              constant:32]];
        
        _messageLabel = messageLabel;
    }
    
    return _messageLabel;
}

- (BOOL)isCameraRunning {
    return self.camera.delegate == self;
}

- (void)notifySnapDidChange {
    @synchronized (_cameraLock) {
        if (self.camera) {
            _messageLabel.hidden = YES;
            if ([self.delegate respondsToSelector:@selector(cameraView:didChangeSnappingStatus:)]) {
                [self.delegate cameraView:self didChangeSnappingStatus:YES];
            }
        }
    }
}

#pragma mark - CZCameraDelegate methods

- (void)camera:(CZCamera *)camera didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {
    if (self.snappingHUD) {
        [_snappingHUD dismiss:YES];
        self.snappingHUD = nil;
    }
    
    if (error != nil || snappingInfo.image == nil) {
        NSString *message = L(@"ERROR_SNAP_TIMEOUT");
        switch (error.code) {
            case kCZCameraNoControlError:
                message = L(@"LIVE_NO_CONTROL_ACCESS_MESSAGE");
                break;
            case kCZCameraIncompatibleFirmwareVersionError:
                message = L(@"ERROR_OLD_AXIOCAM20X_FW");
                break;
            default:
                break;
        }

        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:message level:CZAlertLevelError];
        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
        [alertController presentAnimated:YES completion:nil];
    }
    
    if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
        [self.delegate camera:camera
            didFinishSnapping:snappingInfo
                        error:error];
    }
    _isSnapping = NO;
}

- (void)camera:(CZCamera *)camera didGetVideoFrame:(UIImage *)frame {
    @synchronized (_cameraLock) {
        if (self.camera) {
            [_videoTool setFrame:frame completion:nil];
            
            if ([self.delegate respondsToSelector:@selector(camera:didGetVideoFrame:)]) {
                [self.delegate camera:camera didGetVideoFrame:frame];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self notifySnapDidChange];
               
                // trigger timer, try to dismiss HUD
                if (self.reconnectingHUD) {
                    [self.cameraCheckTimer fire];
                }
            });
        }
    }
}


@end
