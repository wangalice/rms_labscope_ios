//
//  CZFilesPickerController.h
//  Matscope
//
//  Created by Mike Wang on 1/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZLocalFileListController.h"

@class CZFilesPickerController, CZFileEntity;

@protocol CZFilesPickerControllerDelegate <NSObject>

- (void)filesPickerController:(CZFilesPickerController *)filesPickerController
               didSelectFiles:(NSArray <CZFileEntity *>*)selectedFiles;

@end

@interface CZFilesPickerController : UIViewController

@property (nonatomic, weak) id<CZFilesPickerControllerDelegate> delegate;
@property (nonatomic, strong) CZLocalFileListController *localFileListController;

@end

@interface CZFilesPickerPresentationController : UIPresentationController

@property (nonatomic, assign) CGSize contentSize;

@end
