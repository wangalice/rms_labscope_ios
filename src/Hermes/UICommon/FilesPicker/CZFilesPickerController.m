//
//  CZFilesPickerController.m
//  Matscope
//
//  Created by Mike Wang on 1/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZFilesPickerController.h"
#import <CZFileKit/CZFileKit.h>
#import "CZLocalFileListController.h"
#import "CZFilesListViewModel.h"
#import "CZLocalFilesViewModel.h"
#import "CZLocalFileOpenProtocol.h"

static const CGFloat kDefaultViewRadius = 8.0;

@interface CZFilesPickerController () <
CZTextFieldDelegate,
CZFilesListViewModelDelegate,
CZFilesListViewControllerDelegate,
CZLocalFilesViewModelDelegate,
CZLocalFileOpenProtocol,
UIViewControllerTransitioningDelegate
>

@property (nonatomic, strong) CZCheckBox *selectAllButton;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *fileNameLabel;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) CZTextField *searchTextField;
@property (nonatomic, strong) UIView *fileSelectionBackgroundView;
@property (nonatomic, copy) NSArray *selectedFiles;
@property (nonatomic, strong) CZLocalFilesViewModel *localFilesViewModel;

@end

@implementation CZFilesPickerController

- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate  = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    static CGFloat kDefultFileHeaderHeight = 24.0f;
    static CGFloat kDefultSelectionAllButtonWidth = 48.0f;

    self.view.backgroundColor = [UIColor cz_gs10];
    self.view.layer.cornerRadius = kDefaultViewRadius;
    self.view.layer.masksToBounds = YES;
    
    UIView *separateVertialLine = [[UIView alloc] init];
    separateVertialLine.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs60], [UIColor cz_gs110]);
    UIView *separateHorizontalLine = [[UIView alloc] init];
    separateHorizontalLine.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs60], [UIColor cz_gs110]);

    [self.view addSubview:self.headerView];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.searchTextField];
    [self.view addSubview:self.fileSelectionBackgroundView];
    [self.view addSubview:self.doneButton];
    [self.view addSubview:self.cancelButton];
    [self.view addSubview:separateVertialLine];
    [self.view addSubview:separateHorizontalLine];
    
    [self addChildViewController:self.localFileListController];
    [self.fileSelectionBackgroundView addSubview:self.localFileListController.view];
    self.localFileListController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.localFileListController didMoveToParentViewController:self];
    
    [self.fileSelectionBackgroundView addSubview:self.selectAllButton];
    [self.fileSelectionBackgroundView addSubview:self.fileNameLabel];

    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.searchTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileSelectionBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    self.doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    separateVertialLine.translatesAutoresizingMaskIntoConstraints = NO;
    separateHorizontalLine.translatesAutoresizingMaskIntoConstraints = NO;
    self.selectAllButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.headerView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor ].active = YES;
    [self.headerView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.headerView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.headerView.heightAnchor constraintEqualToConstant:8.0f].active = YES;

    [self.titleLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:32.0f].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.headerView.bottomAnchor constant:20.0f].active = YES;
    [self.titleLabel.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-32.0f].active = YES;
    [self.titleLabel.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    
    [self.searchTextField.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.searchTextField.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:27.0].active = YES;
    [self.searchTextField.widthAnchor constraintEqualToAnchor:self.titleLabel.widthAnchor].active = YES;
    [self.searchTextField.heightAnchor constraintEqualToConstant:32.0f].active = YES;

    [self.fileSelectionBackgroundView.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.fileSelectionBackgroundView.topAnchor constraintEqualToAnchor:self.searchTextField.bottomAnchor constant:27.0f].active = YES;
    [self.fileSelectionBackgroundView.widthAnchor constraintEqualToAnchor:self.titleLabel.widthAnchor].active = YES;
    [self.fileSelectionBackgroundView.heightAnchor constraintEqualToConstant:310.0f].active = YES;
    
    [self.cancelButton.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.cancelButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.cancelButton.widthAnchor constraintEqualToAnchor:self.view.widthAnchor multiplier:0.5 constant:-1.0].active = YES;
    [self.cancelButton.heightAnchor constraintEqualToConstant:48.0f].active = YES;
    
    [self.doneButton.leftAnchor constraintEqualToAnchor:separateVertialLine.rightAnchor].active = YES;
    [self.doneButton.centerYAnchor constraintEqualToAnchor:self.cancelButton.centerYAnchor].active = YES;
    [self.doneButton.widthAnchor constraintEqualToAnchor:self.view.widthAnchor multiplier:0.5 constant:-1.0].active = YES;
    [self.doneButton.heightAnchor constraintEqualToConstant:48.0f].active = YES;
    
    [separateHorizontalLine.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [separateHorizontalLine.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [separateHorizontalLine.bottomAnchor constraintEqualToAnchor:self.cancelButton.topAnchor].active = YES;
    [separateHorizontalLine.heightAnchor constraintEqualToConstant:1.0].active = YES;
    
    [separateVertialLine.leftAnchor constraintEqualToAnchor:self.cancelButton.rightAnchor].active = YES;
    [separateVertialLine.topAnchor constraintEqualToAnchor:self.cancelButton.topAnchor].active = YES;
    [separateVertialLine.widthAnchor constraintEqualToConstant:1.0f].active = YES;
    [separateVertialLine.heightAnchor constraintEqualToConstant:48.0f].active = YES;

    [self.localFileListController.view.leftAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.leftAnchor].active = YES;
    [self.localFileListController.view.topAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.topAnchor constant:kDefultFileHeaderHeight].active = YES;
    [self.localFileListController.view.widthAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.widthAnchor].active = YES;
    [self.localFileListController.view.heightAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.heightAnchor constant:-kDefultFileHeaderHeight].active = YES;

    [self.selectAllButton.leftAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.leftAnchor constant:13.0f].active = YES;
    [self.selectAllButton.topAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.topAnchor].active = YES;
    [self.selectAllButton.widthAnchor constraintEqualToConstant:kDefultSelectionAllButtonWidth].active = YES;
    [self.selectAllButton.heightAnchor constraintEqualToConstant:kDefultFileHeaderHeight].active = YES;

    [self.fileNameLabel.leftAnchor constraintEqualToAnchor:self.selectAllButton.rightAnchor].active = YES;
    [self.fileNameLabel.topAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.topAnchor].active = YES;
    [self.fileNameLabel.rightAnchor constraintEqualToAnchor:self.fileSelectionBackgroundView.rightAnchor].active = YES;
    [self.fileNameLabel.heightAnchor constraintEqualToConstant:kDefultFileHeaderHeight].active = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.selectAllButton.selected = NO;
}

- (void)dealloc {
    _localFilesViewModel.delegate = nil;
    _localFileListController.delegate = nil;
}

#pragma mark - Action

- (void)doneButtonAction:(UIButton *)sender {
    @weakify(self);
    [self dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        if ([self.delegate respondsToSelector:@selector(filesPickerController:didSelectFiles:)]) {
            [self.delegate filesPickerController:self
                                  didSelectFiles:self.selectedFiles];
        }
    }];
}

- (void)cancelButtonAction:(UIButton *)sender {
    self.selectedFiles = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)localFilesSelectAllAction:(id)sender {
    self.selectAllButton.selected = !self.selectAllButton.selected;

    if (self.selectAllButton.selected) {
        [self.localFilesViewModel selectAllFileEntities];
    } else {
        [self.localFilesViewModel clearAllSelectedFileEntities];
    }
}

- (void)textFieldInputTextChanged:(CZTextField *)textField {
    self.localFilesViewModel.keyword = textField.text;
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    CZFilesPickerPresentationController *presentationController = [[CZFilesPickerPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    presentationController.contentSize = CGSizeMake(458.0f, 508.f);
    return presentationController;
}

#pragma mark - CZFileViewProtocol

- (void)controller:(UIViewController *)controller didRequestOpeningFileAtPath:(NSString *)filePath {
    CZFileEntity *fileEntity = [self.localFilesViewModel fileEntityFromFileName:filePath.lastPathComponent];
    BOOL isFileSelected = [self.localFilesViewModel isFileEntitySelected:fileEntity];
    if (isFileSelected) {
        [self.localFilesViewModel deselectFile:fileEntity];
    } else {
        [self.localFilesViewModel selectFile:fileEntity];
    }
}

#pragma mark - CZLocalFilesViewModelDelegate

- (void)viewModelDidSelectFileEntities:(CZLocalFilesViewModel *)localViewModel {
    NSMutableArray *selectedFiles = [NSMutableArray arrayWithArray:self.selectedFiles];
    for (CZFileEntity *fileEntity in self.selectedFiles) {
        if (![localViewModel isFileEntitySelected:fileEntity]) {
            [selectedFiles removeObject:fileEntity];
        }
    }
    
    for (CZFileEntity *fileEntity in localViewModel.selectedFiles) {
        if (![selectedFiles containsObject:fileEntity.filePath]) {
            [selectedFiles addObject:fileEntity];
        }
    }
    
    self.selectedFiles = selectedFiles;
}

#pragma mark - Getter

- (CZLocalFileListController *)localFileListController {
    if (!_localFileListController) {
        _localFileListController = [[CZLocalFileListController alloc] initWithViewModel:self.localFilesViewModel.localList];
        _localFileListController.delegate = self;
        _localFileListController.isActive = YES;
    }
    return _localFileListController;
}

- (CZLocalFilesViewModel *)localFilesViewModel {
    if (_localFilesViewModel == nil) {
        NSString *documentPath = [NSFileManager defaultManager].cz_documentPath;
        _localFilesViewModel = [[CZLocalFilesViewModel alloc] initWithDocumentPath:documentPath];
        _localFilesViewModel.filterType = CZFileFilterTypeImage;
        _localFilesViewModel.delegate = self;
    }
    return _localFilesViewModel;
}

- (CZTextField *)searchTextField {
    if (_searchTextField == nil) {
        _searchTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault
                                                         unit:nil
                                                    rightIcon:[CZIcon iconNamed:@"search"]];
        _searchTextField.showClearButtonWhileEditing = YES;
        _searchTextField.placeholder = L(@"ENTER_FILE_NAME");
        _searchTextField.delegate = self;
        [_searchTextField addTarget:self action:@selector(textFieldInputTextChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _searchTextField;
}

- (CZCheckBox *)selectAllButton {
    if (_selectAllButton == nil) {
        _selectAllButton = [[CZCheckBox alloc] init];
        _selectAllButton.selected = NO;
        [_selectAllButton addTarget:self action:@selector(localFilesSelectAllAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectAllButton;
}

- (UIButton *)doneButton {
    if (!_doneButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton setTitle:L(@"DONE") forState:UIControlStateNormal];
        _doneButton.exclusiveTouch = YES;
        _doneButton.titleLabel.font = [UIFont cz_subtitle1];
        [_doneButton setTitleColor:[UIColor cz_pb100] forState:UIControlStateNormal];
        [_doneButton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _doneButton;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.exclusiveTouch = YES;
        _cancelButton.titleLabel.font = [UIFont cz_subtitle1];
        [_cancelButton setTitle:L(@"CANCEL") forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor cz_gs100] forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = L(@"ADD_IMAGE");
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.textColor = [UIColor cz_gs120];
    }
    return _titleLabel;
}

- (UIView *)fileSelectionBackgroundView {
    if (_fileSelectionBackgroundView == nil) {
        _fileSelectionBackgroundView = [[UIView alloc] init];
        _fileSelectionBackgroundView.backgroundColor = [UIColor cz_gs105];
        _fileSelectionBackgroundView.layer.borderWidth = 1.0f;
        _fileSelectionBackgroundView.clipsToBounds = YES;
        _fileSelectionBackgroundView.layer.borderColor = [UIColor cz_gs120].CGColor;
    }
    return _fileSelectionBackgroundView;
}

- (UILabel *)fileNameLabel {
    if (_fileNameLabel == nil) {
        _fileNameLabel = [[UILabel alloc] init];
        _fileNameLabel.text = L(@"FILE_NAME");
        _fileNameLabel.font = [UIFont cz_label2];
        _fileNameLabel.textColor = [UIColor cz_gs80];
    }
    return _fileNameLabel;
}

- (UIView *)headerView {
    if (_headerView == nil) {
        _headerView = [[UIView alloc] init];
        _headerView.backgroundColor = [UIColor cz_gs90];
    }
    return _headerView;
}

@end


@interface CZFilesPickerPresentationController ()

@property (nonatomic, strong) UIView *dimmingView;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation CZFilesPickerPresentationController

- (CGRect)frameOfPresentedViewInContainerView {
    CGRect containerViewBounds = self.containerView.bounds;
    
    // The presented view extends presentedViewContentSize.height points from
    // the bottom edge of the screen.
    CGRect presentedViewControllerFrame = containerViewBounds;
    presentedViewControllerFrame.size.height = _contentSize.height;
    presentedViewControllerFrame.size.width = _contentSize.width;
    presentedViewControllerFrame.origin.y = (CGRectGetHeight([UIScreen mainScreen].bounds) - _contentSize.height) / 2;
    presentedViewControllerFrame.origin.x = (CGRectGetWidth([UIScreen mainScreen].bounds) - _contentSize.width) / 2;
    return presentedViewControllerFrame;
}

#pragma mark - Action

- (void)tapGestureEvent:(UITapGestureRecognizer *)sender {
    if (self.presentingViewController) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Getter
- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                        action:@selector(tapGestureEvent:)];
    }
    return _tapGestureRecognizer;
}

- (UIView *)dimmingView {
    if (!_dimmingView) {
        _dimmingView = [[UIView alloc] initWithFrame:self.containerView.bounds];
        _dimmingView.backgroundColor = [UIColor cz_gs120];
    }
    return _dimmingView;
}

@end
