//
//  CZBalloonView.h
//  Hermes
//
//  Created by Ralph Jin on 7/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CZBalloonDirection) {
    CZBalloonDirectionAny,
    CZBalloonDirectionUp,
    CZBalloonDirectionLeft,
    CZBalloonDirectionDown,
    CZBalloonDirectionRight,
    CZBalloonDirectionNone
};

@interface CZBalloonView : UIView

@property(nonatomic, readonly, retain) UILabel *textLabel;
@property(nonatomic, readonly, retain) UIImageView *arrowImageView;
@property(nonatomic, readonly, assign) CZBalloonDirection direction;

+ (CGSize)calculateSizeOfText:(NSString *)helpText thatFits:(CGSize)size;

- (id)initWithFrame:(CGRect)frame direction:(CZBalloonDirection)direction;

- (void)setHelpText:(NSString *)helpText;

/** the arrow image shall point to right*/
- (void)setArrowImage:(UIImage *)arrowImage;

- (void)pointToView:(UIView *)view;
- (void)pointToPoint:(CGPoint)point;

@end
