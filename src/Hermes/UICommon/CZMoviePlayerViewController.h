//
//  CZMoviePlayerViewController.h
//  Hermes
//
//  Created by Li, Junlin on 6/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMoviePlayerViewController : UIViewController

- (instancetype)initWithContentURL:(NSURL *)contentURL;

@end
