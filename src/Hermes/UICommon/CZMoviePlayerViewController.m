//
//  CZMoviePlayerViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMoviePlayerViewController.h"
#import <AVKit/AVKit.h>
#import "CZPostOffice.h"

@interface CZMoviePlayerViewController ()

@property (nonatomic, readonly, strong) NSURL *contentURL;
@property (nonatomic, strong) CZBarButtonItem *shareItem;
@property (nonatomic, strong) AVPlayerLooper *playerLooper;
@property (nonatomic, strong) AVPlayerViewController *playerViewController;

@end

@implementation CZMoviePlayerViewController

- (instancetype)initWithContentURL:(NSURL *)contentURL {
    self = [super init];
    if (self) {
        _contentURL = contentURL;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appWillEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidEnterBackgroud:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [self stopPlaying];
#if DCM_REFACTORED
    [[CZDCMManager defaultManager] setDelegate:nil];
#endif
    CZLogv(@"Work well.");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:L(@"FILES_PREVIEW_TITLE"), [self.contentURL lastPathComponent]];
    
    self.view.backgroundColor = kDefaultBackGroundColor;
    
    CZBarButtonItem *shareItem = [[CZBarButtonItem alloc] initWithIcon:[CZIcon iconNamed:@"share"] target:self action:@selector(shareAction:)];
    self.navigationItem.rightBarButtonItem = shareItem;
    self.shareItem = shareItem;
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:self.contentURL];
    AVQueuePlayer *player = [AVQueuePlayer queuePlayerWithItems:@[playerItem]];
    
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.player = player;
    playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    playerViewController.showsPlaybackControls = NO;
    playerViewController.view.frame = self.view.bounds;
    playerViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self addChildViewController:playerViewController];
    [self.view addSubview:playerViewController.view];
    [playerViewController didMoveToParentViewController:self];
    self.playerViewController = playerViewController;
    
    [playerItem.asset loadValuesAsynchronouslyForKeys:@[@"duration"] completionHandler:^{
        NSError *error = nil;
        AVKeyValueStatus status = [playerItem.asset statusOfValueForKey:@"duration" error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (status == AVKeyValueStatusLoaded) {
                self.playerLooper = [AVPlayerLooper playerLooperWithPlayer:player templateItem:playerItem];

                [self registerKVO];
                [self.playerViewController.player play];
            } else {
                CZLogv(@"Failed to load duration property [with error: %@]", error);
            }
        });
    }];
}

- (void)didReceiveMemoryWarning {
    CZLogv(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    
    [self.playerLooper disableLooping];
    [self.playerViewController.player pause];
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    [self.playerViewController.player play];
}

- (void)appDidEnterBackgroud:(NSNotification *)notification {
    [self.playerViewController.player pause];
}

- (void)shareAction:(id)sender {
    NSString *filePath = [self.contentURL relativePath];
    if (filePath) {
        CZPostOffice *postOffice = [[CZPostOffice alloc] init];
        postOffice.convertToJPGEnabled = YES;
        [postOffice addShareItemFromPath:filePath];
        
        [postOffice presentActivityViewControllerFromBarButtonItem:self.shareItem];
    }
}

#pragma mark - Private method

- (void)stopPlaying {
    [self.playerViewController.player pause];
    [self unregisterKVO];
    
    [self.playerLooper disableLooping];
    self.playerLooper = nil;
    
    self.playerViewController.player = nil;
}

#pragma mark - KVO Observer

- (void)registerKVO {
    for (NSString *keyPath in [self observerKeyPaths]) {
        [self.playerLooper addObserver:self
                            forKeyPath:keyPath
                               options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                               context:nil];
    }
}

- (void)unregisterKVO {
    for (NSString *keyPath in [self observerKeyPaths]) {
        [self.playerLooper removeObserver:self forKeyPath:keyPath context:nil];
    }
}

- (NSArray <NSString *>*)observerKeyPaths {
    return @[@"loopCount",
             @"status"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (object == self.playerLooper) {
        
        if ([keyPath isEqualToString:@"loopCount"]) {
            CZLogv(@"loopCount: %@", change[NSKeyValueChangeNewKey]);
        }
        
        if ([keyPath isEqualToString:@"status"]) {
            CZLogv(@"status: %@", change[NSKeyValueChangeNewKey]);
            
            AVPlayerLooperStatus status = [change[NSKeyValueChangeNewKey] intValue];
            if (status == AVPlayerLooperStatusFailed) {
                CZLogv(@"error: %@", self.playerLooper.error);
                [self unregisterKVO];
            }
        }
    }
}

@end
