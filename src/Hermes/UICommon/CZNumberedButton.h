//
//  CZNumberedButton.h
//  Hermes
//
//  Created by Li, Junlin on 3/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZNumberedButton : CZButton

- (instancetype)initWithFrame:(CGRect)frame number:(NSUInteger)number;

@end
