//
//  MBProgressHUD+Hide.m
//  Hermes
//
//  Created by Ralph Jin on 7/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "MBProgressHUD+Hide.h"

@implementation MBProgressHUD (Hide)

- (void)dismiss:(BOOL)animated {
    self.removeFromSuperViewOnHide = YES;
    [self hide:animated];
}

@end
