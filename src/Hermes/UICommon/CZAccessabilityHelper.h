//
//  CZAccessabilityHelper.h
//  Labscope
//
//  Created by Ralph Jin on 9/21/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZAccessabilityHelper : NSObject

- (instancetype)init NS_UNAVAILABLE;

+ (void)requestAccessCameraWithCompletion:(void (^)(BOOL success))completion;

@end
