//
//  CZFileNameEditor.h
//  Hermes
//
//  Created by Li, Junlin on 8/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileNameEditor;

@protocol CZFileNameEditorDelegate <NSObject>

@optional
- (BOOL)fileNameEditor:(CZFileNameEditor *)editor validateFileName:(NSString *)fileName;
- (void)fileNameEditor:(CZFileNameEditor *)editor didBeginEditingFileName:(NSString *)fileName;
- (void)fileNameEditor:(CZFileNameEditor *)editor didFinishEditingFileName:(NSString *)fileName;

@end

@interface CZFileNameEditor : UIView

@property (nonatomic, weak) id<CZFileNameEditorDelegate> delegate;
@property (nonatomic, readonly, strong) UILabel *fileNameLabel;
@property (nonatomic, readonly, strong) UIButton *editButton;

@end
