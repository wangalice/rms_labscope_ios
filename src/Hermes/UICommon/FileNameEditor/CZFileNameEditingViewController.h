//
//  CZFileNameEditingViewController.h
//  Hermes
//
//  Created by Li, Junlin on 8/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZFileNameEditingViewController : UIViewController

@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, strong) CZTextField *fileNameTextField;
@property (nonatomic, readonly, strong) UIButton *doneButton;

@end
