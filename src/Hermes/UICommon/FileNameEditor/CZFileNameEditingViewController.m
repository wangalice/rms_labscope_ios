//
//  CZFileNameEditingViewController.m
//  Hermes
//
//  Created by Li, Junlin on 8/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileNameEditingViewController.h"

@implementation CZFileNameEditingViewController

@synthesize stackView = _stackView;
@synthesize fileNameTextField = _fileNameTextField;
@synthesize doneButton = _doneButton;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.stackView];
    [self.stackView addArrangedSubview:self.fileNameTextField];
    [self.stackView addArrangedSubview:self.doneButton];
    
    [self.fileNameTextField setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.doneButton setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [self.doneButton.widthAnchor constraintEqualToConstant:48.0].active = YES;
}

#pragma mark - Views

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.axis = UILayoutConstraintAxisHorizontal;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.spacing = 16.0;
    }
    return _stackView;
}

- (CZTextField *)fileNameTextField {
    if (_fileNameTextField == nil) {
        _fileNameTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _fileNameTextField.showClearButtonWhileEditing = YES;
        _fileNameTextField.returnKeyType = UIReturnKeyDone;
        _fileNameTextField.errorMessage = L(@"FILE_NAME_CONFLICT_ERROR_MESSAGE");
    }
    return _fileNameTextField;
}

- (UIButton *)doneButton {
    if (_doneButton == nil) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_doneButton cz_setImageWithIcon:[CZIcon iconNamed:@"checkmark"]];
    }
    return _doneButton;
}

@end
