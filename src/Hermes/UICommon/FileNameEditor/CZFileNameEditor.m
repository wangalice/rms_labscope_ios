//
//  CZFileNameEditor.m
//  Hermes
//
//  Created by Li, Junlin on 8/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileNameEditor.h"
#import "CZFileNameEditingViewController.h"
#import "UIWindow+CZPresentedViewController.h"

@interface CZFileNameEditor () <CZKeyboardObserver, CZTextFieldDelegate>

@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, strong) CZFileNameEditingViewController *editingViewController;

@end

@implementation CZFileNameEditor

@synthesize stackView = _stackView;
@synthesize fileNameLabel = _fileNameLabel;
@synthesize editButton = _editButton;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.stackView];
        [self.stackView addArrangedSubview:self.fileNameLabel];
        [self.stackView addArrangedSubview:self.editButton];
        
        [self.stackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
        [self.stackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
        [self.stackView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [self.stackView.heightAnchor constraintEqualToConstant:32.0].active = YES;
        
        [self.fileNameLabel setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        [self.editButton setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [self.editButton.widthAnchor constraintEqualToConstant:48.0].active = YES;
    }
    return self;
}

#pragma mark - Views

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.axis = UILayoutConstraintAxisHorizontal;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.spacing = 16.0;
    }
    return _stackView;
}

- (UILabel *)fileNameLabel {
    if (_fileNameLabel == nil) {
        _fileNameLabel = [[UILabel alloc] init];
        _fileNameLabel.font = [UIFont cz_body1];
        _fileNameLabel.textColor = [UIColor cz_gs50];
        _fileNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    }
    return _fileNameLabel;
}

- (UIButton *)editButton {
    if (_editButton == nil) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton cz_setImageWithIcon:[CZIcon iconNamed:@"edit"]];
        [_editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editButton;
}

#pragma mark - Actions

- (void)editButtonAction:(id)sender {
    CZFileNameEditingViewController *editingViewController = [[CZFileNameEditingViewController alloc] init];
    [self.window.cz_frontmostPresentedViewController presentViewController:editingViewController animated:NO completion:^{
        [[CZKeyboardManager sharedManager] addObserver:self];
        
        editingViewController.stackView.hidden = YES;
        
        editingViewController.fileNameTextField.delegate = self;
        editingViewController.fileNameTextField.text = self.fileNameLabel.text;
        [editingViewController.fileNameTextField becomeFirstResponder];
        
        [editingViewController.doneButton addTarget:self action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
    self.editingViewController = editingViewController;
}

- (void)doneButtonAction:(id)sender {
    [self.editingViewController.fileNameTextField resignFirstResponder];
}

#pragma mark - CZKeyboardObserver

- (void)keyboardDidShow:(CZKeyboardTransition *)transition {
    self.editingViewController.stackView.hidden = NO;
    self.editingViewController.stackView.frame = [self.stackView convertRect:self.stackView.bounds toView:self.window];
    
    self.stackView.hidden = YES;
}

#pragma mark - CZTextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameEditor:didBeginEditingFileName:)]) {
        [self.delegate fileNameEditor:self didBeginEditingFileName:textField.text];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.editingViewController dismissViewControllerAnimated:NO completion:^{
        [[CZKeyboardManager sharedManager] removeObserver:self];
        
        BOOL isValid = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameEditor:validateFileName:)]) {
            isValid = [self.delegate fileNameEditor:self validateFileName:textField.text];
        }
        if (isValid) {
            self.fileNameLabel.text = self.editingViewController.fileNameTextField.text;
        }
        
        self.stackView.hidden = NO;
        self.editingViewController = nil;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameEditor:didFinishEditingFileName:)]) {
            [self.delegate fileNameEditor:self didFinishEditingFileName:self.fileNameLabel.text];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEditing:(CZTextField *)textField {
    if (textField.text.length == 0) {
        return NO;
    }
    
    BOOL isValid = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameEditor:validateFileName:)]) {
        isValid = [self.delegate fileNameEditor:self validateFileName:textField.text];
    }
    return !isValid;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEndEditing:(CZTextField *)textField {
    if (textField.text.length == 0) {
        return NO;
    }
    
    BOOL isValid = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameEditor:validateFileName:)]) {
        isValid = [self.delegate fileNameEditor:self validateFileName:textField.text];
    }
    return !isValid;
}

@end
