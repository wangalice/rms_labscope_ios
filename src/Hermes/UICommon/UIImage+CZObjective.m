//
//  UIImage+CZObjective.m
//  Hermes
//
//  Created by Li, Junlin on 7/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImage+CZObjective.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@implementation UIImage (CZObjective)

+ (UIImage *)objectiveImageForMagnification:(float)magnification size:(CGSize)size {
    if (magnification == 0.0) {
        CZIconRenderingOptions *options = [CZIconRenderingOptions renderingOptionsForControlWithSize:size];
        return [[CZIcon iconNamed:@"objective-none"] imageWithOptions:options];
    } else {
        CZIconRenderingOptions *options = [CZIconRenderingOptions renderingOptionsForControlWithSize:size];
        [options setTintColor:[UIColor colorForMagnification:magnification] forElementIdentifier:@"magnification"];
        return [[CZIcon iconNamed:@"objective"] imageWithOptions:options];
    }
}

@end
