//
//  CZBulletedListView.h
//  Matscope
//
//  Created by Halley Gu on 3/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZBulletedListView : UIView

@property (nonatomic, copy) NSArray *contents;
@property (nonatomic, copy) NSString *bullet;
@property (nonatomic, strong) UIFont *font;

@end
