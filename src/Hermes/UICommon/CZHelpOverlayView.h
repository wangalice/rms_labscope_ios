//
//  CZHelpOverlayView.h
//  Hermes
//
//  Created by Ralph Jin on 7/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZBalloonView.h"

typedef NS_ENUM(NSUInteger, CZHelpOverlayDirection) {
    CZHelpOverlayDirectionTopRight,
    CZHelpOverlayDirectionTopLeft,
    CZHelpOverlayDirectionBottomLeft
};

@interface CZHelpOverlayView : UIView

/** key for show next time in user default setting*/
@property (nonatomic, copy) NSString *hideNextTimeKey;

+ (UIControl *)findBarButtonItem:(UIBarButtonItem *)barButtonItem onBar:(UINavigationBar *)bar;

+ (CZHelpOverlayView *)current;

- (void)setIntroductoryText:(NSString *)introductoryText bulletPoints:(NSArray *)bulletPoints;
- (void)setIntroductoryText:(NSString *)introductoryText bulletPoints:(NSArray *)bulletPoints direction:(CZHelpOverlayDirection)direction;

- (void)addBalloonText:(NSString *)text atFrame:(CGRect)frame toView:(UIView *)view direction:(CZBalloonDirection)direction;
- (void)addBalloonText:(NSString *)text atFrame:(CGRect)frame toPoint:(CGPoint)point;

- (void)removeBolloomWithText:(NSString *)text;

- (void)dismiss;

@end

