//
//  CZLogPreviewViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLogPreviewViewController.h"

@interface CZLogPreviewViewController ()

@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) UITextView *textView;

@end

@implementation CZLogPreviewViewController

- (instancetype)initWithLogFile:(NSString *)path {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _path = [path copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.path.lastPathComponent;
    
    self.textView = [[UITextView alloc] initWithFrame:self.view.bounds];
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.textView.backgroundColor = [UIColor cz_gs10];
    self.textView.editable = NO;
    self.textView.font = [UIFont fontWithName:@"Menlo-Regular" size:12.0];
    self.textView.text = [NSString stringWithContentsOfFile:self.path encoding:NSUTF8StringEncoding error:nil];
    self.textView.textColor = [UIColor cz_gs80];
    self.textView.contentOffset = CGPointMake(0.0, CGFLOAT_MAX);
    [self.view addSubview:self.textView];
}

@end
