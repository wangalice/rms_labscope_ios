//
//  CZCameraView.h
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const CZCameraViewWillShowNotification;
extern NSString * const CZCameraViewWillHideNotification;

@class CZCamera;
@class CZCameraView;
@class CZVideoTool;
@protocol CZCameraDelegate;

@protocol CZCameraViewDelegate <NSObject>

@optional
- (void)cameraView:(CZCameraView *)cameraView didChangeReachability:(BOOL)isReachable;
- (void)cameraView:(CZCameraView *)cameraView didChangeSnappingStatus:(BOOL)isEnabled;

@end

@interface CZCameraView : UIView

@property (nonatomic, assign) id<CZCameraDelegate, CZCameraViewDelegate> delegate;
@property (nonatomic, retain) CZCamera *camera;
@property (nonatomic, retain, readonly) CZVideoTool *videoTool;

- (void)setCameraViewFrame:(CGRect)frame;
- (void)play;
- (void)stop;
- (void)pauseCamera;
- (void)resumeCamera;
- (void)beginSnap;
- (void)beginContinuousSnap:(NSUInteger)frameCount;
- (void)beginFastSnap;
- (void)forceAutoCameraMode;
- (void)restoreCameraMode;
- (void)enableAdvancedView:(BOOL)enable;
- (void)setAnnotationHidden:(BOOL)hidden;
- (void)setScaleBarHidden:(BOOL)hidden;
- (void)beginMeasuring;
- (void)beginMeasuringWithMeasurementHidden:(BOOL)hidden;
- (float)measuredValue;
- (void)endMeasuring;
- (void)updateFOV:(float)fov;

@end
