//
//  UIImagePickerController+Autorotate.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImagePickerController+Autorotate.h"

@implementation UIImagePickerController (Autorotate)

/*
 This is a bug in ios 6.0.x which will cause crash when loading UIImagePicker if shouldAutorotat returns YES and
 the device doesn't support portrate mode.  ios 6.1.x fixed it so the following code prevents crash in 6.0.x
 */
- (BOOL)shouldAutorotate {
    return NO;
}

@end
