//
//  CZImagePickerController.m
//  Hermes
//
//  Created by Ralph Jin on 7/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImagePickerController.h"
#import <Photos/Photos.h>

static NSString * const kCellID = @"CellID";

static const NSUInteger kItemRows = 4;
static const NSUInteger kItemColomns = 4;
static const CGFloat kImageItemWidth = 96.0;
static const CGFloat kImageItemHeight = kImageItemWidth;
static const CGFloat kItemPadding = 8;
static const CGFloat kCollectionViewMargin = 32;
static const NSUInteger kUpdateBatchCount = 10;
static const NSUInteger kThumbnailCacheCount = 60;

@interface CZImagePickerController ()

@property (nonatomic, retain) NSArray<NSURL *> *fileURLs;
@property (nonatomic, retain) NSCache *thumnailCache;
@property (nonatomic, retain) NSDictionary *visibleThumbnails;
@property (nonatomic, retain) NSArray<PHAsset *> *assets;
@property (nonatomic, retain) PHCachingImageManager *imageManager;
@property (nonatomic, assign) UILabel *noImageLabel;
@property (nonatomic, copy) NSString *tempFilePath;

@property (nonatomic, retain) NSTimer *updateDelayTimer;
@property (nonatomic, retain) NSMutableArray *updatePenddingRows;
@property (nonatomic, retain) NSMutableDictionary *thumbnailLoadingRows;  // For quick searching, set row as NSNumber for key, and leave value as [NSNull null].

@end

@implementation CZImagePickerController

- (instancetype)init {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(kImageItemWidth, kImageItemHeight)];
    [layout setSectionInset:UIEdgeInsetsMake(kCollectionViewMargin , kCollectionViewMargin, kCollectionViewMargin, kCollectionViewMargin)];
    layout.minimumInteritemSpacing = 8.0f;
    layout.minimumLineSpacing = 8.0f;
    
    if (self = [super initWithCollectionViewLayout:layout]) {
        _imageManager = [[PHCachingImageManager alloc] init];
        _updatePenddingRows = [[NSMutableArray alloc] init];
        _thumbnailLoadingRows = [[NSMutableDictionary alloc] init];
        self.preferredContentSize = CGSizeMake(485.0f, 485.0f);
    }
    
    return self;
}

- (void)dealloc {
    _tempFilePath = nil;
    
    [_updateDelayTimer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = L(@"FILES_LOCAL_TITLE");
    
    UICollectionView *collectionView = self.collectionView;
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kCellID];
    collectionView.backgroundColor = [UIColor cz_gs100];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    const CGFloat kHorzPadding = 20.0f;
    CGRect frame = self.view.frame;
    frame.origin.x += kHorzPadding;
    frame.size.width -= kHorzPadding * 2;
    self.noImageLabel.frame = frame;
}

#pragma mark - Private method

- (void)didCreateThumbnail:(UIImage *)thumbnail atRow:(NSUInteger)row {
    if (thumbnail) {
        NSURL *fileURL = self.fileURLs[row];
        [self.thumnailCache setObject:thumbnail forKey:fileURL];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            NSNumber *rowNumber = @(row);
            [self.thumbnailLoadingRows removeObjectForKey:rowNumber];
            [self.updatePenddingRows addObject:rowNumber];
            
            if (self.updatePenddingRows.count >= kUpdateBatchCount) {
                [self updateDelayTimerFired:nil];
            } else {
                [self.updateDelayTimer invalidate];
                self.updateDelayTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                         target:self
                                                                       selector:@selector(updateDelayTimerFired:)
                                                                       userInfo:nil
                                                                        repeats:NO];
            }
        });
    }
}

- (void)updateDelayTimerFired:(NSTimer *)timer {
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSNumber *row in self.updatePenddingRows) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[row unsignedIntegerValue] inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [self.updatePenddingRows removeAllObjects];
    
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
}

- (void)showNoImageLabel {
    CGSize viewSize = CGSizeMake(kItemRows * kImageItemWidth + kItemPadding * (kItemColomns - 1) + kCollectionViewMargin * 2,
                                 kItemRows * kImageItemHeight + kItemPadding * (kItemRows - 1) + kCollectionViewMargin * 2);
    if (self.fileURLs.count + self.assets.count == 0) {
        NSString *noImagesTitle = L(@"NO_IMAGES_TITLE");
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:noImagesTitle];
        [attributedText setAttributes:@{NSFontAttributeName:[UIFont cz_body1]} range:NSMakeRange(0, noImagesTitle.length)];
        
        UILabel *noImageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewSize.width, viewSize.height)];
        noImageLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        noImageLabel.backgroundColor = [UIColor clearColor];
        noImageLabel.attributedText = attributedText;
        noImageLabel.textColor = [UIColor grayColor];
        noImageLabel.numberOfLines = 10;
        noImageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        noImageLabel.contentMode = UIViewContentModeCenter;
        noImageLabel.userInteractionEnabled = NO;
        noImageLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.view addSubview:noImageLabel];
        self.noImageLabel = noImageLabel;
    }
}

# pragma mark - Private methods

/** Put visible thumbnails into container, so that it will not be deleted by the cache. */
- (void)updateVisibleThumbnails {
    NSArray *visibleIndexPaths = [self.collectionView indexPathsForVisibleItems];
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:visibleIndexPaths.count];
    
    // copy visible thumbnails to 'tempThumbnailCache'
    for (NSIndexPath *indexPath in visibleIndexPaths) {
        NSUInteger row = indexPath.row;
        if (row < [self.fileURLs count]) {
            NSURL *filePath = self.fileURLs[row];
            id image = [self.thumnailCache objectForKey:filePath];
            if (image) {
                tempDict[filePath] = image;
            }
        }
    }
    
    self.visibleThumbnails = tempDict;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger count = self.fileURLs.count + self.assets.count;
    if (count == 0) {
        [self showNoImageLabel];
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
    
    if (cell.backgroundView == nil) {
        UIImageView *view = [[UIImageView alloc] init];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds = YES;
        cell.backgroundView = view;
    }
    
    UIImageView *imageView = (UIImageView *)cell.backgroundView;
    
    [self updateVisibleThumbnails];
    
    if (indexPath.row < self.fileURLs.count) {
        NSInteger index = indexPath.row;
        NSURL *fileURL = self.fileURLs[index];
        
        UIImage *image = [self.visibleThumbnails objectForKey:fileURL];
        if (image == nil) {
            image = [self.thumnailCache objectForKey:fileURL];
        }

        if (image == nil) {
            id found = [self.thumbnailLoadingRows objectForKey:@(index)];
            if (found == nil) {
                [self.thumbnailLoadingRows setObject:[NSNull null] forKey:@(index)];

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
                    const CGFloat screenScale = [[UIScreen mainScreen] scale];
                    UIImage *image = [CZCommonUtils thumnailFromImageFileURL:fileURL atMinSize:kImageItemHeight * screenScale];
                    [self didCreateThumbnail:image atRow:index];
                });
            }
        }

        if (image == nil) {
            imageView.image = [UIImage imageNamed:A(@"report-add-image-icon")];
        } else {
            imageView.image = image;
        }
    } else {
        NSInteger index = indexPath.row - self.fileURLs.count;
        
        if (cell.tag != 0) {
            [self.imageManager cancelImageRequest:(PHImageRequestID)cell.tag];
        }
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.networkAccessAllowed = YES;
        
        cell.tag = [self.imageManager requestImageForAsset:self.assets[index] targetSize:CGSizeMake(kImageItemWidth, kImageItemHeight) contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            imageView.image = result;
        }];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(imagePickerController:didFinishPickingImageFilePath:)]) {
        // TODO: use same delegate as UIImagePickerControllerDelegate
        if (indexPath.row < self.fileURLs.count) {
            NSInteger index = indexPath.row;
            NSString *filePath = [self.fileURLs[index] path];
            [self.delegate imagePickerController:self didFinishPickingImageFilePath:filePath];
        } else {
            NSInteger index = indexPath.row - self.fileURLs.count;
            PHAsset *asset = self.assets[index];
            
            PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
            options.version = PHImageRequestOptionsVersionCurrent;  // Adjustment applied
            options.networkAccessAllowed = YES;
            
            [self.imageManager requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                NSArray<PHAssetResource *> *resouces = [PHAssetResource assetResourcesForAsset:asset];
                NSString *filename = resouces.firstObject.originalFilename ?: dataUTI;
                NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:filename];
                [imageData writeToFile:filePath atomically:YES];
                
                [self.delegate imagePickerController:self didFinishPickingImageFilePath:filePath];
                self.tempFilePath = filePath;
            }];
        }
    }
}

#pragma mark - Setter

- (void)setTempFilePath:(NSString *)tempFilePath {
    [[NSFileManager defaultManager] removeItemAtPath:_tempFilePath error:nil];
    
    _tempFilePath = [tempFilePath copy];
}

#pragma mark - Getter

- (NSArray<NSURL *> *)fileURLs {
    if (_fileURLs == nil) {
        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSMutableArray *tempFilePathArray = [NSMutableArray array];
        
        NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:documentPath];
        NSString *file;
        while (file = [dirEnum nextObject]) {
            CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[file pathExtension]];
            
            if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatPNG || fileFormat == kCZFileFormatTIF) {
                NSURL *url = [NSURL fileURLWithPath:[documentPath stringByAppendingPathComponent:file]];
                [tempFilePathArray addObject:url];
            }
        }
        _fileURLs = [[NSArray alloc] initWithArray:tempFilePathArray];
        
        NSCache *tempThumbnailCache = [[NSCache alloc] init];
        [tempThumbnailCache setCountLimit:kThumbnailCacheCount];
        self.thumnailCache = tempThumbnailCache;
    }
    return _fileURLs;
}

- (NSArray<PHAsset *> *)assets {
    if (_assets == nil) {
        NSMutableArray<PHAsset *> *assets = [NSMutableArray array];
        PHFetchResult *result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
        [result enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger index, BOOL *stop) {
            if ([asset isKindOfClass:[PHAsset class]]) {
                [assets addObject:asset];
            }
        }];
        _assets = [assets copy];
        
        [self.imageManager startCachingImagesForAssets:assets targetSize:CGSizeMake(kImageItemWidth, kImageItemHeight) contentMode:PHImageContentModeAspectFill options:nil];
    }
    return _assets;
}

@end
