//
//  CZImagePickerController.h
//  Hermes
//
//  Created by Ralph Jin on 7/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZImagePickerController;

@protocol CZImagePickerControllerDelegate <NSObject>

@optional
- (void)imagePickerController:(CZImagePickerController *)imagePicker didFinishPickingImageFilePath:(NSString *)filePath;

@end

@interface CZImagePickerController : UICollectionViewController

@property (nonatomic, weak) id<CZImagePickerControllerDelegate> delegate;

@end
