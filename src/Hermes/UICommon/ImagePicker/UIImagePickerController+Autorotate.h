//
//  UIImagePickerController+Autorotate.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImagePickerController (Autorotate)

@end

NS_ASSUME_NONNULL_END
