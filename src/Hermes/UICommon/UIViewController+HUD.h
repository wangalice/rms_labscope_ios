//
//  UIViewController+HUD.h
//  Hermes
//
//  Created by Li, Junlin on 3/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HUD)

@property (nonatomic, readonly, assign, getter=isShowingWaitingHUD) BOOL showingWaitingHUD;

- (void)showWaitingHUD:(BOOL)show;

@end
