//
//  CZDialogActionPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 6/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogAction.h"

@class CZAlertController;

@interface CZDialogAction ()

@property (nonatomic, weak) UIButton *button;
@property (nonatomic, copy) void (^handler)(CZDialogAction *action);

- (instancetype)initWithTitle:(NSString *)title style:(CZDialogActionStyle)style handler:(void (^)(CZDialogAction *action))handler;

@end

@interface UIButton (CZDialogAction)

+ (instancetype)buttonForDialogAction:(CZDialogAction *)action height:(CGFloat)height;
+ (UIStackView *)buttonsForDialogActions:(NSArray<CZDialogAction *> *)actions height:(CGFloat)height;

@end
