//
//  CZDialogPresentationController.h
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZDialogPresentationController : UIPresentationController

@property (nonatomic, assign) BOOL adjustsDialogPositionWhenKeyboardAppears;
@property (nonatomic, assign) BOOL dismissesDialogWhenOutsideTouched;

@end
