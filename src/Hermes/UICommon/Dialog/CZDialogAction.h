//
//  CZDialogAction.h
//  Hermes
//
//  Created by Li, Junlin on 6/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CZDialogActionStyle) {
    CZDialogActionStyleDefault,
    CZDialogActionStyleCancel,
    CZDialogActionStyleDestructive // Same with CZDialogActionStyleCancel in current version.
};

@interface CZDialogAction : NSObject

@property (nonatomic, copy) NSString *identifier;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, readonly, assign) CZDialogActionStyle style;

@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign) BOOL delaysActionHandler;         ///< Perform action hanlder after dialog dismissed.
@property (nonatomic, assign) BOOL disablesAutomaticDismissal;  ///< Only perform action handler, do not dismiss dialog.

@end
