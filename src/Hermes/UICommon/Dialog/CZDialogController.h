//
//  CZDialogController.h
//  Hermes
//
//  Created by Li, Junlin on 7/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZDialogAction.h"

/// An autolayout enabled view controller that presented modally.
/// Custom views should be added into content view, you can add width constraint and height constraint to content view to customize the dialog size.
/// Actions should be added before dialog's viewDidLoad called.
@interface CZDialogController : UIViewController <UIViewControllerTransitioningDelegate>

@property (nonatomic, readonly, strong) UIView *contentView;

@property (nonatomic, readonly, copy) NSArray<CZDialogAction *> *actions;
@property (nonatomic, assign) CGFloat actionHeight;

- (CZDialogAction *)addActionWithTitle:(NSString *)title style:(CZDialogActionStyle)style handler:(void (^)(__kindof CZDialogController *dialog, CZDialogAction *action))handler;

- (void)setHandler:(void (^)(__kindof CZDialogController *dialog, CZDialogAction *action))handler forActionWithIdentifier:(NSString *)identifier;

- (void)presentAnimated:(BOOL)animated completion:(void (^)(void))completion;

@end
