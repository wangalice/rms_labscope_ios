//
//  CZDialogAction.m
//  Hermes
//
//  Created by Li, Junlin on 6/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogAction.h"
#import "CZDialogActionPrivate.h"
#import <objc/runtime.h>

@implementation CZDialogAction

- (instancetype)initWithTitle:(NSString *)title style:(CZDialogActionStyle)style handler:(void (^)(CZDialogAction *))handler {
    self = [super init];
    if (self) {
        _title = [title copy];
        _style = style;
        _handler = [handler copy];
        _enabled = YES;
        _delaysActionHandler = NO;
        _disablesAutomaticDismissal = NO;
    }
    return self;
}

- (void)setTitle:(NSString *)title {
    _title = [title copy];
    [self.button setTitle:_title forState:UIControlStateNormal];
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    self.button.enabled = _enabled;
}

- (void)performAction {
    if (self.handler) {
        self.handler(self);
    }
}

@end

@implementation UIButton (CZDialogAction)

+ (void)load {
    UIBezierPath *rectPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0, 0.0, 1.0, 1.0)];
    [[CZShapeImage imageWithPath:rectPath fillColor:[UIColor cz_gs10]] setName:@"alert-button-normal"];
    [[CZShapeImage imageWithPath:rectPath fillColor:[UIColor cz_gs30]] setName:@"alert-button-highlighted"];
    [[CZShapeImage imageWithPath:rectPath fillColor:[[UIColor cz_gs10] colorWithAlphaComponent:0.5]] setName:@"alert-button-disabled"];
}

+ (instancetype)buttonForDialogAction:(CZDialogAction *)action height:(CGFloat)height {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.enabled = action.isEnabled;
    [button setTitle:action.title forState:UIControlStateNormal];
    [button addTarget:action action:@selector(performAction) forControlEvents:UIControlEventTouchUpInside];
    [button.heightAnchor constraintEqualToConstant:height].active = YES;
    
    [button setBackgroundImage:[[CZShapeImage imageNamed:@"alert-button-normal"] UIImage]
                      forState:UIControlStateNormal];
    [button setBackgroundImage:[[CZShapeImage imageNamed:@"alert-button-highlighted"] UIImage]
                      forState:UIControlStateHighlighted];
    [button setBackgroundImage:[[CZShapeImage imageNamed:@"alert-button-disabled"] UIImage]
                      forState:UIControlStateDisabled];
    
    switch (action.style) {
        case CZDialogActionStyleDefault:
            button.titleLabel.font = [UIFont cz_subtitle1];
            [button setTitleColor:[UIColor cz_pb100]
                         forState:UIControlStateNormal];
            [button setTitleColor:[[UIColor cz_pb100] colorWithAlphaComponent:0.5]
                         forState:UIControlStateDisabled];
            break;
        case CZDialogActionStyleCancel:
        case CZDialogActionStyleDestructive:
            button.titleLabel.font = [UIFont cz_body1];
            [button setTitleColor:[UIColor cz_gs100]
                         forState:UIControlStateNormal];
            [button setTitleColor:[[UIColor cz_gs100] colorWithAlphaComponent:0.5]
                         forState:UIControlStateDisabled];
            break;
    }
    
    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, button.bounds.size.width, 1.0)];
    topBorder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    topBorder.backgroundColor = [UIColor cz_gs40];
    [button addSubview:topBorder];
    
    action.button = button;
    
    return button;
}

+ (UIStackView *)buttonsForDialogActions:(NSArray<CZDialogAction *> *)actions height:(CGFloat)height {
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.distribution = UIStackViewDistributionFill;
    stackView.alignment = UIStackViewAlignmentFill;
    
    if (actions.count == 1 || actions.count > 2) {
        stackView.axis = UILayoutConstraintAxisVertical;
        for (CZDialogAction *action in actions) {
            UIButton *button = [self buttonForDialogAction:action height:height];
            [stackView addArrangedSubview:button];
        }
    } else if (actions.count == 2) {
        stackView.axis = UILayoutConstraintAxisHorizontal;
        UIButton *leftButton = [self buttonForDialogAction:actions[0] height:height];
        [stackView addArrangedSubview:leftButton];
        
        UIView *divider = [[UIView alloc] init];
        divider.backgroundColor = [UIColor cz_gs40];
        [divider.widthAnchor constraintEqualToConstant:1.0].active = YES;
        [stackView addArrangedSubview:divider];
        
        UIButton *rightButton = [self buttonForDialogAction:actions[1] height:height];
        [stackView addArrangedSubview:rightButton];
        
        [rightButton.widthAnchor constraintEqualToAnchor:leftButton.widthAnchor].active = YES;
    }
    
    return stackView;
}

@end
