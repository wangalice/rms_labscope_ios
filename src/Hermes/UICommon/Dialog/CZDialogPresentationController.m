//
//  CZDialogPresentationController.m
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogPresentationController.h"

@interface CZDialogPresentationController () <CZKeyboardObserver, UIGestureRecognizerDelegate>

@property (nonatomic, readonly, strong) UIView *dimmingView;
@property (nonatomic, readonly, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation CZDialogPresentationController

@synthesize dimmingView = _dimmingView;
@synthesize tapGestureRecognizer = _tapGestureRecognizer;

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController {
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if (self) {
        _adjustsDialogPositionWhenKeyboardAppears = YES;
        _dismissesDialogWhenOutsideTouched = NO;
    }
    return self;
}

- (UIView *)dimmingView {
    if (_dimmingView == nil) {
        _dimmingView = [[UIView alloc] init];
        _dimmingView.backgroundColor = [[UIColor cz_gs120] colorWithAlphaComponent:0.6];
    }
    return _dimmingView;
}

- (CGRect)frameOfPresentedViewInContainerView {
    CGFloat containerWidth = self.containerView.bounds.size.width;
    CGFloat containerHeight = self.containerView.bounds.size.height;
    CGFloat preferredWidth = self.presentedViewController.preferredContentSize.width;
    CGFloat preferredHeight = self.presentedViewController.preferredContentSize.height;
    CGRect frame = CGRectMake((containerWidth - preferredWidth) / 2, (containerHeight - preferredHeight) / 2, preferredWidth, preferredHeight);
    return frame;
}

- (void)presentationTransitionWillBegin {
    self.dimmingView.frame = self.containerView.bounds;
    [self.containerView insertSubview:self.dimmingView atIndex:0];
    
    self.dimmingView.alpha = 0.0;
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmingView.alpha = 1.0;
    } completion:nil];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
    if (!completed) {
        [self.dimmingView removeFromSuperview];
    } else {
        if (self.adjustsDialogPositionWhenKeyboardAppears) {
            [[CZKeyboardManager sharedManager] addObserver:self];
        }
        if (self.dismissesDialogWhenOutsideTouched) {
            [self.containerView addGestureRecognizer:self.tapGestureRecognizer];
        }
    }
}

- (void)dismissalTransitionWillBegin {
    [self.presentingViewController.transitionCoordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        self.dimmingView.alpha = 0.0;
    } completion:nil];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.dimmingView removeFromSuperview];
        if (self.adjustsDialogPositionWhenKeyboardAppears) {
            [[CZKeyboardManager sharedManager] removeObserver:self];
        }
        if (self.dismissesDialogWhenOutsideTouched) {
            [self.containerView removeGestureRecognizer:self.tapGestureRecognizer];
        }
    }
}

#pragma mark - CZKeyboardObserver

- (void)keyboardWillShow:(CZKeyboardTransition *)transition {
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        CGRect frame = self.containerView.superview.bounds;
        frame.size.height -= transition.frameEnd.size.height;
        self.containerView.frame = frame;
    } completion:nil];
}

- (void)keyboardWillHide:(CZKeyboardTransition *)transition {
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        self.containerView.frame = self.containerView.superview.bounds;
    } completion:nil];
}

#pragma mark - Tap Gesture Recognizer

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (_tapGestureRecognizer == nil) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        _tapGestureRecognizer.delegate = self;
    }
    return _tapGestureRecognizer;
}

- (void)tapGestureRecognizerAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint locationInPresentedView = [touch locationInView:self.presentedView];
    if (CGRectContainsPoint(self.presentedView.bounds, locationInPresentedView)) {
        return NO;
    } else {
        return YES;
    }
}

@end
