//
//  CZDialogDefaultDismissalAnimator.h
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZDialogDefaultDismissalAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@end
