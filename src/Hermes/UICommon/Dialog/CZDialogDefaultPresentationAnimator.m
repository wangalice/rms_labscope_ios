//
//  CZDialogDefaultPresentationAnimator.m
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogDefaultPresentationAnimator.h"

@implementation CZDialogDefaultPresentationAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.2;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *dialog = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    dialog.view.translatesAutoresizingMaskIntoConstraints = NO;
    [transitionContext.containerView addSubview:dialog.view];
    
    [dialog.view.centerXAnchor constraintEqualToAnchor:transitionContext.containerView.centerXAnchor].active = YES;
    [dialog.view.centerYAnchor constraintEqualToAnchor:transitionContext.containerView.centerYAnchor].active = YES;
    
    dialog.view.alpha = 0.0;
    
    CGAffineTransform transform = dialog.view.transform;
    dialog.view.layer.transform = CATransform3DMakeScale(0.9, 0.9, 0.0);
    
    [UIView animateKeyframesWithDuration:0.2 delay:0.0 options:0 animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.1 animations:^{
            dialog.view.alpha = 1.0;
        }];
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.2 animations:^{
            dialog.view.layer.transform = CATransform3DMakeScale(1.0, 1.0, 0.0);
        }];
    } completion:^(BOOL finished) {
        dialog.view.transform = transform;
        [transitionContext completeTransition:finished];
    }];
}

@end
