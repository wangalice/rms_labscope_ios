//
//  CZDialogController.m
//  Hermes
//
//  Created by Li, Junlin on 7/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogController.h"
#import "CZDialogActionPrivate.h"
#import "CZDialogPresentationController.h"
#import "CZDialogDefaultPresentationAnimator.h"
#import "CZDialogDefaultDismissalAnimator.h"
#import "UIWindow+CZPresentedViewController.h"

const CGFloat CZDialogControllerButtonHeight = 64.0;

@interface CZDialogController ()

@property (nonatomic, readonly, strong) UIStackView *dialogView;
@property (nonatomic, readwrite, copy) NSArray<CZDialogAction *> *actions;
@property (nonatomic, strong) NSMutableDictionary *handlers;

@end

@implementation CZDialogController

@synthesize dialogView = _dialogView;
@synthesize contentView = _contentView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _actions = [NSArray array];
        _actionHeight = CZDialogControllerButtonHeight;
        _handlers = [NSMutableDictionary dictionary];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs10];
    self.view.layer.cornerRadius = 8.0;
    self.view.layer.masksToBounds = YES;
    
    [self.view addSubview:self.dialogView];
    [self.dialogView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
    [self.dialogView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
    [self.dialogView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.dialogView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    
    [self.dialogView addArrangedSubview:self.contentView];
    
    if (self.actions.count > 0) {
        UIStackView *buttonsView = [UIButton buttonsForDialogActions:self.actions height:self.actionHeight];
        [self.dialogView addArrangedSubview:buttonsView];
    }
}

- (CZDialogAction *)addActionWithTitle:(NSString *)title style:(CZDialogActionStyle)style handler:(void (^)(__kindof CZDialogController *, CZDialogAction *))handler {
    @weakify(self);
    CZDialogAction *action = [[CZDialogAction alloc] initWithTitle:title style:style handler:^(CZDialogAction *action) {
        @strongify(self);
        void (^block)(void) = ^{
            @strongify(self);
            if (handler) {
                handler(self, action);
            }
            if (action.identifier) {
                void (^handler)(__kindof CZDialogController *, CZDialogAction *) = self.handlers[action.identifier];
                if (handler) {
                    handler(self, action);
                }
            }
        };
        if (action.disablesAutomaticDismissal) {
            block();
        } else {
            if (action.delaysActionHandler) {
                [self dismissViewControllerAnimated:YES completion:^{
                    block();
                }];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
                block();
            }
        }
    }];
    self.actions = [self.actions arrayByAddingObject:action];
    return action;
}

- (void)setHandler:(void (^)(__kindof CZDialogController *, CZDialogAction *))handler forActionWithIdentifier:(NSString *)identifier {
    if (handler == nil || identifier == nil) {
        return;
    }
    self.handlers[identifier] = handler;
}

- (void)presentAnimated:(BOOL)animated completion:(void (^)(void))completion {
    UIViewController *frontmostPresentedViewController = UIApplication.sharedApplication.keyWindow.cz_frontmostPresentedViewController;
    if ([frontmostPresentedViewController isKindOfClass:[CZDialogController class]]) {
        frontmostPresentedViewController = frontmostPresentedViewController.presentingViewController;
    }
    [frontmostPresentedViewController presentViewController:self animated:animated completion:completion];
}

#pragma mark - Views

- (UIStackView *)dialogView {
    if (_dialogView == nil) {
        _dialogView = [[UIStackView alloc] init];
        _dialogView.translatesAutoresizingMaskIntoConstraints = NO;
        _dialogView.axis = UILayoutConstraintAxisVertical;
        _dialogView.distribution = UIStackViewDistributionFill;
        _dialogView.alignment = UIStackViewAlignmentFill;
    }
    return _dialogView;
}

- (UIView *)contentView {
    if (_contentView == nil) {
        _contentView = [[UIView alloc] init];
        _contentView.translatesAutoresizingMaskIntoConstraints = NO;
        _contentView.backgroundColor = [UIColor clearColor];
    }
    return _contentView;
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[CZDialogDefaultPresentationAnimator alloc] init];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [[CZDialogDefaultDismissalAnimator alloc] init];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[CZDialogPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}

@end
