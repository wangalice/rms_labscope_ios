//
//  CZDialogDefaultDismissalAnimator.m
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDialogDefaultDismissalAnimator.h"

@implementation CZDialogDefaultDismissalAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.15;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *dialog = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        dialog.view.alpha = 0.0;
        dialog.view.transform = CGAffineTransformConcat(dialog.view.transform, CGAffineTransformMakeScale(0.9, 0.9));
    } completion:^(BOOL finished) {
        [dialog.view removeFromSuperview];
        [transitionContext completeTransition:finished];
    }];
}

@end
