//
//  CZButtonGroup.m
//  Hermes
//
//  Created by Ralph Jin on 7/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZButtonGroup.h"
#import "CZUIDefinition.h"

const static CGFloat kCZRadioButtonHeight = 40;
const static CGFloat kCZRatioButtonDefaultPadding = 0;
const static NSUInteger kZoomClickStopsCount = 20;

const static NSUInteger kCZIconImageViewTag = 2013;

@interface CZButtonGroup () {
    CZButtonGroupType _type;
    NSUInteger _numOfCurrentSelections;
}

@property (nonatomic, assign, getter = isMultiSelection) BOOL multiSelection;

@end

@implementation CZButtonGroup

- (id)initWithType:(CZButtonGroupType)type options:(NSArray *)options {
    _type = type;
    _sectionCount = 1;
    _buttonHeight = kCZRadioButtonHeight;
    self = [super init];

    if (self) {
        _numOfCurrentSelections = 0;
        _maxNumOfSelections = options.count;
        
        // Limit the zoom-click stop max count to 12.
        if (options.count == kZoomClickStopsCount) {
            _maxNumOfSelections = 12;
        }
        
        // Create radio buttons
        NSUInteger i = 0;
        for (NSString *title in options) {
            UIButton *button = (type == CZButtonGroupTypeRadio) ? [[CZRadioButton alloc] init] : [[CZCheckBox alloc] init];
            
            [button addTarget:self action:@selector(radioBoxTapped:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:title forState:UIControlStateNormal];
            button.tag = i;
            [self addSubview:button];
            
            i++;
        }
    }

    return self;
}

- (BOOL)isMultiSelection {
    return _type == CZButtonGroupTypeCheck;
}

- (void)selectOptionAtIndex:(NSUInteger)index {
    if (self.isMultiSelection) {
        if (index < self.subviews.count && _numOfCurrentSelections <= _maxNumOfSelections) {
            UIButton *button = (UIButton *)self.subviews[index];
            if (button.selected == NO) {
                _numOfCurrentSelections++;
            }
            button.selected = YES;
        }
    } else {
        for (UIButton *button in self.subviews) {
            button.selected = (index == button.tag);
        }
    }
}

- (void)deselectOptionAtIndex:(NSUInteger)index {
    if (index < self.subviews.count) {
        UIButton *button = (UIButton *)self.subviews[index];
        if (button.selected == YES) {
            _numOfCurrentSelections--;
        }
        button.selected = NO;
    }
}

- (void)clearSelection {
    for (UIButton *button in self.subviews) {
        button.selected = NO;
    }
    _numOfCurrentSelections = 0;
}

- (NSUInteger)selectionCount {
    NSUInteger selectionCount = 0;
    for (UIButton *button in self.subviews) {
        if (button.isSelected) {
            selectionCount ++;
        }
    }
    
    return selectionCount;
}

- (BOOL)isIndexSelected:(NSUInteger)index {
    if (index < self.subviews.count) {
        UIButton *button = (UIButton *)self.subviews[index];
        if (button.tag == index) {
            return button.isSelected;
        }
    }
    
    return NO;
}

- (void)setDisableAll:(BOOL)disableAll {
    _disableAll = disableAll;
    for (int i = 0; i < self.subviews.count; i++) {
        [self setEnabled:!disableAll atIndex:i];
    }
}

- (void)setEnabled:(BOOL)enabled atIndex:(NSUInteger)index {
    if (index < self.subviews.count) {
        UIButton *button = (UIButton *)self.subviews[index];
        if (button.tag == index) {
            button.enabled = enabled;
        }
    }
}

- (UIButton *)buttonAtIndex:(NSUInteger)index {
    if (index < self.subviews.count) {
        UIButton *button = (UIButton *)self.subviews[index];
        return button;
    } else {
        return nil;
    }
}

#pragma mark - override methods

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self repositionButtons];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (UIView *button in self.subviews) {
        for (UIView *sub in button.subviews) {
            if (sub.tag == kCZIconImageViewTag) {
                CGRect frame = sub.frame;
                frame.origin.y = (button.frame.size.height - frame.size.height) / 2;
                sub.frame = frame;
            }
        }
    }
    
}

#pragma mark - private methods

// Put buttons in two rows.
- (void)repositionButtons {
    NSUInteger i = 0;
    NSUInteger j = 0;
    CGFloat x = 0;
    CGFloat y = 0;
    NSUInteger sectionCount = ([self.subviews count] - 1) / _sectionCount + 1;
    CGFloat sectionWidth = self.frame.size.width / _sectionCount;
    
    for (UIButton *button in self.subviews) {
        button.frame = CGRectMake(x, y, sectionWidth, _buttonHeight);
        y += _buttonHeight + kCZRatioButtonDefaultPadding;
        i++;
        j++;
        
        if (j >= sectionCount) {
            x += sectionWidth;
            y = 0;
            j = 0;
        }
    }
}

- (void)radioBoxTapped:(id)sender {
    UIButton *tappedButton = sender;
    
    if (self.isMultiSelection) {
        BOOL selected = tappedButton.isSelected ? NO : YES;
        if (selected && _numOfCurrentSelections >= _maxNumOfSelections) {
            if ([self.delegate respondsToSelector:@selector(buttonGroupWillShowWarningMessage:)]) {
                [self.delegate buttonGroupWillShowWarningMessage:self];
            }
            return;
        }
        tappedButton.selected = selected;
        if (selected) {
            _numOfCurrentSelections++;
            if ([self.delegate respondsToSelector:@selector(buttonGroup:didSelectIndex:)]) {
                [self.delegate buttonGroup:self didSelectIndex:tappedButton.tag];
            }
        } else {
            _numOfCurrentSelections--;
            if ([self.delegate respondsToSelector:@selector(buttonGroup:didDeselectIndex:)]) {
                [self.delegate buttonGroup:self didDeselectIndex:tappedButton.tag];
            }
        }
    } else {
        if (tappedButton.isSelected) {
            return;
        }
        
        for (UIButton *button in self.subviews) {
            button.selected = NO;
        }
        
        tappedButton.selected = YES;
        
        if ([self.delegate respondsToSelector:@selector(buttonGroup:didSelectIndex:)]) {
            [self.delegate buttonGroup:self didSelectIndex:tappedButton.tag];
        }
    }
}

@end
