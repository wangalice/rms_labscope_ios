//
//  CZCameraViewController.h
//  Matscope
//
//  Created by Mike Wang on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZCameraInterface/CZCameraInterface.h>

typedef NS_ENUM(NSInteger, CZCameraViewControllerAction) {
    CZCameraViewControllerActionCustom,
    CZCameraViewControllerActionSnapSingleResolution,
    CZCameraViewControllerActionSnapAllResolutions
};

@class CZCameraViewController;

@protocol CZCameraViewControllerDelegate <NSObject>

@optional
- (void)cameraViewController:(CZCameraViewController *)cameraViewController didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error;
- (void)cameraViewController:(CZCameraViewController *)cameraViewController performCustomActionWithCompletionHandler:(void (^)(BOOL success))completionHandler;

@end

@interface CZCameraViewController : UIViewController

@property (nonatomic, readonly, strong) CZCamera *camera;
@property (nonatomic, readonly, assign) CZCameraViewControllerAction action;
@property (nonatomic, weak) id<CZCameraViewControllerDelegate> delegate;
@property (nonatomic, assign, getter=isAutoCameraModeEnabled) BOOL autoCameraModeEnabled;

- (instancetype)initWithCamera:(CZCamera *)camera action:(CZCameraViewControllerAction)action;

@end
