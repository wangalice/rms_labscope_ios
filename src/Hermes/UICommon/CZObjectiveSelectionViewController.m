//
//  CZObjectiveSelectionViewController.m
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZObjectiveSelectionViewController.h"
#import "CZGridView.h"
#import "CZNosepiecePositionButton.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@interface CZObjectiveSelectionViewController ()

@property (nonatomic, readonly, strong) CZGridView *nosepiecePositionButtonGridView;

@end

@implementation CZObjectiveSelectionViewController

@synthesize nosepiecePositionButtonGridView = _nosepiecePositionButtonGridView;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _microscopeModel = microscopeModel;
        
        NSUInteger positions = self.microscopeModel.nosepiece.positions;
        CGFloat preferredContentWidth = 32.0 + CZNosepiecePositionButtonSize * positions + 16.0 * (positions - 1) + 32.0;
        CGFloat preferredContentHeight = 32.0 + CZNosepiecePositionButtonSize + 32.0;
        self.preferredContentSize = CGSizeMake(preferredContentWidth, preferredContentHeight);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.nosepiecePositionButtonGridView];
    [self reloadNosepiecePositions];
}

- (void)reloadNosepiecePositions {
    [self.nosepiecePositionButtonGridView removeAllContentViews];
    
    for (NSInteger position = 0; position < self.microscopeModel.nosepiece.positions; position++) {
        CZObjective *objective = [self.microscopeModel.nosepiece objectiveAtPosition:position];
        
        CZNosepiecePositionButton *button = [[CZNosepiecePositionButton alloc] initWithPosition:position magnification:objective.magnification displayMagnification:objective.displayMagnification];
        button.tag = position;
        [button addTarget:self action:@selector(nosepiecePositionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"ObjectiveSelectionButton";
        
        [self.nosepiecePositionButtonGridView addContentView:button];
    }
    [self setCurrentPosition:self.microscopeModel.nosepiece.currentPosition];
}

- (void)setCurrentPosition:(NSUInteger)position {
    for (UIButton *button in self.nosepiecePositionButtonGridView.contentViews) {
        button.selected = (button.tag == position);
    }
}

#pragma mark - Views

- (CZGridView *)nosepiecePositionButtonGridView {
    if (_nosepiecePositionButtonGridView == nil) {
        CZGridRow *row = [[CZGridRow alloc] initWithHeight:CZNosepiecePositionButtonSize alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = @[row];
        
        CZGridColumn *column = [[CZGridColumn alloc] initWithWidth:CZNosepiecePositionButtonSize alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = [NSArray cz_arrayWithRepeatedObject:column count:7];
        
        _nosepiecePositionButtonGridView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0, 704.0, CZNosepiecePositionButtonSize) rows:rows columns:columns];
        _nosepiecePositionButtonGridView.columnSpacing = 16.0;
    }
    return _nosepiecePositionButtonGridView;
}

#pragma mark - Actions

- (void)nosepiecePositionButtonAction:(CZButton *)button {
    NSInteger position = button.tag;
    [self setCurrentPosition:position];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(objectiveSelectionViewController:didSelectObjectiveAtPosition:)]) {
        [self.delegate objectiveSelectionViewController:self didSelectObjectiveAtPosition:position];
    }
}

@end
