//
//  UIWindow+CZPresentedViewController.h
//  Hermes
//
//  Created by Li, Junlin on 7/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (CZPresentedViewController)

@property (nonatomic, readonly, strong) UIViewController *cz_frontmostPresentedViewController;

@end
