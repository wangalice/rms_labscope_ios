//
//  CZInputAlertController.m
//  Hermes
//
//  Created by Li, Junlin on 6/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZInputAlertController.h"
#import "CZDialogActionPrivate.h"

const CGFloat CZInputAlertControllerButtonHeight = 48.0;

@interface CZInputAlertController ()

@property (nonatomic, readonly, strong) UIView *levelBar;
@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, strong) UIView *titleView;
@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UIView *messageView;
@property (nonatomic, readonly, strong) UILabel *messageLabel;
@property (nonatomic, readonly, strong) UIView *errorMessageView;
@property (nonatomic, readonly, strong) UIImageView *errorMessageIcon;
@property (nonatomic, readonly, strong) UILabel *errorMessageLabel;

@property (nonatomic, strong) NSLayoutConstraint *errorMessageViewHeightConstraint;

@property (nonatomic, readwrite, copy) NSArray<UILabel *> *textFieldLabels;
@property (nonatomic, readwrite, copy) NSArray<CZTextField *> *textFields;
@property (nonatomic, readwrite, copy) NSArray<CZCheckBox *> *checkBoxes;

@end

@implementation CZInputAlertController

@dynamic title;
@synthesize levelBar = _levelBar;
@synthesize stackView = _stackView;
@synthesize titleView = _titleView;
@synthesize titleLabel = _titleLabel;
@synthesize messageView = _messageView;
@synthesize messageLabel = _messageLabel;
@synthesize errorMessageView = _errorMessageView;
@synthesize errorMessageIcon = _errorMessageIcon;
@synthesize errorMessageLabel = _errorMessageLabel;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message {
    return [[self alloc] initWithTitle:title message:message];
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message {
    self = [super init];
    if (self) {
        self.title = title;
        self.actionHeight = CZInputAlertControllerButtonHeight;
        _message = [message copy];
        _textFieldLabels = [NSArray array];
        _textFields = [NSArray array];
        _checkBoxes = [NSArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentView addSubview:self.levelBar];
    [self.contentView addSubview:self.stackView];
    
    [self.stackView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor].active = YES;
    [self.stackView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor].active = YES;
    [self.stackView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:27.0].active = YES;
    [self.stackView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-16.0].active = YES;
    [self.stackView.widthAnchor constraintEqualToConstant:448.0].active = YES;
    
    if (self.title.length > 0) {
        [self.stackView addArrangedSubview:self.titleView];
        [self.titleView addSubview:self.titleLabel];
        self.titleLabel.text = self.title;
        
        [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.titleView.leadingAnchor constant:32.0].active = YES;
        [self.titleLabel.trailingAnchor constraintEqualToAnchor:self.titleView.trailingAnchor constant:-32.0].active = YES;
        [self.titleLabel.topAnchor constraintEqualToAnchor:self.titleView.topAnchor].active = YES;
        [self.titleLabel.bottomAnchor constraintEqualToAnchor:self.titleView.bottomAnchor constant:-11.0].active = YES;
        [self.titleLabel.heightAnchor constraintEqualToConstant:20.0].active = YES;
    }
    
    if (self.message.length > 0) {
        [self.stackView addArrangedSubview:self.messageView];
        [self.messageView addSubview:self.messageLabel];
        self.messageLabel.text = self.message;
        
        [self.messageLabel.leadingAnchor constraintEqualToAnchor:self.messageView.leadingAnchor constant:32.0].active = YES;
        [self.messageLabel.trailingAnchor constraintEqualToAnchor:self.messageView.trailingAnchor constant:-32.0].active = YES;
        [self.messageLabel.topAnchor constraintEqualToAnchor:self.messageView.topAnchor].active = YES;
        [self.messageLabel.bottomAnchor constraintEqualToAnchor:self.messageView.bottomAnchor].active = YES;
    }
    
    if (self.textFields.count > 0 || self.checkBoxes.count > 0) {
        [self addControls];
    }
    
    [self addErrorMessageView];
}

#pragma mark - Public

- (void)setTitle:(NSString *)title {
    [super setTitle:title];
    self.titleLabel.text = title;
}

- (void)setMessage:(NSString *)message {
    _message = [message copy];
    self.messageLabel.text = message;
}

- (void)setErrorMessage:(NSString *)errorMessage {
    _errorMessage = [errorMessage copy];
    
    [UIView animateWithDuration:0.25 animations:^{
        if (errorMessage.length == 0) {
            self.errorMessageViewHeightConstraint.constant = 0.0;
            self.errorMessageIcon.hidden = YES;
            self.errorMessageLabel.hidden = YES;
            self.errorMessageLabel.text = nil;
            [self.view layoutIfNeeded];
        } else {
            self.errorMessageViewHeightConstraint.constant = 24.0;
            self.errorMessageIcon.hidden = NO;
            self.errorMessageLabel.hidden = NO;
            self.errorMessageLabel.text = errorMessage;
            [self.view layoutIfNeeded];
        }
    }];
}

- (void)addTextFieldWithLabel:(NSString *)label configurationHandler:(void (^)(CZTextField *))configurationHandler {
    UILabel *textFieldLabel = [[UILabel alloc] init];
    textFieldLabel.translatesAutoresizingMaskIntoConstraints = NO;
    textFieldLabel.font = [UIFont cz_label1];
    textFieldLabel.text = label;
    textFieldLabel.textColor = [UIColor cz_gs80];
    textFieldLabel.textAlignment = NSTextAlignmentLeft;
    self.textFieldLabels = [self.textFieldLabels arrayByAddingObject:textFieldLabel];
    
    CZTextField *textField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
    textField.translatesAutoresizingMaskIntoConstraints = NO;
    if (configurationHandler) {
        configurationHandler(textField);
    }
    self.textFields = [self.textFields arrayByAddingObject:textField];
}

- (void)addCheckBoxWithConfigurationHandler:(void (^)(CZCheckBox *))configurationHandler {
    CZCheckBox *checkBox = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
    checkBox.translatesAutoresizingMaskIntoConstraints = NO;
    [checkBox addTarget:self action:@selector(checkBoxAction:) forControlEvents:UIControlEventTouchUpInside];
    if (configurationHandler) {
        configurationHandler(checkBox);
    }
    self.checkBoxes = [self.checkBoxes arrayByAddingObject:checkBox];
}

#pragma mark - Private

- (void)addControls {
    NSInteger numberOfTextFields = self.textFields.count;
    for (NSInteger index = 0; index < numberOfTextFields; index++) {
        UIView *view = [self viewForControl];
        [self.stackView addArrangedSubview:view];
        [view.heightAnchor constraintEqualToConstant:32.0].active = YES;
        
        UILabel *textFieldLabel = self.textFieldLabels[index];
        [view addSubview:textFieldLabel];
        [textFieldLabel.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:32.0].active = YES;
        [textFieldLabel.topAnchor constraintEqualToAnchor:view.topAnchor].active = YES;
        [textFieldLabel.bottomAnchor constraintEqualToAnchor:view.bottomAnchor].active = YES;
        [textFieldLabel.widthAnchor constraintEqualToConstant:128.0].active = YES;
        
        CZTextField *textField = self.textFields[index];
        [view addSubview:textField];
        [textField.leadingAnchor constraintEqualToAnchor:textFieldLabel.trailingAnchor constant:16.0].active = YES;
        [textField.trailingAnchor constraintEqualToAnchor:view.trailingAnchor constant:-32.0].active = YES;
        [textField.topAnchor constraintEqualToAnchor:view.topAnchor].active = YES;
        [textField.bottomAnchor constraintEqualToAnchor:view.bottomAnchor].active = YES;
    }
    
    for (CZCheckBox *checkBox in self.checkBoxes) {
        UIView *view = [self viewForControl];
        [self.stackView addArrangedSubview:view];
        [view.heightAnchor constraintEqualToConstant:32.0].active = YES;
        
        checkBox.translatesAutoresizingMaskIntoConstraints = NO;
        [view addSubview:checkBox];
        [checkBox.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:32.0].active = YES;
        [checkBox.topAnchor constraintEqualToAnchor:view.topAnchor].active = YES;
        [checkBox.bottomAnchor constraintEqualToAnchor:view.bottomAnchor].active = YES;
    }
}

- (UIView *)viewForControl {
    UIView *view = [[UIView alloc] init];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)addErrorMessageView {
    [self.stackView addArrangedSubview:self.errorMessageView];
    self.errorMessageViewHeightConstraint = [self.errorMessageView.heightAnchor constraintEqualToConstant:0.0];
    self.errorMessageViewHeightConstraint.active = YES;
    
    [self.errorMessageView addSubview:self.errorMessageIcon];
    [self.errorMessageIcon.leadingAnchor constraintEqualToAnchor:self.errorMessageView.leadingAnchor constant:32.0].active = YES;
    [self.errorMessageIcon.centerYAnchor constraintEqualToAnchor:self.errorMessageView.centerYAnchor].active = YES;
    
    [self.errorMessageView addSubview:self.errorMessageLabel];
    [self.errorMessageLabel.leadingAnchor constraintEqualToAnchor:self.errorMessageIcon.trailingAnchor constant:8.0].active = YES;
    [self.errorMessageLabel.trailingAnchor constraintEqualToAnchor:self.errorMessageView.trailingAnchor constant:-32.0].active = YES;
    [self.errorMessageLabel.centerYAnchor constraintEqualToAnchor:self.errorMessageView.centerYAnchor].active = YES;
}

#pragma mark - Views

- (UIView *)levelBar {
    if (_levelBar == nil) {
        _levelBar = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.contentView.bounds.size.width, 8.0)];
        _levelBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _levelBar.backgroundColor = [UIColor cz_gs90];
    }
    return _levelBar;
}

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.spacing = 16.0;
    }
    return _stackView;
}

- (UIView *)titleView {
    if (_titleView == nil) {
        _titleView = [[UIView alloc] init];
        _titleView.translatesAutoresizingMaskIntoConstraints = NO;
        _titleView.backgroundColor = [UIColor clearColor];
    }
    return _titleView;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.textColor = [UIColor cz_gs100];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.numberOfLines = 1;
    }
    return _titleLabel;
}

- (UIView *)messageView {
    if (_messageView == nil) {
        _messageView = [[UIView alloc] init];
        _messageView.translatesAutoresizingMaskIntoConstraints = NO;
        _messageView.backgroundColor = [UIColor clearColor];
    }
    return _messageView;
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.font = [UIFont cz_caption];
        _messageLabel.textColor = [UIColor cz_gs80];
        _messageLabel.textAlignment = NSTextAlignmentLeft;
        _messageLabel.numberOfLines = 0;
    }
    return _messageLabel;
}

- (UIView *)errorMessageView {
    if (_errorMessageView == nil) {
        _errorMessageView = [[UIView alloc] init];
        _errorMessageView.translatesAutoresizingMaskIntoConstraints = NO;
        _errorMessageView.backgroundColor = [UIColor clearColor];
    }
    return _errorMessageView;
}

- (UIImageView *)errorMessageIcon {
    if (_errorMessageIcon == nil) {
        _errorMessageIcon = [[UIImageView alloc] init];
        _errorMessageIcon.translatesAutoresizingMaskIntoConstraints = NO;
        _errorMessageIcon.hidden = YES;
        [_errorMessageIcon setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        [_errorMessageIcon cz_setImageWithIcon:[CZIcon iconNamed:@"error"]];
    }
    return _errorMessageIcon;
}

- (UILabel *)errorMessageLabel {
    if (_errorMessageLabel == nil) {
        _errorMessageLabel = [[UILabel alloc] init];
        _errorMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _errorMessageLabel.hidden = YES;
        _errorMessageLabel.font = [UIFont cz_label1];
        _errorMessageLabel.textColor = [UIColor cz_gs85];
        [_errorMessageLabel setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    }
    return _errorMessageLabel;
}

#pragma mark - Actions

- (void)checkBoxAction:(CZCheckBox *)checkBox {
    checkBox.selected = !checkBox.isSelected;
}

@end
