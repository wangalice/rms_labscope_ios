//
//  CZAlertController.h
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZDialogController.h"

typedef NS_ENUM(NSInteger, CZAlertLevel) {
    CZAlertLevelInfo,
    CZAlertLevelWarning,
    CZAlertLevelError
};

@interface CZAlertController : CZDialogController

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, readonly, assign) CZAlertLevel level;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message level:(CZAlertLevel)level;
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message level:(CZAlertLevel)level;

@property (nonatomic, readonly, copy) NSArray<CZCheckBox *> *checkBoxes;
- (void)addCheckBoxWithConfigurationHandler:(void (^)(CZCheckBox *checkBox))configurationHandler;

@end
