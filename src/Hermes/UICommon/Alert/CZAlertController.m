//
//  CZAlertController.m
//  Hermes
//
//  Created by Li, Junlin on 4/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAlertController.h"
#import "CZDialogActionPrivate.h"

@interface CZAlertController ()

@property (nonatomic, readonly, strong) UIView *levelBar;
@property (nonatomic, readonly, strong) UIImageView *levelIcon;
@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, strong) UIView *titleView;
@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UIView *messageView;
@property (nonatomic, readonly, strong) UILabel *messageLabel;

@property (nonatomic, readwrite, copy) NSArray<CZCheckBox *> *checkBoxes;

@end

@implementation CZAlertController

@dynamic title;
@synthesize levelBar = _levelBar;
@synthesize levelIcon = _levelIcon;
@synthesize stackView = _stackView;
@synthesize titleView = _titleView;
@synthesize titleLabel = _titleLabel;
@synthesize messageView = _messageView;
@synthesize messageLabel = _messageLabel;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message level:(CZAlertLevel)level {
    return [[self alloc] initWithTitle:title message:message level:level];
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message level:(CZAlertLevel)level {
    self = [super init];
    if (self) {
        self.title = title;
        _message = [message copy];
        _level = level;
        _checkBoxes = [NSArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentView addSubview:self.levelBar];
    [self.contentView addSubview:self.levelIcon];
    [self.contentView addSubview:self.stackView];
    
    [self.stackView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor].active = YES;
    [self.stackView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor].active = YES;
    [self.stackView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:96.0].active = YES;
    [self.stackView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
    [self.stackView.widthAnchor constraintEqualToConstant:448.0].active = YES;
    
    if (self.title.length > 0) {
        [self.stackView addArrangedSubview:self.titleView];
        [self.titleView addSubview:self.titleLabel];
        self.titleLabel.text = self.title;
        
        [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.titleView.leadingAnchor constant:32.0].active = YES;
        [self.titleLabel.trailingAnchor constraintEqualToAnchor:self.titleView.trailingAnchor constant:-32.0].active = YES;
        [self.titleLabel.topAnchor constraintEqualToAnchor:self.titleView.topAnchor].active = YES;
        [self.titleLabel.bottomAnchor constraintEqualToAnchor:self.titleView.bottomAnchor constant:-40.0].active = YES;
        [self.titleLabel.heightAnchor constraintEqualToConstant:48.0].active = YES;
    }
    
    if (self.message.length > 0) {
        [self.stackView addArrangedSubview:self.messageView];
        [self.messageView addSubview:self.messageLabel];
        self.messageLabel.text = self.message;
        
        [self.messageLabel.leadingAnchor constraintEqualToAnchor:self.messageView.leadingAnchor constant:32.0].active = YES;
        [self.messageLabel.trailingAnchor constraintEqualToAnchor:self.messageView.trailingAnchor constant:-32.0].active = YES;
        [self.messageLabel.topAnchor constraintEqualToAnchor:self.messageView.topAnchor].active = YES;
        [self.messageLabel.bottomAnchor constraintEqualToAnchor:self.messageView.bottomAnchor constant:-48.0].active = YES;
    }
    
    if (self.checkBoxes.count > 0) {
        [self addControls];
    }
}

#pragma mark - Public

- (void)setTitle:(NSString *)title {
    [super setTitle:title];
    self.titleLabel.text = title;
}

- (void)setMessage:(NSString *)message {
    _message = [message copy];
    self.messageLabel.text = message;
}

- (void)addCheckBoxWithConfigurationHandler:(void (^)(CZCheckBox *))configurationHandler {
    CZCheckBox *checkBox = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
    [checkBox addTarget:self action:@selector(checkBoxAction:) forControlEvents:UIControlEventTouchUpInside];
    if (configurationHandler) {
        configurationHandler(checkBox);
    }
    self.checkBoxes = [self.checkBoxes arrayByAddingObject:checkBox];
}

#pragma mark - Private

- (void)addControls {
    for (CZCheckBox *checkBox in self.checkBoxes) {
        UIView *view = [self viewForControl];
        [self.stackView addArrangedSubview:view];
        [view.heightAnchor constraintEqualToConstant:48.0].active = YES;
        
        checkBox.translatesAutoresizingMaskIntoConstraints = NO;
        [view addSubview:checkBox];
        [checkBox.topAnchor constraintEqualToAnchor:view.topAnchor].active = YES;
        [checkBox.centerXAnchor constraintEqualToAnchor:view.centerXAnchor].active = YES;
    }
}

- (UIView *)viewForControl {
    UIView *view = [[UIView alloc] init];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Views

- (UIView *)levelBar {
    if (_levelBar == nil) {
        _levelBar = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.contentView.bounds.size.width, 8.0)];
        _levelBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
        switch (self.level) {
            case CZAlertLevelInfo:
                _levelBar.backgroundColor = [UIColor cz_pb100];
                break;
            case CZAlertLevelWarning:
                _levelBar.backgroundColor = [UIColor cz_sy110];
                break;
            case CZAlertLevelError:
                _levelBar.backgroundColor = [UIColor cz_sr110];
                break;
        }
    }
    return _levelBar;
}

- (UIImageView *)levelIcon {
    if (_levelIcon == nil) {
        _levelIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 32.0, 32.0)];
        _levelIcon.center = CGPointMake(self.contentView.bounds.size.width / 2.0, 48.0 + 32.0 / 2.0);
        _levelIcon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        switch (self.level) {
            case CZAlertLevelInfo:
                _levelIcon.image = [[CZIcon iconNamed:@"alert-level-info"] image];
                break;
            case CZAlertLevelWarning:
                _levelIcon.image = [[CZIcon iconNamed:@"alert-level-warning"] image];
                break;
            case CZAlertLevelError:
                _levelIcon.image = [[CZIcon iconNamed:@"alert-level-error"] image];
                break;
        }
    }
    return _levelIcon;
}

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
    }
    return _stackView;
}

- (UIView *)titleView {
    if (_titleView == nil) {
        _titleView = [[UIView alloc] init];
        _titleView.translatesAutoresizingMaskIntoConstraints = NO;
        _titleView.backgroundColor = [UIColor clearColor];
    }
    return _titleView;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont cz_h1];
        _titleLabel.textColor = [UIColor cz_gs120];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.numberOfLines = 1;
    }
    return _titleLabel;
}

- (UIView *)messageView {
    if (_messageView == nil) {
        _messageView = [[UIView alloc] init];
        _messageView.translatesAutoresizingMaskIntoConstraints = NO;
        _messageView.backgroundColor = [UIColor clearColor];
    }
    return _messageView;
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.font = [UIFont cz_body1];
        _messageLabel.textColor = [UIColor cz_gs80];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.numberOfLines = 0;
    }
    return _messageLabel;
}

#pragma mark - Actions

- (void)checkBoxAction:(CZCheckBox *)checkBox {
    checkBox.selected = !checkBox.isSelected;
}

@end
