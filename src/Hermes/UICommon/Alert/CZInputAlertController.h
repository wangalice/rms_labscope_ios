//
//  CZInputAlertController.h
//  Hermes
//
//  Created by Li, Junlin on 6/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZDialogController.h"

@interface CZInputAlertController : CZDialogController

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *errorMessage;

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message;
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message;

@property (nonatomic, readonly, copy) NSArray<UILabel *> *textFieldLabels;
@property (nonatomic, readonly, copy) NSArray<CZTextField *> *textFields;
- (void)addTextFieldWithLabel:(NSString *)label configurationHandler:(void (^)(CZTextField *textField))configurationHandler;

@property (nonatomic, readonly, copy) NSArray<CZCheckBox *> *checkBoxes;
- (void)addCheckBoxWithConfigurationHandler:(void (^)(CZCheckBox *checkBox))configurationHandler;

@end
