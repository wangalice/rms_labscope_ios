//
//  CZUIDefinition.h
//  Hermes
//
//  Created by Mike Wang on 4/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef Hermes_CZUIDefinition_h
#define Hermes_CZUIDefinition_h

#define kUISystemStatusBarHeight 20

//CZFileViewController UI
#define kFileListSeperatorWidth 2
#define kUIButtonWidth 100.0
#define kUIButtonHeight 44.0
#define KUIButtonCap 10.0
#define kUINavigationBarHeight 44.0
#define kUIStatusBarHeight 30.0
#define kUIFileServerButtonHeight 40
#define kUIProgressViewWidth 500
#define kUIProgressViewHeight 240

//CZLiveViewController UI
#define kUISnapButtonDiameter (80.0)
#define kUISnapButtonRadius (kUISnapButtonDiameter / 2.0)
#define kUISnapSelectButtonWidth (72.0)
#define kUISnapSelectButtonHeight (62.0)
#define kUISnapSelectButtonOffsetX ((kUISnapButtonDiameter - kUISnapSelectButtonWidth) / 2.0)
#define kUISnapSelectButtonOffsetY (28.0)
#define kUISnapOptionButtonWidth (60.0)
#define kUISnapOptionButtonHeight (45.0)
#define kUISnapOptionButtonOffsetX (6.0)
#define kUISnapOptionButtonOffsetY (7.0)
#define kUISnapButtonXPadding (15)
#define kUIRecordingTipYPadding (50)
#define kUIImageViewX (80)
#define kUIImageViewY kUISystemStatusBarHeight
#define kUIImageViewWidth (944)
#define kUIImageViewHeight (748)
#define kUIFilenameLabelXPadding (20.0)
#define kUIDisplayOptionsButtonSize (48.0)
#define kUIDisplayOptionsButtonYPadding 0.0f
#define kUIDisplayOptionsViewYPadding kUIDisplayOptionsButtonYPadding + 10.0
#define kUIDisplayOptionButtonPadding (5.0)
#define kUIDisplayOptionButtonSize 72.0
#define kUIDisplayOptionLEDButtonHeight (20.0)
#define kUIFocusIndicatorHeight 20.0
#define kUIFocusIndicatorXPadding (60.0)
#define kUIFocusIndicatorYPadding (690.0)
#define kUIDrawingTubeSliderHeight 20.0
#define kUIDrawingTubeSliderXPadding (60.0)
#define kUIDrawingTubeSliderYPadding (725.0)

//CZImageViewController UI
#define kUIColorSizeLabelHeight 20
#define kUIColorSizeLabelWidth 90
#define kUIColorSizeLabelPadding 20
#define kUIAppearanceButtonHeight 48
#define kUIAppearanceButtonWidth 48
#define kUIAppearanceViewWidth 430
#define kUIAppearanceViewHeight 224

//CZTabBarController UI
#define kUITabBarButtonHeight (60)
#define kUITabBarWidth (80.0)
#define kUITabBarHeight (4*kUITabBarButtonHeight + 2*kUISystemStatusBarHeight)
#define kUITabBarSeperatorWidth (1.0)
#define kUIZeissLogoSize (64.0)
#define kUISplitViewButtonWidth 32
#define kUISplitViewButtonHeight 32
#define kUISplitViewButtonXPadding 5
#define kUISplitViewButtonYPadding 10

//dark

#define kDefaultBackGroundColor ([UIColor colorWithRed:69.0/255.0 green:77.0/255.0 blue:98.0/255.0 alpha:1.0])
#define kDefaultLabelColor      ([UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:212.0/255.0 alpha:1.0])
#define kDefaultLabelShadowColor [UIColor blackColor]
#define kDefaultToastMessageLabelColor      ([UIColor whiteColor])
#define kDefaultButtonTitleColorNormal [UIColor lightGrayColor]
#define kDefaultButtonTitleColorSelected [UIColor whiteColor]
#define kDefaultButtonTitleColorDisabled [UIColor grayColor]
#define kDefaultGroupButtonTitleColorNormal [UIColor whiteColor]
#define kDefaultGroupButtonTitleColorDisable [UIColor lightGrayColor]
#define kMicroscopeButtonBackgroundColor [UIColor blackColor]
#define kDefaultTitleColor ([UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:212.0/255.0 alpha:1.0])
#define kDefaultBarButtonItemTitleColor ([UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:212.0/255.0 alpha:1.0])
#define kDefaultBarButtonItemTitleDisabledColor [UIColor lightGrayColor]
#define kDefaultAlertViewBackgroundColor [UIColor colorWithRed:1/255.0 green:21/255.0 blue:54/255.0 alpha:0.9]
#define kDefaultAlertViewBackgroundStrokeColor [UIColor colorWithWhite:1.0 alpha:0.7]
#define kDefaultAlertViewTextColor [UIColor whiteColor]
#define kDefaultAlertViewTextShadowColor [UIColor blackColor]
#define kDefaultAlertViewButtonTextColor kDefaultAlertViewTextColor
#define kDefaultAlertViewButtonTextShadowColor kDefaultAlertViewTextShadowColor
#define kDefaultSeperatorColor [UIColor darkGrayColor]
#define kSpinnerBackgroundColor ([UIColor colorWithRed:0.153 green:0.18 blue:0.24 alpha:1.0])
#define kFileNameLabelTextColor [UIColor whiteColor]
#define kDefaultBarTintColor ([UIColor colorWithRed:80.0/255.0 green:89.0/255.0 blue:114.0/255.0 alpha:1.0])
#define kDefaultPercentLabColor  ([UIColor blackColor])


#define kProgressLabelColor ([UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:212.0/255.0 alpha:1.0])

#define kDefaultSwithColor      ([UIColor colorWithRed:34.0/255.0 green:38.0/255.0 blue:53.0/255.0 alpha:1.0])

#define kDefaultDividerColor    ([UIColor colorWithRed:155.0/255.0 green:159.0/255.0 blue:173.0/255.0 alpha:1.0])

#define kWhatsNewPageBottomViewBackGroundColor ([UIColor colorWithRed:203.0/255.0 green:206.0/255.0 blue:212.0/255.0 alpha:1.0])

#define kLastSnapImageButtonBorderColor ([UIColor colorWithRed:53.0/255.0 green:79.0/255.0 blue:160.0/255.0 alpha:1.0])

#define CZTableViewCellSelectButtonSize 40.0
#define CZTableViewCellHeight 67.0
#define CZTableViewCellLabelPadding 5
#define CZTableViewCellLabelStartX 130
#define CZTableViewCellLabelHeight 30
#define CTableViewCellAccessoryButtonWidth 30
#define CZTableViewThumbnailViewPadding 5
#define CZTableViewSelectImageViewWidth 20
#define CZTableViewCellThumbnailHeight 64.0
#define CZTableViewCellThumbnailWidth (CZTableViewCellThumbnailHeight * 4.0 / 3.0)
#define CZTableViewSelectAllButtonWidth 120
#define CZTableViewCellGeneralPadding 1.5

#define kParticlesTableViewHeight 30
#define kParticlesTableHeaderViewWithRangeHeight 84
#define kParticlesTableHeaderViewHeight 64
#define kParticlesTableFooterViewHeight 28

#endif
