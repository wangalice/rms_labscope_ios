//
//  CZZoomClickStopSelectionViewController.m
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZZoomClickStopSelectionViewController.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

static const CGFloat kGeneralXPadding = 20.0;
static const CGFloat kMagnificationLabelWidth = 40.0;
static const CGFloat kTapRectHeight = 60.0;

@interface CZZoomClickStopSelectionViewController ()

@property (nonatomic, readonly, strong) CZSlider *zoomSlider;
@property (nonatomic, readonly, strong) UILabel *magnificationLabel; //zoom * objective
@property (nonatomic, readonly, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign) CGFloat sliderStep;

@property (nonatomic, strong) NSMutableArray *zoomStepMarkers;
@property (nonatomic, strong) NSMutableArray *magnificationLabels;

@end

@implementation CZZoomClickStopSelectionViewController

@synthesize zoomSlider = _zoomSlider;
@synthesize magnificationLabel = _magnificationLabel;
@synthesize tapGestureRecognizer = _tapGestureRecognizer;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModelStereo *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _microscopeModel = microscopeModel;
        _zoomStepMarkers = [[NSMutableArray alloc] init];
        _magnificationLabels = [[NSMutableArray alloc] init];
        
        self.preferredContentSize = CGSizeMake(565.0, 124.0);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.zoomSlider];
    [self.view addSubview:self.magnificationLabel];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self reloadZoomClickStops];
}

- (void)reloadZoomClickStops {
    for (UIView *view in self.magnificationLabels) {
        [view removeFromSuperview];
    }
    
    for (UIView *view in self.zoomStepMarkers) {
        [view removeFromSuperview];
    }
    
    [self.zoomStepMarkers removeAllObjects];
    [self.magnificationLabels removeAllObjects];
    
    if ([self.microscopeModel zoomClickStopPositions] <= 1) {
        self.zoomSlider.hidden = YES;
        self.magnificationLabel.hidden = YES;
        return;
    }
    
    NSInteger stepCount = [self.microscopeModel zoomClickStopPositions] - 1;
    self.sliderStep = (self.zoomSlider.bounds.size.width - 8.0 * 2) / stepCount;
    
    self.zoomSlider.hidden = NO;
    
    for (NSUInteger position = 0; position < [self.microscopeModel zoomClickStopPositions]; position++) {
        UIView *markerView = [[UIView alloc] init];
        markerView.backgroundColor = [UIColor cz_gs120];
        markerView.frame = CGRectMake(position * self.sliderStep + 32.0 + 8.0, 49.0, 1.0, 4.0);
        [self.zoomStepMarkers addObject:markerView];
        [self.view addSubview:markerView];
        
        UILabel *label = [[UILabel alloc] init];
        label.backgroundColor = [UIColor clearColor];
        label.frame = CGRectMake(markerView.frame.origin.x - kMagnificationLabelWidth / 2, 56.0, kMagnificationLabelWidth, 8.0);
        label.font = [UIFont cz_label2];
        label.text = @([[self.microscopeModel zoomClickStopAtPosition:position] magnification]).stringValue;
        label.textColor = [UIColor cz_gs80];
        label.textAlignment = NSTextAlignmentCenter;
        [self.magnificationLabels addObject:label];
        [self.view addSubview:label];
    }
    
    [self.view bringSubviewToFront:self.zoomSlider];
    [self.zoomSlider setMaximumValue:self.microscopeModel.zoomClickStopPositions - 1];
    [self.zoomSlider setValue:self.microscopeModel.currentZoomClickStopPosition];
    [self updateMagnificationLabelWithPosition:self.microscopeModel.currentZoomClickStopPosition];
}

- (void)tapGestureHandle:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.view];
    CGFloat x = p.x;
    NSUInteger position = 0;
    if (x < CGRectGetMinX(self.zoomSlider.frame)) {
        position = 0;
    } else if (x > CGRectGetMaxX(self.zoomSlider.frame)) {
        position = [self.microscopeModel zoomClickStopPositions] - 1;
    } else {
        NSUInteger j = (NSUInteger)(x - kGeneralXPadding) % (NSUInteger)self.sliderStep;
        if (j > self.sliderStep * 0.6) {
            position = ceil((x - kGeneralXPadding) / self.sliderStep);
        } else if (j < self.sliderStep * 0.4) {
            position = floor((x - kGeneralXPadding) / self.sliderStep);
        }
    }
    
    // the orgin.x is keep consistent with magnification label
    CGRect tapSensitiveRect = CGRectMake(position * _sliderStep + kGeneralXPadding + 11 - kMagnificationLabelWidth / 2, 0, kMagnificationLabelWidth, kTapRectHeight);
    
    if (CGRectContainsPoint(tapSensitiveRect, p)) {
        [self.zoomSlider setValue:position];
        [self updateMagnificationLabelWithPosition:position];
        if (self.delegate && [self.delegate respondsToSelector:@selector(zoomClickStopSelectionViewController:didSelectZoomClickStopAtPosition:)]) {
            [self.delegate zoomClickStopSelectionViewController:self didSelectZoomClickStopAtPosition:position];
        }
    }
}

#pragma mark - Lazy-load objects

- (CZSlider *)zoomSlider {
    if (_zoomSlider == nil) {
        _zoomSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        _zoomSlider.frame = CGRectMake(32.0, 32.0, self.view.bounds.size.width - 32.0 * 2, 30.0);
        _zoomSlider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _zoomSlider.minimumValue = 0.0;
        [_zoomSlider addTarget:self action:@selector(zoomSliderValueChanged:) forControlEvents:(UIControlEventValueChanged)]; //Change the current zoom level
        [_zoomSlider addTarget:self action:@selector(zoomSliderTouchUp:) forControlEvents:UIControlEventTouchUpInside];
        [_zoomSlider addTarget:self action:@selector(zoomSliderTouchUp:) forControlEvents:UIControlEventTouchUpOutside];
        [_zoomSlider addTarget:self action:@selector(zoomSliderTouchUp:) forControlEvents:UIControlEventTouchCancel];
    }
    return _zoomSlider;
}

- (UILabel *)magnificationLabel {
    if (_magnificationLabel == nil) {
        _magnificationLabel = [[UILabel alloc] init];
        _magnificationLabel.frame = CGRectMake(32.0, 78.0, self.view.bounds.size.width - 32.0 * 2, 16.0);
        _magnificationLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _magnificationLabel.backgroundColor = [UIColor clearColor];
        _magnificationLabel.font = [UIFont cz_caption];
        _magnificationLabel.numberOfLines = 1;
        _magnificationLabel.textColor = [UIColor cz_gs80];
    }
    return _magnificationLabel;
}

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (_tapGestureRecognizer == nil) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureHandle:)];
        _tapGestureRecognizer.numberOfTapsRequired = 1;
    }
    return _tapGestureRecognizer;
}

#pragma mark - Slider event handlers

- (void)zoomSliderValueChanged:(id)sender {
    double value = _zoomSlider.value;
    int calculatedValue = round(value);
    [_zoomSlider setValue:calculatedValue];
    [self updateMagnificationLabelWithPosition:calculatedValue];
}

- (void)zoomSliderTouchUp:(id)sender {
    double value = _zoomSlider.value;
    int calculatedValue = round(value);
    UISlider *slider = (UISlider*) sender;
    if (self.delegate && [self.delegate respondsToSelector:@selector(zoomClickStopSelectionViewController:didSelectZoomClickStopAtPosition:)]) {
        [self.delegate zoomClickStopSelectionViewController:self didSelectZoomClickStopAtPosition:slider.value];
    }
    [self updateMagnificationLabelWithPosition:calculatedValue];
}

#pragma mark - Private methods

- (void)updateMagnificationLabelWithPosition:(NSUInteger)position {
    NSString *magnification = [self.microscopeModel displayMagnificationAtPosition:position localized:YES];
    self.magnificationLabel.text = [NSString stringWithFormat:@"%@%@%@", L(@"LIVE_MAGNIFICATION"), L(@"COLON_AND_SPACE"), magnification];
}

@end
