//
//  CZPickerViewController.m
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZPickerViewController.h"
#import "CZPickerViewItemCell.h"
#import "UIStackView+CZCustomSpacing.h"
#import "CZDialogActionPrivate.h"

NSString * const CZPickerViewControllerCancelActionIdentifier = @"Cancel";
NSString * const CZPickerViewControllerSaveActionIdentifier = @"Save";

@interface CZPickerViewController () <UITableViewDataSource, UITableViewDelegate, CZTextFieldDelegate>

@property (nonatomic, readwrite, copy) id selectedItem;
@property (nonatomic, readwrite, assign) NSUInteger selectedItemIndex;
@property (nonatomic, strong) NSMutableArray *filteredItems;

@property (nonatomic, readonly, strong) UIView *headerView;

@property (nonatomic, readonly, strong) UIStackView *stackView;
@property (nonatomic, readonly, strong) UILabel *selectedItemLabel;
@property (nonatomic, readonly, strong) CZTextField *searchTextField;
@property (nonatomic, readonly, strong) UITableView *filteredItemsView;
@property (nonatomic, readonly, strong) UILabel *noResultsLabel;

@end

@implementation CZPickerViewController

@synthesize headerView = _headerView;

@synthesize stackView = _stackView;
@synthesize selectedItemTitleLabel = _selectedItemTitleLabel;
@synthesize selectedItemLabel = _selectedItemLabel;
@synthesize selectableItemsTitleLabel = _selectableItemsTitleLabel;
@synthesize searchTextField = _searchTextField;
@synthesize filteredItemsView = _filteredItemsView;
@synthesize noResultsLabel = _noResultsLabel;

- (instancetype)initWithSelectedItem:(id)selectedItem selectableItems:(NSArray *)selectableItems {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _selectedItem = [selectedItem copy];
        _selectedItemIndex = [selectableItems indexOfObject:selectedItem];
        _selectableItems = [selectableItems copy];
        _filteredItems = [NSMutableArray array];
        
        CZDialogAction *cancelAction = [self addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        cancelAction.identifier = CZPickerViewControllerCancelActionIdentifier;
        
        CZDialogAction *saveAction = [self addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleCancel handler:nil];
        saveAction.identifier = CZPickerViewControllerSaveActionIdentifier;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentView.widthAnchor constraintEqualToConstant:458.0].active = YES;
    [self.contentView.heightAnchor constraintEqualToConstant:498.0].active = YES;
    
    [self.contentView addSubview:self.headerView];
    
    [self.contentView addSubview:self.stackView];
    [self.stackView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:32.0].active = YES;
    [self.stackView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-32.0].active = YES;
    [self.stackView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:35.0].active = YES;
    [self.stackView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-32.0].active = YES;
    
    [self.stackView addArrangedSubview:self.selectedItemTitleLabel];
    [self.stackView addArrangedSubview:self.selectedItemLabel];
    [self.stackView addArrangedSubview:self.selectableItemsTitleLabel];
    [self.stackView addArrangedSubview:self.searchTextField];
    [self.stackView addArrangedSubview:self.filteredItemsView];
    [self.stackView cz_addCustomSpacing:3.0 afterView:self.selectedItemTitleLabel];
    [self.stackView cz_addCustomSpacing:22.0 afterView:self.selectedItemLabel];
    
    [self.filteredItemsView addSubview:self.noResultsLabel];
    [self.noResultsLabel.centerXAnchor constraintEqualToAnchor:self.filteredItemsView.centerXAnchor].active = YES;
    [self.noResultsLabel.centerYAnchor constraintEqualToAnchor:self.filteredItemsView.centerYAnchor].active = YES;
    
    self.selectedItemLabel.text = [self titleForItem:self.selectedItem];
    
    [self.filteredItems addObjectsFromArray:self.selectableItems];
    [self.filteredItemsView reloadData];
    self.noResultsLabel.hidden = self.filteredItems.count > 0;
    
    [self updateSaveButtonAppearance];
    
    CZDialogAction *cancelAction = self.actions[0];
    cancelAction.button.isAccessibilityElement = YES;
    cancelAction.button.accessibilityIdentifier = @"CancelButton";
    
    CZDialogAction *saveAction = self.actions[1];
    saveAction.button.isAccessibilityElement = YES;
    saveAction.button.accessibilityIdentifier = @"SaveButton";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.selectedItemIndex != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedItemIndex inSection:0];
        [self.filteredItemsView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:animated];
    }
}

#pragma mark - Views

- (UIView *)headerView {
    if (_headerView == nil) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.contentView.bounds.size.width, 8.0)];
        _headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _headerView.backgroundColor = [UIColor cz_pb100];
    }
    return _headerView;
}

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.backgroundColor = [UIColor clearColor];
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
    }
    return _stackView;
}

- (CZUppercaseLabel *)selectedItemTitleLabel {
    if (_selectedItemTitleLabel == nil) {
        _selectedItemTitleLabel = [[CZUppercaseLabel alloc] init];
        [_selectedItemTitleLabel.heightAnchor constraintEqualToConstant:16.0].active = YES;
        
        _selectedItemTitleLabel.isAccessibilityElement = YES;
        _selectedItemTitleLabel.accessibilityIdentifier = @"SelectedItemTitleLabel";
    }
    return _selectedItemTitleLabel;
}

- (UILabel *)selectedItemLabel {
    if (_selectedItemLabel == nil) {
        _selectedItemLabel = [[UILabel alloc] init];
        _selectedItemLabel.backgroundColor = [UIColor clearColor];
        _selectedItemLabel.font = [UIFont cz_h3];
        _selectedItemLabel.textColor = [UIColor cz_gs100];
        [_selectedItemLabel.heightAnchor constraintEqualToConstant:30.0].active = YES;
        
        _selectedItemLabel.isAccessibilityElement = YES;
        _selectedItemLabel.accessibilityIdentifier = @"SelectedItemLabel";
    }
    return _selectedItemLabel;
}

- (CZUppercaseLabel *)selectableItemsTitleLabel {
    if (_selectableItemsTitleLabel == nil) {
        _selectableItemsTitleLabel = [[CZUppercaseLabel alloc] init];
        [_selectableItemsTitleLabel.heightAnchor constraintEqualToConstant:16.0].active = YES;
        
        _selectableItemsTitleLabel.isAccessibilityElement = YES;
        _selectableItemsTitleLabel.accessibilityIdentifier = @"SelectableItemsTitleLabel";
    }
    return _selectableItemsTitleLabel;
}

- (CZTextField *)searchTextField {
    if (_searchTextField == nil) {
        _searchTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightImagePicker:^UIImage *(CZTheme theme) {
            return [[CZIcon iconNamed:@"search"] imageForControlWithSize:CZIconSizeSmall theme:CZThemeLight state:UIControlStateNormal];
        }];
        _searchTextField.delegate = self;
        _searchTextField.showClearButtonWhileEditing = YES;
        _searchTextField.placeholder = L(@"SEARCH_HINT");
        [_searchTextField addTarget:self action:@selector(searchTextFieldAction:) forControlEvents:UIControlEventEditingChanged];
        [_searchTextField.heightAnchor constraintEqualToConstant:33.0].active = YES;
        
        _searchTextField.isAccessibilityElement = YES;
        _searchTextField.accessibilityIdentifier = @"SearchTextBox";
    }
    return _searchTextField;
}

- (UITableView *)filteredItemsView {
    if (_filteredItemsView == nil) {
        _filteredItemsView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _filteredItemsView.backgroundColor = [UIColor cz_gs20];
        _filteredItemsView.dataSource = self;
        _filteredItemsView.delegate = self;
        _filteredItemsView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _filteredItemsView.rowHeight = 49.0;
        _filteredItemsView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
        [_filteredItemsView registerClass:[CZPickerViewItemCell class] forCellReuseIdentifier:NSStringFromClass([CZPickerViewItemCell class])];
    }
    return _filteredItemsView;
}

- (UILabel *)noResultsLabel {
    if (_noResultsLabel == nil) {
        _noResultsLabel = [[UILabel alloc] init];
        _noResultsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _noResultsLabel.backgroundColor = [UIColor clearColor];
        _noResultsLabel.font = [UIFont cz_body1];
        _noResultsLabel.textColor = [UIColor cz_gs80];
        _noResultsLabel.text = L(@"NO_RESULTS_FOUND");
        
        _noResultsLabel.isAccessibilityElement = YES;
        _noResultsLabel.accessibilityIdentifier = @"NoResultsLabel";
    }
    return _noResultsLabel;
}

#pragma mark - Actions

- (void)searchTextFieldAction:(id)sender {
    [self.filteredItems removeAllObjects];
    if (self.searchTextField.text.length == 0) {
        [self.filteredItems addObjectsFromArray:self.selectableItems];
    } else {
        for (id item in self.selectableItems) {
            NSString *itemTitle = [self titleForItem:item];
            if ([itemTitle rangeOfString:self.searchTextField.text options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self.filteredItems addObject:item];
            }
        }
    }
    [self.filteredItemsView reloadData];
    self.noResultsLabel.hidden = self.filteredItems.count > 0;
}

#pragma mark - Private

- (NSString *)titleForItem:(id)item {
    if (self.titleForItem) {
        return self.titleForItem(self, item);
    } else {
        return [item description];
    }
}

- (void)updateSaveButtonAppearance {
    CZDialogAction *saveAction = self.actions[1];
    saveAction.enabled = self.selectedItem != nil;
    if (self.saveButtonTitleForItem) {
        saveAction.title = self.saveButtonTitleForItem(self, self.selectedItem);
    } else {
        saveAction.title = L(@"SAVE");
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZPickerViewItemCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CZPickerViewItemCell class]) forIndexPath:indexPath];
    cell.itemLabel.text = [self titleForItem:self.filteredItems[indexPath.row]];
    if (indexPath.row == self.selectedItemIndex) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    cell.isAccessibilityElement = YES;
    cell.accessibilityIdentifier = @"SelectableItemView";
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedItem = self.filteredItems[indexPath.row];
    self.selectedItemIndex = [self.selectableItems indexOfObject:self.selectedItem];
    self.selectedItemLabel.text = [self titleForItem:self.selectedItem];
    [self updateSaveButtonAppearance];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
