//
//  CZPickerViewItemCell.m
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZPickerViewItemCell.h"

@interface CZPickerViewItemCell ()

@property (nonatomic, readonly, strong) UIView *separatorView;

@end

@implementation CZPickerViewItemCell

@synthesize itemLabel = _itemLabel;
@synthesize separatorView = _separatorView;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.contentView addSubview:self.itemLabel];
        [self.contentView addSubview:self.separatorView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.contentView.backgroundColor = [UIColor cz_pb100];
        self.itemLabel.font = [UIFont cz_subtitle1];
        self.itemLabel.textColor = [UIColor cz_gs10];
    } else {
        self.contentView.backgroundColor = [UIColor cz_gs20];
        self.itemLabel.font = [UIFont cz_body1];
        self.itemLabel.textColor = [UIColor cz_gs100];
    }
}

- (UILabel *)itemLabel {
    if (_itemLabel == nil) {
        _itemLabel = [[UILabel alloc] initWithFrame:CGRectMake(8.0, 0.0, self.contentView.bounds.size.width - 8.0 * 2, self.contentView.bounds.size.height - 1.0)];
        _itemLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _itemLabel.backgroundColor = [UIColor clearColor];
        _itemLabel.font = [UIFont cz_body1];
        _itemLabel.textColor = [UIColor cz_gs100];
    }
    return _itemLabel;
}

- (UIView *)separatorView {
    if (_separatorView == nil) {
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width, 1.0)];
        _separatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separatorView.backgroundColor = [UIColor cz_gs60];
    }
    return _separatorView;
}

@end
