//
//  CZPickerViewController.h
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZUppercaseLabel.h"
#import "CZDialogController.h"

extern NSString * const CZPickerViewControllerCancelActionIdentifier;
extern NSString * const CZPickerViewControllerSaveActionIdentifier;

@class CZPickerViewController;

@interface CZPickerViewController<ItemType> : CZDialogController

// The item must conform to NSCopying protocol.
@property (nonatomic, readonly, copy) ItemType selectedItem;
@property (nonatomic, readonly, assign) NSUInteger selectedItemIndex;
@property (nonatomic, readonly, copy) NSArray<ItemType> *selectableItems;

@property (nonatomic, readonly, strong) CZUppercaseLabel *selectedItemTitleLabel;
@property (nonatomic, readonly, strong) CZUppercaseLabel *selectableItemsTitleLabel;

// The default value of item's title is [item description].
@property (nonatomic, copy) NSString *(^titleForItem)(CZPickerViewController<ItemType> *picker, ItemType item);
// The default save button title is "Save".
@property (nonatomic, copy) NSString *(^saveButtonTitleForItem)(CZPickerViewController<ItemType> *picker, ItemType item);

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithSelectedItem:(ItemType)selectedItem selectableItems:(NSArray<ItemType> *)selectableItems NS_DESIGNATED_INITIALIZER;

@end
