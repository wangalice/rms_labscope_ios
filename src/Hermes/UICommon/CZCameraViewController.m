//
//  CZCameraViewController.m
//  Matscope
//
//  Created by Mike Wang on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>

#import "CZCameraViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "MBProgressHUD+Hide.h"
#import "CZAppDelegate.h"
#import "CZCameraView.h"

@interface CZCameraViewController () <CZCameraDelegate, CZCameraViewDelegate> {
    SystemSoundID shutterSound;
    CZCameraSnapResolutionPreset _originalSnapResolutionPreset;
    dispatch_semaphore_t _semaphore;
}

@property (nonatomic, readonly, strong) CZCameraView *cameraView;
@property (nonatomic, readonly, strong) UIButton *snapButton;
@property (nonatomic, readonly, strong) MBProgressHUD *loadingHUD;

@end

@implementation CZCameraViewController

@synthesize cameraView = _cameraView;
@synthesize snapButton = _snapButton;
@synthesize loadingHUD = _loadingHUD;

- (instancetype)initWithCamera:(CZCamera *)camera action:(CZCameraViewControllerAction)action {
    self = [super init];
    if (self) {
        _camera = camera;
        _action = action;
        _originalSnapResolutionPreset = [camera snapResolutionPreset];
    }
    return self;
}

- (void)dealloc {
    [_camera setSnapResolutionPreset:_originalSnapResolutionPreset];
    _cameraView.delegate = nil;
    
    AudioServicesDisposeSystemSoundID(shutterSound);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.camera.displayName ?: self.camera.macAddress;
    self.view.backgroundColor = [UIColor cz_gs105];
    
    [self.view addSubview:self.cameraView];
    [self.view addSubview:self.snapButton];
    
    NSString *shutterSoundPath = [[NSBundle mainBundle] pathForResource:@"shutter_click" ofType:@"wav" inDirectory:@"Assets"];
    NSURL *shutterSoundUrl = [[NSURL alloc] initWithString:shutterSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)shutterSoundUrl, &shutterSound);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGRect frame = self.view.bounds;
    [self.cameraView setCameraViewFrame:frame];
    
    self.snapButton.frame = CGRectMake(frame.size.width - self.snapButton.frame.size.width - 48.0,
                                       (frame.size.height - self.snapButton.frame.size.height) / 2.0,
                                       self.snapButton.frame.size.width,
                                       self.snapButton.frame.size.height);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.snapButton.enabled = NO;

    [self.cameraView play];
    [self.cameraView setScaleBarHidden:YES];
    
    [self.view bringSubviewToFront:self.snapButton];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.isAutoCameraModeEnabled) {
        self.autoCameraModeEnabled = NO;
    }
    
    [self.cameraView setScaleBarHidden:NO];
    [self.cameraView stop];
}

- (void)setAutoCameraModeEnabled:(BOOL)autoCameraModeEnabled {
    _autoCameraModeEnabled = autoCameraModeEnabled;
    if (autoCameraModeEnabled) {
        [self.cameraView forceAutoCameraMode];
    } else {
        [self.cameraView restoreCameraMode];
    }
}

#pragma mark - Views

- (CZCameraView *)cameraView {
    if (_cameraView == nil) {
        _cameraView = [[CZCameraView alloc] init];
        _cameraView.camera = self.camera;
        _cameraView.delegate = self;
        [_cameraView enableAdvancedView:NO];
    }
    return _cameraView;
}

- (UIButton *)snapButton {
    if (_snapButton == nil) {
        _snapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _snapButton.frame = CGRectMake(0.0, 0.0, 96.0, 96.0);
        _snapButton.exclusiveTouch = YES;
        _snapButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapButton setImage:[[CZIcon iconNamed:@"snap-option-photo"] imageForControlWithTheme:CZThemeLight state:UIControlStateNormal] forState:UIControlStateNormal];
        [_snapButton setBackgroundImage:[[CZIcon iconNamed:@"snap-button-background"] image] forState:UIControlStateNormal];
        [_snapButton addTarget:self action:@selector(snapButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _snapButton;
}

- (MBProgressHUD *)loadingHUD {
    if (_loadingHUD == nil) {
        _loadingHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [_loadingHUD setColor:kSpinnerBackgroundColor];
        [_loadingHUD setLabelText:L(@"BUSY_INDICATOR_PLEASE_WAIT")];
    }
    return _loadingHUD;
}

#pragma mark - Actions

- (void)snapButtonAction:(id)sender {
    AudioServicesPlaySystemSound(shutterSound);
    
    switch (self.action) {
        case CZCameraViewControllerActionCustom: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(cameraViewController:performCustomActionWithCompletionHandler:)]) {
                [self.loadingHUD show:YES];
                [self.delegate cameraViewController:self performCustomActionWithCompletionHandler:^(BOOL success) {
                    [self.loadingHUD hide:YES];
                    if (success) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }
            break;
        }
        case CZCameraViewControllerActionSnapSingleResolution: {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                _semaphore = dispatch_semaphore_create(0);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.cameraView beginSnap];
                });
                dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            });
            break;
        }
        case CZCameraViewControllerActionSnapAllResolutions: {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                _semaphore = dispatch_semaphore_create(0);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.cameraView beginFastSnap];
                });
                dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.camera.snapResolutionPreset = kCZCameraSnapResolutionLow;
                    [self.cameraView beginSnap];
                });
                dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
                
                // If high resolution is different from low resolution, snap high resolution
                if (!CGSizeEqualToSize([self.camera highSnapResolution], [self.camera lowSnapResolution])) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.camera.snapResolutionPreset = kCZCameraSnapResolutionHigh;
                        [self.cameraView beginSnap];
                    });
                    dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
                }
                
                // If live resolution is different from snap resolution, fast snap a image
//                    if (!CGSizeEqualToSize([self.camera liveResolution], [self.camera lowSnapResolution]) && !CGSizeEqualToSize([self.camera liveResolution], [self.camera highSnapResolution])) {
//
//                    }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            });
            break;
        }
    }
}

#pragma mark - CZCameraViewDelegate

- (void)cameraView:(CZCameraView *)cameraView didChangeReachability:(BOOL)isReachable {
    self.snapButton.enabled = isReachable;
}

- (void)cameraView:(CZCameraView *)cameraView didChangeSnappingStatus:(BOOL)isEnabled {
    self.snapButton.enabled = isEnabled;
}

#pragma mark - CZCameraDelegate

- (void)camera:(CZCamera *)camera didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cameraViewController:didFinishSnapping:error:)]) {
        [self.delegate cameraViewController:self didFinishSnapping:snappingInfo error:error];
    }
    dispatch_semaphore_signal(_semaphore);
}

@end
