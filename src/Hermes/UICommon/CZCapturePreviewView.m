//
//  CZCapturePreviewView.m
//  Labscope
//
//  Created by Ralph Jin on 11/17/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import "CZCapturePreviewView.h"

@implementation CZCapturePreviewView

+ (Class)layerClass {
    return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session {
    return [(AVCaptureVideoPreviewLayer *)[self layer] session];
}

- (void)setSession:(AVCaptureSession *)session {
    [(AVCaptureVideoPreviewLayer *)[self layer] setSession:session];
}

- (void)setVideoOrientation:(UIInterfaceOrientation)orientation {
    if (orientation != UIInterfaceOrientationUnknown) {
        [[(AVCaptureVideoPreviewLayer *)[self layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)orientation];
    }
}

@end

