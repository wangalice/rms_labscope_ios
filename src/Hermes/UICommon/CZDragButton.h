//
//  CZDragButton.h
//  Hermes
//
//  Created by Ralph Jin on 5/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CZDragButtonDelegate;

@interface CZDragButton : UIButton {
    BOOL dragging;
}

@property (nonatomic, assign) id<CZDragButtonDelegate> delegate;
@property (nonatomic, assign) CGRect home;  // Original position.
@property (nonatomic, assign) CGPoint touchLocation;
@property (nonatomic, assign, getter = isDragEnabled) BOOL dragEnabled;

- (void)goHome;  // animates return to home location
- (void)moveByOffset:(CGPoint)offset;

@end


@protocol CZDragButtonDelegate <NSObject>

@optional
- (void)dragButtonStartedTracking:(CZDragButton *)button;
- (void)dragButtonMoved:(CZDragButton *)button;
- (void)dragButtonStoppedTracking:(CZDragButton *)button;

@end
