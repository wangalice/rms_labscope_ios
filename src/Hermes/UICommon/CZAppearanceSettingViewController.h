//
//  CZAppearanceSettingViewController.h
//  Matscope
//
//  Created by Sherry Xu on 7/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

@class CZAnnotationAppearanceButton;

@protocol CZAppearanceSettingViewControllerDelegate;

@interface CZAppearanceSettingViewController : UIViewController

@property (assign, nonatomic) id <CZAppearanceSettingViewControllerDelegate> delegate;

- (void)setSelectedColor:(CZColor)color;
- (void)setSelectedSize:(CZElementSize)size;
- (void)setSelectedBackgroundColor:(CZColor)fillColor;
- (void)updateAppearanceSettingViewColorButton:(BOOL)enableColorButton backgroundButton:(BOOL)enableBackgroundColorButton;

@end

@protocol CZAppearanceSettingViewControllerDelegate <NSObject>
@optional
- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceColor:(CZColor)color;
- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceSize:(CZElementSize)size;
- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceBackgroundColor:(CZColor)fillColor;
- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceStrokeColor:(CZColor)color1 fillColor:(CZColor)color2;
@end
