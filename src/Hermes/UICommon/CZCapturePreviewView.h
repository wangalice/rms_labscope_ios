//
//  CZCapturePreviewView.h
//  Labscope
//
//  Created by Ralph Jin on 11/17/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface CZCapturePreviewView : UIView

@property (nonatomic, retain) AVCaptureSession *session;

- (void)setVideoOrientation:(UIInterfaceOrientation)orientation;

@end
