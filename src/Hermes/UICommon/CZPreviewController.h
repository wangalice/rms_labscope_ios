//
//  CZPreviewController.h
//  Hermes
//
//  Created by Ralph Jin on 7/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <QuickLook/QuickLook.h>

@interface CZPreviewController : QLPreviewController

@property (nonatomic, retain) NSURL *previewFileURL;

@end
