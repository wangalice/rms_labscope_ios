//
//  CZKeyboardExtensionView.m
//  Matscope
//
//  Created by Halley Gu on 8/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZKeyboardExtensionView.h"

@implementation CZKeyboardExtensionView

- (id)init {
    return [self initWithFrame:CGRectMake(0, 0, 1024, 44)];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *titles = @[@"µ", @"²", @"³"];
        NSMutableArray *barButtonItems = [[NSMutableArray alloc] init];
        UIBarButtonItemStyle buttonStyle;
        buttonStyle = UIBarButtonItemStylePlain;
        
        UIImage *bgImage = [[UIImage imageNamed:@"Assets/Light/navigation-bar-bg-blue"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];

        BOOL iOS9Version = [CZCommonUtils isOperatingSystemAtIOS9];
        
        int i = 0;
        for (NSString *title in titles) {
            if (iOS9Version && i != 0) {
                UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
                fixedSpace.width = 12;
                [barButtonItems addObject:fixedSpace];
                [fixedSpace release];
            }
            
            UIButton *internalButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [internalButton setTitle:title forState:UIControlStateNormal];
            [internalButton addTarget:self action:@selector(symbolButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [internalButton setBackgroundImage:bgImage forState:UIControlStateNormal];
            [internalButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:internalButton];
            [item setTintColor:[UIColor blackColor]];
            
            if (@available(iOS 11.0, *)) {
                [[internalButton.widthAnchor constraintEqualToConstant:72] setActive:YES];
            }
            else {
                internalButton.frame = CGRectMake(0, 0, 72, 36);
            }
            
            [barButtonItems addObject:item];
            [item release];
            
            i++;
        }
        
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:self.frame];
        [toolbar setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth)];
        [toolbar setItems:barButtonItems];
        [self addSubview:toolbar];
        [toolbar release];
        
        [barButtonItems release];
    }
    return self;
}

- (void)symbolButtonAction:(id)sender {
    if (self.textField != nil && [sender isKindOfClass:[UIButton class]]) {
        UIButton *item = (UIButton *)sender;
        [self.textField insertText:item.titleLabel.text];
    }
}

#pragma mark - UIInputViewAudioFeedback methods

- (BOOL)enableInputClicksWhenVisible {
    return YES;
}

@end
