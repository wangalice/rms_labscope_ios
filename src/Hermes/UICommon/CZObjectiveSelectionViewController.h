//
//  CZObjectiveSelectionViewController.h
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMicroscopeModel;
@class CZObjectiveSelectionViewController;

@protocol CZObjectiveSelectionViewControllerDelegate <NSObject>

@optional
- (void)objectiveSelectionViewController:(CZObjectiveSelectionViewController *)objectiveSelectionViewController didSelectObjectiveAtPosition:(NSUInteger)position;

@end

@interface CZObjectiveSelectionViewController : UIViewController

@property (nonatomic, weak) id<CZObjectiveSelectionViewControllerDelegate> delegate;
@property (nonatomic, readonly, strong) CZMicroscopeModel *microscopeModel;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel;

@end
