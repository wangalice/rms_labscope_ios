//
//  UIStackView+CZCustomSpacing.h
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStackView (CZCustomSpacing)

- (void)cz_addCustomSpacing:(CGFloat)spacing afterView:(UIView *)arrangedSubview;

@end
