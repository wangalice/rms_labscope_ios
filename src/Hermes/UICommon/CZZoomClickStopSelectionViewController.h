//
//  CZZoomClickStopSelectionViewController.h
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMicroscopeModel;
@class CZZoomClickStopSelectionViewController;

@protocol CZZoomClickStopSelectionViewControllerDelegate <NSObject>

@optional
- (void)zoomClickStopSelectionViewController:(CZZoomClickStopSelectionViewController *)zoomClickStopSelectionViewController didSelectZoomClickStopAtPosition:(NSUInteger)position;

@end

@interface CZZoomClickStopSelectionViewController : UIViewController

@property (nonatomic, weak) id<CZZoomClickStopSelectionViewControllerDelegate> delegate;
@property (nonatomic, readonly, strong) CZMicroscopeModel *microscopeModel;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel;

@end
