//
//  CZAccessabilityHelper.m
//  Labscope
//
//  Created by Ralph Jin on 9/21/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import "CZAccessabilityHelper.h"
#import <AVFoundation/AVFoundation.h>
#import "CZAlertController.h"

@implementation CZAccessabilityHelper

- (id)init {
    [self release];
    return nil;
}

+ (void)requestAccessCameraWithCompletion:(void (^)(BOOL success))completion {
    if (completion == NULL) {
        return;
    }
    
    const BOOL isIOS8andLater = floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1;
    
    BOOL canAccessCamera = NO;
    void (^failBlock)(void) = ^ {
        NSString *productName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
        NSString *message = [NSString stringWithFormat:L(@"ACCESS_CAMERA_MESSAGE"), productName];

        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:@"Privacy" message:message level:CZAlertLevelInfo];
        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            completion(NO);
        }];
        [alertController addActionWithTitle:L(@"SETTINGS") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            completion(NO);
            
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }];
        [alertController presentAnimated:YES completion:nil];
    };
    
    if (isIOS8andLater) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusAuthorized) {
            canAccessCamera = YES;
        } else if(status == AVAuthorizationStatusDenied){
        } else if(status == AVAuthorizationStatusRestricted){
        } else if(status == AVAuthorizationStatusNotDetermined){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (granted) {
                        completion(granted);
                    } else {
                        failBlock();
                    }
                });
            }];
            return;  // skip follwing code, decide call which block later.
        }
    } else {
        canAccessCamera = YES;
    }
    
    if (canAccessCamera) {
        completion(canAccessCamera);
    } else {
        failBlock();
    }
}

@end
