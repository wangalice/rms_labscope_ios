//
//  CZNosepiecePositionButton.h
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat CZNosepiecePositionButtonSize;

@interface CZNosepiecePositionButton : CZButton

- (instancetype)initWithPosition:(NSUInteger)position magnification:(float)magnification displayMagnification:(NSString *)displayMagnification;

@end
