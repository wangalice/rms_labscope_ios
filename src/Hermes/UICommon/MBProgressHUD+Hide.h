//
//  MBProgressHUD+Hide.h
//  Hermes
//
//  Created by Ralph Jin on 7/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Hide)

- (void)dismiss:(BOOL)animated;

@end
