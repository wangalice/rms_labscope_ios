//
//  CZFileOverwriteController.h
//  Hermes
//
//  Created by Mike Wang on 5/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 This is the definition of the overwrite block.  It is set by the 
 client which uses CZFilOverwriteController.  The block will be called
 when file names are conflict and need user to decide if overwritten or not.
 
 parameter id: This is the target which is the client
 parameter NSDictionary: This is the file names mapping.  The key is the original file name
 and the data is the resolved file name.
 */
typedef void (^CZFileOverwriteActionBlock)(NSDictionary *overwriteFilesDic);

@interface CZFileOverwriteController : NSObject

/*
 OverwriteYesBlock is set by client such that files will be copied and overwritten, TODO: merge with overwriteNoBlock
 */
@property (nonatomic, copy) CZFileOverwriteActionBlock overwriteYesBlock;
/*
 OverwriteNoBlock is set by client such that name conflict files will be copied with auto generated name
 */
@property (nonatomic, copy) CZFileOverwriteActionBlock overwriteNoBlock;

/** initialize instance
 *@param sourceFiles, array of files to create;
 *@param desitnationFiles, array of exist files;
 */
- (instancetype)initWithSource:(NSArray *)sourceFiles destination:(NSArray *)desitnationFiles;
- (instancetype)initWithSource:(NSArray *)sourceFiles destination:(NSArray *)desitnationFiles overwriteOption:(NSUInteger)option;

- (BOOL)checkOverwriteFiles:(BOOL)forceOverwrite;

@end
