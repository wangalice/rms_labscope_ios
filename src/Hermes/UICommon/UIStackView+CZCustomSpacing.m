//
//  UIStackView+CZCustomSpacing.m
//  Hermes
//
//  Created by Li, Junlin on 2/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIStackView+CZCustomSpacing.h"

@implementation UIStackView (CZCustomSpacing)

- (void)cz_addCustomSpacing:(CGFloat)spacing afterView:(UIView *)arrangedSubview {
    NSUInteger index = [self.arrangedSubviews indexOfObject:arrangedSubview];
    if (index == NSNotFound) {
        return;
    }
    
    UIView *spacingView = [[UIView alloc] init];
    spacingView.backgroundColor = [UIColor clearColor];
    
    switch (self.axis) {
        case UILayoutConstraintAxisHorizontal:
            [spacingView.widthAnchor constraintEqualToConstant:spacing].active = YES;
            break;
        case UILayoutConstraintAxisVertical:
            [spacingView.heightAnchor constraintEqualToConstant:spacing].active = YES;
            break;
    }
    
    [self insertArrangedSubview:spacingView atIndex:index + 1];
}

@end
