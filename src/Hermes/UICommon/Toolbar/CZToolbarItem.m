//
//  CZToolbarItem.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbarItem.h"
#import "CZToolbarItemPrivate.h"
#import "CZToolbarItemAction.h"

CZToolbarItemIdentifier const CZToolbarItemIdentifierAnonymous = @"CZToolbarItemIdentifierAnonymous";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFixedSpace = @"CZToolbarItemIdentifierFixedSpace";
CZToolbarItemIdentifier const CZToolbarItemIdentifierFlexibleSpace = @"CZToolbarItemIdentifierFlexibleSpace";

@interface CZToolbarItem ()

@property (nonatomic, strong) NSMutableArray<CZToolbarItemAction *> *actions;

@end

@implementation CZToolbarItem

- (instancetype)initWithIdentifier:(CZToolbarItemIdentifier)identifier {
    self = [super init];
    if (self) {
        _identifier = [identifier copy];
        _weakChildItems = [NSPointerArray weakObjectsPointerArray];
        _width = NAN;
        _actions = [NSMutableArray array];
    }
    return self;
}

- (NSArray<CZToolbarItem *> *)childItems {
    return self.weakChildItems.allObjects;
}

- (void)setView:(__kindof UIView *)view {
    if (self.toolbar) {
        return;
    }
    
    if ([_view isKindOfClass:[UIControl class]]) {
        UIControl *control = (UIControl *)_view;
        [control removeTarget:self action:@selector(performAction) forControlEvents:UIControlEventAllEvents];
    }
    
    _view = view;
    
    if ([_view isKindOfClass:[UIControl class]]) {
        UIControl *control = (UIControl *)_view;
        [control addTarget:self action:@selector(performAction) forControlEvents:[self defaultEventForControl:control]];
    }
}

- (CGFloat)actualWidth {
    if (isnan(self.width)) {
        return self.edgeInsets.left + self.view.intrinsicContentSize.width + self.edgeInsets.right;
    } else {
        return self.edgeInsets.left + self.width + self.edgeInsets.right;
    }
}

- (void)addAction:(CZToolbarItemAction *)action {
    [self.actions addObject:action];
}

- (CZToolbarItemAction *)actionForTag:(NSInteger)tag {
    for (CZToolbarItemAction *action in self.actions) {
        if (action.tag == tag) {
            return action;
        }
    }
    return nil;
}

- (void)performAction {
    for (CZToolbarItemAction *action in self.actions) {
        [action performActionOnItem:self];
    }
}

- (UIControlEvents)defaultEventForControl:(UIControl *)control {
    if ([control isKindOfClass:[UIButton class]]) {
        return UIControlEventTouchUpInside;
    } else if ([control isKindOfClass:[UITextField class]]) {
        return UIControlEventEditingChanged;
    } else {
        return UIControlEventValueChanged;
    }
}

@end

@implementation CZToolbarItem (ForwardToView)

- (void)setHidden:(BOOL)hidden {
    self.view.hidden = hidden;
}

- (BOOL)isHidden {
    return self.view.isHidden;
}

- (void)setEnabled:(BOOL)enabled {
    if ([self.view isKindOfClass:[UIControl class]]) {
        UIControl *control = (UIControl *)self.view;
        if (control.enabled != enabled) {
            control.enabled = enabled;
        }
    } else if ([self.view isKindOfClass:[UILabel class]]) {
        UILabel *label = (UILabel *)self.view;
        if (label.enabled != enabled) {
            label.enabled = enabled;
        }
    }
}

- (BOOL)isEnabled {
    if ([self.view isKindOfClass:[UIControl class]]) {
        return [(UIControl *)self.view isEnabled];
    } else if ([self.view isKindOfClass:[UILabel class]]) {
        return [(UILabel *)self.view isEnabled];
    } else {
        return NO;
    }
}

- (void)setSelected:(BOOL)selected {
    if ([self.view isKindOfClass:[UIControl class]]) {
        [(UIControl *)self.view setSelected:selected];
    }
}

- (BOOL)isSelected {
    if ([self.view isKindOfClass:[UIControl class]]) {
        return [(UIControl *)self.view isSelected];
    } else {
        return NO;
    }
}

- (void)setImage:(UIImage *)image {
    if ([self.view isKindOfClass:[UIButton class]]) {
        return [(UIButton *)self.view setImage:image forState:UIControlStateNormal];
    } else if ([self.view isKindOfClass:[UIImageView class]]) {
        return [(UIImageView *)self.view setImage:image];
    }
}

- (UIImage *)image {
    if ([self.view isKindOfClass:[UIButton class]]) {
        return [(UIButton *)self.view imageForState:UIControlStateNormal];
    } else if ([self.view isKindOfClass:[UIImageView class]]) {
        return [(UIImageView *)self.view image];
    } else {
        return nil;
    }
}

@end

@implementation CZToolbarItem (TemplateItem)

+ (instancetype)anonymousItem {
    return [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierAnonymous];
}

+ (instancetype)fixedSpaceItemWithWidth:(CGFloat)width {
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierFixedSpace];
    item.width = width;
    return item;
}

+ (instancetype)flexibleSpaceItem {
    return [[CZToolbarItem alloc] initWithIdentifier:CZToolbarItemIdentifierFlexibleSpace];
}

@end

@implementation CZToolbarItem (ButtonItem)

+ (instancetype)buttonItemWithIdentifier:(CZToolbarItemIdentifier)identifier {
    CZButton *button = [[CZButton alloc] init];
    button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:identifier];
    item.view = button;
    item.width = 64.0;
    return item;
}

- (CZButton *)button {
    return self.view;
}

- (void)alignImageAndTitle {
    if (self.button.currentTitle.length == 0) {
        self.button.titleEdgeInsets = UIEdgeInsetsZero;
        self.button.imageEdgeInsets = UIEdgeInsetsZero;
        self.button.contentEdgeInsets = UIEdgeInsetsZero;
    } else {
        // the space between the image and text
        CGFloat spacing = 2.0;
        
        // lower the text and push it left so it appears centered
        // below the image
        CGSize imageSize = self.button.imageView.image.size;
        self.button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
        
        // raise the image and push it right so it appears centered
        //  above the text
        CGSize titleSize = [self.button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: self.button.titleLabel.font}];
        self.button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width);
        
        // increase the content height to avoid clipping
        CGFloat edgeOffset = fabsf(titleSize.height - imageSize.height) / 2.0;
        self.button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
    }
}

@end

@implementation CZToolbarItem (ImageItem)

+ (instancetype)imageItemWithIdentifier:(CZToolbarItemIdentifier)identifier image:(UIImage *)image {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:identifier];
    item.view = imageView;
    item.width = image.size.width;
    return item;
}

@end

@implementation CZToolbarItem (LabelItem)

+ (instancetype)labelItemWithIdentifier:(CZToolbarItemIdentifier)identifier {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont cz_subtitle1];
    label.textColor = [UIColor cz_gs80];
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:identifier];
    item.view = label;
    return item;
}

- (UILabel *)label {
    return self.view;
}

@end

@implementation CZToolbarItem (SliderItem)

+ (instancetype)sliderItemWithIdentifier:(CZToolbarItemIdentifier)identifier {
    CZSlider *slider = [[CZSlider alloc] init];
    CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:identifier];
    item.view = slider;
    return item;
}

- (CZSlider *)slider {
    return self.view;
}

@end
