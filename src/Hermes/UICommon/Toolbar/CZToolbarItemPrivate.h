//
//  CZToolbarItemPrivate.h
//  Labscope
//
//  Created by Li, Junlin on 1/9/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbarItem.h"

@class CZToolbar;

@interface CZToolbarItem ()

@property (nonatomic, weak) CZToolbar *toolbar;
@property (nonatomic, weak) CZToolbarItem *parentItem;
@property (nonatomic, strong) NSPointerArray *weakChildItems;

@end
