//
//  CZToolbar.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZToolbarItem.h"
#import "CZToolbarItemAction.h"

extern const UIEdgeInsets CZToolbarDefaultInsets;
extern const CGFloat CZToolbarDefaultSpacing;

@class CZToolbar;

@protocol CZToolbarDelegate <NSObject>

@required
- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier;

@optional
- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item;

- (void)toolbar:(CZToolbar *)toolbar willPushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem;
- (void)toolbar:(CZToolbar *)toolbar didPushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem;

- (void)toolbar:(CZToolbar *)toolbar willPopItems:(NSArray<CZToolbarItem *> *)items byItem:(CZToolbarItem *)byItem toItem:(CZToolbarItem *)toItem;
- (void)toolbar:(CZToolbar *)toolbar didPopItems:(NSArray<CZToolbarItem *> *)items byItem:(CZToolbarItem *)byItem toItem:(CZToolbarItem *)toItem;

@end

@protocol CZToolbarResponder <NSObject>

@optional

/**
 The toolbar will find a responder that can perform action for this item in its responder chain from top to bottom.

 @param toolbar The toolbar.
 @param item The item on the toolbar.
 @return Return YES if you can perform action for this item, otherwise, the toolbar will ask the next responder.
 */
- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item;

@end

/**
 You can set items directly by setting toolbar.items = @[...],
 or you can set item identifiers by setting toolbar.itemIdentifiers = @[...] first,
 and implement the delegate method toolbar:makeItemForIdentifier:
 
 If your item action is nil, the toolbar will ask its responder to handle the action.
 */
@interface CZToolbar : UIView

@property (nonatomic, weak) id<CZToolbarDelegate> delegate;

/**
 Add the reponsder to the toolbar's responder chain.
 
 @param responder The responder that conforms to CZToolbarResponder.
 */
- (void)pushResponder:(id<CZToolbarResponder>)responder;

/**
 Remove the topmost responder from the toolbar's responder chain.
 
 @return The topmost responder on the chain.
 */
- (id<CZToolbarResponder>)popResponder;

@property (nonatomic, assign) UIEdgeInsets insets;
@property (nonatomic, assign) CGFloat spacing;

@property (nonatomic, copy) NSArray<CZToolbarItem *> *items;
@property (nonatomic, copy) NSArray<CZToolbarItemIdentifier> *itemIdentifiers;

- (void)pushItems:(NSArray<CZToolbarItem *> *)items;
- (void)pushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem;
- (void)pushItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers;
- (void)pushItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers fromItem:(CZToolbarItem *)fromItem;
- (NSArray<CZToolbarItem *> *)popItems;
- (NSArray<CZToolbarItem *> *)popItemsByItem:(CZToolbarItem *)byItem;

- (NSArray<CZToolbarItem *> *)makeItemsForItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers;

/**
 Search in items and return the item that has the specified identifier.
 
 @param identifier The identifier for search.
 @return The item that has the specified identifier.
 */
- (CZToolbarItem *)itemForIdentifier:(CZToolbarItemIdentifier)identifier;
- (CZToolbarItem *)itemForIdentifierInStack:(CZToolbarItemIdentifier)identifier;

@end
