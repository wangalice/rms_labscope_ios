//
//  CZToolbarItem.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZToolbar;
@class CZToolbarItemAction;

typedef NSString * CZToolbarItemIdentifier;

extern CZToolbarItemIdentifier const CZToolbarItemIdentifierAnonymous;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFixedSpace;
extern CZToolbarItemIdentifier const CZToolbarItemIdentifierFlexibleSpace;

@interface CZToolbarItem : NSObject

@property (nonatomic, readonly, copy) CZToolbarItemIdentifier identifier;
@property (nonatomic, readonly, weak) CZToolbar *toolbar;
@property (nonatomic, readonly, weak) CZToolbarItem *parentItem;
@property (nonatomic, readonly, copy) NSArray<CZToolbarItem *> *childItems;
@property (nonatomic, strong) __kindof UIView *view;
@property (nonatomic, assign) CGFloat width;    // NAN as default
@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@property (nonatomic, readonly, assign) CGFloat actualWidth;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithIdentifier:(CZToolbarItemIdentifier)identifier NS_DESIGNATED_INITIALIZER;

- (void)addAction:(CZToolbarItemAction *)action;
- (CZToolbarItemAction *)actionForTag:(NSInteger)tag;

@end

@interface CZToolbarItem (ForwardToView)

@property (nonatomic, assign, getter=isHidden) BOOL hidden;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign, getter=isSelected) BOOL selected;
@property (nonatomic, strong) UIImage *image;

@end

@interface CZToolbarItem (TemplateItem)

+ (instancetype)anonymousItem;
+ (instancetype)fixedSpaceItemWithWidth:(CGFloat)width;
+ (instancetype)flexibleSpaceItem;

@end

@interface CZToolbarItem (ButtonItem)

+ (instancetype)buttonItemWithIdentifier:(CZToolbarItemIdentifier)identifier;
@property (nonatomic, readonly, strong) CZButton *button;

- (void)alignImageAndTitle;

@end

@interface CZToolbarItem (ImageItem)

+ (instancetype)imageItemWithIdentifier:(CZToolbarItemIdentifier)identifier image:(UIImage *)image;

@end

@interface CZToolbarItem (LabelItem)

+ (instancetype)labelItemWithIdentifier:(CZToolbarItemIdentifier)identifier;
@property (nonatomic, readonly, strong) UILabel *label;

@end

@interface CZToolbarItem (SliderItem)

+ (instancetype)sliderItemWithIdentifier:(CZToolbarItemIdentifier)identifier;
@property (nonatomic, readonly, strong) CZSlider *slider;

@end
