//
//  CZToolbarItemAction.m
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbarItemAction.h"
#import "CZToolbar.h"

@interface CZToolbarItemAction ()

@property (nonatomic, copy) CZToolbarItemActionBlock block;

@end

@implementation CZToolbarItemAction

+ (instancetype)actionWithBlock:(CZToolbarItemActionBlock)block {
    return [[self alloc] initWithBlock:block];
}

+ (instancetype)pushActionWithItems:(NSArray<CZToolbarItem *> *)items {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        CZToolbarItem *imageItem = [CZToolbarItem imageItemWithIdentifier:item.identifier image:item.image];
        [item.toolbar pushItems:[@[imageItem] arrayByAddingObjectsFromArray:items] fromItem:item];
    }];
}

+ (instancetype)pushActionWithItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        CZToolbarItem *imageItem = [CZToolbarItem imageItemWithIdentifier:item.identifier image:item.image];
        NSArray<CZToolbarItem *> *items = [item.toolbar makeItemsForItemIdentifiers:itemIdentifiers];
        [item.toolbar pushItems:[@[imageItem] arrayByAddingObjectsFromArray:items] fromItem:item];
    }];
}

+ (instancetype)popAction {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        [item.toolbar popItemsByItem:item];
    }];
}

+ (instancetype)presentActionWithItems:(NSArray<CZToolbarItem *> *)items {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        [item.toolbar pushItems:items fromItem:item];
    }];
}

+ (instancetype)presentActionWithItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        [item.toolbar pushItemIdentifiers:itemIdentifiers fromItem:item];
    }];
}

+ (instancetype)dismissAction {
    return [self actionWithBlock:^(CZToolbarItem *item) {
        [item.toolbar popItemsByItem:item];
    }];
}

- (instancetype)initWithBlock:(CZToolbarItemActionBlock)block {
    self = [super init];
    if (self) {
        _block = [block copy];
    }
    return self;
}

- (void)performActionOnItem:(CZToolbarItem *)item {
    if (self.block) {
        self.block(item);
    }
}

@end
