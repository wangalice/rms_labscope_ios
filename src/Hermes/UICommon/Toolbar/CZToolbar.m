//
//  CZToolbar.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToolbar.h"
#import "CZToolbarItemPrivate.h"
#import "CZToolbarItemAction.h"

const UIEdgeInsets CZToolbarDefaultInsets = {8.0f, 16.0f, 8.0f, 16.0f};
const CGFloat CZToolbarDefaultSpacing = 8.0;
const NSInteger CZToolbarDelegateActionTag = 2001;

@interface CZToolbarItemView : UIView

@property (nonatomic, readonly, strong) CZToolbarItem *item;

- (instancetype)initWithItem:(CZToolbarItem *)item;

@end

@implementation CZToolbarItemView

- (instancetype)initWithItem:(CZToolbarItem *)item {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _item = item;
        [self addSubview:_item.view];
        
        if (!isnan(_item.width)) {
            [self.widthAnchor constraintEqualToConstant:_item.width + _item.edgeInsets.left + _item.edgeInsets.right].active = YES;
        }
        
        _item.view.translatesAutoresizingMaskIntoConstraints = NO;
        [_item.view.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:_item.edgeInsets.left].active = YES;
        [_item.view.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-_item.edgeInsets.right].active = YES;
        [_item.view.topAnchor constraintEqualToAnchor:self.topAnchor constant:_item.edgeInsets.top].active = YES;
        [_item.view.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:_item.edgeInsets.bottom].active = YES;
        
        [_item.view addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc {
    [_item.view removeObserver:self forKeyPath:@"hidden" context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey, id> *)change context:(void *)context {
    self.hidden = [change[NSKeyValueChangeNewKey] boolValue];
}

@end

@interface CZToolbar ()

@property (nonatomic, copy) NSArray<CZToolbarItemIdentifier> *templateItemIdentifiers;
@property (nonatomic, strong) NSMutableArray<NSArray<CZToolbarItem *> *> *itemsStack;
@property (nonatomic, strong) NSPointerArray *responderChain;

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, strong) NSLayoutConstraint *stackViewLeadingAnchor;
@property (nonatomic, strong) NSLayoutConstraint *stackViewTrailingAnchor;
@property (nonatomic, strong) NSLayoutConstraint *stackViewTopAnchor;
@property (nonatomic, strong) NSLayoutConstraint *stackViewBottomAnchor;

@end

@implementation CZToolbar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _insets = CZToolbarDefaultInsets;
        _spacing = CZToolbarDefaultSpacing;
        
        _templateItemIdentifiers = @[CZToolbarItemIdentifierFixedSpace, CZToolbarItemIdentifierFlexibleSpace];
        _itemsStack = [NSMutableArray array];
        _responderChain = [NSPointerArray weakObjectsPointerArray];
        
        _stackView = [[UIStackView alloc] init];
        _stackView.translatesAutoresizingMaskIntoConstraints = NO;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.spacing = _spacing;
        [self addSubview:_stackView];
        
        _stackViewLeadingAnchor = [_stackView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:_insets.left];
        _stackViewLeadingAnchor.active = YES;
        
        _stackViewTrailingAnchor = [_stackView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-_insets.right];
        _stackViewTrailingAnchor.active = YES;
        
        _stackViewTopAnchor = [_stackView.topAnchor constraintEqualToAnchor:self.topAnchor constant:_insets.top];
        _stackViewTopAnchor.active = YES;
        
        _stackViewBottomAnchor = [_stackView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-_insets.bottom];
        _stackViewBottomAnchor.active = YES;
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (CZToolbarItem *item in self.items) {
        if ([item.identifier isEqualToString:CZToolbarItemIdentifierFixedSpace] || [item.identifier isEqualToString:CZToolbarItemIdentifierFlexibleSpace]) {
            continue;
        }
        CGPoint pointInItemView = [self convertPoint:point toView:item.view];
        if ([item.view pointInside:pointInItemView withEvent:event] == YES) {
            return YES;
        }
    }
    return NO;
}

- (void)setInsets:(UIEdgeInsets)insets {
    _insets = insets;
    
    self.stackViewLeadingAnchor.constant = _insets.left;
    self.stackViewTrailingAnchor.constant = -_insets.right;
    self.stackViewTopAnchor.constant = _insets.top;
    self.stackViewBottomAnchor.constant = -_insets.bottom;
}

- (void)setSpacing:(CGFloat)spacing {
    _spacing = spacing;
    
    self.stackView.spacing = _spacing;
}

#pragma mark - Manage Items Stack

- (void)setItems:(NSArray<CZToolbarItem *> *)items {
    [self.itemsStack removeAllObjects];
    [self.itemsStack addObject:items ?: @[]];
    [self rearrangeStackViewWithItems:items];
}

- (NSArray<CZToolbarItem *> *)items {
    return self.itemsStack.lastObject;
}

- (void)setItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    NSArray<CZToolbarItem *> *items = [self makeItemsForItemIdentifiers:itemIdentifiers];
    self.items = items;
}

- (NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    NSArray<CZToolbarItem *> *items = self.items;
    NSMutableArray<CZToolbarItemIdentifier> *itemIdentifiers = [NSMutableArray arrayWithCapacity:items.count];
    for (CZToolbarItem *item in items) {
        [itemIdentifiers addObject:item.identifier];
    }
    return [itemIdentifiers copy];
}

- (void)pushItems:(NSArray<CZToolbarItem *> *)items {
    [self pushItems:items fromItem:nil];
}

- (void)pushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem {
    for (CZToolbarItem *item in items) {
        item.parentItem = fromItem;
        [fromItem.weakChildItems addPointer:(__bridge void *)(item)];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(toolbar:willPushItems:fromItem:)]) {
        [self.delegate toolbar:self willPushItems:items ?: @[] fromItem:fromItem];
    }
    
    [self.itemsStack addObject:items ?: @[]];
    [self rearrangeStackViewWithItems:items];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(toolbar:didPushItems:fromItem:)]) {
        [self.delegate toolbar:self didPushItems:items ?: @[] fromItem:fromItem];
    }
}

- (void)pushItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    [self pushItemIdentifiers:itemIdentifiers fromItem:nil];
}

- (void)pushItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers fromItem:(CZToolbarItem *)fromItem {
    NSArray<CZToolbarItem *> *items = [self makeItemsForItemIdentifiers:itemIdentifiers];
    [self pushItems:items fromItem:fromItem];
}

- (NSArray<CZToolbarItem *> *)popItems {
    return [self popItemsByItem:nil];
}

- (NSArray<CZToolbarItem *> *)popItemsByItem:(CZToolbarItem *)byItem {
    NSArray<CZToolbarItem *> *items = self.itemsStack.lastObject;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(toolbar:willPopItems:byItem:toItem:)]) {
        [self.delegate toolbar:self willPopItems:items byItem:byItem toItem:items.firstObject.parentItem];
    }
    
    [self.itemsStack removeLastObject];
    [self rearrangeStackViewWithItems:self.itemsStack.lastObject];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(toolbar:didPopItems:byItem:toItem:)]) {
        [self.delegate toolbar:self didPopItems:items byItem:byItem toItem:items.firstObject.parentItem];
    }
    
    return items;
}

- (NSArray<CZToolbarItem *> *)makeItemsForItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers {
    NSAssert(self.delegate != nil, @"Cannot make item for identifier because of the delegate is nil.");
    NSMutableArray<CZToolbarItem *> *items = [NSMutableArray arrayWithCapacity:itemIdentifiers.count];
    for (CZToolbarItemIdentifier identifier in itemIdentifiers) {
        if ([self.templateItemIdentifiers containsObject:identifier]) {
            CZToolbarItem *item = [[CZToolbarItem alloc] initWithIdentifier:identifier];
            [items addObject:item];
        } else {
            CZToolbarItem *item = [self.delegate toolbar:self makeItemForIdentifier:identifier];
            NSAssert(item != nil, @"Cannot return nil for toolbar:makeItemForIdentifier: delegate method with identifier: %@", identifier);
            [items addObject:item];
        }
    }
    return [items copy];
}

#pragma mark - Manage Responder Chain

- (void)pushResponder:(id<CZToolbarResponder>)responder {
    [self.responderChain addPointer:(void *)responder];
}

- (id<CZToolbarResponder>)popResponder {
    if (self.responderChain.count > 0) {
        NSUInteger index = self.responderChain.count - 1;
        id<CZToolbarResponder> responder = [self.responderChain pointerAtIndex:index];
        [self.responderChain removePointerAtIndex:index];
        return responder;
    }
    return nil;
}

#pragma mark - Search Item

- (CZToolbarItem *)itemForIdentifier:(CZToolbarItemIdentifier)identifier {
    for (CZToolbarItem *item in self.items) {
        if ([item.identifier isEqualToString:identifier]) {
            return item;
        }
    }
    return nil;
}

- (CZToolbarItem *)itemForIdentifierInStack:(CZToolbarItemIdentifier)identifier {
    for (NSArray<CZToolbarItem *> *items in self.itemsStack.reverseObjectEnumerator) {
        for (CZToolbarItem *item in items) {
            if ([item.identifier isEqualToString:identifier]) {
                return item;
            }
        }
    }
    return nil;
}

#pragma mark - Private

- (void)rearrangeStackViewWithItems:(NSArray<CZToolbarItem *> *)items {
    NSArray<UIView *> *arrangedSubviews = self.stackView.arrangedSubviews;
    for (UIView *view in arrangedSubviews) {
        [self.stackView removeArrangedSubview:view];
        [view removeFromSuperview];
    }
    
    UIView *firstSpaceItemView = nil;
    
    for (CZToolbarItem *item in items) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(toolbar:willDisplayItem:)]) {
            [self.delegate toolbar:self willDisplayItem:item];
        }
        
        UIView *view = [self viewForItem:item];
        [self.stackView addArrangedSubview:view];
        
        if ([item.identifier isEqualToString:CZToolbarItemIdentifierFlexibleSpace]) {
            if (firstSpaceItemView == nil) {
                firstSpaceItemView = view;
            } else {
                [view.widthAnchor constraintEqualToAnchor:firstSpaceItemView.widthAnchor].active = YES;
            }
        }
        
        item.toolbar = self;
        [self addActionForItem:item];
    }
    
    if (firstSpaceItemView == nil) {
        CZToolbarItem *item = [CZToolbarItem flexibleSpaceItem];
        UIView *view = [self viewForItem:item];
        [self.stackView addArrangedSubview:view];
    }
}

- (UIView *)viewForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierFixedSpace]) {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor clearColor];
        [view.widthAnchor constraintEqualToConstant:item.width].active = YES;
        return view;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierFlexibleSpace]) {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor clearColor];
        [view setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        return view;
    }
    
    UIView *view = [[CZToolbarItemView alloc] initWithItem:item];
    [view setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    return view;
}

- (void)addActionForItem:(CZToolbarItem *)item {
    if ([item actionForTag:CZToolbarDelegateActionTag]) {
        return;
    }
    
    @weakify(self);
    CZToolbarItemAction *action = [CZToolbarItemAction actionWithBlock:^(CZToolbarItem *item) {
        @strongify(self);
        [self.responderChain compact];
        for (id<CZToolbarResponder> responder in self.responderChain.allObjects.reverseObjectEnumerator) {
            if ([responder respondsToSelector:@selector(toolbar:performActionForItem:)] && [responder toolbar:self performActionForItem:item] == YES) {
                break;
            }
        }
    }];
    action.tag = CZToolbarDelegateActionTag;
    [item addAction:action];
}

@end
