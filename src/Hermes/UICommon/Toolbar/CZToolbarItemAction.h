//
//  CZToolbarItemAction.h
//  Hermes
//
//  Created by Li, Junlin on 2/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZToolbarItem.h"

typedef void (^CZToolbarItemActionBlock)(CZToolbarItem *item);

@interface CZToolbarItemAction : NSObject

@property (nonatomic, assign) NSInteger tag;

+ (instancetype)actionWithBlock:(CZToolbarItemActionBlock)block;

+ (instancetype)pushActionWithItems:(NSArray<CZToolbarItem *> *)items;
+ (instancetype)pushActionWithItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers;
+ (instancetype)popAction;
+ (instancetype)presentActionWithItems:(NSArray<CZToolbarItem *> *)items;
+ (instancetype)presentActionWithItemIdentifiers:(NSArray<CZToolbarItemIdentifier> *)itemIdentifiers;
+ (instancetype)dismissAction;

- (void)performActionOnItem:(CZToolbarItem *)item;

@end
