//
//  CZWebBrowserViewController.m
//  Hermes
//
//  Created by Ralph Jin on 7/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZWebBrowserViewController.h"
#import "CZUIDefinition.h"

static const CGFloat kErrorLabelSize = 16;
static const CGFloat kErrorLabelWidth = 300;
static const CGFloat kErrorLabelHeight = kErrorLabelSize * 2;

@interface CZWebBrowserViewController () <UIWebViewDelegate>

@property (nonatomic, retain) UILabel *errorInfoLabel;
@property (nonatomic, retain) UIWebView *webView;

@property (nonatomic, assign) CZBarButtonItem *backButton;
@property (nonatomic, assign) CZBarButtonItem *forwardButton;

@end

@implementation CZWebBrowserViewController

- (void)dealloc {
    [_urlString release];
    [_errorInfoLabel release];
    [_webView stopLoading];
    _webView.delegate = nil;
    [_webView release];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    CGRect frame = self.parentViewController.view.frame;
    frame.origin = CGPointZero;
    self.view.frame = frame;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = kDefaultBackGroundColor;
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:frame];
    webView.autoresizingMask = UIViewAutoresizingNone;
    webView.delegate = self;
    [self.view addSubview:webView];
    self.webView = webView;
    [webView release];
	
    _backButton = [[CZBarButtonItem alloc] initWithImage:[UIImage imageNamed:A(@"back-icon")]
                                                   style:UIBarButtonItemStylePlain
                                                  target:self
                                                  action:@selector(goBackAction:)];
    _forwardButton = [[CZBarButtonItem alloc] initWithImage:[UIImage imageNamed:A(@"forward-icon")]
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                                                     action:@selector(forwardAction:)];
    
    CZBarButtonItem *reloadButton = [[CZBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                  target:self
                                                                                  action:@selector(reloadButtonAction:)];
    
    self.navigationItem.rightBarButtonItems = @[reloadButton, _forwardButton, _backButton];
    
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:16]}];
    
    [self loadURL];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect frame = self.view.bounds;
    self.webView.frame = frame;
}

- (void)goBackAction:(id)sender {
    [self.webView goBack];
    
    [self refreshButtons];
}

- (void)forwardAction:(id)sender {
    [self.webView goForward];
    
    [self refreshButtons];
}

- (void)reloadButtonAction:(id)sender {
    if (self.webView.isHidden) {
        [self loadURL];
    } else {
        [self.webView reload];
    }
}

- (void)refreshButtons {
    self.backButton.enabled = [self.webView canGoBack];
    self.forwardButton.enabled = [self.webView canGoForward];    
}

- (void)loadURL {
    if (self.urlString) {
        NSURL *url = [NSURL URLWithString:self.urlString];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
        self.title = self.urlString;
        
        self.webView.hidden = NO;
    }
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType != UIWebViewNavigationTypeOther) {
        self.title = [[request URL] absoluteString];
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    self.errorInfoLabel.hidden = YES;
    
    [self refreshButtons];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.errorInfoLabel.hidden = YES;
    self.webView.hidden = NO;
    
    [self refreshButtons];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    self.webView.hidden = YES;

    if (self.errorInfoLabel == nil) {
        UILabel *label = [[UILabel alloc] init];
        self.errorInfoLabel = label;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = kDefaultLabelColor;
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:kErrorLabelSize];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.numberOfLines = 2;
        [self.view addSubview:label];
        [label release];
    }
    
    self.errorInfoLabel.text = [error localizedDescription];
    CGSize fitSize = [self.errorInfoLabel sizeThatFits:CGSizeMake(kErrorLabelWidth, kErrorLabelHeight)];
    self.errorInfoLabel.frame = CGRectMake(0, 0, fitSize.width, fitSize.height);
    self.errorInfoLabel.center = self.view.center;
    self.errorInfoLabel.hidden = NO;
    
    [self refreshButtons];
}
@end
