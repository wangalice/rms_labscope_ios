//
//  CZFunctionBar.m
//  Hermes
//
//  Created by Mike Wang on 3/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFunctionBar.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZUIDefinition.h"

#define KUIButtonCap 10.0

@interface CZFunctionButton ()

- (void)updateEdgeInsetsWithIcon:(UIImage *)image;  // protected method, sub-class can override

@end

@implementation CZFunctionButton

- (id)initWithIcon:(UIImage *)icon
      selectedIcon:(UIImage *)selectedIcon
        background:(UIImage *)background
selectedBackground:(UIImage *)selectedBackground
             label:(NSString *)text {
    self = [self initWithFrame:CGRectMake(0, 0, kFunctionButtonWidth, kFunctionButtonHeight)];
    if (self) {
        self.exclusiveTouch = YES;
        [self setContentMode:UIViewContentModeCenter];
        
        [self setBackgroundImage:background forState:UIControlStateNormal];
        [self setBackgroundImage:selectedBackground forState:UIControlStateSelected];
        
        [self setImage:icon forState:UIControlStateNormal];
        [self setImage:selectedIcon forState:UIControlStateSelected];
        
#ifdef CZButtonWithLabel
        CGFloat iconWidth = icon.size.width;

        [self setImageEdgeInsets:UIEdgeInsetsMake(kFunctionButtonIconTopInsect,
                                                  (kFunctionButtonWidth - iconWidth)/2.0, 0,
                                                  (kFunctionButtonWidth - iconWidth)/2.0)];

        if (text) {
            [self setTitle:text forState:UIControlStateNormal];
            [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
            [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateSelected];
            [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
            self.titleLabel.font = [UIFont fontWithName: @"HelveticaNeue" size: 10];
            self.titleLabel.numberOfLines = 2;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            [self setTitleEdgeInsets:UIEdgeInsetsMake(kFunctionButtonLabelTop, -iconWidth, kFunctionButtonLabelBottom, 0)];
        }
#endif
    }
    return self;
}

- (id)initWithIcon:(UIImage *)icon
      selectedIcon:(UIImage *)selectedIcon
        background:(UIImage *)background
selectedBackground:(UIImage *)selectedBackground
             label:(NSString *)text
      includeLabel:(BOOL)includeLabel {
    self = [self initWithFrame:CGRectMake(0, 0, kFunctionButtonWidth, kFunctionButtonHeight)];
    if (self) {
        self.exclusiveTouch = YES;
        [self setContentMode:UIViewContentModeCenter];
        
        [self setBackgroundImage:background forState:UIControlStateNormal];
        [self setBackgroundImage:selectedBackground forState:UIControlStateSelected];
        
        [self setImage:icon forState:UIControlStateNormal];
        [self setImage:selectedIcon forState:UIControlStateSelected];
        
        if (text) {
            [self setTitle:text forState:UIControlStateNormal];
            [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
            [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateSelected];
            [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
            self.titleLabel.font = [UIFont fontWithName: @"HelveticaNeue" size: 10];
            self.titleLabel.numberOfLines = 2;
            self.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            [self updateEdgeInsetsWithIcon:icon];
        }
    }
    return self;
}

// override super class

- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    [super setTitle:title forState:state];
    
    if (title) {
        [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateSelected];
        [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
    }
    
    UIImage *icon = [self imageForState:UIControlStateNormal];
    [self updateEdgeInsetsWithIcon:icon];
}

- (BOOL)includeLabel {
    return [self titleForState:UIControlStateNormal] != nil || [self titleForState:UIControlStateDisabled] != nil || [self titleForState:UIControlStateSelected] != nil;
}

- (void)setImage:(nullable UIImage *)image forState:(UIControlState)state {
    [super setImage:image forState:state];
    
    [self updateEdgeInsetsWithIcon:image];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIImage *icon = [self imageForState:UIControlStateNormal];
    [self updateEdgeInsetsWithIcon:icon];
}

// protected methods

- (void)updateEdgeInsetsWithIcon:(UIImage *)image {
    if (self.includeLabel) {
        CGSize iconSize = image.size;
        CGSize buttonSize = self.bounds.size;
        
        CGFloat margin = (buttonSize.height - iconSize.height) / 3;

        CGFloat topEdge = floor(iconSize.height - buttonSize.height + margin);
        [self setImageEdgeInsets:UIEdgeInsetsMake(topEdge,
                                                  (buttonSize.width - iconSize.width)/2.0,
                                                  0,
                                                  (buttonSize.width - iconSize.width)/2.0)];

        [self setTitleEdgeInsets:UIEdgeInsetsMake(kFunctionButtonLabelTop, -iconSize.width, kFunctionButtonLabelBottom, 0)];
    } else {
        self.imageEdgeInsets = UIEdgeInsetsZero;
        self.titleEdgeInsets = UIEdgeInsetsZero;
    }
}

@end

@implementation CZHalfHeightFunctionButton

- (id)initWithIcon:(UIImage *)icon
      selectedIcon:(UIImage *)selectedIcon
        background:(UIImage *)background
selectedBackground:(UIImage *)selectedBackground
             label:(NSString *)text {
    self = [super initWithIcon:icon selectedIcon:selectedIcon background:background selectedBackground:selectedIcon label:text];
    if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, floor(self.frame.size.height*9.0/14.0));
        
#ifdef CZButtonWithLabel
        CGFloat iconWidth = icon.size.width;
        [self setImageEdgeInsets:UIEdgeInsetsMake(0, (kFunctionButtonWidth - iconWidth)/2.0, 0, (kFunctionButtonWidth - iconWidth)/2.0)];

        if (text) {
            [self setImageEdgeInsets:UIEdgeInsetsMake(0, -0.5*iconWidth, 0, 0)];
            
            [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 0.25*iconWidth, 0, 0)];
        }
#endif
    }

    return self;
}

@end

@implementation CZDoneButton

- (id)init {
    self = [self initWithIcon:[UIImage imageNamed:A(@"apply-icon")]
                 selectedIcon:[UIImage imageNamed:A(@"apply-icon")]
                   background:[UIImage imageNamed:A(@"function-bottom-normal")]
           selectedBackground:[UIImage imageNamed:A(@"function-bottom-selected")]
                        label:L(@"DONE")];
    return self;
}

@end

@implementation CZDoneAsButton

- (id)init {
    self = [self initWithIcon:[UIImage imageNamed:A(@"apply-icon")]
                 selectedIcon:[UIImage imageNamed:A(@"apply-icon")]
                   background:[UIImage imageNamed:A(@"function-bottom-lt-normal")]
           selectedBackground:[UIImage imageNamed:A(@"function-bottom-lt-selected")]
                        label:L(@"DONE")];
    return self;
}

@end

@implementation CZColorSizeButton

- (id)init {
    UIImage *redIcon = [UIImage imageNamed:A(@"red-icon")];
    self = [self initWithIcon:redIcon
                 selectedIcon:redIcon
                   background:[UIImage imageNamed:A(@"function-top-normal")]
           selectedBackground:[UIImage imageNamed:A(@"function-top-selected")]
                        label:L(@"SIZE_M")
                        includeLabel:YES];
    self.titleLabel.font = [UIFont fontWithName: @"HelveticaNeue" size: 12];
    self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, 10, 10);
    [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
    [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateSelected];
    
    CGFloat iconWidth = redIcon.size.width;
    CGFloat iconHeight = redIcon.size.height;
    self.imageView.frame = CGRectMake((self.frame.size.width - self.imageView.frame.size.width)/2.0, (self.frame.size.height - self.imageView.frame.size.height)/2.0, iconWidth, iconHeight);
    [self setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    self.titleLabel.frame = CGRectMake((self.frame.size.width - self.titleLabel.frame.size.width)/2.0, (self.frame.size.height - self.titleLabel.frame.size.height)/2.0, self.titleLabel.frame.size.width, self.titleLabel.frame.size.height);
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    return self;
}

- (void)updateEdgeInsetsWithIcon:(UIImage *)image {
    // do not change edge insets
}

@end

@implementation CZZenStyleButton

- (id)initWithLabel:(NSString *)label {
    self = [super init];
    if (self) {
        UIImage *buttonImage = [[UIImage imageNamed:A(@"button-zenstyle-dark")] stretchableImageWithLeftCapWidth:KUIButtonCap topCapHeight:0];
        UIImage *selectedbuttonImage = [[UIImage imageNamed:A(@"button-zenstyle")] stretchableImageWithLeftCapWidth:KUIButtonCap topCapHeight:0];
        [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self setBackgroundImage:selectedbuttonImage forState:UIControlStateSelected];
        [self setTitle:label forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateHighlighted];
        [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
        
        [self sizeToFit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIImage *buttonImage = [[UIImage imageNamed:A(@"button-zenstyle-dark")] stretchableImageWithLeftCapWidth:KUIButtonCap topCapHeight:0];
        [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateHighlighted];
        [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
        
        // Do not do auto-resizing for those buttons initialized by XIB, since
        // the original size actually makes sense.
    }
    
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGRect labelRect = [self.titleLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, CGFLOAT_MAX) limitedToNumberOfLines:1];
    labelRect.size.width += kFunctionButtonLabelPadding;
    return labelRect.size;
}

@end

@implementation CZDisplayButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setImage:[UIImage imageNamed:A(@"display-curve-icon")] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:A(@"display-curve-icon")] forState:UIControlStateSelected];
        [self setBackgroundColor:[UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:0.5]];
    }
    return self;
}

@end

@implementation CZConfigureButton

- (id)initWithStatus:(BOOL)isConfigured {
    self = [super init];
    if (self) {
        UIImage *buttonImage = [[UIImage imageNamed:A(@"button-zenstyle-dark")] stretchableImageWithLeftCapWidth:KUIButtonCap topCapHeight:0];
        UIImage *selectedbuttonImage = [[UIImage imageNamed:A(@"button-zenstyle")] stretchableImageWithLeftCapWidth:KUIButtonCap topCapHeight:0];
        [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self setBackgroundImage:selectedbuttonImage forState:UIControlStateSelected];
        
        [self setTitleColor:kDefaultButtonTitleColorNormal forState:UIControlStateNormal];
        [self setTitleColor:kDefaultButtonTitleColorSelected forState:UIControlStateHighlighted];
        [self setTitleColor:kDefaultButtonTitleColorDisabled forState:UIControlStateDisabled];
        [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [self setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 5)];
        
        self.titleLabel.font = [UIFont fontWithName: @"HelveticaNeue" size: 12];
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        
        self.exclusiveTouch = YES;
        
        self.configured = isConfigured;
    }
    return self;
}

- (void)dealloc {
    [_notConfiguredIcon release];
    [_configuredIcon release];
    [super dealloc];
}

- (void)setConfigured:(BOOL)configured {
    _configured = configured;
    if (_configured) {
        [self setImage:self.configuredIcon forState:UIControlStateNormal];
    } else {
        [self setImage:self.notConfiguredIcon forState:UIControlStateNormal];
    }
}
@end

@implementation CZFunctionView

- (void)setHidden:(BOOL)hidden {
    const float kFadeOutDuration = 0.25;
    
    if (self.isHidden == hidden) {
        return;
    }
    if (self.isHidden) {
        [UIView animateWithDuration:kFadeOutDuration
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^ {
                             self.alpha = 1.0;
                         }
                         completion:nil];
    } else {
        [UIView animateWithDuration:kFadeOutDuration
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^ {
                             self.alpha = 0.0;
                         }
                         completion:nil];
    }

    _hidden = hidden;
}

@end

@interface CZFunctionBar ()

@property (nonatomic, retain) NSMutableArray *buttons;

@end

@implementation CZFunctionBar

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _buttons = [[NSMutableArray alloc] init];
        self.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:38.0/255.0 blue:53.0/255.0 alpha:1.0];
        _filledHeight = 0.0;
        _filledHeightFromBottom = self.frame.size.height;
    }
    return self;
}

- (void)addButton:(UIButton *)button {
    CGRect rect = CGRectMake(button.frame.origin.x + kFunctionButtonXPadding, _filledHeight,
                             button.frame.size.width, button.frame.size.height);
    button.frame = rect;

    [_buttons addObject:button];
    [self addSubview:button];
    
    _filledHeight += button.frame.size.height;
}

- (UIButton *)buttonAtIndex:(int)index {
    if (index < 0 || index > _buttons.count - 1) {
        return nil;
    }
    
    return _buttons[index];
}

- (NSUInteger)buttonCount {
    return _buttons.count;
}

- (void)addButtonFromBottom:(UIButton *)button {
    _filledHeightFromBottom -= button.frame.size.height;
    CGRect rect = CGRectMake(button.frame.origin.x + kFunctionButtonXPadding, _filledHeightFromBottom,
                             button.frame.size.width, button.frame.size.height);
    button.frame = rect;
    
    [_buttons addObject:button];
    [self addSubview:button];
}

- (void)selectButton:(UIButton *)buttonSelect {
    for (UIButton *button in _buttons) {
        if (button == buttonSelect) {
            [button setSelected:TRUE];
        } else {
            [button setSelected:FALSE];
        }
    }
}

- (void)disableAllAndSelect:(UIButton *)buttonSelect {
    for (UIButton *button in _buttons) {
        if (button == buttonSelect) {
            [button setSelected:TRUE];
        } else if (!buttonSelect) {
            [button setSelected:FALSE];
            [button setEnabled:TRUE];
        } else {
            [button setSelected:FALSE];
            [button setEnabled:FALSE];
        }
    }
}

- (void)setEnable:(BOOL)enable {
    for (UIButton *button in _buttons) {
        button.enabled = enable;
    }
}

- (void)dealloc {
    [_buttons release];
    [super dealloc];
}

@end
