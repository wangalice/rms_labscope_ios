//
//  CZPreviewController.m
//  Hermes
//
//  Created by Ralph Jin on 7/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZPreviewController.h"
#import <QuickLook/QuickLook.h>
#import "CZUIDefinition.h"
#import "CZPostOffice.h"

@interface CZPreviewController () <QLPreviewControllerDataSource>

@property (nonatomic, strong) CZBarButtonItem *shareItem;
@property (nonatomic, strong) CZPostOffice *postOffice;

@end

@implementation CZPreviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.shareItem = [[CZBarButtonItem alloc] initWithIcon:[CZIcon iconNamed:@"share"]
                                                    target:self
                                                    action:@selector(shareAction:)];
    self.navigationItem.rightBarButtonItem = self.shareItem;
    
    self.dataSource = self;
    self.currentPreviewItemIndex = 0;
}

- (void)viewWillDisappear:(BOOL)animated {
    [_postOffice dismiss];
    [super viewWillDisappear:animated];
}

- (void)shareAction:(id)sender {
    NSString *filePath = [self.previewFileURL relativePath];
    if (filePath) {
        [self.postOffice reset];
        [self.postOffice addShareItemFromPath:filePath];
        [self.postOffice presentActivityViewControllerFromBarButtonItem:self.shareItem];
    }
}

- (CZPostOffice *)postOffice {
    if (_postOffice == nil) {
        _postOffice = [[CZPostOffice alloc] init];
    }
    
    return _postOffice;
}

#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController {
    return 1;
}

// Returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)index {
    return self.previewFileURL;
}

@end
