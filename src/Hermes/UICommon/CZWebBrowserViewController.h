//
//  CZWebBrowserViewController.h
//  Hermes
//
//  Created by Ralph Jin on 7/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZWebBrowserViewController : UIViewController

@property (nonatomic, copy) NSString *urlString;

@end
