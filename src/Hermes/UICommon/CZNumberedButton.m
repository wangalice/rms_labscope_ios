//
//  CZNumberedButton.m
//  Hermes
//
//  Created by Li, Junlin on 3/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNumberedButton.h"

@interface CZNumberedButton ()

@property (nonatomic, strong) UILabel *numberLabel;

@end

@implementation CZNumberedButton

- (instancetype)initWithFrame:(CGRect)frame number:(NSUInteger)number {
    self = [super initWithLevel:CZControlEmphasisDefault];
    if (self) {
        self.frame = frame;
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 48.0, 0.0, 0.0);
        
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0, 0.0, 24.0, frame.size.height)];
        _numberLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        _numberLabel.backgroundColor = [UIColor clearColor];
        _numberLabel.text = [NSString stringWithFormat:@"%ld", (long)(number)];
        _numberLabel.font = [UIFont cz_body1];
        _numberLabel.textColor = [UIColor cz_gs80];
        _numberLabel.highlightedTextColor = [UIColor cz_gs50];
        _numberLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_numberLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.numberLabel.highlighted = selected;
}

@end
