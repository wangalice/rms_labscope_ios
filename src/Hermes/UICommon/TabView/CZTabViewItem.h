//
//  CZTabViewItem.h
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZTabViewItem : NSObject

@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, strong) UIView *view;

- (instancetype)initWithTitle:(NSString *)title view:(UIView *)view;

@end
