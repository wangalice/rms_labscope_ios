//
//  CZTabViewController.h
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZTabViewController;

@protocol CZTabViewControllerDelegate <NSObject>

@optional
- (CGFloat)heightForSegmentsInTabViewController:(CZTabViewController *)tabViewController;
- (UIView *)tabViewController:(CZTabViewController *)tabViewController viewForSegmentAtIndex:(NSInteger)index;
- (void)tabViewController:(CZTabViewController *)tabViewController willDeselectView:(UIView *)view forSegmentAtIndex:(NSInteger)index;
- (void)tabViewController:(CZTabViewController *)tabViewController didSelectView:(UIView *)view forSegmentAtIndex:(NSInteger)index;

@end

@interface CZTabViewController : UIViewController

@property (nonatomic, weak) id<CZTabViewControllerDelegate> delegate;
@property (nonatomic, copy) NSArray<UIViewController *> *viewControllers;
@property (nonatomic, readonly, strong) UIViewController *selectedViewController;

- (void)addViewController:(UIViewController *)viewController;
- (void)insertViewController:(UIViewController *)viewController atIndex:(NSInteger)index;
- (void)removeViewController:(UIViewController *)viewController;

- (void)selectViewController:(UIViewController *)viewController;

@end
