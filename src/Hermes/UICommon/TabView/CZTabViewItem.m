//
//  CZTabViewItem.m
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTabViewItem.h"

@implementation CZTabViewItem

- (instancetype)initWithTitle:(NSString *)title view:(UIView *)view {
    self = [super init];
    if (self) {
        _title = [title copy];
        _view = view;
    }
    return self;
}

@end
