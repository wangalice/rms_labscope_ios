//
//  CZTabView.h
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZTabViewItem.h"

@class CZTabView;

@protocol CZTabViewDelegate <NSObject>

@optional
- (CGFloat)heightForSegmentsInTabView:(CZTabView *)tabView;
- (UIView *)tabView:(CZTabView *)tabView viewForSegmentAtIndex:(NSInteger)index;
- (void)tabView:(CZTabView *)tabView willDeselectView:(UIView *)view forSegmentAtIndex:(NSInteger)index;
- (void)tabView:(CZTabView *)tabView didSelectView:(UIView *)view forSegmentAtIndex:(NSInteger)index;

- (BOOL)tabView:(CZTabView *)tabView shouldSelectTabViewItem:(CZTabViewItem *)tabViewItem;
- (void)tabView:(CZTabView *)tabView willSelectTabViewItem:(CZTabViewItem *)tabViewItem;
- (void)tabView:(CZTabView *)tabView didSelectTabViewItem:(CZTabViewItem *)tabViewItem;

@end

@interface CZTabView : UIView

@property (nonatomic, weak) id<CZTabViewDelegate> delegate;
@property (nonatomic, copy) NSArray<CZTabViewItem *> *tabViewItems;
@property (nonatomic, readonly, strong) CZTabViewItem *selectedTabViewItem;
@property (nonatomic, readonly, assign) NSInteger selectedTabViewItemIndex;

- (void)addTabViewItem:(CZTabViewItem *)tabViewItem;
- (void)insertTabViewItem:(CZTabViewItem *)tabViewItem atIndex:(NSInteger)index;
- (void)removeTabViewItem:(CZTabViewItem *)tabViewItem;

- (void)selectTabViewItem:(CZTabViewItem *)tabViewItem;
- (void)selectTabViewItemAtIndex:(NSInteger)index;

@end
