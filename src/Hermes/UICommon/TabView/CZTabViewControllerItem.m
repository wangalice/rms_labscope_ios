//
//  CZTabViewControllerItem.m
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTabViewControllerItem.h"

@implementation CZTabViewControllerItem

- (instancetype)initWithViewController:(UIViewController *)viewController {
    self = [super init];
    if (self) {
        _viewController = viewController;
    }
    return self;
}

- (NSString *)title {
    return self.viewController.title;
}

- (UIView *)view {
    return self.viewController.view;
}

@end
