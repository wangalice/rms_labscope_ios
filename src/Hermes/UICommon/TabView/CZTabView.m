//
//  CZTabView.m
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTabView.h"
#import "CZUppercaseLabel.h"

const CGFloat CZTabViewSegmentedViewHeight = 32.0;
const NSInteger CZTabViewSegmentViewTag = 100;
const NSInteger CZTabViewSegmentViewLabelTag = 101;

@interface CZTabView ()

@property (nonatomic, strong) NSMutableArray<CZTabViewItem *> *mutableTabViewItems;
@property (nonatomic, readwrite, strong) CZTabViewItem *selectedTabViewItem;
@property (nonatomic, readwrite, assign) NSInteger selectedTabViewItemIndex;

@property (nonatomic, copy) NSMutableArray<UIView *> *segmentViews;
@property (nonatomic, strong) UIStackView *segmentedView;
@property (nonatomic, strong) UIView *selectedItemView;

@end

@implementation CZTabView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _mutableTabViewItems = [NSMutableArray array];
        _selectedTabViewItem = nil;
        _selectedTabViewItemIndex = NSNotFound;
        
        _segmentViews = [NSMutableArray array];
        
        _segmentedView = ({
            UIStackView *stackView = [[UIStackView alloc] init];
            CGRect frame = self.bounds;
            frame.size.height = CZTabViewSegmentedViewHeight;
            stackView.frame = frame;
            stackView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
            stackView.distribution = UIStackViewDistributionFillEqually;
            stackView.alignment = UIStackViewAlignmentFill;
            [self addSubview:stackView];
            stackView;
        });
        
        _selectedItemView = ({
            CGRect frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(CZTabViewSegmentedViewHeight, 0.0, 0.0, 0.0));
            UIView *view = [[UIView alloc] initWithFrame:frame];
            view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            view.backgroundColor = [UIColor cz_gs100];
            [self addSubview:view];
            view;
        });
        
        [self bringSubviewToFront:_segmentedView];
    }
    return self;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    
    if (self.selectedTabViewItem == nil) {
        [self selectTabViewItemAtIndex:0];
    }
}

- (void)setTabViewItems:(NSArray<CZTabViewItem *> *)tabViewItems {
    NSAssert(tabViewItems.count > 0, @"tabViewItems array must contain at least one item.");
    
    for (CZTabViewItem *tabViewItem in self.mutableTabViewItems) {
        [tabViewItem.view removeFromSuperview];
    }
    [self.mutableTabViewItems removeAllObjects];
    
    self.selectedTabViewItem = nil;
    self.selectedTabViewItemIndex = NSNotFound;
    
    for (UIView *segmentView in self.segmentViews) {
        [segmentView removeFromSuperview];
        [self.segmentedView removeArrangedSubview:segmentView];
    }
    [self.segmentViews removeAllObjects];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightForSegmentsInTabView:)]) {
        CGFloat height = [self.delegate heightForSegmentsInTabView:self];
        self.segmentedView.frame = CGRectMake(0.0, 0.0, self.bounds.size.width, height);
        self.selectedItemView.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(height, 0.0, 0.0, 0.0));
    }
    
    self.mutableTabViewItems = [tabViewItems mutableCopy];
    
    for (NSInteger index = 0; index < tabViewItems.count; index++) {
        UIView *segmentView = [self makeSegmentViewForTabViewItemAtIndex:index];
        [self.segmentViews addObject:segmentView];
        [self.segmentedView addArrangedSubview:segmentView];
    }
    
    [self selectTabViewItemAtIndex:0];
}

- (NSArray<CZTabViewItem *> *)tabViewItems {
    return [self.mutableTabViewItems copy];
}

- (void)addTabViewItem:(CZTabViewItem *)tabViewItem {
    [self.mutableTabViewItems addObject:tabViewItem];
    
    UIView *segmentView = [self makeSegmentViewForTabViewItemAtIndex:self.mutableTabViewItems.count - 1];
    [self.segmentViews addObject:segmentView];
    [self.segmentedView addArrangedSubview:segmentView];
    
    [self layoutSegmentViewLabel];
}

- (void)insertTabViewItem:(CZTabViewItem *)tabViewItem atIndex:(NSInteger)index {
    [self.mutableTabViewItems insertObject:tabViewItem atIndex:index];
    
    UIView *segmentView = [self makeSegmentViewForTabViewItemAtIndex:index];
    [self.segmentViews insertObject:segmentView atIndex:index];
    [self.segmentedView insertArrangedSubview:segmentView atIndex:index];
    
    [self layoutSegmentViewLabel];
}

- (void)removeTabViewItem:(CZTabViewItem *)tabViewItem {
    NSInteger index = [self.mutableTabViewItems indexOfObject:tabViewItem];
    if (index == NSNotFound) {
        return;
    }
    
    [self.mutableTabViewItems removeObjectAtIndex:index];
    
    UIView *segmentView = self.segmentViews[index];
    [self.segmentViews removeObjectAtIndex:index];
    [self.segmentedView removeArrangedSubview:segmentView];
    [segmentView removeFromSuperview];
    [tabViewItem.view removeFromSuperview];
    
    if (index == self.selectedTabViewItemIndex && self.mutableTabViewItems.count > 0) {
        index = index == self.mutableTabViewItems.count ? index - 1 : index;
        [self selectTabViewItemAtIndex:index];
    } else {
        [self layoutSegmentViewLabel];
    }
}

- (void)selectTabViewItem:(CZTabViewItem *)tabViewItem {
    NSInteger index = [self.mutableTabViewItems indexOfObject:tabViewItem];
    if (index != NSNotFound) {
        [self selectTabViewItemAtIndex:index];
    }
}

- (void)selectTabViewItemAtIndex:(NSInteger)index {
    if (index < 0 || index >= self.mutableTabViewItems.count) {
        return;
    }
    
    CZTabViewItem *tabViewItem = self.mutableTabViewItems[index];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:shouldSelectTabViewItem:)]) {
        if (![self.delegate tabView:self shouldSelectTabViewItem:tabViewItem]) {
            return;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:willSelectTabViewItem:)]) {
        [self.delegate tabView:self willSelectTabViewItem:tabViewItem];
    }
    
    if (self.selectedTabViewItemIndex != NSNotFound && self.selectedTabViewItemIndex != index) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:willDeselectView:forSegmentAtIndex:)]) {
            [self.delegate tabView:self willDeselectView:self.segmentViews[self.selectedTabViewItemIndex] forSegmentAtIndex:self.selectedTabViewItemIndex];
        }
    }
    
    [self.selectedTabViewItem.view removeFromSuperview];
    
    tabViewItem.view.frame = self.selectedItemView.bounds;
    tabViewItem.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.selectedItemView addSubview:tabViewItem.view];
    
    self.selectedTabViewItem = tabViewItem;
    self.selectedTabViewItemIndex = index;
    
    [self layoutSegmentViewLabel];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:didSelectTabViewItem:)]) {
        [self.delegate tabView:self didSelectTabViewItem:tabViewItem];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:didSelectView:forSegmentAtIndex:)]) {
        [self.delegate tabView:self didSelectView:self.segmentViews[index] forSegmentAtIndex:index];
    }
}

#pragma mark - Private

- (UIView *)makeSegmentViewForTabViewItemAtIndex:(NSInteger)index {
    UIView *segmentView = nil;
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabView:viewForSegmentAtIndex:)]) {
        segmentView = [self.delegate tabView:self viewForSegmentAtIndex:index];
        segmentView.userInteractionEnabled = YES;
    }
    
    if (segmentView == nil) {
        segmentView = [[UIView alloc] init];
        segmentView.backgroundColor = [UIColor cz_gs110];
        segmentView.userInteractionEnabled = YES;
        segmentView.tag = CZTabViewSegmentViewTag;
        
        CZUppercaseLabel *label = [[CZUppercaseLabel alloc] initWithFrame:segmentView.bounds];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        label.text = self.mutableTabViewItems[index].title;
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = CZTabViewSegmentViewLabelTag;
        [segmentView addSubview:label];
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapSegmentView:)];
    [segmentView addGestureRecognizer:tapGestureRecognizer];
    
    return segmentView;
}

- (void)layoutSegmentViewLabel {
    if (self.segmentViews.firstObject.tag != CZTabViewSegmentViewTag) {
        return;
    }
    
    if (self.segmentViews.count == 1) {
        UIView *segmentView = self.segmentViews.firstObject;
        CZUppercaseLabel *label = [segmentView viewWithTag:CZTabViewSegmentViewLabelTag];
        segmentView.backgroundColor = [UIColor cz_gs100];
        label.highlighted = NO;
        label.frame = CGRectOffset(segmentView.bounds, 0.0, CZTabViewSegmentedViewHeight / 2);
    } else {
        for (NSInteger i = 0; i < self.segmentViews.count; i++) {
            UIView *segmentView = self.segmentViews[i];
            CZUppercaseLabel *label = [segmentView viewWithTag:CZTabViewSegmentViewLabelTag];
            label.frame = segmentView.bounds;
            if (i == self.selectedTabViewItemIndex) {
                segmentView.backgroundColor = [UIColor cz_gs100];
                label.highlighted = YES;
            } else {
                segmentView.backgroundColor = [UIColor cz_gs110];
                label.highlighted = NO;
            }
        }
    }
}

- (void)handleTapSegmentView:(UITapGestureRecognizer *)tapGestureRecognizer {
    UIView *segmentView = tapGestureRecognizer.view;
    NSUInteger index = [self.segmentViews indexOfObject:segmentView];
    [self selectTabViewItemAtIndex:index];
}

@end
