//
//  CZTabViewController.m
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTabViewController.h"
#import "CZTabView.h"
#import "CZTabViewControllerItem.h"

@interface CZTabViewController () <CZTabViewDelegate>

@property (nonatomic, strong) CZTabView *tabView;
@property (nonatomic, strong) NSMutableArray<UIViewController *> *mutableViewControllers;
@property (nonatomic, readwrite, strong) UIViewController *selectedViewController;

@end

@implementation CZTabViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _mutableViewControllers = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabView = [[CZTabView alloc] initWithFrame:self.view.bounds];
    self.tabView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tabView.delegate = self;
    [self.view addSubview:self.tabView];
    
    NSMutableArray<CZTabViewControllerItem *> *tabViewItems = [NSMutableArray arrayWithCapacity:self.mutableViewControllers.count];
    for (UIViewController *viewController in self.viewControllers) {
        CZTabViewControllerItem *tabViewItem = [[CZTabViewControllerItem alloc] initWithViewController:viewController];
        [tabViewItems addObject:tabViewItem];
    }
    self.tabView.tabViewItems = tabViewItems;
}

- (void)setViewControllers:(NSArray<UIViewController *> *)viewControllers {
    [self.mutableViewControllers removeAllObjects];
    [self.mutableViewControllers addObjectsFromArray:viewControllers];
    
    NSMutableArray<CZTabViewControllerItem *> *tabViewItems = [NSMutableArray arrayWithCapacity:viewControllers.count];
    for (UIViewController *viewController in viewControllers) {
        CZTabViewControllerItem *tabViewItem = [[CZTabViewControllerItem alloc] initWithViewController:viewController];
        [tabViewItems addObject:tabViewItem];
    }
    self.tabView.tabViewItems = tabViewItems;
}

- (NSArray<UIViewController *> *)viewControllers {
    return [self.mutableViewControllers copy];
}

- (void)addViewController:(UIViewController *)viewController {
    [self.mutableViewControllers addObject:viewController];
    
    CZTabViewControllerItem *tabViewItem = [[CZTabViewControllerItem alloc] initWithViewController:viewController];
    [self.tabView addTabViewItem:tabViewItem];
}

- (void)insertViewController:(UIViewController *)viewController atIndex:(NSInteger)index {
    [self.mutableViewControllers insertObject:viewController atIndex:index];
    
    CZTabViewControllerItem *tabViewItem = [[CZTabViewControllerItem alloc] initWithViewController:viewController];
    [self.tabView insertTabViewItem:tabViewItem atIndex:index];
}

- (void)removeViewController:(UIViewController *)viewController {
    [self.mutableViewControllers removeObject:viewController];
    
    for (CZTabViewControllerItem *tabViewItem in self.tabView.tabViewItems) {
        if (tabViewItem.viewController == viewController) {
            [self.tabView removeTabViewItem:tabViewItem];
            break;
        }
    }
}

- (void)selectViewController:(UIViewController *)viewController {
    if (![self.mutableViewControllers containsObject:viewController]) {
        return;
    }
    
    for (CZTabViewControllerItem *tabViewItem in self.tabView.tabViewItems) {
        if (tabViewItem.viewController == viewController) {
            [self.tabView selectTabViewItem:tabViewItem];
            break;
        }
    }
}

#pragma mark - CZTabViewDelegate

- (CGFloat)heightForSegmentsInTabView:(CZTabView *)tabView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightForSegmentsInTabViewController:)]) {
        return [self.delegate heightForSegmentsInTabViewController:self];
    } else {
        return 32.0;
    }
}

- (UIView *)tabView:(CZTabView *)tabView viewForSegmentAtIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabViewController:viewForSegmentAtIndex:)]) {
        return [self.delegate tabViewController:self viewForSegmentAtIndex:index];
    } else {
        return nil;
    }
}

- (void)tabView:(CZTabView *)tabView willDeselectView:(UIView *)view forSegmentAtIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabViewController:willDeselectView:forSegmentAtIndex:)]) {
        [self.delegate tabViewController:self willDeselectView:view forSegmentAtIndex:index];
    }
}

- (void)tabView:(CZTabView *)tabView didSelectView:(UIView *)view forSegmentAtIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabViewController:didSelectView:forSegmentAtIndex:)]) {
        [self.delegate tabViewController:self didSelectView:view forSegmentAtIndex:index];
    }
}

- (void)tabView:(CZTabView *)tabView willSelectTabViewItem:(CZTabViewItem *)tabViewItem {
    [self.selectedViewController willMoveToParentViewController:nil];
    [self addChildViewController:((CZTabViewControllerItem *)tabViewItem).viewController];
}

- (void)tabView:(CZTabView *)tabView didSelectTabViewItem:(CZTabViewItem *)tabViewItem {
    [self.selectedViewController removeFromParentViewController];
    self.selectedViewController = ((CZTabViewControllerItem *)tabViewItem).viewController;
    [self.selectedViewController didMoveToParentViewController:self];
}

@end
