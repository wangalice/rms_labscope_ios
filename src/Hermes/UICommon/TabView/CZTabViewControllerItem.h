//
//  CZTabViewControllerItem.h
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTabViewItem.h"

@interface CZTabViewControllerItem : CZTabViewItem

@property (nonatomic, readonly, strong) UIViewController *viewController;

- (instancetype)initWithViewController:(UIViewController *)viewController;

@end
