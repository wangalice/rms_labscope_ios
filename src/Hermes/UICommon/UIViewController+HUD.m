//
//  UIViewController+HUD.m
//  Hermes
//
//  Created by Li, Junlin on 3/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIViewController+HUD.h"
#import "MBProgressHUD+Hide.h"
#import <objc/runtime.h>

@implementation UIViewController (HUD)

static char waitingHUDKey;

- (MBProgressHUD *)waitingHUD {
    return objc_getAssociatedObject(self, &waitingHUDKey);
}

- (void)setWaitingHUD:(MBProgressHUD *)waitingHUD {
    objc_setAssociatedObject(self, &waitingHUDKey, waitingHUD, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isShowingWaitingHUD {
    return self.waitingHUD != nil;
}

- (void)showWaitingHUD:(BOOL)show {
    if (show) {
        if (self.waitingHUD == nil) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
            [hud setColor:kSpinnerBackgroundColor];
            [hud setLabelText:L(@"BUSY_INDICATOR_PLEASE_WAIT")];
            self.waitingHUD = hud;
        }
    } else {
        [self.waitingHUD dismiss:YES];
        self.waitingHUD = nil;
    }
}

@end
