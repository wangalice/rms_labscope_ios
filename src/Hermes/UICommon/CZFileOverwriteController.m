//
//  CZFileOverwriteController.m
//  Hermes
//
//  Created by Mike Wang on 5/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileOverwriteController.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZAlertController.h"
#import <objc/runtime.h>

typedef NS_ENUM(NSUInteger, CZFileOverwriteButtonIndex) {
    kYes,
    kNo,
    kYesToAll,
    kNoToAll,
};

@interface CZFileOverwriteEntity : NSObject
@property (nonatomic, copy) NSString *fileName;
@property (nonatomic, assign) BOOL needOverwrite;
@end

@implementation CZFileOverwriteEntity

- (id)init {
    self = [super init];
    if (self) {
        _needOverwrite = FALSE;
    }
    return self;
}

- (void)dealloc {
    [_fileName release];
    [super dealloc];
}

@end

@interface CZFileOverwriteController ()
@property (nonatomic, retain) CZAlertController *singleFileOverwriteAlert;
@property (nonatomic, retain) CZAlertController *multipleFilesOverwriteAlert;
@property (nonatomic, retain) NSTimer *multipleFilesAlertTimer;
@property (nonatomic, retain) NSMutableDictionary *filenamesMapping;
@property (nonatomic, retain) NSMutableArray *filesNeedToBeOverwriten;
@property (nonatomic, assign) int currentFileOverwriteIndex;
@property (nonatomic, copy) NSArray *sourceFileNames;
@property (nonatomic, copy) NSArray *destinationFileNames;
@property (nonatomic, assign) CZIFileSaveOverwriteOption saveOverwriteOption;
@end

@implementation CZFileOverwriteController

- (id)initWithSource:(NSMutableArray *)sourceFiles destination:(NSMutableArray *)desitnationFiles {
    return [self initWithSource:sourceFiles destination:desitnationFiles overwriteOption:[CZDefaultSettings sharedInstance].fileOverwriteOption];
}

-(instancetype)initWithSource:(NSArray *)sourceFiles destination:(NSArray *)desitnationFiles overwriteOption:(NSUInteger)option {
    self = [super init];
    if (self) {
        self.sourceFileNames = sourceFiles;
        self.destinationFileNames = desitnationFiles;
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"FILES_OVERWRITE_TITLE") level:CZAlertLevelWarning];
        self.singleFileOverwriteAlert = alert;
        __block CZFileOverwriteController *controller = self;
        [alert addActionWithTitle:L(@"NO") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            [controller alertView:alert willDismissWithButtonIndex:kNo];
            [controller release];
        }];
        [alert addActionWithTitle:L(@"YES") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            [controller alertView:alert willDismissWithButtonIndex:kYes];
            [controller release];
        }];
        
        [self initMultipleAlertView];
        
        self.currentFileOverwriteIndex = 0;
        
        _filenamesMapping = [[NSMutableDictionary alloc] init];
        _saveOverwriteOption = (CZIFileSaveOverwriteOption)option;
    }
    
    return self;
}

- (void)initMultipleAlertView {
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"FILES_OVERWRITE_TITLE") level:CZAlertLevelWarning];
    self.multipleFilesOverwriteAlert = alert;
    __block CZFileOverwriteController *controller = self;
    [alert addActionWithTitle:L(@"NO") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
        [controller alertView:controller.multipleFilesOverwriteAlert willDismissWithButtonIndex:kNo];
        dispatch_async(dispatch_get_main_queue(), ^{
            [controller alertView:controller.multipleFilesOverwriteAlert didDismissWithButtonIndex:kNo];
        });
    }];
    [alert addActionWithTitle:L(@"YES") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
        [controller alertView:controller.multipleFilesOverwriteAlert willDismissWithButtonIndex:kYes];
        dispatch_async(dispatch_get_main_queue(), ^{
            [controller alertView:controller.multipleFilesOverwriteAlert didDismissWithButtonIndex:kYes];
        });
    }];
    [alert addActionWithTitle:L(@"NO_TO_ALL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
        [controller alertView:controller.multipleFilesOverwriteAlert willDismissWithButtonIndex:kNoToAll];
        dispatch_async(dispatch_get_main_queue(), ^{
            [controller alertView:controller.multipleFilesOverwriteAlert didDismissWithButtonIndex:kNoToAll];
        });
    }];
    [alert addActionWithTitle:L(@"YES_TO_ALL") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        [controller alertView:controller.multipleFilesOverwriteAlert willDismissWithButtonIndex:kYesToAll];
        dispatch_async(dispatch_get_main_queue(), ^{
            [controller alertView:controller.multipleFilesOverwriteAlert didDismissWithButtonIndex:kYesToAll];
        });
    }];
}

- (void)dealloc {
    [_sourceFileNames release];
    [_destinationFileNames release];
    
    [_singleFileOverwriteAlert release];
    [_multipleFilesOverwriteAlert release];
    [self.multipleFilesAlertTimer invalidate];
    self.multipleFilesAlertTimer = nil;
    
    [_filenamesMapping release];
    [_filesNeedToBeOverwriten release];
    [_overwriteYesBlock release];
    [_overwriteNoBlock release];

    [super dealloc];
}

- (BOOL)checkOverwriteFiles:(BOOL)forceOverwrite {
    if (forceOverwrite) {
        return NO;
    }
    
    [self.filenamesMapping removeAllObjects];
    for (NSString *sourcefileName in self.sourceFileNames) {
        for (NSString *desintationFileName in self.destinationFileNames) {
            if ([sourcefileName isEqualToString:desintationFileName]) {
                self.filenamesMapping[sourcefileName] = desintationFileName;
            }
        }
    }
    
    self.currentFileOverwriteIndex = 0;
    NSMutableArray *filesNeedToBeOverwritten = [[NSMutableArray alloc] init];
    for (NSString *sourceFileName in self.filenamesMapping.allKeys) {
        CZFileOverwriteEntity *overwriteEntity = [[CZFileOverwriteEntity alloc] init];
        overwriteEntity.fileName = sourceFileName;
        [filesNeedToBeOverwritten addObject:overwriteEntity];
        [overwriteEntity release];
    }
    self.filesNeedToBeOverwriten = filesNeedToBeOverwritten;
    [filesNeedToBeOverwritten release];
    
    if (self.filesNeedToBeOverwriten.count <= 0) {
        return FALSE;
    }
    
    [self handleOverwrite:_saveOverwriteOption];
    return TRUE;
}

- (void)handleOverwrite:(CZIFileSaveOverwriteOption)option {
    switch (option) {
        case kCZAskOnSave: {
            if (self.filesNeedToBeOverwriten.count == 1) {
                CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
                self.singleFileOverwriteAlert.message = [NSString stringWithFormat:L(@"FILES_OVERWRITE_TITLE"),
                                                         [overwriteEntity.fileName lastPathComponent]];
                [self.singleFileOverwriteAlert presentAnimated:YES completion:NULL];
                [self retain];
            } else if (self.filesNeedToBeOverwriten.count > 1) {
                CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
                self.multipleFilesOverwriteAlert.message = [NSString stringWithFormat:L(@"FILES_OVERWRITE_TITLE"),
                                                            [overwriteEntity.fileName lastPathComponent]];
                [self.multipleFilesOverwriteAlert presentAnimated:YES completion:NULL];
                [self retain];
            }
        }
            break;
            
        case kCZAlwaysOverwrite: {
            if (self.overwriteYesBlock) {
                self.overwriteYesBlock(self.filenamesMapping);
            }
        }
            break;
            
        case kCZNeverOverwrite: {
            for (CZFileOverwriteEntity *overwriteEntity in self.filesNeedToBeOverwriten) {
                NSString *newFileName = [CZFileNameGenerator uniqueFilePath:self.filenamesMapping[overwriteEntity.fileName]
                                                                       from:self.destinationFileNames];
                self.filenamesMapping[overwriteEntity.fileName] = newFileName;
            }
            
            if (self.overwriteNoBlock) {
                self.overwriteNoBlock(self.filenamesMapping);
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)multipleAlertViewLoop {
    [self initMultipleAlertView];
    CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
    self.multipleFilesOverwriteAlert.message = [NSString stringWithFormat:L(@"FILES_OVERWRITE_TITLE"),
                                                [overwriteEntity.fileName lastPathComponent]];
    [self.multipleFilesOverwriteAlert presentAnimated:YES completion:NULL];
    [self.multipleFilesAlertTimer invalidate];
    self.multipleFilesAlertTimer = nil;
}

- (void)alertView:(CZAlertController *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView != self.multipleFilesOverwriteAlert) {
        [self release];
        return;
    }
        
    if ((buttonIndex == kYesToAll) ||
        (buttonIndex == kNoToAll)) {
        [self release];
        return;
    }
        
    if (self.currentFileOverwriteIndex >= self.filesNeedToBeOverwriten.count) {
        if (self.overwriteYesBlock && buttonIndex == kYes) {
            self.overwriteYesBlock(self.filenamesMapping);
        } else if (self.overwriteNoBlock && buttonIndex == kNo) {
            self.overwriteNoBlock(self.filenamesMapping);
        }
    } else {        
        if (!self.multipleFilesAlertTimer) {
            self.multipleFilesAlertTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                                            target:self
                                                                          selector:@selector(multipleAlertViewLoop)
                                                                          userInfo:nil
                                                                           repeats:NO];
        }

        [self retain];
    }
    
    [self release];
}

- (void)alertView:(CZAlertController *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == kYes) {
        if (alertView == self.singleFileOverwriteAlert) {
            if (self.overwriteYesBlock) {
                self.overwriteYesBlock(self.filenamesMapping);
            }
        } else if (alertView == self.multipleFilesOverwriteAlert) {
            CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
            overwriteEntity.needOverwrite = TRUE;
            self.currentFileOverwriteIndex++;
        }
    } else if (buttonIndex == kYesToAll) {
        if (self.overwriteYesBlock) {
            self.overwriteYesBlock(self.filenamesMapping);
        }
    } else if (buttonIndex == kNo) {
        CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
        NSString *sourceFileName = overwriteEntity.fileName;
        NSString *newFileName = [CZFileNameGenerator uniqueFilePath:self.filenamesMapping[sourceFileName]
                                                               from:self.destinationFileNames];
        self.filenamesMapping[sourceFileName] = newFileName;
        
        if (alertView == self.singleFileOverwriteAlert) {
            if (self.overwriteNoBlock) {
                self.overwriteNoBlock(self.filenamesMapping);
            }
        } else if (alertView == self.multipleFilesOverwriteAlert) {
            CZFileOverwriteEntity *overwriteEntity = self.filesNeedToBeOverwriten[_currentFileOverwriteIndex];
            overwriteEntity.needOverwrite = FALSE;
            self.currentFileOverwriteIndex++;
        }
    } else if (buttonIndex == kNoToAll) {
        for (CZFileOverwriteEntity *overwriteEntity in self.filesNeedToBeOverwriten) {
            if (!overwriteEntity.needOverwrite) {
                NSString *newFileName = [CZFileNameGenerator uniqueFilePath:self.filenamesMapping[overwriteEntity.fileName]
                                                                       from:self.destinationFileNames];
                self.filenamesMapping[overwriteEntity.fileName] = newFileName;
            }
        }
        
        if (self.overwriteNoBlock) {
            self.overwriteNoBlock(self.filenamesMapping);
        }
    }
}

@end
