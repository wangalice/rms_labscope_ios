//
//  UIImage+CZObjective.h
//  Hermes
//
//  Created by Li, Junlin on 7/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CZObjective)

+ (instancetype)objectiveImageForMagnification:(float)magnification size:(CGSize)size;

@end
