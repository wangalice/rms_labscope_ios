//
//  CZMicroscopeNameValidator.h
//  Hermes
//
//  Created by Li, Junlin on 4/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMicroscopeNameValidator : NSObject <UITextFieldDelegate>

@property (nonatomic, readonly, assign) NSUInteger maxSupportedNameLength;

- (instancetype)initWithMaxSupportedNameLength:(NSUInteger)maxSupportedNameLength;

@end
