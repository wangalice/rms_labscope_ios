//
//  CZIPAddressValidator.m
//  Hermes
//
//  Created by Ding Yu on 13/03/2018.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIPAddressValidator.h"

static const int kMaxDotCount = 3;

@implementation CZIPAddressValidator

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self isPressingBackspace:string withRange:range]) {
        return YES;
    } else if (string.length == 1) {
        // During typing, replacementString normally contains only the
        // single new character that was just typed.
        NSString *existString = [textField text];
        unichar enteredValue = [string characterAtIndex:0];
        NSString *newString = [existString stringByReplacingCharactersInRange:range withString:string];
        if([existString length] == 0) {
            return [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:enteredValue];
        } else {
            BOOL isDot = enteredValue == '.';
            if ([self isValidCharacter:string]) {
                unsigned long dotCount = [self countOfDot:existString];
                if (isDot && dotCount >= kMaxDotCount) {
                    return NO;
                }
                
                BOOL digitalCountLessThanThree = YES;
                for (NSString *digital in [newString componentsSeparatedByString:@"."]) {
                    if (digital.length > 3) {
                        digitalCountLessThanThree = NO;
                        break;
                    }
                }
                return isDot || digitalCountLessThanThree;
            } else {
                return NO;
            }
        }
    } else {
        return NO;
    }
}

- (BOOL)isValidCharacter:(NSString *)input {
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[0-9.]"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSUInteger matchesCount = [regex numberOfMatchesInString:input options:0 range:NSMakeRange(0, [input length])];
    return matchesCount > 0;
}

- (unsigned long)countOfDot:(NSString *) input {
    NSArray<NSString *> *output = [input componentsSeparatedByString:@"."];
    return [output count] - 1;
}

- (BOOL) isPressingBackspace:(NSString *)input withRange:(NSRange)range {
    return range.length == 1 && input.length == 0;
}

@end
