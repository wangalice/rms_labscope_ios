//
//  CZIPAddressValidator.h
//  Hermes
//
//  Created by Ding Yu on 13/03/2018.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZIPAddressValidator : NSObject <UITextFieldDelegate>

@end
