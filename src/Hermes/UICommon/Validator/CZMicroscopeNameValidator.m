//
//  CZMicroscopeNameValidator.m
//  Hermes
//
//  Created by Li, Junlin on 4/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeNameValidator.h"

@implementation CZMicroscopeNameValidator

- (instancetype)initWithMaxSupportedNameLength:(NSUInteger)maxSupportedNameLength {
    self = [super init];
    if (self) {
        _maxSupportedNameLength = maxSupportedNameLength;
    }
    return self;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    unichar *correctName = (unichar *)malloc(sizeof(unichar) * (self.maxSupportedNameLength + 1));
    memset(correctName, 0, self.maxSupportedNameLength + 1);
    NSInteger index = 0;
    BOOL isFirstCorrectCharacterEntered = NO;
    
    for (NSInteger i = 0; i < textField.text.length; i++) {
        unichar c = [textField.text characterAtIndex:i];
        if ([[NSCharacterSet cz_alphanumericCharacterSet] characterIsMember:c]) {
            if (![[NSCharacterSet cz_numericCharacterSet] characterIsMember:c]) {
                isFirstCorrectCharacterEntered = YES;
            }
            
            if (isFirstCorrectCharacterEntered) {
                correctName[index++] = c;
            }
        }
        if (index > self.maxSupportedNameLength - 1) {
            break;
        }
    }
    
    NSString *correctText = [[NSString alloc] initWithCharacters:correctName length:index];
    textField.text = correctText;
    
    free(correctName);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Check whether the name will be longer than the limitation.
    if (textField.text.length + string.length - range.length > self.maxSupportedNameLength) {
        return NO;
    }
    
    // Check whether the string after change will begin with numbers.
    if (range.location == 0) {
        unichar c = 0;
        if (string.length > 0) {
            c = [string characterAtIndex:0];
        } else {
            NSString *newText = [textField.text substringFromIndex:range.length];
            if (newText.length > 0) {
                c = [newText characterAtIndex:0];
            }
        }
        
        if ([[NSCharacterSet cz_numericCharacterSet] characterIsMember:c]) {
            return NO;
        }
    }
    
    for (int i = 0; i < string.length; i++) {
        unichar c = [string characterAtIndex:i];
        if (![[NSCharacterSet cz_alphanumericCharacterSet] characterIsMember:c]) {
            return NO;
        }
    }
    
    return YES;
}

@end
