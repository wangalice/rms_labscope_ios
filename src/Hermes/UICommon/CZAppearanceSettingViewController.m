//
//  CZAppearanceSettingViewController.m
//  Matscope
//
//  Created by Sherry Xu on 7/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAppearanceSettingViewController.h"

static const CGFloat kDefaultPadding = 20.0f;
static const CGFloat kAppearanceButtonXPadding = 10.0f;
static const CGFloat kAppearanceButtonWidth = 48.0f;
static const CGFloat kAppearanceButtonHeight = 48.0f;
static const CGFloat kAppearanceSettingViewPopoverContentViewWidth = 320.0f;
static const CGFloat kAppearanceSettingViewPopoverContentViewHeight = 156.0f;

typedef NSUInteger CZColorButtonIndex;
static const CZColorButtonIndex kCZColorButtonIndexRed = 0;
static const CZColorButtonIndex kCZColorButtonIndexBlue = 1;
static const CZColorButtonIndex kCZColorButtonIndexGreen = 2;
static const CZColorButtonIndex kCZColorButtonIndexWhite = 3;
static const CZColorButtonIndex kCZColorButtonIndexBlack = 4;

typedef NS_ENUM(NSUInteger, CZAnnotationButtonType) {
    kStrokeColor,
    kTextSize,
    kLineWidth,
    kResize,
    kFillColor
};

@interface CZAnnotationAppearanceButton : UIButton

@property (nonatomic, assign) CZAnnotationButtonType appearanceType;
@property (nonatomic, assign) CZColor color;
@property (nonatomic, assign) CZElementSize lineWidth;
@property (nonatomic, assign) BOOL keepAspect;

@end

@implementation CZAnnotationAppearanceButton

@end

@interface CZAppearanceSettingViewController ()

@property (nonatomic, retain) CZAnnotationAppearanceButton *redButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *blueButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *greenButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *yellowButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *blackButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *xLargeWidthButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *largeWidthButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *mediumWidthButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *smallWidthButton;
@property (nonatomic, retain) CZAnnotationAppearanceButton *xSmallWidthButton;

@property (retain, nonatomic) CZAnnotationAppearanceButton *redBackgroundButton;
@property (retain, nonatomic) CZAnnotationAppearanceButton *blueBackgroundButton;
@property (retain, nonatomic) CZAnnotationAppearanceButton *greenBackgroundButton;
@property (retain, nonatomic) CZAnnotationAppearanceButton *whiteBackgroundButton;
@property (retain, nonatomic) CZAnnotationAppearanceButton *blackBackgroundButton;

- (void)appearanceAction:(id)sender;

@end

@implementation CZAppearanceSettingViewController

- (id)init {
    self = [super init];
    if (self) {
        NSMutableArray *colorButtons = [NSMutableArray arrayWithCapacity:5];
        CGRect frame = CGRectMake(kDefaultPadding, kDefaultPadding, kAppearanceButtonWidth, kAppearanceButtonHeight);
        for (NSUInteger i = 0; i < 5; i++) {
            CZAnnotationAppearanceButton *button = [[CZAnnotationAppearanceButton alloc] initWithFrame:frame];
            [button addTarget:self action:@selector(appearanceAction:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-normal")] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-selected")] forState:UIControlStateSelected];
            button.appearanceType = kStrokeColor;
            
            [colorButtons addObject:button];
            CGFloat offset = kAppearanceButtonWidth + kAppearanceButtonXPadding;
            frame.origin.x += offset;
            [button release];
        }
        self.redButton = colorButtons[0];
        self.blueButton = colorButtons[1];
        self.greenButton = colorButtons[2];
        self.yellowButton = colorButtons[3];
        self.blackButton = colorButtons[4];
        
        [self.redButton setImage:[UIImage imageNamed:A(@"red-icon")] forState:UIControlStateNormal];
        self.redButton.color = kCZColorRed;
        
        [self.blueButton setImage:[UIImage imageNamed:A(@"blue-icon")] forState:UIControlStateNormal];
        self.blueButton.color = kCZColorBlue;
        
        [self.greenButton setImage:[UIImage imageNamed:A(@"green-icon")] forState:UIControlStateNormal];
        self.greenButton.color = kCZColorGreen;
        
        [self.yellowButton setImage:[UIImage imageNamed:A(@"yellow-icon")] forState:UIControlStateNormal];
        self.yellowButton.color = kCZColorYellow;
        
        [self.blackButton setImage:[UIImage imageNamed:A(@"black-icon")] forState:UIControlStateNormal];
        self.blackButton.color = kCZColorBlack;

        NSMutableArray *backgroundButtons = [NSMutableArray arrayWithCapacity:5];
        CGFloat offset = kDefaultPadding + kAppearanceButtonHeight;
        frame.origin.y += offset;
        frame.origin.x = kDefaultPadding;
        for (NSUInteger i = 0; i < 5; i++) {
            CZAnnotationAppearanceButton *button = [[CZAnnotationAppearanceButton alloc] initWithFrame:frame];
            [button addTarget:self action:@selector(appearanceAction:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-normal")] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-selected")] forState:UIControlStateSelected];
            button.tag = i;
            button.appearanceType = kFillColor;

            [backgroundButtons addObject:button];
            offset = kAppearanceButtonWidth + kAppearanceButtonXPadding;
            frame.origin.x += offset;
            [button release];
        }
        
        self.redBackgroundButton = backgroundButtons[0];
        self.blueBackgroundButton = backgroundButtons[1];
        self.greenBackgroundButton = backgroundButtons[2];
        self.whiteBackgroundButton = backgroundButtons[3];
        self.blackBackgroundButton = backgroundButtons[4];
        
        [self.redBackgroundButton setImage:[UIImage imageNamed:A(@"red-icon")] forState:UIControlStateNormal];
        self.redBackgroundButton.color = kCZColorRed;
        
        [self.blueBackgroundButton setImage:[UIImage imageNamed:A(@"blue-icon")] forState:UIControlStateNormal];
        self.blueBackgroundButton.color = kCZColorBlue;
        
        [self.greenBackgroundButton setImage:[UIImage imageNamed:A(@"green-icon")] forState:UIControlStateNormal];
        self.greenBackgroundButton.color = kCZColorGreen;
        
        [self.whiteBackgroundButton setImage:[UIImage imageNamed:A(@"white-icon")] forState:UIControlStateNormal];
        self.whiteBackgroundButton.color = kCZColorWhite;
        
        [self.blackBackgroundButton setImage:[UIImage imageNamed:A(@"black-icon")] forState:UIControlStateNormal];
        self.blackBackgroundButton.color = kCZColorBlack;

        NSMutableArray *sizeButtons = [NSMutableArray arrayWithCapacity:5];
        offset = kDefaultPadding + kAppearanceButtonHeight;
        frame.origin.y += offset;
        frame.origin.x = kDefaultPadding;
        for (NSUInteger i = 0; i < 5; i++) {
            CZAnnotationAppearanceButton *button = [[CZAnnotationAppearanceButton alloc] initWithFrame:frame];
            [button addTarget:self action:@selector(appearanceAction:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-normal")] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:A(@"function-round-selected")] forState:UIControlStateSelected];
            [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
            [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            button.appearanceType = kLineWidth;
            
            [sizeButtons addObject:button];
            offset = kAppearanceButtonWidth + kAppearanceButtonXPadding;
            frame.origin.x += offset;
            [button release];
        }
        
        self.xSmallWidthButton = sizeButtons[0];
        self.smallWidthButton = sizeButtons[1];
        self.mediumWidthButton = sizeButtons[2];
        self.largeWidthButton = sizeButtons[3];
        self.xLargeWidthButton = sizeButtons[4];
        
        [self.xSmallWidthButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
        [self.xSmallWidthButton setTitle:L(@"SIZE_XS") forState:UIControlStateNormal];
        self.xSmallWidthButton.lineWidth = CZElementSizeExtraSmall;
        
        [self.smallWidthButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15]];
        [self.smallWidthButton setTitle:L(@"SIZE_S") forState:UIControlStateNormal];
        self.smallWidthButton.lineWidth = CZElementSizeSmall;
        
        [self.mediumWidthButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
        [self.mediumWidthButton setTitle:L(@"SIZE_M") forState:UIControlStateNormal];
        self.mediumWidthButton.lineWidth = CZElementSizeMedium;
        
        [self.largeWidthButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:22]];
        [self.largeWidthButton setTitle:L(@"SIZE_L") forState:UIControlStateNormal];
        self.largeWidthButton.lineWidth = CZElementSizeLarge;
        
        [self.xLargeWidthButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:26]];
        [self.xLargeWidthButton setTitle:L(@"SIZE_XL") forState:UIControlStateNormal];
        self.xLargeWidthButton.lineWidth = CZElementSizeExtraLarge;
    }
    return self;
}

- (void)dealloc {
    [_redButton release];
    [_blueButton release];
    [_greenButton release];
    [_yellowButton release];
    [_blackButton release];
    [_xLargeWidthButton release];
    [_largeWidthButton release];
    [_mediumWidthButton release];
    [_smallWidthButton release];
    [_xSmallWidthButton release];
    
    [_redBackgroundButton release];
    [_blueBackgroundButton release];
    [_greenBackgroundButton release];
    [_whiteBackgroundButton release];
    [_blackBackgroundButton release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.redButton];
    [self.view addSubview:self.blueButton];
    [self.view addSubview:self.greenButton];
    [self.view addSubview:self.yellowButton];
    [self.view addSubview:self.blackButton];
    [self.view addSubview:self.xLargeWidthButton];
    [self.view addSubview:self.largeWidthButton];
    [self.view addSubview:self.mediumWidthButton];
    [self.view addSubview:self.smallWidthButton];
    [self.view addSubview:self.xSmallWidthButton];
    
    [self.view addSubview:self.redBackgroundButton];
    [self.view addSubview:self.blueBackgroundButton];
    [self.view addSubview:self.greenBackgroundButton];
    [self.view addSubview:self.whiteBackgroundButton];
    [self.view addSubview:self.blackBackgroundButton];
    
    
    CGSize size = CGSizeMake(kAppearanceSettingViewPopoverContentViewWidth, kAppearanceSettingViewPopoverContentViewHeight);
    self.preferredContentSize = size;
}

- (void)appearanceAction:(id)sender {
    CZAnnotationAppearanceButton *button = (CZAnnotationAppearanceButton *)sender;
    
    switch (button.appearanceType) {
        case kStrokeColor: {
            [self toggleColorAppearance:button];
            if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceColor:)]) {
                [self.delegate appearanceSettingViewController:self didChangeAppearanceColor:button.color];
            }
        }
            break;
            
        case kFillColor: {
            if (button.isSelected) {
                [self toggleBackgroundColorAppearance:nil];
                
                if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceBackgroundColor:)]) {
                    [self.delegate appearanceSettingViewController:self didChangeAppearanceBackgroundColor:kCZColorTransparent];
                }
            } else {
                CZColor foregroundColor = [self updateForegroundColorWithBackgroundColor:button.tag];
                if (CZColorEqualToColor(foregroundColor, kCZColorTransparent)) {
                    if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceBackgroundColor:)]) {
                        [self.delegate appearanceSettingViewController:self didChangeAppearanceBackgroundColor:button.color];
                    }
                } else {
                    if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceStrokeColor:fillColor:)]) {
                        [self.delegate appearanceSettingViewController:self didChangeAppearanceStrokeColor:foregroundColor fillColor:button.color];
                    }
                }
                [self toggleBackgroundColorAppearance:button];
            }

        }
            break;
        case kLineWidth: {
            [self toggleLineWidthAppearance:button];
            if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceSize:)]) {
                [self.delegate appearanceSettingViewController:self didChangeAppearanceSize:button.lineWidth];
            }
        }
            break;
        default:
            break;
    }
}

- (void)toggleColorAppearance:(UIButton *)buttonSelected {
    self.redButton.selected = NO;
    self.blueButton.selected = NO;
    self.greenButton.selected = NO;
    self.yellowButton.selected = NO;
    self.blackButton.selected = NO;
    buttonSelected.selected = YES;
}

- (void)toggleBackgroundColorAppearance:(UIButton *)buttonSelected {
    for (UIButton *button in @[self.redBackgroundButton,
                               self.blueBackgroundButton,
                               self.greenBackgroundButton,
                               self.whiteBackgroundButton,
                               self.blackBackgroundButton]) {
        button.selected = (buttonSelected == button);
    }
}

- (void)toggleLineWidthAppearance:(UIButton *)buttonSelected {
    self.xLargeWidthButton.selected = NO;
    self.largeWidthButton.selected = NO;
    self.mediumWidthButton.selected = NO;
    self.smallWidthButton.selected = NO;
    self.xSmallWidthButton.selected = NO;
    buttonSelected.selected = YES;
}

- (void)setSelectedColor:(CZColor)color {
    UIButton *button = nil;
 
    if (CZColorEqualToColor(color, kCZColorRed)) {
        button = self.redButton;
    } else if (CZColorEqualToColor(color, kCZColorBlue)) {
        button = self.blueButton;
    } else if (CZColorEqualToColor(color, kCZColorGreen)) {
        button = self.greenButton;
    } else if (CZColorEqualToColor(color, kCZColorYellow)) {
        button = self.yellowButton;
    } else if (CZColorEqualToColor(color, kCZColorBlack)) {
        button = self.blackButton;
    }
    
    [self toggleColorAppearance:button];
}

- (void)setSelectedSize:(CZElementSize)size {
    UIButton *button = nil;
    
    if (size == CZElementSizeExtraLarge) {
        button = self.xLargeWidthButton;
    } else if (size == CZElementSizeLarge) {
        button = self.largeWidthButton;
    } else if (size == CZElementSizeMedium) {
        button = self.mediumWidthButton;
    } else if (size == CZElementSizeSmall) {
        button = self.smallWidthButton;
    } else if (size == CZElementSizeExtraSmall) {
        button = self.xSmallWidthButton;
    }

    [self toggleLineWidthAppearance:button];
}

- (void)setSelectedBackgroundColor:(CZColor)fillColor {
    UIButton *selectedButton = nil;
    
    if (CZColorEqualToColor(fillColor, kCZColorRed)) {
        selectedButton = self.redBackgroundButton;
    } else if (CZColorEqualToColor(fillColor, kCZColorBlue)) {
        selectedButton = self.blueBackgroundButton;
    } else if (CZColorEqualToColor(fillColor, kCZColorGreen)) {
        selectedButton = self.greenBackgroundButton;
    } else if (CZColorEqualToColor(fillColor, kCZColorWhite)) {
        selectedButton = self.whiteBackgroundButton;
    } else if (CZColorEqualToColor(fillColor, kCZColorBlack)) {
        selectedButton = self.blackBackgroundButton;
    } else if (CZColorEqualToColor(fillColor, kCZColorTransparent)) {
        selectedButton = nil;
    }
    [self toggleBackgroundColorAppearance:selectedButton];
}


- (CZColor)updateForegroundColorWithBackgroundColor:(CZColorButtonIndex)backgroundColor {
    CZColorButtonIndex originalForegroundColorIndex;
    if (self.redButton.isSelected) {
        originalForegroundColorIndex = kCZColorButtonIndexRed;
    } else if (self.blueButton.isSelected) {
        originalForegroundColorIndex = kCZColorButtonIndexBlue;
    } else if (self.greenButton.isSelected) {
        originalForegroundColorIndex = kCZColorButtonIndexGreen;
    } else if (self.yellowButton.isSelected) {
        originalForegroundColorIndex = kCZColorButtonIndexWhite;
    } else if (self.blackButton.isSelected) {
        originalForegroundColorIndex = kCZColorButtonIndexBlack;
    } else {
        originalForegroundColorIndex = kCZColorButtonIndexRed;
    }
    CZColor foregroundColor = kCZColorRed;;
    if (originalForegroundColorIndex == backgroundColor) {
        switch (backgroundColor) {
            case kCZColorButtonIndexRed:
                foregroundColor = kCZColorBlue;
                break;
                
            case kCZColorButtonIndexBlue:
                foregroundColor = kCZColorGreen;
                break;
                
            case kCZColorButtonIndexGreen:
            case kCZColorButtonIndexWhite:
                foregroundColor = kCZColorBlack;
                break;
                
            case kCZColorButtonIndexBlack:
                foregroundColor = kCZColorRed;
                
            default:
                break;
        }
        if ([self.delegate respondsToSelector:@selector(appearanceSettingViewController:didChangeAppearanceColor:)]) {
            [self.delegate appearanceSettingViewController:self didChangeAppearanceColor:foregroundColor];
        }
        [self setSelectedColor:foregroundColor];
    } else {
        foregroundColor = kCZColorTransparent;
    }
    
    return foregroundColor;
}

- (void)updateAppearanceSettingViewColorButton:(BOOL)enableColorButton backgroundButton:(BOOL)enableBackgroundColorButton {
    self.redButton.enabled = enableColorButton;
    self.yellowButton.enabled = enableColorButton;
    self.blackButton.enabled = enableColorButton;
    self.greenButton.enabled = enableColorButton;
    self.blueButton.enabled = enableColorButton;
    
    for (UIButton *colorButton in @[self.redBackgroundButton,
                                    self.blueBackgroundButton,
                                    self.greenBackgroundButton,
                                    self.whiteBackgroundButton,
                                    self.blackBackgroundButton]) {
        colorButton.hidden = !enableBackgroundColorButton;
    }
    
    CGFloat centerY;
    if (enableBackgroundColorButton) {
        centerY = 156 + 24;
    } else {
        centerY = self.redBackgroundButton.center.y;
    }
    // set the size label origin outside
    for (UIView *control in @[self.xSmallWidthButton,
                              self.smallWidthButton,
                              self.mediumWidthButton,
                              self.largeWidthButton,
                              self.xLargeWidthButton]) {
        CGPoint center = control.center;
        center.y = centerY;
        control.center = center;
    }
    
    CGRect frame = self.view.frame;
    frame.size.height = CGRectGetMaxY(self.xSmallWidthButton.frame) + 20; // leave 20 margin
    self.view.frame = frame;
}

@end
