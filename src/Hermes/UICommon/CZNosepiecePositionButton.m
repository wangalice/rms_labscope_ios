//
//  CZNosepiecePositionButton.m
//  Hermes
//
//  Created by Li, Junlin on 7/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepiecePositionButton.h"
#import "UIImage+CZObjective.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

const CGFloat CZNosepiecePositionButtonSize = 87.0;

@interface CZNosepiecePositionButton ()

@property (nonatomic, strong) UILabel *positionLabel;

@end

@implementation CZNosepiecePositionButton

- (instancetype)initWithPosition:(NSUInteger)position magnification:(float)magnification displayMagnification:(NSString *)displayMagnification {
    self = [super initWithLevel:CZControlEmphasisDefault];
    if (self) {
        self.frame = CGRectMake(0.0, 0.0, CZNosepiecePositionButtonSize, CZNosepiecePositionButtonSize);
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 5.0, 0.0);
        
        _positionLabel = [[UILabel alloc] initWithFrame:CGRectMake(7.0, 4.0, 16.0, 16.0)];
        _positionLabel.backgroundColor = [UIColor clearColor];
        _positionLabel.text = [NSString stringWithFormat:@"%ld", (long)(position + 1)];
        _positionLabel.font = [UIFont cz_label2];
        _positionLabel.textColor = [UIColor cz_gs90];
        _positionLabel.highlightedTextColor = [UIColor cz_gs50];
        _positionLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_positionLabel];
        
        UIImageView *objectiveImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 32.0, 32.0)];
        objectiveImageView.center = CGPointMake(CZNosepiecePositionButtonSize / 2, CZNosepiecePositionButtonSize / 2 - 4.0);
        objectiveImageView.backgroundColor = [UIColor clearColor];
        objectiveImageView.image = [UIImage objectiveImageForMagnification:magnification size:CZIconSizeLarge];
        [self addSubview:objectiveImageView];
        
        [self setTitle:displayMagnification forState:UIControlStateNormal];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.positionLabel.highlighted = selected;
}

@end
