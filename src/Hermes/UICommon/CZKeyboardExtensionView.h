//
//  CZKeyboardExtensionView.h
//  Matscope
//
//  Created by Halley Gu on 8/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZKeyboardExtensionView : UIView <UIInputViewAudioFeedback>

@property (nonatomic, assign) UITextField *textField;

@end
