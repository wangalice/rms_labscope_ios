//
//  CZHelpOverlayView.m
//  Hermes
//
//  Created by Ralph Jin on 7/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZHelpOverlayView.h"
#import "UINavigationBar+CZHelpOverlayView.h"

static const CGFloat kDefaultPadding = 8;
static const CGFloat kReEnableLabelBottomPadding = 100;
static const CGFloat kBackgroudOpacity = 0.7;
static const CGFloat kCheckBoxHeight = 44;

static const CGFloat kIntroductoryViewWidth = 400;
static const CGFloat kIntroductoryViewHeight = 600;

static NSString * const kDefaultFontName = @"HelveticaNeue-BoldItalic";

static CZHelpOverlayView *currentHelpOverlayView = nil;

@interface CZHelpOverlayView () <UIWebViewDelegate> {
    CZHelpOverlayDirection _direction;
}

@property (nonatomic, retain) UIButton *nextTimeShownButton;
@property (nonatomic, retain) UIWebView *introductoryView;

@end

@implementation CZHelpOverlayView

+ (UIControl *)findBarButtonItem:(UIBarButtonItem *)barButtonItem onBar:(UINavigationBar *)bar {
    return [bar findBarButtonItem:barButtonItem];
}

+ (CZHelpOverlayView *)current {
    return currentHelpOverlayView;
}

- (id)init {
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    UIView *rootView = [[mainWindow rootViewController] view];
    self = [self initWithFrame:rootView.frame];
    if (self) {
        self.transform = rootView.transform;
        self.frame = rootView.frame;
        
        [mainWindow addSubview:self];
        [mainWindow bringSubviewToFront:self];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:kBackgroudOpacity]];
                
        // create next time shown toggle button
        UIImage *radioSelectedImage = [UIImage imageNamed:A(@"check-box-s")];
        UIImage *radioDeselectedImage = [UIImage imageNamed:A(@"check-box")];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextTimeShownButton = [button retain];
        [button setImage:radioDeselectedImage forState:UIControlStateNormal];
        [button setImage:radioDeselectedImage forState:UIControlStateDisabled];
        [button setImage:radioSelectedImage forState:UIControlStateSelected];
        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        button.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:16];
        button.titleLabel.textColor = [UIColor whiteColor];
        [button setTitle:L(@"HELP_OVERLAY_SHOW_TOGGLE") forState:UIControlStateNormal];
        CGSize fitSizeForTitleLabel = [button.titleLabel sizeThatFits:CGSizeMake(500, 100)];
        button.imageEdgeInsets = UIEdgeInsetsMake(kDefaultPadding, 2*kDefaultPadding, kDefaultPadding, 0);
        button.titleEdgeInsets = UIEdgeInsetsMake(2*kDefaultPadding, 3*kDefaultPadding, kDefaultPadding, 0);
        button.selected = YES;

        button.translatesAutoresizingMaskIntoConstraints = NO;
        [button addTarget:self action:@selector(nextTimeShownButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
                
        UILabel *reenableLabel = [[UILabel alloc] init];
        reenableLabel.font = [UIFont fontWithName:kDefaultFontName size:14];
        reenableLabel.textAlignment = NSTextAlignmentLeft;
        reenableLabel.textColor = [UIColor whiteColor];
        reenableLabel.backgroundColor = [UIColor clearColor];
        reenableLabel.numberOfLines = 2;
        reenableLabel.lineBreakMode = NSLineBreakByWordWrapping;
        reenableLabel.text = L(@"HELP_OVERLAY_REENABLE_LABEL");
        CGSize fitSize = [reenableLabel sizeThatFits:CGSizeMake(360, 100)];
        [button addSubview:reenableLabel];
        
        reenableLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [button addConstraint:[NSLayoutConstraint constraintWithItem:reenableLabel
                                                         attribute:NSLayoutAttributeLeft
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button
                                                         attribute:NSLayoutAttributeLeft
                                                        multiplier:1.0f
                                                          constant:2*kDefaultPadding]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:reenableLabel
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button.imageView
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0f
                                                          constant:2*kDefaultPadding]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:reenableLabel
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0f
                                                          constant:fitSize.width]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:reenableLabel
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0f
                                                          constant:fitSize.height]];
        [button sendSubviewToBack:reenableLabel];
        
        [reenableLabel release];
        
        UIImageView *backgroundView = [[UIImageView alloc] init];
        UIImage *bgImage = [UIImage imageNamed:A(@"help-overlay-background")];
        backgroundView.image = [bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)
                                                       resizingMode:UIImageResizingModeStretch];
        backgroundView.userInteractionEnabled = NO;
        backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [button addSubview:backgroundView];
        [button sendSubviewToBack:backgroundView];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1.0f
                                                          constant:0]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView
                                                         attribute:NSLayoutAttributeRight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button
                                                         attribute:NSLayoutAttributeRight
                                                        multiplier:1.0f
                                                          constant:0]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0f
                                                          constant:0]];
        
        [button addConstraint:[NSLayoutConstraint constraintWithItem:backgroundView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:button
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0f
                                                          constant:0]];
        [backgroundView release];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:0.0f
                                                          constant:fitSize.height + kCheckBoxHeight + 4*kDefaultPadding]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:0.0f
                                                          constant:MAX(fitSizeForTitleLabel.width + radioSelectedImage.size.width, fitSize.width) + 4*kDefaultPadding]];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeRight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeRight
                                                        multiplier:1.0f
                                                          constant:-kDefaultPadding]];

        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0f
                                                          constant:-kReEnableLabelBottomPadding]];
        
        // install dismiss gesture
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        [self addGestureRecognizer:tapGestureRecognizer];
        [tapGestureRecognizer release];
        currentHelpOverlayView = self;
    }
    return self;
}

- (void)dealloc {
    if (self == currentHelpOverlayView) {
        currentHelpOverlayView = nil;
    }
    
    [_introductoryView release];
    [_nextTimeShownButton release];
    [_hideNextTimeKey release];
    [super dealloc];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.subviews.count < 1) {
        return;
    }
    
    for (UIView *subView in self.subviews) {
        if (subView.tag <= 0) {
            return;
        }
        
        [self removeCollisionForMasterView:subView withSlaveView:_nextTimeShownButton];
    }
}

- (void)addBalloonText:(NSString *)text atFrame:(CGRect)frame toView:(UIView *)view direction:(CZBalloonDirection)direction {
    CZBalloonView *balloon = [self newBalloonWithText:text frame:frame direction:direction];
    [self insertSubview:balloon belowSubview:_nextTimeShownButton];
    balloon.tag = self.subviews.count - 1;
    if (self.subviews.count >= 1 && balloon.tag - 1 > 0) {
        [self removeCollisionForMasterView:[self viewWithTag:balloon.tag - 1] withSlaveView:balloon];
    }
    [balloon pointToView:view];
    [balloon release];
}

- (void)addBalloonText:(NSString *)text atFrame:(CGRect)frame toPoint:(CGPoint)point {
    CZBalloonView *balloon = [self newBalloonWithText:text frame:frame direction:CZBalloonDirectionAny];
    [self insertSubview:balloon belowSubview:_nextTimeShownButton];
    balloon.tag = self.subviews.count - 1;
    if (self.subviews.count >= 1 && balloon.tag - 1 > 0) {
        [self removeCollisionForMasterView:[self viewWithTag:balloon.tag - 1] withSlaveView:balloon];
    }
    [balloon pointToPoint:point];
    [balloon release];
}

- (void)removeBolloomWithText:(NSString *)text {
    for (CZBalloonView *balloon in self.subviews) {
        if([balloon isKindOfClass:[CZBalloonView class]] &&
           [balloon.textLabel.text isEqualToString:text]) {
            [balloon removeFromSuperview];
            break;
        }
    }
}

- (void)setIntroductoryText:(NSString *)introductoryText bulletPoints:(NSArray *)bulletPoints {
    [self setIntroductoryText:introductoryText bulletPoints:bulletPoints direction:CZHelpOverlayDirectionTopRight];
}

- (void)setIntroductoryText:(NSString *)introductoryText bulletPoints:(NSArray *)bulletPoints direction:(CZHelpOverlayDirection)direction {
    if (_introductoryView == nil) {
        _direction = direction;
        
        CGRect frame = CGRectMake(0, 0, kIntroductoryViewWidth, kIntroductoryViewHeight);
        CGRect superFrame = self.bounds;
        if (CZHelpOverlayDirectionTopLeft == _direction) {
            frame.origin.x = kDefaultPadding;
            frame.origin.y = kUISystemStatusBarHeight;
        } else if (CZHelpOverlayDirectionTopRight == _direction) {
            frame.origin.x = superFrame.size.width - kDefaultPadding - kUISystemStatusBarHeight - kIntroductoryViewWidth;
            frame.origin.y = kUISystemStatusBarHeight;
        } else {
            frame.origin.x = kDefaultPadding;
            frame.origin.y = superFrame.size.height - kDefaultPadding - kIntroductoryViewHeight;
        }

        _introductoryView = [[UIWebView alloc] initWithFrame:frame];
        _introductoryView.delegate = self;
        _introductoryView.opaque = NO;
        _introductoryView.backgroundColor = [UIColor clearColor];
        _introductoryView.userInteractionEnabled = NO;
        [self insertSubview:_introductoryView belowSubview:_nextTimeShownButton];
        _introductoryView.tag = self.subviews.count - 1;
        
        NSMutableString *bulletPointsHtml = [[NSMutableString alloc] init];
        if (bulletPoints.count > 0) {
            [bulletPointsHtml appendString:@"<ul>"];
            for (id bulletPoint in bulletPoints) {
                [bulletPointsHtml appendString:[NSString stringWithFormat:@"<li>%@</li>", bulletPoint]];
            }
            [bulletPointsHtml appendString:@"</ul>"];
        }
        
        NSString *htmlString = [NSString stringWithFormat:@"<html><head>\
                                <style type=\"text/css\">body{font-family:%@;font-style:italic;background-color:transparent;color:#FFF}</style>\
                                </head><body><h3>%@</h3>%@</body></html>", kDefaultFontName, introductoryText, bulletPointsHtml];
        [bulletPointsHtml release];
        
        [_introductoryView loadHTMLString:htmlString baseURL:nil];
    }
}

- (BOOL)translateView:(UIView *)targetView inXDirectionBy:(CGFloat)distance {
    CGRect targetFrame = targetView.frame;
    targetFrame.origin.x += distance;
    targetView.frame = targetFrame;
    
    CGPoint edgePoint = CGPointZero;
    edgePoint.x = CGRectGetMaxX(targetFrame);
    edgePoint.y = CGRectGetMaxY(targetFrame);
    if (distance < 0) {
        edgePoint.x = CGRectGetMinX(targetFrame);
    }
    
    if (CGRectContainsPoint(targetView.superview.bounds, edgePoint)) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)translateView:(UIView *)targetView inYDirectionBy:(CGFloat)distance {
    CGRect targetFrame = targetView.frame;
    targetFrame.origin.y += distance;
    targetView.frame = targetFrame;
    
    CGPoint edgePoint = CGPointZero;
    edgePoint.x = CGRectGetMaxX(targetFrame);
    edgePoint.y = CGRectGetMaxY(targetFrame);
    if (distance < 0) {
        edgePoint.y = CGRectGetMinY(targetFrame);
    }
    
    if (CGRectContainsPoint(targetView.superview.bounds, edgePoint)) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)removeCollisionForMasterView:(UIView *)masterView withSlaveView:(UIView *)slaveView {
    CGRect originalSlaveFrame = slaveView.frame;
    
    CGRect intersectRect = CGRectIntersection(masterView.frame, slaveView.frame);
    if (CGRectIsNull(intersectRect)) {
        return YES;
    }
    
    CGFloat dx = 0.0;
    CGFloat dy = 0.0;
    
    if (CGRectGetMinX(intersectRect) == CGRectGetMinX(slaveView.frame)) {
        dx = intersectRect.size.width + kDefaultPadding;
        
        if (CGRectGetMinX(slaveView.frame) + dx < CGRectGetMaxX(masterView.frame)) {
            CGFloat leftDistance = CGRectGetMaxX(slaveView.frame) - CGRectGetMinX(masterView.frame) + kDefaultPadding;
            CGFloat rightDistance = CGRectGetMaxX(masterView.frame) - CGRectGetMinX(slaveView.frame) + kDefaultPadding;
            dx = rightDistance;
            if (rightDistance > leftDistance) {
                dx = -leftDistance;
            }
        }
    }
    
    if (CGRectGetMinX(intersectRect) == CGRectGetMinX(masterView.frame)) {
        dx = -(intersectRect.size.width + kDefaultPadding);
        
        if (CGRectGetMaxX(masterView.frame) + dx > CGRectGetMinX(slaveView.frame)) {
            CGFloat leftDistance = CGRectGetMaxX(slaveView.frame) - CGRectGetMinX(masterView.frame) + kDefaultPadding;
            CGFloat rightDistance = CGRectGetMaxX(masterView.frame) - CGRectGetMinX(slaveView.frame) + kDefaultPadding;
            dx = rightDistance;
            if (rightDistance > leftDistance) {
                dx = -leftDistance;
            }
        }
    }
    
    if (CGRectGetMinY(intersectRect) == CGRectGetMinY(slaveView.frame)) {
        dy = intersectRect.size.height + kDefaultPadding;
        
        if (CGRectGetMinY(slaveView.frame) + dy < CGRectGetMaxY(masterView.frame)) {
            CGFloat upDistance = CGRectGetMaxY(slaveView.frame) - CGRectGetMinY(masterView.frame) + kDefaultPadding;
            CGFloat downDistance = CGRectGetMaxY(masterView.frame) - CGRectGetMinY(slaveView.frame) + kDefaultPadding;
            dy = downDistance;
            if (downDistance > upDistance) {
                dy = -upDistance;
            }
        }
    }
    
    if (CGRectGetMinY(intersectRect) == CGRectGetMinY(masterView.frame)) {
        dy = -(intersectRect.size.height + kDefaultPadding);
        
        if (CGRectGetMaxY(masterView.frame) + dy > CGRectGetMinY(slaveView.frame)) {
            CGFloat upDistance = CGRectGetMaxY(slaveView.frame) - CGRectGetMinY(masterView.frame) + kDefaultPadding;
            CGFloat downDistance = CGRectGetMaxY(masterView.frame) - CGRectGetMinY(slaveView.frame) + kDefaultPadding;
            dy = downDistance;
            if (downDistance > upDistance) {
                dy = -upDistance;
            }
        }
    }
    
    BOOL isTranslateSuccess = YES;
    if (fabs(dx) < fabs(dy)) {
        if (![self translateView:slaveView inXDirectionBy:dx]) {
            slaveView.frame = originalSlaveFrame;
            isTranslateSuccess = [self translateView:slaveView inYDirectionBy:dy];
        }
    } else {
        if (![self translateView:slaveView inYDirectionBy:dy]) {
            slaveView.frame = originalSlaveFrame;
            isTranslateSuccess = [self translateView:slaveView inXDirectionBy:dy];
        }
    }
    
    if (!isTranslateSuccess) {
        slaveView.frame = originalSlaveFrame;
    }
    
    return !isTranslateSuccess;
}

#pragma mark - event handlers

- (void)nextTimeShownButtonAction:(id)sender {
    self.nextTimeShownButton.selected = !self.nextTimeShownButton.isSelected;
}

- (void)viewTapped:(UIGestureRecognizer *)recognizer {
    if (! _nextTimeShownButton.isSelected) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:self.hideNextTimeKey];
        [defaults setObject:@"0" forKey:kInfoScreen];
    }
    
    [self dismiss];
}

- (void)dismiss {
    self.hidden = YES;
    [self removeFromSuperview];
    
    if (self == currentHelpOverlayView) {
        currentHelpOverlayView = nil;
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    // calculate fitting size
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeMake(kIntroductoryViewWidth, 0)];
    frame.size = fittingSize;
    
    // put at one of three corners
    CGRect superFrame = self.bounds;
    if (CZHelpOverlayDirectionTopLeft == _direction) {
        frame.origin.x = kDefaultPadding;
        frame.origin.y = kUISystemStatusBarHeight;
    } else if (CZHelpOverlayDirectionTopRight == _direction) {
        frame.origin.x = superFrame.size.width - kDefaultPadding - kUISystemStatusBarHeight - kIntroductoryViewWidth;
        frame.origin.y = kUISystemStatusBarHeight;
    } else {
        frame.origin.x = kDefaultPadding;
        frame.origin.y = superFrame.size.height - kDefaultPadding - fittingSize.height;
    }
    
    aWebView.frame = frame;
}

#pragma mark - private methods

- (CZBalloonView *)newBalloonWithText:(NSString *)text frame:(CGRect)frame direction:(CZBalloonDirection)direction {
    CZBalloonView *balloon = [[CZBalloonView alloc] initWithFrame:frame direction:direction];
    [balloon setHelpText:text];
    
    UIImage *arrowImage = [[UIImage imageNamed:A(@"arrow")] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 32)
                                                                                   resizingMode:UIImageResizingModeStretch];
    [balloon setArrowImage:arrowImage];
    
    return balloon;
}

@end
