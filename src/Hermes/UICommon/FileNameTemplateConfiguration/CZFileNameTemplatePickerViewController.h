//
//  CZFileNameTemplatePickerViewController.h
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileNameTemplate;
@class CZFileNameTemplatePickerViewController;

@protocol CZFileNameTemplatePickerViewControllerDelegate <NSObject>

@optional
- (void)fileNameTemplatePickerViewController:(CZFileNameTemplatePickerViewController *)picker didSelectFileNameTemplate:(CZFileNameTemplate *)template;

@end

@interface CZFileNameTemplatePickerViewController : UIViewController

@property (nonatomic, weak) id<CZFileNameTemplatePickerViewControllerDelegate> delegate;
@property (nonatomic, readonly, strong) NSArray<CZFileNameTemplate *> *selectableFileNameTemplates;
@property (nonatomic, readonly, strong) CZFileNameTemplate *selectedFileNameTemplate;

- (instancetype)initWithFileNameTemplatePath:(NSString *)fileNameTemplatePath;

@end
