//
//  CZFileNameTemplateCell.h
//  Hermes
//
//  Created by Li, Junlin on 6/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFileNameEditor.h"

@class CZFileNameTemplateCell;

@protocol CZFileNameTemplateCellDelegate <NSObject>

@optional
- (BOOL)fileNameTemplateCell:(CZFileNameTemplateCell *)cell validateTempateName:(NSString *)templateName;
- (void)fileNameTemplateCell:(CZFileNameTemplateCell *)cell didBeginEditingTemplateName:(NSString *)templateName;
- (void)fileNameTemplateCell:(CZFileNameTemplateCell *)cell didFinishEditingTemplateName:(NSString *)templateName;

@end

@interface CZFileNameTemplateCell : UITableViewCell

@property (nonatomic, weak) id<CZFileNameTemplateCellDelegate> delegate;
@property (nonatomic, assign, getter=isEditable) BOOL editable;
@property (nonatomic, readonly, strong) UIImageView *iconView;
@property (nonatomic, readonly, strong) CZFileNameEditor *fileNameEditor;
@property (nonatomic, readonly, strong) UIView *separator;

@end
