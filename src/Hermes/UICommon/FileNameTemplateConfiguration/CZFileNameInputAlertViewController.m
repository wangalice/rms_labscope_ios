//
//  CZFileNameInputFieldController.m
//  Hermes
//
//  Created by Ralph Jin on 5/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameInputAlertViewController.h"
#import "AHAlertView.h"
#import <Common/CZCommonUtils.h>
#import <Common/CZDefaultSettings.h>
#import <Common/NSFileManager+CommonPath.h>
#import <DocManager/CZDefaultSettings+DocManager.h>
#import <DocManager/CZImageProperties.h>
#import <DocManager/CZDocManager.h>
#import <FileKit/FileKit.h>

@interface CZFileNameInputAlertViewController () <UITextFieldDelegate> {
    AHAlertView *_alert;
    BOOL _preTextChanged;
}

@property (nonatomic, retain) NSMutableArray *textFields;

@end

@implementation CZFileNameInputAlertViewController

- (id)initWithDocManager:(CZDocManager *)docManager {
    self = [super init];
    if (self) {
        _generator = [[CZFileNameGenerator alloc] init];
        _generator.extension = @"czi";
        _generator.operatorName = [CZDefaultSettings sharedInstance].operatorName;
        _generator.microscope = docManager.imageProperties.microscopeName;
        _generator.objective = docManager.imageProperties.magnificationFactorString;
        _generator.captureDate = docManager.captureDate;
        if (_generator.captureDate == nil) {
            _generator.captureDate = [NSDate date];
        }
        [_generator useDefaultValueWhenEmpty];
                        
        CZFileNameTemplate *template = nil;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSURL *defaultTemplateURL = [defaults URLForKey:kDefaultFileTemplateURL];
        if (defaultTemplateURL) {
            NSString *defaultTemplatePath = [[defaults URLForKey:kDefaultFileTemplateURL] relativePath];
            template = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplatePath];
        }
        
        // TODO: create default file name template
    
        if (template) {
            _generator.fileNameTemplate = template;
        } else {
            return nil;
        }
    }
    
    return self;
}

- (BOOL)requiresUserInput {
    CZFileNameTemplate *template = _generator.fileNameTemplate;
    for (NSUInteger i = 0; i < [template fieldCount]; i++) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        if (field.type == kCZFileNameTemplateFieldTypeFreeText && field.isUserInput) {
            return YES;
        }
    }
    
    return NO;
}

- (void)showInputAlert {
    NSMutableArray *labels = [NSMutableArray array];
    NSString *labelFormat = L(@"FNT_INPUT_FIELD_LABLE_FORMAT");
    NSMutableArray *controls = [[NSMutableArray alloc] init];
    NSMutableArray *textFields = [[NSMutableArray alloc] init];
    
    const CGFloat kDefaultWidth = 100;
    const CGFloat kDefaultHeight = 20;
    
    CZFileNameTemplate *template = _generator.fileNameTemplate;
    for (NSUInteger i = 0; i < [template fieldCount]; i++) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        NSUInteger inputFieldCount = i + 1;
        
        BOOL freeTextNeedsInput = (field.type == kCZFileNameTemplateFieldTypeFreeText && field.isUserInput);
        
        if (freeTextNeedsInput) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kDefaultWidth, kDefaultHeight)];
            label.text = [NSString stringWithFormat:labelFormat, inputFieldCount];
            label.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
            label.tag = i;
            [labels addObject:label];
        }
        
        if (freeTextNeedsInput) {
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, kDefaultWidth, kDefaultHeight)];
            textField.text = field.value;
            textField.tag = i;
            [textField setDelegate:self];
            [textField addTarget:self action:@selector(textFieldDidEditingChanged:) forControlEvents:UIControlEventEditingChanged];
            [controls addObject:textField];
            [textFields addObject:textField];
        }
    }
    
    if ([controls count] > 0) {
        NSString *title = [self uniqueName];
        title = [title stringByDeletingPathExtension];
        NSString *message = L(@"FNT_INPUT_ALERT_MESSAGE");
        
        _alert = [[AHAlertView alloc] initWithTitle:title message:message plainControls:controls plainTextLabels:labels];
        _alert.alertViewStyle = AHAlertViewStyleMultipleControlInputs;
        _alert.dismissalStyle = AHAlertViewDismissalStyleZoomDown;
        __block CZFileNameInputAlertViewController *controller = self;
        [_alert addButtonWithTitle:L(@"OK") block:^ {
            [controller okButtonPressAction];
        }];
        [_alert setDestructiveButtonTitle:L(@"DISCARD") block:^ {
            [controller discardButtonPressAction];
        }];
        [_alert show];
    }
    
    self.textFields = [NSMutableArray arrayWithArray:textFields];
}

- (NSString *)uniqueName {
#if DCM_REFACTORED
    return [_generator uniqueFileNameInDirectory:[CZLocalFileList sharedInstance].documentPath];
#else
    return [_generator uniqueFileNameInDirectory:[NSFileManager defaultManager].cz_documentPath];
#endif
   
}

- (void)okButtonPressAction {
    if (_preTextChanged) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSURL *defaultTemplateURL = [defaults URLForKey:kDefaultFileTemplateURL];
        if (defaultTemplateURL) {
            NSString *defaultTemplatePath = [[defaults URLForKey:kDefaultFileTemplateURL] relativePath];
            CZFileNameTemplate *template = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplatePath];
            
            NSUInteger fieldCount = MIN([template fieldCount], [_generator.fileNameTemplate fieldCount]);
            for ( NSUInteger i = 0; i < fieldCount; ++i) {
                CZFileNameTemplateField *field = [template fieldAtIndex:i];
                CZFileNameTemplateField *srcField = [_generator.fileNameTemplate fieldAtIndex:i];
                if (field.type == kCZFileNameTemplateFieldTypePreText) {
                    field.value = srcField.value;
                }
            }
            
            [template writeToFile:defaultTemplatePath];
        }
    }
    
    [self.delegate fileNameInputAlertDidPressOK:self];
}

- (void)discardButtonPressAction {
    [self.delegate fileNameInputAlertDidPressDiscard:self];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEditingChanged:(id)sender {
    UITextField *textField = (UITextField *)sender;
    NSUInteger i = textField.tag;
    CZFileNameTemplate *template = _generator.fileNameTemplate;
    if (i < [template fieldCount]) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        field.value = textField.text;
        
        NSString *title = [self uniqueName];
        title = [title stringByDeletingPathExtension];
        [_alert setTitle:title];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    return [CZCommonUtils isStringValidFileNameTemplate:textEntered];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    UITextField *previousTextField = nil;
    for (UITextField *tf in self.textFields) {
        if (previousTextField == textField) {
            [tf becomeFirstResponder];
            return YES;
        }
        previousTextField = tf;
    }
    
    return NO;
}

@end
