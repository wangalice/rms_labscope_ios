//
//  CZFileNameInputAlertController.h
//  Hermes
//
//  Created by Ralph Jin on 5/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZDocManager;
@class CZFileNameGenerator;
@class CZFileNameInputAlertController;

@protocol CZFileNameInputAlertViewControllerDelegate <NSObject>

- (void)fileNameInputAlertDidPressOK:(CZFileNameInputAlertController *)alert;
- (void)fileNameInputAlertDidPressDiscard:(CZFileNameInputAlertController *)alert;

@end

@interface CZFileNameInputAlertController : NSObject

@property (nonatomic, assign) id<CZFileNameInputAlertViewControllerDelegate> delegate;
@property (nonatomic, retain, readonly) CZDocManager *docManager;
@property (nonatomic, retain, readonly) CZFileNameGenerator *generator;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithDocManager:(CZDocManager *)docManager NS_DESIGNATED_INITIALIZER;

/*! @return return YES, if need user input some text; otherwise NO.
 Always call this method before call showInputAlert.
 */
- (BOOL)requiresUserInput;

/*! Show user input text field alert window.
 Always check requiresUserInput return YES, then call this method.
 */
- (void)showInputAlert;

/*! return unique file name in string.
 The string contains file extension but no parent folder path.
 */
- (NSString *)uniqueName;

@end
