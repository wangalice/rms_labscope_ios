//
//  CZFileNameTemplateConfigurationViewController.h
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileNameTemplateConfigurationViewController;

@protocol CZFileNameTemplateConfigurationViewControllerDelegate <NSObject>

@optional
- (void)fileNameTemplateConfigurationViewControllerWillSaveConfiguration:(CZFileNameTemplateConfigurationViewController *)fileNameTemplateConfigurationViewController;
- (void)fileNameTemplateConfigurationViewControllerDidSaveConfiguration:(CZFileNameTemplateConfigurationViewController *)fileNameTemplateConfigurationViewController;

@end

@interface CZFileNameTemplateConfigurationViewController : UIViewController

@property (nonatomic, weak) id<CZFileNameTemplateConfigurationViewControllerDelegate> delegate;

- (instancetype)initWithPath:(NSString *)path;

@end
