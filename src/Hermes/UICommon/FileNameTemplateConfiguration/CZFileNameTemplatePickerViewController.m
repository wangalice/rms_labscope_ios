//
//  CZFileNameTemplatePickerViewController.m
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameTemplatePickerViewController.h"
#import "CZFileNameTemplateCell.h"
#import "CZSystemToolbar.h"
#import "CZAlertController.h"
#import <CZFileKit/CZFileKit.h>

static NSString * const kDefaultTemplateFile = @"FileNameTemplates/Default template 1.czftp";  // used for create default new template

@interface CZFileNameTemplatePickerViewController () <UITableViewDataSource, UITableViewDelegate, CZKeyboardObserver, CZToolbarDelegate, CZToolbarResponder, CZFileNameEditorDelegate>

@property (nonatomic, readonly, strong) UITableView *tableView;
@property (nonatomic, readonly, strong) CZToolbar *toolbar;

@property (nonatomic, strong) NSMutableArray<CZFileNameTemplate *> *fileNameTemplates;
@property (nonatomic, strong) CZFileNameTemplate *selectedFileNameTemplate;
@property (nonatomic, strong) NSIndexPath *editingIndexPath;

@end

@implementation CZFileNameTemplatePickerViewController

@synthesize tableView = _tableView;
@synthesize toolbar = _toolbar;

- (instancetype)initWithFileNameTemplatePath:(NSString *)fileNameTemplatePath {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _fileNameTemplates = [NSMutableArray arrayWithCapacity:3];
        
        NSString *defaultTemplate1Path = [[NSBundle mainBundle] pathForResource:@"FileNameTemplates/Default template 1" ofType:@"czftp"];
        NSString *defaultTemplate2Path = [[NSBundle mainBundle] pathForResource:@"FileNameTemplates/Default template 2" ofType:@"czftp"];
        NSString *defaultTemplate3Path = [[NSBundle mainBundle] pathForResource:@"FileNameTemplates/Default template 3" ofType:@"czftp"];
        
        CZFileNameTemplate *defaultTemplate1 = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplate1Path];
        CZFileNameTemplate *defaultTemplate2 = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplate2Path];
        CZFileNameTemplate *defaultTemplate3 = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplate3Path];
        
        defaultTemplate1.editable = NO;
        defaultTemplate2.editable = NO;
        defaultTemplate3.editable = NO;
        
        [_fileNameTemplates addObject:defaultTemplate1];
        [_fileNameTemplates addObject:defaultTemplate2];
        [_fileNameTemplates addObject:defaultTemplate3];
        
        NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:[[NSFileManager defaultManager] cz_documentPath]];
        NSString *fileName = nil;
        while (fileName = [enumerator nextObject]) {
            if ([fileName.pathExtension.lowercaseString isEqualToString:@"czftp"]) {
                NSString *path = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:fileName];
                CZFileNameTemplate *template = [[CZFileNameTemplate alloc] initWithContentsOfFile:path];
                if (template) {
                    [_fileNameTemplates addObject:template];
                }
            }
        }
        
        NSString *defaultFileNameTemplatePath = fileNameTemplatePath;
        if (defaultFileNameTemplatePath == nil) {
            defaultFileNameTemplatePath = [[[NSUserDefaults standardUserDefaults] URLForKey:kDefaultFileTemplateURL] path];
        }
        
        for (CZFileNameTemplate *template in _fileNameTemplates) {
            if ([template.path isEqual:defaultFileNameTemplatePath]) {
                _selectedFileNameTemplate = template;
                break;
            }
        }
        
        if (_selectedFileNameTemplate == nil) {
            _selectedFileNameTemplate = _fileNameTemplates.firstObject;
        }
        
        [[CZKeyboardManager sharedManager] addObserver:self];
    }
    return self;
}

- (void)dealloc {
    [[CZKeyboardManager sharedManager] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.toolbar];
    
    [self.tableView reloadData];
    
    if (self.selectedFileNameTemplate) {
        NSInteger row = [self.fileNameTemplates indexOfObject:self.selectedFileNameTemplate];
        if (row != NSNotFound) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplatePickerViewController:didSelectFileNameTemplate:)]) {
            [self.delegate fileNameTemplatePickerViewController:self didSelectFileNameTemplate:self.selectedFileNameTemplate];
        }
    }
}

- (NSArray<CZFileNameTemplate *> *)selectableFileNameTemplates {
    return [self.fileNameTemplates copy];
}

- (void)setSelectedFileNameTemplate:(CZFileNameTemplate *)selectedFileNameTemplate {
    _selectedFileNameTemplate = selectedFileNameTemplate;
    
    CZToolbarItem *deleteItem = [self.toolbar itemForIdentifier:CZToolbarItemIdentifierDelete];
    deleteItem.enabled = _selectedFileNameTemplate.isEditable;
}

#pragma mark - Views

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0.0, 0.0, 64.0, 0.0)) style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor cz_gs105];
        _tableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CZFileNameTemplateCell class] forCellReuseIdentifier:@"FileNameTemplateCell"];
    }
    return _tableView;
}

- (CZToolbar *)toolbar {
    if (_toolbar == nil) {
        _toolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.bounds.size.height - 64.0, self.view.bounds.size.width, 64.0)];
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _toolbar.backgroundColor = [UIColor cz_gs100];
        _toolbar.delegate = self;
        [_toolbar pushResponder:self];
        _toolbar.itemIdentifiers = @[CZToolbarItemIdentifierAdd, CZToolbarItemIdentifierFlexibleSpace, CZToolbarItemIdentifierDelete];
    }
    return _toolbar;
}

#pragma mark - Actions

- (void)addFileNameTemplate {
    NSString *fileName = L(@"FNT_DEFAULT_TEMPLATE");
    fileName = [fileName stringByAppendingPathExtension:@"czftp"];
    NSString *filePath = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:fileName];
    filePath = [CZFileNameGenerator uniqueFilePath:filePath];
    
    NSString *srcFilePath = [[NSBundle mainBundle] bundlePath];
    srcFilePath = [srcFilePath stringByAppendingPathComponent:kDefaultTemplateFile];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] copyItemAtPath:srcFilePath toPath:filePath error:&error];
    if (error) {
        return;
    }
    
    CZFileNameTemplate *template = [[CZFileNameTemplate alloc] initWithContentsOfFile:filePath];
    if (template == nil) {
        return;
    }
    
    [self.fileNameTemplates addObject:template];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.fileNameTemplates.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    self.selectedFileNameTemplate = template;
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplatePickerViewController:didSelectFileNameTemplate:)]) {
        [self.delegate fileNameTemplatePickerViewController:self didSelectFileNameTemplate:self.selectedFileNameTemplate];
    }
}

- (void)deleteFileNameTemplate:(CZFileNameTemplate *)template {
    if (template == nil) {
        return;
    }
    
    NSError *error = nil;
    [[CZFileManager defaultManager] removeItemAtPath:template.path error:&error];
    if (error) {
        return;
    }
    
    NSInteger row = [self.fileNameTemplates indexOfObject:template];
    if (row == NSNotFound) {
        return;
    }
    
    [self.fileNameTemplates removeObjectAtIndex:row];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    self.selectedFileNameTemplate = self.fileNameTemplates[0];
    indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplatePickerViewController:didSelectFileNameTemplate:)]) {
        [self.delegate fileNameTemplatePickerViewController:self didSelectFileNameTemplate:self.selectedFileNameTemplate];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileNameTemplates.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileNameTemplateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FileNameTemplateCell" forIndexPath:indexPath];
    cell.editable = self.fileNameTemplates[indexPath.row].isEditable;
    cell.fileNameEditor.delegate = self;
    cell.fileNameEditor.fileNameLabel.text = [self.fileNameTemplates[indexPath.row].path.lastPathComponent stringByDeletingPathExtension];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedFileNameTemplate = self.fileNameTemplates[indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplatePickerViewController:didSelectFileNameTemplate:)]) {
        [self.delegate fileNameTemplatePickerViewController:self didSelectFileNameTemplate:self.selectedFileNameTemplate];
    }
}

#pragma mark - CZKeyboardObserver

- (void)keyboardWillShow:(CZKeyboardTransition *)transition {
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        self.tableView.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0.0, 0.0, transition.frameEnd.size.height - 64.0, 0.0));
        [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    } completion:nil];
}

- (void)keyboardWillHide:(CZKeyboardTransition *)transition {
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        self.tableView.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0.0, 0.0, 64.0, 0.0));
    } completion:nil];
}

#pragma mark - CZToolbarDelegate

- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierDelete]) {
        item.enabled = self.selectedFileNameTemplate.isEditable;
    }
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierAdd]) {
        [self addFileNameTemplate];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierDelete]) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"FNT_DELETE_MESSAGE") level:CZAlertLevelWarning];
        [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            [self deleteFileNameTemplate:self.selectedFileNameTemplate];
        }];
        [alert presentAnimated:YES completion:nil];
        return YES;
    }
    
    return NO;
}

#pragma mark - CZFileNameEditorDelegate

- (BOOL)fileNameEditor:(CZFileNameEditor *)editor validateFileName:(NSString *)fileName {
    if (fileName.length == 0) {
        return NO;
    }
    
    NSIndexPath *indexPath = [self indexPathForFileNameEditor:editor];
    NSString *originalPath = self.fileNameTemplates[indexPath.row].path;
    NSString *currentName = [fileName stringByAppendingPathExtension:originalPath.pathExtension];
    NSString *currentPath = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:currentName];
    
    if ([currentPath isEqualToString:originalPath]) {
        return YES;
    }
    
    for (CZFileNameTemplate *fileNameTemplate in self.fileNameTemplates) {
        if ([currentPath.lastPathComponent isEqualToString:fileNameTemplate.path.lastPathComponent]) {
            return NO;
        }
    }
    
    return YES;
}

- (void)fileNameEditor:(CZFileNameEditor *)editor didBeginEditingFileName:(NSString *)fileName {
    self.editingIndexPath = [self indexPathForFileNameEditor:editor];
}

- (void)fileNameEditor:(CZFileNameEditor *)editor didFinishEditingFileName:(NSString *)fileName {
    NSIndexPath *indexPath = [self indexPathForFileNameEditor:editor];
    NSString *originalPath = self.fileNameTemplates[indexPath.row].path;
    NSString *currentName = [fileName stringByAppendingPathExtension:originalPath.pathExtension];
    NSString *currentPath = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:currentName];
    
    if ([currentPath isEqualToString:originalPath]) {
        return;
    }
    
    for (CZFileNameTemplate *fileNameTemplate in self.fileNameTemplates) {
        if ([currentPath.lastPathComponent isEqualToString:fileNameTemplate.path.lastPathComponent]) {
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"FILE_NAME_CONFLICT_ERROR_MESSAGE") level:CZAlertLevelError];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
            [alert presentAnimated:YES completion:nil];
            return;
        }
    }
    
    NSError *error = nil;
    [[CZFileManager defaultManager] moveItemAtPath:originalPath toPath:currentPath error:&error];
    if (error) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:error.localizedDescription level:CZAlertLevelError];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    self.fileNameTemplates[indexPath.row].path = currentPath;
    
    self.editingIndexPath = nil;
}

- (NSIndexPath *)indexPathForFileNameEditor:(CZFileNameEditor *)editor {
    for (UITableViewCell *cell in [self.tableView visibleCells]) {
        if ([editor isDescendantOfView:cell]) {
            return [self.tableView indexPathForCell:cell];
        }
    }
    return nil;
}

@end
