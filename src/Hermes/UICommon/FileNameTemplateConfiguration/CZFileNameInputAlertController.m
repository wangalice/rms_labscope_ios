//
//  CZFileNameInputAlertController.m
//  Hermes
//
//  Created by Ralph Jin on 5/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameInputAlertController.h"
#import "CZInputAlertController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZFileKit/CZFileKit.h>

@interface CZFileNameInputAlertController () <CZTextFieldDelegate>

@property (nonatomic, strong) CZInputAlertController *alert;

@end

@implementation CZFileNameInputAlertController

- (id)initWithDocManager:(CZDocManager *)docManager {
    self = [super init];
    if (self) {
        _docManager = docManager;
        
        _generator = [[CZFileNameGenerator alloc] init];
        _generator.extension = @"czi";
        _generator.operatorName = [CZDefaultSettings sharedInstance].operatorName;
        _generator.microscope = docManager.imageProperties.microscopeName;
        _generator.objective = docManager.imageProperties.magnificationFactorString;
        _generator.captureDate = docManager.captureDate;
        if (_generator.captureDate == nil) {
            _generator.captureDate = [NSDate date];
        }
        [_generator useDefaultValueWhenEmpty];
                        
        CZFileNameTemplate *template = nil;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSURL *defaultTemplateURL = [defaults URLForKey:kDefaultFileTemplateURL];
        if (defaultTemplateURL) {
            NSString *defaultTemplatePath = [[defaults URLForKey:kDefaultFileTemplateURL] relativePath];
            template = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplatePath];
        }
        
        // TODO: create default file name template
    
        if (template) {
            _generator.fileNameTemplate = template;
        } else {
            return nil;
        }
    }
    
    return self;
}

- (BOOL)requiresUserInput {
    CZFileNameTemplate *template = _generator.fileNameTemplate;
    for (NSUInteger i = 0; i < [template fieldCount]; i++) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        if (field.type == CZFileNameTemplateFieldTypeFreeText && field.isUserInput) {
            return YES;
        }
    }
    
    return NO;
}

- (void)showInputAlert {
    NSString *title = [[self uniqueName] stringByDeletingPathExtension];
    NSString *message = L(@"FNT_INPUT_ALERT_MESSAGE");
    CZInputAlertController *alert = [CZInputAlertController alertControllerWithTitle:title message:message];
    
    CZFileNameTemplate *template = self.generator.fileNameTemplate;
    for (NSUInteger i = 0; i < [template fieldCount]; i++) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        NSUInteger inputFieldCount = i + 1;
        
        BOOL freeTextNeedsInput = (field.type == CZFileNameTemplateFieldTypeFreeText && field.isUserInput);
        
        if (freeTextNeedsInput) {
            NSString *label = [NSString stringWithFormat:L(@"FNT_INPUT_FIELD_LABLE_FORMAT"), inputFieldCount];
            [alert addTextFieldWithLabel:label configurationHandler:^(CZTextField *textField) {
                textField.text = field.value;
                textField.tag = i;
                textField.delegate = self;
                [textField addTarget:self action:@selector(textFieldDidEditingChanged:) forControlEvents:UIControlEventEditingChanged];
            }];
        }
    }
    
    [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDestructive handler:^(CZInputAlertController *alert, CZDialogAction *action) {
        [self discardButtonPressAction];
    }];
    
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZInputAlertController *alert, CZDialogAction *action) {
        [self okButtonPressAction];
    }];
    
    [alert presentAnimated:YES completion:nil];
    
    self.alert = alert;
}

- (NSString *)uniqueName {
#if DCM_REFACTORED
    return [_generator uniqueFileNameInDirectory:[CZLocalFileList sharedInstance].documentPath];
#else
    return [_generator uniqueFileNameInDirectory:[NSFileManager defaultManager].cz_documentPath];
#endif
   
}

- (void)okButtonPressAction {
    [self.delegate fileNameInputAlertDidPressOK:self];
}

- (void)discardButtonPressAction {
    [self.delegate fileNameInputAlertDidPressDiscard:self];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEditingChanged:(id)sender {
    UITextField *textField = (UITextField *)sender;
    NSUInteger i = textField.tag;
    CZFileNameTemplate *template = _generator.fileNameTemplate;
    if (i < [template fieldCount]) {
        CZFileNameTemplateField *field = [template fieldAtIndex:i];
        field.value = textField.text;
        self.alert.title = [[self uniqueName] stringByDeletingPathExtension];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    return [CZCommonUtils isStringValidFileNameTemplate:textEntered];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    UITextField *previousTextField = nil;
    for (UITextField *tf in self.alert.textFields) {
        if (previousTextField == textField) {
            [tf becomeFirstResponder];
            return YES;
        }
        previousTextField = tf;
    }
    
    return NO;
}

@end
