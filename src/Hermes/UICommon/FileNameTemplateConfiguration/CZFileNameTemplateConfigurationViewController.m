//
//  CZFileNameTemplateConfigurationViewController.m
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameTemplateConfigurationViewController.h"
#import "CZAlertController.h"
#import "CZUIDefinition.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZFunctionBar.h"
#import "CZButtonGroup.h"
#import "CZFileNameTemplatePickerViewController.h"
#import "CZDragButton.h"
#import "CZHelpOverlayView.h"

const static CGFloat kUIFileNameDefaultPadding = 10;
const static CGFloat kUIFileNameDefaultBoarderThickness = 1;
const static CGFloat kUIFileNameDefaultLabelWidth = 150;
const static CGFloat kUIFileNameDefaultLabelHeight = 30;
const static CGFloat kUIFileNameTemplateChooserWidth = 350;
const static CGFloat kUIFileNameTemplateChooserBoarderWidth = 4;
const static CGFloat kUIFileNameLabelX = 32;
const static CGFloat kUIFileNameLabelY = 29;
const static CGFloat kUIFileNameLabelHeight = 16;
const static CGFloat kUIFileNameLabelPadding = 32;
const static CGFloat kUIFileNameFieldButtonWidth = 64;
const static CGFloat kUIFileNameFieldButtonHeight = 48;
const static CGFloat kUIFileNameAddButtonHeight = 32;
const static CGFloat kUIFileNameAddButtonWidth  = 48;
const static CGFloat kUIFileNameDeleteButtonHeight = 32;
const static CGFloat kUIFileNameDeleteButtonWidth  = 48;
const static CGFloat kUIFileNameFreeTextFieldWidth = 176;

const static CGFloat kUIFileNameRadioButtonWidth = 293;

enum {
    kCZFNTOptionIndexFreeText = 0,
    kCZFNTOptionIndexAutoNumber,
    kCZFNTOptionIndexDateTime,
    kCZFNTOptionIndexMicroscope,
    kCZFNTOptionIndexMagnifyFactor
};

@interface CZFileNameTemplateConfigurationViewController () <CZTextFieldDelegate, CZButtonGroupDelegate, CZFileNameTemplatePickerViewControllerDelegate, CZDragButtonDelegate> {
    NSUInteger _selectedFieldIndex;
    NSUInteger _draggingToIndex;
}

@property (nonatomic, copy) NSString *path;
@property (nonatomic, retain) CZFileNameTemplatePickerViewController *templateChooser;

@property (nonatomic, retain) UIView *templateEditingView;

@property (nonatomic, retain) CZFileNameTemplate *template;
@property (nonatomic, retain) CZFileNameGenerator *sampleGenerator;
@property (nonatomic, assign) BOOL activatedTemplateChanged;

@property (nonatomic, retain) UILabel *fileNameContentLabel;
@property (nonatomic, retain) UILabel *fileNameLabel;

@property (nonatomic, retain) CZButton *addButton;
@property (nonatomic, retain) CZButton *deleteButton;

@property (nonatomic, retain) CZTextField *freeTextField;
@property (nonatomic, retain) CZSwitch *askUserInputButton;
@property (nonatomic, retain) UILabel *digitsCountLabel;
@property (nonatomic, retain) CZTextField *digitsCountTextField;

@property (nonatomic, retain) CZButtonGroup *typeRadioGroup;

@property (nonatomic, retain) UIView *detailViewSeparator;
@property (nonatomic, retain) CZButtonGroup *dateFormatRadioGroup;
@property (nonatomic, retain) UIView *freeTextDetailView;
@property (nonatomic, retain) UIView *autoNumberDetailView;

@property (nonatomic, retain) UIView *fieldButtonBar;
@property (nonatomic, retain) UIImageView *fieldRailView;
@property (nonatomic, retain) NSMutableArray *fieldButtons;
@end

@implementation CZFileNameTemplateConfigurationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _sampleGenerator = [[CZFileNameGenerator alloc] init];
        _sampleGenerator.operatorName = L(@"FNT_FIELD_OPERATOR_NAME_SHORT");
        _sampleGenerator.objective = @"10x";
        _sampleGenerator.microscope = L(@"FNT_FIELD_MICROSCOPE_NAME_SHORT");
        _sampleGenerator.captureDate = [NSDate date];
        _sampleGenerator.extension = @"czi";
    }
    return self;
}

- (instancetype)initWithPath:(NSString *)path {
    self = [self init];
    if (self) {
        _path = [path copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }

    self.title = L(@"FNT_EDIT_TITLE");
    self.view.backgroundColor = [UIColor cz_gs105];
    
    // create border between template chooser and template editing view
    UIView *boarder = [[UIView alloc] initWithFrame:CGRectMake(kUIFileNameTemplateChooserWidth,
                                                               0,
                                                               kUIFileNameTemplateChooserBoarderWidth,
                                                               self.view.frame.size.height)];
    boarder.backgroundColor = [UIColor cz_gs110];
    [self.view addSubview:boarder];
    
    // create file template editing view
    UIView *templateEditingView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:templateEditingView];
    self.templateEditingView = templateEditingView;
    
    // create file name label
    UILabel *fileNameLabel = [[UILabel alloc] init];
    fileNameLabel.font = [UIFont cz_label1];
    fileNameLabel.text = L(@"FNT_FILE_NAME_LABEL");
    fileNameLabel.textColor = [UIColor cz_gs80];
    [fileNameLabel setBackgroundColor:[UIColor clearColor]];
    [fileNameLabel sizeToFit];
    fileNameLabel.hidden = YES;
    self.fileNameLabel = fileNameLabel;
    [self.templateEditingView addSubview:fileNameLabel];
    
    boarder = [[UIView alloc] initWithFrame:CGRectZero];
    boarder.backgroundColor = [UIColor cz_gs110];
    [self.templateEditingView addSubview:boarder];
    self.detailViewSeparator = boarder;

    // create file name content label
    UILabel *fileNameContentlabel = [[UILabel alloc] init];
    fileNameContentlabel.font = [UIFont cz_body1];
    fileNameContentlabel.textColor = [UIColor cz_gs50];
    fileNameContentlabel.numberOfLines = 1;
    fileNameContentlabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    [fileNameContentlabel setBackgroundColor:[UIColor clearColor]];
    self.fileNameContentLabel = fileNameContentlabel;
    [self.templateEditingView addSubview:fileNameContentlabel];
    
    // create field type radio group
    NSArray *typeOptions = @[L(@"FNT_OPTION_FREE_TEXT"),
                             L(@"FNT_OPTION_AUTO_NUMBER"),
                             L(@"FNT_OPTION_DATE_TIME"),
                             L(@"FNT_OPTION_MICROSCOPE_NAME"),
                             L(@"FNT_FIELD_MAGNIFICATION_SHORT")];
        
    CZButtonGroup *typeRadioGroup = [[CZButtonGroup alloc] initWithType:CZButtonGroupTypeRadio options:typeOptions];
    typeRadioGroup.delegate = self;
    [self.templateEditingView addSubview:typeRadioGroup];
    typeRadioGroup.hidden = YES;
    self.typeRadioGroup = typeRadioGroup;
    
    // create date format radio group
    NSArray *formatOptions = [CZFileNameGenerator dateTimeFormatOptions];
    CZButtonGroup *formatRadioGroup = [[CZButtonGroup alloc] initWithType:CZButtonGroupTypeRadio options:formatOptions];
    formatRadioGroup.delegate = self;
    [self.templateEditingView addSubview:formatRadioGroup];
    formatRadioGroup.hidden = YES;
    self.dateFormatRadioGroup = formatRadioGroup;
    
    // create free text detail view
    UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                  self.templateEditingView.frame.size.width,
                                                                  self.templateEditingView.frame.size.height)];
    self.freeTextDetailView = detailView;
    detailView.hidden = YES;
    [self.templateEditingView addSubview:detailView];
    
    UILabel *freeTextLabel = [[UILabel alloc] init];
    freeTextLabel.font = [UIFont cz_label1];
    freeTextLabel.text = L(@"FNT_FREE_TEXT_LABEL");
    freeTextLabel.backgroundColor = [UIColor clearColor];
    freeTextLabel.textColor = [UIColor cz_gs80];
    freeTextLabel.textAlignment = NSTextAlignmentLeft;
    freeTextLabel.numberOfLines = 1;
    CGSize size = [freeTextLabel sizeThatFits:CGSizeMake(0, kUIFileNameDefaultLabelHeight)];
    freeTextLabel.frame = CGRectMake(0, 0, size.width, kUIFileNameDefaultLabelHeight);
    [self.freeTextDetailView addSubview:freeTextLabel];
    
    UILabel *userInputTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kUIFileNameDefaultLabelWidth, 2*kUIFileNameDefaultLabelHeight)];
    userInputTextLabel.text = L(@"FNT_USER_INPUT_LABEL");
    userInputTextLabel.font = [UIFont cz_label1];
    userInputTextLabel.backgroundColor = [UIColor clearColor];
    userInputTextLabel.textColor = [UIColor cz_gs80];
    userInputTextLabel.textAlignment = NSTextAlignmentLeft;
    userInputTextLabel.numberOfLines = 2;
    CGRect textFrame = [userInputTextLabel textRectForBounds:CGRectMake(0, 0, kUIFileNameDefaultLabelWidth, 2*kUIFileNameDefaultLabelHeight)
                                      limitedToNumberOfLines:2];
    userInputTextLabel.frame = CGRectMake(0, CGRectGetMaxY(freeTextLabel.frame) + 16, textFrame.size.width, 2*textFrame.size.height);
    [self.freeTextDetailView addSubview:userInputTextLabel];
    
    CZTextField *freeTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
    freeTextField.frame = CGRectMake(112,
                                     freeTextLabel.frame.origin.y,
                                     kUIFileNameFreeTextFieldWidth,
                                     kUIFileNameDefaultLabelHeight);
    freeTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
    freeTextField.returnKeyType = UIReturnKeyDone;
    [freeTextField addTarget:self action:@selector(freeTextFieldEditingChanged:) forControlEvents:UIControlEventEditingDidEnd];
    [freeTextField addTarget:self action:@selector(freeTextFieldEditingChanged:) forControlEvents:UIControlEventEditingDidEndOnExit];
    freeTextField.delegate = self;
    self.freeTextField = freeTextField;
    [self.freeTextDetailView addSubview:freeTextField];
    
    CZSwitch *askUserInputButton = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
    [askUserInputButton setLevel:CZControlEmphasisActivePrimary];
    askUserInputButton.frame = CGRectMake(237, userInputTextLabel.frame.origin.y, 51, 31);
    [askUserInputButton addTarget:self action:@selector(userInputSwitchChanged:) forControlEvents:UIControlEventValueChanged];
    CGPoint position = askUserInputButton.center;
    position.y = userInputTextLabel.center.y;
    askUserInputButton.center = position;
    self.askUserInputButton = askUserInputButton;
    [self.freeTextDetailView addSubview: askUserInputButton];
    
    // create auto number detail view
    UIView *autoNumberDetailView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                            self.templateEditingView.frame.size.width,
                                                                            self.templateEditingView.frame.size.height)];
    self.autoNumberDetailView = autoNumberDetailView;
    autoNumberDetailView.hidden = YES;
    [self.templateEditingView addSubview:autoNumberDetailView];
    
    UILabel *digitsCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kUIFileNameDefaultLabelWidth, kUIFileNameDefaultLabelHeight)];
    digitsCountLabel.text = [NSString stringWithFormat:L(@"FNT_DIGITS_COUNT_LABEL"), 3];
    digitsCountLabel.font = [UIFont cz_label1];
    digitsCountLabel.backgroundColor = [UIColor clearColor];
    digitsCountLabel.textColor = [UIColor cz_gs80];
    digitsCountLabel.textAlignment = NSTextAlignmentLeft;
    digitsCountLabel.numberOfLines = 1;
    size = [digitsCountLabel sizeThatFits:CGSizeMake(0, kUIFileNameDefaultLabelHeight)];
    digitsCountLabel.frame = CGRectMake(0, 0, size.width, kUIFileNameDefaultLabelHeight);
    
    [self.autoNumberDetailView addSubview:digitsCountLabel];
    self.digitsCountLabel = digitsCountLabel;
    
    CZTextField *digitsCountTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
    digitsCountTextField.frame = CGRectMake(192, 0, 96, 32);
    digitsCountTextField.text = @"1";
    [digitsCountTextField addTarget:self action:@selector(digitsCountTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    [self.autoNumberDetailView addSubview:digitsCountTextField];
    self.digitsCountTextField = digitsCountTextField;
    
    // create field button bar
    UIView *fieldButtonBar = [[UIView alloc] init];
    self.fieldButtonBar = fieldButtonBar;
    [self.templateEditingView addSubview:fieldButtonBar];
    
    // create rail background view
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, 64.0, 48.0) cornerRadius:3.0];
    path.lineWidth = 1.0;
    CZShapeImage *railImage = [CZShapeImage imageWithPath:path fillColor:[UIColor cz_gs110] strokeColor:[UIColor cz_gs120] capInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)];
    UIImageView *fieldRailView = [[UIImageView alloc] initWithImage:railImage.UIImage];
    fieldRailView.hidden = YES;
    self.fieldRailView = fieldRailView;
    [self.fieldButtonBar addSubview:fieldRailView];
    
    // create add button
    CZButton *button = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
    [button addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [button cz_setImageWithIcon:[CZIcon iconNamed:@"plus"]];
    button.frame = CGRectMake(0, 0, kUIFileNameAddButtonWidth, kUIFileNameAddButtonHeight);
    [self.fieldButtonBar addSubview:button];
    self.addButton = button;
    
    // create delete button
    button = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
    [button addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [button cz_setImageWithIcon:[CZIcon iconNamed:@"minus"]];
    button.frame = CGRectMake(0, 0, kUIFileNameDeleteButtonWidth, kUIFileNameDeleteButtonHeight);
    [self.fieldButtonBar addSubview:button];
    self.deleteButton = button;
    
    // create field buttons
    NSMutableArray *buttonArray = [[NSMutableArray alloc] initWithCapacity:kMaxFileNameFieldsCount];
    CZShapeImage *fieldButtonSelectedImage = [CZShapeImage imageWithPath:path
                                                               fillColor:[UIColor cz_gs90]
                                                             strokeColor:[UIColor cz_gs120]];
    for (NSUInteger i = 0; i < kMaxFileNameFieldsCount; ++i) {
        CZDragButton *button = [[CZDragButton alloc] initWithFrame:CGRectMake(0, 0, kUIFileNameFieldButtonWidth, kUIFileNameFieldButtonHeight)];
        [button setBackgroundImage:fieldButtonSelectedImage.UIImage forState:UIControlStateSelected];
        [button setBackgroundImage:fieldButtonSelectedImage.UIImage forState:UIControlStateSelected | UIControlStateHighlighted];
        [button addTarget:self action:@selector(fieldButtonTapped:) forControlEvents:UIControlEventTouchDown];
        button.tag = i;
        button.delegate = self;
        [buttonArray addObject:button];
        [self.fieldButtonBar addSubview:button];
    }
    self.fieldButtons = buttonArray;
    
    // create file template chooser
    CZFileNameTemplatePickerViewController *templateChooser = [[CZFileNameTemplatePickerViewController alloc] initWithFileNameTemplatePath:self.path];
    self.templateChooser = templateChooser;
    templateChooser.delegate = self;
    [self addChildViewController:templateChooser];
    [self.view addSubview:self.templateChooser.view];
    [templateChooser didMoveToParentViewController:self];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self repositionControls];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self refreshControlsStatus];
#if HELP_OVERLAY
    [self showHelpOverlay];
#endif
}

- (void)viewWillDisappear:(BOOL)animated {
#if HELP_OVERLAY
    [[CZHelpOverlayView current] dismiss];
#endif
    [super viewWillDisappear:animated];
    
    if (self.activatedTemplateChanged && self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplateConfigurationViewControllerWillSaveConfiguration:)]) {
        [self.delegate fileNameTemplateConfigurationViewControllerWillSaveConfiguration:self];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.activatedTemplateChanged && self.delegate && [self.delegate respondsToSelector:@selector(fileNameTemplateConfigurationViewControllerDidSaveConfiguration:)]) {
        [self.delegate fileNameTemplateConfigurationViewControllerDidSaveConfiguration:self];
    }
    
#if HELP_OVERLAY
    [[CZHelpOverlayView current] dismiss];
#endif
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void)backBarButtonItemAction:(id)sender {
    if (self.template && [self.template fieldCount] > 0) {
        [self.view endEditing:YES];
        
        NSURL *originalFileNameTemplateURL = [[NSUserDefaults standardUserDefaults] URLForKey:kDefaultFileTemplateURL];
        if (self.templateChooser.selectedFileNameTemplate) {
            NSURL *url = [[NSURL alloc] initFileURLWithPath:self.templateChooser.selectedFileNameTemplate.path isDirectory:NO];
            [[NSUserDefaults standardUserDefaults] setURL:url forKey:kDefaultFileTemplateURL];
        }
        
        for (CZFileNameTemplate *template in self.templateChooser.selectableFileNameTemplates) {
            if (template.isEditable && template.isUpdated) {
                [template writeToFile:template.path];
                if (template == self.templateChooser.selectedFileNameTemplate) {
                    self.activatedTemplateChanged = YES;
                }
            }
        }
        
        NSURL *defaultTemplatPath = [[NSUserDefaults standardUserDefaults] URLForKey:kDefaultFileTemplateURL];
        if (![originalFileNameTemplateURL isEqual:defaultTemplatPath]) {
            self.activatedTemplateChanged = YES;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private methods

/** Convert field type to field type radio group's index.*/
+ (NSUInteger)indexFromFieldType:(CZFileNameTemplateField *)field {
    NSUInteger i = kCZFNTOptionIndexFreeText;
    switch (field.type) {
        case CZFileNameTemplateFieldTypeFreeText:
            i = kCZFNTOptionIndexFreeText;
            break;
        case CZFileNameTemplateFieldTypeVariableText: {
            if ([field.value isEqualToString:CZFileNameTemplateFieldValueMicroscopeName]) {
                i = kCZFNTOptionIndexMicroscope;
            } else if ([field.value isEqualToString:CZFileNameTemplateFieldValueObjectiveMagnification]) {
                i = kCZFNTOptionIndexMagnifyFactor;
            } else {
                i = NSNotFound;
            }
            break;
        }
        case CZFileNameTemplateFieldTypeAutoNumber:
            i = kCZFNTOptionIndexAutoNumber;
            break;
        case CZFileNameTemplateFieldTypeDate:
            i = kCZFNTOptionIndexDateTime;
            break;
        default:
            break;
    }
    return i;
}

/** Convert field type radio group's index to field type.*/
+ (CZFileNameTemplateFieldType)fieldTypeForIndex:(NSUInteger)index {
    CZFileNameTemplateFieldType type = CZFileNameTemplateFieldTypeFreeText;
    switch (index) {
        case kCZFNTOptionIndexFreeText:
            type = CZFileNameTemplateFieldTypeFreeText;
            break;
        case kCZFNTOptionIndexAutoNumber:
            type = CZFileNameTemplateFieldTypeAutoNumber;
            break;
        case kCZFNTOptionIndexDateTime:
            type = CZFileNameTemplateFieldTypeDate;
            break;
        case kCZFNTOptionIndexMicroscope:
        case kCZFNTOptionIndexMagnifyFactor:
            type = CZFileNameTemplateFieldTypeVariableText;
            break;
        default:
            break;
    }
    return type;
}

+ (NSString *)defaultFieldValueForIndex:(NSUInteger)index {
    NSString *value = @"";
    switch (index) {
        case kCZFNTOptionIndexFreeText:
            value = @"Snap";
            break;
        case kCZFNTOptionIndexAutoNumber:
            value = @"3";
            break;
        case kCZFNTOptionIndexDateTime:
            value = [CZFileNameGenerator dateTimeFormatOptions][0];
            break;
        case kCZFNTOptionIndexMicroscope:
            value = CZFileNameTemplateFieldValueMicroscopeName;
            break;
        case kCZFNTOptionIndexMagnifyFactor:
            value = CZFileNameTemplateFieldValueObjectiveMagnification;
            break;
        default:
            break;
    }
    return value;
}

/** Refresh the controls status like:
 * Hide or show the detail view;
 * Select type radio of current select field;
 * Update the label of the field button;
 */
- (void)refreshControlsStatus {
    BOOL isFieldSelected = NO;
    
    NSMutableIndexSet *optionIndexSet = [NSMutableIndexSet indexSet];
    for (NSUInteger i = 0; i < [self.fieldButtons count]; ++i) {
        UIButton *button = self.fieldButtons[i];
        button.tag = i;
        
        if (i < [self.template fieldCount]) {
            BOOL selected = (i == _selectedFieldIndex);
            button.selected = selected;
            if (selected) {
                isFieldSelected = YES;
            }
            
            CZFileNameTemplateField *field = [self.template fieldAtIndex:i];
            if (i != _selectedFieldIndex) {
                NSUInteger index = [CZFileNameTemplateConfigurationViewController indexFromFieldType:field];
                if (index != NSNotFound) {
                    [optionIndexSet addIndex:index];
                }
            }
            
            NSString *iconName = nil;
            switch (field.type) {
                case CZFileNameTemplateFieldTypeFreeText:
                    iconName = @"file-name-template-text";
                    break;
                case CZFileNameTemplateFieldTypeVariableText:
                    if ([field.value isEqualToString:CZFileNameTemplateFieldValueMicroscopeName]) {
                        iconName = @"file-name-template-microscope-name";
                    } else if ([field.value isEqualToString:CZFileNameTemplateFieldValueObjectiveMagnification]) {
                        iconName = @"objective-none";
                    }
                    break;
                case CZFileNameTemplateFieldTypeAutoNumber:
                    iconName = @"file-name-template-number";
                    break;
                case CZFileNameTemplateFieldTypeDate:
                    iconName = @"file-name-template-date-time";
                    break;
                default:
                    iconName = nil;
                    break;
            }
            
            [button cz_setImageWithIcon:[CZIcon iconNamed:iconName]];
        }
    }
    
    self.typeRadioGroup.hidden = (self.template == nil) || !isFieldSelected;
    self.deleteButton.hidden = (self.template == nil) || !isFieldSelected;
    
    if (self.template.isEditable) {
        [self.typeRadioGroup setEnabled:YES atIndex:kCZFNTOptionIndexFreeText];
        for (int optionIndex = kCZFNTOptionIndexAutoNumber; optionIndex <= kCZFNTOptionIndexMagnifyFactor; ++optionIndex) {
            BOOL disable = [optionIndexSet containsIndex:optionIndex];
            [self.typeRadioGroup setEnabled:!disable atIndex:optionIndex];
        }
    } else {
        self.typeRadioGroup.disableAll = YES;
    }
    
    // update detail views
    self.freeTextDetailView.hidden = YES;
    self.autoNumberDetailView.hidden = YES;
    self.dateFormatRadioGroup.hidden = YES;
    
    if (_selectedFieldIndex < [self.template fieldCount]) {
        CZFileNameTemplateField *field = [self.template fieldAtIndex:_selectedFieldIndex];
        NSUInteger index = [CZFileNameTemplateConfigurationViewController indexFromFieldType:field];
        if (index != NSNotFound) {
            [self.typeRadioGroup selectOptionAtIndex:index];
        }
        
        switch (index) {
            case kCZFNTOptionIndexFreeText:
                self.freeTextDetailView.hidden = NO;
                break;
            case kCZFNTOptionIndexAutoNumber:
                self.autoNumberDetailView.hidden = NO;
                break;
            case kCZFNTOptionIndexDateTime:
                self.dateFormatRadioGroup.hidden = NO;
            default:
                break;
        }
        
        if (field.type == CZFileNameTemplateFieldTypeDate) {
            NSUInteger index = [field.value integerValue];
            [self.dateFormatRadioGroup selectOptionAtIndex:index];
        } else if (field.type == CZFileNameTemplateFieldTypeAutoNumber) {
            const int kMinDigitCount = 1;
            const int kMaxDigitCount = 6;
            int digits = [field.value intValue];
            if (digits < kMinDigitCount) {
                digits = kMinDigitCount;
            } else if (digits > kMaxDigitCount) {
                digits = kMaxDigitCount;
            }
            
            self.digitsCountTextField.text = @(digits).stringValue;
        } else if (field.type == CZFileNameTemplateFieldTypeFreeText) {
            self.freeTextField.text = field.value;
            self.askUserInputButton.on = field.userInput;
        }
        
        if (field.type != CZFileNameTemplateFieldTypeFreeText) {
            [self.freeTextField resignFirstResponder];
        }
    }
    
    self.freeTextField.enabled = self.template.isEditable;
    self.askUserInputButton.enabled = self.template.isEditable;
    self.digitsCountTextField.enabled = self.template.isEditable;
    self.dateFormatRadioGroup.disableAll = !self.template.isEditable;
    
    [self refreshFileName];
}

- (void)refreshFileName {
    // refresh file name label
    if (self.template) {
        self.sampleGenerator.fileNameTemplate = self.template;
        NSString *finalFileName = [self.sampleGenerator sampleFileName];
        
        self.fileNameContentLabel.text = finalFileName;
        CGSize maximumLabelSize = CGSizeMake(self.templateEditingView.frame.size.width - CGRectGetMaxX(self.fileNameLabel.frame) - kUIFileNameLabelPadding * 2, kUIFileNameLabelHeight);
        
        CGSize size = [self.fileNameContentLabel.text boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.fileNameContentLabel.font} context:nil].size;
        size.width = ceil(size.width);
        size.height = ceil(size.height);
        
        self.fileNameContentLabel.frame = CGRectMake(CGRectGetMaxX(self.fileNameLabel.frame) + kUIFileNameLabelPadding,
                                                     CGRectGetMinY(self.fileNameLabel.frame), size.width, kUIFileNameLabelHeight);
    }
}

- (void)repositionControls {
    self.templateEditingView.frame = CGRectMake(kUIFileNameTemplateChooserWidth + kUIFileNameTemplateChooserBoarderWidth,
                                                0,
                                                self.view.frame.size.width - (kUIFileNameTemplateChooserWidth + kUIFileNameTemplateChooserBoarderWidth),
                                                self.view.frame.size.height);

    self.fileNameLabel.frame = CGRectMake(kUIFileNameLabelX, kUIFileNameLabelY, self.fileNameLabel.frame.size
                                          .width, kUIFileNameLabelHeight);
    
    CGSize maximumLabelSize = CGSizeMake(self.templateEditingView.frame.size.width - CGRectGetMaxX(self.fileNameLabel.frame) - kUIFileNameLabelPadding * 2, kUIFileNameLabelHeight);
    
    CGSize size = [self.fileNameContentLabel.text boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.fileNameContentLabel.font} context:nil].size;
    size.width = ceil(size.width);
    size.height = ceil(size.height);
    
    self.fileNameContentLabel.frame = CGRectMake(CGRectGetMaxX(self.fileNameLabel.frame) + kUIFileNameLabelPadding,
                                                 CGRectGetMinY(self.fileNameLabel.frame),
                                                 size.width,
                                                 kUIFileNameLabelHeight);
    
    self.fieldButtonBar.frame = CGRectMake(0, CGRectGetMaxY(self.fileNameLabel.frame) + kUIFileNameDefaultPadding,
                                           self.templateEditingView.frame.size.width, kUIFileNameFieldButtonHeight + kUIFileNameDefaultPadding * 2);
    CGRect frame = self.fieldButtonBar.frame;
    frame.origin = CGPointZero;
    self.fieldRailView.frame = frame;

    self.templateChooser.view.frame = CGRectMake(0, 0,
                                                 kUIFileNameTemplateChooserWidth,
                                                 self.view.frame.size.height);
    
    CGFloat y = CGRectGetMaxY(self.fileNameLabel.frame) + kUIFileNameDefaultPadding;
    self.typeRadioGroup.frame = CGRectMake(kUIFileNameLabelX,
                                           CGRectGetMaxY(self.fieldButtonBar.frame) + kUIFileNameDefaultPadding,
                                           kUIFileNameRadioButtonWidth,
                                           self.templateEditingView.frame.size.height - y); // half of 6 type options
    
    y = CGRectGetMaxY(self.fieldButtonBar.frame) + kUIFileNameDefaultPadding * 2;
    self.detailViewSeparator.frame = CGRectMake(CGRectGetMaxX(self.typeRadioGroup.frame) + kUIFileNameDefaultPadding,
                                                y,
                                                kUIFileNameDefaultBoarderThickness,
                                                232);
    
    CGRect detailViewFrame = self.typeRadioGroup.frame;
    detailViewFrame.origin.x = CGRectGetMaxX(self.detailViewSeparator.frame) + 16;
    detailViewFrame.size.width = self.templateEditingView.frame.size.width - detailViewFrame.origin.x - 32;
    detailViewFrame.size.height = self.view.frame.size.height - detailViewFrame.origin.y - kUIFileNameDefaultPadding;
    
    self.dateFormatRadioGroup.frame = detailViewFrame;
    self.freeTextDetailView.frame = detailViewFrame;
    self.autoNumberDetailView.frame = detailViewFrame;
    
    [self repostionFieldButtons];
}

/** Update field buttons in position. Call this method when field count changes.*/
- (void)repostionFieldButtons {
    CGFloat y = kUIFileNameDefaultPadding;
    CGFloat x = kUIFileNameLabelX;
    
    for (NSUInteger i = 0; i < kMaxFileNameFieldsCount; ++i) {
        CZDragButton *button = self.fieldButtons[i];
        CGRect frame = button.frame;
        frame.origin.y = y;
        frame.origin.x = x;
        button.home = frame;
        button.frame = frame;
        if (i < [self.template fieldCount]) {
            x += frame.size.width + kUIFileNameDefaultPadding;
        }
        
        if (i < [self.template fieldCount]) {
            button.hidden = NO;
        } else {
            button.hidden = YES;
        }
        button.dragEnabled = self.template.isEditable;
    }
    
    if (self.fieldButtons.count > 0) {
        self.fieldRailView.frame = CGRectMake(kUIFileNameLabelX,
                                              y,
                                              x - kUIFileNameLabelX - kUIFileNameDefaultPadding,
                                              kUIFileNameFieldButtonHeight);
    }
    
    {
        x += 6;
        CGRect frame = self.deleteButton.frame;
        frame.origin.y = (self.deleteButton.superview.bounds.size.height - self.deleteButton.bounds.size.height) / 2;
        frame.origin.x = x;
        self.deleteButton.frame = frame;
        x += frame.size.width + 16;
    }
    
    {
        CGRect frame = self.addButton.frame;
        frame.origin.y = (self.addButton.superview.bounds.size.height - self.addButton.bounds.size.height) / 2;
        frame.origin.x = x;
        self.addButton.frame = frame;
    }
    
    if (self.template == nil) {
        self.deleteButton.hidden = YES;
        self.addButton.hidden = YES;
    } else {
        self.deleteButton.hidden = NO;
        self.deleteButton.enabled = self.template.isEditable && self.template.fieldCount > 1;
        self.addButton.hidden = NO;
        self.addButton.enabled = self.template.isEditable && self.template.fieldCount < kMaxFileNameFieldsCount;
    }
}

- (void)switchToTemplate:(CZFileNameTemplate *)template {
    self.template = template;
    
    _selectedFieldIndex = 0;  // select first field
    
    self.fieldRailView.hidden = NO;
    self.fileNameLabel.hidden = NO;
    
    [self repostionFieldButtons];
    [self refreshControlsStatus];
}

- (void)showHelpOverlay {
    BOOL hide = [[NSUserDefaults standardUserDefaults] boolForKey:kHideInfoScreenFNT];
    if (!hide) {
        CZHelpOverlayView *helpOverlayer = [[CZHelpOverlayView alloc] init];
        helpOverlayer.hideNextTimeKey = kHideInfoScreenFNT;
        
        [helpOverlayer setIntroductoryText:L(@"FNT_HELP_TITLE")
                              bulletPoints:nil];
        
        [helpOverlayer addBalloonText:L(@"FNT_HELP_CHOOSER")
                              atFrame:CGRectMake(22, 250, 220, 380)
                               toView:nil
                            direction:CZBalloonDirectionNone];
        
        [helpOverlayer addBalloonText:L(@"FNT_HELP_SAMPLE_FILE")
                              atFrame:CGRectMake(270, 220, 220, 68)
                               toView:self.fileNameLabel
                            direction:CZBalloonDirectionUp];
        
        [helpOverlayer addBalloonText:L(@"FNT_HELP_RAIL")
                              atFrame:CGRectMake(680, 260, 280, 68)
                               toView:self.fieldRailView
                            direction:CZBalloonDirectionUp];
    }
}

#pragma mark - UI actions

- (void)freeTextFieldEditingChanged:(id)sender {
    if (self.template && _selectedFieldIndex < [self.template fieldCount]) {
        CZFileNameTemplateField *field = [self.template fieldAtIndex:_selectedFieldIndex];
        if (field.type == CZFileNameTemplateFieldTypeFreeText) {
            NSString *text = self.freeTextField.text;
            if (![text isEqualToString:field.value]) {
                field.value = text;
            }
            [self refreshControlsStatus];
        }
    }
}

- (void)userInputSwitchChanged:(id)sender {
    self.askUserInputButton.on = !self.askUserInputButton.isOn;
    if (self.template && _selectedFieldIndex < [self.template fieldCount]) {
        CZFileNameTemplateField *field = [self.template fieldAtIndex:_selectedFieldIndex];
        if (field.type == CZFileNameTemplateFieldTypeFreeText) {
            field.userInput = self.askUserInputButton.isOn;
        }
    }
}

- (void)digitsCountTextFieldAction:(id)sender {
    int digits = self.digitsCountTextField.text.integerValue;
    digits = MAX(digits, 1);
    digits = MIN(digits, 6);
    self.digitsCountTextField.text = @(digits).stringValue;
    if (self.template && _selectedFieldIndex < [self.template fieldCount]) {
        CZFileNameTemplateField *field = [self.template fieldAtIndex:_selectedFieldIndex];
        if (field.type == CZFileNameTemplateFieldTypeAutoNumber) {
            field.value = [NSString stringWithFormat:@"%d", digits];
            [self refreshControlsStatus];
        }
    }
}

- (void)fieldButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    UIButton *button = sender;
    _selectedFieldIndex = button.tag;
    
    NSUInteger i = 0;
    for (UIButton *button in self.fieldButtons) {
        button.selected = i == _selectedFieldIndex;
        i++;
    }
    
    CZLogv(@"selected No.%lu field", (unsigned long)_selectedFieldIndex);
    
    [self refreshControlsStatus];
}

- (void)addButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (self.template && [self.template fieldCount] < kMaxFileNameFieldsCount) {
        CZFileNameTemplateField *field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeFreeText
                                                                                 value:@"Snap"];  // Add free text for default field
        [self.template addField:field];
        
        _selectedFieldIndex = [self.template fieldCount] - 1;
        
        [self repostionFieldButtons];
        [self refreshControlsStatus];
    }
}

- (void)deleteButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (self.template && _selectedFieldIndex < [self.template fieldCount]) {
        [self.template removeFieldAtIndex:_selectedFieldIndex];
        if (_selectedFieldIndex == [self.template fieldCount]) {
            _selectedFieldIndex--;
        }
        
        [self repostionFieldButtons];
        [self refreshControlsStatus];
    }
}

#pragma mark - CZFileNameTemplatePickerViewControllerDelegate

- (void)fileNameTemplatePickerViewController:(CZFileNameTemplatePickerViewController *)picker didSelectFileNameTemplate:(CZFileNameTemplate *)template {
    [self.view endEditing:YES];
    [self switchToTemplate:template];
}

#pragma mark - CZButtonGroupDelegate methods

- (void)buttonGroup:(CZButtonGroup *)buttonGroup didSelectIndex:(NSUInteger)i {
    if (self.typeRadioGroup == buttonGroup) {
        CZLogv(@"select type %lu", (unsigned long)i);
        
        CZFileNameTemplateFieldType type = [CZFileNameTemplateConfigurationViewController fieldTypeForIndex:i];
        NSString *defaultValue = [CZFileNameTemplateConfigurationViewController defaultFieldValueForIndex:i];
        if (_selectedFieldIndex < [self.template fieldCount]) {
            CZFileNameTemplateField *field = [[CZFileNameTemplateField alloc] initWithType:type value:defaultValue];
            [self.template replaceFieldAtIndex:_selectedFieldIndex withField:field];
        }
        
        [self refreshControlsStatus];
    } else if (self.dateFormatRadioGroup == buttonGroup) {
        // change date time format
        if (_selectedFieldIndex < [self.template fieldCount]) {
            CZFileNameTemplateField *field = [self.template fieldAtIndex:_selectedFieldIndex];
            field.value = [NSString stringWithFormat:@"%lu", (unsigned long)i];
        }
        
        [self refreshControlsStatus];
    }
}

#pragma mark - CZDragButtonDelegate methods

- (void)dragButtonStartedTracking:(CZDragButton *)button {
    [self.fieldButtonBar bringSubviewToFront:button];
    _draggingToIndex = button.tag;
}

- (void)dragButtonMoved:(CZDragButton *)draggingButton {
    // if the button is above the delete button
    if (CGRectIntersectsRect([draggingButton frame], [self.deleteButton frame])) {
        draggingButton.alpha = 0.5;
    } else {
        draggingButton.alpha = 1.0;
    }
  
    // we'll reorder only if the button is overlapping the its super view
    if (CGRectIntersectsRect([draggingButton frame], [self.fieldButtonBar bounds])) {
        BOOL draggingRight = [draggingButton frame].origin.x > [draggingButton home].origin.x ? YES : NO;
        
        // we're going to shift over all the buttons who live between the home of the moving button
        // and the current touch location. A button counts as living in this area if the midpoint
        // of its home is contained in the area.
        NSMutableArray *buttonsToShift = [[NSMutableArray alloc] init];
        
        // get the touch location in the coordinate system of the scroll view
        CGPoint touchLocation = [draggingButton convertPoint:[draggingButton touchLocation] toView:self.fieldButtonBar];
        
        // calculate minimum and maximum boundaries of the affected area
        float minX = draggingRight ? CGRectGetMaxX([draggingButton home]) : touchLocation.x;
        float maxX = draggingRight ? touchLocation.x : CGRectGetMinX([draggingButton home]);
        
        // iterate through field buttons and see which ones need to move over
        for (CZDragButton *button in self.fieldButtons) {
            
            // skip the button being dragged
            if (button == draggingButton) continue;
            
            // skip the hidden buttons
            if (button.hidden) {
                continue;
            }
            
            float buttonMidpoint = CGRectGetMidX([button home]);
            if (buttonMidpoint >= minX && buttonMidpoint <= maxX) {
                [buttonsToShift addObject:button];
            }
        }
        
        // shift over the other buttons to make room for the dragging button. (if we're dragging right, they shift to the left)
        float otherButtonShift = ([draggingButton home].size.width + kUIFileNameDefaultPadding) * (draggingRight ? -1 : 1);
        
        // as we shift over the other thumbs, we'll calculate how much the dragging button's home is going to move
        float draggingThumbShift = 0.0;
        
        // send each of the shifting thumbs to its new home
        for (CZDragButton *otherThumb in buttonsToShift) {
            CGRect home = [otherThumb home];
            home.origin.x += otherButtonShift;
            [otherThumb setHome:home];
            [otherThumb goHome];
            draggingThumbShift += ([otherThumb frame].size.width + kUIFileNameDefaultPadding) * (draggingRight ? 1 : -1);
        }

        // change the home of the dragging button, but don't send it there because it's still being dragged
        CGRect home = [draggingButton home];
        home.origin.x += draggingThumbShift;
        [draggingButton setHome:home];
        
        if ([buttonsToShift count] > 0) {
            if (draggingRight) {
                _draggingToIndex += [buttonsToShift count];
            } else {
                _draggingToIndex -= [buttonsToShift count];
            }
            
            CZLogv(@"draging to index %lu.", (unsigned long)_draggingToIndex);
        }
    }
}

- (void)dragButtonStoppedTracking:(CZDragButton *)button {
    button.alpha = 1.0;  // clear the deleting appearance
    
    // if button is above delete button
    if (CGRectIntersectsRect([button frame], [self.deleteButton frame]) && self.deleteButton.isEnabled) {
        button.hidden = YES;
        [self deleteButtonTapped:button];
        return;
    }

    // dragging is finished, then modify the template
    CZLogv(@"final draging to index %lu.", (unsigned long)_draggingToIndex);
    NSUInteger oldIndex = button.tag;
    NSUInteger minCount = MIN([self.template fieldCount], [self.fieldButtons count]);    
    
    if (oldIndex != _draggingToIndex &&
        self.template &&
        oldIndex < minCount &&
        _draggingToIndex < minCount) {
        
        // re-order fields
        CZFileNameTemplateField *field = [self.template fieldAtIndex:oldIndex];
        [self.template removeFieldAtIndex:oldIndex];
        [self.template insertField:field atIndex:_draggingToIndex];
        
        // re-order the field buttons
        id button = [self.fieldButtons objectAtIndex:oldIndex];
        [self.fieldButtons removeObjectAtIndex:oldIndex];
        [self.fieldButtons insertObject:button atIndex:_draggingToIndex];
        
        // reset tag
        NSUInteger i = 0;
        for (UIButton *button in self.fieldButtons) {
            button.tag = i;
            i++;
        }
        
        _selectedFieldIndex = _draggingToIndex;
        
        // refresh UI
        [self refreshFileName];
    }
}

#pragma mark - UITextFieldDelegate method

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    return [CZCommonUtils isStringValidFileNameTemplate:textEntered];
}

@end
