//
//  CZFileNameTemplateCell.m
//  Hermes
//
//  Created by Li, Junlin on 6/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileNameTemplateCell.h"

@implementation CZFileNameTemplateCell

@synthesize iconView = _iconView;
@synthesize fileNameEditor = _fileNameEditor;
@synthesize separator = _separator;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        selectedBackgroundView.backgroundColor = [UIColor cz_gs110];
        self.selectedBackgroundView = selectedBackgroundView;
        
        [self.contentView addSubview:self.iconView];
        [self.contentView addSubview:self.fileNameEditor];
        [self.contentView addSubview:self.separator];
        
        [self.iconView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:16.0].active = YES;
        [self.iconView.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
        
        [self.fileNameEditor.leadingAnchor constraintEqualToAnchor:self.iconView.trailingAnchor constant:16.0].active = YES;
        [self.fileNameEditor.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-4.0].active = YES;
        [self.fileNameEditor.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
        [self.fileNameEditor.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
        
        [self.separator.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor].active = YES;
        [self.separator.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor].active = YES;
        [self.separator.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    self.fileNameEditor.editButton.hidden = !self.editable || !selected;
}

#pragma mark - Views

- (UIImageView *)iconView {
    if (_iconView == nil) {
        _iconView = [[UIImageView alloc] init];
        _iconView.translatesAutoresizingMaskIntoConstraints = NO;
        _iconView.image = [UIImage cz_imageWithIcon:[CZIcon iconNamed:@"file-icon-folder"]];
        [_iconView.widthAnchor constraintEqualToConstant:24.0].active = YES;
        [_iconView.heightAnchor constraintEqualToConstant:24.0].active = YES;
    }
    return _iconView;
}

- (CZFileNameEditor *)fileNameEditor {
    if (_fileNameEditor == nil) {
        _fileNameEditor = [[CZFileNameEditor alloc] init];
        _fileNameEditor.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _fileNameEditor;
}

- (UIView *)separator {
    if (_separator == nil) {
        _separator = [[UIView alloc] init];
        _separator.translatesAutoresizingMaskIntoConstraints = NO;
        _separator.backgroundColor = [UIColor cz_gs115];
        [_separator.heightAnchor constraintEqualToConstant:1.0].active = YES;
    }
    return _separator;
}

@end
