//
//  CZFileEntity+Icon.m
//  Hermes
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileEntity+Icon.h"

@implementation CZFileEntity (Icon)

- (CZIcon *)icon {
    NSString *extension = self.filePath.pathExtension.lowercaseString;
    NSString *suffix = @"general";
    if ([extension isEqualToString:@"jpeg"]) {
        suffix = @"jpg";
    } else if ([extension isEqualToString:@"tif"]) {
        suffix = @"tiff";
    } else if ([extension isEqualToString:@"jpg"] ||
               [extension isEqualToString:@"tiff"] ||
               [extension isEqualToString:@"czi"] ||
               [extension isEqualToString:@"png"] ||
               [extension isEqualToString:@"pdf"] ||
               [extension isEqualToString:@"rtf"] ||
               [extension isEqualToString:@"mp4"] ||
               [extension isEqualToString:@"czrtj"] ||
               [extension isEqualToString:@"czftp"] ||
               [extension isEqualToString:@"czamt"] ||
               [extension isEqualToString:@"csv"]) {
        suffix = extension;
    }
    
    NSString *iconName = [NSString stringWithFormat:@"file-icon-%@", suffix];
    return [CZIcon iconNamed:iconName];
}

@end

@implementation NSString (Icon)

- (CZIcon *)icon {
    NSString *extension = self.pathExtension.lowercaseString;
    NSString *suffix = @"general";
    if ([extension isEqualToString:@"jpeg"]) {
        suffix = @"jpg";
    } else if ([extension isEqualToString:@"tif"]) {
        suffix = @"tiff";
    } else if ([extension isEqualToString:@"jpg"] ||
               [extension isEqualToString:@"tiff"] ||
               [extension isEqualToString:@"czi"] ||
               [extension isEqualToString:@"png"] ||
               [extension isEqualToString:@"pdf"] ||
               [extension isEqualToString:@"rtf"] ||
               [extension isEqualToString:@"mp4"] ||
               [extension isEqualToString:@"czrtj"] ||
               [extension isEqualToString:@"czftp"] ||
               [extension isEqualToString:@"czamt"] ||
               [extension isEqualToString:@"csv"]) {
        suffix = extension;
    }
    
    NSString *iconName = [NSString stringWithFormat:@"file-icon-%@", suffix];
    return [CZIcon iconNamed:iconName];
}

@end
