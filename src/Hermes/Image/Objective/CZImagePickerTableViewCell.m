//
//  CZImagePickerTableViewCell.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/15. 
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImagePickerTableViewCell.h"
#import "CZUIDefinition.h"
#import <CZFileKit/CZFileKit.h>
#import "CZFileEntity+Icon.h"

static const CGFloat kThumbnailDefaultSize = 48.0f;
static const CGFloat kControlsDefaultMargin = 8.0f;
static const CGFloat kLabelsDefaultHeight = 18.0f;

@interface CZImagePickerTableViewCell ()

@property (nonatomic, strong) UILabel *fileLabel;
@property (nonatomic, strong) UIImageView *thumbnailView;
@property (nonatomic, strong) UIView *separatorLine;

@end

@implementation CZImagePickerTableViewCell

@synthesize fileDetialLabel = _fileDetialLabel;
@synthesize fileEntity = _fileEntity;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor cz_gs105];
        [self.contentView addSubview:self.fileDetialLabel];
        [self.contentView addSubview:self.thumbnailView];
        [self.contentView addSubview:self.fileLabel];
        [self.contentView addSubview:self.separatorLine];
        
        self.fileDetialLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
        self.fileLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.separatorLine.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.thumbnailView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:kControlsDefaultMargin * 2].active = YES;
        [self.thumbnailView.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
        [self.thumbnailView.widthAnchor constraintEqualToConstant:kThumbnailDefaultSize].active = YES;
        [self.thumbnailView.heightAnchor constraintEqualToConstant:kThumbnailDefaultSize].active = YES;

        [self.fileLabel.leftAnchor constraintEqualToAnchor:self.thumbnailView.rightAnchor constant:kControlsDefaultMargin * 2].active = YES;
        [self.fileLabel.topAnchor constraintEqualToAnchor:self.thumbnailView.centerYAnchor constant:-kLabelsDefaultHeight].active = YES;
        [self.fileLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-kControlsDefaultMargin * 2].active = YES;
        [self.fileLabel.heightAnchor constraintEqualToConstant:kLabelsDefaultHeight].active = YES;

        [self.fileDetialLabel.leftAnchor constraintEqualToAnchor:self.fileLabel.leftAnchor].active = YES;
        [self.fileDetialLabel.topAnchor constraintEqualToAnchor:self.fileLabel.bottomAnchor].active = YES;
        [self.fileDetialLabel.rightAnchor constraintEqualToAnchor:self.fileLabel.rightAnchor].active = YES;
        [self.fileDetialLabel.heightAnchor constraintEqualToAnchor:self.fileLabel.heightAnchor].active = YES;

        [self.separatorLine.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
        [self.separatorLine.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
        [self.separatorLine.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = YES;
        [self.separatorLine.heightAnchor constraintEqualToConstant:1.0f].active = YES;
    }
    
    return self;
}

- (void)updateThumbnail:(UIImage *)thumbnail fileEntity:(CZFileEntity *)fileEntity {
    if (thumbnail == nil) {
        thumbnail = [UIImage cz_imageWithIcon:fileEntity.icon];
        self.thumbnailView.contentMode = UIViewContentModeCenter;
    } else {
        self.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    }
    self.thumbnailView.image = thumbnail;
    self.fileLabel.text = [fileEntity.filePath lastPathComponent];
    _fileEntity = fileEntity;
}

- (CZFileEntity *)fileEntity {
    return _fileEntity;
}

#pragma mark - Getter

- (UIImageView *)thumbnailView {
    if (_thumbnailView == nil) {
        _thumbnailView = [[UIImageView alloc] init];
        _thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailView.backgroundColor = [UIColor cz_gs105];
        _thumbnailView.clipsToBounds = YES;
    }

    return _thumbnailView;
}

- (UILabel *)fileLabel {
    if (_fileLabel == nil) {
        _fileLabel = [[UILabel alloc] init];
        _fileLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _fileLabel.font = [UIFont cz_caption];
        _fileLabel.backgroundColor = [UIColor clearColor];
        _fileLabel.textColor = [UIColor cz_gs50];
    }
    
    return _fileLabel;
}

- (UILabel *)fileDetialLabel {
    if (_fileDetialLabel == nil) {
        _fileDetialLabel = [[UILabel alloc] init];
        _fileDetialLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _fileDetialLabel.font = [UIFont cz_caption];
        _fileDetialLabel.backgroundColor = [UIColor clearColor];
        _fileDetialLabel.textColor = [UIColor cz_gs80];
    }
    
    return _fileDetialLabel;
}

- (UIView *)separatorLine {
    if (_separatorLine == nil) {
        _separatorLine = [[UIView alloc] init];
        _separatorLine.backgroundColor = [UIColor cz_gs115];
    }
    return _separatorLine;
}

@end
