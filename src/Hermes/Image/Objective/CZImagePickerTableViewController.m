//
//  CZImagePickerTableViewController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/15.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImagePickerTableViewController.h"
#import "CZUIDefinition.h"
#import "CZImagePickerTableViewCell.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZFileKit/CZFileKit.h>
#import "CZToolbar.h"
#import "CZLocalFilesViewModel.h"
#import "CZLocalFileList.h"

static NSString * const kTextCellIdentifier = @"TextCell";
static const CGFloat kNavigationBarDefaultHeight = 48;

@interface CZImagePickerTableViewController ()<
UITableViewDelegate,
UITableViewDataSource,
CZLocalFileListDelegate
>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) CZToolbar *navigationToolBar;
@property (nonatomic, strong) CZLocalFilesViewModel *localFilesViewModel;
@property (nonatomic, strong) UIStackView *stackView;

@end

@implementation CZImagePickerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CZToolbarItem *spaceItem1 = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierFlexibleSpace];
    
    CZToolbarItem *titleItem = [CZToolbarItem labelItemWithIdentifier:CZToolbarItemIdentifierAnonymous];
    titleItem.label.text = L(@"FILES_LOCAL_TITLE");
    titleItem.label.font = [UIFont cz_h4];
    titleItem.label.textColor = [UIColor cz_gs50];

    CZToolbarItem *spaceItem2 = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierFlexibleSpace];
    self.navigationToolBar.items = @[spaceItem1, titleItem, spaceItem2];
    
    [self.stackView addArrangedSubview:self.titleLabel];
    [self.stackView addArrangedSubview:self.navigationToolBar];
    [self.stackView addArrangedSubview:self.tableView];
    
    [self.view addSubview:self.stackView];

    self.stackView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.stackView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:32.0f].active = YES;
    [self.stackView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.stackView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor constant:-32.0f * 2].active = YES;
    [self.stackView.heightAnchor constraintEqualToAnchor:self.view.heightAnchor constant:-27.0f * 2].active = YES;

    [CZLocalFileList sharedInstance].delegate = self;
}

- (void)dealloc {
    [CZLocalFileList sharedInstance].delegate = nil;
    _localFilesViewModel.delegate = nil;
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
}

#pragma mark - Public method

- (BOOL)hasVisibleFiles {
    NSArray *localFileEntities = self.localFilesViewModel.files;
    
    for (CZFileEntity *fileEntity in localFileEntities) {
        NSString *filePath = fileEntity.filePath;
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[filePath pathExtension]];
        if (fileFormat == kCZFileFormatCZI || fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Private method

+ (NSDate *)modifiedDateOfFile:(NSString *)filePath {
    NSDate *modificationDate = nil;
    NSError *error = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&error];
    
    if (error) {
        modificationDate = [NSDate dateWithTimeIntervalSinceNow:0];
        CZLogv(@"Failed to get file attribute for %@ in thumbnailTaskDidFinish:%@", filePath, error.description);
    } else {
        modificationDate = [fileAttributes fileModificationDate];
    }
    
    return modificationDate;
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CZTableViewCellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.localFilesViewModel.visibleFiles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZImagePickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTextCellIdentifier forIndexPath:indexPath];
    NSUInteger row = indexPath.row;

    NSArray *visibleFiles = self.localFilesViewModel.visibleFiles;
    if (row < visibleFiles.count) {
        CZFileEntity *fileEntity = visibleFiles[row];
        
        UIImage *thumbnail = [[CZLocalFileList sharedInstance] thumbnailFrom:fileEntity];
        [cell updateThumbnail:thumbnail fileEntity:fileEntity];
        
        if ([self.selectionDelegate respondsToSelector:@selector(imagePicker:queryDetailOfFile:)]) {
            NSString *detailText = [self.selectionDelegate imagePicker:self queryDetailOfFile:fileEntity.filePath];
            if (detailText) {
                cell.fileDetialLabel.text = detailText;
            }
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *visibleFiles = self.localFilesViewModel.visibleFiles;
    if ([self.selectionDelegate respondsToSelector:@selector(imagePicker:didSelectFile:atIndex:)]) {
        [self.selectionDelegate imagePicker:self didSelectFile:visibleFiles atIndex:indexPath.row];
    }
}

#pragma mark - CZLocalFileListDelegate

- (void)localFileListShouldUpdateThumbnail:(UIImage *)thumbnail filePath:(NSString *)path {
    if (path) {
        for (CZImagePickerTableViewCell *cell in self.tableView.visibleCells) {
            if ([path isEqualToString:cell.fileEntity.filePath]) {
                [cell updateThumbnail:thumbnail fileEntity:cell.fileEntity];
                break;
            }
        }
    }
}

#pragma mark - CZLocalFileListManagerDelegate

- (void)localFileListUpdate {
    [self.tableView reloadData];
}

- (BOOL)shouldQueryShowFile {
    return [self.selectionDelegate respondsToSelector:@selector(imagePicker:shouldListImageProperties:fromFilePath:)];
}

- (BOOL)shouldShowFile:(CZFileEntity *)fileEntity {
    NSString *filePath = fileEntity.filePath;
    CZDocManager *docManager = [CZDocManager newDocManagerFromFilePath:filePath];
    CZImageProperties *imageProperties = docManager.imageProperties;
    return [self.selectionDelegate imagePicker:self shouldListImageProperties:imageProperties fromFilePath:filePath];
}

- (void)thumbnail:(UIImage *)thumbnail didGenerateFrom:(NSString *)path {
    NSArray *visibleFiles = self.localFilesViewModel.visibleFiles;
    if (visibleFiles.count == 0) {
        return;
    }
    
    // calculate visible low bound and high bound
    NSArray *indexPaths = [self.tableView indexPathsForVisibleRows];
    NSUInteger low = NSNotFound, high = 0U;
    for (NSIndexPath *indexPath in indexPaths) {
        NSUInteger row = indexPath.row;
        if (row < low) {
            low = indexPath.row;
        }
        
        if (row > high) {
            high = indexPath.row;
        }
    }
    
    if (low == NSNotFound) {
        return;
    }
    
    // expand range both head and tail by 1
    if (low > 0) {
        low--;
    }
    high++;
    if (high >= visibleFiles.count) {
        high = visibleFiles.count - 1;
    }
    
    // search index of path in visible files array
    NSUInteger index = [self.localFilesViewModel indexOfVisibleFile:path inRange:NSMakeRange(low, high - low + 1U)];
    
    if (index != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        CZImagePickerTableViewCell *cell = (CZImagePickerTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        CZFileEntity *tmpFileEntity = nil;
        for (CZFileEntity *fileEntity in visibleFiles) {
            if ([fileEntity.filePath isEqualToString:path]) {
                tmpFileEntity = fileEntity;
                break;
            }
        }
        [cell updateThumbnail:thumbnail fileEntity:tmpFileEntity];
    }
}

#pragma mark - Getter

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor cz_gs105];
        _tableView.backgroundView = nil;
        _tableView.separatorColor = [UIColor darkGrayColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = YES;
        _tableView.showsVerticalScrollIndicator = YES;
        _tableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _tableView.bounces = YES;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        [_tableView registerClass:[CZImagePickerTableViewCell class] forCellReuseIdentifier:kTextCellIdentifier];
    }
    
    return _tableView;
}

- (CZToolbar *)navigationToolBar {
    if (_navigationToolBar == nil) {
        _navigationToolBar = [[CZToolbar alloc] initWithFrame:CGRectZero];
        _navigationToolBar.backgroundColor = [UIColor cz_gs105];
        [_navigationToolBar.heightAnchor constraintEqualToConstant:kNavigationBarDefaultHeight].active = YES;
    }
    
    return _navigationToolBar;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor cz_gs10];
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.text = L(@"IMAGE_PICKER_TITLE");
        [_titleLabel.heightAnchor constraintEqualToConstant:72.0].active = YES;
    }
    
    return _titleLabel;
}

- (CZLocalFilesViewModel *)localFilesViewModel {
    if (_localFilesViewModel == nil) {
        NSString *documentPath = [NSFileManager defaultManager].cz_documentPath;
        _localFilesViewModel = [[CZLocalFilesViewModel alloc] initWithDocumentPath:documentPath];
        _localFilesViewModel.filterType = CZFileFilterTypeImage;
        _localFilesViewModel.sortOrder = CZSortByNameAscend;
    }
    
    return _localFilesViewModel;
}

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.alignment = UIStackViewAlignmentFill;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.spacing = 2;
    }
    return _stackView;
}

@end
