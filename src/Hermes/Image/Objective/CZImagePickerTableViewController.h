//
//  CZImagePickerTableViewController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/15.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZImagePickerTableViewController;
@class CZImageProperties;

@protocol CZImagePickerTableViewDelegate <NSObject>
@optional
- (void)imagePicker:(CZImagePickerTableViewController *)picker didSelectFile:(NSArray *)fileEntities atIndex:(NSUInteger)index;
- (NSString *)imagePicker:(CZImagePickerTableViewController *)picker queryDetailOfFile:(NSString *)filePath;

/** @return if YES, list file in image picker table view, otherwise remove from list. If delegate doesn't implement this method, effect equals to return YES. */
- (BOOL)imagePicker:(CZImagePickerTableViewController *)picker shouldListImageProperties:(CZImageProperties *)imageProperties fromFilePath:(NSString *)filePath;
@end

@interface CZImagePickerTableViewController : UIViewController

@property (nonatomic, weak) id<CZImagePickerTableViewDelegate> selectionDelegate;
@property (nonatomic, copy) NSString *exceptionFilePath;

- (BOOL)hasVisibleFiles;

@end
