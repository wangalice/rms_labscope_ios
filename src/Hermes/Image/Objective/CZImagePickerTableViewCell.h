//
//  CZImagePickerTableViewCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/15.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileEntity;
@interface CZImagePickerTableViewCell : UITableViewCell

@property (nonatomic, readonly, strong) CZFileEntity *fileEntity;
@property (nonatomic, readonly, strong) UILabel *fileDetialLabel;

- (void)updateThumbnail:(UIImage *)thumbnail fileEntity:(CZFileEntity *)fileEntity;

@end

