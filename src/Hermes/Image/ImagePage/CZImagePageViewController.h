//
//  CZImagePageViewController.h
//  Hermes
//
//  Created by Ralph Jin on 6/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZAnnotationKit/CZElementScaleBar.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZAnnotationKit/CZSelectTool.h>
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZAnnotationMarco.h"
#import "CZImageViewEditingDelegate.h"

@class CZDocManager, CZMagnifierView, CZImageEditingViewController, CZImagePageViewController;
@protocol CZImagePageViewControllerDelegate;

@protocol CZImagePageViewControllerDelegate <NSObject>
@optional
- (void)imagePageViewController:(CZImagePageViewController *)controller docManagerModified:(CZDocManager *)docManager;

- (void)imagePageViewControllerDidFinishLoading:(CZImagePageViewController *)controller;
- (void)imagePageViewControllerDidPinchDismiss:(CZImagePageViewController *)controller;

@end

typedef id<CZImageToolDelegate, CZImagePageViewControllerDelegate, CZImageViewEditingDelegate, CZDocManagerDelegate> CZImagePageViewCombineDelegate;

@interface CZImagePageViewController : UIViewController<UIScrollViewDelegate, CZMagnifyDelegate>

@property (nonatomic, readonly, retain) CZImageTool *imageTool;
@property (nonatomic, readonly, assign) UIView *imageView;
@property (nonatomic, retain) CZDocManager *docManager;
@property (nonatomic, assign) CZImagePageViewCombineDelegate delegate;
@property (nonatomic, assign) id<CZImageViewEditingDelegate> imageEditingDelegate;
@property (nonatomic, assign) id<CZElementLayerDelegate> elementLayerDelegate;  // sencond hand delegate
@property (nonatomic, assign) CZMagnifierView *magnifyView;
@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, assign) BOOL lockScrolling;
@property (nonatomic, assign) NSUInteger pageIndex;

@property (atomic, assign, getter = isImageLoaded) BOOL imageLoaded;
@property (atomic, assign) BOOL overExposeEnableAfterNewImageLoaded;
@property (nonatomic, assign) CZDisplayCurvePreset currentDisplayCurve;
@property (nonatomic, assign) CZEditingMode editingMode;
@property (nonatomic, assign) BOOL needInvalidateMultiphaseOverlayLayer;

/*! This method is desinged only for call in main thread*/
- (void)loadImage;
/*! This method is desinged only for call in main thread*/
- (void)cancelLoadingImage;
- (void)loadThumbnail;

- (void)beginEditingAnnotation;  // TODO: rename to Add editing margin
- (void)endEditingAnnotation;  // TODO: rename to Remove editing margin
- (void)invalidateAllAnnotations;

/*! These method is desinged only for hide & render annotations, changed nothing */
- (void)hideAllAnnotations;
- (void)showAllAnnotations;

- (void)undoAction;
- (void)redoAction;

- (CGFloat)zoomScale;
- (void)fitImageViewIntoScrollView:(BOOL)resetZoom;
- (void)scrollRectToVisible:(CGRect)rect;
- (void)assignSecondaryScrollViewDelegate:(CZImagePageViewController *)secondaryPageViewController;

- (void)invalidateMultiphaseAreaAndUpdateAnalyzerResult:(BOOL)updatesAnalyzerResult
                                        completionBlock:(void (^)())completionBlock;

- (void)invalidateMultiphaseAreaFromAnalyzerResult;

- (void)invalidateMultiphaseArea;

- (void)updateGrainSizeLayer;

- (void)ennablePinchDismiss:(BOOL)enabled;

@end
