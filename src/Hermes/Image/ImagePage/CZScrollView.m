//
//  CZScrollView.m
//  Hermes
//
//  Created by Mike Wang on 7/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZScrollView.h"

@interface CZScrollView() {
    CGPoint _previousSecondaryScrollViewContentOffset;
    BOOL _horizontalLockScrollViewStandby;
    BOOL _verticalLockScrollViewStandby;
    BOOL _isLockStandbyOnTop;
    BOOL _isLockStandbyOnLeft;
    BOOL _isZoomByPrimaryScrollView;
    BOOL _isDragByPrimaryScrollView;
    
    BOOL _bouncesBeforeLocking;
    BOOL _bouncesZoomBeforeLocking;

    CGFloat _prevScale;
}

@end

@implementation CZScrollView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (void)dealloc {
    _secondaryScrollViewDelegate = nil;
}

- (void)setLockScrolling:(BOOL)lockScrolling {
    if (_lockScrolling == lockScrolling) {
        return;
    }

    _lockScrolling = lockScrolling;
    _lockStandbySecondaryScrollViewContentOffset = CGPointZero;
    _verticalLockScrollViewStandby = NO;
    _horizontalLockScrollViewStandby = NO;

    if (lockScrolling) {
        _minimumZoomScaleBeforeLocking = self.minimumZoomScale;
        _maximumZoomScaleBeforeLocking = self.maximumZoomScale;
        _bouncesBeforeLocking = self.bounces;
        self.bounces = NO;
        _bouncesZoomBeforeLocking = self.bouncesZoom;
        self.bouncesZoom = NO;
        [self.pinchGestureRecognizer addTarget:self action:@selector(scrollViewHandlePinch:)];
        [self.panGestureRecognizer addTarget:self action:@selector(scrollViewHandlePan:)];
    } else {
        self.minimumZoomScale = _minimumZoomScaleBeforeLocking;
        self.maximumZoomScale = _maximumZoomScaleBeforeLocking;
        self.bounces = _bouncesBeforeLocking;
        self.bouncesZoom = _bouncesZoomBeforeLocking;
        [self.pinchGestureRecognizer removeTarget:self action:@selector(scrollViewHandlePinch:)];
        [self.panGestureRecognizer removeTarget:self action:@selector(scrollViewHandlePan:)];
    }
}

- (void)scrollViewHandlePinch:(UIPinchGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.numberOfTouches < 2) {
        return;
    }

    [self.secondaryScrollViewDelegate zoomRectFromScrollView:self];
}

- (void)scrollViewHandlePan:(UIPanGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.numberOfTouches > 1) {
        return;
    }

    [self scrollRecFromScrollView:self];
}

#pragma mark = CZScrollViewDelegate
- (void)scrollViewDidExitFromHorizontalLockStandby:(CZScrollView *)scrollView {
    CGPoint offset = CGPointZero;
    offset.x = scrollView.lockStandbySecondaryScrollViewContentOffset.x;
    offset.y = self.contentOffset.y;
    self.contentOffset = offset;
}

- (void)scrollViewDidExitFromVerticalLockStandby:(CZScrollView *)scrollView {
    CGPoint offset = CGPointZero;
    offset.y = scrollView.lockStandbySecondaryScrollViewContentOffset.y;
    offset.x = self.contentOffset.x;
    self.contentOffset = offset;
}


- (void)zoomRectFromScrollView:(CZScrollView *)scrollView {
    CGRect zoomRect;
    
    CGFloat newScale = scrollView.zoomScale;
    if (newScale <= 0.0001) {
        newScale = 0.0001;
    }
    
    zoomRect.size.height = self.bounds.size.height / newScale;
    zoomRect.size.width  = self.bounds.size.width  / newScale;
        

    UIView *primaryImageView = [scrollView viewForZoomingInScrollView:scrollView];
    CGFloat percentOfXDirectionMovedByPrimaryView = (scrollView.contentOffset.x)/primaryImageView.frame.size.width;
    CGFloat percentOfYDirectionMovedByPrimaryView = (scrollView.contentOffset.y)/primaryImageView.frame.size.height;
    
    UIView *imageView = [self viewForZoomingInScrollView:self];
    zoomRect.origin.x = percentOfXDirectionMovedByPrimaryView*imageView.frame.size.width/newScale;
    zoomRect.origin.y = percentOfYDirectionMovedByPrimaryView*imageView.frame.size.height/newScale;


    //TODO: work around, if zoomout and contentoffset is negative, content image will drop down to bottom
    BOOL isZoomOut = NO;
    if (_prevScale >= newScale) {
        isZoomOut = YES;
    }
    
    if (isZoomOut && self.contentOffset.y <= 0 && self.contentOffset.x <=0) {
       return;
    }

    [self zoomToRect:zoomRect animated:NO];
}

- (void)scrollRecFromScrollView:(CZScrollView *)scrollView {
    UIView *contentView = [self.primaryScrollViewDelegate viewForZoomingInScrollView:self];
    if (scrollView == self) {
        if (self.lockScrolling && !_isZoomByPrimaryScrollView && !_isDragByPrimaryScrollView) {
            if (_horizontalLockScrollViewStandby) {
                //primary scroll view is hit to the edge
                if (self.contentOffset.x <= 0 ||
                    self.contentOffset.x >= contentView.frame.size.width) {
                    return;
                }
                
                //if primary scroll view is moved away from horizontal edge,
                //resync the horizontal position with secondary scroll view
                _horizontalLockScrollViewStandby = NO;
                return [self.secondaryScrollViewDelegate scrollViewDidExitFromHorizontalLockStandby:self];
            }
            
            if (_verticalLockScrollViewStandby) {
                if (self.contentOffset.y <= 0 || self.contentOffset.y >= contentView.frame.size.width) {
                    return;
                }
                
                //if primary scroll view is moved away from vertical edge,
                //resync the horizontal position with secondary scroll view
                _verticalLockScrollViewStandby = NO;
                return [self.secondaryScrollViewDelegate scrollViewDidExitFromVerticalLockStandby:self];
            }
            
            [self.secondaryScrollViewDelegate scrollRecFromScrollView:scrollView];
        }
    } else {
        CGRect viewToUpdateBounds = self.bounds;
        
        CGPoint secondaryScrollViewoffsetDifference = CGPointMake(scrollView.contentOffset.x - _previousSecondaryScrollViewContentOffset.x,
                                                                  scrollView.contentOffset.y - _previousSecondaryScrollViewContentOffset.y);
        _previousSecondaryScrollViewContentOffset = scrollView.contentOffset;
        
        //check if the secondary scroll view can move away from the horizontal edge after primary scrollview is moved
        BOOL canExitHorizontalLockScrollViewStandby = secondaryScrollViewoffsetDifference.x >= 0 ?
        (fabs(_lockStandbySecondaryScrollViewContentOffset.x) <= fabs(scrollView.contentOffset.x)) && _isLockStandbyOnLeft :
        (fabs(_lockStandbySecondaryScrollViewContentOffset.x) >= fabs(scrollView.contentOffset.x)) && !_isLockStandbyOnLeft;
        
        if (!_horizontalLockScrollViewStandby || canExitHorizontalLockScrollViewStandby) {
            CGFloat scrollViewLeftEdge = self.bounds.origin.x + secondaryScrollViewoffsetDifference.x;
            CGFloat scrollViewRightEdge = scrollViewLeftEdge + self.bounds.size.width;
            if (scrollViewLeftEdge >= contentView.frame.origin.x &&
                scrollViewRightEdge <= contentView.frame.origin.x + contentView.frame.size.width) {
                if (_horizontalLockScrollViewStandby) {
                    //scroll view can move away from horizontal edge
                    viewToUpdateBounds.origin.x += scrollView.contentOffset.x - _lockStandbySecondaryScrollViewContentOffset.x;
                } else {
                    viewToUpdateBounds.origin.x += secondaryScrollViewoffsetDifference.x;
                }
                _horizontalLockScrollViewStandby = NO;
            } else {
                //scroll view is hit to the horizontal edge
                _horizontalLockScrollViewStandby = YES;
                //check if the scroll view hits left or right edge
                _isLockStandbyOnLeft = self.bounds.origin.x + secondaryScrollViewoffsetDifference.x <= contentView.frame.origin.x;
                
                if (_isLockStandbyOnLeft) {
                    _lockStandbySecondaryScrollViewContentOffset.x = scrollView.contentOffset.x + (fabs(secondaryScrollViewoffsetDifference.x) - fabs(viewToUpdateBounds.origin.x - contentView.frame.origin.x));
                    viewToUpdateBounds.origin.x = contentView.frame.origin.x;
                    
                } else {
                    _lockStandbySecondaryScrollViewContentOffset.x = scrollView.contentOffset.x - (fabs(secondaryScrollViewoffsetDifference.x) - fabs(contentView.frame.origin.x + contentView.frame.size.width - viewToUpdateBounds.size.width - viewToUpdateBounds.origin.x));
                    viewToUpdateBounds.origin.x = contentView.frame.origin.x + contentView.frame.size.width - viewToUpdateBounds.size.width;
                }
                
            }
        }
        
        //check if the secondary scroll view can move away from the vertical edge after primary scrollview is moved
        BOOL canExitVerticalLockScrollViewStandby = secondaryScrollViewoffsetDifference.y >= 0 ?
        (fabs(_lockStandbySecondaryScrollViewContentOffset.y) <= fabs(scrollView.contentOffset.y)) && _isLockStandbyOnTop :
        (fabs(_lockStandbySecondaryScrollViewContentOffset.y) >= fabs(scrollView.contentOffset.y)) && !_isLockStandbyOnTop;
        
        if (!_verticalLockScrollViewStandby || canExitVerticalLockScrollViewStandby)
        {
            CGFloat scrollViewTopEdge = self.bounds.origin.y + secondaryScrollViewoffsetDifference.y;
            CGFloat scrollViewBottomEdge = scrollViewTopEdge + self.bounds.size.height;
            
            if (scrollViewTopEdge >= contentView.frame.origin.y &&
                scrollViewBottomEdge <= contentView.frame.origin.y + contentView.frame.size.height) {
                if (_verticalLockScrollViewStandby) {
                    //scroll view can move away from vertical edge
                    viewToUpdateBounds.origin.y += scrollView.contentOffset.y - _lockStandbySecondaryScrollViewContentOffset.y;
                } else {
                    viewToUpdateBounds.origin.y += secondaryScrollViewoffsetDifference.y;
                }
                
                _verticalLockScrollViewStandby = NO;
            } else {
                //scroll view is hit to the vertical edge
                _verticalLockScrollViewStandby = YES;
                //check if the scroll view hits top or bottom edge
                _isLockStandbyOnTop = self.bounds.origin.y + secondaryScrollViewoffsetDifference.y <= contentView.frame.origin.y;
                
                if (_isLockStandbyOnTop) {
                    _lockStandbySecondaryScrollViewContentOffset.y = scrollView.contentOffset.y + (fabs(secondaryScrollViewoffsetDifference.y) - fabs(viewToUpdateBounds.origin.y - contentView.frame.origin.y));
                    viewToUpdateBounds.origin.y = contentView.frame.origin.y;
                    
                } else {
                    _lockStandbySecondaryScrollViewContentOffset.y = scrollView.contentOffset.y - (fabs(secondaryScrollViewoffsetDifference.y) - fabs(contentView.frame.origin.y + contentView.frame.size.height - viewToUpdateBounds.size.height - viewToUpdateBounds.origin.y));
                    viewToUpdateBounds.origin.y = contentView.frame.origin.y + contentView.frame.size.height - viewToUpdateBounds.size.height;
                }
            }
        }
        
        self.bounds = viewToUpdateBounds;
    }

}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return [self.primaryScrollViewDelegate viewForZoomingInScrollView:scrollView];
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if (scrollView == self) {
        [self.primaryScrollViewDelegate scrollViewDidZoom:scrollView];
    } 
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    if (scrollView == self) {
        [self.primaryScrollViewDelegate scrollViewWillBeginZooming:scrollView withView:view];
        
        if (self.lockScrolling) {
            _isZoomByPrimaryScrollView = NO;
            self.scrollEnabled = NO;
            [self.secondaryScrollViewDelegate scrollViewWillBeginZooming:scrollView withView:view];
        }
    } else {
        _isZoomByPrimaryScrollView = YES;
        self.scrollEnabled = NO;

        _prevScale = scrollView.zoomScale;
        
        [self.primaryScrollViewDelegate scrollViewWillBeginZooming:scrollView withView:view];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if (scrollView == self) {
        [self.primaryScrollViewDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];

        if (self.lockScrolling) {
            _isZoomByPrimaryScrollView = NO;
            self.scrollEnabled = YES;
            [self.secondaryScrollViewDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
        }

    } else {
        _isZoomByPrimaryScrollView = NO;
        self.scrollEnabled = YES;
        
        [self.primaryScrollViewDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self) {
        if (self.lockScrolling) {
            _isDragByPrimaryScrollView = NO;
            
            [self.secondaryScrollViewDelegate scrollViewWillBeginDragging:scrollView];
        }
    } else {
        _isDragByPrimaryScrollView = YES;
        
        _previousSecondaryScrollViewContentOffset = scrollView.contentOffset;
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (scrollView == self && self.lockScrolling) {
        *targetContentOffset = scrollView.contentOffset;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView == self) {
        if (self.lockScrolling) {
            _isDragByPrimaryScrollView = NO;
            [self.secondaryScrollViewDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
        }
    } else {
        _isDragByPrimaryScrollView = NO;
    }
}

@end
