//
//  CZImagePageViewController.m
//  Hermes
//
//  Created by Ralph Jin on 6/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImagePageViewController.h"
#import <ImageIO/ImageIO.h>

#import "MBProgressHUD+Hide.h"
#import "CZToastMessage.h"
#import <CZAnnotationKit/CALayer+ReplaceWithSameName.h>
#import <CZAnnotationKit/CZElementLayer.h>
#import <CZAnnotationKit/CZElementLayerRenderer.h>
#import <CZAnnotationKit/CZAnnotationGestureRecognizer.h>
#import <CZAnnotationKit/CZLaserPointerGestureRecognizer.h>
#import <CZIKit/CZIKit.h>
#import <CZFileKit/CZFileKit.h>
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZMagnifierView.h"
#import "CZScrollView.h"
#import "CZFileEntity+Icon.h"

typedef NS_ENUM(NSUInteger, CZImagePageViewPinchState) {
    CZImagePageViewPinchStateUnknown,
    CZImagePageViewPinchStateZooming,
    CZImagePageViewPinchStateDismiss
};

static const float kImageEditingEdgeRatio = 0.25;
static NSString *const kGrainsMeasurementLayerName = @"grainsMeasurementLayer";

static dispatch_queue_t image_document_loading_queue() {
    static dispatch_queue_t cz_image_document_loading_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cz_image_document_loading_queue = dispatch_queue_create("com.zeisscn.image-document.loading", DISPATCH_QUEUE_SERIAL);
    });
    
    return cz_image_document_loading_queue;
}

CF_RETURNS_RETAINED static inline CZRegion2D * createRegionFromChords(CZScanLine *chords, uint32_t phaseIndex) {
    uint32_t regionClassID = [CZDocManager regionsIDFromPhaseID:phaseIndex];
    CZRegion2D *region = [[CZRegion2D alloc] initWithScanLines:chords classID:regionClassID];
    return region;
}

static inline void collectRegion(NSMutableArray *regionsCollection, NSMutableArray *regionIntancesCollection, CZScanLine *chords, CZRegion2D *region, NSUInteger phaseIndex) {
    NSMutableArray *regions = regionsCollection[phaseIndex];
    
    [regions addObject:region];
    
    CZRegion2DInstanceItem *regionInstanceItem = [[CZRegion2DInstanceItem alloc] initWith:region.idx regionClassID:region.classID];
    regionInstanceItem.left = chords.minX;
    regionInstanceItem.top = chords.minY;
    regionInstanceItem.width = chords.maxX - chords.minX;
    regionInstanceItem.height = chords.maxY - chords.minY;
    
    NSMutableArray *regionIntances = regionIntancesCollection[phaseIndex];
    [regionIntances addObject:regionInstanceItem];
    [regionInstanceItem release];
}

static inline BOOL collectChords(NSMutableArray *regionsCollection, NSMutableArray *regionIntancesCollection, CZScanLine *chords, uint32_t phaseIndex) {
    CZRegion2D *region = createRegionFromChords(chords, phaseIndex);
    if (region == nil) {
        //        CZLogv(@"failed to convert CZGemChords to CZRegion2D");
        return NO;
    }
    
    NSMutableArray *regions = regionsCollection[phaseIndex];
    
    [regions addObject:region];
    
    CZRegion2DInstanceItem *regionInstanceItem = [[CZRegion2DInstanceItem alloc] initWith:region.idx regionClassID:region.classID];
    regionInstanceItem.left = chords.minX;
    regionInstanceItem.top = chords.minY;
    regionInstanceItem.width = chords.maxX - chords.minX;
    regionInstanceItem.height = chords.maxY - chords.minY;
    
    NSMutableArray *regionIntances = regionIntancesCollection[phaseIndex];
    [regionIntances addObject:regionInstanceItem];
    
    [regionInstanceItem release];
    [region release];
    
    return YES;
}

@interface CZImagePageViewController () <UIGestureRecognizerDelegate,
CZAnnotationGestureRecognizerDelegate,
CZDocManagerDelegate,
CZImageToolDelegate,
CZElementLayerDelegate> {
@private
    CZImagePageViewPinchState _pinchState;
}

@property (nonatomic, assign) CZScrollView *scrollView;  // alias of self.view
@property (nonatomic, retain) UIView *imageBorderView;
@property (nonatomic, assign) UIView *imageView;
@property (nonatomic, retain) UIImageView *thumbnailView;
@property (nonatomic, retain, readonly) UILabel *unsupportLabel;
@property (nonatomic, assign) CGFloat fitScale;

@property (atomic, assign, getter = isLoadingCancelled) BOOL loadingCancelled;
@property (atomic, assign, getter = isLoadingImage) BOOL loadingImage;
@property (atomic, assign, getter = isLoadingThumbnail) BOOL loadingThumbnail;

// tools
@property (nonatomic, retain, readwrite) CZImageTool *imageTool;
@property (nonatomic, retain) CZAnnotationGestureRecognizer *annotationRecognizer;
@property (nonatomic, retain) CZLaserPointerGestureRecognizer *laserPointerRecongnizer;
@property (nonatomic, retain) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, retain) UITapGestureRecognizer *doubleTapRecognizer;

@property (nonatomic, retain) MBProgressHUD *progressHUD;

@end

@implementation CZImagePageViewController

@synthesize unsupportLabel = _unsupportLabel;

static NSUInteger sCZImagePageViewControllerCount = 0U;

- (void)dealloc {
    sCZImagePageViewControllerCount --;
    CZLogv(@"CZImagePageViewController live objects count = %lu", (unsigned long)sCZImagePageViewControllerCount);
    
    self.docManager = nil;
    _scrollView.primaryScrollViewDelegate = nil;
    
    [_thumbnailView release];
    [_imageBorderView release];
    [_unsupportLabel release];
    
    [_imageTool release];
    [_annotationRecognizer release];
    [_laserPointerRecongnizer release];
    [_tapRecognizer release];
    [_doubleTapRecognizer release];
    
    [_filePath release];
    [_progressHUD release];
    
    [super dealloc];
}

- (void)loadView {
    sCZImagePageViewControllerCount ++;
    CZLogv(@"CZImagePageViewController live objects count = %lu", (unsigned long)sCZImagePageViewControllerCount);
    
    CGRect frame = self.parentViewController.view.frame;
    frame.origin = CGPointZero;
    CZScrollView *scrollView = [[CZScrollView alloc] initWithFrame:frame];
    scrollView.exclusiveTouch = YES;
    scrollView.primaryScrollViewDelegate = self;
    self.view = scrollView;
    //scrollView is assign property, no need to release
    self.scrollView = scrollView;
    [scrollView release];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // install gesture recognizers
    CZAnnotationGestureRecognizer *annotationRecognizer = [[CZAnnotationGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewHandleTwoFingerTouches:)];
    annotationRecognizer.annotationDelegate = self;
    [self.scrollView addGestureRecognizer:annotationRecognizer];
    self.annotationRecognizer = annotationRecognizer;
    [annotationRecognizer release];
    
    // install laser pointer gesture
    CZLaserPointerGestureRecognizer *laserPointerRecognizer = [[CZLaserPointerGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewHandleFingerTouchMove:)];
    laserPointerRecognizer.delegate = self;
    [self.scrollView addGestureRecognizer:laserPointerRecognizer];
    
    self.laserPointerRecongnizer = laserPointerRecognizer;
    [laserPointerRecognizer release];
    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    doubleTapRecognizer.delegate = self;
    [self.scrollView addGestureRecognizer:doubleTapRecognizer];
    self.doubleTapRecognizer = doubleTapRecognizer;
    [doubleTapRecognizer release];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [tapRecognizer requireGestureRecognizerToFail:doubleTapRecognizer];
    [self.scrollView addGestureRecognizer:tapRecognizer];
    self.tapRecognizer = tapRecognizer;
    [tapRecognizer release];
    
    BOOL imageBorderViewExist = (self.imageBorderView != nil);
    
    [self verifyToCreateImageBorderView];
    
    if (!imageBorderViewExist) {
        CGRect rect = self.scrollView.frame;
        rect.origin = CGPointZero;
        _imageBorderView.frame = rect;
    }
    
    [self.scrollView addSubview:_imageBorderView];
    [self fitImageViewIntoScrollView:YES];
    
    if (_thumbnailView) {
        [self.scrollView addSubview:_thumbnailView];
        _thumbnailView.frame = self.scrollView.bounds;
    }
    
    UILabel *unsupportLabel = self.unsupportLabel;
    const CGSize fitSize = unsupportLabel.frame.size;
    [self.imageBorderView addSubview:unsupportLabel];
    
    unsupportLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.imageBorderView addConstraint:[NSLayoutConstraint constraintWithItem:unsupportLabel
                                                                     attribute:NSLayoutAttributeCenterX
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.imageBorderView
                                                                     attribute:NSLayoutAttributeCenterX
                                                                    multiplier:1.0f
                                                                      constant:0]];
    
    [self.imageBorderView addConstraint:[NSLayoutConstraint constraintWithItem:unsupportLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.imageBorderView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0]];
    
    [self.unsupportLabel addConstraint:[NSLayoutConstraint constraintWithItem:unsupportLabel
                                                                    attribute:NSLayoutAttributeWidth
                                                                    relatedBy:NSLayoutRelationLessThanOrEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1.0f
                                                                     constant:300]];
    
    [self.unsupportLabel addConstraint:[NSLayoutConstraint constraintWithItem:unsupportLabel
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                       toItem:nil
                                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                                   multiplier:1.0f
                                                                     constant:fitSize.height]];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self centerScrollViewContents];
}

- (UILabel *)unsupportLabel {
    if (_unsupportLabel) {
        return _unsupportLabel;
    }
    
    UILabel *unsupportLabel = [[UILabel alloc] init];
    unsupportLabel.textAlignment = NSTextAlignmentCenter;
    unsupportLabel.alpha = 0.5;
    unsupportLabel.backgroundColor = [UIColor clearColor];
    unsupportLabel.textColor = kDefaultLabelColor;
    unsupportLabel.shadowColor = kDefaultLabelShadowColor;
    unsupportLabel.shadowOffset = CGSizeMake(1.0f, 2.0f);
    unsupportLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    unsupportLabel.text = L(@"ERROR_FILE_OPEN_IMAGE");
    unsupportLabel.lineBreakMode = NSLineBreakByWordWrapping;
    unsupportLabel.numberOfLines = 10;
    CGSize fitSize = [unsupportLabel sizeThatFits:CGSizeMake(300, 24 * 10)];
    unsupportLabel.frame = CGRectMake(0, 0, fitSize.width, fitSize.height);
    unsupportLabel.center = self.imageBorderView.center;
    unsupportLabel.hidden = YES;
    _unsupportLabel = [unsupportLabel retain];
    [unsupportLabel release];
    
    return _unsupportLabel;
}

- (void)setEditingMode:(CZEditingMode)editingMode {
    _editingMode = editingMode;
    if (_editingMode == CZEditingModePCB) {
        self.annotationRecognizer.multiTouchEnable = YES;
    } else {
        self.annotationRecognizer.multiTouchEnable = NO;
    }
}

- (void)setThumbnailImage:(UIImage *)thumbnailImage originalImageSize:(CGSize)originalImageSize {
    if (self.thumbnailView) {
        [self.thumbnailView removeFromSuperview];
        self.thumbnailView = nil;
    }
    
    if (thumbnailImage) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:thumbnailImage];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        CGRect rect = _scrollView.frame;
        if (rect.size.width < originalImageSize.width || rect.size.height < originalImageSize.height) {
            rect.origin = CGPointZero;
            imageView.frame = rect;
            [_scrollView addSubview:imageView];
        } else {
            imageView.frame = CGRectMake((rect.size.width - originalImageSize.width) / 2,
                                         (rect.size.height - originalImageSize.height) / 2,
                                         originalImageSize.width,
                                         originalImageSize.height);
        }
        
        self.thumbnailView = imageView;
        [imageView release];
        
        if (_scrollView) {
            [_scrollView addSubview:imageView];
        }
    }
    
    [self updateUnsupportedLabel];
}

- (void)loadImage {
    if (self.isImageLoaded) {
        self.imageTool.overExposureEnabled = self.overExposeEnableAfterNewImageLoaded;
        [self.imageTool applyDisplayCurvePreset:self.currentDisplayCurve];
        CZLogv(@"%@ is already loaded", [self.filePath lastPathComponent]);
        return;
    }
    
    if (self.isLoadingImage) {
        self.loadingCancelled = NO;
        CZLogv(@"%@ is already loading.", [self.filePath lastPathComponent]);
        return;
    }
    
    self.loadingImage = YES;
    self.imageLoaded = NO;
    self.loadingCancelled = NO;
    
    // hide image view before loading is finished.
    self.imageTool.imageView.hidden = YES;
    
    id delegate = self.delegate;
    
    // Load image in a another thread for image swiping performance.
    dispatch_async(image_document_loading_queue(), ^() {
        if (self.loadingCancelled) {
            CZLogv(@"CZImagePageViewController cancel loading image: %@, after thumbnail loaded", [self.filePath lastPathComponent]);
            self.loadingImage = NO;
            return;
        }
        
        CZDocManager *newDocManager = [CZDocManager newDocManagerFromFilePath:self.filePath];
        
        if (self.loadingCancelled) {
            CZLogv(@"CZImagePageViewController cancel loading image: %@, after docManager loaded", [self.filePath lastPathComponent]);
            
            [newDocManager release];
            self.loadingImage = NO;
            return;
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^ {
            [self setDocManager:newDocManager];
            self.imageTool.imageView.hidden = YES;
            self.imageTool.overExposureEnabled = self.overExposeEnableAfterNewImageLoaded;
            [self.imageTool applyDisplayCurvePreset:self.currentDisplayCurve];
        });
        
        [newDocManager release];
        
        // wait gpu image view 0.1 second to finish renderering; TODO: integrate with new API of GPUImage
        for (int i = 0; i < 10; i++) {
            if (self.loadingCancelled) {
                CZLogv(@"CZImagePageViewController cancel loading image: %@, after apply display curve.", [self.filePath lastPathComponent]);
                self.loadingImage = NO;
                return;
            }
            usleep(1e4);
        }
        
        self.imageLoaded = YES;
        self.loadingImage = NO;
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            if (self.thumbnailView) {
                [self.scrollView insertSubview:self.thumbnailView aboveSubview:self.imageTool.imageView];
            }
            self.imageTool.imageView.hidden = NO;
            
            [UIView animateWithDuration:0.3 animations:^{
                self.thumbnailView.alpha = 0;
            } completion:^(BOOL completed) {
                [self setThumbnailImage:nil originalImageSize:CGSizeZero];
                if ([delegate respondsToSelector:@selector(imagePageViewControllerDidFinishLoading:)]) {
                    [delegate imagePageViewControllerDidFinishLoading:self];
                }
            }];
        });
    });
}

- (void)loadThumbnail {
    if (self.isLoadingThumbnail) {
        self.loadingCancelled = NO;
        return;
    }
    
    self.loadingThumbnail = YES;
    self.loadingCancelled = NO;
    
    // hide image view before loading is finished.
    self.imageTool.imageView.hidden = YES;
    
    dispatch_async(image_document_loading_queue(), ^() {
        if (self.loadingCancelled) {
            CZLogv(@"CZImagePageViewController cancel loading image: %@, at beginning", [self.filePath lastPathComponent]);
            self.loadingThumbnail = NO;
            return;
        }
        
        CGSize originalImageSize = CGSizeZero;
        
        // load thumbnail
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:self.filePath.pathExtension];
        UIImage *thumbnail = nil;
        if (fileFormat == kCZFileFormatJPEG) {
            thumbnail = [[UIImage alloc] initThumbWithEXIFOfJPEGFile:self.filePath];
        } else if (fileFormat == kCZFileFormatTIF) {
            thumbnail = [[UIImage alloc] initThumbWithTiffFile:self.filePath];
        }
        if (fileFormat == kCZFileFormatCZI) {
            CZIDocument *czi = [[CZIDocument alloc] initWithFile:self.filePath];
            thumbnail = [[UIImage alloc] initWithData:[czi thumbnail]];
            originalImageSize = [czi imageSize];
            [czi release];
        } else if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
            originalImageSize = [CZCommonUtils imageSizeFromFilePath:self.filePath];
            
            if (thumbnail == nil) {  // scale to generate thumbnail
                CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:self.filePath], NULL);
                if (imageSource) {
                    NSDictionary *options = @{(id)kCGImageSourceCreateThumbnailWithTransform: @YES,
                                              (id)kCGImageSourceCreateThumbnailFromImageIfAbsent: @YES,
                                              (id)kCGImageSourceThumbnailMaxPixelSize: @160};
                    
                    CGImageRef imageRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (CFDictionaryRef)options);
                    CFRelease(imageSource);
                    
                    if (imageRef) {
                        thumbnail = [[UIImage alloc] initWithCGImage:imageRef];
                        CGImageRelease(imageRef);
                    }
                }
            }
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^ {
            UIImage *thumbnailImage = [thumbnail retain];
            
            if (thumbnailImage == nil) {
                CZLocalFileEntity *fileEntity = [[CZLocalFileEntity alloc] initWithFilePath:self.filePath];
                thumbnailImage = [[UIImage cz_imageWithIcon:fileEntity.icon] retain];
                [fileEntity release];
            }
            
            [self setThumbnailImage:thumbnailImage originalImageSize:originalImageSize];
            [thumbnailImage release];
        });
        
        [thumbnail release];
        
        self.loadingThumbnail = NO;
    });
}

- (void)invalidateMultiphaseAreaAndUpdateAnalyzerResult:(BOOL)updatesAnalyzerResult
                                        completionBlock:(void (^)())completionBlock {
    if (!_needInvalidateMultiphaseOverlayLayer) {
        [self.imageTool invalidate];
        return;
    }
    
    if (![self.docManager.multiphase updatePhaseAreas]) {
        [self.docManager.multiphase setProcessingImage:[self.docManager.document outputImage]];
        [self.docManager.multiphase updatePhaseAreas];
    };
    
    BOOL shouldUpdateWithGPU = [CZImageTool multiphaseShouldUpdateWithGPU:self.docManager.multiphase];
    if (!shouldUpdateWithGPU || updatesAnalyzerResult) {
        if (self.docManager.multiphase.isParticlesMode) {
            [self showProcessIndicatorWithText:L(@"BUSY_INDICATOR_PARTICLES")];
        } else {
            [self showProcessIndicatorWithText:L(@"BUSY_INDICATOR_MULTIPHASE")];
        }
    } else {
        [self.imageTool setMultiphaseLayerContentsWithImage:nil];
        [self.imageTool invalidate];
        
        if (completionBlock) {
            completionBlock();
        }
        
        return;
    }
    
    UIImage *workingImage = self.imageTool.workingImage;
    CZDocManager *workingDocManager = self.docManager;
    CZImageTool *imageTool = self.imageTool;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        @autoreleasepool {
            if (updatesAnalyzerResult) {
                workingDocManager.analyzer2dResult = nil;
                workingDocManager.analyzer2dData = nil;
            }
            
            CZParticleCriteria *particleCriteria = workingDocManager.multiphase.particleCriteria;
            
            NSUInteger phaseCount = [workingDocManager.multiphase phaseCount];
            __block NSMutableArray *regionCollection = nil;
            __block NSMutableArray *regionInstanceCollection = nil;
            
            if (updatesAnalyzerResult) {
                regionCollection = [NSMutableArray arrayWithCapacity:phaseCount];
                regionInstanceCollection = [NSMutableArray arrayWithCapacity:phaseCount];
                for (NSUInteger phaseIndex = 0; phaseIndex < phaseCount; phaseIndex++) {
                    [regionCollection addObject:[NSMutableArray array]];
                    [regionInstanceCollection addObject:[NSMutableArray array]];
                }
            }
            
            ImageWithScanLineCallback block = NULL;
            if (updatesAnalyzerResult || particleCriteria) {
                if (particleCriteria.filters == nil) {  // no filters
                    block = ^(CZScanLine *chords, uint32_t phaseIndex, NSUInteger chordsArea) {
                        @autoreleasepool {
                            return collectChords(regionCollection, regionInstanceCollection, chords, phaseIndex);
                        }
                    };
                } else {
                    BOOL areaOnly = NO;
                    if (particleCriteria.filters.count == 2) {
                        areaOnly = [particleCriteria hasFilterForKey:@"area"];
                    }
                    
                    if (areaOnly) {
                        double minAreaThreshold = [particleCriteria filterMinValueForKey:@"area"];
                        double maxAreaThreshold = [particleCriteria filterMaxValueForKey:@"area"];
                        block = ^(CZScanLine *chords, uint32_t phaseIndex, NSUInteger chordsArea) {
                            @autoreleasepool {
                                if (chordsArea >= minAreaThreshold && chordsArea <= maxAreaThreshold) {
                                    return collectChords(regionCollection, regionInstanceCollection, chords, phaseIndex);
                                } else {
                                    return NO;
                                }
                            }
                        };
                    } else {
                        block = ^(CZScanLine *chords, uint32_t phaseIndex, NSUInteger chordsArea) {
                            @autoreleasepool {
                                CZRegion2D *region = createRegionFromChords(chords, phaseIndex);
                                [region autorelease];
                                if ([particleCriteria isValidRegion:region]) {
                                    collectRegion(regionCollection, regionInstanceCollection, chords, region, phaseIndex);
                                    return YES;
                                } else {
                                    return NO;
                                }
                            }
                        };
                    }
                }
            }
            
            UIImage *multiphaseImage = [workingImage imageWithMultiphase:workingDocManager.multiphase
                                                       scanLinesCallback:block];
            
            if (updatesAnalyzerResult) {
                CZAnalyzer2dResult *analyzer2dResult = [[CZAnalyzer2dResult alloc] init];
                for (NSUInteger phaseIndex = 0; phaseIndex < phaseCount; phaseIndex++) {
                    [analyzer2dResult addRegions:(NSArray *)regionCollection[phaseIndex]
                                 regionInstances:(NSArray *)regionInstanceCollection[phaseIndex]
                                         atIndex:phaseIndex];
                }
                workingDocManager.analyzer2dResult = analyzer2dResult;
                [analyzer2dResult release];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self hideProcessIndicator];
                
                [imageTool setMultiphaseLayerContentsWithImage:multiphaseImage];
                
                [imageTool invalidate];
                
                if (completionBlock) {
                    completionBlock();
                }
            });
        }
    });
}

- (void)invalidateMultiphaseAreaFromAnalyzerResult {
    if (!_needInvalidateMultiphaseOverlayLayer) {
        [self.imageTool invalidate];
        return;
    }
    
    if ([self.docManager.multiphase phaseCount] > 0) {
        if (self.docManager.multiphase.isParticlesMode) {
            [self showProcessIndicatorWithText:L(@"BUSY_INDICATOR_PARTICLES")];
        } else {
            [self showProcessIndicatorWithText:L(@"BUSY_INDICATOR_MULTIPHASE")];
        }
    } else {
        [self.imageTool setMultiphaseLayerContentsWithImage:nil];
        [self.imageTool invalidate];
        return;
    }
    
    CZDocManager *workingDocManager = self.docManager;
    CZImageTool *imageTool = self.imageTool;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        UIGraphicsBeginImageContext(workingDocManager.document.imageSize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        [workingDocManager drawMultiphaseRegionsOnContext:context];
        
        UIImage *multiphaseImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self hideProcessIndicator];
            
            [imageTool setMultiphaseLayerContentsWithImage:multiphaseImage];
            [imageTool invalidate];
        });
    });
}

- (void)invalidateMultiphaseArea {
    if (self.docManager.analyzer2dResult == nil) {
        [self invalidateMultiphaseAreaAndUpdateAnalyzerResult:NO completionBlock:NULL];
    } else {
        [self invalidateMultiphaseAreaFromAnalyzerResult];
    }
}

- (void)showProcessIndicatorWithText:(NSString *)text {
    @synchronized (self) {
        if (self.progressHUD == nil) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
            [hud setColor:kSpinnerBackgroundColor];
            [hud setLabelText:text];
            self.progressHUD = hud;
        }
    }
}

- (void)hideProcessIndicator {
    @synchronized (self) {
        if (self.progressHUD) {
            [self.progressHUD dismiss:YES];
            self.progressHUD = nil;
        }
    }
}

- (void)cancelLoadingImage {
    self.loadingCancelled = YES;
    self.imageLoaded = NO;
    if (self.docManager.filePath) {
        self.filePath = self.docManager.filePath;  // remember file path for next loading
    }
    
    if (!self.docManager.isModified && !self.docManager.isImageNew) {  // clear memory if docManager do not require save.
        self.docManager = nil;
    }
}

- (void)setDocManager:(CZDocManager *)docManager {
    if (docManager == _docManager) {
        [self updateUnsupportedLabel];
        return;
    }
    
    if (_docManager) {
        _docManager.elementLayer.delegate = nil;
        _imageTool.delegate = nil;
        
        [self.imageView removeFromSuperview];
        [self.imageBorderView removeFromSuperview];
        
        self.imageView = nil;
        self.imageBorderView = nil;
        self.imageTool = nil;
        
        [_docManager removeObserver:self forKeyPaths:@"imageModified", @"elementLayerModified", @"multichannelModified", @"multiphaseModified", @"grainSizeModified", nil];
        [_docManager release];
        _docManager = nil;
    }
    
    if (docManager) {
        _docManager = [docManager retain];
        _docManager.delegate = self.delegate;
        self.filePath = _docManager.filePath;
        
        [_docManager addObserver:self forKeyPaths:@"imageModified", @"elementLayerModified", @"multichannelModified", @"multiphaseModified", @"grainSizeModified", nil];
        _docManager.elementLayer.delegate = self;
        
        _imageTool = [[CZImageTool alloc] initWithDocManager:_docManager];
        _imageTool.delegate = self;
        
        _imageView = [_imageTool imageView];
        _imageView.exclusiveTouch = YES;
        CGSize imageSize = _docManager.document.imageSize;
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        CGRect frame = CGRectMake(0, 0, imageSize.width / screenScale, imageSize.height / screenScale);
        
        _needInvalidateMultiphaseOverlayLayer = YES;
        
        [self verifyToCreateImageBorderView];
        self.imageBorderView.frame = frame;
        self.imageBorderView.bounds = frame;
        [self.imageBorderView addSubview:_imageView];
        
        if (_scrollView) {
            [_scrollView addSubview:self.imageBorderView];
            [self fitImageViewIntoScrollView:YES];
        }
        
        _docManager.elementLayer.scaling = _docManager.calibratedScaling;  // reset scaling and update all the CALayer in image view
    }
    
    [self updateUnsupportedLabel];
}

- (void)setLockScrolling:(BOOL)lockScrolling {
    self.scrollView.lockScrolling = lockScrolling;
}

- (void)assignSecondaryScrollViewDelegate:(CZImagePageViewController *)secondaryPageViewController {
    if (!secondaryPageViewController) {
        self.scrollView.secondaryScrollViewDelegate = nil;
        return;
    }
    self.scrollView.secondaryScrollViewDelegate = secondaryPageViewController.scrollView;
}

- (void)beginEditingAnnotation {
    [_imageTool dismissInteractiveViews];
    
    // save visible content rect
    CGRect visibleContentRect = [self calcVisibleContentRectOfScrollView];
    
    // calculate image size in point
    CGSize imageSize = self.docManager.document.imageSize;
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    imageSize.width /= screenScale;
    imageSize.height /= screenScale;
    
    // calculate annotation editing border
    CGSize borderSize;
    borderSize.width = floor(imageSize.width * kImageEditingEdgeRatio);
    borderSize.height = floor(imageSize.height * kImageEditingEdgeRatio);
    
    // add editing edge
    CGFloat saveZoomScale = self.scrollView.zoomScale;
    self.scrollView.zoomScale = 1.0;
    CGRect frame = CGRectMake(0, 0, imageSize.width + borderSize.width * 2, imageSize.height + borderSize.height * 2);
    self.imageBorderView.frame = frame;
    self.imageBorderView.bounds = frame;
    self.imageView.center = CGPointMake(frame.size.width / 2, frame.size.height / 2);
    self.scrollView.zoomScale = saveZoomScale;
    
    // add editing edge
    visibleContentRect.origin.x += borderSize.width * saveZoomScale;
    visibleContentRect.origin.y += borderSize.height * saveZoomScale;
    [self.scrollView scrollRectToVisible:visibleContentRect animated:NO];
    
    [self centerScrollViewContents];
}

- (void)endEditingAnnotation {
    CGRect visibleContentRect = [self calcVisibleContentRectOfScrollView];
    
    // calculate image size in point
    CGSize imageSize = self.docManager.document.imageSize;
    CGFloat scale = [[UIScreen mainScreen] scale];
    imageSize.width /= scale;
    imageSize.height /= scale;
    
    // calculate annotation editing border
    CGSize borderSize;
    borderSize.width = floor(imageSize.width * kImageEditingEdgeRatio);
    borderSize.height = floor(imageSize.height * kImageEditingEdgeRatio);
    
    // remove the editing edge by setting same frame
    CGFloat saveZoomScale = self.scrollView.zoomScale;
    self.scrollView.zoomScale = 1.0;
    CGRect rect = CGRectMake(0, 0, imageSize.width, imageSize.height);
    self.imageBorderView.frame = rect;
    self.imageBorderView.bounds = rect;
    self.imageView.center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
    self.scrollView.zoomScale = saveZoomScale;
    
    visibleContentRect.origin.x -= borderSize.width * saveZoomScale;
    visibleContentRect.origin.y -= borderSize.height * saveZoomScale;
    [self.scrollView scrollRectToVisible:visibleContentRect animated:NO];
    
    [self centerScrollViewContents];
}

- (void)invalidateAllAnnotations {
    CZElementLayer *elementLayer = self.docManager.elementLayer;
    if (elementLayer) {
        NSUInteger count = [elementLayer elementCount];
        for (NSUInteger i = 0; i < count; i++) {
            CZElement *element = [elementLayer elementAtIndex:i];
            element.featureValidated = NO;
        }
        
        [self redrawElementLayer:elementLayer];
    }
    
    self.docManager.roiRegion.featureValidated = NO;
    
    [self.imageTool refreshROIRegion];
    [self.imageTool showROISizePanelWithMode:self.imageTool.croppingMode];
}

- (void)hideAllAnnotations {
    CZElementLayer *elementLayer = self.docManager.elementLayer;
    if (elementLayer) {
        NSUInteger count = [elementLayer elementCount];
        if (count) {
            for (NSUInteger i = 0; i < count; i++) {
                CZElement *element = [elementLayer elementAtIndex:i];
                
                CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
                [renderer updateAllElements:elementLayer];
                [renderer hideElementOnly:element inElementLayer:elementLayer];
                [renderer release];
            }
        }
    }
}

- (void)showAllAnnotations {
    CZElementLayer *elementLayer = self.docManager.elementLayer;
    if (elementLayer) {
        NSUInteger count = [elementLayer elementCount];
        if (count) {
            CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
            [renderer updateAllElements:elementLayer];
            [renderer release];
        }
    }
}

- (void)undoAction {
    [_imageTool dismissInteractiveViews];
    
    if ([_docManager canUndo]) {
        [_docManager undo];
    }
}

- (void)redoAction {
    [_imageTool dismissInteractiveViews];
    
    if ([_docManager canRedo]) {
        [_docManager redo];
    }
}

- (void)scrollRectToVisible:(CGRect)rect {
    rect = [self.imageView convertRect:rect toView:self.imageBorderView];
    
    CGAffineTransform t = CGAffineTransformMakeScale(self.scrollView.zoomScale, self.scrollView.zoomScale);
    rect = CGRectApplyAffineTransform(rect, t);
    
    [self.scrollView scrollRectToVisible:rect animated:YES];
}

#pragma mark - Private methods

- (void)updateUnsupportedLabel {
    if (_docManager == nil) {
        self.unsupportLabel.hidden = !self.isImageLoaded;
    } else {
        self.unsupportLabel.hidden = YES;
    }
}

- (CGRect)calcVisibleContentRectOfScrollView {
    CGRect visibleRect = self.scrollView.bounds;
    CGRect visibleImageRect = [self.scrollView convertRect:visibleRect toView:self.imageBorderView];
    CGAffineTransform t = CGAffineTransformMakeScale(_scrollView.zoomScale, _scrollView.zoomScale);
    CGRect visibleContentRect = CGRectApplyAffineTransform(visibleImageRect, t);
    return visibleContentRect;
}

- (void)centerScrollViewContents {
    // Reset scroll view content size and image view position
    CGRect contentsFrame;
    contentsFrame = self.imageBorderView.frame;
    
    CGSize boundsSize = self.scrollView.bounds.size;
    
    // Calculate natural edge, which center the image frame
    CGSize naturalEdge = CGSizeZero;
    if (contentsFrame.size.width < boundsSize.width) {
        naturalEdge.width = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        naturalEdge.height = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    }
    
    contentsFrame.origin.x = naturalEdge.width;
    contentsFrame.origin.y = naturalEdge.height;
    
    self.imageBorderView.frame = contentsFrame;
}

- (BOOL)calcZoomScaleForScrollView {
    // 16x of original size, referring to practice in Photoshop
    const CGFloat kMaxZoomScale = 16.0f;
    
    if (self.docManager.document == nil) {
        return NO;
    }
    
    CGSize imageViewSize = self.docManager.document.imageSize;
    if (imageViewSize.width <= 0 || imageViewSize.height <= 0) {
        CZLogv(@"Invalid image size in calcZoomScaleForScrollView");
        return NO;
    }
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    imageViewSize.width /= screenScale;
    imageViewSize.height /= screenScale;
    
    // Set up the minimum & maximum zoom scales
    CGSize scrollViewSize;
    if (self.parentViewController) {
        scrollViewSize = self.parentViewController.view.bounds.size;
    } else {
        scrollViewSize = _scrollView.bounds.size;
    }
    if (scrollViewSize.width <= 0 || scrollViewSize.height <= 0) {
        CZLogv(@"Invalid scroll view size in calcZoomScaleForScrollView");
        return NO;
    }
    
    CGFloat scaleWidth = scrollViewSize.width / imageViewSize.width;
    CGFloat scaleHeight = scrollViewSize.height / imageViewSize.height;
    _fitScale = MIN(scaleWidth, scaleHeight);
    
    _scrollView.minimumZoomScale = MIN(screenScale, _fitScale);
    _scrollView.minimumZoomScaleBeforeLocking = _scrollView.minimumZoomScale;
    _scrollView.maximumZoomScale = MAX(kMaxZoomScale * screenScale, _fitScale);
    _scrollView.maximumZoomScaleBeforeLocking = _scrollView.maximumZoomScale;
    
    return YES;
}

- (CGFloat)zoomScale {
    return self.scrollView.zoomScale;
}

- (void)fitImageViewIntoScrollView:(BOOL)resetZoom {
    if (![self calcZoomScaleForScrollView]) {
        return;
    }
    
    if (resetZoom) {
        [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:NO];
    } else {
        CGFloat scale = _scrollView.zoomScale;
        if (scale < _scrollView.minimumZoomScale) {
            [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:NO];
        } else if (scale > _scrollView.maximumZoomScale) {
            [_scrollView setZoomScale:_scrollView.maximumZoomScale animated:NO];
        }
    }

    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:didChangeToZoom:)]) {
        [self.imageEditingDelegate imageView:self.imageView didChangeToZoom:_scrollView.zoomScale];
    }
    
    [self updateGrainSizeLayer];
    
    [self centerScrollViewContents];
}

- (void)verifyToCreateImageBorderView {
    if (self.imageBorderView) {
        return;
    }
    
    UIView *imageBorderView = [[UIView alloc] init];
    imageBorderView.exclusiveTouch = YES;
    self.imageBorderView = imageBorderView;
    [imageBorderView release];
}

- (void)redrawElementLayer:(CZElementLayer *)layer {
    CZElement *temporarilyHiddenElement = nil;
    
    if ([self.imageEditingDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.imageEditingDelegate temporarilyHiddenElement];
    }
    
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer updateAllElements:layer];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if (_magnifyView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_magnifyView.viewToMagnify];
        [renderer updateAllElements:layer];
        [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
        [renderer release];
    }
}

- (void)updateGrainSizeLayer {
    BOOL grainSizeLayerEanbled = self.imageTool ? self.imageTool.multiphaseEnabled : NO;
    if (_docManager.grainSizeResult && grainSizeLayerEanbled) {
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        CGFloat baseLineWidth = [self.docManager.elementLayer suggestLineWidthForSize:CZElementSizeMedium];
        CGFloat imageViewScale = [self zoomScale];
        if (!isnormal(imageViewScale)) {
            imageViewScale = 1;
        }
        CGFloat lineWidth = baseLineWidth / imageViewScale * screenScale;
        
        CGSize imageSize = self.docManager.document.imageSize;
        CALayer *newLayer = [_docManager.grainSizeResult newLayerWithFrame:CGRectMake(0, 0, imageSize.width, imageSize.height)
                                                                 lineWidth:lineWidth];
        
        CGFloat scale = 1.0f / screenScale;
        newLayer.transform = CATransform3DMakeScale(scale, scale, 1.0f);
        newLayer.name = kGrainsMeasurementLayerName;
        
        [self.imageView.layer replaceSublayerWithSameNameLayer:newLayer bringTop:YES];
        [newLayer release];
        newLayer = nil;
        
        // update layer on magnifier view
        CGFloat magnifyFactor = (self.magnifyView == nil) ? 5.0 : self.magnifyView.magnifyFactor;
        lineWidth = baseLineWidth / magnifyFactor;
        newLayer = [_docManager.grainSizeResult newLayerWithFrame:CGRectMake(0, 0, imageSize.width, imageSize.height)
                                                        lineWidth:lineWidth];
        newLayer.transform = CATransform3DMakeScale(scale, scale, 1.0f);
        newLayer.name = kGrainsMeasurementLayerName;
        
        UIView *viewToMagnify = self.magnifyView.viewToMagnify;
        [viewToMagnify.layer replaceSublayerWithSameNameLayer:newLayer bringTop:YES];
        [newLayer release];
    } else {
        [self.imageView.layer removeSubLayerWithName:kGrainsMeasurementLayerName];
        UIView *viewToMagnify = self.magnifyView.viewToMagnify;
        [viewToMagnify.layer removeSubLayerWithName:kGrainsMeasurementLayerName];
    }
}

- (void)ennablePinchDismiss:(BOOL)enabled {
    if (_docManager == nil) {  // ignore unsupported image document
        return;
    }
    
    if (enabled) {
        NSAssert(self.scrollView.pinchGestureRecognizer, @"Too early to call ennablePinchDismiss, scrollView.pinchGestureRecognizer is not valid yet.");
        [self.scrollView.pinchGestureRecognizer addTarget:self action:@selector(scrollViewHandlePinch:)];
    } else {
        [self.scrollView.pinchGestureRecognizer removeTarget:self action:@selector(scrollViewHandlePinch:)];
    }
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    BOOL docManagerModified = NO;
    if (object == _docManager) {
        if ([keyPath isEqualToString:@"imageModified"]) {
            docManagerModified = YES;
            if (_docManager.isImageModified) {
                [self fitImageViewIntoScrollView:YES];
                
                [_docManager.multiphase setProcessingImage:[_docManager.document outputImage]];
            }
            
            if (self.magnifyView) {
                __block UIImageView *viewToManify = self.magnifyView.viewToMagnify;
                dispatch_async(dispatch_get_main_queue(), ^{
                    viewToManify.image = [_docManager.document outputImage];
                    CGSize imageSize = [_docManager.document imageSize];
                    CGFloat screenScale = [[UIScreen mainScreen] scale];
                    viewToManify.frame = CGRectMake(0, 0, imageSize.width / screenScale, imageSize.height / screenScale);
                });
            }
        } else if ([keyPath isEqualToString:@"elementLayerModified"]) {
            docManagerModified = YES;
        } else if ([keyPath isEqualToString:@"multichannelModified"]) {
            docManagerModified = YES;
        } else if ([keyPath isEqualToString:@"multiphaseModified"]) {
            docManagerModified = YES;
            if (_docManager.isMultiphaseModified) {
                self.needInvalidateMultiphaseOverlayLayer = YES;
                if (_docManager.analyzer2dResult == nil) {
                    [self invalidateMultiphaseAreaAndUpdateAnalyzerResult:YES completionBlock:NULL];
                } else {
                    [self invalidateMultiphaseAreaFromAnalyzerResult];
                }
                self.needInvalidateMultiphaseOverlayLayer = NO;
            }
        } else if ([keyPath isEqualToString:@"grainSizeModified"]) {
            docManagerModified = YES;
            if (_docManager.isGrainSizeModified) {
                [self updateGrainSizeLayer];
            }
        }
        
        if (docManagerModified) {
            if ([_delegate respondsToSelector:@selector(imagePageViewController:docManagerModified:)]) {
                [_delegate imagePageViewController:self docManagerModified:_docManager];
            }
            
            if ([self.imageEditingDelegate respondsToSelector:@selector(imageViewDocManagerModified:)]) {
                [self.imageEditingDelegate imageViewDocManagerModified:self.imageView];
            }
        }
        
        if (_docManager.isModified) {
            [_docManager scheduleBackup];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if (self.isEditing && self.editingMode == CZEditingModeImageProcessing) {
        return nil;
    } else {
        return self.imageBorderView;
    }
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    if ([self.delegate respondsToSelector:@selector(imageView:willBeginZooming:)]) {
        [self.delegate imageView:self.imageView willBeginZooming:self.scrollView.zoomScale];
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
    
    CGFloat scale = self.scrollView.zoomScale;
    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:isChangingZoom:)]) {
        [self.imageEditingDelegate imageView:self.imageView isChangingZoom:scale];
    }  else if ([self.delegate respondsToSelector:@selector(imageView:isChangingZoom:)]) {
        [self.delegate imageView:self.imageView isChangingZoom:scale];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    // Disabled the performance optimization for rendering because of a bug.
    //[_imageTool invalidate];
    if (scrollView == self.scrollView) {
        if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:didChangeToZoom:)]) {
            [self.imageEditingDelegate imageView:self.imageView didChangeToZoom:scale];
        } else if ([self.delegate respondsToSelector:@selector(imageView:didChangeToZoom:)]) {
            [self.delegate imageView:self.imageView didChangeToZoom:scale];
        }
    }
    
    [self updateGrainSizeLayer];
}

#pragma mark - CZAnnotationGestureRecognizerDelegate

- (CZShouldBeginTouchState)annotationGetstureShouldBeginTouch:(UITouch *)touch {
    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:shouldBeginTouch:)]) {
        return [self.imageEditingDelegate imageView:self.imageView shouldBeginTouch:touch];
    }
    return CZShouldBeginTouchStateNo;
}

#pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)isEditingAnnotation {
    return [self isEditing] && (self.editingMode == CZEditingModeAnnotationBasic ||
                                self.editingMode == CZEditingModePCB);
}

- (BOOL)isEditingPCBAnnotation {
    return [self isEditing] && (self.editingMode == CZEditingModePCB);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == _annotationRecognizer) {
        return (self.imageEditingDelegate != nil);
    } else if (gestureRecognizer == _laserPointerRecongnizer) {
        BOOL enableLaserPoionter = [[CZDefaultSettings sharedInstance] showLaserPointer];
        if (!enableLaserPoionter || self.isEditing) {
            return NO;
        }
    } else if (gestureRecognizer == _doubleTapRecognizer) {
        if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:shouldHandleDoubleTapGesture:)]) {
            return [self.imageEditingDelegate imageView:self.imageView shouldHandleDoubleTapGesture:gestureRecognizer];
        }
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer == self.tapRecognizer) {
        if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:shouldTapGestureRequireOtherFail:)]) {
            return [self.imageEditingDelegate imageView:self.imageView shouldTapGestureRequireOtherFail:gestureRecognizer];
        }
    }
    return NO;
}

- (void)scrollViewTapped:(UIGestureRecognizer *)recognizer {
    if (recognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    CZLogv(@"scroll view handle tap");
    
    [_magnifyView removeFromSuperview];
    
    BOOL handled = NO;
    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:handleTapGesture:)]) {
        handled = [self.imageEditingDelegate imageView:self.imageView handleTapGesture:recognizer];
    }
    
    if (!handled && [self.delegate respondsToSelector:@selector(imageView:handleTapGesture:)]) {
        [self.delegate imageView:self.imageView handleTapGesture:recognizer];
    }
}

- (void)scrollViewHandleFingerTouchMove:(CZLaserPointerGestureRecognizer *)gestureRecognizer {
    
}

- (void)scrollViewHandleTwoFingerTouches:(CZAnnotationGestureRecognizer *)gestureRecognizer {
    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:handleTouchGesture:)]) {
        [self.imageEditingDelegate imageView:self.imageView handleTouchGesture:gestureRecognizer];
    }
    
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan &&
        gestureRecognizer.state != UIGestureRecognizerStateChanged) {
        [_magnifyView removeFromSuperview];
    }
}

- (void)scrollViewDoubleTapped:(UIGestureRecognizer *)recognizer {
    if (recognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    
    if (self.imageView == nil) {
        return;
    }
    if ([self.imageEditingDelegate respondsToSelector:@selector(imageView:handleDoubleTapGesture:)]) {
        [_magnifyView removeFromSuperview];
        
        BOOL handled = [self.imageEditingDelegate imageView:self.imageView handleDoubleTapGesture:recognizer];
        if (handled) {
            return;
        }
    }
    CGFloat fullScale = [[UIScreen mainScreen] scale];
    CGFloat oldScale = [_scrollView zoomScale];
    CGFloat newScale = (fabs(fullScale - oldScale) >= fabs(_fitScale - oldScale)) ? fullScale : _fitScale;
    
    if (_fitScale < fullScale && newScale == fullScale) {
        // If the image is larger than scroll view, and now it's fit or smaller,
        // zoom to 100%.
        // Calculate the rect to zoom to according to the touch point.
        CGSize scrollViewSize = self.scrollView.bounds.size;
        CGPoint pointInView = [recognizer locationInView:self.imageBorderView];
        
        CGFloat w = scrollViewSize.width / newScale;
        CGFloat h = scrollViewSize.height / newScale;
        CGFloat x = pointInView.x - (w / 2.0f);
        CGFloat y = pointInView.y - (h / 2.0f);
        
        CGRect rectToZoomTo = CGRectMake(x, y, w, h);
        [self.scrollView zoomToRect:rectToZoomTo animated:YES];
    } else {
        if (self.isEditing) {
            CGSize scrollViewSize = self.scrollView.bounds.size;
            CGPoint center = self.imageBorderView.center;
            center.x /= oldScale;
            center.y /= oldScale;
            
            CGFloat w = scrollViewSize.width / newScale;
            CGFloat h = scrollViewSize.height / newScale;
            CGFloat x = center.x - w / 2;
            CGFloat y = center.y - h / 2;
            
            CGRect rectToZoomTo = CGRectMake(x, y, w, h);
            [self.scrollView zoomToRect:rectToZoomTo animated:YES];
        } else {
            [_scrollView setZoomScale:newScale animated:YES];
        }
    }
}

- (void)scrollViewHandlePinch:(UIPinchGestureRecognizer *)gestureRecognizer {
    // TODO: find a better way to get imageTabView
    UIViewController *imageTabViewController = self.parentViewController.parentViewController;
    UIView *imageTabView = imageTabViewController.view;
    
    const CGFloat kEpsilon = 1e-5;
    const CGFloat screenScale = [[UIScreen mainScreen] scale];
    const CGFloat minScale = MIN(screenScale, _fitScale);
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if (fabs(gestureRecognizer.scale - minScale) < kEpsilon) {
            _pinchState = CZImagePageViewPinchStateUnknown;
        } else if (self.scrollView.zoomScale < minScale - kEpsilon) {
            _pinchState = CZImagePageViewPinchStateDismiss;
        } else {
            _pinchState = CZImagePageViewPinchStateZooming;
        }
        
        if (_pinchState != CZImagePageViewPinchStateZooming) {
            self.scrollView.minimumZoomScale = 1e-6;
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (_pinchState == CZImagePageViewPinchStateUnknown) {
            if (gestureRecognizer.scale < minScale - kEpsilon) {
                _pinchState = CZImagePageViewPinchStateDismiss;
            } else if (gestureRecognizer.scale > minScale + kEpsilon) {
                _pinchState = CZImagePageViewPinchStateZooming;
                self.scrollView.minimumZoomScale = minScale;
            }
        } else if (_pinchState == CZImagePageViewPinchStateDismiss) {
            if (gestureRecognizer.scale < minScale) {
                CGFloat alpha = gestureRecognizer.scale / minScale;
                imageTabView.backgroundColor = [kDefaultBackGroundColor colorWithAlphaComponent:alpha];
            } else {
                imageTabView.backgroundColor = kDefaultBackGroundColor;
            }
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (_pinchState == CZImagePageViewPinchStateDismiss &&
            gestureRecognizer.velocity < 0 &&
            gestureRecognizer.scale < minScale) {
            // animate to dismiss
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 self.scrollView.zoomScale = 0.05;  // zoom out to a small scale then disappear
                                 imageTabView.backgroundColor = [kDefaultBackGroundColor colorWithAlphaComponent:0];
                             }
                             completion:^(BOOL finished) {
                                 imageTabView.backgroundColor = kDefaultBackGroundColor;
                                 imageTabView.hidden = YES;
                                 
                                 if ([self.delegate respondsToSelector:@selector(imagePageViewControllerDidPinchDismiss:)]) {
                                     [self.delegate imagePageViewControllerDidPinchDismiss:self];
                                 }
                             }];
        } else if (gestureRecognizer.scale < minScale) {
            imageTabView.backgroundColor = kDefaultBackGroundColor;
            
            // animate back to minimum scale
            [UIView animateWithDuration:0.3 animations:^{
                self.scrollView.zoomScale = minScale;
            } completion:^(BOOL finished) {
                self.scrollView.zoomScale = minScale;
            }];
        }
    } else {
        imageTabView.backgroundColor = kDefaultBackGroundColor;
    }
    
    // restore minimum zoom scale
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan &&
        gestureRecognizer.state != UIGestureRecognizerStateChanged) {
        self.scrollView.minimumZoomScale = minScale;
    }
}

#pragma mark - CZElementLayerDelegate methods

- (void)layerUpdateAllMeasurements:(CZElementLayer *)layer {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer updateAllElements:layer];
    [renderer release];
    
    if ([self.elementLayerDelegate respondsToSelector:@selector(layerUpdateAllMeasurements:)]) {
        [self.elementLayerDelegate layerUpdateAllMeasurements:layer];
    }
}

- (void)layer:(CZElementLayer *)layer updateElementMeasurementsLargerThanIndex:(NSUInteger)index {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer layer:layer updateElementMeasurementsLargerThanIndex:index];
    [renderer release];
    
    if ([self.elementLayerDelegate respondsToSelector:@selector(layer:updateElementMeasurementsLargerThanIndex:)]) {
        [self.elementLayerDelegate layer:layer updateElementMeasurementsLargerThanIndex:index];
    }
}

- (void)layer:(CZElementLayer *)layer didAddElement:(CZElement *)element {
    CZElement *temporarilyHiddenElement = nil;
    if ([self.imageEditingDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.imageEditingDelegate temporarilyHiddenElement];
    }
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer layer:layer didAddElement:element];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if (_magnifyView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_magnifyView.viewToMagnify];
        [renderer layer:layer didAddElement:element];
        [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
        [renderer release];
    }
    
    if ([self.elementLayerDelegate respondsToSelector:@selector(layer:didAddElement:)]) {
        [self.elementLayerDelegate layer:layer didAddElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer layer:layer didRemoveElement:element];
    [renderer release];
    
    if (_magnifyView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_magnifyView.viewToMagnify];
        [renderer layer:layer didRemoveElement:element];
        [renderer release];
    }
        if ([self.elementLayerDelegate respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [self.elementLayerDelegate layer:layer didRemoveElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {
    CZElement *temporarilyHiddenElement = nil;
    if ([self.imageEditingDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.imageEditingDelegate temporarilyHiddenElement];
    }

    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.imageView];
    [renderer layer:layer didChangeElement:element];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if (_magnifyView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_magnifyView.viewToMagnify];
        [renderer layer:layer didChangeElement:element];
        
        CZElement *selectedElement = nil;
        if ([self.imageEditingDelegate respondsToSelector:@selector(selectedElement)]) {
            selectedElement = [self.imageEditingDelegate selectedElement];
        }
        
        BOOL hidden = (selectedElement == element) || (temporarilyHiddenElement == element);
        if (hidden) {
            [renderer hideElementOnly:element inElementLayer:layer];
        }
        
        [renderer release];
    }
    
    if ([_elementLayerDelegate respondsToSelector:@selector(layer:didChangeElement:)]) {
        [_elementLayerDelegate layer:layer didChangeElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didRemoveGroup:(CZElementGroup *)group {
    if ([_elementLayerDelegate respondsToSelector:@selector(layer:didRemoveGroup:)]) {
        [_elementLayerDelegate layer:layer didRemoveGroup:group];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeGroup:(CZElementGroup *)group {
    if ([_elementLayerDelegate respondsToSelector:@selector(layer:didChangeGroup:)]) {
        [_elementLayerDelegate layer:layer didChangeGroup:group];
    }
}

- (void)layerDidChangeScaling:(CZElementLayer *)layer {
    [self redrawElementLayer:layer];
    
    if ([_elementLayerDelegate respondsToSelector:@selector(layerDidChangeScaling:)]) {
        [_elementLayerDelegate layerDidChangeScaling:layer];
    }
}

#pragma mark - CZImageToolDelegate Methods

- (void)imageTool:(CZImageTool *)imageTool changedImageSizeTo:(CGSize)newSize {
    CZElementLayer *elementLayer = self.docManager.elementLayer;
    if (elementLayer) {
        elementLayer.frame = CGRectMake(0, 0, newSize.width, newSize.height);
    }
    
    BOOL hasEditingBorder = self.isEditing && (self.editingMode == CZEditingModeAnnotationBasic ||
                                               self.editingMode == CZEditingModePCB);
    
    CGRect frame = CGRectZero;
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    frame.size.width = newSize.width / screenScale;
    frame.size.height = newSize.height / screenScale;
    CGRect borderFrame = CGRectZero;
    if (hasEditingBorder) {
        CGSize borderSize;
        borderSize.width = floor(frame.size.width * kImageEditingEdgeRatio);
        borderSize.height = floor(frame.size.height * kImageEditingEdgeRatio);
        
        borderFrame.size.width = frame.size.width + borderSize.width * 2;
        borderFrame.size.height = frame.size.height + borderSize.height * 2;
    } else {
        borderFrame = frame;
    }
    
    // save zoom scale
    CGFloat zoomScale = self.scrollView.zoomScale;
    
    // change the image view & image border view size to new
    self.scrollView.zoomScale = 1.0;
    self.imageBorderView.frame = borderFrame;
    self.imageBorderView.bounds = borderFrame;
    self.imageView.frame = frame;
    self.imageView.bounds = frame;
    self.imageView.center = CGPointMake(borderFrame.size.width / 2, borderFrame.size.height / 2);
    
    // set zoom scale back
    self.scrollView.zoomScale = zoomScale;
    
    [self fitImageViewIntoScrollView:YES];
}

- (void)imageToolDidDismissInteractiveRotation:(CZImageTool *)imageTool {
    if ([_delegate respondsToSelector:@selector(imageToolDidDismissInteractiveRotation:)]) {
        [_delegate imageToolDidDismissInteractiveRotation:imageTool];
    }
}

- (void)imageToolDidDismissInteractiveMirroring:(CZImageTool *)imageTool {
    if ([_delegate respondsToSelector:@selector(imageToolDidDismissInteractiveMirroring:)]) {
        [_delegate imageToolDidDismissInteractiveMirroring:imageTool];
    }
}

- (void)imageTool:(CZImageTool *)imageTool didDismissInteractiveDisplayCurve:(CZDisplayCurvePreset)preset {
    if ([_delegate respondsToSelector:@selector(imageTool:didDismissInteractiveDisplayCurve:)]) {
        [_delegate imageTool:imageTool didDismissInteractiveDisplayCurve:preset];
    }
}

- (void)imageTool:(CZImageTool *)imageTool didSwitchToCroppingMode:(CZCroppingMode)mode {
    if ([_delegate respondsToSelector:@selector(imageTool:didSwitchToCroppingMode:)]) {
        [_delegate imageTool:imageTool didSwitchToCroppingMode:mode];
    }
}

- (CGFloat)currentZoomScaleForImageTool:(CZImageTool *)imageTool {
    return self.scrollView.zoomScale;
}

#pragma mark - CZDocManagerDelegate

- (void)docManager:(CZDocManager *)docManager didSaveFiles:(NSArray *)files success:(BOOL)success {
    if ([_delegate respondsToSelector:@selector(docManager:didSaveFiles:success:)]) {
        [_delegate docManager:docManager didSaveFiles:files success:success];
    }
}

- (void)docManager:(CZDocManager *)docManager willSaveFiles:(NSArray *)files {
    if ([_delegate respondsToSelector:@selector(docManager:willSaveFiles:)]) {
        [_delegate docManager:docManager willSaveFiles:files];
    }
}

#pragma mark - CZMagnifyDelegate methods

- (void)magnifyAtPoint:(CGPoint)point {
    UIView *superView = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
    if (![_magnifyView isDescendantOfView:superView]) {
        [superView addSubview:_magnifyView];
        _magnifyView.viewToMagnifyScale = self.scrollView.zoomScale;
    }
    
    CGPoint pointOfRootView = [self.imageView convertPoint:point toView:self.magnifyView];
    [_magnifyView validateTouchPoint:pointOfRootView];
    
    _magnifyView.mode = CZMagnifierViewModeSinglePoint;
    _magnifyView.touchPoint = point;
    [_magnifyView setNeedsDisplay];
}

- (void)dismissMagnifierView {
    [_magnifyView removeFromSuperview];
}

- (void)magnifyViewAtPoint:(CGPoint)point point2:(CGPoint)point2 {
    UIView *superView = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
    if (![_magnifyView isDescendantOfView:superView]) {
        [superView addSubview:_magnifyView];
    }
    
    CGPoint pointOfRootView = [self.imageView convertPoint:point toView:self.magnifyView];
    CGPoint pointOfRootView2 = [self.imageView convertPoint:point2 toView:self.magnifyView];
    [_magnifyView validateTouchPoint:pointOfRootView point2:pointOfRootView2];
    
    _magnifyView.mode = CZMagnifierViewModeTwoPoints;
    _magnifyView.touchPoint = point;
    _magnifyView.touchPoint2 = point2;
    [_magnifyView setNeedsDisplay];
}

- (void)temporaryHideElement:(CZElement *)element {
    if (_magnifyView.viewToMagnify) {
        CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_magnifyView.viewToMagnify];
        [renderer hideElementOnly:element inElementLayer:element.parent];
        [renderer release];
    }
}

@end

