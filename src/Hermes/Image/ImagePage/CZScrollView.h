//
//  CZScrollView.h
//  Hermes
//
//  Created by Mike Wang on 7/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZScrollView;

@protocol CZScrollViewDelegate <NSObject>
- (void)scrollViewDidExitFromHorizontalLockStandby:(CZScrollView *)scrollView;
- (void)scrollViewDidExitFromVerticalLockStandby:(CZScrollView *)scrollView;
- (void)zoomRectFromScrollView:(CZScrollView *)scrollView;
- (void)scrollRecFromScrollView:(CZScrollView *)scrollView;
@end

@interface CZScrollView : UIScrollView <UIScrollViewDelegate, CZScrollViewDelegate>
@property (nonatomic, assign) id<UIScrollViewDelegate> primaryScrollViewDelegate;
@property (nonatomic, retain) id<UIScrollViewDelegate, CZScrollViewDelegate> secondaryScrollViewDelegate;
@property (nonatomic, assign) BOOL lockScrolling;
@property (nonatomic, assign) CGPoint lockStandbySecondaryScrollViewContentOffset;
@property (nonatomic, assign) CGFloat minimumZoomScaleBeforeLocking;
@property (nonatomic, assign) CGFloat maximumZoomScaleBeforeLocking;
@end
