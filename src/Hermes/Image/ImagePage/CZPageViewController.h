//
//  CZPageViewController.h
//  Hermes
//
//  Created by Mike Wang on 9/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CZPageViewControllerDelegate <NSObject>

- (void)didChangeLayout;

@end

@interface CZPageViewController : UIPageViewController

@property (nonatomic, assign) id<CZPageViewControllerDelegate> layoutChangeDelegate;

@end

@interface UIPageViewController (Additions)  // patch for UIPageViewController for invalidate inner page cache

- (void)setViewControllers:(NSArray *)viewControllers
                 direction:(UIPageViewControllerNavigationDirection)direction
           invalidateCache:(BOOL)invalidateCache
                  animated:(BOOL)animated
                completion:(void (^)(BOOL finished))completion;

@end
