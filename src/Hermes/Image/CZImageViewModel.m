//
//  CZImageViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/12.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageViewModel.h"
#import "CZAnnotationEditingViewController.h"

@implementation CZImageViewModel

#pragma mark - Public method

- (BOOL)canSwitchPage {
#if IMAGE_REFACTOR
    if (self.currentPage.isEditing ||
        [self.displayOptionsView isButtonSelected:kCZDisplayOptionsViewButtonTagDrawing] || self.lockScrolling) {
        return NO;
    } else {
        return YES;
    }
#endif
    return YES;
}


@end

#pragma mark - CZImageViewModel (SaveFileFormats)

@implementation CZImageViewModel (SaveFileFormats)

+ (NSArray *)saveFileFormats {
    static NSArray *content = nil;
    if (content == nil) {
        content = [[NSArray alloc] initWithObjects:L(@"SAVE_FORMAT_CZI"),
                                                   L(@"SAVE_FORMAT_CZI_AND_JPG_MARKUP"),
                                                   L(@"SAVE_FORMAT_JPG"),
                                                   L(@"SAVE_FORMAT_JPG_NO_MARKUP"),
                                                   L(@"SAVE_FORMAT_JPG_MEDIUM"),
                                                   L(@"SAVE_FORMAT_CZI_AND_TIF_MARKUP"),
                                                   L(@"SAVE_FORMAT_TIF"),
                                                   L(@"SAVE_FORMAT_TIF_NO_MARKUP"),
                                                   nil];
    }
    return content;
}

@end

#pragma mark - CZImageViewModel (AnnotationEditing)

@implementation CZImageViewModel (AnnotationEditing)

+ (CZImageEditingViewController *)imageEditingViewControllerWithEditingMode:(CZEditingMode)editingMode {
    CZLogv(@"image editing view Controller with mode: %d", editingMode);
    CZImageEditingViewController *viewController = nil;
    if (editingMode == CZEditingModeMultiphase) {
        //        viewController = [[CZMultiphaseEditingViewController alloc] init];
    } else if (editingMode == CZEditingModeParticles) {
        //        viewController = [[CZParticlesEditingViewController alloc] init];
    } else if (editingMode == CZEditingModeGrainSize) {
        //        viewController = [[CZGrainsEditingViewController alloc] init];
    } else if (editingMode == CZEditingModeCurveLayer) {
        //        viewController = [[CZCurveLayerThicknessViewController alloc] init];
    } else if (editingMode == CZEditingModeAnnotationBasic) {
        viewController = [[CZAnnotationEditingViewController alloc] init];
    } else if (editingMode == CZEditingModePCB) {
        //        viewController = [[CZPCBEditingViewController alloc] init];
    }
    return viewController;
}

@end
