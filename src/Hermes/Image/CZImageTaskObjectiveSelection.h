//
//  CZImageTaskObjectiveSelection.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/16.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CZObjectiveStatesDelegate <NSObject>

@optional
- (void)objectiveSelectionButtonTitleDidChange;
- (void)objectiveSelectionButtonImageDidChange;
- (void)objectiveSelectionButtonHiddenDidChange;
- (void)objectiveSelectionButtonEnabledDidChange;

- (void)zoomSelectionButtonHiddenDidChange;
- (void)zoomSelectionButtonEnabledDidChange;
- (void)zoomSelectionButtonTitleDidChange;
- (void)zoomSelectionButtonImageDidChange;

- (void)magnificationButtonEnabledDidChange;
- (void)magnificationButtonTitleDidChange;
- (void)magnificationButtonImageDidChange;

@end

//TODO: Move to the CZImageViewModel

@protocol CZImageTaskObjectiveSelection <NSObject>

@optional

@property (nonatomic, weak) id <CZObjectiveStatesDelegate> objectiveStatesDelegate;
@property (nonatomic, readonly, assign) BOOL objectiveSelectionButtonHidden;
@property (nonatomic, readonly, assign) BOOL objectiveSelectionButtonEnabled;
@property (nonatomic, readonly, copy) NSString *objectiveSelectionButtonTitle;
@property (nonatomic, readonly, strong) UIImage *objectiveSelectionButtonImage;

@property (nonatomic, readonly, assign) BOOL zoomSelectionButtonHidden;
@property (nonatomic, readonly, assign) BOOL zoomSelectionButtonEnabled;
@property (nonatomic, readonly, copy) NSString *zoomSelectionButtonTitle;
@property (nonatomic, readonly, strong) UIImage *zoomSelectionButtonImage;

@property (nonatomic, readonly, assign) BOOL magnificationButtonEnabled;
@property (nonatomic, readonly, copy) NSString *magnificationButtonTitle;
@property (nonatomic, readonly, strong) UIImage *magnificationButtonImage;

- (void)selectObjectiveAtPosition:(NSUInteger)position;
- (void)selectZoomClickStopAtPosition:(NSUInteger)position;
- (void)selectMagnificationAtPosition:(NSUInteger)position isObjectiveSelection:(BOOL)isObjectiveSelection;

@end
