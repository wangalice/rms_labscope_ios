//
//  CZImageTask.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageTask.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZImagePageViewController.h"
#import "CZImageViewModel.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "UIImage+CZObjective.h"
#import "CZFileCommonMarco.h"
#import "CZLocalFileList.h"
#import <CZFileKit/CZFileKit.h>

@interface CZImageTask () <CZChannelObserver>

@property (nonatomic, strong) CZDocManager *docManager;
@property (strong) NSArray *localFiles;
@property (nonatomic, copy) NSArray<CZImageChannel *> *allChannels; // All channels
@property (nonatomic, readwrite, copy) NSArray<CZImageChannel *> *channels; // Visible channels
@property (nonatomic, strong) CZEditMultichannelAction *editMultichannelAction;

@end

@implementation CZImageTask

@dynamic delegate;

@synthesize objectiveStatesDelegate = _objectiveStatesDelegate;
@synthesize magnificationButtonEnabled = _magnificationButtonEnabled;
@synthesize magnificationButtonTitle = _magnificationButtonTitle;
@synthesize magnificationButtonImage = _magnificationButtonImage;

- (instancetype)initWithIdentifier:(NSString *)identifier {
    self = [super initWithIdentifier:identifier];
    if (self) {
        _currentDisplayCurve = CZDisplayCurveLinear;
        _localFiles = [NSArray array];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(localFileListDidUpdate:)
                                                     name:CZLocalFileListDidUpdateNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userInteractionLogChanged:)
                                                     name:kCZEventLogChangedNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(showDefaultSettingSwitchingMessage:)
                                                     name:kShowDefaultSettingSwitchingMessage
                                                   object:nil];
    }
    return self;
}

- (void)didEnterForeground {
    [super didEnterForeground];
    
    if (self.docManager == nil || ![self.docManager.filePath isEqualToString:self.filePath]) {
        CZDocManager *docManager = [CZDocManager newDocManagerFromFilePath:self.filePath];
        [self presentImageDocument:docManager];
    }
}

- (void)didEnterBackground {
    [super didEnterBackground];
    
    if (self.docManager && !self.docManager.isModified) {
        self.lastOpenedFilePath = self.docManager.filePath;
    } else {
        self.lastOpenedFilePath = nil;
    }
    
    self.editMultichannelAction = nil;
}

#pragma mark - Public methods

- (BOOL)openFileAtPath:(NSString *)filePath {
    if (self.docManager.isImageNew || self.docManager.isModified) {
        // do not open image when old image is not saved yet.
        return FALSE;
    } else {
        return [self privateOpenFileAtPath:filePath];
    }
}

- (void)presentImageDocument:(CZDocManager *)docManager {
    [self presentImageDocument:docManager filePathOfDocument:docManager.filePath];
}

- (BOOL)hasUnsavedMultichannelAction {
    return self.editMultichannelAction != nil;
}

- (void)saveMultichannelAction {
    if (self.editMultichannelAction) {
        [self.docManager pushAction:self.editMultichannelAction];
        self.editMultichannelAction = nil;
    }
}

- (void)discardMultichannelAction {
    if (self.editMultichannelAction) {
        self.editMultichannelAction = nil;
    }
}

- (void)updateChannels {
    NSMutableArray<CZImageChannel *> *allChannels = [NSMutableArray array];
    NSMutableArray<CZImageChannel *> *channels = [NSMutableArray array];
    for (CZImageBlock *imageBlock in self.docManager.document.imageBlocks) {
        CZImageChannel *channel = [[CZImageChannel alloc] initWithImageBlock:imageBlock];
        [allChannels addObject:channel];
        if (imageBlock.channelProperties.channelName && imageBlock.channelProperties.channelColor) {
            [channels addObject:channel];
            [channel addObserver:self];
        }
    }
    self.allChannels = allChannels;
    self.channels = channels;
}

- (BOOL)shouldConvertToCZIFile {
    return NO;
}

- (BOOL)isLicenseEnabled {
    //TODO: Add the license enabled value here to control the annotation enable the logic
    return YES;
}

- (BOOL)isPageUpEnabled {
    //TODO: current page is not the first one
#if IMAGE_REFACTOR
    if (self.currentPage.isEditing ||
        [self.displayOptionsView isButtonSelected:kCZDisplayOptionsViewButtonTagDrawing] || self.lockScrolling) {
        return NO;
    }
#endif
    NSUInteger currentIndex = [self currentPageIndex];
    if (self.isEditing || self.isLockScrolling || currentIndex == 0 || currentIndex == NSNotFound) {
        return NO;
    }
    return YES;
}

- (BOOL)isPageDownEnabled {
    NSUInteger currentIndex = [self currentPageIndex];
    if (self.isEditing || self.isLockScrolling || currentIndex == self.localFiles.count - 1 || currentIndex == NSNotFound) {
        return NO;
    }
    return YES;
}

- (BOOL)isLockScrolling {
    return NO;
}

- (BOOL)isStereoMicroscopeMode {
    return [self.docManager.imageProperties isStereoMicroscopeMode];
}

- (BOOL)isMacroImageMicroscopeMode {
    return [self.docManager.imageProperties isMacroImageMicroscopeMode];
}

- (void)updateMagnificationWithFilePath:(NSString *)filePath {
    CZDocManager *selectedDocumentManager = [CZDocManager newDocManagerFromFilePath:filePath];
    CZImageProperties *selectedImageProperties = selectedDocumentManager.imageProperties;
    
    CZDocManager *docManager = self.docManager;
    [docManager updateImagePropertiesMicroscopeModel:selectedImageProperties.microscopeModel];
    [docManager updateImagePropertiesObjectiveModel:selectedImageProperties.objectiveModel];
    [docManager updateImagePropertiesTotalMagnification:selectedImageProperties.totalMagnification];
    [docManager updateImagePropertiesEyepieceMagnification:selectedImageProperties.eyepieceMagnification];
    [docManager updateImagePropertiesZoom:selectedImageProperties.zoom];
    [docManager updateImagePropertiesCameraModel:selectedImageProperties.cameraModel];
    [docManager updateImagePropertiesCameraAdapter:selectedImageProperties.cameraAdapter];
    
    [docManager updateUnitOfElementLayer];
    
    docManager.calibratedScaling = selectedImageProperties.scaling; // refresh scaling and update all related elements.
    
    [self updateMagnificationButtonImageTitle];
}

#pragma mark - Setter

- (void)setMagnificationButtonEnabled:(BOOL)magnificationButtonEnabled {
    _magnificationButtonEnabled = magnificationButtonEnabled;
    
    if (self.objectiveStatesDelegate && [self.objectiveStatesDelegate respondsToSelector:@selector(magnificationButtonEnabledDidChange)]) {
        [self.objectiveStatesDelegate magnificationButtonEnabledDidChange];
    }
}

- (void)setMagnificationButtonTitle:(NSString *)magnificationButtonTitle {
    _magnificationButtonTitle = [magnificationButtonTitle copy];
    
    if (self.objectiveStatesDelegate && [self.objectiveStatesDelegate respondsToSelector:@selector(magnificationButtonTitleDidChange)]) {
        [self.objectiveStatesDelegate magnificationButtonTitleDidChange];
    }
}

- (void)setMagnificationButtonImage:(UIImage *)magnificationButtonImage {
    _magnificationButtonImage = magnificationButtonImage;
    
    if (self.objectiveStatesDelegate && [self.objectiveStatesDelegate respondsToSelector:@selector(magnificationButtonImageDidChange)]) {
        [self.objectiveStatesDelegate magnificationButtonImageDidChange];
    }
}

#pragma mark - Private methods

- (CZDocManager *)docManager {
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(currentDocManager)]) {
        return [self.dataSource currentDocManager];
    }
    return nil;
}

- (BOOL)privateOpenFileAtPath:(NSString *)filePath {
    CZDocManager *newDocManager = [CZDocManager newDocManagerFromFilePath:filePath];
    if (newDocManager) {
        [self presentImageDocument:newDocManager];
    } else {
        [self presentImageDocument:nil filePathOfDocument:filePath];
#if IMAGE_REFACTOR
        if (!self.isSecondary) {
            CZAlert *alert = [[CZAlert alloc] initWithTitle:nil
                                                    message:L(@"ERROR_FILE_OPEN_IMAGE")];
            [alert addButtonWithTitle:L(@"OK") block:NULL];
            [alert presentInViewController:self animated:YES];
        }
#endif
    }
    
    return (newDocManager != nil);
}

/*! Present the document, if the document is nil, it can still present it as if readonly document.
 * @param docManager, the document to present
 * @param filePath, the full file path of the document, if the document is nil (for example, failed to open), you still need to pass in the real file path, so that file name can be shown on the UI.
 */
- (void)presentImageDocument:(CZDocManager *)docManager filePathOfDocument:(NSString *)filePath {
    if ([self.delegate respondsToSelector:@selector(imageTaskDidPresentImageDocument:filePathOfDocument:)]) {
        [self.delegate imageTaskDidPresentImageDocument:docManager filePathOfDocument:filePath];
    }
    
    [self updateMagnificationButtonImageTitle];
}

- (void)updateObjectiveSetting {
    if (self.microscopeModel.hasObjectivesAssigned == NO) {
        self.magnificationButtonEnabled = self.docManager.isImageSnapped == NO;
        self.magnificationButtonTitle = nil;
        self.magnificationButtonImage = [UIImage objectiveImageForMagnification:0.0 size:CZIconSizeMedium];
    } else {
        NSString *magnificationName = [[self.microscopeModel.nosepiece objectiveAtCurrentPosition] displayMagnification];
        if (magnificationName.length > 0 && ![magnificationName isEqualToString:kNotAvailableMagnification]) {
            self.magnificationButtonEnabled = YES;
            self.magnificationButtonTitle = magnificationName;
            
            float magnification = [[self.microscopeModel.nosepiece objectiveAtCurrentPosition] magnification];
            UIImage *image = [UIImage objectiveImageForMagnification:magnification size:CZIconSizeSmall];
            self.magnificationButtonImage = image;
        } else {
            self.magnificationButtonEnabled = self.docManager.isImageSnapped == NO;
            self.magnificationButtonTitle = nil;
            self.magnificationButtonImage = [UIImage objectiveImageForMagnification:0.0 size:CZIconSizeMedium];
        }
    }
}

- (void)updateZoomSetting {
    if ([self.microscopeModel zoomClickStopPositions] == 0) {
        self.magnificationButtonEnabled = self.docManager.isImageSnapped == NO;
        self.magnificationButtonTitle = nil;
        self.magnificationButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeMedium];
    } else {
        if ([self.microscopeModel zoomClickStopPositions] == 1)  {
            NSString *displayMagnification = [[self.microscopeModel currentZoomClickStop] displayMagnification];
            self.magnificationButtonEnabled = self.docManager.isImageSnapped == NO;
            self.magnificationButtonTitle = displayMagnification;
            self.magnificationButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeSmall];
        } else {
            self.magnificationButtonEnabled = YES;
            NSString *displayMagnification = [[self.microscopeModel currentZoomClickStop] displayMagnification];
            self.magnificationButtonTitle = displayMagnification;
            self.magnificationButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeSmall];
        }
    }
}

- (void)updateMagnificationButtonImageTitle {
    UIImage *magnificationButtonImage = nil;
    NSString *magnificationButtonTitle = nil;
    BOOL magnificationButtonEnabled =  self.docManager.isImageSnapped == NO;

    if ([self isStereoMicroscopeMode]) {
        NSNumber *zoom = self.docManager.imageProperties.zoom;
        if (zoom != nil && zoom.floatValue > 0) {
            magnificationButtonTitle = [CZCommonUtils localizedStringFromNumber:zoom precision:2];
            magnificationButtonTitle = [magnificationButtonTitle stringByAppendingString:@"x"];
            magnificationButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeSmall];
        } else {
            magnificationButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeMedium];
        }
        
    } else {
        NSNumber *eyepieceFactor = self.docManager.imageProperties.eyepieceMagnification;
        NSNumber *magnification = self.docManager.imageProperties.magnificationFactor;
        if (magnification && magnification.floatValue > 0) {
            if (eyepieceFactor && eyepieceFactor.floatValue > 0) {
                float magnificationFactor = magnification.floatValue / eyepieceFactor.floatValue;
                magnification = [NSNumber numberWithFloat:magnificationFactor];
            }
            
            magnificationButtonTitle = [CZCommonUtils localizedStringFromNumber:magnification precision:2];
            magnificationButtonTitle = [magnificationButtonTitle stringByAppendingString:@"x"];
            magnificationButtonImage = [UIImage objectiveImageForMagnification:magnification.floatValue size:CZIconSizeSmall];
        } else {
            magnificationButtonTitle = nil;
            magnificationButtonImage = [UIImage objectiveImageForMagnification:0.0 size:CZIconSizeMedium];
        }
        
    }
    
    self.magnificationButtonImage = magnificationButtonImage;
    self.magnificationButtonTitle = magnificationButtonTitle;
    self.magnificationButtonEnabled = magnificationButtonEnabled;
}

- (void)updateVisibleFiles:(NSArray *)localFileEntities {
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:[localFileEntities count]];
    for (CZFileEntity *fileEntity in localFileEntities) {
        NSString *filePath = fileEntity.filePath;
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[filePath pathExtension]];
        if (fileFormat == kCZFileFormatCZI || fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
            [tempArray addObject:filePath];
        }
    }
    
    self.localFiles = tempArray;
    
    [self updateCurrentFileIndex];
}

- (void)updateCurrentFileIndex {
    if (self.delegate && [self.delegate respondsToSelector:@selector(imageTaskPageControllerDataSourceDidUpdate)]) {
        [self.delegate imageTaskPageControllerDataSourceDidUpdate];
    }
}

- (NSUInteger)currentPageIndex {
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(currentPageIndex)]) {
       return [self.dataSource currentPageIndex];
    }
    return NSNotFound;
}

#pragma mark - Notification

- (void)localFileListDidUpdate:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    NSMutableArray *localFileEntities = [info objectForKey:CZLocalFileListDidUpdateNotificationFileList];
    
    [self updateVisibleFiles:localFileEntities];
}

// User interaction log - annotation default settings
- (void)userInteractionLogChanged:(NSNotification *)aNotification {
#if IMAGE_REFACTOR
    // Trigger the analytics
    NSDictionary *info = [aNotification userInfo];
    NSString *logCategory = info[kCZUserInteractionLogCategory];
    if (![logCategory isKindOfClass:[NSString class]]) {
        return;
    }
    CZUserInteractionLog *log = info[kCZUserInteractionLog];
    
    // Use a same annotation analytics to parse log
    if ([logCategory isEqualToString:kCZUIActionEventCategory]) {
        if ([log.event isEqualToString:@"annotation_setting_button_pressed"]) {
            if (_annotationDefaultSettingAnalytics == nil) {
                CZAnnotationDefaultSettingsAnalytics *analytics = [[CZAnnotationDefaultSettingsAnalytics alloc] init];
                self.annotationDefaultSettingAnalytics = analytics;
            }
            
            [self.annotationDefaultSettingAnalytics parseLog:log];
        } else if ([log.event isEqualToString:@"file_save_overwrite_event"]) {
            if (_fileOverwriteOptionsAnalytics == nil) {
                CZFileOverwriteOptionsAnalytics *analytics = [[CZFileOverwriteOptionsAnalytics alloc] init];
                self.fileOverwriteOptionsAnalytics = analytics;
            }
            
            [self.fileOverwriteOptionsAnalytics parseLog:log];
        }
    }
#endif
}

- (void)showDefaultSettingSwitchingMessage:(NSNotification *)aNotification {
#if IMAGE_REFACTOR
    NSDictionary *info = [aNotification userInfo];
    NSString *strokeColorString = info[@"stroke_color"];
    NSString *sizeString = info[@"size"];
    NSString *fillColorString = info[@"fill_color"];
    
    if (![strokeColorString isKindOfClass:[NSString class]]) {
        return;
    }
    
    if (![sizeString isKindOfClass:[NSString class]]) {
        return;
    }
    
    if (![fillColorString isKindOfClass:[NSString class]]) {
        return;
    }
    
    CZColor strokeColor = kCZColorRed;
    CZElementSize size = CZElementSizeMedium;
    CZColor fillColor = kCZColorTransparent;
    
    NSString *message = nil;
    if (strokeColorString.length > 0) {
        strokeColor = stringToCZColor(strokeColorString);
        strokeColorString = [self switchToReadableColorStringFromColor:strokeColor];
        message = [NSString stringWithFormat:L(@"LOG_ANNOTATION_STROKE_COLOR_MESSAGE"), [strokeColorString lowercaseString]];
    } else if (sizeString.length > 0) {
        size = (CZElementSize)[sizeString integerValue];
        sizeString = [self switchToReadableSizeStringFromElementSize:size];
        message = [NSString stringWithFormat:L(@"LOG_ANNOTATION_SIZE_MESSAGE"), sizeString];
    } else if (fillColorString.length > 0) {
        fillColor = stringToCZColor(fillColorString);
        fillColorString = [self switchToReadableFillColorStringFromColor:fillColor];
        message = [NSString stringWithFormat:L(@"LOG_ANNOTATION_FILL_COLOR_MESSAGE"), [fillColorString lowercaseString]];
    } else {
        return;
    }
    
    CZAlert *alert = [[CZAlert alloc] initWithTitle:nil message:message];
    [alert addButtonWithTitle:L(@"OK") block:^ {
        if (strokeColorString.length > 0) {
            [[CZDefaultSettings sharedInstance] setColor:strokeColor];
        } else if (sizeString.length > 0) {
            [[CZDefaultSettings sharedInstance] setElementSize:size];
        } else if (fillColorString.length > 0) {
            [[CZDefaultSettings sharedInstance] setBackgroundColor:fillColor];
        }
    }];
    [alert addButtonWithTitle:L(@"CANCEL") block:NULL];
    [alert presentInViewController:self animated:YES];
#endif
}

#pragma mark - CZImageTaskObjectiveSelection

- (void)selectMagnificationAtPosition:(NSUInteger)position isObjectiveSelection:(BOOL)isObjectiveSelection {
    CZDocManager *docManager = self.docManager;
    
    if (isObjectiveSelection) {
        self.microscopeModel.nosepiece.currentPosition = position;
    } else {
        self.microscopeModel.currentZoomClickStopPosition = position;
    }
    
    [docManager updateImagePropertyFrom:self.microscopeModel updateFilePath:YES];
    docManager.calibratedScaling = [self.microscopeModel currentFOVWidth] / docManager.document.imageSize.width;
    
    if (docManager.isImageSnapped) { // file path name might be changed, after docManager is udpated.
        NSString *newFilename = [docManager.filePath lastPathComponent];
        //TODO: update the fileNameLabel with new name
    }
    
    if (isObjectiveSelection) {
        [self updateObjectiveSetting];
    } else {
        [self updateZoomSetting];
    }
}

#pragma mark - CZChannelObserver

- (void)channelNameDidChange:(CZImageChannel *)channel {
    if (self.editMultichannelAction == nil) {
        self.editMultichannelAction = [[CZEditMultichannelAction alloc] initWithDocManager:self.docManager];
    }
    NSUInteger channelIndex = [self.allChannels indexOfObject:channel];
    [self.editMultichannelAction setName:channel.name forChannelAtIndex:channelIndex];
}

- (void)channelColorDidChange:(CZImageChannel *)channel {
    if (self.delegate && [self.delegate respondsToSelector:@selector(imageTaskWillCompositeMultichannelImage:)]) {
        [self.delegate imageTaskWillCompositeMultichannelImage:self];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.editMultichannelAction == nil) {
            self.editMultichannelAction = [[CZEditMultichannelAction alloc] initWithDocManager:self.docManager];
        }
        NSUInteger channelIndex = [self.allChannels indexOfObject:channel];
        [self.editMultichannelAction setColor:channel.color.value forChannelAtIndex:channelIndex];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(imageTaskDidCompositeMultichannelImage:)]) {
                [self.delegate imageTaskDidCompositeMultichannelImage:self];
            }
        });
    });
}

- (void)channelSelectedDidChange:(CZImageChannel *)channel {
    if (self.delegate && [self.delegate respondsToSelector:@selector(imageTaskWillCompositeMultichannelImage:)]) {
        [self.delegate imageTaskWillCompositeMultichannelImage:self];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.editMultichannelAction == nil) {
            self.editMultichannelAction = [[CZEditMultichannelAction alloc] initWithDocManager:self.docManager];
        }
        NSUInteger channelIndex = [self.allChannels indexOfObject:channel];
        [self.editMultichannelAction setSelection:channel.isSelected forChannelAtIndex:channelIndex];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(imageTaskDidCompositeMultichannelImage:)]) {
                [self.delegate imageTaskDidCompositeMultichannelImage:self];
            }
        });
    });
}

@end
