//
//  CZContentChooserController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/4/28.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZContentChooserController.h"

static const CGFloat kDefaultItemHeight = 48.0;
static const CGFloat kDefaultHorizontalMarign = 32.0;

@interface CZContentChooserCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *seperatorLine;

@end

@interface CZContentChooserController ()

@property (nonatomic, strong) NSArray  *contentToDisplay;

@end

@implementation CZContentChooserController

- (instancetype)initWithContent:(NSArray *)content title:(NSString *)title preferredContentSize:(CGSize)preferredContentSize {
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        self.contentToDisplay = [content copy];
        self.title = [title copy];
        self.preferredContentSize = [self updateContentSizeWithPreferredContentSize:preferredContentSize];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.cz_backgroundColorPicker = [CZColorTable defaultBackgroundColor];
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - Private methods

- (CGSize)updateContentSizeWithPreferredContentSize:(CGSize)preferredContentSize {
    NSString *maxLegthItem = nil;
    CGFloat widthForMaxLengthItem = CGFLOAT_MIN;
    
    for (NSString *item in self.contentToDisplay) {
        maxLegthItem = maxLegthItem.length >= item.length ? maxLegthItem : item;
    }
    if (maxLegthItem) {
        widthForMaxLengthItem = [maxLegthItem sizeWithAttributes:@{NSFontAttributeName: [UIFont cz_body1]}].width + kDefaultHorizontalMarign * 2;
    }
    
    CGSize contentSize = preferredContentSize;
    contentSize.width = MAX(preferredContentSize.width, ceil(widthForMaxLengthItem));
    contentSize.height = MAX(preferredContentSize.height, self.contentToDisplay.count * kDefaultItemHeight);
    return contentSize;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contentToDisplay.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CZContentChooserControllerCell";

    CZContentChooserCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[CZContentChooserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (indexPath.row < self.contentToDisplay.count) {
        cell.titleLabel.text = self.contentToDisplay[indexPath.row];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultItemHeight;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    CZContentChooserCell *chooserCell = (CZContentChooserCell *)cell;
    if (indexPath.row == self.contentToDisplay.count - 1) {
        chooserCell.seperatorLine.hidden = YES;
    } else {
        chooserCell.seperatorLine.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(contentChooserController:didSelectedItem:)]) {
        [self.delegate contentChooserController:self didSelectedItem:indexPath.row];
    }
}

@end

@implementation CZContentChooserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.cz_backgroundColorPicker = [CZColorTable defaultSelectedBackgroundColor];
        self.selectedBackgroundView = selectedBackgroundView;
        self.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs30]);

        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.seperatorLine];
        [self.contentView bringSubviewToFront:self.seperatorLine];
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.seperatorLine.translatesAutoresizingMaskIntoConstraints = NO;
        [self.titleLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:kDefaultHorizontalMarign].active = YES;
        [self.titleLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-kDefaultHorizontalMarign].active = YES;
        [self.titleLabel.heightAnchor constraintEqualToConstant:21.0].active = YES;
        [self.titleLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
        
        [self.seperatorLine.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
        [self.seperatorLine.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = YES;
        [self.seperatorLine.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
        [self.seperatorLine.heightAnchor constraintEqualToConstant:1.0].active = YES;
    }
    return self;
}

#pragma mark - Getters

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont cz_body1];
        _titleLabel.numberOfLines = 1;
        _titleLabel.cz_textColorPicker = [CZColorTable defaultTextColor];
    }
    return _titleLabel;
}

- (UIView *)seperatorLine {
    if (_seperatorLine == nil) {
        _seperatorLine = [[UIView alloc] init];
        _seperatorLine.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs110],[UIColor cz_gs10]);
    }
    return _seperatorLine;
}

@end
