//
//  CZContentChooserController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/4/28.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZContentChooserController;

@protocol CZContentChooserControllerDelegate <NSObject>

@optional
- (void)contentChooserController:(CZContentChooserController *)controller didSelectedItem:(NSInteger)item;

@end

@interface CZContentChooserController : UITableViewController

@property (nonatomic, weak) id <CZContentChooserControllerDelegate> delegate;
@property (nonatomic, assign, readonly) NSInteger selectedItem;
@property (nonatomic, assign) NSInteger destructiveItemIndex;

- (instancetype)initWithContent:(NSArray <NSString *> *)content
                          title:(nullable NSString *)title
           preferredContentSize:(CGSize)preferredContentSize;

@end

NS_ASSUME_NONNULL_END
