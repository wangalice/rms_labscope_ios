//
//  CZImageViewController.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZImageTask.h"
#import "CZSystemToolbar.h"

@interface CZImageViewController : UIViewController <CZTaskDelegate, CZMainToolbarDataSource, CZVirtualToolbarDataSource, CZToolbarDelegate, CZToolbarResponder>

@property (nonatomic, readonly, strong) CZImageTask *task;

@end
