//
//  CZImageViewController.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageViewController.h"
#import "CZMultitaskViewController.h"
#import "CZPageViewController.h"
#import "CZImagePageViewController.h"
#import "CZFileNameLabel.h"
#import "CZChannelButtonGroup.h"
#import "CZSimpleFileNameTemplateController.h"
#import "CZPopoverController.h"
#import "CZFileOverwriteController.h"
#import "CZContentChooserController.h"
#import "CZPostOffice.h"
#import "CZFileInfoViewController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZFileKit/CZFileKit.h>
#import "CZAnnotationPickerView.h"
#import "CZImageEditingViewController.h"
#import "CZImageViewModel.h"
#import "CZAnnotationEditingViewController.h"
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZImageProcessingViewController.h"
#import "CZAlertController.h"
#import "CZReportTemplateViewController.h"
#import "CZObjectiveSelectionViewController.h"
#import "CZImagePickerTableViewController.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "CZZoomClickStopSelectionViewController.h"
#import "CZToastManager.h"
#import "UIViewController+HUD.h"

static const CGFloat kDefaultStatusBarHeight = 20.0;
static const CGFloat kDefaultHorizontalMargin = 16.0;
static const CGFloat kDefaultButtonItemWidth = 64.0;
static const CGFloat kDefaultButtonItemHeight = 48.0;

@interface CZImageViewController ()<
UIPageViewControllerDelegate,
UIPageViewControllerDataSource,
CZPageViewControllerDelegate,
CZTaskDelegate,
CZImageTaskDelegate,
CZImageTaskDataSource,
CZImagePageViewControllerDelegate,
CZDocManagerDelegate,
CZSimpleFileNameTemplateControllerDelegate,
CZPopoverControllerDelegate,
CZDocSaveDelegate,
CZContentChooserControllerDelegate,
CZPostOfficeDelegate,
CZChannelButtonGroupDelegate,
CZAnnotationPickerViewDelegate,
CZAnnotationPickerViewDataSource,
CZImageEditingViewControllerDataSource,
CZImageEditingViewControllerDelegate,
CZReportTemplateViewControllerDataSource,
CZKeyboardObserver,
CZObjectiveSelectionViewControllerDelegate,
CZImagePickerTableViewDelegate,
CZObjectiveStatesDelegate,
CZZoomClickStopSelectionViewControllerDelegate,
CZImageToolDelegate,
CZImageViewEditingDelegate
>

@property (nonatomic, copy) CZTaskEventCompletionHandler taskWillEnterBackgroundCompletionHandler;
@property (nonatomic, strong) CZPageViewController *pageViewController;
@property (nonatomic, strong) CZImagePageViewController *currentPage;
@property (nonatomic, strong) CZButton *previousPageButton;
@property (nonatomic, strong) CZButton *nextPageButton;
@property (nonatomic, strong) CZFileNameLabel *fileNameLabel;
@property (nonatomic, strong) CZContentChooserController *exportFileFormatChooserController;
@property (nonatomic, strong) CZPostOffice *postOffice;
@property (nonatomic, strong) CZPopoverController *presentedPopoverController;
@property (nonatomic, copy) NSString *tempShareDocumentFilePath;
@property (nonatomic, strong) CZImageEditingViewController *imageEditingController;
@property (nonatomic, strong) CZImageProcessingViewController *imageProcessingViewController;
@property (nonatomic, strong) CZImagePickerTableViewController *imagePickerTableViewController;
@property (nonatomic, weak) NSLayoutConstraint *imageProcessingViewBottomConstraint;
@property (nonatomic, strong) CZDocManager *previousPageDocument;  // it shall be released after CZAlertController dismissed

@end

@implementation CZImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    [self recreatePageViewController];
    
    self.previousPageButton.frame = CGRectMake(kDefaultHorizontalMargin, CGRectGetMidY(self.view.frame) - kDefaultButtonItemHeight / 2, kDefaultButtonItemWidth, kDefaultButtonItemHeight);
    self.nextPageButton.frame = CGRectMake(CGRectGetMaxX(self.view.frame) - kDefaultButtonItemWidth - kDefaultHorizontalMargin, CGRectGetMidY(self.view.frame) - kDefaultButtonItemHeight / 2, kDefaultButtonItemWidth, kDefaultButtonItemHeight);
    self.fileNameLabel.frame = CGRectMake(8, 16 + kDefaultStatusBarHeight, 240, 48.0);
    [self.view addSubview:self.previousPageButton];
    [self.view addSubview:self.nextPageButton];
    [self.view addSubview:self.fileNameLabel];
    
    [self imageTaskDidPresentImageDocument:self.task.docManager filePathOfDocument:self.task.docManager.filePath];
    [self bindViewWithViewModelState];
    self.task.objectiveStatesDelegate = self;
}

- (void)dealloc {
    CZLogv(@"CZImageViewController did dealloc");
    [self unbindViewWithViewModelState];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZDisableMeasurementColorCodingDidChangeNotification object:nil];
}

#pragma mark - Private methods

- (CZImagePageViewController *)createImagePageViewControllerFromImageFile:(NSString *)filePath pageIndex:(NSUInteger)pageIndex {
    if (filePath == nil) {
        return nil;
    }
    
    CZImagePageViewController *pageView = [[CZImagePageViewController alloc] init];
    pageView.pageIndex = pageIndex;
    pageView.filePath = filePath;
    return pageView;
}

- (void)recreatePageViewController {
    if (_pageViewController) {
        [_pageViewController removeFromParentViewController];
        [_pageViewController.view removeFromSuperview];
        self.pageViewController = nil;
    }
    
    CZPageViewController *pageViewController = [[CZPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                                               navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                                             options:@{UIPageViewControllerOptionInterPageSpacingKey: @20}];
    pageViewController.doubleSided = NO;
    pageViewController.dataSource = self;
    pageViewController.delegate = self;
    pageViewController.layoutChangeDelegate = self;
    self.pageViewController = pageViewController;
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.view sendSubviewToBack:self.pageViewController.view];

    UIScrollView *pageScrollView = nil;
    for (id view in [self.pageViewController.view subviews]) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            pageScrollView = view;
            break;
        }
    }
}

- (void)updateUIForNewDocManager:(CZDocManager *)docManager {
    if (docManager) {
        self.fileNameLabel.docManager = docManager;
    } else {
        [self.fileNameLabel setName:[self.currentPage.filePath lastPathComponent]];
        self.fileNameLabel.docManager = nil;
    }

    [self.task updateMagnificationButtonImageTitle];
    [self setNeedsVirtualToolbarItemsUpdate];
}

- (void)shareDocument {
    @autoreleasepool {
        self.tempShareDocumentFilePath = nil;

        NSString *shareFilePath = nil;
        NSString *originFilePath = self.task.docManager.filePath;
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[originFilePath pathExtension]];

        if (fileFormat == kCZFileFormatJPEG) {
            shareFilePath = originFilePath;
        } else {
            NSString *jpegFilePath = [[self.task.docManager.filePath stringByDeletingPathExtension] stringByAppendingPathExtension:kFileExtensionJPEG];
            NSString *fileName = [jpegFilePath lastPathComponent];
            shareFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        }

        if (shareFilePath) {
            [self.postOffice reset];
            if (![[NSFileManager defaultManager] fileExistsAtPath:shareFilePath]) {
                UIImage *image = [self.task.docManager imageWithBurninAnnotations];
                NSData *imageData = [self.task.docManager.imageProperties JPEGRepresentationOfImage:image compression:1.0];
                if ([imageData writeToFile:shareFilePath atomically:YES]) {
                    self.tempShareDocumentFilePath = shareFilePath;
                }
            }
            [self.postOffice addShareItemFromPath:shareFilePath];
            CZToolbarItem *shareToolBarItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierShare];
            [self.postOffice presentActivityViewControllerFromToolbarItem:shareToolBarItem];
        }
    }
}

- (void)bindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.task addObserver:self
                    forKeyPath:observableKeyPath
                       options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                       context:NULL];
    }
}

- (void)unbindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.task removeObserver:self
                       forKeyPath:observableKeyPath
                          context:NULL];
    }
}

- (NSArray <NSString *> *)observableKeyPaths {
    return @[@"isImageLoadedSucceed", @"isEditing"];
}

- (void)updateControlEnabled:(BOOL)enabled {
    self.previousPageButton.enabled = enabled;
    self.nextPageButton.enabled = enabled;
    for (CZToolbarItem *item in self.multitaskViewController.toolbarItems) {
        item.enabled = enabled;
    }
    self.fileNameLabel.enableEdit = enabled;
}

- (void)updatePageControlVisible {
    self.previousPageButton.hidden = !self.task.isPageUpEnabled;
    self.nextPageButton.hidden = !self.task.isPageDownEnabled;
}

- (void)updateControlsWhileImageEditing {
    self.fileNameLabel.hidden = self.task.isEditing;
    self.previousPageButton.hidden = self.task.isEditing;
    self.nextPageButton.hidden = self.task.isEditing;
}

- (void)enterAnnotationEditingMode:(BOOL)isEditing {
    if (isEditing) {
        if (self.imageEditingController) {
            [self.imageEditingController.view removeFromSuperview];
            [self.imageEditingController removeFromParentViewController];
            [self.imageEditingController didMoveToParentViewController:nil];
            self.imageEditingController.delegate = nil;
            self.imageEditingController.dataSource = nil;
            self.imageEditingController = nil;
        }

        CZImagePageViewController *imageViewController = self.currentPage;
        
        if (imageViewController.docManager == nil) {  // if invalid image
            return;
        }
        
        CZImageEditingViewController *imageEditingViewController = [CZImageViewModel imageEditingViewControllerWithEditingMode:CZEditingModeAnnotationBasic];
        self.imageEditingController = imageEditingViewController;
        
        self.imageEditingController.delegate = self;
        self.imageEditingController.dataSource = self;
        
        imageViewController.editing = YES;
        imageViewController.editingMode = CZEditingModeAnnotationBasic;

        if ([self.imageEditingController conformsToProtocol:@protocol(CZImageViewEditingDelegate)]) {
            imageViewController.imageEditingDelegate = (id<CZImageViewEditingDelegate>)self.imageEditingController;
        }
        if ([self.imageEditingController conformsToProtocol:@protocol(CZElementLayerDelegate)]) {
            imageViewController.elementLayerDelegate = (id<CZElementLayerDelegate>)self.imageEditingController;
        }
        
        [self addChildViewController:self.imageEditingController];
        [self.view addSubview:self.imageEditingController.view];
        self.imageEditingController.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CZMainToolbarHeight - kDefaultStatusBarHeight);
        [self.imageEditingController didMoveToParentViewController:self];
    } else {
        [self.imageEditingController.view removeFromSuperview];
        [self.imageEditingController removeFromParentViewController];
        [self.imageEditingController didMoveToParentViewController:nil];
        self.imageEditingController.delegate = nil;
        self.imageEditingController.dataSource = nil;
        self.imageEditingController = nil;
    }
}

- (void)updateMainToolBarItemsStates {
    CZToolbarItem *undoItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierUndo];
    CZToolbarItem *redoItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierRedo];
    undoItem.enabled = [self.task.docManager canUndo];
    redoItem.enabled = [self.task.docManager canRedo];
}

- (void)imageProcessingItemAction:(CZToolbarItem *)item {
#if IMAGE_REFACTOR
    if ([self isPageViewSwipping]) {
        return;
    }
#endif
    
#if IMAGE_REFACTOR
    CZMultiphase *multiphase = self.task.docManager.multiphase;
    BOOL hasMultiphase = (multiphase.phaseCount > 0);
    BOOL hasGrainSize = (self.task.docManager.grainSizeResult != nil);
    if (!hasGrainSize && !hasMultiphase) {
#endif
        [self beginImageProcessing];
#if IMAGE_REFACTOR
    } else {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"IMAGE_WARNING_ABOUT_MULTIPHASE") level:CZAlertLevelWarning];
        [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        [alert addActionWithTitle:L(@"YES") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
            
            if (hasMultiphase) {
                CZChangeMultiphaseAction *action = [[CZChangeMultiphaseAction alloc] initWithDocManager:self.task.docManager
                                                                                             multiphase:[[CZMultiphase alloc] init]
                                                                                     originalMultiphase:self.task.docManager.multiphase];
                if (action) {
                    [compoundAction addAction:action];
                }
            }
            
            if (hasGrainSize) {
                CZGrainSizeAction *action = [[CZGrainSizeAction alloc] initWithDocManager:self.docManager
                                                                          grainSizeResult:nil];
                if (action) {
                    [compoundAction addAction:action];
                }
            }
            
            if (compoundAction.count > 0) {
                [self.task.docManager pushAction:compoundAction];
            }
            
            [self beginImageProcessing];
        }];
        [alert presentAnimated:YES completion:nil];
    }
#endif
}

- (void)beginImageProcessing {
#if IMAGE_REFACTOR
    if (!_imageProcessBar) {
        [self setupImageProcessBar];
    }
    
    if (!_imageProcessToolBar) {
        // Initialize the image process segment control content
        [self setupImageProcessToolBar];
    }
    
    [self toggleImageProcessIcons:nil];
    [self turnOffDrawingTube];
    [self hideAllToolBoxViews];
#endif
    
    // TODO: Better to disable all the buttons in display curve mode>
    [self.currentPage.imageTool dismissInteractiveViews];
    
    // Disable display curve while image processing.
    self.currentPage.imageTool.overExposureEnabled = NO;
    [self.currentPage.imageTool setIgnoresDisplayCurve:YES];
    [self.currentPage.imageTool invalidate];
    
#if IMAGE_REFACTOR
    _displayCurveDisabledIcon.hidden = YES;
    _imageBar.hidden = YES;
    [self hideControls:YES];
    _imageProcessBar.hidden = NO;
    _imageProcessView.hidden = NO;
    
    _hasShownImageProcessAlert = NO;
#endif
    
    [self.currentPage fitImageViewIntoScrollView:YES];
    self.currentPage.editing = YES;
    self.currentPage.editingMode = CZEditingModeImageProcessing;
    
    [self addImageProcessingViewController];
    
#if IMAGE_REFACTOR
    [self checkIfEnablePaging];
    
    [self updateControlState];
#endif
}

- (void)cancelImageProcessingItemAction:(CZToolbarItem *)item {
#if IMAGE_REFACTOR
    // Re-enable display curve while image processing.
    self.currentPage.imageTool.overExposureEnabled = self.overExposeButton.isSelected;
    [self.currentPage.imageTool setIgnoresDisplayCurve:NO];
    _displayCurveDisabledIcon.hidden = YES;
#endif
    
    [self.currentPage.imageTool revert];
    self.currentPage.editing = NO;
    
    [self removeImageProcessingViewController];
    
#if IMAGE_REFACTOR
    [self updateControlState];
    _imageProcessBar.hidden = YES;
    _imageBar.hidden = NO;
    _displayOptionsButton.hidden = NO;
    _fileNameLabel.hidden = NO;
    [self hideControls:NO];
    
    [self updateMagnificationSettingAppearance:kObjectiveEdit];
    _hasShownImageProcessAlert = NO;
#endif
}

- (void)applyImageProcessingItemAction:(CZToolbarItem *)item {
#if IMAGE_REFACTOR
    // Re-enable display curve while image processing.
    self.currentPage.imageTool.overExposureEnabled = self.overExposeButton.isSelected;
    [self.currentPage.imageTool setIgnoresDisplayCurve:NO];
    _displayCurveDisabledIcon.hidden = YES;
#endif
    
    [self.currentPage.imageTool apply];
    self.currentPage.editing = NO;
    
    [self removeImageProcessingViewController];
    
#if IMAGE_REFACTOR
    [self updateControlState];
    _imageProcessBar.hidden = YES;
    _imageBar.hidden = NO;
    _displayOptionsButton.hidden = NO;
    _fileNameLabel.hidden = NO;
    [self hideControls:NO];
    
    [self updateMagnificationSettingAppearance:kObjectiveEdit];
    _hasShownImageProcessAlert = NO;
#endif
}

- (void)addImageProcessingViewController {
    if (self.imageProcessingViewController != nil) {
        return;
    }
    
    CZImageProcessingViewController *imageProcessingViewController = [[CZImageProcessingViewController alloc] initWithImageTool:self.currentPage.imageTool];
    
    [self addChildViewController:imageProcessingViewController];
    
    imageProcessingViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:imageProcessingViewController.view];
    [imageProcessingViewController.view.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:16.0].active = YES;
    self.imageProcessingViewBottomConstraint = [imageProcessingViewController.view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(CZMainToolbarHeight + 16.0)];
    self.imageProcessingViewBottomConstraint.active = YES;
    [imageProcessingViewController.view.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-(CZMainToolbarHeight + 16.0)].active = YES;
    [imageProcessingViewController.view.widthAnchor constraintEqualToConstant:432.0].active = YES;
    [imageProcessingViewController.view.heightAnchor constraintEqualToConstant:289.0].active = YES;
    
    [imageProcessingViewController didMoveToParentViewController:self];
    
    self.imageProcessingViewController = imageProcessingViewController;
}

- (void)removeImageProcessingViewController {
    if (self.imageProcessingViewController == nil) {
        return;
    }
    
    [self.imageProcessingViewController willMoveToParentViewController:nil];
    [self.imageProcessingViewController.view removeFromSuperview];
    [self.imageProcessingViewController removeFromParentViewController];
    self.imageProcessingViewController = nil;
}

- (void)clearPageViewControllerCache {
    CZLogv(@"clearPageViewControllerCache");
    
    CZImagePageViewController *currentPage = self.currentPage;
    if (currentPage == nil) {
        return;
    }
    
    [_pageViewController setViewControllers:@[currentPage]
                                  direction:UIPageViewControllerNavigationDirectionForward
                            invalidateCache:YES
                                   animated:NO   // use NO to avoid flick
                                 completion:NULL];
}

- (void)cleanupImageController {
    [self enterAnnotationEditingMode:NO];
    
    // clean up
    if (self.pageViewController) {
        [self.pageViewController removeFromParentViewController];
        [self.pageViewController.view removeFromSuperview];
        self.pageViewController = nil;
    }
    self.currentPage = nil;
    [self updateUIForNewDocManager:nil];
}

#pragma mark - Action

- (void)reportDocument {
#if IMAGE_REFACTOR
    [self turnOffDrawingTube];
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    MBFingerTipWindow *window = (MBFingerTipWindow *)mainWindow;
    window.turnOn = NO;
    [window updateFingertipsAreActive];
#endif
    CZReportTemplateViewController *reportTemplateViewController = [[CZReportTemplateViewController alloc] init];
    reportTemplateViewController.editDataSource = self;
    [self.navigationController pushViewController:reportTemplateViewController animated:YES];
}

- (void)exportCSVFile {
    //TODO: Expprt CSV File
}

- (void)magnificationItemAction:(CZToolbarItem *)item {
    BOOL isMacro = [self.task isMacroImageMicroscopeMode];
    
    if (isMacro) {
        //TODO: implement the marco scaling 
    } else {
        if (self.task.docManager.isImageSnapped) {
            BOOL isStereo = [self.task isStereoMicroscopeMode];
            if (isStereo) {
                [self zoomClickStopItemSelectAction:item];
            } else {
                [self objectiveItemSwitchAction:item];
            }
        } else {
            if (self.imagePickerTableViewController == nil) {
                self.imagePickerTableViewController = [[CZImagePickerTableViewController alloc] init];
            }
            self.imagePickerTableViewController.preferredContentSize = CGSizeMake(512.0f, 577.0f);
            self.imagePickerTableViewController.selectionDelegate = self;
            self.imagePickerTableViewController.exceptionFilePath = self.task.docManager.filePath;
            
            CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:self.imagePickerTableViewController];
            popover.delegate = self;
            [popover presentFromToolbarItem:item animated:YES completion:nil];
        }
    }
}

- (void)objectiveItemSwitchAction:(CZToolbarItem *)item {
    CZObjectiveSelectionViewController *objectiveSelectionViewController = [[CZObjectiveSelectionViewController alloc] initWithMicroscopeModel:self.task.microscopeModel];
    objectiveSelectionViewController.delegate = self;
    
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:objectiveSelectionViewController];
    popover.delegate = self;
    [popover presentFromToolbarItem:item animated:YES completion:nil];
}

- (void)zoomClickStopItemSelectAction:(CZToolbarItem *)item {
    CZZoomClickStopSelectionViewController *zoomClickStopSelectionViewController = [[CZZoomClickStopSelectionViewController alloc] initWithMicroscopeModel:self.task.microscopeModel];
    zoomClickStopSelectionViewController.delegate = self;
    
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:zoomClickStopSelectionViewController];
    popover.delegate = self;
    [popover presentFromToolbarItem:item animated:YES completion:nil];
}

- (void)switchToPreviousPage:(CZButton *)sender {
    
}

- (void)switchToNextPage:(CZButton *)sender {
    
}

#pragma mark - Notifications

- (void)disableMeasurementColorCodingDidChange:(NSNotification *)note {
    CZElementLayer *elementLayer = self.currentPage.docManager.elementLayer;
    if ([elementLayer.delegate respondsToSelector:@selector(layerUpdateAllMeasurements:)]) {
        [elementLayer.delegate layerUpdateAllMeasurements:elementLayer];
    }
}

#pragma mark - CZTaskDelegate

- (instancetype)initWithTask:(CZImageTask *)task {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        NSAssert([task isKindOfClass:[CZImageTask class]], @"%@ is not an image task", task);
        _task = task;
        _task.dataSource = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableMeasurementColorCodingDidChange:) name:CZDisableMeasurementColorCodingDidChangeNotification object:nil];
    }
    return self;
}

- (UIViewController *)taskViewController {
    return self;
}

- (CZTask *)secondaryTaskForTask:(CZTask *)task {
    return self.task.manager.filesTask;
}

- (void)taskDidEnterForeground:(CZTask *)task {
    CZLogv(@"Did enter Foreground.");
    [[CZKeyboardManager sharedManager] addObserver:self];
}

- (void)taskWillEnterBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    CZLogv(@"Will enter Background.");
    if (self.task.docManager.isModified || [self.task hasUnsavedMultichannelAction]) {
        NSString *message = L(@"IMAGE_CLOSE_MSG");
        message = [NSString stringWithFormat:message, [self.task.docManager.filePath lastPathComponent]];
        @weakify(self);
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:message level:CZAlertLevelWarning];
        [alertController addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            self.task.isEditing = NO;
            [self.task saveMultichannelAction];
            [self.task.docManager scheduleSaveInDefaultFormatWithDelegate:self];
            self.taskWillEnterBackgroundCompletionHandler = completionHandler;
        }];

        [alertController addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            self.task.isEditing = NO;
            [self.task discardMultichannelAction];
            [self.task.docManager discardBackup];
            [self cleanupImageController];
            NSString *filePath = self.task.docManager.originalFilePath != nil ? self.task.docManager.originalFilePath : self.task.docManager.filePath;
            [self.task openFileAtPath:filePath];
            completionHandler(CZTaskEventAllow);
        }];
        
        [alertController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            completionHandler(CZTaskEventDisallow);
        }];
        
        [alertController presentAnimated:YES completion:nil];
    } else {
        completionHandler(CZTaskEventAllow);
    }
}

- (void)taskDidEnterBackground:(CZTask *)task {
    CZLogv(@"Did enter Background.");
    [self removeImageProcessingViewController];
    [[CZKeyboardManager sharedManager] removeObserver:self];
}

#pragma mark - CZImageTaskDataSource

- (CZDocManager *)currentDocManager {
    return self.currentPage.docManager;
}

- (NSUInteger)currentPageIndex {
    return self.currentPage.pageIndex;
}

#pragma mark - CZMainToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarPrimaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    return @[CZToolbarItemIdentifierMagnification, CZToolbarItemIdentifierAnnotation, CZToolbarItemIdentifierHistogram];
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarSecondaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    NSArray<CZToolbarItemIdentifier> *itemIdentifiers = @[CZToolbarItemIdentifierOverexposureIndicator, CZToolbarItemIdentifierImageProcessing, CZToolbarItemIdentifierLaserPointer, CZToolbarItemIdentifierSplitView, CZToolbarItemIdentifierDrawingTube];
#if DEBUG
    itemIdentifiers = [itemIdentifiers arrayByAddingObject:CZToolbarItemIdentifierFileInfo];
#endif
    return itemIdentifiers;
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarCenterItemIdentifiers:(CZToolbar *)mainToolbar {
    if (self.task.docManager.isModified) {
        return @[CZToolbarItemIdentifierUndo, CZToolbarItemIdentifierRedo, CZToolbarItemIdentifierSave];
    } else {
        return @[CZToolbarItemIdentifierShare, CZToolbarItemIdentifierSaveAs, CZToolbarItemIdentifierReport];
    }
}

#pragma mark - CZVirtualToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarLeadingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarCenterItemIdentifiers:(CZToolbar *)virtualToolbar {
    if ([self.mainToolbar.items.firstObject.identifier isEqualToString:[self mainToolbarSecondaryLeadingItemIdentifiers:self.mainToolbar].firstObject]) {
        return @[CZToolbarItemIdentifierChannels];
    } else {
        return nil;
    }
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarTrailingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

#pragma mark - CZToolbarDelegate

- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier {
    return [super toolbar:toolbar makeItemForIdentifier:identifier];
}

- (CZToolbarItem *)makeToolbarItemForChannels {
    [self.task updateChannels];
    
    CZToolbarItem *item = [super makeToolbarItemForChannels];
    CZChannelButtonGroup *buttonGroup = [[CZChannelButtonGroup alloc] initWithChannels:self.task.channels];
    buttonGroup.delegate = self;
    item.view = buttonGroup;
    return item;
}

- (CZToolbarItem *)makeToolbarItemForAnnotationTools {
    CZToolbarItem *item = [super makeToolbarItemForAnnotationTools];
    CZAnnotationPickerView *annotationPickerView = [[CZAnnotationPickerView alloc] initWithDataSource:self];
    annotationPickerView.delegate = self;
    annotationPickerView.dataSource = self;
    item.view = annotationPickerView;
    return item;
}

- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierUndo]) {
        item.enabled = [self.task.docManager canUndo];
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierRedo]) {
        item.enabled = [self.task.docManager canRedo];
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierMagnification]) {
        item.button.enabled = self.task.magnificationButtonEnabled;
        [item.button setTitle:self.task.magnificationButtonTitle forState:UIControlStateNormal];
        [item.button setImage:self.task.magnificationButtonImage forState:UIControlStateNormal];
        [item alignImageAndTitle];
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierImageProcessing]) {
        item.enabled = ![self.task.docManager.document isMultichannelImage];
    }
}

- (void)toolbar:(CZToolbar *)toolbar didPushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem {
    if ([fromItem.identifier isEqualToString:CZToolbarItemIdentifierMore]) {
        [self setNeedsVirtualToolbarItemsUpdate];
    }
    
    if ([fromItem.identifier isEqualToString:CZToolbarItemIdentifierAnnotation]) {
        self.task.isEditing = YES;
    }
}

- (void)toolbar:(CZToolbar *)toolbar didPopItems:(NSArray<CZToolbarItem *> *)items byItem:(CZToolbarItem *)byItem toItem:(CZToolbarItem *)toItem {
    if ([toItem.identifier isEqualToString:CZToolbarItemIdentifierMore]) {
        [self.task saveMultichannelAction];
        [self setNeedsMainToolbarItemsUpdate];
        [self setNeedsVirtualToolbarItemsUpdate];
    }
    
    if ([toItem.identifier isEqualToString:CZToolbarItemIdentifierAnnotation]) {
        self.task.isEditing = NO;
        [self setNeedsMainToolbarItemsUpdate];
    }
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    CZLogv(@"ToolBar did Select item: %@", item.identifier);
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierMagnification]) {
        [self magnificationItemAction:item];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierSaveAs]) {
        CZContentChooserController *exportChooserControoler = [[CZContentChooserController alloc] initWithContent:[CZImageViewModel saveFileFormats] title:@"Export" preferredContentSize:CGSizeMake(304.0, 384.0)];
        exportChooserControoler.delegate = self;
        
        CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:exportChooserControoler];
        popover.delegate = self;
        popover.backgroundColor = [UIColor cz_gs100];
        popover.adjustsPopoverPositionWhenKeyboardAppears = YES;
        [popover presentFromToolbarItem:item animated:YES completion:nil];
        self.presentedPopoverController = popover;
        self.exportFileFormatChooserController = exportChooserControoler;
        
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierFileInfo]) {
        CZFileInfoViewController *fileInfoViewController = [[CZFileInfoViewController alloc] initWithFileAtPath:self.task.docManager.filePath];
        [self presentViewController:fileInfoViewController animated:YES completion:nil];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierRedo]) {
        [self.currentPage redoAction];
        [self updateMainToolBarItemsStates];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierUndo]) {
        [self.currentPage undoAction];
        [self updateMainToolBarItemsStates];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierReport]) {
        [self reportDocument];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierSave]){
        [self.task.docManager scheduleSaveInDefaultFormatWithDelegate:self];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierShare]) {
        item.selected = YES;
#if IMAGE_REFACTOR
        [self turnOffDrawingTube];
#endif
        [self shareDocument];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierImageProcessing]) {
        [self imageProcessingItemAction:item];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierCancel]) {
        [self cancelImageProcessingItemAction:item];
        [toolbar popItems];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierConfirm] && [item.parentItem.identifier isEqualToString:CZToolbarItemIdentifierImageProcessing]) {
        [self applyImageProcessingItemAction:item];
        [toolbar popItems];
        return YES;
    }
    
    return NO;
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSArray *localFiles = self.task.localFiles;
    if (localFiles.count == 0) {
        return nil;
    }
    
    NSInteger currentIndex = [(CZImagePageViewController *)viewController pageIndex];
    NSInteger nextIndex = (currentIndex != NSNotFound) ? currentIndex + 1 : 0;
    NSString *nextFilePath = (nextIndex < [localFiles count]) ? localFiles[nextIndex] : nil;

    CZImagePageViewController *nextViewController = [self createImagePageViewControllerFromImageFile:nextFilePath pageIndex:nextIndex];
    nextViewController.delegate = self;
    return nextViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSArray *localFiles = self.task.localFiles;
    if (localFiles.count == 0) {
        return nil;
    }
    
    NSUInteger currentIndex = [(CZImagePageViewController *)viewController pageIndex];
    NSUInteger previousIndex = (currentIndex != NSNotFound) ? currentIndex - 1 : 0;
    NSString *previousFilePath = (previousIndex < [localFiles count]) ? localFiles[previousIndex] : nil;
    
    CZImagePageViewController *previousViewController = [self createImagePageViewControllerFromImageFile:previousFilePath pageIndex:previousIndex];
    previousViewController.delegate = self;
    return previousViewController;
}

#pragma mark - UIPageViewControllerDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers {
    for (CZImagePageViewController *pageViewController in pendingViewControllers) {
#if IMAGE_REFACTOR
        pageViewController.overExposeEnableAfterNewImageLoaded = self.overExposeButton.isSelected;
#endif
        pageViewController.currentDisplayCurve = self.task.currentDisplayCurve;
        [pageViewController loadThumbnail];
        [pageViewController fitImageViewIntoScrollView:NO];
    }
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        if ([self.task hasUnsavedMultichannelAction]) {
            [self.task saveMultichannelAction];
        }
        
        for (CZImagePageViewController *pageViewController in previousViewControllers) {
            [pageViewController cancelLoadingImage];
        }
        
        @autoreleasepool {
            NSArray *viewControllers = pageViewController.viewControllers;
            if (viewControllers.count > 0) {
                self.currentPage = viewControllers[0];
            } else {
                self.currentPage = nil;
            }
        }
        
        CZImagePageViewController *currentPage = self.currentPage;
        [currentPage loadImage];
#if IMAGE_REFACTOR
        currentPage.magnifyView = self.magnifyView;
        
#endif
        if (previousViewControllers.count) {
            CZImagePageViewController *previousPage = (CZImagePageViewController *)previousViewControllers[0];
            previousPage.magnifyView = nil;
            
            if (previousPage.docManager.isModified || previousPage.docManager.isImageNew) {
                NSString *filePath = previousPage.docManager.filePath;
                @weakify(self);
                dispatch_async(dispatch_get_main_queue(), ^ {  // delay show the alert view, to avoid alert view animation conflict with page view animation, which cause crash!!!
                    NSString *message = L(@"IMAGE_CLOSE_MSG");
                    message = [NSString stringWithFormat:message, [filePath lastPathComponent]];
                    CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:message level:CZAlertLevelWarning];
                    [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDestructive handler:^(__kindof CZDialogController *dialog, CZDialogAction *action) {
                        @strongify(self);
                        [self.previousPageDocument discardBackup];
                        [self clearPageViewControllerCache];
                        self.previousPageDocument = nil;
                    }];
                    [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(__kindof CZDialogController *dialog, CZDialogAction *action) {
                        @strongify(self);
                        [self.previousPageDocument scheduleSaveInDefaultFormatWithDelegate:self];
                        self.previousPageDocument = nil;
                    }];
                    [alert presentAnimated:YES completion:nil];
                });
                self.previousPageDocument = previousPage.docManager;
            }
        }
        
        CZDocManager *docManager = currentPage.docManager;
        docManager.isImageSnapped = NO;
        docManager.isImageNew = NO;
        [self updateUIForNewDocManager:docManager];
#if IMAGE_REFACTOR
        [self dismissControls];
        
        [self.delegate imageViewController:self willCurrentPageChangeTo:self.currentPage];
#endif
        [self updatePageControlVisible];
        self.currentPage.lockScrolling = self.task.isLockScrolling;
    }
}

#pragma mark - CZPageViewControllerDelegate

- (void)didChangeLayout {
    
}

#pragma mark - CZImagePageViewControllerDelegate

- (void)imagePageViewController:(CZImagePageViewController *)controller docManagerModified:(CZDocManager *)docManager {
#if IMAGE_REFACTOR
    if ([NSThread isMainThread]) {
//        _imageControlState = docManager.isModified ? kImageEdit : kImageView;
//        [self updateControlState];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^ {
//            self.imageControlState = docManager.isModified ? kImageEdit : kImageView;
//            [self updateControlState];
        });
    }
#endif
}

- (BOOL)imageView:(UIView *)imageView handleTapGesture:(UIGestureRecognizer *)recognizer {
#if IMAGE_REFACTOR
    if (self.currentPage.isEditing) {
        return NO;
    }
    
    // dismiss display option view
    CGPoint pointInView = [recognizer locationInView:self.view];
    
    if (!self.tas.isFullScreen && !self.isSplitScreen) {
        if (!CGRectContainsPoint(self.displayOptionsView.frame, pointInView)) {
            self.displayOptionsView.hidden = YES;
            self.displayOptionsButton.hidden = NO;
            self.displayOptionsButton.selected = NO;
        }
        if ([self isMacroImageMicroscopeMode]) {
            return NO;
        } else {
            if ([self isStereoMicroscopeMode]) {
                if (!CGRectContainsPoint(self.zoomSettingView.frame, pointInView)) {
                    self.zoomSettingView.hidden = YES;
                    self.microSettingButton.hidden = NO;
                }
            } else {
                if (!CGRectContainsPoint(self.objectiveSettingView.frame, pointInView)) {
                    self.objectiveSettingView.hidden = YES;
                    self.microSettingButton.hidden = NO;
                }
            }
        }
    } else {
        BOOL shouldShowButton = !CGRectContainsPoint([_displayOptionsView frame], pointInView) ||
        (self.displayOptionsView.isHidden && self.displayOptionsButton.isHidden);
        if (shouldShowButton) {
            self.displayOptionsView.hidden = YES;
            self.displayOptionsButton.hidden = NO;
            self.displayOptionsButton.selected = NO;
            self.fullscreenLastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
        }
    }
#endif
    return YES;
}

- (void)imagePageViewControllerDidFinishLoading:(CZImagePageViewController *)controller {
    if (self.currentPage == controller) {
        CZDocManager *docManager = controller.docManager;
        [self updateUIForNewDocManager:docManager];
        
        NSString *mnaLevel = [[NSUserDefaults standardUserDefaults] stringForKey:kMNALevel];
        BOOL hasMultiphase = (docManager.analyzer2dResult != nil);
        BOOL hasGrainSize = (docManager.grainSizeResult != nil);
        if ((hasMultiphase || hasGrainSize) && [mnaLevel isEqualToString:kMNALevelAdvanced]) {
            // If multiphase toggle was off when an image with multiphase is opened, enable the option.
#if IMAGE_REFACTOR
            if (!self.multiphaseButton.selected) {
                [self displayOptionsView:self.displayOptionsView didSelectOptionButton:self.multiphaseButton];
            }
#endif
            controller.imageTool.multiphaseEnabled = YES;
        } else {
#if IMAGE_REFACTOR
            controller.imageTool.multiphaseEnabled = self.multiphaseButton.isSelected;
#endif
        }
        
        if (controller.imageTool.multiphaseEnabled) {
            [controller invalidateMultiphaseArea];
            controller.needInvalidateMultiphaseOverlayLayer = NO;
            [controller updateGrainSizeLayer];
        }
    }
#if IMAGE_REFACTOR
    if (self.isSecondary) {
        [controller ennablePinchDismiss:YES];
    }
    
    //if connected with DCM Service, when user change the page index, reapply the configuration
    if ([CZDCMManager defaultManager].isCMServerConnected) {
        [self configurationApplyNotification:nil];
    }
#endif
}

- (void)imagePageViewControllerDidPinchDismiss:(CZImagePageViewController *)controller {
#if IMAGE_REFACTOR
    if ([self.delegate respondsToSelector:@selector(imageViewControllerDidPinchDismiss:)]) {
        [self.delegate imageViewControllerDidPinchDismiss:self];
    }
#endif
}

#pragma mark - CZImageTaskDelegate

- (void)imageTaskDidPresentImageDocument:(CZDocManager *)docManager filePathOfDocument:(NSString *)filePath {
    self.task.lastOpenedFilePath = nil;  // clear last opened file path, since it's override by current opened filePath
    
#if IMAGE_REFACTOR
    
    [self resetImageTab];
    
    _imageControlState = kImageView;
    [self updateControlState];
    
    if (_magnifyView == nil) {
        _magnifyView = [[CZMagnifierView alloc] initWithFrame:self.view.frame];
        _magnifyView.magnifyFactor = 3.0;
        _magnifyView.minimumMagnifyFactor = 5.0 * [UIScreen mainScreen].scale;
        CGRect frame = _magnifyView.frame;
        frame.origin.x += kFunctionBarWidth;
        _magnifyView.frame = frame;
    }
#endif
    [self recreatePageViewController];
    
    CGRect pageFrame = self.view.frame;
#if IMAGE_SPLIT_VIEW
    if (self.isSplitScreen || self.isFullScreen) {
        pageFrame.origin = CGPointZero;
    } else {
#endif
    pageFrame.origin.x = 0;
    pageFrame.origin.y = kDefaultStatusBarHeight;
    pageFrame.size.height -= CZMainToolbarHeight + kDefaultStatusBarHeight;
#if IMAGE_SPLIT_VIEW
    }
#endif

    self.pageViewController.view.frame = pageFrame;
    
    CZImagePageViewController *pageViewController = [[CZImagePageViewController alloc] init];
    //    pageViewController.magnifyView = self.magnifyView;
    pageViewController.delegate = self;
    self.currentPage = pageViewController;
    _pageViewController.dataSource = self;
    
    pageViewController.imageLoaded = YES;
    [pageViewController setDocManager:docManager];
    NSArray *tempArray = [[NSArray alloc] initWithObjects:pageViewController, nil];
    
    
    [_pageViewController setViewControllers:tempArray
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:NULL];
#if IMAGE_SPLIT_VIEW
    if (self.isSecondary) {
        [pageViewController ennablePinchDismiss:YES];
    }
#endif
    
#if IMAGE_REFACTOR
    if (docManager.document) {
        pageViewController.overExposeEnableAfterNewImageLoaded = self.overExposeButton.isSelected;
        pageViewController.currentDisplayCurve = self.currentDisplayCurve;
        pageViewController.imageTool.overExposureEnabled = pageViewController.overExposeEnableAfterNewImageLoaded;
        
        NSString *mnaLevel = [[NSUserDefaults standardUserDefaults] stringForKey:kMNALevel];
        BOOL hasMultiphase = (docManager.analyzer2dResult != nil);
        BOOL hasGrainSize = (docManager.grainSizeResult != nil);
        
        if ((hasMultiphase || hasGrainSize) && [mnaLevel isEqualToString:kMNALevelAdvanced]) {
            // If multiphase toggle was off when an image with multiphase is opened, enable the option.
            if (!self.multiphaseButton.selected) {
                [self displayOptionsView:self.displayOptionsView didSelectOptionButton:self.multiphaseButton];
            }
            pageViewController.imageTool.multiphaseEnabled = YES;
        } else {
            pageViewController.imageTool.multiphaseEnabled = self.multiphaseButton.isSelected;
        }
        
        [pageViewController.imageTool applyDisplayCurvePreset:self.currentDisplayCurve];
        if (self.multiphaseButton.isSelected) {
            if (hasMultiphase) {
                [pageViewController invalidateMultiphaseArea];
            }
            if (hasGrainSize) {
                [pageViewController updateGrainSizeLayer];
            }
        }
    }
#else
    if (docManager.document) {
        pageViewController.currentDisplayCurve = self.task.currentDisplayCurve;
        [pageViewController.imageTool applyDisplayCurvePreset:self.task.currentDisplayCurve];
    }
#endif
    
    pageViewController.filePath = filePath;
    NSUInteger found = [self.task.localFiles indexOfObject:filePath];
    pageViewController.pageIndex = (found == NSNotFound) ? -1 : found;
    
    pageViewController.imageLoaded = YES;

    [self.fileNameLabel setName:[filePath lastPathComponent]];

    [self updateUIForNewDocManager:docManager];
#if IMAGE_REFACTOR
    if (docManager.isImageNew) {
        [self hideCloseButton:YES];
        self.deleteAfterSnapButton.hidden = NO;
        self.displayOptionsButton.enabled = FALSE;
        [self saveFileAction];
    }
#endif
}

- (void)imageTaskPageControllerDataSourceDidUpdate {
    CZImagePageViewController *currentPage = self.currentPage;
    if (currentPage) {
        CZDocManager *docManager = currentPage.docManager;
        NSString *filePath = (docManager == nil) ? currentPage.filePath : docManager.filePath;
        NSUInteger found = [self.task.localFiles indexOfObject:filePath];
        
        if (found != NSNotFound) {
            currentPage.pageIndex = found;
            if (!currentPage.isEditing) {
                [self clearPageViewControllerCache];
            }
        } else {
            // if image view is at backgroud and the file opened is deleted or renamed
            if (docManager &&
                ![[NSFileManager defaultManager] fileExistsAtPath:docManager.filePath] &&
                !self.task.imageViewModel.isImageSaving &&
                !docManager.isModified &&
                !docManager.isImageNew) {
                
                // clean up the image document
                CZLogv(@"clean up the image document, if file is not exist.");
                [self.multitaskViewController.multitaskManager terminateTask:self.task completionHandler:^(CZTaskEventDisposition disposition) {
                    [self cleanupImageController];
                }];
            }
        }
    } else if (self.task.lastOpenedFilePath) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:self.task.lastOpenedFilePath]) {
            self.task.lastOpenedFilePath = nil;
#if IMAGE_REFACTOR
            [[CZAppDelegate get].tabBar disableTabAtIndex:kCZImageViewController];
#endif
        }
    }
}

- (void)imageTaskWillCompositeMultichannelImage:(CZImageTask *)task {
    [self showWaitingHUD:YES];
}

- (void)imageTaskDidCompositeMultichannelImage:(CZImageTask *)task {
    [self showWaitingHUD:NO];
}

#pragma mark - CZDocManagerDelegate

- (void)docManager:(CZDocManager *)docManager didSaveFiles:(NSArray *)files success:(BOOL)success {
    if (self.task.docManager == docManager) {
        [self.fileNameLabel setName:[docManager.filePath lastPathComponent]];

        self.task.imageViewModel.isImageSaving = NO;

#if IMAGE_REFACTOR
        _imageControlState = success ? kImageView : kImageEdit;
        [self updateControlState];
#endif
        if (!success) {
            [[CZToastManager sharedManager] showToastMessage:L(@"FAILED_SAVE_IMAGE_MSG") sourceRect:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        }
    }
#if IMAGE_REFACTOR
    [self restoreFileOverwriteOption];
#endif
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self setNeedsMainToolbarItemsUpdate];
    CZLogv(@"did save docManager: %@, files: %@", docManager, files);
}

- (void)docManager:(CZDocManager *)docManager willSaveFiles:(NSArray *)files {
    if (self.task.docManager == docManager) {
        self.task.imageViewModel.isImageSaving = YES;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setColor:kSpinnerBackgroundColor];
    [hud setLabelText:L(@"BUSY_INDICATOR_PLEASE_WAIT")];
    CZLogv(@"will save docManager: %@, files: %@", docManager, files);
}

#pragma mark - CZDocSaveDelegate
- (void)performOverwriteWithSourceFiles:(NSArray *)sourceFiles
                       destinationFiles:(NSArray *)destinationFiles
                      overwirteYesBlock:(void (^)(NSDictionary *))overwirteYesBlock
                       overwirteNoBlock:(void (^)(NSDictionary *))overwirteNoBlock
                    forceOverwriteBlock:(void (^)(void))forceOverwriteBlock {
    void (^taskEnterBackground)(void) = ^{
        if (self.taskWillEnterBackgroundCompletionHandler) {
            self.taskWillEnterBackgroundCompletionHandler(CZTaskEventAllow);
            self.taskWillEnterBackgroundCompletionHandler = nil;
        }
    };
    
    CZFileOverwriteController *fileOverwriteController = [[CZFileOverwriteController alloc] initWithSource:sourceFiles
                                                                                               destination:destinationFiles];
    fileOverwriteController.overwriteYesBlock = ^(NSDictionary *overwriteFilesDic) {
        overwirteYesBlock(overwriteFilesDic);
        taskEnterBackground();
    };
    fileOverwriteController.overwriteNoBlock = ^(NSDictionary *overwriteFilesDic) {
        overwirteNoBlock(overwriteFilesDic);
        taskEnterBackground();
    };
    if (![fileOverwriteController checkOverwriteFiles:NO]) {
        forceOverwriteBlock();
        taskEnterBackground();
    }
}

#pragma mark - CZContentChooserControllerDelegate

- (void)contentChooserController:(CZContentChooserController *)controller didSelectedItem:(NSInteger)item {
    if (controller == self.exportFileFormatChooserController) {
        @weakify(self);
        [self.presentedPopoverController dismissViewControllerAnimated:YES completion:^{
            @strongify(self);
            [self.currentPage.imageTool dismissInteractiveViews];
            [self.task.docManager scheduleSave:(CZIFileSaveFormat)item withDelegate:self];
        }];
    }
}

#pragma mark - CZPopoverControllerDelegate

- (void)popoverControllerWillDismiss:(CZPopoverController *)popoverController {
    if (popoverController.contentViewController == self.exportFileFormatChooserController) {
        self.exportFileFormatChooserController.delegate = nil;
        self.exportFileFormatChooserController = nil;
    }
    
    self.presentedPopoverController = nil;
}

#pragma mark - CZPostOfficeDelegate
- (void)postOfficeDidDismissActivityController:(CZPostOffice *)postOffice {
}

#pragma mark - CZChannelButtonGroupDelegate

- (void)channelButtonGroup:(CZChannelButtonGroup *)buttonGroup didTapButton:(CZChannelButton *)button atIndex:(NSUInteger)index {
    if (button.isSelected) {
        NSInteger numberOfSelectedButtonsButThis = 0;
        for (UIButton *channelButton in buttonGroup.buttons) {
            if (channelButton != button && channelButton.isSelected) {
                numberOfSelectedButtonsButThis++;
            }
        }
        if (numberOfSelectedButtonsButThis == 0) {
            return;
        }
    }
    
    button.selected = !button.isSelected;
    button.channel.selected = button.isSelected;
}

#pragma mark - CZAnnotationPickerViewDelegate
- (void)annotationPickerView:(CZAnnotationPickerView *)annotationPickerView didSelectAnnotationID:(CZAnnotationButtonID)selectedAnnotionButtonID {
    if ([self.imageEditingController isKindOfClass:[CZAnnotationEditingViewController class]]) {
        CZAnnotationEditingViewController *imageEditingController = (CZAnnotationEditingViewController *)self.imageEditingController;
        [imageEditingController annotationButtonPressed:selectedAnnotionButtonID on:NO];
    }
}

#pragma mark - CZAnnoatationPickerViewDataSource
- (NSArray<NSNumber *> *)annotationButtonsOfAnnotationPickerView:(CZAnnotationPickerView *)annotationPickerView {
    if (self.task.isLicenseEnabled) {
        return @[@(CZAnnotationButtonLine),
                 @(CZAnnotationButtonSquare),
                 @(CZAnnotationButtonCircle),
                 @(CZAnnotationButtonPolygon),
                 @(CZAnnotationButtonPolyline),
                 @(CZAnnotationButtonSplineContour),
                 @(CZAnnotationButtonSpline),
                 @(CZAnnotationButtonArrow),
                 @(CZAnnotationButtonAngle),
                 @(CZAnnotationButtonDisconnectedAngle),
                 @(CZAnnotationButtonCount),
                 @(CZAnnotationButtonCalipers),
                 @(CZAnnotationButtonMultiCalipers),
                 @(CZAnnotationButtonText),
                 @(CZAnnotationButtonScaleBar)];
    } else {
        return @[@(CZAnnotationButtonLine),
                 @(CZAnnotationButtonSquare),
                 @(CZAnnotationButtonCircle),
                 @(CZAnnotationButtonPolygon),
                 @(CZAnnotationButtonArrow),
                 @(CZAnnotationButtonAngle),
                 @(CZAnnotationButtonCount),
                 @(CZAnnotationButtonText),
                 @(CZAnnotationButtonScaleBar)];
    }
    return nil;
}

#pragma mark - CZImageEditingViewControllerDelegate

- (void)viewControllerDidBeginEditing:(UIViewController *)viewController {
    if (self.imageEditingController == viewController) {
        
        CZImagePageViewController *currentPage = self.currentPage;
        if (currentPage.editingMode == CZEditingModeAnnotationBasic ||
            currentPage.editingMode == CZEditingModePCB) {
            
            [self.currentPage beginEditingAnnotation];
        }
    }
}

- (void)viewControllerDidEndEditing:(UIViewController *)viewController {
    if (self.imageEditingController == viewController) {
        CZImagePageViewController *currentPage = self.currentPage;
#if IMAGE_REFACTOR
        // Enable multiphase visibility when coming out of editing mode.
        if (currentPage.imageTool.multiphaseEnabled) {
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagOverExposure
                                        enable:NO];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagOverExposure
                                      selected:NO];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagMultiphase
                                        enable:YES];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagMultiphase
                                      selected:YES];
        } else if (currentPage.imageTool.overExposureEnabled) {
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagOverExposure
                                        enable:YES];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagOverExposure
                                      selected:YES];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagMultiphase
                                        enable:NO];
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagMultiphase
                                      selected:NO];
        }
#endif
        if (currentPage.editingMode == CZEditingModeAnnotationBasic ||
            currentPage.editingMode == CZEditingModePCB) {
            [currentPage endEditingAnnotation];
        }
        
        currentPage.editing = NO;
        if ((id)viewController == currentPage.imageEditingDelegate) {
            currentPage.imageEditingDelegate = nil;
            currentPage.elementLayerDelegate = nil;
        }
        self.imageEditingController = nil;
        
        //FIXME: Show the hidden annotations
        if (currentPage.editingMode == CZEditingModeMultiphase || currentPage.editingMode == CZEditingModeParticles) {
            [self.currentPage showAllAnnotations];
        }
        
#if IMAGE_REFACTOR
        const BOOL isExitFromPCBMode = currentPage.editingMode == CZEditingModePCB;
        
        if (isExitFromPCBMode) {
            if (currentPage.docManager.shortcutMode == kCZShortcutModePCBMeasurement) {
                [self annotationMeasurementShortcutDone];
                [self exitImageTab];
            } else {
                [self annotationMeasurementShortcutNoneDone];
            }
        }
#endif
    }
}

- (NSUInteger)viewControllerBeginCropping:(UIViewController *)viewController {
    CZImageTool *imageTool = self.currentPage.imageTool;
    [imageTool updateCroppingModeWithROIRegion];
    
    CZCroppingMode mode = [imageTool croppingMode];
    if (mode == CZCroppingModeUnknown) {
        [imageTool updateCroppingModeWithDefaultROI];
        mode = [imageTool croppingMode];
    }
    
    [imageTool showInteractiveCroppingOnView:self.view
                                   withFrame:self.pageViewController.view.frame];
    
    if (mode == CZCroppingModeUnknown) {
        mode = CZCroppingModeRectangle;
        [imageTool setCroppingMode:mode];
    }
    
    return mode;
}

- (void)scrollRectToVisible:(CGRect)rect {
    [self.currentPage scrollRectToVisible:rect];
}

- (void)viewControllerDidModifyDocManager:(UIViewController *)viewController {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierAnnotationTools];
    if ([item.view isKindOfClass:[CZAnnotationPickerView class]]) {
        CZAnnotationPickerView *annotationPickerView = (CZAnnotationPickerView *)item.view;
        [annotationPickerView deselectAnnotationButton];
    }
    [self updateMainToolBarItemsStates];
}

#pragma mark - CZImageEditingViewControllerDataSource

- (UIView *)editingImageView {
    return self.currentPage.imageView;
}

- (CZDocManager *)editingImageDocument {
    return self.task.docManager;
}

- (UIView *)imageView {
    return self.currentPage.imageTool.imageView;
}

- (UIScrollView *)imageScrollView {
    return (UIScrollView *)self.currentPage.view;
}

- (id<CZMagnifyDelegate>)magnifyDelegate {
    return self.currentPage;
}

#pragma mark - CZReportTemplateViewControllerDataSource

- (CZDocManager *)reportMainDocManager {
    return self.task.docManager;
}

#pragma mark - CZObjectiveSelectionViewControllerDelegate

- (void)objectiveSelectionViewController:(CZObjectiveSelectionViewController *)objectiveSelectionViewController didSelectObjectiveAtPosition:(NSUInteger)position {
    CZToolbarItem *magnificationItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierMagnification];
    magnificationItem.selected = NO;
    [self.task selectMagnificationAtPosition:position isObjectiveSelection:YES];
}

#pragma mark - CZZoomClickStopSelectionViewControllerDelegate

- (void)zoomClickStopSelectionViewController:(CZZoomClickStopSelectionViewController *)zoomClickStopSelectionViewController didSelectZoomClickStopAtPosition:(NSUInteger)position {
    CZToolbarItem *magnificationItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierMagnification];
    magnificationItem.selected = NO;
    [self.task selectMagnificationAtPosition:position isObjectiveSelection:NO];
}

#pragma mark - CZImagePickerTableViewDelegate

- (void)imagePicker:(CZImagePickerTableViewController *)picker didSelectFile:(NSArray *)fileEntities atIndex:(NSUInteger)index {
    if (picker == self.imagePickerTableViewController) {
        [self.imagePickerTableViewController dismissViewControllerAnimated:YES completion:nil];
        self.imagePickerTableViewController.selectionDelegate = nil;
        self.imagePickerTableViewController = nil;
    }
    CZToolbarItem *magnificationItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierMagnification];
    magnificationItem.selected = NO;
    //TODO: The DocManager should add a method to update the properties with other model;
    NSString *filePath = ((CZFileEntity *)fileEntities[index]).filePath;
    [self.task updateMagnificationWithFilePath:filePath];
}

- (NSString *)imagePicker:(CZImagePickerTableViewController *)picker queryDetailOfFile:(NSString *)filePath {
    CZDocManager *selectedDocManager = [CZDocManager newDocManagerFromFilePath:filePath];
    CZImageProperties *selectedImageProperties = selectedDocManager.imageProperties;
    
    NSString *magnificationString = kNotAvailableMagnification;
    NSString *scaleString = L(@"FILES_INFO_UNKNOWN");
    if (selectedImageProperties) {
        NSNumber *eyepieceFactor = selectedImageProperties.eyepieceMagnification;
        NSNumber *magnification = selectedImageProperties.magnificationFactor;
        
        if (magnification != nil && [magnification floatValue] > 0) {
            if (eyepieceFactor && [eyepieceFactor floatValue] > 0) {
                float magnificationFactor = [magnification floatValue] / [eyepieceFactor floatValue];
                magnification = [NSNumber numberWithFloat:magnificationFactor];
            }
            
            magnificationString = [CZCommonUtils localizedStringFromNumber:magnification precision:2];
            magnificationString = [magnificationString stringByAppendingString:@"x"];
        }
        
        if (selectedImageProperties.scaling > 0) {
            BOOL prefersBigUnit = [CZMicroscopeModel shouldPreferBigUnitByModelName:selectedImageProperties.microscopeModel];
            CGFloat scaling = [CZElement scaling:selectedImageProperties.scaling
                                     ofUnitStyle:[CZElement unitStyle]
                                  prefersBigUnit:prefersBigUnit];
            NSString *unitKey = [CZElement distanceUnitStringByStyle:[CZElement unitStyle]
                                                      prefersBigUnit:prefersBigUnit];
            
            scaleString = [CZCommonUtils localizedStringFromFloat:scaling precision:4U];
            scaleString = [scaleString stringByAppendingFormat:@"(%@/%@)", L(unitKey), L(@"UNIT_PIXEL")];
        }
    }
    
    NSString *detailText = [NSString stringWithFormat:@"%@/%@", magnificationString, scaleString];
    
    return detailText;
}

- (BOOL)imagePicker:(CZImagePickerTableViewController *)picker shouldListImageProperties:(CZImageProperties *)imageProperties fromFilePath:(NSString *)filePath {
    return ![imageProperties.microscopeModel isEqualToString:kModelNameMacroImage];
}

#pragma mark - CZObjectiveSelectionDelegate

- (void)magnificationButtonEnabledDidChange {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierMagnification];
    item.button.enabled = self.task.magnificationButtonEnabled;
}

- (void)magnificationButtonTitleDidChange {
    [self setNeedsMainToolbarItemsUpdate];
}

- (void)magnificationButtonImageDidChange {
    [self setNeedsMainToolbarItemsUpdate];
}

#pragma mark - CZKeyboardObserver

- (void)keyboardWillShow:(CZKeyboardTransition *)transition {
    self.imageProcessingViewBottomConstraint.constant = -(transition.frameEnd.size.height + 16.0);
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)keyboardWillHide:(CZKeyboardTransition *)transition {
    self.imageProcessingViewBottomConstraint.constant = -(CZMainToolbarHeight + 16.0);
    [UIView animateWithDuration:transition.animationDuration delay:0.0 options:transition.animationCurve animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(nullable NSString *)keyPath
                      ofObject:(nullable id)object
                        change:(nullable NSDictionary<NSKeyValueChangeKey, id> *)change
                       context:(nullable void *)context {
    if (object == self.task) {
        if ([keyPath isEqualToString:@"isImageLoadedSucceed"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                [self updateControlEnabled:self.task.isImageLoadedSucceed];
            }
        }
        
        if ([keyPath isEqualToString:@"isEditing"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                [self updateControlsWhileImageEditing];
                [self enterAnnotationEditingMode:newValue];
            }
        }
    }
}

#pragma mark - Getters

- (CZButton *)previousPageButton {
    if (_previousPageButton == nil) {
        _previousPageButton = [CZButton buttonWithType:UIButtonTypeCustom];
        _previousPageButton.level = CZControlEmphasisDefault;
        _previousPageButton.enabled = YES;
        [_previousPageButton cz_setImageWithIcon:[CZIcon iconNamed:@"previous-page"]];
        [_previousPageButton addTarget:self action:@selector(switchToPreviousPage:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _previousPageButton;
}

- (CZButton *)nextPageButton {
    if (_nextPageButton == nil) {
        _nextPageButton = [CZButton buttonWithType:UIButtonTypeCustom];
        _nextPageButton.level = CZControlEmphasisDefault;
        _nextPageButton.enabled = YES;
        [_nextPageButton cz_setImageWithIcon:[CZIcon iconNamed:@"next-page"]];
        [_nextPageButton addTarget:self action:@selector(switchToNextPage:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextPageButton;
}

- (CZFileNameLabel *)fileNameLabel {
    if (_fileNameLabel == nil) {
        _fileNameLabel = [[CZFileNameLabel alloc] init];
    }
    return _fileNameLabel;
}

- (CZPostOffice *)postOffice {
    if (_postOffice == nil) {
        _postOffice = [[CZPostOffice alloc] init];
        _postOffice.delegate = self;
        [_postOffice setSocialNetworksEnabled:YES];
    }
    return _postOffice;
}

@end
