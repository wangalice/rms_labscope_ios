//
//  CZImageViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/12.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnnotationMarco.h"

NS_ASSUME_NONNULL_BEGIN

@class CZImageEditingViewController;

@interface CZImageViewModel : NSObject

@property (nonatomic, assign) CZEditingMode editingMode;
@property (nonatomic, assign) BOOL isImageSaving;

- (BOOL)canSwitchPage;

@end

@interface CZImageViewModel (AnnotationEditing)

+ (CZImageEditingViewController *)imageEditingViewControllerWithEditingMode:(CZEditingMode)editingMode;

@end

@interface CZImageViewModel (SaveFileFormats)

+ (NSArray *)saveFileFormats;

@end

NS_ASSUME_NONNULL_END
