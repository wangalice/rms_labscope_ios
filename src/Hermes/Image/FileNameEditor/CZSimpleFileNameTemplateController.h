//
//  CZSimpleFileNameTemplateController.h
//  Hermes
//
//  Created by Ralph Jin on 7/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileNameTemplateField;
@class CZSimpleFileNameTemplateController;

@protocol CZSimpleFileNameTemplateControllerDelegate <NSObject>

@optional
- (void)simpleFileNameTemplateDidPressOK:(CZSimpleFileNameTemplateController *)simpleFilenameTemplateController;
- (void)simpleFileNameTemplateDidPressConfigure:(CZSimpleFileNameTemplateController *)simpleFilenameTemplateController;
- (void)simpleFileNameTemplateDidPressAutoRename:(CZSimpleFileNameTemplateController *)simpleFilenameTemplateController;
@end

@interface CZSimpleFileNameTemplateController : UIViewController

@property (nonatomic, copy) NSString *filename;
@property (nonatomic, assign, getter = isImageSnapped) BOOL imageSnapped;
@property (nonatomic, assign) id<CZSimpleFileNameTemplateControllerDelegate> delegate;

@end
