//
//  CZFileNameLabel.h
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZDocManager;

@interface CZFileNameLabel : UIView

@property (nonatomic, assign, getter = isEnableEdit) BOOL enableEdit;
@property (nonatomic, assign) BOOL showEditButton;

@property (nonatomic, retain) CZDocManager *docManager;

- (void)setName:(NSString *)name;
- (BOOL)shouldAutorotate;
- (void)updateContentSize;

@end
