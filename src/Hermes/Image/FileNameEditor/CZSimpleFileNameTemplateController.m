//
//  CZSimpleFileNameTemplateController.m
//  Hermes
//
//  Created by Ralph Jin on 7/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSimpleFileNameTemplateController.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>

static const CGFloat kDefaultContentHorizatonalMargin = 32.0;
static const CGFloat kDefaultContentVertailMargin = 32.0;
static const CGFloat kDefaultItemsMargin = 16.0;
static const CGFloat kDefaultItemHeight = 32.0;
static const CGFloat kDefaultItemWidth = 48.0;


@interface CZSimpleFileNameTemplateController ()<
UITextFieldDelegate,
CZTextFieldDelegate
>

@property (nonatomic, strong) CZFileNameTemplateField *field;
@property (nonatomic, assign) BOOL isEditingSimpleTemplate;
@property (nonatomic, strong) CZTextField *textField;
@property (nonatomic, strong) UILabel *autoNumberLabel;
@property (nonatomic, strong) CZButton *autoRenameButton;
@property (nonatomic, strong) CZButton *configureFileNameButton;
@property (nonatomic, strong) CZButton *saveFileNameButton;

@end


@implementation CZSimpleFileNameTemplateController

+ (NSUInteger)splitPositionOfFilename:(NSString *)filename {
    NSRange range = [filename rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"_"] options:NSBackwardsSearch];
    NSUInteger position = range.location != NSNotFound ? range.location : [filename length];
    return position;
}

- (instancetype)init {
    if (self = [super init]) {
        self.preferredContentSize = CGSizeMake(448.0, 208.0);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view addSubview:self.textField];
    [self.view addSubview:self.saveFileNameButton];
    [self.view addSubview:self.configureFileNameButton];
    [self.view addSubview:self.autoRenameButton];
    
    self.textField.translatesAutoresizingMaskIntoConstraints = NO;
    self.saveFileNameButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.configureFileNameButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.autoRenameButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.textField.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:kDefaultContentHorizatonalMargin].active = YES;
    [self.textField.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:kDefaultContentVertailMargin].active = YES;
    [self.textField.heightAnchor constraintEqualToConstant:kDefaultItemHeight].active = YES;
    [self.textField.rightAnchor constraintEqualToAnchor:self.saveFileNameButton.leftAnchor constant:-kDefaultItemsMargin].active = YES;
    
    [self.saveFileNameButton.centerYAnchor constraintEqualToAnchor:self.textField.centerYAnchor].active = YES;
    [self.saveFileNameButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kDefaultContentHorizatonalMargin].active = YES;
    [self.saveFileNameButton.widthAnchor constraintEqualToConstant:kDefaultItemWidth].active = YES;
    [self.saveFileNameButton.heightAnchor constraintEqualToConstant:kDefaultItemHeight].active = YES;
    
    [self.configureFileNameButton.leftAnchor constraintEqualToAnchor:self.textField.leftAnchor].active = YES;
    [self.configureFileNameButton.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-kDefaultContentVertailMargin].active = YES;
    [self.configureFileNameButton.heightAnchor constraintEqualToConstant:kDefaultItemHeight].active = YES;
    [self.configureFileNameButton.bottomAnchor constraintEqualToAnchor:self.autoRenameButton.topAnchor constant:-kDefaultItemsMargin].active = YES;
    
    [self.autoRenameButton.leftAnchor constraintEqualToAnchor:self.configureFileNameButton.leftAnchor].active = YES;
    [self.autoRenameButton.widthAnchor constraintEqualToAnchor:self.configureFileNameButton.widthAnchor].active = YES;
    [self.autoRenameButton.heightAnchor constraintEqualToAnchor:self.configureFileNameButton.heightAnchor].active = YES;
    [self.autoRenameButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-kDefaultContentVertailMargin].active = YES;
    
    self.autoRenameButton.enabled = !self.isImageSnapped && [self hasActivateTemplate];
}

- (void)dealloc {
    
    [self.textField resignFirstResponder];
    self.textField.delegate = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.textField becomeFirstResponder];
}

#pragma mark - Private methods

- (BOOL)performRename {
    if (self.filename == nil) {
        return NO;
    }
    
    NSString *text = self.textField.text;
    if ([text length] == 0) {
        return NO;
    }
    
    if (_isEditingSimpleTemplate) {
        [[NSUserDefaults standardUserDefaults] setValue:text forKey:kDefaultFileNameTemplateText];
        
        self.field.value = text;
        
        NSString *extension = [self.filename pathExtension];
        text = [text stringByAppendingString:_autoNumberLabel.text];
        self.filename = [text stringByAppendingPathExtension:extension];
    } else {
        NSString *extension = [self.filename pathExtension];
        NSString *newFileName = [text stringByAppendingPathExtension:extension];
        
        if ([self isFileNameExisted:newFileName]) {
            return NO;
        }
        self.filename = newFileName;
    }

    return YES;
}

- (BOOL)hasActivateTemplate {
    BOOL hasActivateTemplate = NO;
    NSURL *defaultTemplateURL = [[NSUserDefaults standardUserDefaults] URLForKey:kDefaultFileTemplateURL];
    if (defaultTemplateURL) {
        hasActivateTemplate = [[NSFileManager defaultManager] fileExistsAtPath:[defaultTemplateURL relativePath]];
    }
    return hasActivateTemplate;
}

- (void)setTextFieldText:(NSString *)text {
    if (_isEditingSimpleTemplate) {
        NSUInteger splitPosition = [CZSimpleFileNameTemplateController splitPositionOfFilename:text];
        self.textField.text = [text substringToIndex:splitPosition];
        self.autoNumberLabel.text = [text substringFromIndex:splitPosition];
    } else {
        self.textField.text = text;
    }
}

// check if filename exists
- (BOOL)isFileNameExisted:(NSString *)newFileName {
    if (![self.filename isEqualToString:newFileName]) {
        NSString *newFilePath = [[NSFileManager defaultManager].cz_documentPath stringByAppendingPathComponent:newFileName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:newFilePath]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isInputTextChanged {
    NSString *extension = [self.filename pathExtension];
    NSString *newFileName = [self.textField.text stringByAppendingPathExtension:extension];
    if (![self.filename isEqualToString:newFileName]) {
        return YES;
    }
    return NO;
}

#pragma mark Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (_isEditingSimpleTemplate) {
        BOOL validCharacter = [CZCommonUtils isStringValidFileNameTemplate:string];
        if (!validCharacter) {
            return NO;
        }
        
        NSString *newName = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newName.length == 0) {
            return YES;
        }
        
        // calculate the new name that would be
        NSString *savedFieldValue = self.field.value;
        self.field.value = newName;
        
        // generate the unique filename based on new name
        CZFileNameTemplate *defaultTemplate = [CZDocManager defaultFileNameTemplate];
        CZFileNameGenerator *filenameGenerator = [[CZFileNameGenerator alloc] initWithTemplate:defaultTemplate];
        filenameGenerator.extension = [self.filename pathExtension];
        [filenameGenerator useDefaultValueWhenEmpty];
        
        NSString *newFileName = [filenameGenerator uniqueFileNameInDirectory:[NSFileManager defaultManager].cz_documentPath
                                                                excludedName:self.filename];
        newFileName = [newFileName stringByDeletingPathExtension];
        
        // update auto number
        NSUInteger splitPosition = [CZSimpleFileNameTemplateController splitPositionOfFilename:newFileName];
        self.autoNumberLabel.text = [newFileName substringFromIndex:splitPosition];
        
        // restore field value
        self.field.value = savedFieldValue;
        
        return YES;
    } else {
        return [CZCommonUtils isStringValidFileName:string];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _textField) {
        if ([_textField isFirstResponder]) {
            if ([self performRename]) {
                [_textField resignFirstResponder];
                if ([self.delegate respondsToSelector:@selector(simpleFileNameTemplateDidPressOK:)]) {
                    [self.delegate simpleFileNameTemplateDidPressOK:self];
                }
            } else {
                return NO;
            }
        }
    }
    
    return YES;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEditing:(CZTextField *)textField {
    NSString *extension = [self.filename pathExtension];
    NSString *newFileName = [self.textField.text stringByAppendingPathExtension:extension];
    if (newFileName && [self isFileNameExisted:newFileName]) {
        return YES;
    }
    return NO;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEndEditing:(CZTextField *)textField {
    NSString *extension = [self.filename pathExtension];
    NSString *newFileName = [self.textField.text stringByAppendingPathExtension:extension];
    if (newFileName && [self isFileNameExisted:newFileName]) {
        return YES;
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.saveFileNameButton.enabled = NO;
}

#pragma mark - Action

- (void)configureButtonAction:(CZButton *)button {
    [self.view endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(simpleFileNameTemplateDidPressConfigure:)]) {
        [self.delegate simpleFileNameTemplateDidPressConfigure:self];
    }
}

- (void)autoRenameButtonAction:(CZButton *)button {
    [self.view endEditing:YES];
    
    if ([self.delegate respondsToSelector:@selector(simpleFileNameTemplateDidPressAutoRename:)]) {
        [self.delegate simpleFileNameTemplateDidPressAutoRename:self];
    }
}

- (void)clearInputTextAction:(CZButton *)button {
    self.textField.text = nil;
    self.saveFileNameButton.enabled = NO;
}

- (void)renameButtonAction:(CZButton *)button {
    if ([self performRename]) {
        if ([self.delegate respondsToSelector:@selector(simpleFileNameTemplateDidPressOK:)]) {
            [self.delegate simpleFileNameTemplateDidPressOK:self];
        }
    }
}

- (void)textFieldTextDidChangeEvent:(UITextField *)textField {
    self.saveFileNameButton.enabled = textField.text.length != 0 && [self isInputTextChanged] && ![self.textField.delegate shouldShowErrorMessageWhileTextFieldEditing:self.textField];
}

#pragma mark - Setters
- (void)setFilename:(NSString *)filename {
    _filename = [filename copy];

    [self setTextFieldText:[self.filename stringByDeletingPathExtension]];
}

#pragma mark - Getters
- (CZFileNameTemplateField *)field {
    if (_field == nil) {
        CZFileNameTemplate *defaultTemplate = [CZDocManager defaultFileNameTemplate];
        _field  = [defaultTemplate fieldAtIndex:0];
        NSAssert(_field, @"_field must not be nil.");
    }
    return _field;
}

- (CZTextField *)textField {
    if (_textField == nil) {
        _textField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _textField.errorMessage = L(@"FILE_NAME_CONFLICT_ERROR_MESSAGE");
        _textField.showClearButtonWhileEditing = YES;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.adjustsFontSizeToFitWidth = NO;
        _textField.font = [UIFont cz_body1];
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFieldTextDidChangeEvent:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UILabel *)autoNumberLabel {
    if (_autoNumberLabel == nil) {
        _autoNumberLabel = [[UILabel alloc] init];
    }
    return _autoNumberLabel;
}

- (CZButton *)saveFileNameButton {
    if (_saveFileNameButton == nil) {
        _saveFileNameButton = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        [_saveFileNameButton cz_setImageWithIcon:[CZIcon iconNamed:@"checkmark"]];
        _saveFileNameButton.selected = YES;
        [_saveFileNameButton addTarget:self action:@selector(renameButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveFileNameButton;
}

- (CZButton *)autoRenameButton {
    if (_autoRenameButton == nil) {
        _autoRenameButton = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        [_autoRenameButton setTitle:L(@"AUTO_RENAME") forState:UIControlStateNormal];
        _autoRenameButton.titleLabel.font = [UIFont cz_body1];
        [_autoRenameButton addTarget:self action:@selector(autoRenameButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _autoRenameButton;
}

- (CZButton *)configureFileNameButton {
    if (_configureFileNameButton == nil) {
        _configureFileNameButton = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        [_configureFileNameButton setTitle:L(@"FNT_EDIT_TITLE") forState:UIControlStateNormal];
        _configureFileNameButton.titleLabel.font = [UIFont cz_body1];
        [_configureFileNameButton addTarget:self action:@selector(configureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _configureFileNameButton;
}

// check existence of default template
- (BOOL)isEditingSimpleTemplate {
    return ![self hasActivateTemplate];
}

@end
