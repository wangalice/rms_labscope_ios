//
//  CZFileNameLabel.m
//  Hermes
//
//  Created by Mike Wang on 5/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameLabel.h"
#import "CZSimpleFileNameTemplateController.h"
#import "CZFileNameTemplateConfigurationViewController.h"
#import "CZPopoverController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZFileKit/CZFileKit.h>
#import "CZAlertController.h"

static const CGFloat kUIEditButtonWidth = 48.0;
static const CGFloat kUIEditButtonHeight = 32.0;

@interface CZFileNameLabel ()<
CZPopoverControllerDelegate,
CZSimpleFileNameTemplateControllerDelegate,
CZFileNameTemplateConfigurationViewControllerDelegate
>

@property (nonatomic, strong) CZButton *editButton;
@property (nonatomic, strong) UILabel *fileNameLabel;
@property (nonatomic, strong) UIView *fileNameLabelBackground;
@property (nonatomic, strong) CZPopoverController *popoverController;

@end

@implementation CZFileNameLabel

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.layer.cornerRadius = 8.0;
        self.layer.masksToBounds = YES;
        _enableEdit = YES;

        [self addSubview:self.fileNameLabelBackground];
        [self addSubview:self.editButton];
        [self addSubview:self.fileNameLabel];
        
        self.fileNameLabelBackground.translatesAutoresizingMaskIntoConstraints = NO;
        self.fileNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.editButton.translatesAutoresizingMaskIntoConstraints = NO;


        [self.fileNameLabelBackground.leftAnchor constraintEqualToAnchor:self.leftAnchor].active = YES;
        [self.fileNameLabelBackground.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [self.fileNameLabelBackground.rightAnchor constraintEqualToAnchor:self.rightAnchor].active = YES;
        [self.fileNameLabelBackground.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
        
        [self.fileNameLabel.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [self.fileNameLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:16.0].active = YES;
        [self.fileNameLabel.rightAnchor constraintEqualToAnchor:self.editButton.leftAnchor constant:-16.0].active = YES;
        [self.fileNameLabel.heightAnchor constraintEqualToConstant:21.0].active = YES;
        [self.fileNameLabel.widthAnchor constraintLessThanOrEqualToConstant:300.0].active = YES;
        
        [self.editButton.widthAnchor constraintEqualToConstant:kUIEditButtonWidth].active = YES;
        [self.editButton.heightAnchor constraintEqualToConstant:kUIEditButtonHeight].active = YES;
        [self.editButton.topAnchor constraintEqualToAnchor:self.topAnchor constant:8.0].active = YES;
        [self.editButton.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-8.0].active = YES;
        [self.editButton.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-20.0].active = YES;
    }
    return self;
}

#pragma mark - Public methods

- (void)updateContentSize {

}

- (void)setName:(NSString *)name {
    self.fileNameLabel.text = name;
}

- (void)setEnableEdit:(BOOL)enableEdit {
    _enableEdit = enableEdit;
    
    [self updateContentStates];
}

 - (BOOL)shouldAutorotate {
     return self.popoverController == nil;
 }

- (void)setDocManager:(CZDocManager *)docManager {
    _docManager = nil;
    _docManager = docManager;
    
    [self updateContentStates];
}

#pragma mark - Private methods

- (void)enterFileNameTemplateEditor {
    CZFileNameTemplateConfigurationViewController *controller = [[CZFileNameTemplateConfigurationViewController alloc] init];
    controller.delegate = self;
    [(UINavigationController *)UIApplication.sharedApplication.keyWindow.rootViewController pushViewController:controller animated:YES];
}

- (void)updateFileName:(NSString *)newFilename {
    NSString *filePath = _docManager.filePath;
    filePath = [filePath stringByDeletingLastPathComponent];
    filePath = [filePath stringByAppendingPathComponent:newFilename];
    if (![filePath isEqualToString:_docManager.filePath]) {
        NSString *oldFilePath = _docManager.filePath;
        _docManager.filePath = filePath;
        if (_docManager.originalFilePath == nil) {
            NSError *error = nil;
            BOOL isSuccess = [[CZFileManager defaultManager] moveItemAtPath:oldFilePath toPath:filePath error:&error];
            if (isSuccess == NO) {
                CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR")
                                                                                         message:error.localizedDescription
                                                                                           level:CZAlertLevelError];
                [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
                [alertController presentAnimated:YES completion:nil];
                [self setName:[oldFilePath lastPathComponent]];
                _docManager.filePath = oldFilePath;
            } else {
                [self setName:newFilename];
            }
        }
    }
}

- (void)updateFileNameWithTemplate {
    NSString *currentFileName = [self.docManager.filePath lastPathComponent];
    self.docManager.fileNameGenerator = nil;  // reset the file name template, later it will reload the default template.
    NSString *newFilename = [self.docManager uniqueFileNameExcludedName:currentFileName];
    if ([currentFileName isEqualToString:newFilename]) {
        return;
    }
    
    [self updateFileName:newFilename];
}

- (void)updateContentStates {
    BOOL enableEdit = self.isEnableEdit && self.docManager;
    self.editButton.hidden = !enableEdit;
    self.editButton.enabled = enableEdit;
}

#pragma mark - Action

- (void)editButtonAction:(CZButton *)sender {
    if (self.editButton.isHidden) {
        return;
    }
    
    CZSimpleFileNameTemplateController *renameController = [[CZSimpleFileNameTemplateController alloc] init];
    renameController.delegate = self;
    renameController.imageSnapped = self.docManager.isImageSnapped;
    renameController.filename = [self.docManager.filePath lastPathComponent];
    
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:renameController];
    popover.sourceRect = CGRectOffset(self.editButton.frame, 0.0, 12.0);
    popover.sourceView = self;
    popover.arrowDirection = CZPopoverArrowDirectionUp;
    [popover presentAnimated:YES completion:nil];
    self.popoverController = popover;
}

- (void)fileNameLabelTapped:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self editButtonAction:self.editButton];
    }
}

#pragma mark - CZSimpleFileNameTemplateControllerDelegate

- (void)simpleFileNameTemplateDidPressOK:(CZSimpleFileNameTemplateController *)simpleFilenameTemplateController {
    @weakify(self);
    [self.popoverController dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        [self updateFileName:simpleFilenameTemplateController.filename];
        self.popoverController = nil;
    }];
}

- (void)simpleFileNameTemplateDidPressConfigure:(CZSimpleFileNameTemplateController *)simpleFilenameTemplaeController {
    [self.popoverController dismissViewControllerAnimated:YES completion:^{
        [self enterFileNameTemplateEditor];
    }];
    self.popoverController = nil;
}

- (void)simpleFileNameTemplateDidPressAutoRename:(CZSimpleFileNameTemplateController *)simpleFilenameTemplateController {
    [self.popoverController dismissViewControllerAnimated:YES completion:^{
        if (!self.docManager.isImageSnapped) {
            [self updateFileNameWithTemplate];
        }
    }];
    self.popoverController = nil;
}

#pragma mark - CZFileNameTemplateConfigurationViewControllerDelegate

- (void)fileNameTemplateConfigurationViewControllerDidSaveConfiguration:(CZFileNameTemplateConfigurationViewController *)fileNameTemplateConfigurationViewController {
    [self updateFileNameWithTemplate];
}

#pragma mark - Getters

- (CZButton *)editButton {
    if (_editButton == nil) {
        _editButton = [CZButton buttonWithType:UIButtonTypeCustom];
        _editButton.level = CZControlEmphasisDefault;
        [_editButton cz_setImageWithIcon:[CZIcon iconNamed:@"edit"]];
        [_editButton addTarget:self action:@selector(editButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editButton;
}

- (UILabel *)fileNameLabel {
    if (_fileNameLabel == nil) {
        _fileNameLabel = [[UILabel alloc] init];
        _fileNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _fileNameLabel.numberOfLines = 1;
        _fileNameLabel.cz_textColorPicker = [CZColorTable defaultTextColor];
        _fileNameLabel.font = [UIFont cz_body1];
        _fileNameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fileNameLabelTapped:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        tapGestureRecognizer.numberOfTouchesRequired = 1;
        [_fileNameLabel addGestureRecognizer:tapGestureRecognizer];
    }
    return _fileNameLabel;
}

- (UIView *)fileNameLabelBackground {
    if (_fileNameLabelBackground == nil) {
        _fileNameLabelBackground = [[UIView alloc] init];
        _fileNameLabelBackground.backgroundColor = [UIColor cz_gs120];
        _fileNameLabelBackground.alpha = 0.5;
    }
    return _fileNameLabelBackground;
}

@end
