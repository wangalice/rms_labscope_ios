//
//  CZImageProcessingViewController.h
//  Hermes
//
//  Created by Li, Junlin on 7/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZImageTool;

@interface CZImageProcessingViewController : UIViewController

- (instancetype)initWithImageTool:(CZImageTool *)imageTool;

@end
