//
//  CZImageProcessingViewController.m
//  Hermes
//
//  Created by Li, Junlin on 7/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProcessingViewController.h"
#import "CZGridView.h"
#import <CZImageProcessing/CZImageProcessing.h>

static const float kSliderStepDefault = 1.0f;
static const float kSliderStepSmall = 0.05f;

@interface CZImageProcessingViewController ()

@property (nonatomic, readonly, strong) CZImageTool *imageTool;

@property (nonatomic, readonly, strong) CZGridView *gridView;

@property (nonatomic, readonly, strong) UILabel *gammaLabel;
@property (nonatomic, readonly, strong) UILabel *brightnessLabel;
@property (nonatomic, readonly, strong) UILabel *contrastLabel;
@property (nonatomic, readonly, strong) UILabel *colorIntensityLabel;
@property (nonatomic, readonly, strong) UILabel *sharpeningLabel;

@property (nonatomic, readonly, strong) CZSlider *gammaSlider;
@property (nonatomic, readonly, strong) CZSlider *brightnessSlider;
@property (nonatomic, readonly, strong) CZSlider *contrastSlider;
@property (nonatomic, readonly, strong) CZSlider *colorIntensitySlider;
@property (nonatomic, readonly, strong) CZSlider *sharpeningSlider;

@property (nonatomic, readonly, strong) CZTextField *gammaTextField;
@property (nonatomic, readonly, strong) CZTextField *brightnessTextField;
@property (nonatomic, readonly, strong) CZTextField *contrastTextField;
@property (nonatomic, readonly, strong) CZTextField *colorIntensityTextField;
@property (nonatomic, readonly, strong) CZTextField *sharpeningTextField;

@end

@implementation CZImageProcessingViewController

@synthesize gridView = _gridView;

@synthesize gammaLabel = _gammaLabel;
@synthesize brightnessLabel = _brightnessLabel;
@synthesize contrastLabel = _contrastLabel;
@synthesize colorIntensityLabel = _colorIntensityLabel;
@synthesize sharpeningLabel = _sharpeningLabel;

@synthesize gammaSlider = _gammaSlider;
@synthesize brightnessSlider = _brightnessSlider;
@synthesize contrastSlider = _contrastSlider;
@synthesize colorIntensitySlider = _colorIntensitySlider;
@synthesize sharpeningSlider = _sharpeningSlider;

@synthesize gammaTextField = _gammaTextField;
@synthesize brightnessTextField = _brightnessTextField;
@synthesize contrastTextField = _contrastTextField;
@synthesize colorIntensityTextField = _colorIntensityTextField;
@synthesize sharpeningTextField = _sharpeningTextField;

- (instancetype)initWithImageTool:(CZImageTool *)imageTool {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _imageTool = imageTool;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs100];
    self.view.layer.cornerRadius = 8.0;
    
    [self.view addSubview:self.gridView];
    [self.gridView addContentView:self.gammaLabel];
    [self.gridView addContentView:self.brightnessLabel];
    [self.gridView addContentView:self.contrastLabel];
    [self.gridView addContentView:self.colorIntensityLabel];
    [self.gridView addContentView:self.sharpeningLabel];
    [self.gridView addContentView:self.gammaSlider];
    [self.gridView addContentView:self.brightnessSlider];
    [self.gridView addContentView:self.contrastSlider];
    [self.gridView addContentView:self.colorIntensitySlider];
    [self.gridView addContentView:self.sharpeningSlider];
    [self.gridView addContentView:self.gammaTextField];
    [self.gridView addContentView:self.brightnessTextField];
    [self.gridView addContentView:self.contrastTextField];
    [self.gridView addContentView:self.colorIntensityTextField];
    [self.gridView addContentView:self.sharpeningTextField];
}

#pragma mark - Views

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row4 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:64.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:192.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column2 = [[CZGridColumn alloc] initWithWidth:48.0 alignment:CZGridColumnAlignmentFill];
        
        _gridView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0, self.view.bounds.size.width - 32.0 * 2, self.view.bounds.size.height - 32.0 * 2) rows:@[row0, row1, row2, row3, row4] columns:@[column0, column1, column2]];
        _gridView.axis = UILayoutConstraintAxisHorizontal;
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 16.0;
        [_gridView setCustomSpacing:48.0 afterColumnAtIndex:0];
    }
    return _gridView;
}

- (UILabel *)gammaLabel {
    if (_gammaLabel == nil) {
        _gammaLabel = [[UILabel alloc] init];
        _gammaLabel.font = [UIFont cz_label1];
        _gammaLabel.text = L(@"IMAGE_GAMMA");
        _gammaLabel.textColor = [UIColor cz_gs80];
        _gammaLabel.numberOfLines = 2;
    }
    return _gammaLabel;
}

- (UILabel *)brightnessLabel {
    if (_brightnessLabel == nil) {
        _brightnessLabel = [[UILabel alloc] init];
        _brightnessLabel.font = [UIFont cz_label1];
        _brightnessLabel.text = L(@"IMAGE_BRIGHTNESS");
        _brightnessLabel.textColor = [UIColor cz_gs80];
        _brightnessLabel.numberOfLines = 2;
    }
    return _brightnessLabel;
}

- (UILabel *)contrastLabel {
    if (_contrastLabel == nil) {
        _contrastLabel = [[UILabel alloc] init];
        _contrastLabel.font = [UIFont cz_label1];
        _contrastLabel.text = L(@"IMAGE_CONTRAST");
        _contrastLabel.textColor = [UIColor cz_gs80];
        _contrastLabel.numberOfLines = 2;
    }
    return _contrastLabel;
}

- (UILabel *)colorIntensityLabel {
    if (_colorIntensityLabel == nil) {
        _colorIntensityLabel = [[UILabel alloc] init];
        _colorIntensityLabel.font = [UIFont cz_label1];
        _colorIntensityLabel.text = L(@"IMAGE_COLOR_INTENSITY");
        _colorIntensityLabel.textColor = [UIColor cz_gs80];
        _colorIntensityLabel.numberOfLines = 2;
    }
    return _colorIntensityLabel;
}

- (UILabel *)sharpeningLabel {
    if (_sharpeningLabel == nil) {
        _sharpeningLabel = [[UILabel alloc] init];
        _sharpeningLabel.font = [UIFont cz_label1];
        _sharpeningLabel.text = L(@"IMAGE_SHARPNESS");
        _sharpeningLabel.textColor = [UIColor cz_gs80];
        _sharpeningLabel.numberOfLines = 2;
    }
    return _sharpeningLabel;
}

- (CZSlider *)gammaSlider {
    if (_gammaSlider == nil) {
        _gammaSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_gammaSlider setMinimumValue:0.0];
        [_gammaSlider setMaximumValue:3.0];
        [_gammaSlider setValue:1.5];
        [_gammaSlider addTarget:self action:@selector(gammaSliderAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _gammaSlider;
}

- (CZSlider *)brightnessSlider {
    if (_brightnessSlider == nil) {
        _brightnessSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_brightnessSlider setMinimumValue:-100.0];
        [_brightnessSlider setMaximumValue:100.0];
        [_brightnessSlider setValue:0.0];
        [_brightnessSlider addTarget:self action:@selector(brightnessSliderAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _brightnessSlider;
}

- (CZSlider *)contrastSlider {
    if (_contrastSlider == nil) {
        _contrastSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_contrastSlider setMinimumValue:-100.0];
        [_contrastSlider setMaximumValue:100.0];
        [_contrastSlider setValue:0.0];
        [_contrastSlider addTarget:self action:@selector(contrastSliderAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _contrastSlider;
}

- (CZSlider *)colorIntensitySlider {
    if (_colorIntensitySlider == nil) {
        _colorIntensitySlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_colorIntensitySlider setMinimumValue:-100.0];
        [_colorIntensitySlider setMaximumValue:100.0];
        [_colorIntensitySlider setValue:0.0];
        [_colorIntensitySlider addTarget:self action:@selector(colorIntensitySliderAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _colorIntensitySlider;
}

- (CZSlider *)sharpeningSlider {
    if (_sharpeningSlider == nil) {
        _sharpeningSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_sharpeningSlider setMinimumValue:0.0];
        [_sharpeningSlider setMaximumValue:100.0];
        [_sharpeningSlider setValue:0.0];
        [_sharpeningSlider addTarget:self action:@selector(sharpeningSliderAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _sharpeningSlider;
}

- (CZTextField *)gammaTextField {
    if (_gammaTextField == nil) {
        _gammaTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        [_gammaTextField setText:@"1"];
        [_gammaTextField setTextAlignment:NSTextAlignmentRight];
        [_gammaTextField addTarget:self action:@selector(gammaTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _gammaTextField;
}

- (CZTextField *)brightnessTextField {
    if (_brightnessTextField == nil) {
        _brightnessTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        [_brightnessTextField setText:@"0"];
        [_brightnessTextField setTextAlignment:NSTextAlignmentRight];
        [_brightnessTextField addTarget:self action:@selector(brightnessTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _brightnessTextField;
}

- (CZTextField *)contrastTextField {
    if (_contrastTextField == nil) {
        _contrastTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        [_contrastTextField setText:@"0"];
        [_contrastTextField setTextAlignment:NSTextAlignmentRight];
        [_contrastTextField addTarget:self action:@selector(contrastTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _contrastTextField;
}

- (CZTextField *)colorIntensityTextField {
    if (_colorIntensityTextField == nil) {
        _colorIntensityTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        [_colorIntensityTextField setText:@"0"];
        [_colorIntensityTextField setTextAlignment:NSTextAlignmentRight];
        [_colorIntensityTextField addTarget:self action:@selector(colorIntensityTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _colorIntensityTextField;
}

- (CZTextField *)sharpeningTextField {
    if (_sharpeningTextField == nil) {
        _sharpeningTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        [_sharpeningTextField setText:@"0"];
        [_sharpeningTextField setTextAlignment:NSTextAlignmentRight];
        [_sharpeningTextField addTarget:self action:@selector(sharpeningTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _sharpeningTextField;
}

#pragma mark - Actions

- (void)gammaSliderAction:(id)sender {
    // Since the range of the slider should be [3, 0] but UISlider only
    // supports ascending values, so the range of slider is actually set to
    // [0, 3], and we need to convert this value to real Gamma value first.
    
    CGFloat realGamma = [self.gammaSlider value];
    if (realGamma < 1.5) {
        realGamma = - 4.0 / 3.0 * realGamma + 3.0;
    } else {
        realGamma = - 2.0 / 3.0 * realGamma + 2.0;
    }
    
    CGFloat roundedRealGamma = round(realGamma / kSliderStepSmall) * kSliderStepSmall;
    
    CGFloat sliderValue = roundedRealGamma;
    if (sliderValue < 1.0) {
        sliderValue = 3.0 / 2.0 * (2.0 - sliderValue);
    } else {
        sliderValue = 3.0 / 4.0 * (3.0 - sliderValue);
    }
    
    [self.gammaSlider setValue:sliderValue];
    
    NSString *gammaText = [CZCommonUtils localizedStringFromFloat:roundedRealGamma precision:2];
    [self.gammaTextField setText:gammaText];
    
    [self.imageTool setGamma:roundedRealGamma];
}

- (void)brightnessSliderAction:(id)sender {
    CGFloat brightness = round([self.brightnessSlider value] / kSliderStepDefault) * kSliderStepDefault;
    [self.brightnessSlider setValue:brightness];
    
    NSString *brightnessText = [CZCommonUtils localizedStringFromFloat:brightness precision:2];
    [self.brightnessTextField setText:brightnessText];
    
    [self.imageTool setBrightness:brightness];
}

- (void)contrastSliderAction:(id)sender {
    CGFloat contrast = round([self.contrastSlider value] / kSliderStepDefault) * kSliderStepDefault;
    [self.contrastSlider setValue:contrast];
    
    NSString *contrastText = [CZCommonUtils localizedStringFromFloat:contrast precision:2];
    [self.contrastTextField setText:contrastText];
    
    [self.imageTool setContrast:contrast];
}

- (void)colorIntensitySliderAction:(id)sender {
    CGFloat colorIntensity = round([self.colorIntensitySlider value] / kSliderStepDefault) * kSliderStepDefault;
    [self.colorIntensitySlider setValue:colorIntensity];
    
    NSString *colorIntensityText = [CZCommonUtils localizedStringFromFloat:colorIntensity precision:2];
    [self.colorIntensityTextField setText:colorIntensityText];
    
    [self.imageTool setColorIntensity:colorIntensity];
}

- (void)sharpeningSliderAction:(id)sender {
    CGFloat sharpening = round([self.sharpeningSlider value] / kSliderStepDefault) * kSliderStepDefault;
    [self.sharpeningSlider setValue:sharpening];
    
    NSString *sharpeningText = [CZCommonUtils localizedStringFromFloat:sharpening precision:2];
    [self.sharpeningTextField setText:sharpeningText];
    
    [self.imageTool setSharpness:sharpening];
}

- (void)gammaTextFieldAction:(id)sender {
    CGFloat gamma = [[CZCommonUtils numberFromLocalizedString:self.gammaTextField.text] floatValue];
    gamma = MAX(gamma, 0.0);
    gamma = MIN(gamma, 3.0);
    gamma = round(gamma / kSliderStepSmall) * kSliderStepSmall;
    
    CGFloat sliderValue = gamma;
    if (sliderValue < 1.0) {
        sliderValue = 3.0 / 2.0 * (2.0 - sliderValue);
    } else {
        sliderValue = 3.0 / 4.0 * (3.0 - sliderValue);
    }
    
    [self.gammaSlider setValue:sliderValue];
    
    NSString *gammaText = [CZCommonUtils localizedStringFromFloat:gamma precision:2];
    [self.gammaTextField setText:gammaText];
    
    [self.imageTool setGamma:gamma];
}

- (void)brightnessTextFieldAction:(id)sender {
    CGFloat brightness = [[CZCommonUtils numberFromLocalizedString:self.brightnessTextField.text] floatValue];
    brightness = MAX(brightness, -100.0);
    brightness = MIN(brightness, 100.0);
    brightness = round(brightness / kSliderStepDefault) * kSliderStepDefault;
    [self.brightnessSlider setValue:brightness];
    
    NSString *brightnessText = [CZCommonUtils localizedStringFromFloat:brightness precision:2];
    [self.brightnessTextField setText:brightnessText];
    
    [self.imageTool setBrightness:brightness];
}

- (void)contrastTextFieldAction:(id)sender {
    CGFloat contrast = [[CZCommonUtils numberFromLocalizedString:self.contrastTextField.text] floatValue];
    contrast = MAX(contrast, -100.0);
    contrast = MIN(contrast, 100.0);
    contrast = round(contrast / kSliderStepDefault) * kSliderStepDefault;
    [self.contrastSlider setValue:contrast];
    
    NSString *contrastText = [CZCommonUtils localizedStringFromFloat:contrast precision:2];
    [self.contrastTextField setText:contrastText];
    
    [self.imageTool setContrast:contrast];
}

- (void)colorIntensityTextFieldAction:(id)sender {
    CGFloat colorIntensity = [[CZCommonUtils numberFromLocalizedString:self.colorIntensityTextField.text] floatValue];
    colorIntensity = MAX(colorIntensity, -100.0);
    colorIntensity = MIN(colorIntensity, 100.0);
    colorIntensity = round(colorIntensity / kSliderStepDefault) * kSliderStepDefault;
    [self.colorIntensitySlider setValue:colorIntensity];
    
    NSString *colorIntensityText = [CZCommonUtils localizedStringFromFloat:colorIntensity precision:2];
    [self.colorIntensityTextField setText:colorIntensityText];
    
    [self.imageTool setColorIntensity:colorIntensity];
}

- (void)sharpeningTextFieldAction:(id)sender {
    CGFloat sharpening = [[CZCommonUtils numberFromLocalizedString:self.sharpeningTextField.text] floatValue];
    sharpening = MAX(sharpening, 0.0);
    sharpening = MIN(sharpening, 100.0);
    sharpening = round(sharpening / kSliderStepDefault) * kSliderStepDefault;
    [self.sharpeningSlider setValue:sharpening];
    
    NSString *sharpeningText = [CZCommonUtils localizedStringFromFloat:sharpening precision:2];
    [self.sharpeningTextField setText:sharpeningText];
    
    [self.imageTool setSharpness:sharpening];
}

@end
