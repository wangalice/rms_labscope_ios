//
//  CZImageTask.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZImageChannel.h"
#import "CZImageTaskObjectiveSelection.h"

@class CZImageTask;
@class CZDocManager, CZMicroscopeModel, CZImagePageViewController, CZImageViewModel;

@protocol CZImageTaskDelegate <CZTaskDelegate>

@optional

/** Call the delegate: image task did present a image file */
- (void)imageTaskDidPresentImageDocument:(CZDocManager *)docManager filePathOfDocument:(NSString *)filePath;
- (void)imageTaskPageControllerDataSourceDidUpdate;
- (void)imageTaskWillCompositeMultichannelImage:(CZImageTask *)task;
- (void)imageTaskDidCompositeMultichannelImage:(CZImageTask *)task;

@end

@protocol CZImageTaskDataSource <NSObject>

@required

- (CZDocManager *)currentDocManager;

- (NSUInteger)currentPageIndex;

@end

@interface CZImageTask : CZTask <
CZImageTaskObjectiveSelection
>

@property (nonatomic, readonly, weak) id <CZImageTaskDelegate>delegate;
@property (nonatomic, weak) id <CZImageTaskDataSource>dataSource;
@property (nonatomic, readonly, strong) CZDocManager *docManager;
@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, copy) CZMicroscopeModel *microscopeModel;

@property (nonatomic, assign) CZDisplayCurvePreset currentDisplayCurve;
@property (strong, readonly) NSArray *localFiles;
@property (nonatomic, readonly, copy) NSArray<CZImageChannel *> *channels;

/** remember last opened file path for saving memory.
 * Release memory used by opening a image when image view controller resign activity;
 * then open it from |lastOpenedFilePath| when image view controller get active again.
 */
@property (nonatomic, copy) NSString *lastOpenedFilePath;

// Status for the Image tab
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, assign, getter=isLockScrolling) BOOL isLockScrolling;
@property (nonatomic, assign) BOOL isImageLoadedSucceed;
@property (nonatomic, readonly, assign, getter=isLicenseEnabled) BOOL isLicenseEnabled;
@property (nonatomic, readonly, assign, getter=shouldConvertToCZIFile) BOOL shouldConvertToCZIFile;
@property (nonatomic, readonly, assign, getter=isPageUpEnabled) BOOL isPageUpEnabled;
@property (nonatomic, readonly, assign, getter=isPageDownEnabled) BOOL isPageDownEnabled;

@property (nonatomic, readonly, strong) CZImageViewModel *imageViewModel;

- (void)presentImageDocument:(CZDocManager *)docManager;
- (BOOL)openFileAtPath:(NSString *)filePath;
- (BOOL)hasUnsavedMultichannelAction;
- (void)saveMultichannelAction;
- (void)discardMultichannelAction;
- (void)updateChannels;

- (BOOL)isStereoMicroscopeMode;
- (BOOL)isMacroImageMicroscopeMode;

- (void)updateMagnificationWithFilePath:(NSString *)filePath;
- (void)updateMagnificationButtonImageTitle;

@end
