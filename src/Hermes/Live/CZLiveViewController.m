//
//  CZLiveViewController.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveViewController.h"
#import "CZMultitaskViewController.h"
#import "CZVideoTool.h"
#import "CZLiveSnapModesView.h"
#import "CZCameraSettingsViewController.h"
#import "MBProgressHUD+Hide.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZPopoverController.h"
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZChannelButtonGroup.h"
#import "UIViewController+HUD.h"
#import "CZTimeLapseVideoSettingsViewController.h"
#import "CZLiveSnapPreviewView.h"
#import "CZBuiltInCameraViewController.h"
#import "CZDefaultSettings+Multichannel.h"
#import "CZAlertController.h"
#import "CZMoviePlayerViewController.h"
#import "CZToastManager.h"
#import "CZAnnotationPickerView.h"
#import "CZImageDocument+CZCameraSnappingInfo.h"
#import "CZAnnotationEditingViewController.h"
#import "CZUIDefinition.h"
#import "CZLiveViewModel.h"
#import "CZObjectiveSelectionViewController.h"
#import "CZZoomClickStopSelectionViewController.h"
#import "CZLiveTask+CZAnnotationReadWrite.h"
#import "CZGlobalSettingsViewController.h"

const static uint64_t kFreeSpaceLowerLimit = 200 * 1024 * 1024; // 200 MB
const static float kLowBatteyThreshold = 0.1;  // 10%
const static NSUInteger kOnekeyFluorescenceButtonIndex = 0;
const static NSUInteger kManualFluorescenceButtonIndex = 1;
static NSString * kRecordingTipToastIdentifier = @"kRecordingTipToastIdentifier";
static NSString * kSwitchLightTipToastIdentifier = @"kSwitchLightTipToastIdentifier";

@interface CZLiveViewController () <
CZLiveSnapModesViewDelegate,
CZLiveTaskDelegate,
CZLiveChannelObserver,
CZChannelButtonGroupDelegate,
CZPopoverControllerDelegate,
CZCameraSettingsViewControllerDelegate,
CZObjectiveSelectionViewControllerDelegate,
CZZoomClickStopSelectionViewControllerDelegate,
CZLiveSnapPreviewDelegate,
CZBuiltInCameraViewControllerTapDelegate,
CZAnnotationPickerViewDelegate,
CZAnnotationPickerViewDataSource,
CZImageEditingViewControllerDelegate,
CZImageEditingViewControllerDataSource
> {
    SystemSoundID shutterSound;
    BOOL _isSnapping;
}

@property (nonatomic, strong) CZLiveSnapModesView *snapModesView;
@property (nonatomic, strong) MBProgressHUD *reconnectingHUD;
@property (nonatomic, strong) MBProgressHUD *snappingHUD;
@property (nonatomic, strong) CZLiveSnapPreviewView *snapPreviewView;
@property (nonatomic, strong) UIView *shutterAnimationView;
@property (nonatomic, strong) CZBuiltInCameraViewController *builtInCameraViewController;
@property (nonatomic, assign) NSUInteger edofSnappedCount;
@property (nonatomic, strong) CZCameraSnappingInfo *edofMergedImage;
@property (nonatomic, strong) CZCameraSnappingInfo *edofPreviousImage;
@property (nonatomic, strong) NSTimer *recordingTimer;
@property (nonatomic, strong) NSDate *recordingStartTime;
@property (nonatomic, assign) BOOL connectionStatus;
@property (nonatomic, strong) CZImageEditingViewController *imageEditingController;

@end

@implementation CZLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    
    UIView *view = self.task.videoTool.view;
    view.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(kUISystemStatusBarHeight, 0.0, CZMainToolbarHeight, 0.0));
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    view.backgroundColor = [UIColor cz_gs105];
    [self.view addSubview:view];

    CGFloat width4SnapModesView = [CZLiveSnapModesView defaultSnapModesViewWidth];
    CGFloat x = CGRectGetWidth(view.frame) - width4SnapModesView - 48;
    self.snapModesView.frame = CGRectMake(0, 0, width4SnapModesView,CGRectGetHeight(view.frame));
    self.snapModesView.center = CGPointMake(x + width4SnapModesView / 2, CGRectGetMidY(view.frame));
    [self.multitaskViewController.view addSubview:self.snapModesView];
    [self.multitaskViewController.view addSubview:self.snapPreviewView];
    [self.multitaskViewController.view bringSubviewToFront:self.snapModesView];
    
    [self.view addSubview:self.shutterAnimationView];
    // add shutter sound
    NSString *shutterSoundPath = [[NSBundle mainBundle] pathForResource:@"shutter_click"
                                                                 ofType:@"wav"
                                                            inDirectory:@"Assets"];
    NSURL *shutterSoundUrl = [[NSURL alloc] initWithString:shutterSoundPath];
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain(shutterSoundUrl), &shutterSound);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifyCameraStopForSnapping:)
                                                 name:CZCameraStreamingStopNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(globalSettingDidChange:)
                                                 name:CZGlobalSettingsDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableMeasurementColorCodingDidChange:)
                                                 name:CZDisableMeasurementColorCodingDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fluorescenceSnappingTemplateDidSave:)
                                                 name:CZFluorescenceSnappingTemplateDidSaveNotification
                                               object:nil];
    [self bindViewWithViewModelState];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.multitaskViewController.view addSubview:self.snapModesView];
    [self.multitaskViewController.view addSubview:self.snapPreviewView];
    [self.multitaskViewController.view bringSubviewToFront:self.snapModesView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.snapModesView collapseSnapModes];
    [self.snapModesView removeFromSuperview];
    [self.snapPreviewView removeFromSuperview];
}

- (void)dealloc {
    CZLogv(@"dealloc work well");
    
    AudioServicesDisposeSystemSoundID(shutterSound);

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CZCameraStreamingStopNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CZGlobalSettingsDidChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CZDisableMeasurementColorCodingDidChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CZFluorescenceSnappingTemplateDidSaveNotification
                                                  object:nil];
    [self unbindViewWithViewModelState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if (self.snapModesView.mode == kCZCaptureModeTimelapseVideoRecording) {
        [self snapModesViewDidSnapWithMode:kCZCaptureModeTimelapseVideoRecording];
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_NO_MEMORY") level:CZAlertLevelError];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
    }
}

#pragma mark - Private

- (void)popUpTimelapseVideoSettingsView {
    @weakify(self);
    CZTimelapseVideoSettingsViewController *timelapseVideoSettingViewController = [[CZTimelapseVideoSettingsViewController alloc] init];
    [timelapseVideoSettingViewController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
    CZDialogAction *recordAction = [timelapseVideoSettingViewController addActionWithTitle:L(@"TIMELAPSE_VIDEO_RECORD") style:CZDialogActionStyleDefault handler:^(__kindof CZDialogController *dialog, CZDialogAction *action) {
        @strongify(self);
        if (self.snapModesView.mode == kCZCaptureModeTimelapseVideoStandby) {
            [self switchModeToNextMode];
        }
    }];
    recordAction.delaysActionHandler = YES;
    [timelapseVideoSettingViewController presentAnimated:YES completion:nil];
}

- (void)popUpRemoteStreamSettingsView {
#if LIVE_REMOTE_CAMERA
    CZRemoteStreamSenderSettingViewController *streamSettingViewController = [[CZRemoteStreamSenderSettingViewController alloc] init];
    streamSettingViewController.delegate = self;
    CZRemoteStreamSenderSettingViewController *content = [streamSettingViewController retain];
    [streamSettingViewController release];
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:content];
    navigation.modalPresentationStyle = UIModalPresentationFormSheet;
    navigation.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:navigation animated:YES completion:NULL];
    
    [content release];
    [navigation release];
#endif
}

- (void)switchModeToNextMode {
    [self.snapModesView collapseSnapModes];
    
    CZCaptureMode mode = self.snapModesView.mode;
    if (mode == kCZCaptureModeVideoStandby ||
        mode == kCZCaptureModeTimelapseVideoStandby) {
        if ([CZCommonUtils freeSpaceInBytes] <= kFreeSpaceLowerLimit) {
            // Do not allow video recording if free space is too lowe.
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_NO_SPACE") level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
            return;
        }
        
        BOOL isNormalRecording;
        if (mode == kCZCaptureModeVideoStandby) {
            mode = kCZCaptureModeVideoRecording;
            isNormalRecording = YES;
        } else {
            mode = kCZCaptureModeTimelapseVideoRecording;
            isNormalRecording = NO;
        }
        
        if (!isNormalRecording) {
            UIDevice *device = [UIDevice currentDevice];
            if (device.batteryState == UIDeviceBatteryStateUnplugged &&
                device.batteryLevel <= kLowBatteyThreshold) {
                CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_NO_POWER") level:CZAlertLevelError];
                [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
                [alertController presentAnimated:YES completion:nil];
                return;
            }
        }
#if LIVE_REFACTORED
        // Prevent user from using display options during video recording.
        self.displayOptionsView.hidden = YES;
        [self showDisplayOptionsButton:NO];
        self.displayOptionsButton.selected = NO;
        
#endif
        if ([self.task isScaleBarOverlappedTimeStamp] && !isNormalRecording) {
            CZAlertController *alertController  = [CZAlertController alertControllerWithTitle:L(@"ALERT") message:L(@"LIVE_SCALEBAR_OVERLAPPED_MSG") level:CZAlertLevelWarning];
            [alertController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:NULL];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.snapModesView switchToSnapMode:mode];
                    [self.task updateMode:mode];
                    [self beginRecording:isNormalRecording];
                    [self updateToolbarItemsStatus:NO];
                });
            }];
            [alertController presentAnimated:YES completion:nil];
        } else {
            [self.snapModesView switchToSnapMode:mode];
            [self.task updateMode:mode];
            [self beginRecording:isNormalRecording];
            [self updateToolbarItemsStatus:NO];
        }
//        [self setMagnifierEnabled:NO];
    } else if (mode == kCZCaptureModeVideoRecording ||
               mode == kCZCaptureModeTimelapseVideoRecording) {
        BOOL isNormalRecording;
        if (mode == kCZCaptureModeVideoRecording) {
            mode = kCZCaptureModeVideoStandby;
            isNormalRecording = YES;
        } else {  // kCZCaptureModeTimelapseVideoRecording
            mode = kCZCaptureModeTimelapseVideoStandby;
            isNormalRecording = NO;
        }
        [self.task stopRecording:isNormalRecording];

        [self.snapModesView.recordingTimeLabel setHidden:YES];
        [self.snapModesView switchToSnapMode:mode];
        [self.task updateMode:mode];

        [self.recordingTimer invalidate];
        self.recordingTimer = nil;
        self.recordingStartTime = nil;

        [self updateToolbarItemsStatus:YES];
//        [self setMagnifierEnabled:YES];
    } else if (mode == kCZCaptureModeOnekeyFluorescenceStandby) {
        if ([CZCommonUtils freeSpaceInBytes] <= kFreeSpaceLowerLimit) {
            // Do not allow video recording if free space is too lowe.
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"SNAP_NO_SPACE") level:CZAlertLevelError];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
            [alert presentAnimated:YES completion:nil];
            return;
        }
        
        [self.task canBeginOnekeyFluorescenceSnapping:^(BOOL canBeginOnekeyFluorescenceSnapping) {
            if (!canBeginOnekeyFluorescenceSnapping) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"LIVE_FL_ONEKEY_UNAVAILABLE") level:CZAlertLevelError];
                [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                [alert presentAnimated:YES completion:nil];
                return;
            }
            
            [self.task beginOnekeyFluorescenceSnapping];
            [self.snapModesView switchToSnapMode:kCZCaptureModeOnekeyFluorescencePlaying];
            [self.task updateMode:kCZCaptureModeOnekeyFluorescencePlaying];
            
            AudioServicesPlaySystemSound(shutterSound);
            
            if (self.snappingHUD == nil) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                self.snappingHUD = hud;
                
                [hud setColor:kSpinnerBackgroundColor];
                [hud setLabelText:L(@"BUSY_INDICATOR_SNAPPING")];
            }
            
            [self showShutterAnimationView];
            _isSnapping = YES;
            [self updateToolbarItemsStatus:NO];
        }];
    } else if (mode == kCZCaptureModeOnekeyFluorescencePlaying) {
        [self.task cancelOnekeyFluorescenceSnapping];
        [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
        
        [self updateToolbarItemsStatus:YES];
    } else if (mode == kCZCaptureModePhoto ||
               mode == kCZCaptureModeEDOF ||
               mode == kCZCaptureModeSnapFromStream ||
               mode == kCZCaptureModeSnapDenoise ||
               mode == kCZCaptureModeMacroPhoto ||
               mode == kCZCaptureModeManualFluorescence) {
        
        if ([CZCommonUtils freeSpaceInBytes] <= kFreeSpaceLowerLimit) {
            // Do not allow video recording if free space is too lowe.
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"SNAP_NO_SPACE") level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
            return;
        }
        
        // Don't need to play sound effect for macro snapping because it includes a system sound already.
        if (mode != kCZCaptureModeMacroPhoto) {
            AudioServicesPlaySystemSound(shutterSound);
        }
        
        if (self.snappingHUD == nil) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            self.snappingHUD = hud;
            
            [hud setColor:kSpinnerBackgroundColor];
            [hud setLabelText:L(@"BUSY_INDICATOR_SNAPPING")];
        }
        [self updateSnapModesViewStatus:NO];
        if (mode == kCZCaptureModeSnapFromStream) {
            [self.task.currentCamera beginSnappingFromVideoStream];
        } else if (mode == kCZCaptureModeMacroPhoto) {
            [self.builtInCameraViewController beginSnapping];
        } else if (mode == kCZCaptureModeManualFluorescence) {
            [self.task beginOrContinueManualFluorescenceSnapping];
        } else {
            BOOL enableDenoising = [self.task isDenoisingEnableFor:self.task.currentCamera mode:mode];
            
            if (enableDenoising) {
                [self.task updateDenoiser:[[CZVideoDenoiser alloc] init]];
                [self.task.currentCamera beginContinuousSnapping:3];  // CZVideoDenoiser need 3 frames to denoise
            } else {
                [self.task updateDenoiser:nil];
                [self.task.currentCamera beginSnapping];
            }
        }
        
        [self showShutterAnimationView];
        _isSnapping = YES;
        [self updateToolbarItemsStatus:NO];
    }
#if LIVE_REMOTE_CAMERA
    else if (mode == kCZCaptureModeRemoteStreamStandby) {
        mode = kCZCaptureModeRemoteStream;
        
        // start relay mode
        NSString *urlString = [NSString stringWithFormat:@"rtsp://%@:%d", self.remoteCameraIP, _port];
        NSURL *url = [NSURL URLWithString:urlString];
        [self.currentCamera startRelayToURL:url];
        
        // Prevent user from using display options during video recording.
        self.displayOptionsView.hidden = YES;
        [self showDisplayOptionsButton:NO];
        self.displayOptionsButton.selected = NO;
        
        BOOL isAlreadyFullScreen = [CZAppDelegate get].tabBar.isFullScreen;
        if (!isAlreadyFullScreen) {
            [[CZAppDelegate get].tabBar showFullScreen:YES];
        }
        //TODO:change the image
        [_snapButton setImage:[UIImage imageNamed:A(@"capture_button_recording.png")]
                     forState:UIControlStateNormal];
        [_snapButton setImage:[UIImage imageNamed:A(@"capture_button_recording.png")]
                     forState:UIControlStateHighlighted];
        
        [_snapSelectButton setImage:[UIImage imageNamed:A(@"capture_dropdown_recording.png")]
                           forState:UIControlStateNormal];
        [_snapSelectButton setUserInteractionEnabled:NO];
        
        NSString *ipAddress = [NSString stringWithFormat:@"%@:%d", self.remoteCameraIP, _port];
        
        NSString *tipText = [[NSString alloc] initWithFormat:L(@"RECEIVE_STREAM_SENDER_TIP"),
                             ipAddress];
        [[CZToastMessage sharedInstance] showNotificationMessage:tipText andHideAfter:0];
        [tipText release];
        
        [self setMagnifierEnabled:NO];
    } else if (_mode == kCZCaptureModeRemoteStream) {
        _mode = kCZCaptureModeRemoteStreamStandby;
        
        [self.currentCamera endRelay];
        
        [_snapSelectButton setImage:[UIImage imageNamed:A(@"capture_dropdown.png")]
                           forState:UIControlStateNormal];
        [_snapSelectButton setUserInteractionEnabled:YES];
        
        [_snapButton setImage:[UIImage imageNamed:A(@"capture_button_snap_from_stream.png")]
                     forState:UIControlStateNormal];
        [_snapButton setImage:[UIImage imageNamed:A(@"capture_button_snap_from_stream_pressed.png")]
                     forState:UIControlStateHighlighted];
        
        [[CZAppDelegate get].tabBar showFullScreen:NO];
        [[CZToastMessage sharedInstance] hideNotificationMessage];
        
        [self setMagnifierEnabled:YES];
    }
#endif
}

- (void)refreshReconnectingLabel {
    if (self.reconnectingHUD) {
        NSString *labelText;
        if ([self.task.currentCamera displayName]) {
            labelText = [NSString stringWithFormat:L(@"BUSY_INDICATOR_RECONNECTING_TO"),
                         [self.task.currentCamera displayName]];
        } else {
            labelText = L(@"BUSY_INDICATOR_RECONNECTING");
        }
        
        NSString *wlanTitle = L(@"WLAN_TITLE");
        NSString *ssid = [CZWifiNotifier copySSID];
        
        NSString *detailsLabelText = [wlanTitle stringByAppendingString:ssid];
        
        [self.reconnectingHUD setLabelText:labelText];
        [self.reconnectingHUD setDetailsLabelText:detailsLabelText];
    }
}

- (void)showShutterAnimationView {
    [self.shutterAnimationView setAlpha:1.0];
    [self.shutterAnimationView setHidden:NO];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5
                     animations:^() {
                         [weakSelf.shutterAnimationView setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [weakSelf.shutterAnimationView setHidden:YES];
                     }];
}

- (void)updateFluorescenceSnapButtonStatus {
    self.snapModesView.snapButton.enabled = [self.task isFluorescenceSnapButtonEnabledForMode:self.snapModesView.mode];
}

- (void)showBuiltInCameraUIComponents:(BOOL)show {
#if SPLIT_VIEW
    if (self.splitScreen) {
        show = NO;
    }
#endif
    
    [self.builtInCameraViewController showBuiltInCameraUIComponents:show];
}

- (void)updateBuiltInCameraVisibility {
    [self restoreFuctionBarButton];
    [self.task updateZoomRelatedControls];

    do {
        if (![self.task isRemoteCamera]) {
            if (self.snapModesView.mode == kCZCaptureModeMacroPhoto) { //|| [self.displayOptionsView isButtonSelected:kCZDisplayOptionsViewButtonTagDrawing]) {
                CGSize resolution;
                CZBuiltInCameraViewMode builtInViewMode;
                if (self.snapModesView.mode  == kCZCaptureModeMacroPhoto) {
                    builtInViewMode = CZBuiltInCameraViewModeMacro;
                    resolution = CGSizeZero;
                } else {
                    builtInViewMode = CZBuiltInCameraViewModeDrawingTube;
                    resolution = self.task.currentCamera.liveResolution;
                }
                
                if (self.builtInCameraViewController == nil) {
                    CZBuiltInCameraViewController *builtinCameraVC = [[CZBuiltInCameraViewController alloc] init];
                    builtinCameraVC.mode = builtInViewMode;
                    [builtinCameraVC startRunningInViewController:self resolution:resolution aboveView:self.task.videoTool.view];
                    self.builtInCameraViewController = builtinCameraVC;
                    builtinCameraVC.delegate = self;
                    builtinCameraVC.cameraDelegate = self.task;
                    
                    if (self.snapModesView.mode  == kCZCaptureModeMacroPhoto) {
                        [self.task.currentCamera pause];
                    }
                } else {
                    self.builtInCameraViewController.mode = builtInViewMode;
                }
                
                break;
            }
        }
        
        // fall back solution
        [self stopBuiltInCamera];
        [self.task.currentCamera resume];
    } while (false);
    
#if LIVE_REFACTORED
    [self setMagnifierEnabled:YES];
    [self showDisplayOptionsButton:YES];
#endif
}

- (void)stopBuiltInCamera {
    [self.builtInCameraViewController stopRunningFromParentViewController];
    self.builtInCameraViewController = nil;
}

- (void)handleDismissTouch:(UITouch *)touch {
    CGPoint pointInView = [touch locationInView:self.view];
    [self handleDismissTouchInPoint:pointInView];
}

- (void)handleDismissTouchInPoint:(CGPoint)pointInView {
#if LIVE_REFACTORED
    if (self.isEditingMode) {
        return;
    }
    
    if (CGRectContainsPoint([_functionBar frame], pointInView)) {
        return;
    }
    
    if (self.operatorNameViewController) {
        if (CGRectContainsPoint([self.operatorNameViewController.view frame], pointInView)) {
            return;
        }
    }
    
    // Handle resetting action for focus indicator
    if (CGRectContainsPoint([_focusIndicator frame], pointInView)) {
        [_focusIndicator reset];
        return;
    }
    
    if (!self.isFullScreen && !self.isSplitScreen) {
        if (!CGRectContainsPoint([_cameraControlView frame], pointInView)) {
            // TODO: Retain camera control if snap button is triggered while camera control
            // panel is still on.
            
            [self showCameraControlView:NO];
            self.cameraControlButton.selected = NO;
        }
        
        if (self.operatorNameViewController &&
            !CGRectContainsPoint(self.operatorNameViewController.view.frame, pointInView)) {
            [self hideOperatorNameView:YES];
        }
        
        if (!CGRectContainsPoint([_objectiveSettingView frame], pointInView)) {
            if (self.objectiveSettingButton.enabled) {
                self.objectiveSettingView.hidden = YES;
                self.objectiveSettingButton.selected = NO;
            }
        }
        
        if (!CGRectContainsPoint([_zoomSettingView frame], pointInView)) {
            if (self.zoomSettingButton.enabled) {
                self.zoomSettingView.hidden = YES;
                self.zoomSettingButton.selected = NO;
            }
        }
        
        if (!CGRectContainsPoint([_displayOptionsView frame], pointInView)) {
            self.displayOptionsView.hidden = YES;
            self.displayOptionsButton.selected = NO;
            [self showDisplayOptionsButton:YES];
        }
    } else {
        BOOL shouldShowButton = !CGRectContainsPoint([_displayOptionsView frame], pointInView) ||
        (self.displayOptionsView.isHidden && self.displayOptionsButton.isHidden);
        
        // Display options is disabled while recording
        shouldShowButton &= (_mode != kCZCaptureModeVideoRecording && _mode != kCZCaptureModeTimelapseVideoRecording && _mode != kCZCaptureModeRemoteStream);
        
        if (shouldShowButton) {
            self.displayOptionsView.hidden = YES;
            self.displayOptionsButton.selected = NO;
            [self showDisplayOptionsButton:YES];
            
            _snapButton.hidden = NO;
            if (!_snapSelectButton.isSelected) {
                _snapSelectButton.hidden = NO;
            }
            
            self.fullscreenLastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
        }
    }
#endif
}

- (void)restoreFuctionBarButton {
    BOOL reachable = [self.task isCurrentCameraReachable];
    [self updateSnapModesViewStatus:reachable];
}

- (void)beginRecording:(BOOL)isNormalRecording {
    [self.snapModesView.recordingTimeLabel setText:@"0:00"];
    [self.snapModesView.recordingTimeLabel setHidden:NO];
    
    CZTimelapseVideoPlayRate rate = [[CZDefaultSettings sharedInstance] videoPlayRate];
    NSUInteger frameRate = [CZTimelapseVideoSettingsViewController playRateValueForLevel:rate];
    
    [self.task beginRecordingWithFrameRate:frameRate isNormalRecording:isNormalRecording];
    
    self.recordingStartTime = [NSDate date];
    
    const NSTimeInterval kRecordingTimerInterval = 0.1;
    [self.recordingTimer invalidate];
    self.recordingTimer = [NSTimer scheduledTimerWithTimeInterval:kRecordingTimerInterval
                                                           target:self
                                                         selector:@selector(recordingTimerHandler:)
                                                         userInfo:nil
                                                          repeats:YES];
}

- (void)mergeEDOFWithImageInfo:(CZCameraSnappingInfo *)imageInfo {
    self.edofSnappedCount++;

    if (self.edofSnappedCount > 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.edofSnappedCount >= 2) {
                self.snapPreviewView.undoActionSupported = !(self.edofPreviousImage == self.edofMergedImage);
            }
        });
        
        self.edofPreviousImage = [self.edofMergedImage copy];
        
        CZLaplacianBlending *blending = [[CZLaplacianBlending alloc] init];
        [blending addImage:[self.edofMergedImage image] anotherImage:[imageInfo image]];
        
        [self.edofMergedImage updateImage:[blending blend]];

        if (self.edofMergedImage == nil) {
            [self endEDOFSnapping];
            [self.snapPreviewView dismissPreview];
        }
    } else {
        self.edofMergedImage = imageInfo;
    }
    
    [self.task.videoTool dismissMagnifierView];
}

- (void)undoMeregeAction {
    self.snapPreviewView.undoActionSupported = NO;
    if (self.edofPreviousImage != nil) {
        self.edofMergedImage = self.edofPreviousImage;
        self.edofSnappedCount--;
        self.edofPreviousImage = nil;
        [self.snapPreviewView updatePreviewWithImage:self.edofMergedImage.image];
    }
}

- (void)endEDOFSnapping {
    self.snapPreviewView.undoActionSupported = NO;
    self.edofMergedImage = nil;
    self.edofPreviousImage = nil;
    self.edofSnappedCount = 0;
}

- (void)updateSnapModesViewStatus:(BOOL)cameraReachable {
    BOOL enabled;
    BOOL selectable;
    
    if (self.snapModesView.mode == kCZCaptureModeMacroPhoto ||
        self.snapModesView.mode == kCZCaptureModeTimelapseVideoRecording) {
        enabled = YES;
        selectable = YES;
    } else {
        enabled = cameraReachable;
        selectable = cameraReachable;
    }

    if (cameraReachable) {
        if (self.snapModesView.mode == kCZCaptureModeManualFluorescence || self.snapModesView.mode == kCZCaptureModeOnekeyFluorescenceStandby) {
            enabled = enabled && [self.task isFluorescenceSnapButtonEnabledForMode:self.snapModesView.mode];
            selectable = YES;
        } else if (self.snapModesView.mode == kCZCaptureModeOnekeyFluorescencePlaying) {
            // Enable the snap modes view while one-key snapping
            enabled = YES;
            selectable = YES;
        }
    }

    // Enable the snap button while one-key snapping
    self.snapModesView.snapButton.enabled = self.snapModesView.mode == kCZCaptureModeOnekeyFluorescencePlaying ? enabled : enabled && !_isSnapping;
    self.snapModesView.snapSelectButton.enabled = selectable && !_isSnapping;
}

- (void)updateToolbarItemsStatus:(BOOL)cameraReachable {
    if (self.task.viewModel.isEditing) {
        return;
    }
    
    BOOL enabled = cameraReachable && !_isSnapping;
    switch (self.snapModesView.mode) {
        case kCZCaptureModeMacroPhoto:
        case kCZCaptureModeTimelapseVideoRecording:
        case kCZCaptureModeVideoRecording:
        case kCZCaptureModeOnekeyFluorescencePlaying: {
            enabled = NO;
        }
            break;
            
        default:
            break;
    }
    for (CZToolbarItem *item in self.mainToolbar.items) {
        if ([item.identifier isEqualToString:CZToolbarItemIdentifierObjective]) {
            item.enabled = self.task.objectiveSelectionButtonEnabled && enabled;
        } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierZoomClickStop]) {
            item.enabled = self.task.zoomSelectionButtonEnabled && enabled;
        } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierFilterSet]) {
            item.enabled = self.task.filterSetSelectionButtonEnabled;
        } else {
            item.enabled = enabled;
        }
    }
    
    CZToolbarItem *fluorescenceSnapModeItem = [self.virtualToolbar itemForIdentifier:CZToolbarItemIdentifierFluorescenceSnapMode];
    fluorescenceSnapModeItem.enabled = enabled;

    if (enabled == NO && [self.presentedViewController isKindOfClass:[CZPopoverController class]]) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)enterAnnotationEditingMode:(BOOL)isEditing {
    if (isEditing) {
        if (self.imageEditingController) {
            [self.imageEditingController.view removeFromSuperview];
            [self.imageEditingController removeFromParentViewController];
            [self.imageEditingController didMoveToParentViewController:nil];
            self.imageEditingController.delegate = nil;
            self.imageEditingController.dataSource = nil;
            self.imageEditingController = nil;
        }
        
        
        if (self.task.videoTool.docManager == nil) {  // if invalid image
            return;
        }
        
        CZImageEditingViewController *imageEditingViewController = [[CZAnnotationEditingViewController alloc] init];
        self.imageEditingController = imageEditingViewController;
        self.imageEditingController.delegate = self;
        self.imageEditingController.dataSource = self;
        
        [self addChildViewController:self.imageEditingController];
        [self.view addSubview:self.imageEditingController.view];
        self.imageEditingController.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CZMainToolbarHeight - kUISystemStatusBarHeight);
        [self.imageEditingController didMoveToParentViewController:self];

        if ([imageEditingViewController conformsToProtocol:@protocol(CZElementLayerDelegate)] &&
            [imageEditingViewController conformsToProtocol:@protocol(CZImageViewEditingDelegate)]) {
            self.task.videoTool.combineDelegate = (id<CZElementLayerDelegate, CZImageViewEditingDelegate>)imageEditingViewController;
        }
    } else {
        [self.imageEditingController.view removeFromSuperview];
        [self.imageEditingController removeFromParentViewController];
        [self.imageEditingController didMoveToParentViewController:nil];
        self.imageEditingController.delegate = nil;
        self.imageEditingController.dataSource = nil;
        self.imageEditingController = nil;
    }

    self.snapModesView.hidden = isEditing;
    [self updateMaintoolbarItemsEnabledWhileEditing];
}

- (void)updateMaintoolbarItemsEnabledWhileEditing {
    CZToolbarItem *undoItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierUndo];
    CZToolbarItem *redoItem = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierRedo];
    undoItem.enabled = [self.task canUndo];
    redoItem.enabled = [self.task canRedo];
}

- (void)bindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.task.viewModel addObserver:self
                              forKeyPath:observableKeyPath
                                 options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                                 context:NULL];
    }
}

- (void)unbindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.task.viewModel removeObserver:self
                                 forKeyPath:observableKeyPath
                                    context:NULL];
    }
}

- (NSArray <NSString *> *)observableKeyPaths {
    return @[@"snapAvailable", @"snapModesViewVisible", @"reconnecting", @"isEditing"];
}

#pragma mark - Timer
- (void)recordingTimerHandler:(NSTimer *)timer {
    if (self.recordingStartTime) {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                                   fromDate:self.recordingStartTime
                                                     toDate:[NSDate date]
                                                    options:0];
        NSString *timeElapsed = nil;
        if (components.hour == 0) {
            timeElapsed = [[NSString alloc] initWithFormat:@"%li:%02li", (long)components.minute, (long)components.second];
        } else {
            timeElapsed = [[NSString alloc] initWithFormat:@"%li:%02li:%02li", (long)components.hour, (long)components.minute, (long)components.second];
        }
        [self.snapModesView.recordingTimeLabel setText:timeElapsed];
    }
}

#pragma mark - Notification

- (void)notifyCameraStopForSnapping:(NSNotification *)aNotification {
    id macAddress = [aNotification userInfo][@"macAddress"];
    if (![macAddress isKindOfClass:[NSString class]]) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        if (_isSnapping) {
            return;
        }
        
        if ([self.task.currentCamera.macAddress isEqualToString:macAddress]) {
            if ([self.snapModesView isSnapEnabled]) {  // avoid show message twice
                if (self.snapModesView.mode != kCZCaptureModeTimelapseVideoRecording) {
                    self.snapModesView.snapButton.enabled = NO;
                    self.snapModesView.snapSelectButton.hidden = YES;
                    [self.snapModesView collapseSnapModes];
                }
                [[CZToastManager sharedManager] showToastMessage:L(@"LIVE_CAMERA_BUSY_MESSAGE")
                                                      sourceRect:CGRectOffset(self.view.frame, 0, CGRectGetMidY(self.view.frame))];
            }
        }
    });
}

- (void)globalSettingDidChange:(NSNotification *)aNotification {
    if (self.task.currentCamera) {
        [self.snapModesView updateWithAvaliableSnapModes:[self.task availableModes]];
        [self.snapModesView switchToSnapMode:self.task.defaultSnappingMode];
    }
}

- (void)disableMeasurementColorCodingDidChange:(NSNotification *)note {
    CZElementLayer *elementLayer = self.task.videoTool.docManager.elementLayer;
    if ([elementLayer.delegate respondsToSelector:@selector(layerUpdateAllMeasurements:)]) {
        [elementLayer.delegate layerUpdateAllMeasurements:elementLayer];
    }
}

- (void)fluorescenceSnappingTemplateDidSave:(NSNotification *)note {
    if ([self.task canDisplayOnekeyFluorescenceSnapMode]) {
        if ([[CZDefaultSettings sharedInstance] dontShowFluorescenceSnapTemplateSavedMessage] == NO) {
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"LIVE_FL_SAVED_TITLE") message:L(@"LIVE_FL_SAVED_MESSAGE") level:CZAlertLevelInfo];
            [alert addCheckBoxWithConfigurationHandler:^(CZCheckBox *checkBox) {
                [checkBox setTitle:L(@"LIVE_FL_SAVED_DONT_SHOW_AGAIN") forState:UIControlStateNormal];
            }];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
                BOOL dontShowMessage = alert.checkBoxes.firstObject.isSelected;
                [[CZDefaultSettings sharedInstance] setDontShowFluorescenceSnapTemplateSavedMessage:dontShowMessage];
            }];
            [alert presentAnimated:YES completion:nil];
        }
    }
    [self setNeedsVirtualToolbarItemsUpdate];
    [self.snapModesView updateWithAvaliableSnapModes:[self.task availableModes]];
}

#pragma mark - CZTaskDelegate

- (instancetype)initWithTask:(CZLiveTask *)task {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        NSAssert([task isKindOfClass:[CZLiveTask class]], @"%@ is not a live task", task);
        _task = task;
    }
    return self;
}

- (UIViewController *)taskViewController {
    return self;
}

- (void)taskDidEnterForeground:(CZTask *)task {
    CZLogv(@"current snap mode: %zd", (NSInteger)self.snapModesView.mode);
    self.snapModesView.hidden = NO;
    
    [self updateBuiltInCameraVisibility];
    [self updateSnapModesViewStatus:NO];
    [self updateToolbarItemsStatus:NO];
    if (self.snapModesView.mode != kCZCaptureModeSnapFromStream) {
        CGFloat ratio = self.task.currentCamera.snapResolution.width / self.task.currentCamera.liveResolution.width;
        [self.task updateElementLayerLogicalScaling:ratio];
    }
    
#if LIVE_REFACTORED
    _everEntered = YES;
    _isActiveTab = YES;
    _didUpdateSnapModesFromMNA = NO;
    
    self.videoTool.needUpdateScaleBar = YES;
    [self.videoTool refreshLayers];
    
#endif
    [self.task updateZoomRelatedControls];
    [self.task updateScaling];
#if LIVE_REFACTORED
    [self updateScaleBarStatus];
    [self updateDisplayOptionsForLaserPointer];
    [self showHelpOverlay];
    
    [self updateElementLayerLogicalScaling];
    
    [self updateRemoteStreamReceiverViewVisibility];
    
    //Add camera connection when Labscope enter live image
    if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
        NSString *macAddress = self.currentCamera.macAddress.length ? self.currentCamera.macAddress : @"";
        [[CZDCMManager defaultManager] sendInstrumentConnectingChanged:macAddress isConnecting:true];
        [self configurationApplyNotification:nil];
    }
#endif
}

- (void)taskWillEnterBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    // TODO: to complate with recording
    // if camera is snapping or recording, alert message: cancel current operation?
    if (self.snappingHUD || self.task.snappedChannels.count > 0 || self.edofSnappedCount > 0) {
        CZCaptureMode mode = self.snapModesView.mode;
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_DISCARD_EDOF_MSG") level:CZAlertLevelWarning];
        [alertController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            completionHandler(CZTaskEventDisallow);
        }];
        [alertController addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            [self endEDOFSnapping];
            [self.task endFluorescenceSnappingForMode:mode action:CZFluorescenceSnappingEndActionUndone];
            [self.snapPreviewView dismissPreview];
            [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
            
            // Dismiss the snappingHUD
            if (self.snappingHUD) {
                [_snappingHUD dismiss:YES];
                self.snappingHUD = nil;
            }
            _isSnapping = NO;
            
            completionHandler(CZTaskEventAllow);
        }];
        [alertController presentAnimated:YES completion:nil];
    } else if (self.snapModesView.mode == kCZCaptureModeVideoRecording || self.snapModesView.mode == kCZCaptureModeTimelapseVideoRecording) {
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_DISCARD_RECORDING_MSG") level:CZAlertLevelWarning];
        [alertController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            completionHandler(CZTaskEventDisallow);
        }];
        [alertController addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            [self switchModeToNextMode];
            [self.snapPreviewView dismissPreview];
            completionHandler(CZTaskEventAllow);
        }];
        [alertController presentAnimated:YES completion:nil];
    } else {
        completionHandler(CZTaskEventAllow);
    }
}

- (void)taskDidEnterBackground:(CZTask *)task {
    CZLogv(@"current snap mode: %zd", (NSInteger)self.snapModesView.mode);

    _isSnapping = NO;
    [self stopBuiltInCamera];
    [self.snapModesView collapseSnapModes];
    self.snapModesView.hidden = YES;
    [self.snapPreviewView dismissPreview];
    self.task.viewModel.isEditing = NO;
}

- (void)taskWillTerminate:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    [self.task exitFluorescenceSnappingMode];
    completionHandler(CZTaskEventAllow);
}

#pragma mark - CZMainToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarPrimaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    NSMutableArray<CZToolbarItemIdentifier> *items = [NSMutableArray array];
    if (self.task.objectiveSelectionButtonHidden == NO) {
        [items addObject:CZToolbarItemIdentifierObjective];
    }
    if ([self.task.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]] && self.task.zoomSelectionButtonHidden == NO) {
        [items addObject:CZToolbarItemIdentifierZoomClickStop];
    }
    if (self.task.filterSetSelectionButtonHidden == NO) {
        [items addObject:CZToolbarItemIdentifierFilterSet];
    }
    [items addObject:CZToolbarItemIdentifierCameraSettings];
    [items addObject:CZToolbarItemIdentifierHistogram];
    return [items copy];
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarSecondaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    return @[CZToolbarItemIdentifierAnnotation, CZToolbarItemIdentifierGraticuleOverlay, CZToolbarItemIdentifierOverexposureIndicator, CZToolbarItemIdentifierFocusIndicator, CZToolbarItemIdentifierSplitView, CZToolbarItemIdentifierLaserPointer, CZToolbarItemIdentifierDrawingTube];
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarCenterItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

#pragma mark - CZVirtualToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarLeadingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarCenterItemIdentifiers:(CZToolbar *)virtualToolbar {
    switch (self.snapModesView.mode) {
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby:
        case kCZCaptureModeOnekeyFluorescencePlaying:
            return @[CZToolbarItemIdentifierChannels];
        default:
            return nil;
    }
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarTrailingItemIdentifiers:(CZToolbar *)virtualToolbar {
    switch (self.snapModesView.mode) {
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby:
        case kCZCaptureModeOnekeyFluorescencePlaying:
            if ([self.task canDisplayOnekeyFluorescenceSnapMode]) {
                return @[CZToolbarItemIdentifierFluorescenceSnapMode];
            } else {
                return nil;
            }
        default:
            return nil;
    }
}

#pragma mark - CZToolbarDelegate

- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier {
    return [super toolbar:toolbar makeItemForIdentifier:identifier];
}

- (CZToolbarItem *)makeToolbarItemForChannels {
    CZToolbarItem *item = [super makeToolbarItemForChannels];
    CZChannelButtonGroup *buttonGroup = [[CZChannelButtonGroup alloc] initWithChannels:self.task.channels];
    buttonGroup.delegate = self;
    item.view = buttonGroup;
    return item;
}

- (CZToolbarItem *)makeToolbarItemForAnnotationTools {
    CZToolbarItem *item = [super makeToolbarItemForAnnotationTools];
    CZAnnotationPickerView *annotationPickerView = [[CZAnnotationPickerView alloc] initWithDataSource:self];
    annotationPickerView.delegate = self;
    annotationPickerView.dataSource = self;
    item.view = annotationPickerView;
    return item;
}

- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierFilterSet]) {
        CZButton *button = item.button;
        button.hidden = self.task.filterSetSelectionButtonHidden;
        button.enabled = self.task.filterSetSelectionButtonEnabled;
        [button setTitle:self.task.filterSetSelectionButtonTitle forState:UIControlStateNormal];
        [button setImage:self.task.filterSetSelectionButtonImage forState:UIControlStateNormal];
        [item alignImageAndTitle];
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierFluorescenceSnapMode]) {
        CZToggleButton *toggleButton = item.view;
        switch (self.snapModesView.mode) {
            case kCZCaptureModeOnekeyFluorescenceStandby:
            case kCZCaptureModeOnekeyFluorescencePlaying:
                toggleButton.selectedIndex = kOnekeyFluorescenceButtonIndex;
                break;
            default:
                toggleButton.selectedIndex = kManualFluorescenceButtonIndex;
                break;
        }
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierObjective]) {
        CZButton *button = item.button;
        button.hidden = self.task.objectiveSelectionButtonHidden;
        button.enabled = self.task.objectiveSelectionButtonEnabled;
        [button setTitle:self.task.objectiveSelectionButtonTitle forState:UIControlStateNormal];
        [button setImage:self.task.objectiveSelectionButtonImage forState:UIControlStateNormal];
        [item alignImageAndTitle];
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierZoomClickStop]) {
        CZButton *button = item.button;
        button.hidden = self.task.zoomSelectionButtonHidden;
        button.enabled = self.task.zoomSelectionButtonEnabled;
        [button setTitle:self.task.zoomSelectionButtonTitle forState:UIControlStateNormal];
        [button setImage:self.task.zoomSelectionButtonImage forState:UIControlStateNormal];
        [item alignImageAndTitle];
    }
}

- (void)toolbar:(CZToolbar *)toolbar didPushItems:(NSArray<CZToolbarItem *> *)items fromItem:(CZToolbarItem *)fromItem {
    if ([fromItem.identifier isEqualToString:CZToolbarItemIdentifierAnnotation]) {
        self.task.viewModel.isEditing = YES;
    }
}

- (void)toolbar:(CZToolbar *)toolbar didPopItems:(NSArray<CZToolbarItem *> *)items byItem:(CZToolbarItem *)byItem toItem:(CZToolbarItem *)toItem {
    if ([toItem.identifier isEqualToString:CZToolbarItemIdentifierAnnotation]) {
        self.task.viewModel.isEditing = NO;
    }
    
    if ([byItem.identifier isEqualToString:CZToolbarItemIdentifierLess]) {
        [self setNeedsMainToolbarItemsUpdate];
    }
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierCameraSettings]) {
        CZCameraSettingsViewController *cameraSettingsViewController = [[CZCameraSettingsViewController alloc] initWithCamera:self.task.currentCamera microscopeModel:self.task.microscopeModel];
        cameraSettingsViewController.delegate = self;
        
        [self showWaitingHUD:YES];
        
        @weakify(self);
        [cameraSettingsViewController prepareForSettingsWithCompletionHandler:^(BOOL prepared) {
            @strongify(self);
            
            [self showWaitingHUD:NO];
            
            if (prepared == NO) {
                CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"LIVE_NO_CONTROL_ACCESS_MESSAGE") level:CZAlertLevelError];
                [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
                [alertController presentAnimated:YES completion:nil];
                return;
            }
            
            CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:cameraSettingsViewController];
            popover.delegate = self;
            popover.adjustsPopoverPositionWhenKeyboardAppears = YES;
            [popover presentFromToolbarItem:item animated:YES completion:nil];
        }];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierFluorescenceSnapMode]) {
        CZToggleButton *toggleButton = item.view;
        if (toggleButton.selectedIndex == kOnekeyFluorescenceButtonIndex) {
            [self.snapModesView switchToSnapMode:kCZCaptureModeOnekeyFluorescenceStandby];
            [self.task updateMode:kCZCaptureModeOnekeyFluorescenceStandby];
            [self updateFluorescenceSnapButtonStatus];
            [self.task switchToFluorescenceSnappingMode:kCZCaptureModeOnekeyFluorescenceStandby];
        } else if (toggleButton.selectedIndex == kManualFluorescenceButtonIndex) {
            [self.snapModesView switchToSnapMode:kCZCaptureModeManualFluorescence];
            [self.task updateMode:kCZCaptureModeManualFluorescence];
            [self updateFluorescenceSnapButtonStatus];
            [self.task switchToFluorescenceSnappingMode:kCZCaptureModeManualFluorescence];
        }
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierObjective]) {
        if (self.task.canGetCurrentZoomFromMNA && self.task.microscopeModel.canSupportMNA) {
            return YES;
        }
        
        [self.task.videoTool endMeasuring];
        
        CZObjectiveSelectionViewController *objectionSelectionViewController = [[CZObjectiveSelectionViewController alloc] initWithMicroscopeModel:self.task.microscopeModel];
        objectionSelectionViewController.delegate = self;
        
        CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:objectionSelectionViewController];
        popover.delegate = self;
        [popover presentFromToolbarItem:item animated:YES completion:nil];
        
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierRedo]) {
        [self.task redoAction];
        [self updateMaintoolbarItemsEnabledWhileEditing];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierUndo]) {
        [self.task undoAction];
        [self updateMaintoolbarItemsEnabledWhileEditing];
        return YES;
    }
    
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierZoomClickStop]) {
        if (self.task.canGetCurrentZoomFromMNA && self.task.microscopeModel.canSupportMNA) {
            return YES;
        }
        
        [self.task.videoTool endMeasuring];
        
        CZZoomClickStopSelectionViewController *zoomClickStopSelectionViewController = [[CZZoomClickStopSelectionViewController alloc] initWithMicroscopeModel:self.task.microscopeModel];
        zoomClickStopSelectionViewController.delegate = self;
        
        CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:zoomClickStopSelectionViewController];
        popover.delegate = self;
        [popover presentFromToolbarItem:item animated:YES completion:nil];
        
        return YES;
    }
    
    return NO;
}

#pragma mark - CZLiveSnapModesViewDelegate

- (void)snapModesViewDidSnapWithMode:(CZCaptureMode)snapMode {
    if (snapMode != kCZCaptureModeMacroPhoto && [self.task isCurrentCameraInvalid]) {
        CZLogv(@"video is not ready for snapping or recording");
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error = nil;
        BOOL isCompatible = [self.task.currentCamera isFirmwareCompatibleForSnapping:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isCompatible) {
                if (snapMode == kCZCaptureModeTimelapseVideoStandby) {
                    [self popUpTimelapseVideoSettingsView];
                    return;
                }
                
                if (snapMode == kCZCaptureModeRemoteStreamStandby) {
                    [self popUpRemoteStreamSettingsView];
                    return;
                }
                
                [self switchModeToNextMode];
            } else if (error.code == kCZCameraIncompatibleFirmwareVersionError) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"ERROR_OLD_AXIOCAM20X_FW") level:CZAlertLevelError];
                [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                [alert presentAnimated:YES completion:nil];
            }
        });
    });
}

- (void)shouldSnapModesViewSwitchTo:(CZCaptureMode)snapMode completionHandler:(void (^)(BOOL))completionHandler {
    //unsupported snap mode
    if (![[self.task availableModes] containsObject:@(snapMode)]) {
        completionHandler(NO);
        return;
    }
    
    CZCaptureMode previousMode = self.snapModesView.mode;
    if (snapMode == previousMode) {
        completionHandler(YES);
        return;
    }
    
    if (snapMode == kCZCaptureModeManualFluorescence || snapMode == kCZCaptureModeOnekeyFluorescenceStandby) {
        [self.task enterFluorescenceSnappingMode:snapMode completionHandler:^(BOOL canEnterFluorescenceSnappingMode) {
            if (canEnterFluorescenceSnappingMode == NO) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"LIVE_NO_CONTROL_ACCESS_MESSAGE") level:CZAlertLevelError];
                [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                [alert presentAnimated:YES completion:nil];
            }
            completionHandler(canEnterFluorescenceSnappingMode);
        }];
        return;
    }
    
    switch (previousMode) {
        case kCZCaptureModeEDOF:
            if (self.edofSnappedCount) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_DISCARD_EDOF_MSG") level:CZAlertLevelWarning];
                [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
                    completionHandler(NO);
                }];
                [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
                    [self endEDOFSnapping];
                    [self.snapPreviewView dismissPreview];
                    completionHandler(YES);
                }];
                [alert presentAnimated:YES completion:nil];
            } else {
                completionHandler(YES);
            }
            break;
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby:
        case kCZCaptureModeOnekeyFluorescencePlaying:
            if (self.task.snappedChannels.count > 0) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_DISCARD_EDOF_MSG") level:CZAlertLevelWarning];
                [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
                    completionHandler(NO);
                }];
                [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
                    [self.task endFluorescenceSnappingForMode:previousMode action:CZFluorescenceSnappingEndActionUndone];
                    [self.snapPreviewView dismissPreview];
                    [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
                    completionHandler(YES);
                }];
                [alert presentAnimated:YES completion:nil];
            } else {
                completionHandler(YES);
            }
            break;
        default:
            completionHandler(YES);
            break;
    }
}

- (void)snapModesViewWillSwitchTo:(CZCaptureMode)snapMode {
    CZCaptureMode previousMode = self.snapModesView.mode;
    switch (previousMode) {
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby:
        case kCZCaptureModeOnekeyFluorescencePlaying:
            [self.task exitFluorescenceSnappingMode];
            break;
        default:
            break;
    }
}

- (void)snapModesViewDidSwitchTo:(CZCaptureMode)snapMode {
    [self.task updateMode:snapMode];
    [self.task updateDefaultSnappingMode:snapMode];
    
    [self setNeedsVirtualToolbarItemsUpdate];
    
#if LIVE_REFACTORED
    self.fullscreenLastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
#endif
    if (self.snapModesView.mode == kCZCaptureModeEDOF && snapMode != kCZCaptureModeEDOF && self.edofSnappedCount <= 0) {
#if LIVE_REFACTORED
        [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagDrawing enable:YES];
#endif
    }
    else if (snapMode == kCZCaptureModeManualFluorescence || snapMode == kCZCaptureModeOnekeyFluorescenceStandby) {
        [self updateBuiltInCameraVisibility];
    }
    else {
#if LIVE_REFACTORED
        self.selectView.hidden = YES;
#endif
        if (snapMode == kCZCaptureModeSnapFromStream) {
#if HELP_OVERLAY
            if ([self.task.currentCamera isKindOfClass:[CZKappaCamera class]]) {
                [self showHelpOverlayOnFastSnapping];
            }
#endif
        } else if (snapMode == kCZCaptureModeEDOF) {
#if HELP_OVERLAY
            [self showHelpOverlayOnEDOF];
            if ([self.displayOptionsView isButtonSelected:kCZDisplayOptionsViewButtonTagDrawing]) {
                [self turnOffDrawingTube];
            }
            
            [self.displayOptionsView setButton:kCZDisplayOptionsViewButtonTagDrawing enable:NO];
#endif
        }
        else if (snapMode == kCZCaptureModeMacroPhoto) {
            // Macro mode doesn't need web camera, so doesn't need reconnecting HUD.
            if (self.reconnectingHUD) {
                [self.reconnectingHUD dismiss:NO];
                self.reconnectingHUD = nil;
            }
#if HELP_OVERLAY
            BOOL modeChanged = (snapMode != kCZCaptureModeMacroPhoto);
            if (modeChanged) {
                [self showHelpOverlayOnMacro];
            }
#endif
            [self updateBuiltInCameraVisibility];
            return;  // skip following common code
        }

        if (snapMode == kCZCaptureModeSnapFromStream) {
            [self.task updateElementLayerLogicalScaling:1.0];
        } else {
            CGFloat ratio = self.task.currentCamera.snapResolution.width / self.task.currentCamera.liveResolution.width;
            [self.task updateElementLayerLogicalScaling:ratio];
        }
        
        [self updateBuiltInCameraVisibility];
    }
}

#pragma mark - CZLiveTaskDelegate
- (void)camera:(CZCamera *)camera connectingStatusChanged:(BOOL)status {
    CZLogv(@"Camera connecting status:%d", status);
    BOOL reachable = [self.task isCurrentCameraReachable];
    if (self.connectionStatus != status) {
        self.connectionStatus = status;
        [self updateSnapModesViewStatus:status];
        [self updateToolbarItemsStatus:status];
    }

    const BOOL showReconnectingHUD = (self.task.currentCamera &&
                                      self.task.isReconnecting &&
                                      !reachable &&
                                      self.snapModesView.mode != kCZCaptureModeMacroPhoto);
    if (showReconnectingHUD) {
        if (self.reconnectingHUD == nil &&
            self.task.viewModel.isEditing == NO &&
            [self.task.videoTool isShowingSelection] == NO) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.userInteractionEnabled = NO;
            self.reconnectingHUD = hud;
            [hud setColor:kSpinnerBackgroundColor];
        }
        
        [self refreshReconnectingLabel];
    } else {
        if (self.reconnectingHUD) {
            [self.reconnectingHUD dismiss:NO];
            self.reconnectingHUD = nil;
        }
    }
}

- (void)liveTaskDidLostCamera:(CZCamera *)camera {
    // Stop recording while switching camera.
    if (self.snapModesView.mode == kCZCaptureModeVideoRecording) {
        [self switchModeToNextMode];
        [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kRecordingTipToastIdentifier];
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_INTERRUPTED") level:CZAlertLevelError];
        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
        [alertController presentAnimated:YES completion:nil];
    }
#if LIVE_REFACTORED
    if (_isRemoteCamera) {
        self.remoteCameraView.hidden = NO;
        [self diableAllFuctionBarButtons];
        [self setMagnifierEnabled:!_isRemoteCamera];
        [self showDisplayOptionsButton:!_isRemoteCamera];
        self.displayOptionsView.hidden = YES;
        self.displayOptionsButton.selected = NO;
        [self showSnapControl:!_isRemoteCamera];
    }
#endif
}

- (void)liveTaskDidPauseSnapping:(CZCameraSnappingInfo *)snappingInfo reason:(CZCameraSnappingPauseReason)reason {
    NSString *message = [NSString stringWithFormat:L(@"LIVE_FL_SWITCH_LIGHT"), @(snappingInfo.lightSource.wavelength)];
    [[CZToastManager sharedManager] showToastMessage:message
                                          identifier:kSwitchLightTipToastIdentifier
                                        dismissAfter:kCZAnimationForever
                                          sourceRect:CGRectOffset(self.virtualToolbar.frame, 0, -16.0)
                                            animated:YES];
}

- (void)liveTaskDidContinueSnapping {
    [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
}

- (void)liveTaskDidFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {

    //When the isSnapping set to NO before getting a image, do nothing with it.
    if (_isSnapping == NO) return;
    
    if (snappingInfo.currentSnappingNumber == snappingInfo.totalSnappingNumber) {
        if (self.snappingHUD) {
            [self.snappingHUD dismiss:YES];
            self.snappingHUD = nil;
        }
    }

    // Only set the isSnapping to NO when snap with built-in camera
    if (self.snapModesView.mode == kCZCaptureModeMacroPhoto) {
        _isSnapping = NO;
    }
    
    if (self.snapModesView.mode != kCZCaptureModeOnekeyFluorescencePlaying) {
        self.snapModesView.snapButton.enabled = YES;
        self.snapModesView.snapSelectButton.hidden = NO;
        [self updateToolbarItemsStatus:YES];
    }
    
    if (error != nil && snappingInfo.image == nil) {
#if LIVE_REFACTORED
        //if current active tab is not the live tab, we should dismiss the error alert dialogue
        if ([CZAppDelegate get].tabBar.activatedControllerIndex != kCZLiveViewController) {
            return;
        }
#endif
    }
    
    if (error) {
        if (self.snappingHUD) {
            [self.snappingHUD dismiss:YES];
            self.snappingHUD = nil;
        }
        
        if (error.userInfo[NSLocalizedDescriptionKey]) {
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:error.localizedDescription level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        }
        
        if (self.snapModesView.mode == kCZCaptureModeOnekeyFluorescencePlaying) {
            if (self.task.snappedChannels.count > 0) {
                CZImageDocument *document = [[CZImageDocument alloc] initFromChannels:self.task.channels];
                self.snapPreviewView.shouldShowOperationHeaderView = YES;
                [self.snapPreviewView presentPreview];
                [self.snapPreviewView updatePreviewWithImage:[document outputThumbnail]];
            }
            
            [self.snapModesView switchToSnapMode:kCZCaptureModeOnekeyFluorescenceStandby];
            [self.task updateMode:kCZCaptureModeOnekeyFluorescenceStandby];
            [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
        }
        
        return;
    }
    
    //control with snap mode
    CZCaptureMode mode = self.snapModesView.mode;
    if (mode == kCZCaptureModePhoto ||
        mode == kCZCaptureModeSnapFromStream ||
        mode == kCZCaptureModeMacroPhoto ||
        mode == kCZCaptureModeSnapDenoise) {
        
        CZImageDocument *document = [[CZImageDocument alloc] initFromSnappingInfo:snappingInfo];
        [self.task presentNewDocManagerWithDocument:document showsAnnotation:(self.snapModesView.mode != kCZCaptureModeMacroPhoto)];
        
        // Log snap action here
        CZUserInteractionTracker *tracker = [CZUserInteractionTracker defaultTracker];
        [tracker logEventWithCategory:kCZTimingEventCategory
                                event:@"snap_button_pressed"
                                 time:[NSDate date]
                           parameters:[NSDictionary dictionaryWithObjectsAndKeys:self.task.currentCamera.macAddress, @"camera_mac_address", nil]
                     postNotification:NO];
        
    } else if (mode == kCZCaptureModeEDOF) {
        self.snapPreviewView.shouldShowOperationHeaderView = YES;
        [self.snapPreviewView presentPreview];
        [self mergeEDOFWithImageInfo:snappingInfo];
        [self.snapPreviewView updatePreviewWithImage:[self.edofMergedImage image]];
    } else if (mode == kCZCaptureModeManualFluorescence || mode == kCZCaptureModeOnekeyFluorescenceStandby || mode == kCZCaptureModeOnekeyFluorescencePlaying) {
        for (CZLiveChannel *channel in self.task.channels) {
            if ([channel isKindOfClass:[CZLightSourceChannel class]]) {
                CZLightSourceChannel *lightSourceChannel = (CZLightSourceChannel *)channel;
                if ([lightSourceChannel.lightSource isEqualToLightSource:snappingInfo.lightSource]) {
                    lightSourceChannel.snappingInfo = snappingInfo;
                    break;
                }
            } else if ([channel isKindOfClass:[CZVirtualLightSourceChannel class]]) {
                CZVirtualLightSourceChannel *virtualLightSourceChannel = (CZVirtualLightSourceChannel *)channel;
                if (virtualLightSourceChannel.isSelected) {
                    virtualLightSourceChannel.snappingInfo = snappingInfo;
                    break;
                }
            }
        }
        
        if (mode == kCZCaptureModeManualFluorescence) {
            CZImageDocument *document = [[CZImageDocument alloc] initFromChannels:self.task.channels];
            self.snapPreviewView.shouldShowOperationHeaderView = YES;
            [self.snapPreviewView presentPreview];
            [self.snapPreviewView updatePreviewWithImage:[document outputThumbnail]];
        }
        
        if (mode == kCZCaptureModeOnekeyFluorescencePlaying && snappingInfo.currentSnappingNumber == snappingInfo.totalSnappingNumber) {
            CZImageDocument *document = [[CZImageDocument alloc] initFromChannels:self.task.channels];
            self.snapPreviewView.shouldShowOperationHeaderView = YES;
            [self.snapPreviewView presentPreview];
            [self.snapPreviewView updatePreviewWithImage:[document outputThumbnail]];
            
            [self.snapModesView switchToSnapMode:kCZCaptureModeOnekeyFluorescenceStandby];
            [self.task updateMode:kCZCaptureModeOnekeyFluorescenceStandby];
        }
    } else {
        CZLogv(@"ERROR!! Unknown snapping mode after finished snapping.");
    }
}

- (void)liveTaskDidBeginRecordingWithFileName:(NSString *)fileName {
    NSString *tipText = [[NSString alloc] initWithFormat:@"%@\n%@",
                         fileName,
                         L(@"VIDEO_RECORDING_TIP")];
    [[CZToastManager sharedManager] showToastMessage:tipText
                                          identifier:kRecordingTipToastIdentifier
                                         dismissAfter:kCZAnimationForever
                                          sourceRect:CGRectOffset(self.mainToolbar.frame, 0, -16)
                                            animated:YES];
}

- (void)liveTaskDidFinishRecording {
    [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kRecordingTipToastIdentifier];
    
    if ([CZCommonUtils freeSpaceInBytes] <= kFreeSpaceLowerLimit) {
        CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_NO_SPACE") level:CZAlertLevelError];
        [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
        [alertController presentAnimated:YES completion:nil];
    }

    // Present the snap preview
    if (self.task.firstRecordingFrame && self.task.state == CZTaskStateForeground) {
        self.snapPreviewView.shouldShowOperationHeaderView = NO;
        [self.snapPreviewView updatePreviewWithImage:self.task.firstRecordingFrame];
        [self.snapPreviewView presentPreview];
        self.snapPreviewView.imageFromRecording = YES;
    }
    
#if DCM_REFACTORED
    if ([[CZDCMManager defaultManager] isCMServerConnected]) {
        [self updateSnapUIStatus:YES];
    }
#endif
}

- (void)liveTaskWillSaveDocument {
    [self showWaitingHUD:YES];
}

- (void)liveTaskDidSaveDocument {
    UIImage *thumbnail = [self.task.snappedImageDocument outputThumbnail];
    self.task.snappedImageDocument = nil;
    
    self.snapPreviewView.shouldShowOperationHeaderView = NO;
    [self.snapPreviewView presentPreview];
    [self.snapPreviewView updatePreviewWithImage:thumbnail];
    
    [self showWaitingHUD:NO];
}

- (void)liveTaskDidGetVideoFrame:(UIImage *)frame {
    if (self.task.isRemoteCamera) {
#if LIVE_REMOTE_CAMERA
        self.remoteCameraView.hidden = YES;
        [self showSnapControl:YES];
        [self snapOptionButtonClicked:_snapSelectSnapFromStreamButton];
        _snapSelectButton.hidden = YES;
        [self diableAllFuctionBarButtons];
        [self showDisplayOptionsButton:NO];
#endif
    } else {
        if (self.snapModesView.mode != kCZCaptureModeOnekeyFluorescencePlaying) {
            if (_snappingHUD == nil) {  // when snap is finished and first frame comes back
                _isSnapping = NO;
            }
        }
        
        self.snapModesView.snapSelectButton.hidden = NO;
        [self updateSnapModesViewStatus:YES];
        [self updateToolbarItemsStatus:YES];
        
        //TODO : hide the snap selection button when in full screen
#ifdef LIVE_FULL_SCREEN
        if (![self.videoTool isShowingSelection] &&
            !self.isEditingMode &&
            !self.isFullScreen) {
            _snapSelectButton.hidden = NO;
        }
#endif
    }
}

- (void)liveTaskDidConfigurateCamera:(CZCamera *)camera {
    // switch to default mode if current camera doesn't support current mode.
    if (camera) {
        [self.snapModesView updateWithAvaliableSnapModes:[self.task availableModes]];
        
        switch (self.task.defaultSnappingMode) {
            case kCZCaptureModeManualFluorescence:
            case kCZCaptureModeOnekeyFluorescenceStandby: {
                [self.task enterFluorescenceSnappingMode:self.task.defaultSnappingMode completionHandler:^(BOOL canEnterFluorescenceSnappingMode) {
                    if (canEnterFluorescenceSnappingMode == YES) {
                        [self.snapModesView switchToSnapMode:self.task.defaultSnappingMode];
                        [self.task updateMode:self.task.defaultSnappingMode];
                    } else {
                        [self.snapModesView switchToSnapMode:kCZCaptureModePhoto];
                        [self.task updateMode:kCZCaptureModePhoto];
                        [self.task updateDefaultSnappingMode:kCZCaptureModePhoto];
                    }
                    [self updateBuiltInCameraVisibility];
                    [self setNeedsVirtualToolbarItemsUpdate];
                }];
                break;
            }
            default: {
                [self.snapModesView switchToSnapMode:self.task.defaultSnappingMode];
                [self.task updateMode:self.task.defaultSnappingMode];
                [self updateBuiltInCameraVisibility];
                [self setNeedsVirtualToolbarItemsUpdate];
                break;
            }
        }
    }

    if ([self.snapModesView isSnapEnabled] && (self.snapModesView.mode != kCZCaptureModeMacroPhoto)) {  // Disable snap until first video frame comes up.
        self.snapModesView.snapButton.enabled = NO;
        self.snapModesView.snapSelectButton.hidden = YES;
        [self.snapModesView collapseSnapModes];
    }
}

- (void)liveTaskChannelsWillChange:(CZLiveTask *)task {
    for (CZLiveChannel *channel in self.task.channels) {
        [channel removeObserver:self];
    }
}

- (void)liveTaskChannelsDidChange:(CZLiveTask *)task {
    for (CZLiveChannel *channel in self.task.channels) {
        [channel addObserver:self];
    }
    
    [self updateFluorescenceSnapButtonStatus];
    [self setNeedsVirtualToolbarItemsUpdate];
    [self.snapModesView switchToSnapMode:self.snapModesView.mode];
    [self.snapModesView updateWithAvaliableSnapModes:[self.task availableModes]];
}

- (void)liveTaskGeminiCameraDidReceiveHardwareEvent:(CZLiveTask *)task availableEventType:(CZGeminiCameraEventStandType)type {
    if (type == NSNotFound || self.task.state != CZTaskStateForeground) {
        [[CZToastManager sharedManager] showToastMessage:L(@"LIVE_MORE_THAN_ONE_CLIENT_CONNECTED") sourceRect:CGRectOffset(self.view.frame, 0, CGRectGetMidY(self.view.frame))];
    } else {
        if (self.snapModesView.mode == kCZCaptureModeVideoRecording) {
            // stop recording
            [self switchModeToNextMode];
            return;
        }
        
        if (_isSnapping ||
            self.snapModesView.mode == kCZCaptureModeTimelapseVideoRecording ||
            self.snapModesView.mode == kCZCaptureModeOnekeyFluorescencePlaying ||
            self.edofSnappedCount ||
            (self.snapModesView.mode == kCZCaptureModeManualFluorescence && self.task.snappedChannels.count)) {
            [[CZToastManager sharedManager] showToastMessage:L(@"LIVE_CONFLICT_INTERACTION_FOR_STAND_SNAP") sourceRect:CGRectOffset(self.view.frame, 0, CGRectGetMidY(self.view.frame))];
            return;
        }
        
        switch (type) {
            case CZGeminiCameraEventStandSnap: {
                // switch to normal snap mode
                [self.snapModesView switchToSnapMode:kCZCaptureModePhoto];
            }
                break;
            case CZGeminiCameraEventStandRecording: {
                // switch to record mode
                [self.snapModesView switchToSnapMode:kCZCaptureModeVideoStandby];
            }
                break;
        }
        [self switchModeToNextMode];
    }
}

- (void)liveTaskObjectiveSelectionButtonHiddenDidChange:(CZLiveTask *)task {
    [self setNeedsMainToolbarItemsUpdate];
}

- (void)liveTaskObjectiveSelectionButtonEnabledDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierObjective];
    item.button.enabled = self.task.objectiveSelectionButtonEnabled;
}

- (void)liveTaskObjectiveSelectionButtonTitleDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierObjective];
    [item.button setTitle:self.task.objectiveSelectionButtonTitle forState:UIControlStateNormal];
    [item alignImageAndTitle];
}

- (void)liveTaskObjectiveSelectionButtonImageDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierObjective];
    [item.button setImage:self.task.objectiveSelectionButtonImage forState:UIControlStateNormal];
    [item alignImageAndTitle];
}

- (void)liveTaskZoomSelectionButtonHiddenDidChange:(CZLiveTask *)task {
    [self setNeedsMainToolbarItemsUpdate];
}

- (void)liveTaskZoomSelectionButtonEnabledDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierZoomClickStop];
    item.button.enabled = self.task.zoomSelectionButtonEnabled;
}

- (void)liveTaskZoomSelectionButtonTitleDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierZoomClickStop];
    [item.button setTitle:self.task.zoomSelectionButtonTitle forState:UIControlStateNormal];
    [item alignImageAndTitle];
}

- (void)liveTaskZoomSelectionButtonImageDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierZoomClickStop];
    [item.button setImage:self.task.zoomSelectionButtonImage forState:UIControlStateNormal];
    [item alignImageAndTitle];
}

- (void)liveTaskFilterSetSelectionButtonHiddenDidChange:(CZLiveTask *)task {
    [self setNeedsMainToolbarItemsUpdate];
}

- (void)liveTaskFilterSetSelectionButtonEnabledDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierFilterSet];
    item.button.enabled = self.task.filterSetSelectionButtonEnabled;
}

- (void)liveTaskFilterSetSelectionButtonTitleDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierFilterSet];
    [item.button setTitle:self.task.filterSetSelectionButtonTitle forState:UIControlStateNormal];
    [item alignImageAndTitle];
    
    if (self.snapModesView.mode == kCZCaptureModeOnekeyFluorescenceStandby) {
        [self updateFluorescenceSnapButtonStatus];
    }
}

- (void)liveTaskFilterSetSelectionButtonImageDidChange:(CZLiveTask *)task {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifierInStack:CZToolbarItemIdentifierFilterSet];
    [item.button setImage:self.task.filterSetSelectionButtonImage forState:UIControlStateNormal];
    [item alignImageAndTitle];
}

#pragma mark - CZLiveChannelObserver

- (void)channelNameDidChange:(CZLiveChannel *)channel {
    NSUInteger index = [self.task.channels indexOfObject:channel];
    [[CZDefaultSettings sharedInstance] setChannelName:channel.name atIndex:index forCamera:self.task.currentCamera];
}

- (void)channelColorDidChange:(CZLiveChannel *)channel {
    NSUInteger index = [self.task.channels indexOfObject:channel];
    [[CZDefaultSettings sharedInstance] setChannelColor:channel.color atIndex:index forCamera:self.task.currentCamera];
    
    if (channel.snappingInfo) {
        CZImageDocument *document = [[CZImageDocument alloc] initFromChannels:self.task.channels];
        [self.snapPreviewView presentPreview];
        [self.snapPreviewView updatePreviewWithImage:[document outputThumbnail]];
    }
}

- (void)channelSelectedDidChange:(CZLiveChannel *)channel {
    [self updateFluorescenceSnapButtonStatus];
}

- (void)liveChannelSnappingInfoDidChange:(CZLiveChannel *)channel {
    if (![channel isKindOfClass:[CZVirtualLightSourceChannel class]]) {
        return;
    }
    
    if (channel.snappingInfo == nil) {
        return;
    }
    
    NSInteger index = [self.task.channels indexOfObject:channel];
    if (index == NSNotFound || index + 1 >= self.task.channels.count) {
        return;
    }
    
    if (index != self.task.channels.count - 1) {
        channel.selected = NO;
    }
    
    CZLiveChannel *nextChannel = self.task.channels[index + 1];
    nextChannel.selected = YES;
    nextChannel.enabled = YES;
}

#pragma mark - CZChannelButtonGroupDelegate

- (void)channelButtonGroup:(CZChannelButtonGroup *)buttonGroup didTapButton:(CZChannelButton *)button atIndex:(NSUInteger)index {
    if ([button.channel isKindOfClass:[CZVirtualLightSourceChannel class]] &&
        !button.channel.isSelected) {
        for (CZLiveChannel *channel in self.task.channels) {
            channel.selected = (channel == button.channel);
        }
    } else if ([button.channel isKindOfClass:[CZLightSourceChannel class]] &&
        [self.task.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
        CZLightSourceChannel *channel = (CZLightSourceChannel *)button.channel;
        [(CZCamera<CZFluorescenceSnapping> *)self.task.currentCamera toggleLightSourceAtPosition:channel.lightSource.position];
    }
}

#pragma mark - CZPopoverControllerDelegate

- (void)popoverControllerWillDismiss:(CZPopoverController *)popoverController {
}

- (void)popoverControllerDidDismiss:(CZPopoverController *)popoverController {
}

#pragma mark - CZCameraSettingsViewControllerDelegate

- (void)cameraSettingsViewControllerWillDismissAutomatically:(UIViewController *)sender {
}

- (void)cameraSettingsViewControllerDidDismissAutomatically:(UIViewController *)sender {
}

#pragma mark - CZObjectiveSelectionViewControllerDelegate

- (void)objectiveSelectionViewController:(CZObjectiveSelectionViewController *)objectiveSelectionViewController didSelectObjectiveAtPosition:(NSUInteger)position {
    [self.task selectObjectiveAtPosition:position];
}

#pragma mark - CZZoomClickStopSelectionViewControllerDelegate

- (void)zoomClickStopSelectionViewController:(CZZoomClickStopSelectionViewController *)zoomClickStopSelectionViewController didSelectZoomClickStopAtPosition:(NSUInteger)position {
    [self.task selectZoomClickStopAtPosition:position];
}

#pragma mark - CZLiveSnapPreviewViewDelegate
- (void)liveSnapPreviewDidDeleteImage:(UIImage *)image {
    @weakify(self);
    CZAlertController *alertController = [CZAlertController alertControllerWithTitle:nil message:L(@"LIVE_DISCARD_EDOF_MSG") level:CZAlertLevelWarning];
    [alertController addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:NULL];
    [alertController addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        CZCaptureMode mode = self.snapModesView.mode;
        switch (mode) {
            case kCZCaptureModeEDOF:
                [self endEDOFSnapping];
                [self.snapPreviewView dismissPreview];
                break;
            case kCZCaptureModeManualFluorescence:
            case kCZCaptureModeOnekeyFluorescenceStandby:
            case kCZCaptureModeOnekeyFluorescencePlaying:
                [self.task endFluorescenceSnappingForMode:mode action:CZFluorescenceSnappingEndActionUndone];
                [self.snapPreviewView dismissPreview];
                [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
                break;
            default:
                break;
        }
    }];
    [alertController presentAnimated:YES completion:nil];
}

- (void)liveSnapPreviewDidSaveImage:(UIImage *)image {
    CZCaptureMode mode = self.snapModesView.mode;
    switch (mode) {
        case kCZCaptureModeEDOF: {
            CZImageDocument *document = [[CZImageDocument alloc] initFromSnappingInfo:self.edofMergedImage];
            [self.task presentNewDocManagerWithDocument:document showsAnnotation:YES];
            [self endEDOFSnapping];
            break;
        }
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby:
        case kCZCaptureModeOnekeyFluorescencePlaying:
            [self.task saveMultichannelImage];
            [self.task endFluorescenceSnappingForMode:mode action:CZFluorescenceSnappingEndActionDone];
            [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kSwitchLightTipToastIdentifier];
            break;
        default:
            break;
    }
}

- (void)liveSnapPreviewDidSelectPreviewImage:(UIImage *)image {
    if (self.snapPreviewView.isImageFromRecording == NO) {
        CZImageTask *imageTask = self.task.manager.imageTask;
        imageTask.currentDisplayCurve = [self.task.videoTool currentDisplayCurve];
        imageTask.microscopeModel = self.task.microscopeModel;
        imageTask.filePath = self.task.snappedImageFilePath;
        [self.task.manager switchToImageTask];
    } else {
        NSURL *url = [NSURL fileURLWithPath:self.task.recordedVideoFile];
        CZMoviePlayerViewController *player = [[CZMoviePlayerViewController alloc] initWithContentURL:url];
        [self.navigationController pushViewController:player animated:YES];
    }
    [self.snapPreviewView dismissPreview];
    self.snapPreviewView.imageFromRecording = NO;
}

- (void)liveSnapPreviewDidSelectUndoButton {
    [self undoMeregeAction];
}

- (void)liveSnapPreviewDidEnterFullScreen:(BOOL)isFullScreen {
    self.snapModesView.hidden = isFullScreen;
}

- (void)liveSnapPreviewDidDismiss {
    self.task.snappedImageDocument = nil;
    self.task.snappedImageFilePath = nil;
}

#pragma mark - CZBuiltInCameraViewControllerTapDelegate

- (BOOL)shouldBuiltInCameraView:(CZBuiltInCameraViewController *)cameraView handleTapGesture:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint pointInView = [gestureRecognizer locationInView:self.view];

    CGRect snapButtonFrame = [self.snapModesView.snapButton convertRect:self.view.bounds toView:nil];
    BOOL tapOnSomething = (!self.snapModesView.snapButton.isHidden && CGRectContainsPoint(snapButtonFrame, pointInView));
    if (!tapOnSomething) {
        tapOnSomething = (!self.snapModesView.snapSelectVideoButton.isHidden && CGRectContainsPoint(self.snapModesView.snapSelectVideoButton.frame, pointInView));
    }

    [self handleDismissTouchInPoint:pointInView];

    return !tapOnSomething;
}

- (void)builtinCameraView:(CZBuiltInCameraViewController *)cameraView didHandleTapGesture:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint pointInView = [gestureRecognizer locationInView:self.view];
    [self handleDismissTouchInPoint:pointInView];
}

#pragma mark - CZAnnotationPickerViewDataSource

- (NSArray<NSNumber *> *)annotationButtonsOfAnnotationPickerView:(CZAnnotationPickerView *)annotationPickerView {
    return [self.task.viewModel supportAnnotationTypes];
}

#pragma mark - CZAnnotationPickerViewDelegate

- (void)annotationPickerView:(CZAnnotationPickerView *)annotationPickerView didSelectAnnotationID:(CZAnnotationButtonID)selectedAnnotionButtonID {
    CZAnnotationEditingViewController *imageEditingController = (CZAnnotationEditingViewController *)self.imageEditingController;
    [imageEditingController annotationButtonPressed:selectedAnnotionButtonID on:NO];
}

#pragma mark - CZImageEditingViewControllerDataSource

- (CZDocManager *)editingImageDocument {
    return self.task.videoTool.docManager;
}

- (UIView *)imageView {
    return self.task.videoTool.annotationView;
}

- (CGFloat)imageViewZoomScale {
    return self.task.videoTool.annotationViewZoomScale;
}

- (id<CZMagnifyDelegate>)magnifyDelegate {
    return (id<CZMagnifyDelegate>)self.task.videoTool;
}

- (UIView *)editingImageView {
    return nil;
}

#pragma mark - CZImageEditingViewControllerDelgate

- (void)viewControllerDidBeginEditing:(UIViewController *)viewController {
    self.task.videoTool.shouldNotChangeScaleBarVisibility = YES;
    if (_imageEditingController == viewController) {
        [self.task.videoTool dismissMagnifierView];
    }
}

- (void)viewControllerDidEndEditing:(UIViewController *)viewController {
    self.task.videoTool.shouldNotChangeScaleBarVisibility = NO;
    if (_imageEditingController == viewController) {
        [self.task writeElementXMLFile];
        if ((id)viewController == self.task.videoTool.combineDelegate) {
            self.task.videoTool.combineDelegate = nil;
        }
        _imageEditingController = nil;
        
#if DCM_REFACTORED
        //if connected with CM service, show the function bar in tab bar
        if ([CZDCMManager defaultManager].hasConnectedToDCMServer) {
            CZTabBarController *tabBarController = [CZAppDelegate get].tabBar;
            [tabBarController showFuncitonBar];
            if (_currentCamera != nil) {
                [tabBarController restoreTabsExcludeIndex:kCZLiveViewController];
            }
            if (_shouldConfiguratedTemplateApplyAfterEditing) {
                [self configurationApplyNotification:nil];
            }
        }
#endif
    }
}

- (NSUInteger)viewControllerBeginCropping:(UIViewController *)viewController {
#if LIVE_REFACTORED
    return CZCroppingModeUnknown;
#endif
    return 0;
}

- (void)viewControllerDidModifyDocManager:(UIViewController *)viewController {
    CZToolbarItem *item = [self.mainToolbar itemForIdentifier:CZToolbarItemIdentifierAnnotationTools];
    if ([item.view isKindOfClass:[CZAnnotationPickerView class]]) {
        CZAnnotationPickerView *annotationPickerView = (CZAnnotationPickerView *)item.view;
        [annotationPickerView deselectAnnotationButton];
    }
    [self updateMaintoolbarItemsEnabledWhileEditing];
}

#pragma mark - KVO Observing

- (void)observeValueForKeyPath:(nullable NSString *)keyPath
                      ofObject:(nullable id)object
                        change:(nullable NSDictionary<NSKeyValueChangeKey, id> *)change
                       context:(nullable void *)context {
    if (object == self.task.viewModel) {
        if ([keyPath isEqualToString:@"snapAvailable"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                
            }
        } else if ([keyPath isEqualToString:@"snapModesViewVisible"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                
            }
        } else if ([keyPath isEqualToString:@"reconnecting"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                
            }
        } else if ([keyPath isEqualToString:@"isEditing"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                [self enterAnnotationEditingMode:newValue];
            }
        }
    }
}

#pragma mark - Getters
- (CZLiveSnapModesView *)snapModesView {
    if (_snapModesView == nil) {
        _snapModesView = [[CZLiveSnapModesView alloc] initWithSnapModes:self.task.allSnapModes];
        _snapModesView.delegate = self;
    }
    return _snapModesView;
}

- (CZLiveSnapPreviewView *)snapPreviewView {
    if (_snapPreviewView == nil) {
        _snapPreviewView = [[CZLiveSnapPreviewView alloc] init];
        _snapPreviewView.delegate = self;
        _snapPreviewView.isAccessibilityElement = YES;
        _snapPreviewView.accessibilityIdentifier = @"LiveSnapPreviewView";
    }
    return _snapPreviewView;
}

- (UIView *)shutterAnimationView {
    if (_shutterAnimationView == nil) {
        _shutterAnimationView = [[UIView alloc] initWithFrame:self.view.bounds];
        [_shutterAnimationView setBackgroundColor:[UIColor whiteColor]];
        [_shutterAnimationView setHidden:YES];
    }
    return _shutterAnimationView;
}

@end
