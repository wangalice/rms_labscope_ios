//
//  CZVideoTool.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZVideoTool.h"
#include <libkern/OSAtomic.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZDefaultSettings+VideoTool.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZMagnifierView.h"
#import "CZAppearanceSettingViewController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZToastManager.h"

#if DCM_REFACTORED
#import "CZFileListManager.h"
#endif

#if DCM_REFACTORED
#import "CZDCMManager.h"
#import "CZDCMManager+DefaultSettings.h"
#endif

#import "CZDefaultSettings+CZElementScaleBar.h"

static const int kInvalidWidth = 32;
static const int kInvalidHeight = kInvalidWidth;
static const long kInvalidTime = -1;
static const long kMagnifierTimeout = 10;

typedef NS_ENUM(NSUInteger, CZVideoToolSelectionMode){
    kCZVideoToolNoneSelection,
    kCZVideoToolDisplayCurveSelection,
    kCZVideoToolOverlaySelection
};

static int GCD(int a, int b) {
    int r;
    if (a < b) {
        r = a;
        a = b;
        b = r;
    }
    
    while (b > 0) {
        r = a%b;
        a = b;
        b = r;
    }
    return a;
}

@interface CZVideoTool () <UIPopoverPresentationControllerDelegate, CZElementLayerDelegate, CZUndoManaging, CZMagnifyDelegate, CZAppearanceSettingViewControllerDelegate> {
  @private
    CALayer *_overlayLayers[kCZOverlayModeCount];
    CGRect _lastBoundBox;
    CZSelectTool *_selectTool;  // only available when beginMeasuring
    UIImage *_blankImage;  // NOTICE: assign, unretained unsafe
    time_t _magnifierHideTime;
    volatile int32_t _processImageCount;
}

@property (nonatomic, retain) UIImage *lastFrame;

@property (nonatomic, assign) CZVideoToolSelectionMode currentSelectionMode;
@property (nonatomic, retain) UIView *overlayView;
@property (nonatomic, retain) CALayer *mainOverlayLayer;

@property (nonatomic, retain) CZMagnifierView *magnifierView;
@property (nonatomic, assign) BOOL magnifierViewHidden;
@property (nonatomic, retain) NSTimer *magnifierUpdatingTimer;
@property (nonatomic, assign) BOOL magnifierNeedsUpdating;

@property (nonatomic, readonly, retain) CZSelectTool *selectTool;
@property (nonatomic, retain) CZAppearanceSettingViewController *overlayAppearanceViewController;
@property (nonatomic, retain) NSNumber *overlayKey;

- (void)dispatchOutput:(GPUImageOutput *)output size:(CGSize)size completion:(void (^)(void))completion;

- (void)updateOverlay;

- (CALayer *)newLayerInBoundBox:(CGRect)boundBox
                 forOverlayMode:(CZOverlayMode)mode
                    fieldOfView:(float)fieldOfView;

+ (NSString *)nameOfOverlayMode:(CZOverlayMode)mode;

@end

@implementation CZVideoTool

- (id)init {
    self = [super init];
    if (self) {
        _currentSelectionMode = kCZVideoToolNoneSelection;
        _currentDisplayCurve = CZDisplayCurveLinear;
        _currentOverlay = kCZOverlayNone;
        
        _view = [[CZImageOutputView alloc] initWithMainImageViewIndex:_currentDisplayCurve
                                                 andForcesFourToThree:NO];
        [_view setDelegate:self];
        
        UIPinchGestureRecognizer *pinGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
        [_view addGestureRecognizer:pinGesture];
        [pinGesture release];
        
        _overlayView = [[UIView alloc] init];
        _overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _overlayView.clipsToBounds = YES;
        [_view addSubview:_overlayView];
        
        _annotationView = [[UIView alloc] init];
        _annotationView.autoresizingMask = UIViewAutoresizingNone;
        _annotationView.layer.anchorPoint = CGPointZero;
        [_overlayView addSubview:_annotationView];
        _annotationViewZoomScale = 1;
        
        UIGraphicsBeginImageContext(CGSizeMake(1, 1));
        UIImage *dummyImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        CZElementLayer *elementLayer = [[CZElementLayer alloc] init];
#if DCM_REFACTORED
        NSString *directory = [CZLocalFileList sharedInstance].documentPath;
#else
        NSString *directory = [NSFileManager defaultManager].cz_documentPath;
#endif
        CZImageDocument *document = [[CZImageDocument alloc] initWithImage:dummyImage grayscale:NO];
        _docManager = [[CZDocManager alloc] initWithDirectory:directory document:document elementLayer:elementLayer];
        [_docManager addObserver:self forKeyPaths:@"elementLayerModified", nil];
        elementLayer.delegate = self;
        [elementLayer release];
        [document release];
        
        _fieldOfView = kInvalidScaling;
        
        for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
            _overlayLayers[i] = NULL;
        }
        
        _lastFrameSize = CGSizeZero;
        _lastBoundBox = CGRectZero;
        _preferredLiveResolution = CGSizeMake(640, 480);  // minimum possible live resolution
        _needUpdateScaleBar = NO;
        
        CGRect rootFrame = [[UIScreen mainScreen] bounds];
        _magnifierView = [[CZMagnifierView alloc] initWithFrame:rootFrame];
        _magnifierView.magnifyFactor = 3.0;
        self.magnifierViewHidden = YES;
        [_magnifierView setHidden:YES];
        _magnifierView.alpha = 0.0;
        [_view addSubview:_magnifierView];
        
        // Use a timer to update magnifier content to avoid performance issue
        // while moving pressed finger on the screen.
        // 
        // With this the magnifier has fps = 16, which is a balance between
        // performance and user experience.
        self.magnifierEnabled = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appBecomesActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appWillResignActive:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];

    [_magnifierUpdatingTimer invalidate];
    [_magnifierUpdatingTimer release];
    
    for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
        [_overlayLayers[i] release];
    }
    
    _view.delegate = nil;
    [_view release];
    [_overlayView release];
    [_annotationView release];
    
    self.docManager = nil;
    [_docManager release];
    
    [_mainOverlayLayer release];
    [_magnifierView release];
    [_selectTool release];
    [_lastFrame release];
    [_shadingCorrectionTool release];
    
    [_overlayAppearanceViewController release];
    [_overlayKey release];
    
    [_placeholdImage release];
    
    [super dealloc];
}

- (void)setDocManager:(CZDocManager *)docManager {
    if (docManager == _docManager) {
        return;
    }
    
    if (_docManager) {
        if (_docManager.elementLayer.delegate == self) {
            _docManager.elementLayer.delegate = nil;
            [_docManager removeObserver:self forKeyPaths:@"elementLayerModified", nil];
        }
        [_docManager release];
        _docManager = nil;
    }
    if (docManager) {
        _docManager = [docManager retain];
        
        [_docManager addObserver:self forKeyPaths:@"elementLayerModified", nil];
        _docManager.elementLayer.delegate = self;
        
        // docManager did change, update all the elements on element layer
        // and hide elements temporarily until next valid frame comes
        _lastFrameSize = CGSizeZero;
        [self redrawElementLayer];
        self.annotationView.hidden = YES;
    }
}

- (void)setMagnifierEnabled:(BOOL)magnifierEnabled {
    _magnifierEnabled = magnifierEnabled;
    if (magnifierEnabled) {
        [self.magnifierUpdatingTimer invalidate];
        self.magnifierUpdatingTimer = [NSTimer scheduledTimerWithTimeInterval:0.0625
                                                                       target:self
                                                                     selector:@selector(updateMagnifier:)
                                                                     userInfo:NULL
                                                                      repeats:YES];
        _magnifierNeedsUpdating = NO;
        _magnifierEnabled = YES;
    } else {
        [self.magnifierUpdatingTimer invalidate];
        self.magnifierUpdatingTimer = nil;
    }
}

- (void)setAnnotationHidden:(BOOL)annotationHidden {
    _annotationHidden = annotationHidden;

    if (_annotationHidden) {
        self.annotationView.hidden = _annotationHidden;
    }
    self.needUpdateScaleBar = YES;
}

- (void)setPreferredLiveResolution:(CGSize)preferredLiveResolution {
    if (preferredLiveResolution.width < kInvalidWidth ||
        preferredLiveResolution.height < kInvalidHeight) {
        _preferredLiveResolution = CGSizeMake(kInvalidWidth, kInvalidHeight);
    } else {
        _preferredLiveResolution = preferredLiveResolution;
    }
}

- (void)setFrame:(UIImage *)frame completion:(void (^)(void))completion {
    if (frame) {
        self.lastFrame = frame;
        CGSize newFrameSize = frame.size;
        
        GPUImagePicture *picture = nil;
        @autoreleasepool {
            picture = [[GPUImagePicture alloc] initWithImage:frame];
        }

        if (picture) {
            @synchronized ([_view animationLock]) {
                [self dispatchOutput:picture size:newFrameSize completion:completion];
            }
        }
        [picture release];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            CGFloat screenScale = [[UIScreen mainScreen] scale];
            UIImageView *viewToMagnify = _magnifierView.viewToMagnify;
            viewToMagnify.frame = CGRectMake(0,
                                             0,
                                             frame.scale * newFrameSize.width / screenScale,
                                             frame.scale * newFrameSize.height / screenScale);
            viewToMagnify.image = frame;
            
            if (!_magnifierViewHidden) {
                [_magnifierView setNeedsDisplay];
            }
            
            [self updateLayersWithFrameSize:newFrameSize];
            
            if (frame != _blankImage && !self.annotationHidden) {
                self.annotationView.hidden = NO;
            }
        });
    }
}

- (void)setFieldOfView:(float)fieldOfView {
    _fieldOfView = fieldOfView;
    _needUpdateScaleBar = YES;
}

- (void)clear {
    // GPUImage doesn't have a way to clear the GPUImageView directly,
    // the workaround is to send a black image to it.
    CGSize blankFrameSize = _lastFrameSize;
    int ratio = GCD(blankFrameSize.width, blankFrameSize.height);
    blankFrameSize.width = ceil(blankFrameSize.width / ratio);
    blankFrameSize.height = ceil(blankFrameSize.height / ratio);
    if (CGSizeEqualToSize(blankFrameSize, CGSizeZero)) {
        // Fix the bug about 0x0 CGContext.
        blankFrameSize = CGSizeMake(1, 1);
    }
    
    CZDisplayCurvePreset originalCurve = self.currentDisplayCurve;
    CZOverlayMode originalOverlay = self.currentOverlay;
    
    self.currentDisplayCurve = CZDisplayCurveLinear;
    self.currentOverlay = kCZOverlayNone;
    
    if (self.placeholdImage) {
        [self setFrame:self.placeholdImage completion:nil];
    } else {
        _blankImage = [CZCommonUtils blankImageWithSize:blankFrameSize];
        [self setFrame:_blankImage completion:nil];
    }
    
    self.currentDisplayCurve = originalCurve;
    self.currentOverlay = originalOverlay;
}

- (void)showDisplayCurveSelection {
    _currentSelectionMode = kCZVideoToolDisplayCurveSelection;
    [_view changeMainImageViewIndexTo:_currentDisplayCurve];
    
    @synchronized ([_view animationLock]) {
        for (NSUInteger i = 0; i < CZDisplayCurvePresetCount; ++i) {
            UILabel *label = [_view imageLabels][i];
            [label setText:[CZDisplayCurvePresetTool nameOfDisplayCurvePreset:i]];
        }
    }
    
    [_view showSupplementalViewsWithMode:kImageOutputViewGridMode];
    
    self.annotationHidden = YES;
    [self dismissMagnifierView];
}

- (void)showOverlaySelection {
    _currentSelectionMode = kCZVideoToolOverlaySelection;
    [_view changeMainImageViewIndexTo:_currentOverlay];
    
    @synchronized ([_view animationLock]) {
        for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
            UILabel *label = [_view imageLabels][i];
            [label setText:[CZVideoTool nameOfOverlayMode:i]];
        }
    }
    
    [_view showSupplementalViewsWithMode:kImageOutputViewGridModeWithButtons];
    
    self.annotationHidden = YES;
    [self dismissMagnifierView];
}

- (BOOL)isShowingSelection {
    return _currentSelectionMode != kCZVideoToolNoneSelection;
}

- (void)dismissInteractiveSelectionAnimated:(BOOL)animated {
    if (self.overlayAppearanceViewController != nil) {
        [self.overlayAppearanceViewController dismissViewControllerAnimated:animated completion:nil];
    }
    
    for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
        [_overlayLayers[i] removeFromSuperlayer];
        [_overlayLayers[i] release];
        _overlayLayers[i] = nil;
    }
    
    BOOL wasAnimationEnabled = [_view animationEnabled];
    [_view setAnimationEnabled:animated];
    [_view dismissSupplementalViews];
    [_view setAnimationEnabled:wasAnimationEnabled];
}

- (void)showMagnifierViewAtPoint:(CGPoint)point {
    if (_magnifierEnabled) {
        if (_magnifierViewHidden) {
            _magnifierViewHidden = NO;
            _magnifierView.hidden = NO;
            [UIView animateWithDuration:0.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionBeginFromCurrentState
                             animations:^ {
                                 _magnifierView.alpha = 1.0;
                             }
                             completion:NULL];
        }

        //May need to reset the frame if the frame set when init is not correct
        [_magnifierView resetFrame];
        
        [self updateMagnifierTouchPoint:point];
        
        [_magnifierView setNeedsDisplay];
        [_view bringSubviewToFront:_magnifierView];
        
        if ([_delegate respondsToSelector:@selector(videoToolDidShowMagnifierView:)]) {
            [_delegate videoToolDidShowMagnifierView:self];
        }
    }
    
    _magnifierHideTime = time(NULL) + kMagnifierTimeout;
}

- (CGRect)magnifierViewFrame {
    if (_magnifierView && !_magnifierViewHidden) {
        return _magnifierView.frame;
    } else {
        return CGRectNull;
    }
}

- (void)beginMeasuring {
    [self beginMeasuringWithMeasurementHidden:NO];
}

- (void)beginMeasuringWithMeasurementHidden:(BOOL)measurementHidden {
    if (_selectTool == nil) {
        _selectTool = [[CZSelectTool alloc] initWithDocManager:self.docManager view:self.annotationView];
        [_selectTool setMagnifyDelegate:self];
        [_selectTool setZoomOfView:_annotationViewZoomScale];
    }
    
    CZElementLine *line = (CZElementLine *)[self findElementOfClass:[CZElementLine class]];
    if (line == nil) {
        CZElementLayer *elementLayer = self.docManager.elementLayer;
        CGSize frameSize = elementLayer.frame.size;
        line = [[CZElementLine alloc] initWithPoint:CGPointMake(frameSize.width / 3.0f, frameSize.height / 2.0f)
                                       anotherPoint:CGPointMake(2.0f * frameSize.width / 3.0f, frameSize.height / 2.0f)];
        line.strokeColor = [[CZDefaultSettings sharedInstance] color];
        line.size = [[CZDefaultSettings sharedInstance] elementSize];
        line.measurementHidden = measurementHidden;
        line.measurementIDHidden = YES;
        [elementLayer addElement:line];
        [line release];
    } else if (line.isMeasurementHidden != measurementHidden) {
        line.measurementHidden = measurementHidden;
        [line notifyDidChange];
    }
}

- (float)measuredValue {
    CZElementLine *line = (CZElementLine *)[self findElementOfClass:[CZElementLine class]];
    if (line) {
        CGFloat dx = line.p1.x - line.p2.x;
        CGFloat dy = line.p1.y - line.p2.y;
        return sqrt(dx * dx + dy * dy);
    }
    return 0.0f;
}

- (void)endMeasuring {
    if (_selectTool) {
        CZElementLine *line = (CZElementLine *)[self findElementOfClass:[CZElementLine class]];
        [line removeFromParent];
        
        [_selectTool touchCancelled:CGPointZero];
        [_selectTool release];
        _selectTool = nil;
    }
}

- (void)refreshLayers {
    [self updateLayersWithFrameSize:_lastFrameSize];
}

- (void)showHiddenElementsInMagnifierView {
    if (self.magnifierView.viewToMagnify) {
        CZElementLayer *layer = _docManager.elementLayer;
        CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer hideElementOnly:nil inElementLayer:layer];
        [renderer release];
    }
}

- (void)redrawElementLayer {
    CZElement *temporarilyHiddenElement = nil;
    if ([self.combineDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.combineDelegate temporarilyHiddenElement];
    }
    
    CZElementLayer *layer = _docManager.elementLayer;
    
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer updateAllElements:layer];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:_docManager.elementLayer];
    [renderer release];
    
    if (self.magnifierView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer updateAllElements:layer];
        [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
        [renderer release];
    }
}

- (void)updateUnitByMicroscopeModelName:(NSString *)modelName {
    self.docManager.elementLayer.prefersBigUnit = [CZMicroscopeModel shouldPreferBigUnitByModelName:modelName];
}

- (UIImage *)screenshot {
    if (self.lastFrame == nil) {
        return nil;
    }
    
    // Render the last frame with display filters enabled.
    
    UIImage *image = [self.lastFrame retain];
    
    GPUImagePicture *picture = nil;
    @autoreleasepool {
        picture = [[GPUImagePicture alloc] initWithImage:image];
    }
    GPUImageOutput *lastOutput = picture;
    
    GPUImageOutput<GPUImageInput> *curveFilter = [CZDisplayCurvePresetTool newFilterForDisplayCurvePreset:[_view mainImageViewIndex]
                                                                                            withImageSize:image.size
                                                                                                 forVideo:YES];
    [curveFilter forceProcessingAtSize:image.size];
    if (curveFilter) {
        [lastOutput addTarget:curveFilter];
        [curveFilter release];
        lastOutput = curveFilter;
    }
    
    CZOverExposeFilter *overExposeFilter = nil;
    if (self.overExposeEnabled) {
        overExposeFilter = [[CZOverExposeFilter alloc] init];
        [overExposeFilter forceProcessingAtSize:_lastFrameSize];
    }
    if (overExposeFilter) {
        [lastOutput addTarget:overExposeFilter];
        lastOutput = overExposeFilter;
        [overExposeFilter release];
    }
    
    UIImage *content = nil;
    if (lastOutput == picture) {
        content = image;
    } else {
        [lastOutput useNextFrameForImageCapture];
        [picture processImage];
        content = [lastOutput imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
    }

    [picture release];
    
    // Screen scale needs to be considered here.
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    CGRect bounds = self.view.bounds;
    bounds.size.width *= screenScale;
    bounds.size.height *= screenScale;
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [self.view.backgroundColor CGColor]);
    CGContextFillRect(context, bounds);
    
    // Draw video content.
    CGRect actualBox = [CZCommonUtils calcActualBoxByBoundBox:bounds
                                                  contentSize:content.size];
    [content drawInRect:actualBox];
    
    // Draw annotations (distance tool, scale bar).
    CALayer *annotationsLayer = [self.docManager.elementLayer newLayer];
    CGFloat outputScaleX = actualBox.size.width / annotationsLayer.frame.size.width;
    CGFloat outputScaleY = actualBox.size.height / annotationsLayer.frame.size.height;
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, actualBox.origin.x, actualBox.origin.y);
    CGContextScaleCTM(context, outputScaleX, outputScaleY);
    [annotationsLayer renderInContext:context];
    CGContextRestoreGState(context);
    [annotationsLayer release];
    
    // Draw reticle overlay.
    UIImage *overlay = [CZCommonUtils imageFromLayer:self.mainOverlayLayer];
    [overlay drawInRect:bounds];
    
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [image release];
    
    return screenshot;
}

- (BOOL)hasNoScaleBar {
    if ([self.docManager.elementLayer isMemberOfClass:[CZElementLayer class]]) {
        return ![self.docManager.elementLayer hasScaleBarElement];
    }
    return NO;
}

#pragma mark - Private methods

- (void)updateLayersWithFrameSize:(CGSize)frameSize {
    if (_currentSelectionMode == kCZVideoToolNoneSelection) {
        BOOL frameChanged = !CGSizeEqualToSize(frameSize, _lastFrameSize) || !CGRectEqualToRect(_view.mainImageView.bounds, _lastBoundBox);
        
        if (frameChanged || _needUpdateScaleBar) {
            _lastFrameSize = frameSize;
            _lastBoundBox = _view.mainImageView.bounds;
            
            [self updateOverlay];
            
            if (_lastFrameSize.width >= self.preferredLiveResolution.width &&
                _lastFrameSize.height >= self.preferredLiveResolution.height) {
                // update frame
                CGRect actualBox = [CZCommonUtils calcActualBoxByBoundBox:_lastBoundBox
                                                              contentSize:_lastFrameSize];
                
                CGRect annotationViewFrame;
                annotationViewFrame.origin = actualBox.origin;
                
                CGFloat screenScale = [[UIScreen mainScreen] scale];
                annotationViewFrame.size.width = _lastFrameSize.width / screenScale;
                annotationViewFrame.size.height = _lastFrameSize.height / screenScale;
                
                self.annotationView.transform = CGAffineTransformIdentity;
                self.annotationView.frame = annotationViewFrame;
                self.docManager.elementLayer.frame = CGRectMake(0, 0, _lastFrameSize.width, _lastFrameSize.height);
                
                // calculate scaling for element layer
                float scaling;
                if (_fieldOfView < 0.0f) {
                    scaling = kInvalidScaling;
                } else {
                    scaling = _fieldOfView / _lastFrameSize.width;
                }
                if (isnan(scaling) || isinf(scaling)) {
                    scaling = kInvalidScaling;
                }
                
                // refresh zoom of scale
                CGFloat zoomScale = actualBox.size.width / annotationViewFrame.size.width;
                self.annotationView.layer.anchorPoint = CGPointZero;
                self.annotationView.transform = CGAffineTransformMakeScale(zoomScale, zoomScale);
                
                _annotationViewZoomScale = zoomScale;
                if ([self.combineDelegate respondsToSelector:@selector(imageView:didChangeToZoom:)]) {
                    [self.combineDelegate imageView:self.annotationView didChangeToZoom:zoomScale];
                }
                
                [self.selectTool setZoomOfView:zoomScale];
                _magnifierView.viewToMagnifyScale = zoomScale;
                
                if (scaling != self.docManager.calibratedScaling) {
                    self.docManager.calibratedScaling = scaling;
                }
                self.annotationHidden = NO;
                [self updateScaleBar];
                
                _needUpdateScaleBar = NO;
            } else {
                self.annotationHidden = YES;
            }
        }
    }
}

- (void)updateMagnifierTouchPoint:(CGPoint)point {
    if (self.magnifierViewHidden) {
        return;
    }
    
    CGPoint pointOfRootView = [self.view convertPoint:point
                                               toView:_magnifierView];
    BOOL validated = [_magnifierView validateTouchPoint:pointOfRootView];
    if (validated) {
        if ([_delegate respondsToSelector:@selector(videoToolDidShowMagnifierView:)]) {
            [_delegate videoToolDidShowMagnifierView:self];
        }
    }
    
    CGRect actualBox = [CZCommonUtils calcActualBoxByBoundBox:_lastBoundBox
                                                  contentSize:_lastFrameSize];
    CGFloat ratio = actualBox.size.width / _lastFrameSize.width * [[UIScreen mainScreen] scale];
    
    CGPoint touchPoint = CGPointZero;
    touchPoint.x = (point.x - actualBox.origin.x) / ratio;
    touchPoint.y = (point.y - actualBox.origin.y) / ratio;
    
    _magnifierView.touchPoint = touchPoint;
    _magnifierNeedsUpdating = YES;
    
    _magnifierHideTime = time(NULL) + kMagnifierTimeout;
}

- (void)updateMagnifier:(NSTimer *)timer {
    if (!_magnifierViewHidden && _magnifierNeedsUpdating) {
        [_magnifierView setNeedsDisplay];
        _magnifierNeedsUpdating = NO;
    }
    
    time_t now;
    time(&now);

    if (_magnifierHideTime > 0 && now >= _magnifierHideTime) {
        [self dismissMagnifierView];
    }
}

- (void)dispatchOutput:(GPUImagePicture *)output size:(CGSize)size completion:(void (^)(void))completion {
    for (NSUInteger i = 0; i < CZDisplayCurvePresetCount; ++i) {
        if ([_view isSupplementalViewsShown] || i == [_view mainImageViewIndex]) {
            GPUImageView *imageView = [_view imageViews][i];
            if ([imageView isKindOfClass:[GPUImageView class]]) {
                GPUImageOutput *lastOutput = output;
                
                GPUImageOutput<GPUImageInput> *shadingCorrectionFilter = nil;
                if (self.shadingCorrectionEnabled) {
                    shadingCorrectionFilter = [self.shadingCorrectionTool compoundFilter];
                }
                if (shadingCorrectionFilter) {
                    [shadingCorrectionFilter forceProcessingAtSize:size];
                    [lastOutput addTarget:shadingCorrectionFilter];
                    lastOutput = shadingCorrectionFilter;
                }
            
                CZOverExposeFilter *overExposeFilter = nil;
                if (self.overExposeEnabled) {
                    overExposeFilter = [[CZOverExposeFilter alloc] init];
                    [overExposeFilter forceProcessingAtSize:size];
                }
                if (overExposeFilter) {
                    [lastOutput addTarget:overExposeFilter];
                    lastOutput = overExposeFilter;
                }
                
                GPUImageOutput<GPUImageInput> *curveFilter = nil;
                if (_currentSelectionMode == kCZVideoToolDisplayCurveSelection) {
                    curveFilter = [CZDisplayCurvePresetTool newFilterForDisplayCurvePreset:i
                                                                             withImageSize:size
                                                                                  forVideo:YES];
                } else {
                    curveFilter = [CZDisplayCurvePresetTool newFilterForDisplayCurvePreset:_currentDisplayCurve
                                                                             withImageSize:size
                                                                                  forVideo:YES];
                }
                if (curveFilter) {
                    [curveFilter forceProcessingAtSize:size];
                    [lastOutput addTarget:curveFilter];
                    lastOutput = curveFilter;
                }
                
                [lastOutput addTarget:imageView];
                
                [overExposeFilter release];
                [curveFilter release];
            }
        }
    }

    //Add this logic to main thread to avoid runtime waring. Because [UIApplication applicationState] must be used from main thread only.
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            return;
        }
        
        OSAtomicIncrement32(&_processImageCount);
        [output processImageWithCompletionHandler:^{
            OSAtomicDecrement32(&_processImageCount);
            dispatch_async(dispatch_get_main_queue(), ^{
                if (completion) {
                    completion();
                }
            });
        }];
    });
}

- (void)updateOverlay {
    [self.mainOverlayLayer removeFromSuperlayer];
    self.mainOverlayLayer = nil;
    
    CALayer *overlayLayer = [self newLayerInBoundBox:_lastBoundBox
                                             forOverlayMode:_currentOverlay
                                                fieldOfView:_fieldOfView];
    [[self.overlayView layer] addSublayer:overlayLayer];
    if (_magnifierView) {
        [_view bringSubviewToFront:_magnifierView];
        [_view insertSubview:_overlayView belowSubview:_magnifierView];
    } else {
        [_view bringSubviewToFront:_overlayView];
    }
    self.mainOverlayLayer = overlayLayer;
    [overlayLayer release];
}

- (void)updateScaleBar {
    // Fix bug 245465
    // When the user set the global settings of scale bar to last position, and remove scale bar in live manually, the global settings will not change until the user exit the editing mode, so the scale bar will still be displayed in the next of call of the method.
    // So if the user is in the editing mode, we will make sure the scale bar's visibility will not be changed.
    if (self.shouldNotChangeScaleBarVisibility) {
        return;
    }
    
#if DCM_REFACTORED
    CZScaleBarDisplayMode defaultPosition = [[CZDCMManager defaultManager] scaleBarDisplayMode];
#endif

    CZScaleBarDisplayMode defaultDisplayMode = [CZDefaultSettings sharedInstance].scaleBarDisplayMode;
    
    CZElementScaleBar *scaleBar = (CZElementScaleBar *)[self findElementOfClass:[CZElementScaleBar class]];
    if (defaultDisplayMode == kCZScaleBarPositionHidden || self.annotationHidden || self.scaleBarHidden) {  // remove scale bar if need
        [scaleBar removeFromParent];
        scaleBar = nil;
    } else {  // add scale bar if need
        if (scaleBar == nil) {
            scaleBar = [[CZElementScaleBar alloc] init];
            [scaleBar setPosition:kCZScaleBarPositionBottomRight];
            [self.docManager.elementLayer addElement:scaleBar];
            [scaleBar release];
        }
        scaleBar.size = [CZDefaultSettings sharedInstance].elementSize;
        scaleBar.fillColor = [CZDefaultSettings sharedInstance].fillColor;
        scaleBar.strokeColor = [CZDefaultSettings sharedInstance].strokeColor;
    }
    
    [scaleBar notifyWillChange];
    CGFloat percentX = [CZDefaultSettings sharedInstance].percentX;
    CGFloat percentY = [CZDefaultSettings sharedInstance].percentY;
    scaleBar.point = [scaleBar scaleBarPointWithScaleBarCoordinate:CZElementScaleBarCoordinateMake(percentX, percentY)
                                                         imageSize:[scaleBar.parent frame].size];
    [scaleBar snapToDefaultPosition:scaleBar.tolerance];
    [scaleBar notifyDidChange];
    
    CZElementLayer *elementLayer = self.docManager.elementLayer;
    NSUInteger count = elementLayer.elementCount;
    
    for (NSUInteger i = 0; i < count; ++i) {
        CZElement *element = [elementLayer elementAtIndex:i];
        [element notifyDidChange];
    }
}

- (CZElement *)findElementOfClass:(Class)class {
    return [self.docManager.elementLayer firstElementOfClass:class];
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)pinchGesture {
    [[CZToastManager sharedManager] showToastMessage:L(@"LIVE_NO_PINCH_WARNING") sourceRect:CGRectOffset([UIScreen mainScreen].bounds, 0, [UIScreen mainScreen].bounds.size.height / 2.0)];
}

- (CALayer *)newLayerInBoundBox:(CGRect)boundBox
                 forOverlayMode:(CZOverlayMode)mode
                    fieldOfView:(float)fieldOfView {
    CGSize contentSize = _lastFrameSize;
    if (contentSize.width < self.preferredLiveResolution.width ||
        contentSize.height < self.preferredLiveResolution.height) {
        return nil;
    }
    
    CAShapeLayer *newLayer = [[CAShapeLayer alloc] init];
    newLayer.frame = boundBox;
    newLayer.bounds = CGRectMake(0, 0, boundBox.size.width, boundBox.size.height);
    newLayer.fillColor = nil;
    
    CZColor strokeColor = [[CZDefaultSettings sharedInstance] colorForOverlay:mode];
    strokeColor.a = 128; // Semi-transparent
    newLayer.strokeColor = CGColorFromCZColor(strokeColor);
    strokeColor.a = 255;
    
    CZElementSize strokeWidth = [[CZDefaultSettings sharedInstance] sizeForOverlay:mode];
    switch (strokeWidth) {
        case CZElementSizeExtraLarge:
            newLayer.lineWidth = 7.0;
            break;
            
        case CZElementSizeLarge:
            newLayer.lineWidth = 5.0;
            break;
            
        case CZElementSizeSmall:
            newLayer.lineWidth = 2.0;
            break;
            
        case CZElementSizeExtraSmall:
            newLayer.lineWidth = 1.0;
            break;
            
        case CZElementSizeMedium:
        default:
            newLayer.lineWidth = 3.0;
            break;
    }
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint middlePoint = CGPointMake(boundBox.size.width / 2.0f,
                                      boundBox.size.height / 2.0f);
    
    CGFloat boxArea = boundBox.size.width * boundBox.size.height;
    CGRect screenBox = [[UIScreen mainScreen] bounds];
    CGFloat screenArea = screenBox.size.width * screenBox.size.height;
    BOOL isThumbnailMode = (boxArea <= screenArea / 4.0f);
    
    CGRect actualBox = [CZCommonUtils calcActualBoxByBoundBox:boundBox contentSize:contentSize];
    
    BOOL hasValidScaling = YES;
    float v_1_scaling = actualBox.size.width / fieldOfView;  // 1.0 / scaling
    if (fieldOfView <= 0.0f || isnan(fieldOfView) || isinf(fieldOfView) || v_1_scaling <= 0.0f ) {
        v_1_scaling = 1.0;
        hasValidScaling = NO;
    }
    
    switch (mode) {
        case kCZOverlayGrid:
        {
            int gridWidthInMicrometer = 100;
            CGFloat gap = gridWidthInMicrometer * v_1_scaling;  // 100 micrometer in pixels
            if (gap < 32.f) {
                gridWidthInMicrometer = 500;
                gap = gridWidthInMicrometer * v_1_scaling;
            }
            
            for (NSUInteger x = 1; gap > 0.0f; ++x) {
                CGFloat divisionXLeft = middlePoint.x - (x - 0.5) * gap;
                CGFloat divisionXRight = middlePoint.x + (x - 0.5) * gap;
                if (divisionXRight >= CGRectGetMaxX(actualBox)) {
                    --x;
                    break;
                }
                
                CGPathMoveToPoint(path, NULL, divisionXLeft, actualBox.origin.y);
                CGPathAddLineToPoint(path, NULL, divisionXLeft, CGRectGetMaxY(actualBox));
                CGPathMoveToPoint(path, NULL, divisionXRight, actualBox.origin.y);
                CGPathAddLineToPoint(path, NULL, divisionXRight, CGRectGetMaxY(actualBox));
            }
            
            for (NSUInteger y = 1; gap > 0.0f; ++y) {
                CGFloat divisionYUp = middlePoint.y - (y - 0.5) * gap;
                CGFloat divisionYDown = middlePoint.y + (y - 0.5) * gap;
                if (divisionYDown >= CGRectGetMaxY(actualBox)) {
                    --y;
                    break;
                }
                
                CGPathMoveToPoint(path, NULL, actualBox.origin.x, divisionYUp);
                CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(actualBox), divisionYUp);
                CGPathMoveToPoint(path, NULL, actualBox.origin.x, divisionYDown);
                CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(actualBox), divisionYDown);
            }
            
            // Only create text labels for outermost divisions when scaling is
            // valid.
            if (hasValidScaling && !isThumbnailMode) {
                NSString *captionText = [NSString stringWithFormat:L(@"OVERLAY_GRID_DIVISION"),
                                         gridWidthInMicrometer,
                                         L(@"UNIT_UM")];
                
                UILabel *tempLabel = [[UILabel alloc] init];
                [tempLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
                
                // Get actual caption size with label control
                [tempLabel setText:captionText];
                CGRect captionRect = [tempLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, CGFLOAT_MAX)
                                           limitedToNumberOfLines:1];
                CGRect captionFrame = CGRectMake(middlePoint.x - captionRect.size.width / 2,
                                                 CGRectGetMaxY(actualBox) - captionRect.size.height - 5,
                                                 captionRect.size.width,
                                                 captionRect.size.height);
                
                [tempLabel release];
                
                // Add text layer as sub-layer of the overlay layer.
                CATextLayer *textLayer = [[CATextLayer alloc] init];
                textLayer.font = @"HelveticaNeue";
                textLayer.fontSize = 16;
                textLayer.frame = captionFrame;
                textLayer.alignmentMode = kCAAlignmentCenter;
                textLayer.string = captionText;
                textLayer.foregroundColor = CGColorFromCZColor(strokeColor);
                [newLayer addSublayer:textLayer];
                [textLayer release];
            }
            
            break;
        }
            
        case kCZOverlayCrossWithDivisions:
        {
            // Draw horizontal and vertical center lines.
            CGPathMoveToPoint(path, NULL, actualBox.origin.x, middlePoint.y);
            CGPathAddLineToPoint(path, NULL, actualBox.origin.x + actualBox.size.width, middlePoint.y);
            CGPathMoveToPoint(path, NULL, middlePoint.x, actualBox.origin.y);
            CGPathAddLineToPoint(path, NULL, middlePoint.x, actualBox.origin.y + actualBox.size.height);
            
            // Set length of division marks.
            const CGFloat halfShortMarkSize = 8.0f;
            
            // Set gap between marks according to scaling.
            const CGFloat divisionWidthInMicrometer = 10.0f;
            CGFloat gap = divisionWidthInMicrometer * v_1_scaling;  // 10 micrometer in pixels
            BOOL showsShort = (gap >= 5.0f);
            
            // Draw the division marks and collect position data for outermost
            // (left and bottom) division marks.
            
            NSUInteger x = 0;
            NSUInteger y = 0;
            
            for (x = 1; ; ++x) {
                CGFloat divisionXLeft = middlePoint.x - x * gap;
                CGFloat divisionXRight = middlePoint.x + x * gap;
                if (divisionXRight >= CGRectGetMaxX(actualBox)) {
                    --x;
                    break;
                }
                
                CGFloat halfMarkSize = halfShortMarkSize;
                if (x % 100 == 0) {
                    halfMarkSize *= 3.0f;
                } else if (x % 10 == 0) {
                    halfMarkSize *= 2.0f;
                } else if (!showsShort) {
                    continue;
                }
                
                CGPathMoveToPoint(path, NULL, divisionXLeft, middlePoint.y - halfMarkSize);
                CGPathAddLineToPoint(path, NULL, divisionXLeft, middlePoint.y + halfMarkSize);
                CGPathMoveToPoint(path, NULL, divisionXRight, middlePoint.y - halfMarkSize);
                CGPathAddLineToPoint(path, NULL, divisionXRight, middlePoint.y + halfMarkSize);
            }
            
            for (y = 1; ; ++y) {
                CGFloat divisionYUp = middlePoint.y - y * gap;
                CGFloat divisionYDown = middlePoint.y + y * gap;
                if (divisionYDown >= CGRectGetMaxY(actualBox)) {
                    --y;
                    break;
                }
                
                CGFloat halfMarkSize = halfShortMarkSize;
                if (y % 100 == 0) {
                    halfMarkSize *= 3.0f;
                } else if (y % 10 == 0) {
                    halfMarkSize *= 2.0f;
                } else if (!showsShort) {
                    continue;
                }
                
                CGPathMoveToPoint(path, NULL, middlePoint.x - halfMarkSize, divisionYUp);
                CGPathAddLineToPoint(path, NULL, middlePoint.x + halfMarkSize, divisionYUp);
                CGPathMoveToPoint(path, NULL, middlePoint.x - halfMarkSize, divisionYDown);
                CGPathAddLineToPoint(path, NULL, middlePoint.x + halfMarkSize, divisionYDown);
            }

            // Only create text labels for outermost divisions when scaling is
            // valid.
            if (hasValidScaling && !isThumbnailMode) {
                // Calculate position and size for the text overlays on outermost
                // divisions.
                
                CGFloat leftCaptionOffset = halfShortMarkSize;
                if (x >= 100) {
                    leftCaptionOffset *= 3.0f;
                    x -= x % 100;
                } else if (x >= 10) {
                    leftCaptionOffset *= 2.0f;
                    x -= x % 10;
                }
                CGFloat leftCaptionX = middlePoint.x - x * gap;
                
                CGFloat bottomCaptionOffset = halfShortMarkSize;
                if (y >= 100) {
                    bottomCaptionOffset *= 3.0f;
                    y -= y % 100;
                } else if (y >= 10) {
                    bottomCaptionOffset *= 2.0f;
                    y -= y % 10;
                }
                CGFloat bottomCaptionY = middlePoint.y + y * gap;
                
                NSString *captionText[2];
                captionText[0] = [NSString stringWithFormat:@"%lu %@", (unsigned long)x * 10, L(@"UNIT_UM")];
                captionText[1] = [NSString stringWithFormat:@"%lu %@", (unsigned long)y * 10, L(@"UNIT_UM")];
                
                CGRect captionFrame[2];
                UILabel *tempLabel = [[UILabel alloc] init];
                [tempLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
                
                // Left caption size
                [tempLabel setText:captionText[0]];
                CGRect leftCaptionRect = [tempLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, CGFLOAT_MAX)
                                               limitedToNumberOfLines:1];
                captionFrame[0] = CGRectMake(leftCaptionX - leftCaptionRect.size.width / 2,
                                             middlePoint.y - leftCaptionOffset - leftCaptionRect.size.height - 5,
                                             leftCaptionRect.size.width,
                                             leftCaptionRect.size.height);
                
                // Bottom caption size
                [tempLabel setText:captionText[1]];
                CGRect bottomCaptionRect = [tempLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, CGFLOAT_MAX)
                                                 limitedToNumberOfLines:1];
                captionFrame[1] = CGRectMake(middlePoint.x - bottomCaptionOffset - bottomCaptionRect.size.width - 5,
                                             bottomCaptionY - bottomCaptionRect.size.height / 2,
                                             bottomCaptionRect.size.width,
                                             bottomCaptionRect.size.height);
                
                [tempLabel release];
                
                // If the rect goes out of the bound box, bring it back.
                
                if (captionFrame[0].origin.x < 0) {
                    captionFrame[0].origin.x = 0;
                }
                CGFloat exceededY = CGRectGetMaxY(captionFrame[1]) - CGRectGetMaxY(actualBox);
                if (exceededY > 0) {
                    captionFrame[1].origin.y -= exceededY;
                }
                
                // Add two text layers as sub-layers of the overlay layer.
                
                for (NSUInteger i = 0; i < 2; ++i) {
                    CATextLayer *textLayer = [[CATextLayer alloc] init];
                    textLayer.font = @"HelveticaNeue";
                    textLayer.fontSize = 16;
                    textLayer.frame = captionFrame[i];
                    textLayer.alignmentMode = kCAAlignmentCenter;
                    textLayer.string = captionText[i];
                    textLayer.foregroundColor = CGColorFromCZColor(strokeColor);
                    [newLayer addSublayer:textLayer];
                    [textLayer release];
                }
            }
            
            break;
        }
            
        case kCZOverlayCrosshair:
        {
            CGFloat diameter = actualBox.size.height * 0.7;
            CGRect rect = CGRectMake(actualBox.origin.x + (actualBox.size.width - diameter) / 2.0f,
                                     actualBox.origin.y + (actualBox.size.height - diameter) / 2.0f,
                                     diameter,
                                     diameter);
            CGPathAddEllipseInRect(path, NULL, rect);
            CGPathMoveToPoint(path, NULL, actualBox.origin.x, middlePoint.y);
            CGPathAddLineToPoint(path, NULL, actualBox.origin.x + actualBox.size.width, middlePoint.y);
            CGPathMoveToPoint(path, NULL, middlePoint.x, actualBox.origin.y);
            CGPathAddLineToPoint(path, NULL, middlePoint.x, actualBox.origin.y + actualBox.size.height);
            break;
        }
            
        default:
            break;
    }
    
    newLayer.path = path;
    CGPathRelease(path);
    
    return newLayer;
}

+ (NSString *)nameOfOverlayMode:(CZOverlayMode)mode {
    NSString *name = nil;
    
    switch (mode) {
        case kCZOverlayNone:
            name = L(@"NONE");
            break;
            
        case kCZOverlayGrid:
            name = L(@"OVERLAY_GRID");
            break;
            
        case kCZOverlayCrossWithDivisions:
            name = L(@"OVERLAY_CROSS_WITH_DIVISIONS");
            break;
            
        case kCZOverlayCrosshair:
            name = L(@"OVERLAY_CROSSHAIR");
            break;
            
        default:
            break;
    }
    
    return name;
}

- (void)updateOverlayLayerForMode:(CZOverlayMode)mode {
    if (_overlayLayers[mode]) {
        [_overlayLayers[mode] removeFromSuperlayer];
        [_overlayLayers[mode] release];
    }
    
    GPUImageView *imageView = [_view imageViews][mode];
    _overlayLayers[mode] = [self newLayerInBoundBox:[imageView bounds]
                                            forOverlayMode:mode
                                               fieldOfView:_fieldOfView];
    [[imageView layer] addSublayer:_overlayLayers[mode]];
}

- (void)appBecomesActive:(NSNotification *)notification {
    self.needUpdateScaleBar = YES;
    [self refreshLayers];
}

- (void)appWillResignActive:(NSNotification *)notification {
    [self dismissInteractiveSelectionAnimated:NO];
}

- (void)appDidEnterBackground:(NSNotification *)notification {
    for (NSUInteger i = 0; i < 40; i++) {  // wait no more than 4 seconds
        if (_processImageCount == 0) {
            break;
        }
        usleep(0.1 * USEC_PER_SEC);
    }
}

#pragma mark - CZImageOutputViewDelegate methods

- (void)imageOutputViewWillShowSupplementalViews:(CZImageOutputView *)view {
    [self.mainOverlayLayer removeFromSuperlayer];
    self.mainOverlayLayer = nil;
    self.annotationView.hidden = YES;
}

- (void)imageOutputViewDidShowSupplementalViews:(CZImageOutputView *)view {
    for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
        if (_overlayLayers[i]) {
            [_overlayLayers[i] release];
        }
        
        GPUImageView *imageView = [_view imageViews][i];
        CZOverlayMode mode = i;
        if (_currentSelectionMode == kCZVideoToolDisplayCurveSelection) {
            mode = _currentOverlay;
        }
        _overlayLayers[i] = [self newLayerInBoundBox:[imageView bounds]
                                             forOverlayMode:mode
                                                fieldOfView:_fieldOfView];
        [[imageView layer] addSublayer:_overlayLayers[i]];
    }
}

- (void)imageOutputView:(CZImageOutputView *)view didChangeMainViewIndexTo:(NSUInteger)index {
    for (NSUInteger i = 0; i < kCZOverlayModeCount; ++i) {
        [_overlayLayers[i] removeFromSuperlayer];
        [_overlayLayers[i] release];
        _overlayLayers[i] = nil;
    }
}

- (void)imageOutputViewWillDismissSupplementalViews:(CZImageOutputView *)view {
    if (_currentSelectionMode == kCZVideoToolDisplayCurveSelection) {
        _currentDisplayCurve = [_view mainImageViewIndex];
    } else if (_currentSelectionMode == kCZVideoToolOverlaySelection) {
        _currentOverlay = [_view mainImageViewIndex];
    }
    
    [self updateOverlay];
    
    self.annotationHidden = NO;
}

- (void)imageOutputViewDidDismissSupplementalViews:(CZImageOutputView *)view {
    if ([_delegate respondsToSelector:@selector(videoToolDidDismissInteractiveSelection:)]) {
        [_delegate videoToolDidDismissInteractiveSelection:self];
    }
    
    _currentSelectionMode = kCZVideoToolNoneSelection;
}

- (CZShouldBeginTouchState)imageOutputView:(CZImageOutputView *)view shouldBeginAnnotationTouch:(UITouch *)touch {
    if (self.combineDelegate) {
        if ([self.combineDelegate respondsToSelector:@selector(imageView:shouldBeginTouch:)]) {
            return [self.combineDelegate imageView:self.annotationView shouldBeginTouch:touch];
        }
    } else if (_selectTool) {
        CGPoint pointInView = [touch locationInView:self.annotationView];
        return [self.selectTool shouldBeginTouch:pointInView];
    } else if (self.magnifierEnabled) {
        return CZShouldBeginTouchStateMaybe;
    }
    
    return CZShouldBeginTouchStateNo;
}

- (BOOL)imageOutputView:(CZImageOutputView *)view gesture:(UIGestureRecognizer *)recognizer shouldReceiveTouch:(UITouch *)touch {
    if ([recognizer isKindOfClass:[CZAnnotationGestureRecognizer class]] ||
        [recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        if (self.combineDelegate || self.selectTool || self.magnifierEnabled) {
            return YES;
        }
    } else if ([recognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        UISwipeGestureRecognizer *swipeGesture = (UISwipeGestureRecognizer *)recognizer;
        if (!(self.combineDelegate || self.selectTool)) {
            if ([self.delegate respondsToSelector:@selector(videoTool:swipeGesture:shouldReceiveTouch:)]) {
                if ([self.delegate videoTool:self swipeGesture:swipeGesture shouldReceiveTouch:touch]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)imageOutputView:(CZImageOutputView *)view handlePanGesture:(UIGestureRecognizer *)recognizer {
    BOOL handled = NO;
    if ([self.combineDelegate respondsToSelector:@selector(imageView:handlePanGesture:)]) {
        handled = [self.combineDelegate imageView:self.annotationView handlePanGesture:recognizer];
    }
    
    if (!handled && self.selectTool) {
        CGPoint pointInView = [recognizer locationInView:self.annotationView];
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            handled = [self.selectTool touchBegin:pointInView];
        } else if (recognizer.state == UIGestureRecognizerStateChanged) {
            handled = [self.selectTool touchMoved:pointInView];
        } else if (recognizer.state == UIGestureRecognizerStateEnded) {
            handled = [self.selectTool touchEnd:pointInView];
        } else if (recognizer.state == UIGestureRecognizerStateEnded){
            handled = [self.selectTool touchCancelled:pointInView];
        }
    }
    
    // if delegate do not handle, then fall back to magnify handler
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if (!handled) {
            CGPoint point = [recognizer locationInView:self.view];
            if (self.magnifierViewHidden) {
                [self showHiddenElementsInMagnifierView];
                [self showMagnifierViewAtPoint:point];
            } else {
                CGPoint pointInMagView = [self.view convertPoint:point toView:self.magnifierView];
                if (CGRectContainsPoint(self.magnifierView.bounds, pointInMagView)) {
                    [self dismissMagnifierView];
                } else {
                    [self updateMagnifierTouchPoint:point];
                }
            }
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (!handled) {
            if (_magnifierEnabled) {
                CGPoint point = [recognizer locationInView:self.view];
                [self updateMagnifierTouchPoint:point];
            }
        }
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        if (handled) {
            [self dismissMagnifierView];
        }
    }
}

- (void)imageOutputView:(CZImageOutputView *)view handleDoubleTapGesture:(UIGestureRecognizer *)recognizer {
    if ([self.combineDelegate respondsToSelector:@selector(imageView:handleDoubleTapGesture:)]) {
        [self.combineDelegate imageView:self.annotationView handleDoubleTapGesture:recognizer];
    }
}

- (void)imageOutputView:(CZImageOutputView *)view handleSwipeGesture:(UISwipeGestureRecognizer *)gesture {
    if ([self.delegate respondsToSelector:@selector(videoTool:handleSwipeGesture:)]) {
        [self.delegate videoTool:self handleSwipeGesture:gesture];
    }
}

- (void)imageOutputView:(CZImageOutputView *)view handleTapGesture:(UIGestureRecognizer *)recognizer {
    BOOL handled = NO;
    if ([self.combineDelegate respondsToSelector:@selector(imageView:handleTapGesture:)]) {
        handled = [self.combineDelegate imageView:self.annotationView handleTapGesture:recognizer];
    }
    
    if (!handled) {
        CGPoint point = [recognizer locationInView:self.view];
        if (self.magnifierViewHidden) {
            if (self.combineDelegate == nil) {
                [self showHiddenElementsInMagnifierView];
                [self showMagnifierViewAtPoint:point];
            }
        } else {
            CGPoint pointInMagView = [self.view convertPoint:point toView:self.magnifierView];
            if (CGRectContainsPoint(self.magnifierView.bounds, pointInMagView)) {
                [self dismissMagnifierView];
            } else {
                [self updateMagnifierTouchPoint:point];
            }
        }
    }
}

- (void)imageOutputView:(CZImageOutputView *)view didClickOverlayGearButton:(UIButton *)button {
    if (self.overlayAppearanceViewController == nil) {
        CZOverlayMode mode = button.tag;
        
        CZColor overlayColor = [[CZDefaultSettings sharedInstance] colorForOverlay:mode];
        CZElementSize overlaySize = [[CZDefaultSettings sharedInstance] sizeForOverlay:mode];
        self.overlayKey = @(mode);
        
        CZAppearanceSettingViewController *contentController = [[CZAppearanceSettingViewController alloc] init];
        contentController.delegate = self;
        [contentController setSelectedColor:overlayColor];
        [contentController setSelectedSize:overlaySize];
        [contentController setSelectedBackgroundColor:kCZColorTransparent];
        [contentController updateAppearanceSettingViewColorButton:YES backgroundButton:NO];
        contentController.modalPresentationStyle = UIModalPresentationPopover;
        
        UIPopoverPresentationController *popoverPresentationController = contentController.popoverPresentationController;
        popoverPresentationController.delegate = self;
        popoverPresentationController.popoverBackgroundViewClass = [UIPopoverBackgroundView class];
        popoverPresentationController.sourceView = self.view;
        popoverPresentationController.sourceRect = button.frame;
        popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        
        [UIApplication.sharedApplication.delegate.window.rootViewController presentViewController:contentController animated:YES completion:nil];
        
        self.overlayAppearanceViewController = contentController;
        [contentController release];
    }
}

#pragma mark - UIPopoverPresentationControllerDelegate methods

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    if (popoverPresentationController.presentedViewController == self.overlayAppearanceViewController) {
        self.overlayAppearanceViewController = nil;
        self.overlayKey = nil;
    }
}

#pragma mark - CZAppearanceSettingViewDelegate methods

- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceColor:(CZColor)color {
    [[CZDefaultSettings sharedInstance] setColor:color forOverlay:[self.overlayKey unsignedIntegerValue]];
    
    CZOverlayMode mode = [self.overlayKey unsignedIntegerValue];
    [self updateOverlayLayerForMode:mode];
}

- (void)appearanceSettingViewController:(CZAppearanceSettingViewController *)controller didChangeAppearanceSize:(CZElementSize)size {
    [[CZDefaultSettings sharedInstance] setSize:size forOverlay:[self.overlayKey unsignedIntegerValue]];
    
    CZOverlayMode mode = [self.overlayKey unsignedIntegerValue];
    [self updateOverlayLayerForMode:mode];
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer didAddElement:(CZElement *)element {
    CZElement *temporarilyHiddenElement = nil;
    if ([self.combineDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.combineDelegate temporarilyHiddenElement];
    } else if (_selectTool) {
        temporarilyHiddenElement = _selectTool.temporarilyHiddenElement;
    }
    
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer layer:layer didAddElement:element];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if (self.magnifierView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer layer:layer didAddElement:element];
        [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
        [renderer release];
    }
    
    if ([self.combineDelegate respondsToSelector:@selector(layer:didAddElement:)]) {
        [self.combineDelegate layer:layer didAddElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer willRemoveElement:(CZElement *)element {
    if ([self.combineDelegate respondsToSelector:@selector(layer:willRemoveElement:)]) {
        [self.combineDelegate layer:layer willRemoveElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer layer:layer didRemoveElement:element];
    [renderer release];
    
    if (self.magnifierView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer layer:layer didRemoveElement:element];
        [renderer release];
    }
    
    if ([_selectTool respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [_selectTool layer:layer didRemoveElement:element];
    }
    
    if ([self.combineDelegate respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [self.combineDelegate layer:layer didRemoveElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer willChangeElement:(CZElement *)element {
    if ([self.combineDelegate respondsToSelector:@selector(layer:willChangeElement:)]) {
        [self.combineDelegate layer:layer willChangeElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {
    CZElement *temporarilyHiddenElement = nil;
    if ([self.combineDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
        temporarilyHiddenElement = [self.combineDelegate temporarilyHiddenElement];
    } else if (_selectTool) {
        temporarilyHiddenElement = _selectTool.temporarilyHiddenElement;
    }
    
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer layer:layer didChangeElement:element];
    [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if (self.magnifierView.viewToMagnify) {
        renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer layer:layer didChangeElement:element];
        
        CZElement *selectedElement = nil;
        if ([self.combineDelegate respondsToSelector:@selector(selectedElement)]) {
            selectedElement = [self.combineDelegate selectedElement];
        } else if (_selectTool) {
            selectedElement = _selectTool.selectedElement;
        }
        
        BOOL hidden = (selectedElement == element) || (temporarilyHiddenElement == element);
        if (hidden) {
            [renderer hideElementOnly:element inElementLayer:layer];
        }
        
        [renderer release];
    }
    
    if ([_selectTool respondsToSelector:@selector(layer:didChangeElement:)]) {
        [_selectTool layer:layer didChangeElement:element];
    }
    
//    if ([element isKindOfClass:[CZElementScaleBar class]]) {
//        CZElementScaleBar *scaleBar = (CZElementScaleBar *)element;
//        if ([CZElementScaleBar isValidPosition:scaleBar.position] &&
//            scaleBar.position != [[CZDefaultSettings sharedInstance] scaleBarDisplayMode]) {
//            [[CZDefaultSettings sharedInstance] setScaleBarDisplayMode:scaleBar.position];
//        }
//    }
    
    if ([self.combineDelegate respondsToSelector:@selector(layer:didChangeElement:)]) {
        [self.combineDelegate layer:layer didChangeElement:element];
    }
}

- (void)layer:(CZElementLayer *)layer updateElementMeasurementsLargerThanIndex:(NSUInteger)index {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer layer:layer updateElementMeasurementsLargerThanIndex:index];
    [renderer release];
    
    if ([self.combineDelegate respondsToSelector:@selector(layer:updateElementMeasurementsLargerThanIndex:)]) {
        [self.combineDelegate layer:layer updateElementMeasurementsLargerThanIndex:index];
    }
}

- (void)layer:(CZElementLayer *)layer willRemoveGroup:(CZElementGroup *)group {
    if ([self.combineDelegate respondsToSelector:@selector(layer:willRemoveElement:)]) {
        [self.combineDelegate layer:layer willRemoveGroup:group];
    }
}

- (void)layer:(CZElementLayer *)layer didRemoveGroup:(CZElementGroup *)group {
    if ([self.combineDelegate respondsToSelector:@selector(layer:didRemoveGroup:)]) {
        [self.combineDelegate layer:layer didRemoveGroup:group];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeGroup:(CZElementGroup *)group {
    if ([self.combineDelegate respondsToSelector:@selector(layer:didChangeGroup:)]) {
        [self.combineDelegate layer:layer didChangeGroup:group];
    }
}

- (void)layerUpdateAllMeasurements:(CZElementLayer *)layer {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    [renderer updateAllElements:layer];
    [renderer release];
    
    if ([self.combineDelegate respondsToSelector:@selector(layerUpdateAllMeasurements:)]) {
        [self.combineDelegate layerUpdateAllMeasurements:layer];
    }
}

- (void)layerDidChangeScaling:(CZElementLayer *)layer {
    if (_magnifierView) {
        [_view bringSubviewToFront:_magnifierView];
        [_view insertSubview:_overlayView belowSubview:_magnifierView];
    } else {
        [_view bringSubviewToFront:_overlayView];
    }
    
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.annotationView];
    
    if (_lastFrameSize.width >= self.preferredLiveResolution.width &&
        _lastFrameSize.height >= self.preferredLiveResolution.height) {
        // update scale bar length
        if ([layer elementCount]) {
            CZElementScaleBar *scaleBar = (CZElementScaleBar *)[self findElementOfClass:[CZElementScaleBar class]];
            [scaleBar updateLength];
        }
        
        [renderer updateAllElements:layer];
        CZElement *temporarilyHiddenElement = nil;
        if ([self.combineDelegate respondsToSelector:@selector(temporarilyHiddenElement)]) {
            temporarilyHiddenElement = [self.combineDelegate temporarilyHiddenElement];
        } else if (_selectTool) {
            temporarilyHiddenElement = _selectTool.temporarilyHiddenElement;
        }
        
        [renderer hideElementOnly:temporarilyHiddenElement inElementLayer:layer];
    }
    
    [renderer release];
    
    if ([self.combineDelegate respondsToSelector:@selector(layerDidChangeScaling:)]) {
        [self.combineDelegate layerDidChangeScaling:layer];
    }
}

#pragma mark - CZUndoManaging methods

- (void)pushAction:(id<CZUndoAction>)anAction {
    [anAction do];
}

- (void)undo {
}

- (void)redo {
}

- (BOOL)canUndo {
    return NO;
}

- (BOOL)canRedo {
    return NO;
}

#pragma mark - CZMagnifyDelegate methods

- (void)magnifyAtPoint:(CGPoint)point {
    if (!_magnifierEnabled) {
        return;
    }
    
    point = [self.annotationView convertPoint:point toView:self.view];
    if (self.magnifierViewHidden) {
        [self showMagnifierViewAtPoint:point];
    } else {
        [self updateMagnifierTouchPoint:point];
    }
}

- (void)dismissMagnifierView {
    if (_magnifierViewHidden) {
        return;
    }
    
    _magnifierViewHidden = YES;
    _magnifierHideTime = kInvalidTime;
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionBeginFromCurrentState
                     animations:^ {
                         _magnifierView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             _magnifierView.hidden = YES;
                         }
                     }
     ];
    
    if ([_delegate respondsToSelector:@selector(videoToolDidDismissMagnifierView:)]) {
        [_delegate videoToolDidDismissMagnifierView:self];
    }
}

- (void)temporaryHideElement:(CZElement *)element {
    if (self.magnifierView.viewToMagnify) {
        CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.magnifierView.viewToMagnify];
        [renderer hideElementOnly:element inElementLayer:element.parent];
        [renderer release];
    }
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == _docManager) {
        if ([keyPath isEqualToString:@"elementLayerModified"]) {
            if (_docManager.isModified) {
                if ([self.combineDelegate respondsToSelector:@selector(imageViewDocManagerModified:)]) {
                    [self.combineDelegate imageViewDocManagerModified:self.annotationView];
                }
            }
        }
    }
}

@end
