//
//  CZDefaultSettings+VideoTool.m
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+VideoTool.h"

@implementation CZDefaultSettings (VideoTool)

- (NSDictionary *)overlayColorDictionary {
    // Try to get the dictionary from user defaults.
    NSDictionary *colorDict = [[NSUserDefaults standardUserDefaults] objectForKey:kOverlayColor];
    
    // If no dictionary exists in the user defaults, use default settings and save.
    if (colorDict == nil) {
        colorDict = @{[@(kCZOverlayGrid) stringValue]: NSStringFromColor(kCZColorGreen),
                      [@(kCZOverlayCrossWithDivisions) stringValue]: NSStringFromColor(kCZColorBlue),
                      [@(kCZOverlayCrosshair) stringValue]: NSStringFromColor(kCZColorRed)};
        [[NSUserDefaults standardUserDefaults] setObject:colorDict forKey:kOverlayColor];
    }
    
    return colorDict;
}

- (NSDictionary *)overlaySizeDictionary {
    // Try to get the dictionary from user defaults.
    NSDictionary *sizeDict = [[NSUserDefaults standardUserDefaults] objectForKey:kOverlaySize];
    
    // If no dictionary exists in the user defaults, use default settings and save.
    if (sizeDict == nil) {
        sizeDict = @{[@(kCZOverlayGrid) stringValue]: @(CZElementSizeSmall),
                     [@(kCZOverlayCrossWithDivisions) stringValue]: @(CZElementSizeSmall),
                     [@(kCZOverlayCrosshair) stringValue]: @(CZElementSizeSmall)};
        [[NSUserDefaults standardUserDefaults] setObject:sizeDict forKey:kOverlaySize];
    }
    
    return sizeDict;
}

- (CZColor)colorForOverlay:(CZOverlayMode)overlay {
    NSDictionary *colorDict = [self overlayColorDictionary];
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)overlay];
    
    // Extract color for specified overlay option.
    NSString *colorString = colorDict[key];
    if (colorString != nil) {
        CZColor color = CZColorFromString(colorString);
        return color;
    }
    
    return kCZColorRed;
}

- (void)setColor:(CZColor)color forOverlay:(CZOverlayMode)overlay {
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)overlay];
    NSMutableDictionary *colorDict = [[self overlayColorDictionary] mutableCopy];
    colorDict[key] = NSStringFromColor(color);
    [[NSUserDefaults standardUserDefaults] setObject:colorDict forKey:kOverlayColor];
    [colorDict release];
}

- (CZElementSize)sizeForOverlay:(CZOverlayMode)overlay {
    NSDictionary *sizeDict = [self overlaySizeDictionary];
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)overlay];
    
    // Extract size for specified overlay option.
    NSNumber *sizeValue = sizeDict[key];
    if (sizeValue != nil) {
        CZElementSize size = [sizeValue unsignedIntegerValue];
        return size;
    }
    
    return CZElementSizeSmall;
}

- (void)setSize:(CZElementSize)size forOverlay:(CZOverlayMode)overlay {
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)overlay];
    NSMutableDictionary *sizeDict = [[self overlaySizeDictionary] mutableCopy];
    sizeDict[key] = @(size);
    [[NSUserDefaults standardUserDefaults] setObject:sizeDict forKey:kOverlaySize];
    [sizeDict release];
}

@end
