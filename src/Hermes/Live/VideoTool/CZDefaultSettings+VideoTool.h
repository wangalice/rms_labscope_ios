//
//  CZDefaultSettings+VideoTool.h
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZVideoTool.h"

/*! color settings for all the overlay options. since v5 [dictionary<NSString, NSString>]*/
#define kOverlayColor @"overlay_color"

/*! size settings for all the overlay options. since v5 [dictionary<NSString, NSNumber>]*/
#define kOverlaySize @"overlay_size"

@interface CZDefaultSettings (VideoTool)

- (CZColor)colorForOverlay:(CZOverlayMode)overlay;
- (void)setColor:(CZColor)color forOverlay:(CZOverlayMode)overlay;

- (CZElementSize)sizeForOverlay:(CZOverlayMode)overlay;
- (void)setSize:(CZElementSize)size forOverlay:(CZOverlayMode)overlay;

@end
