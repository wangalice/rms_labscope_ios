//
//  CZVideoTool.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZImageViewEditingDelegate.h"

typedef NS_ENUM(NSUInteger, CZOverlayMode) {
    kCZOverlayNone,
    kCZOverlayGrid,
    kCZOverlayCrossWithDivisions,
    kCZOverlayCrosshair,
    
    kCZOverlayModeCount
};

@class CZVideoTool;
@class CZDocManager;
@class CZShadingCorrectionTool;

@protocol CZVideoToolDelegate <NSObject>

- (void)videoToolDidDismissInteractiveSelection:(CZVideoTool *)videoTool;

// magnifier view callbacks
- (void)videoToolDidShowMagnifierView:(CZVideoTool *)videoTool;
- (void)videoToolDidDismissMagnifierView:(CZVideoTool *)videoTool;

@optional
- (BOOL)videoTool:(CZVideoTool *)videoTool swipeGesture:(UISwipeGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;
- (void)videoTool:(CZVideoTool *)videoTool handleSwipeGesture:(UISwipeGestureRecognizer *)gestureRecognizer;
@end

/**
 * A tool controller similar with CZImageTool, which contains a video output
 * view and provides interfaces for live video playback and display curve
 * switching.
 */
@interface CZVideoTool : NSObject<CZImageOutputViewDelegate>

@property (nonatomic, assign) id<CZVideoToolDelegate> delegate;
@property (nonatomic, assign) id<CZElementLayerDelegate, CZImageViewEditingDelegate> combineDelegate;  // sencond hand delegate

@property (nonatomic, retain) CZDocManager *docManager;

@property (nonatomic, assign) CZDisplayCurvePreset currentDisplayCurve;
@property (nonatomic, retain, readonly) CZImageOutputView *view;
@property (nonatomic, assign) BOOL magnifierEnabled;
@property (nonatomic, assign) BOOL needUpdateScaleBar;
@property (nonatomic, assign) BOOL overExposeEnabled;
@property (nonatomic, assign) BOOL annotationHidden;
@property (nonatomic, assign) BOOL scaleBarHidden;
@property (nonatomic, assign) CZOverlayMode currentOverlay;
@property (nonatomic, assign) float fieldOfView;

@property (nonatomic, retain, readonly) UIView *annotationView;
@property (nonatomic, assign, readonly) CGFloat annotationViewZoomScale;

@property (nonatomic, retain, readonly) UIImage *lastFrame;
@property (nonatomic, assign, readonly) CGSize lastFrameSize;

@property (nonatomic, retain) UIImage *placeholdImage;
@property (nonatomic, assign) CGSize preferredLiveResolution;

@property (nonatomic, assign) BOOL shadingCorrectionEnabled;
@property (nonatomic, retain) CZShadingCorrectionTool *shadingCorrectionTool;
@property (nonatomic, assign) BOOL shouldNotChangeScaleBarVisibility;//This key to identifier whether to change the scale bar visibility, when set to YES, do not change the scale bar visibility, else, use the original logic by checking other variables. Default value is NO;

- (void)setFrame:(UIImage *)frame completion:(void (^)(void))completion;

- (void)clear;

/*! Display curve switching UI controlling. */
- (void)showDisplayCurveSelection;

/*! Overlay UI controlling. */
- (void)showOverlaySelection;

/*! @return YES, if video tool is in seletion mode, otherwise NO.*/
- (BOOL)isShowingSelection;

/*! Dismiss interactive selection UI for either display curve or overlay. */
- (void)dismissInteractiveSelectionAnimated:(BOOL)animated;

- (void)dismissMagnifierView;
- (CGRect)magnifierViewFrame;

- (void)beginMeasuring;
- (void)beginMeasuringWithMeasurementHidden:(BOOL)measurementHidden;
- (float)measuredValue;  // unit in pixel
- (void)endMeasuring;

/*! Refresh all overlays, such as scale bar and reticle overlay. */
- (void)refreshLayers;
- (void)updateUnitByMicroscopeModelName:(NSString *)modelName;

- (UIImage *)screenshot;

- (BOOL)hasNoScaleBar;

@end
