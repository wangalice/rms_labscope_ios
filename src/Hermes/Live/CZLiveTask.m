//
//  CZLiveTask.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveTask.h"
#import "CZAlertController.h"
#import "CZMultitaskManager.h"
#import "CZMultitaskViewController.h"
#import "CZCameraView.h"

#import <CZVideoEngine/CZVideoEngine.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZImageProcessing/CZImageProcessing.h>

//Image
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZFileOverwriteController.h"
#import "CZDefaultSettings+Multichannel.h"
#import "CZImageDocument+CZCameraSnappingInfo.h"
#import "CZFileNameInputAlertController.h"
#import "CZToastManager.h"
#import "UIImage+CZObjective.h"

//Recording
#import <CZFileKit/CZFileKit.h>

#import "CZLiveViewModel.h"
#import "CZLiveTask+CZAnnotationReadWrite.h"

#define kDefaultSnappingMode @"default-snapping-mode"

// default snapping mode key: binding with mac address
static inline NSString *defaultSnappingMode(NSString *key) {
    return [NSString stringWithFormat:@"%@+%@", kDefaultSnappingMode, key];
}

static const NSUInteger geminiCameraBusyLimit = 1;

static NSString *const kMicroscopeModelShadingCorrectionToolKey = @"kMicroscopeModelShadingCorrectionToolKey";

@interface CZLiveTask () <
CZVideoToolDelegate,
CZMNAParingManagerDelegate,
CZMicroscopeModelUpdateInfoFromMNADelegate,
CZDocSaveDelegate,
CZDocManagerDelegate,
CZFileNameInputAlertViewControllerDelegate
> {
    NSObject *_cameraLock;
    
}

@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;
@property (nonatomic, assign) BOOL cameraAvailable; //declare current cameram is not available
@property (atomic, strong) CZCameraControlLock *cameraControlLock;  // lock for snapping
@property (nonatomic, strong) CZVideoDenoiser *denoiser;
@property (nonatomic, readwrite, copy) NSArray<CZLiveChannel *> *channels;
@property (nonatomic, copy) NSString *currentCameraName;
@property (nonatomic, strong) NSTimer *reconnectingTimer;
@property (nonatomic, strong) NSTimer *cameraCheckTimer;
@property (nonatomic, strong) CZFileNameTemplate *videoFileNameTemplate;
@property (nonatomic, strong) NSString *recordingVideoFile;
@property (nonatomic, assign) CZCaptureMode mode;
@property (nonatomic, assign, readwrite) BOOL isReconnecting;
@property (nonatomic, strong) UIImage *firstRecordingFrame;
@property (nonatomic, strong) CZCameraSnappingInfo *pausedSnappingInfo;
@property (nonatomic, assign) BOOL canGetCurrentObjectiveFromMNA;
@property (nonatomic, assign) BOOL canGetCurrentZoomFromMNA;
@property (nonatomic, strong) CZMNAParingManager *mnaParingManager;
@property (nonatomic, strong) NSTimer *updateObjectiveTimer;
@property (nonatomic, assign) BOOL didUpdateSnapModesFromMNA;
@property (nonatomic, assign) BOOL shouldValidateMNALevel;
@property (nonatomic, strong) CZCamera *interruptedCamera;

@property (nonatomic, readwrite, assign) BOOL objectiveSelectionButtonHidden;
@property (nonatomic, readwrite, assign) BOOL objectiveSelectionButtonEnabled;
@property (nonatomic, readwrite, copy) NSString *objectiveSelectionButtonTitle;
@property (nonatomic, readwrite, strong) UIImage *objectiveSelectionButtonImage;

@property (nonatomic, readwrite, assign) BOOL zoomSelectionButtonHidden;
@property (nonatomic, readwrite, assign) BOOL zoomSelectionButtonEnabled;
@property (nonatomic, readwrite, copy) NSString *zoomSelectionButtonTitle;
@property (nonatomic, readwrite, strong) UIImage *zoomSelectionButtonImage;

@property (nonatomic, readwrite, assign) BOOL filterSetSelectionButtonHidden;
@property (nonatomic, readwrite, assign) BOOL filterSetSelectionButtonEnabled;
@property (nonatomic, readwrite, copy) NSString *filterSetSelectionButtonTitle;
@property (nonatomic, readwrite, strong) UIImage *filterSetSelectionButtonImage;

@end

@implementation CZLiveTask
@dynamic delegate;
@synthesize lostFrame = _lostFrame;

- (instancetype)initWithIdentifier:(NSString *)identifier {
    self = [super initWithIdentifier:identifier];
    if (self) {
        _videoTool = [[CZVideoTool alloc] init];
        _videoTool.delegate = self;
        
        _objectiveSelectionButtonHidden = NO;
        _objectiveSelectionButtonEnabled = YES;
        
        _zoomSelectionButtonHidden = NO;
        _zoomSelectionButtonEnabled = YES;
        
        _filterSetSelectionButtonHidden = NO;
        _filterSetSelectionButtonEnabled = YES;
        
        _cameraLock = [[NSObject alloc] init];
        _mode = NSNotFound;
        _viewModel = [[CZLiveViewModel alloc] init];
        const NSTimeInterval kCameraCheckInterval = 1.0;
        [self.cameraCheckTimer invalidate];
        self.cameraCheckTimer = [NSTimer scheduledTimerWithTimeInterval:kCameraCheckInterval
                                                                 target:self
                                                               selector:@selector(cameraCheckTimerHandler:)
                                                               userInfo:nil
                                                                repeats:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(geminiCameraDidReceiveEvent:) name:CZGeminiCameraDidReceiveEventNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(geminiCameraStatusDidChange:) name:CZGeminiCameraStatusDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraViewWillShow:) name:CZCameraViewWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraViewWillHide:) name:CZCameraViewWillHideNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [_reconnectingTimer invalidate];
    [_cameraCheckTimer invalidate];
    [_updateObjectiveTimer invalidate];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZGeminiCameraDidReceiveEventNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZGeminiCameraStatusDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZCameraViewWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZCameraViewWillHideNotification object:nil];
}

- (void)didEnterBackground {
    [super didEnterBackground];
    
    [self.updateObjectiveTimer invalidate];
    self.updateObjectiveTimer = nil;
    
    self.didUpdateSnapModesFromMNA = NO;
    
    self.microscopeModel = nil;
}

- (void)didEnterForeground {
    self.didUpdateSnapModesFromMNA = NO;
    
    self.microscopeModel = [CZMicroscopeModel newMicroscopeModelWithCamera:self.currentCamera];
    [self updateMicroscopeModel];
    
    [super didEnterForeground];
}

#pragma mark - Public methods

- (void)setCurrentCamera:(CZCamera *)camera {
    [self setCurrentCamera:camera resetExclusiveLock:NO];
}

- (void)setCurrentCamera:(CZCamera *)camera resetExclusiveLock:(BOOL)resetExclusiveLock {
    self.cameraAvailable = YES;
    //Update microscope model
    @synchronized (_cameraLock) {
        if (camera && _currentCamera != camera) {
            //invalidate microscope model
            self.microscopeModel = nil;
            self.videoTool.shadingCorrectionTool = nil;
        }
        
        BOOL isRecordingInterrupted = NO;
        
        if (_currentCamera != camera) {
#if LIVE_REFACTORED
            // If current camera is changed, camera control should be dismissed.
            [self showCameraControlView:NO];
            [self hideOperatorNameView:YES];
            self.cameraControlView.camera = nil;
#endif
            
            //set snap modes changed to NO when camera changed;
            _didUpdateSnapModesFromMNA = NO;
            
            if (camera) {
                self.videoTool.preferredLiveResolution = camera.liveResolution;
            }
            if (_currentCamera) {
                self.videoTool.view.hidden = YES;
                [self.videoTool clear];
            }
            [self.videoTool endMeasuring];
#if LIVE_REFACTORED
            [self.focusIndicator reset];
#endif
            
            if (_currentCamera) {
                [self.reconnectingTimer invalidate];
                self.reconnectingTimer = nil;
                
                // Stop recording while switching camera.
                if (self.mode == kCZCaptureModeVideoRecording) {
#if LIVE_REFACTORED
                    [self snapButtonClicked:nil];
#endif
                    isRecordingInterrupted = YES;
                } else if (self.mode == kCZCaptureModeTimelapseVideoRecording) {
                    if ([camera.macAddress isEqualToString:_currentCamera.macAddress] &&
                        [camera class] == [_currentCamera class]) {
                        [camera handoverTimelapseRecorderFromCamera:_currentCamera];  // do not stop time-lapse recording, if the camera is not really changed.
                    } else {
#if LIVE_REFACTORED
                        [self snapButtonClicked:nil];
#endif
                        isRecordingInterrupted = YES;
                    }
                } else if (self.mode == kCZCaptureModeRemoteStream) {
#if LIVE_REFACTORED
                    [self snapButtonClicked:nil];
#endif
                }
                
                if (resetExclusiveLock && _currentCamera.exclusiveLock) {
                    __block CZCamera *tempCamera = _currentCamera;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        tempCamera.exclusiveLock = nil;
                    });
                }
                
                self.cameraControlLock = nil;
                [self exitFluorescenceSnappingMode];
                
                [_currentCamera endEventListening];
                [_currentCamera stop];
                [_currentCamera setDelegate:nil];
                _currentCamera = nil;
            }
            
            // Reset the video view to general mode while switching camera.
            [_videoTool dismissInteractiveSelectionAnimated:NO];
            [_videoTool dismissMagnifierView];
            
#if LIVE_REFACTORED
            [self showSnapOptions:NO];
            
            // switch to default mode if current camera doesn't support current mode.
            if (camera) {
                [self updateEnabledSnapModeForCamera:camera];
                if (!_allSelectSnapOptionEnableSetting[@(_mode)].boolValue) {
                    NSUInteger index = [self defaultSnapModeFor:camera];
                    _mode = index != NSNotFound ? (CZCaptureMode)index : kCZCaptureModePhoto;
                }
                
                [self snapOptionButtonClicked:_allSelectSnapOption[@(_mode)]];
            }
            
            [self.functionBar selectButton:nil];
#endif
            
            if (camera) {
                _currentCamera = camera;
                
                self.currentCameraName = camera.displayName;
                
#if LIVE_REFACTORED
                //set live tab label
                [delegate.tabBar setTabItemTitleAtIndex:kCZLiveViewController title:self.currentCameraName];
#endif
                
                [_currentCamera setDelegate:self];
                [_currentCamera play];
                if (self.mode == kCZCaptureModeMacroPhoto) {
                    [_currentCamera pause];
                }
                [_currentCamera beginEventListening];
                
                [self updateZoomRelatedControls];
#if LIVE_REFACTORED
                [self updateScaleBarStatus];
                [self updateCameraControlButtonIcon];
#endif
                
                camera.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
                [self.reconnectingTimer invalidate];
                self.reconnectingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                          target:self
                                                                        selector:@selector(streamReconnectingCheck:)
                                                                        userInfo:nil
                                                                         repeats:YES];
                
                if (![self.microscopeModel canSupportMNA]) {
                    self.canGetCurrentObjectiveFromMNA = NO;
                    self.canGetCurrentZoomFromMNA = NO;
                }
                
                if (!self.viewModel.isEditing) {
                    // load the elementLayer from the xml file
                    CZElementLayer *elementLayer = [self readElementXMLFile];
                    UIGraphicsBeginImageContext(CGSizeMake(1, 1));
                    UIImage *dummyImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    NSString *directory = [NSFileManager defaultManager].cz_documentPath;
                    CZImageDocument *document = [[CZImageDocument alloc] initWithImage:dummyImage grayscale:NO];
                    CZDocManager *docManager = [[CZDocManager alloc] initWithDirectory:directory document:document elementLayer:elementLayer];
                    [self.videoTool setDocManager:docManager];
                    
                    CGFloat ratio = _currentCamera.snapResolution.width / _currentCamera.liveResolution.width;
                    elementLayer.logicalScaling = ratio;
                }
            }
        }
        
#if LIVE_REFACTORED
        if (self.builtInCameraViewController != nil) {
            if (self.mode == kCZCaptureModeMacroPhoto) {
                [self.builtInCameraViewController setPreferredResolution:CGSizeZero];
            } else {
                if (_currentCamera) {
                    [self.builtInCameraViewController setPreferredResolution:_currentCamera.liveResolution];
                }
            }
        }
#endif
        
        // Manually trigger the camera reachability check to update HUD status.
        [self.cameraCheckTimer fire];
        
        // Pop up a dialog to let user know why video recording is stopped automatically.
        if (isRecordingInterrupted) {
            CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"VIDEO_RECORDING_INTERRUPTED") level:CZAlertLevelError];
            [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
            [alertController presentAnimated:YES completion:nil];
        }
    }
    
    self.microscopeModel = [CZMicroscopeModel newMicroscopeModelWithCamera:_currentCamera];
    [self updateMicroscopeModel];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(liveTaskDidConfigurateCamera:)]) {
            [self.delegate liveTaskDidConfigurateCamera:camera];
        }
    });
}

- (NSArray<CZLiveChannel *> *)enabledChannels {
    NSMutableArray<CZLiveChannel *> *enabledChannels = [NSMutableArray array];
    for (CZLiveChannel *channel in self.channels) {
        if (channel.isEnabled) {
            [enabledChannels addObject:channel];
        }
    }
    return [enabledChannels copy];
}

- (NSArray<CZLiveChannel *> *)selectedChannels {
    NSMutableArray<CZLiveChannel *> *selectedChannels = [NSMutableArray array];
    for (CZLiveChannel *channel in self.channels) {
        if (channel.isSelected) {
            [selectedChannels addObject:channel];
        }
    }
    return [selectedChannels copy];
}

- (NSArray<CZLiveChannel *> *)snappedChannels {
    NSMutableArray<CZLiveChannel *> *snappedChannels = [NSMutableArray array];
    for (CZLiveChannel *channel in self.channels) {
        if (channel.snappingInfo) {
            [snappedChannels addObject:channel];
        }
    }
    return [snappedChannels copy];
}

- (NSArray<NSNumber *> *)allSnapModes {
    return @[@(kCZCaptureModePhoto),
             @(kCZCaptureModeSnapFromStream),
             @(kCZCaptureModeSnapDenoise),
             @(kCZCaptureModeVideoStandby),
             @(kCZCaptureModeTimelapseVideoStandby),
             @(kCZCaptureModeMacroPhoto),
             @(kCZCaptureModeEDOF),
             @(kCZCaptureModeManualFluorescence),
             @(kCZCaptureModeOnekeyFluorescenceStandby)];
}

- (NSArray<NSNumber *> *)availableModes {
    NSMutableArray *snapModes = [NSMutableArray arrayWithArray:[self allSnapModes]];
    [self adjustSnapMode:snapModes withCamera:self.currentCamera];
    return snapModes;
}

- (void)updateDefaultSnappingMode:(CZCaptureMode)snappingMode {
    switch (snappingMode) {
        case kCZCaptureModeVideoRecording:
            snappingMode = kCZCaptureModeVideoStandby;
            break;
        case kCZCaptureModeTimelapseVideoRecording:
            snappingMode = kCZCaptureModeTimelapseVideoStandby;
            break;
        case kCZCaptureModeOnekeyFluorescencePlaying:
            snappingMode = kCZCaptureModeOnekeyFluorescenceStandby;
            break;
        default:
            break;
    }
    [self updateDefaultSnappingMode:snappingMode macAddress:self.currentCamera.macAddress];
}

- (CZCaptureMode)defaultSnappingMode {
    NSString *key = defaultSnappingMode([self.currentCamera.macAddress uppercaseString]);
    CZCaptureMode mode = [[[NSUserDefaults standardUserDefaults] objectForKey:key] integerValue];
    CZLogv(@"current default snapping mode: %zd", mode);
    return mode == NSNotFound ? kCZCaptureModePhoto : mode;
}

- (BOOL)isCurrentCameraInvalid {
    return (_currentCamera == nil || [_currentCamera isMemberOfClass:[CZCamera class]] || !_cameraAvailable);
}

- (BOOL)isCurrentCameraReachable {
    BOOL reachable = YES;
    if ([self isCurrentCameraInvalid]) {
        reachable = NO;
    } else {
        const NSTimeInterval kLostTime = -15.0;
        NSTimeInterval timeInterval = [[_currentCamera lastActiveTime] timeIntervalSinceNow];
        if (timeInterval <= kLostTime) {
            CZLogv(@"Camera becomes unreachable because of timeout.");
            reachable = NO;
        }
    }
    return reachable;
}

- (BOOL)isDenoisingEnableFor:(CZCamera *)camera mode:(CZCaptureMode)mode {
    BOOL enableDenoising = NO;
    if ([camera isKindOfClass:[CZMoticCamera class]]) {
        enableDenoising = YES;
    } else if ([camera isKindOfClass:[CZWisionCamera class]]) {
        if (mode == kCZCaptureModeSnapDenoise) {
            enableDenoising = YES;
        }
    } else if ([camera isKindOfClass:[CZKappaCamera class]]) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kappa_enable_denoise"]) {
            enableDenoising = YES;
        }
    }
    return enableDenoising;
}

- (void)updateDenoiser:(CZVideoDenoiser *)desnoiser {
    self.denoiser = desnoiser;
}

- (void)updateMode:(CZCaptureMode)mode {
    _mode = mode;
    [self updateDefaultSnappingMode:mode];
}

- (void)beginRecordingWithFrameRate:(NSUInteger)frameRate isNormalRecording:(BOOL)isNormalRecording {
    self.firstRecordingFrame = nil;
    if (_videoFileNameTemplate == nil) {
        _videoFileNameTemplate = [[CZFileNameTemplate alloc] init];

        CZFileNameTemplateField *field;
        field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeFreeText
                                                        value:@"Video"];
        [_videoFileNameTemplate addField:field];

        field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeAutoNumber
                                                        value:@"3"];
        [_videoFileNameTemplate addField:field];
    }
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:_videoFileNameTemplate];
    generator.extension = @"mp4";
    // Fix bug 187000 --Ding Yu
    //        NSString *fileName = [generator uniqueFileNameInDirectory:[CZAppDelegate get].documentPath];
#if DCM_REFACTORED
    NSString *fileName = [generator uniqueFileNameInDirectory:[CZLocalFileList sharedInstance].documentPath];
    NSString *filePath = [[CZLocalFileList sharedInstance].documentPath stringByAppendingPathComponent:fileName];
#else
    NSString *fileName = [generator uniqueFileNameInDirectory:[NSFileManager defaultManager].cz_documentPath];
    NSString *filePath = [[NSFileManager defaultManager].cz_documentPath stringByAppendingPathComponent:fileName];

#endif
    if ([self.delegate respondsToSelector:@selector(liveTaskDidBeginRecordingWithFileName:)]) {
        [self.delegate liveTaskDidBeginRecordingWithFileName:fileName];
    }
    self.recordingVideoFile = filePath;
    
    if (isNormalRecording) {
        [_currentCamera beginRecordingToFile:filePath];
    } else {
        CALayer *scaleBarLayer = [self scaleBarLayerForTimelapseRecording];
        NSTimeInterval timeInterval = [[CZDefaultSettings sharedInstance] timelapseIntervel];
        [CZTimelapseRecorder setNoSignalText:L(@"VIDEO_RECORDING_NO_SINGAL")];
        [_currentCamera beginTimelapseRecordingToFile:filePath
                                          inFrameRate:frameRate
                                             interval:timeInterval
                                            overlayer:scaleBarLayer];
        UIDevice *device = [UIDevice currentDevice];
        device.batteryMonitoringEnabled = YES;
    }

}

- (void)stopRecording:(BOOL)isNormalRecording {
    if (isNormalRecording) {
        [self.currentCamera endRecording];
    } else {  // kCZCaptureModeTimelapseVideoRecording
        [self.currentCamera endTimelapseRecording];
        UIDevice *device = [UIDevice currentDevice];
        device.batteryMonitoringEnabled = NO;
    }
    
    if (self.recordingVideoFile) {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:kCZFileUpdateNotification
                                          object:nil
                                        userInfo:@{kCZFileUpdateNotificationFileList: @[self.recordingVideoFile]}];
        self.recordedVideoFile = self.recordingVideoFile;
        self.recordingVideoFile = nil;
    }
}

- (void)updateElementLayerLogicalScaling:(CGFloat)ratio {
    self.videoTool.docManager.elementLayer.logicalScaling = ratio;
}

- (BOOL)isScaleBarOverlappedTimeStamp {
    CZElementScaleBar *scaleBar = (CZElementScaleBar *)[self.videoTool.docManager.elementLayer firstElementOfClass:[CZElementScaleBar class]];
    if (scaleBar) {
        CGRect timeStampFrame = [CZTimelapseRecorder timeStampFrameInOverlayer:[self scaleBarLayerForTimelapseRecording] videoSize:self.currentCamera.liveResolution];
        return [scaleBar isScaleBarOverlappedWithRect:timeStampFrame];
    }
    return NO;
}

- (void)enterFluorescenceSnappingMode:(CZCaptureMode)snapMode completionHandler:(void (^)(BOOL))completionHandler {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        if (completionHandler) {
            completionHandler(NO);
        }
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZFluorescenceSnappingMode mode = CZFluorescenceSnappingModeNone;
        switch (snapMode) {
            case kCZCaptureModeManualFluorescence:
                mode = CZFluorescenceSnappingModeManual;
                break;
            case kCZCaptureModeOnekeyFluorescenceStandby:
                mode = CZFluorescenceSnappingModeOnekey;
                break;
            default:
                break;
        }
        BOOL canEnterFluorescenceSnappingMode = [flCamera enterFluorescenceSnappingMode:mode delegate:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(canEnterFluorescenceSnappingMode);
            }
        });
    });
}

- (void)switchToFluorescenceSnappingMode:(CZCaptureMode)snapMode {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    CZFluorescenceSnappingMode mode = CZFluorescenceSnappingModeNone;
    switch (snapMode) {
        case kCZCaptureModeManualFluorescence:
            mode = CZFluorescenceSnappingModeManual;
            break;
        case kCZCaptureModeOnekeyFluorescenceStandby:
            mode = CZFluorescenceSnappingModeOnekey;
            break;
        default:
            break;
    }
    [flCamera switchToFluorescenceSnappingMode:mode];
}

- (void)exitFluorescenceSnappingMode {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    [flCamera exitFluorescenceSnappingMode];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskChannelsWillChange:)]) {
        [self.delegate liveTaskChannelsWillChange:self];
    }
    
    self.channels = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskChannelsDidChange:)]) {
        [self.delegate liveTaskChannelsDidChange:self];
    }
}

- (void)beginOrContinueManualFluorescenceSnapping {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    [flCamera beginOrContinueManualFluorescenceSnapping];
}

- (void)canBeginOnekeyFluorescenceSnapping:(void (^)(BOOL))completionHandler {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        if (completionHandler) {
            completionHandler(NO);
        }
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL canBeginOnekeyFluorescenceSnapping = [flCamera canBeginOnekeyFluorescenceSnapping];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(canBeginOnekeyFluorescenceSnapping);
            }
        });
    });
}

- (void)beginOnekeyFluorescenceSnapping {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    [flCamera beginOnekeyFluorescenceSnapping];
}

- (void)cancelOnekeyFluorescenceSnapping {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    [flCamera cancelOnekeyFluorescenceSnapping];
}

- (void)endFluorescenceSnappingForMode:(CZCaptureMode)mode action:(CZFluorescenceSnappingEndAction)action {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    switch (mode) {
        case kCZCaptureModeManualFluorescence:
            [flCamera endManualFluorescenceSnapping:action];
            [self resetChannels];
            break;
        case kCZCaptureModeOnekeyFluorescenceStandby:
            [flCamera endOnekeyFluorescenceSnapping:action];
            [self resetChannels];
        case kCZCaptureModeOnekeyFluorescencePlaying:
            [flCamera cancelOnekeyFluorescenceSnapping];
            [self resetChannels];
        default:
            break;
    }
}

- (void)updateScaling {
    if (self.microscopeModel) {
        // TODO: Replace the method getting the scaling info in Gemini Camera as other camera do.
        if ([self.microscopeModel isKindOfClass:[CZMicroscopeAxioline class]]) {
            NSUInteger position = self.microscopeModel.nosepiece.currentPosition;
            self.videoTool.fieldOfView = [self.microscopeModel fovWidthAtPosition:position];
        } else {
            self.videoTool.fieldOfView = [self.microscopeModel currentFOVWidth];
        }
        // If there is no scaling info, also display the scale bar with xxx pixels.
        [self.videoTool setScaleBarHidden:NO];
    }
}

- (void)updateMicroscopeModel {
    if (self.microscopeModel) {
        [self.videoTool updateUnitByMicroscopeModelName:[self.microscopeModel modelName]];
    }
    [self updateScaling];
}

- (BOOL)canUndo {
    return [self.videoTool.docManager canUndo];
}

- (BOOL)canRedo {
    return [self.videoTool.docManager canRedo];
}

- (void)undoAction {
    if ([self canUndo]) {
        [self.videoTool.docManager undo];
    }
}

- (void)redoAction {
    if ([self canRedo]) {
        [self.videoTool.docManager redo];
    }
}

- (void)selectObjectiveAtPosition:(NSUInteger)position {
    self.microscopeModel.nosepiece.currentPosition = position;
    [self.microscopeModel saveSlotsToDefaults];
    
    [self updateObjectiveSetting];
    [self updateScaling];
    [self updateLiveShadingCorrectionToolWithImageSize:self.videoTool.lastFrame.size];
}

- (void)selectZoomClickStopAtPosition:(NSUInteger)position {
    self.microscopeModel.currentZoomClickStopPosition = position;
    [self.microscopeModel saveSlotsToDefaults];
    
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        [self updateZoomSetting];
    } else {
        [self updateObjectiveSetting];
    }
    [self updateScaling];
    [self updateLiveShadingCorrectionToolWithImageSize:self.videoTool.lastFrame.size];
}

- (void)updateZoomRelatedControls {
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        [self updateZoomSetting];
        
        self.objectiveSelectionButtonHidden = YES;
        self.zoomSelectionButtonHidden = NO;
        if ([self.microscopeModel canSupportMNA]) {
            self.zoomSelectionButtonEnabled = NO;
        } else if ([self.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
            self.zoomSelectionButtonEnabled = NO;
        }
    } else {
        [self updateObjectiveSetting];
        
        self.objectiveSelectionButtonHidden = NO;
        self.zoomSelectionButtonHidden = YES;
        
        if ([self.microscopeModel canSupportMNA]) {
            self.objectiveSelectionButtonEnabled = NO;
        } else if ([self.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
            self.objectiveSelectionButtonEnabled = NO;
        }
    }
    
    [self updateFilterSetSetting];
    
    if ([self.microscopeModel hasReflector]) {
        self.filterSetSelectionButtonHidden = NO;
        self.filterSetSelectionButtonEnabled = NO;
    } else {
        self.filterSetSelectionButtonHidden = YES;
        self.filterSetSelectionButtonEnabled = NO;
    }
    
    [self updateMicoscopeModelFromMNA];
}

#pragma mark - Private

- (CZMNAParingManager *)mnaParingManager {
    if (!_mnaParingManager) {
        _mnaParingManager = [[CZMNAParingManager alloc] init];
        _mnaParingManager.delegate = self;
    }
    return _mnaParingManager;
}


- (void)updateDefaultSnappingMode:(CZCaptureMode)snappingMode macAddress:(NSString *)macAddress {
    NSString *key = defaultSnappingMode([macAddress uppercaseString]);
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:snappingMode]
                                             forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)adjustSnapMode:(NSMutableArray<NSNumber *> *)snapMode withCamera:(CZCamera *)camera {
    if (![CZDefaultSettings sharedInstance].enableMacroSnap) {
        [snapMode removeObject:@(kCZCaptureModeMacroPhoto)];
    }
    
    if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
        [snapMode removeObject:@(kCZCaptureModeEDOF)];
        [snapMode removeObject:@(kCZCaptureModeVideoStandby)];
        [snapMode removeObject:@(kCZCaptureModeTimelapseVideoStandby)];
    } else if (!(self.microscopeModel.type == kPrimoStar ||
                 self.microscopeModel.type == kPrimotech ||
                 self.microscopeModel.type == kPrimovert ||
                 self.microscopeModel.type == kAxiolab   ||
                 self.microscopeModel.type == kAxioscope ||
                 self.microscopeModel.type == kAxiocamCompound)) {
        [snapMode removeObject:@(kCZCaptureModeEDOF)];
    }

    if (!([camera isKindOfClass:[CZKappaCamera class]] && (self.microscopeModel.type != kUnknownModelType))) {
        [snapMode removeObject:@(kCZCaptureModeSnapFromStream)];
    }

    if (![camera isKindOfClass:[CZWisionCamera class]]) {
        [snapMode removeObject:@(kCZCaptureModeSnapDenoise)];
    }
    
    if (![camera isKindOfClass:[CZGeminiCamera class]] || ([camera hasCapability:CZCameraCapabilityEncoding] && ![camera hasLightPath:CZCameraLightPathReflected])) {
        [snapMode removeObject:@(kCZCaptureModeManualFluorescence)];
        [snapMode removeObject:@(kCZCaptureModeOnekeyFluorescenceStandby)];
    } else if (![self canDisplayOnekeyFluorescenceSnapMode]) {
        [snapMode removeObject:@(kCZCaptureModeOnekeyFluorescenceStandby)];
    }
}

// Shading correction
- (UIImage *)shadingCorrectionWithImage:(UIImage *)image {
    if (self.mode == kCZCaptureModeMacroPhoto) {
        return image;
    }
    
    NSString *path = [self.microscopeModel currentPathForShadingCorrectionMaskWithImageResolution:image.size];
    CZShadingCorrectionData *shadingCorrectionData = [self.microscopeModel currentShadingCorrectionDataWithImageResolution:image.size];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path] || shadingCorrectionData == nil) {
        return image;
    }
    
    @autoreleasepool {
        //FIXME: the empty shading correction data will result that the image snaped/HQ snaped on live tab covered with green color
        CZShadingCorrectionTool *shadingCorrectionTool = [[CZShadingCorrectionTool alloc] initWithShadingMaskFile:path
                                                                                                         maxMaskX:shadingCorrectionData.maxMaskX
                                                                                                         maxMaskY:shadingCorrectionData.maxMaskY
                                                                                                         maxMaskZ:shadingCorrectionData.maxMaskZ
                                                                                                        grayscale:image.czi_grayscale];
        UIImage *shadingCorrectedImage = [shadingCorrectionTool shadingCorrectedImageFrom:image];
        switch (image.czi_pixelType) {
            case CZIPixelTypeGray8:
                shadingCorrectedImage = [CZCommonUtils grayImageFromImage:shadingCorrectedImage];
                break;
            case CZIPixelTypeGray16:
                shadingCorrectedImage = [CZCommonUtils gray16ImageFromImage:shadingCorrectedImage];
                break;
            default:
                break;
        }
        return shadingCorrectedImage;
    }
}

- (void)presentNewDocManagerWithDocument:(CZImageDocument *)document showsAnnotation:(BOOL)showsAnnotation {
    self.snappedImageDocument = document;
    
    CZMicroscopeModel *model = self.microscopeModel;
    if (_mode == kCZCaptureModeMacroPhoto) {
        model = [[CZMicroscopeModelMacroImage alloc] init];
    }
    
    CZElementLayer *elementLayer = nil;
    if (showsAnnotation) {
        // copy and transform elements
        CGSize srcSize = _videoTool.docManager.elementLayer.frame.size;
        CGSize dstSize = document.imageSize;
        CGFloat scale = dstSize.width / srcSize.width;
        CGSize scaledSrcSize = CGSizeMake(dstSize.width, srcSize.height * scale);
        CGFloat yOffset = (dstSize.height - scaledSrcSize.height) / 2;
        
        elementLayer = [_videoTool.docManager.elementLayer copy];
        elementLayer.logicalScaling = 1.0;
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, yOffset);
        transform = CGAffineTransformScale(transform, scale, scale);
        if (!CGAffineTransformIsIdentity(transform)) {
            [elementLayer applyTransform:transform];
        }
        
        elementLayer.frame = CGRectMake(0, 0, dstSize.width, dstSize.height);
    }
    
#if DCM_REFACTORED
    NSString *directory = [CZLocalFileList sharedInstance].documentPath];
#else
    NSString *directory = [NSFileManager defaultManager].cz_documentPath;
#endif
    CZDocManager *newDocManager = [[CZDocManager alloc] initWithDirectory:directory document:document elementLayer:elementLayer];
    newDocManager.isImageSnapped = YES;
    newDocManager.isImageNew = YES;
    newDocManager.delegate = self;
    [newDocManager updateImagePropertyFrom:model updateFilePath:YES];
    self.snappedImageFilePath = newDocManager.filePath;
    
    CZFileNameInputAlertController *controller = [[CZFileNameInputAlertController alloc] initWithDocManager:newDocManager];
    controller.delegate = self;
    
    if ([controller requiresUserInput]) {
        [controller showInputAlert];
    } else {
        [newDocManager scheduleSaveInDefaultFormatWithDelegate:self];
    }
    
    if (_mode == kCZCaptureModeMacroPhoto) {
#if LIVE_REFACTORED
        [self showSnapOptions:NO];
        [self snapOptionButtonClicked:_snapSelectPhotoButton];
        [self pauseCamera];

        CZImageScalingViewController *controller = [[CZImageScalingViewController alloc] initWithDocManager:newDocManager];
        controller.delegate = self;
        [[[CZAppDelegate get] navigationController] pushViewController:controller animated:YES];
#endif
    } else {
        
#if LIVE_REFACTORED
        CZAppDelegate *delegate = [CZAppDelegate get];
        CZImageViewController *imageViewController = delegate.tabBar.tabViewControllers[kCZImageViewController];
        [imageViewController presentImageDocument:docManager];
        
        BOOL continuousSnapping = [[CZDefaultSettings sharedInstance] stayInLiveTabAfterSnappingEnabled];
        if (continuousSnapping || _isLocked) {
            [delegate.tabBar showImageTabFlashAnimation];
        } else {
            [delegate.tabBar switchToTabAtIndex:(kCZImageViewController)];
            [self showFullScreen:NO];
            [[delegate tabBar] showFullScreen:NO];
            [imageViewController applyShortcut];
        }
#endif
    }
}

- (void)updateEnabledSnapModeForCamera:(CZCamera *)camera {
#if DCM_REFACTORED
    NSArray<NSNumber *> *allmode = [self availableSnapModeFor:camera];
    NSArray<NSNumber *> *dcmSetting = [self availableSnapModeFromDCMTemplateFor:camera];
    
    for (NSNumber* key in _allSelectSnapOptionEnableSetting.allKeys) {
        _allSelectSnapOptionEnableSetting[key] = @(NO);
    }
    
    for (NSNumber* mode in allmode) {
        _allSelectSnapOptionEnableSetting[mode] = @([dcmSetting containsObject:mode]);
    }
#endif
}

- (CALayer *)scaleBarLayerForTimelapseRecording {
    CZElementScaleBar *scaleBar = (CZElementScaleBar *)[self.videoTool.docManager.elementLayer firstElementOfClass:[CZElementScaleBar class]];
    if (scaleBar) {
        CGRect frame = CGRectZero;
        frame.size = [self.currentCamera liveResolution];
        
        CZElementLayer *elementLayer = [[CZElementLayer alloc] init];
        elementLayer.frame = frame;
        elementLayer.scaling = self.videoTool.docManager.elementLayer.scaling;
        
        CZElementScaleBar *newScaleBar = [scaleBar copy];
        newScaleBar.size = [[CZDefaultSettings sharedInstance] elementSize];
        newScaleBar.fillColor = CZColorMakeRGBA(255, 255, 255, 128);
        newScaleBar.margin = 8.0;
        [elementLayer addElement:newScaleBar];
        
        CALayer *layer = [elementLayer newLayer];
        [layer removeAllAnimations];
        return layer;
    }
    return nil;
}

- (void)updateObjectiveSetting {
    BOOL enabled = (self.mode != kCZCaptureModeMacroPhoto && [self isCurrentCameraReachable]);
    self.objectiveSelectionButtonEnabled = enabled;
    
    if (self.canGetCurrentObjectiveFromMNA || [self.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
        self.objectiveSelectionButtonEnabled = NO;
    }
    
    if (!self.microscopeModel.hasObjectivesAssigned) {
        self.objectiveSelectionButtonEnabled = NO;
        self.objectiveSelectionButtonTitle = nil;
        self.objectiveSelectionButtonImage = [UIImage objectiveImageForMagnification:0.0 size:CZIconSizeMedium];
    } else {
        NSString *magnificationName = [[self.microscopeModel.nosepiece objectiveAtCurrentPosition] displayMagnification];
        if (magnificationName.length > 0 && ![magnificationName isEqualToString:kNotAvailableMagnification]) {
            self.objectiveSelectionButtonTitle = magnificationName;
            
            float magnification = [[self.microscopeModel.nosepiece objectiveAtCurrentPosition] magnification];
            UIImage *image = [UIImage objectiveImageForMagnification:magnification size:CZIconSizeSmall];
            self.objectiveSelectionButtonImage = image;
        } else {
            self.objectiveSelectionButtonTitle = nil;
            self.objectiveSelectionButtonImage = [UIImage objectiveImageForMagnification:0.0 size:CZIconSizeMedium];
        }
    }
}

- (void)updateZoomSetting {
    BOOL enabled = (self.mode != kCZCaptureModeMacroPhoto && [self isCurrentCameraReachable]);
    self.zoomSelectionButtonEnabled = enabled;
    
    if (self.canGetCurrentZoomFromMNA) {
        self.zoomSelectionButtonEnabled = NO;
    }
    
    if ([self.microscopeModel zoomClickStopPositions] == 0) {
        self.zoomSelectionButtonEnabled = NO;
        self.zoomSelectionButtonTitle = nil;
        self.zoomSelectionButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeMedium];
    } else {
        if ([self.microscopeModel zoomClickStopPositions] == 1)  {
            NSString *displayMagnification = [self.microscopeModel displayMagnificationAtPosition:self.microscopeModel.currentZoomClickStopPosition localized:YES];
            self.zoomSelectionButtonEnabled = NO;
            self.zoomSelectionButtonTitle = displayMagnification;
            self.zoomSelectionButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeSmall];
        } else {
            NSString *displayMagnification = [self.microscopeModel displayMagnificationAtPosition:self.microscopeModel.currentZoomClickStopPosition localized:YES];
            self.zoomSelectionButtonTitle = displayMagnification;
            self.zoomSelectionButtonImage = [[CZIcon iconNamed:@"zoom-in-zoom-out"] imageForControlWithSize:CZIconSizeSmall];
        }
    }
}

- (void)updateFilterSetSetting {
    CZFilterSet *filterSet = [self.microscopeModel.reflector filterSetAtCurrentPosition];
    if (filterSet == nil) {
        self.filterSetSelectionButtonTitle = nil;
        self.filterSetSelectionButtonImage = [[CZIcon iconNamed:@"reflector"] imageForControlWithSize:CZIconSizeMedium];
    } else {
        self.filterSetSelectionButtonTitle = filterSet.filterSetName;
        self.filterSetSelectionButtonImage = [[CZIcon iconNamed:@"reflector"] imageForControlWithSize:CZIconSizeSmall];
    }
}

- (void)updateLiveShadingCorrectionToolWithImageSize:(CGSize)size {
    NSString *path = [self.microscopeModel currentPathForShadingCorrectionMaskWithImageResolution:size];
    BOOL hasMask = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (hasMask) {
        // TODO: Temporary workaround, shading correction in live will use the x, y, z of snapped image.
        CZShadingCorrectionData *shadingCorrectionData = [self.microscopeModel currentShadingCorrectionDataWithImageResolution:[self.currentCamera snapResolution]];
        if (shadingCorrectionData == nil) {
            [self.microscopeModel.shadingCorrectionUserInfo removeObjectForKey:kMicroscopeModelShadingCorrectionToolKey];
        }
        
        if ((self.microscopeModel.shadingCorrectionMaskPath == nil) ||
            ![self.microscopeModel.shadingCorrectionMaskPath isEqualToString:path] ||
            ![self.microscopeModel.shadingCorrectionMaskData isEqualToShadingCorrectionData:shadingCorrectionData]||
            !CGSizeEqualToSize(size, self.videoTool.lastFrameSize)) {
            self.microscopeModel.shadingCorrectionMaskPath = path;
            self.microscopeModel.shadingCorrectionMaskData = shadingCorrectionData;
            
            //FIXME: the diff between the correct logic is that: Shading data load from file is nil, but the image is existed. Fix this with filtering
            // the nil sharding correction data. I will update this with a document Later.
            if ( shadingCorrectionData != nil ){
                CZShadingCorrectionTool *tool = [[CZShadingCorrectionTool alloc] initWithShadingMaskFile:path
                                                                                                maxMaskX:shadingCorrectionData.maxMaskX
                                                                                                maxMaskY:shadingCorrectionData.maxMaskY
                                                                                                maxMaskZ:shadingCorrectionData.maxMaskZ
                                                                                               grayscale:NO];
                self.microscopeModel.shadingCorrectionUserInfo[kMicroscopeModelShadingCorrectionToolKey] = tool;
            } else {
                CZLogv(@"Shading correnction is enbaled. But couldn't get the currect shading connection data");
            }
        }
    } else {
        [self.microscopeModel.shadingCorrectionUserInfo removeObjectForKey:kMicroscopeModelShadingCorrectionToolKey];
        self.microscopeModel.shadingCorrectionMaskPath = nil;
        self.microscopeModel.shadingCorrectionMaskData = nil;
    }
    
    self.videoTool.shadingCorrectionTool = self.microscopeModel.shadingCorrectionUserInfo[kMicroscopeModelShadingCorrectionToolKey];
    self.videoTool.shadingCorrectionEnabled = !(self.videoTool.shadingCorrectionTool == nil);
}

- (void)updateMicoscopeModelFromMNA {
    if (self.microscopeModel == nil) {
        return;
    }
    
    if (!self.microscopeModel.canSupportMNA) {
        [self mnaPairingManager:self.mnaParingManager didFindPairedMNA:nil];
        return;
    }
    
    [self.mnaParingManager searchMNAPairedWithModelAsync:self.microscopeModel];
}

- (void)updateObjectiveSettingFromMNA:(NSTimer *)timer {
    if (self.microscopeModel && self.microscopeModel.canSupportMNA) {
        [self.microscopeModel retrieveCurrentObjectiveFromMNAAsync:self];
    }
}

- (void)updateSnapModesFromMNA {
    if (!_didUpdateSnapModesFromMNA) {
        _didUpdateSnapModesFromMNA = YES;
#if LIVE_REFACTORED
        [self updateSnapUIStatus:NO];
#endif
    }
}

#pragma mark - Channels

- (void)resetChannels {
    [self.channels enumerateObjectsUsingBlock:^(CZLiveChannel *channel, NSUInteger index, BOOL *stop) {
        channel.snappingInfo = nil;
        if ([channel isKindOfClass:[CZVirtualLightSourceChannel class]]) {
            channel.selected = index == 0;
            channel.enabled = index == 0;
        }
    }];
}

- (void)saveMultichannelImage {
    CZImageDocument *document = [[CZImageDocument alloc] initFromChannels:self.channels];
    [self presentNewDocManagerWithDocument:document showsAnnotation:YES];
}

- (BOOL)canDisplayOnekeyFluorescenceSnapMode {
    if (![self.currentCamera conformsToProtocol:@protocol(CZFluorescenceSnapping)]) {
        return NO;
    }
    
    CZCamera<CZFluorescenceSnapping> *flCamera = (CZCamera<CZFluorescenceSnapping> *)self.currentCamera;
    
    if (![flCamera hasCapability:CZCameraCapabilityEncoding]) {
        return NO;
    }
    
    if (flCamera.onekeyFluorescenceSnappingTemplate == nil) {
        return NO;
    }
    
    if (flCamera.onekeyFluorescenceSnappingTemplate.snappingInfos.count == 0) {
        return NO;
    }
    
    NSString *filterSetID = nil;
    for (CZCameraSnappingInfo *snappingInfo in flCamera.onekeyFluorescenceSnappingTemplate.snappingInfos) {
        if (snappingInfo.filterSetID == nil) {
            return NO;
        }
        if (filterSetID == nil) {
            filterSetID = snappingInfo.filterSetID;
        } else if (![snappingInfo.filterSetID isEqualToString:filterSetID]) {
            return NO;
        }
    }
    
    if (filterSetID == nil) {
        return NO;
    }
    
    CZReflector *reflector = [[CZReflector alloc] init];
    for (CZFilterSet *filterSet in reflector.availableFilterSets) {
        if ([filterSet.matID isEqualToString:filterSetID]) {
            return filterSet.isMultiBandpass;
        }
    }
    
    return NO;
}

- (BOOL)isFluorescenceSnapButtonEnabledForMode:(CZCaptureMode)mode {
    switch (mode) {
        case kCZCaptureModeManualFluorescence:
            return self.selectedChannels.count == 1;
        case kCZCaptureModeOnekeyFluorescenceStandby:
            return self.enabledChannels.count > 0 && [self.microscopeModel.reflector filterSetAtCurrentPosition].isMultiBandpass;
        default:
            return NO;
    }
}

#pragma mark - Timer

- (void)streamReconnectingCheck:(NSTimer *)timer {
    if (self.currentCamera && ![self.currentCamera isPaused]) {
        NSTimeInterval interval = [self.currentCamera.lastFrameTime timeIntervalSinceNow];
        if (interval < -3.0) {
            _lostFrame = YES;
            if ([self.delegate respondsToSelector:@selector(liveTaskDidLostCamera:)]) {
                [self.delegate liveTaskDidLostCamera:self.currentCamera];
            }
            
            // Stop recording while switching camera.
            if (self.mode == kCZCaptureModeTimelapseVideoRecording) {
                [self.currentCamera.timelapseRecorder recordVideoFrame:nil];  // record no signal frame
            }

            if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
                    // If there was no new frame for at least 3 seconds.
                    CZLogv(@"Restarting live playback because no frames comes in for a while.");
                    [self.currentCamera stop];
                    [self.currentCamera play];
                });
            }
            
            // Set a fake frame refresh time to delay next retry, give a
            // chance to current stop and play.
            self.currentCamera.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0.0];
        }
    }
}

- (void)cameraCheckTimerHandler:(NSTimer *)timer {
    if (self.currentCamera == nil) return;
    BOOL reachable = [self isCurrentCameraReachable];
    if (!self.isReconnecting && !reachable) {
        self.isReconnecting = YES;
    } else if (self.isReconnecting && reachable) {
        self.isReconnecting = NO;
    }
    
    BOOL status = NO;
    if (reachable == YES && _lostFrame == NO) {
        status = YES;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(camera:connectingStatusChanged:)]) {
            [self.delegate camera:self.currentCamera connectingStatusChanged:status];
        }
    });
}

#pragma mark - Notification

- (void)geminiCameraStatusDidChange:(NSNotification *)note {
    if (self.currentCamera != note.object || ![self.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
        return;
    }
    
    CZGeminiCameraStatus *status = note.userInfo[CZGeminiCameraStatusKey];
    CZGeminiCameraStatusChangeMask changes = [note.userInfo[CZGeminiCameraStatusChangesKey] unsignedIntegerValue];
    
    if ((changes & CZGeminiCameraStatusChangeMaskObjectivePosition) != 0) {
        self.microscopeModel.nosepiece.currentPosition = status.objectivePosition;
        [self updateObjectiveSetting];
        [self updateScaling];
    }
    
    if ((changes & CZGeminiCameraStatusChangeMaskReflectorPosition) != 0) {
        self.microscopeModel.reflector.currentPosition = status.reflectorPosition;
        [self updateFilterSetSetting];
    }
    
    if ((self.mode == kCZCaptureModeOnekeyFluorescencePlaying) &&
        (((changes & CZGeminiCameraStatusChangeMaskObjectivePosition) != 0) ||
        ((changes & CZGeminiCameraStatusChangeMaskReflectorPosition) != 0) ||
        ((changes & CZGeminiCameraStatusChangeMaskRLTLPosition) != 0))) {
        [self cancelOnekeyFluorescenceSnapping];
        if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskDidFinishSnapping:error:)]) {
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:kCZCameraInterrupttedError userInfo:@{NSLocalizedDescriptionKey: L(@"LIVE_FL_INTERRUPTTED")}];
            [self.delegate liveTaskDidFinishSnapping:nil error:error];
        }
    }
}

- (void)geminiCameraDidReceiveEvent:(NSNotification *)note {
    if (self.currentCamera != note.object || ![self.currentCamera hasCapability:CZCameraCapabilityEncoding]) {
        return;
    }
    
    CZGeminiCameraHardwareEvent *event = note.userInfo[CZGeminiCameraEventKey];
    NSInteger currentClientNumber = [note.userInfo[CZGeminiCameraClientNumberKey] integerValue];
    if ([event.eventType isEqualToString:CZGeminiCameraEventTypeHardwareSnap]) {
        CZGeminiCameraEventStandType availableStandEventType = NSNotFound;
        // if current camera connected client Number > 1
        if (currentClientNumber == geminiCameraBusyLimit) {
            availableStandEventType = [event.value integerValue];
        }
        
        if ([self.delegate respondsToSelector:@selector(liveTaskGeminiCameraDidReceiveHardwareEvent:availableEventType:)]) {
            [self.delegate liveTaskGeminiCameraDidReceiveHardwareEvent:self availableEventType:availableStandEventType];
        }
    }
}

- (void)cameraViewWillShow:(NSNotification *)note {
    self.interruptedCamera = self.currentCamera;
    [self setCurrentCamera:nil];
}

- (void)cameraViewWillHide:(NSNotification *)note {
    [self setCurrentCamera:self.interruptedCamera];
    self.interruptedCamera = nil;
}

#pragma mark - CZVideoToolDelegate

- (void)videoToolDidDismissInteractiveSelection:(CZVideoTool *)videoTool {
    
}

- (void)videoToolDidShowMagnifierView:(CZVideoTool *)videoTool {
    
}

- (void)videoToolDidDismissMagnifierView:(CZVideoTool *)videoTool {
    
}

#pragma mark - CZCameraDelegate

- (void)camera:(CZCamera *)camera didGetControlBeforeSnapping:(CZCameraControlLock *)lock {
    if (camera == self.currentCamera) {
        self.cameraControlLock = lock;
    }
}

- (void)camera:(CZCamera *)camera didPauseSnapping:(CZCameraSnappingInfo *)snappingInfo reason:(CZCameraSnappingPauseReason)reason {
    if (camera == self.currentCamera) {
        self.pausedSnappingInfo = snappingInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(liveTaskDidPauseSnapping:reason:)]) {
                [self.delegate liveTaskDidPauseSnapping:snappingInfo reason:reason];
            }
        });
    }
}

- (void)camera:(CZCamera *)camera didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {
    CZLogv(@"Camera: %@, snapping image success! Image: %@, exposureTime:%@, gray: %d, error: %@", camera, snappingInfo.image, snappingInfo.exposureTimeInMilliseconds, snappingInfo.grayscale, error);

    __block NSError *snappingError = nil;
    
    if (camera == nil) {
        return;
    }
    
    __block UIImage *snapOutputImage = nil;
    
    if (self.denoiser) {
        if (snappingInfo.image == nil) {  // snap failed
            self.denoiser = nil;
        } else {
            UIImage *denoisedImage = [self.denoiser newDenoisedFrame:snappingInfo.image];
            if (denoisedImage) {
                self.denoiser = nil;
                snapOutputImage = denoisedImage;
            } else {
                return;  // skip and wait for next frame
            }
        }
    } else {
        snapOutputImage = snappingInfo.image;
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        @autoreleasepool {
            self.cameraControlLock = nil;
            
            // error message
            if (error != nil || snapOutputImage == nil) {
                NSString *message = nil;
                switch (error.code) {
                    case kCZCameraTimeoutError:
                        message = L(@"ERROR_SNAP_TIMEOUT");
                        break;
                    case kCZCameraNoControlError:
                        message = L(@"LIVE_NO_CONTROL_ACCESS_MESSAGE");
                        break;
                    case kCZCameraIncompatibleFirmwareVersionError:
                        message = L(@"ERROR_OLD_AXIOCAM20X_FW");
                        break;
                    default:
                        break;
                }
                NSDictionary *userInfo = message ? @{NSLocalizedDescriptionKey: message} : nil;
                snappingError = [NSError errorWithDomain:NSURLErrorDomain
                                                    code:error.code
                                                userInfo:userInfo];
                
                if ([self.delegate respondsToSelector:@selector(liveTaskDidFinishSnapping:error:)]) {
                    CZCameraSnappingInfo *outputSnappingInfo = [snappingInfo snappingInfoByTransformingImageToImage:snapOutputImage];
                    [self.delegate liveTaskDidFinishSnapping:outputSnappingInfo error:snappingError];
                }
                return;
            }
            
            // Shadding correction
            snapOutputImage = [self shadingCorrectionWithImage:snapOutputImage];

            if ([self.delegate respondsToSelector:@selector(liveTaskDidFinishSnapping:error:)]) {
                CZCameraSnappingInfo *outputSnappingInfo = [snappingInfo snappingInfoByTransformingImageToImage:snapOutputImage];
                [self.delegate liveTaskDidFinishSnapping:outputSnappingInfo error:snappingError];
            }
        }
    });
}

- (void)camera:(CZCamera *)camera didGetVideoFrame:(UIImage *)frame {
    if (camera == self.currentCamera) {
        _lostFrame = NO;
        if (self.recordingVideoFile != nil && self.firstRecordingFrame == nil) {
            _firstRecordingFrame = frame;
        }
        if (_currentCamera) {
            if (self.state == CZTaskStateForeground) {
                // don't re-create shading correction tool if path and mask don't change.
                [self updateLiveShadingCorrectionToolWithImageSize:frame.size];
                @weakify(self);
                [self.videoTool setFrame:frame completion:^{
                    @strongify(self);
                    if (self.videoTool.view.isHidden) {
                        self.videoTool.view.hidden = NO;
                    }
                }];
#if LIVE_REFACTORED
                //Refactoring this logic, becase [UIView isHidden] must be used from main thread only.
                dispatch_sync(dispatch_get_main_queue(), ^() {
                    if ([_focusIndicator canUpdateWithImage:frame]) {
                        [_focusIndicator updateWithImage:frame];
                    }
                });
#endif
            }
            
            if (_currentCamera) {
                dispatch_async(dispatch_get_main_queue(), ^() {
                    if ([self.delegate respondsToSelector:@selector(liveTaskDidGetVideoFrame:)]) {
                        [self.delegate liveTaskDidGetVideoFrame:frame];
                    }
                });
            }
        }
    }
}

- (void)cameraDidFinishRecording:(CZCamera *)camera {
    if (camera == self.currentCamera) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            if ([self.delegate respondsToSelector:@selector(liveTaskDidFinishRecording)]) {
                [self.delegate liveTaskDidFinishRecording];
            }
            self.firstRecordingFrame = nil;
        });
    }
}

#pragma mark - CZFluorescenceSnappingDelegate

- (void)camera:(CZCamera<CZFluorescenceSnapping> *)camera didGetLightSources:(NSArray<CZCameraLightSource *> *)lightSources {
    NSMutableArray<CZLiveChannel *> *channels = [NSMutableArray array];
    
    if ([camera hasCapability:CZCameraCapabilityEncoding]) {
        for (CZCameraLightSource *lightSource in lightSources) {
            if (lightSource.wavelength > 0) {
                CZLightSourceChannel *channel = [[CZLightSourceChannel alloc] initWithCamera:camera index:lightSource.position lightSource:lightSource];
                channel.selected = lightSource.isOn;
                channel.enabled = lightSource.canOn;
                [channels addObject:channel];
            }
        }
        
        if (lightSources.count == 0 && [camera hasLightPath:CZCameraLightPathReflected]) {
            for (NSUInteger index = 0; index < 4; index++) {
                CZVirtualLightSourceChannel *channel = [[CZVirtualLightSourceChannel alloc] initWithCamera:camera index:index];
                [channels addObject:channel];
            }
        }
    } else if ([self isCurrentCameraReachable]) {
        for (NSUInteger index = 0; index < 4; index++) {
            CZVirtualLightSourceChannel *channel = [[CZVirtualLightSourceChannel alloc] initWithCamera:camera index:index];
            [channels addObject:channel];
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskChannelsWillChange:)]) {
        [self.delegate liveTaskChannelsWillChange:self];
    }
    
    self.channels = [channels copy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskChannelsDidChange:)]) {
        [self.delegate liveTaskChannelsDidChange:self];
    }
}

- (void)camera:(CZCamera<CZFluorescenceSnapping> *)camera didUpdateLightSources:(NSArray<CZCameraLightSource *> *)lightSources {
    NSMutableArray<CZCameraLightSource *> *activeLightSources = [NSMutableArray arrayWithCapacity:lightSources.count];
    for (CZCameraLightSource *lightSource in lightSources) {
        if (lightSource.isOn) {
            [activeLightSources addObject:lightSource];
        }
    }
    
    for (CZLightSourceChannel *channel in self.channels) {
        NSInteger index = [lightSources indexOfObject:channel.lightSource];
        if (index == NSNotFound) {
            channel.selected = NO;
            channel.enabled = NO;
        } else {
            channel.selected = lightSources[index].isOn;
            channel.enabled = lightSources[index].canOn;
        }
    }
    
    if ([camera isOnekeyFluorescenceSnappingPaused] && self.pausedSnappingInfo && activeLightSources.count == 1 && activeLightSources.firstObject.wavelength == self.pausedSnappingInfo.lightSource.wavelength) {
        [camera continueOnekeyFluorescenceSnapping];
        if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskDidContinueSnapping)]) {
            [self.delegate liveTaskDidContinueSnapping];
        }
        self.pausedSnappingInfo = nil;
    }
}

#pragma mark - CZMNAParingManagerDelegate

- (void)mnaPairingManager:(CZMNAParingManager *)pairingManager didFindPairedMNA:(CZMNA *)pairedMNA {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (pairedMNA) {
            NSDictionary *modelList = [self.mnaParingManager newPairedModelList];
            CZMicroscopeModel *modelFromPairedMNA = modelList[pairedMNA.macAddress];
            
            [self.microscopeModel updateDataFromMNAModel:modelFromPairedMNA];
            
            [self.microscopeModel saveSlotsToDefaults];
            
            const NSTimeInterval kUpdateObjectiveInterval = 2.0;
            [self.updateObjectiveTimer invalidate];
            self.updateObjectiveTimer = [NSTimer scheduledTimerWithTimeInterval:kUpdateObjectiveInterval
                                                                         target:self
                                                                       selector:@selector(updateObjectiveSettingFromMNA:)
                                                                       userInfo:nil
                                                                        repeats:YES];
            [self updateObjectiveSettingFromMNA:nil];
            [self updateSnapModesFromMNA];
            BOOL enabled = self.mode != kCZCaptureModeMacroPhoto;
            self.objectiveSelectionButtonEnabled = enabled;
            self.zoomSelectionButtonEnabled = enabled;
        } else {
            self.microscopeModel.mnaMACAddress = kNULLMACAddress;
            self.microscopeModel.mnaIPAddress = kNULLIPAddress;
            self.canGetCurrentObjectiveFromMNA = NO;
            self.canGetCurrentZoomFromMNA = NO;
            
            if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
                [self updateZoomSetting];
            } else {
                [self updateObjectiveSetting];
            }
        }
        
        //TODO: Advanced features. Pls check it.
        // According to the requirement, do not change MNA level in Labscope, even with MNA associated.
        if (self.shouldValidateMNALevel &&
            ![[NSUserDefaults standardUserDefaults] boolForKey:kEnableMNALevelSimulator]) {
            NSString *storedMNALevel = [[NSUserDefaults standardUserDefaults] stringForKey:kMNALevel];
            NSString *currenttMNALevel = !pairedMNA.level ? kMNALevelNone : pairedMNA.level;
            if (![currenttMNALevel isEqualToString:storedMNALevel]) {
                [[NSUserDefaults standardUserDefaults] setObject:currenttMNALevel
                                                          forKey:kMNALevel];
                NSString *notificationMessage = L(@"LIVE_MNA_LEVEL_NONE");
                if ([currenttMNALevel isEqualToString:kMNALevelBasic]) {
                    notificationMessage = L(@"LIVE_MNA_LEVEL_BASIC");
                } else if ([currenttMNALevel isEqualToString:kMNALevelAdvanced]) {
                    notificationMessage = L(@"LIVE_MNA_LEVEL_ADVANCED");
                }
                
                [[CZToastManager sharedManager] showToastMessage:notificationMessage sourceRect:CGRectZero];
            }
        }
        
        self.shouldValidateMNALevel = NO;
    });
}

#pragma mark - CZMicroscopeModelUpdateInfoFromMNADelegate

- (void)microscopeModel:(CZMicroscopeModel *)queryModel
          hasUpdateInfo:(CZMicroscopeModel *)newModel
                fromMNA:(CZMNA *)mna {
    return;
}

- (void)microscopeModel:(CZMicroscopeModel *)myModel
   retrieveNewObjective:(NSUInteger)objective {
    if (myModel.type == kPrimotech) {
        self.canGetCurrentObjectiveFromMNA = YES;
    } else if (myModel.type == kStemi508)  {
        self.canGetCurrentZoomFromMNA = YES;
    }
    
    if ([myModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        [self updateZoomSetting];
    } else {
        [self updateObjectiveSetting];
    }
    
    if ((self.canGetCurrentObjectiveFromMNA) || (self.canGetCurrentZoomFromMNA)) {
        [self updateScaling];
    }
}

- (void)microscopeModelFailedToRetrieveObjective:(CZMicroscopeModel *)myModel {
    NSDictionary *livingMNAs = [self.mnaParingManager newMNAList];
    BOOL mnaLost = ([livingMNAs objectForKey:myModel.mnaMACAddress] == nil);
    
    if (mnaLost) {  // mna is lost
        BOOL valueChanged = NO;
        if (self.canGetCurrentZoomFromMNA) {
            self.canGetCurrentZoomFromMNA = NO;
            valueChanged = YES;
        }
        
        if (self.canGetCurrentObjectiveFromMNA) {
            self.canGetCurrentObjectiveFromMNA = NO;
            valueChanged = YES;
        }
        
        if (valueChanged) {
            if (self.state == CZTaskStateForeground) {
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:L(@"ERROR_GET_OBJECTIVE_FROM_MNA") level:CZAlertLevelInfo];
                [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
                [alert presentAnimated:YES completion:nil];
            }
            
            if ([myModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
                [self updateZoomSetting];
            } else {
                [self updateObjectiveSetting];
            }
        }
    }
}

#pragma mark - CZDocSaveDelegate
- (void)performOverwriteWithSourceFiles:(NSArray *)sourceFiles
                       destinationFiles:(NSArray *)destinationFiles
                      overwirteYesBlock:(void (^)(NSDictionary *))overwirteYesBlock
                       overwirteNoBlock:(void (^)(NSDictionary *))overwirteNoBlock
                    forceOverwriteBlock:(void (^)(void))forceOverwriteBlock {
    CZFileOverwriteController *fileOverwriteController = [[CZFileOverwriteController alloc] initWithSource:sourceFiles
                                                                                               destination:destinationFiles];
    fileOverwriteController.overwriteYesBlock = overwirteYesBlock;
    fileOverwriteController.overwriteNoBlock = overwirteNoBlock;
    BOOL forceOverwrite = ([[CZDefaultSettings sharedInstance] shortcutMode] == kCZShortcutModePCBMeasurement);
    if (![fileOverwriteController checkOverwriteFiles:forceOverwrite]) {
        forceOverwriteBlock();
    }
}

- (CZIFileSaveFormat)saveFormatForSnappedImage {
#if DCM_REFACTORED
    if (CZDCMClassWatcher.sharedInstance.takingClass) {
        return [self saveFormatFromDCMTemplateSettings];
    } else {
#endif
        return [[CZDefaultSettings sharedInstance] fileFormat];
#if DCM_REFACTORED
    }
#endif
}

- (CZIFileSaveFormat)saveFormatFromDCMTemplateSettings {
#if DCM_REFACTORED
    CZCMTemplateNode* rootNode = [CZCMTemplateTools templateControlRootNode];
    NSDictionary *templateConfiguration = [CZCMLocalStorage shareInstance].configurationTemplate;
    if (CZUnit(templateConfiguration)) {
        rootNode = [CZCMTemplateTools buildTemplateControlRootNodeWithData:templateConfiguration];
    }
#endif
    
    CZIFileSaveFormat format = kCZI;
#if DCM_REFACTORED
    if (rootNode) {
        CZCMTemplateNode* saveFormatNode = [CZCMTemplateTools nodeForFeatureId:kDefaultFileFormat node:rootNode];
        format = [self parseSaveFormatFrom:saveFormatNode.value];
    }
#endif
    return format;
}

#pragma mark - CZDocManagrerDelegate
- (void)docManager:(CZDocManager *)docManager didSaveFiles:(NSArray *)files success:(BOOL)success {
    CZLogv(@"docManager: %@, didSaveFiles: %@, success: %d", docManager, files, success);
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskDidSaveDocument)]) {
        [self.delegate liveTaskDidSaveDocument];
    }
}

- (void)docManager:(CZDocManager *)docManager willSaveFiles:(NSArray *)files {
    CZLogv(@"docManager: %@, willSaveFiles: %@", docManager, files);
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskWillSaveDocument)]) {
        [self.delegate liveTaskWillSaveDocument];
    }
}

#pragma mark - CZFileNameInputAlertViewControllerDelegate

- (void)fileNameInputAlertDidPressOK:(CZFileNameInputAlertController *)alert {
    NSString *newFileName = [alert uniqueName];
    CZDocManager *docManager = alert.docManager;
#if DCM_REFACTORED
    NSString *newFilePath = [[CZLocalFileList sharedInstance].documentPath stringByAppendingPathComponent:newFileName];
#else
    NSString *newFilePath = [[NSFileManager defaultManager].cz_documentPath stringByAppendingPathComponent:newFileName];
#endif
    docManager.fileNameGenerator = alert.generator;
    docManager.filePath = newFilePath;
    [docManager scheduleSaveInDefaultFormatWithDelegate:self];
    self.snappedImageFilePath = newFilePath;
}

- (void)fileNameInputAlertDidPressDiscard:(CZFileNameInputAlertController *)alert {
}

#pragma mark - Setters

- (void)setObjectiveSelectionButtonHidden:(BOOL)objectiveSelectionButtonHidden {
    _objectiveSelectionButtonHidden = objectiveSelectionButtonHidden;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskObjectiveSelectionButtonHiddenDidChange:)]) {
        [self.delegate liveTaskObjectiveSelectionButtonHiddenDidChange:self];
    }
}

- (void)setObjectiveSelectionButtonEnabled:(BOOL)objectiveSelectionButtonEnabled {
    _objectiveSelectionButtonEnabled = objectiveSelectionButtonEnabled;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskObjectiveSelectionButtonEnabledDidChange:)]) {
        [self.delegate liveTaskObjectiveSelectionButtonEnabledDidChange:self];
    }
}

- (void)setObjectiveSelectionButtonTitle:(NSString *)objectiveSelectionButtonTitle {
    _objectiveSelectionButtonTitle = objectiveSelectionButtonTitle;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskObjectiveSelectionButtonTitleDidChange:)]) {
        [self.delegate liveTaskObjectiveSelectionButtonTitleDidChange:self];
    }
}

- (void)setObjectiveSelectionButtonImage:(UIImage *)objectiveSelectionButtonImage {
    _objectiveSelectionButtonImage = objectiveSelectionButtonImage;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskObjectiveSelectionButtonImageDidChange:)]) {
        [self.delegate liveTaskObjectiveSelectionButtonImageDidChange:self];
    }
}

- (void)setZoomSelectionButtonHidden:(BOOL)zoomSelectionButtonHidden {
    _zoomSelectionButtonHidden = zoomSelectionButtonHidden;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskZoomSelectionButtonHiddenDidChange:)]) {
        [self.delegate liveTaskZoomSelectionButtonHiddenDidChange:self];
    }
}

- (void)setZoomSelectionButtonEnabled:(BOOL)zoomSelectionButtonEnabled {
    _zoomSelectionButtonEnabled = zoomSelectionButtonEnabled;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskZoomSelectionButtonEnabledDidChange:)]) {
        [self.delegate liveTaskZoomSelectionButtonEnabledDidChange:self];
    }
}

- (void)setZoomSelectionButtonTitle:(NSString *)zoomSelectionButtonTitle {
    _zoomSelectionButtonTitle = zoomSelectionButtonTitle;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskZoomSelectionButtonTitleDidChange:)]) {
        [self.delegate liveTaskZoomSelectionButtonTitleDidChange:self];
    }
}

- (void)setZoomSelectionButtonImage:(UIImage *)zoomSelectionButtonImage {
    _zoomSelectionButtonImage = zoomSelectionButtonImage;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskZoomSelectionButtonImageDidChange:)]) {
        [self.delegate liveTaskZoomSelectionButtonImageDidChange:self];
    }
}

- (void)setFilterSetSelectionButtonHidden:(BOOL)filterSetSelectionButtonHidden {
    _filterSetSelectionButtonHidden = filterSetSelectionButtonHidden;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskFilterSetSelectionButtonHiddenDidChange:)]) {
        [self.delegate liveTaskFilterSetSelectionButtonHiddenDidChange:self];
    }
}

- (void)setFilterSetSelectionButtonEnabled:(BOOL)filterSetSelectionButtonEnabled {
    _filterSetSelectionButtonEnabled = filterSetSelectionButtonEnabled;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskFilterSetSelectionButtonEnabledDidChange:)]) {
        [self.delegate liveTaskFilterSetSelectionButtonEnabledDidChange:self];
    }
}

- (void)setFilterSetSelectionButtonTitle:(NSString *)filterSetSelectionButtonTitle {
    _filterSetSelectionButtonTitle = filterSetSelectionButtonTitle;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskFilterSetSelectionButtonTitleDidChange:)]) {
        [self.delegate liveTaskFilterSetSelectionButtonTitleDidChange:self];
    }
}

- (void)setFilterSetSelectionButtonImage:(UIImage *)filterSetSelectionButtonImage {
    _filterSetSelectionButtonImage = filterSetSelectionButtonImage;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveTaskFilterSetSelectionButtonImageDidChange:)]) {
        [self.delegate liveTaskFilterSetSelectionButtonImageDidChange:self];
    }
}

@end
