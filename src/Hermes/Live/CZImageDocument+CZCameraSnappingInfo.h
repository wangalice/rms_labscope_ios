//
//  CZImageDocument+CZCameraSnappingInfo.h
//  Hermes
//
//  Created by Li, Junlin on 6/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZDocumentKit/CZDocumentKit.h>

@class CZCameraSnappingInfo;
@class CZChannel;
@class CZLiveChannel;

@interface CZImageDocument (CZCameraSnappingInfo)

- (instancetype)initFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo;
- (instancetype)initFromChannels:(NSArray<CZLiveChannel *> *)channels;

@end

@interface CZImageBlock (CZCameraSnappingInfo)

+ (instancetype)imageBlockFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo;
+ (instancetype)imageBlockFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo channel:(CZChannel *)channel;

@end
