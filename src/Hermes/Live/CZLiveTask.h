//
//  CZLiveTask.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"
#import "CZVideoTool.h"
#import "CZLightSourceChannel.h"
#import "CZVirtualLightSourceChannel.h"
#import <CZCameraInterface/CZCameraInterface.h>

#undef HAS_CAP_MODE_FL

typedef NS_ENUM(NSUInteger, CZCaptureMode) {
    kCZCaptureModePhoto = 0, // simple snap mode
    kCZCaptureModeSnapFromStream, // Fast snap mode
    kCZCaptureModeSnapDenoise,  // high quaility mode
    kCZCaptureModeVideoStandby, // video record mode
    kCZCaptureModeVideoRecording, //video recording mode
    kCZCaptureModeTimelapseVideoStandby, // timelapse video record mode
    kCZCaptureModeTimelapseVideoRecording, // timelapse video recording mode
    kCZCaptureModeRemoteStreamStandby,
    kCZCaptureModeRemoteStream NS_ENUM_DEPRECATED_IOS(2_0, 9_0, "Not use here according previous logic"),
    kCZCaptureModeMacroPhoto, // macro snap mode
    kCZCaptureModeRemoteStreamReceiverSnap NS_ENUM_DEPRECATED_IOS(2_0, 9_0, "Not use here according previous logic"),
    kCZCaptureModeEDOF, //EDF snap mode
    kCZCaptureModeAdvancedBegin = kCZCaptureModeEDOF,
    
    kCZCaptureModeManualFluorescence,
    kCZCaptureModeOnekeyFluorescenceStandby,
    kCZCaptureModeOnekeyFluorescencePlaying
};

@class CZLiveTask, CZCamera, CZCameraSnappingInfo, CZMicroscopeModel, CZVideoDenoiser, CZImageDocument, CZLiveViewModel;
@protocol CZCameraDelegate;

@protocol CZLiveTaskDelegate <CZTaskDelegate>

@optional

- (void)liveTaskDidConfigurateCamera:(CZCamera *)camera;

- (void)camera:(CZCamera *)camera connectingStatusChanged:(BOOL)status;

- (void)liveTaskConnectedCamera:(CZCamera *)camera statusChanged:(BOOL)status;
- (void)liveTaskDidLostCamera:(CZCamera *)camera;
- (void)liveTaskDidGetVideoFrame:(UIImage *)frame;

- (void)liveTaskDidPauseSnapping:(CZCameraSnappingInfo *)snappingInfo reason:(CZCameraSnappingPauseReason)reason;
- (void)liveTaskDidContinueSnapping;
- (void)liveTaskDidFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error;
- (void)liveTaskDidBeginRecordingWithFileName:(NSString *)fileName;
- (void)liveTaskDidFinishRecording;

- (void)liveTaskWillSaveDocument;
- (void)liveTaskDidSaveDocument;

- (void)liveTaskChannelsWillChange:(CZLiveTask *)task;
- (void)liveTaskChannelsDidChange:(CZLiveTask *)task;

- (void)liveTaskGeminiCameraDidReceiveHardwareEvent:(CZLiveTask *)task availableEventType:(CZGeminiCameraEventStandType)type;

- (void)liveTaskObjectiveSelectionButtonHiddenDidChange:(CZLiveTask *)task;
- (void)liveTaskObjectiveSelectionButtonEnabledDidChange:(CZLiveTask *)task;
- (void)liveTaskObjectiveSelectionButtonTitleDidChange:(CZLiveTask *)task;
- (void)liveTaskObjectiveSelectionButtonImageDidChange:(CZLiveTask *)task;

- (void)liveTaskZoomSelectionButtonHiddenDidChange:(CZLiveTask *)task;
- (void)liveTaskZoomSelectionButtonEnabledDidChange:(CZLiveTask *)task;
- (void)liveTaskZoomSelectionButtonTitleDidChange:(CZLiveTask *)task;
- (void)liveTaskZoomSelectionButtonImageDidChange:(CZLiveTask *)task;

- (void)liveTaskFilterSetSelectionButtonHiddenDidChange:(CZLiveTask *)task;
- (void)liveTaskFilterSetSelectionButtonEnabledDidChange:(CZLiveTask *)task;
- (void)liveTaskFilterSetSelectionButtonTitleDidChange:(CZLiveTask *)task;
- (void)liveTaskFilterSetSelectionButtonImageDidChange:(CZLiveTask *)task;

@end

@interface CZLiveTask : CZTask <CZCameraDelegate, CZFluorescenceSnappingDelegate>

@property (nonatomic, readonly, weak) id <CZLiveTaskDelegate>delegate;

@property (nonatomic, readonly, strong) CZVideoTool *videoTool;

@property (nonatomic, readonly, strong) CZCamera *currentCamera;

@property (nonatomic, readonly, strong) CZLiveViewModel *viewModel;

@property (nonatomic, readonly, copy) NSArray<CZLiveChannel *> *channels;
@property (nonatomic, readonly, copy) NSArray<CZLiveChannel *> *enabledChannels;
@property (nonatomic, readonly, copy) NSArray<CZLiveChannel *> *selectedChannels;
@property (nonatomic, readonly, copy) NSArray<CZLiveChannel *> *snappedChannels;
- (void)resetChannels;
- (void)saveMultichannelImage;
- (BOOL)canDisplayOnekeyFluorescenceSnapMode;
- (BOOL)isFluorescenceSnapButtonEnabledForMode:(CZCaptureMode)mode;

@property (nonatomic, readonly, assign) BOOL objectiveSelectionButtonHidden;
@property (nonatomic, readonly, assign) BOOL objectiveSelectionButtonEnabled;
@property (nonatomic, readonly, copy) NSString *objectiveSelectionButtonTitle;
@property (nonatomic, readonly, strong) UIImage *objectiveSelectionButtonImage;

@property (nonatomic, readonly, assign) BOOL zoomSelectionButtonHidden;
@property (nonatomic, readonly, assign) BOOL zoomSelectionButtonEnabled;
@property (nonatomic, readonly, copy) NSString *zoomSelectionButtonTitle;
@property (nonatomic, readonly, strong) UIImage *zoomSelectionButtonImage;

@property (nonatomic, readonly, assign) BOOL filterSetSelectionButtonHidden;
@property (nonatomic, readonly, assign) BOOL filterSetSelectionButtonEnabled;
@property (nonatomic, readonly, copy) NSString *filterSetSelectionButtonTitle;
@property (nonatomic, readonly, strong) UIImage *filterSetSelectionButtonImage;

@property (nonatomic, readonly) CZCaptureMode defaultSnappingMode;

@property (nonatomic, readonly) CZVideoDenoiser *denoiser;

@property (nonatomic, assign) BOOL isRemoteCamera;

@property (nonatomic, strong, readonly) CZMicroscopeModel *microscopeModel;

@property (nonatomic, strong, readonly) UIImage *firstRecordingFrame;

- (void)setCurrentCamera:(CZCamera *)camera;
- (void)setCurrentCamera:(CZCamera *)camera resetExclusiveLock:(BOOL)resetExclusiveLock;

- (void)updateMode:(CZCaptureMode)mode;

- (NSArray <NSNumber *> *)allSnapModes;
- (NSArray <NSNumber *> *)availableModes;

// camera status

@property (nonatomic, assign, readonly) BOOL isReconnecting;
@property (nonatomic, assign, readonly) BOOL lostFrame;

@property (nonatomic, readonly, assign) BOOL canGetCurrentZoomFromMNA;

- (BOOL)isCurrentCameraInvalid;

- (BOOL)isCurrentCameraReachable;

- (void)updateDefaultSnappingMode:(CZCaptureMode)snappingMode;

// Camera storage
- (void)presentNewDocManagerWithDocument:(CZImageDocument *)document showsAnnotation:(BOOL)showsAnnotation;

@property (nonatomic, strong) CZImageDocument *snappedImageDocument;
@property (nonatomic, copy) NSString *snappedImageFilePath;
@property (nonatomic, copy) NSString *recordedVideoFile;

// Vedio denoiser
- (BOOL)isDenoisingEnableFor:(CZCamera *)camera mode:(CZCaptureMode)mode;
- (void)updateDenoiser:(CZVideoDenoiser *)desnoiser;

// Recording
- (void)beginRecordingWithFrameRate:(NSUInteger)frameRate isNormalRecording:(BOOL)isNormalRecording;
- (void)stopRecording:(BOOL)isNormalRecording;
- (BOOL)isScaleBarOverlappedTimeStamp;

// Fluorescence
- (void)enterFluorescenceSnappingMode:(CZCaptureMode)snapMode completionHandler:(void (^)(BOOL canEnterFluorescenceSnappingMode))completionHandler;
- (void)switchToFluorescenceSnappingMode:(CZCaptureMode)snapMode;
- (void)exitFluorescenceSnappingMode;
- (void)beginOrContinueManualFluorescenceSnapping;
- (void)canBeginOnekeyFluorescenceSnapping:(void (^)(BOOL canBeginOnekeyFluorescenceSnapping))completionHandler;
- (void)beginOnekeyFluorescenceSnapping;
- (void)cancelOnekeyFluorescenceSnapping;
- (void)endFluorescenceSnappingForMode:(CZCaptureMode)mode action:(CZFluorescenceSnappingEndAction)action;

// Annotation
- (void)updateElementLayerLogicalScaling:(CGFloat)ratio;
- (void)updateScaling;
- (BOOL)canUndo;
- (BOOL)canRedo;
- (void)undoAction;
- (void)redoAction;

// Selection
- (void)selectObjectiveAtPosition:(NSUInteger)position;
- (void)selectZoomClickStopAtPosition:(NSUInteger)position;
- (void)updateZoomRelatedControls;

@end
