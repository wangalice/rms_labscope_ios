//
//  CZImageDocument+CZCameraSnappingInfo.m
//  Hermes
//
//  Created by Li, Junlin on 6/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageDocument+CZCameraSnappingInfo.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZChannel.h"
#import "CZLiveChannel.h"

@implementation CZImageDocument (CZCameraSnappingInfo)

- (instancetype)initFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    CZImageBlock *imageBlock = [CZImageBlock imageBlockFromSnappingInfo:snappingInfo];
    return [self initWithImageBlocks:@[imageBlock]];
}

- (instancetype)initFromChannels:(NSArray<CZLiveChannel *> *)channels {
    NSMutableArray<CZImageBlock *> *imageBlocks = [NSMutableArray array];
    for (CZLiveChannel *channel in channels) {
        if (channel.color && channel.snappingInfo) {
            CZImageBlock *imageBlock = [CZImageBlock imageBlockFromSnappingInfo:channel.snappingInfo channel:channel];
            [imageBlocks addObject:imageBlock];
        }
    }
    return [self initWithImageBlocks:imageBlocks];
}

@end

@implementation CZImageBlock (CZCameraSnappingInfo)

+ (instancetype)imageBlockFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    return [self imageBlockFromSnappingInfo:snappingInfo channel:nil];
}

+ (instancetype)imageBlockFromSnappingInfo:(CZCameraSnappingInfo *)snappingInfo channel:(CZChannel *)channel {
    NSString *lightSource = nil;
    switch (snappingInfo.lightSourceType) {
        case CZCameraLightSourceTypeColibri3:
            lightSource = @"Colibri 3";
            break;
        case CZCameraLightSourceTypeFL_LED:
            lightSource = @"FL LED Lamp";
            break;
        case CZCameraLightSourceTypeRL_LED:
            lightSource = @"RL LED Lamp";
            break;
        case CZCameraLightSourceTypeTL_LED:
            lightSource = @"TL LED Lamp";
            break;
        case CZCameraLightSourceTypeRL_Halogen:
            lightSource = @"RL Halogen Lamp";
            break;
        case CZCameraLightSourceTypeTL_Halogen:
            lightSource = @"TL Halogen Lamp";
            break;
        case CZCameraLightSourceTypeRL_External:
            lightSource = @"RL External Lamp";
            break;
        case CZCameraLightSourceTypeTL_External:
            lightSource = @"TL External Lamp";
            break;
        default:
            break;
    }
    
    NSNumber *exposureTimeInSeconds = nil;
    if (snappingInfo.exposureTimeInMilliseconds) {
        exposureTimeInSeconds = @(snappingInfo.exposureTimeInMilliseconds.floatValue * 1e-3f);
    }
    
    CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] initWithGrayscale:@(snappingInfo.grayscale)
                                                                                          channelName:channel.name
                                                                                         channelColor:channel.color.value
                                                                                      channelSelected:channel ? @YES : nil
                                                                                          filterSetID:snappingInfo.filterSetID
                                                                                        filterSetName:[CZReflector filterSetNameForMatID:snappingInfo.filterSetID]
                                                                                          lightSource:lightSource
                                                                                           wavelength:@(snappingInfo.lightSource.wavelength).stringValue
                                                                                exposureTimeInSeconds:exposureTimeInSeconds
                                                                                                 gain:snappingInfo.gain];
    return [[self alloc] initWithImage:snappingInfo.image channelProperties:channelProperties];
}

@end
