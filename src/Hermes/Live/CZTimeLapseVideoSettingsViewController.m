//
//  CZTimeLapseVideoSettingsViewController.m
//  Matscope
//
//  Created by Sherry Xu on 12/19/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZTimeLapseVideoSettingsViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDialogPresentationController.h"

static const CGFloat kDefaultLabelHeight = 21;
static const NSUInteger kMaxTimeInterval = 100; //seconds
static const NSUInteger kMinTimeIntervel = 1;
static const CGFloat kDefaultHorizontalMargin = 32;
static const CGFloat kDefaultHorizontalControlsMargin = 16;
static const CGFloat kDefaultVerticalControlsMargin = 8;

@interface CZTimelapseVideoSettingsViewController () <
CZTextFieldDelegate,
UIViewControllerTransitioningDelegate
>
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *recordRateLabel;
@property (nonatomic, strong) UILabel *playBackRateLabel;
@property (nonatomic, strong) CZTextField *rangeTextField;
@property (nonatomic, strong) UILabel *exampleLabel;
@property (nonatomic, strong) UILabel *warningLabel;
@property (nonatomic, strong) UILabel *noticeLabel;
@property (nonatomic, strong) CZRadioButton *playBackRateLowButton;
@property (nonatomic, strong) CZRadioButton *playBackRateMediumButton;
@property (nonatomic, strong) CZRadioButton *playBackRateHighButton;
@property (nonatomic, strong) UIButton *selectedPlayBackRateButton;

@end

@implementation CZTimelapseVideoSettingsViewController

+ (NSUInteger)playRateValueForLevel:(CZTimelapseVideoPlayRate)playRate {
    NSUInteger playRateValue;
    switch (playRate) {
        case kCZTimelapseVideoPlayRateHigh:
            playRateValue = 30;
            break;
            
        case kCZTimelapseVideoPlayRateLow:
            playRateValue = 5;
            break;
            
        case kCZTimelapseVideoPlayRateMedium:
        default:
            playRateValue = 15;
            break;
    }
    return playRateValue;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.contentView.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs10], [UIColor cz_gs120]);
    
    const CGFloat kPlayBackRadioButtonWidth = 143.0f;
    const CGFloat kPlayBackRadioButtonHeight = 32.0f;
    const CGFloat kRangeTextFieldWidth = 80.0f;
    const CGFloat kRangeTextFieldHeight = 32.0f;

    [self.contentView addSubview:self.headerView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.recordRateLabel];
    [self.contentView addSubview:self.rangeTextField];
    [self.contentView addSubview:self.playBackRateLabel];
    [self.contentView addSubview:self.playBackRateLowButton];
    [self.contentView addSubview:self.playBackRateMediumButton];
    [self.contentView addSubview:self.playBackRateHighButton];
    [self.contentView addSubview:self.exampleLabel];
    [self.contentView addSubview:self.warningLabel];
    [self.contentView addSubview:self.noticeLabel];

    [self.contentView.widthAnchor constraintEqualToConstant:448.0f].active = YES;
    [self.contentView.heightAnchor constraintGreaterThanOrEqualToConstant:349.0f].active = YES;
    
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.recordRateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.rangeTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.playBackRateLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.playBackRateLowButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.playBackRateMediumButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.playBackRateHighButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.exampleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.warningLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.noticeLabel.translatesAutoresizingMaskIntoConstraints = NO;

    [self.headerView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
    [self.headerView.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = YES;
    [self.headerView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
    [self.headerView.heightAnchor constraintEqualToConstant:kDefaultVerticalControlsMargin].active = YES;
    
    [self.titleLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:kDefaultHorizontalMargin].active = YES;
    [self.titleLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-kDefaultHorizontalMargin].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.headerView.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.titleLabel.heightAnchor constraintEqualToConstant:kDefaultLabelHeight].active = YES;
    
    [self.recordRateLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.recordRateLabel.rightAnchor constraintEqualToAnchor:self.rangeTextField.leftAnchor constant: -kDefaultHorizontalControlsMargin].active = YES;
    [self.recordRateLabel.topAnchor constraintEqualToAnchor:self.titleLabel.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.recordRateLabel.heightAnchor constraintEqualToConstant:kDefaultLabelHeight].active = YES;

    [self.rangeTextField.topAnchor constraintEqualToAnchor:self.recordRateLabel.topAnchor].active = YES;
    [self.rangeTextField.widthAnchor constraintEqualToConstant:kRangeTextFieldWidth].active = YES;
    [self.rangeTextField.heightAnchor constraintEqualToConstant:kRangeTextFieldHeight].active = YES;
    
    [self.playBackRateLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.playBackRateLabel.rightAnchor constraintEqualToAnchor:self.recordRateLabel.rightAnchor].active = YES;
    [self.playBackRateLabel.centerYAnchor constraintEqualToAnchor:self.playBackRateLowButton.centerYAnchor].active = YES;
    [self.playBackRateLabel.heightAnchor constraintEqualToConstant:kDefaultLabelHeight].active = YES;
    
    [self.playBackRateLowButton.leftAnchor constraintEqualToAnchor:self.rangeTextField.leftAnchor].active = YES;
    [self.playBackRateLowButton.topAnchor constraintEqualToAnchor:self.rangeTextField.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.playBackRateLowButton.widthAnchor constraintEqualToConstant:kPlayBackRadioButtonWidth].active = YES;
    [self.playBackRateLowButton.heightAnchor constraintEqualToConstant:kPlayBackRadioButtonHeight].active = YES;

    [self.playBackRateMediumButton.leftAnchor constraintEqualToAnchor:self.playBackRateLowButton.leftAnchor].active = YES;
    [self.playBackRateMediumButton.topAnchor constraintEqualToAnchor:self.playBackRateLowButton.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.playBackRateMediumButton.widthAnchor constraintEqualToConstant:kPlayBackRadioButtonWidth].active = YES;
    [self.playBackRateMediumButton.heightAnchor constraintEqualToConstant:kPlayBackRadioButtonHeight].active = YES;

    [self.playBackRateHighButton.leftAnchor constraintEqualToAnchor:self.playBackRateMediumButton.leftAnchor].active = YES;
    [self.playBackRateHighButton.topAnchor constraintEqualToAnchor:self.playBackRateMediumButton.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.playBackRateHighButton.widthAnchor constraintEqualToConstant:kPlayBackRadioButtonWidth].active = YES;
    [self.playBackRateHighButton.heightAnchor constraintEqualToConstant:kPlayBackRadioButtonHeight].active = YES;

    [self.exampleLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.exampleLabel.rightAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor].active = YES;
    [self.exampleLabel.topAnchor constraintEqualToAnchor:self.playBackRateHighButton.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;

    [self.warningLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.warningLabel.rightAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor].active = YES;
    [self.warningLabel.topAnchor constraintEqualToAnchor:self.exampleLabel.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;

    [self.noticeLabel.leftAnchor constraintEqualToAnchor:self.titleLabel.leftAnchor].active = YES;
    [self.noticeLabel.rightAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor].active = YES;
    [self.noticeLabel.topAnchor constraintEqualToAnchor:self.warningLabel.bottomAnchor constant:kDefaultVerticalControlsMargin * 2].active = YES;
    [self.noticeLabel.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-20.0f].active = YES;
    
    CZTimelapseVideoPlayRate rate = [[CZDefaultSettings sharedInstance] videoPlayRate];
    
    [self updatePlayBackButtonsWithRate:rate];
    [self updateTimeOfExampleLabel];
}

- (void)dealloc {
    CZLogv(@"Works well");
}

#pragma mark - Private methods

- (void)updatePlayBackButtonsWithRate:(CZTimelapseVideoPlayRate)rate {
    switch (rate) {
        case kCZTimelapseVideoPlayRateLow: {
            self.playBackRateLowButton.selected = YES;
            self.selectedPlayBackRateButton = _playBackRateLowButton;
        }
            break;
        case kCZTimelapseVideoPlayRateMedium: {
            self.playBackRateMediumButton.selected = YES;
            self.selectedPlayBackRateButton = _playBackRateMediumButton;
        }
            break;
        case kCZTimelapseVideoPlayRateHigh: {
            self.playBackRateHighButton.selected = YES;
            self.selectedPlayBackRateButton = _playBackRateHighButton;
        }
            break;
    }
}

- (void)updateTimeOfExampleLabel {
    CZTimelapseVideoPlayRate rate = [[CZDefaultSettings sharedInstance] videoPlayRate];
    double playRateValue = [CZTimelapseVideoSettingsViewController playRateValueForLevel:rate];
    double rangValue = 1.0;
    double totalPlayValue = 0.0;
    
    NSString *playRateValueString = @"?";
    if (self.rangeTextField.text) {
        rangValue = [[CZCommonUtils numberFromLocalizedString:self.rangeTextField.text] doubleValue];
        totalPlayValue = 3600.0 / (rangValue * playRateValue);
        playRateValueString = [CZCommonUtils localizedStringFromFloat:totalPlayValue
                                                            precision:1];
    }
    playRateValueString = [NSString stringWithFormat:L(@"TIMELAPSE_VIDEO_EXAMPLE_LABEL"), playRateValueString];
    
    self.exampleLabel.text = playRateValueString;
}

#pragma mark - Action

- (void)rangeTextFieldEditingDidEndAction:(id)sender {
    if (self.rangeTextField.text) {
        double rangeValue = [[CZCommonUtils numberFromLocalizedString:self.rangeTextField.text] doubleValue];
        NSUInteger value = (NSUInteger)round(rangeValue);
        
        if (value > kMaxTimeInterval) {
            value = kMaxTimeInterval;
        } else if (value < kMinTimeIntervel) {
            value = kMinTimeIntervel;
        }
        
        [[CZDefaultSettings sharedInstance] setTimelapseIntervel:value];
        
        NSString *range = [NSString stringWithFormat:@"%lu",(unsigned long)value];
        self.rangeTextField.text = range;
    }
    
    [self updateTimeOfExampleLabel];
}

- (void)radioButtonSelectAction:(CZRadioButton *)sender {
    if (sender == _selectedPlayBackRateButton) return;

    _selectedPlayBackRateButton.selected = NO;
    _selectedPlayBackRateButton = sender;
    _selectedPlayBackRateButton.selected = YES;
    
    [self updateTimeOfExampleLabel];
    
    CZTimelapseVideoPlayRate playRate = sender.tag;
    [[CZDefaultSettings sharedInstance] setVideoPlayRate:playRate];
    [self updateTimeOfExampleLabel];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    if (textField == self.rangeTextField) {
        return [CZCommonUtils numericTextField:textField shouldAcceptChange:textEntered];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.rangeTextField) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    CZDialogPresentationController *presentationController = (CZDialogPresentationController *)[super presentationControllerForPresentedViewController:presented presentingViewController:presenting sourceViewController:source];
    presentationController.adjustsDialogPositionWhenKeyboardAppears = NO;
    return presentationController;
}

#pragma mark - Lazy-load objects

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] init];
        _headerView.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs85], [UIColor cz_gs85]);
    }
    return _headerView;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs120], [UIColor cz_gs80]);
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.text = L(@"TIMELAPSE_VIDEO_SETTINGS_TITLE");
    }
    return _titleLabel;
}

- (UILabel *)recordRateLabel {
    if (_recordRateLabel == nil) {
        _recordRateLabel = [[UILabel alloc] init];
        _recordRateLabel.font = [UIFont cz_label1];
        _recordRateLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs120]);
        _recordRateLabel.text = L(@"TIMELAPSE_VIDEO_RECORDING_RATE_LABEL");
    }
    return _recordRateLabel;
}

- (CZTextField *)rangeTextField {
    if (_rangeTextField == nil) {
        _rangeTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:@"s" rightImagePicker:NULL];
        NSTimeInterval timelapseInterval = [[CZDefaultSettings sharedInstance] timelapseIntervel];
        _rangeTextField.text = [NSString stringWithFormat:@"%d", (int)round(timelapseInterval)];
        _rangeTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _rangeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _rangeTextField.rightViewMode = UITextFieldViewModeAlways;
        _rangeTextField.returnKeyType = UIReturnKeyDone;
        _rangeTextField.placeholder = L(@"TIMELAPSE_VIDEO_TIME_RANGE_HINT");
        _rangeTextField.delegate = self;
        _rangeTextField.font = [UIFont cz_body1];
        [_rangeTextField addTarget:self action:@selector(rangeTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _rangeTextField;
}

- (UILabel *)playBackRateLabel {
    if (_playBackRateLabel == nil) {
        _playBackRateLabel = [[UILabel alloc] init];
        _playBackRateLabel.font = [UIFont cz_label1];
        _playBackRateLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _playBackRateLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs120]);
        _playBackRateLabel.text = L(@"TIMELAPSE_VIDEO_PLAYBACK_LABEL");
    }
    return _playBackRateLabel;
}

- (CZRadioButton *)playBackRateLowButton {
    if (_playBackRateLowButton == nil) {
        _playBackRateLowButton = [CZRadioButton buttonWithType:UIButtonTypeCustom];
        [_playBackRateLowButton setTitle:L(@"TIMELAPSE_VIDEO_PLAYBACK_RATE_LOW") forState:UIControlStateNormal];
        _playBackRateLowButton.titleLabel.font = [UIFont cz_body1];
        [_playBackRateLowButton addTarget:self action:@selector(radioButtonSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        _playBackRateLowButton.tag = kCZTimelapseVideoPlayRateLow;
    }
    return _playBackRateLowButton;
}

- (CZRadioButton *)playBackRateMediumButton {
    if (_playBackRateMediumButton == nil) {
        _playBackRateMediumButton = [CZRadioButton buttonWithType:UIButtonTypeCustom];
        [_playBackRateMediumButton setTitle:L(@"TIMELAPSE_VIDEO_PLAYBACK_RATE_MEDIUM") forState:UIControlStateNormal];
        _playBackRateMediumButton.titleLabel.font = [UIFont cz_body1];
        [_playBackRateMediumButton addTarget:self action:@selector(radioButtonSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        _playBackRateMediumButton.tag = kCZTimelapseVideoPlayRateMedium;
    }
    return _playBackRateMediumButton;
}

- (CZRadioButton *)playBackRateHighButton {
    if (_playBackRateHighButton == nil) {
        _playBackRateHighButton = [CZRadioButton buttonWithType:UIButtonTypeCustom];
        [_playBackRateHighButton setTitle:L(@"TIMELAPSE_VIDEO_PLAYBACK_RATE_HIGH") forState:UIControlStateNormal];
        _playBackRateHighButton.titleLabel.font = [UIFont cz_body1];
        [_playBackRateHighButton addTarget:self action:@selector(radioButtonSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        _playBackRateHighButton.tag = kCZTimelapseVideoPlayRateHigh;
    }
    return _playBackRateHighButton;
}

- (UILabel *)exampleLabel {
    if (_exampleLabel == nil) {
        _exampleLabel = [[UILabel alloc] init];
        _exampleLabel.backgroundColor = [UIColor clearColor];
        _exampleLabel.font = [UIFont cz_caption];
        _exampleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _exampleLabel.numberOfLines = 0;
        NSString *string = [NSString stringWithFormat:L(@"TIMELAPSE_VIDEO_EXAMPLE_LABEL"), @"?"];
        _exampleLabel.text = string;
        _exampleLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs120]);
    }
    return _exampleLabel;
}

- (UILabel *)warningLabel {
    if (_warningLabel == nil) {
        _warningLabel = [[UILabel alloc] init];
        _warningLabel.backgroundColor = [UIColor clearColor];
        _warningLabel.font = [UIFont cz_caption];
        _warningLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _warningLabel.numberOfLines = 0;
        _warningLabel.text = L(@"TIMELAPSE_VIDEO_WARNING_LABEL");
        _warningLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs120]);
    }
    return _warningLabel;
}

- (UILabel *)noticeLabel {
    if (_noticeLabel == nil) {
        _noticeLabel = [[UILabel alloc] init];
        _noticeLabel.backgroundColor = [UIColor clearColor];
        _noticeLabel.font = [UIFont cz_caption];
        _noticeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _noticeLabel.numberOfLines = 0;
        _noticeLabel.text = L(@"TIMELAPSE_VIDEO_WARNING_LABEL1");
        _noticeLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs120]);
    }
    return _noticeLabel;
}

@end
