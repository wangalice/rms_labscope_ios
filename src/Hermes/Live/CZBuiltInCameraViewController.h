//
//  CZBuiltInCameraViewController.h
//  Labscope
//
//  Created by Ralph Jin on 11/17/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CZCameraDelegate;
@protocol CZBuiltInCameraViewControllerTapDelegate;

typedef NS_ENUM(NSUInteger, CZBuiltInCameraViewMode) {
    CZBuiltInCameraViewModeDrawingTube,
    CZBuiltInCameraViewModeMacro
};

@interface CZBuiltInCameraViewController : UIViewController

@property (nonatomic, assign) id <CZCameraDelegate> cameraDelegate;
@property (nonatomic, assign) id <CZBuiltInCameraViewControllerTapDelegate> delegate;
@property (nonatomic, assign) CZBuiltInCameraViewMode mode;

@property (nonatomic, retain, readonly) UISlider *drawingTubeSlider;

- (void)startRunningInViewController:(UIViewController *)parentVC resolution:(CGSize)size aboveView:(UIView *)targetView;
- (void)stopRunningFromParentViewController;

- (void)setPreferredResolution:(CGSize)resolution;
- (void)beginSnapping;

- (void)showBuiltInCameraUIComponents:(BOOL)show;

@end

@protocol CZBuiltInCameraViewControllerTapDelegate <NSObject>

@optional
- (BOOL)shouldBuiltInCameraView:(CZBuiltInCameraViewController *)cameraView
               handleTapGesture:(UIGestureRecognizer *)gestureRecognizer;

- (void)builtinCameraView:(CZBuiltInCameraViewController *)cameraView
      didHandleTapGesture:(UIGestureRecognizer *)gestureRecognizer;

@end
