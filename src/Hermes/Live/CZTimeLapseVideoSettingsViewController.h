//
//  CZTimeLapseVideoSettingsViewController.h
//  Matscope
//
//  Created by Sherry Xu on 12/19/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZDialogController.h"

@interface CZTimelapseVideoSettingsViewController : CZDialogController

+ (NSUInteger)playRateValueForLevel:(CZTimelapseVideoPlayRate)playRate;

@end
