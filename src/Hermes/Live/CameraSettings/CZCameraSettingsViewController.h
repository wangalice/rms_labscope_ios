//
//  CZCameraSettingsViewController.h
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZCameraSettingsViewControllerDelegate.h"

@interface CZCameraSettingsViewController : UIViewController <CZCameraControlDelegate>

@property (nonatomic, weak) id<CZCameraSettingsViewControllerDelegate> delegate;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel NS_DESIGNATED_INITIALIZER;

- (void)prepareForSettingsWithCompletionHandler:(void (^)(BOOL prepared))completionHandler;

@end
