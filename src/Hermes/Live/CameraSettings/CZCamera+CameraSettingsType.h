//
//  CZCamera+CameraSettingsType.h
//  Hermes
//
//  Created by Li, Junlin on 4/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZCameraInterface/CZCameraInterface.h>

typedef NS_ENUM(NSInteger, CZCameraSettingsType) {
    CZCameraSettingsTypeEncodedAxiocam202,
    CZCameraSettingsTypeNonEncodedAxiocam202,
    CZCameraSettingsTypeEncodedAxiocam208,
    CZCameraSettingsTypeNonEncodedAxiocam208,
    CZCameraSettingsTypeKappa,
    CZCameraSettingsTypeWision,
    CZCameraSettingsTypeMotic,
    CZCameraSettingsTypeOther
};

@interface CZCamera (CameraSettingsType)

@property (nonatomic, readonly, assign) CZCameraSettingsType cameraSettingsType;

@end
