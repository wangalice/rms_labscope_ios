//
//  CZBasicCameraSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 3/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZBasicCameraSettingsViewController.h"
#import "CZWhiteBalanceHelpViewController.h"
#import "CZPopoverController.h"
#import "CZGridView.h"
#import "UIViewController+HUD.h"

static const NSInteger kAutoExposureButtonIndex = 0;
static const NSInteger kManualExposureButtonIndex = 1;
static const NSInteger kAutoWhiteBalanceButtonIndex = 0;
static const NSInteger kManualWhiteBalanceButtonIndex = 1;
static const NSInteger kNormalWhiteBalanceButtonIndex = 0;
static const NSInteger kEyepiecesWhiteBalanceButtonIndex = 1;
static const NSInteger kRLButtonIndex = 0;
static const NSInteger kTLButtonIndex = 1;

@interface CZBasicCameraSettingsViewController () <CZTextFieldDelegate>

@property (nonatomic, strong) CZCamera *camera;
@property (nonatomic, assign, getter=isAutoExposure) BOOL autoExposure;
@property (nonatomic, assign, getter=isAutoWhiteBalance) BOOL autoWhiteBalance;
@property (nonatomic, assign) float exposureTime;
@property (nonatomic, copy) NSArray<NSNumber *> *supportedExposureTimeValues;

@property (nonatomic, readonly, strong) CZGridView *exposureView;
@property (nonatomic, readonly, strong) UILabel *exposureLabel;
@property (nonatomic, readonly, strong) CZToggleButton *autoExposureToggleButton;
@property (nonatomic, readonly, strong) UILabel *exposureTimeLabel;
@property (nonatomic, readonly, strong) CZSlider *exposureTimeSlider;
@property (nonatomic, readonly, strong) CZTextField *exposureTimeTextField;
@property (nonatomic, readonly, strong) UILabel *gainLabel;
@property (nonatomic, readonly, strong) CZSlider *gainSlider;
@property (nonatomic, readonly, strong) CZTextField *gainTextField;
@property (nonatomic, readonly, strong) UILabel *exposureIntensityLabel;
@property (nonatomic, readonly, strong) CZSlider *exposureIntensitySlider;
@property (nonatomic, readonly, strong) CZTextField *exposureIntensityTextField;
@property (nonatomic, readonly, strong) CZButton *exposurePushButton;

@property (nonatomic, readonly, strong) CZGridView *whiteBalanceView;
@property (nonatomic, readonly, strong) UILabel *whiteBalanceLabel;
@property (nonatomic, readonly, strong) CZToggleButton *autoWhiteBalanceToggleButton;
@property (nonatomic, readonly, strong) CZSlider *colorTemperatureSlider;
@property (nonatomic, readonly, strong) CZButton *whiteBalancePushButton;
@property (nonatomic, readonly, strong) CZButton *whiteBalanceHelpButton;
@property (nonatomic, readonly, strong) CZButton *whiteBalanceColorPickerButton;
@property (nonatomic, readonly, strong) CZToggleButton *normalWhiteBalanceToggleButton;
@property (nonatomic, readonly, strong) CZDropdownMenu *illuminationDropdown;

@property (nonatomic, readonly, strong) CZGridView *lightIntensityView;
@property (nonatomic, readonly, strong) UILabel *lightIntensityLabel;
@property (nonatomic, readonly, strong) CZSlider *lightIntensitySlider;
@property (nonatomic, readonly, strong) CZTextField *lightIntensityTextField;
@property (nonatomic, readonly, strong) CZToggleButton *lightPathToggleButton;

@property (nonatomic, readonly, strong) UIActivityIndicatorView *loadingIndicator;

@property (nonatomic, weak) CZWhiteBalanceHelpViewController *whiteBalanceHelpViewController;

@end

@implementation CZBasicCameraSettingsViewController

@synthesize exposureView = _exposureView;
@synthesize exposureLabel = _exposureLabel;
@synthesize autoExposureToggleButton = _autoExposureToggleButton;
@synthesize exposureTimeLabel = _exposureTimeLabel;
@synthesize exposureTimeSlider = _exposureTimeSlider;
@synthesize exposureTimeTextField = _exposureTimeTextField;
@synthesize gainLabel = _gainLabel;
@synthesize gainSlider = _gainSlider;
@synthesize gainTextField = _gainTextField;
@synthesize exposureIntensityLabel = _exposureIntensityLabel;
@synthesize exposureIntensitySlider = _exposureIntensitySlider;
@synthesize exposureIntensityTextField = _exposureIntensityTextField;
@synthesize exposurePushButton = _exposurePushButton;

@synthesize whiteBalanceView = _whiteBalanceView;
@synthesize whiteBalanceLabel = _whiteBalanceLabel;
@synthesize autoWhiteBalanceToggleButton = _autoWhiteBalanceToggleButton;
@synthesize colorTemperatureSlider = _colorTemperatureSlider;
@synthesize whiteBalancePushButton = _whiteBalancePushButton;
@synthesize whiteBalanceHelpButton = _whiteBalanceHelpButton;
@synthesize whiteBalanceColorPickerButton = _whiteBalanceColorPickerButton;
@synthesize normalWhiteBalanceToggleButton = _normalWhiteBalanceToggleButton;
@synthesize illuminationDropdown = _illuminationDropdown;

@synthesize lightIntensityView = _lightIntensityView;
@synthesize lightIntensityLabel = _lightIntensityLabel;
@synthesize lightIntensitySlider = _lightIntensitySlider;
@synthesize lightIntensityTextField = _lightIntensityTextField;
@synthesize lightPathToggleButton = _lightPathToggleButton;

@synthesize loadingIndicator = _loadingIndicator;

- (instancetype)initWithCamera:(CZCamera *)camera {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _camera = camera;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs100];
    
    [self.view addSubview:self.exposureView];
    [self.exposureView addContentView:self.exposureLabel atRowIndex:0 columnRange:NSMakeRange(0, 5)];
    [self.exposureView addContentView:self.autoExposureToggleButton atRowIndex:1 columnRange:NSMakeRange(0, 5)];
    [self.exposureView addContentView:self.exposureTimeLabel atRowIndex:2 columnIndex:0];
    [self.exposureView addContentView:self.exposureTimeSlider atRowIndex:2 columnRange:NSMakeRange(1, 2)];
    [self.exposureView addContentView:self.exposureTimeTextField atRowIndex:2 columnRange:NSMakeRange(3, 2)];
    [self.exposureView addContentView:self.gainLabel atRowIndex:3 columnIndex:0];
    [self.exposureView addContentView:self.gainSlider atRowIndex:3 columnRange:NSMakeRange(1, 3)];
    [self.exposureView addContentView:self.gainTextField atRowIndex:3 columnIndex:4];
    [self.exposureView addContentView:self.exposureIntensityLabel atRowIndex:4 columnIndex:0];
    [self.exposureView addContentView:self.exposureIntensitySlider atRowIndex:4 columnIndex:1];
    [self.exposureView addContentView:self.exposureIntensityTextField atRowIndex:4 columnRange:NSMakeRange(2, 2)];
    [self.exposureView addContentView:self.exposurePushButton atRowIndex:4 columnIndex:4];
    
    [self.view addSubview:self.whiteBalanceView];
    [self.whiteBalanceView addContentView:self.whiteBalanceLabel atRowIndex:0 columnRange:NSMakeRange(0, 5)];
    [self.whiteBalanceView addContentView:self.autoWhiteBalanceToggleButton atRowIndex:1 columnRange:NSMakeRange(0, 5)];
    [self.whiteBalanceView addContentView:self.colorTemperatureSlider atRowIndex:2 columnRange:NSMakeRange(0, 4)];
    [self.whiteBalanceView addContentView:self.whiteBalancePushButton atRowIndex:2 columnIndex:4];
    [self.whiteBalanceView addContentView:self.whiteBalanceHelpButton atRowIndex:3 columnIndex:0];
//    [self.whiteBalanceView addContentView:self.whiteBalanceColorPickerButton atRowIndex:3 columnIndex:4];
    [self.whiteBalanceView addContentView:self.normalWhiteBalanceToggleButton atRowIndex:4 columnRange:NSMakeRange(0, 5)];
    [self.whiteBalanceView addContentView:self.illuminationDropdown atRowIndex:4 columnRange:NSMakeRange(0, 5)];

    [self.view addSubview:self.lightIntensityView];
    [self.lightIntensityView addContentView:self.lightIntensityLabel atRowIndex:0 columnRange:NSMakeRange(0, 5)];
    [self.lightIntensityView addContentView:self.lightIntensitySlider atRowIndex:1 columnRange:NSMakeRange(0, 4)];
    [self.lightIntensityView addContentView:self.lightIntensityTextField atRowIndex:1 columnIndex:4];
    [self.lightIntensityView addContentView:self.lightPathToggleButton atRowIndex:2 columnRange:NSMakeRange(0, 5)];

    [self.view addSubview:self.loadingIndicator];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadCameraSettings];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    if (self.whiteBalanceHelpViewController) {
        [self.whiteBalanceHelpViewController dismissViewControllerAnimated:NO completion:nil];
    }
    
    if (_illuminationDropdown && self.illuminationDropdown.isExpanded) {
        [self.illuminationDropdown collapseMenu];
    }
    
    [self showWaitingHUD:NO];
}

#pragma mark - Public

- (void)loadCameraSettings {
    [self hideControls];
    [self.loadingIndicator startAnimating];
    
    dispatch_group_t group = dispatch_group_create();
    
    if ([self.camera hasCapability:CZCameraCapabilityExposureMode]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CZCameraExposureMode exposureMode = [self.camera exposureMode];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.autoExposure = (exposureMode == kCZCameraExposureAutomatic);
                self.autoExposureToggleButton.selectedIndex = self.isAutoExposure ? kAutoExposureButtonIndex : kManualExposureButtonIndex;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityExposureTime]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            float minExposureTime = [self.camera minExposureTime];
            float maxExposureTime = [self.camera maxExposureTime];
            NSArray<NSNumber *> *supportedExposureTimeValues = [self.camera supportedExposureTimeValues];
            float exposureTime = [self.camera exposureTime];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.supportedExposureTimeValues = supportedExposureTimeValues;
                if (self.supportedExposureTimeValues.count > 0) {
                    self.exposureTimeSlider.minimumValue = 0.0;
                    self.exposureTimeSlider.maximumValue = self.supportedExposureTimeValues.count - 1;
                } else {
                    self.exposureTimeSlider.minimumValue = minExposureTime;
                    self.exposureTimeSlider.maximumValue = maxExposureTime;
                }
                NSString *exposureTimeText = [CZCommonUtils localizedStringFromFloat:exposureTime precision:2];
                self.exposureTimeTextField.text = exposureTimeText;
                [self exposureTimeTextFieldEditingDidEndAction:nil];
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityGain]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUInteger minGain = [self.camera minGain];
            NSUInteger maxGain = [self.camera maxGain];
            NSUInteger gain = [self.camera gain];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.gainSlider.minimumValue = minGain;
                self.gainSlider.maximumValue = maxGain;
                self.gainSlider.value = gain;
                [self gainSliderValueChangedAction:nil];
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityExposureIntensity]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUInteger minExposureIntensity = [self.camera minExposureIntensity];
            NSUInteger maxExposureIntensity = [self.camera maxExposureIntensity];
            NSUInteger exposureIntensity = [self.camera exposureIntensity];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.exposureIntensitySlider.minimumValue = minExposureIntensity;
                self.exposureIntensitySlider.maximumValue = maxExposureIntensity;
                self.exposureIntensitySlider.value = exposureIntensity;
                [self exposureIntensitySliderValueChangedAction:nil];
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityWhiteBalanceMode]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL isWhiteBalanceLocked = [self.camera isWhiteBalanceLocked];
            CZCameraWhiteBalanceMode whiteBalanceMode = [self.camera whiteBalanceMode];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.autoWhiteBalance = !isWhiteBalanceLocked;
                self.autoWhiteBalanceToggleButton.selectedIndex = self.isAutoWhiteBalance ? kAutoWhiteBalanceButtonIndex : kManualWhiteBalanceButtonIndex;
                if (whiteBalanceMode == CZCameraWhiteBalanceModeEyepieces) {
                    self.normalWhiteBalanceToggleButton.selectedIndex = kEyepiecesWhiteBalanceButtonIndex;
                } else {
                    self.normalWhiteBalanceToggleButton.selectedIndex = kNormalWhiteBalanceButtonIndex;
                }
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityColorTemperature]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUInteger coldestWhiteBalanceValue = [self.camera coldestWhiteBalanceValue];
            NSUInteger warmestWhiteBalanceValue = [self.camera warmestWhiteBalanceValue];
            NSUInteger whiteBalanceValue = [self.camera whiteBalanceValue];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.colorTemperatureSlider.minimumValue = coldestWhiteBalanceValue;
                self.colorTemperatureSlider.maximumValue = warmestWhiteBalanceValue;
                self.colorTemperatureSlider.value = whiteBalanceValue;
                [self colorTemperatureSliderValueChangedAction:nil];
                [self updateIlluminationDropdownTitle];
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityLightIntensity]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CZCameraLightPath lightPath = [self.camera lightPath];
            NSUInteger minLightIntensity = [self.camera minLightIntensityAtPath:lightPath];
            NSUInteger maxLightIntensity = [self.camera maxLightIntensityAtPath:lightPath];
            NSUInteger lightIntensity = [self.camera lightIntensityAtPath:lightPath];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.lightIntensitySlider.minimumValue = minLightIntensity;
                self.lightIntensitySlider.maximumValue = maxLightIntensity;
                self.lightIntensitySlider.value = lightIntensity;
                [self lightIntensitySliderValueChangedAction:nil];
                if (lightPath == CZCameraLightPathReflected) {
                    self.lightPathToggleButton.selectedIndex = kRLButtonIndex;
                } else if (lightPath == CZCameraLightPathTransmitted) {
                    self.lightPathToggleButton.selectedIndex = kTLButtonIndex;
                }
                dispatch_group_leave(group);
            });
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.loadingIndicator stopAnimating];
        [self showControlsAccordingToCameraType];
        [self updateControls];
    });
}

- (BOOL)shouldResetAutomaticDismissalTimer {
    if (self.whiteBalanceHelpViewController) {
        return YES;
    }
    
    if (_illuminationDropdown && self.illuminationDropdown.isExpanded) {
        return YES;
    }
    
    if (self.loadingIndicator.isAnimating) {
        return YES;
    }
    
    if (self.isShowingWaitingHUD) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Private

- (void)hideControls {
    self.exposureView.hidden = YES;
    self.whiteBalanceView.hidden = YES;
    self.lightIntensityView.hidden = YES;
}

- (void)showControlsAccordingToCameraType {
    BOOL isGeminiCamera = [self.camera isKindOfClass:[CZGeminiCamera class]];
    BOOL isMoticCamera = [self.camera isKindOfClass:[CZMoticCamera class]];
    
    self.exposureView.hidden = NO;
    self.gainLabel.hidden = ![self.camera hasCapability:CZCameraCapabilityGain];
    self.gainSlider.hidden = ![self.camera hasCapability:CZCameraCapabilityGain];
    self.gainTextField.hidden = ![self.camera hasCapability:CZCameraCapabilityGain];
    self.exposureIntensityLabel.hidden = ![self.camera hasCapability:CZCameraCapabilityExposureIntensity];
    self.exposureIntensitySlider.hidden = ![self.camera hasCapability:CZCameraCapabilityExposureIntensity];
    self.exposureIntensityTextField.hidden = ![self.camera hasCapability:CZCameraCapabilityExposureIntensity];
    self.exposurePushButton.hidden = ![self.camera hasCapability:CZCameraCapabilityExposureTimeAutoAdjust];
    
    self.whiteBalanceView.hidden = ![self.camera hasCapability:CZCameraCapabilityWhiteBalanceMode];
    self.colorTemperatureSlider.hidden = ![self.camera hasCapability:CZCameraCapabilityColorTemperature];
    self.whiteBalancePushButton.hidden = ![self.camera hasCapability:CZCameraCapabilityWhiteBalanceAutoAdjust];
    self.whiteBalanceHelpButton.hidden = ![self.camera hasCapability:CZCameraCapabilityWhiteBalanceAutoAdjust];
    self.normalWhiteBalanceToggleButton.hidden = !isGeminiCamera;
    self.illuminationDropdown.hidden = !isMoticCamera;
    
    self.lightIntensityView.hidden = ![self.camera hasCapability:CZCameraCapabilityLightIntensity];
    self.lightPathToggleButton.hidden = ![self.camera hasLightPath:CZCameraLightPathReflected] || ![self.camera hasLightPath:CZCameraLightPathTransmitted];
    
    // Move light intensity view left.
    if (self.whiteBalanceView.isHidden && !self.lightIntensityView.isHidden) {
        CGRect lightIntensityViewFrame = self.lightIntensityView.frame;
        lightIntensityViewFrame.origin.x = self.whiteBalanceView.frame.origin.x;
        self.lightIntensityView.frame = lightIntensityViewFrame;
    }
    
    // Move white balance help button up.
    if (self.colorTemperatureSlider.isHidden) {
        [self.whiteBalanceView removeContentView:self.whiteBalanceHelpButton];
        [self.whiteBalanceView addContentView:self.whiteBalanceHelpButton atRowIndex:2 columnIndex:0];
    }
    
    // Make color temperature slider wider, and move illumination dropdown up.
    if (isMoticCamera) {
        [self.whiteBalanceView removeContentView:self.colorTemperatureSlider];
        [self.whiteBalanceView addContentView:self.colorTemperatureSlider atRowIndex:2 columnRange:NSMakeRange(0, 5)];

        [self.whiteBalanceView removeContentView:self.illuminationDropdown];
        [self.whiteBalanceView addContentView:self.illuminationDropdown atRowIndex:3 columnRange:NSMakeRange(0, 5)];
    }
}

- (void)updateControls {
    // Disable all controls on camera control panel if the camera is Demo
    BOOL isDemoCamera = [self.camera isKindOfClass:[CZLocalFileCamera class]];
    self.autoExposureToggleButton.enabled = !isDemoCamera;
    self.autoWhiteBalanceToggleButton.enabled = !isDemoCamera;
    if (isDemoCamera) {
        self.autoExposure = YES;
        self.autoWhiteBalance = YES;
    }
    
    // Remove snapping exposure time settings when it's not necessary anymore.
    [self.camera setSnappingExposureTime:0.0f];
    
    [UIView animateWithDuration:0.25 animations:^{
        // Enable/disable controls in exposure view.
        NSArray *exposureControls = @[self.exposureTimeLabel, self.exposureTimeSlider, self.exposureTimeTextField,
                                      self.gainLabel, self.gainSlider, self.gainTextField,
                                      self.exposurePushButton];
        for (UIView *control in exposureControls) {
            control.userInteractionEnabled = !self.isAutoExposure;
            control.alpha = self.isAutoExposure ? 0.5 : 1.0;
        }
        
        // Enable/disable controls in white balance view.
        NSArray *whiteBalanceControls = @[self.colorTemperatureSlider, self.whiteBalancePushButton,
                                          self.whiteBalanceHelpButton,
                                          self.illuminationDropdown];
        for (UIView *control in whiteBalanceControls) {
            control.userInteractionEnabled = !self.isAutoWhiteBalance;
            control.alpha = self.isAutoWhiteBalance ? 0.5 : 1.0;
        }
        
        if ([self.camera isKindOfClass:[CZGeminiCamera class]]) {
            self.normalWhiteBalanceToggleButton.hidden = !self.isAutoWhiteBalance;
        }
    }];
}

- (void)updateIlluminationDropdownTitle {
    NSUInteger whiteBalanceValue = roundf(self.colorTemperatureSlider.value);
    NSArray *illuminationPresetNames = [self illuminationPresetNames];
    NSArray *illuminationPresetValues = [self illuminationPresetValues];
    BOOL foundIlluminationPreset = NO;
    for (NSUInteger i = 0; i < illuminationPresetNames.count - 1; ++i) {
        NSNumber *value = illuminationPresetValues[i];
        if (value.unsignedIntegerValue == whiteBalanceValue) {
            foundIlluminationPreset = YES;
            self.illuminationDropdown.selectedItemTitle = illuminationPresetNames[i];
            break;
        }
    }
    if (!foundIlluminationPreset) {
        self.illuminationDropdown.selectedItemTitle = L(@"LIVE_LIGHTSOURCE_OTHER");
    }
}

#pragma mark - Models

- (NSArray<NSString *> *)illuminationPresetNames {
    return @[L(@"LIVE_LIGHTSOURCE_SPOTLIGHT"),
             L(@"LIVE_LIGHTSOURCE_RINGLIGHT"),
             L(@"LIVE_LIGHTSOURCE_VERTICAL"),
             L(@"LIVE_LIGHTSOURCE_TRANSMITTED"),
             L(@"LIVE_LIGHTSOURCE_OTHER")];
}

- (NSArray<NSNumber *> *)illuminationPresetValues {
    return @[@95, @210, @90, @100, @130];
}

#pragma mark - Views

- (CZGridView *)makeGridViewWithFrame:(CGRect)frame needsDivider:(BOOL)needsDivider {
    CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:21.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row4 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    NSArray<CZGridRow *> *rows = @[row0, row1, row2, row3, row4];
    
    CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:48.0 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:frame.size.width - 48.0 * 3 - 16.0 * 3 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column2 = [[CZGridColumn alloc] initWithWidth:16.0 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column3 = [[CZGridColumn alloc] initWithWidth:16.0 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column4 = [[CZGridColumn alloc] initWithWidth:48.0 alignment:CZGridColumnAlignmentFill];
    NSArray<CZGridColumn *> *columns = @[column0, column1, column2, column3, column4];
    
    CZGridView *gridView = [[CZGridView alloc] initWithFrame:frame rows:rows columns:columns];
    
    gridView.rowSpacing = 16.0;
    gridView.columnSpacing = 16.0;
    [gridView setCustomSpacing:26.0 afterRowAtIndex:0];
    
    if (needsDivider) {
        UIView *divider = [[UIView alloc] initWithFrame:UIEdgeInsetsInsetRect(gridView.bounds, UIEdgeInsetsMake(5.0, -16.0, 0.0, gridView.bounds.size.width + 15.0))];
        divider.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        divider.backgroundColor = [UIColor cz_gs110];
        [gridView addSubview:divider];
    }
    
    return gridView;
}

- (CZGridView *)exposureView {
    if (_exposureView == nil) {
        _exposureView = [self makeGridViewWithFrame:CGRectMake(32.0, 16.0, 288.0, 223.0) needsDivider:NO];
    }
    return _exposureView;
}

- (UILabel *)exposureLabel {
    if (_exposureLabel == nil) {
        _exposureLabel = [[UILabel alloc] init];
        _exposureLabel.backgroundColor = [UIColor clearColor];
        _exposureLabel.font = [UIFont cz_subtitle1];
        _exposureLabel.text = L(@"LIVE_CAMERA_EXPOSURE");
        _exposureLabel.textColor = [UIColor cz_gs10];
        _exposureLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _exposureLabel;
}

- (CZToggleButton *)autoExposureToggleButton {
    if (_autoExposureToggleButton == nil) {
        _autoExposureToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"LIVE_AUTO_SWITCH_LABEL_ON"), L(@"LIVE_AUTO_SWITCH_LABEL_OFF")]];
        _autoExposureToggleButton.selectedIndex = kAutoExposureButtonIndex;
        [_autoExposureToggleButton addTarget:self action:@selector(autoExposureToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _autoExposureToggleButton;
}

- (UILabel *)exposureTimeLabel {
    if (_exposureTimeLabel == nil) {
        _exposureTimeLabel = [[UILabel alloc] init];
        _exposureTimeLabel.backgroundColor = [UIColor clearColor];
        _exposureTimeLabel.font = [UIFont cz_label1];
        _exposureTimeLabel.text = L(@"LIVE_CAMERA_EXPOSURE_TIME");
        _exposureTimeLabel.textColor = [UIColor cz_gs80];
        _exposureTimeLabel.textAlignment = NSTextAlignmentLeft;
        _exposureTimeLabel.numberOfLines = 2;
    }
    return _exposureTimeLabel;
}

- (CZSlider *)exposureTimeSlider {
    if (_exposureTimeSlider == nil) {
        _exposureTimeSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_exposureTimeSlider addTarget:self action:@selector(exposureTimeSliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_exposureTimeSlider addTarget:self action:@selector(exposureTimeSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_exposureTimeSlider addTarget:self action:@selector(exposureTimeSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_exposureTimeSlider addTarget:self action:@selector(exposureTimeSliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _exposureTimeSlider;
}

- (CZTextField *)exposureTimeTextField {
    if (_exposureTimeTextField == nil) {
        _exposureTimeTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:L(@"LIVE_MILLISECOND") rightImagePicker:nil];
        _exposureTimeTextField.delegate = self;
        _exposureTimeTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _exposureTimeTextField.adjustsFontSizeToFitWidth = YES;
        [_exposureTimeTextField addTarget:self action:@selector(exposureTimeTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _exposureTimeTextField;
}

- (UILabel *)gainLabel {
    if (_gainLabel == nil) {
        _gainLabel = [[UILabel alloc] init];
        _gainLabel.backgroundColor = [UIColor clearColor];
        _gainLabel.font = [UIFont cz_label1];
        _gainLabel.text = L(@"LIVE_CAMERA_GAIN");
        _gainLabel.textColor = [UIColor cz_gs80];
        _gainLabel.textAlignment = NSTextAlignmentLeft;
        _gainLabel.numberOfLines = 2;
    }
    return _gainLabel;
}

- (CZSlider *)gainSlider {
    if (_gainSlider == nil) {
        _gainSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_gainSlider addTarget:self action:@selector(gainSliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_gainSlider addTarget:self action:@selector(gainSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_gainSlider addTarget:self action:@selector(gainSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_gainSlider addTarget:self action:@selector(gainSliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _gainSlider;
}

- (CZTextField *)gainTextField {
    if (_gainTextField == nil) {
        _gainTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _gainTextField.delegate = self;
        _gainTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_gainTextField addTarget:self action:@selector(gainTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _gainTextField;
}

- (UILabel *)exposureIntensityLabel {
    if (_exposureIntensityLabel == nil) {
        _exposureIntensityLabel = [[UILabel alloc] init];
        _exposureIntensityLabel.backgroundColor = [UIColor clearColor];
        _exposureIntensityLabel.font = [UIFont cz_label1];
        _exposureIntensityLabel.text = L(@"LIVE_CAMERA_INTENSITY");
        _exposureIntensityLabel.textColor = [UIColor cz_gs80];
        _exposureIntensityLabel.textAlignment = NSTextAlignmentLeft;
        _exposureIntensityLabel.numberOfLines = 2;
    }
    return _exposureIntensityLabel;
}

- (CZSlider *)exposureIntensitySlider {
    if (_exposureIntensitySlider == nil) {
        _exposureIntensitySlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_exposureIntensitySlider addTarget:self action:@selector(exposureIntensitySliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_exposureIntensitySlider addTarget:self action:@selector(exposureIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_exposureIntensitySlider addTarget:self action:@selector(exposureIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_exposureIntensitySlider addTarget:self action:@selector(exposureIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _exposureIntensitySlider;
}

- (CZTextField *)exposureIntensityTextField {
    if (_exposureIntensityTextField == nil) {
        _exposureIntensityTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _exposureIntensityTextField.delegate = self;
        _exposureIntensityTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_exposureIntensityTextField addTarget:self action:@selector(exposureIntensityTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _exposureIntensityTextField;
}

- (CZButton *)exposurePushButton {
    if (_exposurePushButton == nil) {
        _exposurePushButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        [_exposurePushButton setTitle:L(@"LIVE_AUTO_ADJUST") forState:UIControlStateNormal];
        [_exposurePushButton addTarget:self action:@selector(exposurePushButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _exposurePushButton;
}

- (CZGridView *)whiteBalanceView {
    if (_whiteBalanceView == nil) {
        _whiteBalanceView = [self makeGridViewWithFrame:CGRectMake(352.0, 16.0, 288.0, 223.0) needsDivider:YES];
    }
    return _whiteBalanceView;
}

- (UILabel *)whiteBalanceLabel {
    if (_whiteBalanceLabel == nil) {
        _whiteBalanceLabel = [[UILabel alloc] init];
        _whiteBalanceLabel.backgroundColor = [UIColor clearColor];
        _whiteBalanceLabel.font = [UIFont cz_subtitle1];
        _whiteBalanceLabel.text = L(@"LIVE_CAMERA_WHITE_BALANCE");
        _whiteBalanceLabel.textColor = [UIColor cz_gs10];
        _whiteBalanceLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _whiteBalanceLabel;
}

- (CZToggleButton *)autoWhiteBalanceToggleButton {
    if (_autoWhiteBalanceToggleButton == nil) {
        _autoWhiteBalanceToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"LIVE_AUTO_SWITCH_LABEL_ON"), L(@"LIVE_AUTO_SWITCH_LABEL_OFF")]];
        _autoWhiteBalanceToggleButton.selectedIndex = kAutoWhiteBalanceButtonIndex;
        [_autoWhiteBalanceToggleButton addTarget:self action:@selector(autoWhiteBalanceToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _autoWhiteBalanceToggleButton;
}

- (CZSlider *)colorTemperatureSlider {
    if (_colorTemperatureSlider == nil) {
        _colorTemperatureSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        _colorTemperatureSlider.minimumValueText = L(@"COLD");
        _colorTemperatureSlider.maximumValueText = L(@"WARM");
        [_colorTemperatureSlider addTarget:self action:@selector(colorTemperatureSliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_colorTemperatureSlider addTarget:self action:@selector(colorTemperatureSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_colorTemperatureSlider addTarget:self action:@selector(colorTemperatureSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_colorTemperatureSlider addTarget:self action:@selector(colorTemperatureSliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _colorTemperatureSlider;
}

- (CZButton *)whiteBalancePushButton {
    if (_whiteBalancePushButton == nil) {
        _whiteBalancePushButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        [_whiteBalancePushButton setTitle:L(@"LIVE_AUTO_ADJUST") forState:UIControlStateNormal];
        [_whiteBalancePushButton addTarget:self action:@selector(whiteBalancePushButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whiteBalancePushButton;
}

- (CZButton *)whiteBalanceHelpButton {
    if (_whiteBalanceHelpButton == nil) {
        _whiteBalanceHelpButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        [_whiteBalanceHelpButton setTitle:@"?" forState:UIControlStateNormal];
        [_whiteBalanceHelpButton addTarget:self action:@selector(whiteBalanceHelpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whiteBalanceHelpButton;
}

- (CZButton *)whiteBalanceColorPickerButton {
    if (_whiteBalanceColorPickerButton == nil) {
        _whiteBalanceColorPickerButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        [_whiteBalanceColorPickerButton addTarget:self action:@selector(whiteBalanceColorPickerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _whiteBalanceColorPickerButton;
}

- (CZToggleButton *)normalWhiteBalanceToggleButton {
    if (_normalWhiteBalanceToggleButton == nil) {
        _normalWhiteBalanceToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"LIVE_CAMERA_WHITE_BALANCE_NW"), L(@"LIVE_CAMERA_WHITE_BALANCE_EYEPIECES")]];
        [_normalWhiteBalanceToggleButton addTarget:self action:@selector(normalWhiteBalanceToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _normalWhiteBalanceToggleButton;
}

- (CZDropdownMenu *)illuminationDropdown {
    if (_illuminationDropdown == nil) {
        _illuminationDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:[self illuminationPresetNames]];
        _illuminationDropdown.animationDirection = CZDropdownAnimationDirectionDown;
        [_illuminationDropdown addTarget:self action:@selector(illuminationDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _illuminationDropdown;
}

- (CZGridView *)lightIntensityView {
    if (_lightIntensityView == nil) {
        _lightIntensityView = [self makeGridViewWithFrame:CGRectMake(672.0, 16.0, 288.0, 223.0) needsDivider:YES];
    }
    return _lightIntensityView;
}

- (UILabel *)lightIntensityLabel {
    if (_lightIntensityLabel == nil) {
        _lightIntensityLabel = [[UILabel alloc] init];
        _lightIntensityLabel.backgroundColor = [UIColor clearColor];
        _lightIntensityLabel.font = [UIFont cz_subtitle1];
        _lightIntensityLabel.text = L(@"LIVE_CAMERA_LIGHT_INTENSITY");
        _lightIntensityLabel.textColor = [UIColor cz_gs10];
        _lightIntensityLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _lightIntensityLabel;
}

- (CZSlider *)lightIntensitySlider {
    if (_lightIntensitySlider == nil) {
        _lightIntensitySlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        _lightIntensitySlider.minimumValueText = @"0";
        _lightIntensitySlider.maximumValueText = @"100";
        [_lightIntensitySlider addTarget:self action:@selector(lightIntensitySliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_lightIntensitySlider addTarget:self action:@selector(lightIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_lightIntensitySlider addTarget:self action:@selector(lightIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_lightIntensitySlider addTarget:self action:@selector(lightIntensitySliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _lightIntensitySlider;
}

- (CZTextField *)lightIntensityTextField {
    if (_lightIntensityTextField == nil) {
        _lightIntensityTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _lightIntensityTextField.delegate = self;
        _lightIntensityTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_lightIntensityTextField addTarget:self action:@selector(lightIntensityTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _lightIntensityTextField;
}

- (CZToggleButton *)lightPathToggleButton {
    if (_lightPathToggleButton == nil) {
        _lightPathToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"LIVE_CAMERA_LIGHT_RL"), L(@"LIVE_CAMERA_LIGHT_TL")]];
        [_lightPathToggleButton addTarget:self action:@selector(lightPathToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _lightPathToggleButton;
}

- (UIActivityIndicatorView *)loadingIndicator {
    if (_loadingIndicator == nil) {
        _loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _loadingIndicator.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
        _loadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        _loadingIndicator.hidesWhenStopped = YES;
        [_loadingIndicator stopAnimating];
    }
    return _loadingIndicator;
}

#pragma mark - Actions

- (void)autoExposureToggleButtonAction:(id)sender {
    self.autoExposure = self.autoExposureToggleButton.selectedIndex == kAutoExposureButtonIndex;
    
    // Force a refresh on exposure time.
    [self exposureTimeTextFieldEditingDidEndAction:nil];
    
    if (self.isAutoExposure) {
        [self.camera setExposureMode:kCZCameraExposureAutomatic];
    } else {
        [self.camera beginSwitchToManualExposureMode];
    }
    
    [self updateControls];
}

- (void)exposureTimeSliderValueChangedAction:(id)sender {
    if (self.supportedExposureTimeValues.count > 0) {
        NSInteger index = roundf(self.exposureTimeSlider.value);
        if (index != self.exposureTimeSlider.value) {
            self.exposureTimeSlider.value = index;
        }
        
        NSNumber *exposureTime = self.supportedExposureTimeValues[index];
        self.exposureTime = exposureTime.floatValue;
    } else {
        self.exposureTime = [self.camera nearestValidExposureTime:self.exposureTimeSlider.value];
        self.exposureTimeSlider.value = self.exposureTime;
    }
    
    // Update text field accordingly.
    NSString *exposureTimeText = [CZCommonUtils localizedStringFromFloat:self.exposureTime precision:2];
    if (![exposureTimeText isEqualToString:self.exposureTimeTextField.text]) {
        self.exposureTimeTextField.text = exposureTimeText;
    }
}

- (void)exposureTimeSliderTouchEndAction:(id)sender {
    // Only apply the exposure time setting to the camera when user releases the knob on the slider.
    if (!self.isAutoExposure) {
        CZLogv(@"Exposure time changed to %f ms by slider.", self.exposureTime);
        [self.camera setExposureTime:self.exposureTime];
    }
    
    [self updateControls];
}

- (void)exposureTimeTextFieldEditingDidEndAction:(id)sender {
    if (self.supportedExposureTimeValues.count > 0) {
        float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.exposureTimeTextField.text] floatValue];
        self.exposureTime = [self.camera nearestValidExposureTime:preferredValue];
        
        // Update slider accordingly. Doesn't set if nothing changes.
        CGFloat newSliderValue = -1.0f;
        for (NSUInteger i = 0; i < self.supportedExposureTimeValues.count; ++i) {
            NSNumber *exposure = self.supportedExposureTimeValues[i];
            if ([exposure floatValue] == self.exposureTime) {
                newSliderValue = i;
            }
        }
        if (newSliderValue >= 0 && newSliderValue != self.exposureTimeSlider.value) {
            self.exposureTimeSlider.value = newSliderValue;
        }
    } else {
        float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.exposureTimeTextField.text] floatValue];
        self.exposureTime = [self.camera nearestValidExposureTime:preferredValue];
        if (preferredValue != self.exposureTimeSlider.value) {
            self.exposureTimeSlider.value = preferredValue;
        }
    }
    
    if (!self.isAutoExposure && sender != nil) {
        [self.camera setExposureTime:self.exposureTime];
    }
    
    CZLogv(@"Exposure time changed to %f ms by text field or back-end.", self.exposureTime);
    
    NSString *exposureTimeText = [CZCommonUtils localizedStringFromFloat:self.exposureTime precision:2];
    if (![exposureTimeText isEqualToString:self.exposureTimeTextField.text]) {
        self.exposureTimeTextField.text = exposureTimeText;
    }
    
    [self updateControls];
}

- (void)gainSliderValueChangedAction:(id)sender {
    NSUInteger newGainLevel = [self.camera nearestValidGain:self.gainSlider.value];
    if (newGainLevel != self.gainSlider.value) {
        self.gainSlider.value = newGainLevel;
    }
    
    // Update text field accordingly.
    NSString *gainText = [CZCommonUtils localizedStringFromFloat:newGainLevel precision:2];
    if (![gainText isEqualToString:self.gainTextField.text]) {
        self.gainTextField.text = gainText;
    }
}

- (void)gainSliderTouchEndAction:(id)sender {
    // Only apply the gain setting to the camera when user releases the knob on the slider.
    if (!self.isAutoExposure) {
        NSUInteger newGainLevel = self.gainSlider.value;
        CZLogv(@"Gain level changed to %lu.", (unsigned long)newGainLevel);
        [self.camera setGain:newGainLevel];
    }
}

- (void)gainTextFieldEditingDidEndAction:(id)sender {
    float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.gainTextField.text] floatValue];
    preferredValue = MAX(preferredValue, self.gainSlider.minimumValue);
    preferredValue = MIN(preferredValue, self.gainSlider.maximumValue);
    float newGainLevel = [self.camera nearestValidGain:preferredValue];
    
    if (!self.isAutoExposure && sender != nil) {
        [self.camera setGain:newGainLevel];
    }
    
    CZLogv(@"Gain changed to %f by text field.", newGainLevel);
    
    NSString *gainText = [CZCommonUtils localizedStringFromFloat:newGainLevel precision:2];
    if (![gainText isEqualToString:self.gainTextField.text]) {
        self.gainTextField.text = gainText;
    }
    
    // Update slider accordingly. Doesn't set if nothing changes.
    if (newGainLevel != self.gainSlider.value) {
        self.gainSlider.value = newGainLevel;
    }
    
    [self updateControls];
}

- (void)exposureIntensitySliderValueChangedAction:(id)sender {
    NSUInteger intensity = roundf(self.exposureIntensitySlider.value);
    if (intensity != self.exposureIntensitySlider.value) {
        self.exposureIntensitySlider.value = intensity;
    }
    
    // Update text field accordingly.
    NSUInteger intensityPercent = roundf((intensity - self.exposureIntensitySlider.minimumValue) / (self.exposureIntensitySlider.maximumValue - self.exposureIntensitySlider.minimumValue) * 100);
    NSString *intensityText = [CZCommonUtils localizedStringFromFloat:intensityPercent precision:2];
    if (![intensityText isEqualToString:self.exposureIntensityTextField.text]) {
        self.exposureIntensityTextField.text = intensityText;
    }
}

- (void)exposureIntensitySliderTouchEndAction:(id)sender {
    NSUInteger intensity = self.exposureIntensitySlider.value;
    CZLogv(@"Intensity changed to %lu.", (unsigned long)intensity);
    [self.camera setExposureIntensity:intensity];
}

- (void)exposureIntensityTextFieldEditingDidEndAction:(id)sender {
    float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.exposureIntensityTextField.text] floatValue];
    preferredValue = MAX(preferredValue, 0.0);
    preferredValue = MIN(preferredValue, 100.0);
    NSUInteger intensityPercent = roundf(preferredValue);
    NSUInteger intensity = self.exposureIntensitySlider.minimumValue + (self.exposureIntensitySlider.maximumValue - self.exposureIntensitySlider.minimumValue) * (intensityPercent / 100.0);
    
    if (sender != nil) {
        [self.camera setExposureIntensity:intensity];
    }
    
    CZLogv(@"Intensity changed to %ld by text field.", (long)intensity);
    
    NSString *intensityText = [CZCommonUtils localizedStringFromFloat:intensityPercent precision:2];
    if (![intensityText isEqualToString:self.exposureIntensityTextField.text]) {
        self.exposureIntensityTextField.text = intensityText;
    }
    
    // Update slider accordingly. Doesn't set if nothing changes.
    if (intensity != self.exposureIntensitySlider.value) {
        self.exposureIntensitySlider.value = intensity;
    }
    
    [self updateControls];
}

- (void)exposurePushButtonAction:(id)sender {
    CZLogv(@"Begin exposure time auto adjustment.");
    [self showWaitingHUD:YES];
    
    [self.camera beginExposureTimeAutoAdjust];
}

- (void)autoWhiteBalanceToggleButtonAction:(id)sender {
    self.autoWhiteBalance = self.autoWhiteBalanceToggleButton.selectedIndex == kAutoWhiteBalanceButtonIndex;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.autoWhiteBalance) {
            if (self.normalWhiteBalanceToggleButton.selectedIndex == kEyepiecesWhiteBalanceButtonIndex) {
                [self.camera setWhiteBalanceMode:CZCameraWhiteBalanceModeEyepieces];
            } else {
                [self.camera setIsWhiteBalanceLocked:NO];
            }
        } else {
            [self.camera setIsWhiteBalanceLocked:YES];
        }
        sleep(1); // Wait for one second.
        
        NSUInteger whiteBalanceValue = [self.camera whiteBalanceValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.colorTemperatureSlider setValue:whiteBalanceValue animated:YES];
            [self colorTemperatureSliderValueChangedAction:nil];
        });
    });
    
    [self updateControls];
}

- (void)colorTemperatureSliderValueChangedAction:(id)sender {
    NSUInteger value = [self.camera nearestValidWhiteBalanceValue:self.colorTemperatureSlider.value];
    if (value != self.colorTemperatureSlider.value) {
        self.colorTemperatureSlider.value = value;
    }
    
    [self updateIlluminationDropdownTitle];
}

- (void)colorTemperatureSliderTouchEndAction:(id)sender {
    NSUInteger value = self.colorTemperatureSlider.value;
    
    [self.camera setWhiteBalanceWithValue:value];
}

- (void)whiteBalancePushButtonAction:(id)sender {
    CZLogv(@"Begin white balance auto adjustment.");
    [self showWaitingHUD:YES];
    
    [self.camera beginWhiteBalanceAutoAdjust];
}

- (void)whiteBalanceHelpButtonAction:(id)sender {
    CZWhiteBalanceHelpViewController *whiteBalanceHelpViewController = [[CZWhiteBalanceHelpViewController alloc] init];
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:whiteBalanceHelpViewController];
    popover.sourceView = self.whiteBalanceHelpButton;
    popover.sourceRect = self.whiteBalanceHelpButton.bounds;
    popover.arrowDirection = CZPopoverArrowDirectionDown;
    popover.backgroundColor = [UIColor whiteColor];
    [popover presentAnimated:YES completion:nil];
    self.whiteBalanceHelpViewController = whiteBalanceHelpViewController;
}

- (void)whiteBalanceColorPickerButtonAction:(id)sender {
    
}

- (void)normalWhiteBalanceToggleButtonAction:(id)sender {
    if (self.normalWhiteBalanceToggleButton.selectedIndex == kNormalWhiteBalanceButtonIndex) {
        [self.camera setWhiteBalanceMode:CZCameraWhiteBalanceModeAutomatic];
    } else if (self.normalWhiteBalanceToggleButton.selectedIndex == kEyepiecesWhiteBalanceButtonIndex) {
        [self.camera setWhiteBalanceMode:CZCameraWhiteBalanceModeEyepieces];
    }
}

- (void)illuminationDropdownAction:(id)sender {
    NSArray *values = [self illuminationPresetValues];
    NSUInteger value = 125;
    if (self.illuminationDropdown.selectedIndex < values.count) {
        value = [values[self.illuminationDropdown.selectedIndex] unsignedIntegerValue];
    }
    
    CZLogv(@"Begin white balance temperature adjustment.");
    
    [self.camera setWhiteBalanceWithValue:value];
    if (value != self.colorTemperatureSlider.value) {
        self.colorTemperatureSlider.value = value;
    }
}

- (void)lightIntensitySliderValueChangedAction:(id)sender {
    NSUInteger lightIntensity = roundf(self.lightIntensitySlider.value);
    if (lightIntensity != self.lightIntensitySlider.value) {
        self.lightIntensitySlider.value = lightIntensity;
    }
    
    // Update text field accordingly.
    NSUInteger lightIntensityPercent = roundf((lightIntensity - self.lightIntensitySlider.minimumValue) / (self.lightIntensitySlider.maximumValue - self.lightIntensitySlider.minimumValue) * 100);
    NSString *lightIntensityText = [CZCommonUtils localizedStringFromFloat:lightIntensityPercent precision:2];
    if (![lightIntensityText isEqualToString:self.lightIntensityTextField.text]) {
        self.lightIntensityTextField.text = lightIntensityText;
    }
}

- (void)lightIntensitySliderTouchEndAction:(id)sender {
    NSUInteger lightIntensity = self.lightIntensitySlider.value;
    
    if (self.lightPathToggleButton.selectedIndex == kRLButtonIndex) {
        [self.camera setLightIntensity:lightIntensity atPath:CZCameraLightPathReflected];
    } else if (self.lightPathToggleButton.selectedIndex == kTLButtonIndex) {
        [self.camera setLightIntensity:lightIntensity atPath:CZCameraLightPathTransmitted];
    }
}

- (void)lightIntensityTextFieldEditingDidEndAction:(id)sender {
    float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.lightIntensityTextField.text] floatValue];
    preferredValue = MAX(preferredValue, 0.0);
    preferredValue = MIN(preferredValue, 100.0);
    NSUInteger lightIntensityPercent = roundf(preferredValue);
    NSUInteger lightIntensity = self.lightIntensitySlider.minimumValue + (self.lightIntensitySlider.maximumValue - self.lightIntensitySlider.minimumValue) * (lightIntensityPercent / 100.0);
    
    if (sender != nil) {
        if (self.lightPathToggleButton.selectedIndex == kRLButtonIndex) {
            [self.camera setLightIntensity:lightIntensity atPath:CZCameraLightPathReflected];
        } else if (self.lightPathToggleButton.selectedIndex == kTLButtonIndex) {
            [self.camera setLightIntensity:lightIntensity atPath:CZCameraLightPathTransmitted];
        }
    }
    
    CZLogv(@"Light intensity changed to %ld by text field.", (long)lightIntensityPercent);
    
    NSString *lightIntensityText = [CZCommonUtils localizedStringFromFloat:lightIntensityPercent precision:2];
    if (![lightIntensityText isEqualToString:self.lightIntensityTextField.text]) {
        self.lightIntensityTextField.text = lightIntensityText;
    }
    
    // Update slider accordingly. Doesn't set if nothing changes.
    if (lightIntensity != self.lightIntensitySlider.value) {
        self.lightIntensitySlider.value = lightIntensity;
    }
    
    [self updateControls];
}

- (void)lightPathToggleButtonAction:(id)sender {
    CZCameraLightPath lightPath = CZCameraLightPathUnknown;
    if (self.lightPathToggleButton.selectedIndex == kRLButtonIndex) {
        lightPath = CZCameraLightPathReflected;
    } else if (self.lightPathToggleButton.selectedIndex == kTLButtonIndex) {
        lightPath = CZCameraLightPathTransmitted;
    }
    
    if (lightPath == CZCameraLightPathUnknown) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setLightPath:lightPath];
        sleep(1); // Wait for one second.
        NSUInteger minLightIntensity = [self.camera minLightIntensityAtPath:lightPath];
        NSUInteger maxLightIntensity = [self.camera maxLightIntensityAtPath:lightPath];
        NSUInteger lightIntensity = [self.camera lightIntensityAtPath:lightPath];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.lightIntensitySlider.minimumValue = minLightIntensity;
            self.lightIntensitySlider.maximumValue = maxLightIntensity;
            [self.lightIntensitySlider setValue:lightIntensity animated:YES];
            [self lightIntensitySliderValueChangedAction:nil];
        });
    });
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string {
    return [CZCommonUtils numericTextField:textField shouldAcceptChange:string];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - CZCameraControlDelegate

- (void)cameraDidFinishExposureTimeAutoAdjust:(CZCamera *)camera {
    // Force a refresh of exposure time after auto adjust.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        float exposureTime = [self.camera exposureTime];
        NSUInteger gainLevel = [self.camera gain];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *exposureTimeText = [CZCommonUtils localizedStringFromFloat:exposureTime precision:2];
            self.exposureTimeTextField.text = exposureTimeText;
            [self exposureTimeTextFieldEditingDidEndAction:nil];
            
            [self.gainSlider setValue:gainLevel animated:YES];
            [self gainSliderValueChangedAction:nil];
        });
    });
    
    [self showWaitingHUD:NO];
}

- (void)cameraDidFinishSwitchToManualExposureMode:(CZCamera *)camera userInfo:(NSDictionary *)userInfo {
    float exposureTime = [userInfo[CZCameraExposureTimeKey] floatValue];
    NSUInteger gain = [userInfo[CZCameraGainKey] unsignedIntegerValue];
    
    NSString *exposureTimeText = [CZCommonUtils localizedStringFromFloat:exposureTime precision:2];
    self.exposureTimeTextField.text = exposureTimeText;
    [self exposureTimeTextFieldEditingDidEndAction:nil];
    
    [self.gainSlider setValue:gain animated:YES];
    [self gainSliderValueChangedAction:nil];
}

- (void)cameraDidFinishWhiteBalanceAutoAdjust:(CZCamera *)camera userInfo:(NSDictionary *)userInfo {
    // Force a refresh of color temperature after auto adjust.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSNumber *whiteBalanceValue = userInfo[CZCameraWhiteBalanceValueKey];
        if (whiteBalanceValue == nil) {
            whiteBalanceValue = @([self.camera whiteBalanceValue]);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.colorTemperatureSlider setValue:whiteBalanceValue.unsignedIntegerValue animated:YES];
            [self colorTemperatureSliderValueChangedAction:nil];
        });
    });
    
    [self showWaitingHUD:NO];
}

@end
