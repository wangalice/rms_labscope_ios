//
//  CZWhiteBalanceHelpViewController.m
//  Hermes
//
//  Created by Li, Junlin on 3/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZWhiteBalanceHelpViewController.h"
#import <WebKit/WebKit.h>

@interface CZWhiteBalanceHelpViewController ()

@end

@implementation CZWhiteBalanceHelpViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.preferredContentSize = CGSizeMake(850.0, 450.0);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0))];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webView.backgroundColor = [UIColor whiteColor];
    
    NSString *htmlFormat = @"<html><head><style type=\"text/css\">body {font-family:HelveticaNeue;}</style></head><body><h3>%@</h3><p>%@</p><h4>%@</h4><p><ol><li>%@</li><li>%@</li><li>%@</li><li>%@</li></ol></p><h4>%@</h4><p><ol><li>%@</li><li>%@</li><li>%@</li><li>%@</li></ol></p></body></html>";
    
    NSString *htmlString = [NSString stringWithFormat:htmlFormat,
                            L(@"HELP_WB_TITLE"),
                            L(@"HELP_WB_SUMMARY"),
                            L(@"HELP_WB_TL_SUBTITLE"),
                            L(@"HELP_WB_TL_STEP1"),
                            L(@"HELP_WB_TL_STEP2"),
                            L(@"HELP_WB_TL_STEP3"),
                            L(@"HELP_WB_TL_STEP4"),
                            L(@"HELP_WB_RL_SUBTITLE"),
                            L(@"HELP_WB_RL_STEP1"),
                            L(@"HELP_WB_RL_STEP2"),
                            L(@"HELP_WB_RL_STEP3"),
                            L(@"HELP_WB_RL_STEP4")];
    
    [webView loadHTMLString:htmlString baseURL:nil];
    
    [self.view addSubview:webView];
}

@end
