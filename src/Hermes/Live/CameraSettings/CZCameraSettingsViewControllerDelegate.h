//
//  CZCameraSettingsViewControllerDelegate.h
//  Labscope
//
//  Created by Li, Junlin on 3/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CZCameraSettingsViewControllerDelegate <NSObject>

@optional
- (void)cameraSettingsViewControllerWillDismissAutomatically:(UIViewController *)sender;
- (void)cameraSettingsViewControllerDidDismissAutomatically:(UIViewController *)sender;

@end
