//
//  CZCameraSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCameraSettingsViewController.h"
#import "CZBasicCameraSettingsViewController.h"
#import "CZAdvancedCameraSettingsViewController.h"
#import "CZApplication.h"
#import "UIViewController+HUD.h"

const NSTimeInterval CZCameraSettingsViewControllerAutomaticDismissalTime = 30.0;

static const CGFloat kCameraSettingsHeaderHeight = 64.0;
static const NSInteger kBasicCameraSettingsButtonIndex = 0;
static const NSInteger kAdvancedCameraSettingsButtonIndex = 1;

@interface CZCameraSettingsViewController ()

@property (nonatomic, strong) CZCamera *camera;
@property (atomic, strong) CZCameraControlLock *cameraControlLock;
@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;

@property (nonatomic, readonly, strong) UILabel *cameraSettingsLabel;
@property (nonatomic, readonly, strong) CZButton *resetAllButton;
@property (nonatomic, readonly, strong) CZToggleButton *advancedCameraSettingsToggleButton;
@property (nonatomic, readonly, strong) UIView *childViewControllerContentView;
@property (nonatomic, readonly, strong) CZBasicCameraSettingsViewController *basicCameraSettingsViewController;
@property (nonatomic, readonly, strong) CZAdvancedCameraSettingsViewController *advancedCameraSettingsViewController;

@property (nonatomic, strong) NSTimer *automaticDismissalTimer;
@property (nonatomic, strong) NSDate *latestEventTime;

@end

@implementation CZCameraSettingsViewController

@synthesize cameraSettingsLabel = _cameraSettingsLabel;
@synthesize resetAllButton = _resetAllButton;
@synthesize advancedCameraSettingsToggleButton = _advancedCameraSettingsToggleButton;
@synthesize childViewControllerContentView = _childViewControllerContentView;
@synthesize basicCameraSettingsViewController = _basicCameraSettingsViewController;
@synthesize advancedCameraSettingsViewController = _advancedCameraSettingsViewController;

- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _camera = camera;
        _camera.controlDelegate = self;
        _microscopeModel = microscopeModel;
        self.preferredContentSize = CGSizeMake(992.0, 335.0);
    }
    return self;
}

- (void)dealloc {
    _camera.controlDelegate = nil;
    [_automaticDismissalTimer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs100];
    
    [self.view addSubview:self.cameraSettingsLabel];
    [self.view addSubview:self.resetAllButton];
    [self.view addSubview:self.advancedCameraSettingsToggleButton];
    [self.view addSubview:self.childViewControllerContentView];
    [self addBasicCameraSettingsViewController];
    
    [self.cameraSettingsLabel.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.cameraSettingsLabel.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:kCameraSettingsHeaderHeight / 2 + 5.0].active = YES;
    
    [self.resetAllButton.leadingAnchor constraintEqualToAnchor:self.cameraSettingsLabel.trailingAnchor constant:72.0].active = YES;
    [self.resetAllButton.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:kCameraSettingsHeaderHeight / 2 + 5.0].active = YES;
    
    [self.advancedCameraSettingsToggleButton.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-32.0].active = YES;
    [self.advancedCameraSettingsToggleButton.centerYAnchor constraintEqualToAnchor:self.view.topAnchor constant:kCameraSettingsHeaderHeight / 2 + 5.0].active = YES;
    
    self.resetAllButton.hidden = ![self.camera hasCapability:CZCameraCapabilityResetAll];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.cameraControlLock == nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CZCameraControlLock *cameraLock = [[CZCameraControlLock alloc] initWithType:CZCameraControlTypeConfiguration camera:self.camera retained:YES];
            self.cameraControlLock = cameraLock;
        });
    }
    
    [self.automaticDismissalTimer invalidate];
    self.automaticDismissalTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(automaticDismissalTimerHandler:) userInfo:nil repeats:YES];
    self.latestEventTime = [NSDate date];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.cameraControlLock) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self.cameraControlLock = nil;
        });
    }
    
    [self.automaticDismissalTimer invalidate];
    self.automaticDismissalTimer = nil;
    self.latestEventTime = nil;
}

#pragma mark - Public

- (void)prepareForSettingsWithCompletionHandler:(void (^)(BOOL))completionHandler {
    // Do communications with cameras in background.
    __block CZCamera *camera = self.camera;  // self.camera might change, use "block" camera instead.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Get the control access from camera.
        CZCameraControlLock *cameraLock = [[CZCameraControlLock alloc] initWithType:CZCameraControlTypeConfiguration camera:camera retained:YES];
        self.cameraControlLock = cameraLock;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL prepared = (self.cameraControlLock != nil);
            if (completionHandler) {
                completionHandler(prepared);
            }
        });
    });
}

#pragma mark - Private

- (void)addBasicCameraSettingsViewController {
    [self addChildViewController:self.basicCameraSettingsViewController];
    self.basicCameraSettingsViewController.view.frame = self.childViewControllerContentView.bounds;
    self.basicCameraSettingsViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.childViewControllerContentView addSubview:self.basicCameraSettingsViewController.view];
    [self.basicCameraSettingsViewController didMoveToParentViewController:self];
}

- (void)removeBasicCameraSettingsViewController {
    [self.basicCameraSettingsViewController willMoveToParentViewController:nil];
    [self.basicCameraSettingsViewController.view removeFromSuperview];
    [self.basicCameraSettingsViewController removeFromParentViewController];
}

- (void)addAdvancedCameraSettingsViewController {
    [self addChildViewController:self.advancedCameraSettingsViewController];
    self.advancedCameraSettingsViewController.view.frame = self.childViewControllerContentView.bounds;
    self.advancedCameraSettingsViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.childViewControllerContentView addSubview:self.advancedCameraSettingsViewController.view];
    [self.advancedCameraSettingsViewController didMoveToParentViewController:self];
}

- (void)removeAdvancedCameraSettingsViewController {
    [self.advancedCameraSettingsViewController willMoveToParentViewController:nil];
    [self.advancedCameraSettingsViewController.view removeFromSuperview];
    [self.advancedCameraSettingsViewController removeFromParentViewController];
}

#pragma mark - Timers

- (void)automaticDismissalTimerHandler:(NSTimer *)timer {
    BOOL shouldResetAutomaticDismissalTimer = NO;
    if (self.advancedCameraSettingsToggleButton.selectedIndex == kBasicCameraSettingsButtonIndex) {
        shouldResetAutomaticDismissalTimer = [self.basicCameraSettingsViewController shouldResetAutomaticDismissalTimer];
    } else if (self.advancedCameraSettingsToggleButton.selectedIndex == kAdvancedCameraSettingsButtonIndex) {
        shouldResetAutomaticDismissalTimer = [self.advancedCameraSettingsViewController shouldResetAutomaticDismissalTimer];
    }
    
    if (shouldResetAutomaticDismissalTimer) {
        self.latestEventTime = [NSDate date];
    } else {
        // latestEventTime = MAX(latestEventTime, application's latestEventSendDate)
        self.latestEventTime = [self.latestEventTime compare:CZApplication.sharedApplication.latestEventSendDate] == NSOrderedAscending ? CZApplication.sharedApplication.latestEventSendDate : self.latestEventTime;
    }
    
    if ([self.latestEventTime timeIntervalSinceNow] <= -CZCameraSettingsViewControllerAutomaticDismissalTime) {
        [self.automaticDismissalTimer invalidate];
        self.automaticDismissalTimer = nil;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(cameraSettingsViewControllerWillDismissAutomatically:)]) {
            [self.delegate cameraSettingsViewControllerWillDismissAutomatically:self];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(cameraSettingsViewControllerDidDismissAutomatically:)]) {
                [self.delegate cameraSettingsViewControllerDidDismissAutomatically:self];
            }
        }];
    }
}

#pragma mark - Views

- (UILabel *)cameraSettingsLabel {
    if (_cameraSettingsLabel == nil) {
        _cameraSettingsLabel = [[UILabel alloc] init];
        _cameraSettingsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _cameraSettingsLabel.backgroundColor = [UIColor clearColor];
        _cameraSettingsLabel.font = [UIFont cz_h2];
        _cameraSettingsLabel.text = L(@"LIVE_CAMERA_CONTROL_TITLE");
        _cameraSettingsLabel.textColor = [UIColor cz_gs10];
    }
    return _cameraSettingsLabel;
}

- (CZButton *)resetAllButton {
    if (_resetAllButton == nil) {
        _resetAllButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        _resetAllButton.translatesAutoresizingMaskIntoConstraints = NO;
        _resetAllButton.hidden = YES;
        [_resetAllButton setTitle:L(@"RESET_ALL") forState:UIControlStateNormal];
        [_resetAllButton addTarget:self action:@selector(resetAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_resetAllButton.widthAnchor constraintEqualToConstant:112.0].active = YES;
        [_resetAllButton.heightAnchor constraintEqualToConstant:32.0].active = YES;
    }
    return _resetAllButton;
}

- (CZToggleButton *)advancedCameraSettingsToggleButton {
    if (_advancedCameraSettingsToggleButton == nil) {
        _advancedCameraSettingsToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"BASIC"), L(@"ADVANCED")]];
        _advancedCameraSettingsToggleButton.translatesAutoresizingMaskIntoConstraints = NO;
        _advancedCameraSettingsToggleButton.selectedIndex = kBasicCameraSettingsButtonIndex;
        [_advancedCameraSettingsToggleButton addTarget:self action:@selector(advancedCameraSettingsToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
        
        [_advancedCameraSettingsToggleButton.widthAnchor constraintEqualToConstant:272.0].active = YES;
        [_advancedCameraSettingsToggleButton.heightAnchor constraintEqualToConstant:32.0].active = YES;
    }
    return _advancedCameraSettingsToggleButton;
}

- (UIView *)childViewControllerContentView {
    if (_childViewControllerContentView == nil) {
        _childViewControllerContentView = [[UIView alloc] initWithFrame:UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(kCameraSettingsHeaderHeight, 0.0, 0.0, 0.0))];
        _childViewControllerContentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _childViewControllerContentView.backgroundColor = [UIColor clearColor];
    }
    return _childViewControllerContentView;
}

- (CZBasicCameraSettingsViewController *)basicCameraSettingsViewController {
    if (_basicCameraSettingsViewController == nil) {
        _basicCameraSettingsViewController = [[CZBasicCameraSettingsViewController alloc] initWithCamera:self.camera];
        _basicCameraSettingsViewController.delegate = self.delegate;
    }
    return _basicCameraSettingsViewController;
}

- (CZAdvancedCameraSettingsViewController *)advancedCameraSettingsViewController {
    if (_advancedCameraSettingsViewController == nil) {
        _advancedCameraSettingsViewController = [[CZAdvancedCameraSettingsViewController alloc] initWithCamera:self.camera microscopeModel:self.microscopeModel];
        _advancedCameraSettingsViewController.delegate = self.delegate;
    }
    return _advancedCameraSettingsViewController;
}

#pragma mark - Actions

- (void)resetAllButtonAction:(id)sender {
    [self showWaitingHUD:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera beginReset];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Hide camera settings popover while resetting. Will reload after resetting is finished.
            self.childViewControllerContentView.hidden = YES;
        });
    });
}

- (void)advancedCameraSettingsToggleButtonAction:(id)sender {
    if (self.advancedCameraSettingsToggleButton.selectedIndex == kBasicCameraSettingsButtonIndex) {
        [self removeAdvancedCameraSettingsViewController];
        [self addBasicCameraSettingsViewController];
    } else if (self.advancedCameraSettingsToggleButton.selectedIndex == kAdvancedCameraSettingsButtonIndex) {
        [self removeBasicCameraSettingsViewController];
        [self addAdvancedCameraSettingsViewController];
    }
}

#pragma mark - CZCameraControlDelegate

- (void)cameraDidFinishExposureTimeAutoAdjust:(CZCamera *)camera {
    [self.basicCameraSettingsViewController cameraDidFinishExposureTimeAutoAdjust:camera];
}

- (void)cameraDidFinishSwitchToManualExposureMode:(CZCamera *)camera userInfo:(NSDictionary *)userInfo {
    [self.basicCameraSettingsViewController cameraDidFinishSwitchToManualExposureMode:camera userInfo:userInfo];
}

- (void)cameraDidFinishWhiteBalanceAutoAdjust:(CZCamera *)camera userInfo:(NSDictionary *)userInfo {
    [self.basicCameraSettingsViewController cameraDidFinishWhiteBalanceAutoAdjust:camera userInfo:userInfo];
}

- (void)cameraDidFinishReset:(CZCamera *)camera {
    [self showWaitingHUD:NO];
    
    self.childViewControllerContentView.hidden = NO;
    
    if (self.advancedCameraSettingsToggleButton.selectedIndex == kBasicCameraSettingsButtonIndex) {
        [self.basicCameraSettingsViewController loadCameraSettings];
    } else if (self.advancedCameraSettingsToggleButton.selectedIndex == kAdvancedCameraSettingsButtonIndex) {
        [self.advancedCameraSettingsViewController loadCameraSettings];
    }
}

@end
