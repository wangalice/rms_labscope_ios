//
//  CZAdvancedCameraSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 3/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAdvancedCameraSettingsViewController.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZCamera+CameraSettingsType.h"
#import "CZGridView.h"
#import "UIViewController+HUD.h"
#import "CZMicroscopeNameValidator.h"

static const NSInteger kColorButtonIndex = 0;
static const NSInteger kGrayscaleButtonIndex = 1;

@interface CZAdvancedCameraSettingsViewController () <CZTextFieldDelegate, UITextViewDelegate, CZKeyboardObserver>

@property (nonatomic, strong) CZCamera *camera;
@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;

@property (nonatomic, readonly, strong) CZDropdownItem *imageOrientationItemOriginal;
@property (nonatomic, readonly, strong) CZDropdownItem *imageOrientationItemFlipH;
@property (nonatomic, readonly, strong) CZDropdownItem *imageOrientationItemFlipV;
@property (nonatomic, readonly, strong) CZDropdownItem *imageOrientationItemFlipBoth;

@property (nonatomic, readonly, strong) CZGridView *section0;
@property (nonatomic, readonly, strong) CZGridView *section1;
@property (nonatomic, readonly, strong) CZGridView *section2;
@property (nonatomic, copy) NSArray<CZGridView *> *sections;

@property (nonatomic, readonly, strong) UILabel *microscopeNameLabel;
@property (nonatomic, readonly, strong) CZTextField *microscopeNameTextField;
@property (nonatomic, readonly, strong) CZMicroscopeNameValidator *microscopeNameValidator;
@property (nonatomic, readonly, strong) UILabel *macAddressLabel;
@property (nonatomic, readonly, strong) CZTextField *macAddressTextField;
@property (nonatomic, readonly, strong) UILabel *ipAddressLabel;
@property (nonatomic, readonly, strong) CZTextField *ipAddressTextField;
@property (nonatomic, readonly, strong) UILabel *firmwareVersionLabel;
@property (nonatomic, readonly, strong) CZTextField *firmwareVersionTextField;

@property (nonatomic, readonly, strong) UILabel *streamingQualityLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *streamingQualityDropdown;
@property (nonatomic, readonly, strong) UILabel *imageOrientationLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *imageOrientationDropdown;
@property (nonatomic, readonly, strong) UILabel *snapResolutionLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *snapResolutionDropdown;
@property (nonatomic, readonly, strong) UILabel *sharpeningLabel;
@property (nonatomic, readonly, strong) CZSwitch *sharpeningSwitch;
@property (nonatomic, readonly, strong) CZSlider *sharpeningSlider;
@property (nonatomic, readonly, strong) CZTextField *sharpeningTextField;
@property (nonatomic, readonly, strong) UILabel *denoiseLabel;
@property (nonatomic, readonly, strong) CZSwitch *denoiseSwitch;
@property (nonatomic, readonly, strong) UILabel *pixelCorrectionLabel;
@property (nonatomic, readonly, strong) CZSwitch *pixelCorrectionSwitch;
@property (nonatomic, readonly, strong) UILabel *hdrLabel;
@property (nonatomic, readonly, strong) CZSwitch *hdrSwitch;
@property (nonatomic, readonly, strong) UILabel *bitDepthLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *bitDepthDropdown;
@property (nonatomic, readonly, strong) UILabel *grayscaleModeLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *grayscaleModeDropdown;
@property (nonatomic, readonly, strong) UILabel *gammaLabel;
@property (nonatomic, readonly, strong) CZSlider *gammaSlider;
@property (nonatomic, readonly, strong) CZTextField *gammaTextField;
@property (nonatomic, readonly, strong) UILabel *webConfigurationLabel;
@property (nonatomic, readonly, strong) UITextView *webConfigurationLinkView;

@property (nonatomic, readonly, strong) UIActivityIndicatorView *loadingIndicator;

@property (nonatomic, strong) CZDropdownMenu *pendingDropdown;

@end

@implementation CZAdvancedCameraSettingsViewController

@synthesize section0 = _section0;
@synthesize section1 = _section1;
@synthesize section2 = _section2;

@synthesize microscopeNameLabel = _microscopeNameLabel;
@synthesize microscopeNameTextField = _microscopeNameTextField;
@synthesize microscopeNameValidator = _microscopeNameValidator;
@synthesize macAddressLabel = _macAddressLabel;
@synthesize macAddressTextField = _macAddressTextField;
@synthesize ipAddressLabel = _ipAddressLabel;
@synthesize ipAddressTextField = _ipAddressTextField;
@synthesize firmwareVersionLabel = _firmwareVersionLabel;
@synthesize firmwareVersionTextField = _firmwareVersionTextField;

@synthesize streamingQualityLabel = _streamingQualityLabel;
@synthesize streamingQualityDropdown = _streamingQualityDropdown;
@synthesize imageOrientationLabel = _imageOrientationLabel;
@synthesize imageOrientationDropdown = _imageOrientationDropdown;
@synthesize snapResolutionLabel = _snapResolutionLabel;
@synthesize snapResolutionDropdown = _snapResolutionDropdown;
@synthesize sharpeningLabel = _sharpeningLabel;
@synthesize sharpeningSwitch = _sharpeningSwitch;
@synthesize sharpeningSlider = _sharpeningSlider;
@synthesize sharpeningTextField = _sharpeningTextField;
@synthesize denoiseLabel = _denoiseLabel;
@synthesize denoiseSwitch = _denoiseSwitch;
@synthesize pixelCorrectionLabel = _pixelCorrectionLabel;
@synthesize pixelCorrectionSwitch = _pixelCorrectionSwitch;
@synthesize hdrLabel = _hdrLabel;
@synthesize hdrSwitch = _hdrSwitch;
@synthesize bitDepthLabel = _bitDepthLabel;
@synthesize bitDepthDropdown = _bitDepthDropdown;
@synthesize grayscaleModeLabel = _grayscaleModeLabel;
@synthesize grayscaleModeDropdown = _grayscaleModeDropdown;
@synthesize gammaLabel = _gammaLabel;
@synthesize gammaSlider = _gammaSlider;
@synthesize gammaTextField = _gammaTextField;
@synthesize webConfigurationLabel = _webConfigurationLabel;
@synthesize webConfigurationLinkView = _webConfigurationLinkView;

@synthesize loadingIndicator = _loadingIndicator;

- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _camera = camera;
        _microscopeModel = microscopeModel;
        
        if (_microscopeModel.type == kPrimotech || _microscopeModel.type == kPrimoStar) {
            _imageOrientationItemOriginal = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_ORIGINAL_CAMERA")];
            _imageOrientationItemFlipH = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_H_STAGE")];
            _imageOrientationItemFlipV = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_V_EYEPIECES")];
            _imageOrientationItemFlipBoth = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_BOTH_CAMERA")];
        } else {
            _imageOrientationItemOriginal = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_ORIGINAL")];
            _imageOrientationItemFlipH = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_H")];
            _imageOrientationItemFlipV = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_V")];
            _imageOrientationItemFlipBoth = [CZDropdownItem itemWithTitle:L(@"MIC_LIVE_ORIENTATION_FLIP_BOTH")];
        }
        
        [[CZKeyboardManager sharedManager] addObserver:self];
    }
    return self;
}

- (void)dealloc {
    [[CZKeyboardManager sharedManager] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs100];
    
    void (^switchConfigurationHandler)(CZGridViewLayoutAttributes *layoutAttributes) = ^(CZGridViewLayoutAttributes *layoutAttributes) {
        layoutAttributes.columnAlignment = CZGridColumnAlignmentTrailing;
        layoutAttributes.rowAlignment = CZGridRowAlignmentCenter;
    };
    
    [self.view addSubview:self.section0];
    [self.section0 addContentView:self.microscopeNameLabel atRowIndex:0 columnIndex:0];
    [self.section0 addContentView:self.microscopeNameTextField atRowIndex:0 columnRange:NSMakeRange(1, 2)];
    [self.section0 addContentView:self.macAddressLabel atRowIndex:1 columnIndex:0];
    [self.section0 addContentView:self.macAddressTextField atRowIndex:1 columnRange:(NSMakeRange(1, 2))];
    [self.section0 addContentView:self.ipAddressLabel atRowIndex:2 columnIndex:0];
    [self.section0 addContentView:self.ipAddressTextField atRowIndex:2 columnRange:(NSMakeRange(1, 2))];
    [self.section0 addContentView:self.firmwareVersionLabel atRowIndex:3 columnIndex:0];
    [self.section0 addContentView:self.firmwareVersionTextField atRowIndex:3 columnRange:(NSMakeRange(1, 2))];
    
    switch (self.camera.cameraSettingsType) {
        case CZCameraSettingsTypeEncodedAxiocam202:
        case CZCameraSettingsTypeNonEncodedAxiocam202:
            [self.view addSubview:self.section1];
            [self.section1 addContentView:self.streamingQualityLabel atRowIndex:0 columnIndex:0];
            [self.section1 addContentView:self.streamingQualityDropdown atRowIndex:0 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.imageOrientationLabel atRowIndex:1 columnIndex:0];
            [self.section1 addContentView:self.imageOrientationDropdown atRowIndex:1 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.bitDepthLabel atRowIndex:2 columnIndex:0];
            [self.section1 addContentView:self.bitDepthDropdown atRowIndex:2 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.gammaLabel atRowIndex:3 columnIndex:0];
            [self.section1 addContentView:self.gammaSlider atRowIndex:3 columnIndex:1];
            [self.section1 addContentView:self.gammaTextField atRowIndex:3 columnIndex:2];
            
            [self.view addSubview:self.section2];
            [self.section2 addContentView:self.denoiseLabel atRowIndex:0 columnIndex:0];
            [self.section2 addContentView:self.denoiseSwitch atRowRange:NSMakeRange(0, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            [self.section2 addContentView:self.sharpeningLabel atRowIndex:1 columnIndex:0];
            [self.section2 addContentView:self.sharpeningSwitch atRowRange:NSMakeRange(1, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            [self.section2 addContentView:self.pixelCorrectionLabel atRowIndex:2 columnIndex:0];
            [self.section2 addContentView:self.pixelCorrectionSwitch atRowRange:NSMakeRange(2, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            
            self.sections = @[self.section0, self.section1, self.section2];
            
            break;
        case CZCameraSettingsTypeEncodedAxiocam208:
        case CZCameraSettingsTypeNonEncodedAxiocam208:
            [self.view addSubview:self.section1];
            [self.section1 addContentView:self.streamingQualityLabel atRowIndex:0 columnIndex:0];
            [self.section1 addContentView:self.streamingQualityDropdown atRowIndex:0 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.imageOrientationLabel atRowIndex:1 columnIndex:0];
            [self.section1 addContentView:self.imageOrientationDropdown atRowIndex:1 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.grayscaleModeLabel atRowIndex:2 columnIndex:0];
            [self.section1 addContentView:self.grayscaleModeDropdown atRowIndex:2 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.gammaLabel atRowIndex:3 columnIndex:0];
            [self.section1 addContentView:self.gammaSlider atRowIndex:3 columnIndex:1];
            [self.section1 addContentView:self.gammaTextField atRowIndex:3 columnIndex:2];
            
            [self.view addSubview:self.section2];
            [self.section2 addContentView:self.denoiseLabel atRowIndex:0 columnIndex:0];
            [self.section2 addContentView:self.denoiseSwitch atRowRange:NSMakeRange(0, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            [self.section2 addContentView:self.sharpeningLabel atRowIndex:1 columnIndex:0];
            [self.section2 addContentView:self.sharpeningSwitch atRowRange:NSMakeRange(1, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            [self.section2 addContentView:self.pixelCorrectionLabel atRowIndex:2 columnIndex:0];
            [self.section2 addContentView:self.pixelCorrectionSwitch atRowRange:NSMakeRange(2, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            [self.section2 addContentView:self.hdrLabel atRowIndex:3 columnIndex:0];
            [self.section2 addContentView:self.hdrSwitch atRowRange:NSMakeRange(3, 1) columnRange:NSMakeRange(1, 2) configurationHandler:switchConfigurationHandler];
            
            self.sections = @[self.section0, self.section1, self.section2];
            
            break;
        case CZCameraSettingsTypeKappa:
            [self.view addSubview:self.section1];
            [self.section1 addContentView:self.streamingQualityLabel atRowIndex:0 columnIndex:0];
            [self.section1 addContentView:self.streamingQualityDropdown atRowIndex:0 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.imageOrientationLabel atRowIndex:1 columnIndex:0];
            [self.section1 addContentView:self.imageOrientationDropdown atRowIndex:1 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.grayscaleModeLabel atRowIndex:2 columnIndex:0];
            [self.section1 addContentView:self.grayscaleModeDropdown atRowIndex:2 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.snapResolutionLabel atRowIndex:3 columnIndex:0];
            [self.section1 addContentView:self.snapResolutionDropdown atRowIndex:3 columnRange:NSMakeRange(1, 2)];
            
            [self.view addSubview:self.section2];
            [self.section2 addContentView:self.sharpeningLabel atRowIndex:0 columnIndex:0];
            [self.section2 addContentView:self.sharpeningSlider atRowIndex:0 columnIndex:1];
            [self.section2 addContentView:self.sharpeningTextField atRowIndex:0 columnIndex:2];
            
            self.sections = @[self.section0, self.section1, self.section2];
            
            break;
        case CZCameraSettingsTypeWision:
            [self.view addSubview:self.section1];
            [self.section1 addContentView:self.streamingQualityLabel atRowIndex:0 columnIndex:0];
            [self.section1 addContentView:self.streamingQualityDropdown atRowIndex:0 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.imageOrientationLabel atRowIndex:1 columnIndex:0];
            [self.section1 addContentView:self.imageOrientationDropdown atRowIndex:1 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.grayscaleModeLabel atRowIndex:2 columnIndex:0];
            [self.section1 addContentView:self.grayscaleModeDropdown atRowIndex:2 columnRange:NSMakeRange(1, 2)];
            
            [self.view addSubview:self.section2];
            [self.section2 addContentView:self.sharpeningLabel atRowIndex:0 columnIndex:0];
            [self.section2 addContentView:self.sharpeningSlider atRowIndex:0 columnIndex:1];
            [self.section2 addContentView:self.sharpeningTextField atRowIndex:0 columnIndex:2];
            
            self.sections = @[self.section0, self.section1, self.section2];
            
            break;
        case CZCameraSettingsTypeMotic:
            [self.view addSubview:self.section1];
            [self.section1 addContentView:self.grayscaleModeLabel atRowIndex:0 columnIndex:0];
            [self.section1 addContentView:self.grayscaleModeDropdown atRowIndex:0 columnRange:NSMakeRange(1, 2)];
            [self.section1 addContentView:self.sharpeningLabel atRowIndex:1 columnIndex:0];
            [self.section1 addContentView:self.sharpeningSlider atRowIndex:1 columnIndex:1];
            [self.section1 addContentView:self.sharpeningTextField atRowIndex:1 columnIndex:2];
//            [self.section1 addContentView:self.webConfigurationLabel atRowIndex:2 columnIndex:0];
            [self.section1 addContentView:self.webConfigurationLinkView atRowIndex:2 columnRange:NSMakeRange(0, 3)];
            
            self.sections = @[self.section0, self.section1];
            
            break;
        case CZCameraSettingsTypeOther:
            break;
    }
    
    [self.view addSubview:self.loadingIndicator];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadCameraSettings];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    if (_streamingQualityDropdown && self.streamingQualityDropdown.isExpanded) {
        [self.streamingQualityDropdown collapseMenu];
    }
    
    if (_imageOrientationDropdown && self.imageOrientationDropdown.isExpanded) {
        [self.imageOrientationDropdown collapseMenu];
    }
    
    if (_snapResolutionDropdown && self.snapResolutionDropdown.isExpanded) {
        [self.snapResolutionDropdown collapseMenu];
    }
    
    if (_bitDepthDropdown && self.bitDepthDropdown.isExpanded) {
        [self.bitDepthDropdown collapseMenu];
    }
    
    [self showWaitingHUD:NO];
}

#pragma mark - Public

- (void)loadCameraSettings {
    [self hideSections];
    [self.loadingIndicator startAnimating];
    
    dispatch_group_t group = dispatch_group_create();
    
    if (![self.camera.mockDisplayName isEqualToString:self.camera.displayName]) {
        self.microscopeNameTextField.text = self.camera.displayName;
    }
    self.microscopeNameTextField.placeholder = self.camera.mockDisplayName;
    
    self.macAddressTextField.text = self.camera.macAddress;
    
    self.ipAddressTextField.text = self.camera.ipAddress;
    
    dispatch_group_enter(group);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *firmwareVersion = [self.camera firmwareVersion];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.firmwareVersionTextField.text = firmwareVersion;
            dispatch_group_leave(group);
        });
    });
    
    if ([self.camera hasCapability:CZCameraCapabilityStreamingQuality]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CZCameraBitRate bitRate = [self.camera bitRate];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.streamingQualityDropdown.selectedIndex = bitRate;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityImageOrientation]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CZCameraImageOrientation imageOrientation = [self.camera imageOrientation];
            dispatch_async(dispatch_get_main_queue(), ^{
                CZDropdownItem *item = [self itemForImageOrientation:imageOrientation];
                NSArray<CZDropdownItem *> *items = self.imageOrientationDropdown.expandItems;
                NSInteger index = [items indexOfObject:item];
                if (index == NSNotFound) {
                    self.imageOrientationDropdown.selectedIndex = 0;
                    [self imageOrientationDropdownAction:nil];
                } else {
                    self.imageOrientationDropdown.selectedIndex = index;
                }
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilitySnapResolution] && ![self.camera isKindOfClass:[CZGeminiCamera class]]) {
        self.snapResolutionDropdown.selectedIndex = [self.camera snapResolutionPreset];
    }
    
    if ([self.camera hasCapability:CZCameraCapabilitySharpening]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            uint32_t sharpnessStepCount = [self.camera sharpnessStepCount];
            uint32_t sharpness = [self.camera sharpness];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (sharpnessStepCount > 2) {
                    self.sharpeningSlider.minimumValue = 0.0;
                    self.sharpeningSlider.maximumValue = [self.camera sharpnessStepCount] - 1;
                    self.sharpeningSlider.value = sharpness;
                    [self sharpeningSliderValueChangedAction:nil];
                } else {
                    self.sharpeningSwitch.on = sharpness > 0;
                }
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityDenoise]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL isDenoiseEnabled = [self.camera isDenoiseEnabled];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.denoiseSwitch.on = isDenoiseEnabled;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityPixelCorrection]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL isPixelCorrectionEnabled = [self.camera isPixelCorrectionEnabled];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.pixelCorrectionSwitch.on = isPixelCorrectionEnabled;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityHDR]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL isHDREnabled = [self.camera isHDREnabled];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.hdrSwitch.on = isHDREnabled;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityBitDepth]) {
        self.bitDepthDropdown.selectedIndex = [self.camera bitDepth];
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityGrayscaleMode]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL isGrayscaleModeEnabled = [self.camera isGrayscaleModeEnabled];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.grayscaleModeDropdown.selectedIndex = isGrayscaleModeEnabled ? kGrayscaleButtonIndex : kColorButtonIndex;
                dispatch_group_leave(group);
            });
        });
    }
    
    if ([self.camera hasCapability:CZCameraCapabilityGamma]) {
        dispatch_group_enter(group);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            float minGamma = [self.camera minGamma];
            float maxGamma = [self.camera maxGamma];
            float gamma = [self.camera gamma];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.gammaSlider.minimumValue = minGamma;
                self.gammaSlider.maximumValue = maxGamma;
                self.gammaSlider.value = gamma;
                [self gammaSliderValueChangedAction:nil];
                dispatch_group_leave(group);
            });
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.loadingIndicator stopAnimating];
        [self showSections];
    });
}

- (BOOL)shouldResetAutomaticDismissalTimer {
    if (_streamingQualityDropdown && self.streamingQualityDropdown.isExpanded) {
        return YES;
    }
    
    if (_imageOrientationDropdown && self.imageOrientationDropdown.isExpanded) {
        return YES;
    }
    
    if (_snapResolutionDropdown && self.snapResolutionDropdown.isExpanded) {
        return YES;
    }
    
    if (_bitDepthDropdown && self.bitDepthDropdown.isExpanded) {
        return YES;
    }
    
    if (self.loadingIndicator.isAnimating) {
        return YES;
    }
    
    if (self.isShowingWaitingHUD) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Private

- (void)hideSections {
    for (CZGridView *section in self.sections) {
        section.hidden = YES;
    }
}

- (void)showSections {
    for (CZGridView *section in self.sections) {
        section.hidden = NO;
    }
}

- (NSArray<CZDropdownItem *> *)imageOrientationDropdownItems {
    NSArray<CZDropdownItem *> *items;
    if ([self.camera bitDepth] == CZCameraBitDepth12Bit) {
        items = @[self.imageOrientationItemOriginal, self.imageOrientationItemFlipV];
    } else {
        items = @[self.imageOrientationItemOriginal, self.imageOrientationItemFlipH, self.imageOrientationItemFlipV, self.imageOrientationItemFlipBoth];
    }
    return items;
}

- (CZCameraImageOrientation)imageOrientationForItem:(CZDropdownItem *)item {
    if (item == self.imageOrientationItemOriginal) {
        return kCZCameraImageOrientationOriginal;
    } else if (item == self.imageOrientationItemFlipH) {
        return kCZCameraImageOrientationFlippedHorizontally;
    } else if (item == self.imageOrientationItemFlipV) {
        return kCZCameraImageOrientationFlippedVertically;
    } else if (item == self.imageOrientationItemFlipBoth) {
        return kCZCameraImageOrientationRotated180;
    } else {
        // Should not enter this else.
        return kCZCameraImageOrientationOriginal;
    }
}

- (CZDropdownItem *)itemForImageOrientation:(CZCameraImageOrientation)imageOrientation {
    switch (imageOrientation) {
        case kCZCameraImageOrientationOriginal:
            return self.imageOrientationItemOriginal;
        case kCZCameraImageOrientationFlippedHorizontally:
            return self.imageOrientationItemFlipH;
        case kCZCameraImageOrientationFlippedVertically:
            return self.imageOrientationItemFlipV;
        case kCZCameraImageOrientationRotated180:
            return self.imageOrientationItemFlipBoth;
    }
}

#pragma mark - Views

- (CZGridView *)makeGridViewWithFrame:(CGRect)frame needsDivider:(BOOL)needsDivider {
    CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
    NSArray<CZGridRow *> *rows = @[row0, row1, row2, row3];
    
    CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:96.0 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:112.0 alignment:CZGridColumnAlignmentFill];
    CZGridColumn *column2 = [[CZGridColumn alloc] initWithWidth:48.0 alignment:CZGridColumnAlignmentFill];
    NSArray<CZGridColumn *> *columns = @[column0, column1, column2];
    
    CZGridView *gridView = [[CZGridView alloc] initWithFrame:frame rows:rows columns:columns];
    
    gridView.rowSpacing = 16.0;
    gridView.columnSpacing = 16.0;
    [gridView setCustomSpacing:4.0 afterRowAtIndex:3];
    [gridView setCustomSpacing:4.0 afterRowAtIndex:4];
    [gridView setCustomSpacing:4.0 afterRowAtIndex:5];
    
    if (needsDivider) {
        UIView *divider = [[UIView alloc] initWithFrame:UIEdgeInsetsInsetRect(gridView.bounds, UIEdgeInsetsMake(0.0, -16.0, 0.0, gridView.bounds.size.width + 15.0))];
        divider.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        divider.backgroundColor = [UIColor cz_gs110];
        [gridView addSubview:divider];
    }
    
    return gridView;
}

- (UILabel *)makeLabelWithText:(NSString *)text {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont cz_label1];
    label.text = text;
    label.textColor = [UIColor cz_gs80];
    label.textAlignment = NSTextAlignmentLeft;
    return label;
}

- (CZGridView *)section0 {
    if (_section0 == nil) {
        _section0 = [self makeGridViewWithFrame:CGRectMake(32.0, 16.0, 288.0, 176.0) needsDivider:NO];
    }
    return _section0;
}

- (CZGridView *)section1 {
    if (_section1 == nil) {
        _section1 = [self makeGridViewWithFrame:CGRectMake(352.0, 16.0, 288.0, 176.0) needsDivider:YES];
    }
    return _section1;
}

- (CZGridView *)section2 {
    if (_section2 == nil) {
        _section2 = [self makeGridViewWithFrame:CGRectMake(672.0, 16.0, 288.0, 176.0) needsDivider:YES];
    }
    return _section2;
}

- (UILabel *)microscopeNameLabel {
    if (_microscopeNameLabel == nil) {
        _microscopeNameLabel = [self makeLabelWithText:L(@"MIC_CONFIG_NAME")];
    }
    return _microscopeNameLabel;
}

- (CZTextField *)microscopeNameTextField {
    if (_microscopeNameTextField == nil) {
        _microscopeNameTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _microscopeNameTextField.delegate = self;
        _microscopeNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }
    return _microscopeNameTextField;
}

- (CZMicroscopeNameValidator *)microscopeNameValidator {
    if (_microscopeNameValidator == nil) {
        _microscopeNameValidator = [[CZMicroscopeNameValidator alloc] initWithMaxSupportedNameLength:[self.camera maxSupportedNameLength]];
    }
    return _microscopeNameValidator;
}

- (UILabel *)macAddressLabel {
    if (_macAddressLabel == nil) {
        _macAddressLabel = [self makeLabelWithText:L(@"MIC_LIVE_HWADDRESS")];
    }
    return _macAddressLabel;
}

- (CZTextField *)macAddressTextField {
    if (_macAddressTextField == nil) {
        _macAddressTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleReadOnly];
        _macAddressTextField.userInteractionEnabled = NO;
    }
    return _macAddressTextField;
}

- (UILabel *)ipAddressLabel {
    if (_ipAddressLabel == nil) {
        _ipAddressLabel = [self makeLabelWithText:L(@"MIC_LIVE_IPADDRESS")];
    }
    return _ipAddressLabel;
}

- (CZTextField *)ipAddressTextField {
    if (_ipAddressTextField == nil) {
        _ipAddressTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleReadOnly];
        _ipAddressTextField.userInteractionEnabled = NO;
    }
    return _ipAddressTextField;
}

- (UILabel *)firmwareVersionLabel {
    if (_firmwareVersionLabel == nil) {
        _firmwareVersionLabel = [self makeLabelWithText:L(@"MIC_LIVE_FIRMWARE_VERSION")];
    }
    return _firmwareVersionLabel;
}

- (CZTextField *)firmwareVersionTextField {
    if (_firmwareVersionTextField == nil) {
        _firmwareVersionTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleReadOnly];
        _firmwareVersionTextField.userInteractionEnabled = NO;
    }
    return _firmwareVersionTextField;
}

- (UILabel *)streamingQualityLabel {
    if (_streamingQualityLabel == nil) {
        _streamingQualityLabel = [self makeLabelWithText:L(@"MIC_LIVE_IMAGE_QUALITY")];
    }
    return _streamingQualityLabel;
}

- (CZDropdownMenu *)streamingQualityDropdown {
    if (_streamingQualityDropdown == nil) {
        NSString *bitRateHigh = [NSString stringWithFormat:L(@"MIC_LIVE_IMAGE_QUALITY_HIGH"),
                                 [CZCommonUtils localizedStringFromFloat:[[self.camera class] bitRateValueForLevel:kCZCameraBitRateHigh] precision:1]];
        NSString *bitRateMedium = [NSString stringWithFormat:L(@"MIC_LIVE_IMAGE_QUALITY_MEDIUM"),
                                   [CZCommonUtils localizedStringFromFloat:[[self.camera class] bitRateValueForLevel:kCZCameraBitRateMedium] precision:1]];
        NSString *bitRateLow = [NSString stringWithFormat:L(@"MIC_LIVE_IMAGE_QUALITY_LOW"),
                                [CZCommonUtils localizedStringFromFloat:[[self.camera class] bitRateValueForLevel:kCZCameraBitRateLow] precision:1]];
        NSArray *bitRateOptions = @[bitRateHigh, bitRateMedium, bitRateLow];
        _streamingQualityDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:bitRateOptions];
        _streamingQualityDropdown.automaticallyTriggersPrimaryAction = NO;
        [_streamingQualityDropdown addTarget:self action:@selector(dropdownPrimaryAction:) forControlEvents:UIControlEventPrimaryActionTriggered];
        [_streamingQualityDropdown addTarget:self action:@selector(streamingQualityDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _streamingQualityDropdown;
}

- (UILabel *)imageOrientationLabel {
    if (_imageOrientationLabel == nil) {
        _imageOrientationLabel = [self makeLabelWithText:L(@"MIC_LIVE_ORIENTATION")];
    }
    return _imageOrientationLabel;
}

- (CZDropdownMenu *)imageOrientationDropdown {
    if (_imageOrientationDropdown == nil) {
        NSArray<CZDropdownItem *> *items = [self imageOrientationDropdownItems];
        _imageOrientationDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandItems:items];
        _imageOrientationDropdown.automaticallyTriggersPrimaryAction = NO;
        [_imageOrientationDropdown addTarget:self action:@selector(dropdownPrimaryAction:) forControlEvents:UIControlEventPrimaryActionTriggered];
        [_imageOrientationDropdown addTarget:self action:@selector(imageOrientationDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _imageOrientationDropdown;
}

- (UILabel *)snapResolutionLabel {
    if (_snapResolutionLabel == nil) {
        _snapResolutionLabel = [self makeLabelWithText:L(@"MIC_LIVE_SNAP_RESOLUTION")];
    }
    return _snapResolutionLabel;
}

- (CZDropdownMenu *)snapResolutionDropdown {
    if (_snapResolutionDropdown == nil) {
        NSString *resolutionHigh = [NSString stringWithFormat:@"%d \u00D7 %d", (int)[self.camera highSnapResolution].width, (int)[self.camera highSnapResolution].height];
        NSString *resolutionLow = [NSString stringWithFormat:@"%d \u00D7 %d", (int)[self.camera lowSnapResolution].width, (int)[self.camera lowSnapResolution].height];
        NSArray *resolutionOptions = @[resolutionHigh, resolutionLow];
        _snapResolutionDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:resolutionOptions];
        _snapResolutionDropdown.automaticallyTriggersPrimaryAction = NO;
        [_snapResolutionDropdown addTarget:self action:@selector(dropdownPrimaryAction:) forControlEvents:UIControlEventPrimaryActionTriggered];
        [_snapResolutionDropdown addTarget:self action:@selector(snapResolutionDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _snapResolutionDropdown;
}

- (UILabel *)sharpeningLabel {
    if (_sharpeningLabel == nil) {
        _sharpeningLabel = [self makeLabelWithText:L(@"MIC_LIVE_SHARPENING")];
    }
    return _sharpeningLabel;
}

- (CZSwitch *)sharpeningSwitch {
    if (_sharpeningSwitch == nil) {
        _sharpeningSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_sharpeningSwitch setLevel:CZControlEmphasisActivePrimary];
        [_sharpeningSwitch addTarget:self action:@selector(sharpeningSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _sharpeningSwitch;
}

- (CZSlider *)sharpeningSlider {
    if (_sharpeningSlider == nil) {
        _sharpeningSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_sharpeningSlider addTarget:self action:@selector(sharpeningSliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_sharpeningSlider addTarget:self action:@selector(sharpeningSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_sharpeningSlider addTarget:self action:@selector(sharpeningSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_sharpeningSlider addTarget:self action:@selector(sharpeningSliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _sharpeningSlider;
}

- (CZTextField *)sharpeningTextField {
    if (_sharpeningTextField == nil) {
        _sharpeningTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _sharpeningTextField.delegate = self;
        _sharpeningTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_sharpeningTextField addTarget:self action:@selector(sharpeningTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _sharpeningTextField;
}

- (UILabel *)denoiseLabel {
    if (_denoiseLabel == nil) {
        _denoiseLabel = [self makeLabelWithText:L(@"MIC_LIVE_DENOISE_MODE")];
    }
    return _denoiseLabel;
}

- (CZSwitch *)denoiseSwitch {
    if (_denoiseSwitch == nil) {
        _denoiseSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_denoiseSwitch setLevel:CZControlEmphasisActivePrimary];
        [_denoiseSwitch addTarget:self action:@selector(denoiseSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _denoiseSwitch;
}

- (UILabel *)pixelCorrectionLabel {
    if (_pixelCorrectionLabel == nil) {
        _pixelCorrectionLabel = [self makeLabelWithText:L(@"MIC_LIVE_PIXEL_CORRECTION")];
    }
    return _pixelCorrectionLabel;
}

- (CZSwitch *)pixelCorrectionSwitch {
    if (_pixelCorrectionSwitch == nil) {
        _pixelCorrectionSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_pixelCorrectionSwitch setLevel:CZControlEmphasisActivePrimary];
        [_pixelCorrectionSwitch addTarget:self action:@selector(pixelCorrectionSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _pixelCorrectionSwitch;
}

- (UILabel *)hdrLabel {
    if (_hdrLabel == nil) {
        _hdrLabel = [self makeLabelWithText:L(@"MIC_LIVE_HDR_MODE")];
    }
    return _hdrLabel;
}

- (CZSwitch *)hdrSwitch {
    if (_hdrSwitch == nil) {
        _hdrSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_hdrSwitch setLevel:CZControlEmphasisActivePrimary];
        [_hdrSwitch addTarget:self action:@selector(hdrSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _hdrSwitch;
}

- (UILabel *)bitDepthLabel {
    if (_bitDepthLabel == nil) {
        _bitDepthLabel = [self makeLabelWithText:L(@"FILES_INFO_BITDEPTH")];
    }
    return _bitDepthLabel;
}

- (CZDropdownMenu *)bitDepthDropdown {
    if (_bitDepthDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"MIC_LIVE_BIT_DEPTH_8"), L(@"MIC_LIVE_BIT_DEPTH_12")];
        _bitDepthDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        _bitDepthDropdown.automaticallyTriggersPrimaryAction = NO;
        [_bitDepthDropdown addTarget:self action:@selector(dropdownPrimaryAction:) forControlEvents:UIControlEventPrimaryActionTriggered];
        [_bitDepthDropdown addTarget:self action:@selector(bitDepthDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _bitDepthDropdown;
}

- (UILabel *)grayscaleModeLabel {
    if (_grayscaleModeLabel == nil) {
        _grayscaleModeLabel = [self makeLabelWithText:L(@"MIC_LIVE_COLOR_MODE")];
    }
    return _grayscaleModeLabel;
}

- (CZDropdownMenu *)grayscaleModeDropdown {
    if (_grayscaleModeDropdown == nil) {
        NSArray<NSString *> *options = @[L(@"MIC_LIVE_COLOR_MODE"), L(@"MIC_LIVE_GRAYSCALE_MODE")];
        _grayscaleModeDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:options];
        _grayscaleModeDropdown.automaticallyTriggersPrimaryAction = NO;
        [_grayscaleModeDropdown addTarget:self action:@selector(dropdownPrimaryAction:) forControlEvents:UIControlEventPrimaryActionTriggered];
        [_grayscaleModeDropdown addTarget:self action:@selector(grayscaleModeDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _grayscaleModeDropdown;
}

- (UILabel *)gammaLabel {
    if (_gammaLabel == nil) {
        _gammaLabel = [self makeLabelWithText:L(@"IMAGE_GAMMA")];
    }
    return _gammaLabel;
}

- (CZSlider *)gammaSlider {
    if (_gammaSlider == nil) {
        _gammaSlider = [[CZSlider alloc] initWithType:CZSliderHorizontalOneHandle];
        [_gammaSlider addTarget:self action:@selector(gammaSliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        [_gammaSlider addTarget:self action:@selector(gammaSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpInside];
        [_gammaSlider addTarget:self action:@selector(gammaSliderTouchEndAction:) forControlEvents:UIControlEventTouchUpOutside];
        [_gammaSlider addTarget:self action:@selector(gammaSliderTouchEndAction:) forControlEvents:UIControlEventTouchCancel];
    }
    return _gammaSlider;
}

- (CZTextField *)gammaTextField {
    if (_gammaTextField == nil) {
        _gammaTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _gammaTextField.delegate = self;
        _gammaTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_gammaTextField addTarget:self action:@selector(gammaTextFieldEditingDidEndAction:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _gammaTextField;
}

- (UILabel *)webConfigurationLabel {
    if (_webConfigurationLabel == nil) {
        _webConfigurationLabel = [self makeLabelWithText:L(@"MIC_LIVE_WEB_INTERFACE")];
    }
    return _webConfigurationLabel;
}

- (UITextView *)webConfigurationLinkView {
    if (_webConfigurationLinkView == nil) {
        _webConfigurationLinkView = [[UITextView alloc] init];
        _webConfigurationLinkView.backgroundColor = [UIColor clearColor];
        _webConfigurationLinkView.scrollEnabled = NO;
        _webConfigurationLinkView.delegate = self;
        _webConfigurationLinkView.contentInset = UIEdgeInsetsZero;
        _webConfigurationLinkView.textContainerInset = UIEdgeInsetsZero;
        _webConfigurationLinkView.textContainer.lineFragmentPadding = 0.0;
        _webConfigurationLinkView.editable = NO;
        _webConfigurationLinkView.linkTextAttributes = @{
            NSForegroundColorAttributeName : [UIColor cz_pb100]
        };
        
        NSURL *url = [self.camera webInterfaceURL];
        if (url) {
            NSString *text = [NSString stringWithFormat:@"%@\n%@: admin / %@: ZEISS1846", L(@"MIC_LIVE_WEB_INTERFACE"), L(@"USERNAME"), L(@"PASSWORD")];
            NSDictionary *attributes = @{
                NSFontAttributeName : [UIFont cz_label1],
                NSForegroundColorAttributeName : [UIColor cz_gs80]
            };
            NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
            [attributedText addAttribute:NSLinkAttributeName value:url range:[text rangeOfString:L(@"MIC_LIVE_WEB_INTERFACE")]];
            _webConfigurationLinkView.attributedText = attributedText;
        }
    }
    return _webConfigurationLinkView;
}

- (UIActivityIndicatorView *)loadingIndicator {
    if (_loadingIndicator == nil) {
        _loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _loadingIndicator.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
        _loadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        _loadingIndicator.hidesWhenStopped = YES;
        [_loadingIndicator stopAnimating];
    }
    return _loadingIndicator;
}

#pragma mark - Actions

- (void)dropdownPrimaryAction:(CZDropdownMenu *)dropdown {
    if ([self.microscopeNameTextField isFirstResponder] || [self.sharpeningTextField isFirstResponder] || [self.gammaTextField isFirstResponder]) {
        self.pendingDropdown = dropdown;
        [self.view endEditing:YES];
    } else {
        [dropdown expandMenu];
    }
}

- (void)streamingQualityDropdownAction:(id)sender {
    NSUInteger index = self.streamingQualityDropdown.selectedIndex;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setBitRate:index];
    });
}

- (void)imageOrientationDropdownAction:(id)sender {
    CZDropdownItem *selectedItem = self.imageOrientationDropdown.expandItems[self.imageOrientationDropdown.selectedIndex];
    CZCameraImageOrientation imageOrientation = [self imageOrientationForItem:selectedItem];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setImageOrientation:imageOrientation];
    });
}

- (void)snapResolutionDropdownAction:(id)sender {
    NSUInteger index = self.snapResolutionDropdown.selectedIndex;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setSnapResolutionPreset:index];
    });
}

- (void)sharpeningSwitchAction:(id)sender {
    uint32_t sharpness = self.sharpeningSwitch.isOn ? 1 : 0;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setSharpness:sharpness];
    });
}

- (void)sharpeningSliderValueChangedAction:(id)sender {
    CGFloat roundedValue = round(self.sharpeningSlider.value);
    self.sharpeningSlider.value = roundedValue;
    
    // Update text field accordingly.
    NSString *sharpeningText = [CZCommonUtils localizedStringFromFloat:(roundedValue + 1.0) precision:1];
    if (![sharpeningText isEqualToString:self.sharpeningTextField.text]) {
        self.sharpeningTextField.text = sharpeningText;
    }
}

- (void)sharpeningSliderTouchEndAction:(id)sender {
    float value = self.sharpeningSlider.value;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setSharpness:round(value)];
    });
}

- (void)sharpeningTextFieldEditingDidEndAction:(id)sender {
    float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.sharpeningTextField.text] floatValue] - 1.0;
    preferredValue = MAX(preferredValue, self.sharpeningSlider.minimumValue);
    preferredValue = MIN(preferredValue, self.sharpeningSlider.maximumValue);
    preferredValue = roundf(preferredValue);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setSharpness:round(preferredValue)];
    });
    
    CZLogv(@"Sharpening changed to %f by text field.", preferredValue);
    
    NSString *sharpeningText = [CZCommonUtils localizedStringFromFloat:(preferredValue + 1.0) precision:1];
    if (![sharpeningText isEqualToString:self.sharpeningTextField.text]) {
        self.sharpeningTextField.text = sharpeningText;
    }
    
    // Update slider accordingly. Doesn't set if nothing changes.
    if (preferredValue != self.sharpeningSlider.value) {
        self.sharpeningSlider.value = preferredValue;
    }
}

- (void)denoiseSwitchAction:(id)sender {
    BOOL isDenoiseEnabled = self.denoiseSwitch.isOn;
    [self.camera setDenoiseEnabled:isDenoiseEnabled];
}

- (void)pixelCorrectionSwitchAction:(id)sender {
    BOOL isPixelCorrectionEnabled = self.pixelCorrectionSwitch.isOn;
    [self.camera setPixelCorrectionEnabled:isPixelCorrectionEnabled];
}

- (void)hdrSwitchAction:(id)sender {
    BOOL isHDREnabled = self.hdrSwitch.isOn;
    [self.camera setHDREnabled:isHDREnabled];
}

- (void)bitDepthDropdownAction:(id)sender {
    CZCameraBitDepth bitDepth = self.bitDepthDropdown.selectedIndex;
    [self.camera setBitDepth:bitDepth];
    
    CZDropdownItem *previousSelectedItem = self.imageOrientationDropdown.expandItems[self.imageOrientationDropdown.selectedIndex];
    NSArray<CZDropdownItem *> *currentItems = [self imageOrientationDropdownItems];
    
    [self.imageOrientationDropdown removeAllItems];
    for (CZDropdownItem *item in currentItems) {
        [self.imageOrientationDropdown addItem:item];
    }
    [self.imageOrientationDropdown invalidateMenu];
    
    NSInteger index = [currentItems indexOfObject:previousSelectedItem];
    if (index == NSNotFound) {
        self.imageOrientationDropdown.selectedIndex = 0;
        [self imageOrientationDropdownAction:nil];
    } else {
        self.imageOrientationDropdown.selectedIndex = index;
    }
}

- (void)grayscaleModeDropdownAction:(id)sender {
    BOOL isGrayscaleModeEnabled = self.grayscaleModeDropdown.selectedIndex == kGrayscaleButtonIndex;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.camera setGrayscaleModeEnabled:isGrayscaleModeEnabled];
    });
}

- (void)gammaSliderValueChangedAction:(id)sender {
    // Update text field accordingly.
    float value = self.gammaSlider.value;
    NSString *gammaText = [CZCommonUtils localizedStringFromFloat:value precision:2];
    if (![gammaText isEqualToString:self.gammaTextField.text]) {
        self.gammaTextField.text = gammaText;
    }
}

- (void)gammaSliderTouchEndAction:(id)sender {
    float value = self.gammaSlider.value;
    [self.camera setGamma:value];
}

- (void)gammaTextFieldEditingDidEndAction:(id)sender {
    float preferredValue = [[CZCommonUtils numberFromLocalizedString:self.gammaTextField.text] floatValue];
    preferredValue = MAX(preferredValue, self.gammaSlider.minimumValue);
    preferredValue = MIN(preferredValue, self.gammaSlider.maximumValue);
    
    [self.camera setGamma:preferredValue];
    
    CZLogv(@"Gamma changed to %f by text field.", preferredValue);
    
    NSString *gammaText = [CZCommonUtils localizedStringFromFloat:preferredValue precision:2];
    if (![gammaText isEqualToString:self.gammaTextField.text]) {
        self.gammaTextField.text = gammaText;
    }
    
    // Update slider accordingly. Doesn't set if nothing changes.
    if (preferredValue != self.gammaSlider.value) {
        self.gammaSlider.value = preferredValue;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.microscopeNameTextField) {
        [self.microscopeNameValidator textFieldDidEndEditing:textField];
        if (textField.text.length > 0 && ![textField.text isEqualToString:self.camera.displayName]) {
            [self.camera setDisplayName:textField.text andUpload:YES];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string {
    if (textField == self.microscopeNameTextField) {
        return [self.microscopeNameValidator textField:textField shouldChangeCharactersInRange:range replacementString:string];
    } else if (textField.keyboardType == UIKeyboardTypeNumbersAndPunctuation) {
        return [CZCommonUtils numericTextField:textField shouldAcceptChange:string];
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    return YES;
}

#pragma mark - CZKeyboardObserver

- (void)keyboardDidHide:(CZKeyboardTransition *)transition {
    if (self.pendingDropdown) {
        [self.pendingDropdown expandMenu];
        self.pendingDropdown = nil;
    }
}

@end
