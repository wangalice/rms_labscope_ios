//
//  CZCamera+CameraSettingsType.m
//  Hermes
//
//  Created by Li, Junlin on 4/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCamera+CameraSettingsType.h"

@implementation CZCamera (CameraSettingsType)

- (CZCameraSettingsType)cameraSettingsType {
    if ([self isKindOfClass:[CZGeminiCamera class]]) {
        CZGeminiCamera *camera = (CZGeminiCamera *)self;
        switch (camera.systemInfo.camera.modelType) {
            case CZGeminiAPICameraModelTypeUnknown:
                return CZCameraSettingsTypeOther;
            case CZGeminiAPICameraModelTypeAxiocam202:
                if ([camera hasCapability:CZCameraCapabilityEncoding]) {
                    return CZCameraSettingsTypeEncodedAxiocam202;
                } else {
                    return CZCameraSettingsTypeNonEncodedAxiocam202;
                }
            case CZGeminiAPICameraModelTypeAxiocam208:
                if ([camera hasCapability:CZCameraCapabilityEncoding]) {
                    return CZCameraSettingsTypeEncodedAxiocam208;
                } else {
                    return CZCameraSettingsTypeNonEncodedAxiocam208;
                }
        }
    } else if ([self isKindOfClass:[CZKappaCamera class]]) {
        return CZCameraSettingsTypeKappa;
    } else if ([self isKindOfClass:[CZWisionCamera class]]) {
        return CZCameraSettingsTypeWision;
    } else if ([self isKindOfClass:[CZMoticCamera class]]) {
        return CZCameraSettingsTypeMotic;
    } else {
        return CZCameraSettingsTypeOther;
    }
}

@end
