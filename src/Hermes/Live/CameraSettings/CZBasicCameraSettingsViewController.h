//
//  CZBasicCameraSettingsViewController.h
//  Hermes
//
//  Created by Li, Junlin on 3/13/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZCameraSettingsViewControllerDelegate.h"

@interface CZBasicCameraSettingsViewController : UIViewController <CZCameraControlDelegate>

@property (nonatomic, weak) id<CZCameraSettingsViewControllerDelegate> delegate;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithCamera:(CZCamera *)camera NS_DESIGNATED_INITIALIZER;

- (void)loadCameraSettings;
- (BOOL)shouldResetAutomaticDismissalTimer;

@end
