//
//  CZLiveSnapPreviewView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/4/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveSnapPreviewView.h"
#import <CZIKit/CZIKit.h>
#import "CZMultitaskViewController.h"

static const CGFloat kDefaultSnapPreviewWidth = 264.0;
static const CGFloat kDefaultSnapPreviewHeight = 247.0;
static const CGFloat kDefaultSnapPreviewPadding = 2.0;
static const CGFloat kDefaultSnapPreviewMargin = 8.0;
static const CGFloat kDefaultSnapOperationHeight = 48.0;
static const CGFloat kDefaultOperationButtonWidth = 44.0;
static const CGFloat kDefaultOperationButtonHeight = 44.0;
static const CGFloat kDefaultAnimationDuration = 0.3f;
static const CGFloat kDefaultHideExpireTimeout = 2.0;
static const CGFloat kDefaultHideExpireTimeInterval = 1.0;

@interface CZLiveSnapPreviewView ()<
UIGestureRecognizerDelegate
>

@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *zoomButton;
@property (nonatomic, strong) UIButton *undoButton;
@property (nonatomic, strong) UIImageView *previewImageView;
@property (nonatomic, strong) NSTimer *hideExpireTimer;
@property (nonatomic, strong) NSDate *lastActiveTime;
@property (nonatomic, assign) CGRect originalFrame;
@property (nonatomic, assign) CGRect fullScreenRect;
@property (nonatomic, assign) CGSize defaultContentSize;
@property (nonatomic, assign) UIEdgeInsets defaultPadding;
@property (nonatomic, strong) NSLayoutConstraint *previewImageViewBottomConstraint;
@property (nonatomic, assign) BOOL isFullScreen;

@end

@implementation CZLiveSnapPreviewView

- (instancetype)init {
    if (self = [super init]) {
        _shouldShowOperationHeaderView = NO;
        _imageFromRecording = NO;
        self.hidden = YES;
        [self setup];
    }
    return self;
}

#pragma mark - Public
- (void)updatePreviewWithImage:(UIImage *)image {
    if (image == nil || _isFullScreen) return;
    
    if (image.czi_componentBitCount == CZIComponentBitCount12) {
        image = [CZCommonUtils imageFromGrayImage:image];
    }
    
    self.previewImageView.image = image;
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
    CGFloat imageRatio = image.size.width / image.size.height;
    [self updateSubViewsWithImageRatio:imageRatio];
}

- (void)presentPreview {
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];

    if (self.hidden == NO) return;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kDefaultAnimationDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         weakSelf.hidden = NO;
                     }
                     completion:^(BOOL finished) {}];
    
    if (_hideExpireTimer == nil) {
        _hideExpireTimer = [NSTimer scheduledTimerWithTimeInterval:kDefaultHideExpireTimeInterval
                                                            target:self
                                                          selector:@selector(hideExpireTimerHandler:)
                                                          userInfo:nil
                                                           repeats:YES];
    }
}

- (void)dismissPreview {
    [self.zoomButton cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-in"]];
    _isFullScreen = NO;
    self.imageFromRecording = NO;
    if (self.hidden == YES) return;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kDefaultAnimationDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                          weakSelf.hidden = YES;
                     }
                     completion:^(BOOL finished) {
                         weakSelf.frame = weakSelf.originalFrame;
                     }];
    [self.hideExpireTimer invalidate];
    self.hideExpireTimer = nil;
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveSnapPreviewDidDismiss)]) {
        [self.delegate liveSnapPreviewDidDismiss];
    }
}

- (void)setShouldShowOperationHeaderView:(BOOL)shouldShowOperationHeaderView {
    if (_shouldShowOperationHeaderView != shouldShowOperationHeaderView) {
        CGFloat previewHeight = shouldShowOperationHeaderView ? CGRectGetHeight(self.previewImageView.frame) + kDefaultSnapPreviewPadding * 2 + kDefaultSnapOperationHeight : CGRectGetHeight(self.previewImageView.frame) + kDefaultSnapPreviewPadding * 2;
        CGRect frame = self.frame;
        frame.size.height = previewHeight;
        frame.origin.y += CGRectGetHeight(self.frame) - previewHeight;
        self.frame = frame;

        [self removeConstraint:self.previewImageViewBottomConstraint];
        if (shouldShowOperationHeaderView) {
            self.previewImageViewBottomConstraint = [self.previewImageView.bottomAnchor constraintEqualToAnchor:self.cancelButton.topAnchor constant:-(kDefaultSnapOperationHeight - kDefaultOperationButtonHeight) /2.0];
        } else {
            self.previewImageViewBottomConstraint = [self.previewImageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-kDefaultSnapPreviewPadding];
        }
        self.previewImageViewBottomConstraint.active = YES;
    }

    _shouldShowOperationHeaderView = shouldShowOperationHeaderView;

    self.cancelButton.hidden = !_shouldShowOperationHeaderView;
    self.saveButton.hidden = !_shouldShowOperationHeaderView;
    self.zoomButton.hidden = !_shouldShowOperationHeaderView;
}

- (void)setUndoActionSupported:(BOOL)undoActionSupported {
    _undoActionSupported = undoActionSupported;
    self.undoButton.hidden = !undoActionSupported;
}

- (UIEdgeInsets)defaultPadding {
   return UIEdgeInsetsMake(kDefaultSnapPreviewMargin,
                           kDefaultSnapPreviewMargin,
                           -kDefaultSnapPreviewMargin,
                           -kDefaultSnapPreviewMargin);
}

- (CGSize)defaultContentSize {
    return CGSizeMake(kDefaultSnapPreviewWidth, kDefaultSnapPreviewHeight);
}

#pragma mark - Private

- (void)setup {
    
    UIEdgeInsets padding = [self defaultPadding];
    CGSize size = [self defaultContentSize];
    self.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - size.width + padding.right,
                            [UIScreen mainScreen].bounds.size.height - size.height + padding.bottom - CZMainToolbarHeight,
                            size.width,
                            size.height);
    self.originalFrame = self.frame;
    self.fullScreenRect = CGRectMake(padding.left,
                                     padding.top,
                                     [UIScreen mainScreen].bounds.size.width - padding.left + padding.right,
                                     [UIScreen mainScreen].bounds.size.height - padding.top + padding.bottom - CZMainToolbarHeight);
    
    // Add the layout contraints
    self.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs100]);
    
    [self addSubview:self.cancelButton];
    [self addSubview:self.zoomButton];
    [self addSubview:self.saveButton];
    [self addSubview:self.previewImageView];
    [self addSubview:self.undoButton];
    
    [self addLayoutConstraints];
    
    [self.hideExpireTimer invalidate];
    self.hideExpireTimer = nil;
    self.undoActionSupported = NO;
}

- (void)addLayoutConstraints {
    self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.zoomButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.saveButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.undoButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.previewImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.cancelButton.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:kDefaultSnapPreviewMargin].active = YES;
    [self.cancelButton.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-(kDefaultSnapOperationHeight - kDefaultOperationButtonHeight) /2.0].active = YES;
    [self.cancelButton.widthAnchor constraintEqualToConstant:kDefaultOperationButtonWidth].active = YES;
    [self.cancelButton.heightAnchor constraintEqualToConstant:kDefaultOperationButtonHeight].active = YES;
    
    [self.zoomButton.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.zoomButton.bottomAnchor constraintEqualToAnchor:self.cancelButton.bottomAnchor].active = YES;
    [self.zoomButton.widthAnchor constraintEqualToConstant:kDefaultOperationButtonWidth].active = YES;
    [self.zoomButton.heightAnchor constraintEqualToConstant:kDefaultOperationButtonHeight].active = YES;
    
    [self.saveButton.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-kDefaultSnapPreviewMargin].active = YES;
    [self.saveButton.bottomAnchor constraintEqualToAnchor:self.cancelButton.bottomAnchor].active = YES;
    [self.saveButton.widthAnchor constraintEqualToConstant:kDefaultOperationButtonWidth].active = YES;
    [self.saveButton.heightAnchor constraintEqualToConstant:kDefaultOperationButtonHeight].active = YES;
    
    [self.undoButton.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:kDefaultSnapPreviewMargin].active = YES;
    [self.undoButton.topAnchor constraintEqualToAnchor:self.topAnchor constant:kDefaultSnapPreviewMargin].active = YES;
    [self.undoButton.widthAnchor constraintEqualToConstant:kDefaultOperationButtonWidth].active = YES;
    [self.undoButton.heightAnchor constraintEqualToConstant:kDefaultOperationButtonHeight].active = YES;
    
    [self.previewImageView.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:kDefaultSnapPreviewPadding].active = YES;
    [self.previewImageView.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-kDefaultSnapPreviewPadding].active = YES;
    [self.previewImageView.topAnchor constraintEqualToAnchor:self.topAnchor constant:kDefaultSnapPreviewPadding].active = YES;
    self.previewImageViewBottomConstraint = [self.previewImageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-kDefaultSnapPreviewPadding];
    self.previewImageViewBottomConstraint.active = YES;
}

- (void)enterFullScreen:(BOOL)fullScreen {
    _isFullScreen = fullScreen;
    CGRect frame = fullScreen ? _fullScreenRect : _originalFrame;
    self.zoomButton.selected = fullScreen;
    if (fullScreen) {
        [self.zoomButton cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-out"]];
    } else {
        [self.zoomButton cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-in"]];
    }
    self.cancelButton.hidden = NO;
    self.saveButton.hidden = NO;
    self.zoomButton.hidden = NO;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kDefaultAnimationDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         // TODO: Refactor this, becasue use this method to support the animation
                         // is memory-consuming operation
                         weakSelf.frame = frame;
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL completed) {
                         weakSelf.cancelButton.hidden = NO;
                         weakSelf.saveButton.hidden = NO;
                         weakSelf.zoomButton.hidden = NO;
                     }];
}

- (void)updateSubViewsWithImageRatio:(CGFloat)imageRatio {
    CGFloat imagePreviewWidth = kDefaultSnapPreviewWidth - kDefaultSnapPreviewPadding * 2;
    CGFloat imagePreviewHeight = imagePreviewWidth / imageRatio;
    
    CGFloat previewHeight = _shouldShowOperationHeaderView ? imagePreviewHeight + kDefaultSnapPreviewPadding * 2 + kDefaultSnapOperationHeight : imagePreviewHeight + kDefaultSnapPreviewPadding * 2;
    CGRect frame = self.frame;
    frame.size.height = previewHeight;
    frame.origin.y += CGRectGetHeight(self.frame) - previewHeight;
    self.frame = frame;
    self.originalFrame = frame;
}

#pragma mark - Action
- (void)saveButtonClickEvent:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(liveSnapPreviewDidSaveImage:)]) {
        [self.delegate liveSnapPreviewDidSaveImage:self.previewImageView.image];
    }
}

- (void)cancelButtonClickEvent:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(liveSnapPreviewDidDeleteImage:)]) {
        [self.delegate liveSnapPreviewDidDeleteImage:self.previewImageView.image];
    }
}

- (void)zoomButtonClickEvent:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self enterFullScreen:sender.selected];
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveSnapPreviewDidEnterFullScreen:)]) {
        [self.delegate liveSnapPreviewDidEnterFullScreen:_isFullScreen];
    }
}

- (void)undoButtonClickEvent:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(liveSnapPreviewDidSelectUndoButton)]) {
        [self.delegate liveSnapPreviewDidSelectUndoButton];
    }
}

- (void)tapPreviewImageViewEvent:(UITapGestureRecognizer *)sender {
    if (_shouldShowOperationHeaderView) return;
    if ([self.delegate respondsToSelector:@selector(liveSnapPreviewDidSelectPreviewImage:)]) {
        [self.delegate liveSnapPreviewDidSelectPreviewImage:self.previewImageView.image];
    }
}

#pragma mark - Timer

- (void)hideExpireTimerHandler:(NSTimer *)timer {
    if (_shouldShowOperationHeaderView) return;
    
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
    
    if (self.lastActiveTime == nil) {
        self.lastActiveTime = now;
        return;
    }
    
    if ([self.lastActiveTime timeIntervalSinceNow] < -kDefaultHideExpireTimeout) {
        [self dismissPreview];
        [timer invalidate];
        timer = nil;
    }
}

#pragma mark - UIResponder

// Add touch responder here

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
}

#pragma mark - Getters
- (UIButton *)saveButton {
    if (_saveButton == nil) {
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveButton addTarget:self action:@selector(saveButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_saveButton cz_setImageWithIcon:[CZIcon iconNamed:@"checkmark"]];
        _saveButton.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _saveButton;
}

- (UIButton *)zoomButton {
    if (_zoomButton == nil) {
        _zoomButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_zoomButton addTarget:self action:@selector(zoomButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_zoomButton cz_setImageWithIcon:[CZIcon iconNamed:@"zoom-in"]];
        _zoomButton.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _zoomButton;
}

- (UIButton *)cancelButton {
    if (_cancelButton == nil) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton addTarget:self action:@selector(cancelButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_cancelButton cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
        _cancelButton.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _cancelButton;
}

- (UIButton *)undoButton {
    if (_undoButton == nil) {
        _undoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_undoButton addTarget:self action:@selector(undoButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_undoButton cz_setImageWithIcon:[CZIcon iconNamed:@"undo"]];
        _undoButton.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _undoButton;
}

- (UIImageView *)previewImageView {
    if (_previewImageView == nil) {
        _previewImageView = [[UIImageView alloc] init];
        _previewImageView.userInteractionEnabled = YES;
        _previewImageView.clipsToBounds = YES;
        _previewImageView.contentMode = UIViewContentModeScaleAspectFit;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(tapPreviewImageViewEvent:)];
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.numberOfTouchesRequired = 1;
        [_previewImageView addGestureRecognizer:tapGesture];
    }
    return _previewImageView;
}

@end
