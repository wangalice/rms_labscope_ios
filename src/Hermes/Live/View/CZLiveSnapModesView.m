//
//  CZLiveSnapModesView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/3/5.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveSnapModesView.h"
#import "CZAccessabilityHelper.h"

static CGFloat kDefaultSnapButtonDiameter = 96;
static CGFloat kDefaultSnapSelectionButtonDiameter = 48;
static CGFloat kDefaultSnapOptionDiameter = 48;
static CGFloat kDefaultSnapButtonsMargin = 16;

@interface CZIcon (CZLiveSnapModesView)

@property (nonatomic, readonly, strong) UIImage *darkImage;
@property (nonatomic, readonly, strong) UIImage *lightImage;

@end

@implementation CZIcon (CZLiveSnapModesView)

- (UIImage *)darkImage {
    return [self imageForControlWithTheme:CZThemeLight state:UIControlStateNormal];
}

- (UIImage *)lightImage {
    return [self imageForControlWithTheme:CZThemeLight state:UIControlStateSelected];
}

@end

@interface CZLiveSnapModesView ()
@property (nonatomic, strong) UIButton *snapButton;
@property (nonatomic, strong) UIButton *snapSelectButton;
@property (nonatomic, strong) UIButton *snapSelectPhotoButton;
@property (nonatomic, strong) UIButton *snapSelectSnapFromStreamButton;
@property (nonatomic, strong) UIButton *snapSelectSnapDenoiseButton;
@property (nonatomic, strong) UIButton *snapSelectVideoButton;
@property (nonatomic, strong) UIButton *snapSelectEDOFButton;
@property (nonatomic, strong) UIButton *snapSelectMacroPhotoButton;
@property (nonatomic, strong) UIButton *snapSelectTimelapseVideoButton;
@property (nonatomic, strong) UIButton *snapSelectManualFLButton;
@property (nonatomic, strong) UIButton *snapSelectOnekeyFLButton;
@property (nonatomic, strong) UILabel *recordingTimeLabel;
@property (nonatomic, strong) UIImage *snapOptionBackgroundImage;
@property (nonatomic, strong) UIImage *snapSelectBackgroundImage;
@property (nonatomic, strong) NSMutableArray <NSNumber *> *snapModes;

@property (nonatomic, strong) NSMutableArray <NSNumber *> *availableSnapOptions;

@property (nonatomic, assign) CZCaptureMode mode;

@end

@implementation CZLiveSnapModesView

- (instancetype)initWithSnapModes:(NSArray<NSNumber *> *)snapModes {
    if (self = [super init]) {
        _snapModes = [snapModes mutableCopy];
        // default mode
        _mode = kCZCaptureModePhoto;
        _expand = NO;
        self.backgroundColor = [UIColor clearColor];
        
        _snapButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _snapButton.frame = CGRectMake(0, 0, kDefaultSnapButtonDiameter, kDefaultSnapButtonDiameter);
        _snapButton.exclusiveTouch = YES;
        _snapButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapButton setImage:[[CZIcon iconNamed:@"snap-option-photo"] darkImage] forState:UIControlStateNormal];
        [_snapButton setBackgroundImage:[[CZIcon iconNamed:@"snap-button-background"] image] forState:UIControlStateNormal];
        [_snapButton addTarget:self action:@selector(snapButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapButton.isAccessibilityElement = YES;
        _snapButton.accessibilityIdentifier = @"LiveSnapButton";
        
        _snapSelectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _snapSelectButton.frame = CGRectMake(0, 0, kDefaultSnapSelectionButtonDiameter, kDefaultSnapSelectionButtonDiameter);
        _snapSelectButton.exclusiveTouch = YES;
        _snapSelectButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectButton setImage:[[CZIcon iconNamed:@"snap-option-more"] darkImage] forState:UIControlStateNormal];
        [_snapSelectButton setBackgroundImage:self.snapSelectBackgroundImage forState:UIControlStateNormal];
        [_snapSelectButton addTarget:self action:@selector(snapSelectButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectButton.accessibilityIdentifier = @"LiveSnapSelectButton";
        
        _snapSelectPhotoButton = [self snapOptionButtonWithBackground];
        _snapSelectPhotoButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectPhotoButton.exclusiveTouch = YES;
        _snapSelectPhotoButton.hidden = YES;
        _snapSelectPhotoButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectPhotoButton setImage:[[CZIcon iconNamed:@"snap-option-photo"] lightImage] forState:UIControlStateNormal];
        [_snapSelectPhotoButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectPhotoButton.accessibilityIdentifier = @"LiveSnapSelectOptionSnapButton";
        
        _snapSelectSnapFromStreamButton = [self snapOptionButtonWithBackground];
        _snapSelectSnapFromStreamButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectSnapFromStreamButton.exclusiveTouch = YES;
        _snapSelectSnapFromStreamButton.hidden = YES;
        _snapSelectSnapFromStreamButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectSnapFromStreamButton setImage:[[CZIcon iconNamed:@"snap-option-fast-snap"] lightImage] forState:UIControlStateNormal];
        [_snapSelectSnapFromStreamButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectSnapFromStreamButton.accessibilityIdentifier = @"LiveSnapSelectOptionFastSnapButton";
        
        _snapSelectSnapDenoiseButton = [self snapOptionButtonWithBackground];
        _snapSelectSnapDenoiseButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectSnapDenoiseButton.exclusiveTouch = YES;
        _snapSelectSnapDenoiseButton.hidden = YES;
        _snapSelectSnapDenoiseButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectSnapDenoiseButton setImage:[[CZIcon iconNamed:@"snap-option-denoise"] lightImage] forState:UIControlStateNormal];
        [_snapSelectSnapDenoiseButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectSnapDenoiseButton.accessibilityIdentifier = @"LiveSnapSelectOptionHDSnapButton";
        
        _snapSelectEDOFButton = [self snapOptionButtonWithBackground];
        _snapSelectEDOFButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectEDOFButton.exclusiveTouch = YES;
        _snapSelectEDOFButton.hidden = YES;
        _snapSelectEDOFButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectEDOFButton setImage:[[CZIcon iconNamed:@"snap-option-edof"] lightImage] forState:UIControlStateNormal];
        [_snapSelectEDOFButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectEDOFButton.accessibilityIdentifier = @"LiveSnapSelectOptionEDFSnapButton";
        
        _snapSelectMacroPhotoButton = [self snapOptionButtonWithBackground];
        _snapSelectMacroPhotoButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectMacroPhotoButton.exclusiveTouch = YES;
        _snapSelectMacroPhotoButton.hidden = YES;
        _snapSelectMacroPhotoButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectMacroPhotoButton setImage:[[CZIcon iconNamed:@"snap-option-macro"] lightImage] forState:UIControlStateNormal];
        [_snapSelectMacroPhotoButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectMacroPhotoButton.accessibilityIdentifier = @"LiveSnapSelectOptionMacroSnapButton";
        
        _snapSelectTimelapseVideoButton = [self snapOptionButtonWithBackground];
        _snapSelectTimelapseVideoButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectTimelapseVideoButton.exclusiveTouch = YES;
        _snapSelectTimelapseVideoButton.hidden = YES;
        _snapSelectTimelapseVideoButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectTimelapseVideoButton setImage:[[CZIcon iconNamed:@"snap-option-timelapse"] lightImage] forState:UIControlStateNormal];
        [_snapSelectTimelapseVideoButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectTimelapseVideoButton.accessibilityIdentifier = @"LiveSnapSelectOptionTimelapseVideoButton";
        
        _snapSelectVideoButton = [self snapOptionButtonWithBackground];
        _snapSelectVideoButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectVideoButton.exclusiveTouch = YES;
        _snapSelectVideoButton.hidden = YES;
        _snapSelectVideoButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectVideoButton setImage:[[CZIcon iconNamed:@"snap-option-video"] lightImage] forState:UIControlStateNormal];
        [_snapSelectVideoButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectVideoButton.accessibilityIdentifier = @"LiveSnapSelectOptionVideoButton";
        
        _snapSelectManualFLButton = [self snapOptionButtonWithBackground];
        _snapSelectManualFLButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectManualFLButton.exclusiveTouch = YES;
        _snapSelectManualFLButton.hidden = YES;
        _snapSelectManualFLButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectManualFLButton setImage:[[CZIcon iconNamed:@"snap-option-multichannel"] lightImage] forState:UIControlStateNormal];
        [_snapSelectManualFLButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectManualFLButton.accessibilityIdentifier = @"LiveSnapSelectOptionFLButton";
        
        _snapSelectOnekeyFLButton = [self snapOptionButtonWithBackground];
        _snapSelectOnekeyFLButton.frame = CGRectMake(0, 0, kDefaultSnapOptionDiameter, kDefaultSnapOptionDiameter);
        _snapSelectOnekeyFLButton.exclusiveTouch = YES;
        _snapSelectOnekeyFLButton.hidden = YES;
        _snapSelectOnekeyFLButton.contentMode = UIViewContentModeScaleAspectFit;
        [_snapSelectOnekeyFLButton setImage:[[CZIcon iconNamed:@"snap-option-multichannel"] lightImage] forState:UIControlStateNormal];
        [_snapSelectOnekeyFLButton addTarget:self action:@selector(snapOptionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _snapSelectOnekeyFLButton.accessibilityIdentifier = @"LiveSnapSelectOptionFLButton";

        _recordingTimeLabel = [[UILabel alloc] init];
        [_recordingTimeLabel setTextAlignment:NSTextAlignmentCenter];
        [_recordingTimeLabel setBackgroundColor:[UIColor clearColor]];
        [_recordingTimeLabel setCz_textColorPicker:CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs30])];
        [_recordingTimeLabel setFont:[UIFont cz_label1]];
        [_recordingTimeLabel setHidden:YES];
        
        [self addSubview:_snapSelectButton];
        [self addSubview:_snapSelectPhotoButton];
        [self addSubview:_snapSelectSnapFromStreamButton];
        [self addSubview:_snapSelectSnapDenoiseButton];
        [self addSubview:_snapSelectEDOFButton];
        [self addSubview:_snapSelectMacroPhotoButton];
        [self addSubview:_snapSelectTimelapseVideoButton];
        [self addSubview:_snapSelectVideoButton];
        [self addSubview:_snapSelectManualFLButton];
        [self addSubview:_snapSelectOnekeyFLButton];
        [self addSubview:_snapButton];
        [self addSubview:_recordingTimeLabel];

    }
    return self;
}

- (void)dealloc {
    CZLogv(@"dealloc Work well");
}

- (void)layoutSubviews {
    [super layoutSubviews];

    [self updateButtonsFrameExpand:self.expand];
    
    self.recordingTimeLabel.frame = CGRectMake(CGRectGetMinX(self.snapSelectButton.frame),
                                               CGRectGetMinY(self.snapSelectButton.frame),
                                               kDefaultSnapOptionDiameter,
                                               kDefaultSnapOptionDiameter);
}

#pragma mark - Public

- (void)switchToSnapMode:(CZCaptureMode)mode {
    NSParameterAssert(mode != NSNotFound);
    
    // Set selected index button
    if (![self.availableSnapOptions containsObject:@(mode)]) {
        if (mode != kCZCaptureModeOnekeyFluorescencePlaying &&
            mode != kCZCaptureModeTimelapseVideoRecording &&
            mode != kCZCaptureModeVideoRecording) {
            mode = [[self.availableSnapOptions firstObject] integerValue];
        }
    }
    [self updateSnapButtonWithMode:mode];
    self.mode = mode;
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    
    if (self.enabled) {
        self.snapButton.enabled = YES;
        self.snapSelectButton.enabled = YES;
    } else {
        if (self.isExpanded) {
            [self collapseSnapModes];
        }
        self.snapButton.enabled = NO;
        self.snapSelectButton.enabled = NO;
    }
}

- (void)setSnapMode:(CZCaptureMode)snapMode enabled:(BOOL)enabled {
    NSParameterAssert([self.snapModes containsObject:@(snapMode)]);
    
    for (NSNumber *mode in [[self snapOptionsTable] allKeys]) {
        UIButton *button = [[self snapOptionsTable] objectForKey:mode];
        if ([self.availableSnapOptions containsObject:@(snapMode)]) {
            button.enabled = enabled;
        }
        break;
    }
}

- (void)updateWithAvaliableSnapModes:(NSArray <NSNumber *> *)snapModes {
    if (snapModes == nil)  return;
    if (self.isExpanded) {
        [self collapseSnapModes];
    }
    NSMutableArray *availableSnapOptions = [self.availableSnapOptions mutableCopy];
    for (NSNumber *snapMode in snapModes) {
        if (![self.availableSnapOptions containsObject:snapMode]) {
            [availableSnapOptions addObject:snapMode];
        }
    }
    for (NSNumber *availableSnapMode in self.availableSnapOptions) {
        if (![snapModes containsObject:availableSnapMode]) {
            [availableSnapOptions removeObject:availableSnapMode];
        }
    }
    self.availableSnapOptions = availableSnapOptions;
    
    if (![self.availableSnapOptions containsObject:@(self.mode)]) {
        if (self.mode == kCZCaptureModeOnekeyFluorescenceStandby && [self.availableSnapOptions containsObject:@(kCZCaptureModeManualFluorescence)]) {
            self.mode = kCZCaptureModeManualFluorescence;
        } else {
            self.mode = [[self.availableSnapOptions firstObject] integerValue];
        }
        [self updateSnapButtonWithMode:self.mode];
    }
}

- (void)expandSnapModes {
    [self showSnapOptions:YES];
}

- (void)collapseSnapModes {
    [self showSnapOptions:NO];
}

+ (CGFloat)defaultSnapModesViewWidth {
    return kDefaultSnapButtonDiameter;
}

- (BOOL)isSnapEnabled {
    return _snapButton.enabled;
}

- (void)disableSnapButton {
    self.snapButton.enabled = NO;
}

- (void)enableSnapButton {
    self.snapButton.enabled = YES;
}

#pragma mark - Private
- (NSDictionary<NSNumber*, UIButton*> *)snapOptionsTable {
    NSDictionary* table = @{
        @(kCZCaptureModePhoto)                      : _snapSelectPhotoButton,
        @(kCZCaptureModeSnapFromStream)             : _snapSelectSnapFromStreamButton,
        @(kCZCaptureModeSnapDenoise)                : _snapSelectSnapDenoiseButton,
        @(kCZCaptureModeEDOF)                       : _snapSelectEDOFButton,
        @(kCZCaptureModeMacroPhoto)                 : _snapSelectMacroPhotoButton,
        @(kCZCaptureModeTimelapseVideoStandby)      : _snapSelectTimelapseVideoButton,
        @(kCZCaptureModeVideoStandby)               : _snapSelectVideoButton,
        @(kCZCaptureModeManualFluorescence)         : _snapSelectManualFLButton,
        @(kCZCaptureModeOnekeyFluorescenceStandby)  : _snapSelectOnekeyFLButton
    };
    
    return table;
}

//TODO: Update the animation
- (void)showSnapOptions:(BOOL)show {
    if (_mode == kCZCaptureModeVideoRecording || _mode == kCZCaptureModeTimelapseVideoRecording) {
        return;
    }
    
    self.snapSelectButton.selected = show;
    
    [self updateButtonsFrameExpand:show];

    if (show) {
        [self.snapSelectButton setImage:[[CZIcon iconNamed:@"close"] darkImage] forState:UIControlStateNormal];
        
        NSMutableArray<NSNumber *> *availableSnapOptions = [self.availableSnapOptions mutableCopy];
        if ([availableSnapOptions containsObject:@(kCZCaptureModeManualFluorescence)] && [availableSnapOptions containsObject:@(kCZCaptureModeOnekeyFluorescenceStandby)]) {
            if (self.mode == kCZCaptureModeOnekeyFluorescenceStandby) {
                [availableSnapOptions removeObject:@(kCZCaptureModeManualFluorescence)];
            } else {
                [availableSnapOptions removeObject:@(kCZCaptureModeOnekeyFluorescenceStandby)];
            }
        }

        //The collection of snap mode is sorted by the order in snap option drop-down list. Macro-snap always has lowest priority.
        NSMutableArray <UIButton *> *optionsAvailable = [NSMutableArray array];
        NSArray <NSNumber *>*sortArray = @[@(kCZCaptureModePhoto),
                                           @(kCZCaptureModeSnapFromStream),
                                           @(kCZCaptureModeSnapDenoise),
                                           @(kCZCaptureModeManualFluorescence),
                                           @(kCZCaptureModeOnekeyFluorescenceStandby),
                                           @(kCZCaptureModeEDOF),
                                           @(kCZCaptureModeVideoStandby),
                                           @(kCZCaptureModeTimelapseVideoStandby),
                                           @(kCZCaptureModeMacroPhoto)];
        
        for (NSNumber *snapMode in sortArray) {
            if ([availableSnapOptions containsObject:snapMode] && snapMode.integerValue != self.mode) {
                UIButton *button = [[self snapOptionsTable] objectForKey:snapMode];
                [optionsAvailable addObject:button];
            }
        }
        
        for (NSUInteger i = 0; i < optionsAvailable.count; ++i) {
            UIButton *button = optionsAvailable[i];
            button.enabled = YES;
            button.hidden = NO;
            CGRect frame = button.frame;
            frame.origin.x = (CGRectGetWidth(self.frame) - kDefaultSnapOptionDiameter)/2;
            frame.origin.y = CGRectGetMaxY(self.snapButton.frame) + kDefaultSnapButtonsMargin + i * (kDefaultSnapOptionDiameter + kDefaultSnapButtonsMargin);
            button.frame = frame;
        }
    } else {
        
        [self.snapSelectButton setImage:[[CZIcon iconNamed:@"snap-option-more"] darkImage] forState:UIControlStateNormal];
        
        self.snapSelectPhotoButton.hidden = YES;
        self.snapSelectSnapFromStreamButton.hidden = YES;
        self.snapSelectSnapDenoiseButton.hidden = YES;
        self.snapSelectVideoButton.hidden = YES;
        self.snapSelectEDOFButton.hidden = YES;
#ifdef HAS_CAP_MODE_FL
        self.snapSelectFLButton.hidden = YES;
#endif // HAS_CAP_MODE_FL
        self.snapSelectMacroPhotoButton.hidden = YES;
        self.snapSelectTimelapseVideoButton.hidden = YES;
        self.snapSelectManualFLButton.hidden = YES;
        self.snapSelectOnekeyFLButton.hidden = YES;
#if REC_STREAM_ENABLED
        self.snapSelectRemoteStreamButton.hidden = YES;
#endif
    }
}

- (void)shouldSnapModesViewSwitchToMode:(CZCaptureMode)snapMode completionHandler:(void (^)(BOOL shouldSwitch))completionHandler {
    if (snapMode == NSNotFound) {
        completionHandler(NO);
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(shouldSnapModesViewSwitchTo:completionHandler:)]) {
        [self.delegate shouldSnapModesViewSwitchTo:snapMode completionHandler:completionHandler];
    }
}

- (void)updateSnapButtonWithMode:(CZCaptureMode)snapMode {
    switch (snapMode) {
        case kCZCaptureModePhoto: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-photo"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeSnapDenoise: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-denoise"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeSnapFromStream: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-fast-snap"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeEDOF: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-edof"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeMacroPhoto: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-macro"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeVideoStandby: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-video"] darkImage] forState:UIControlStateNormal];
            [self.snapSelectButton setUserInteractionEnabled:YES];
            [self.snapSelectButton setImage:[[CZIcon iconNamed:@"snap-option-more"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeTimelapseVideoStandby: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-timelapse"] darkImage] forState:UIControlStateNormal];
            [self.snapSelectButton setUserInteractionEnabled:YES];
            [self.snapSelectButton setImage:[[CZIcon iconNamed:@"snap-option-more"] darkImage] forState:UIControlStateNormal];
        }
            break;
        case kCZCaptureModeVideoRecording:
        case kCZCaptureModeTimelapseVideoRecording: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-recording"] darkImage] forState:UIControlStateNormal];
            
            [self.snapSelectButton setImage:self.snapSelectBackgroundImage forState:UIControlStateNormal];
            [self.snapSelectButton setUserInteractionEnabled:NO];

            [self.recordingTimeLabel setText:@"0:00"];
            [self.recordingTimeLabel setHidden:NO];
        }
            break;
#if REC_STREAM_ENABLED
        case kCZCaptureModeRemoteStream: {
            [self.snapButton setImage:[UIImage imageNamed:A(@"capture-button-snap-from-stream")] forState:UIControlStateNormal];
            [self.snapButton setImage:[UIImage imageNamed:A(@"capture-button-snap-from-stream-pressed")] forState:UIControlStateHighlighted];
        }
            break;
#endif // REC_STREAM_ENABLED
        case kCZCaptureModeManualFluorescence:
        case kCZCaptureModeOnekeyFluorescenceStandby: {
            // TODO: update the snap button to the FL snap mode
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-option-multichannel"] darkImage] forState:UIControlStateNormal];
            [self.snapSelectButton setImage:[[CZIcon iconNamed:@"snap-option-more"] darkImage] forState:UIControlStateNormal];
            [self.snapSelectButton setUserInteractionEnabled:YES];
        }
            break;
        case kCZCaptureModeOnekeyFluorescencePlaying: {
            [self.snapButton setImage:[[CZIcon iconNamed:@"snap-recording"] darkImage] forState:UIControlStateNormal];
            [self.snapSelectButton setUserInteractionEnabled:NO];
        }
            break;
    }
}

- (void)updateButtonsFrameExpand:(BOOL)expand {
    CGFloat height4SnapButtons = 0;
    if (expand) {
        NSUInteger snapOptionsCount = [self.availableSnapOptions containsObject:@(self.mode)] ? self.availableSnapOptions.count - 1 : self.availableSnapOptions.count;
        height4SnapButtons = kDefaultSnapButtonDiameter + (kDefaultSnapOptionDiameter + kDefaultSnapButtonsMargin) * snapOptionsCount + kDefaultSnapSelectionButtonDiameter + kDefaultSnapButtonsMargin;
    } else {
        height4SnapButtons = kDefaultSnapButtonDiameter + kDefaultSnapSelectionButtonDiameter + kDefaultSnapButtonsMargin;
    }
    
    CGFloat snapButtonY = (CGRectGetHeight(self.frame) - height4SnapButtons)/2;
    CGRect snapButtonFrame = self.snapButton.frame;
    snapButtonFrame.origin.x = (CGRectGetWidth(self.frame) - kDefaultSnapButtonDiameter)/2;
    snapButtonFrame.origin.y = snapButtonY;
    self.snapButton.frame = snapButtonFrame;
    
    CGFloat snapSelectButtonY = (CGRectGetHeight(self.frame)+ height4SnapButtons)/2 - kDefaultSnapSelectionButtonDiameter;
    CGRect snapSelectButtonFrame = self.snapSelectButton.frame;
    snapSelectButtonFrame.origin.x = (CGRectGetWidth(self.frame) - kDefaultSnapSelectionButtonDiameter)/2;
    snapSelectButtonFrame.origin.y = snapSelectButtonY;
    self.snapSelectButton.frame = snapSelectButtonFrame;
}

- (void)updateSnapModeTo:(CZCaptureMode)snapMode {
    // Set the button to selected status;
    [self shouldSnapModesViewSwitchToMode:snapMode completionHandler:^(BOOL shouldSwitch) {
        if (shouldSwitch) {
            if ([self.delegate respondsToSelector:@selector(snapModesViewWillSwitchTo:)]) {
                [self.delegate snapModesViewWillSwitchTo:snapMode];
            }
            
            //Add snap mode set action
            [self updateSnapButtonWithMode:snapMode];
            self.mode = snapMode;
            
            if ([self.delegate respondsToSelector:@selector(snapModesViewDidSwitchTo:)]) {
                [self.delegate snapModesViewDidSwitchTo:snapMode];
            }
        } else {
            CZLogv(@"switch mode is not support here: %lu", (unsigned long)snapMode);
        }
    }];
}

- (UIButton *)snapOptionButtonWithBackground {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:self.snapOptionBackgroundImage forState:UIControlStateNormal];
    return button;
}

+ (UIImage *)circleImageWithRadius:(CGFloat)radius
                         lineWidth:(CGFloat)lineWidth
                         fillColor:(UIColor *)fillColor
                       strokeColor:(UIColor *)strokeColor {
    UIImage *image = nil;
    if (image == nil) {
        CGFloat width = radius * 2 + lineWidth;
        CGFloat height = radius * 2 + lineWidth;

        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, 0.0f);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        CGRect rect = CGRectMake(0, 0, width, height);
        if (fillColor && !CGRectEqualToRect(rect, CGRectZero)) {
            CGContextSetFillColorWithColor(ctx, fillColor.CGColor);
            CGContextFillEllipseInRect(ctx, rect);
        }
        if (lineWidth > 0 && strokeColor) {
            CGContextSetStrokeColorWithColor(ctx, strokeColor.CGColor);
        }
        CGContextRestoreGState(ctx);
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return image;
}

#pragma mark - Action

- (void)snapButtonClicked:(UIButton *)sender {
    CZLogv(@"did begin snapping: %@", sender);

    // Hide snap options before snapping
    if (!self.snapSelectPhotoButton.hidden) {
        [self snapSelectButtonClicked:nil];
    }
    
    if ([self.delegate respondsToSelector:@selector(snapModesViewDidSnapWithMode:)]) {
        [self.delegate snapModesViewDidSnapWithMode:self.mode];
    }
}

- (void)snapSelectButtonClicked:(UIButton *)sender {
    CZLogv(@"show/hide options: %d", sender.selected);

    // Fix Bug #3563, avoid click when full screen expire timer trigger.
    if (!self.snapSelectButton.isUserInteractionEnabled) {
        return;
    }
    
    [self showSnapOptions:!self.snapSelectButton.selected];
}

- (void)snapOptionButtonClicked:(UIButton *)sender {
    CZLogv(@"did select snap options: %@", sender);
    [self showSnapOptions:NO];
    self.snapButton.hidden = NO;
    self.snapSelectButton.hidden = NO;

    //Get current snap mode
    CZCaptureMode switchSnapMode = NSNotFound;
    for (NSNumber *snapMode in [[self snapOptionsTable] allKeys]) {
        UIButton *snapButton = [[self snapOptionsTable] objectForKey:snapMode];
        if (snapButton == sender) {
            switchSnapMode = [snapMode integerValue];
            break;
        }
    }
    
    if (switchSnapMode == kCZCaptureModeMacroPhoto) {
        [CZAccessabilityHelper requestAccessCameraWithCompletion:^(BOOL success) {
            if (!success) {
                return;
            }
            [self updateSnapModeTo:switchSnapMode];
        }];
    } else {
        [self updateSnapModeTo:switchSnapMode];
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CZLogv(@"point: %@", NSStringFromCGPoint(point));
    for (UIView *view in [self subviews]) {
        if ([view isKindOfClass:[UIButton class]] && view.hidden != YES) {
            CGPoint pointInView = [self convertPoint:point toView:view];
            CZLogv(@"pointInView: %@", NSStringFromCGPoint(pointInView));
            if ([view pointInside:pointInView withEvent:event] == YES) {
                return YES;
            }
        }
    }
    return NO;
}

#pragma mark - Getters
- (NSMutableArray<NSNumber *> *)availableSnapOptions {
    if (!_availableSnapOptions) {
        _availableSnapOptions = [NSMutableArray array];
    }
    return _availableSnapOptions;
}

- (UIImage *)snapOptionBackgroundImage {
    if (_snapOptionBackgroundImage == nil) {
        _snapOptionBackgroundImage = [[self class] circleImageWithRadius:kDefaultSnapOptionDiameter/2 - 1
                                                               lineWidth:1.0
                                                               fillColor:[[UIColor cz_gs110] colorWithAlphaComponent:0.8]
                                                             strokeColor:[UIColor cz_gs120]];
    }
    return _snapOptionBackgroundImage;
}

- (UIImage *)snapSelectBackgroundImage {
    if (_snapSelectBackgroundImage == nil) {
        _snapSelectBackgroundImage = [[self class] circleImageWithRadius:kDefaultSnapSelectionButtonDiameter/2.0
                                                               lineWidth:0
                                                               fillColor:[UIColor cz_gs10]
                                                             strokeColor:nil];
    }
    return _snapSelectBackgroundImage;
}

@end
