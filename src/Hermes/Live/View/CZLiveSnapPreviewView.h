//
//  CZLiveSnapPreviewView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/4/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CZLiveSnapPreviewDelegate <NSObject>

@optional
- (void)liveSnapPreviewDidDeleteImage:(UIImage *)image;

- (void)liveSnapPreviewDidSaveImage:(UIImage *)image;

- (void)liveSnapPreviewDidSelectPreviewImage:(UIImage *)image;

- (void)liveSnapPreviewDidSelectUndoButton;

- (void)liveSnapPreviewDidEnterFullScreen:(BOOL)isFullScreen;

- (void)liveSnapPreviewDidDismiss;

@end

@interface CZLiveSnapPreviewView : UIView

@property (nonatomic, assign) BOOL shouldShowOperationHeaderView; //Default is NO.
@property (nonatomic, assign) BOOL undoActionSupported; // Default is NO.
@property (nonatomic, assign, getter=isImageFromRecording) BOOL imageFromRecording;
@property (nonatomic, weak) id <CZLiveSnapPreviewDelegate> delegate;

/* Should call this method in the main thread. */
- (void)updatePreviewWithImage:(UIImage *)image;

- (void)presentPreview;

- (void)dismissPreview;

@end

NS_ASSUME_NONNULL_END
