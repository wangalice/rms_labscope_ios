//
//  CZLiveSnapModesView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/3/5.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZLiveTask.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CZLiveSnapModesViewDelegate <NSObject>

@optional
// Tell the delgate that the snap mode has been selected;
- (void)snapModesViewDidSnapWithMode:(CZCaptureMode)snapMode;

// Tell the delegate that the snapping mode will be changed;
- (void)snapModesViewWillSwitchTo:(CZCaptureMode)snapMode;

// Tell the delegate that the snapping mode has been changed;
- (void)snapModesViewDidSwitchTo:(CZCaptureMode)snapMode;

// Asks the delegate if snapping mode could switch in the snap modes view;
- (void)shouldSnapModesViewSwitchTo:(CZCaptureMode)snapMode completionHandler:(void (^)(BOOL shouldSwitch))completionHandler;

@end

@interface CZLiveSnapModesView : UIControl

@property (nonatomic, readonly, strong) UIButton *snapButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectPhotoButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectSnapFromStreamButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectSnapDenoiseButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectVideoButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectEDOFButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectMacroPhotoButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectTimelapseVideoButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectManualFLButton;
@property (nonatomic, readonly, strong) UIButton *snapSelectOnekeyFLButton;
@property (nonatomic, readonly, strong) UILabel *recordingTimeLabel;

@property (nonatomic, assign, getter=isExpanded) BOOL expand;

@property (nonatomic, weak, nullable) id <CZLiveSnapModesViewDelegate>delegate;

@property (nonatomic, assign, readonly) CZCaptureMode mode;

@property (nonatomic, assign, getter=isSnapEnabled) BOOL snapEnabled;

- (instancetype)initWithSnapModes:(NSArray <NSNumber *> *)snapModes;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;

- (void)switchToSnapMode:(CZCaptureMode)snapMode;

- (void)setSnapMode:(CZCaptureMode)snapMode enabled:(BOOL)enabled;

// update snap modes to the current snap mode, this method will remove current available snap modes which could not found in the parameter: *snapModes*;
- (void)updateWithAvaliableSnapModes:(nullable NSArray <NSNumber *> *)snapModes;

- (void)expandSnapModes;
- (void)collapseSnapModes;

+ (CGFloat)defaultSnapModesViewWidth;

@end

NS_ASSUME_NONNULL_END
