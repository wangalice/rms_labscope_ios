//
//  CZLiveViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnnotationMarco.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZLiveViewModel : NSObject

@property (nonatomic, assign, getter=isLicenseEnabled) BOOL licenseEnabled;
@property (nonatomic, assign, getter=isSnapAvailable) BOOL snapAvailable;
@property (nonatomic, assign, getter=isSnapModesViewVisile) BOOL snapModesViewVisible;
@property (nonatomic, assign, getter=isReconnecting) BOOL reconnecting;
@property (nonatomic, assign) BOOL isEditing;

- (NSArray <NSNumber *> *)supportAnnotationTypes;

@end

NS_ASSUME_NONNULL_END
