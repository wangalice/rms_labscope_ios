//
//  CZDefaultSettings+CZElementScaleBar.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+CZElementScaleBar.h"

static NSString * const kFillColor = @"shared_scalebar_fillColor";
static NSString * const kStrokeColor = @"shared_scalebar_strokeColor";
static NSString * const kScaleBarElementSize = @"shared_scalebar_elementSize";
static NSString * const kScaleBarDisplayMode = @"shared_scalebar_scaleBarDisplayMode";
static NSString * const kScaleBarUnitStyle = @"shared_scalebar_unitStyle";
static NSString * const kPercentX = @"shared_scalebar_percentX";
static NSString * const kPercentY = @"shared_scalebar_percentY";
static NSString * const kTolerance = @"shared_scalebar_tolerance";

@implementation CZDefaultSettings (CZElementScaleBar)

- (CZColor)fillColor {
    CZColor fillColor = [[CZDefaultSettings sharedInstance] backgroundColor];
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kFillColor];
    if ([valueString isEqualToString:@"0"]) {
        fillColor = kCZColorTransparent;
    } else if ([valueString isEqualToString:@"1"]) {
        fillColor = kCZColorRed;
    } else if ([valueString isEqualToString:@"2"]) {
        fillColor = kCZColorBlue;
    } else if ([valueString isEqualToString:@"3"]) {
        fillColor = kCZColorGreen;
    } else if ([valueString isEqualToString:@"4"]) {
        fillColor = kCZColorWhite;
    } else if ([valueString isEqualToString:@"5"]) {
        fillColor = kCZColorBlack;
    }
    return fillColor;
}

- (void)setFillColor:(CZColor)fillColor {
    NSString *valueString;
    if (CZColorEqualToColor(fillColor, kCZColorTransparent)) {
        valueString = @"0";
    } else if (CZColorEqualToColor(fillColor, kCZColorRed)) {
        valueString = @"1";
    } else if (CZColorEqualToColor(fillColor, kCZColorBlue)) {
        valueString = @"2";
    } else if (CZColorEqualToColor(fillColor, kCZColorGreen)) {
        valueString = @"3";
    } else if (CZColorEqualToColor(fillColor, kCZColorWhite)) {
        valueString = @"4";
    } else if (CZColorEqualToColor(fillColor, kCZColorBlack)){
        valueString = @"5";
    } else {
        valueString = @"4";
    }
    [[NSUserDefaults standardUserDefaults] setValue:valueString forKey:kFillColor];
}

- (CZColor)strokeColor {
    CZColor strokeColor = [[CZDefaultSettings sharedInstance] color];
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kStrokeColor];
    if ([valueString isEqualToString:@"0"]) {
        strokeColor = kCZColorRed;
    } else if ([valueString isEqualToString:@"1"]) {
        strokeColor = kCZColorGreen;
    } else if ([valueString isEqualToString:@"2"]) {
        strokeColor = kCZColorBlue;
    } else if ([valueString isEqualToString:@"3"]) {
        strokeColor = kCZColorBlack;
    } else if ([valueString isEqualToString:@"4"]) {
        strokeColor = kCZColorYellow;
    }
    return strokeColor;
}

- (void)setStrokeColor:(CZColor)strokeColor {
    NSString *valueString;
    if (CZColorEqualToColor(strokeColor, kCZColorRed)) {
        valueString = @"0";
    } else if (CZColorEqualToColor(strokeColor, kCZColorGreen)) {
        valueString = @"1";
    } else if (CZColorEqualToColor(strokeColor, kCZColorBlue)) {
        valueString = @"2";
    } else if (CZColorEqualToColor(strokeColor, kCZColorBlack)) {
        valueString = @"3";
    } else if (CZColorEqualToColor(strokeColor, kCZColorYellow)) {
        valueString = @"4";
    } else {
        valueString = @"0";
    }
    [[NSUserDefaults standardUserDefaults] setValue:valueString forKey:kStrokeColor];
}

- (CZElementSize)scaleBarElementSize {
    CZElementSize size = [[CZDefaultSettings sharedInstance] elementSize];
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kScaleBarElementSize];
    if ([valueString isEqualToString:@"0"]) {
        size = CZElementSizeExtraSmall;
    } else if ([valueString isEqualToString:@"1"]) {
        size = CZElementSizeSmall;
    } else if ([valueString isEqualToString:@"2"]) {
        size = CZElementSizeMedium;
    } else if ([valueString isEqualToString:@"3"]) {
        size = CZElementSizeLarge;
    } else if ([valueString isEqualToString:@"4"]) {
        size = CZElementSizeExtraLarge;
    } else {
        size = CZElementSizeMedium;
    }
    return size;
}

- (void)setScaleBarElementSize:(CZElementSize)scaleBarElementSize {
    NSString *string;
    switch (scaleBarElementSize) {
        case CZElementSizeExtraSmall:
            string = @"0";
            break;
        case CZElementSizeSmall:
            string = @"1";
            break;
        case CZElementSizeMedium:
            string = @"2";
            break;
        case CZElementSizeLarge:
            string = @"3";
            break;
        case CZElementSizeExtraLarge:
            string = @"4";
            break;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:string forKey:kScaleBarElementSize];
}

- (CZScaleBarDisplayMode)scaleBarDisplayMode {
    CZScaleBarDisplayMode displayMode;
    
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kScaleBarDisplayMode];
    if ([valueString isEqualToString:@"1"]) {
        displayMode = kCZScaleBarPositionHidden;
    } else if ([valueString isEqualToString:@"0"]){
        displayMode = kCZScaleBarPositionLastPosition;
    } else {
        displayMode = kCZScaleBarPositionHidden;
    }
    
    return displayMode;
}

- (void)setScaleBarDisplayMode:(CZScaleBarDisplayMode)scaleBarDisplayMode {
    NSString *valueString = @"1";

    switch (scaleBarDisplayMode) {
        case kCZScaleBarPositionLastPosition: {
            valueString = @"0";
        }
            break;
        case kCZScaleBarPositionHidden: {
            valueString = @"1";
        }
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] setObject:valueString forKey:kScaleBarDisplayMode];
}

- (CZElementUnitStyle)scaleBarUnitStyle {
    CZElementUnitStyle unitStyle = [CZDefaultSettings sharedInstance].unitStyle;
    
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kScaleBarUnitStyle];
    NSInteger index = [valueString integerValue];
    switch (index) {
        case 0:
            unitStyle = CZElementUnitStyleMicrometer;
            break;
        case 1:
            unitStyle = CZElementUnitStyleMil;
            break;
        case 2:
            unitStyle = CZElementUnitStyleMM;
            break;
        case 3:
            unitStyle = CZElementUnitStyleInch;
            break;
        case 4: // fallthrough
        default: {
            unitStyle = CZElementUnitStyleAuto;
            break;
        }
    }
    
    return unitStyle;
}

- (void)setScaleBarUnitStyle:(CZElementUnitStyle)scaleBarUnitStyle {
    switch (scaleBarUnitStyle) {
        case CZElementUnitStyleMicrometer:
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleMil:
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleMM:
            [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleInch:
            [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleMetricAuto:
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleImperialAuto:
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kScaleBarUnitStyle];
            break;
        case CZElementUnitStyleAuto: {
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kScaleBarUnitStyle];
            break;
        }
    }
}

- (CGFloat)percentX {
    return [[NSUserDefaults standardUserDefaults] floatForKey:kPercentX];
}

- (void)setPercentX:(CGFloat)percentX {
    [[NSUserDefaults standardUserDefaults] setObject:@(percentX) forKey:kPercentX];
}

- (CGFloat)percentY {
    return [[NSUserDefaults standardUserDefaults] floatForKey:kPercentY];
}

- (void)setPercentY:(CGFloat)percentY {
    [[NSUserDefaults standardUserDefaults] setObject:@(percentY) forKey:kPercentY];
}

- (CGFloat)tolerance {
    return [[NSUserDefaults standardUserDefaults] floatForKey:kTolerance];
}

- (void)setTolerance:(CGFloat)tolerance {
    [[NSUserDefaults standardUserDefaults] setObject:@(tolerance) forKey:kTolerance];
}

@end
