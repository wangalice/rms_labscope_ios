//
//  CZLiveTask+CZAnnotationReadWrite.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/1.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveTask.h"

@class CZElementLayer;

@interface CZLiveTask (CZAnnotationReadWrite)

- (CZElementLayer *)readElementXMLFile;

- (void)writeElementXMLFile;

@end
