//
//  CZElementScaleBarManager.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/1.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZElementScaleBarManager : NSObject

@property (nonatomic) CZColor annotationColor;
@property (nonatomic) CZElementSize annotationSize;
@property (nonatomic) CZColor backgroundColor;
@property (nonatomic) CZElementSize annotationSize;
@property (nonatomic) CGPoint point;

@end

NS_ASSUME_NONNULL_END
