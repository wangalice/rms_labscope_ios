//
//  CZElementScaleBar+CZCoordinate.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZElementScaleBar+CZCoordinate.h"

@implementation CZElementScaleBar (CZCoordinate)

- (CZElementScaleBarCoordinate)scaleBarCoordinateTransformerWithImageSize:(CGSize)imageSize {
    CZLogv(@"[Enter coordinate system conversion] imageSize:%@, boundary: %@", NSStringFromCGSize(imageSize), NSStringFromCGSize([self boundary].size));
    if (CGSizeEqualToSize(imageSize, CGSizeZero) == NO) {
        CZElementScaleBarCoordinate scaleBarCoordinate;
        CGFloat originX = imageSize.width / 2.0f;
        CGFloat originY = imageSize.height / 2.0f;
        CGFloat suggestionX = (self.point.x - originX) >= 0 ? self.point.x + [self boundary].size.width : self.point.x;
        CGFloat suggestionY = (self.point.y - originY) >= 0 ? self.point.y + [self boundary].size.height : self.point.y;
        CGFloat percentX = (suggestionX - originX) / (imageSize.width / 2.0);
        CGFloat percentY = (suggestionY - originY) / (imageSize.height / 2.0);
        scaleBarCoordinate = CZElementScaleBarCoordinateMake(percentX, percentY);
        
        CZLogv(@"[Leave coordinate system conversion] point: %@, imageSize:%@, scaleBarCoordinate: %@", NSStringFromCGPoint(self.point), NSStringFromCGSize(imageSize), NSStringFromCGPoint(CGPointMake(scaleBarCoordinate.percentX, scaleBarCoordinate.percentY)));
        
        return scaleBarCoordinate;
    }
    
    return CZElementScaleBarCoordinateMake(0, 0);
}

- (CGPoint)scaleBarPointWithScaleBarCoordinate:(CZElementScaleBarCoordinate)scaleBarCoordinate imageSize:(CGSize)imageSize {
    CZLogv(@"[Enter coordinate system conversion] scaleBarCoordinate: %@, imageSize:%@, boundary: %@", NSStringFromCGPoint(CGPointMake(scaleBarCoordinate.percentX, scaleBarCoordinate.percentY)),  NSStringFromCGSize(imageSize), NSStringFromCGSize([self boundary].size));
    if (CGSizeEqualToSize(imageSize, CGSizeZero) == NO) {
        CGPoint point;
        CGFloat originX = imageSize.width / 2.0;
        CGFloat originY = imageSize.height / 2.0;
        CGFloat originalX = scaleBarCoordinate.percentX * (imageSize.width / 2.0) + originX;
        CGFloat originalY = scaleBarCoordinate.percentY * (imageSize.height / 2.0) + originY;
        point.x = scaleBarCoordinate.percentX >= 0 ? originalX - [self boundary].size.width : originalX;
        point.y = scaleBarCoordinate.percentY >= 0 ? originalY - [self boundary].size.height : originalY;

        CZLogv(@"[Leave coordinate system conversion] scaleBarCoordinate: %@, imageSize:%@, point: %@", NSStringFromCGPoint(CGPointMake(scaleBarCoordinate.percentX, scaleBarCoordinate.percentY)),  NSStringFromCGSize(imageSize), NSStringFromCGPoint(point));
        
        return point;
    }

    return CGPointZero;
}

@end
