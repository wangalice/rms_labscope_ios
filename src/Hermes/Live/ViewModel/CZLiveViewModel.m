//
//  CZLiveViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveViewModel.h"

@interface CZLiveViewModel ()

@end

@implementation CZLiveViewModel

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - Public method

- (NSArray <NSNumber *> *)supportAnnotationTypes {
    if (self.isLicenseEnabled) {
        return @[@(CZAnnotationButtonLine),
                 @(CZAnnotationButtonSquare),
                 @(CZAnnotationButtonCircle),
                 @(CZAnnotationButtonPolygon),
                 @(CZAnnotationButtonPolyline),
                 @(CZAnnotationButtonSplineContour),
                 @(CZAnnotationButtonSpline),
                 @(CZAnnotationButtonArrow),
                 @(CZAnnotationButtonAngle),
                 @(CZAnnotationButtonDisconnectedAngle),
                 @(CZAnnotationButtonCount),
                 @(CZAnnotationButtonCalipers),
                 @(CZAnnotationButtonMultiCalipers),
                 @(CZAnnotationButtonText),
                 @(CZAnnotationButtonScaleBar)];
    } else {
        return @[@(CZAnnotationButtonLine),
                 @(CZAnnotationButtonSquare),
                 @(CZAnnotationButtonCircle),
                 @(CZAnnotationButtonPolygon),
                 @(CZAnnotationButtonArrow),
                 @(CZAnnotationButtonAngle),
                 @(CZAnnotationButtonCount),
                 @(CZAnnotationButtonText),
                 @(CZAnnotationButtonScaleBar)];
    }
}

- (BOOL)isLicenseEnabled {
    return YES;
}

#pragma mark - Private method


@end
