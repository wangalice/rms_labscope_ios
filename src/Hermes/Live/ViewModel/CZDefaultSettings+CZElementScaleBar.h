//
//  CZDefaultSettings+CZElementScaleBar.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZToolbox/CZToolbox.h>
#import "CZElementScaleBar+CZCoordinate.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZDefaultSettings (CZElementScaleBar)

@property (nonatomic, readonly, assign) CZColor fillColor;
@property (nonatomic, readonly, assign) CZColor strokeColor;
@property (nonatomic, readonly, assign) CZElementSize scaleBarElementSize;
@property (nonatomic, readonly, assign) CZScaleBarDisplayMode scaleBarDisplayMode;
@property (nonatomic, readonly, assign) CZElementUnitStyle scaleBarUnitStyle;
@property (nonatomic, readonly, assign) CGFloat percentX;
@property (nonatomic, readonly, assign) CGFloat percentY;
@property (nonatomic, readonly, assign) CGFloat tolerance;

- (void)setFillColor:(CZColor)fillColor;
- (void)setStrokeColor:(CZColor)strokeColor;
- (void)setScaleBarElementSize:(CZElementSize)scaleBarElementSize;
- (void)setScaleBarDisplayMode:(CZScaleBarDisplayMode)scaleBarDisplayMode;
- (void)setScaleBarUnitStyle:(CZElementUnitStyle)scaleBarUnitStyle;
- (void)setPercentX:(CGFloat)percentX;
- (void)setPercentY:(CGFloat)percentY;
- (void)setTolerance:(CGFloat)tolerance;

@end

NS_ASSUME_NONNULL_END
