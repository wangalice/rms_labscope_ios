//
//  CZLiveTask+CZAnnotationReadWrite.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/1.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveTask+CZAnnotationReadWrite.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZAnnotationKit/CZElement.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZDefaultSettings+CZElementScaleBar.h"

static NSString * const kCZElementScaleBarConfiguration = @"CZElementScaleBarConfiguration";

@implementation CZLiveTask (CZAnnotationReadWrite)

#pragma mark - Public method

- (CZElementLayer *)readElementXMLFile {
    CZElementLayer *elementLayer = nil;
    NSString *path = [self elementCachedFilePath];

    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSData *elementData = [NSData dataWithContentsOfFile:path];
        elementLayer = [CZElementLayer readElementXMLFile:elementData];
    }
    
    return elementLayer;
}

- (void)writeElementXMLFile {
    NSString *path = [self elementCachedFilePath];
    if (path != nil) {
        [self.videoTool.docManager.elementLayer exportElementToXMLFilePath:path];
    }
    [self synchronizeScaleBarSettingWithElementLayer:self.videoTool.docManager.elementLayer];
}

- (NSString *)elementCachedFilePath {
    NSString *cachedPath = [NSFileManager defaultManager].cz_cachePath;
    NSString *macAddress = self.currentCamera.macAddress;
    NSString *elementFile = nil;
    if (macAddress.length > 0) {
        macAddress = [macAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
        elementFile = [cachedPath stringByAppendingPathComponent:macAddress];
        BOOL isDir = NO;
        if (![[NSFileManager defaultManager] fileExistsAtPath:elementFile isDirectory:&isDir]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:elementFile
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:NULL];
        }
        elementFile = [elementFile stringByAppendingPathComponent:@"element.xml"];
    }
    return elementFile;
}

- (void)synchronizeScaleBarSettingWithElementLayer:(CZElementLayer *)elementLayer {
    CZElementScaleBar *scaleBar = nil;

    for (CZElement *element in elementLayer) {
        if ([element isKindOfClass:[CZElementScaleBar class]]) {
            scaleBar = (CZElementScaleBar *)element;
            break;
        }
    }

    if (scaleBar) {
        [[CZDefaultSettings sharedInstance] setFillColor:scaleBar.fillColor];
        [[CZDefaultSettings sharedInstance] setElementSize:scaleBar.size];
        [[CZDefaultSettings sharedInstance] setStrokeColor:scaleBar.strokeColor];
        [[CZDefaultSettings sharedInstance] setUnitStyle:[CZElement unitStyle]];
        [[CZDefaultSettings sharedInstance] setPercentX:[scaleBar scaleBarCoordinateTransformerWithImageSize:[elementLayer frame].size].percentX];
        [[CZDefaultSettings sharedInstance] setPercentY:[scaleBar scaleBarCoordinateTransformerWithImageSize:[elementLayer frame].size].percentY];
        [[CZDefaultSettings sharedInstance] setTolerance:scaleBar.tolerance];
    }
    
    CZScaleBarDisplayMode scaleBarDisplayMode = scaleBar == nil ? kCZScaleBarPositionHidden : kCZScaleBarPositionLastPosition;
    [[CZDefaultSettings sharedInstance] setScaleBarDisplayMode:scaleBarDisplayMode];
}

@end
