//
//  CZElementScaleBar+CZCoordinate.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZAnnotationKit/CZAnnotationKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 The scale bar element's coordinate origin is the image center.
 Reuse the scale bar element position between images with differnt scaling will out of the bounds if we only calculate the scale bar frame's point (scale bar element size depends on the scalings). Calculating the point which is closest to the image boundary will resolve this issue.
 So the percent range is [-1.0, 1.0].
 */
typedef struct {
    CGFloat percentX; // range is [-1.0, 1.0]
    CGFloat percentY; // range is [-1.0, 1.0]
} CZElementScaleBarCoordinate;

static inline CZElementScaleBarCoordinate CZElementScaleBarCoordinateMake(CGFloat percentX, CGFloat percentY) {
    CZElementScaleBarCoordinate scaleBarCoordinate;
    scaleBarCoordinate.percentX = percentX;
    scaleBarCoordinate.percentY = percentY;
    return scaleBarCoordinate;
}

@interface CZElementScaleBar (CZCoordinate)

- (CZElementScaleBarCoordinate)scaleBarCoordinateTransformerWithImageSize:(CGSize)imageSize;

- (CGPoint)scaleBarPointWithScaleBarCoordinate:(CZElementScaleBarCoordinate)scaleBarCoordinate
                                     imageSize:(CGSize)imageSize;

@end

NS_ASSUME_NONNULL_END
