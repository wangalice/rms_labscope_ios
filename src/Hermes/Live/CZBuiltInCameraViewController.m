//
//  CZBuiltInCameraViewController.m
//  Labscope
//
//  Created by Ralph Jin on 11/17/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import "CZBuiltInCameraViewController.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZCapturePreviewView.h"

@interface CZBuiltInCameraViewController () <UIGestureRecognizerDelegate> {
    CGSize _liveResolution;
}

@property (nonatomic, retain) CZCapturePreviewView *builtInCameraView;
@property (nonatomic, retain) CZBuiltInCamera *builtInCamera;
@property (nonatomic, retain) UILabel *drawingTubeLabel;
@property (nonatomic, retain) UISlider *drawingTubeSlider;
@property (nonatomic, retain) UIView *focusBoxView;
@end

@implementation CZBuiltInCameraViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appWillEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}

- (void)loadView {
    self.view = self.builtInCameraView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setCz_backgroundColorPicker:CZThemeColorWithColors([UIColor cz_gs110], [UIColor cz_gs110])];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGRect frame = self.view.frame;
    
    CGRect labelFrame = CGRectMake(frame.origin.x + kUIDrawingTubeSliderXPadding,
                                   kUIDrawingTubeSliderYPadding,
                                   CGFLOAT_MAX,
                                   kUIDrawingTubeSliderHeight);
    labelFrame = [self.drawingTubeLabel textRectForBounds:labelFrame
                                          limitedToNumberOfLines:1];
    [self.drawingTubeLabel setFrame:labelFrame];
    
    CGRect sliderFrame = CGRectMake(CGRectGetMaxX(self.drawingTubeLabel.frame) + 30,
                                    kUIDrawingTubeSliderYPadding,
                                    frame.size.width - kUIDrawingTubeSliderXPadding * 2 - self.drawingTubeLabel.frame.size.width - 30,
                                    kUIDrawingTubeSliderHeight);
    [self.drawingTubeSlider setFrame:sliderFrame];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [_builtInCameraView setVideoOrientation:toInterfaceOrientation];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [_builtInCamera stop];
    self.builtInCamera = nil;
    self.builtInCameraView.session = nil;
}

- (void)createBuiltInCamera {
    if (_builtInCamera == nil) {
        _builtInCamera = [[CZBuiltInCamera alloc] init];
    }
}

- (CZCapturePreviewView *)builtInCameraView {
    if (_builtInCameraView == nil) {
        _builtInCameraView = [[CZCapturePreviewView alloc] init];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBuiltInCameraView:)];
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.numberOfTouchesRequired = 1;
        tapGesture.delegate = self;
        [_builtInCameraView addGestureRecognizer:tapGesture];
    }
    return _builtInCameraView;
}

- (UILabel *)drawingTubeLabel {
    if (_drawingTubeLabel == nil) {
        _drawingTubeLabel = [[UILabel alloc] init];
        [_drawingTubeLabel setBackgroundColor:[UIColor clearColor]];
        [_drawingTubeLabel setText:L(@"DRAWING_TUBE_LABEL")];
        [_drawingTubeLabel setTextColor:[UIColor blackColor]];
        
        _drawingTubeLabel.layer.shadowColor = [[UIColor whiteColor] CGColor];
        _drawingTubeLabel.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        _drawingTubeLabel.layer.shadowRadius = 7.0;
        _drawingTubeLabel.layer.shadowOpacity = 1.0;
        _drawingTubeLabel.layer.masksToBounds = NO;
        
        CGRect baseFrame = self.view.frame;
        CGRect frame = [_drawingTubeLabel textRectForBounds:CGRectMake(baseFrame.origin.x + kUIDrawingTubeSliderXPadding,
                                                                       kUIDrawingTubeSliderYPadding,
                                                                       CGFLOAT_MAX,
                                                                       kUIDrawingTubeSliderHeight)
                                     limitedToNumberOfLines:1];
        [_drawingTubeLabel setFrame:frame];
    }
    return _drawingTubeLabel;
}

- (UISlider *)drawingTubeSlider {
    if (_drawingTubeSlider == nil) {
        CGRect baseFrame = self.view.frame;
        _drawingTubeSlider = [[UISlider alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.drawingTubeLabel.frame) + 30,
                                                                        kUIDrawingTubeSliderYPadding,
                                                                        baseFrame.size.width - kUIDrawingTubeSliderXPadding * 2 - self.drawingTubeLabel.frame.size.width - 30,
                                                                        kUIDrawingTubeSliderHeight)];
        [_drawingTubeSlider setMinimumValue:0.0];
        [_drawingTubeSlider setMaximumValue:1.0];
        [_drawingTubeSlider setValue:0.5];
        [_drawingTubeSlider addTarget:self action:@selector(drawingTubeSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _drawingTubeSlider;
}

- (UIView *)focusBoxView {
    if (_focusBoxView == nil) {
        _focusBoxView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _focusBoxView.alpha = 0.0;
        _focusBoxView.layer.borderWidth = 2;
        _focusBoxView.layer.borderColor = [[UIColor colorWithRed:1.0 green:0.8 blue:0.0 alpha:1.0] CGColor];
        _focusBoxView.userInteractionEnabled = NO;
    }
    
    return _focusBoxView;
}

- (id<CZCameraDelegate>)cameraDelegate {
    return self.builtInCamera.delegate;
}

- (void)setCameraDelegate:(id<CZCameraDelegate>)cameraDelegate {
    self.builtInCamera.delegate = cameraDelegate;
}

- (void)setMode:(CZBuiltInCameraViewMode)mode {
    if (_mode != mode) {
        _mode = mode;
        if (_builtInCameraView) {
            [self refreshUIStatus];
        }
    }
}

- (void)startRunningInViewController:(UIViewController *)parentVC resolution:(CGSize)resolution aboveView:(UIView *)targetView {
    _liveResolution = resolution;
    CGSize liveResolution = resolution;
    if (self.mode == CZBuiltInCameraViewModeMacro) {
        liveResolution = CGSizeZero;
    }
    
    [self createBuiltInCamera];
    [self.builtInCamera setPreferredResolution:liveResolution];
    [self.builtInCamera play];
    
    CZCapturePreviewView *builtInCameraView = (CZCapturePreviewView *)self.view;
    
    [builtInCameraView setSession:self.builtInCamera.session];
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    [builtInCameraView setVideoOrientation:interfaceOrientation];
    
    [parentVC addChildViewController:self];
    
    UIView *superView = parentVC.view;
    builtInCameraView.frame = targetView.frame;
    [superView insertSubview:builtInCameraView aboveSubview:targetView];

    [self refreshUIStatus];
}

// refresh UI status according to current mode
- (void)refreshUIStatus {
    CGFloat alpha;
    CGSize liveResolution;
    BOOL userInteractionEnabled;
    if (self.mode == CZBuiltInCameraViewModeMacro) {
        liveResolution = CGSizeZero;
        alpha = 1.0;
        userInteractionEnabled = YES;
    } else {
        liveResolution = _liveResolution;
        alpha = 0.5;
        userInteractionEnabled = NO;
    }
    
    [self.builtInCamera setPreferredResolution:liveResolution];
    
    CZCapturePreviewView *builtInCameraView = (CZCapturePreviewView *)self.view;
    
    [builtInCameraView setAlpha:alpha];
    [builtInCameraView setSession:self.builtInCamera.session];
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    [builtInCameraView setVideoOrientation:interfaceOrientation];
    builtInCameraView.userInteractionEnabled = userInteractionEnabled;
    
    if (self.mode == CZBuiltInCameraViewModeDrawingTube) {
        UIViewController *parentVC = self.parentViewController;
        UIView *superView = parentVC.view;
        
        [superView addSubview:self.drawingTubeLabel];
        [superView addSubview:self.drawingTubeSlider];

        [self.focusBoxView removeFromSuperview];
        self.focusBoxView = nil;
        
    } else {
        [self.drawingTubeLabel removeFromSuperview];
        self.drawingTubeLabel = nil;
        
        [self.drawingTubeSlider removeFromSuperview];
        self.drawingTubeSlider = nil;
        
        if (_focusBoxView == nil) {
            [self.view addSubview:self.focusBoxView];
        }
    }
}

- (void)stopRunningFromParentViewController {
    [_builtInCamera stop];
    self.builtInCamera = nil;
    
    self.builtInCameraView = nil;
    
    [_drawingTubeLabel removeFromSuperview];
    self.drawingTubeLabel = nil;
    
    [_drawingTubeSlider removeFromSuperview];
    self.drawingTubeSlider = nil;
    
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)setPreferredResolution:(CGSize)resolution {
    [self.builtInCamera setPreferredResolution:resolution];
    _builtInCameraView.session = self.builtInCamera.session;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    [_builtInCameraView setVideoOrientation:interfaceOrientation];
}

- (void)beginSnapping {
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    [self.builtInCamera setOrientation:interfaceOrientation];
    [self.builtInCamera beginSnapping];
}

- (void)showBuiltInCameraUIComponents:(BOOL)show {
    BOOL hidden = !show;
    _builtInCameraView.hidden = hidden;
    _drawingTubeLabel.hidden = hidden;
    _drawingTubeSlider.hidden = hidden;
    _focusBoxView.hidden = hidden;
}

#pragma mark - actions

- (void)tapBuiltInCameraView:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint pointInView = [gestureRecognizer locationInView:self.view];
    
    BOOL shouldHandleTap = YES;
    if ([self.delegate respondsToSelector:@selector(shouldBuiltInCameraView:handleTapGesture:)]) {
        shouldHandleTap = [self.delegate shouldBuiltInCameraView:self handleTapGesture:gestureRecognizer];
    }
    
    if (shouldHandleTap && CGRectContainsPoint(_builtInCameraView.bounds, pointInView)) {

        AVCaptureVideoPreviewLayer *layer = (AVCaptureVideoPreviewLayer *)self.builtInCameraView.layer;
        CGPoint devicePoint = [layer captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:_builtInCameraView]];
        [_builtInCamera setPointOfInterest:devicePoint];
        
        self.focusBoxView.center = pointInView;
        self.focusBoxView.alpha = 1.0;
        
        [UIView animateWithDuration:0.5
                              delay:2.5
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^(){
                             self.focusBoxView.alpha = 0.0;
                         }
                         completion:NULL];
        
        if ([self.delegate respondsToSelector:@selector(builtinCameraView:didHandleTapGesture:)]) {
            [self.delegate builtinCameraView:self didHandleTapGesture:gestureRecognizer];
        }
    }
    
}

- (void)drawingTubeSliderValueChanged:(id)sender {
    self.builtInCameraView.alpha = self.drawingTubeSlider.value;
}

#pragma mark - notification

- (void)appDidEnterBackground:(NSNotification *)notification {
    [self.builtInCamera stop];
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    if (_builtInCameraView) {
        [self.builtInCamera play];
        [_builtInCameraView setSession:self.builtInCamera.session];
    }
}

#pragma mark - UIGestureReco

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return (BOOL)(self.mode == CZBuiltInCameraViewModeMacro);
}

@end
