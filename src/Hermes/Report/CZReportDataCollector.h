//
//  CZReportDataCollector.h
//  Matscope
//
//  Created by Ralph Jin on 6/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZDocManager;

/** Abstract base class of CZReportDataCollector.*/
@protocol CZReportDataCollector <NSObject>
@required
- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID;

- (NSDictionary *)collectedData;

@end

/** Class of report particle table collector. */
@interface CZReportParticleTableCollector : NSObject<CZReportDataCollector>

@end

/** Class of report particle table collector, which merge multiple tables into one. */
@interface CZReportMergedParticleTableCollector : NSObject<CZReportDataCollector>

@end