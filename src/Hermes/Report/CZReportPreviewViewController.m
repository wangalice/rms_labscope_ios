//
//  CZReportPreviewViewController.m
//  Hermes
//
//  Created by Ralph Jin on 4/3/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportPreviewViewController.h"
#import <QuickLook/QuickLook.h>
#import <ReportFramework/CZReportEngine.h>
#import <ReportFramework/CZReportGeneratorErrorHandler.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "CZAlertController.h"
#import "CZUIDefinition.h"
#import "CZAppDelegate.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZAbstractReportManager.h"
#import <CZAnnotationKit/CZElementLayer.h>
#import <CZAnnotationKit/CZElement.h>
#import <CZAnnotationKit/CZFeature.h>
#import "CZReportField.h"
#import "CZPostOffice.h"
#import <CZFileKit/CZFileKit.h>

@interface CZDummyPreviewItem : NSObject<QLPreviewItem>

@property(nullable, retain, nonatomic) NSURL * previewItemURL;

/*!
 * @abstract The item's title this will be used as apparent item title.
 * @discussion The title replaces the default item display name. This property is optional.
 */
@property(nullable, retain, nonatomic) NSString * previewItemTitle;

@end

@implementation CZDummyPreviewItem

- (id)init {
    self = [super init];
    if (self) {
        _previewItemURL = [NSURL fileURLWithPath:@""];
    }
    
    return self;
}

- (void)dealloc {
    _previewItemTitle = nil;
    _previewItemURL = nil;
}

@end

@interface CZReportPreviewViewController () <GenerateReportDelegate, CZReportManagerDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate> {
  @private
    BOOL _isSaving;
    BOOL _errorReceived;
    BOOL _isButtonEnabled;
    ReportEngineReportType _previewType;
    ReportEngineReportType _generateType;
    dispatch_queue_t _reportQueue;
    
    BOOL _shouldRelease;
}

@property (nonatomic, copy) NSString *previewFilePath;
@property (nonatomic, copy) NSString *reportFilePath;  // final result of report file path

@property (nonatomic, strong) CZReportEngine *reportEngine;
@property (nonatomic, copy) void(^reportFinishBlock)(CZReportPreviewViewController*, BOOL);

@property (nonatomic, strong) CZPostOffice *postOffice;
@property (nonatomic, strong) CZBarButtonItem *shareButton;
@property (nonatomic, strong) CZBarButtonItem *saveButton;

@property (atomic, assign) BOOL reportDataReady;

@property (nonatomic, strong) QLPreviewController *previewController;
@property (nonatomic, strong) CZDummyPreviewItem *dummyPreviewItem;

@end

@implementation CZReportPreviewViewController

+ (NSString *)extensionFromReportType:(ReportEngineReportType)type {
    switch (type) {
        case ReportEngineReportTypePDF:
            return @"pdf";
        case ReportEngineReportTypeRTF:
            return @"rtf";
        default:
            return @"";
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _reportQueue = dispatch_queue_create("com.zeisscn.report.generating", NULL);
        _postOffice = [[CZPostOffice alloc] init];
        _dummyPreviewItem = [[CZDummyPreviewItem alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    self.view.backgroundColor = [UIColor cz_gs105];
    CZBarButtonItem *saveButton = [[CZBarButtonItem alloc] initWithTitle:L(@"SAVE")
                                                                  target:self
                                                                  action:@selector(saveAction:)];
    CZBarButtonItem *shareButton = [[CZBarButtonItem alloc] initWithIcon:[CZIcon iconNamed:@"upload"]
                                                                  target:self
                                                                  action:@selector(shareAction:)];

    self.navigationItem.title = L(@"REPORT_PREVIEW_VIEW_TITLE");
    self.navigationItem.rightBarButtonItems = @[saveButton, shareButton];

    self.saveButton = saveButton;
    self.shareButton = shareButton;
    
    [self enableButtons:NO];
    
    [self addChildViewController:self.previewController];
    [self.view addSubview:self.previewController.view];
    [self.previewController didMoveToParentViewController:self];

    self.previewController.currentPreviewItemIndex = 0;
    
    // TODO: investigate not to push to navigation view controller.
    // Workaround for memory leak of iOS 10, see also https://forums.developer.apple.com/thread/63020
    NSOperatingSystemVersion ios11_0_0 = (NSOperatingSystemVersion){11, 0, 0};
    NSOperatingSystemVersion ios10_0_0 = (NSOperatingSystemVersion){10, 0, 0};
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios11_0_0]) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
            _shouldRelease = NO;
        }
    } else if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios10_0_0]) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
            _shouldRelease = YES;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_shouldRelease) {
        _shouldRelease = NO;
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.previewController.view.frame = self.view.bounds;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.postOffice dismiss];
    
    if (!_isSaving) {
        [[NSFileManager defaultManager] removeItemAtPath:_reportFilePath error:nil];
    }
    
    if (_generateType != _previewType) {
        [[NSFileManager defaultManager] removeItemAtPath:_previewFilePath error:nil];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    BOOL previewFileExist = [[NSFileManager defaultManager] fileExistsAtPath:self.previewFilePath];    
    if (self.reportDataReady && !previewFileExist) {
        self.reportFinishBlock = nil;
        [self.reportEngine cancel];
        
        [self enableButtons:NO];
        
        NSError *error = [NSError errorWithDomain:kReportGeneratorErrorDomain
                                             code:CZReportGeneratorErrorMemoryNotEnough
                                         userInfo:nil];
        [self showReportErrorAlert:error];
    }
}

- (void)dealloc {
    _reportManager.delegate = nil;
    _reportEngine.delegate = nil;
}

- (void)saveAction:(id)sender {
    if ([self.postOffice canDismiss]) {
        [self.postOffice dismiss];
        return;
    }

    _isSaving = YES;
    
    if (_generateType == _previewType) {
        [self popSelfToRootViewController];
    } else {
        if ([_reportFilePath length] == 0) {
            NSString *extension = [CZReportPreviewViewController extensionFromReportType:_generateType];
            self.reportFilePath = [self uniqueReportFileNameOfExtension:extension];
            
            CZReportEngine *engine = [[CZReportEngine alloc] initEngine:_reportManager.reportData
                                                         reportTemplate:_reportManager.reportTemplate];
            engine.delegate = self;
            
            NSError *error = nil;
            if ([engine validateReport:&error isReport:YES]) {
                self.reportEngine = engine;
                
                dispatch_async(_reportQueue, ^{
                    [engine exportReport:_reportFilePath
                              reportType:_generateType
                       imageCompressRate:_reportManager.imageCompressRatio
                                  useDpi:_reportManager.imageDPI];
                });
                
                [self showBusyIndicator];
                [self enableButtons:NO];

                __weak typeof(self) weakSelf = self;
                self.reportFinishBlock = ^(CZReportPreviewViewController *controller, BOOL success) {
                    [weakSelf popSelfToRootViewController];
                };
            }

        } else {
            [self popSelfToRootViewController];
        }
    }
}

- (void)shareFilePath:(NSString *)filePath {
    if (_errorReceived) {
        return;
    }
    
    if (filePath) {
        [self.postOffice reset];
        [self.postOffice addShareItemFromPath:filePath];
        [self.postOffice presentActivityViewControllerFromBarButtonItem:self.shareButton];
    }
}

- (void)shareAction:(id)sender {
    if (_generateType == _previewType) {
        [self shareFilePath:_previewFilePath];
    } else {
        if ([self.reportFilePath length] == 0) {
            NSString *extension = [CZReportPreviewViewController extensionFromReportType:_generateType];
            self.reportFilePath = [self uniqueReportFileNameOfExtension:extension];

            CZReportEngine *engine = [[CZReportEngine alloc] initEngine:_reportManager.reportData
                                                         reportTemplate:_reportManager.reportTemplate];
            engine.delegate = self;

            NSError *error = nil;
            if ([engine validateReport:&error isReport:YES]) {
                self.reportEngine = engine;
                
                dispatch_async(_reportQueue, ^{
                    [engine exportReport:_reportFilePath
                              reportType:_generateType
                       imageCompressRate:_reportManager.imageCompressRatio
                                  useDpi:_reportManager.imageDPI];
                });
            
                [self showBusyIndicator];
                [self enableButtons:NO];
            
                self.reportFinishBlock = ^(CZReportPreviewViewController *controller, BOOL success) {
                    if (success) {
                        [controller shareFilePath:controller.reportFilePath];
                    }
                    
                    [controller enableButtons:YES];
                };
            }
        } else {
            [self shareFilePath:self.reportFilePath];
        }
    }
}

- (void)setMutipleImageOptionsPreset:(NSUInteger)preset {
    
}

- (BOOL)generateReport {
    _generateType = self.reportManager.reportType;
    _previewType = self.reportManager.reportType;
    
    if (_generateType == ReportEngineReportTypeRTF) {
        _previewType = ReportEngineReportTypePDF;
        NSString *extension = [CZReportPreviewViewController extensionFromReportType:_previewType];
        NSString *fileName = @"Report";
        fileName = [fileName stringByAppendingPathExtension:extension];
        
        self.previewFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        self.reportFilePath = nil;
    } else {
        NSString *extension = [CZReportPreviewViewController extensionFromReportType:_generateType];
        self.previewFilePath = [self uniqueReportFileNameOfExtension:extension];
        self.reportFilePath = self.previewFilePath;
    }
    
    [self showBusyIndicator];
    
    _reportManager.delegate = self;
    
    self.reportDataReady = NO;
    
    dispatch_async(_reportQueue, ^{
        NSError *error = nil;
        BOOL success = [_reportManager fillReportData:&error];
        if (!success) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self hideBusyIndicator];
                if (error) {
                    [self showReportErrorAlert:error];
                }
            });
            return;
        }
        
        [_reportManager generateReportImageIfNeed];
        self.reportDataReady = YES;
        
        // validate and report
        error = nil;
        CZReportEngine *engine = [[CZReportEngine alloc] initEngine:_reportManager.reportData reportTemplate:_reportManager.reportTemplate];
        engine.delegate = self;

        if ([engine validateReport:&error isReport:YES]) {
            self.reportEngine = engine;
            self.reportFinishBlock = ^(CZReportPreviewViewController *controller, BOOL success) {
                if (success) {
                    [controller enableButtons:YES];
                    [controller previewFile];
                }
            };
            
            [engine exportReport:_previewFilePath
                      reportType:_previewType
               imageCompressRate:_reportManager.imageCompressRatio
                          useDpi:_reportManager.imageDPI];
        } else {
#ifdef DEBUG
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"lastReportData.plist"];
            [_reportManager.reportData writeToFile:filePath atomically:YES];
            filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"lastReportTemplate.plist"];
            [_reportManager.reportTemplate writeToFile:filePath atomically:YES];
#endif
            NSString *errorMsg = [NSString stringWithFormat:@"error domain is: %@\n error code is: %ld\n  error message is: %@",
                                  [error domain], (long)[error code], [error localizedDescription]];
            CZLogv(@"%@", errorMsg);
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self hideBusyIndicator];
                [self showReportErrorAlert:error];
            });
        }
        
    });

    return YES;
}

#pragma mark - Private methods

- (NSString *)uniqueReportFileNameOfExtension:(NSString *)extension {
    NSString *reportFilePath = [self.reportManager uniqueReportFilePathOfExtension:extension];
    if ([[NSFileManager defaultManager] fileExistsAtPath:reportFilePath]) {
        reportFilePath = [CZFileNameGenerator uniqueFilePath:reportFilePath];
    }
    
    return reportFilePath;
}

- (void)previewFile {
    if (_errorReceived) {
        return;
    }

    self.previewController.currentPreviewItemIndex = 0;

    if (self.dummyPreviewItem) {
        self.dummyPreviewItem = nil;
    }

    [self.previewController reloadData];
}

- (void)enableButtons:(BOOL)enabled {
    for (UIBarButtonItem *button in self.navigationItem.rightBarButtonItems) {
        button.enabled = enabled;
    }
    
    for (UIBarButtonItem *button in self.navigationItem.leftBarButtonItems) {
        button.enabled = enabled;
    }
    
    self.shareButton.enabled = enabled;
    
    _isButtonEnabled = enabled;
}

- (void)showBusyIndicator {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setColor:kSpinnerBackgroundColor];
    [hud setLabelText:L(@"BUSY_INDICATOR_GENERATING_REPORT")];
    hud.taskInProgress = YES;

    if (self.dummyPreviewItem) {
        self.dummyPreviewItem.previewItemTitle = hud.labelText;
        [self.previewController reloadData];
    }
}

- (void)hideBusyIndicator {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)popSelfToRootViewController {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController {
    return 1;
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)index {
    if (self.dummyPreviewItem == nil) {
        NSURL *fileURL = nil;
        if ([_previewFilePath length] > 0) {
            fileURL = [NSURL fileURLWithPath:_previewFilePath];
        }
        
        if (fileURL == nil) {
            fileURL = [NSURL fileURLWithPath:@""];
        }
        
        return fileURL;
    } else {
        return self.dummyPreviewItem;
    }
}

#pragma mark - QLPreviewControllerDelegate

- (BOOL)previewController:(QLPreviewController *)controller shouldOpenURL:(NSURL *)url forPreviewItem:(id <QLPreviewItem>)item {
    return YES;
}

#pragma mark - CZReportManagerDelegate

- (void)reportManager:(CZAbstractReportManager *)reportManager prepareReportDataReachProgress:(float)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.progress = progress * 0.5;
        
        if (self.dummyPreviewItem) {
            NSString *title = [NSString stringWithFormat:@"%@ %.0f%%", hud.labelText, hud.progress * 100.0];
            self.dummyPreviewItem.previewItemTitle = title;
            [self.previewController reloadData];
        }
    });
}

#pragma mark - GenerateReportDelegate

- (void)generateDidFinishDestination:(NSString *)filePath {
    dispatch_async(dispatch_get_main_queue(), ^{
        CZLogv(@"the generating report %@ did finished.", filePath);
    
        if (_reportFinishBlock) {
            _reportFinishBlock(self, YES);
            self.reportFinishBlock = nil;
        }
        
        self.reportEngine = nil;
        
        [self hideBusyIndicator];

        self.navigationItem.title = [[filePath lastPathComponent] stringByDeletingPathExtension];
    });
}

- (void)generateDidFailWithError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *errorInfo = [NSString stringWithFormat:@"error domain is: %@\n error code is: %ld\n  error message is: %@",
                               [error domain], (long)[error code], [error localizedDescription]];
        CZLogv(@"%@", errorInfo);
        
        if (_reportFinishBlock) {
            _reportFinishBlock(self, NO);
            self.reportFinishBlock = nil;
        }
        
        self.reportEngine = nil;

        [self hideBusyIndicator];
        
        [self showReportErrorAlert:error];
    });
}

- (void)generateProgress:(ReportEngineReportType)type currentPercentage:(CGFloat)percent destination:(NSString *)filePath {
    NSString *progress = [NSString stringWithFormat:@"the percentage is %f, the report type is %lu, destination is %@", percent, (unsigned long)type, filePath];
    CZLogv(@"%@", progress);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        hud.progress = percent * 0.5 + 0.5;
        
        if (self.dummyPreviewItem) {
            NSString *title = [NSString stringWithFormat:@"%@ %.0f%%", hud.labelText, hud.progress * 100.0];
            self.dummyPreviewItem.previewItemTitle = title;
            [self.previewController reloadData];
        }
    });
}

- (void)showReportErrorAlert:(NSError *)error {
    if (error == nil || _errorReceived) {
        return;
    }
    
    NSString *message = nil;
    
    switch ([error code]) {
        case CZReportGeneratorErrorReportDataError:
            message = L(@"REPORT_ERROR_NO_DATA");
            break;
        case CZReportGeneratorErrorUserCanceled:
            break;
        case CZReportGeneratorErrorMemoryNotEnough:
            message = L(@"REPORT_ERROR_NOT_ENOUGH_MEMORY");
            break;
        case CZReportGeneratorErrorPageNumberUnexpected:
            message = [NSString stringWithFormat:L(@"REPORT_CANCELLED_TOO_MANY_PAGES"), 100];
            break;
        default:
            message = L(@"REPORT_ERROR_INTERNAL");
            break;
    }

    if (message) {
        _errorReceived = YES;
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:message level:CZAlertLevelError];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert presentAnimated:YES completion:nil];
    }
}

#pragma mark - Getter

- (QLPreviewController *)previewController {
    if (_previewController == nil) {
        _previewController = [[QLPreviewController alloc] init];
        _previewController.delegate = self;
        _previewController.dataSource = self;
    }
    return _previewController;
}

@end
