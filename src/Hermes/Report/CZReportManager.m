//
//  CZReportManager.m
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportManager.h"

#import <ReportFramework/CZGlobal.h>
#import <ReportFramework/CZReportGeneratorErrorHandler.h>

#import <CZDocumentKit/CZDocumentKit.h>
#import "CZReportField.h"
#import <CZAnnotationKit/CZElementLayer.h>
#if REPORT_REFACTOR
#import "CZGrainSizeMeasurement.h"
#endif

// report data collectors
#import "CZReportDataCollector.h"
#import "CZReportMeasurementTableCollector.h"
#import "CZReportPhaseChartCollector.h"
#import "CZReportTemplateFilesManager.h"

@interface CZReportManager () {
    /** pair of original file path and temp file path; If the paths in pair are same, then it doesn't use temp file. */
    BOOL _requireGrainSize;
}

@end

@implementation CZReportManager

@synthesize mainDocManager = _mainDocManager;

// moved to super class

- (id)initWithDocManager:(CZDocManager *)docManager {
    self = [super init];
    if (self) {
        _mainDocManager = docManager;
    }
    return self;
}

- (void)dealloc {

}

- (void)checkAdditionalFiles {
    self.mixedRemainingMultiphase = NO;
    self.hasUnsupportedFiles = NO;
    self.mixedScaling = NO;
    
    if (self.mainDocManager == nil) {
        self.hasUnsupportedFiles = YES;
        return;
    }
        
    if (self.additionalFiles.count == 0) {
        return;
    }
    
    const BOOL requireMultiphaseChart = [self requireCollectMultiphaseChart];
    const BOOL requireParticlesTable = [self requireCollectParticleTable];

    NSNumber *firstMultiphaseDocScaling = nil;
    NSNumber *firstParticleDocScaling = nil;
    NSNumber *firstDocRemaingEnabled = nil;
    
    if (self.mainDocManager.multiphase.phaseCount > 0) {
        if (self.mainDocManager.multiphase.isParticlesMode) {
            firstParticleDocScaling = @(self.mainDocManager.elementLayer.scaling);
        } else {
            firstMultiphaseDocScaling = @(self.mainDocManager.elementLayer.scaling);
            firstDocRemaingEnabled = @(self.mainDocManager.multiphase.remainingEnabled);
        }
    }
    
    for (NSString *filePath in self.additionalFiles) {
        CZDocManager *docManager = [CZDocManager newDocManagerWithDummyImageFromFilePath:filePath];
        if (docManager == nil) {  // check unsupport file
            self.hasUnsupportedFiles = YES;
            return;
        }
        
        if (self.perGroupMeasurements) {
            continue;
        }
        
        if (docManager.multiphase.phaseCount == 0) {
            continue;
        }
        
        if (docManager.multiphase.isParticlesMode) {  // check if it is OK to merge particle files
            if (requireParticlesTable) {
                if (firstParticleDocScaling == nil) {
                    firstParticleDocScaling = @(docManager.elementLayer.scaling);
                } else {
                    BOOL mixedFlag = (docManager.elementLayer.scaling > 0) != ([firstParticleDocScaling floatValue] > 0);
                    if (mixedFlag) {
                        self.mixedScaling = YES;
                        return;
                    }
                }
            }
        } else {  // check if it is OK to merge multiphase files
            if (requireMultiphaseChart) {
                if (firstDocRemaingEnabled == nil) {
                    firstDocRemaingEnabled = @(docManager.multiphase.remainingEnabled);
                } else {
                    BOOL mixedFlag = docManager.multiphase.isRemainingEnabled != [firstDocRemaingEnabled boolValue];
                    if (mixedFlag) {
                        self.mixedRemainingMultiphase = YES;
                        return;
                    }
                }
                
                if (firstMultiphaseDocScaling == nil) {
                    firstMultiphaseDocScaling = @(docManager.elementLayer.scaling);
                } else {
                    BOOL mixedFlag = (docManager.elementLayer.scaling > 0) != ([firstMultiphaseDocScaling floatValue] > 0);
                    if (mixedFlag) {
                        self.mixedScaling = YES;
                        return;
                    }
                }
            }
        }
    }
}

- (BOOL)fillReportData:(NSError **)error {
    NSString *unknownString = L(@"FILES_INFO_UNKNOWN");
    
    NSMutableDictionary *reportData = [self.userInputData mutableCopy];
    
    reportData[@"_date"] = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    
    reportData[@"_filename"] = [self.mainDocManager.filePath lastPathComponent];
    reportData[@"_operatorName"] = [CZDefaultSettings sharedInstance].operatorName;
    
    if (self.mainDocManager.imageProperties.acquisitionDate) {
        reportData[@"_acquisitionTime"] = [NSDateFormatter localizedStringFromDate:self.mainDocManager.imageProperties.acquisitionDate
                                                                   dateStyle:NSDateFormatterMediumStyle
                                                                   timeStyle:NSDateFormatterShortStyle];
    } else {
        reportData[@"_acquisitionTime"] = unknownString;
    }
    
    if (self.mainDocManager.imageProperties.channels[0].exposureTimeInSeconds) {
        reportData[@"_exposureTime"] = [CZCommonUtils localizedStringFromFloat:self.mainDocManager.imageProperties.channels[0].exposureTimeInSeconds.floatValue  * 1e3f precision:2U];
    } else {
        reportData[@"_exposureTime"] = unknownString;
    }
    
    if (self.mainDocManager.imageProperties.magnificationFactor) {
        reportData[@"_magnification"] = self.mainDocManager.imageProperties.magnificationFactorString;
    } else {
        reportData[@"_magnification"] = unknownString;
    }
    
    if (self.mainDocManager.imageProperties.objectiveModel) {
        reportData[@"_objective"] = self.mainDocManager.imageProperties.objectiveModel;
    } else {
        reportData[@"_objective"] = unknownString;
    }
    
    const NSUInteger imageCount = self.additionalFiles.count + 1;
    reportData[@"_numberOfImages"] = [NSString stringWithFormat:@"%lu", (unsigned long)imageCount];

    _requireGrainSize = NO;
    for (CZReportField *field in self.textFields) {
        if ([field.key isEqualToString:@"_grainSizeNumber"]) {
            _requireGrainSize = YES;
            break;
        }
    }
    
    if (_requireGrainSize) {
        CZGrainSizeResult *grainSizeResult = self.mainDocManager.grainSizeResult;
        if (grainSizeResult) {
            reportData[@"_grainSizeNumber"] = [CZCommonUtils localizedStringFromFloat:grainSizeResult.roundedGrainSize precision:1U];
            reportData[@"_grainSizeStandard"] = [grainSizeResult standardName];
            NSString *meanDistance = [NSString stringWithFormat:@"%@%@",
                                      [CZCommonUtils localizedStringFromFloat:grainSizeResult.meanDistance precision:1U],
                                      L(@"UNIT_UM")];
            reportData[@"_grainMeanDistance"] = meanDistance;
            reportData[@"_grainPattern"] = grainSizeResult.localizedPatternName;
            reportData[@"_grainIntersectionPoints"] = [CZCommonUtils localizedStringFromFloat:grainSizeResult.intersectionPointsCount precision:0U];
        } else {
            reportData[@"_grainSizeNumber"] = unknownString;
            reportData[@"_grainSizeStandard"] = unknownString;
            reportData[@"_grainMeanDistance"] = unknownString;
            reportData[@"_grainPattern"] = unknownString;
            reportData[@"_grainIntersectionPoints"] = unknownString;
        }
    }

    BOOL needCollectData = (([self.chartFields count] > 0) || ([self.tableFields count] > 0));

    if (needCollectData) {
        NSMutableDictionary *collectors = [[NSMutableDictionary alloc] initWithCapacity:4];
        // create measurement chart collector
        if ([self requireCollectMeasurementChart]) {
            CZReportMeasurementChartCollector *collector = [[CZReportMeasurementChartCollector alloc] init];
            collector.perGroupMeasurements = self.perGroupMeasurements;
            collectors[@"chart.measurements"] = collector;
        }
        
        // create multiphase chart collector
        if ([self requireCollectMultiphaseChart]) {
            if (self.perGroupMeasurements || self.additionalFiles.count == 0) {
                CZReportPhaseChartCollector *collector = [[CZReportPhaseChartCollector alloc] init];
                collectors[@"chart.multiphase"] = collector;
            } else {
                CZReportMergedPhaseChartCollector *collector = [[CZReportMergedPhaseChartCollector alloc] init];
                collector.isRemainingEnabled = self.mainDocManager.multiphase.isRemainingEnabled;
                collectors[@"chart.multiphase"] = collector;
            }
        }
        
        // create measurement table collector
        if ([self requireCollectMeasurementTable]) { // require collect table data
            CZReportMeasurementTableCollector *collector = [[CZReportMeasurementTableCollector alloc] init];
            collector.perGroupMeasurements = self.perGroupMeasurements;
            collectors[@"table.measurements"] = collector;
        }
        
        // create particles table collector
        if ([self requireCollectParticleTable]) {
            id collector = nil;
            if (self.perGroupMeasurements || self.additionalFiles.count == 0) {
                collector = [[CZReportParticleTableCollector alloc] init];
            } else {
                collector = [[CZReportMergedParticleTableCollector alloc] init];
            }
            collectors[@"table.particles"] = collector;
        }
        
        for (id key in collectors) {
            id <CZReportDataCollector> collector = collectors[key];
            [collector collectFromDocManager:self.mainDocManager docID:0];
        }

        NSUInteger docID = 1;
        
        for (NSString *filePath in self.additionalFiles) {
            CZDocManager *docManager = [CZDocManager newDocManagerWithDummyImageFromFilePath:filePath];
            NSAssert(docManager, @"failed to open image file at: %@", filePath);
            
            if (docManager) {
                for (id key in collectors) {
                    id <CZReportDataCollector> collector = collectors[key];
                    [collector collectFromDocManager:docManager docID:docID];
                }
                
                ++docID;
            }
            
            // notify progress
            if ([self.delegate respondsToSelector:@selector(reportManager:prepareReportDataReachProgress:)]) {
                float progress = (float)docID / imageCount;
                progress = progress * 0.5;
                [self.delegate reportManager:self prepareReportDataReachProgress:progress];
            }
        }

        for (CZReportField *reportField in self.chartFields) {
            NSString *collectKey = @"";
            if ([reportField.key isEqualToString:kCZMultiphaseChartKey]) {
                collectKey = @"chart.multiphase";
            } else {  // any other chart treats as measurement chart
                collectKey = @"chart.measurements";
            }
            
            id <CZReportDataCollector> collector = collectors[collectKey];
            NSDictionary *data = [collector collectedData];
            if (data) {
                reportData[reportField.key] = data;
            }
        }

        for (CZReportField *reportField in self.tableFields) {
            NSString *collectKey = @"";
            if ([reportField.key isEqualToString:kCZParticleTableKey]) {
                collectKey = @"table.particles";
            } else {  // any other chart treats as measurement chart
                collectKey = @"table.measurements";
            }
            
            id <CZReportDataCollector> collector = collectors[collectKey];
            NSDictionary *data = [collector collectedData];
            if (data) {
                NSUInteger itemCount = 0;
                NSArray *values = data[kValues];
                if ([values isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *valueDict in values) {
                        if ([valueDict isKindOfClass:[NSDictionary class]]) {
                            id value = valueDict[kValue];
                            if ([value respondsToSelector:@selector(count)]) {
                                itemCount += [value count];
                            }
                        }
                    }
                }
                
                if (itemCount < 6000) { // App doesn't support too large table in report.
                    reportData[reportField.key] = data;
                } else {
                    if (error) {
                        *error = [CZReportGeneratorErrorHandler createErrorWithCode:CZReportGeneratorErrorPageNumberUnexpected];
                    }
                    return NO;
                }
            }
        }
    }

    // fill report image with temporary burn image
    if ([self requireCollectImage]) {
        [self appendMainDocImage];
        
        for (NSString *filePath in self.additionalFiles) {
            [self appendReportImage:filePath];
        }
        
        id value = nil;
        if (self.tempImagePaths.count) {
            // collect image providers, same as [_tempImagePaths allValues] but in |additionalFiles|'s order.
            NSMutableArray *tempArray = [NSMutableArray array];
            
            NSUInteger imageID = 1;
            NSString *imageCaption = [CZReportManager imageCaptionFromFile:self.mainDocManager.filePath andID:imageID++];
            NSString *tempImagePath = self.mainDocManager.isModified ? self.tempImagePaths[kModifiedMainDocKey] : self.tempImagePaths[self.mainDocManager.filePath];
            [tempArray addObject:@{kValue:tempImagePath,
                                   kImageCaption: imageCaption}];
            
            if (self.imageRenderMode) {
                for (NSString *filePath in self.additionalFiles) {
                    if (self.tempImagePaths[filePath]) {
                        imageCaption = [CZReportManager imageCaptionFromFile:filePath andID:imageID++];
                        [tempArray addObject:@{kValue: self.tempImagePaths[filePath],
                                               kImageCaption: imageCaption}];
                    }
                }
            
                value = @{kVersion: @1,
                          kValues: tempArray,
                          kImageRenderMode: self.imageRenderMode};
            } else {
                value = @{kVersion: @1,
                          kValues: tempArray};
            }
        }
        
        if (value) {
            for (CZReportField *reportField in self.imageFields) {
                if ([reportField.key isEqualToString:kCZReserveReportImageKey]) {
                    reportData[reportField.key] = value;
                }
            }
        }
    }

    self.reportData = reportData;
    return YES;
}

// moved to super class

- (void)generateReportImageIfNeed {
    [self createTemporaryDirectory];
    
    const BOOL isMultiphaseShown = [self requireCollectMultiphaseChart];
    const BOOL isParticlesShown = [self requireCollectParticleTable];
    
    NSDictionary *tempImagePaths = [NSDictionary dictionaryWithDictionary:self.tempImagePaths];
    NSUInteger imageCount = tempImagePaths.count;
    NSUInteger i = 0;
    
    for (NSString *originalFilePath in tempImagePaths) {
        @autoreleasepool {
            NSString *tempFilePath = tempImagePaths[originalFilePath];
            if (![[NSFileManager defaultManager] fileExistsAtPath:tempFilePath]) {
                CZDocManager *docManager = nil;
                if ([originalFilePath isEqualToString:kModifiedMainDocKey]) {
                    docManager = self.mainDocManager;
                } else if (!self.mainDocManager.isModified &&
                    [originalFilePath isEqualToString:self.mainDocManager.filePath]) {
                    docManager = self.mainDocManager;
                } else {
                    if (self.imageRenderMode == nil) {
                        i++;
                        continue;
                    }
                    docManager = [CZDocManager newDocManagerFromFilePath:originalFilePath];
                }
                
                UIImage *originalImage = [docManager.document outputImage];
                
                UIImage *burnInImage = nil;
                @autoreleasepool {
                    UIImage *tempImage = [docManager imageByBurningAnnotationsOnImage:originalImage];
                    burnInImage = tempImage;
                }
                UIImage *reportImage = nil;
                
                CZMultiphase *multiphase = docManager.multiphase;

                BOOL showMultiphaseOverlay;
                if (multiphase.isParticlesMode) {
                    showMultiphaseOverlay = isParticlesShown && multiphase.phaseCount;
                } else {
                    showMultiphaseOverlay = isMultiphaseShown && multiphase.phaseCount;
                }
                
                BOOL showGrainSizeOverlay = NO;
                if (_requireGrainSize && docManager.grainSizeResult) {
                    showGrainSizeOverlay = YES;
                }
                
                if (showMultiphaseOverlay || showGrainSizeOverlay) {  // apply multiphase overlay if need
                    CGSize imageSize = originalImage.size;
                    CGSize firstImageSize = imageSize;
                    if (showGrainSizeOverlay) {
                        firstImageSize.width /= 4;
                        firstImageSize.height /= 4;
                    }
                    
                    BOOL isLandscape = imageSize.width > imageSize.height;
                    
                    // merge original image and multiphase image together
                    const CGFloat kGapBetweenImages = 20;

                    CGSize canvasSize;
                    if (isLandscape) {
                        canvasSize = CGSizeMake(imageSize.width, ceil(imageSize.height + firstImageSize.height) + kGapBetweenImages);
                    } else {
                        canvasSize = CGSizeMake(imageSize.width + ceil(firstImageSize.width) + kGapBetweenImages, imageSize.height);
                    }
                    UIGraphicsBeginImageContext(canvasSize);
                    
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    
                    // fill white background
                    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
                    CGContextFillRect(context, CGRectMake(0, 0, canvasSize.width, canvasSize.height));
                    
                    // draw original image in 1/4 size
                    if (isLandscape) {
                        [burnInImage drawInRect:CGRectMake((imageSize.width - firstImageSize.width) / 2, 0, firstImageSize.width, firstImageSize.height)];
                    } else {
                        [burnInImage drawInRect:CGRectMake(0, (imageSize.height - firstImageSize.height) / 2, firstImageSize.width, firstImageSize.height)];
                    }
                    burnInImage = nil;
                    
                    // draw image with multiphase/grainSize overlay
                    if (isLandscape) {
                        CGContextTranslateCTM(context, 0, firstImageSize.height + kGapBetweenImages);
                    } else {
                        CGContextTranslateCTM(context, firstImageSize.width + kGapBetweenImages, 0);
                    }
                    
                    CGContextClipToRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
                    [originalImage drawAtPoint:CGPointZero];
                    
                    if (docManager != self.mainDocManager) {
#if REPORT_REFACTOR
                        docManager.image = nil; // release the image;
#endif
                    }
                    
                    CZGrainSizeResult *grainSizeResult = nil;
                    CGFloat lineWidth = 1;
                    CGFloat scaling = docManager.calibratedScaling;
                    if (showGrainSizeOverlay) {
                        lineWidth = [docManager.elementLayer suggestLineWidthForSize:CZElementSizeMedium];
                        grainSizeResult = docManager.grainSizeResult;
                    }
                    
                    if (showMultiphaseOverlay) {
                        CZScanLine *scanline = [[CZScanLine alloc] initWithWidth:imageSize.width height:imageSize.height];
                        
                        CZRegions2DItemCollection *regionCollection = docManager.analyzer2dResult.regions2dItemCollection;
                        docManager = nil;
                        
                        for (NSNumber *key in regionCollection) {
                            uint32_t i = [key unsignedIntValue];
                            i = [CZDocManager phaseIDFromRegionsID:i];
                            
                            UIColor *color = nil;
                            if (i < multiphase.phaseCount) {
                                CZPhase *phase = [multiphase phaseAtIndex:i];
                                color = phase.color;
                            }
                            
                            if (color) {
                                CZRegions2DItem *regionsItem = [regionCollection objectForKey:key];
                                for (CZRegion2DItem *regionItem in regionsItem) {
                                    CZRegion2D* rgn = regionItem.region;
                                    [rgn fillInImage:scanline];
                                }

                                CGContextSetFillColorWithColor(context, [color CGColor]);
                                [scanline draw];
                                [scanline clear];
                            }
                        }
                        
                        if (_mainDocManager.analyzer2dData) {
                            _mainDocManager.analyzer2dResult = nil;
                        }
                    }
                    
#if REPORT_REFACTOR
                    if (grainSizeResult) {
                        CZGrainSizeMeasurement *grainSizeMeasurement = [[CZGrainSizeMeasurement alloc] initWithImageSize:imageSize scaling:scaling];
                        grainSizeMeasurement.grainSizeResult = grainSizeResult;
                        
                        CALayer *grainSizeLayer = [grainSizeMeasurement newLayerWithLineWidth:lineWidth];
                        
                        [grainSizeLayer renderInContext:context];
                    }
#endif
                    
                    reportImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                } else {
                    reportImage = burnInImage;
                }
                
                NSData *imageData = UIImagePNGRepresentation(reportImage);
                [imageData writeToFile:tempFilePath atomically:YES];
            }
            
            i++;
            if ([self.delegate respondsToSelector:@selector(reportManager:prepareReportDataReachProgress:)]) {
                float progress = (float)i / imageCount;
                progress = 0.5 + progress * 0.5;
                [self.delegate reportManager:self prepareReportDataReachProgress:progress];
            }
        }
    }
}

- (NSString *)uniqueReportFilePathOfExtension:(NSString *)extension {
    NSString *documentsDirectory = [CZReportTemplateFilesManager documentPath];
    NSString *fileName = [[self.mainDocManager.filePath lastPathComponent] stringByDeletingPathExtension];
    fileName = [fileName stringByAppendingPathExtension:extension];
    NSString *reportFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    return reportFilePath;
}

#pragma mark - Private methods

+ (NSString *)imageCaptionFromFile:(NSString *)filePath andID:(NSUInteger)imageID {
    NSMutableString *imageCaption = [NSMutableString string];
    
    [imageCaption appendString:[NSString stringWithFormat:@"%lu) ", (unsigned long)imageID]];
    
    NSString *tempString = [[filePath lastPathComponent] stringByDeletingPathExtension];
    [imageCaption appendString:tempString];
    [imageCaption replaceOccurrencesOfString:@"_" withString:@" / " options:0 range:NSMakeRange(0, imageCaption.length)];
    
    return [NSString stringWithString:imageCaption];
}

// moved to super class

- (void)appendMainDocImage {
    NSString *filePath = self.mainDocManager.filePath;
    NSString *fileName = nil;
    fileName = [filePath lastPathComponent];
    fileName = [fileName stringByDeletingPathExtension];
    
    if (self.mainDocManager.isModified) {
        fileName = [CZCommonUtils md5FromString:fileName];
    }

    fileName = [fileName stringByAppendingPathExtension:@"png"];
    NSString *tempFilePath = [[self temporaryFilePath] stringByAppendingPathComponent:fileName];
    if (tempFilePath == nil) {
        return;
    }
    
    if (self.mainDocManager.isModified) {
        self.tempImagePaths[kModifiedMainDocKey] = tempFilePath;
    } else {
        if (filePath) {
            self.tempImagePaths[filePath] = tempFilePath;
        }
    }
}

@end
