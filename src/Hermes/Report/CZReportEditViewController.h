//
//  CZReportEditViewController.h
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZAbstractReportManager;

@interface CZReportEditViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) CZAbstractReportManager *reportManager;

@end
