//
//  CZReportField.m
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportField.h"
#import <ReportFramework/CZGlobal.h>

#define kFieldDescription @"description"

@implementation CZReportField

- (void)dealloc {

}

- (id)initWithElementDict:(NSDictionary *)element {
    self = [super init];
    if (self) {
        id mandatory = [element objectForKey:kReuiredKey];
        if ([mandatory isKindOfClass:[NSNumber class]]) {
            _mandatory = [mandatory boolValue];
        }
        
        id elementType = [element objectForKey:kElementType];
        NSDictionary *value = [element objectForKey:kValue];
        if ([kTextElement isEqualToString:elementType]) {
            _type = CZReportFieldTypeText;
            self.key = [value objectForKey:kTextValue];
        } else if ([kImageElement isEqualToString:elementType]) {
            _type = CZReportFieldTypeImage;
            self.key = [value objectForKey:kImageFile];
        } else if ([kColumnElement isEqualToString:elementType]) {
            _type = CZReportFieldTypeChart;
            self.key = [value objectForKey:kColumnComponentArray];
        } else if ([kPieElement isEqualToString:elementType]) {
            _type = CZReportFieldTypeChart;
            self.key = [value objectForKey:kPieComponentArray];
        } else if ([kTableElement isEqualToString:elementType]) {
            _type = CZReportFieldTypeTable;
            self.key = [value objectForKey:kTableValue];
        } else {
            CZLogv(@"unknown type of report element: %@!", elementType);
        }
        
        NSString *description = [element objectForKey:kFieldDescription];
        if (description) {
            _fieldDescription = [description copy];
        }
    }
    
    return self;
}

- (id)initWithKey:(NSString *)key type:(CZReportFieldType)type {
    self = [super init];
    if (self) {
        self.key = key;
        _type = type;
        _mandatory = NO;
        _fieldDescription = nil;
    }
    
    return self;
}

#pragma mark - Private Methods

- (void)setKey:(NSString *)key {
    if (key.length > 2) {
        _key = [key substringWithRange:NSMakeRange(1, key.length - 2)];  // Remove the $ charactor, eg. $key$ -> key.
    } else {
        _key = [key substringWithRange:NSMakeRange(0, key.length)];
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"{%@, %@, %lu, %@}", _key, (_mandatory ? @"YES" : @"NO"), (unsigned long)_type, _fieldDescription];
}

@end
