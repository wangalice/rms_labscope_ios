//
//  CZReportPhaseChartCollector.h
//  Matscope
//
//  Created by Ralph Jin on 7/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZReportDataCollector.h"

@interface CZReportPhaseChartCollector : NSObject<CZReportDataCollector>

@end

@interface CZReportMergedPhaseChartCollector : NSObject<CZReportDataCollector>

@property (nonatomic, assign) BOOL isRemainingEnabled;

@end
