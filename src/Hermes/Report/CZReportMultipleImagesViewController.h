//
//  CZReportMultipleImagesViewController.h
//  Matscope
//
//  Created by Sherry Xu on 1/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

static const float kCellHeight = 96.0;
static const float kCellWidth = 128.0;
static const CGFloat kDefaultPadding = 16.0;

NS_ASSUME_NONNULL_BEGIN

@class CZReportMultipleImagesViewController;
@protocol CZReportMultipleImagesViewControllerDelegate <NSObject>

- (void)multipleImagesViewControllerDidChanged:(CZReportMultipleImagesViewController *)controller;
- (void)multipleImagesViewControllerScrollToVisible:(CZReportMultipleImagesViewController *)controller;
- (void)multipleImagesViewControllerScrollToOriginal:(CZReportMultipleImagesViewController *)controller;

@end

@class CZFilesPickerController;

@class CZDocManager;

@interface CZReportMultipleImagesViewController : UICollectionViewController

@property (nonatomic, weak) id<CZReportMultipleImagesViewControllerDelegate> delegate;

- (instancetype)initWithDocManager:(CZDocManager *)docManager addedImageFilePaths:(NSArray <NSString *> *)imageFilePaths;
- (nullable NSArray *)selectedImagePaths;
- (void)popoverControllerUpdatePopoverPosition:(UIView *)view;
- (NSUInteger)cellCount;
- (void)addImageActionWithButton:(CZButton *)button;

@end

NS_ASSUME_NONNULL_END
