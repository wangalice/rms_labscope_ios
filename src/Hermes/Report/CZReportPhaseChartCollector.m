//
//  CZReportPhaseChartCollector.m
//  Matscope
//
//  Created by Ralph Jin on 7/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZReportPhaseChartCollector.h"

#import <ReportFramework/CZGlobal.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZPhaseInfoItem : NSObject

@property (nonatomic, retain) UIColor *color;
@property (nonatomic, retain, readonly) NSMutableArray *phaseAreas;  // array of NSNumber;
@property (nonatomic, assign) NSUInteger regionsCount;

@end

@implementation CZPhaseInfoItem

@synthesize phaseAreas = _phaseAreas;

- (void)dealloc {
    
}

- (NSMutableArray *)phaseAreas {
    if (_phaseAreas == nil) {
        _phaseAreas = [[NSMutableArray alloc] init];
    }
    return _phaseAreas;
}

@end

@interface CZReportPhaseChartCollector()

@property (nonatomic, retain, readonly) NSMutableArray *phaseChartData;

@end

@implementation CZReportPhaseChartCollector
@synthesize phaseChartData = _phaseChartData;

+ (void)collectMultiphaseChartData:(NSMutableArray *)chartData inDocManager:(CZDocManager *)docManager {
    if (docManager.multiphase.isParticlesMode) {
        return;
    }
    
    [docManager analyzer2dResult];  // CAUTION: lazy load analyzer result data and multiphase area
    
    NSMutableArray *chartDataGroup = [[NSMutableArray alloc] init];
    CZMultiphase *multiphase = docManager.multiphase;
    
    CZElementUnitStyle unitStyle = [[CZDefaultSettings sharedInstance] unitStyle];
    CGFloat scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    NSString *unitString = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
    NSUInteger precision = [CZElement defaultPrecisionFromScaling:scaling];
    
    for (NSUInteger phaseID = 0; phaseID < [multiphase phaseCount]; phaseID++) {
        CZPhase *phase = [multiphase phaseAtIndex:phaseID];
        
        CGFloat r, g, b, a;
        [phase.color getRed:&r green:&g blue:&b alpha:&a];
        id colorValue = @{kRedColor: @(r), kGreenColor: @(g), kBlueColor: @(b), kAlphaColor: @(a)};
        
        NSString *phaseArea = L(@"REPORT_PHASE_AREA");
        phaseArea = [phaseArea stringByAppendingString:[CZCommonUtils localizedStringFromFloat:phase.area * scaling * scaling precision:precision]];
        phaseArea = [phaseArea stringByAppendingString:unitString];
        
        uint32_t regionCollectionID = [CZDocManager regionsIDFromPhaseID:(uint32_t)phaseID];
        CZRegions2DItem *regionsItem = [docManager.analyzer2dResult.regions2dItemCollection objectForKey:@(regionCollectionID)];
        NSString *phaseRegions = L(@"REPORT_PHASE_REGIONS");
        phaseRegions = [phaseRegions stringByAppendingString:[CZCommonUtils localizedStringFromNumber:@([regionsItem count]) precision:0]];
        
        float phasePercent = [multiphase percentOfPhaseAtIndex:phaseID];
        NSDictionary *item = @{kComponentTitle: phase.name,
                               kComponentValue: [NSNumber numberWithFloat:phasePercent * 100.0f],
                               kComponentAdditionalValues: @[phaseArea, phaseRegions],
                               kColor: colorValue};
        
        [chartDataGroup addObject:item];
    }
    
    if (docManager.multiphase.isRemainingEnabled) {
        float remainPercent = (float)multiphase.remainingArea / multiphase.imagePixelCount;
        
        CGFloat r, g, b, a;
        [docManager.multiphase.remainingColor getRed:&r green:&g blue:&b alpha:&a];
        id colorValue = @{kRedColor: @(r), kGreenColor: @(g), kBlueColor: @(b), kAlphaColor: @(a)};
        
        NSString *phaseArea = L(@"REPORT_PHASE_AREA");
        phaseArea = [phaseArea stringByAppendingString:[CZCommonUtils localizedStringFromFloat:multiphase.remainingArea * scaling * scaling precision:precision]];
        phaseArea = [phaseArea stringByAppendingString:unitString];
        
        NSDictionary *item = @{kComponentTitle: L(@"REPORT_MULTIPHASE_REMAINING"),
                               kComponentValue: [NSNumber numberWithFloat:remainPercent * 100.0f],
                               kComponentAdditionalValues: @[phaseArea],
                               kColor: colorValue};
        [chartDataGroup addObject:item];
    }
    
    if (chartDataGroup.count) {
        [chartData addObject:@{kUnit: @"%", kValue: chartDataGroup}];
    }
}

- (void)dealloc {

}

- (NSMutableArray *)phaseChartData {
    if (_phaseChartData == nil) {
        _phaseChartData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _phaseChartData;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    [[self class] collectMultiphaseChartData:self.phaseChartData inDocManager:docManager];
}

- (NSDictionary *)collectedData {
    if (self.phaseChartData.count > 0) {
        return @{kVersion: @1, kValues: self.phaseChartData};
    } else {
        return nil;
    }
}

@end

@interface CZReportMergedPhaseChartCollector() {
    double totalArea;
    double remainingArea;
    NSUInteger precision;
}

@property (nonatomic, copy) NSString *unitString;
@property (nonatomic, retain, readonly) NSMutableArray *phaseChartData;
@property (nonatomic, retain) NSMutableDictionary *mergedMultiphaseInfo;

@end

@implementation CZReportMergedPhaseChartCollector
@synthesize mergedMultiphaseInfo = _mergedMultiphaseInfo;
@synthesize phaseChartData = _phaseChartData;

+ (void)collectPhaseInfoInDocManager:(CZDocManager *)docManager info:(NSMutableDictionary *)info {
    if (docManager.multiphase.isParticlesMode) {
        return;
    }
    
    [docManager analyzer2dResult];  // CAUTION: lazy load analyzer result data and multiphase area
    
    CZElementUnitStyle unitStyle = [[CZDefaultSettings sharedInstance] unitStyle];
    CGFloat scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    if (scaling <= 0) {
        scaling = 1;
    }
    
    uint32_t phaseID = 0;
    for (CZPhase *phase in docManager.multiphase) {
        NSString *key = phase.name;
        double phaseArea = phase.area * scaling * scaling;
        
        CZPhaseInfoItem *phaseInfo = [info objectForKey:key];
        if (phaseInfo == nil) {
            phaseInfo = [[CZPhaseInfoItem alloc] init];
            [info setObject:phaseInfo forKey:key];
        }
        
        [phaseInfo.phaseAreas addObject:@(phaseArea)];
        
        uint32_t regionCollectionID = [CZDocManager regionsIDFromPhaseID:phaseID];
        
        CZRegions2DItem *regionsItem = [docManager.analyzer2dResult.regions2dItemCollection objectForKey:@(regionCollectionID)];
        phaseInfo.regionsCount = phaseInfo.regionsCount + [regionsItem count];
        
        // collect phase color
        if (phaseInfo.color == nil) {
            phaseInfo.color = phase.color;
        }
        
        phaseID++;
    }
    
    if (docManager.multiphase.isRemainingEnabled) {
        NSString *key = L(@"REPORT_MULTIPHASE_REMAINING");
        double phaseArea = docManager.multiphase.remainingArea * scaling * scaling;
        
        CZPhaseInfoItem *phaseInfo = [info objectForKey:key];
        if (phaseInfo == nil) {
            phaseInfo = [[CZPhaseInfoItem alloc] init];
            [info setObject:phaseInfo forKey:key];
        }
        
        [phaseInfo.phaseAreas addObject:@(phaseArea)];
        
        phaseID = (uint32_t)[docManager.multiphase phaseCount];
        NSUInteger regionCollectionID = [CZDocManager regionsIDFromPhaseID:phaseID];
        
        CZRegions2DItem *regionsItem = [docManager.analyzer2dResult.regions2dItemCollection objectForKey:@(regionCollectionID)];
        phaseInfo.regionsCount = phaseInfo.regionsCount + [regionsItem count];
        
        // collect phase color
        if (phaseInfo.color == nil) {
            phaseInfo.color = docManager.multiphase.remainingColor;
        }
    }
}

- (void)dealloc {
}

- (NSMutableDictionary *)mergedMultiphaseInfo {
    if (_mergedMultiphaseInfo == nil) {
        _mergedMultiphaseInfo = [[NSMutableDictionary alloc] initWithCapacity:16];
    }
    
    return _mergedMultiphaseInfo;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    CZElementUnitStyle unitStyle = [[CZDefaultSettings sharedInstance] unitStyle];
    
    if (docID == 0) {
        precision = docManager.elementLayer.precision;
        self.unitString = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
    } else {
        if (precision > docManager.elementLayer.precision) {
            precision = docManager.elementLayer.precision;
        }
    }
    
    [[self class] collectPhaseInfoInDocManager:docManager
                                             info:self.mergedMultiphaseInfo];
    
    CZMultiphase *multiphase = docManager.multiphase;
    if (multiphase.phaseCount && !multiphase.isParticlesMode) {
        CGFloat scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
        if (scaling <= 0) {
            scaling = 1;
        }
        
        totalArea += multiphase.imagePixelCount * scaling * scaling;
        remainingArea += multiphase.remainingArea * scaling * scaling;
    }
}

- (NSMutableArray *)phaseChartData {
    if (_phaseChartData == nil) {
        _phaseChartData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _phaseChartData;
}

- (NSDictionary *)collectedData {
    [self finishCollection];
    
    if (self.phaseChartData.count > 0) {
        return @{kVersion: @1, kValues: self.phaseChartData};
    } else {
        return nil;
    }
}

- (void)finishCollection {
    NSMutableArray *chartDataGroup = [NSMutableArray array];
    
    for (NSString *key in self.mergedMultiphaseInfo.allKeys) {
        CZPhaseInfoItem *phaseInfo = [self.mergedMultiphaseInfo objectForKey:key];
        if ([phaseInfo.phaseAreas count] == 0) {
            continue;
        }
        
        double totalAreaOfOnePhase = 0;
        for (NSNumber *phaseArea in phaseInfo.phaseAreas) {
            totalAreaOfOnePhase += [phaseArea doubleValue];
        }
        
        float percent = 0.0f;
        if (self.isRemainingEnabled) {
            percent = totalAreaOfOnePhase / totalArea;
        } else {
            percent = totalAreaOfOnePhase / (totalArea - remainingArea);
        }
        
        CGFloat r, g, b, a;
        UIColor *phaseColor = phaseInfo.color;
        if (phaseColor == nil) {
            phaseColor = [UIColor redColor];
        }
        [phaseColor getRed:&r green:&g blue:&b alpha:&a];
        id colorValue = @{kRedColor: @(r), kGreenColor: @(g), kBlueColor: @(b), kAlphaColor: @(a)};
        
        NSMutableArray *additionalValues = [NSMutableArray array];
        
        // phase area and region count row
        NSString *phaseArea = L(@"REPORT_PHASE_AREA");
        phaseArea = [phaseArea stringByAppendingString:[CZCommonUtils localizedStringFromFloat:totalAreaOfOnePhase precision:precision]];
        phaseArea = [phaseArea stringByAppendingString:self.unitString];
        [additionalValues addObject:phaseArea];
        
        if (phaseInfo.regionsCount) {
            NSString *phaseRegions = L(@"REPORT_PHASE_REGIONS");
            phaseRegions = [phaseRegions stringByAppendingString:[CZCommonUtils localizedStringFromNumber:@(phaseInfo.regionsCount) precision:0]];
            [additionalValues addObject:phaseRegions];
        }
        
        if (phaseInfo.phaseAreas.count > 1) {
            double areaMeanValue = totalAreaOfOnePhase / phaseInfo.phaseAreas.count;
            
            // mean area row
            NSString *meanArea = L(@"REPORT_MEAN_AREA_PER_IMAGE");
            meanArea = [meanArea stringByAppendingString:[CZCommonUtils localizedStringFromFloat:areaMeanValue precision:precision]];
            meanArea = [meanArea stringByAppendingString:@" "];
            meanArea = [meanArea stringByAppendingString:self.unitString];
            
            [additionalValues addObject:meanArea];
        }
        
        NSDictionary *item = @{kComponentTitle: key,
                               kComponentValue: [NSNumber numberWithFloat:percent * 100.0f],
                               kComponentAdditionalValues: additionalValues,
                               kColor: colorValue};
        [chartDataGroup addObject:item];
    }
    
    [self.phaseChartData addObject:@{kUnit: @"%", kValue: chartDataGroup}];
    
    self.mergedMultiphaseInfo = nil;  // release memory
}

@end
