//
//  CZReportMeasurementTableCollector.m
//  Matscope
//
//  Created by Ralph Jin on 7/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZReportMeasurementTableCollector.h"

#import <ReportFramework/CZGlobal.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

@interface CZReportMeasurementTableCollector ()
@property (nonatomic, retain, readonly) NSMutableArray *measurementTableData;
@end

@implementation CZReportMeasurementTableCollector
@synthesize measurementTableData = _measurementTableData;

+ (void)collectMeasurementTableData:(NSMutableArray *)measurementTable
                          inElement:(CZElement *)element
                          withDocID:(NSUInteger)docID {
    if (!element.hasMeasurement || element.isMeasurementHidden) {
        return;
    }
    
    NSString *elementType = [element type];
    NSUInteger precision = [element precision];
    
    CZColor backgroundColor;
    BOOL disableMeasrementColorCoding = [[CZDefaultSettings sharedInstance] disableMeasurementColorCoding];
    if (disableMeasrementColorCoding) {
        backgroundColor = kCZColorWhite;
    } else {
        backgroundColor = [element measurementIDColor];
    }
    NSUInteger rgba = (backgroundColor.r << 24) + (backgroundColor.g << 16) + (backgroundColor.b << 8) + backgroundColor.a;
    
    NSString *combineId = nil;
    if (element.parentGroup == nil) {
        combineId = [NSString stringWithFormat:@"%lu-N-%@", (unsigned long)docID + 1, element.measurementID];
    } else {
        combineId = [NSString stringWithFormat:@"%lu-%lu-%@", (unsigned long)docID + 1, (unsigned long)[element.parentGroup index] + 1, element.measurementID];
    }
    
    for (CZFeature *feature in element.features) {
        @autoreleasepool {
            if (feature.value < 0) {
                continue;
            }
            
            NSDictionary *item = @{@"Id" : combineId,
                                   @"Id.backgroundColor": [NSNumber numberWithUnsignedInteger:rgba],
                                   @"Tool" : L(elementType),
                                   @"Unit" : L(feature.unit),
                                   @"Feature": L(feature.type),
                                   @"Value" : [NSNumber numberWithFloat:feature.value],
                                   @"Value.precision" : [NSNumber numberWithUnsignedInteger:precision]};
            [measurementTable addObject:item];
        }
    }
}

+ (void)collectMeasurementTableData:(NSMutableArray *)measurementTable
                       inDocManager:(CZDocManager *)docManager
                          withDocID:(NSUInteger)docID
                            byGroup:(BOOL)byGroup {
    if (byGroup) {
        // collect no group
        NSMutableArray *tableDataGroup = [[NSMutableArray alloc] init];
        for (CZElement *element in docManager.elementLayer) {
            if (element.parentGroup == nil) {
                [self collectMeasurementTableData:tableDataGroup
                                        inElement:element
                                        withDocID:docID];
            }
        }
        if (tableDataGroup.count) {
            [measurementTable addObject:@{kValue: tableDataGroup}];
        }
        
        // collect each group
        for (CZElementGroup *group in [docManager.elementLayer groupEnumerator]) {
            tableDataGroup = [[NSMutableArray alloc] init];
            for (CZElement *element in group) {
                [self collectMeasurementTableData:tableDataGroup
                                        inElement:element
                                        withDocID:docID];
            }
            if (tableDataGroup.count) {
                [measurementTable addObject:@{kValue: tableDataGroup}];
            }
        }
    } else {  // collect all
        NSMutableArray *tableDataGroup = [[NSMutableArray alloc] init];
        for (CZElement *element in docManager.elementLayer) {
            [self collectMeasurementTableData:tableDataGroup
                                    inElement:element
                                    withDocID:docID];
        }
        if (tableDataGroup.count) {
            if (measurementTable.count == 0) {
                [measurementTable addObject:@{kValue: tableDataGroup}];
            } else {
                NSMutableArray *array = [measurementTable[0] objectForKey:kValue];
                [array addObjectsFromArray:tableDataGroup];
            }
        }
    }
}

- (void)dealloc {
    
}

- (NSMutableArray *)measurementTableData {
    if (_measurementTableData == nil) {
        _measurementTableData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _measurementTableData;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    [[self class] collectMeasurementTableData:self.measurementTableData
                                 inDocManager:docManager
                                    withDocID:docID
                                      byGroup:self.perGroupMeasurements];
}

- (NSDictionary *)collectedData {
    if (self.measurementTableData.count > 0) {
        return @{kVersion: @1, kValues: self.measurementTableData};
    } else {
        return nil;
    }
}

@end

@interface CZReportMeasurementChartCollector ()
@property (nonatomic, strong, readonly) NSMutableArray *measurementChartData;
@end

@implementation CZReportMeasurementChartCollector
@synthesize measurementChartData = _measurementChartData;

/* @return return nil, if element has no valid featrue;
 * return @"mix" if units are mixed
 * return actrual unit if units are same
 */
+ (NSString *)collectMeasurementChartData:(NSMutableArray *)chartDataGroup inElement:(CZElement *)element {
    if (!element.hasMeasurement || element.isMeasurementHidden) {
        return nil;
    }
    
    NSString *unit = nil;
    
    for (CZFeature *feature in element.features) {
        @autoreleasepool {
            if (feature.value < 0) {
                continue;
            }
            
            if (unit) {
                if (![unit isEqualToString:feature.unit]) {
                    unit = @"mix";
                }
            } else {
                unit = feature.unit;
            }
            
            NSString *title = [NSString stringWithFormat:@"%@[%@]", L(feature.type), L(feature.unit)];
            NSDictionary *item = @{kComponentTitle: title,
                                   kComponentValue: [NSNumber numberWithFloat:feature.value]};
            [chartDataGroup addObject:item];
        }
    }
    
    return unit;
}

+ (void)collectMeasurementChartData:(NSMutableArray *)chartData inElements:(id<NSFastEnumeration>)enumerator {
    NSMutableArray *chartDataGroup = [[NSMutableArray alloc] init];
    NSString *groupUnit = nil;
    for (CZElement *element in enumerator) {
        NSString *unit = [self collectMeasurementChartData:chartDataGroup inElement:element];
        if (groupUnit) {
            if (![groupUnit isEqualToString:unit]) {
                groupUnit = @"mix";
            }
        } else {
            groupUnit = unit;
        }
    }
    if (chartDataGroup.count) {
        if (groupUnit && ![groupUnit isEqualToString:@"mix"]) {
            [chartData addObject:@{kValue: chartDataGroup, kUnit: L(groupUnit)}];
        } else {
            [chartData addObject:@{kValue: chartDataGroup}];
        }
    }
}

+ (void)collectMeasurementChartData:(NSMutableArray *)chartData
                       inDocManager:(CZDocManager *)docManager
                            byGroup:(BOOL)byGroup {
    if (byGroup) {
        // collect no group
        NSMutableArray *gourpZero = [[NSMutableArray alloc] init];
        for (CZElement *element in docManager.elementLayer) {
            if (element.parentGroup == nil) {
                [gourpZero addObject:element];
            }
        }
        
        [self collectMeasurementChartData:chartData inElements:gourpZero];
        
        // collect each group
        for (CZElementGroup *group in [docManager.elementLayer groupEnumerator]) {
            [self collectMeasurementChartData:chartData inElements:group];
        }
    } else {
        // collect all
        [self collectMeasurementChartData:chartData inElements:docManager.elementLayer];
    }
}

- (void)dealloc {
  
}

- (NSMutableArray *)measurementChartData {
    if (_measurementChartData == nil) {
        _measurementChartData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _measurementChartData;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    [[self class] collectMeasurementChartData:self.measurementChartData
                                 inDocManager:docManager
                                      byGroup:self.perGroupMeasurements];
}

- (NSDictionary *)collectedData {
    if (self.measurementChartData.count > 0) {
        return @{kVersion: @1, kValues: self.measurementChartData};
    } else {
        return nil;
    }
}

@end
