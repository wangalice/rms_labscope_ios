//
//  CZReportEditTextView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportEditTextView.h"

@implementation CZReportEditTextView

@synthesize textField = _textField;

- (instancetype)init {
    if (self = [super init]) {
        [self.contentBackgroundView addSubview:self.textField];

        _textField.translatesAutoresizingMaskIntoConstraints = NO;
        [_textField.leftAnchor constraintEqualToAnchor:self.contentBackgroundView.leftAnchor].active = YES;
        [_textField.bottomAnchor constraintEqualToAnchor:self.contentBackgroundView.bottomAnchor].active = YES;
        [_textField.widthAnchor constraintEqualToConstant:400.0].active = YES;
        [_textField.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    }
    return self;
}

#pragma mark - Getter

- (CZTextField *)textField {
    if (_textField == nil) {
        _textField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightIcon:nil];
        _textField.showClearButtonWhileEditing = YES;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        _textField.adjustsFontSizeToFitWidth = YES;
        _textField.font = [UIFont cz_body1];
    }
    return _textField;
}

@end
