//
//  CZReportEditTextView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportBaseEditView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZReportEditTextView : CZReportBaseEditView

@property (nonatomic, readonly, strong) CZTextField *textField;

@end

NS_ASSUME_NONNULL_END
