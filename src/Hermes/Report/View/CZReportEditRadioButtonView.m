//
//  CZReportEditRadioButtonView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportEditRadioButtonView.h"
#import "CZButtonGroup.h"

static const CGFloat kImageOptionSize = 64.0f;
static const CGFloat kOptionItemSize = 32.0f;
static const CGFloat kDefaultControlsMargin = 6.0f;
static const CGFloat kDefaultOptionItemPadding = 42.0f;

@interface CZReportEditRadioButtonView ()

@property (nonatomic, strong) NSMutableArray <CZRadioButton *> *imageOptions;
@property (nonatomic, strong) NSMutableArray <UIImageView *> *imageViews;

@end

@implementation CZReportEditRadioButtonView

- (instancetype)init {
    if (self = [super init]) {
        [_imageOptions removeAllObjects];
        [_imageViews removeAllObjects];

        if (_imageOptions == nil) {
            _imageOptions = [NSMutableArray array];
        }
        
        if (_imageViews == nil) {
            _imageViews = [NSMutableArray array];
        }
        
        NSArray *imageOptionsIcons = @[[UIImage imageNamed:A(@"report-image-option-one-image")],
                                       [UIImage imageNamed:A(@"report-image-option-one-page")],
                                       [UIImage imageNamed:A(@"report-image-option-pages")]];

        
        for (UIImage *image in imageOptionsIcons) {
            NSUInteger index = [imageOptionsIcons indexOfObject:image];
            CZRadioButton *optionItem = [[CZRadioButton alloc] init];
            optionItem.selected = (index == 0);
            [optionItem addTarget:self action:@selector(imageOptionsAction:) forControlEvents:UIControlEventTouchUpInside];
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.image = image;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.contentBackgroundView addSubview:optionItem];
            [self.contentBackgroundView addSubview:imageView];
            [self.imageOptions addObject:optionItem];
            [self.imageViews addObject:imageView];
        }
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (CZRadioButton *optionItem in self.imageOptions) {
        NSUInteger index = [self.imageOptions indexOfObject:optionItem];
        optionItem.frame = [self radioButtonFrameWithIndex:index];
        UIImageView *imageView = [self.imageViews objectAtIndex:index];
        imageView.frame = CGRectMake(CGRectGetMaxX(optionItem.frame) + kDefaultControlsMargin, 0, kImageOptionSize, kImageOptionSize);
        CGPoint optionItemCenter = optionItem.center;
        CGPoint imageViewCenter = imageView.center;
        optionItemCenter.y = self.contentBackgroundView.center.y + 8.0;
        imageViewCenter.y = self.contentBackgroundView.center.y + 8.0;
        optionItem.center = optionItemCenter;
        imageView.center =  imageViewCenter;
    }
}

- (CGRect)radioButtonFrameWithIndex:(NSUInteger)index {
    CGFloat optionItemX = (kDefaultOptionItemPadding + kOptionItemSize + kDefaultControlsMargin + kImageOptionSize) * index;
    return CGRectMake(optionItemX, 0, kOptionItemSize, kOptionItemSize);
}

- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];

    for (CZRadioButton *optionItem in self.imageOptions) {
        optionItem.hidden = hidden;
    }

    for (UIImageView *imageView in self.imageViews) {
        imageView.hidden = hidden;
    }
    
    self.titleLabel.hidden = hidden;
}

#pragma mark - Action

- (void)imageOptionsAction:(CZRadioButton *)sender {
    for (CZRadioButton *optionItem in self.imageOptions) {
        optionItem.selected = (optionItem == sender);
    }

    NSInteger index = [self.imageOptions indexOfObject:sender];
    if ([self.delegate respondsToSelector:@selector(reportEditRadioButtonView:didSelectIndex:)]) {
        [self.delegate reportEditRadioButtonView:self didSelectIndex:index];
    }
}

@end
