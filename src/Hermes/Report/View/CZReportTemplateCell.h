//
//  CZReportTemplateCell.h
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZReportTemplateCell : UICollectionViewCell

@property (nonatomic, readonly, strong) UIImageView *imageView;
@property (nonatomic, readonly, strong) UILabel *label;

- (void)setLabelText:(NSString *)text;

@end
