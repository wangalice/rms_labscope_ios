//
//  CZReportMultipleImagesCell.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportMultipleImagesCell.h"

@interface CZReportMultipleImagesCell ()

@property (nonatomic, readonly, strong) UIView *deleteBackgroundView;
@property (nonatomic, readonly, strong) UIButton *deleteButton;

@end

@implementation CZReportMultipleImagesCell

@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize deleteButton = _deleteButton;
@synthesize deleteBackgroundView = _deleteBackgroundView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self.contentView addSubview:self.thumbnailImageView];
        [self.contentView addSubview:self.deleteBackgroundView];
        [self.contentView addSubview:self.deleteButton];
        [self.contentView bringSubviewToFront:self.deleteButton];
        
        self.deleteBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.thumbnailImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.deleteBackgroundView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
        [self.deleteBackgroundView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
        [self.deleteBackgroundView.widthAnchor constraintEqualToAnchor:self.contentView.widthAnchor].active = YES;
        [self.deleteBackgroundView.heightAnchor constraintEqualToConstant:32.0f].active = YES;

        [self.thumbnailImageView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
        [self.thumbnailImageView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
        [self.thumbnailImageView.widthAnchor constraintEqualToAnchor:self.contentView.widthAnchor].active = YES;
        [self.thumbnailImageView.heightAnchor constraintEqualToAnchor:self.contentView.heightAnchor].active = YES;

        [self.deleteButton.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = YES;
        [self.deleteButton.centerYAnchor constraintEqualToAnchor:self.deleteBackgroundView.centerYAnchor].active = YES;
        [self.deleteButton.widthAnchor constraintEqualToConstant:32.0f].active = YES;
        [self.deleteButton.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    }
    return self;
}

#pragma mark - Public

- (void)setShouldDeleteButtonHidden:(BOOL)shouldDeleteButtonHidden {
    _shouldDeleteButtonHidden = shouldDeleteButtonHidden;
    self.deleteButton.hidden = shouldDeleteButtonHidden;
    self.deleteBackgroundView.hidden = shouldDeleteButtonHidden;
}

#pragma mark - Action

- (void)deleteButtonTapped:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(reportMultipleImagesCellWillDelete:)]) {
        [self.delegate reportMultipleImagesCellWillDelete:self];
    }
}

#pragma mark - Getter

- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
        [_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}

- (UIImageView *)thumbnailImageView {
    if (_thumbnailImageView == nil) {
        _thumbnailImageView = [[UIImageView alloc] init];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailImageView.clipsToBounds = YES;
    }
    return _thumbnailImageView;
}

- (UIView *)deleteBackgroundView {
    if (_deleteBackgroundView == nil) {
        _deleteBackgroundView = [[UIView alloc] init];
        _deleteBackgroundView.backgroundColor = [[UIColor cz_gs110] colorWithAlphaComponent:0.6];
        _deleteBackgroundView.userInteractionEnabled = YES;
    }
    return _deleteBackgroundView;
}

@end
