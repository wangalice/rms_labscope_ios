//
//  CZReportTemplateCell.m
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportTemplateCell.h"
#import "CZUIDefinition.h"

static const CGFloat kReportTemplateCellMargin = 12.0f;

@interface CZReportTemplateCell ()

@property (nonatomic, assign) CGFloat originalTextHeight;

@end

@implementation CZReportTemplateCell

@synthesize imageView = _imageView;
@synthesize label = _label;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.label];
        [self.contentView addSubview:self.imageView];
        
        CGRect labelFrame = [_label textRectForBounds:CGRectMake(kReportTemplateCellMargin,
                                                                 kReportTemplateCellMargin * 2,
                                                                 frame.size.width - kReportTemplateCellMargin * 2,
                                                                 frame.size.height)
                               limitedToNumberOfLines:0];
        _originalTextHeight = labelFrame.size.height;
        
        CGFloat imageHeight = frame.size.height - _originalTextHeight - kReportTemplateCellMargin * 2;
        
        _imageView.frame = CGRectMake(kReportTemplateCellMargin,
                                      kReportTemplateCellMargin,
                                      frame.size.width - kReportTemplateCellMargin * 2,
                                      imageHeight);
        
        _label.frame = CGRectMake(kReportTemplateCellMargin,
                                  CGRectGetMaxY(_imageView.frame) + kReportTemplateCellMargin,
                                  _imageView.frame.size.width,
                                  _originalTextHeight);
    }
    return self;
}

- (void)setLabelText:(NSString *)text {
    NSString *labelText = text;
    _label.text = labelText;
    
    do {
        CGRect actualFrame = [_label textRectForBounds:CGRectMake(0,
                                                                  0,
                                                                  self.frame.size.width - kReportTemplateCellMargin * 2,
                                                                  CGFLOAT_MAX)
                                limitedToNumberOfLines:0];
        if (actualFrame.size.height <= _originalTextHeight) {
            CGRect frame = _label.frame;
            frame.size.height = actualFrame.size.height;
            _label.frame = frame;
            break;
        }
        
        labelText = [labelText substringToIndex:labelText.length - 1];
        _label.text = [labelText stringByAppendingString:@"..."];
    } while (labelText.length > 0);
}

#pragma mark - Getter
- (UILabel *)label {
    if (_label == nil) {
        _label = [[UILabel alloc] init];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.numberOfLines = 2;
        _label.lineBreakMode = NSLineBreakByWordWrapping;
        _label.textColor = [UIColor cz_gs50];
        _label.font = [UIFont cz_subtitle1];
        _label.backgroundColor = [UIColor clearColor];
        _label.text = @".\n."; // Simulate 2 lines
    }
    return _label;
}

- (UIImageView *)imageView {
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
    }
    return _imageView;
}

@end
