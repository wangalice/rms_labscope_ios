//
//  CZReportBaseEditView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/17.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportBaseEditView.h"

static const CGFloat kContentDefaultMargin = 16;

@implementation CZReportBaseEditView

@synthesize titleLabel = _titleLabel;
@synthesize contentBackgroundView = _contentBackgroundView;

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLabel];
        [self addSubview:self.contentBackgroundView];
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.contentBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.titleLabel.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:kContentDefaultMargin].active = YES;
        [self.titleLabel.topAnchor constraintEqualToAnchor:self.topAnchor constant:kContentDefaultMargin].active = YES;
        [self.titleLabel.heightAnchor constraintEqualToConstant:20.0f].active = YES;
        [self.titleLabel.widthAnchor constraintGreaterThanOrEqualToConstant:kTitlePlaceHolderWidth].active = YES;
        
        [self.contentBackgroundView.leftAnchor constraintEqualToAnchor:self.titleLabel.rightAnchor constant:kContentDefaultMargin].active = YES;
        [self.contentBackgroundView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
        [self.contentBackgroundView.heightAnchor constraintEqualToAnchor:self.heightAnchor].active = YES;
        [self.contentBackgroundView.widthAnchor constraintEqualToAnchor:self.widthAnchor constant:-kContentDefaultMargin * 2].active = YES;
    }
    
    return self;
}

#pragma mark - Getter


- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont cz_label1];
        _titleLabel.textColor = [UIColor cz_gs80];
    }
    
    return _titleLabel;
}

- (UIView *)contentBackgroundView {
    if (_contentBackgroundView == nil) {
        _contentBackgroundView = [[UIView alloc] init];
    }
    
    return _contentBackgroundView;
}

@end
