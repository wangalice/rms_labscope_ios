//
//  CZReportEditImageView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportEditImageView.h"
#import "CZImagePickerController.h"
#import "CZPopoverController.h"

static const CGFloat kImageThumbnailSize = 96;

@interface CZReportEditImageView () <
CZImagePickerControllerDelegate
>

@property (nonatomic, strong) CZImagePickerController *imagePickerController;

@end

@implementation CZReportEditImageView

@synthesize deleteBackgroundView = _deleteBackgroundView;
@synthesize reportImageView = _reportImageView;
@synthesize deleteButton = _deleteButton;
@synthesize addImageViewButton = _addImageViewButton;

- (instancetype)init {
    if (self = [super init]) {
        self.reportImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.deleteBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        self.addImageViewButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.reportImageView addSubview:self.deleteBackgroundView];
        [self.reportImageView addSubview:self.deleteButton];
        [self.reportImageView bringSubviewToFront:self.deleteButton];
        [self.contentBackgroundView addSubview:self.reportImageView];
        [self.contentBackgroundView addSubview:self.addImageViewButton];
        
        [self.addImageViewButton.leftAnchor constraintEqualToAnchor:self.contentBackgroundView.leftAnchor].active = YES;
        [self.addImageViewButton.bottomAnchor constraintEqualToAnchor:self.contentBackgroundView.bottomAnchor].active = YES;
        [self.addImageViewButton.widthAnchor constraintEqualToConstant:128.0f].active = YES;
        [self.addImageViewButton.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    }
    return self;
}

- (void)setShouldDeleteButtonHidden:(BOOL)shouldDeleteButtonHidden {
    _shouldDeleteButtonHidden = shouldDeleteButtonHidden;
    self.deleteButton.hidden = _shouldDeleteButtonHidden;
    self.deleteBackgroundView.hidden = _shouldDeleteButtonHidden;
    self.reportImageView.hidden = _shouldDeleteButtonHidden;

    if (_shouldDeleteButtonHidden) {
        [self.contentBackgroundView removeConstraints:self.reportImageView.constraints];
        [self.contentBackgroundView removeConstraints:self.deleteBackgroundView.constraints];
        [self.contentBackgroundView removeConstraints:self.deleteButton.constraints];
    } else {
        [self.reportImageView.topAnchor constraintEqualToAnchor:self.contentBackgroundView.topAnchor constant:16.0f].active = YES;
        [self.reportImageView.leftAnchor constraintEqualToAnchor:self.contentBackgroundView.leftAnchor].active = YES;
        [self.reportImageView.widthAnchor constraintEqualToConstant:kImageThumbnailSize].active = YES;
        [self.reportImageView.heightAnchor constraintEqualToConstant:kImageThumbnailSize].active = YES;
        
        [self.deleteBackgroundView.topAnchor constraintEqualToAnchor:self.reportImageView.topAnchor].active = YES;
        [self.deleteBackgroundView.leftAnchor constraintEqualToAnchor:self.reportImageView.leftAnchor].active = YES;
        [self.deleteBackgroundView.widthAnchor constraintEqualToConstant:kImageThumbnailSize].active = YES;
        [self.deleteBackgroundView.heightAnchor constraintEqualToConstant:32.0f].active = YES;
        
        [self.deleteButton.rightAnchor constraintEqualToAnchor:self.reportImageView.rightAnchor].active = YES;
        [self.deleteButton.centerYAnchor constraintEqualToAnchor:self.deleteBackgroundView.centerYAnchor].active = YES;
        [self.deleteButton.widthAnchor constraintEqualToConstant:32.0f].active = YES;
        [self.deleteButton.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    }
}

#pragma mark - Action

- (void)deleteButtonAction:(CZButton *)sender {
    [self.addImageViewButton setTitle:L(@"ADD_IMAGE") forState:UIControlStateNormal];

    if ([self.delegate respondsToSelector:@selector(reportEditImageView:didPickingImageFilePath:)]) {
        [self.delegate reportEditImageView:self didPickingImageFilePath:@""];
    }
}

- (void)addImageAction:(CZButton *)sender {
    CZImagePickerController *imagePickerController = [[CZImagePickerController alloc] init];
    imagePickerController.delegate = self;

    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:imagePickerController];
    popover.sourceView = sender;
    popover.sourceRect = CGRectOffset(sender.frame, 8, 0);
    popover.arrowDirection = CZPopoverArrowDirectionLeft;
    [popover presentAnimated:YES completion:nil];
    
    self.imagePickerController = imagePickerController;
}

#pragma mark - CZImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)imagePicker didFinishPickingImageFilePath:(NSString *)filePath {
    [self.addImageViewButton setTitle:L(@"UPDATE_IMAGE") forState:UIControlStateNormal];
    if ([self.delegate respondsToSelector:@selector(reportEditImageView:didPickingImageFilePath:)]) {
        [self.delegate reportEditImageView:self didPickingImageFilePath:filePath];
    }
    
    [self.imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Getter

- (UIImageView *)reportImageView {
    if (_reportImageView == nil) {
        _reportImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kImageThumbnailSize, kImageThumbnailSize)];
        _reportImageView.contentMode = UIViewContentModeScaleAspectFill;
        _reportImageView.userInteractionEnabled = YES;
        _reportImageView.clipsToBounds = YES;
        _reportImageView.backgroundColor = [UIColor clearColor];
    }
    return _reportImageView;
}

- (UIView *)deleteBackgroundView {
    if (_deleteBackgroundView == nil) {
        _deleteBackgroundView = [[UIView alloc] init];
        _deleteBackgroundView.backgroundColor = [[UIColor cz_gs110] colorWithAlphaComponent:0.6];
    }
    return _deleteBackgroundView;
}

- (CZButton *)addImageViewButton {
    if (_addImageViewButton == nil) {
        _addImageViewButton = [CZButton buttonWithType:UIButtonTypeCustom];
        [_addImageViewButton setTitle:L(@"ADD_IMAGE") forState:UIControlStateNormal];
        [_addImageViewButton addTarget:self action:@selector(addImageAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageViewButton;
}

- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton cz_setImageWithIcon:[CZIcon iconNamed:@"delete"]];
        [_deleteButton addTarget:self action:@selector(deleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}
@end
