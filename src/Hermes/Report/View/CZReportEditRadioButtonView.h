//
//  CZReportEditRadioButtonCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportBaseEditView.h"

NS_ASSUME_NONNULL_BEGIN
@class CZButtonGroup;

@protocol CZReportEditRadioButtonViewDelegate <NSObject>

@optional
- (void)reportEditRadioButtonView:(CZReportBaseEditView *)CZReportBaseEditView didSelectIndex:(NSUInteger)index;

@end

@interface CZReportEditRadioButtonView : CZReportBaseEditView

@property (nonatomic, weak) id<CZReportEditRadioButtonViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
