//
//  CZReportBaseEditView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/17.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static const CGFloat kTitlePlaceHolderWidth = 200.0;

@interface CZReportBaseEditView : UIView

@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UIView *contentBackgroundView;

@end

NS_ASSUME_NONNULL_END
