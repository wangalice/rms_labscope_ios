//
//  CZReportEditMultipleImageCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportBaseEditView.h"

NS_ASSUME_NONNULL_BEGIN
@class CZReportEditMultipleImageView;

@class CZReportMultipleImagesViewController;

@protocol CZReportEditMultiImageViewDelegate <NSObject>

- (void)multiImageViewDidSelectAddImageAction:(CZReportEditMultipleImageView *)multiImageView;

@end
@interface CZReportEditMultipleImageView : CZReportBaseEditView

@property (nonatomic, weak) id <CZReportEditMultiImageViewDelegate>delegate;
@property (nonatomic, strong, readonly) CZButton *addImageButton;

@end

NS_ASSUME_NONNULL_END
