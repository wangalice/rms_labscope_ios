//
//  CZReportEditImageView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportBaseEditView.h"

NS_ASSUME_NONNULL_BEGIN

@class CZImagePickerController, CZReportEditImageView;
@protocol CZReportEditImageViewDelegate <NSObject>

@optional
- (void)reportEditImageView:(CZReportEditImageView *)imageEditView didPickingImageFilePath:(NSString *)filePath;

@end

@interface CZReportEditImageView : CZReportBaseEditView <UIPopoverPresentationControllerDelegate>

@property (nonatomic, readonly, strong) UIImageView *reportImageView;
@property (nonatomic, readonly, strong) UIButton *deleteButton;
@property (nonatomic, readonly, strong) UIView *deleteBackgroundView;
@property (nonatomic, readonly, strong) CZButton *addImageViewButton;
@property (nonatomic, readonly, strong) CZImagePickerController *imagePickerController;
@property (nonatomic, weak, nullable) id<CZReportEditImageViewDelegate> delegate;
@property (nonatomic, assign) BOOL shouldDeleteButtonHidden;

@end

NS_ASSUME_NONNULL_END
