//
//  CZReportEditMultipleImageView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportEditMultipleImageView.h"

@implementation CZReportEditMultipleImageView

@synthesize addImageButton = _addImageButton;

- (instancetype)init {
    if (self = [super init]) {
        [self.contentBackgroundView addSubview:self.addImageButton];
        [self.contentBackgroundView bringSubviewToFront:self.addImageButton];
        
        self.addImageButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.addImageButton.leftAnchor constraintEqualToAnchor:self.contentBackgroundView.leftAnchor].active = YES;
        [self.addImageButton.bottomAnchor constraintEqualToAnchor:self.contentBackgroundView.bottomAnchor].active = YES;
        [self.addImageButton.widthAnchor constraintEqualToConstant:128.0].active = YES;
        [self.addImageButton.heightAnchor constraintEqualToConstant:32.0f].active = YES;
    }
    return self;
}

#pragma mark - Action

- (void)addImageAction:(CZButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(multiImageViewDidSelectAddImageAction:)]) {
        [self.delegate multiImageViewDidSelectAddImageAction:self];
    }
}

- (CZButton *)addImageButton {
    if (_addImageButton == nil) {
        _addImageButton = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        [_addImageButton setTitle:L(@"ADD_IMAGE") forState:UIControlStateNormal];
        [_addImageButton addTarget:self action:@selector(addImageAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageButton;
}

@end
