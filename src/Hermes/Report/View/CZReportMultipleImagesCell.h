//
//  CZReportMultipleImagesCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZReportMultipleImagesCell;
@protocol CZReportMultipleImagesCellDelegate <NSObject>

- (void)reportMultipleImagesCellWillDelete:(CZReportMultipleImagesCell *)cell;

@end

@interface CZReportMultipleImagesCell : UICollectionViewCell

@property (nonatomic, readonly, strong) UIImageView *thumbnailImageView;
@property (nonatomic, weak) id <CZReportMultipleImagesCellDelegate> delegate;
@property (nonatomic, assign) BOOL shouldDeleteButtonHidden;

@end
