//
//  CZScalingReportManager.h
//  Matscope
//
//  Created by Ralph Jin on 5/22/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZAbstractReportManager.h"

@class CZMicroscopeModel;

@interface CZScalingReportManager : CZAbstractReportManager

@property (nonatomic, strong, readonly) CZMicroscopeModel *microscopeModel;
@property (nonatomic, assign) NSUInteger cameraSnapshotWidth;

@end
