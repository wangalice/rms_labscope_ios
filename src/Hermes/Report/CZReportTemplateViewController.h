//
//  CZReportTemplateViewController.h
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZDocManager;

@protocol CZReportTemplateViewControllerDataSource <NSObject>

@required
- (CZDocManager *)reportMainDocManager;

@optional
- (nullable NSArray <NSString *> *)reportImagesFilePath;

@end

@interface CZReportTemplateViewController : UICollectionViewController

@property (nonatomic, weak, nullable) id <CZReportTemplateViewControllerDataSource> editDataSource;

@end

NS_ASSUME_NONNULL_END
