//
//  CZReportMeasurementTableCollector.h
//  Matscope
//
//  Created by Ralph Jin on 7/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZReportDataCollector.h"

@interface CZReportMeasurementTableCollector : NSObject<CZReportDataCollector>

@property (nonatomic, assign) BOOL perGroupMeasurements;

@end

@interface CZReportMeasurementChartCollector : NSObject<CZReportDataCollector>

@property (nonatomic, assign) BOOL perGroupMeasurements;

@end
