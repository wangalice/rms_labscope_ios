//
//  CZReportTemplateFilesManager.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/3.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZReportTemplateFilesManager : NSObject

+ (void)copyDefaultTemplateFiles:(NSString *)templateType useLocal:(BOOL)useLocal;

+ (NSString *)documentPath;

+ (nullable UIImage *)loadThumbnailFromFile:(NSString *)filePath minSize:(CGFloat)minSize;

@end

NS_ASSUME_NONNULL_END
