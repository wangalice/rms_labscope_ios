//
//  CZReportField.h
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZReportFieldType) {
    CZReportFieldTypeText,
    CZReportFieldTypeImage,
    CZReportFieldTypeTable,
    CZReportFieldTypeChart
};

@interface CZReportField : NSObject

@property (nonatomic, readonly, strong) NSString *key;
@property (nonatomic, readonly, strong) NSString *fieldDescription;
@property (nonatomic, readonly, assign) BOOL mandatory;
@property (nonatomic, readonly, assign) CZReportFieldType type;

- (instancetype)initWithElementDict:(NSDictionary *)element;
- (instancetype)initWithKey:(NSString *)key type:(CZReportFieldType)type;

@end
