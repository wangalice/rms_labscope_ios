//
//  CZScalingReportManager.m
//  Matscope
//
//  Created by Ralph Jin on 5/22/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZScalingReportManager.h"

#import <ReportFramework/CZGlobal.h>
#import "CZReportField.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZAnnotationKit/CZElement.h>

@implementation CZScalingReportManager

+ (CZElementUnitStyle)scalingReportUnitStyle:(CZElementUnitStyle)unitStyle {
    switch (unitStyle) {
        case CZElementUnitStyleMetricAuto:
        case CZElementUnitStyleMM:
        case CZElementUnitStyleMicrometer:
            unitStyle = CZElementUnitStyleMicrometer;
            break;
        case CZElementUnitStyleImperialAuto:
        case CZElementUnitStyleInch:
        case CZElementUnitStyleMil:
            unitStyle = CZElementUnitStyleMil;
        case CZElementUnitStyleAuto: {
            BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
            if (isMetric) {
                unitStyle = CZElementUnitStyleMicrometer;
            } else {
                unitStyle = CZElementUnitStyleMil;
            }
        }
        default:
            break;
    }
    return unitStyle;
}

- (void)dealloc {
 
}

// override super class
+ (NSString *)translateTemplateKey:(NSString *)key {
    if ([key isEqualToString:@"SCALING_UNIT"]) {
        CZElementUnitStyle style = [CZScalingReportManager scalingReportUnitStyle:[CZElement unitStyle]];
        
        // report template is English, so no localization here.
        if (style == CZElementUnitStyleMil) {
            return @"(mil/pixel)";
        } else {
            return @"(µm/pixel)";
        }
    } else {
        return [super translateTemplateKey:key];
    }
};

- (BOOL)fillReportData:(NSError **)error {
    NSString *unknownString = L(@"FILES_INFO_UNKNOWN");
    
    NSMutableDictionary *reportData = [self.userInputData mutableCopy];
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    
    reportData[@"_date"] = [NSString stringWithFormat:@"%@", dateString];
    
    reportData[@"_microscope"] = [NSString stringWithFormat:@"%@ (%@)",
                                  self.microscopeModel.name,
                                  self.microscopeModel.cameraMACAddress];
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    // Matscope only feature
    reportData[@"_software"] = [NSString stringWithFormat:@"Matscope %@ on iPad %@", version, uuid];
    
    NSString *parentDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cameraMACAddress = self.microscopeModel.cameraMACAddress;
    cameraMACAddress = [cameraMACAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    parentDir = [parentDir stringByAppendingPathComponent:cameraMACAddress];
    parentDir = [parentDir stringByAppendingPathComponent:@"Scaling"];
    
    // fill table
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
#if REPORT_REFACTOR
    
    NSMutableArray *tableContent = [NSMutableArray array];
    NSMutableArray *imageArray = [NSMutableArray array];
    int imageID = 0;
    for (NSUInteger slot = 0; slot < self.microscopeModel.slotCount; ++slot) {
        CZZoomLevel *zoomLevel = [self.microscopeModel zoomLevelAtSlot:slot];
        if (zoomLevel.magnification <= 0) {
            continue;
        }
        
        NSString *scalingString;
        double fovWidth = [self.microscopeModel fovWidthAtSlot:slot];
        BOOL isDefaultScaling = YES;
        if (fovWidth == kNonCalibratedValue) {
            scalingString = L(@"NONE");
        } else {
            isDefaultScaling = [self.microscopeModel isDefaultFOVWidthAtSlot:slot];
            CZElementUnitStyle style = [CZScalingReportManager scalingReportUnitStyle:[CZElement unitStyle]];
            if (!isDefaultScaling) {
                CGFloat scaling = fovWidth / self.cameraSnapshotWidth;
                
                scaling = [CZElement scaling:scaling ofUnitStyle:style prefersBigUnit:NO];
                scalingString = [CZCommonUtils localizedStringFromFloat:scaling precision:5U];
            } else {
                CGFloat scaling = [self.microscopeModel defaultFOVWidthAtSlot:slot] / self.cameraSnapshotWidth;
                
                scaling = [CZElement scaling:scaling ofUnitStyle:style prefersBigUnit:NO];
                scalingString = [CZCommonUtils localizedStringFromFloat:scaling precision:5U];
            }
        }
        
        NSString *displayMagnification = [self.microscopeModel displayMagnificationAtPosition:slot localized:NO];
        
        BOOL imageFileExist = NO;
        if (!isDefaultScaling) { // collect image
            NSString *imageFileName = [NSString stringWithFormat:@"%@_scaling_%lu.jpg",
                                       displayMagnification,
                                       (unsigned long)slot];
            
            NSString *imageFilePath = [parentDir stringByAppendingPathComponent:imageFileName];
            if ([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath]) {
                imageFileExist = YES;
                NSString *imageCaption = [NSString stringWithFormat:@"%d) %@", ++imageID, zoomLevel.name];
                [imageArray addObject:@{kValue: imageFilePath,
                                        kImageCaption: imageCaption}];
            }
        }
        
        NSString *metadataFileName = [NSString stringWithFormat:@"%@_scaling_%lu.xml",
                                      displayMagnification,
                                      (unsigned long)slot];
        NSString *metadataFilePath = [parentDir stringByAppendingPathComponent:metadataFileName];
        
        CZImageProperties *imageProperties = [[CZImageProperties alloc] init];
        BOOL readSuccess = [imageProperties readFromXMLFile:metadataFilePath];
        
        NSMutableDictionary *tableRow = [[NSMutableDictionary alloc] initWithCapacity:4];
        if (imageFileExist) {
            tableRow[@"Id"] = @(imageID);
        }
        tableRow[@"Objective"] = zoomLevel.name;
        tableRow[@"Scaling"] = scalingString;
        if (readSuccess) {
            tableRow[@"Date"] = [formatter stringFromDate:imageProperties.originalDateTime];
            tableRow[@"Operator"] = imageProperties.operatorName.length > 0 ? imageProperties.operatorName : unknownString;
        } else {
            tableRow[@"Date"] = unknownString;
            tableRow[@"Operator"] = unknownString;
        }
        if (isDefaultScaling) {
            tableRow[@"Date"] = unknownString;
            tableRow[@"Operator"] = @"(Factory)";
        }
        [tableContent addObject:[NSDictionary dictionaryWithDictionary:tableRow]];
    }
    
    if (tableContent) {
        for (CZReportField *reportField in self.tableFields) {
            if ([reportField.key isEqualToString:@"table.scaling"]) {
                reportData[@"table.scaling"] = tableContent;
            }
        }
    }
    
    NSDictionary *value = @{kVersion: @1,
                            kValues: imageArray,
                            kImageRenderMode: self.imageRenderMode};
    
    if (value) {
        for (CZReportField *reportField in self.imageFields) {
            if ([reportField.key isEqualToString:kCZReserveReportImageKey]) {
                reportData[kCZReserveReportImageKey] = value;
            }
        }
    }
    
    self.reportData = reportData;
#endif
    return YES;
}

- (NSString *)uniqueReportFilePathOfExtension:(NSString *)extension {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    
    NSString *fileName = [NSString stringWithFormat:@"Scaling_report_%@_%@", self.microscopeModel.name, dateString];
    fileName = [fileName stringByAppendingPathExtension:extension];
#if DCM_REFACTORED
    NSString *filePath = [CZLocalFileList sharedInstance].documentPath;
#else
    NSString *filePath = [NSFileManager defaultManager].cz_documentPath;
#endif
    return [filePath stringByAppendingPathComponent:fileName];
}

@end
