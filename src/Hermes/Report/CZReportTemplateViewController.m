//
//  CZReportTemplateViewController.m
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportTemplateViewController.h"
#import <SBJson/SBJson.h>
#import "CZUIDefinition.h"
#import "CZReportTemplateCell.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZReportTemplateServerViewController.h"
#import "CZReportManager.h"
#import "CZReportPreviewViewController.h"
#import "CZReportEditViewController.h"
#import "CZReportTemplateFilesManager.h"
#import "CZPreviewController.h"
#import <ReportFramework/CZReportEngine.h>

static const NSUInteger kNewestReportTemplateVersion = 1;

static NSString * const kCellID = @"CellID";
static const float kTemplateItemWidth = 240;
static const float kTemplateItemHeight = 240;
static const float kTemplateThumbnailWidth = 200;
static const float kTemplateThumbnailHeight = 200;

@interface CZReportTemplateViewController () <UIWebViewDelegate> {
    NSArray *_templateFilePathArray;
    NSMutableDictionary <NSString*, UIImage *>*_thumbnailDictionary;
    dispatch_queue_t _threadQueue;
    CZReportManager *_reportManager;
}

@property (atomic, assign) BOOL thumbnailLoadingCancelled;
@property (nonatomic, strong) UIWebView *reportTemplateNotificationView;

@end

@implementation CZReportTemplateViewController

- (instancetype)init {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(kTemplateItemWidth, kTemplateItemHeight)];
    
    self = [super initWithCollectionViewLayout:layout];
    
    if (self) {
        _templateFilePathArray = nil;
        _thumbnailDictionary = [[NSMutableDictionary alloc] init];
        _threadQueue = NULL;
    }
    
    return self;
}

- (void)dealloc {
    
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }

    CZBarButtonItem *editButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"EDIT")
                                                                      target:self
                                                                      action:@selector(editButtonAction:)];
    editButtonItem.isAccessibilityElement = YES;
    editButtonItem.accessibilityIdentifier = @"EditButton";
    self.navigationItem.rightBarButtonItem = editButtonItem;
    
    self.navigationItem.title = L(@"REPORT_TEMPLATE_VIEW_TITLE");

    UICollectionView *collectionView = self.collectionView;
    [collectionView registerClass:[CZReportTemplateCell class] forCellWithReuseIdentifier:kCellID];
    collectionView.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs105], [UIColor cz_gs105]);

    [CZReportTemplateFilesManager copyDefaultTemplateFiles:@"czrtj" useLocal:YES];
    
    UIWebView *reportTemplateNotificationView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 654, 1024, 50)];
    reportTemplateNotificationView.delegate = self;
    reportTemplateNotificationView.scrollView.scrollEnabled = NO;
    reportTemplateNotificationView.scrollView.bounces = NO;
    reportTemplateNotificationView.backgroundColor = [UIColor clearColor];
    reportTemplateNotificationView.opaque = NO;
  
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    
    UIImage *exclamationMarkImage = [UIImage imageNamed:A(@"exclamation-icon")];
    NSData *exclamationMarkImageData = UIImagePNGRepresentation(exclamationMarkImage);
    NSString *exclamationMarkImageBase64String = [exclamationMarkImageData base64EncodedStringWithOptions:0];
    
    NSURL *pdfFileURL = [[NSBundle mainBundle] URLForResource:@"web/Report_template_format_documentation.pdf"
                                                withExtension:nil];
    CGFloat r, g, b;
    [kDefaultLabelColor getRed:&r green:&g blue:&b alpha:NULL];
    [htmlString appendFormat:@"<html><head><style type=\"text/css\">body{font-family:HelveticaNeue; font-size:12px; background-color:transparent; color:#%02x%02x%02x; margin-left:15px; margin-right:15px;}</style></head><body>", (int)(255.0 * r), (int)(255.0 * g), (int)(255.0 * b)];
    
    NSString *pdfFileLink = [NSString stringWithFormat:@"<a href=​%@>%@</a>", pdfFileURL, L(@"REPORT_TEMPLATE_FORMAT_FILE_LINK")];
    NSString *hintMessage = [NSString stringWithFormat:L(@"REPORT_TEMPLATE_HINT_MESSAGE"), pdfFileLink];
    
    [htmlString appendFormat:@"<p><img src=\"data:image/png;base64, %@\" width=16 height=16;\"/> &#45 %@</p></body></html>", exclamationMarkImageBase64String, hintMessage];
  
    [reportTemplateNotificationView loadHTMLString:htmlString baseURL:nil];

    [self.view addSubview:reportTemplateNotificationView];
    self.reportTemplateNotificationView = reportTemplateNotificationView;
    self.reportTemplateNotificationView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self reloadReportTemplateFile];
    
    UIImage *defaultThumbnail = [CZReportTemplateViewController defaultThumbnail];
    
    for (NSUInteger i = 0; i < [_templateFilePathArray count]; ++i) {
        id thumbnail = _thumbnailDictionary[_templateFilePathArray[i]];
        if (thumbnail == defaultThumbnail) {
            NSString *fullFileName = [_templateFilePathArray objectAtIndex:i];
            
            if (_threadQueue == NULL) {
                _threadQueue = dispatch_queue_create("com.zeisscn.report.thumbnail.updating", DISPATCH_QUEUE_SERIAL);
            }
            
            dispatch_async(_threadQueue, ^() {
                if (self.thumbnailLoadingCancelled) {
                    return;
                }
                
                [self asyncLoadThumbnail:fullFileName inRow:i];
            });
        }
    }
    
    self.thumbnailLoadingCancelled = NO;
    
    [self.collectionView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.thumbnailLoadingCancelled = YES;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.reportTemplateNotificationView.isHidden) {
        self.collectionView.frame = self.view.bounds;
        self.collectionView.scrollIndicatorInsets = UIEdgeInsetsZero;
    } else {
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
        self.collectionView.scrollIndicatorInsets = self.collectionView.contentInset;
    }
}

#pragma mark - ApplicationDidReceiveMemoryWaring

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    _templateFilePathArray = nil;
    [_thumbnailDictionary removeAllObjects];
    self.thumbnailLoadingCancelled = YES;
}

#pragma mark - Action

- (void)editButtonAction:(id)sender {
    CZReportTemplateServerViewController *serverView = [[CZReportTemplateServerViewController alloc] init];
    [self.navigationController pushViewController:serverView animated:YES];
}

#pragma mark - Private method

- (void)reloadReportTemplateFile {
    NSMutableArray *files = [NSMutableArray array];
    NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:[[NSFileManager defaultManager] cz_documentPath]];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        @autoreleasepool {
            if ([[[file pathExtension] lowercaseString] isEqualToString:@"czrtj"]) {
                NSString *templateFilePath = [[CZReportTemplateFilesManager documentPath] stringByAppendingPathComponent:file];
                NSData *data = [NSData dataWithContentsOfFile:templateFilePath];
                SBJsonParser *json_parser = [[SBJsonParser alloc] init];
                id templateData = [json_parser objectWithData:data];

                if ([templateData isKindOfClass:[NSDictionary class]]) {
                    NSNumber *versionNumber = [templateData valueForKeyPath:@"reportTemplateMetadata.templateFormatVersion"];
                    if ([versionNumber unsignedIntegerValue] <= kNewestReportTemplateVersion) {
                        [files addObject:file];
                        if ([versionNumber unsignedIntegerValue] < kNewestReportTemplateVersion) {
                            self.reportTemplateNotificationView.hidden = NO;
                        }
                    }
                }
            }
        }
    }
    _templateFilePathArray = [files copy];
    
    // create thumbnail array and fill with default thumbnail
    UIImage *defaultThumbnail = [CZReportTemplateViewController defaultThumbnail];
    assert(defaultThumbnail);
    
    if (defaultThumbnail) {
        for (NSUInteger i = 0; i < [_templateFilePathArray count]; ++i) {
            if (_thumbnailDictionary[_templateFilePathArray[i]] == nil) {
                _thumbnailDictionary[_templateFilePathArray[i]] = defaultThumbnail;
            }
        }
    }
}

+ (UIImage *)defaultThumbnail {
    static UIImage *defaultThumbnail = nil;
    if (defaultThumbnail == nil) {
        defaultThumbnail = [[CZIcon iconNamed:@"file-icon-czrtj"] image];
    }
    return defaultThumbnail;
}

- (void)asyncLoadThumbnail:(NSString *)filePath inRow:(NSUInteger) row {
    NSString *templateFilePath = [[CZReportTemplateFilesManager documentPath] stringByAppendingPathComponent:filePath];
    NSDictionary *templateData = [CZReportManager reportTemplateFromFile:templateFilePath];
    NSNumber *versionNumber = [templateData valueForKeyPath:@"reportTemplateMetadata.templateFormatVersion"];
    CZReportEngine *engine = [[CZReportEngine alloc] initEngine:nil reportTemplate:templateData];
    UIImage *image = nil;
    
    NSError *error = nil;
    if ([engine validateReport:&error isReport:NO]) {
        const CGFloat screenScale = [[UIScreen mainScreen] scale];
        const CGFloat kPageHeight = kTemplateThumbnailHeight - 10.0f;  // edge 10 shall bigger than shadow offset and blur
        const CGFloat kPageWidth = floor(kPageHeight * 0.7f);
        const CGSize kShadowOffset = {3.0f, 3.0f};
        const CGFloat kShadowBlur = 5.0f;
        
        NSArray *imageArray = [engine generateThumbnails:NSMakeRange(0, 2)
                                           thumbnailSize:CGSizeMake(kPageWidth * screenScale, kPageHeight * screenScale)];
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(kTemplateThumbnailWidth, kTemplateThumbnailHeight), NO, 0.0);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
        
        UIImage *exclamationMark = [UIImage imageNamed:A(@"exclamation-icon")];
        CGSize exclamationMarkSize = exclamationMark.size;
        CGRect exclamationMarkRect = CGRectZero;
        
        if (imageArray.count >= 2) {  // draw 2 pages thumbnail, second page drawn firstly.
            UIImage *thumbnail = imageArray[1];
            CGSize thumbnailSize = thumbnail.size;
            thumbnailSize.width /= screenScale;
            thumbnailSize.height /= screenScale;
            
            CGRect rect2 = CGRectMake(kTemplateThumbnailWidth - thumbnailSize.width - kShadowOffset.width - kShadowBlur,
                                      kTemplateThumbnailHeight - thumbnailSize.height - kShadowOffset.height - kShadowBlur,
                                      thumbnailSize.width, thumbnailSize.height);
            
            CGContextSetShadowWithColor(context, kShadowOffset, kShadowBlur, [[[UIColor blackColor] colorWithAlphaComponent:0.7] CGColor]);
            CGContextFillRect(context, rect2);
            CGContextSetShadowWithColor(context, CGSizeZero, 0.0, NULL);
            [thumbnail drawInRect:rect2];
            
            thumbnail = imageArray[0];
            CGRect rect1 = CGRectMake(0, 0, thumbnailSize.width, thumbnailSize.height);
            CGContextSetShadowWithColor(context, kShadowOffset, kShadowBlur, [[[UIColor blackColor] colorWithAlphaComponent:0.7] CGColor]);
            CGContextFillRect(context, rect1);
            CGContextSetShadowWithColor(context, CGSizeZero, 0.0, NULL);
            [thumbnail drawInRect:rect1];
            
            exclamationMarkRect = CGRectMake(CGRectGetMaxX(rect2) - exclamationMarkSize.width / 2, CGRectGetMaxY(rect2) - exclamationMarkSize.height / 2, exclamationMarkSize.width / 2, exclamationMarkSize.height / 2);
        } else if (imageArray.count == 1) {  // draw 1 page thumbnail
            UIImage *thumbnail = imageArray[0];
            CGSize thumbnailSize = thumbnail.size;
            thumbnailSize.width /= screenScale;
            thumbnailSize.height /= screenScale;
            
            CGRect centerRect = CGRectMake((kTemplateThumbnailWidth - thumbnailSize.width) / 2.0f,
                                           (kTemplateThumbnailHeight - thumbnailSize.height) / 2.0f,
                                           thumbnailSize.width, thumbnailSize.height);
            
            CGContextSetShadowWithColor(context, kShadowOffset, kShadowBlur, [[[UIColor blackColor] colorWithAlphaComponent:0.7] CGColor]);
            CGContextFillRect(context, centerRect);
            CGContextSetShadowWithColor(context, CGSizeZero, 0.0, NULL);
            [thumbnail drawInRect:centerRect];
            
            exclamationMarkRect = CGRectMake(CGRectGetMaxX(centerRect) - exclamationMarkSize.width / 2, CGRectGetMaxY(centerRect) - exclamationMarkSize.height / 2, exclamationMarkSize.width / 2, exclamationMarkSize.height / 2);
        }
        
        if ([versionNumber unsignedIntegerValue] < kNewestReportTemplateVersion) {
            [exclamationMark drawInRect:exclamationMarkRect];
        }
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    if (image) {
        NSString *fullFileName = [_templateFilePathArray objectAtIndex:row];
        dispatch_async(dispatch_get_main_queue(), ^ {
            if (fullFileName) {
                _thumbnailDictionary[fullFileName] = image;
                [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]]];
            }
        });
    }
}

- (CZDocManager *)mainDocManager {
    if (self.editDataSource && [self.editDataSource respondsToSelector:@selector(reportMainDocManager)]) {
        return [self.editDataSource reportMainDocManager];
    }
    return nil;
}

- (NSArray <NSString *> *)reportImagesFilePath {
    if (self.editDataSource && [self.editDataSource respondsToSelector:@selector(reportImagesFilePath)]) {
        return [self.editDataSource reportImagesFilePath];
    }
    return nil;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        NSURL *pdfFileURL = [[NSBundle mainBundle] URLForResource:@"web/Report_template_format_documentation.pdf"
                                                    withExtension:nil];
        
        NSString *filePath = [pdfFileURL absoluteString];
        NSString *fileName = [filePath lastPathComponent];
        NSString *extension = [filePath pathExtension];
        if ([extension isEqualToString:@"pdf"]) {
            CZPreviewController *previewController = [[CZPreviewController alloc] init];
            previewController.previewFileURL = pdfFileURL;
            previewController.navigationItem.title = [NSString stringWithFormat:@"%@",fileName];
        }
        return NO;
    }
    return YES;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_templateFilePathArray count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZReportTemplateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];

    NSUInteger row = indexPath.row;
    NSString *fullFileName = [_templateFilePathArray objectAtIndex:row];
    NSString *fileName = [fullFileName stringByDeletingPathExtension];

    [cell setLabelText:fileName];
    cell.imageView.image = _thumbnailDictionary[fullFileName];

    if (_thumbnailDictionary[fullFileName] == [CZReportTemplateViewController defaultThumbnail]) {
        cell.imageView.contentMode = UIViewContentModeCenter;
    } else {
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = indexPath.row;
    NSString *templateFilePath = row < [_templateFilePathArray count] ? _templateFilePathArray[row] : nil;
    if (templateFilePath) {
        NSString *documentPath = [CZReportTemplateFilesManager documentPath];
        if (_reportManager == nil) {
            _reportManager = [[CZReportManager alloc] initWithDocManager:[self mainDocManager]];
        }
        _reportManager.templateFilePath = [documentPath stringByAppendingPathComponent:templateFilePath];
        _reportManager.additionalFiles = [self reportImagesFilePath];
        [_reportManager loadCachedUserInputData];

        CZReportEditViewController *controller = [[CZReportEditViewController alloc] init];
        controller.reportManager = _reportManager;

        [self.navigationController pushViewController:controller animated:YES];

        self.thumbnailLoadingCancelled = YES;
    }
}

@end
