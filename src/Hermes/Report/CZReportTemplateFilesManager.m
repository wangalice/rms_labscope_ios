//
//  CZReportTemplateFilesManager.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/7/3.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReportTemplateFilesManager.h"
#import <CZFileKit/CZFileKit.h>
#import <CZIKit/CZIDocument.h>

@implementation CZReportTemplateFilesManager

+ (void)copyDefaultTemplateFiles:(NSString *)templateType useLocal:(BOOL)useLocal {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentPath = [CZReportTemplateFilesManager documentPath];
    // check document path, if it contains report template file
    BOOL hasTemplate = NO;
    NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:documentPath];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        if ([[[file pathExtension] lowercaseString] isEqualToString:templateType]) {
            hasTemplate = YES;
            break;
        }
    }
    
    if (!hasTemplate) {
        // get templates directory
        NSString *reportTemplatePath = [[NSBundle mainBundle] bundlePath];
        if ([templateType isEqualToString:@"czrtj"]) {
            reportTemplatePath = [reportTemplatePath stringByAppendingPathComponent:@"ReportTemplates"];
        } else {
            reportTemplatePath = [reportTemplatePath stringByAppendingPathComponent:@"Templates"];
        }
        
        assert([fileManager fileExistsAtPath:reportTemplatePath]);  // this folder must exist after install the App.
        
        if (useLocal) {
            // get local template, if exists
            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
            NSString *localReportTemplatePath = [reportTemplatePath stringByAppendingPathComponent:language];
            BOOL isDir = NO;
            if ([fileManager fileExistsAtPath:localReportTemplatePath isDirectory:&isDir] && isDir) {
                reportTemplatePath = localReportTemplatePath;
            }
        }
        
        // copy template files to document directory
        dirEnum = [fileManager enumeratorAtPath:reportTemplatePath];
        while (file = [dirEnum nextObject]) {
            if ([[[file pathExtension] lowercaseString] isEqualToString:templateType]) {
                NSString *fullSrcPath = [reportTemplatePath stringByAppendingPathComponent:file];
                NSString *fullDstPath = [documentPath stringByAppendingPathComponent:file];
                [fileManager copyItemAtPath:fullSrcPath toPath:fullDstPath error:nil];
            }
        }
    }
}

+ (NSString *)documentPath {
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
}

+ (nullable UIImage *)loadThumbnailFromFile:(NSString *)filePath minSize:(CGFloat)minSize {
    UIImage *thumbnail = nil;
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[filePath pathExtension]];
    if (fileFormat == kCZFileFormatCZI) {
        CZIDocument *czi = [[CZIDocument alloc] initWithFile:filePath];
        thumbnail = [[UIImage alloc] initWithData:[czi thumbnail]];
    } else if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
        thumbnail = [CZCommonUtils thumnailFromImageFilePath:filePath atMinSize:minSize];
    }
    
    return thumbnail;
}

@end
