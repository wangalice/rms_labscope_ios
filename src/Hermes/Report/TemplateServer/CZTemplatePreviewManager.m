//
//  CZTemplatePreviewManager.m
//  Hermes
//
//  Created by Johnny on 7/24/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import "CZTemplatePreviewManager.h"
#import <CocoaHttpServer/HTTPServer.h>
//#import "DDLog.h"
//#import "DDTTYLogger.h"
#import "CZHTTPConnection.h"

#import <ReportFramework/CZGlobal.h>
#import <ReportFramework/CZReportEngine.h>
#import <ReportFramework/CZDictionaryTool.h>

#import <SBJson/SBJson.h>

#import "CZMergeDataManager.h"
#import "CZValidateStateConfig.h"
#import "ZipArchive.h"
#import "CZDefines.h"
#import "CZXMLValidation.h"
/// for ip adress
#import <ifaddrs.h>
#import <arpa/inet.h>

// Log levels: off, error, warn, info, verbose
//static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#define kServerPort 8080


static CZTemplatePreviewManager *templatePreViewManager = nil;


@interface CZTemplatePreviewManager ()<GenerateReportDelegate,CZValidateDelegate>{
    HTTPServer              *httpServer;
    CZReportEngine          *reportEngine;
    
    NSDictionary            *reportDataDic;
    NSMutableDictionary     *bakeupDataDic;
    
    CZMergeDataManager      *mergeDataManager;
    BOOL                     isGenerateFinished;
    NSString                *reportFilePath;
    
    NSString                *documentRoot;
}

@property (nonatomic, readwrite, retain)  NSString *reportTemplatePath;

@end

@implementation CZTemplatePreviewManager

@synthesize indexHtmlString;
@synthesize uploadHtmlString;
@synthesize reportDataDic;
@synthesize reportFilePath;
@synthesize delegate;

/// Get the share instance.
+ (CZTemplatePreviewManager*)sharedInstance {
    @synchronized (self) {
		if (templatePreViewManager == nil)
			templatePreViewManager = [[CZTemplatePreviewManager alloc] init];
	}
    return templatePreViewManager;
}

/// Free the instance for momory optimize using.
+ (void)freeInstance{
    if (templatePreViewManager) {
        FreeObj(templatePreViewManager);
    }
}

- (id)init{
    self = [super init];
    
    if (self) {
        [self initBakeUpData];
        NSString *fileName = kPDFName;
        NSString *documentsDirectory = NSTemporaryDirectory();
        NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
        reportFilePath =[[NSString alloc] initWithString:pdfFileName];
        
        mergeDataManager = [[CZMergeDataManager alloc]initWithData:bakeupDataDic];
        isGenerateFinished = NO;
        [[CZValidateStateConfig sharedInstance] setValidateDelegate:self];
            
        /// if begin to use the preview manager, defaultly,we install and start the iPad server for montoring.
        [self installServer];
    }

    return self;
}


- (void)initBakeUpData{
    NSString *dataPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"templateData" ofType:@"plist"];
    NSString *imagePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"Untitled"
                                                                           ofType:nil];
    CZLogv(@"the image path is %@",imagePath);
    FreeObj(bakeupDataDic);
    bakeupDataDic = [[NSMutableDictionary alloc] initWithContentsOfFile:dataPath];
    [bakeupDataDic setValue:imagePath forKey:kReportImageDataPath];
}


#pragma mark - notify the external class method ------------

- (void)notifyState:(CZTemplatePreviewState)aState{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(notifyGeneratePreviewState:)]){
            [self.delegate notifyGeneratePreviewState:aState];
        }
    });
}

- (void)notifyError:(NSError*)errorInfo{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate respondsToSelector:@selector(notifyPreviewFailWithError:)]){
            [self.delegate notifyPreviewFailWithError:errorInfo];
        }
    });
}

#pragma mark - Server Methods ------------

- (BOOL)installServer{
    BOOL success = YES;
    FreeObj(httpServer);
    /// Configure our logging framework.
	/// To keep things simple and fast, we're just going to log to the Xcode console.
//	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	
	/// Initalize our http server
	httpServer = [[HTTPServer alloc] init];
	
	/// Tell the server to broadcast its presence via Bonjour.
	/// This allows browsers such as Safari to automatically discover our service.
	[httpServer setType:@"_http._tcp."];
    
	/// Normally there's no need to run our server on any specific port.
	/// Technologies like Bonjour allow clients to dynamically discover the server's port at runtime.
	/// However, for easy testing you may want force a certain port so you can just hit the refresh button.
    
    [httpServer setPort:kServerPort];
	
	/// Serve files from the standard Sites folder
	NSString *docRoot = [self documentRoot];
	CZLogv(@"Setting document root: %@", docRoot);

    [self updateIndexHtml];
	
	///[httpServer setDocumentRoot:docRoot];
    [httpServer setDocumentRoot:docRoot];
    
	[httpServer setConnectionClass:[CZHTTPConnection class]];
	
	NSError *error = nil;
	if(![httpServer start:&error]){
        success = NO;
		CZLogv(@"Error starting HTTP Server: %@", error);
	}
    return success;
}

- (BOOL)startServer{
    BOOL success = YES;
    NSError *error = nil;
    if (![httpServer isRunning]) {
        [httpServer setConnectionClass:[CZHTTPConnection class]];
        if(![httpServer start:&error]){
            success = NO;
        }
    }
    if (success) {
        [self notifyState:CZTemplatePreviewStateServerStarted];
    }else{
        [self notifyError:error];
    }
    return success;
}

- (BOOL)isServerRunning{
    return [httpServer isRunning];
}

- (NSString *)getServerIpAdress{
    NSString *ip = [CZCommonUtils localIpAddress];
    NSString *serverAdress = [NSString stringWithFormat:@"http://%@:%d/",ip,kServerPort];
    serverAdress = [serverAdress stringByAppendingString:@" or "];
    
    NSString *hostAddress = [CZCommonUtils localHostName];
    hostAddress = [NSString stringWithFormat:@"http://%@:%d/",hostAddress,kServerPort];
    serverAdress = [serverAdress stringByAppendingString:hostAddress];
    
    return serverAdress;
}

- (void)stopServer{
   [httpServer stop];
}

- (NSString *)documentRoot{
    if (documentRoot == nil) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSString *documentsDirectory = NSTemporaryDirectory();
        documentRoot = [[documentsDirectory stringByAppendingPathComponent:@"web/"] retain];
        
        NSError *aError = nil;
        BOOL isFilePathValid = YES;
        if ([fileManager fileExistsAtPath:documentRoot]) {
            isFilePathValid = [fileManager removeItemAtPath:documentRoot error:&aError];
        }
        if (isFilePathValid) {
            NSString *docRoot = [[[NSBundle mainBundle] pathForResource:@"index"
                                                                 ofType:@"html"
                                                            inDirectory:@"web"] stringByDeletingLastPathComponent];
            
            isFilePathValid = [fileManager copyItemAtPath:docRoot
                                                   toPath:documentRoot
                                                    error:&aError];
            
            [self archiveReportTemplateFiles];
            
            assert(isFilePathValid);
            NSString *indexHtmlPath = [[NSBundle mainBundle] pathForResource:@"index"
                                                                      ofType:@"html"
                                                                 inDirectory:@"web"];
            NSData *indexHtmlData = [NSData dataWithContentsOfFile:indexHtmlPath];
            NSMutableString *indexHtmlStr = [[NSMutableString alloc]initWithData:indexHtmlData
                                                                        encoding:NSUTF8StringEncoding];
            self.indexHtmlString = indexHtmlStr;
            CZLogv(@"the index html string is %@",indexHtmlStr);
            FreeObj(indexHtmlStr);
            
            
            NSString *uploadHtmlPath = [[NSBundle mainBundle] pathForResource:@"upload"
                                                                       ofType:@"html"
                                                                  inDirectory:@"web"];
            NSData *uploadHtmlData = [NSData dataWithContentsOfFile:uploadHtmlPath];
            NSMutableString *uploadHtmlStr = [[NSMutableString alloc]initWithData:uploadHtmlData
                                                                         encoding:NSUTF8StringEncoding];
            self.uploadHtmlString = uploadHtmlStr;
            CZLogv(@"the upload html string is %@",uploadHtmlStr);
            FreeObj(uploadHtmlStr);
            
        }
    }
    return documentRoot;
}

#pragma mark - validate the template ------------

- (BOOL) validateWithTemplate:(NSString *)path{
    assert([path length]>0);
    NSString *templatePath = [NSString stringWithFormat:@"%@",path];
    BOOL success = YES;
    self.reportTemplatePath = templatePath;
    [self notifyState:CZTemplatePreviewStateTemplateUploaded];
    if (templatePath && [templatePath length]>0) {
        NSDictionary *templateDic = nil;
        
        NSData *data = [NSData dataWithContentsOfFile:templatePath];
        SBJsonParser *json_parser = [[SBJsonParser alloc] init];
        id templateData = [json_parser objectWithData:data];
        
        if ([templateData isKindOfClass:[NSDictionary class]]) {
            templateDic = templateData;
        
            if (reportDataDic && [[reportDataDic allKeys] count]>0) {
                FreeObj(reportEngine);
                reportEngine = [[CZReportEngine alloc]initEngine:reportDataDic
                                                  reportTemplate:templateDic];
            }else{
                [self initBakeUpData];
                [mergeDataManager prepareDataForTemplate:templatePath
                                            originalData:bakeupDataDic];
                FreeObj(reportEngine);
                reportEngine = [[CZReportEngine alloc]initEngine:mergeDataManager.dataDic
                                                  reportTemplate:templateDic];
            }
            reportEngine.delegate = self;
            
            NSError *aError = nil;
            [self notifyState:CZTemplatePreviewStateBeginValidate];
            if ([reportEngine validateReport:&aError isReport:NO]){
                [self beginPreview];
                [self syncStep:templatePath];
            }else{
                success = NO;
                if (!templateDic) {
                    [self xmlPlistFileBaseValidate:templatePath fileError:&aError];
                }
                
                [self notifyError:aError];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self updateErrorResponseHtml:aError Path:templatePath];
                    [[CZValidateStateConfig sharedInstance] setCurState:CZValidateStateIdle];
                });
                
            }
        } else if (templateData == nil) {
            NSString *errorString = [@"Parse JSON file error. " stringByAppendingString:json_parser.error];
            NSError *error = [NSError errorWithDomain:@"SBJson"
                                                 code:-1
                                             userInfo:@{NSLocalizedDescriptionKey: errorString}];
            success = NO;
            
            [self notifyError:error];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self updateErrorResponseHtml:error Path:templatePath];
                [[CZValidateStateConfig sharedInstance] setCurState:CZValidateStateIdle];
            });
        }
        
        [json_parser release];
    }else{
        success = NO;
    }
    return success;
}

- (BOOL)xmlPlistFileBaseValidate:(NSString *)path
                       fileError:(NSError **)aError{
    BOOL success = YES;
    CZXMLValidation *xmlValidate = [[CZXMLValidation alloc]initWithTemplateFile:path];
    [xmlValidate startValidate];
    if (xmlValidate.xmlFileError) {
        if (aError) {
           *aError = xmlValidate.xmlFileError;
            success = NO;
        }
    }
    FreeObj(xmlValidate);
    return success;
}

#pragma mark - validating the template success methods ------------
#pragma mark

- (void)syncStep:(NSString*)templatePath{
    while (YES) {
        if (isGenerateFinished) {
            [[CZValidateStateConfig sharedInstance] setCurState:CZValidateStateIdle];
            
            [self updateSucResponseHtml:templatePath];
            NSLog(@"generating is finished!!!");
            break;
        }
        [NSThread sleepForTimeInterval:0.1f];
    }
}


- (void)initUploadHtmlStr{
    NSString *uploadHtmlPath = [[NSBundle mainBundle] pathForResource:@"upload"
                                                               ofType:@"html"
                                                          inDirectory:@"web"];
    NSData *uploadHtmlData = [NSData dataWithContentsOfFile:uploadHtmlPath];
    NSMutableString *uploadStr = [[NSMutableString alloc]initWithData:uploadHtmlData
                                                             encoding:NSUTF8StringEncoding];
    self.uploadHtmlString = uploadStr;
    FreeObj(uploadStr);
}

- (void)updateSucResponseHtml:(NSString *)templatePath{
    [self initUploadHtmlStr];
    NSRange aPendRange = [self.uploadHtmlString rangeOfString:@"<body>"];
    NSString *value = [NSString stringWithFormat:@" %@",kSucResponseDes];
    value = [value stringByReplacingOccurrencesOfString:@"pdfFileName" withString:kPDFName];
    CZLogv(@"the range location is %lu,%lu",(unsigned long)aPendRange.location,(unsigned long)aPendRange.length);
    
    NSRange valueContentRange = [self.uploadHtmlString rangeOfString:value];
    if (valueContentRange.length >0) {
        [self.uploadHtmlString deleteCharactersInRange:valueContentRange];
    }
    
    CZLogv(@"before html %@",uploadHtmlString);
    [self.uploadHtmlString insertString:value atIndex:(aPendRange.location + aPendRange.length)];
    CZLogv(@"end html %@",uploadHtmlString);
    
    NSString *dir = nil;
    if ([templatePath rangeOfString:@"/upload"].length >0 ) {
        dir = [[templatePath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    }else{
        dir = [templatePath stringByDeletingLastPathComponent];
    }
    NSError *aError = nil;
    NSMutableString *serverPdfPath = [[NSMutableString alloc]initWithCapacity:15];
    [serverPdfPath appendString:dir];
    [serverPdfPath appendString:@"/"];
    [serverPdfPath appendString:kPDFName];
    [[NSFileManager defaultManager] removeItemAtPath:serverPdfPath error:nil];
    [[NSFileManager defaultManager] copyItemAtPath:reportFilePath
                                            toPath:serverPdfPath
                                             error:&aError];
    assert(aError == nil);
    FreeObj(serverPdfPath);
    
    NSString *uploadHtmlPath = [NSString stringWithFormat:@"%@/%@",dir,kUploadHtml];
    
    [uploadHtmlString writeToFile:uploadHtmlPath
                       atomically:YES
                         encoding:NSUTF8StringEncoding
                            error:&aError];
    CZLogv(@"%@",aError);
    assert(aError == nil);
    
}

#pragma mark - update index.html methods ------------

///update the index.html content and make the download dropdown list.
- (void)updateIndexHtml {
    if ([self.indexHtmlString length] == 0) {
        return;
    }
    
    NSRange beginRange = [self.indexHtmlString rangeOfString:@"<!--Begin Download List-->"];
    NSRange endRange = [self.indexHtmlString rangeOfString:@"<!--End Download List-->"];
    
    assert(beginRange.location != NSNotFound);
    assert(endRange.location != NSNotFound);
    
    if (beginRange.location != NSNotFound && endRange.location != NSNotFound) {
        NSMutableString *itemsString = [NSMutableString string];
        
        NSArray *subFileArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self documentRoot] error:nil];
        for (NSString *filePath in subFileArray) {
            NSString *extension = [[filePath pathExtension] lowercaseString];
            if ([extension isEqualToString:@"zip"]) {
                NSString *value = [filePath lastPathComponent];
                NSString *valueShow = [value stringByDeletingPathExtension];
                NSString *optionItemStr = [NSString stringWithFormat:@"<option value=\"%@\">%@</option>\n",value, valueShow];
                [itemsString appendString:optionItemStr];
            }
        }
        
        NSRange replaceRange = NSMakeRange(NSMaxRange(beginRange), endRange.location - NSMaxRange(beginRange));
        [self.indexHtmlString replaceCharactersInRange:replaceRange withString:itemsString];
    
        NSString *indexHtmlPath = [NSString stringWithFormat:@"%@/index.html", [self documentRoot]];
        NSError *aError = nil;
        [self.indexHtmlString writeToFile:indexHtmlPath
                          atomically:YES
                            encoding:NSUTF8StringEncoding
                               error:&aError];
        if (aError) {
            CZLogv(@"%@",aError);
        }

        assert(aError == nil);
    }
}

- (BOOL)archiveReportTemplateFiles {
    BOOL sucess = YES;
    
    NSString *iosDocPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSArray *filePaths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:iosDocPath error:nil];
    for (NSString* fileName in filePaths ) {
        NSString *extension = [[fileName pathExtension] lowercaseString];
        if ([extension isEqualToString:@"czrtj"]) {
            sucess = sucess && [self zipFile:[iosDocPath stringByAppendingPathComponent:fileName] toDirectory:self.documentRoot];
        }
    }
    
    return sucess;
}


- (BOOL)zipFile:(NSString *)templateFilePath toDirectory:(NSString *)toDirectory{
    BOOL success = NO;
    ZipArchive* zipTool = [[ZipArchive alloc]init];
    
    NSString *newName = [templateFilePath lastPathComponent];

    NSString *zipPath = [NSString stringWithFormat:@"%@/%@.zip",toDirectory,newName];
    
    success = [zipTool CreateZipFile2:zipPath];
    if (success) {
        success = [zipTool addFileToZip:templateFilePath newname:newName];
        if (success) {
            success = [zipTool CloseZipFile2];
        }
    }
    FreeObj(zipTool);
    
    return success;
}



#pragma mark - validating the template failed methods ------------


- (NSString *)convertToStringFromNSError:(NSError *)aError{
    
    //<br/>
    //NSString *errorDomain = aError.domain;
    NSDictionary *userInfo = [aError userInfo];
    NSDictionary *localUserDic = [userInfo valueForKey:NSLocalizedDescriptionKey];
    
    NSMutableArray *errorArray = [[NSMutableArray alloc]initWithCapacity:5];
    if (localUserDic && [localUserDic isKindOfClass:[NSDictionary class]]) {
        NSArray *detailArray = [localUserDic valueForKey:kErrorDetails];
        [errorArray addObjectsFromArray:detailArray];
    }
    if (errorArray && [errorArray count]>0) {
        NSMutableString *errorContentStr = [[NSMutableString alloc]initWithCapacity:20];
        
        NSString *errorDes = [localUserDic valueForKey:kErrorDes];
        
        //NSString *domainStr = [NSString stringWithFormat:@"error domain is: %@%@",errorDomain,@"<br/>"];
        NSString *codeStr = [NSString stringWithFormat:@"Error code is: %ld%@",(long)[aError code],@"<br/>"];
        NSString *details = [NSString stringWithFormat:@"Error details:%@",@"<br/>"];
        NSString *errorDescription = [NSString stringWithFormat:@"Error description is: %@%@",errorDes,@"<br/>"];
        
        //[errorContentStr appendString:domainStr];
        [errorContentStr appendString:codeStr];
        [errorContentStr appendString:errorDescription];
        [errorContentStr appendString:details];
        
        NSInteger msgIndex = 0;
        for (NSDictionary  *errorDic in errorArray) {
            /**
             NSNumber *errorCode = [errorDic valueForKey:kErrorOfCode];
             NSNumber *errorSequence = [errorDic valueForKey:kErrorSequence];
             NSString *information = [errorDic valueForKey:kErrorDetailsInfo];
             NSString *errorArea = [errorDic valueForKey:kErrorArea];
             NSString *errorType = [errorDic valueForKey:kErrorType];
             NSString *content = [NSString stringWithFormat:@"%@ is %@, %@ is %@, %@ is %@, %@ is %@;",kErrorArea,errorArea,kErrorType,errorType,kErrorSequence,errorSequence,kErrorDetailsInfo,information,     kErrorOfCode,errorCode];
             [errorContentStr appendString:content];
             */
            msgIndex += 1;
            NSString *errorDes = [errorDic  description];
            errorDes = [errorDes stringByReplacingOccurrencesOfString:@";" withString:@";<br/>"];
            errorDes = [errorDes stringByReplacingOccurrencesOfString:@"{" withString:@"{<br/> <div style=\"background-color:#ccc\">"];
            errorDes = [errorDes stringByReplacingOccurrencesOfString:@"}" withString:@"</div>}"];

            [errorContentStr appendString:[NSString stringWithFormat:@"Error message  <i>%ld</i><br/>",(long)msgIndex]];
            [errorContentStr appendString:errorDes];
            
            [errorContentStr appendString:@"<br/><br/>"];
        }
        FreeObj(errorArray);
        return [errorContentStr autorelease];
        
    }else{
        FreeObj(errorArray);
        NSString *errorInfo = [NSString stringWithFormat:@"Error code is: %ld<br/>  Error message is: %@<br/>",(long)[aError code],[aError localizedDescription]];
        return errorInfo;
    }
    
    
}

- (void)updateErrorInfoHtml:(NSError *)error Path:(NSString *)templatePath{
    
    NSString *errorHtmlPath = [[NSBundle mainBundle] pathForResource:@"ErrorInfo"
                                                               ofType:@"html"
                                                          inDirectory:@"web"];
    NSData *uploadHtmlData = [NSData dataWithContentsOfFile:errorHtmlPath];
    NSMutableString *errorInfoStr = [[NSMutableString alloc]initWithData:uploadHtmlData
                                                             encoding:NSUTF8StringEncoding];
    NSString *errorContent = [self convertToStringFromNSError:error];
    
    NSRange aPendRange = [errorInfoStr rangeOfString:@"</h4>"];
    NSString *value = [NSString stringWithFormat:@" %@",errorContent];
    CZLogv(@"the range location is %lu,%lu",(unsigned long)aPendRange.location,(unsigned long)aPendRange.length);
    
    
    [errorInfoStr insertString:value atIndex:(aPendRange.location + aPendRange.length)];
    CZLogv(@"end html %@",errorInfoStr);
    
    NSString *dir = nil;
    if ([templatePath rangeOfString:@"/upload"].length >0 ) {
        dir = [[templatePath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    }else{
        dir = [templatePath stringByDeletingLastPathComponent];
    }
    NSString *webErrorHtmlPath = [NSString stringWithFormat:@"%@/%@",dir,@"ErrorInfo.html"];
    NSError *aError = nil;
    [errorInfoStr writeToFile:webErrorHtmlPath
                       atomically:YES
                         encoding:NSUTF8StringEncoding
                            error:&aError];
    CZLogv(@"%@",aError);
    assert(aError == nil);
    FreeObj(errorInfoStr);
    
}

- (void)updateErrorResponseHtml:(NSError *)error Path:(NSString *)templatePath{
    /*
    [self initUploadHtmlStr];
    NSString *errorContent = [self convertToStringFromNSError:error];
    
    NSRange aPendRange = [self.uploadHtmlString rangeOfString:@"<body>"];
    NSString *value = [NSString stringWithFormat:@" %@",errorContent];
    CZLogv(@"the range location is %d,%d",aPendRange.location,aPendRange.length);
    
    
    CZLogv(@"before html %@",uploadHtmlString);
    [self.uploadHtmlString insertString:value atIndex:(aPendRange.location + aPendRange.length)];
    CZLogv(@"end html %@",uploadHtmlString);
    
    NSString *dir = nil;
    if ([templatePath rangeOfString:@"/upload"].length >0 ) {
        dir = [[templatePath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    }else{
        dir = [templatePath stringByDeletingLastPathComponent];
    }
    NSString *uploadHtmlPath = [NSString stringWithFormat:@"%@/%@",dir,kUploadHtml];
    NSError *aError = nil;
    [uploadHtmlString writeToFile:uploadHtmlPath
                       atomically:YES
                         encoding:NSUTF8StringEncoding
                            error:&aError];
    CZLogv(@"%@",aError);
    assert(aError == nil);
     */
    [self updateErrorInfoHtml:error Path:templatePath];
    [self initUploadHtmlStr];
    NSRange aPendRange = [self.uploadHtmlString rangeOfString:@"<body>"];
    NSString *value = [NSString stringWithFormat:@"%@",kErrorResponseDes];
    //value = [value stringByReplacingOccurrencesOfString:@"pdfFileName" withString:kPDFName];
    CZLogv(@"the range location is %lu,%lu",(unsigned long)aPendRange.location,(unsigned long)aPendRange.length);
    
    NSRange valueContentRange = [self.uploadHtmlString rangeOfString:value];
    if (valueContentRange.length >0) {
        [self.uploadHtmlString deleteCharactersInRange:valueContentRange];
    }
    
    CZLogv(@"before html %@",uploadHtmlString);
    [self.uploadHtmlString insertString:value atIndex:(aPendRange.location + aPendRange.length)];
    CZLogv(@"end html %@",uploadHtmlString);
    
    NSString *dir = nil;
    if ([templatePath rangeOfString:@"/upload"].length >0 ) {
        dir = [[templatePath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    }else{
        dir = [templatePath stringByDeletingLastPathComponent];
    }
    NSError *aError = nil;
    NSString *uploadHtmlPath = [NSString stringWithFormat:@"%@/%@",dir,kUploadHtml];
    
    [uploadHtmlString writeToFile:uploadHtmlPath
                       atomically:YES
                         encoding:NSUTF8StringEncoding
                            error:&aError];
    CZLogv(@"%@",aError);
    assert(aError == nil);
}



-(void)beginPreview{
    [[NSFileManager defaultManager]removeItemAtPath:reportFilePath error:nil];
    isGenerateFinished = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self notifyState:CZTemplatePreviewStateBeginGenerate];
        [reportEngine exportReport:reportFilePath
                        reportType:ReportEngineReportTypePDF
                 imageCompressRate:0.88 useDpi:200];
    });
    
}


#pragma mark reportEngine delegate
/**
 * @brief notify current generated report type ,generated progress and file desination.
 * @param type: the needed generated report type.
 * @param percent: the current generating percentage.
 * @param filePath: the file stored location.
 */
- (void)generateProgress:(ReportEngineReportType)type
       currentPercentage:(CGFloat)percent
             destination:(NSString *) filePath{
    
}

/**
 * @brief notify the generated state is finished.
 * @param filePath: the file stored location.
 */
- (void)generateDidFinishDestination:(NSString *)filePath {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        isGenerateFinished = YES;
        [self notifyState:CZTemplatePreviewStateGenerateFinished];
    });
}


/**
 * @brief notify the generated report failed.
 * @param error: indicate some error information.
 */
- (void)generateDidFailWithError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        isGenerateFinished = YES;
        [self notifyError:error];
    });
}

- (void)dealloc{
    self.delegate = nil;
    
    [CZValidateStateConfig freeInstance];
    
    FreeObj(indexHtmlString);
    FreeObj(uploadHtmlString);
    FreeObj(httpServer);
    
    FreeObj(reportDataDic);
    FreeObj(bakeupDataDic);
    
    FreeObj(mergeDataManager);
    FreeObj(reportEngine);
    FreeObj(reportFilePath);
    FreeObj(documentRoot);
    FreeObj(_reportTemplatePath);
    
    [super dealloc];
}

@end
