//
//  CZXMLValidation.h
//  
//
//  Created by Johnny on 7/30/13.
//
//

#import <Foundation/Foundation.h>

@interface CZXMLValidation : NSObject

@property (nonatomic,retain) NSError *xmlFileError;

- (id)initWithTemplateFile:(NSString *)filePath;

- (void)startValidate;

@end
