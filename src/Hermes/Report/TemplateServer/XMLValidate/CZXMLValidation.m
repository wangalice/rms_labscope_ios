//
//  CZXMLValidation.m
//  
//
//  Created by Johnny on 7/30/13.
//
//

#import "CZXMLValidation.h"
#import <libxml/tree.h>
#import <libxml/xmlschemas.h>
#import <libxml/parser.h>

// called from libxml functions
@interface CZXMLValidation (){
    BOOL  isXmlFormatRight;
    
    xmlParserCtxtPtr _xmlParserContext;
    NSString           *templatePath;
    
    /// Then following is for schema validation.
    xmlSchemaValidCtxtPtr  _xmlSchemaContext;
    xmlDocPtr               _docPtr;
}

@end


// Forward reference. The structure is defined in full at the end of the file.
static xmlSAXHandler simpleSAXHandlerStruct;

@implementation CZXMLValidation

@synthesize xmlFileError;


- (id)initWithTemplateFile:(NSString *)filePath{
    self = [super init];
    if (self) {
        _xmlParserContext = xmlCreatePushParserCtxt(&simpleSAXHandlerStruct, self, NULL, 0, NULL);
        
        templatePath = filePath;
        [templatePath retain];
        isXmlFormatRight = YES;
    }
    return self;
}


- (void)startValidate{
    int offset = 0;
    int chunkSize = 1024;     //Read 1KB chunks.
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:templatePath];
    NSData *data = [handle readDataOfLength:chunkSize];
    
    while ([data length]>0){
        ///Make sure for the next line you choose the appropriate string encoding.
        if ([data length] < chunkSize) {
            [self didReceiveData:data isFinish:YES];
        }else{
            [self didReceiveData:data isFinish:NO];
        }
        /* PERFORM STRING PROCESSING HERE */
        /* END STRING PROCESSING */
        offset += [data length];
        
        [handle seekToFileOffset:offset];
        data = [handle readDataOfLength:chunkSize];
    }
    
    [handle closeFile];
    
    [self SchemaValidate];
}

- (BOOL)SchemaValidate{
    if (!isXmlFormatRight) {
        return NO;
    }
    NSString *stdFile = [[NSBundle bundleForClass:[self class]] pathForResource:@"plistXml" ofType:@"xsd"];
    
    const char *stdfile = [stdFile cStringUsingEncoding:NSUTF8StringEncoding];
    
    xmlDocPtr schema_doc = xmlReadFile(stdfile, NULL, XML_PARSE_NONET);
    xmlSchemaParserCtxtPtr parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    xmlSchemaPtr schema = xmlSchemaParse(parser_ctxt);
    if (_xmlSchemaContext) {
     xmlSchemaFreeValidCtxt(_xmlSchemaContext);
        _xmlSchemaContext = nil;
    }
    _xmlSchemaContext =  xmlSchemaNewValidCtxt(schema);
    
    xmlSchemaSetValidErrors(_xmlSchemaContext,xmlSchemaValidityErrorFunc1,NULL,self);
    
    const char *file = [templatePath cStringUsingEncoding:NSUTF8StringEncoding];
    if (_docPtr) {
        xmlFreeDoc(_docPtr);
        _docPtr = nil;
    }
    _docPtr = xmlParseFile(file);
    xmlSchemaSetValidOptions(_xmlSchemaContext,XML_SCHEMA_VAL_VC_I_CREATE);
    BOOL is_valid = (xmlSchemaValidateDoc(_xmlSchemaContext, _docPtr) == 0);
    
    
    xmlSchemaFree(schema);
    xmlSchemaFreeParserCtxt(parser_ctxt);
    xmlFreeDoc(schema_doc);
    
    return is_valid;
}

// Called when a chunk of data has been downloaded.
- (void)didReceiveData:(NSData *)data isFinish:(BOOL)isDataFinished{
    xmlParseChunk(_xmlParserContext, (const char *)[data bytes], (int)[data length], 0);
    if (isDataFinished) {
        xmlParseChunk(_xmlParserContext, NULL, 0, 1);
    }
}

- (void)xmlFormatError:(NSString *)errorString{
    
     NSString *errorDesString = [errorString stringByReplacingOccurrencesOfString:@"line 0" withString:@""];
    
     isXmlFormatRight = NO;
     NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : errorDesString};
     NSError *error = [NSError errorWithDomain:@"XmlParsingDomain"
                                             code:101
                                         userInfo:userInfo];
    self.xmlFileError = error;
    
    NSLog(@"Error happend,error is %@",error);
}

- (void)xmlSchemaError:(NSString *)errorString{
    if ([errorString isEqualToString:@"Element 'key': This element is not expected.\n"]) {
        return;
    }
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : errorString};

    NSError *error = [NSError errorWithDomain:@"XmlParsingDomain"
                                         code:101
                                     userInfo:userInfo];
     self.xmlFileError = error;
    NSLog(@"Error happend,error is %@",error);
}


#pragma mark SAX Parsing Callbacks

static void startElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                            const xmlChar *URI, int nb_namespaces, const xmlChar **namespaces,
                            int nb_attributes, int nb_defaulted, const xmlChar **attributes) {
}

static void	endElementSAX(void *ctx, const xmlChar *localname, const xmlChar *prefix,
                          const xmlChar *URI) {
}

static void	charactersFoundSAX(void *ctx, const xmlChar *ch, int len) {
    
}

static void errorEncounteredSAX(void *ctx, const char *msg, ...) {
    va_list argList;
    va_start(argList, msg);
    NSString *format = [[NSString alloc] initWithBytes:msg length:strlen(msg)
                                              encoding:NSUTF8StringEncoding];
    
    CFStringRef resultString = NULL;
    //va_list argList;
    va_start(argList, msg);
    resultString = CFStringCreateWithFormatAndArguments(NULL, NULL,
                                                        (CFStringRef)format,
                                                        argList);
    va_end(argList);

    [format release];
    format = nil;
    
    [(CZXMLValidation*)ctx xmlFormatError:(NSString*)resultString];
    CFRelease(resultString);
    
}

static void xmlSchemaValidityErrorFunc1(void *ctx, const char *msg, ...) {
    va_list argList;
    va_start(argList, msg);
    // [((EarthquakeParser *)ctx) parsingError:msg, argList];
    NSString *format = [[NSString alloc] initWithBytes:msg length:strlen(msg)
                                              encoding:NSUTF8StringEncoding];
    
    CFStringRef resultString = NULL;
    //va_list argList;
    va_start(argList, msg);
    resultString = CFStringCreateWithFormatAndArguments(NULL, NULL,
                                                        (CFStringRef)format,
                                                        argList);
    va_end(argList);
    [format release];
    format = nil;
    
    [(CZXMLValidation*)ctx xmlSchemaError:(NSString*)resultString];
    CFRelease(resultString);
    
}

static void endDocumentSAX(void *ctx) {
}

static xmlSAXHandler simpleSAXHandlerStruct = {
    NULL,                       /* internalSubset */
    NULL,                       /* isStandalone   */
    NULL,                       /* hasInternalSubset */
    NULL,                       /* hasExternalSubset */
    NULL,                       /* resolveEntity */
    NULL,                       /* getEntity */
    NULL,                       /* entityDecl */
    NULL,                       /* notationDecl */
    NULL,                       /* attributeDecl */
    NULL,                       /* elementDecl */
    NULL,                       /* unparsedEntityDecl */
    NULL,                       /* setDocumentLocator */
    NULL,                       /* startDocument */
    endDocumentSAX,             /* endDocument */
    NULL,                       /* startElement*/
    NULL,                       /* endElement */
    NULL,                       /* reference */
    charactersFoundSAX,         /* characters */
    NULL,                       /* ignorableWhitespace */
    NULL,                       /* processingInstruction */
    NULL,                       /* comment */
    NULL,                       /* warning */
    errorEncounteredSAX,        /* error */
    NULL,                       /* fatalError //: unused error() get all the errors */
    NULL,                       /* getParameterEntity */
    NULL,                       /* cdataBlock */
    NULL,                       /* externalSubset */
    XML_SAX2_MAGIC,             // initialized? not sure what it means just do it
    NULL,                       // private
    startElementSAX,            /* startElementNs */
    endElementSAX,              /* endElementNs */
    NULL,                       /* serror */
};

- (void)dealloc{
    [xmlFileError release];
    if (_xmlSchemaContext) {
        xmlSchemaFreeValidCtxt(_xmlSchemaContext);
        _xmlSchemaContext = nil;
    }
    if (_docPtr) {
        xmlFreeDoc(_docPtr);
        _docPtr = nil;
    }
    xmlFreeParserCtxt(_xmlParserContext);
    [super dealloc];
}


@end
