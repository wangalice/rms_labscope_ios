//
//  CZDefines.h
//  Hermes
//
//  Created by Johnny on 7/16/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#ifndef TemplateManagement_CZDefines_h
#define TemplateManagement_CZDefines_h

#define  CZReportTemplateChangedNotification    @"ReportTemplateChanged"
#define  kAlertViewTag                          1000
#define  kReportImageDataPath                   @"reportImagePath"


#define kErrorArea                              @"Error happended area"
#define kErrorSequence                          @"Error element's sequence"
#define kErrorType                              @"Error element's type"
#define kErrorOfCode                            @"Error code"
#define kErrorDetailsInfo                       @"Error detailed information"

#define kPDFName                                @"Preview.pdf"

#define kSucResponseDes                         @"Template validate successfully!<br/> \
                                                It will return to home page in 3 seconds...  \
                                                <a  id = \"templatepdfId\" href=\"pdfFileName\" target=\"_blank\">Download Template Detail Instruction</a>\
                                                <br/>\
                                                     \
                                                <script type=\"text/javascript\">\
                                                document.getElementById(\"templatepdfId\").style.display=\"none\";\
                                                setTimeout(\"window.history.back()\",4500);\
                                                setTimeout(\"document.getElementById('templatepdfId').click()\",500);\
                                                </script> "


#define kErrorResponseDes                       @"Template's validation is Failed!<br/> \
                                                It will return to home page in 3 seconds...  \
                                                <a  id = \"templatepdfId\" href=\"ErrorInfo.html\" target=\"_blank\">Download Template Detail Instruction</a>\
                                                <br/>\
                                                \
                                                <script type=\"text/javascript\">\
                                                document.getElementById(\"templatepdfId\").style.display=\"none\";\
                                                setTimeout(\"window.history.back()\",4500);\
                                                setTimeout(\"document.getElementById('templatepdfId').click()\",500);\
                                                </script> "

#define kUploadHtml                             @"upload.html"

/// The followed defines is used in CZMergeDataManager class.

#define kDepartmentName                         @"departmentName"
#define kCompanyName                            @"companyName"
#define kCompanyAddress                         @"companyAdress"
#define kRegistDate                             @"registDate"
#define kCompanySite                            @"companySite"
#define kCompanyMail                            @"companyMail"
#define kReportTitle                            @"reportTitle"
#define kReportImagePath                        @"reportImagePath"
#define kPieAndColumnData                       @"allYearsProduction"
#define kTableData                              @"tableContent"
#define kTextData                               @"commonDescription"

#define kFooterTextA                            @"footerA"
#define kFooterTextB                            @"footerB"
#define kFooterTextC                            @"footerC"

#define kDataFromTemplate                       @"reportSampleData"

#endif
