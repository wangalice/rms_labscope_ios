//
//  CZMergeDataManager.h
//  Hermes
//
//  Created by Johnny on 6/20/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZMergeDataManager : NSObject{
    NSMutableDictionary          *_dataDic;
}

@property (nonatomic,readonly) NSMutableDictionary *dataDic;

- (id)initWithData:(NSMutableDictionary *)data;

/// Every time when user submit the template,should prepare the data that the template used.
- (void)prepareDataForTemplate:(NSString *)templatePath
                  originalData:(NSMutableDictionary *)data;
@end
