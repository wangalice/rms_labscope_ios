//
//  CZHTTPConnection.h
//  TemplateMangment
//
//  Created by Johnny on 6/5/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import <CocoaHttpServer/HTTPConnection.h>

@class MultipartFormDataParser;

@interface CZHTTPConnection : HTTPConnection  {
    MultipartFormDataParser*        parser;
	NSFileHandle*					storeFile;
	
	NSMutableArray*					uploadedFiles;
    
    NSString*                       fileDestination;
}

@end
