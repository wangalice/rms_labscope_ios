//
//  CZValidateStateConfig.m
//  Hermes
//
//  Created by Johnny on 7/15/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import "CZValidateStateConfig.h"

static CZValidateStateConfig *validateStateConfig = nil;


@implementation CZValidateStateConfig

@synthesize curState;
@synthesize validateDelegate;

#pragma mark -
#pragma mark Singleton Pattern

/// Get the share instance.
+ (id)sharedInstance {
    @synchronized (self) {
		if (validateStateConfig == nil)
			validateStateConfig = [[CZValidateStateConfig alloc] init];
	}
    return validateStateConfig;
}

/// Free the instance for momory optimize using.
+ (void)freeInstance{
    [validateStateConfig release];
    validateStateConfig = nil;
}

- (id)init{
    self = [super init];
    self.curState = CZValidateStateIdle;
    return self;
}

/// note: this method is processed by sub-thread when called frome the CZHttpConnection class.
- (void)notifyValidate:(NSString *)templatePath{
    NSThread *curThread = [NSThread currentThread];
    /// hold current thread, until the method is finished.
    [self performSelector:@selector(notifyDelegateController:)
                 onThread:curThread
               withObject:templatePath
            waitUntilDone:YES];
}


/** if pre-validation is not finished, but the notifycation is come again, the later validate notification will be abort.
 */
- (void)notifyDelegateController:(NSString *)path{
    if (self.curState == CZValidateStateValidating) {
        return;
    }
    self.curState = CZValidateStateValidating;
    if (self.validateDelegate && [self.validateDelegate respondsToSelector:@selector(validateWithTemplate:)]) {
        [self.validateDelegate validateWithTemplate:path];
    }
}


- (void)dealloc{
    self.validateDelegate = nil;
    [super dealloc];
}
@end
