//
//  CZValidateStateConfig.h
//  Hermes
//
//  Created by Johnny on 7/15/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZValdationState){
    CZValidateStateIdle,
    CZValidateStateValidating,
    CZValidateStateFinished,
};

@protocol CZValidateDelegate <NSObject>

- (BOOL) validateWithTemplate:(NSString *)path;

@end

@interface CZValidateStateConfig : NSObject

@property (nonatomic,assign)  CZValdationState curState;

@property (nonatomic,assign) id<CZValidateDelegate> validateDelegate;

/**
 * @brief CZValidateStateConfig shared instance.
 * @return Returns the CZValidateStateConfig’s shared instance, which is singleton implement internally.
 */
+ (id)sharedInstance;

/** Free the instance for momory optimize using.
 */
+ (void)freeInstance;

/**
 * This method must be synchronization, because the http web response is base on the template validation
 * result, if validation is successful,the response is preview the PDF file,if the validation is failed,the response will be error information.
 */
- (void)notifyValidate:(NSString *)templatePath;

@end
