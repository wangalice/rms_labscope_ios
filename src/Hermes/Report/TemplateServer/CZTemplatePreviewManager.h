//
//  CZTemplatePreviewManager.h
//  Hermes
//
//  Created by Johnny on 7/24/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import <Foundation/Foundation.h>

/// the generated report type
typedef NS_ENUM(NSUInteger, CZTemplatePreviewState) {
    CZTemplatePreviewStateIdle,
    CZTemplatePreviewStateServerStarted,
    CZTemplatePreviewStateTemplateUploaded,
    CZTemplatePreviewStateBeginValidate,
    CZTemplatePreviewStateBeginGenerate,
    CZTemplatePreviewStateGenerateFinished,
};

@protocol TemplateManagerDelegate<NSObject>

@required

/**
 * @brief notify the template validate failed.
 * @param error: indicate some error information.
 */
- (void)notifyPreviewFailWithError:(NSError *)error;

/**
 * @brief notify the template validate and generate report state,if generate finished, it can begin 
 *  to preview.
 * @param aState: indicate the state information.
 */
- (void)notifyGeneratePreviewState:(CZTemplatePreviewState)aState;
@end

@interface CZTemplatePreviewManager : NSObject

@property (nonatomic,retain)    NSMutableString             *indexHtmlString;
@property (nonatomic,retain)    NSMutableString             *uploadHtmlString;
@property (nonatomic,retain)    NSDictionary                *reportDataDic;
@property (nonatomic,readonly)  NSString                    *reportFilePath;
@property (nonatomic,readonly, retain)  NSString            *reportTemplatePath;
@property (nonatomic,assign)    id<TemplateManagerDelegate>  delegate;

/**
 * @brief CZTemplatePreviewManager shared instance.
 * @return Returns the CZTemplatePreviewManager’s shared instance, which is singleton implement internally.
 */
+ (CZTemplatePreviewManager*)sharedInstance;

+ (void)freeInstance;

/**
 *  Install the server to iPad app, this action maybe failed, so when use this method should consider 
 *  this situation.
 */
- (BOOL)installServer;

/// Start the web server for using or responsing.
- (BOOL)startServer;

/// return the boolean value indicate the server is running.
- (BOOL)isServerRunning;

/// Get the server ip address (maybe ipv4 or ipv6).
- (NSString *)getServerIpAdress;

/// Stop the server.
- (void)stopServer;

@end
