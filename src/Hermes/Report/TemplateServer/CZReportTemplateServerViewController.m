//
//  CZReportTemplateServerViewController.m
//  Hermes
//
//  Created by Johnny on 6/5/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import "CZReportTemplateServerViewController.h"

// for ip adress
#import <ifaddrs.h>
#import <arpa/inet.h>

#import <QuickLook/QuickLook.h>

#import <ReportFramework/CZGlobal.h>
#import <ReportFramework/CZDictionaryTool.h>

#import "CZToastManager.h"
#import "CZUIDefinition.h"

#import "CZMergeDataManager.h"
#import "CZDefines.h"
#import "CZTemplatePreviewManager.h"
#import <CZFileKit/CZFileKit.h>

const static CGFloat kLableWidth = 800;
const static CGFloat kLabelHeight = 20;
const static NSUInteger kLabelMaxLineCount = 12;
const static CGFloat kLabelTopMagin = 32;

@interface CZReportTemplateServerViewController () <TemplateManagerDelegate, QLPreviewControllerDataSource> {
    NSString *_reportFilePath;
    BOOL _shouldRelease;
}

@property (nonatomic, retain) UILabel *detailInfoLabel;
@property (nonatomic, retain) UILabel *serverAdress;
@property (nonatomic, retain) UILabel *erroInfoLabel;
@property (nonatomic, retain) QLPreviewController *previewController;

@end

@implementation CZReportTemplateServerViewController

+ (UILabel *)newLabelAtPoint:(CGPoint)point {
    point.x = (1024.0 - kLableWidth) * 0.5;
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(point.x, point.y, kLableWidth, kLabelHeight)];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [UIColor cz_gs50];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont cz_subtitle1];
    return label;
}

- (void)dealloc {
    [_reportFilePath release];
    [_previewController release];
    [_detailInfoLabel release];
    [_erroInfoLabel release];
    [_serverAdress release];
    [super dealloc];
}

- (void)viewDidLoad{
    _reportFilePath = nil;
    
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    self.view.backgroundColor = [UIColor cz_gs105];
    CZBarButtonItem *saveBarButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"SAVE")
                                                                         target:self
                                                                         action:@selector(saveAction:)];
    
    saveBarButtonItem.accessibilityIdentifier = @"SaveButton";
    saveBarButtonItem.isAccessibilityElement = YES;
    self.navigationItem.rightBarButtonItem = saveBarButtonItem;
    
    [self enableButtons:NO];
    
    [CZTemplatePreviewManager sharedInstance].delegate = self;
    [[CZTemplatePreviewManager sharedInstance] startServer];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    // stop http server
    [[CZTemplatePreviewManager sharedInstance] stopServer];
    [CZTemplatePreviewManager freeInstance];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _previewController.view.frame = self.view.bounds;
    
    if (_shouldRelease) {
        _shouldRelease = NO;
        [_previewController release];
    }
}

#pragma mark - Action

- (void)saveAction:(id)sender {
    NSString *srcReportTemplatePath = [CZTemplatePreviewManager sharedInstance].reportTemplatePath;
    if ([[NSFileManager defaultManager] fileExistsAtPath:srcReportTemplatePath]) {
   
        NSString *reportTemplateName = [srcReportTemplatePath lastPathComponent];
        NSString *dstReportTemplatePath = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:reportTemplateName];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:dstReportTemplatePath]) {
            dstReportTemplatePath = [CZFileNameGenerator uniqueFilePath:dstReportTemplatePath];
        }
        [[NSFileManager defaultManager] linkItemAtPath:srcReportTemplatePath toPath:dstReportTemplatePath error:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)enableButtons:(BOOL)enabled {
    for (UIBarButtonItem *button in self.navigationItem.rightBarButtonItems) {
        button.enabled = enabled;
    }
}

#pragma mark - Private method

- (void)popUpPreview {
    self.detailInfoLabel.text = L(@"REPORT_PREVIEW_GENERATED");
    
    // release the preview controller, otherwise the preview view doesn't really reload the pdf file.
    if (_previewController) {
        [_previewController removeFromParentViewController];
        [_previewController.view removeFromSuperview];
        self.previewController = nil;
    }
    
    QLPreviewController *preViewController = self.previewController;
    [self.view bringSubviewToFront:preViewController.view];
    self.previewController.view.hidden = NO;
    
    [self.previewController reloadData];
    
    // Workaround for memory leak of iOS 10, see also https://forums.developer.apple.com/thread/63020
    NSOperatingSystemVersion ios11_0_0 = (NSOperatingSystemVersion){11, 0, 0};
    NSOperatingSystemVersion ios10_0_0 = (NSOperatingSystemVersion){10, 0, 0};
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios11_0_0]) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
            _shouldRelease = NO;
        }
    } else if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios10_0_0]) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
            _shouldRelease = YES;
        }
    }
}

- (void)dismissPreview {
    _previewController.view.hidden = YES;
}

- (void)showAlert{
    [[CZToastManager sharedManager] showToastMessage:L(@"REPORT_TEMPLATE_INVALID") sourceRect:CGRectOffset([UIScreen mainScreen].bounds, 0, CGRectGetMidY([UIScreen mainScreen].bounds))];
}

#pragma mark - TemplateManagerDelegate

- (void)notifyPreviewFailWithError:(NSError *)error{
    NSString *errorInfo = [NSString stringWithFormat:L(@"REPORT_ERROR_INFO"),
                           [error code],[error localizedDescription]];
    self.erroInfoLabel.hidden = NO;
    self.erroInfoLabel.text = errorInfo;
    [self enableButtons:NO];
    
    [self dismissPreview];
    [self showAlert];
}

- (void)notifyGeneratePreviewState:(CZTemplatePreviewState)aState{
    switch (aState) {
        case CZTemplatePreviewStateServerStarted: {
            self.detailInfoLabel.text = L(@"REPORT_SERVER_STARTED");
            NSString *serverAddress = [[CZTemplatePreviewManager sharedInstance] getServerIpAdress];
            self.serverAdress.text = [NSString stringWithFormat:L(@"REPORT_SERVER_ADDRESS"),serverAddress];
            _erroInfoLabel.hidden = YES;
            break;
        }
        case CZTemplatePreviewStateTemplateUploaded: {
            _erroInfoLabel.hidden = YES;
            self.detailInfoLabel.text = L(@"REPORT_TEMPLATE_UPLOAD_SUCCESS");
            [self dismissPreview];
            break;
        }
        case CZTemplatePreviewStateBeginValidate: {
            self.detailInfoLabel.text = L(@"REPORT_SERVER_BEGIN");
            break;
        }
        case CZTemplatePreviewStateBeginGenerate: {
            self.detailInfoLabel.text = L(@"REPORT_SERVER_BEGIN_GENERATE");
            break;
        }
        case CZTemplatePreviewStateGenerateFinished: {
            [_reportFilePath release];
            _reportFilePath = [CZTemplatePreviewManager sharedInstance].reportFilePath;
            [_reportFilePath retain];
            [self popUpPreview];
            if ([[NSFileManager defaultManager] fileExistsAtPath:_reportFilePath]) {
               [self enableButtons:YES];
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark - QLPreviewControllerDataSource

// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController {
    return 1;
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller{
    // if the preview dismissed (done button touched), use this method to post-process previews
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx {
    NSURL *fileURL = nil;
    if (_reportFilePath && [_reportFilePath length]> 0) {
        fileURL = [NSURL fileURLWithPath:_reportFilePath];
    }
    
    if (fileURL == nil) {
        fileURL = [NSURL fileURLWithPath:@""];
    }
    
    return fileURL;
}

#pragma mark - Getter

- (UILabel *)detailInfoLabel {
    if (_detailInfoLabel == nil) {
        _detailInfoLabel = [CZReportTemplateServerViewController newLabelAtPoint:CGPointMake(0, kLabelTopMagin)];
        [self.view addSubview:_detailInfoLabel];
    }
    return _detailInfoLabel;
}

- (UILabel *)serverAdress {
    if (_serverAdress == nil) {
        CGPoint origin = CGPointMake(0, kLabelTopMagin + kLabelHeight);
        _serverAdress = [CZReportTemplateServerViewController newLabelAtPoint:origin];
        origin = _serverAdress.frame.origin;
        _serverAdress.frame = CGRectMake(origin.x, origin.y, kLableWidth, kLabelHeight * 2);
        _serverAdress.lineBreakMode = NSLineBreakByWordWrapping;
        _serverAdress.numberOfLines = 2;
        [self.view addSubview:_serverAdress];
    }
    
    return _serverAdress;
}

- (UILabel *)erroInfoLabel {
    if (_erroInfoLabel == nil) {
        _erroInfoLabel = [CZReportTemplateServerViewController newLabelAtPoint:CGPointMake(0, kLabelTopMagin + kLabelHeight * 2)];
        CGRect frame = _erroInfoLabel.frame;
        frame.size.height = kLabelHeight * kLabelMaxLineCount;
        _erroInfoLabel.frame = frame;
        _erroInfoLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _erroInfoLabel.numberOfLines = 0;
        [self.view addSubview:_erroInfoLabel];
    }
    
    return _erroInfoLabel;
}

- (QLPreviewController *)previewController {
    if (_previewController == nil) {
        QLPreviewController *preViewController = [[QLPreviewController alloc] init];
        preViewController.dataSource = self;
        
        [self.view addSubview:preViewController.view];
        
        // start previewing the document at the current section index
        preViewController.currentPreviewItemIndex = 0;
        _previewController = preViewController;
    }
    
    return _previewController;
}

@end
