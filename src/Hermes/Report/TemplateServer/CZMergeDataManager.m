//
//  CZMergeDataManager.m
//  Hermes
//
//  Created by Johnny on 6/20/13.
//  Copyright (c) 2013 Camel Team. All rights reserved.
//

#import "CZMergeDataManager.h"

#import <ReportFramework/CZGlobal.h>
#import <ReportFramework/CZReportEngine.h>
#import <ReportFramework/CZDictionaryTool.h>

#import "CZDefines.h"


@interface CZMergeDataManager (){
    CZReportEngine          *reportEngine;
    
    NSMutableDictionary     *templateDataDic;
}

@end

@implementation CZMergeDataManager

@synthesize  dataDic = _dataDic;

- (id)initWithData:(NSMutableDictionary *)data{
    if (self = [super init]) {
        [data retain];
        _dataDic = data;
    }
    return self;
}

- (void)prepareDataForTemplate:(NSString *)templatePath
                  originalData:(NSMutableDictionary *)data{
    FreeObj(_dataDic);
    [data retain];
    _dataDic = data;
    [self prepareDataForTemplate:templatePath];
}

/**
 if the template define the ReportData, use ReportData firstly.
 set the key and value frome ReportData to bakeup data
 */
- (void)updateDataFromTemplate:(NSString *)templatePath{
    FreeObj(templateDataDic);
    NSDictionary *templateDic = [NSDictionary dictionaryWithContentsOfFile:templatePath];
    if (templateDic) {
        templateDataDic = [templateDic valueForKey:kDataFromTemplate];
        [templateDataDic retain];
        if (![templateDataDic isKindOfClass:[NSDictionary class]]) {
            FreeObj(templateDataDic);
        }else{
            for (NSString *key in [templateDataDic allKeys]) {
                id value = [templateDataDic valueForKey:key];
                [_dataDic setValue:value forKey:key];
            }
        }
    }
}

- (void)prepareDataForTemplate:(NSString *)templatePath{
    NSDictionary *templateDic = [NSDictionary dictionaryWithContentsOfFile:templatePath];
    [self updateDataFromTemplate:templatePath];
    FreeObj(reportEngine);
    reportEngine = [[CZReportEngine alloc]initEngine:_dataDic
                                      reportTemplate:templateDic];
    reportEngine.delegate = nil;
    NSError *aError = nil;
    if (![reportEngine validateReport:&aError isReport:NO]) {
        NSArray* dataMissedElements = [reportEngine scanTemplate];
        if (!dataMissedElements || [dataMissedElements count]<1) {
            return ;
        }
        for (NSDictionary *dic in dataMissedElements) {
            [self extractKey:dic];
        }
    }
}

#pragma mark - extract key from the template ------------

- (BOOL)extractKey:(NSDictionary *)missedValueDic{
    assert([missedValueDic allKeys]>0);
    BOOL isSuc = YES;
    NSString *elementType = [missedValueDic valueForKey:kElementType];
    NSDictionary *value = [missedValueDic valueForKey:kValue];
    
    if (!elementType || !value || ![value isKindOfClass:[NSDictionary class]]) {
        isSuc = NO;
        return isSuc;
    }
    if ([elementType isEqualToString:kHeaderElement]) {
        NSString *key = [value valueForKey:kHeaderElementValue];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            [self addKeyValueForHeader:key];
        }
        return isSuc;
        
    }else if ([elementType isEqualToString: kTextElement]){
        NSString *key = [value valueForKey:kTextValue];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        NSNumber *numBool = [missedValueDic valueForKey:kIsInFooter];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            if (numBool && [numBool boolValue]) {
                [self addKeyValueForFooterText:key];
            }else{
                [self addKeyValueForText:key];
             }
        }
        return isSuc;
        
    }else if ([elementType isEqualToString: kImageElement]){
        NSString *key  = [value valueForKey:kImageFile];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            [self addKeyValueForImage:key];
        }
        return isSuc;
        
    }else if ([elementType isEqualToString: kPieElement]){
        NSString *key = [value valueForKey:kPieComponentArray];
        assert([key isKindOfClass:[NSString class]]);
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            [self addKeyValueForPie:key];
        }
        return isSuc;
    }else if ([elementType isEqualToString: kColumnElement]){
        NSString *key = [value valueForKey:kColumnComponentArray];
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            [self addKeyValueForColumn:key];

        }
        return isSuc;
    }else if ([elementType isEqualToString: kTableElement]){
        NSString *key = [value valueForKey:kTableValue];
        assert([key isKindOfClass:[NSString class]]);
        BOOL isAssigned = [CZDictionaryTool isHaveAssigned:key];
        if (!isAssigned) {
            key = [CZDictionaryTool extractKey:key];
            [self addKeyValueForTable:key];
        }
        return isSuc;
    }
    return isSuc;
}

- (NSString *)getValueForHeader{
    NSString *headerValue = nil;
    NSInteger srandValue = (arc4random() % 6)+1;
    switch (srandValue) {
        case 1:
            headerValue = [_dataDic valueForKey:kDepartmentName];
            break;
        case 2:
            headerValue = [_dataDic valueForKey:kCompanyName];
            break;
        case 3:
            headerValue = [_dataDic valueForKey:kCompanyAddress];
            break;
        case 4:
            headerValue = [_dataDic valueForKey:kRegistDate];
            break;
        case 5:
            headerValue = [_dataDic valueForKey:kCompanySite];
            break;
        case 6:
            headerValue = [_dataDic valueForKey:kCompanyMail];
            break;
        default:
            break;
    }
    return headerValue;
}

- (NSString *)getValueForFooter{
    NSString *footerValue = nil;
    NSInteger srandValue = (arc4random() % 3)+1;
    switch (srandValue) {
        case 1:
            footerValue = [_dataDic valueForKey:kFooterTextA];
            break;
        case 2:
            footerValue = [_dataDic valueForKey:kFooterTextB];
            break;
        case 3:
            footerValue = [_dataDic valueForKey:kFooterTextC];
            break;
            
        default:
            break;
    }
    return footerValue;
}

- (BOOL)dataDicIsContainKey:(NSString *)key{
    BOOL isContain = NO;
    
    NSArray *keyArray = [_dataDic allKeys];
    if (keyArray && [keyArray count]>0) {
        if ([keyArray containsObject:key]) {
            isContain = YES;
        }
    }
    return isContain;
}


#pragma mark -assign value for extract key  ------------


- (void)addKeyValueForHeader:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString *value = [self getValueForHeader];
    [_dataDic setValue:value forKey:key];
}

- (void)addKeyValueForText:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString * textData = [_dataDic valueForKey:kTextData];
    [_dataDic setValue:textData forKey:key];
}

- (void)addKeyValueForFooterText:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString * textData = [self getValueForFooter];
    [_dataDic setValue:textData forKey:key];
}

- (void)addKeyValueForImage:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString *imageData = [_dataDic valueForKey:kReportImagePath];
    [_dataDic setValue:imageData forKey:key];
}

- (void)addKeyValueForPie:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString *pieData = [_dataDic valueForKey:kPieAndColumnData];
    [_dataDic setValue:pieData forKey:key];
}

- (void)addKeyValueForColumn:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString *columnData = [_dataDic valueForKey:kPieAndColumnData];
    [_dataDic setValue:columnData forKey:key];
}

- (void)addKeyValueForTable:(NSString *)key{
    assert(key && [key length]>0);
    if ([self dataDicIsContainKey:key]) {
        return;
    }
    NSString *tableData = [_dataDic valueForKey:kTableData];
    [_dataDic setValue:tableData forKey:key];
}

- (void)dealloc{
    FreeObj(reportEngine);
    FreeObj(_dataDic);
    FreeObj(templateDataDic);
    [super dealloc];
}


@end
