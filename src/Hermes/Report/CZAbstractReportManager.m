//
//  CZAbstractReportManager.m
//  Matscope
//
//  Created by Ralph Jin on 5/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZAbstractReportManager.h"

#import <ReportFramework/CZReportEngine.h>

#import <SBJson/SBJson.h>
#import "CZReportField.h"

NSString *const kCZReserveReportImageKey = @"_reportImagePath";
static NSString *const kReportTemparoyDirectory = @"ReportTemporaryFiles";

@interface CZAbstractReportManager ()

@property (nonatomic, retain) NSString *templateFileMD5;

@end

@implementation CZAbstractReportManager

+ (NSDictionary *)reportTemplateFromFile:(NSString *)filePath {
    id templateData = nil;
    
    if (filePath) {
        NSStringEncoding encoding;
        NSError *err = nil;
        NSString *string = [[NSString alloc] initWithContentsOfFile:filePath usedEncoding:&encoding error:&err];
        
        if (string && err == nil) {
            string = [self translateTemplateString:string];
            
            SBJsonParser *json_parser = [[SBJsonParser alloc] init];
            templateData = [json_parser objectWithString:string];
            
            if (![templateData isKindOfClass:[NSDictionary class]]) {
                templateData = nil;
            }
        }
    }
    if (templateData == nil) {
        return [NSDictionary dictionary];
    } else {
        return templateData;
    }
}

- (id)init {
    self = [super init];
    if (self) {
        _tempImagePaths = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc {
    [CZAbstractReportManager deleteTemporaryDirectory];
}

+ (NSString *)translateTemplateKey:(NSString *)key {
    return L([key uppercaseString]);
};

+ (NSString *)translateTemplateString:(NSString *)templateString {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"%[^%]*%"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    BOOL matched = YES;
    NSUInteger position = 0;
    do {
        NSTextCheckingResult *match = [regex firstMatchInString:templateString
                                                        options:0
                                                          range:NSMakeRange(position, [templateString length] - position)];
        if (match && [match numberOfRanges] > 0) {
            matched = YES;
            NSRange matchRange = [match rangeAtIndex:0];
            NSString *matchString = [templateString substringWithRange:matchRange];
            matchString = [matchString substringWithRange:NSMakeRange(1, matchString.length - 2)];

            NSString *translated = [self translateTemplateKey:matchString];
            templateString = [templateString stringByReplacingCharactersInRange:matchRange withString:translated];
            position = matchRange.location + translated.length;
            
            if (position >= [templateString length]) {
                matched = NO;
            }
        } else {
            matched = NO;
        }
    } while (matched);
    
    return templateString;
}

- (NSDictionary *)reportTemplate {
    return [[self class] reportTemplateFromFile:_templateFilePath];
}

- (void)loadCachedUserInputData {
    NSString *templateFileName = [self.templateFilePath lastPathComponent];
    NSString *templateCacheFileName = [[templateFileName stringByDeletingPathExtension] stringByAppendingString:@".cache.plist"];
    
    // load user input data
    NSString *cachedUserInputDataFile = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    cachedUserInputDataFile = [cachedUserInputDataFile stringByAppendingPathComponent:templateCacheFileName];
    NSDictionary *tempDict = [NSDictionary dictionaryWithContentsOfFile:cachedUserInputDataFile];
    NSMutableDictionary *persistanceValues = [NSMutableDictionary dictionary];
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    // remove empty value
    for (id<NSCopying> key in tempDict.allKeys) {
        id value = [tempDict objectForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            NSString *string = (NSString *)value;
            if ([string hasPrefix:@"%DOCUMENTS%"]) {
                string = [string substringFromIndex:11];
                string = [docPath stringByAppendingPathComponent:string];
            }
            if (string.length) {
                [persistanceValues setObject:string forKey:key];
            }
        } else {
            [persistanceValues setObject:value forKey:key];
        }
    }
    
    self.userInputData = persistanceValues;
}

- (void)saveCachedUserInputData {
    NSString *cachedUserInputDataFile = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *templateCacheFileName = [[[self.templateFilePath lastPathComponent] stringByDeletingPathExtension] stringByAppendingString:@".cache.plist"];
    cachedUserInputDataFile = [cachedUserInputDataFile stringByAppendingPathComponent:templateCacheFileName];
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSMutableDictionary *convertedUserInputData = [self.userInputData mutableCopy];
    for (id<NSCopying> key in convertedUserInputData.allKeys) {
        NSString *string = convertedUserInputData[key];
        if ([string isKindOfClass:[NSString class]]) {
            if ([string hasPrefix:docPath]) {
                string = [string substringFromIndex:docPath.length];
                string = [@"%DOCUMENTS%" stringByAppendingPathComponent:string];
                convertedUserInputData[key] = string;
            }
        }
    }
    
    [convertedUserInputData writeToFile:cachedUserInputDataFile atomically:YES];
}

- (void)reloadReportFields {
    NSDictionary *reportTemplate = [self reportTemplate];
    self.templateFileMD5 = [CZCommonUtils md5FromContentOfFile:self.templateFilePath];
    
    // scan template and generate report element array
    NSMutableDictionary *textFields = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *imageFields = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *chartFields = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *tableFields = [[NSMutableDictionary alloc] init];
    
    CZReportEngine *engine = [[CZReportEngine alloc] initEngine:nil reportTemplate:reportTemplate];
    for (NSDictionary *element in [engine scanTemplate]) {
        CZReportField *reportField = [[CZReportField alloc] initWithElementDict:element];
        CZLogv(@"report Field: %@", reportField);
        
        switch (reportField.type) {
            case CZReportFieldTypeText:
                if (textFields[reportField.key] == nil) {
                    textFields[reportField.key] = reportField;
                }
                break;
            case CZReportFieldTypeImage:
                if (imageFields[reportField.key] == nil) {
                    imageFields[reportField.key] = reportField;
                }
                break;
            case CZReportFieldTypeChart:
                if (chartFields[reportField.key] == nil) {
                    chartFields[reportField.key] = reportField;
                }
                break;
            case CZReportFieldTypeTable:
                if (tableFields[reportField.key] == nil) {
                    tableFields[reportField.key] = reportField;
                }
                break;
        }
    }
    
    // scan meta data
    id metadata = reportTemplate[@"reportTemplateMetadata"];
    if ([metadata isKindOfClass:[NSDictionary class]]) {
        for (id key in ((NSDictionary *)metadata).allKeys) {
            id value = ((NSDictionary *)metadata)[key];
            
            if ([value isKindOfClass:[NSString class]]) {
                NSString *stringValue = (NSString *)value;
                
                if ([stringValue hasPrefix:@"$"] && [stringValue hasSuffix:@"$"]) {
                    CZReportField *reportField = [[CZReportField alloc] initWithKey:stringValue type:CZReportFieldTypeText];
                    CZLogv(@"report Field: %@", reportField);
                    
                    if (textFields[reportField.key] == nil) {
                        textFields[reportField.key] = reportField;
                    }
                }
            }
        }
        
        id mergeMeasurements = metadata[@"mergeMeasurements"];
        if ([mergeMeasurements isKindOfClass:[NSNumber class]]) {
            self.perGroupMeasurements = ![((NSNumber *)mergeMeasurements) boolValue];
        } else {
            self.perGroupMeasurements = NO;
        }
    }
    
    self.textFields = textFields.allValues;
    self.imageFields = imageFields.allValues;
    self.tableFields = tableFields.allValues;
    self.chartFields = chartFields.allValues;
}

- (NSUInteger)findFirstMandatoryMissingData {
    NSUInteger index = 0;
    for (CZReportField *reportField in self.textFields) {
        if (reportField.mandatory) {
            NSString *value = self.userInputData[reportField.key];
            if ([value length] == 0) {
                return index;
            }
        }
        
        ++index;
    }
    
    return NSNotFound;
}

- (void)checkAdditionalFiles {
    // sub-class override
}

- (BOOL)fillReportData:(NSError **)error {
    // sub-class override
    return YES;
}

- (void)generateReportImageIfNeed {
    // sub-class override
}

- (NSString *)uniqueReportFilePathOfExtension:(NSString *)extension {
    // sub-class override
    return nil;
}

- (BOOL)requireCollectMeasurementTable {
    for (CZReportField *field in self.tableFields) {
        if (![field.key isEqualToString:kCZParticleTableKey]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)requireCollectMeasurementChart {
    for (CZReportField *field in self.chartFields) {
        if (![field.key isEqualToString:kCZMultiphaseChartKey]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)requireCollectMultiphaseChart {
    for (CZReportField *field in self.chartFields) {
        if ([field.key isEqualToString:kCZMultiphaseChartKey]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)requireCollectParticleTable {
    for (CZReportField *field in self.tableFields) {
        if ([field.key isEqualToString:kCZParticleTableKey]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Private methods

+ (void)deleteTemporaryDirectory {
    NSString *directory = [NSTemporaryDirectory() stringByAppendingPathComponent:kReportTemparoyDirectory];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSError *err = nil;
    BOOL result = [fm removeItemAtPath:directory error:&err];
    if (!result && err) {
        CZLogv(@"Failed to delete report temporary directory: %@", err);
    }
}

@end

@implementation CZAbstractReportManager (ForSubclassEyesOnly)

- (void)appendReportImage:(NSString *)filePath {
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[filePath pathExtension]];
    if (fileFormat == kCZFileFormatCZI) {  // use temp png file, if it's a CZI file
        NSString *fileName = [filePath lastPathComponent];
        fileName = [[fileName stringByDeletingPathExtension] stringByAppendingPathExtension:@"png"];
        NSString *tempFilePath = [[self temporaryFilePath] stringByAppendingPathComponent:fileName];
        
        _tempImagePaths[filePath] = tempFilePath;
    } else {
        _tempImagePaths[filePath] = filePath;
    }
}

- (void)createTemporaryDirectory {
    NSString *temporaryFilePath = [self temporaryFilePath];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:temporaryFilePath]) {
        [fm createDirectoryAtPath:temporaryFilePath
      withIntermediateDirectories:YES
                       attributes:nil
                            error:NULL];
    }
}

- (NSString *)temporaryFilePath {
    NSAssert(self.templateFileMD5, @"templateFileMD5 shall not be empty");
    
    NSString *temporaryFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:kReportTemparoyDirectory];
    temporaryFilePath = [temporaryFilePath stringByAppendingPathComponent:self.templateFileMD5];
    return temporaryFilePath;
}

- (BOOL)requireCollectImage {
    for (CZReportField *reportField in self.imageFields) {
        if ([reportField.key isEqualToString:kCZReserveReportImageKey]) {
            return YES;
        }
    }
    
    return NO;
}

@end
