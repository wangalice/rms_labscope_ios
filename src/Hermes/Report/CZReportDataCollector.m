//
//  CZReportDataCollector.m
//  Matscope
//
//  Created by Ralph Jin on 6/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZReportDataCollector.h"

#import <ReportFramework/CZGlobal.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZAnnotationKit/CZElement.h>

@interface CZReportParticleTableCollector ()

@property (nonatomic, strong, readonly) NSMutableArray *particlesTableData;

@end

@implementation CZReportParticleTableCollector
@synthesize particlesTableData = _particlesTableData;

- (NSMutableArray *)particlesTableData {
    if (_particlesTableData == nil) {
        _particlesTableData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _particlesTableData;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    if (!docManager.multiphase.isParticlesMode) {
        return;
    }
    
    NSArray *groupTableData = [self collectionFromDocManager:docManager docID:docID];
    if (groupTableData) {
        [self.particlesTableData addObject:@{kValue: groupTableData}];
    }
}

- (NSDictionary *)collectedData {
    if (self.particlesTableData.count > 0) {
        return @{kVersion: @1, kValues: self.particlesTableData};
    } else {
        return nil;
    }
}

- (NSArray *)collectionFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    NSString *unitName1d = L([docManager.elementLayer unitNameOfDistanceByStyle:unitStyle]);
    NSString *unitName2d = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
    double scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    NSUInteger precision = [docManager.elementLayer precision];
    
    NSMutableArray *group = [NSMutableArray array];
    
    CZParticles2DItem *particles = [[CZParticles2DItem alloc] initWithAnalysisResult:docManager.analyzer2dResult];
    particles.criteria = docManager.multiphase.particleCriteria;
    
    id unitRow = @{@"Id": @"",
                   @"Diameter": unitName1d,
                   @"Perimeter": unitName1d,
                   @"Area":  unitName2d,
                   @"FeretMaximum": unitName1d,
                   @"FeretMinimum": unitName1d};
    [group addObject:unitRow];

    for (CZParticle2DItem *particle in particles.visibleItems) {
        @autoreleasepool {
            NSDictionary *tableItem = @{@"Id": [NSString stringWithFormat:@"%lu-%@", (unsigned long)docID + 1, particle.index],
                                        @"Diameter": @(scaling * [particle.diameter floatValue]),
                                        @"Diameter.precision": @(precision),
                                        @"Perimeter": @(scaling * [particle.perimeter floatValue]),
                                        @"Perimeter.precision": @(precision),
                                        @"Area":  @(scaling * scaling * [particle.area floatValue]),
                                        @"Area.precision": @(precision),
                                        @"FeretMaximum": @(scaling * [particle.maxFeret floatValue]),
                                        @"FeretMaximum.precision": @(precision),
                                        @"FeretMinimum": @(scaling * [particle.minFeret floatValue]),
                                        @"FeretMinimum.precision": @(precision),
                                        @"FeretRatio": particle.feretRatio,
                                        @"FeretRatio.precision": @(kCZFeretRatioPrecision)
                                        };
            [group addObject:tableItem];
        }
    }
    return group;
}

@end

@interface CZReportMergedParticleTableCollector () {
    BOOL _sortAscending;
    BOOL _collectionFinished;
}

@property (nonatomic, copy) NSString *sortKey;
@property (nonatomic, retain, readonly) NSMutableArray *particlesTableData;
@property (nonatomic, retain) NSDictionary *unitRow;

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID;

@end

@implementation CZReportMergedParticleTableCollector

@synthesize particlesTableData = _particlesTableData;

- (void)dealloc {
    
}

- (NSMutableArray *)particlesTableData {
    if (_particlesTableData == nil) {
        _particlesTableData = [[NSMutableArray alloc] initWithCapacity:16];
    }
    
    return _particlesTableData;
}

- (void)collectFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    if (!docManager.multiphase.isParticlesMode) {
        return;
    }
    
    if (docID == 0) {
        self.sortKey = docManager.multiphase.particleCriteria.sortKey;
        _sortAscending = docManager.multiphase.particleCriteria.sortAscending;
    }
    
    if (self.unitRow == nil) {
        CZElementUnitStyle unitStyle = [CZElement unitStyle];
        NSString *unitName1d = L([docManager.elementLayer unitNameOfDistanceByStyle:unitStyle]);
        NSString *unitName2d = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
        self.unitRow = @{@"Id": @"",
                    @"Diameter": unitName1d,
                    @"Perimeter": unitName1d,
                    @"Area":  unitName2d,
                    @"FeretMaximum": unitName1d,
                    @"FeretMinimum": unitName1d};
    }
    
    NSArray *groupTableData = [self collectionFromDocManager:docManager docID:docID];
    if (groupTableData) {
        [self.particlesTableData addObjectsFromArray:groupTableData];
    }
}

- (NSDictionary *)collectedData {
    [self finishCollecting];
    
    if (self.particlesTableData.count > 0) {
        return @{kVersion: @1, kValues: self.particlesTableData};
    } else {
        return nil;
    }
}

- (NSArray *)collectionFromDocManager:(CZDocManager *)docManager docID:(NSUInteger)docID {
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    double scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    NSUInteger precision = [docManager.elementLayer precision];
    
    NSMutableArray *group = [NSMutableArray array];
    
    CZParticles2DItem *particles = [[CZParticles2DItem alloc] initWithAnalysisResult:docManager.analyzer2dResult];
    particles.criteria = docManager.multiphase.particleCriteria;
    [particles sortByKey:nil ascending:NO];
    
    for (CZParticle2DItem *particle in particles.visibleItems) {
        NSDictionary *tableItem = @{@"Id" : [NSString stringWithFormat:@"%lu-%@", (unsigned long)docID + 1, particle.index],
                                    @"Diameter" : @(scaling * [particle.diameter floatValue]),
                                    @"Diameter.precision" : @(precision),
                                    @"Perimeter" : @(scaling * [particle.perimeter floatValue]),
                                    @"Perimeter.precision" : @(precision),
                                    @"Area":  @(scaling * scaling * [particle.area floatValue]),
                                    @"Area.precision" : @(precision),
                                    @"FeretMaximum" : @(scaling * [particle.maxFeret floatValue]),
                                    @"FeretMaximum.precision" : @(precision),
                                    @"FeretMinimum" :  @(scaling * [particle.minFeret floatValue]),
                                    @"FeretMinimum.precision" : @(precision),
                                    @"FeretRatio": particle.feretRatio,
                                    @"FeretRatio.precision": @(kCZFeretRatioPrecision)
                                    };
        [group addObject:tableItem];
    }
    
    return group;
}

- (void)finishCollecting {
    if (_collectionFinished) {
        return;
    }
    
    if (_sortKey) {
        if ([_sortKey isEqualToString:kCZParticle2DKeyArea]) {  //kCZParticle2DKeyID
            if (!_sortAscending) {
                //_particlesTableData reverse order
                if (_particlesTableData.count > 1) {
                    for (NSUInteger i = 0, j = _particlesTableData.count - 1; i < j; ++i, --j) {
                        [_particlesTableData exchangeObjectAtIndex:i withObjectAtIndex:j];
                    }
                }
            }
        } else {
            NSString *translatedSortKey = nil;
            if ([_sortKey isEqualToString:kCZParticle2DKeyArea]) {
                translatedSortKey = @"Area";
            } else if ([_sortKey isEqualToString:kCZParticle2DKeyPerimeter]) {
                translatedSortKey = @"Perimeter";
            } else if ([_sortKey isEqualToString:kCZParticle2DKeyDiameter]) {
                translatedSortKey = @"Diameter";
            } else if ([_sortKey isEqualToString:kCZParticle2DKeyMinFeret]) {
                translatedSortKey = @"FeretMinimum";
            } else if ([_sortKey isEqualToString:kCZParticle2DKeyMaxFeret]) {
                translatedSortKey = @"FeretMaximum";
            } else if ([_sortKey isEqualToString:kCZParticle2DKeyFeretRatio]) {
                translatedSortKey = @"FeretRatio";
            }
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:translatedSortKey ascending:_sortAscending];
            [_particlesTableData sortUsingDescriptors:@[sortDescriptor]];
        }
    }
    
    NSMutableArray * tempData = [NSMutableArray arrayWithArray:_particlesTableData];
    if (self.unitRow) {
        [tempData insertObject:self.unitRow atIndex:0];
    }
    
    if (tempData == nil) {
        tempData = [[NSMutableArray alloc] init];
    }
    _particlesTableData = [[NSMutableArray alloc] initWithCapacity:1];
    [_particlesTableData addObject: @{kValue: tempData}];
    _collectionFinished = YES;
}

@end
