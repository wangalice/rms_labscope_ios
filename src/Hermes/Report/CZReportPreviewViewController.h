//
//  CZReportPreviewViewController.h
//  Hermes
//
//  Created by Ralph Jin on 4/3/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZAbstractReportManager;

@protocol CZReportPreviewViewControllerDelegate;

@interface CZReportPreviewViewController : UIViewController

@property (nonatomic, strong) CZAbstractReportManager *reportManager;

- (BOOL)generateReport;

@end
