//
//  CZReportMultipleImagesViewController.m
//  Matscope
//
//  Created by Sherry Xu on 1/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <ImageIO/ImageIO.h>
#import "CZReportMultipleImagesViewController.h"
#import "CZReportMultipleImagesCell.h"
#import "CZFilesPickerController.h"
#import "CZPopoverController.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import <CZIKit/CZIKit.h>
#import "CZReportTemplateFilesManager.h"
#import "CZAlertController.h"
#import "CZFileEntity+Icon.h"

static NSString * const kCellID = @"CellID";
static const CGFloat kImageItemWidth = 75;
static const int kMaxImagesNumber = 50;

@interface CZReportMultipleImagesViewController () <UIPopoverPresentationControllerDelegate, CZFilesPickerControllerDelegate, CZReportMultipleImagesCellDelegate>

@property (nonatomic, strong) NSMutableArray *reportFilesPathArray;  // Total image file paths
@property (nonatomic, strong) NSMutableDictionary *thumbnailDictionary;
@property (nonatomic, strong) NSArray *addedImageFilePaths;
@property (nonatomic, strong) UIImage *originalImage;
@property (nonatomic, weak) CZDocManager *docManager;
@property (nonatomic, strong) CZFilesPickerController *multipleFilesPickerController;

@end

@implementation CZReportMultipleImagesViewController

- (instancetype)init {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(kCellWidth, kCellHeight)];
    layout.minimumInteritemSpacing = kDefaultPadding;
    layout.minimumLineSpacing = kDefaultPadding;
    
    self = [super initWithCollectionViewLayout:layout];

    if (self) {
        _thumbnailDictionary = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (instancetype)initWithDocManager:(CZDocManager *)docManager addedImageFilePaths:(NSArray <NSString *> *)imageFilePaths {
    self = [self init];
    if (self) {
        _docManager = docManager;
        _addedImageFilePaths = [imageFilePaths copy];
        _reportFilesPathArray = [NSMutableArray array];
    }
    return self;
}

- (void)dealloc {
    _docManager = nil;
    _addedImageFilePaths = nil;
    [_reportFilesPathArray removeAllObjects];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [UIView setAnimationsEnabled:NO];
    [self performBatchUpdatesWithoutAnimation:^{
        [self.collectionView reloadData];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [_reportFilesPathArray removeAllObjects];
    
    for (NSString *filePath in _addedImageFilePaths) {
        [_reportFilesPathArray addObject:filePath];
        UIImage *thumbnail = [CZReportTemplateFilesManager loadThumbnailFromFile:filePath minSize:kImageItemWidth * [[UIScreen mainScreen] scale]];
        if (thumbnail) {
            _thumbnailDictionary[filePath] = thumbnail;
        } else {
            CZLogv(@"Error, failed to read thumbnail of %@", filePath);
        }
    }
    
    UICollectionView *collectionView = self.collectionView;
    [collectionView registerClass:[CZReportMultipleImagesCell class] forCellWithReuseIdentifier:kCellID];
    collectionView.backgroundColor = [UIColor clearColor];
}

#pragma mark - Public method

- (NSArray *)selectedImagePaths {
    return [NSArray arrayWithArray:self.reportFilesPathArray];
}

- (void)popoverControllerUpdatePopoverPosition:(UIView *)view {
    NSUInteger lastIndex = [_reportFilesPathArray count] + 1;
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:lastIndex inSection:0]];
    
    CGRect frame = [view convertRect:cell.frame fromView:nil];
    
    self.multipleFilesPickerController.popoverPresentationController.sourceView = view;
    self.multipleFilesPickerController.popoverPresentationController.sourceRect = frame;
}

- (NSUInteger)cellCount {
    return [self.selectedImagePaths count] + 1;
}

#pragma mark - Private methods

- (void)performBatchUpdatesWithoutAnimation:(void (^)(void))updates {
    [UIView setAnimationsEnabled:NO];
    [self.collectionView performBatchUpdates:updates completion:^(BOOL finished) {
        [UIView setAnimationsEnabled:YES];
    }];
}

- (void)notifyImageCountChanged {
    if ([self.delegate respondsToSelector:@selector(multipleImagesViewControllerDidChanged:)]) {
        [self.delegate multipleImagesViewControllerDidChanged:self];
    }
}

- (void)updateCollectionsWithSelectedFiles:(NSArray *)selectedFiles maxAddImagesNumber:(NSUInteger)maxAddImagesNumber {
    NSMutableArray *newFiles = [NSMutableArray arrayWithArray:selectedFiles];
    if (newFiles.count <= 0) {
        return;
    }
    
    NSRange updateRange;
    
    updateRange = NSMakeRange(self.reportFilesPathArray.count, newFiles.count);
    if (updateRange.location + updateRange.length > maxAddImagesNumber) {
        updateRange.length = maxAddImagesNumber - updateRange.location;
    }
    
    NSUInteger usedCount = 0;
    for (CZFileEntity *fileEntity in newFiles) {
        if (self.reportFilesPathArray.count >= maxAddImagesNumber) {
            break;
        }
        [self.reportFilesPathArray addObject:fileEntity.filePath];
        usedCount++;
    }
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:newFiles.count];
    for (NSUInteger i = 0 ; i < updateRange.length; ++i) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(i + updateRange.location) inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
    
    [UIView setAnimationsEnabled:NO];
    if (self.reportFilesPathArray.count == maxAddImagesNumber) {
        [self performBatchUpdatesWithoutAnimation:^{
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:maxAddImagesNumber inSection:0]]];
        }];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        static const NSUInteger kBatchCount = 21;
        NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:kBatchCount];
        
        for (NSUInteger i = 0 ; i < updateRange.length; ++i) {
            if ((i + updateRange.location) >= _reportFilesPathArray.count) {
                break;
            }
            
            NSString *filePath = _reportFilesPathArray[i + updateRange.location];
            UIImage *thumbnail = [CZReportTemplateFilesManager loadThumbnailFromFile:filePath minSize:kImageItemWidth * [[UIScreen mainScreen] scale]];
            if (thumbnail) {
                _thumbnailDictionary[filePath] = thumbnail;
            } else {
                CZLogv(@"Error, failed to read thumbnail of %@", filePath);
            }
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i + updateRange.location + 1 inSection:0];
            [indexPaths addObject:indexPath];
            
            if (indexPaths.count >= kBatchCount) {
                dispatch_async(dispatch_get_main_queue(), ^ {
                    [UIView setAnimationsEnabled:NO];
                    [self performBatchUpdatesWithoutAnimation:^{
                        [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                    }];
                });
                indexPaths = [NSMutableArray arrayWithCapacity:kBatchCount];
            }
        }
        
        if (indexPaths.count) {
            dispatch_async(dispatch_get_main_queue(), ^ {
                [UIView setAnimationsEnabled:NO];
                [self performBatchUpdatesWithoutAnimation:^{
                    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                }];
            });
        }
    });
    
    [self notifyImageCountChanged];
    
    if ([self.delegate respondsToSelector:@selector(multipleImagesViewControllerScrollToOriginal:)]) {
        [self.delegate multipleImagesViewControllerScrollToOriginal:self];
    }
}

#pragma mark - Action

- (void)addImageActionWithButton:(CZButton *)button {
    if (([_reportFilesPathArray count] < kMaxImagesNumber)){
        CZFilesPickerController *multipleImagePickerController = [[CZFilesPickerController alloc] init];
        multipleImagePickerController.delegate = self;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:multipleImagePickerController animated:YES completion:nil];
        
        self.multipleFilesPickerController = multipleImagePickerController;
        if (self.multipleFilesPickerController) {
            if ([self.delegate respondsToSelector:@selector(multipleImagesViewControllerScrollToVisible:)]) {
                [self.delegate multipleImagesViewControllerScrollToVisible:self];
            }
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_reportFilesPathArray count] + 1; // one is the original image
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZReportMultipleImagesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
    NSUInteger row = indexPath.row;
    cell.delegate = self;
    
    if (row == 0) {
        if (self.originalImage == nil) {
            const CGFloat screenScale = [[UIScreen mainScreen] scale];
            CGRect size = CGRectMake(0, 0, kImageItemWidth * screenScale, kImageItemWidth * screenScale);
            UIImage *burnInImage = [self.docManager imageWithBurninAnnotations];
            self.originalImage = burnInImage ? [CZCommonUtils scaleImage:burnInImage intoSize:size.size] : [self.docManager.filePath.icon image];
        }
        cell.thumbnailImageView.image = self.originalImage;
        cell.shouldDeleteButtonHidden = YES;
    } else {
        NSUInteger index = row - 1;
        NSString *filePath = _reportFilesPathArray[index];
        UIImage *image = nil;
        if (filePath) {
            image = _thumbnailDictionary[filePath];
        }
        cell.thumbnailImageView.image = _thumbnailDictionary[filePath] ? : [filePath.icon image];
        cell.shouldDeleteButtonHidden = NO;
    }
    
    return cell;
}

#pragma mark - CZReportMultipleImagesCellDelegate

- (void)reportMultipleImagesCellWillDelete:(CZReportMultipleImagesCell *)cell {
    BOOL refreshLastCell = self.reportFilesPathArray.count == kMaxImagesNumber;
    
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    
    NSUInteger index = indexPath.row - 1;
    [self.reportFilesPathArray removeObjectAtIndex:index];
    
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    
    [UIView setAnimationsEnabled:NO];
    if (refreshLastCell) {
        [self performBatchUpdatesWithoutAnimation:^{
            [self.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.reportFilesPathArray.count + 1 inSection:0]]];
        }];
    }
    
    [self notifyImageCountChanged];
}

#pragma mark - UIPopoverPresentationControllerDelegate methods

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    if (self.multipleFilesPickerController == popoverPresentationController.presentedViewController) {
        self.multipleFilesPickerController = nil;
        if (!self.multipleFilesPickerController) {
            if ([self.delegate respondsToSelector:@selector(multipleImagesViewControllerScrollToOriginal:)]) {
                [self.delegate multipleImagesViewControllerScrollToOriginal:self];
            }
        }
    }
}

#pragma mark - CZFilesPickerControllerDelegate

- (void)filesPickerController:(CZFilesPickerController *)filesPickerController
                     didSelectFiles:(NSArray *)selectedFiles {
    // do not ignore the main doc manage as one of the report file count;
    NSUInteger maxAddImagesNumber = kMaxImagesNumber - 1;
    if (self.reportFilesPathArray.count + selectedFiles.count > maxAddImagesNumber) {
        @weakify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"REPORT_INFO_SELECTION_OVER_MAX_COUNT") level:CZAlertLevelWarning];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(__kindof CZDialogController *dialog, CZDialogAction *action) {
                @strongify(self);
                [self updateCollectionsWithSelectedFiles:selectedFiles maxAddImagesNumber:maxAddImagesNumber];
            }];
            [alert presentAnimated:YES completion:nil];
        });
    } else {
        [self updateCollectionsWithSelectedFiles:selectedFiles maxAddImagesNumber:maxAddImagesNumber];
    }
}

@end
