//
//  CZReportEditViewController.m
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReportEditViewController.h"
#import <ImageIO/ImageIO.h>
#import <ReportFramework/CZGlobal.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZReportField.h"
#import "CZReportManager.h"
#import "CZReportPreviewViewController.h"
#import "CZButtonGroup.h"
#import "CZFilesPickerController.h"
#import <CZFileKit/CZFileKit.h>
#import "CZReportMultipleImagesViewController.h"
#import "CZToastManager.h"
#import "CZReportEditMultipleImageView.h"
#import "CZReportEditImageView.h"
#import "CZReportEditTextView.h"
#import "CZReportEditRadioButtonView.h"
#import "CZPopoverController.h"
#import "CZAlertController.h"
#import "CZContentChooserController.h"
#import "CZKeyboardExtensionView.h"

typedef NS_ENUM(NSInteger, kBarButtonItemIndex) {
    kBarButtonItemPDF = 1,
    kBarButtonItemRTF
};

static const int kImageSingleRowNumber = 5;
static const CGFloat kVisibleSize = 704;
static const CGFloat kImageThumbnailSize = 96;
static const CGFloat kGroupRowHeight = 64;
static const CGFloat kContentViewMargin = 32.0;
static const CGFloat kTextViewhHeight = 48.0;
static const CGFloat kControlVerticalMagin = 16.0;
static const CGFloat kDefaultButtonHeight = 32.0;

@interface CZReportEditViewController () <
CZReportEditImageViewDelegate,
CZTextFieldDelegate,
CZReportEditRadioButtonViewDelegate,
CZReportMultipleImagesViewControllerDelegate,
CZContentChooserControllerDelegate,
CZReportEditMultiImageViewDelegate,
UIScrollViewDelegate
>
{
@private
    UITextField *_currentTextField;
    kBarButtonItemIndex _selectedBarButtonItemIndex;
}

@property (nonatomic, assign) NSUInteger rowCount;
@property (nonatomic, strong) NSArray *imageFields;
@property (nonatomic, strong) NSArray <CZReportField *> *textFields;
@property (nonatomic, strong) CZContentChooserController *reportSizeChooserController;
@property (nonatomic, strong) CZReportPreviewViewController *reportPreviewController;
@property (nonatomic, strong) CZReportMultipleImagesViewController *multipleImagesViewController;
@property (nonatomic, assign, readonly) BOOL userChooseImage;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, strong) CZReportEditMultipleImageView *multiImageView;
@property (nonatomic, strong) CZReportEditRadioButtonView *radioButtonView;
@property (nonatomic, strong) CZReportEditImageView *editImageView;
@property (nonatomic, strong) NSMutableArray <CZReportEditTextView *> *textViewArray;

@end

@implementation CZReportEditViewController

- (instancetype)init {
    if (self = [super init]) {
        _currentTextField = nil;
        _selectedBarButtonItemIndex = -1;
    }
    return self;
}

- (void)dealloc {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = L(@"REPORT_EDIT_VIEW_TITLE");
    
    CZBarButtonItem *pdfButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"REPORT_EDIT_VIEW_PDF_BUTTON")
                                                                     target:self
                                                                     action:@selector(generatePDFAction:)];
    pdfButtonItem.isAccessibilityElement = YES;
    pdfButtonItem.accessibilityIdentifier = @"PDFButton";
    pdfButtonItem.tag = kBarButtonItemPDF;

    CZBarButtonItem *rtfButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"REPORT_EDIT_VIEW_RTF_BUTTON")
                                                                     target:self
                                                                     action:@selector(generateRTFAction:)];
    rtfButtonItem.isAccessibilityElement = YES;
    rtfButtonItem.accessibilityIdentifier = @"RTFButton";
    rtfButtonItem.tag = kBarButtonItemRTF;
    
    if ([[CZDefaultSettings sharedInstance] enableRTFReportGeneration]) {
        self.navigationItem.rightBarButtonItems = @[rtfButtonItem, pdfButtonItem];
    } else {
        self.navigationItem.rightBarButtonItems = @[pdfButtonItem];
    }
    
    [self.reportManager reloadReportFields];
    self.reportManager.imageRenderMode = @"";
    
    // skip reserved text field
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (CZReportField *field in self.reportManager.textFields) {
        if (![field.key hasPrefix:@"_"]) {
            [tempArray addObject:field];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"key" ascending:YES];
    [tempArray sortUsingDescriptors:@[sortDescriptor]];
    
    self.textFields = [NSArray arrayWithArray:tempArray];
    
    // skip reserved image field
    tempArray = [[NSMutableArray alloc] init];
    for (CZReportField *field in self.reportManager.imageFields) {
        if (![field.key hasPrefix:@"_"]) {
            [tempArray addObject:field];
        }
    }
    self.imageFields = [NSArray arrayWithArray:tempArray];
    
    _userChooseImage = [self.reportManager isKindOfClass:[CZReportManager class]];
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.multiImageView];
    [self.scrollView addSubview:self.editImageView];
    [self.stackView addArrangedSubview:self.radioButtonView];
    [self.scrollView addSubview:self.stackView];


    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.scrollView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.scrollView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.scrollView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.scrollView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;

    self.multiImageView.titleLabel.text = L(@"REPORT_IMAGES");
    if ([self.reportManager isKindOfClass:[CZReportManager class]]) {
        CZReportManager *reportManager = (CZReportManager *)self.reportManager;
        [self createMultipleImagesViewWithDocManager:[reportManager mainDocManager] addedImageFilePaths:[reportManager additionalFiles]];
    }

    [self addChildViewController:self.multipleImagesViewController];
    self.multipleImagesViewController.view.frame = [self multipleImagesViewControllerFrame];
    self.multipleImagesViewController.collectionView.frame = self.multipleImagesViewController.view.bounds;
    [self.multiImageView.contentBackgroundView addSubview:self.multipleImagesViewController.view];
    [self.multipleImagesViewController didMoveToParentViewController:self];
    self.multiImageView.contentBackgroundView.clipsToBounds = YES;
    [self.multiImageView.contentBackgroundView bringSubviewToFront:self.multiImageView.addImageButton];

    [self.textViewArray removeAllObjects];
    for (CZReportField *field in self.textFields) {
        CZReportEditTextView *textView = [[CZReportEditTextView alloc] init];
        textView.titleLabel.text = field.key;

        NSString *value = _reportManager.userInputData[field.key];
        if (value == nil) {
            value = @"";
        }
        
        NSString *placeHolder = field.fieldDescription;
        if ([placeHolder length] == 0 && field.mandatory) {
            placeHolder = L(@"REPORT_MANDATORY_FIELD_PLACEHOLDER");
        }
        
        textView.textField.placeholder = placeHolder;
        textView.textField.text = value;
        textView.textField.returnKeyType = UIReturnKeyNext;

        NSInteger index = [self.textFields indexOfObject:field];
        if (index == (self.textFields.count - 1)) {
            textView.textField.returnKeyType = UIReturnKeyDone;
        } else {
            textView.textField.returnKeyType = UIReturnKeyNext;
        }
        textView.textField.tag = index;
        textView.textField.delegate = self;

        [textView.heightAnchor constraintEqualToConstant:48.0f].active = YES;
        [self.stackView addArrangedSubview:textView];
        [self.textViewArray addObject:textView];
    }

    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), [self contentViewHeight]);
}

- (void)viewWillLayoutSubviews {
    self.multiImageView.frame = CGRectMake(0, kContentViewMargin, CGRectGetWidth(self.scrollView.frame), [self multipleImageViewHeight]);
    self.stackView.frame = CGRectMake(0, CGRectGetMaxY(self.multiImageView.frame), CGRectGetWidth(self.scrollView.frame), [self textViewsHeight]);
    self.editImageView.frame = CGRectMake(0, CGRectGetMaxY(self.stackView.frame), CGRectGetWidth(self.scrollView.frame), [self reportImageViewHeight]);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self registerKeyboardNotification];

    [self updateMultiImageView];
    [self updateReportRadioButtonView];
    [self updateReportImageEditView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (@available(iOS 11.0, *)) {
        // do nothing
    } else {
        // Fix bug the PDF button and RTF button show "..."
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self unregisterKeyboardNotification];
}

#pragma mark - Private methods

- (void)createMultipleImagesViewWithDocManager:(CZDocManager *)docManager addedImageFilePaths:(NSArray *)imageFilePaths {
    if (_multipleImagesViewController == nil) {
        _multipleImagesViewController = [[CZReportMultipleImagesViewController alloc] initWithDocManager:docManager addedImageFilePaths:imageFilePaths];
        _multipleImagesViewController.delegate = self;
    }
}

- (void)registerKeyboardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)unregisterKeyboardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
}

- (void)generateReportWithType:(ReportEngineReportType)type dpi:(CGFloat)dpi imageCompressRatio:(CGFloat)imageCompressRatio {
    [_currentTextField resignFirstResponder];  // resign first responder, so that it will collection data from UI.
    
    // save what user input
    [self.reportManager saveCachedUserInputData];
    self.reportManager.additionalFiles = [self.multipleImagesViewController selectedImagePaths];
    
    // check mandatory fields
    NSUInteger index = [_reportManager findFirstMandatoryMissingData];
    if (index != NSNotFound && index < self.textViewArray.count) {
        CZReportEditTextView *textView = [self.textViewArray objectAtIndex:index];
        [self.scrollView setContentOffset:CGPointMake(0, CGRectGetMaxY(textView.frame)) animated:YES];
        [textView becomeFirstResponder];
        return;
    }
    
    [self.reportManager checkAdditionalFiles];
    
    if (self.reportManager.hasUnsupportedFiles) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"REPORT_UNSUPPORT_IMAGE_ERROR") level:CZAlertLevelWarning];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    if (self.reportManager.isMixedScaling) {
        [[CZToastManager sharedManager] showToastMessage:L(@"REPORT_MIXED_SCALING_MSG") sourceRect:CGRectOffset(self.view.bounds, 0, CGRectGetMidY(self.view.bounds))];
        return;
    }
    
    if (self.reportManager.isMixedRemainingMultiphase) {
        [[CZToastManager sharedManager] showToastMessage:L(@"REPORT_MIXED_MULTIPHASE_REMOVED_MSG") sourceRect:CGRectOffset(self.view.bounds, 0, CGRectGetMidY(self.view.bounds))];
        return;
    }
    
    // generate report
    CZReportPreviewViewController *controller = [[CZReportPreviewViewController alloc] init];
    controller.reportManager = self.reportManager;
    [self.navigationController pushViewController:controller animated:YES];
    self.reportManager.reportType = type;
    self.reportManager.imageDPI = dpi;
    self.reportManager.imageCompressRatio = imageCompressRatio;
    [controller generateReport];
}

- (CGRect)multipleImagesViewControllerFrame {
    NSUInteger cellCount = self.multipleImagesViewController ? [self.multipleImagesViewController cellCount] : 0;
    NSUInteger rowCount = (cellCount - 1) / kImageSingleRowNumber + 1;
    return CGRectMake(0, 0, (kCellWidth + kDefaultPadding) * kImageSingleRowNumber, (kCellHeight + kDefaultPadding) * rowCount);
}

- (CGFloat)contentViewHeight {
    CGFloat height = kContentViewMargin * 2 + [self multipleImageViewHeight] + [self textViewsHeight] + [self reportImageViewHeight];
    return height;
}

- (CGFloat)reportImageViewHeight {
    CZReportField *field = [self.imageFields firstObject];
    NSString *filePath = _reportManager.userInputData[field.key];
    
    return filePath ? kImageThumbnailSize + kControlVerticalMagin * 2 + kDefaultButtonHeight : kControlVerticalMagin + kDefaultButtonHeight;
}

- (CGFloat)textViewsHeight {
    CGFloat height = kTextViewhHeight * self.textFields.count;
    height += self.multipleImagesViewController.selectedImagePaths.count ? kGroupRowHeight + kControlVerticalMagin : 0;

    return height;
}

- (CGFloat)multipleImageViewHeight {
    return [self multipleImagesViewControllerFrame].size.height + kDefaultButtonHeight;
}

- (void)collectViewCellScrollToVisible {
    if (_rowCount == 1) {
        [self.multipleImagesViewController popoverControllerUpdatePopoverPosition:self.view];
    } else {
        NSUInteger scrollRowCount = _rowCount - 1;
        CGFloat offSet;
        offSet = scrollRowCount * kCellHeight - 18; // ios7  table view top margin add -64
        CGFloat scrollViewContentHeight = self.scrollView.contentSize.height;
        CGFloat inset = (offSet + kVisibleSize - scrollViewContentHeight > 0) ? offSet + kVisibleSize - scrollViewContentHeight : 0;
        UIEdgeInsets contentInset = UIEdgeInsetsMake(0, 0, inset, 0);

        self.scrollView.contentInset = contentInset;
        self.scrollView.scrollIndicatorInsets = contentInset;
        [self.scrollView scrollRectToVisible:CGRectMake(0, offSet + kVisibleSize - 1, 1, 1) animated:NO];
        [self.scrollView scrollRectToVisible:CGRectMake(0, offSet, 1, 1) animated:NO];
        [self.multipleImagesViewController popoverControllerUpdatePopoverPosition:self.view];
    }
}

- (void)updateReportImageEditView {
    CZReportField *field = [self.imageFields firstObject];

    UIImage *image = nil;
    
    NSString *filePath = _reportManager.userInputData[field.key];
    
    if(filePath) {
        image = [CZCommonUtils thumnailFromImageFilePath:filePath atMinSize:(kImageThumbnailSize * [[UIScreen mainScreen] scale])];
        if (image == nil) {
            [_reportManager.userInputData removeObjectForKey:field.key];
        }
    }
    self.editImageView.titleLabel.text = field.key;
    self.editImageView.reportImageView.image = image;
    self.editImageView.shouldDeleteButtonHidden = (image == nil);
    
    CGRect frame = self.editImageView.frame;
    frame.origin.y = CGRectGetMaxY(self.stackView.frame);
    frame.size.height = [self reportImageViewHeight];
    self.editImageView.frame = frame;
}

- (void)updateReportRadioButtonView {
    [UIView animateWithDuration:0.25 animations:^{
        CZReportEditRadioButtonView *radioButton = self.stackView.arrangedSubviews[0];
        radioButton.hidden = (self.multipleImagesViewController.selectedImagePaths.count == 0);
        
        CGRect frame = self.stackView.frame;
        frame.size.height = [self textViewsHeight];
        frame.origin.y = CGRectGetMaxY(self.multiImageView.frame);
        self.stackView.frame = frame;
        
        [self.stackView setNeedsLayout];
    }];
}

- (void)updateMultiImageView {
    CGRect multiImageEditViewFrame = self.multipleImagesViewController.view.frame;
    multiImageEditViewFrame.size.height = [self multipleImageViewHeight];
    self.multipleImagesViewController.view.frame = multiImageEditViewFrame;
    self.multipleImagesViewController.collectionView.frame = self.multipleImagesViewController.view.bounds;
    
    CGRect multiImageViewFrame = self.multiImageView.frame;
    multiImageViewFrame.size.height = [self multipleImageViewHeight];
    self.multiImageView.frame = multiImageViewFrame;
}

#pragma mark - Notification

- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect = [self.scrollView convertRect:kbRect fromView:nil];
    
    if (@available(iOS 11.0, *)) {
        //do nothing
    } else {
        UIEdgeInsets contenInset = UIEdgeInsetsMake(0, 0, kbRect.size.height, 0);
        self.scrollView.contentInset = contenInset;
        self.scrollView.scrollIndicatorInsets = contenInset;
    }
    
    if (_currentTextField) {
        CGRect rect = [self.scrollView convertRect:_currentTextField.bounds fromView:nil];
        [self.scrollView setContentOffset:CGPointMake(0, CGRectGetMaxY(rect)) animated:YES];
    }
}

- (void)keyboardDidHide:(NSNotification *)aNotification {
    self.scrollView.contentInset = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;

    if (_currentTextField) {
        CGRect rect = [self.scrollView convertRect:_currentTextField.bounds fromView:nil];
        [self.scrollView scrollRectToVisible:rect animated:YES];
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - Action

- (void)generateRTFAction:(CZBarButtonItem *)sender {
    _selectedBarButtonItemIndex = kBarButtonItemRTF;
    [self generateReportAction:sender];
}

- (void)generatePDFAction:(CZBarButtonItem *)sender {
    _selectedBarButtonItemIndex = kBarButtonItemPDF;
    [self generateReportAction:sender];
}

- (void)generateReportAction:(CZBarButtonItem *)sender {
    CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:self.reportSizeChooserController];
    [popover presentFromBarButtonItem:sender animated:YES completion:nil];
}

#pragma mark - CZContentChooserControllerDelegate

- (void)contentChooserController:(CZContentChooserController *)controller didSelectedItem:(NSInteger)item {
    if (self.reportSizeChooserController == controller) {
        @weakify(self);
        [controller dismissViewControllerAnimated:YES completion:^{
            @strongify(self);
            self.reportSizeChooserController.delegate = nil;
            self.reportSizeChooserController = nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                CGFloat dpi, compress;
                if (item == 0) {
                    dpi = 600.0f;
                    compress = 1.0f;
                } else if (item == 1) {
                    dpi = 200.0f;
                    compress = 0.88f;
                } else {
                    return;
                }
                
                ReportEngineReportType type;
                switch (_selectedBarButtonItemIndex) {
                    case kBarButtonItemRTF: {
                        type = ReportEngineReportTypeRTF;
                    }
                        break;
                    case kBarButtonItemPDF: {
                        type = ReportEngineReportTypePDF;
                    }
                        break;
                }
                
                [self generateReportWithType:type dpi:dpi imageCompressRatio:compress];

            });
        }];
    }
}

#pragma mark - CZReportEditMultiImageCellDelegate

- (void)multiImageViewDidSelectAddImageAction:(CZReportEditMultipleImageView *)multiImageView {
    [self.multipleImagesViewController addImageActionWithButton:multiImageView.addImageButton];
}

#pragma mark - CZReportEditRadioButtonCellDelegate

- (void)reportEditRadioButtonView:(CZReportBaseEditView *)CZReportBaseEditView didSelectIndex:(NSUInteger)index {
    if (index == 0) {
        self.reportManager.imageRenderMode = @"";
    } else if (index == 1) {
        self.reportManager.imageRenderMode = kImageRenderModeTile;
    } else if (index == 2) {
        self.reportManager.imageRenderMode = kImageRenderModeMultipage;
    } else {
        self.reportManager.imageRenderMode = @"";
    }
}

#pragma mark - CZReportEditImageCellDelegate

- (void)reportEditImageView:(CZReportEditImageView *)imageEditView didPickingImageFilePath:(NSString *)filePath {
    CZReportField *field = [_imageFields firstObject];
    if ([filePath length]) {
        _reportManager.userInputData[field.key] = filePath;
    } else {
        [_reportManager.userInputData removeObjectForKey:field.key];
    }
    
    [self updateReportImageEditView];
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), [self contentViewHeight]);
}

#pragma mark - CZReportMultipleImagesViewControllerDelegate

- (void)multipleImagesViewControllerDidChanged:(CZReportMultipleImagesViewController *)controller {
    [self updateMultiImageView];
    [self updateReportRadioButtonView];
    [self updateReportImageEditView];
    
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), [self contentViewHeight]);
}

- (void)multipleImagesViewControllerScrollToVisible:(CZReportMultipleImagesViewController *)controller {
    [self collectViewCellScrollToVisible];
}

- (void)multipleImagesViewControllerScrollToOriginal:(CZReportMultipleImagesViewController *)controller {
    UIEdgeInsets contentInset = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInset;
    self.scrollView.scrollIndicatorInsets = contentInset;
    CGRect rect = CGRectMake(0, 0, self.scrollView.contentSize.width, kVisibleSize);
    [self.scrollView scrollRectToVisible:rect animated:NO];
}

#pragma mark - Text field delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _currentTextField = textField;
    CZKeyboardExtensionView *extensionView = [[CZKeyboardExtensionView alloc] init];
    [extensionView setTextField:textField];
    [textField setInputAccessoryView:extensionView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSUInteger row = textField.tag;
    if (row >= 0 && row < ([_textFields count] - 1)) { // not the last text field
        CZReportEditTextView *textView = [self.textViewArray objectAtIndex:row + 1];
        CGRect rect = [self.scrollView convertRect:textView.bounds toView:nil];
        [self.scrollView setContentOffset:CGPointMake(0, CGRectGetMaxY(rect)) animated:YES];
        [textView.textField becomeFirstResponder];
    } else {
        [_currentTextField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [_currentTextField setInputAccessoryView:nil];
    _currentTextField = nil;
    
    NSInteger row = textField.tag;
    if (row < [_textFields count] && row >= 0) {
        CZReportEditTextView *textView = [_textViewArray objectAtIndex:row];
        CZReportField *field = [_textFields objectAtIndex:row];
        NSString *value = textField.text;
        _reportManager.userInputData[field.key] = value;
        if (field.mandatory && [value length] == 0) {
            if ([value length] == 0 && field.mandatory) {
                textView.titleLabel.textColor = [UIColor cz_sr110];
            } else {
                textView.titleLabel.textColor = [UIColor cz_gs80];
            }
        }
    
        NSString *placeHolder = field.fieldDescription;
        if ([placeHolder length] == 0 && field.mandatory) {
            placeHolder = L(@"REPORT_MANDATORY_FIELD_PLACEHOLDER");
        }
        textView.textField.placeholder = placeHolder;
    }
}

#pragma mark - Getter

- (CZContentChooserController *)reportSizeChooserController {
    if (_reportSizeChooserController == nil) {
        _reportSizeChooserController = [[CZContentChooserController alloc] initWithContent:@[L(@"REPORT_STANDARD_SIZE"),
                                                                                             L(@"REPORT_SMALL_SIZE")]
                                                                                     title:nil
                                                                      preferredContentSize:CGSizeMake(304.0f, 96.0f)];
        _reportSizeChooserController.delegate = self;
    }
    return _reportSizeChooserController;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = [UIColor cz_gs105];
        _scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    }
    
    return _scrollView;
}

- (UIStackView *)stackView {
    if (_stackView == nil) {
        _stackView = [[UIStackView alloc] init];
        _stackView.axis = UILayoutConstraintAxisVertical;
        _stackView.distribution = UIStackViewDistributionFill;
        _stackView.alignment = UIStackViewAlignmentFill;
    }
    
    return _stackView;
}

- (CZReportEditMultipleImageView *)multiImageView {
    if (_multiImageView == nil) {
        _multiImageView = [[CZReportEditMultipleImageView alloc] init];
        _multiImageView.delegate = self;
    }
 
    return _multiImageView;
}

- (CZReportEditRadioButtonView *)radioButtonView {
    if (_radioButtonView == nil) {
        _radioButtonView = [[CZReportEditRadioButtonView alloc] init];
        _radioButtonView.delegate = self;
        _radioButtonView.titleLabel.text = L(@"REPORT_IMAGE_LAYOUT");
        [_radioButtonView.heightAnchor constraintEqualToConstant:kGroupRowHeight + kControlVerticalMagin].active = YES;
    }

    return _radioButtonView;
}

- (CZReportEditImageView *)editImageView {
    if (_editImageView == nil) {
        _editImageView = [[CZReportEditImageView alloc] init];
        _editImageView.delegate = self;
    }
    
    return _editImageView;
}

- (NSMutableArray<CZReportEditTextView *> *)textViewArray {
    if (_textViewArray == nil) {
        _textViewArray = [NSMutableArray array];
    }
    
    return _textViewArray;
}

@end
