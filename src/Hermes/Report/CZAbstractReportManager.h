//
//  CZAbstractReportManager.h
//  Matscope
//
//  Created by Ralph Jin on 5/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReportFramework/CZTypedefine.h>

static NSString *const kCZParticleTableKey = @"particlesContent";
static NSString *const kCZMultiphaseChartKey = @"multiphase";
static NSString *const kModifiedMainDocKey = @"modifiedMainDocManager";

@class CZAbstractReportManager;

extern NSString *const kCZReserveReportImageKey;

@protocol CZReportManagerDelegate <NSObject>
@optional
- (void)reportManager:(CZAbstractReportManager *)reportManager prepareReportDataReachProgress:(float)progress;
@end

@interface CZAbstractReportManager : NSObject

+ (NSDictionary *)reportTemplateFromFile:(NSString *)filePath;
+ (NSString *)translateTemplateKey:(NSString *)key;

@property (nonatomic, weak) id<CZReportManagerDelegate> delegate;

@property (nonatomic, copy) NSString *templateFilePath;
@property (nonatomic, strong, readonly) NSMutableDictionary *tempImagePaths;
@property (nonatomic, strong) NSMutableDictionary *userInputData;
@property (nonatomic, strong) NSDictionary *reportData;

@property (nonatomic, copy) NSString *imageRenderMode;

@property (nonatomic, copy) NSArray *additionalFiles;

// data fields
@property (nonatomic, strong) NSArray *textFields;
@property (nonatomic, strong) NSArray *imageFields;
@property (nonatomic, strong) NSArray *chartFields;
@property (nonatomic, strong) NSArray *tableFields;

@property (nonatomic, assign) ReportEngineReportType reportType;
@property (nonatomic, assign) CGFloat imageDPI;
@property (nonatomic, assign) CGFloat imageCompressRatio;

/** It doesn't make sense to merge multiphase with remaining enabled and disabled.
 * Check this flag after invoke |checkAdditionalFiles|, see if there are illegal ones.
 */
@property (nonatomic, assign, getter = isMixedRemainingMultiphase) BOOL mixedRemainingMultiphase;
/** It is illegal to report unsupported image (image can't be opened in Hermes).
 * Check this flag after invoke |checkAdditionalFiles|, see if there are illegal ones.
 */
@property (nonatomic, assign) BOOL hasUnsupportedFiles;
/** It doesn't make sense to merge multiphase with scaling valid and invalid.
 * Check this flag after invoke |checkAdditionalFiles|, see if there are illegal ones.
 */
@property (nonatomic, assign, getter = isMixedScaling) BOOL mixedScaling;

@property (nonatomic, assign) BOOL perGroupMeasurements;

@property (nonatomic, strong, readonly) NSDictionary *reportTemplate;

- (void)loadCachedUserInputData;
- (void)saveCachedUserInputData;

- (void)reloadReportFields;

- (void)checkAdditionalFiles;

/** fill report data with main doc manager and additional image files
 * @return YES, if success, otherwise NO.
 */
- (BOOL)fillReportData:(NSError **)error;

- (NSUInteger)findFirstMandatoryMissingData;

- (void)generateReportImageIfNeed;

- (NSString *)uniqueReportFilePathOfExtension:(NSString *)extension;

- (BOOL)requireCollectMeasurementTable;
- (BOOL)requireCollectMultiphaseChart;
- (BOOL)requireCollectMeasurementChart;
- (BOOL)requireCollectParticleTable;

@end

@interface CZAbstractReportManager (ForSubclassEyesOnly)

- (void)appendReportImage:(NSString *)filePath;

- (void)createTemporaryDirectory;
- (NSString *)temporaryFilePath;

- (BOOL)requireCollectImage;

@end

