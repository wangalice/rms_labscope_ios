//
//  CZReportManager.h
//  Hermes
//
//  Created by Ralph Jin on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAbstractReportManager.h"

@class CZDocManager;

@interface CZReportManager : CZAbstractReportManager

@property (nonatomic, readonly, strong) CZDocManager *mainDocManager;

- (instancetype)initWithDocManager:(CZDocManager *)docManager;

@end
