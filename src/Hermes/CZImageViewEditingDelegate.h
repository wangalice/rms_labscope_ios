//
//  CZImageViewEditingDelegate.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/11.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZElement;

NS_ASSUME_NONNULL_BEGIN

@protocol CZImageViewEditingDelegate <NSObject>

@optional

// zooming
- (void)imageView:(UIView *)imageView willBeginZooming:(CGFloat)zoomOfView;
- (void)imageView:(UIView *)imageView isChangingZoom:(CGFloat)zoomOfView;
- (void)imageView:(UIView *)imageView didChangeToZoom:(CGFloat)zoomOfView;

// gesture
- (CZShouldBeginTouchState)imageView:(UIView *)imageView shouldBeginTouch:(UITouch *)touch;
- (BOOL)imageView:(UIView *)imageView handleTapGesture:(UIGestureRecognizer *)recognizer;
- (void)imageView:(UIView *)imageView handleTouchGesture:(UIGestureRecognizer *)recognizer;
- (BOOL)imageView:(UIView *)imageView handlePanGesture:(UIGestureRecognizer *)recognizer;
- (BOOL)imageView:(UIView *)imageView shouldHandleDoubleTapGesture:(UIGestureRecognizer *)recognizer;
- (BOOL)imageView:(UIView *)imageView handleDoubleTapGesture:(UIGestureRecognizer *)recognizer;
- (BOOL)imageView:(UIView *)imageView shouldTapGestureRequireOtherFail:(UIGestureRecognizer *)recognizer;
- (void)imageViewDocManagerModified:(UIView *)imageView;

// annotations
- (CZElement *)selectedElement;
- (CZElement *)temporarilyHiddenElement;

@end

NS_ASSUME_NONNULL_END
