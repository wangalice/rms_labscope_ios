//
//  CZChannel.h
//  Hermes
//
//  Created by Li, Junlin on 5/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZChannelColor.h"

@class CZChannel;

@protocol CZChannelObserver <NSObject>

@optional
- (void)channelNameDidChange:(CZChannel *)channel;
- (void)channelColorDidChange:(CZChannel *)channel;
- (void)channelEnabledDidChange:(CZChannel *)channel;
- (void)channelSelectedDidChange:(CZChannel *)channel;

@end

@interface CZChannel : NSObject <NSSecureCoding>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) CZChannelColor *color;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected NS_DESIGNATED_INITIALIZER;

- (BOOL)isNameEditable;

- (void)addObserver:(id<CZChannelObserver>)observer;
- (void)removeObserver:(id<CZChannelObserver>)observer;
- (void)sendActionToObservers:(SEL)action;

@end
