//
//  CZChannelColorPickerViewController.h
//  Hermes
//
//  Created by Li, Junlin on 3/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZChannel;

@interface CZChannelColorPickerViewController : UIViewController

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithChannel:(CZChannel *)channel NS_DESIGNATED_INITIALIZER;

@end
