//
//  CZVirtualLightSourceChannel.m
//  Hermes
//
//  Created by Li, Junlin on 5/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZVirtualLightSourceChannel.h"
#import "CZDefaultSettings+Multichannel.h"

@implementation CZVirtualLightSourceChannel

- (instancetype)initWithCamera:(CZCamera *)camera index:(NSUInteger)index {
    NSString *name = [[CZDefaultSettings sharedInstance] channelNameAtIndex:index forCamera:camera];
    CZChannelColor *color = [[CZDefaultSettings sharedInstance] channelColorAtIndex:index forCamera:camera];
    self = [super initWithName:name color:color enabled:index == 0 selected:index == 0 camera:camera];
    if (self) {
    }
    return self;
}

- (BOOL)isNameEditable {
    return YES;
}

@end
