//
//  CZChannelColorPickerViewController.m
//  Hermes
//
//  Created by Li, Junlin on 3/28/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelColorPickerViewController.h"
#import "CZChannel.h"
#import "CZGridView.h"
#import "CZUppercaseLabel.h"

static const NSInteger kCheckmarkViewTag = 101;

@interface CZChannelColorPickerViewController () <CZTextFieldDelegate>

@property (nonatomic, strong) CZChannel *channel;
@property (nonatomic, copy) NSArray<CZChannelColor *> *selectableColors;

@property (nonatomic, readonly, strong) UILabel *channelNameLabel;
@property (nonatomic, readonly, strong) CZTextField *channelNameTextField;
@property (nonatomic, readonly, strong) CZGridView *gridView;

@end

@implementation CZChannelColorPickerViewController

@synthesize channelNameLabel = _channelNameLabel;
@synthesize channelNameTextField = _channelNameTextField;
@synthesize gridView = _gridView;

- (instancetype)initWithChannel:(CZChannel *)channel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _channel = channel;
        self.preferredContentSize = CGSizeMake(608.0, 522.0);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectableColors = [CZChannelColor availableColors];
    
    if ([self.channel isNameEditable]) {
        [self.view addSubview:self.channelNameTextField];
        self.channelNameTextField.text = self.channel.name;
    } else {
        [self.view addSubview:self.channelNameLabel];
        self.channelNameLabel.text = self.channel.name;
    }
    
    [self.view addSubview:self.gridView];
    for (CZChannelColor *color in self.selectableColors) {
        UIView *colorCellView = [self makeColorCellViewWithColor:color];
        [self.gridView addContentView:colorCellView];
        
        UIImageView *checkmarkView = [colorCellView viewWithTag:kCheckmarkViewTag];
        checkmarkView.hidden = ![color isEqualToColor:self.channel.color];
    }
}

#pragma mark - Views

- (UILabel *)channelNameLabel {
    if (_channelNameLabel == nil) {
        _channelNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0, 26.0, self.view.bounds.size.width - 64.0, 22.0)];
        _channelNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _channelNameLabel.font = [UIFont cz_subtitle1];
        _channelNameLabel.textColor = [UIColor cz_gs10];
        _channelNameLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _channelNameLabel;
}

- (CZTextField *)channelNameTextField {
    if (_channelNameTextField == nil) {
        _channelNameTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _channelNameTextField.frame = CGRectMake(32.0, 26.0, 96.0, 22.0);
        _channelNameTextField.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        _channelNameTextField.delegate = self;
    }
    return _channelNameTextField;
}

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = [NSArray cz_arrayWithRepeatedObject:row count:9];
        
        CZGridColumn *column = [[CZGridColumn alloc] initWithWidth:256 alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = [NSArray cz_arrayWithRepeatedObject:column count:2];
        
        _gridView = [[CZGridView alloc] initWithFrame:UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(74.0, 32.0, 32.0, 32.0)) rows:rows columns:columns];
        _gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _gridView.axis = UILayoutConstraintAxisHorizontal;
        
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 32.0;
    }
    return _gridView;
}

- (UIView *)makeColorCellViewWithColor:(CZChannelColor *)color {
    UIView *colorCellView = [[UIView alloc] init];
    colorCellView.translatesAutoresizingMaskIntoConstraints = NO;
    colorCellView.backgroundColor = [UIColor clearColor];
    colorCellView.userInteractionEnabled = YES;
    colorCellView.tag = color.index;
    
    UIView *colorBlockView = [[UIView alloc] init];
    colorBlockView.translatesAutoresizingMaskIntoConstraints = NO;
    colorBlockView.backgroundColor = color.value;
    [colorCellView addSubview:colorBlockView];
    
    [colorBlockView.leadingAnchor constraintEqualToAnchor:colorCellView.leadingAnchor].active = YES;
    [colorBlockView.topAnchor constraintEqualToAnchor:colorCellView.topAnchor].active = YES;
    [colorBlockView.bottomAnchor constraintEqualToAnchor:colorCellView.bottomAnchor].active = YES;
    [colorBlockView.widthAnchor constraintEqualToConstant:96.0].active = YES;
    
    CZUppercaseLabel *colorNameLabel = [[CZUppercaseLabel alloc] init];
    colorNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    colorNameLabel.text = color.name;
    colorNameLabel.textAlignment = NSTextAlignmentLeft;
    [colorCellView addSubview:colorNameLabel];
    
    [colorNameLabel.leadingAnchor constraintEqualToAnchor:colorBlockView.trailingAnchor constant:16.0].active = YES;
    [colorNameLabel.trailingAnchor constraintEqualToAnchor:colorCellView.trailingAnchor].active = YES;
    [colorNameLabel.topAnchor constraintEqualToAnchor:colorCellView.topAnchor].active = YES;
    [colorNameLabel.bottomAnchor constraintEqualToAnchor:colorCellView.bottomAnchor].active = YES;
    
    UIImageView *checkmarkView = [[UIImageView alloc] init];
    checkmarkView.translatesAutoresizingMaskIntoConstraints = NO;
    checkmarkView.tag = kCheckmarkViewTag;
    checkmarkView.hidden = YES;
    [colorCellView addSubview:checkmarkView];
    
    if (color.isLightColor) {
        CZIconRenderingOptions *options = [CZIconRenderingOptions automaticRenderingOptionsWithTintColor:[UIColor cz_gs120]];
        checkmarkView.image = [[CZIcon iconNamed:@"checkmark"] imageWithOptions:options];
    } else {
        CZIconRenderingOptions *options = [CZIconRenderingOptions automaticRenderingOptionsWithTintColor:[UIColor cz_gs10]];
        checkmarkView.image = [[CZIcon iconNamed:@"checkmark"] imageWithOptions:options];
    }
    
    [checkmarkView.leadingAnchor constraintEqualToAnchor:colorCellView.leadingAnchor constant:8.0].active = YES;
    [checkmarkView.centerYAnchor constraintEqualToAnchor:colorCellView.centerYAnchor].active = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
    [colorCellView addGestureRecognizer:tapGestureRecognizer];
    
    return colorCellView;
}

#pragma mark - Actions

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)sender {
    UIView *colorCellView = sender.view;
    self.channel.color = self.selectableColors[colorCellView.tag];
    
    for (UIView *colorCellView in self.gridView.contentViews) {
        CZChannelColor *color = self.selectableColors[colorCellView.tag];
        UIImageView *checkmarkView = [colorCellView viewWithTag:kCheckmarkViewTag];
        checkmarkView.hidden = ![color isEqualToColor:self.channel.color];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0) {
        self.channel.name = textField.text;
    } else {
        textField.text = self.channel.name;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
