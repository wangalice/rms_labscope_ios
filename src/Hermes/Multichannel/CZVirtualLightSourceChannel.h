//
//  CZVirtualLightSourceChannel.h
//  Hermes
//
//  Created by Li, Junlin on 5/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveChannel.h"

@interface CZVirtualLightSourceChannel : CZLiveChannel

- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected camera:(CZCamera *)camera NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithCamera:(CZCamera *)camera index:(NSUInteger)index NS_DESIGNATED_INITIALIZER;

@end
