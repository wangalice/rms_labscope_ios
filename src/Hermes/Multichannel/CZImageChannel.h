//
//  CZImageChannel.h
//  Hermes
//
//  Created by Li, Junlin on 5/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannel.h"
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZImageChannel : CZChannel

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithImageBlock:(CZImageBlock *)imageBlock NS_DESIGNATED_INITIALIZER;

@end
