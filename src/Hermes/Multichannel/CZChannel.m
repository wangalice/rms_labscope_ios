//
//  CZChannel.m
//  Hermes
//
//  Created by Li, Junlin on 5/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannel.h"

@interface CZChannel ()

@property (nonatomic, strong) NSPointerArray *observers;

@end

@implementation CZChannel

- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected {
    self = [super init];
    if (self) {
        _name = [name copy];
        _color = color;
        _enabled = enabled;
        _selected = selected;
        _observers = [NSPointerArray weakObjectsPointerArray];
    }
    return self;
}

- (BOOL)isNameEditable {
    return NO;
}

- (void)setName:(NSString *)name {
    if (_name == name) {
        return;
    }
    
    _name = [name copy];
    
    [self sendActionToObservers:@selector(channelNameDidChange:)];
}

- (void)setColor:(CZChannelColor *)color {
    if (_color == color) {
        return;
    }
    
    _color = color;
    
    [self sendActionToObservers:@selector(channelColorDidChange:)];
}

- (void)setEnabled:(BOOL)enabled {
    if (_enabled == enabled) {
        return;
    }
    
    _enabled = enabled;
    
    [self sendActionToObservers:@selector(channelEnabledDidChange:)];
}

- (void)setSelected:(BOOL)selected {
    if (_selected == selected) {
        return;
    }
    
    _selected = selected;
    
    [self sendActionToObservers:@selector(channelSelectedDidChange:)];
}

- (void)addObserver:(id<CZChannelObserver>)observer {
    [self.observers addPointer:(void *)observer];
}

- (void)removeObserver:(id<CZChannelObserver>)observer {
    [self.observers compact];
    for (NSInteger index = 0; index < self.observers.count; index++) {
        if ([self.observers pointerAtIndex:index] == (__bridge void *)observer) {
            [self.observers removePointerAtIndex:index];
            break;
        }
    }
}

- (void)sendActionToObservers:(SEL)action {
    for (id<CZChannelObserver> observer in self.observers) {
        if ([observer respondsToSelector:action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [observer performSelector:action withObject:self];
#pragma clang diagnostic pop
        }
    }
}

#pragma mark - Coding

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInteger:self.color.index forKey:@"colorIndex"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _name = [aDecoder decodeObjectOfClass:[NSString class] forKey:@"name"];
        _color = [CZChannelColor colorAtIndex:[aDecoder decodeIntegerForKey:@"colorIndex"]];
    }
    return self;
}

@end
