//
//  CZDefaultSettings+Multichannel.m
//  Hermes
//
//  Created by Li, Junlin on 4/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+Multichannel.h"

@implementation CZDefaultSettings (Multichannel)

#pragma mark - Channel Name

- (NSArray<NSString *> *)defaultChannelNamesForCamera:(CZCamera *)camera {
    NSString *key = [NSString stringWithFormat:kDefaultChannelNames, camera.macAddress.uppercaseString];
    NSArray<NSString *> *defaultChannelNames = [[NSUserDefaults standardUserDefaults] stringArrayForKey:key];
    if (defaultChannelNames) {
        return defaultChannelNames;
    }
    return @[@"CH1", @"CH2", @"CH3", @"CH4", @"CH5", @"CH6"];
}

- (NSString *)channelNameAtIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera {
    NSArray<NSString *> *defaultChannelNames = [self defaultChannelNamesForCamera:camera];
    if (channelIndex < defaultChannelNames.count) {
        return defaultChannelNames[channelIndex];
    } else {
        return nil;
    }
}

- (void)setDefaultChannelNames:(NSArray<NSString *> *)defaultChannelNames forCamera:(CZCamera *)camera {
    NSString *key = [NSString stringWithFormat:kDefaultChannelNames, camera.macAddress.uppercaseString];
    [[NSUserDefaults standardUserDefaults] setObject:defaultChannelNames forKey:key];
}

- (void)setChannelName:(NSString *)channelName atIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera {
    NSMutableArray<NSString *> *defaultChannelNames = [[self defaultChannelNamesForCamera:camera] mutableCopy];
    if (channelName && channelIndex < defaultChannelNames.count) {
        defaultChannelNames[channelIndex] = channelName;
        [self setDefaultChannelNames:defaultChannelNames forCamera:camera];
    }
}

#pragma mark - Channel Color

- (NSArray<CZChannelColor *> *)defaultChannelColorsForCamera:(CZCamera *)camera {
    NSMutableArray<CZChannelColor *> *defaultChannelColors = [NSMutableArray array];
    NSString *key = [NSString stringWithFormat:kDefaultChannelColorIndics, camera.macAddress.uppercaseString];
    NSArray<NSNumber *> *defaultChannelColorIndics = [[NSUserDefaults standardUserDefaults] arrayForKey:key];
    for (NSNumber *index in defaultChannelColorIndics) {
        CZChannelColor *color = [CZChannelColor availableColors][index.integerValue];
        if (color) {
            [defaultChannelColors addObject:color];
        }
    }
    
    if (defaultChannelColors.count > 0) {
        return defaultChannelColors;
    }
    
    return @[
        [CZChannelColor redColor],
        [CZChannelColor greenColor],
        [CZChannelColor blueColor],
        [CZChannelColor yellowColor],
        [CZChannelColor magentaColor],
        [CZChannelColor cyanColor]
    ];
}

- (CZChannelColor *)channelColorAtIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera {
    NSArray<CZChannelColor *> *defaultChannelColors = [[CZDefaultSettings sharedInstance] defaultChannelColorsForCamera:camera];
    if (channelIndex < defaultChannelColors.count) {
        return defaultChannelColors[channelIndex];
    } else {
        return nil;
    }
}

- (void)setDefaultChannelColors:(NSArray<CZChannelColor *> *)defaultChannelColors forCamera:(CZCamera *)camera {
    NSMutableArray<NSNumber *> *defaultChannelColorIndics = [NSMutableArray arrayWithCapacity:defaultChannelColors.count];
    for (CZChannelColor *color in defaultChannelColors) {
        [defaultChannelColorIndics addObject:@(color.index)];
    }
    NSString *key = [NSString stringWithFormat:kDefaultChannelColorIndics, camera.macAddress.uppercaseString];
    [[NSUserDefaults standardUserDefaults] setObject:defaultChannelColorIndics forKey:key];
}

- (void)setChannelColor:(CZChannelColor *)channelColor atIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera {
    NSMutableArray<CZChannelColor *> *colors = [[self defaultChannelColorsForCamera:camera] mutableCopy];
    if (channelColor && channelIndex < colors.count) {
        [colors replaceObjectAtIndex:channelIndex withObject:channelColor];
        [self setDefaultChannelColors:colors forCamera:camera];
    }
}

#pragma mark - Don't Show FL Snap Template Saved Message

- (BOOL)dontShowFluorescenceSnapTemplateSavedMessage {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kDontShowFluorescenceSnapTemplateSavedMessageKey];
}

- (void)setDontShowFluorescenceSnapTemplateSavedMessage:(BOOL)dontShowFluorescenceSnapTemplateSavedMessage {
    [[NSUserDefaults standardUserDefaults] setBool:dontShowFluorescenceSnapTemplateSavedMessage forKey:kDontShowFluorescenceSnapTemplateSavedMessageKey];
}

@end
