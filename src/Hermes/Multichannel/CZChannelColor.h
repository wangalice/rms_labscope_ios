//
//  CZChannelColor.h
//  Hermes
//
//  Created by Li, Junlin on 4/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZChannelColor : NSObject

@property (nonatomic, readonly, assign) NSUInteger index;
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, strong) UIColor *value;
@property (nonatomic, readonly, assign) BOOL isLightColor;

+ (NSArray<CZChannelColor *> *)availableColors;
+ (instancetype)colorAtIndex:(NSUInteger)index;
+ (instancetype)colorWithValue:(UIColor *)value;

+ (instancetype)redColor;
+ (instancetype)greenColor;
+ (instancetype)blueColor;
+ (instancetype)yellowColor;
+ (instancetype)magentaColor;
+ (instancetype)cyanColor;

- (BOOL)isEqualToColor:(CZChannelColor *)otherColor;

@end
