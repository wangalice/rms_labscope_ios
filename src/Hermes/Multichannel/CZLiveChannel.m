//
//  CZLiveChannel.m
//  Hermes
//
//  Created by Li, Junlin on 3/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLiveChannel.h"

@implementation CZLiveChannel

- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected camera:(CZCamera *)camera {
    self = [super initWithName:name color:color enabled:enabled selected:selected];
    if (self) {
        _camera = camera;
    }
    return self;
}

- (void)setSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    if (_snappingInfo == snappingInfo) {
        return;
    }
    
    _snappingInfo = snappingInfo;
    
    [self sendActionToObservers:@selector(liveChannelSnappingInfoDidChange:)];
}

#pragma mark - Coding

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.snappingInfo forKey:@"snappingInfo"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _snappingInfo = [aDecoder decodeObjectOfClass:[CZCameraSnappingInfo class] forKey:@"snappingInfo"];
    }
    return self;
}

@end
