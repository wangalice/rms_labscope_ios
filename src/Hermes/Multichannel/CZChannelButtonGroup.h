//
//  CZChannelButtonGroup.h
//  Hermes
//
//  Created by Li, Junlin on 5/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZChannelButton.h"

@class CZChannel;
@class CZChannelButtonGroup;

@protocol CZChannelButtonGroupDelegate <NSObject>

@optional
- (void)channelButtonGroup:(CZChannelButtonGroup *)buttonGroup didTapButton:(CZChannelButton *)button atIndex:(NSUInteger)index;

@end

@interface CZChannelButtonGroup : UIView

@property (nonatomic, weak) id<CZChannelButtonGroupDelegate> delegate;
@property (nonatomic, readonly, copy) NSArray<CZChannel *> *channels;
@property (nonatomic, readonly, copy) NSArray<CZChannelButton *> *buttons;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)initWithChannels:(NSArray<CZChannel *> *)channels;

@end
