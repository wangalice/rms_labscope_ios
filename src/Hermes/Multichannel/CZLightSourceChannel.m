//
//  CZLightSourceChannel.m
//  Hermes
//
//  Created by Li, Junlin on 5/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLightSourceChannel.h"
#import "CZDefaultSettings+Multichannel.h"

@implementation CZLightSourceChannel

- (instancetype)initWithCamera:(CZCamera *)camera index:(NSUInteger)index lightSource:(CZCameraLightSource *)lightSource {
    NSString *name = @(lightSource.wavelength).stringValue;
    CZChannelColor *color = [[CZDefaultSettings sharedInstance] channelColorAtIndex:index forCamera:camera];
    self = [super initWithName:name color:color enabled:YES selected:lightSource.isOn camera:camera];
    if (self) {
        _lightSource = lightSource;
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZLightSourceChannel class]]) {
        return NO;
    } else {
        CZLightSourceChannel *otherChannel = (CZLightSourceChannel *)object;
        return self.lightSource.wavelength == otherChannel.lightSource.wavelength;
    }
}

- (BOOL)isNameEditable {
    return NO;
}

#pragma mark - Coding

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInteger:self.lightSource.position forKey:@"lightSourcePosition"];
    [aCoder encodeInteger:self.lightSource.wavelength forKey:@"lightSourceWavelength"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSUInteger lightSourcePosition = [aDecoder decodeIntegerForKey:@"lightSourcePosition"];
        NSUInteger lightSourceWavelength = [aDecoder decodeIntegerForKey:@"lightSourceWavelength"];
        _lightSource = [[CZCameraLightSource alloc] initWithPosition:lightSourcePosition wavelength:lightSourceWavelength isOn:NO canOn:NO];
    }
    return self;
}

@end
