//
//  CZLiveChannel.h
//  Hermes
//
//  Created by Li, Junlin on 3/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannel.h"
#import <CZCameraInterface/CZCameraInterface.h>

@class CZLiveChannel;

@protocol CZLiveChannelObserver <CZChannelObserver>

@optional
- (void)liveChannelSnappingInfoDidChange:(CZLiveChannel *)channel;

@end

@interface CZLiveChannel : CZChannel <NSSecureCoding>

@property (nonatomic, readonly, strong) CZCamera *camera;
@property (nonatomic, strong) CZCameraSnappingInfo *snappingInfo;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithName:(NSString *)name color:(CZChannelColor *)color enabled:(BOOL)enabled selected:(BOOL)selected camera:(CZCamera *)camera NS_DESIGNATED_INITIALIZER;

@end
