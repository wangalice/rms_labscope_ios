//
//  CZChannelButtonGroup.m
//  Hermes
//
//  Created by Li, Junlin on 5/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelButtonGroup.h"
#import "CZImageChannel.h"
#import "CZToolbar.h"

@implementation CZChannelButtonGroup

- (instancetype)initWithChannels:(NSArray<CZChannel *> *)channels {
    self = [super init];
    if (self) {
        _channels = [channels copy];
        
        NSMutableArray<CZChannelButton *> *buttons = [NSMutableArray arrayWithCapacity:_channels.count];
        for (NSInteger index = 0; index < _channels.count; index++) {
            CZChannel *channel = _channels[index];
            
            CZChannelButton *button = [[CZChannelButton alloc] initWithChannel:channel];
            button.frame = CGRectMake((64.0 + CZToolbarDefaultSpacing) * index, 0.0, 64.0, 48.0);
            [button addTarget:self action:@selector(channelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            [buttons addObject:button];
        }
        _buttons = [buttons copy];
    }
    return self;
}

- (CGSize)intrinsicContentSize {
    if (self.channels.count == 0) {
        return CGSizeMake(0.0, 48.0);
    } else {
        return CGSizeMake(64.0 * self.channels.count + CZToolbarDefaultSpacing * (self.channels.count - 1), 48.0);
    }
}

- (void)channelButtonAction:(CZChannelButton *)button {
    if (self.delegate && [self.delegate respondsToSelector:@selector(channelButtonGroup:didTapButton:atIndex:)]) {
        NSUInteger index = [self.buttons indexOfObject:button];
        [self.delegate channelButtonGroup:self didTapButton:button atIndex:index];
    }
}

@end

