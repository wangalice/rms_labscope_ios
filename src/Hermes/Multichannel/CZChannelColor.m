//
//  CZChannelColor.m
//  Hermes
//
//  Created by Li, Junlin on 4/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelColor.h"

@implementation CZChannelColor

+ (NSArray<CZChannelColor *> *)availableColors {
    static NSArray<CZChannelColor *> *colors = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        colors = @[
            [self colorWithIndex:0 name:L(@"IMAGE_MULTICHANNEL_COLOR_BLACK") value:[UIColor blackColor]],
            [self colorWithIndex:1 name:L(@"IMAGE_MULTICHANNEL_COLOR_WHITE") value:[UIColor whiteColor]],
            [self colorWithIndex:2 name:L(@"IMAGE_MULTICHANNEL_COLOR_DARKGRAY") value:[UIColor cz_colorWithRed:102 green:102 blue:102]],
            [self colorWithIndex:3 name:L(@"IMAGE_MULTICHANNEL_COLOR_LIGHTGRAY") value:[UIColor cz_colorWithRed:179 green:179 blue:179]],
            [self colorWithIndex:4 name:L(@"IMAGE_MULTICHANNEL_COLOR_LIGHTBLUE") value:[UIColor cz_colorWithRed:0 green:104 blue:255]],
            [self colorWithIndex:5 name:L(@"IMAGE_MULTICHANNEL_COLOR_BLUE") value:[UIColor blueColor]],
            [self colorWithIndex:6 name:L(@"IMAGE_MULTICHANNEL_COLOR_GREEN") value:[UIColor greenColor]],
            [self colorWithIndex:7 name:L(@"IMAGE_MULTICHANNEL_COLOR_LIMEGREEN") value:[UIColor cz_colorWithRed:0 green:153 blue:51]],
            [self colorWithIndex:8 name:L(@"IMAGE_MULTICHANNEL_COLOR_YELLOW") value:[UIColor yellowColor]],
            [self colorWithIndex:9 name:L(@"IMAGE_MULTICHANNEL_COLOR_OLIVE") value:[UIColor cz_colorWithRed:153 green:153 blue:51]],
            [self colorWithIndex:10 name:L(@"IMAGE_MULTICHANNEL_COLOR_RED") value:[UIColor redColor]],
            [self colorWithIndex:11 name:L(@"IMAGE_MULTICHANNEL_COLOR_DARKRED") value:[UIColor cz_colorWithRed:153 green:0 blue:0]],
            [self colorWithIndex:12 name:L(@"IMAGE_MULTICHANNEL_COLOR_VIOLET") value:[UIColor magentaColor]],
            [self colorWithIndex:13 name:L(@"IMAGE_MULTICHANNEL_COLOR_PURPLE") value:[UIColor cz_colorWithRed:153 green:0 blue:204]],
            [self colorWithIndex:14 name:L(@"IMAGE_MULTICHANNEL_COLOR_ORANGE") value:[UIColor cz_colorWithRed:255 green:102 blue:0]],
            [self colorWithIndex:15 name:L(@"IMAGE_MULTICHANNEL_COLOR_DARKORANGE") value:[UIColor cz_colorWithRed:204 green:78 blue:0]],
            [self colorWithIndex:16 name:L(@"IMAGE_MULTICHANNEL_COLOR_TURQUOISE") value:[UIColor cyanColor]],
            [self colorWithIndex:17 name:L(@"IMAGE_MULTICHANNEL_COLOR_BLUEGREEN") value:[UIColor cz_colorWithRed:51 green:204 blue:153]]
        ];
    });
    return colors;
}

+ (instancetype)colorAtIndex:(NSUInteger)index {
    NSArray<CZChannelColor *> *availableColors = [self availableColors];
    if (index >= availableColors.count) {
        return nil;
    }
    return availableColors[index];
}

+ (instancetype)colorWithValue:(UIColor *)value {
    NSArray<CZChannelColor *> *availableColors = [self availableColors];
    for (CZChannelColor *color in availableColors) {
        if ([color.value isEqual:value]) {
            return color;;
        }
    }
    return [self colorWithIndex:NSNotFound name:nil value:value];
}

+ (instancetype)redColor {
    return [self colorAtIndex:10];
}

+ (instancetype)greenColor {
    return [self colorAtIndex:6];
}

+ (instancetype)blueColor {
    return [self colorAtIndex:5];
}

+ (instancetype)yellowColor {
    return [self colorAtIndex:8];
}

+ (instancetype)magentaColor {
    return [self colorAtIndex:12];
}

+ (instancetype)cyanColor {
    return [self colorAtIndex:16];
}

+ (instancetype)colorWithIndex:(NSUInteger)index name:(NSString *)name value:(UIColor *)value {
    return [[self alloc] initWithIndex:index name:name value:value];
}

- (instancetype)initWithIndex:(NSUInteger)index name:(NSString *)name value:(UIColor *)value {
    self = [super init];
    if (self) {
        _index = index;
        _name = [name copy];
        _value = value;
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZChannelColor class]]) {
        return NO;
    } else {
        return [self isEqualToColor:object];
    }
}

- (BOOL)isEqualToColor:(CZChannelColor *)otherColor {
    return self.index == otherColor.index;
}

- (BOOL)isLightColor {
    CGFloat brightness;
    [self.value getHue:nil saturation:nil brightness:&brightness alpha:nil];
    return brightness > 0.5;
}

@end
