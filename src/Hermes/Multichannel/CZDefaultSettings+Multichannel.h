//
//  CZDefaultSettings+Multichannel.h
//  Hermes
//
//  Created by Li, Junlin on 4/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLightSourceChannel.h"

/*! Default channel names. since v14 [array<string>]*/
#define kDefaultChannelNames @"default_channel_names_%@"

/*! Default channel colors. since v14 [array<integer>]*/
#define kDefaultChannelColorIndics @"default_channel_color_indics_%@"

/*! Don't fluorescence snap template saved message. since v14 [boolean]*/
#define kDontShowFluorescenceSnapTemplateSavedMessageKey @"dont_show_fluorescence_template_saved_message"

@interface CZDefaultSettings (Multichannel)

- (NSString *)channelNameAtIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera;
- (void)setChannelName:(NSString *)channelName atIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera;

- (CZChannelColor *)channelColorAtIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera;
- (void)setChannelColor:(CZChannelColor *)channelColor atIndex:(NSUInteger)channelIndex forCamera:(CZCamera *)camera;

@property (nonatomic, assign) BOOL dontShowFluorescenceSnapTemplateSavedMessage;

@end
