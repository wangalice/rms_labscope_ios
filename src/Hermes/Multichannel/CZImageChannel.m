//
//  CZImageChannel.m
//  Hermes
//
//  Created by Li, Junlin on 5/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageChannel.h"

@implementation CZImageChannel

- (instancetype)initWithImageBlock:(CZImageBlock *)imageBlock {
    CZImageChannelProperties *channelProperties = imageBlock.channelProperties;
    NSString *name = channelProperties.channelName;
    CZChannelColor *color = [CZChannelColor colorWithValue:channelProperties.channelColor];
    BOOL selected = channelProperties.channelSelection ? channelProperties.channelSelection.boolValue : YES;
    self = [super initWithName:name color:color enabled:YES selected:selected];
    if (self) {
    }
    return self;
}

- (BOOL)isNameEditable {
    return YES;
}

@end
