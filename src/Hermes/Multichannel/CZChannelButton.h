//
//  CZChannelButton.h
//  Hermes
//
//  Created by Li, Junlin on 3/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZChannel;

@interface CZChannelButton : CZButton

@property (nonatomic, readonly, strong) CZChannel *channel;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)initWithLevel:(CZControlEmphasisLevel)level NS_UNAVAILABLE;
- (instancetype)initWithChannel:(CZChannel *)channel NS_DESIGNATED_INITIALIZER;

@end
