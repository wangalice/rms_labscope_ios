//
//  CZChannelButton.m
//  Hermes
//
//  Created by Li, Junlin on 3/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelButton.h"
#import "CZChannel.h"
#import "CZChannelColorPickerViewController.h"
#import "CZPopoverController.h"

@interface CZChannelButton () <CZChannelObserver>

@property (nonatomic, readonly, strong) UIView *colorIndicatorBackgroundView;
@property (nonatomic, readonly, strong) UIView *colorIndicatorView;
@property (nonatomic, readonly, strong) UILongPressGestureRecognizer *longPressGestureRecognizer;

@end

@implementation CZChannelButton

@synthesize colorIndicatorBackgroundView = _colorIndicatorBackgroundView;
@synthesize colorIndicatorView = _colorIndicatorView;
@synthesize longPressGestureRecognizer = _longPressGestureRecognizer;

- (instancetype)initWithChannel:(CZChannel *)channel {
    self = [super initWithLevel:CZControlEmphasisActivePrimary];
    if (self) {
        _channel = channel;
        [_channel addObserver:self];
        
        self.frame = CGRectMake(0.0, 0.0, 64.0, 48.0);
        self.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 16.0, 0.0);
        [self addSubview:self.colorIndicatorBackgroundView];
        [self.colorIndicatorBackgroundView addSubview:self.colorIndicatorView];
        [self addGestureRecognizer:self.longPressGestureRecognizer];
        
        [self setTitle:_channel.name forState:UIControlStateNormal];
        self.enabled = _channel.enabled;
        self.selected = _channel.isSelected;
    }
    return self;
}

- (void)dealloc {
    [_channel removeObserver:self];
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    
    self.colorIndicatorBackgroundView.backgroundColor = enabled ? [UIColor cz_gs120] : [[UIColor cz_gs120] colorWithAlphaComponent:0.5];
    self.colorIndicatorView.backgroundColor = enabled ? self.channel.color.value : [self.channel.color.value colorWithAlphaComponent:0.5];
}

#pragma mark - Views

- (UIView *)colorIndicatorBackgroundView {
    if (_colorIndicatorBackgroundView == nil) {
        _colorIndicatorBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.bounds.size.height - 16.0, self.bounds.size.width, 16.0)];
        _colorIndicatorBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _colorIndicatorBackgroundView.backgroundColor = [UIColor cz_gs120];
        
        CAShapeLayer *mask = [CAShapeLayer layer];
        mask.path = [[UIBezierPath bezierPathWithRoundedRect:_colorIndicatorBackgroundView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(3.0, 3.0)] CGPath];
        _colorIndicatorBackgroundView.layer.mask = mask;
    }
    return _colorIndicatorBackgroundView;
}

- (UIView *)colorIndicatorView {
    if (_colorIndicatorView == nil) {
        _colorIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(1.0, 1.0, self.bounds.size.width - 2.0, 14.0)];
        _colorIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        CAShapeLayer *mask = [CAShapeLayer layer];
        mask.path = [[UIBezierPath bezierPathWithRoundedRect:_colorIndicatorView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(2.0, 2.0)] CGPath];
        _colorIndicatorView.layer.mask = mask;
    }
    return _colorIndicatorView;
}

#pragma mark - Long Press Gesture Recognizer

- (UILongPressGestureRecognizer *)longPressGestureRecognizer {
    if (_longPressGestureRecognizer == nil) {
        _longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognizerAction:)];
    }
    return _longPressGestureRecognizer;
}

- (void)longPressGestureRecognizerAction:(UILongPressGestureRecognizer *)longPressGestureRecognizer {
    if (longPressGestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CZChannelColorPickerViewController *colorPickerViewController = [[CZChannelColorPickerViewController alloc] initWithChannel:self.channel];
        
        UIViewController *presentingViewController = UIApplication.sharedApplication.keyWindow.rootViewController;
        CGFloat horizontalLayoutMargin = (presentingViewController.view.bounds.size.width - colorPickerViewController.preferredContentSize.width) / 2;
        
        CZPopoverController *popover = [[CZPopoverController alloc] initWithContentViewController:colorPickerViewController];
        popover.sourceView = self;
        popover.sourceRect = CGRectOffset(self.bounds, 0.0, -4.0);
        popover.arrowDirection = CZPopoverArrowDirectionDown;
        popover.popoverLayoutMargins = UIEdgeInsetsMake(CZPopoverDefaultLayoutMargin, horizontalLayoutMargin, CZPopoverDefaultLayoutMargin, horizontalLayoutMargin);
        [popover presentAnimated:YES completion:nil];
    }
}

#pragma mark - CZChannelObserver

- (void)channelNameDidChange:(CZChannel *)channel {
    [self setTitle:channel.name forState:UIControlStateNormal];
}

- (void)channelColorDidChange:(CZChannel *)channel {
    self.colorIndicatorView.backgroundColor = channel.color.value;
}

- (void)channelEnabledDidChange:(CZChannel *)channel {
    self.enabled = channel.isEnabled;
}

- (void)channelSelectedDidChange:(CZChannel *)channel {
    self.selected = channel.isSelected;
}

@end
