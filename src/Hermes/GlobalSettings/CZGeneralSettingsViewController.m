//
//  CZGeneralSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeneralSettingsViewController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZGridView.h"

@interface CZGeneralSettingsViewController ()

@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) CZGridView *gridView;
@property (nonatomic, readonly, strong) UILabel *informationScreensLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *informationScreensDropdown;
@property (nonatomic, readonly, strong) UILabel *showVirtualMicroscopesLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *showVirtualMicroscopesDropdown;
@property (nonatomic, readonly, strong) UILabel *microscopesCanBeAddedManuallyLabel;
@property (nonatomic, readonly, strong) CZSwitch *microscopesCanBeAddedManuallySwitch;
@property (nonatomic, readonly, strong) UILabel *defaultFileFormatLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *defaultFileFormatDropdown;
@property (nonatomic, readonly, strong) UILabel *overwriteImageOnSaveLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *overwriteImageOnSaveDropdown;
@property (nonatomic, readonly, strong) UILabel *enableMacroSnapLabel;
@property (nonatomic, readonly, strong) CZSwitch *enableMacroSnapSwitch;

@end

@implementation CZGeneralSettingsViewController

@synthesize titleLabel = _titleLabel;
@synthesize gridView = _gridView;
@synthesize informationScreensLabel = _informationScreensLabel;
@synthesize informationScreensDropdown = _informationScreensDropdown;
@synthesize showVirtualMicroscopesLabel = _showVirtualMicroscopesLabel;
@synthesize showVirtualMicroscopesDropdown = _showVirtualMicroscopesDropdown;
@synthesize microscopesCanBeAddedManuallyLabel = _microscopesCanBeAddedManuallyLabel;
@synthesize microscopesCanBeAddedManuallySwitch = _microscopesCanBeAddedManuallySwitch;
@synthesize defaultFileFormatLabel = _defaultFileFormatLabel;
@synthesize defaultFileFormatDropdown = _defaultFileFormatDropdown;
@synthesize overwriteImageOnSaveLabel = _overwriteImageOnSaveLabel;
@synthesize overwriteImageOnSaveDropdown = _overwriteImageOnSaveDropdown;
@synthesize enableMacroSnapLabel = _enableMacroSnapLabel;
@synthesize enableMacroSnapSwitch = _enableMacroSnapSwitch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    void (^switchConfigurationHandler)(CZGridViewLayoutAttributes *layoutAttributes) = ^(CZGridViewLayoutAttributes *layoutAttributes) {
        layoutAttributes.columnAlignment = CZGridColumnAlignmentTrailing;
        layoutAttributes.rowAlignment = CZGridRowAlignmentCenter;
    };
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.gridView];
    [self.gridView addContentView:self.informationScreensLabel];
    [self.gridView addContentView:self.informationScreensDropdown];
    [self.gridView addContentView:self.showVirtualMicroscopesLabel];
    [self.gridView addContentView:self.showVirtualMicroscopesDropdown];
    [self.gridView addContentView:self.microscopesCanBeAddedManuallyLabel];
    [self.gridView addContentView:self.microscopesCanBeAddedManuallySwitch configurationHandler:switchConfigurationHandler];
    [self.gridView addContentView:self.defaultFileFormatLabel];
    [self.gridView addContentView:self.defaultFileFormatDropdown];
    [self.gridView addContentView:self.overwriteImageOnSaveLabel];
    [self.gridView addContentView:self.overwriteImageOnSaveDropdown];
    [self.gridView addContentView:self.enableMacroSnapLabel];
    [self.gridView addContentView:self.enableMacroSnapSwitch configurationHandler:switchConfigurationHandler];
    
    [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:27.0].active = YES;
    
    [self.gridView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.gridView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:59.0].active = YES;
}

#pragma mark - Views

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.text = L(@"GROUP_GENERAL");
        _titleLabel.textColor = [UIColor cz_gs20];
    }
    return _titleLabel;
}

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row4 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row5 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:128.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:304.0 alignment:CZGridColumnAlignmentFill];
        
        _gridView = [[CZGridView alloc] initWithRows:@[row0, row1, row2, row3, row4, row5]
                                             columns:@[column0, column1]];
        _gridView.translatesAutoresizingMaskIntoConstraints = NO;
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 16.0;
    }
    return _gridView;
}

- (UILabel *)informationScreensLabel {
    if (_informationScreensLabel == nil) {
        _informationScreensLabel = [[UILabel alloc] init];
        _informationScreensLabel.font = [UIFont cz_label1];
        _informationScreensLabel.text = L(@"INFO_SCREEN");
        _informationScreensLabel.textColor = [UIColor cz_gs80];
        _informationScreensLabel.numberOfLines = 2;
    }
    return _informationScreensLabel;
}

- (CZDropdownMenu *)informationScreensDropdown {
    if (_informationScreensDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"INFO_SCREEN_USER_DEFINED"), L(@"INFO_SCREEN_ENABLE_ALL"), L(@"INFO_SCREEN_DISABLE_ALL")];
        _informationScreensDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        _informationScreensDropdown.selectedIndex = [[CZDefaultSettings sharedInstance] infoScreenOption];
    }
    return _informationScreensDropdown;
}

- (UILabel *)showVirtualMicroscopesLabel {
    if (_showVirtualMicroscopesLabel == nil) {
        _showVirtualMicroscopesLabel = [[UILabel alloc] init];
        _showVirtualMicroscopesLabel.font = [UIFont cz_label1];
        _showVirtualMicroscopesLabel.text = L(@"SHOW_VIRTUAL_MICROSCOPES");
        _showVirtualMicroscopesLabel.textColor = [UIColor cz_gs80];
        _showVirtualMicroscopesLabel.numberOfLines = 2;
    }
    return _showVirtualMicroscopesLabel;
}

- (CZDropdownMenu *)showVirtualMicroscopesDropdown {
    if (_showVirtualMicroscopesDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"VIRTUAL_MICROSCOPES_AUTOMATIC"), L(@"VIRTUAL_MICROSCOPES_ALWAYS"), L(@"VIRTUAL_MICROSCOPES_NEVER")];
        _showVirtualMicroscopesDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        _showVirtualMicroscopesDropdown.selectedIndex = [[CZDefaultSettings sharedInstance] virtualMicroscopesOption];
    }
    return _showVirtualMicroscopesDropdown;
}

- (UILabel *)microscopesCanBeAddedManuallyLabel {
    if (_microscopesCanBeAddedManuallyLabel == nil) {
        _microscopesCanBeAddedManuallyLabel = [[UILabel alloc] init];
        _microscopesCanBeAddedManuallyLabel.font = [UIFont cz_label1];
        _microscopesCanBeAddedManuallyLabel.text = L(@"MANUALLY_ADD_MICROSCOPES");
        _microscopesCanBeAddedManuallyLabel.textColor = [UIColor cz_gs80];
        _microscopesCanBeAddedManuallyLabel.numberOfLines = 2;
    }
    return _microscopesCanBeAddedManuallyLabel;
}

- (CZSwitch *)microscopesCanBeAddedManuallySwitch {
    if (_microscopesCanBeAddedManuallySwitch == nil) {
        _microscopesCanBeAddedManuallySwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_microscopesCanBeAddedManuallySwitch setLevel:CZControlEmphasisActivePrimary];
        _microscopesCanBeAddedManuallySwitch.on = [[CZDefaultSettings sharedInstance] enableManuallyAddMicroscopes];
    }
    return _microscopesCanBeAddedManuallySwitch;
}

- (UILabel *)defaultFileFormatLabel {
    if (_defaultFileFormatLabel == nil) {
        _defaultFileFormatLabel = [[UILabel alloc] init];
        _defaultFileFormatLabel.font = [UIFont cz_label1];
        _defaultFileFormatLabel.text = L(@"DEFAULT_FILE_FORMAT");
        _defaultFileFormatLabel.textColor = [UIColor cz_gs80];
        _defaultFileFormatLabel.numberOfLines = 2;
    }
    return _defaultFileFormatLabel;
}

- (CZDropdownMenu *)defaultFileFormatDropdown {
    if (_defaultFileFormatDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"DEFAULT_FILE_FORMAT_CZI"), L(@"DEFAULT_FILE_FORMAT_CZI_JPEG"), L(@"DEFAULT_FILE_FORMAT_JPEG"), L(@"DEFAULT_FILE_FORMAT_CZI_TIF"), L(@"DEFAULT_FILE_FORMAT_TIF")];
        _defaultFileFormatDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        CZIFileSaveFormat fileFormat = [[CZDefaultSettings sharedInstance] fileFormat];
        _defaultFileFormatDropdown.selectedIndex = [self indexForDefaultFileFormat:fileFormat];
    }
    return _defaultFileFormatDropdown;
}

- (UILabel *)overwriteImageOnSaveLabel {
    if (_overwriteImageOnSaveLabel == nil) {
        _overwriteImageOnSaveLabel = [[UILabel alloc] init];
        _overwriteImageOnSaveLabel.font = [UIFont cz_label1];
        _overwriteImageOnSaveLabel.text = L(@"OVERWRITE_IMAGE_ON_SAVE");
        _overwriteImageOnSaveLabel.textColor = [UIColor cz_gs80];
        _overwriteImageOnSaveLabel.numberOfLines = 2;
    }
    return _overwriteImageOnSaveLabel;
}

- (CZDropdownMenu *)overwriteImageOnSaveDropdown {
    if (_overwriteImageOnSaveDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"ASK_ON_SAVE"), L(@"NEVER_OVERWRITE"), L(@"ALWAYS_OVERWRITE")];
        _overwriteImageOnSaveDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        _overwriteImageOnSaveDropdown.selectedIndex = [[CZDefaultSettings sharedInstance] fileOverwriteOption];
    }
    return _overwriteImageOnSaveDropdown;
}

- (UILabel *)enableMacroSnapLabel {
    if (_enableMacroSnapLabel == nil) {
        _enableMacroSnapLabel = [[UILabel alloc] init];
        _enableMacroSnapLabel.font = [UIFont cz_label1];
        _enableMacroSnapLabel.text = L(@"ENABLE_MACRO_SNAP");
        _enableMacroSnapLabel.textColor = [UIColor cz_gs80];
        _enableMacroSnapLabel.numberOfLines = 2;
    }
    return _enableMacroSnapLabel;
}

- (CZSwitch *)enableMacroSnapSwitch {
    if (_enableMacroSnapSwitch == nil) {
        _enableMacroSnapSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_enableMacroSnapSwitch setLevel:CZControlEmphasisActivePrimary];
        _enableMacroSnapSwitch.on = [[CZDefaultSettings sharedInstance] enableMacroSnap];
    }
    return _enableMacroSnapSwitch;
}

#pragma mark - Private

- (NSUInteger)indexForDefaultFileFormat:(CZIFileSaveFormat)fileFormat {
    switch (fileFormat) {
        case kCZI:
            return 0;
        case kCZIAndJPG:
            return 1;
        case kJPGWithMarkup:
            return 2;
        case kCZIAndTIF:
            return 3;
        case kTIFWithMarkup:
            return 4;
        default:
            return 0;
    }
}

- (CZIFileSaveFormat)defaultFileFormatForIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return kCZI;
        case 1:
            return kCZIAndJPG;
        case 2:
            return kJPGWithMarkup;
        case 3:
            return kCZIAndTIF;
        case 4:
            return kTIFWithMarkup;
        default:
            return kCZI;
    }
}

#pragma mark - CZGlobalSettingsGroupViewController

- (BOOL)areSettingsChanged {
    CZDefaultSettings *defaultSettings = [CZDefaultSettings sharedInstance];
    return (self.informationScreensDropdown.selectedIndex != defaultSettings.infoScreenOption ||
            self.showVirtualMicroscopesDropdown.selectedIndex != defaultSettings.virtualMicroscopesOption ||
            self.microscopesCanBeAddedManuallySwitch.isOn != defaultSettings.enableManuallyAddMicroscopes ||
            [self defaultFileFormatForIndex:self.defaultFileFormatDropdown.selectedIndex] != defaultSettings.fileFormat ||
            self.overwriteImageOnSaveDropdown.selectedIndex != defaultSettings.fileOverwriteOption ||
            self.enableMacroSnapSwitch.isOn != defaultSettings.enableMacroSnap);
}

- (void)applySettings {
    [[CZDefaultSettings sharedInstance] setInfoScreenOption:self.informationScreensDropdown.selectedIndex];
    
    [[CZDefaultSettings sharedInstance] setVirtualMicroscopesOption:self.showVirtualMicroscopesDropdown.selectedIndex];
    
    [[CZDefaultSettings sharedInstance] setEnableManuallyAddMicroscopes:self.microscopesCanBeAddedManuallySwitch.isOn];
    
    [[CZDefaultSettings sharedInstance] setFileFormat:[self defaultFileFormatForIndex:self.defaultFileFormatDropdown.selectedIndex]];
    
    [[CZDefaultSettings sharedInstance] setFileOverwriteOption:self.overwriteImageOnSaveDropdown.selectedIndex];
    
    [[CZDefaultSettings sharedInstance] setEnableMacroSnap:self.enableMacroSnapSwitch.isOn];
}

@end
