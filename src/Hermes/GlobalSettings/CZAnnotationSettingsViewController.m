//
//  CZAnnotationSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationSettingsViewController.h"
#import <CZAnnotationKit/CZDefaultSettings+Annotations.h>
#import "CZGridView.h"
#import "CZAnnotationColorPickerView.h"

@interface CZAnnotationSettingsViewController ()

@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) CZGridView *gridView;
@property (nonatomic, readonly, strong) UILabel *measurementUnitLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *measurementUnitDropdown;
@property (nonatomic, readonly, strong) UILabel *showMeasurementsByDefaultLabel;
@property (nonatomic, readonly, strong) CZSwitch *showMeasurementsByDefaultSwitch;
@property (nonatomic, readonly, strong) UILabel *defaultColorLabel;
@property (nonatomic, readonly, strong) CZAnnotationColorPickerView *defaultColorPickerView;
@property (nonatomic, readonly, strong) UILabel *textBackgroundTransparencyLabel;
@property (nonatomic, readonly, strong) CZSwitch *textBackgroundTransparencySwitch;
@property (nonatomic, readonly, strong) UILabel *textBackgroundColorLabel;
@property (nonatomic, readonly, strong) CZAnnotationColorPickerView *textBackgroundColorPickerView;
@property (nonatomic, readonly, strong) UILabel *defaultSizeLabel;
@property (nonatomic, readonly, strong) CZToggleButton *defaultSizeToggleButton;
@property (nonatomic, readonly, strong) UILabel *disableMeasurementColorCodingLabel;
@property (nonatomic, readonly, strong) CZSwitch *disableMeasurementColorCodingSwitch;

@end

@implementation CZAnnotationSettingsViewController

@synthesize titleLabel = _titleLabel;
@synthesize gridView = _gridView;
@synthesize measurementUnitLabel = _measurementUnitLabel;
@synthesize measurementUnitDropdown = _measurementUnitDropdown;
@synthesize showMeasurementsByDefaultLabel = _showMeasurementsByDefaultLabel;
@synthesize showMeasurementsByDefaultSwitch = _showMeasurementsByDefaultSwitch;
@synthesize defaultColorLabel = _defaultColorLabel;
@synthesize defaultColorPickerView = _defaultColorPickerView;
@synthesize textBackgroundTransparencyLabel = _textBackgroundTransparencyLabel;
@synthesize textBackgroundTransparencySwitch = _textBackgroundTransparencySwitch;
@synthesize textBackgroundColorLabel = _textBackgroundColorLabel;
@synthesize textBackgroundColorPickerView = _textBackgroundColorPickerView;
@synthesize defaultSizeLabel = _defaultSizeLabel;
@synthesize defaultSizeToggleButton = _defaultSizeToggleButton;
@synthesize disableMeasurementColorCodingLabel = _disableMeasurementColorCodingLabel;
@synthesize disableMeasurementColorCodingSwitch = _disableMeasurementColorCodingSwitch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    void (^switchConfigurationHandler)(CZGridViewLayoutAttributes *layoutAttributes) = ^(CZGridViewLayoutAttributes *layoutAttributes) {
        layoutAttributes.columnAlignment = CZGridColumnAlignmentTrailing;
        layoutAttributes.rowAlignment = CZGridRowAlignmentCenter;
    };
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.gridView];
    [self.gridView addContentView:self.measurementUnitLabel];
    [self.gridView addContentView:self.measurementUnitDropdown];
    [self.gridView addContentView:self.showMeasurementsByDefaultLabel];
    [self.gridView addContentView:self.showMeasurementsByDefaultSwitch configurationHandler:switchConfigurationHandler];
    [self.gridView addContentView:self.defaultColorLabel];
    [self.gridView addContentView:self.defaultColorPickerView configurationHandler:switchConfigurationHandler];
    [self.gridView addContentView:self.textBackgroundTransparencyLabel];
    [self.gridView addContentView:self.textBackgroundTransparencySwitch configurationHandler:switchConfigurationHandler];
    [self.gridView addContentView:self.textBackgroundColorLabel];
    [self.gridView addContentView:self.textBackgroundColorPickerView configurationHandler:switchConfigurationHandler];
    [self.gridView addContentView:self.defaultSizeLabel];
    [self.gridView addContentView:self.defaultSizeToggleButton];
    [self.gridView addContentView:self.disableMeasurementColorCodingLabel];
    [self.gridView addContentView:self.disableMeasurementColorCodingSwitch configurationHandler:switchConfigurationHandler];
    
    [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:27.0].active = YES;
    
    [self.gridView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.gridView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:59.0].active = YES;
}

#pragma mark - Views

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.text = L(@"ANNOTATION_DEFAULT_SETTINGS");
        _titleLabel.textColor = [UIColor cz_gs20];
    }
    return _titleLabel;
}

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row4 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row5 = [[CZGridRow alloc] initWithHeight:48.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row6 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:128.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:304.0 alignment:CZGridColumnAlignmentFill];
        
        _gridView = [[CZGridView alloc] initWithRows:@[row0, row1, row2, row3, row4, row5, row6]
                                             columns:@[column0, column1]];
        _gridView.translatesAutoresizingMaskIntoConstraints = NO;
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 16.0;
    }
    return _gridView;
}

- (UILabel *)measurementUnitLabel {
    if (_measurementUnitLabel == nil) {
        _measurementUnitLabel = [[UILabel alloc] init];
        _measurementUnitLabel.font = [UIFont cz_label1];
        _measurementUnitLabel.text = L(@"MEASUREMENT_UNIT");
        _measurementUnitLabel.textColor = [UIColor cz_gs80];
        _measurementUnitLabel.numberOfLines = 2;
    }
    return _measurementUnitLabel;
}

- (CZDropdownMenu *)measurementUnitDropdown {
    if (_measurementUnitDropdown == nil) {
        NSArray<NSString *> *titles = @[L(@"MEASUREMENT_UNIT_AUTOMATIC"), L(@"MEASUREMENT_UNIT_UM"), L(@"MEASUREMENT_UNIT_MIL"), L(@"MEASUREMENT_UNIT_MM"), L(@"MEASUREMENT_UNIT_INCH")];
        _measurementUnitDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:titles];
        CZElementUnitStyle unitStyle = [[CZDefaultSettings sharedInstance] unitStyle];
        _measurementUnitDropdown.selectedIndex = [self indexForMeasurementUnit:unitStyle];
    }
    return _measurementUnitDropdown;
}

- (UILabel *)showMeasurementsByDefaultLabel {
    if (_showMeasurementsByDefaultLabel == nil) {
        _showMeasurementsByDefaultLabel = [[UILabel alloc] init];
        _showMeasurementsByDefaultLabel.font = [UIFont cz_label1];
        _showMeasurementsByDefaultLabel.text = L(@"MEASUREMENT_ENABLED");
        _showMeasurementsByDefaultLabel.textColor = [UIColor cz_gs80];
        _showMeasurementsByDefaultLabel.numberOfLines = 2;
    }
    return _showMeasurementsByDefaultLabel;
}

- (CZSwitch *)showMeasurementsByDefaultSwitch {
    if (_showMeasurementsByDefaultSwitch == nil) {
        _showMeasurementsByDefaultSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_showMeasurementsByDefaultSwitch setLevel:CZControlEmphasisActivePrimary];
        _showMeasurementsByDefaultSwitch.on = [[CZDefaultSettings sharedInstance] enableMeasurement];
    }
    return _showMeasurementsByDefaultSwitch;
}

- (UILabel *)defaultColorLabel {
    if (_defaultColorLabel == nil) {
        _defaultColorLabel = [[UILabel alloc] init];
        _defaultColorLabel.font = [UIFont cz_label1];
        _defaultColorLabel.text = L(@"DEFAULT_COLOR");
        _defaultColorLabel.textColor = [UIColor cz_gs80];
        _defaultColorLabel.numberOfLines = 2;
    }
    return _defaultColorLabel;
}

- (CZAnnotationColorPickerView *)defaultColorPickerView {
    if (_defaultColorPickerView == nil) {
        _defaultColorPickerView = [[CZAnnotationColorPickerView alloc] initWithColors:kCZColorValueRed, kCZColorValueBlue, kCZColorValueGreen, kCZColorValueYellow, kCZColorValueBlack, nil];
        _defaultColorPickerView.selectedColor = [[CZDefaultSettings sharedInstance] color];
    }
    return _defaultColorPickerView;
}

- (UILabel *)textBackgroundTransparencyLabel {
    if (_textBackgroundTransparencyLabel == nil) {
        _textBackgroundTransparencyLabel = [[UILabel alloc] init];
        _textBackgroundTransparencyLabel.font = [UIFont cz_label1];
        _textBackgroundTransparencyLabel.text = L(@"DEFAULT_BACKGROUND_COLOR_CLEAR");
        _textBackgroundTransparencyLabel.textColor = [UIColor cz_gs80];
        _textBackgroundTransparencyLabel.numberOfLines = 2;
    }
    return _textBackgroundTransparencyLabel;
}

- (CZSwitch *)textBackgroundTransparencySwitch {
    if (_textBackgroundTransparencySwitch == nil) {
        _textBackgroundTransparencySwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_textBackgroundTransparencySwitch setLevel:CZControlEmphasisActivePrimary];
        _textBackgroundTransparencySwitch.on = CZColorEqualToColor([[CZDefaultSettings sharedInstance] backgroundColor], kCZColorTransparent);
        [_textBackgroundTransparencySwitch addTarget:self action:@selector(textBackgroundTransparencySwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _textBackgroundTransparencySwitch;
}

- (UILabel *)textBackgroundColorLabel {
    if (_textBackgroundColorLabel == nil) {
        _textBackgroundColorLabel = [[UILabel alloc] init];
        _textBackgroundColorLabel.font = [UIFont cz_label1];
        _textBackgroundColorLabel.text = L(@"DEFAULT_BACKGROUND_COLOR");
        _textBackgroundColorLabel.textColor = [UIColor cz_gs80];
        _textBackgroundColorLabel.numberOfLines = 2;
    }
    return _textBackgroundColorLabel;
}

- (CZAnnotationColorPickerView *)textBackgroundColorPickerView {
    if (_textBackgroundColorPickerView == nil) {
        _textBackgroundColorPickerView = [[CZAnnotationColorPickerView alloc] initWithColors:kCZColorValueRed, kCZColorValueBlue, kCZColorValueGreen, kCZColorValueWhite, kCZColorValueBlack, nil];
        CZColor backgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        _textBackgroundColorPickerView.selectedColor = backgroundColor;
        _textBackgroundColorPickerView.userInteractionEnabled = !CZColorEqualToColor(backgroundColor, kCZColorTransparent);
        [_textBackgroundColorPickerView addTarget:self action:@selector(textBackgroundColorPickerViewAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _textBackgroundColorPickerView;
}

- (UILabel *)defaultSizeLabel {
    if (_defaultSizeLabel == nil) {
        _defaultSizeLabel = [[UILabel alloc] init];
        _defaultSizeLabel.font = [UIFont cz_label1];
        _defaultSizeLabel.text = L(@"DEFAULT_SIZE");
        _defaultSizeLabel.textColor = [UIColor cz_gs80];
        _defaultSizeLabel.numberOfLines = 2;
    }
    return _defaultSizeLabel;
}

- (CZToggleButton *)defaultSizeToggleButton {
    if (_defaultSizeToggleButton == nil) {
        NSArray<NSString *> *titles = @[L(@"SIZE_XS"), L(@"SIZE_S"), L(@"SIZE_M"), L(@"SIZE_L"), L(@"SIZE_XL")];
        _defaultSizeToggleButton = [[CZToggleButton alloc] initWithItems:titles];
        _defaultSizeToggleButton.selectedIndex = [[CZDefaultSettings sharedInstance] elementSize];
    }
    return _defaultSizeToggleButton;
}

- (UILabel *)disableMeasurementColorCodingLabel {
    if (_disableMeasurementColorCodingLabel == nil) {
        _disableMeasurementColorCodingLabel = [[UILabel alloc] init];
        _disableMeasurementColorCodingLabel.font = [UIFont cz_label1];
        _disableMeasurementColorCodingLabel.text = L(@"MEASUREMENT_COLOR_CODING_DISABLED");
        _disableMeasurementColorCodingLabel.textColor = [UIColor cz_gs80];
        _disableMeasurementColorCodingLabel.numberOfLines = 2;
    }
    return _disableMeasurementColorCodingLabel;
}

- (CZSwitch *)disableMeasurementColorCodingSwitch {
    if (_disableMeasurementColorCodingSwitch == nil) {
        _disableMeasurementColorCodingSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        [_disableMeasurementColorCodingSwitch setLevel:CZControlEmphasisActivePrimary];
        _disableMeasurementColorCodingSwitch.on = [[CZDefaultSettings sharedInstance] disableMeasurementColorCoding];
    }
    return _disableMeasurementColorCodingSwitch;
}

#pragma mark - Actions

- (void)textBackgroundTransparencySwitchAction:(id)sender {
    if (self.textBackgroundTransparencySwitch.isOn) {
        self.textBackgroundColorPickerView.selectedColorIndex = CZAnnotationColorPickerViewNoSelectionColor;
        self.textBackgroundColorPickerView.userInteractionEnabled = NO;
    } else {
        self.textBackgroundColorPickerView.selectedColor = kCZColorWhite;
        self.textBackgroundColorPickerView.userInteractionEnabled = YES;
    }
}

- (void)textBackgroundColorPickerViewAction:(CZAnnotationColorPickerView *)sender {
    CZColor defaultColor = [self defaultColorAfterChangingBackgroundColor:sender.selectedColor originalForegroundColor:self.defaultColorPickerView.selectedColor];
    self.defaultColorPickerView.selectedColor = defaultColor;
}

#pragma mark - Private

- (NSUInteger)indexForMeasurementUnit:(CZElementUnitStyle)measurementUnit {
    switch (measurementUnit) {
        case CZElementUnitStyleAuto:
            return 0;
        case CZElementUnitStyleMicrometer:
            return 1;
        case CZElementUnitStyleMil:
            return 2;
        case CZElementUnitStyleMM:
            return 3;
        case CZElementUnitStyleInch:
            return 4;
        default:
            return 0;
    }
}

- (CZElementUnitStyle)measurementUnitForIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return CZElementUnitStyleAuto;
        case 1:
            return CZElementUnitStyleMicrometer;
        case 2:
            return CZElementUnitStyleMil;
        case 3:
            return CZElementUnitStyleMM;
        case 4:
            return CZElementUnitStyleInch;
        default:
            return CZElementUnitStyleAuto;
    }
}

- (CZColor)selectedTextBackgroundColor {
    if (self.textBackgroundTransparencySwitch.isOn) {
        return kCZColorTransparent;
    } else {
        return self.textBackgroundColorPickerView.selectedColor;
    }
}

- (CZColor)defaultColorAfterChangingBackgroundColor:(CZColor)backgroundColor originalForegroundColor:(CZColor)originalForegroundColor {
    CZColor foregroundColor = kCZColorRed;
    
    if (CZColorEqualToColor(originalForegroundColor, backgroundColor)) {
        if (CZColorEqualToColor(backgroundColor, kCZColorRed)) {
            foregroundColor = kCZColorBlue;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorBlue)) {
            foregroundColor = kCZColorGreen;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorWhite)) {
            foregroundColor = kCZColorBlack;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorGreen)) {
            foregroundColor = kCZColorBlack;
        }
        if (CZColorEqualToColor(backgroundColor, kCZColorBlack)) {
            foregroundColor = kCZColorRed;
        }
    } else {
        if (CZColorEqualToColor(backgroundColor, kCZColorWhite) && CZColorEqualToColor(originalForegroundColor, kCZColorYellow)) {
            foregroundColor = kCZColorBlack;
        } else {
            foregroundColor = originalForegroundColor;
        }
    }
    
    return foregroundColor;
}


#pragma mark - CZGlobalSettingsGroupViewController

- (BOOL)areSettingsChanged {
    CZDefaultSettings *defaultSettings = [CZDefaultSettings sharedInstance];
    return ([self measurementUnitForIndex:self.measurementUnitDropdown.selectedIndex] != defaultSettings.unitStyle ||
            self.showMeasurementsByDefaultSwitch.isOn != defaultSettings.enableMeasurement ||
            !CZColorEqualToColor(self.defaultColorPickerView.selectedColor, defaultSettings.color) ||
            !CZColorEqualToColor([self selectedTextBackgroundColor], defaultSettings.backgroundColor) ||
            self.disableMeasurementColorCodingSwitch.isOn != defaultSettings.disableMeasurementColorCoding ||
            self.defaultSizeToggleButton.selectedIndex != defaultSettings.elementSize);
}

- (void)applySettings {
    CZElementUnitStyle unitStyle = [self measurementUnitForIndex:self.measurementUnitDropdown.selectedIndex];
    [[CZDefaultSettings sharedInstance] setUnitStyle:unitStyle];
    [CZElement setUnitStyle:unitStyle];
    
    [[CZDefaultSettings sharedInstance] setEnableMeasurement:self.showMeasurementsByDefaultSwitch.isOn];
    
    [[CZDefaultSettings sharedInstance] setColor:self.defaultColorPickerView.selectedColor];
    
    [[CZDefaultSettings sharedInstance] setBackgroundColor:[self selectedTextBackgroundColor]];
    
    [[CZDefaultSettings sharedInstance] setDisableMeasurementColorCoding:self.disableMeasurementColorCodingSwitch.isOn];
    
    [[CZDefaultSettings sharedInstance] setElementSize:self.defaultSizeToggleButton.selectedIndex];
}

@end
