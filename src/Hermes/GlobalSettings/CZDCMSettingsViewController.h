//
//  CZDCMSettingsViewController.h
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZGlobalSettingsGroupViewController.h"

@interface CZDCMSettingsViewController : UIViewController <CZGlobalSettingsGroupViewController>

@end
