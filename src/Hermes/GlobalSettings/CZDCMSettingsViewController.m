//
//  CZDCMSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDCMSettingsViewController.h"
#import "CZGridView.h"

@interface CZDCMSettingsViewController ()

@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) CZGridView *gridView;
@property (nonatomic, readonly, strong) UILabel *serverIPAddressLabel;
@property (nonatomic, readonly, strong) CZTextField *serverIPAddressTextField;

@end

@implementation CZDCMSettingsViewController

@synthesize titleLabel = _titleLabel;
@synthesize gridView = _gridView;
@synthesize serverIPAddressLabel = _serverIPAddressLabel;
@synthesize serverIPAddressTextField = _serverIPAddressTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.gridView];
    [self.gridView addContentView:self.serverIPAddressLabel];
    [self.gridView addContentView:self.serverIPAddressTextField];
    
    [self.titleLabel.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.titleLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:27.0].active = YES;
    
    [self.gridView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.gridView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:59.0].active = YES;
}

#pragma mark - Views

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.text = L(@"GROUP_DCM");
        _titleLabel.textColor = [UIColor cz_gs20];
    }
    return _titleLabel;
}

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:128.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:304.0 alignment:CZGridColumnAlignmentFill];
        
        _gridView = [[CZGridView alloc] initWithRows:@[row0]
                                             columns:@[column0, column1]];
        _gridView.translatesAutoresizingMaskIntoConstraints = NO;
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 16.0;
    }
    return _gridView;
}

- (UILabel *)serverIPAddressLabel {
    if (_serverIPAddressLabel == nil) {
        _serverIPAddressLabel = [[UILabel alloc] init];
        _serverIPAddressLabel.font = [UIFont cz_label1];
        _serverIPAddressLabel.text = L(@"DCM_IP_ADDRESS");
        _serverIPAddressLabel.textColor = [UIColor cz_gs80];
        _serverIPAddressLabel.numberOfLines = 2;
    }
    return _serverIPAddressLabel;
}

- (CZTextField *)serverIPAddressTextField {
    if (_serverIPAddressTextField == nil) {
        _serverIPAddressTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _serverIPAddressTextField.text = [[NSUserDefaults standardUserDefaults] stringForKey:kDigitalClassroomHost];
    }
    return _serverIPAddressTextField;
}

#pragma mark - CZGlobalSettingsGroupViewController

- (BOOL)isServerIPAddressChanged {
    NSString *old = [[NSUserDefaults standardUserDefaults] stringForKey:kDigitalClassroomHost];
    NSString *new = self.serverIPAddressTextField.text;
    if (old.length == 0 && new.length == 0) {
        return NO;
    } else if (old.length == 0 && new.length > 0) {
        return YES;
    } else if (old.length > 0 && new.length == 0) {
        return YES;
    } else {
        return ![old isEqualToString:new];
    }
}

- (BOOL)areSettingsChanged {
    return [self isServerIPAddressChanged];
}

- (void)applySettings {
    [[NSUserDefaults standardUserDefaults] setObject:self.serverIPAddressTextField.text forKey:kDigitalClassroomHost];
}

@end
