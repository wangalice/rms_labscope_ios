//
//  CZModule.h
//  Hermes
//
//  Created by Li, Junlin on 6/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const CZModuleDidActivateNotification;
extern NSString * const CZModuleDidDeactivateNotification;
extern NSString * const CZModuleDidInstallNotification;
extern NSString * const CZModuleDidUninstallNotification;

@interface CZModule : NSObject

@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, assign, getter=isActive) BOOL active;
@property (nonatomic, readonly, assign, getter=isInstalled) BOOL installed;

- (instancetype)initWithName:(NSString *)name isActive:(BOOL)isActive isInstalled:(BOOL)isInstalled;

- (void)activate;
- (void)deactivate;

- (void)install;
- (void)uninstall;

@end
