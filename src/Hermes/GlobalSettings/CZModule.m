//
//  CZModule.m
//  Hermes
//
//  Created by Li, Junlin on 6/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZModule.h"

NSString * const CZModuleDidActivateNotification = @"CZModuleDidActivateNotification";
NSString * const CZModuleDidDeactivateNotification = @"CZModuleDidDeactivateNotification";
NSString * const CZModuleDidInstallNotification = @"CZModuleDidInstallNotification";
NSString * const CZModuleDidUninstallNotification = @"CZModuleDidUninstallNotification";

@interface CZModule ()

@property (nonatomic, readwrite, assign, getter=isActive) BOOL active;
@property (nonatomic, readwrite, assign, getter=isInstalled) BOOL installed;

@end

@implementation CZModule

- (instancetype)initWithName:(NSString *)name isActive:(BOOL)isActive isInstalled:(BOOL)isInstalled {
    self = [super init];
    if (self) {
        _name = [name copy];
        _active = isActive;
        _installed = isInstalled;
    }
    return self;
}

- (void)activate {
    self.active = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:CZModuleDidActivateNotification object:self];
}

- (void)deactivate {
    self.active = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:CZModuleDidDeactivateNotification object:self];
}

- (void)install {
    self.installed = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:CZModuleDidInstallNotification object:self];
}

- (void)uninstall {
    self.installed = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:CZModuleDidUninstallNotification object:self];
}

@end
