//
//  CZModuleManagerViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZModuleManagerViewController.h"
#import "CZModule.h"
#import "CZModuleCell.h"

@interface CZModuleManagerViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readonly, copy) NSArray<CZModule *> *modules;
@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UITableView *tableView;

@end

@implementation CZModuleManagerViewController

@synthesize titleLabel = _titleLabel;
@synthesize tableView = _tableView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        CZModule *dcmModule = [[CZModule alloc] initWithName:L(@"GROUP_DCM") isActive:NO isInstalled:NO];
        _modules = @[dcmModule];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.tableView];
    
    [self.tableView reloadData];
}

#pragma mark - Views

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0, 27.0, 448.0, 21.0)];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        _titleLabel.font = [UIFont cz_subtitle1];
        _titleLabel.text = L(@"AVAILABLE_MODULES");
        _titleLabel.textColor = [UIColor cz_gs20];
    }
    return _titleLabel;
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 42.0, self.view.bounds.size.width, self.view.bounds.size.height - 42.0 * 2) style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CZModuleCell class] forCellReuseIdentifier:@"ModuleCell"];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.modules.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZModuleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ModuleCell" forIndexPath:indexPath];
    cell.module = self.modules[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0;
}

@end
