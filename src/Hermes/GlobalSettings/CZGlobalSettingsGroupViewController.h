//
//  CZGlobalSettingsAppliable.h
//  Hermes
//
//  Created by Li, Junlin on 6/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CZGlobalSettingsGroupViewController <NSObject>

@required
- (BOOL)areSettingsChanged;
- (void)applySettings;

@end
