//
//  CZModuleCell.h
//  Hermes
//
//  Created by Li, Junlin on 6/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZModule;

@interface CZModuleCell : UITableViewCell

@property (nonatomic, readonly, strong) UILabel *moduleNameLabel;
@property (nonatomic, readonly, strong) CZSwitch *activateSwitch;
@property (nonatomic, readonly, strong) CZButton *installButton;
@property (nonatomic, readonly, strong) UIView *separator;

@property (nonatomic, strong) CZModule *module;

@end
