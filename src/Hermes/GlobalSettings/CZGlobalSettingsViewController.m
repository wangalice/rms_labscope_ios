//
//  CZGlobalSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 6/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGlobalSettingsViewController.h"
#import "CZGlobalSettingsGroupViewController.h"
#import "CZGeneralSettingsViewController.h"
#import "CZAnnotationSettingsViewController.h"
#import "CZDCMSettingsViewController.h"
#import "CZModuleManagerViewController.h"
#import "CZModule.h"
#import "CZAlertController.h"

NSString * const CZGlobalSettingsDidChangeNotification = @"CZGlobalSettingsDidChangeNotification";

NSString * const CZGeneralSettingsGroupIdentifier = @"CZGeneralSettingsGroupIdentifier";
NSString * const CZAnnotationSettingsGroupIdentifier = @"CZAnnotationSettingsGroupIdentifier";
NSString * const CZDCMSettingsGroupIdentifier = @"CZDCMSettingsGroupIdentifier";
NSString * const CZModuleManagerGroupIdentifier = @"CZModuleManagerGroupIdentifier";

@interface CZGlobalSettingsGroup : NSObject

@property (nonatomic, readonly, copy) NSString *identifier;
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, strong) UIViewController *viewController;

@end

@implementation CZGlobalSettingsGroup

- (instancetype)initWithIdentifier:(NSString *)identifier name:(NSString *)name {
    self = [super init];
    if (self) {
        _identifier = [identifier copy];
        _name = [name copy];
    }
    return self;
}

@end

@interface CZGlobalSettingsViewController ()

@property (nonatomic, copy) NSMutableArray<CZGlobalSettingsGroup *> *groups;
@property (nonatomic, strong) CZGlobalSettingsGroup *currentGroup;
@property (nonatomic, readonly, strong) CZToggleButton *groupToggleButton;

@end

@implementation CZGlobalSettingsViewController

@synthesize groupToggleButton = _groupToggleButton;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        CZGlobalSettingsGroup *generalSettingsGroup = [[CZGlobalSettingsGroup alloc] initWithIdentifier:CZGeneralSettingsGroupIdentifier name:L(@"GROUP_GENERAL")];
        CZGlobalSettingsGroup *annotationSettingsGroup = [[CZGlobalSettingsGroup alloc] initWithIdentifier:CZAnnotationSettingsGroupIdentifier name:L(@"GROUP_ANNOTATION")];
        CZGlobalSettingsGroup *moduleManagerGroup = [[CZGlobalSettingsGroup alloc] initWithIdentifier:CZModuleManagerGroupIdentifier name:L(@"MODULE_MANAGER")];
        _groups = [NSMutableArray arrayWithObjects:generalSettingsGroup, annotationSettingsGroup, nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moduleDidActivate:) name:CZModuleDidActivateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moduleDidDeactivate:) name:CZModuleDidDeactivateNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZModuleDidActivateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZModuleDidDeactivateNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = L(@"GLOBAL_SETTINGS");
    
    CZBarButtonItem *applyButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"APPLY") target:self action:@selector(applyButtonAction:)];
    applyButtonItem.isAccessibilityElement = YES;
    applyButtonItem.accessibilityIdentifier = @"ApplyButton";
    self.navigationItem.rightBarButtonItems = @[applyButtonItem];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    
    [self.view addSubview:self.groupToggleButton];
    
    self.groupToggleButton.selectedIndex = 0;
    [self addChildViewControllerForGroup:self.groups.firstObject];
    self.currentGroup = self.groups.firstObject;
}

- (void)backBarButtonItemAction:(id)sender {
    BOOL areSettingsChanged = NO;
    for (CZGlobalSettingsGroup *group in self.groups) {
        if (group.viewController && [group.viewController conformsToProtocol:@protocol(CZGlobalSettingsGroupViewController)]) {
            id<CZGlobalSettingsGroupViewController> groupViewController = (id<CZGlobalSettingsGroupViewController>)group.viewController;
            areSettingsChanged |= [groupViewController areSettingsChanged];
        }
    }
    
    if (areSettingsChanged == NO) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:nil level:CZAlertLevelWarning];
    @weakify(self);
    [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDestructive handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self applyButtonAction:nil];
    }];
    [alert presentAnimated:YES completion:nil];
}

#pragma mark - Views

- (CZToggleButton *)groupToggleButton {
    if (_groupToggleButton == nil) {
        NSMutableArray<NSString *> *items = [NSMutableArray arrayWithCapacity:self.groups.count];
        for (CZGlobalSettingsGroup *group in self.groups) {
            [items addObject:group.name];
        }
        _groupToggleButton = [[CZToggleButton alloc] initWithItems:items];
        _groupToggleButton.frame = CGRectMake(32.0, 32.0, self.view.bounds.size.width - 32.0 * 2, 48.0);
        _groupToggleButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        [_groupToggleButton addTarget:self action:@selector(groupToggleButtonAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _groupToggleButton;
}

- (void)addChildViewControllerForGroup:(CZGlobalSettingsGroup *)group {
    if (group.viewController == nil) {
        if ([group.identifier isEqualToString:CZGeneralSettingsGroupIdentifier]) {
            group.viewController = [[CZGeneralSettingsViewController alloc] init];
        } else if ([group.identifier isEqualToString:CZAnnotationSettingsGroupIdentifier]) {
            group.viewController = [[CZAnnotationSettingsViewController alloc] init];
        } else if ([group.identifier isEqualToString:CZDCMSettingsGroupIdentifier]) {
            group.viewController = [[CZDCMSettingsViewController alloc] init];
        } else if ([group.identifier isEqualToString:CZModuleManagerGroupIdentifier]) {
            group.viewController = [[CZModuleManagerViewController alloc] init];
        }
    }
    
    [self addChildViewController:group.viewController];
    
    group.viewController.view.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(80.0, 0.0, 0.0, 0.0));
    group.viewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:group.viewController.view];
    
    [group.viewController didMoveToParentViewController:self];
}

- (void)removeCurrentViewController {
    if (self.currentGroup.viewController) {
        [self.currentGroup.viewController willMoveToParentViewController:nil];
        [self.currentGroup.viewController.view removeFromSuperview];
        [self.currentGroup.viewController removeFromParentViewController];
        self.currentGroup = nil;
    }
}

#pragma mark - Actions

- (void)groupToggleButtonAction:(id)sender {
    [self removeCurrentViewController];
    CZGlobalSettingsGroup *group = self.groups[self.groupToggleButton.selectedIndex];
    [self addChildViewControllerForGroup:group];
    self.currentGroup = group;
}

- (void)applyButtonAction:(id)sender {
    for (CZGlobalSettingsGroup *group in self.groups) {
        if (group.viewController && [group.viewController conformsToProtocol:@protocol(CZGlobalSettingsGroupViewController)]) {
            id<CZGlobalSettingsGroupViewController> groupViewController = (id<CZGlobalSettingsGroupViewController>)group.viewController;
            [groupViewController applySettings];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZGlobalSettingsDidChangeNotification object:self];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Notification

- (void)moduleDidActivate:(NSNotification *)note {
    CZGlobalSettingsGroup *dcmSettingsGroup = [[CZGlobalSettingsGroup alloc] initWithIdentifier:CZDCMSettingsGroupIdentifier name:L(@"GROUP_DCM")];
    [self.groups insertObject:dcmSettingsGroup atIndex:2];
    [_groupToggleButton removeFromSuperview];
    _groupToggleButton = nil;
    [self.view addSubview:self.groupToggleButton];
    self.groupToggleButton.selectedIndex = 3;
}

- (void)moduleDidDeactivate:(NSNotification *)note {
    [self.groups removeObjectAtIndex:2];
    [_groupToggleButton removeFromSuperview];
    _groupToggleButton = nil;
    [self.view addSubview:self.groupToggleButton];
    self.groupToggleButton.selectedIndex = 2;
}

@end
