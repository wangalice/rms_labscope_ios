//
//  CZModuleCell.m
//  Hermes
//
//  Created by Li, Junlin on 6/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZModuleCell.h"
#import "CZModule.h"

@implementation CZModuleCell

@synthesize moduleNameLabel = _moduleNameLabel;
@synthesize activateSwitch = _activateSwitch;
@synthesize installButton = _installButton;
@synthesize separator = _separator;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.moduleNameLabel];
        [self.contentView addSubview:self.activateSwitch];
        [self.contentView addSubview:self.installButton];
        [self.contentView addSubview:self.separator];
        
        [self.moduleNameLabel.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:32.0].active = YES;
        [self.moduleNameLabel.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
        
        [self.activateSwitch.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor constant:176.0].active = YES;
        [self.activateSwitch.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
        
        [self.installButton.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor constant:-32.0].active = YES;
        [self.installButton.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
    }
    return self;
}

- (void)setModule:(CZModule *)module {
    _module = module;
    
    self.moduleNameLabel.text = self.module.name;
    self.activateSwitch.on = self.module.isActive;
    [self.installButton setTitle:self.module.isInstalled ? L(@"UNINSTALL") : L(@"INSTALL") forState:UIControlStateNormal];
}

#pragma mark - Views

- (UILabel *)moduleNameLabel {
    if (_moduleNameLabel == nil) {
        _moduleNameLabel = [[UILabel alloc] init];
        _moduleNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _moduleNameLabel.font = [UIFont cz_label1];
        _moduleNameLabel.textColor = [UIColor cz_gs80];
    }
    return _moduleNameLabel;
}

- (CZSwitch *)activateSwitch {
    if (_activateSwitch == nil) {
        _activateSwitch = [[CZSwitch alloc] initWithStyle:CZSwitchStyleLarge];
        _activateSwitch.translatesAutoresizingMaskIntoConstraints = NO;
        [_activateSwitch setLevel:CZControlEmphasisActivePrimary];
        [_activateSwitch addTarget:self action:@selector(activateSwitchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _activateSwitch;
}

- (CZButton *)installButton {
    if (_installButton == nil) {
        _installButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        _installButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_installButton.widthAnchor constraintEqualToConstant:120.0].active = YES;
        [_installButton.heightAnchor constraintEqualToConstant:32.0].active = YES;
        [_installButton addTarget:self action:@selector(installButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _installButton;
}

- (UIView *)separator {
    if (_separator == nil) {
        _separator = [[UIView alloc] initWithFrame:CGRectMake(32.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width - 32.0 * 2, 1.0)];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separator.backgroundColor = [UIColor cz_gs110];
    }
    return _separator;
}

#pragma mark - Actions

- (void)activateSwitchAction:(id)sender {
    if (self.module.isActive) {
        [self.module deactivate];
    } else {
        [self.module activate];
    }
}

- (void)installButtonAction:(id)sender {
    if (self.module.isInstalled) {
        [self.module uninstall];
        [self.installButton setTitle:L(@"INSTALL") forState:UIControlStateNormal];
    } else {
        [self.module install];
        [self.installButton setTitle:L(@"UNINSTALL") forState:UIControlStateNormal];
    }
}

@end
