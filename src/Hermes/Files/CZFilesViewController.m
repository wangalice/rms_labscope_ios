//
//  CZFilesViewController.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFilesViewController.h"
#import "CZMultitaskViewController.h"
#import "CZLocalFilesController.h"
#import "CZRemoteFilesController.h"
#import "CZFilesViewModel.h"
#import "CZLocalFilesViewModel.h"
#import "CZRemoteFilesViewModel.h"
#import "CZFileInfoViewController.h"
#import "CZPostOffice.h"
#import "CZPopoverBackgroundView.h"
#import <Photos/Photos.h>
#import "CZReportTemplateViewController.h"
#import "CZAlertController.h"
#import "CZPopoverController.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "UIImagePickerController+Autorotate.h"

@interface CZFilesViewController ()<
CZFilesViewModelDelegate,
CZFileInfoViewControllerDelegate,
CZPostOfficeDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
UIPopoverPresentationControllerDelegate,
CZReportTemplateViewControllerDataSource
>

@property (nonatomic, strong) UIView *fileListSeperator;
@property (nonatomic, strong) CZLocalFilesController *localFilesController;
@property (nonatomic, strong) CZRemoteFilesController *remoteFilesController;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) CZPostOffice *postOffice;

@end

@implementation CZFilesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    [self.view addSubview:self.fileListSeperator];

    [self addChildViewController:self.localFilesController];
//    [self addChildViewController:self.remoteFilesController];
    [self.view addSubview:self.localFilesController.view];
//    [self.view addSubview:self.remoteFilesController.view];
    [self.localFilesController didMoveToParentViewController:self];
//    [self.remoteFilesController didMoveToParentViewController:self];
    
    // Frame
    self.localFilesController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - CZMainToolbarHeight);
//    self.remoteFilesController.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height - CZMainToolbarHeight);

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackgroud:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
}

#pragma mark - Private method

- (void)importToolbarItemAction:(CZToolbarItem *)item {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.preferredContentSize = CGSizeMake(472.0, 472.0);
    imagePickerController.modalPresentationStyle = UIModalPresentationPopover;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [imagePickerController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [imagePickerController.navigationBar setBarTintColor:[UIColor cz_gs105]];

    if (@available(iOS 11.0, *)) {
        CZPopoverController *popoverController = [[CZPopoverController alloc] initWithContentViewController:imagePickerController];
        [popoverController presentFromToolbarItem:item animated:YES completion:nil];
    } else {
        UIPopoverPresentationController *popoverPresentationController = imagePickerController.popoverPresentationController;
        popoverPresentationController.sourceRect = CGRectOffset(item.view.frame, 0, -12);
        popoverPresentationController.sourceView = item.view;
        popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    
    self.imagePickerController = imagePickerController;
}

- (void)deleteToolbarItemAction:(CZToolbarItem *)item {
    [self.task.viewModel removeSelectedFiles];
}

- (void)cutToolbarItemAction:(CZToolbarItem *)item {
    
}

- (void)copyToolbarItemAction:(CZToolbarItem *)item {
    
}

- (void)reportToolbarItemAction:(CZToolbarItem *)item {
    if ([self.task.viewModel containsUnsupportedFilesForReporting]) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"REPORT_UNSUPPORT_IMAGE_ERROR") level:CZAlertLevelWarning];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    if ([self.task.viewModel exceedMaximumLimitationForReporting]) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"REPORT_INFO_SELECTION_OVER_MAX_COUNT") level:CZAlertLevelWarning];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            CZReportTemplateViewController *reportTemplateViewController = [[CZReportTemplateViewController alloc] init];
            reportTemplateViewController.editDataSource = self;
            [self.navigationController pushViewController:reportTemplateViewController animated:YES];
        }];
        [alert presentAnimated:YES completion:nil];
    } else {
        CZReportTemplateViewController *reportTemplateViewController = [[CZReportTemplateViewController alloc] init];
        reportTemplateViewController.editDataSource = self;
        [self.navigationController pushViewController:reportTemplateViewController animated:YES];
    }
}

#pragma mark - Notification

- (void)appWillEnterForeground:(NSNotification *)notification {
    [self.task.viewModel clearAllSelection];
}

- (void)appDidEnterBackgroud:(NSNotification *)notification {
    
}

#pragma mark - CZTaskDelegate

- (instancetype)initWithTask:(CZFilesTask *)task {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        NSAssert([task isKindOfClass:[CZFilesTask class]], @"%@ is not a files task", task);
        _task = task;
        
        _postOffice = [[CZPostOffice alloc] init];
        _postOffice.convertToJPGEnabled = YES;
        _postOffice.delegate = self;
    }
    return self;
}

- (UIViewController *)taskViewController {
    return self;
}

- (void)taskDidEnterForeground:(CZTask *)task {
    [self filesViewSelectedFilesDidChanged];
}

- (void)taskWillEnterBackground:(CZTask *)task completionHandler:(CZTaskEventCompletionHandler)completionHandler {
    //Clear all selection when user switch task.
    [self.task.viewModel clearAllSelection];
    completionHandler(CZTaskEventAllow);
}

#pragma mark - CZMainToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarPrimaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    NSMutableArray *items = [NSMutableArray arrayWithArray:@[CZToolbarItemIdentifierImport]];

    if ([self.task.viewModel enableFileOperations]) {
        [items addObjectsFromArray:@[CZToolbarItemIdentifierCut,
                                     CZToolbarItemIdentifierCopy,
                                     CZToolbarItemIdentifierDelete,
                                     CZToolbarItemIdentifierShare]];
    }
    
    if ([self.task.viewModel enableFileReport]) {
        [items addObject:CZToolbarItemIdentifierReport];
    }
    
    if ([self.task.viewModel enableShowMetadata]) {
        [items addObject:CZToolbarItemIdentifierFileInfo];
    }
    
    return items;
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarSecondaryLeadingItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)mainToolbarCenterItemIdentifiers:(CZToolbar *)mainToolbar {
    return nil;
}

#pragma mark - CZVirtualToolbarDataSource

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarLeadingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarCenterItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

- (NSArray<CZToolbarItemIdentifier> *)virtualToolbarTrailingItemIdentifiers:(CZToolbar *)virtualToolbar {
    return nil;
}

#pragma mark - CZToolbarDelegate

- (CZToolbarItem *)toolbar:(CZToolbar *)toolbar makeItemForIdentifier:(CZToolbarItemIdentifier)identifier {
    return [super toolbar:toolbar makeItemForIdentifier:identifier];
}

- (void)toolbar:(CZToolbar *)toolbar willDisplayItem:(CZToolbarItem *)item {
}

#pragma mark - CZToolbarResponder

- (BOOL)toolbar:(CZToolbar *)toolbar performActionForItem:(CZToolbarItem *)item {
    if ([item.identifier isEqualToString:CZToolbarItemIdentifierImport]) {
        [self importToolbarItemAction:item];
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierCut]) {
        [self cutToolbarItemAction:item];
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierCopy]) {
        [self copyToolbarItemAction:item];
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierDelete]) {
        [self deleteToolbarItemAction:item];
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierShare]) {
        NSArray<CZFileEntity *> *selectedFiles = self.task.viewModel.localFilesViewModel.selectedFiles;
        if (selectedFiles.count > 0) {
            [self.postOffice reset];
            for (CZFileEntity *fileEntity in selectedFiles) {
                [self.postOffice addShareItemFromPath:fileEntity.filePath];
            }
            [self.postOffice presentActivityViewControllerFromToolbarItem:item];
#if FILE_REFACTOR
            self.fileViewControllerLocalDelegate.state = kBusy;
#endif
        }
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierFileInfo]) {
        CZFileEntity *fileEntity = self.task.viewModel.localFilesViewModel.selectedFiles[0];
        NSString *filePath = fileEntity.filePath;
        CZFileInfoViewController *fileInfoViewController = [[CZFileInfoViewController alloc] initWithFileAtPath:filePath];
        fileInfoViewController.delegate = self;
        [self presentViewController:fileInfoViewController animated:YES completion:nil];
        return YES;
    } else if ([item.identifier isEqualToString:CZToolbarItemIdentifierReport]) {
        [self reportToolbarItemAction:item];
        return YES;
    }
    return NO;
}

#pragma mark - CZFilesViewModelDelegate

- (void)localFilesViewDidEnterFullScreen:(BOOL)fullScreen {
    
}

- (void)filesViewDidEnterConfiguration:(BOOL)isConfigurating {
    
}

- (void)filesViewSelectedFilesDidChanged {
    [self setNeedsMainToolbarItemsUpdate];
}

#pragma mark - CZFileInfoViewControllerDelegate

- (void)fileInfoViewController:(CZFileInfoViewController *)fileInfoViewController didRenameFileAtPath:(NSString *)path toName:(NSString *)name {
    // TODO: Re-sort file list and scroll the view to make the file in the center.
}

#pragma mark - CZPostOfficeDelegate

- (void)postOfficeDidDismissActivityController:(CZPostOffice *)postOffice {
#if FILE_REFACTOR
    self.fileViewControllerLocalDelegate.state = kFree;

    if (!postOffice.failed) {
        [self localFilesDoneAction:NO];
    }
#endif
}

#pragma mark - CZReportTemplateViewControllerDataSource
- (CZDocManager *)reportMainDocManager {
    return [self.task.viewModel reportMainDocManager];
}

- (NSArray <NSString *> *)reportImagesFilePath {
    return [[self.task.viewModel reportFiles] copy];
}

#pragma mark - UIImagePickerViewControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    [self.imagePickerController dismissViewControllerAnimated:YES completion:nil];
    
    PHAsset *asset = nil;
    if (@available(iOS 11.0, *)) {
        asset = info[UIImagePickerControllerPHAsset];
    } else {
        NSURL *url = info[UIImagePickerControllerReferenceURL];
        if (url) {
            PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[url] options:nil];
            asset = result.firstObject;
        }
    }
    
    if (asset) {
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.version = PHImageRequestOptionsVersionOriginal;
        options.networkAccessAllowed = YES;
        
        @weakify(self);
        [[PHImageManager defaultManager] requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            @strongify(self);
            if (imageData) {
                UIImage *image = [UIImage imageWithData:imageData];
                CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)imageData, NULL);
                NSDictionary *imageMetaData = CFBridgingRelease(CGImageSourceCopyPropertiesAtIndex(source, 0, NULL));
                CZLogv(@"imageMetaData %@", imageMetaData);
                
                CZImageProperties *imageProperties = [[CZImageProperties alloc] init];
                BOOL parseSuccess = [imageProperties readMetadataFromDict:imageMetaData];
                if (parseSuccess) {
                    imageData = [imageProperties JPEGRepresentationOfImage:image compression:1.0];
                } else {
                    imageData = UIImageJPEGRepresentation(image, 1.0);
                }
                
                [self.task.viewModel writeToLocalDirectoryWithImageData:imageData];
                
                CFRelease(source);
            } else {
                UIImage *image = info[UIImagePickerControllerOriginalImage];

                if (image) {
                    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
                    [self.task.viewModel writeToLocalDirectoryWithImageData:imageData];
                }
            }
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Setters

#pragma mark - Getters

- (CZLocalFilesController *)localFilesController {
    if (!_localFilesController) {
        _localFilesController = [[CZLocalFilesController alloc] initWithViewModel:_task.viewModel.localFilesViewModel];
    }
    return _localFilesController;
}

- (CZRemoteFilesController *)remoteFilesController {
    if (!_remoteFilesController) {
        _remoteFilesController = [[CZRemoteFilesController alloc] initWithViewModel:_task.viewModel.remoteFilesViewModel];
    }
    return _remoteFilesController;
}

- (UIView *)fileListSeperator {
    if (!_fileListSeperator) {
        _fileListSeperator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, [UIScreen mainScreen].bounds.size.height)];
        _fileListSeperator.backgroundColor = [UIColor cz_gs120];
        _fileListSeperator.isAccessibilityElement = YES;
        _fileListSeperator.hidden = YES;
        _fileListSeperator.accessibilityIdentifier = @"FileListSeperator";
    }
    return _fileListSeperator;
}

@end
