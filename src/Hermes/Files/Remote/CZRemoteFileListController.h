//
//  CZRemoteFileListController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZRemoteFileListController : UITableViewController

@end

NS_ASSUME_NONNULL_END
