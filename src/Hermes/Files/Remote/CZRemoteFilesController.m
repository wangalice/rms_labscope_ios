//
//  CZRemoteFilesController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZRemoteFilesController.h"

@interface CZRemoteFilesController ()

@property (nonatomic, strong) CZRemoteFilesViewModel *viewModel;

@end

@implementation CZRemoteFilesController

- (instancetype)initWithViewModel:(CZRemoteFilesViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)dealloc {
    CZLogv(@"Works well.");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

@end
