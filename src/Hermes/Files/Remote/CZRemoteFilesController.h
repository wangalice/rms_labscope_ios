//
//  CZRemoteFilesController.h
//  Hermes
//
//  Created by Sun on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZRemoteFilesViewModel;
@interface CZRemoteFilesController : UIViewController

- (instancetype)initWithViewModel:(CZRemoteFilesViewModel *)viewModel;

@end

NS_ASSUME_NONNULL_END
