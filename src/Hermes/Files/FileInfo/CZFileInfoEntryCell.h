//
//  CZFileInfoEntryCell.h
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileInfoEntryCell;

@protocol CZFileInfoEntryCellDelegate <NSObject>

@optional
- (BOOL)fileInfoEntryCell:(CZFileInfoEntryCell *)cell validateEntryValue:(NSString *)entryValue;
- (void)fileInfoEntryCell:(CZFileInfoEntryCell *)cell didFinishEditingEntryValue:(NSString *)entryValue;;

@end

@interface CZFileInfoEntryCell : UITableViewCell

@property (nonatomic, weak) id<CZFileInfoEntryCellDelegate> delegate;
@property (nonatomic, assign, getter=isEditable) BOOL editable;
@property (nonatomic, readonly, strong) UILabel *entryNameLabel;
@property (nonatomic, readonly, strong) UILabel *entryValueLabel;
@property (nonatomic, readonly, strong) CZTextField *entryValueTextField;
@property (nonatomic, readonly, strong) CZButton *entryValueEditButton;
@property (nonatomic, readonly, strong) UIView *separator;

@end
