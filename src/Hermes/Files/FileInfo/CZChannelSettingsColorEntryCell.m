//
//  CZChannelSettingsColorEntryCell.m
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelSettingsColorEntryCell.h"

@implementation CZChannelSettingsColorEntryCell

@synthesize entryNameLabel = _entryNameLabel;
@synthesize separator = _separator;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor cz_gs20];
        
        [self.contentView addSubview:self.entryNameLabel];
        [self.contentView addSubview:self.separator];
        
        NSMutableArray<UIView *> *entryValueViews = [NSMutableArray array];
        for (NSInteger index = 0; index < 5; index++) {
            UIView *entryValueView = [self makeEntryValueViewWithIndex:index];
            [entryValueViews addObject:entryValueView];
            [self.contentView addSubview:entryValueView];
        }
        _entryValueViews = [entryValueViews copy];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (UIView *entryValueView in self.entryValueViews) {
        CGPoint center = entryValueView.center;
        center.y = self.contentView.bounds.size.height / 2;
        entryValueView.center = center;
    }
}

#pragma mark - Views

- (UILabel *)entryNameLabel {
    if (_entryNameLabel == nil) {
        _entryNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0, 0.0, 160.0, self.contentView.bounds.size.height)];
        _entryNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        _entryNameLabel.backgroundColor = [UIColor clearColor];
        _entryNameLabel.font = [UIFont cz_label1];
        _entryNameLabel.textColor = [UIColor cz_gs80];
    }
    return _entryNameLabel;
}

- (UIView *)makeEntryValueViewWithIndex:(NSInteger)index {
    UIView *entryValueView = [[UIView alloc] initWithFrame:CGRectMake(200.0 + 100.0 * index, (self.contentView.bounds.size.height - 24.0) / 2, 24.0, 24.0)];
    entryValueView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    entryValueView.layer.cornerRadius = 12.0;
    return entryValueView;
}

- (UIView *)separator {
    if (_separator == nil) {
        _separator = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width, 1.0)];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separator.backgroundColor = [UIColor cz_gs40];
    }
    return _separator;
}

@end
