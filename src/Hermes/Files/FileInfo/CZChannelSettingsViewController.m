//
//  CZChannelSettingsViewController.m
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelSettingsViewController.h"
#import "CZChannelSettingsEntry.h"
#import "CZChannelSettingsHeaderView.h"
#import "CZChannelSettingsTextEntryCell.h"
#import "CZChannelSettingsColorEntryCell.h"

@interface CZChannelSettingsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readonly, strong) UITableView *tableView;

@end

@implementation CZChannelSettingsViewController

@synthesize tableView = _tableView;

- (instancetype)initWithEntries:(NSArray<CZChannelSettingsEntry *> *)entries {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _entries = [entries copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

#pragma mark - Views

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor cz_gs20];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.alwaysBounceVertical = NO;
        _tableView.allowsSelection = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CZChannelSettingsHeaderView class] forHeaderFooterViewReuseIdentifier:@"HeaderView"];
        [_tableView registerClass:[CZChannelSettingsTextEntryCell class] forCellReuseIdentifier:@"TextEntryCell"];
        [_tableView registerClass:[CZChannelSettingsColorEntryCell class] forCellReuseIdentifier:@"ColorEntryCell"];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.entries.count - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZChannelSettingsEntry *entry = self.entries[indexPath.row + 1];
    switch (entry.type) {
        case CZChannelSettingsEntryTypeText: {
            CZChannelSettingsTextEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextEntryCell" forIndexPath:indexPath];
            cell.entryNameLabel.text = entry.localizedName;
            [cell.entryValueLabels enumerateObjectsUsingBlock:^(UILabel *entryValueLabel, NSUInteger index, BOOL *stop) {
                if (index < entry.values.count) {
                    entryValueLabel.text = entry.values[index];
                } else {
                    entryValueLabel.text = nil;
                }
            }];
            return cell;
        }
        case CZChannelSettingsEntryTypeColor: {
            CZChannelSettingsColorEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ColorEntryCell" forIndexPath:indexPath];
            cell.entryNameLabel.text = entry.localizedName;
            [cell.entryValueViews enumerateObjectsUsingBlock:^(UIView *entryValueView, NSUInteger index, BOOL *stop) {
                if (index < entry.values.count) {
                    entryValueView.backgroundColor = entry.values[index];
                } else {
                    entryValueView.backgroundColor = [UIColor clearColor];
                }
            }];
            return cell;
        }
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 56.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CZChannelSettingsEntry *channelNameEntry = self.entries.firstObject;
    CZChannelSettingsHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"HeaderView"];
    [view.channelLabels enumerateObjectsUsingBlock:^(UILabel *channelLabel, NSUInteger index, BOOL *stop) {
        if (index < channelNameEntry.values.count) {
            channelLabel.text = channelNameEntry.values[index];
        } else {
            channelLabel.text = nil;
        }
    }];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0;
}

@end
