//
//  CZFileInfoSegmentView.h
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZFileInfoSegmentView : UIView

@property (nonatomic, readonly, strong) UILabel *titleLabel;
@property (nonatomic, readonly, strong) UIView *leftBorder;
@property (nonatomic, readonly, strong) UIView *bottomBorder;
@property (nonatomic, readonly, strong) UIView *selectionIndicator;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

@end
