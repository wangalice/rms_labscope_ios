//
//  CZFileInfoEntriesViewController.m
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileInfoEntriesViewController.h"
#import "CZFileInfoEntry.h"
#import "CZFileInfoEntryCell.h"

@interface CZFileInfoEntriesViewController () <UITableViewDataSource, UITableViewDelegate, CZFileInfoEntryCellDelegate>

@property (nonatomic, readonly, copy) NSString *filePath;
@property (nonatomic, readonly, strong) UITableView *tableView;

@end

@implementation CZFileInfoEntriesViewController

@synthesize tableView = _tableView;

- (instancetype)initWithFileAtPath:(NSString *)path entries:(NSArray<CZFileInfoEntry *> *)entries {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _filePath = [path copy];
        _entries = [entries copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

#pragma mark - Views

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.backgroundColor = [UIColor cz_gs20];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.alwaysBounceVertical = NO;
        _tableView.allowsSelection = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CZFileInfoEntryCell class] forCellReuseIdentifier:@"EntryCell"];
        [_tableView registerClass:[CZFileInfoEntryCell class] forCellReuseIdentifier:@"EditableEntryCell"];
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.entries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileInfoEntry *entry = self.entries[indexPath.row];
    if (entry.isEditable) {
        CZFileInfoEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditableEntryCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.editable = YES;
        cell.entryNameLabel.text = entry.localizedName;
        cell.entryValueLabel.text = entry.value;
        cell.entryValueTextField.errorMessage = L(@"FILE_NAME_CONFLICT_ERROR_MESSAGE");
        return cell;
    } else {
        CZFileInfoEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EntryCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.entryNameLabel.text = entry.localizedName;
        cell.entryValueLabel.text = entry.value;
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0;
}

#pragma mark - CZFileInfoEntryCellDelegate

- (BOOL)fileInfoEntryCell:(CZFileInfoEntryCell *)cell validateEntryValue:(NSString *)entryValue {
    NSString *fileName = [entryValue stringByAppendingPathExtension:self.filePath.pathExtension];
    NSString *filePath = [[[NSFileManager defaultManager] cz_documentPath] stringByAppendingPathComponent:fileName];
    if (![filePath isEqualToString:self.filePath] && [[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)fileInfoEntryCell:(CZFileInfoEntryCell *)cell didFinishEditingEntryValue:(NSString *)entryValue {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    CZFileInfoEntry *entry = self.entries[indexPath.row];
    entry.value = entryValue;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileInfoEntriesViewController:didFinishEditingEntry:)]) {
        [self.delegate fileInfoEntriesViewController:self didFinishEditingEntry:entry];
    }
}

@end
