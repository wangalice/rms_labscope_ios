//
//  CZFileInfoEntriesViewController.h
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZFileInfoEntry;
@class CZFileInfoEntriesViewController;

@protocol CZFileInfoEntriesViewControllerDelegate <NSObject>

@optional
- (void)fileInfoEntriesViewController:(CZFileInfoEntriesViewController *)fileInfoEntriesViewController didFinishEditingEntry:(CZFileInfoEntry *)entry;

@end

@interface CZFileInfoEntriesViewController : UIViewController

@property (nonatomic, weak) id<CZFileInfoEntriesViewControllerDelegate> delegate;
@property (nonatomic, readonly, copy) NSArray<CZFileInfoEntry *> *entries;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithFileAtPath:(NSString *)path entries:(NSArray<CZFileInfoEntry *> *)entries NS_DESIGNATED_INITIALIZER;

@end
