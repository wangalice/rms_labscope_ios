//
//  CZFileInfoViewController.m
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileInfoViewController.h"
#import "CZTabViewController.h"
#import "CZDialogPresentationController.h"
#import "CZFileInfoEntry.h"
#import "CZFileInfoEntriesViewController.h"
#import "CZChannelSettingsEntry.h"
#import "CZChannelSettingsViewController.h"
#import "CZFileInfoSegmentView.h"
#import "CZAlertController.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZIKit/CZIKit.h>
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZFileInfoViewController () <CZTabViewControllerDelegate, CZFileInfoEntriesViewControllerDelegate>

@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, strong) CZImageProperties *imageProperties;
@property (nonatomic, readonly, copy) NSArray<CZFileInfoEntry *> *basicInfoEntries;
@property (nonatomic, readonly, copy) NSArray<CZFileInfoEntry *> *dimensionInfoEntries;
@property (nonatomic, readonly, copy) NSArray<CZChannelSettingsEntry *> *channelSettingsEntries;

@end

@implementation CZFileInfoViewController

@synthesize basicInfoEntries = _basicInfoEntries;
@synthesize dimensionInfoEntries = _dimensionInfoEntries;
@synthesize channelSettingsEntries = _channelSettingsEntries;

- (instancetype)initWithFileAtPath:(NSString *)path {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _filePath = [path copy];
        
        CZDocManager *docManager = [CZDocManager newDocManagerFromFilePath:path];
        _imageProperties = docManager.imageProperties;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentView.widthAnchor constraintEqualToConstant:702.0].active = YES;
    [self.contentView.heightAnchor constraintEqualToConstant:526.0].active = YES;
    
    CZFileInfoEntriesViewController *basicInfoViewController = [[CZFileInfoEntriesViewController alloc] initWithFileAtPath:self.filePath entries:self.basicInfoEntries];
    basicInfoViewController.delegate = self;
    basicInfoViewController.title = L(@"FILES_INFO_TAB_HEADER_FILE_INFO");
    
    CZFileInfoEntriesViewController *dimensionInfoViewController = [[CZFileInfoEntriesViewController alloc] initWithFileAtPath:self.filePath entries:self.dimensionInfoEntries];
    dimensionInfoViewController.delegate = self;
    dimensionInfoViewController.title = L(@"FILES_INFO_TAB_HEADER_IMAGE_DIMENSION");
    
    CZChannelSettingsViewController *channelSettingsViewController = [[CZChannelSettingsViewController alloc] initWithEntries:self.channelSettingsEntries];
    channelSettingsViewController.title = L(@"FILES_INFO_TAB_HEADER_CHA_SETTING");
    
    CZTabViewController *tabViewController = [[CZTabViewController alloc] init];
    tabViewController.delegate = self;
    tabViewController.viewControllers = @[basicInfoViewController, dimensionInfoViewController, channelSettingsViewController];
    [self addChildViewController:tabViewController];
    tabViewController.view.frame = self.contentView.bounds;
    [self.contentView addSubview:tabViewController.view];
    [tabViewController didMoveToParentViewController:self];
}

#pragma mark - File Info Enties

- (NSArray<CZFileInfoEntry *> *)basicInfoEntries {
    if (_basicInfoEntries == nil) {
        NSMutableArray<CZFileInfoEntry *> *entries = [NSMutableArray array];
        
        NSString *fileName = self.filePath.lastPathComponent;
        CZFileInfoEntry *fileNameEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILE_NAME_SECTION_TITLE") value:fileName isEditable:YES];
        [entries addObject:fileNameEntry];
        
        NSString *microscopeManufacturer = self.imageProperties.microscopeManufacturer;
        CZFileInfoEntry *microscopeManufacturerEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_MANUFACTURER") value:microscopeManufacturer];
        [entries addObject:microscopeManufacturerEntry];
        
        NSString *microscopeName = self.imageProperties.microscopeName;
        CZFileInfoEntry *microscopeNameEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"MIC_CONFIG_NAME") value:microscopeName];
        [entries addObject:microscopeNameEntry];
        
        NSString *microscopeModel = self.imageProperties.microscopeModel;
        CZFileInfoEntry *microscopeModelEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_MODEL") value:microscopeModel];
        [entries addObject:microscopeModelEntry];
        
        NSString *cameraModel = self.imageProperties.cameraModel;
        CZFileInfoEntry *cameraModelEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_CAMERA") value:cameraModel];
        [entries addObject:cameraModelEntry];
        
        NSString *acquisitionDate = nil;
        if (self.imageProperties.acquisitionDate) {
            acquisitionDate = [NSDateFormatter localizedStringFromDate:self.imageProperties.acquisitionDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
        }
        CZFileInfoEntry *acquisitionDateEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_ACQUISITION_DATE") value:acquisitionDate];
        [entries addObject:acquisitionDateEntry];
        
        _basicInfoEntries = [entries copy];
    }
    return _basicInfoEntries;
}

- (NSArray<CZFileInfoEntry *> *)dimensionInfoEntries {
    if (_dimensionInfoEntries == nil) {
        NSMutableArray<CZFileInfoEntry *> *entries = [NSMutableArray array];
        
        // objective model
        NSString *objectiveModel = nil;
        if (self.imageProperties.objectiveModel.length > 0) {
            if ([self.imageProperties.objectiveModel isEqualToString:@"None"]) {
                objectiveModel = L(@"NONE");
            } else {
                objectiveModel = self.imageProperties.objectiveModel;
            }
        }
        CZFileInfoEntry *objectiveEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_OBJECTIVE") value:objectiveModel];
        [entries addObject:objectiveEntry];
        
        // zoom
        NSString *zoom = nil;
        if (self.imageProperties.zoom.floatValue > 0) {
            NSString *zoomNumber = [CZCommonUtils localizedStringFromNumber:self.imageProperties.zoom precision:2];
            zoom = [zoomNumber stringByAppendingString:@"x"];
        }
        CZFileInfoEntry *zoomEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_ZOOM") value:zoom];
        [entries addObject:zoomEntry];
        
        // camera adapter
        NSString *cameraAdapter = nil;
        if (self.imageProperties.displayCameraAdapter.length > 0) {
            cameraAdapter = self.imageProperties.displayCameraAdapter;
        } else if (self.imageProperties.cameraAdapter.floatValue > 0) {
            NSString *cameraAdapterNumber = [CZCommonUtils localizedStringFromNumber:self.imageProperties.cameraAdapter precision:2];
            cameraAdapter = [cameraAdapterNumber stringByAppendingString:@"x"];
        }
        CZFileInfoEntry *cameraAdapterEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"MIC_CONFIG_CAMERA_ADAPTER") value:cameraAdapter];
        [entries addObject:cameraAdapterEntry];
        
        // scaling
        NSString *scalingEntryName = L(@"FILES_INFO_SCALING");
        NSString *scalingEntryValue = nil;
        if (self.imageProperties.scaling > 0) {
            BOOL prefersBigUnit = [CZMicroscopeModel shouldPreferBigUnitByModelName:self.imageProperties.microscopeModel];
            NSString *unitString = [CZElement distanceUnitStringByStyle:[CZElement unitStyle] prefersBigUnit:prefersBigUnit];
            CGFloat scaling = [CZElement scaling:self.imageProperties.scaling ofUnitStyle:[CZElement unitStyle] prefersBigUnit:prefersBigUnit];
            CZElementUnitStyle style = [CZElement determineUnitStyle:[CZElement unitStyle] prefersBigUnit:prefersBigUnit];
            NSUInteger precision = 4U;
            switch (style) {
                case CZElementUnitStyleInch:
                    precision += 2;
                    break;
                case CZElementUnitStyleMM:
                    precision += 1;
                    break;
                default:
                    break;
            }
            NSString *scalingValue = [CZCommonUtils localizedStringFromFloat:scaling precision:precision];
            scalingEntryValue = [NSString stringWithFormat:@"%@%@ \u00D7 %@%@", scalingValue, L(unitString), scalingValue, L(unitString)];
        } else if (self.imageProperties.scalingInPixels.integerValue > 0) {
            NSNumber *scalingInPixels = self.imageProperties.scalingInPixels;
            scalingEntryValue = [NSString stringWithFormat:@"%@%@ \u00D7 %@%@", scalingInPixels, L(@"UNIT_PIXELS"), scalingInPixels, L(@"UNIT_PIXELS")];
        }
        CZFileInfoEntry *scalingEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:scalingEntryName value:scalingEntryValue];
        [entries addObject:scalingEntry];
        
        // resolution
        NSString *resolution = nil;
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:self.filePath.pathExtension];
        CGSize imageSize = CGSizeZero;
        if (fileFormat == kCZFileFormatCZI) {
            CZIDocument *czi = [[CZIDocument alloc] initWithFile:self.filePath];
            imageSize = czi.imageSize;
        } else if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
            imageSize = [CZCommonUtils imageSizeFromFilePath:self.filePath];
        }
        if (imageSize.width > 0 && imageSize.height > 0) {
            resolution = [NSString stringWithFormat:@"%ld \u00D7 %ld", (long)imageSize.width, (long)imageSize.height];
        }
        CZFileInfoEntry *resolutionEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_IMAGE_RESOLUTION") value:resolution];
        [entries addObject:resolutionEntry];
        
        // bit depth
        NSString *bitDepth = self.imageProperties.bitDepth ? [NSString stringWithFormat:@"%@ %@", self.imageProperties.bitDepth, L(@"BIT")] : nil;
        CZFileInfoEntry *bitDepthEntry = [[CZFileInfoEntry alloc] initWithLocalizedName:L(@"FILES_INFO_BITDEPTH") value:bitDepth];
        [entries addObject:bitDepthEntry];
        
        _dimensionInfoEntries = [entries copy];
    }
    return _dimensionInfoEntries;
}

- (NSArray<CZChannelSettingsEntry *> *)channelSettingsEntries {
    if (_channelSettingsEntries == nil) {
        NSMutableArray<CZChannelSettingsEntry *> *entries = [NSMutableArray array];
        
        NSArray<CZImageChannelProperties *> *channels = self.imageProperties.channels;
        NSMutableArray<NSString *> *channelNames = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<UIColor *> *channelColors = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<NSString *> *reflectors = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<NSString *> *lightSources = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<NSString *> *wavelengths = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<NSString *> *exposureTimes = [NSMutableArray arrayWithCapacity:channels.count];
        NSMutableArray<NSString *> *gains = [NSMutableArray arrayWithCapacity:channels.count];
        
        for (CZImageChannelProperties *channelProperties in channels) {
            [channelNames addObject:channelProperties.channelName ?: @""];
            [channelColors addObject:channelProperties.channelColor ?: [UIColor clearColor]];
            [reflectors addObject:channelProperties.filterSetName ?: @""];
            [lightSources addObject:channelProperties.lightSource ?: @""];
            [wavelengths addObject:channelProperties.wavelength.integerValue > 0 ? channelProperties.wavelength : @""];
            
            NSString *exposureTime = channelProperties.exposureTimeInSeconds ? [CZCommonUtils localizedStringFromFloat:channelProperties.exposureTimeInSeconds.floatValue * 1e3f precision:2] : @"";
            [exposureTimes addObject:exposureTime];
            
            [gains addObject:channelProperties.gain.stringValue ?: @""];
        }
        
        CZChannelSettingsEntry *channelNameEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_CHANNELNAME") values:channelNames];
        [entries addObject:channelNameEntry];
        
        CZChannelSettingsEntry *channelColorEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_CHANNELCOLOR") values:channelColors type:CZChannelSettingsEntryTypeColor];
        [entries addObject:channelColorEntry];
        
        CZChannelSettingsEntry *reflectorEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"REFLECTOR") values:reflectors];
        [entries addObject:reflectorEntry];
        
        CZChannelSettingsEntry *lightSourceEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_LIGHTSOURCE") values:lightSources];
        [entries addObject:lightSourceEntry];
        
        CZChannelSettingsEntry *excitationWavelength = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_EXCIWAVELEN") values:wavelengths];
        [entries addObject:excitationWavelength];
        
        CZChannelSettingsEntry *exposureTimeEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_EXPOSURE_TIME") values:exposureTimes];
        [entries addObject:exposureTimeEntry];
        
        CZChannelSettingsEntry *gainEntry = [[CZChannelSettingsEntry alloc] initWithLocalizedName:L(@"FILES_INFO_GAIN") values:gains];
        [entries addObject:gainEntry];
        
        _channelSettingsEntries = [entries copy];
    }
    return _channelSettingsEntries;
}

#pragma mark - UIViewControllerTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    CZDialogPresentationController *presentationController = (CZDialogPresentationController *)[super presentationControllerForPresentedViewController:presented presentingViewController:presenting sourceViewController:source];
    presentationController.adjustsDialogPositionWhenKeyboardAppears = NO;
    presentationController.dismissesDialogWhenOutsideTouched = YES;
    return presentationController;
}

#pragma mark - CZTabViewControllerDelegate

- (CGFloat)heightForSegmentsInTabViewController:(CZTabViewController *)tabViewController {
    return 64.0;
}

- (UIView *)tabViewController:(CZTabViewController *)tabViewController viewForSegmentAtIndex:(NSInteger)index {
    CZFileInfoSegmentView *segmentView = [[CZFileInfoSegmentView alloc] init];
    segmentView.titleLabel.text = tabViewController.viewControllers[index].title;
    segmentView.leftBorder.hidden = index == 0;
    return segmentView;
}

- (void)tabViewController:(CZTabViewController *)tabViewController willDeselectView:(UIView *)view forSegmentAtIndex:(NSInteger)index {
    CZFileInfoSegmentView *segmentView = (CZFileInfoSegmentView *)view;
    segmentView.selected = NO;
}

- (void)tabViewController:(CZTabViewController *)tabViewController didSelectView:(UIView *)view forSegmentAtIndex:(NSInteger)index {
    CZFileInfoSegmentView *segmentView = (CZFileInfoSegmentView *)view;
    segmentView.selected = YES;
}

#pragma mark - CZFileInfoEntriesViewControllerDelegate

- (void)fileInfoEntriesViewController:(CZFileInfoEntriesViewController *)fileInfoEntriesViewController didFinishEditingEntry:(CZFileInfoEntry *)entry {
    NSString *newFileName = entry.value;
    NSString *oldFileName = self.filePath.lastPathComponent;
    
    if ([newFileName isEqualToString:oldFileName]) {
        return;
    }
    
    NSString *parentFolder = [self.filePath stringByDeletingLastPathComponent];
    NSString *newFilePath = [parentFolder stringByAppendingPathComponent:newFileName];
    
    CZLogv(@"renamed file from %@ to %@", self.filePath, newFilePath);
    
    if (newFileName.length == 0) {
        return;
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:newFilePath]) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"FILE_NAME_CONFLICT_ERROR_MESSAGE") level:CZAlertLevelError];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    NSError *error = nil;
    [[CZFileManager defaultManager] moveItemAtPath:self.filePath toPath:newFilePath error:&error];
    if (error) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:error.localizedDescription level:CZAlertLevelError];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileInfoViewController:didRenameFileAtPath:toName:)]) {
        [self.delegate fileInfoViewController:self didRenameFileAtPath:self.filePath toName:entry.value];
    }
    
    self.filePath = newFilePath;
}

@end
