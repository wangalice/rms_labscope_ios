//
//  CZFileInfoViewController.h
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZDialogController.h"

@class CZFileInfoViewController;

@protocol CZFileInfoViewControllerDelegate <NSObject>

@optional
- (void)fileInfoViewController:(CZFileInfoViewController *)fileInfoViewController didRenameFileAtPath:(NSString *)path toName:(NSString *)name;

@end

@interface CZFileInfoViewController : CZDialogController

@property (nonatomic, weak) id<CZFileInfoViewControllerDelegate> delegate;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithFileAtPath:(NSString *)path NS_DESIGNATED_INITIALIZER;

@end
