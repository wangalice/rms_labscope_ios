//
//  CZFileInfoEntry.m
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileInfoEntry.h"

@implementation CZFileInfoEntry

- (instancetype)initWithLocalizedName:(NSString *)localizedName value:(NSString *)value {
    return [self initWithLocalizedName:localizedName value:value isEditable:NO];
}

- (instancetype)initWithLocalizedName:(NSString *)localizedName value:(NSString *)value isEditable:(BOOL)isEditable {
    self = [super init];
    if (self) {
        _localizedName = [localizedName copy];
        _value = [value copy];
        _isEditable = isEditable;
    }
    return self;
}

- (void)setValue:(NSString *)value {
    if (self.isEditable) {
        _value = [value copy];
    }
}

@end
