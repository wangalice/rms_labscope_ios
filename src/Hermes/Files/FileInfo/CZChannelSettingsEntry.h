//
//  CZChannelSettingsEntry.h
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CZChannelSettingsEntryType) {
    CZChannelSettingsEntryTypeText,
    CZChannelSettingsEntryTypeColor
};

@interface CZChannelSettingsEntry : NSObject

@property (nonatomic, readonly, copy) NSString *localizedName;
@property (nonatomic, readonly, copy) NSArray *values;
@property (nonatomic, readonly, assign) CZChannelSettingsEntryType type;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithLocalizedName:(NSString *)localizedName values:(NSArray *)values;
- (instancetype)initWithLocalizedName:(NSString *)localizedName values:(NSArray *)values type:(CZChannelSettingsEntryType)type NS_DESIGNATED_INITIALIZER;

@end
