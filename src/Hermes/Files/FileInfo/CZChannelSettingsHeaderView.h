//
//  CZChannelSettingsHeaderView.h
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZChannelSettingsHeaderView : UITableViewHeaderFooterView

@property (nonatomic, readonly, copy) NSArray<UILabel *> *channelLabels;

@end
