//
//  CZChannelSettingsEntry.m
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelSettingsEntry.h"

@implementation CZChannelSettingsEntry

- (instancetype)initWithLocalizedName:(NSString *)localizedName values:(NSArray *)values {
    return [self initWithLocalizedName:localizedName values:values type:CZChannelSettingsEntryTypeText];
}

- (instancetype)initWithLocalizedName:(NSString *)localizedName values:(NSArray *)values type:(CZChannelSettingsEntryType)type {
    self = [super init];
    if (self) {
        _localizedName = [localizedName copy];
        _values = [values copy];
        _type = type;
    }
    return self;
}

@end
