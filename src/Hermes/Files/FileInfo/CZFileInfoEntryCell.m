//
//  CZFileInfoEntryCell.m
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileInfoEntryCell.h"

@interface CZFileInfoEntryCell () <CZTextFieldDelegate>

@property (nonatomic, assign) BOOL isEditingEntryValue;

@end

@implementation CZFileInfoEntryCell

@synthesize entryNameLabel = _entryNameLabel;
@synthesize entryValueLabel = _entryValueLabel;
@synthesize entryValueTextField = _entryValueTextField;
@synthesize entryValueEditButton = _entryValueEditButton;
@synthesize separator = _separator;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor cz_gs20];
        
        [self.contentView addSubview:self.entryNameLabel];
        [self.contentView addSubview:self.entryValueLabel];
        [self.contentView addSubview:self.separator];
    }
    return self;
}

- (void)setEditable:(BOOL)editable {
    _editable = editable;
    
    if (_editable) {
        [self.contentView addSubview:self.entryValueTextField];
        [self.contentView addSubview:self.entryValueEditButton];
    }
}

#pragma mark - Views

- (UILabel *)entryNameLabel {
    if (_entryNameLabel == nil) {
        _entryNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0, 0.0, 300.0, self.contentView.bounds.size.height)];
        _entryNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        _entryNameLabel.backgroundColor = [UIColor clearColor];
        _entryNameLabel.font = [UIFont cz_label1];
        _entryNameLabel.textColor = [UIColor cz_gs80];
    }
    return _entryNameLabel;
}

- (UILabel *)entryValueLabel {
    if (_entryValueLabel == nil) {
        _entryValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(337.0, 0.0, 270.0, self.contentView.bounds.size.height)];
        _entryValueLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _entryValueLabel.backgroundColor = [UIColor clearColor];
        _entryValueLabel.font = [UIFont cz_body1];
        _entryValueLabel.textColor = [UIColor cz_gs100];
    }
    return _entryValueLabel;
}

- (CZTextField *)entryValueTextField {
    if (_entryValueTextField == nil) {
        _entryValueTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault];
        _entryValueTextField.frame = CGRectMake(337.0, 16.0, 270.0, self.contentView.bounds.size.height - 32.0);
        _entryValueTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _entryValueTextField.hidden = YES;
        _entryValueTextField.showClearButtonWhileEditing = YES;
        _entryValueTextField.delegate = self;
        _entryValueTextField.returnKeyType = UIReturnKeyDone;
        [_entryValueTextField addTarget:self action:@selector(entryValueTextFieldEditingChangedAction:) forControlEvents:UIControlEventEditingChanged];
    }
    return _entryValueTextField;
}

- (CZButton *)entryValueEditButton {
    if (_entryValueEditButton == nil) {
        _entryValueEditButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        _entryValueEditButton.frame = CGRectMake(self.contentView.bounds.size.width - 32.0 - 48.0, 16.0, 48.0, self.contentView.bounds.size.height - 32.0);
        _entryValueEditButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
        [_entryValueEditButton cz_setImageWithIcon:[CZIcon iconNamed:@"edit"]];
        [_entryValueEditButton addTarget:self action:@selector(entryValueEditButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _entryValueEditButton;
}

- (UIView *)separator {
    if (_separator == nil) {
        _separator = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width, 1.0)];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separator.backgroundColor = [UIColor cz_gs40];
    }
    return _separator;
}

#pragma mark - Actions

- (void)entryValueTextFieldEditingChangedAction:(id)sender {
    self.entryValueEditButton.enabled = self.entryValueTextField.text.length > 0 && ![self.entryValueTextField.text isEqualToString:self.entryValueLabel.text] && ![self shouldShowErrorMessageWhileTextFieldEditing:self.entryValueTextField];
}

- (void)entryValueEditButtonAction:(id)sender {
    if (self.isEditingEntryValue == NO) {
        self.isEditingEntryValue = YES;
        self.entryValueLabel.hidden = YES;
        self.entryValueTextField.hidden = NO;
        
        self.entryValueTextField.text = [self.entryValueLabel.text stringByDeletingPathExtension];
        [self.entryValueTextField becomeFirstResponder];
    } else {
        self.isEditingEntryValue = NO;
        self.entryValueLabel.hidden = NO;
        self.entryValueTextField.hidden = YES;
        
        self.entryValueLabel.text = [self.entryValueTextField.text stringByAppendingPathExtension:self.entryValueLabel.text.pathExtension];
        [self.entryValueTextField resignFirstResponder];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(fileInfoEntryCell:didFinishEditingEntryValue:)]) {
            [self.delegate fileInfoEntryCell:self didFinishEditingEntryValue:self.entryValueLabel.text];
        }
    }
}

#pragma mark - CZTextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.entryValueEditButton cz_setImageWithIcon:[CZIcon iconNamed:@"checkmark"]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.entryValueEditButton cz_setImageWithIcon:[CZIcon iconNamed:@"edit"]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEditing:(CZTextField *)textField {
    if (textField.text.length == 0) {
        return NO;
    }
    
    BOOL isValid = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileInfoEntryCell:validateEntryValue:)]) {
        isValid = [self.delegate fileInfoEntryCell:self validateEntryValue:textField.text];
    }
    return !isValid;
}

- (BOOL)shouldShowErrorMessageWhileTextFieldEndEditing:(CZTextField *)textField {
    if (textField.text.length == 0) {
        return NO;
    }
    
    BOOL isValid = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileInfoEntryCell:validateEntryValue:)]) {
        isValid = [self.delegate fileInfoEntryCell:self validateEntryValue:textField.text];
    }
    return !isValid;
}

@end
