//
//  CZChannelSettingsViewController.h
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZChannelSettingsEntry;

@interface CZChannelSettingsViewController : UIViewController

@property (nonatomic, readonly, copy) NSArray<CZChannelSettingsEntry *> *entries;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithEntries:(NSArray<CZChannelSettingsEntry *> *)entries NS_DESIGNATED_INITIALIZER;

@end
