//
//  CZChannelSettingsHeaderView.m
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelSettingsHeaderView.h"

@implementation CZChannelSettingsHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor cz_gs20];
        
        NSMutableArray<UILabel *> *channelLabels = [NSMutableArray array];
        for (NSInteger index = 0; index < 5; index++) {
            UILabel *channelLabel = [self makeChannelLabelWithIndex:index];
            [channelLabels addObject:channelLabel];
            [self.contentView addSubview:channelLabel];
        }
        _channelLabels = [channelLabels copy];
    }
    return self;
}

- (UILabel *)makeChannelLabelWithIndex:(NSInteger)index {
    UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(200.0 + 100.0 * index, self.contentView.bounds.size.height - 20.0, 100.0, 16.0)];
    channelLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    channelLabel.backgroundColor = [UIColor clearColor];
    channelLabel.font = [UIFont cz_label1];
    channelLabel.textColor = [UIColor cz_gs80];
    return channelLabel;
}

@end
