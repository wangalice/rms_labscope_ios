//
//  CZFileInfoEntry.h
//  Hermes
//
//  Created by Li, Junlin on 5/23/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZFileInfoEntry : NSObject

@property (nonatomic, readonly, copy) NSString *localizedName;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, readonly, assign) BOOL isEditable;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithLocalizedName:(NSString *)localizedName value:(NSString *)value;
- (instancetype)initWithLocalizedName:(NSString *)localizedName value:(NSString *)value isEditable:(BOOL)isEditable;

@end
