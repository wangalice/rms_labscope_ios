//
//  CZChannelSettingsTextEntryCell.m
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZChannelSettingsTextEntryCell.h"

@implementation CZChannelSettingsTextEntryCell

@synthesize entryNameLabel = _entryNameLabel;
@synthesize separator = _separator;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor cz_gs20];
        
        [self.contentView addSubview:self.entryNameLabel];
        [self.contentView addSubview:self.separator];
        
        NSMutableArray<UILabel *> *entryValueLabels = [NSMutableArray array];
        for (NSInteger index = 0; index < 5; index++) {
            UILabel *entryValueLabel = [self makeEntryValueLabelWithIndex:index];
            [entryValueLabels addObject:entryValueLabel];
            [self.contentView addSubview:entryValueLabel];
        }
        _entryValueLabels = [entryValueLabels copy];
    }
    return self;
}

#pragma mark - Views

- (UILabel *)entryNameLabel {
    if (_entryNameLabel == nil) {
        _entryNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0, 0.0, 160.0, self.contentView.bounds.size.height)];
        _entryNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        _entryNameLabel.backgroundColor = [UIColor clearColor];
        _entryNameLabel.font = [UIFont cz_label1];
        _entryNameLabel.textColor = [UIColor cz_gs80];
        _entryNameLabel.numberOfLines = 2;
    }
    return _entryNameLabel;
}

- (UILabel *)makeEntryValueLabelWithIndex:(NSInteger)index {
    UILabel *entryValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(200.0 + 100.0 * index, 0.0, 100.0, self.contentView.bounds.size.height)];
    entryValueLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    entryValueLabel.backgroundColor = [UIColor clearColor];
    entryValueLabel.font = [UIFont cz_body1];
    entryValueLabel.textColor = [UIColor cz_gs100];
    entryValueLabel.numberOfLines = 2;
    return entryValueLabel;
}

- (UIView *)separator {
    if (_separator == nil) {
        _separator = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width, 1.0)];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separator.backgroundColor = [UIColor cz_gs40];
    }
    return _separator;
}

@end
