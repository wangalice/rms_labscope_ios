//
//  CZChannelSettingsTextEntryCell.h
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZChannelSettingsTextEntryCell : UITableViewCell

@property (nonatomic, readonly, strong) UILabel *entryNameLabel;
@property (nonatomic, readonly, copy) NSArray<UILabel *> *entryValueLabels;
@property (nonatomic, readonly, strong) UIView *separator;

@end
