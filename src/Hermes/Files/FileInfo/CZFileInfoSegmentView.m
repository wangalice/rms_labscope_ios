//
//  CZFileInfoSegmentView.m
//  Hermes
//
//  Created by Li, Junlin on 5/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileInfoSegmentView.h"

@implementation CZFileInfoSegmentView

@synthesize titleLabel = _titleLabel;
@synthesize leftBorder = _leftBorder;
@synthesize bottomBorder = _bottomBorder;
@synthesize selectionIndicator = _selectionIndicator;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.leftBorder];
        [self addSubview:self.bottomBorder];
        [self addSubview:self.selectionIndicator];
        [self setSelected:NO];
    }
    return self;
}

#pragma mark - Views

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    
    if (_selected) {
        self.backgroundColor = [UIColor cz_gs20];
        self.titleLabel.font = [UIFont cz_subtitle1];
        self.titleLabel.textColor = [UIColor cz_gs100];
        self.bottomBorder.hidden = YES;
        self.selectionIndicator.hidden = NO;
    } else {
        self.backgroundColor = [UIColor cz_gs40];
        self.titleLabel.font = [UIFont cz_body1];
        self.titleLabel.textColor = [UIColor cz_gs80];
        self.bottomBorder.hidden = NO;
        self.selectionIndicator.hidden = YES;
    }
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont cz_body1];
        _titleLabel.textColor = [UIColor cz_gs80];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)leftBorder {
    if (_leftBorder == nil) {
        _leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 1.0, self.bounds.size.height)];
        _leftBorder.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        _leftBorder.backgroundColor = [UIColor cz_gs60];
    }
    return _leftBorder;
}

- (UIView *)bottomBorder {
    if (_bottomBorder == nil) {
        _bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.bounds.size.height - 1.0, self.bounds.size.width, 1.0)];
        _bottomBorder.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _bottomBorder.backgroundColor = [UIColor cz_gs60];
    }
    return _bottomBorder;
}

- (UIView *)selectionIndicator {
    if (_selectionIndicator == nil) {
        _selectionIndicator = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.bounds.size.width, 8.0)];
        _selectionIndicator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _selectionIndicator.backgroundColor = [UIColor cz_pb110];
    }
    return _selectionIndicator;
}

@end
