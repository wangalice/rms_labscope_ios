//
//  CZFilesTask.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTask.h"

@class CZFilesViewModel;

@protocol CZFilesViewModelDelegate;

@interface CZFilesTask : CZTask

@property (nonatomic, weak) id <CZTaskDelegate, CZFilesViewModelDelegate> delegate;
@property (nonatomic, strong, readonly) CZFilesViewModel *viewModel;

- (void)resetFilesViewModel;

@end
