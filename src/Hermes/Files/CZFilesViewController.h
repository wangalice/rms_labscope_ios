//
//  CZFilesViewController.h
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFilesTask.h"
#import "CZSystemToolbar.h"

@interface CZFilesViewController : UIViewController <CZTaskDelegate, CZMainToolbarDataSource, CZVirtualToolbarDataSource, CZToolbarDelegate, CZToolbarResponder>

@property (nonatomic, readonly, strong) CZFilesTask *task;

@end
