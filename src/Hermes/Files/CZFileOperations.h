//
//  CZFileOperations.h
//  Matscope
//
//  Created by Halley Gu on 5/30/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZFileEntity;

@protocol CZFileOperations <NSObject>

@optional
- (void)setSelected:(BOOL)isSelected forFile:(CZFileEntity *)fileEntity;
- (void)setSelectedForAllFiles:(BOOL)isSelected;
- (BOOL)isFileEntitySelected:(CZFileEntity *)fileEntity;

- (void)transferSingleFile:(CZFileEntity *)fileEntity andOpen:(BOOL)opensAfterFinish;
- (void)copyFile:(CZFileEntity *)source to:(CZFileEntity *)target;
- (void)moveFile:(CZFileEntity *)source to:(CZFileEntity *)target;
- (void)removeFile:(CZFileEntity *)fileEntity;

@end
