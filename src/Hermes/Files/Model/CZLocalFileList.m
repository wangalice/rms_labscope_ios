//
//  CZLocalFileList.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLocalFileList.h"
#import <CZFileKit/CZFileKit.h>
#import "MHWDirectoryWatcher.h"

static CZLocalFileList *instance;

@interface CZLocalFileList ()<
CZFileThumbnailDelegate
>

@property (nonatomic, strong) CZFileThumbnail *fileThumbnail;
@property (nonatomic, strong) MHWDirectoryWatcher *direcotryWatcher;
@property (nonatomic, strong) NSString *documentPath;
@property (nonatomic, strong, readonly) NSMutableDictionary<NSString *, CZFileEntity *> *mutableFiles;

@end

@implementation CZLocalFileList

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[CZLocalFileList alloc] init];
    });
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        _fileThumbnail = [[CZFileThumbnail alloc] init];
        _fileThumbnail.delegate = self;
#if DCM_REFACTORED
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(dmcLoginFinished:)
                                                     name:kLoginFinishedNotification
                                                   object:nil];
#endif
    }
    return self;
}

- (void)dealloc {
#if DCM_REFACTORED
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kLoginFinishedNotification
                                                  object:nil];
#endif
    [_direcotryWatcher stopWatching];
    _fileThumbnail.delegate = nil;
}

#pragma mark - Public methods

- (UIImage *)thumbnailFrom:(CZFileEntity *)fileEntity {
    return [self.fileThumbnail newThumbnailFrom:fileEntity.filePath modifiedDate:fileEntity.modificationDate];
}

- (void)configurateWithMonitorDocumentPath:(NSString *)documentPath initialFileEntities:(NSDictionary<NSString *,CZFileEntity *> *)initialFileEntities {
    _documentPath = [documentPath copy];
    _mutableFiles = [NSMutableDictionary dictionaryWithDictionary:initialFileEntities];

    if (_direcotryWatcher == nil) {
        _direcotryWatcher = [MHWDirectoryWatcher directoryWatcherAtPath:self.documentPath callback:^(NSDictionary *filesCollection) {
            [self loadFilesWithFilesCollection:filesCollection];
        }];
    }
}

#pragma mark - Private methods

- (void)loadFilesWithFilesCollection:(NSDictionary *)filesCollection {
    NSMutableDictionary *updateFiles = [[NSMutableDictionary alloc] init];
    
    for (NSString *filePath in filesCollection.allKeys) {
        NSDictionary *fileAttributes = filesCollection[filePath];
        BOOL isFolder = [fileAttributes[NSFileType] isEqualToString:NSFileTypeDirectory];
        if (!isFolder) {
            CZFileEntity *oldFileEntity = _mutableFiles[filePath];
            if (oldFileEntity) {
                NSDate *creationDate = fileAttributes[NSFileCreationDate];
                NSDate *modificationDate = fileAttributes[NSFileModificationDate];
                //update file entity when modification date is changed
                if (fabs([oldFileEntity.creationDate timeIntervalSinceDate:creationDate]) > 3 || fabs([modificationDate timeIntervalSinceDate:oldFileEntity.modificationDate]) > 3) {
                    CZLocalFileEntity *fileEntity = [[CZLocalFileEntity alloc] initWithFilePath:filePath
                                                                              andFileAttributes:fileAttributes];
                    [updateFiles setObject:fileEntity forKey:filePath];
                }
            } else {
                CZLocalFileEntity *fileEntity = [[CZLocalFileEntity alloc] initWithFilePath:filePath
                                                                          andFileAttributes:fileAttributes];
                [updateFiles setObject:fileEntity forKey:filePath];
            }
        }
    }
    
    NSMutableArray *removedFiles = [[NSMutableArray alloc] init];
    
    for (NSString *filePath in _mutableFiles.allKeys) {
        id value = filesCollection[filePath];
        if (value == nil) {
            CZFileEntity *fileEntity = _mutableFiles[filePath];
            if (fileEntity) {
                [removedFiles addObject:fileEntity];
                [_mutableFiles removeObjectForKey:filePath];
                [self.fileThumbnail removeThumbnailFor:filePath];
            }
        }
    }
    
    [_mutableFiles setValuesForKeysWithDictionary:updateFiles];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(localFileListShouldUpdateFiles:removedFiles:)]) {
        [self.delegate localFileListShouldUpdateFiles:updateFiles.allValues removedFiles:removedFiles];
    }
    CZLogv(@"add files: %@, removeFiles: %@", updateFiles, removedFiles);
}

#pragma mark - CZFileThumbnailDelegate

- (void)thumbnail:(UIImage *)thumbnail didGenerateFrom:(NSString *)path {
    if (self.delegate && [self.delegate respondsToSelector:@selector(localFileListShouldUpdateThumbnail:filePath:)]) {
        [self.delegate localFileListShouldUpdateThumbnail:thumbnail filePath:path];
    }
}

@end
