//
//  CZLocalFileList.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/6/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity;

@protocol CZLocalFileListDelegate <NSObject>

@optional
- (void)localFileListShouldUpdateThumbnail:(nullable UIImage *)thumbnail filePath:(nullable NSString *)path;
- (void)localFileListShouldUpdateFiles:(nullable NSArray *)updateFiles removedFiles:(nullable NSArray *)removedFiles;

@end

@interface CZLocalFileList : NSObject

@property (nonatomic, weak, nullable) id <CZLocalFileListDelegate>delegate;

+ (instancetype)sharedInstance;

- (void)configurateWithMonitorDocumentPath:(NSString *)documentPath
                       initialFileEntities:(nullable NSDictionary<NSString *, CZFileEntity *> *)initialFileEntities;

- (UIImage *)thumbnailFrom:(CZFileEntity *)fileEntity;

@end

NS_ASSUME_NONNULL_END
