//
//  CZRemoteFilesViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/23.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZRemoteFilesViewModel.h"

@implementation CZRemoteFilesViewModel

@synthesize filterType = _filterType;
@synthesize sortOrder = _sortOrder;
@synthesize keyword = _keyword;
@synthesize listMode = _listMode;
@synthesize hasConnection = _hasConnection;
@synthesize sharePath = _sharePath;
@synthesize prevSharePath = _prevSharePath;
@synthesize rootPath = _rootPath;
@synthesize isServerSetup = _isServerSetup;
@synthesize lastUpdateDate = _lastUpdateDate;
@synthesize filesUpdated = _filesUpdated;
@synthesize remoteList = _remoteList;

- (instancetype)init {
    if (self = [super init]) {
        _filterType = CZFileFilterTypeAll;
        _sortOrder = CZSortByDefault;
        _keyword = nil;
        _listMode = CZFilesListShowListMode;
        _hasConnection = NO;
        _sharePath = nil;
        _prevSharePath = nil;
        _rootPath = nil;
        _isServerSetup = NO;
        _lastUpdateDate = nil;
        _filesUpdated = NO;
        _remoteList = nil;
    }
    return self;
}

#pragma mark - Public method

- (void)clearAllSelectedFileEntities {
    
}

#pragma mark - Private method

#pragma mark - Setters

#pragma mark - Getters

@end
