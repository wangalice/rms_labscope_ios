//
//  CZFilesViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFilesViewModel.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZFilesListViewModel.h"
#import "CZLocalFilesViewModel.h"
#import "CZRemoteFilesViewModel.h"

@interface CZFilesViewModel ()<
CZLocalFilesViewModelDelegate
>

@end

@implementation CZFilesViewModel

@synthesize isLocked = _isLocked;
@synthesize isConfigurating = _isConfigurating;
@synthesize state = _state;
@synthesize localFileFullScreen = _localFileFullScreen;
@synthesize isServerSetup = _isServerSetup;
@synthesize selectedFilesType = _selectedFilesType;
@synthesize localFilesViewModel = _localFilesViewModel;
@synthesize remoteFilesViewModel = _remoteFilesViewModel;

- (instancetype)init {
    if (self = [super init]) {
        _isLocked = NO;
        _isConfigurating = NO;
        _state = CZFileControllerStateFree;
        _localFileFullScreen = YES;
        _isServerSetup = NO;
        _selectedFilesType = kUnknownStorage;
        NSString *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
        _localFilesViewModel = [[CZLocalFilesViewModel alloc] initWithDocumentPath:documentPath];
        _localFilesViewModel.sortOrder = [[NSUserDefaults standardUserDefaults] integerForKey:kCZLocalSortingOrderKey];
        _localFilesViewModel.filterType = [[NSUserDefaults standardUserDefaults] integerForKey:kCZLocalFileFilterKey];
        _localFilesViewModel.keyword = @"";
        _localFilesViewModel.delegate = self;
        _remoteFilesViewModel = [[CZRemoteFilesViewModel alloc] init];
    }
    return self;
}

- (void)dealloc {
    CZLogv(@"Works well");
}

#pragma mark - Public method

- (void)removeSelectedFiles {
    if (self.localFilesViewModel.selectedFiles.count) {
        [self.localFilesViewModel removeSelectedFiles];
    } else if (self.remoteFilesViewModel.selectedFiles.count){

    }
}

- (BOOL)enableShowMetadata {
    if (self.localFilesViewModel.selectedFiles.count == 1) {
        CZFileEntity *selectedFileEntity = self.localFilesViewModel.selectedFiles[0];
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[selectedFileEntity.filePath pathExtension]];
       
        BOOL supportMetadataInfo = NO;
        
        switch (fileFormat) {
            case kCZFileFormatCZI:
            case kCZFileFormatJPEG:
            case kCZFileFormatTIF: {
                supportMetadataInfo = YES;
            }
                break;
                
            default:
                break;
        }
        
        if (selectedFileEntity && supportMetadataInfo) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)enableFileOperations {
    if (self.localFilesViewModel.selectedFiles.count > 0  || self.remoteFilesViewModel.selectedFiles.count > 0) {
        return YES;
    }
    return NO;
}

- (BOOL)enableFileReport {
    return self.localFilesViewModel.selectedFiles.count > 0;
}

- (BOOL)exceedMaximumLimitationForReporting {
    return self.localFilesViewModel.selectedFiles.count > [self reportFileMaximumCount];
}

- (NSUInteger)reportFileMaximumCount {
    return 50;
}

- (BOOL)containsUnsupportedFilesForReporting {
    BOOL enabled = NO;
    for (CZFileEntity *fileEntity in self.localFilesViewModel.selectedFiles) {
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[fileEntity.filePath pathExtension]];
        if (fileFormat != kCZFileFormatCZI &&
            fileFormat != kCZFileFormatJPEG &&
            fileFormat != kCZFileFormatTIF) {
            enabled = YES;
            break;
        }
    }
    return enabled;
}

- (NSArray <NSString *> *)reportFiles {
    NSMutableArray *reportFiles = [NSMutableArray array];
    //ignore the first object
    NSInteger reportFilesCount = 0;
    for (CZFileEntity *fileEntity in self.localFilesViewModel.selectedFiles) {
        if (reportFilesCount >= [self reportFileMaximumCount] - 1) {
            break;
        }
        
        if (fileEntity.filePath && fileEntity != [self.localFilesViewModel.selectedFiles firstObject]) {
            [reportFiles addObject:fileEntity.filePath];
            reportFilesCount++;
        }
    }
    return reportFiles;
}

- (CZDocManager *)reportMainDocManager {
    CZFileEntity *firstFileEntity = [self.localFilesViewModel.selectedFiles firstObject];
    assert(firstFileEntity.filePath != nil);
    CZDocManager *docManager = [CZDocManager newDocManagerFromFilePath:firstFileEntity.filePath];
    return docManager;
}

- (void)transferFilesRemovingSource:(BOOL)removesSource {
    //TODO: implement this when the remote files finished;
}

- (void)writeToLocalDirectoryWithImageData:(NSData *)imageData {
    NSString *importImageSavePath = [CZFileNameGenerator uniqueFilePath:[self.localFilesViewModel.documentPath stringByAppendingPathComponent:@"imported.jpg"]];
    [imageData writeToFile:importImageSavePath atomically:YES];
}

- (void)clearAllSelection {
    if (self.localFilesViewModel.selectedFiles.count) {
        [self.localFilesViewModel clearAllSelectedFileEntities];
    } else if (self.localFilesViewModel.selectedFiles.count) {
        [self.remoteFilesViewModel clearAllSelectedFileEntities];
    }
}

#pragma mark - Private method

#pragma mark - CZLocalFilesViewModelDelegate

- (void)viewModelDidSelectFileEntities:(CZLocalFilesViewModel *)localViewModel {
    if (localViewModel == self.localFilesViewModel) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(filesViewSelectedFilesDidChanged)]) {
            [self.delegate filesViewSelectedFilesDidChanged];
        }
    }
}

- (void)viewModel:(CZLocalFilesViewModel *)localViewModel didChangeDocumentPath:(NSString *)documentPath {
    
}

- (void)localFilesListDidUpdate {
    if (self.delegate && [self.delegate respondsToSelector:@selector(filesViewSelectedFilesDidChanged)]) {
        [self.delegate filesViewSelectedFilesDidChanged];
    }
}

#pragma mark - Settters

#pragma mark - Getters

@end


@implementation CZFilesViewModel (Sort)

+ (NSArray *)localizedOrderLabels {
    return @[L(@"FILE_SORTING_NAME_ASCENDING"),
             L(@"FILE_SORTING_NAME_DESCENDING"),
             L(@"FILE_SORTING_DATE_ASCENDING"),
             L(@"FILE_SORTING_DATE_DESCENDING")];
}

+ (NSComparator)comparatorOfOrder:(CZFileSortOrder)order {
    NSComparator comparator = NULL;
    switch (order) {
        case CZSortByNameAscend: {
            comparator = ^(id obj1, id obj2) {
                CZFileEntity *file1 = obj1;
                CZFileEntity *file2 = obj2;
                if ([file1 isFolder] && ![file2 isFolder]) {
                    return NSOrderedAscending;
                } else if (![file1 isFolder] && [file2 isFolder]) {
                    return NSOrderedDescending;
                }
                return [file1.fileName compare:file2.fileName options:NSCaseInsensitiveSearch];
            };
        }
            break;
        case CZSortByTimeAscend: {
            comparator = ^(id obj1, id obj2) {
                CZFileEntity *file1 = obj1;
                CZFileEntity *file2 = obj2;
                if ([file1 isFolder] && ![file2 isFolder]) {
                    return NSOrderedAscending;
                } else if (![file1 isFolder] && [file2 isFolder]) {
                    return NSOrderedDescending;
                }
                return [file1.modificationDate compare:file2.modificationDate];
            };
        }
            break;
        case CZSortByNameDescend: {
            comparator = ^(id obj1, id obj2) {
                CZFileEntity *file1 = obj1;
                CZFileEntity *file2 = obj2;
                if ([file1 isFolder] && ![file2 isFolder]) {
                    return NSOrderedDescending;
                } else if (![file1 isFolder] && [file2 isFolder]) {
                    return NSOrderedAscending;
                }
                return [file2.fileName compare:file1.fileName options:NSCaseInsensitiveSearch];
            };
        }
            break;
        case CZSortByTimeDescend: {
            comparator = ^(id obj1, id obj2) {
                CZFileEntity *file1 = obj1;
                CZFileEntity *file2 = obj2;
                if ([file1 isFolder] && ![file2 isFolder]) {
                    return NSOrderedAscending;
                } else if (![file1 isFolder] && [file2 isFolder]) {
                    return NSOrderedDescending;
                }
                return [file2.modificationDate compare:file1.modificationDate];
            };
        }
            break;
        case CZSortingCount: {}
            break;
    }
    
    return comparator;
}

+ (void)sortFileEntities:(NSMutableArray<CZFileEntity *> *)files inOrder:(CZFileSortOrder)order {
    NSComparator comparator = [self comparatorOfOrder:order];
    if (comparator) {
        [files sortUsingComparator:comparator];
    }
}

@end

@implementation CZFilesViewModel (Filter)

+ (NSArray *)localizedFileFilterOptionLabels {
    return @[L(@"FILE_FILTER_IMAGE"),
             L(@"FILE_FILTER_VIDEO"),
             L(@"FILE_FILTER_REPORT"),
             L(@"FILE_FILTER_TEMPLATE"),
             L(@"FILE_FILTER_SHOW_ALL")];
}

+ (NSArray *)localizedFileFilterOptionLabelsWithOperator {
    NSString *operatorName = [[CZDefaultSettings sharedInstance] operatorName];
    
    NSString *operatorFilterString = nil;
    if (operatorName.length == 0) {
        operatorFilterString = L(@"FILE_FILTER_OPERATOR");
    } else {
        operatorFilterString = [NSString stringWithFormat:@"%@%@%@%@",
                                L(@"FILE_FILTER_OPERATOR"),
                                L(@"BRACKET_LEFT"),
                                operatorName,
                                L(@"BRACKET_RIGHT")];
    }
    
    return @[L(@"FILE_FILTER_IMAGE"),
             L(@"FILE_FILTER_VIDEO"),
             L(@"FILE_FILTER_REPORT"),
             L(@"FILE_FILTER_TEMPLATE"),
             operatorFilterString,
             L(@"FILE_FILTER_SHOW_ALL")];
}

+ (NSMutableArray<CZFileEntity *> *)newVisbileFilesFilteredFromFiles:(NSArray<CZFileEntity *> *)files
                                                              option:(CZFileFilterOptions)filterType {
    if ((filterType & CZFileFilterTypeAll) == CZFileFilterTypeAll) {
        return [files mutableCopy];
    }
    
    NSMutableArray *visibleFiles = [[NSMutableArray alloc] init];
    NSMutableArray *supportedFileFormats = [NSMutableArray array];
    
    if ((filterType & CZFileFilterTypeImage) == CZFileFilterTypeImage) {
        [supportedFileFormats addObject:@(kCZFileFormatTypeImage)];
    }
    
    if ((filterType & CZFileFilterTypeVideo) == CZFileFilterTypeVideo) {
        [supportedFileFormats addObject:@(kCZFileFormatTypeVideo)];
    }
    
    if ((filterType & CZFileFilterTypeReport) == CZFileFilterTypeReport){
        [supportedFileFormats addObject:@(kCZFileFormatTypeReport)];
    }
    
    if ((filterType & CZFileFilterTypeTemplate) == CZFileFilterTypeTemplate) {
        [supportedFileFormats addObjectsFromArray:@[@(kCZFileFormatTypeReportTemplate),
                                                    @(kCZFileFormatTypeFileNameTemplate),
                                                    @(kCZFileFormatTypeAdvanceMeasurementsTemplate)]];
    }

    for (CZFileEntity *fileEntity in files) {
        if (fileEntity.isFolder) {
            [visibleFiles addObject:fileEntity];
            continue;
        }
        
        NSString *extension = [fileEntity.fileName pathExtension];
        CZFileFormatType formatType = [CZCommonUtils fileFormatTypeForExtension:extension];
        if ([supportedFileFormats containsObject:@(formatType)]) {
            [visibleFiles addObject:fileEntity];
        }
    }
    
    return visibleFiles;
}

+ (NSMutableArray *)newFoundFilesFromSourceFiles:(NSArray<CZFileEntity *> *)sourceFiles
                                 useSearchString:(NSString *)searchString {
    // replace special char
    NSMutableString *escapedString = [NSMutableString stringWithCapacity:searchString.length];
    NSCharacterSet *specialChars = [NSCharacterSet characterSetWithCharactersInString:@"+-^%$()[]."];
    for (NSUInteger i = 0; i < searchString.length; i++) {
        unichar c = [searchString characterAtIndex:i];
        if ([specialChars characterIsMember:c]) {
            unichar backslash = '\\';
            [escapedString appendString:[NSString stringWithCharacters:&backslash length:1]];
        } else if (c == '*') {
            unichar dot = '.';
            [escapedString appendString:[NSString stringWithCharacters:&dot length:1]];
        }
        [escapedString appendString:[NSString stringWithCharacters:&c length:1]];
    }
    
    // create regular expression for match file name
    NSRegularExpression *regex = nil;
    NSMutableString *expressionString = escapedString;
    
    NSError *error = NULL;
    regex = [NSRegularExpression regularExpressionWithPattern:expressionString
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    if (error) {
        CZLogv(@"Filename generator regular expression error: %@", [error localizedDescription]);
        return nil;
    }
    
    NSMutableArray *foundFiles = [[NSMutableArray alloc] init];
    
    if (regex) {
        // scan each file name and calculate max index
        for (CZFileEntity *fileEntity in sourceFiles) {
            NSString *lastPathComponent = fileEntity.fileName;
            NSTextCheckingResult *match = [regex firstMatchInString:lastPathComponent
                                                            options:0
                                                              range:NSMakeRange(0, [lastPathComponent length])];
            if (match) {
                [foundFiles addObject:fileEntity];
            }
        }
    }
    
    return foundFiles;
}

@end
