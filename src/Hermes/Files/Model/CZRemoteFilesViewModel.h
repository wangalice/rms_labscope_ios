//
//  CZRemoteFilesViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/23.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZFileOperations.h"
#import "CZFileCommonMarco.h"

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity, CZFilesListViewModel;

@interface CZRemoteFilesViewModel : NSObject<
CZFileOperations
>

@property (nonatomic, assign) CZFileFilterOptions filterType;
@property (nonatomic, assign) CZFileSortOrder sortOrder;
@property (nonatomic, copy, readonly) NSString *keyword;
@property (nonatomic, assign) CZFilesListShowMode listMode;
@property (nonatomic, assign, readonly) BOOL hasConnection;
@property (nonatomic, copy) NSString *sharePath;
@property (nonatomic, copy, readonly) NSString *prevSharePath;
@property (nonatomic, copy, readonly) NSString *rootPath;
@property (nonatomic, assign, readonly) BOOL isServerSetup;
@property (nonatomic, strong, readonly) NSDate *lastUpdateDate;
@property (nonatomic, assign, readonly) BOOL filesUpdated;
@property (nonatomic, strong, readonly) CZFilesListViewModel *remoteList;

@property (nonatomic, strong, readonly) NSMutableArray <CZFileEntity *> *files;
@property (nonatomic, strong, readonly) NSMutableArray <CZFileEntity *> *selectedFiles;
@property (nonatomic, strong, readonly) NSMutableArray <CZFileEntity *> *visibleFiles;

- (void)sortFiles;
- (void)filterFilesWithKeyword:(NSString *)keyword;
- (void)navigateToParentFolder;
- (void)insertItem:(id)sender;

- (BOOL)isServerIPChanged;
- (void)setupServerPath;
- (BOOL)loadFilesFromServer;

- (void)clearAllSelectedFileEntities;

@end

NS_ASSUME_NONNULL_END
