//
//  CZFilesViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFileCommonMarco.h"
#import <CZFileKit/CZFileKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CZFilesViewModelDelegate <NSObject>

@optional
- (void)localFilesViewDidEnterFullScreen:(BOOL)fullScreen;
- (void)filesViewDidEnterConfiguration:(BOOL)isConfigurating;
- (void)filesViewSelectedFilesDidChanged;

@end

@class CZFilesListViewModel, CZFileEntity, CZLocalFilesViewModel, CZRemoteFilesViewModel, CZImageProperties;
@class CZDocManager;
 
@interface CZFilesViewModel : NSObject

@property (nonatomic, weak) id <CZFilesViewModelDelegate>delegate;
@property (nonatomic, assign) BOOL isLocked;
@property (nonatomic, assign) BOOL isConfigurating;
@property (nonatomic, assign) CZFileControllerState state;
@property (nonatomic, assign, readonly) BOOL localFileFullScreen;
@property (nonatomic, assign, readonly) BOOL isServerSetup;
@property (nonatomic, assign, readonly) BOOL enableShowMetadata;
@property (nonatomic, assign, readonly) BOOL enableFileOperations;
@property (nonatomic, assign, readonly) BOOL enableFileReport;
@property (nonatomic, assign, readonly) CZFileStorageType selectedFilesType;
@property (nonatomic, strong, readonly) CZLocalFilesViewModel *localFilesViewModel;
@property (nonatomic, strong, readonly) CZRemoteFilesViewModel *remoteFilesViewModel;

// Report
- (BOOL)containsUnsupportedFilesForReporting;
- (BOOL)exceedMaximumLimitationForReporting;

- (NSUInteger)reportFileMaximumCount;
- (NSArray <NSString *> *)reportFiles;
- (CZDocManager *)reportMainDocManager;

/** copy file from local to remote or vice versa.
 * @param removesSource, if YES, remove source file after success.
 */
- (void)transferFilesRemovingSource:(BOOL)removesSource;

- (void)writeToLocalDirectoryWithImageData:(NSData *)imageData;

/** File operations */
- (void)clearAllSelection;
- (void)removeSelectedFiles;

@end

@interface CZFilesViewModel (Sort)

+ (NSArray *)localizedOrderLabels;

+ (NSComparator)comparatorOfOrder:(CZFileSortOrder)order;
+ (void)sortFileEntities:(NSMutableArray<CZFileEntity *> *)files inOrder:(CZFileSortOrder)order;

@end

@interface CZFilesViewModel (Filter)

+ (NSArray *)localizedFileFilterOptionLabels;
+ (NSArray *)localizedFileFilterOptionLabelsWithOperator;

+ (NSMutableArray<CZFileEntity *> *)newVisbileFilesFilteredFromFiles:(NSArray<CZFileEntity *> *)files
                                                              option:(CZFileFilterOptions)filterType;

+ (NSMutableArray *)newFoundFilesFromSourceFiles:(NSArray<CZFileEntity *> *)sourceFiles
                                 useSearchString:(NSString *)searchString;

@end

NS_ASSUME_NONNULL_END
