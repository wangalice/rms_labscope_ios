//
//  CZFilesModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFilesListViewModel.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZLocalFileList.h"

@interface CZFilesListViewModel ()

@property (nonatomic, strong) NSArray <CZFileEntity *> *files;

@end

@implementation CZFilesListViewModel

#pragma mark - Public method

- (UIImage *)thumbnailFrom:(CZFileEntity *)fileEntity {
    return [[CZLocalFileList sharedInstance] thumbnailFrom:fileEntity];
}

- (void)updateThumbnail:(UIImage *)thumbnail filePath:(NSString *)path {
    if (self.viewControllerDelegate && [self.viewControllerDelegate respondsToSelector:@selector(fileListViewModel:didUpdateThumbnail:filPath:)]) {
        [self.viewControllerDelegate fileListViewModel:self didUpdateThumbnail:thumbnail filPath:path];
    }
}

- (NSString *)newFileInfoLabel:(unsigned long long)fileSize modifiedDate:(NSDate *)modifiedDate {
    NSString *fileSizeText = nil;
    
    if (fileSize > 1024 * 1024 * 1024) {
        fileSizeText = [NSString stringWithFormat:@"%.2f GB", (fileSize/(1024.0 * 1024.0 * 1024.0))];
    } else if (fileSize > 1024 * 1024) {
        fileSizeText = [NSString stringWithFormat:@"%.2f MB", fileSize/(1024.0 * 1024.0)];
    } else if (fileSize > 1024) {
        fileSizeText = [NSString stringWithFormat:@"%.2f KB", fileSize/1024.0];
    } else {
        fileSizeText = [NSString stringWithFormat:@"%.2f byte", fileSize * 1.0];
    }
    
    NSString *modificationDateText = [NSDateFormatter localizedStringFromDate:modifiedDate
                                                                    dateStyle:NSDateFormatterShortStyle
                                                                    timeStyle:NSDateFormatterMediumStyle];
    
    NSString *fileInfoText = [[NSString alloc] initWithFormat:@"%@ %@", fileSizeText, modificationDateText];
    return fileInfoText;
}

- (BOOL)isFileEntitySelected:(CZFileEntity *)fileEntity {
    if (self.delegate && [self.delegate respondsToSelector:@selector(isFileEntitySelected:)]) {
        return [self.delegate isFileEntitySelected:fileEntity];
    }
    return NO;
}

- (void)updateFileEntities:(NSArray <CZFileEntity *> *)fileEntities {
    self.files = fileEntities;
    if (self.viewControllerDelegate && [self.viewControllerDelegate respondsToSelector:@selector(fileListDidUpdate)]) {
        [self.viewControllerDelegate fileListDidUpdate];
    }
}

- (void)updateFileEntitiesSelection:(NSArray <CZFileEntity *> *)fileEntites {
    
}

- (void)removeFileEntities:(NSArray <CZFileEntity *> *)fileEntities {
    
}

- (void)addFileEntities:(NSArray <CZFileEntity *> *)fileEntities {
    
}

- (void)selectFileEntity:(CZFileEntity *)fileEntity isSelected:(BOOL)isSelected {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileEntity:didSelected:)]) {
        [self.delegate fileEntity:fileEntity didSelected:isSelected];
    }
}

- (void)uploadFileEntity:(CZFileEntity *)fileEntity {
    if (self.delegate && [self.delegate respondsToSelector:@selector(willUploadFileEntity:)]) {
        [self.delegate willUploadFileEntity:fileEntity];
    }
}

#pragma mark - CZFileOperations

- (void)setSelected:(BOOL)isSelected forFile:(CZFileEntity *)fileEntity {
    if (self.viewControllerDelegate && [self.viewControllerDelegate respondsToSelector:@selector(fileListDidUpdate)]) {
        [self.viewControllerDelegate fileListDidUpdate];
    }
}

@end
