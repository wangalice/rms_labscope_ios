//
//  CZLocalFilesViewModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/23.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZFileCommonMarco.h"
#import "CZFileOperations.h"

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity, CZFilesListViewModel, CZLocalFilesViewModel;

@protocol CZLocalFilesViewModelDelegate <NSObject>

@optional
- (void)viewModel:(CZLocalFilesViewModel *)localViewModel didChangeDocumentPath:(NSString *)documentPath;

- (void)viewModelDidSelectFileEntities:(CZLocalFilesViewModel *)localViewModel;

- (void)localFilesListDidUpdate;

@end

@interface CZLocalFilesViewModel : NSObject<
CZFileOperations
>

@property (nonatomic, weak) id <CZLocalFilesViewModelDelegate>delegate;
@property (nonatomic, readonly, copy) NSString *documentPath;
@property (nonatomic, readonly, strong) CZFilesListViewModel *localList;
@property (nonatomic, readonly, assign) BOOL isEditing;
@property (nonatomic, readonly, strong, nullable) NSArray <CZFileEntity *> *files;
@property (nonatomic, readonly, strong, nullable) NSArray <CZFileEntity *> *selectedFiles;
@property (nonatomic, readonly, strong, nullable) NSArray <CZFileEntity *> *visibleFiles;

@property (nonatomic, copy, nullable) NSString *keyword;
@property (nonatomic, assign) CZFileSortOrder sortOrder;
@property (nonatomic, assign) CZFileFilterOptions filterType;
@property (nonatomic, assign) CZFilesListShowMode listMode;
@property (nonatomic, assign) CGFloat freeSpacePercent;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, assign) BOOL shouldSelectedAllFileEntities;

- (instancetype)initWithDocumentPath:(nullable NSString *)documentPath;

// Selection
- (void)selectFile:(CZFileEntity *)fileEntity;
- (void)deselectFile:(CZFileEntity *)fileEntity;
- (void)selectAllFileEntities;
- (void)clearAllSelectedFileEntities;

// Add/Delete file
- (void)removeFile:(CZFileEntity *)fileEntity;
- (void)removeSelectedFiles;
- (void)addFileWith:(NSString *)filePath;
- (void)removeFileWith:(NSString *)filePath;

// Sort/Filter/Search
- (void)sortFiles;

- (void)switchRootDocumentPath:(NSString *)documentPath;

- (CZFileEntity *)fileEntityFromFileName:(NSString *)fileName;

- (void)synchronizeFilters;
- (void)synchronizeSort;

/** return index of file in visibleFiles. If not found return NSNotFound. */
- (NSUInteger)indexOfVisibleFile:(NSString *)filePath;
- (NSUInteger)indexOfVisibleFile:(NSString *)filePath inRange:(NSRange)range;

@end

NS_ASSUME_NONNULL_END
