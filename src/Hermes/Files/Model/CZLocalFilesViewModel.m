//
//  CZLocalFilesViewModel.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/23.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLocalFilesViewModel.h"
#import "CZFilesListViewModel.h"
#import <CZFileKit/CZFileKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "MHWDirectoryWatcher.h"
#import "CZFilesViewModel.h"
#import "CZLocalFileList.h"

@interface CZLocalFilesViewModel () <
CZFilesListViewModelDelegate,
CZLocalFileListDelegate
>

@property (nonatomic, assign) BOOL filesLoaded;
@property (nonatomic, assign) BOOL sortDirtyFlag;
@property (nonatomic, assign) BOOL filterDirtyFlag;
@property (nonatomic, strong) MHWDirectoryWatcher *directoryWatcher;
@property (nonatomic, strong) NSMutableDictionary <NSString *, CZFileEntity *> *mutableFiles; // all files
@property (nonatomic, strong) NSMutableArray <CZFileEntity *> *allVisibleFiles; // visible files
@property (nonatomic, strong) NSMutableArray <CZFileEntity *> *mutableVisibleFiles; //filter files
@property (nonatomic, strong) NSMutableSet <CZFileEntity *> *mutableSelectedFiles; // selected files

@end

@implementation CZLocalFilesViewModel

@synthesize documentPath     = _documentPath;
@synthesize filterType       = _filterType;
@synthesize sortOrder        = _sortOrder;
@synthesize keyword          = _keyword;
@synthesize listMode         = _listMode;
@synthesize freeSpacePercent = _freeSpacePercent;
@synthesize isEditing        = _isEditing;

- (instancetype)initWithDocumentPath:(NSString *)documentPath {
    if (self = [super init]) {
        _documentPath = [documentPath copy];
        _filterType = CZFileFilterTypeAll;
        _sortOrder = CZSortByDefault;
        _keyword = nil;
        _listMode = CZFilesListShowListMode;
        _freeSpacePercent = 0.00f;
        _localList = [[CZFilesListViewModel alloc] init];
        _localList.delegate = self;
        _isEditing = NO;
        _mutableFiles = [NSMutableDictionary dictionary];
        _allVisibleFiles = [NSMutableArray array];
        _mutableVisibleFiles = [NSMutableArray array];
        _mutableSelectedFiles = [NSMutableSet set];
        _filesLoaded = NO;

        [self loadFiles];

        [[CZLocalFileList sharedInstance] configurateWithMonitorDocumentPath:_documentPath initialFileEntities:_mutableFiles];
        [[CZLocalFileList sharedInstance] setDelegate:self];
    }
    return self;
}

- (instancetype)init {
    return [self initWithDocumentPath:NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject];
}

- (void)resetViewModel {
    _documentPath = nil;
    _filterType = CZFileFilterTypeAll;
    _sortOrder = CZSortByDefault;
    _keyword = nil;
    _listMode = CZFilesListShowListMode;
    _freeSpacePercent = 0.00f;
    _localList.delegate = nil;
    _localList = nil;
    _isEditing = NO;
    _mutableFiles = nil;
    _allVisibleFiles = nil;
    _mutableVisibleFiles = nil;
    _mutableSelectedFiles = nil;
    _filesLoaded = NO;
    _shouldSelectedAllFileEntities = YES;
}

- (void)dealloc {
    [self resetViewModel];

    [[CZLocalFileList sharedInstance] setDelegate:nil];
}

#pragma mark - Public method

- (void)selectFile:(CZFileEntity *)fileEntity {
    
}

- (void)deselectFile:(CZFileEntity *)fileEntity {
    
}

- (void)selectAllFileEntities {
    [self.mutableSelectedFiles removeAllObjects];
    
    for (CZFileEntity *fileEntity in self.mutableVisibleFiles) {
        if (!fileEntity.isFolder) {
            [self.mutableSelectedFiles addObject:fileEntity];
        }
    }
    self.shouldSelectedAllFileEntities = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewModelDidSelectFileEntities:)]) {
        [self.delegate viewModelDidSelectFileEntities:self];
    }
}

- (void)clearAllSelectedFileEntities {
    [self.mutableSelectedFiles removeAllObjects];
    
    self.shouldSelectedAllFileEntities = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewModelDidSelectFileEntities:)]) {
        [self.delegate viewModelDidSelectFileEntities:self];
    }
}

- (void)addFileWith:(NSString *)filePath {
    CZLocalFileEntity *newFileEntity = [[CZLocalFileEntity alloc] initWithFilePath:filePath];
    
    if (newFileEntity.isFolder) {
        return;
    }
    
    self.mutableFiles[filePath] = newFileEntity;

    [self localFileListShouldUpdateFiles:@[newFileEntity] removedFiles:nil];
}

- (void)removeFileWith:(NSString *)filePath {
    CZFileEntity *removeFileEntity = self.mutableFiles[filePath];
    
    if (removeFileEntity) {
        [self removeFile:removeFileEntity];
    }
}

- (void)removeSelectedFiles {
    NSArray *selectedFiles = [self.mutableSelectedFiles allObjects];
    
    for (CZFileEntity *selectedFileEntity in selectedFiles) {
        [self removeFile:selectedFileEntity];
    }
}

- (void)removeFile:(CZFileEntity *)fileEntity {
    [[NSFileManager defaultManager] removeItemAtPath:fileEntity.filePath error:NULL];

    [self.mutableFiles removeObjectForKey:fileEntity.filePath];
    
    [self localFileListShouldUpdateFiles:nil removedFiles:@[fileEntity]];
}

- (void)sortFiles {
    if (self.sortDirtyFlag == NO) {
        return;
    }
    self.sortDirtyFlag = NO;
    
    self.allVisibleFiles = [NSMutableArray arrayWithArray:[self.mutableFiles allValues]];
    [CZFilesViewModel sortFileEntities:self.allVisibleFiles inOrder:self.sortOrder];

    [self postSortResultChangedNotification];
}

- (void)switchRootDocumentPath:(NSString *)documentPath {
    if (self.documentPath != documentPath) {
        [self resetViewModel];
        
        _documentPath = [documentPath copy];
        self.filterType = CZFileFilterTypeAll;
        self.sortOrder = CZSortByDefault;
        self.keyword = nil;
        self.listMode = CZFilesListShowListMode;
        self.freeSpacePercent = 0.00f;
        _localList = [[CZFilesListViewModel alloc] init];
        _localList.delegate = self;
        _isEditing = NO;
        self.mutableFiles = [NSMutableDictionary dictionary];
        self.mutableVisibleFiles = [NSMutableArray array];
        _mutableSelectedFiles = [NSMutableSet set];
        self.filesLoaded = NO;
    }

    [self loadFiles];
}

- (CZFileEntity *)fileEntityFromFileName:(NSString *)fileName {
    NSString *filePath = [self.documentPath stringByAppendingPathComponent:fileName];
    return self.mutableFiles[filePath];
}

- (void)synchronizeFilters {
    [[NSUserDefaults standardUserDefaults] setInteger:_filterType forKey:kCZLocalFileFilterKey];
}

- (void)synchronizeSort {
    [[NSUserDefaults standardUserDefaults] setInteger:_sortOrder forKey:kCZLocalSortingOrderKey];
}

- (NSUInteger)indexOfVisibleFile:(NSString *)filePath {
    return [self indexOfVisibleFile:filePath inRange:NSMakeRange(0, NSUIntegerMax)];
}

- (NSUInteger)indexOfVisibleFile:(NSString *)filePath inRange:(NSRange)range {
    NSUInteger foundIndex = NSNotFound;
    NSUInteger end = MIN(self.mutableVisibleFiles.count, (range.location + range.length));
    
    for (NSUInteger i = range.location; i < end; i++) {
        CZFileEntity *fileEntity = self.mutableVisibleFiles[i];
        if ([fileEntity.filePath isEqualToString:filePath]) {
            foundIndex = i;
            break;
        }
    }
    return foundIndex;
}

#pragma mark - Getters

- (NSArray<CZFileEntity *> *)visibleFiles {
    return self.mutableVisibleFiles;
}

- (NSArray<CZFileEntity *> *)files {
    return [self.mutableFiles allValues];
}

- (NSArray<CZFileEntity *> *)selectedFiles {
    NSMutableArray <CZFileEntity *> *selectFiles = [NSMutableArray arrayWithArray:[self.mutableSelectedFiles allObjects]];
    [CZFilesViewModel sortFileEntities:selectFiles inOrder:self.sortOrder];
    return selectFiles;
}

#pragma mark - Private method

- (void)loadFiles {
    if (self.filesLoaded) {
        return;
    }
    self.filesLoaded = YES;
    
    NSArray<NSString *> *arrayOfFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.documentPath error:nil];

    NSString *docPath = [self.documentPath copy];
    
    for (NSString *file in arrayOfFiles) {
        NSString *filePath = [docPath stringByAppendingPathComponent:file];
        CZLocalFileEntity *fileEntity = [[CZLocalFileEntity alloc] initWithFilePath:filePath];
        if (fileEntity.isFolder == NO) {
            self.mutableFiles[filePath] = fileEntity;
        }
    }

    self.sortDirtyFlag = YES;
    [self sortFiles];
    [self.localList updateFileEntities:self.allVisibleFiles];
}

- (void)updateVisibleFiles {
    [self sortFiles];
    
    if (self.filterDirtyFlag == NO) {
        return;
    }
    self.filterDirtyFlag = NO;
    
    NSMutableArray *visibleFiles = [CZFilesViewModel newVisbileFilesFilteredFromFiles:self.allVisibleFiles
                                                                               option:self.filterType];

    
    if (self.keyword.length) {
        visibleFiles = [CZFilesViewModel newFoundFilesFromSourceFiles:visibleFiles
                                                     useSearchString:self.keyword];
    }
    
    self.mutableVisibleFiles = visibleFiles;
    
    [self.localList updateFileEntities:self.mutableVisibleFiles];
}

- (void)fileEntityUpdated:(CZFileEntity *)newFileEntity {
    if (newFileEntity == nil) {
        return;
    }
    
    NSString *filePath = newFileEntity.filePath;

    [self.mutableFiles setValue:newFileEntity forKey:filePath];
    
    NSUInteger i = 0;
    for (CZFileEntity *fileEntity in self.allVisibleFiles) {
        if ([fileEntity.filePath isEqualToString:filePath]) {
            [self.mutableSelectedFiles removeObject:fileEntity];
            [self.allVisibleFiles removeObjectAtIndex:i];
            break;
        }
        i++;
    }
    
    NSInteger order = self.sortOrder;
    NSComparator comparator = [CZFilesViewModel comparatorOfOrder:order];
    if (comparator) {
        NSUInteger insertionIndex = [self.allVisibleFiles indexOfObject:newFileEntity
                                                      inSortedRange:NSMakeRange(0, [self.allVisibleFiles count])
                                                            options:NSBinarySearchingInsertionIndex
                                                    usingComparator:comparator];
        [self.allVisibleFiles insertObject:newFileEntity atIndex:insertionIndex];
    } else {
        [self.allVisibleFiles addObject:newFileEntity];
    }
    
    self.filterDirtyFlag = YES;

    if (self.delegate && [self.delegate respondsToSelector:@selector(localFilesListDidUpdate)]) {
        [self.delegate localFilesListDidUpdate];
    }

    [self postSortResultChangedNotification];
}

- (void)fileEntityRemoved:(CZFileEntity *)removedFileEntity {
    [self.allVisibleFiles removeObject:removedFileEntity];
    [self.mutableSelectedFiles removeObject:removedFileEntity];
    
    self.filterDirtyFlag = YES;

    if (self.delegate && [self.delegate respondsToSelector:@selector(localFilesListDidUpdate)]) {
        [self.delegate localFilesListDidUpdate];
    }
    
    [self postSortResultChangedNotification];
}

- (void)postSortResultChangedNotification {
    NSArray *theFiles = [NSArray arrayWithArray:_allVisibleFiles];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:CZLocalFileListDidUpdateNotification
                                      object:self
                                    userInfo:@{CZLocalFileListDidUpdateNotificationFileList : theFiles}];
}

#pragma mark - CZFilesListViewModelDelegate

- (BOOL)isFileEntitySelected:(CZFileEntity *)fileEntity {
    return [self.mutableSelectedFiles containsObject:fileEntity];
}

- (void)fileEntity:(CZFileEntity *)fileEntity didSelected:(BOOL)isSelected {
    if (isSelected) {
        [self.mutableSelectedFiles addObject:fileEntity];
    } else {
        if ([self.mutableSelectedFiles containsObject:fileEntity]) {
            [self.mutableSelectedFiles removeObject:fileEntity];
        }
    }
    BOOL shouldSelectedAllFilesEntities = YES;
    for (CZFileEntity *fileEntity in self.allVisibleFiles) {
        if (![self.mutableSelectedFiles containsObject:fileEntity]) {
            shouldSelectedAllFilesEntities = NO;
            break;
        }
    }
    self.shouldSelectedAllFileEntities = shouldSelectedAllFilesEntities;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewModelDidSelectFileEntities:)]) {
        [self.delegate viewModelDidSelectFileEntities:self];
    }
}

#pragma mark - CZLocalFileListDelegate

- (void)localFileListShouldUpdateFiles:(NSArray *)updateFiles removedFiles:(NSArray *)removedFiles {
    if (updateFiles.count == 0 && removedFiles.count == 0) {
        return;
    }
    
    for (CZFileEntity *fileEntity in updateFiles) {
        [self fileEntityUpdated:fileEntity];
    }
    
    for (CZFileEntity *fileEntity in removedFiles) {
        [self fileEntityRemoved:fileEntity];
    }

    [self updateVisibleFiles];
}

- (void)localFileListShouldUpdateThumbnail:(UIImage *)thumbnail filePath:(NSString *)path {
    [self.localList updateThumbnail:thumbnail filePath:path];
}

#pragma mark - Setters

- (void)setSortOrder:(CZFileSortOrder)sortOrder {
    if (_sortOrder != sortOrder) {
        _sortOrder = sortOrder;

        _sortDirtyFlag = YES;
        _filterDirtyFlag = YES;
    }

    [self updateVisibleFiles];
}

- (void)setFilterType:(CZFileFilterOptions)filterType {
    if (_filterType != filterType) {
        _filterType = filterType;
     
        _filterDirtyFlag = YES;
        _sortDirtyFlag = YES;
    }
    
    [self updateVisibleFiles];
}

- (void)setKeyword:(NSString *)keyword {
    _keyword = [keyword copy];

    _filterDirtyFlag = YES;
    
    [self updateVisibleFiles];
}

@end
