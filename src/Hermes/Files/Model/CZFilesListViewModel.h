//
//  CZFilesModel.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZFileCommonMarco.h"
#import "CZFileOperations.h"

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity, CZImageProperties, CZFilesListViewModel;

@protocol CZFileOperations;

@protocol CZFilesListViewModelDelegate <NSObject>

@optional
- (BOOL)isFileEntitySelected:(CZFileEntity *)fileEntity;

- (void)fileEntity:(CZFileEntity *)fileEntity didSelected:(BOOL)isSelected;

- (void)willUploadFileEntity:(CZFileEntity *)fileEntity;

@end

@protocol CZFilesListViewControllerDelegate <NSObject>

@optional
- (void)fileListDidUpdate;
- (void)fileListViewModel:(CZFilesListViewModel *)viewModel didUpdateThumbnail:(UIImage *)thumbnail filPath:(NSString *)filePath;

@end

@interface CZFilesListViewModel : NSObject<
CZFileOperations
>

@property (nonatomic, strong, readonly) NSArray <CZFileEntity *> *files;

@property (nonatomic, weak, nullable) id <CZFilesListViewModelDelegate> delegate;
@property (nonatomic, weak, nullable) id <CZFilesListViewControllerDelegate> viewControllerDelegate;

- (UIImage *)thumbnailFrom:(CZFileEntity *)fileEntity;

- (void)updateThumbnail:(UIImage *)thumbnail filePath:(NSString *)path;

- (NSString *)newFileInfoLabel:(unsigned long long)fileSize modifiedDate:(NSDate *)modifiedDate;

- (void)updateFileEntities:(NSArray <CZFileEntity *> *)fileEntities;

- (void)removeFileEntities:(NSArray <CZFileEntity *> *)fileEntities;

- (void)addFileEntities:(NSArray <CZFileEntity *> *)fileEntities;

- (BOOL)isFileEntitySelected:(CZFileEntity *)fileEntity;

- (void)selectFileEntity:(CZFileEntity *)fileEntity isSelected:(BOOL)isSelected;

- (void)uploadFileEntity:(CZFileEntity *)fileEntity;

@end

NS_ASSUME_NONNULL_END
