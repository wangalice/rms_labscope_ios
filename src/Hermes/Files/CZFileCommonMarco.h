//
//  CZFileCommonMarco.h
//  Labscope
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#ifndef CZFileCommonMarco_h
#define CZFileCommonMarco_h

typedef NS_ENUM(NSUInteger, CZFileSortOrder) {
    CZSortByNameAscend,
    CZSortByNameDescend,
    CZSortByTimeAscend,
    CZSortByTimeDescend,
    CZSortingCount,
    CZSortByDefault = CZSortByNameAscend,
};

typedef NS_ENUM(NSUInteger, CZFileFilterOptions) {
    CZFileFilterTypeImage      = 0x1 << 0,
    CZFileFilterTypeVideo      = 0x1 << 1,
    CZFileFilterTypeReport     = 0x1 << 2,
    CZFileFilterTypeTemplate   = 0x1 << 3,
    CZFileFilterTypeOperator   = 0x1 << 4,
    CZFileFilterTypeAll        = 0x1 << 5,
};

typedef NS_ENUM(NSUInteger, CZFileControllerState) {
    CZFileControllerStateFree,
    CZFileControllerStateBusy
};

typedef NS_ENUM(NSUInteger, CZFileTransferActionType) {
    CZFileTransferActionCopy,
    CZFileTransferActionMove
};

typedef NS_ENUM(NSUInteger, CZFilesListShowMode) {
    CZFilesListShowListMode,
    CZFilesListShowGridMode
};

static CGFloat const kDefaultSearchFilterViewHeight = 56.0;
static CGFloat const kDefaultFileListBottomHeight = 64.0;

// notification keys
static NSString * const CZLocalFileListDidUpdateNotification = @"CZLocalFileListDidUpdateNotification";
static NSString * const CZLocalFileListDidUpdateNotificationFileList = @"CZLocalFileListDidUpdateNotificationFileList";
static NSString * const CZLocalFileListDidUpdateNotificationImagePropertiesMap = @"CZLocalFileListDidUpdateNotificationImagePropertiesMap";


#endif /* CZFileCommonMarco_h */
