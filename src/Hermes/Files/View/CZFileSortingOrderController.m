//
//  CZFileSortingOrderController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileSortingOrderController.h"
#import "CZFileCommonMarco.h"

@interface CZFileSortingOrderController ()

@property (nonatomic, strong) NSString *titleMessage;
@property (nonatomic, strong) NSArray *sortingOrders;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray <CZButton *>*sortingOrderItems;
@property (nonatomic, strong) CZFileSortOrderSelectionAction sortOrderSelectedAction;

@end

@implementation CZFileSortingOrderController

- (instancetype)initWithTitle:(NSString *)title sortingOrders:(NSArray <NSNumber *> *)sortingOrders {
    if (self = [super init]) {
        _titleMessage = [title copy];
        _sortingOrders = [sortingOrders copy];
        _sortingOrderItems = [NSMutableArray array];
        _selectedSortingOrder = kDefaultFileSortingOrderUnknown;
        self.preferredContentSize = CGSizeMake(208, 255);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.sortingOrderItems removeAllObjects];

    self.titleLabel.text = self.titleMessage;
    [self.view addSubview:self.titleLabel];
    self.titleLabel.frame = CGRectMake(24.0f, 21.0f, self.preferredContentSize.width - 24.0 * 2, 18.0f);
    
    for (NSNumber *sortingOrder in self.sortingOrders) {
        NSUInteger index = [self.sortingOrders indexOfObject:sortingOrder];
        NSString *title = [self sortingOrderWith:sortingOrder.integerValue];
        CZButton *button = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        [button setTitle:title forState:UIControlStateNormal];
        [button addTarget:self action:@selector(sortingOrderItemSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(24.0f,
                                  CGRectGetMaxY(self.titleLabel.frame) + 22.0f + (32.0f + 16.0f) * index,
                                  self.preferredContentSize.width - 24.0f * 2,
                                  32.0f);
        if (self.selectedSortingOrder == sortingOrder.integerValue) {
            button.selected = YES;
        }
        [self.sortingOrderItems addObject:button];
        [self.view addSubview:button];
    }
}

#pragma mark - Public method

- (void)didSelectSortOrder:(CZFileSortOrderSelectionAction)sortOrderSelectedAction {
    self.sortOrderSelectedAction = sortOrderSelectedAction;
}

- (void)setSelectedSortingOrder:(NSUInteger)selectedSortingOrder {
    _selectedSortingOrder = selectedSortingOrder;

    if (_selectedSortingOrder == kDefaultFileSortingOrderUnknown) {
        for (CZButton *button in self.sortingOrderItems) {
            button.selected = NO;
        }
    } else {
        for (CZButton *button in self.sortingOrderItems) {
            NSInteger sortOrder = [self.sortingOrderItems indexOfObject:button];
            if (sortOrder == selectedSortingOrder) {
                button.selected = YES;
            } else {
                button.selected = NO;
            }
        }
    }
}

#pragma mark - Private method

- (NSString *)sortingOrderWith:(CZFileSortOrder)sortOrder {
    switch (sortOrder) {
        case CZSortByNameAscend: {
            return L(@"FILE_SORTING_NAME_ASCENDING");
        }
            break;
        case CZSortByNameDescend: {
            return L(@"FILE_SORTING_NAME_DESCENDING");
        }
            break;
        case CZSortByTimeAscend:{
            return L(@"FILE_SORTING_DATE_ASCENDING");
        }
            break;
        case CZSortByTimeDescend: {
            return L(@"FILE_SORTING_DATE_DESCENDING");
        }
            break;
        case CZSortingCount: {}
            break;
    }
    return nil;
}

#pragma mark - Action

- (void)sortingOrderItemSelectedAction:(CZButton *)item {
    if (self.sortOrderSelectedAction) {
        CZFileSortOrder sortOrder = [self.sortingOrderItems indexOfObject:item];
        self.sortOrderSelectedAction(sortOrder);
        self.selectedSortingOrder = sortOrder;
    }
}

#pragma mark - Getter

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont cz_label2];
        _titleLabel.textColor = [UIColor cz_gs80];
    }
    return _titleLabel;
}

@end
