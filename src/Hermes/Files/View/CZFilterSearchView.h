//
//  CZFilterSearchView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZFileKit/CZFileKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CZFilterSearchViewDidShowBlock)();
typedef void(^CZFilterSearchViewDidDismissBlock)();

@class CZFilterSearchView;

@protocol CZFilterSearchViewDelegate <NSObject>

@optional
- (void)filterSearchViewDidChangeFilterKey:(CZFilterSearchView *)searchView;
- (void)filterSearchViewDidChangeFilterOptions:(CZFilterSearchView *)searchView;

@end

@interface CZFilterSearchView : UIControl

- (instancetype)initWithFilters:(NSInteger)filterOptions;

@property (nonatomic, weak) id <CZFilterSearchViewDelegate>delegate;
@property (nonatomic, assign, readonly) NSInteger filterOptions;
@property (nonatomic, strong, readonly) CZTextField *filterSearchTextField;
@property (nonatomic, assign) NSInteger selectedFilterOptions;
/** ! Default is NO, when you set YES, the filterSearch textfield will update size. */
@property (nonatomic, assign) BOOL fullScreen;

- (void)showWithCompletion:(CZFilterSearchViewDidShowBlock)completion;

- (void)dismissWithCompletion:(CZFilterSearchViewDidDismissBlock)completion;

@end

NS_ASSUME_NONNULL_END
