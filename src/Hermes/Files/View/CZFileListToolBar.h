//
//  CZFileListBottomView.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFileCommonMarco.h"

NS_ASSUME_NONNULL_BEGIN

@class CZFileListToolBar;

@protocol CZFileListBottomViewDelegate <NSObject>

@optional
- (void)fileListToolBar:(CZFileListToolBar *)fileListToolBar multiSelection:(BOOL)multiSelection;
- (void)fileListToolBar:(CZFileListToolBar *)fileListToolBar toggleListMode:(CZFilesListShowMode)listMode;

@end

@interface CZFileListToolBar : UIControl

@property (nonatomic, weak) id <CZFileListBottomViewDelegate> delegate;
@property (nonatomic, strong, readonly) CZButton *multiSelectionButtonItem;
@property (nonatomic, strong, readonly) UIProgressView *freeSpaceItem;
@property (nonatomic, strong, readonly) CZToggleButton *listModeToggleItem;
@property (nonatomic, strong, readonly) UILabel *freeSpaceLabel;

@end

NS_ASSUME_NONNULL_END
