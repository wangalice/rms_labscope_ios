//
//  CZFileGridCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFileCommonMarco.h"
#import <CZFileKit/CZFileKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity, CZFileGridCell;

@protocol CZFileGridCellDelegate <NSObject>

@optional
- (void)collectionCellDidSelectButton:(CZFileGridCell *)collectionCell;

@end

@interface CZFileGridCell : UICollectionViewCell

@property (nonatomic, weak) id <CZFileGridCellDelegate> delegate;
@property (nonatomic, strong, readonly) CZCheckBox *selectButton;
@property (nonatomic, assign) CZFileStorageType type;
@property (nonatomic, strong) CZFileEntity *fileEntity;
@property (nonatomic, strong, readonly) UILabel *fileLabel;
@property (nonatomic, strong, readonly) UILabel *fileInfoLabel;
@property (nonatomic, strong, readonly) UIImageView *thumbnailView;

- (void)setCellWithFileEntity:(CZFileEntity *)fileEntity
                     selected:(BOOL)isSelected
                    thumbnail:(UIImage *)thumbnail;

@end

NS_ASSUME_NONNULL_END
