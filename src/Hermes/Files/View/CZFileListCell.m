//
//  CZFileListCell.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileListCell.h"
#import "CZFileEntity+Icon.h"

@interface CZFileListCell ()

@property (nonatomic, strong) CZCheckBox *selectButton;
@property (nonatomic, strong) UILabel *fileLabel;
@property (nonatomic, strong) UILabel *fileInfoLabel;
@property (nonatomic, strong) UIImageView *thumbnailView;
@property (nonatomic, strong) UIButton *accessoryButton;
@property (nonatomic, strong) UIView *separatorLine;

@end

@implementation CZFileListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.selectButton];
        [self.contentView addSubview:self.thumbnailView];
        [self.contentView addSubview:self.fileLabel];
        [self.contentView addSubview:self.fileInfoLabel];
        [self.contentView addSubview:self.accessoryButton];
        [self.contentView addSubview:self.separatorLine];
        self.selectedBackgroundView = [[UIView alloc] init];
        self.selectedBackgroundView.backgroundColor = [UIColor cz_gs110];
        self.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs40]);   
        // auto layout
        [self addLayoutContraints];
    }
    return self;
}

- (void)dealloc {
    CZLogv(@"Works well");
}

#pragma mark - Public method
- (void)setCellWithFileEntity:(CZFileEntity *)fileEntity
                     selected:(BOOL)isSelected
                    thumbnail:(UIImage *)thumbnail {
    if (!self.thumbnailView.image ||
        self.fileEntity != fileEntity ||
        ![self.fileEntity.filePath isEqualToString:fileEntity.filePath] ||
        ![self.fileEntity.modificationDate isEqualToDate:fileEntity.modificationDate]) {
        if (thumbnail == nil) {
            thumbnail = [UIImage cz_imageWithIcon:fileEntity.icon];
            self.thumbnailView.contentMode = UIViewContentModeCenter;
        } else {
            self.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
        }
        self.thumbnailView.image = thumbnail;
    }
    
    self.fileEntity = fileEntity;
    self.type = fileEntity.storageType;
    self.selectButton.selected = isSelected;
    self.fileLabel.text = [fileEntity.filePath lastPathComponent];
    
    switch (fileEntity.storageType) {
        case kLocal: {
            [self.accessoryButton setImage:[UIImage imageNamed:A(@"Icons/uploadIcon.png")] forState:UIControlStateNormal];
        }
            break;
        case kRemote: {
            [self.accessoryButton setImage:[UIImage imageNamed:A(@"Icons/downloadIcon.png")] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Priavate method

- (void)addLayoutContraints {

    self.selectButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileInfoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.accessoryButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.selectButton.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:18.0].active = YES;
    [self.selectButton.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
    [self.selectButton.widthAnchor constraintEqualToConstant:44.0].active = YES;
    [self.selectButton.heightAnchor constraintEqualToConstant:44.0].active = YES;
    
    [self.thumbnailView.leftAnchor constraintEqualToAnchor:self.selectButton.rightAnchor constant:2.0].active = YES;
    [self.thumbnailView.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
    [self.thumbnailView.widthAnchor constraintEqualToConstant:56.0f].active = YES;
    [self.thumbnailView.heightAnchor constraintEqualToConstant:56.0f].active = YES;
    
    [self.fileLabel.leftAnchor constraintEqualToAnchor:self.thumbnailView.rightAnchor constant:15.0f].active = YES;
    [self.fileLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:13.0f].active = YES;
    [self.fileLabel.heightAnchor constraintEqualToConstant:18.0f].active = YES;
    [self.fileLabel.rightAnchor constraintEqualToAnchor:self.accessoryButton.leftAnchor].active = YES;
    
    [self.fileInfoLabel.leftAnchor constraintEqualToAnchor:self.fileLabel.leftAnchor].active = YES;
    [self.fileInfoLabel.topAnchor constraintEqualToAnchor:self.fileLabel.bottomAnchor].active = YES;
    [self.fileInfoLabel.rightAnchor constraintEqualToAnchor:self.fileInfoLabel.rightAnchor].active = YES;
    [self.fileInfoLabel.heightAnchor constraintEqualToConstant:18.0f].active = YES;
    
    [self.accessoryButton.centerYAnchor constraintEqualToAnchor:self.contentView.centerYAnchor].active = YES;
    [self.accessoryButton.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-32.0f].active = YES;
    [self.accessoryButton.widthAnchor constraintEqualToConstant:44.0f].active = YES;
    [self.accessoryButton.heightAnchor constraintEqualToConstant:44.0f].active = YES;
}

#pragma mark - Action

- (void)selectAction:(CZCheckBox *)sender {
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(tableViewCellDidSelectButton:)]) {
        [self.delegate tableViewCellDidSelectButton:self];
    }
}

- (void)accessoryAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(tableViewCellDidClickAccessoryButton:)]) {
        [self.delegate tableViewCellDidClickAccessoryButton:self];
    }
}

#pragma mark - Getters

- (CZCheckBox *)selectButton {
    if (_selectButton == nil) {
        _selectButton = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
        _selectButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _selectButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_selectButton addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectButton;
}

- (UIImageView *)thumbnailView {
    if (_thumbnailView == nil) {
        _thumbnailView = [[UIImageView alloc] init];
        _thumbnailView.clipsToBounds = YES;
        _thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _thumbnailView;
}

- (UILabel *)fileLabel {
    if (_fileLabel == nil) {
        _fileLabel = [[UILabel alloc] init];
        _fileLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs50], [UIColor cz_gs100]);
        _fileLabel.font = [UIFont cz_caption];
    }
    return _fileLabel;
}

- (UILabel *)fileInfoLabel {
    if (_fileInfoLabel == nil) {
        _fileInfoLabel = [[UILabel alloc] init];
        _fileInfoLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80],[UIColor cz_gs80]);
        _fileInfoLabel.font = [UIFont cz_caption];
    }
    return _fileInfoLabel;
}

- (UIButton *)accessoryButton {
    if (_accessoryButton == nil) {
        _accessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _accessoryButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _accessoryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_accessoryButton addTarget:self action:@selector(accessoryAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _accessoryButton;
}

- (UIView *)separatorLine {
    if (_separatorLine == nil) {
        _separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.contentView.bounds.size.height - 1.0, self.contentView.bounds.size.width, 1.0)];
        _separatorLine.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _separatorLine.backgroundColor = [UIColor cz_gs110];
    }
    return _separatorLine;
}

@end
