//
//  CZFileListBottomView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileListToolBar.h"
#import "CZToolbar.h"

@interface CZFileListToolBar ()

@property (nonatomic, strong) CZButton *multiSelectionButtonItem;
@property (nonatomic, strong) UIProgressView *freeSpaceItem;
@property (nonatomic, strong) UILabel *freeSpaceLabel;
@property (nonatomic, strong) CZToggleButton *listModeToggleItem;

@end

@implementation CZFileListToolBar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self addSubview:self.multiSelectionButtonItem];
        [self addSubview:self.freeSpaceItem];
        [self addSubview:self.freeSpaceLabel];
        [self addSubview:self.listModeToggleItem];

        self.multiSelectionButtonItem.translatesAutoresizingMaskIntoConstraints = NO;
        self.freeSpaceItem.translatesAutoresizingMaskIntoConstraints = NO;
        self.freeSpaceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.listModeToggleItem.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.multiSelectionButtonItem.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:CZToolbarDefaultInsets.left].active = YES;
        [self.multiSelectionButtonItem.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [self.multiSelectionButtonItem.widthAnchor constraintEqualToConstant:64.0f].active = YES;
        [self.multiSelectionButtonItem.heightAnchor constraintEqualToConstant:48.0f].active = YES;
        
        [self.freeSpaceItem.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
        [self.freeSpaceItem.centerYAnchor constraintEqualToAnchor:self.centerYAnchor constant:-4.0f].active = YES;
        [self.freeSpaceItem.widthAnchor constraintEqualToConstant:247.0f].active = YES;
        [self.freeSpaceItem.heightAnchor constraintEqualToConstant:4.0f].active = YES;
        
        [self.freeSpaceLabel.centerXAnchor constraintEqualToAnchor:self.freeSpaceItem.centerXAnchor].active = YES;
        [self.freeSpaceLabel.topAnchor constraintEqualToAnchor:self.freeSpaceItem.bottomAnchor].active = YES;
        [self.freeSpaceLabel.heightAnchor constraintEqualToConstant:18.0f].active = YES;
        
        [self.listModeToggleItem.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [self.listModeToggleItem.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-CZToolbarDefaultInsets.right].active = YES;
        [self.listModeToggleItem.widthAnchor constraintEqualToConstant:128.0f].active = YES;
        [self.listModeToggleItem.heightAnchor constraintEqualToConstant:48.0f].active = YES;
    }
    return self;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

#pragma mark - Action

- (void)listModeToggleAction:(CZToggleButton *)sender {
    CZFilesListShowMode listMode = sender.selectedIndex == 0 ? CZFilesListShowListMode : CZFilesListShowGridMode;
    if (self.delegate && [self.delegate respondsToSelector:@selector(fileListToolBar:toggleListMode:)]) {
        [self.delegate fileListToolBar:self toggleListMode:listMode];
    }
}

- (void)multiSelectionAction:(CZButton *)sender {
    sender.selected = !sender.selected;

    if (self.delegate && [self.delegate respondsToSelector:@selector(fileListToolBar:multiSelection:)]) {
        [self.delegate fileListToolBar:self multiSelection:sender.selected];
    }
}

#pragma mark - Getters

- (CZButton *)multiSelectionButtonItem {
    if (_multiSelectionButtonItem == nil) {
        _multiSelectionButtonItem = [CZButton buttonWithLevel:CZControlEmphasisDefault];
        
        UIImage *selectAllImage = [[CZIcon iconNamed:@"select-all-files"] imageForControlWithState:UIControlStateNormal];
        [_multiSelectionButtonItem setImage:selectAllImage forState:UIControlStateNormal];
        
        UIImage *unselectAllImage = [[CZIcon iconNamed:@"unselect-all-files"] imageForControlWithState:UIControlStateSelected];
        [_multiSelectionButtonItem setImage:unselectAllImage forState:UIControlStateSelected];
        
        [_multiSelectionButtonItem addTarget:self action:@selector(multiSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _multiSelectionButtonItem;
}

- (UIProgressView *)freeSpaceItem {
    if (_freeSpaceItem == nil) {
        _freeSpaceItem = [[UIProgressView alloc] init];
        _freeSpaceItem.progress = 0.3;
        _freeSpaceItem.progressTintColor = [UIColor cz_gs40];
        _freeSpaceItem.trackTintColor = [UIColor cz_gs90];
    }
    return _freeSpaceItem;
}

- (UILabel *)freeSpaceLabel {
    if (_freeSpaceLabel == nil) {
        _freeSpaceLabel = [[UILabel alloc] init];
        _freeSpaceLabel.textAlignment = NSTextAlignmentCenter;
        _freeSpaceLabel.font = [UIFont cz_label1];
        _freeSpaceLabel.text = [NSString stringWithFormat:@"%@ %@", L(@"FILES_FREE_SPACE_TITLE"), [CZCommonUtils freeSpaceString]];
        _freeSpaceLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80], [UIColor cz_gs80]);
    }
    return _freeSpaceLabel;
}

- (CZToggleButton *)listModeToggleItem {
    if (_listModeToggleItem == nil) {
        _listModeToggleItem = [[CZToggleButton alloc] initWithItems:@[[CZIcon iconNamed:@"list-mode"], [CZIcon iconNamed:@"grid-mode"]]];
        _listModeToggleItem.selectedIndex = 0;
        [_listModeToggleItem addTarget:self action:@selector(listModeToggleAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _listModeToggleItem;
}

@end
