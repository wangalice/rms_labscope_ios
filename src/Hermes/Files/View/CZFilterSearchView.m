//
//  CZFilterSearchView.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFilterSearchView.h"
#import "CZFileCommonMarco.h"

@interface CZFilterSearchView ()<
CZTextFieldDelegate
>

@property (nonatomic, assign) NSInteger filterOptions;
@property (nonatomic, strong) CZTextField *filterSearchTextField;
@property (nonatomic, strong) NSString *filterKeywords;
@property (nonatomic, strong) NSMutableArray <UIButton *> *filterOptionItems;

@end

@implementation CZFilterSearchView

- (instancetype)initWithFilters:(NSInteger)filterOptions {
    if (self = [super init]) {
        _filterOptions = filterOptions;
        _selectedFilterOptions = CZFileFilterTypeAll;
        _filterKeywords = nil;
        _fullScreen = YES;
        _filterOptionItems = [NSMutableArray array];

        [self addSubview:self.filterSearchTextField];
        [self generateFilterItems];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat kDefaultInputBoxWidth = _fullScreen ? 280.0f : 109.0;
    CGFloat kDefaultInputBoxHeight = 32.0f;
    CGFloat kDefaultInputBoxY = CGRectGetMidY(self.bounds) - kDefaultInputBoxHeight /2;
    CGFloat kDefaultInputBoxX = _fullScreen ? CGRectGetMaxX(self.bounds) - kDefaultInputBoxWidth - 32.0f : CGRectGetMaxX(self.bounds) - kDefaultInputBoxWidth - 16.0f;
    
    self.filterSearchTextField.frame = CGRectMake(kDefaultInputBoxX, kDefaultInputBoxY, kDefaultInputBoxWidth, kDefaultInputBoxHeight);
    
    for (CZButton *button in self.filterOptionItems) {
        NSInteger index = [self.filterOptionItems indexOfObject:button];
        button.frame = CGRectMake(16.0f + (150.0 + 16.0f) * index, CGRectGetMidY(self.bounds) - kDefaultInputBoxHeight/2, 150.f, 32.0f);
    }
}

#pragma mark - Public Method

- (void)showWithCompletion:(CZFilterSearchViewDidShowBlock)completion {
    if (self.hidden == NO) {
        return;
    }
    self.hidden = NO;
    [self.filterSearchTextField becomeFirstResponder];
    
    if (completion) {
        completion();
    }
}

- (void)dismissWithCompletion:(CZFilterSearchViewDidDismissBlock)completion {
    if (self.hidden == YES) {
        return;
    }
    self.hidden = YES;
    self.filterKeywords = nil;
    [self.filterSearchTextField resignFirstResponder];
    
    [self textFieldInputTextChanged:nil];
    
    if (completion) {
        completion();
    }
}

- (void)setFullScreen:(BOOL)fullScreen {
    _fullScreen = fullScreen;

    [self layoutIfNeeded];
}

- (void)setSelectedFilterOptions:(NSInteger)selectedFilterOptions {
    _selectedFilterOptions = selectedFilterOptions;

    for (CZButton *button in self.filterOptionItems) {
        CZFileFilterOptions filterOption = button.tag;
        button.selected = (_selectedFilterOptions & filterOption) == filterOption;
    }
}

#pragma mark - Private Method

- (void)generateFilterItems {
    [self.filterOptionItems removeAllObjects];
    
    for (int i = 0; i < CZFileFilterTypeAll; i++) {
        BOOL enableFilterOption = (self.filterOptions >> i) & 1;
        if (enableFilterOption == YES) {
            CZFileFilterOptions filterOption = 0x1 << i;
            UIButton *button = [self buttonWithFileFilterOptions:filterOption];
            button.tag = filterOption;
            [button addTarget:self action:@selector(filterItemSelectAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            [self.filterOptionItems addObject:button];
        } else {
            continue;
        }
    }
}

- (NSString *)filterStringWithFilterOption:(CZFileFilterOptions)filtOption {
    switch (filtOption) {
        case CZFileFilterTypeAll: {
            return L(@"FILE_FILTER_SHOW_ALL");
        }
            break;
        case CZFileFilterTypeImage: {
            return L(@"FILE_FILTER_IMAGE");
        }
            break;
        case CZFileFilterTypeVideo: {
            return L(@"FILE_FILTER_VIDEO");
        }
            break;
        case CZFileFilterTypeReport: {
            return L(@"FILE_FILTER_REPORT");
        }
            break;
        case CZFileFilterTypeOperator: {
            return L(@"FILE_FILTER_OPERATOR");
        }
            break;
        case CZFileFilterTypeTemplate: {
            return L(@"FILE_FILTER_TEMPLATE");
        }
            break;
    }
}

- (UIButton *)buttonWithFileFilterOptions:(CZFileFilterOptions)filterOption {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    NSString *title = [self filterStringWithFilterOption:filterOption];
    button.titleLabel.font = [UIFont cz_body1];
    [button setTitleColor:[UIColor cz_gs50] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button setBackgroundImage:[self imageForButtonState:UIControlStateNormal] forState:UIControlStateNormal];
    [button setBackgroundImage:[self imageForButtonState:UIControlStateSelected] forState:UIControlStateSelected];
    return button;
}

- (UIImage *)imageForButtonState:(UIControlState)state {
    NSString *imageName = @"filter";
    UIColor *fillColor = [UIColor cz_gs110];
    UIColor *storkeColor = [UIColor cz_gs120];
    switch (state) {
        case UIControlStateSelected: {
            imageName = [imageName stringByAppendingString:@"-selected"];
            fillColor = [UIColor cz_gs90];
            storkeColor = [UIColor cz_gs120];
        }
            break;
        case UIControlStateNormal:
        default: {
            imageName = [imageName stringByAppendingString:@"-enable"];
        }
            break;
    }
    
    if ([CZShapeImage imageNamed:imageName].UIImage == nil) {
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, 34.0, 32.0) cornerRadius:16.0];
        path.lineWidth = 1.0;
        
        [[CZShapeImage imageWithPath:path
                           fillColor:fillColor
                         strokeColor:storkeColor
                           capInsets:UIEdgeInsetsMake(16.0, 16.0, 16.0, 16.0)]
         setName:imageName];
    }
    return [CZShapeImage imageNamed:imageName].UIImage;
}

#pragma mark - Action

- (void)filterItemSelectAction:(CZButton *)sender {
    sender.selected = !sender.selected;
    
    CZFileFilterOptions filterOption = sender.tag;
    if (self.selectedFilterOptions == CZFileFilterTypeAll) {
        _selectedFilterOptions = filterOption;
    } else {
        if (sender.selected) {
            _selectedFilterOptions = _selectedFilterOptions | filterOption;
        } else {
            _selectedFilterOptions = _selectedFilterOptions & ~filterOption ? : CZFileFilterTypeAll;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(filterSearchViewDidChangeFilterOptions:)]) {
        [self.delegate filterSearchViewDidChangeFilterOptions:self];
    }
}

- (void)textFieldInputTextChanged:(CZTextField *)textField {
    self.filterKeywords = textField.text;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(filterSearchViewDidChangeFilterKey:)]) {
        [self.delegate filterSearchViewDidChangeFilterKey:self];
    }
}

#pragma mark - Getters

- (CZTextField *)filterSearchTextField {
    if (_filterSearchTextField == nil) {
        _filterSearchTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightIcon:[CZIcon iconNamed:@"search"]];
        _filterSearchTextField.delegate = self;
        _filterSearchTextField.showClearButtonWhileEditing = YES;
        [_filterSearchTextField addTarget:self action:@selector(textFieldInputTextChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _filterSearchTextField;
}

@end
