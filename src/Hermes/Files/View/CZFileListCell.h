//
//  CZFileListCell.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZFileCommonMarco.h"
#import <CZFileKit/CZFileKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZFileEntity, CZFileListCell;

@protocol CZFileListCellDelegate <NSObject>

@optional
- (void)tableViewCellDidSelectButton:(CZFileListCell *)tableViewCell;
- (void)tableViewCellDidClickAccessoryButton:(CZFileListCell *)tableViewCell;

@end

@interface CZFileListCell : UITableViewCell

@property (nonatomic, assign) id <CZFileListCellDelegate> delegate;
@property (nonatomic, strong, readonly) CZCheckBox *selectButton;
@property (nonatomic, assign) CZFileStorageType type;
@property (nonatomic, strong) CZFileEntity *fileEntity;
@property (nonatomic, strong, readonly) UILabel *fileLabel;
@property (nonatomic, strong, readonly) UILabel *fileInfoLabel;
@property (nonatomic, strong, readonly) UIImageView *thumbnailView;
@property (nonatomic, strong, readonly) UIButton *accessoryButton;

- (void)setCellWithFileEntity:(CZFileEntity *)fileEntity
                     selected:(BOOL)isSelected
                    thumbnail:(UIImage *)thumbnail;

@end

NS_ASSUME_NONNULL_END
