//
//  CZFileSortingOrderController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

enum {
    kDefaultFileSortingOrderUnknown = -1
};

typedef void(^CZFileSortOrderSelectionAction)(NSUInteger sortOrder);

@interface CZFileSortingOrderController : UIViewController

- (instancetype)initWithTitle:(NSString *)title sortingOrders:(NSArray <NSNumber *> *)sortingOrders;

/** Default sorting order is kDefaultFileSortingOrderUnknown */
@property (nonatomic, assign) NSUInteger selectedSortingOrder;

- (void)didSelectSortOrder:(CZFileSortOrderSelectionAction)sortOrderSelectedAction;

@end

NS_ASSUME_NONNULL_END
