//
//  CZFileGridCell.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFileGridCell.h"
#import "CZFileEntity+Icon.h"

@interface CZFileGridCell ()

@property (nonatomic, strong) CZCheckBox *selectButton;
@property (nonatomic, strong) UILabel *fileLabel;
@property (nonatomic, strong) UILabel *fileInfoLabel;
@property (nonatomic, strong) UIImageView *thumbnailView;

@end

@implementation CZFileGridCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.selectButton];
        [self.contentView addSubview:self.thumbnailView];
        [self.contentView addSubview:self.fileLabel];
        [self.contentView addSubview:self.fileInfoLabel];
        
        [self addLayoutContraints];
        
        self.layer.borderWidth = 1.0;
        self.layer.cz_borderColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs40]);
        self.layer.cornerRadius = kControlDefaultBorderRadius;
        self.layer.masksToBounds = YES;
        self.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs40]);
    }
    return self;
}

#pragma mark - Public method

- (void)setCellWithFileEntity:(CZFileEntity *)fileEntity
                     selected:(BOOL)isSelected
                    thumbnail:(UIImage *)thumbnail {
    if (!self.thumbnailView.image ||
        self.fileEntity != fileEntity ||
        ![self.fileEntity.filePath isEqualToString:fileEntity.filePath] ||
        ![self.fileEntity.modificationDate isEqualToDate:fileEntity.modificationDate]) {
        if (thumbnail == nil) {
            thumbnail = [UIImage cz_imageWithIcon:fileEntity.icon];
            self.thumbnailView.contentMode = UIViewContentModeCenter;
        } else {
            self.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
        }
        self.thumbnailView.image = thumbnail;
    }
    
    self.fileEntity = fileEntity;
    self.type = fileEntity.storageType;
    self.selectButton.selected = isSelected;
    self.fileLabel.text = [fileEntity.filePath lastPathComponent];
}

#pragma mark - Private method

- (void)addLayoutContraints {
    self.thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileInfoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.selectButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.thumbnailView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = YES;
    [self.thumbnailView.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = YES;
    [self.thumbnailView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
    [self.thumbnailView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-56.0].active = YES;
    
    [self.fileLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:8.0f].active = YES;
    [self.fileLabel.bottomAnchor constraintEqualToAnchor:self.fileInfoLabel.topAnchor].active = YES;
    [self.fileLabel.rightAnchor constraintEqualToAnchor:self.selectButton.leftAnchor constant:-10.0f].active = YES;
    [self.fileLabel.heightAnchor constraintEqualToConstant:18.0f].active = YES;
    
    [self.fileInfoLabel.leftAnchor constraintEqualToAnchor:self.fileLabel.leftAnchor].active = YES;
    [self.fileInfoLabel.rightAnchor constraintEqualToAnchor:self.fileLabel.rightAnchor].active = YES;
    [self.fileInfoLabel.heightAnchor constraintEqualToAnchor:self.fileLabel.heightAnchor].active = YES;
    [self.fileInfoLabel.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-10].active = YES;
    
    [self.selectButton.widthAnchor constraintEqualToConstant:44.0f].active = YES;
    [self.selectButton.heightAnchor constraintEqualToConstant:44.0f].active = YES;
    [self.selectButton.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor constant:-6.0f].active = YES;
    [self.selectButton.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-2.0f].active = YES;
}

#pragma mark - Action

- (void)selectAction:(CZCheckBox *)sender {
    sender.selected = !sender.selected;
    if ([self.delegate respondsToSelector:@selector(collectionCellDidSelectButton:)]) {
        [self.delegate collectionCellDidSelectButton:self];
    }
}

#pragma mark - Getters

- (CZCheckBox *)selectButton {
    if (_selectButton == nil) {
        _selectButton = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
        _selectButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _selectButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_selectButton addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _selectButton;
}

- (UIImageView *)thumbnailView {
    if (_thumbnailView == nil) {
        _thumbnailView = [[UIImageView alloc] init];
        _thumbnailView.clipsToBounds = YES;
        _thumbnailView.backgroundColor = [UIColor cz_gs110];
        _thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _thumbnailView;
}

- (UILabel *)fileLabel {
    if (_fileLabel == nil) {
        _fileLabel = [[UILabel alloc] init];
        _fileLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs50], [UIColor cz_gs100]);
        _fileLabel.font = [UIFont cz_caption];
    }
    return _fileLabel;
}

- (UILabel *)fileInfoLabel {
    if (_fileInfoLabel == nil) {
        _fileInfoLabel = [[UILabel alloc] init];
        _fileInfoLabel.cz_textColorPicker = CZThemeColorWithColors([UIColor cz_gs80],[UIColor cz_gs80]);
        _fileInfoLabel.font = [UIFont cz_caption];
    }
    return _fileInfoLabel;
}

@end
