//
//  CZFilesTask.m
//  Hermes
//
//  Created by Li, Junlin on 1/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFilesTask.h"
#import "CZFilesViewModel.h"

@interface CZFilesTask ()

@property (nonatomic, strong) CZFilesViewModel *viewModel;

@end

@implementation CZFilesTask

@dynamic delegate;

- (instancetype)initWithIdentifier:(NSString *)identifier {
    self = [super initWithIdentifier:identifier];
    if (self) {}
    return self;
}

- (void)dealloc {
    CZLogv(@"Works well");
    _viewModel.delegate = nil;
    _viewModel = nil;
}

- (void)didEnterBackground {
    [super didEnterBackground];
}

- (void)didEnterForeground {
    [super didEnterForeground];
    
    [self initialFilesViewModel];
}

#pragma mark - Public method

- (void)resetFilesViewModel {
    self.viewModel.delegate = nil;
    self.viewModel = nil;
}

#pragma mark - Private method

- (void)initialFilesViewModel {
    if (self.viewModel == nil) {
        self.viewModel = [[CZFilesViewModel alloc] init];
        self.viewModel.delegate = self.delegate;
    }
}

@end
