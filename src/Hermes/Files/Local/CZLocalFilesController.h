//
//  CZLocalFilesController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZLocalFilesViewModel, CZLocalFilesController;

@protocol CZLocalFilesControllerDelegate <NSObject>

@optional
- (void)localFilesController:(CZLocalFilesController *)localFilesController didEnterFullScreen:(BOOL)isFullScreen;

@end

@interface CZLocalFilesController : UIViewController

@property (nonatomic, weak) id <CZLocalFilesControllerDelegate> delegate;

- (instancetype)initWithViewModel:(CZLocalFilesViewModel *)viewModel;

@end

NS_ASSUME_NONNULL_END
