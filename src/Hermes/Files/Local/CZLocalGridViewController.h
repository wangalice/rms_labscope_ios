//
//  CZLocalGridViewController.h
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class CZFilesListViewModel;

@protocol CZLocalFileOpenProtocol;

@interface CZLocalGridViewController : UICollectionViewController

@property (nonatomic, assign, nullable) id <CZLocalFileOpenProtocol> delegate;
@property (nonatomic, assign) BOOL isActive;

- (instancetype)initWithViewModel:(CZFilesListViewModel *)viewModel;

- (void)scrollSelectedRowToVisible;

@end

NS_ASSUME_NONNULL_END
