//
//  CZLocalFilesController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLocalFilesController.h"
#import "CZLocalFileListController.h"
#import "CZLocalGridViewController.h"
#import "CZFilesListViewModel.h"
#import "CZLocalFilesViewModel.h"
#import "CZFileListToolBar.h"
#import "CZFilterSearchView.h"
#import "CZPopoverController.h"
#import "CZFileSortingOrderController.h"
#import "CZFileCommonMarco.h"
#import "CZLocalFileOpenProtocol.h"
#import "CZMoviePlayerViewController.h"
#import "CZFileNameTemplateConfigurationViewController.h"
#import "CZToolbar.h"
#import "CZMultitaskViewController.h"
#import "CZPreviewController.h"
#import "CZLogPreviewViewController.h"
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZLocalFilesController () <
CZFilterSearchViewDelegate,
CZFileListBottomViewDelegate,
CZLocalFileOpenProtocol,
CZPopoverControllerDelegate
>

@property (nonatomic, strong) CZToolbar *navigationToolbar;
@property (nonatomic, strong) CZLocalFileListController *fileListController;
@property (nonatomic, strong) CZLocalGridViewController *fileGridController;
@property (nonatomic, strong) CZFileListToolBar *fileListToolBar;
@property (nonatomic, strong) CZLocalFilesViewModel *viewModel;
@property (nonatomic, strong) CZFilterSearchView *filterSearchView;
@property (nonatomic, strong) CZPopoverController *sortingOrderPopoverController;
@property (nonatomic, strong) NSLayoutConstraint *filterSearchViewHeightConstant;

@end

@implementation CZLocalFilesController

- (instancetype)initWithViewModel:(CZLocalFilesViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
        [self bindViewWithViewModelState];
    }
    return self;
}

- (void)dealloc {
    self.fileGridController.delegate = nil;
    self.fileGridController = nil;
    self.fileListController.delegate = nil;
    self.fileListController = nil;
    
    [self unbindViewWithViewModelState];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CZToolbarItem *titleItem = [CZToolbarItem labelItemWithIdentifier:CZToolbarItemIdentifierAnonymous];
    titleItem.label.text = L(@"FILES_LOCAL_TITLE");
    titleItem.label.font = [UIFont cz_h1];
    titleItem.label.textColor = [UIColor cz_gs50];
    
    CZToolbarItem *sortItem = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAnonymous];
    [sortItem.button cz_setImageWithIcon:[CZIcon iconNamed:@"sort"]];
    [sortItem.button addTarget:self action:@selector(sortAction:) forControlEvents:UIControlEventTouchUpInside];
    
    CZToolbarItem *searchItem = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAnonymous];
    [searchItem.button cz_setImageWithIcon:[CZIcon iconNamed:@"search-filter"]];
    [searchItem.button addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    
    CZToolbarItem *splitItem = [CZToolbarItem buttonItemWithIdentifier:CZToolbarItemIdentifierAnonymous];
    [splitItem.button cz_setImageWithIcon:[CZIcon iconNamed:@"remote-files"]];
    [splitItem.button addTarget:self action:@selector(splitAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationToolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, 20.0, self.view.bounds.size.width, 64.0)];
    self.navigationToolbar.backgroundColor = [UIColor cz_gs100];
    self.navigationToolbar.items = @[titleItem, [CZToolbarItem flexibleSpaceItem], sortItem, searchItem, splitItem];
    [self.view addSubview:self.navigationToolbar];
    
    [self addChildViewController:self.fileListController];
    [self addChildViewController:self.fileGridController];
    [self.view addSubview:self.fileListController.view];
    [self.view addSubview:self.fileGridController.view];
    [self.fileListController didMoveToParentViewController:self];
    [self.fileGridController didMoveToParentViewController:self];
    
    [self.view addSubview:self.fileListToolBar];
    [self.view addSubview:self.filterSearchView];

    self.filterSearchView.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileListController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileGridController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.fileListToolBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.filterSearchView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.filterSearchView.topAnchor constraintEqualToAnchor:self.navigationToolbar.bottomAnchor].active = YES;
    [self.filterSearchView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    self.filterSearchViewHeightConstant = [self.filterSearchView.heightAnchor constraintEqualToConstant:0];
    self.filterSearchViewHeightConstant.active = YES;
    
    [self.fileListController.view.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.fileListController.view.topAnchor constraintEqualToAnchor:self.filterSearchView.bottomAnchor constant:1.0f].active = YES;
    [self.fileListController.view.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.fileListController.view.bottomAnchor constraintEqualToAnchor:self.fileListToolBar.topAnchor].active = YES;
    
    [self.fileGridController.view.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.fileGridController.view.topAnchor constraintEqualToAnchor:self.filterSearchView.bottomAnchor].active = YES;
    [self.fileGridController.view.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.fileGridController.view.bottomAnchor constraintEqualToAnchor:self.fileListToolBar.topAnchor].active = YES;
    
    [self.fileListToolBar.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.fileListToolBar.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.fileListToolBar.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.fileListToolBar.heightAnchor constraintEqualToConstant:kDefaultFileListBottomHeight].active = YES;

    [self switchChildController];
}

#pragma mark - Private method

- (void)switchChildController {
    switch (self.viewModel.listMode) {
        case CZFilesListShowGridMode: {
            self.fileListController.isActive = NO;
            self.fileListController.delegate = nil;

            self.fileGridController.isActive = YES;
            self.fileGridController.delegate = self;

            [self.fileListController willMoveToParentViewController:nil];
            [self.fileListController.view removeFromSuperview];
            [self.fileListController removeFromParentViewController];
            
            [self addChildViewController:self.fileGridController];
            [self.view addSubview:self.fileGridController.view];
            [self.fileGridController.view.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
            [self.fileGridController.view.topAnchor constraintEqualToAnchor:self.filterSearchView.bottomAnchor].active = YES;
            [self.fileGridController.view.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
            [self.fileGridController.view.bottomAnchor constraintEqualToAnchor:self.fileListToolBar.topAnchor].active = YES;
            [self.fileGridController didMoveToParentViewController:self];
        }
            break;
        case CZFilesListShowListMode: {
            self.fileGridController.isActive = NO;
            self.fileGridController.delegate = nil;
            
            self.fileListController.isActive = YES;
            self.fileListController.delegate = self;
            
            [self.fileGridController willMoveToParentViewController:nil];
            [self.fileGridController.view removeFromSuperview];
            [self.fileGridController removeFromParentViewController];
            
            [self addChildViewController:self.fileListController];
            [self.view addSubview:self.fileListController.view];
            [self.fileListController.view.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
            [self.fileListController.view.topAnchor constraintEqualToAnchor:self.filterSearchView.bottomAnchor constant:1.0f].active = YES;
            [self.fileListController.view.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
            [self.fileListController.view.bottomAnchor constraintEqualToAnchor:self.fileListToolBar.topAnchor].active = YES;
            [self.fileListController didMoveToParentViewController:self];
        }
            break;
    }
}

- (void)updateFreeSpace {
    self.fileListToolBar.freeSpaceItem.progress = self.viewModel.freeSpacePercent;
    self.fileListToolBar.freeSpaceLabel.text = [NSString stringWithFormat:@"%@ %@", L(@"FILES_FREE_SPACE_TITLE"), [CZCommonUtils freeSpaceString]];
}

- (void)updateMultiSelectionItemState {
    self.fileListToolBar.multiSelectionButtonItem.selected = self.viewModel.shouldSelectedAllFileEntities;
}

- (void)bindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.viewModel addObserver:self
                         forKeyPath:observableKeyPath
                            options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                            context:NULL];
    }
}

- (void)unbindViewWithViewModelState {
    for (NSString *observableKeyPath in [self observableKeyPaths]) {
        [self.viewModel removeObserver:self
                            forKeyPath:observableKeyPath
                               context:NULL];
    }
}

- (NSArray <NSString *> *)observableKeyPaths {
    return @[@"shouldSelectedAllFileEntities", @"keyword"];
}

#pragma mark - CZFilterSearchViewDelegate

- (void)filterSearchViewDidChangeFilterKey:(CZFilterSearchView *)searchView {
    self.viewModel.keyword = searchView.filterSearchTextField.text;
}

- (void)filterSearchViewDidChangeFilterOptions:(CZFilterSearchView *)searchView {
    [self.viewModel clearAllSelectedFileEntities];
    self.viewModel.filterType = searchView.selectedFilterOptions;
    [self.viewModel synchronizeFilters];
    
    if (self.fileGridController.isActive) {
        [self.fileGridController scrollSelectedRowToVisible];
    } else if (self.fileListController.isActive) {
        [self.fileListController scrollSelectedRowToVisible];
    }
}

#pragma mark - CZFileListBottomViewDelegate

- (void)fileListToolBar:(CZFileListToolBar *)fileListToolBar multiSelection:(BOOL)multiSelection {
    if (multiSelection) {
        [self.viewModel selectAllFileEntities];
    } else {
        [self.viewModel clearAllSelectedFileEntities];
    }
    if (self.fileGridController.isActive) {
        [self.fileGridController.collectionView reloadData];
    } else if (self.fileListController.isActive) {
        [self.fileListController.tableView reloadData];
    }
    [self updateMultiSelectionItemState];
}

- (void)fileListToolBar:(CZFileListToolBar *)fileListToolBar toggleListMode:(CZFilesListShowMode)listMode {
    self.viewModel.listMode = listMode;
    [self switchChildController];
}

#pragma mark - CZPopoverControllerDelegate

- (void)popoverControllerDidDismiss:(CZPopoverController *)popoverController {
    if (popoverController == self.sortingOrderPopoverController) {
        self.sortingOrderPopoverController.delegate = nil;
        self.sortingOrderPopoverController = nil;
    }
}

#pragma mark - CZLocalFileOpenProtocol

- (void)controller:(UIViewController *)controller didRequestOpeningFileAtPath:(NSString *)filePath {
    if (controller == self.fileListController && self.fileListController.isActive == NO) {
        return;
    }
    
    if (controller == self.fileGridController && self.fileGridController.isActive == NO) {
        return;
    }
    
    CZMultitaskManager *multitaskManager = self.multitaskViewController.multitaskManager;
    
    if (multitaskManager.filesTask.state != CZTaskStateForeground) {
#if FILE_REFACTOR
        [[CZToastMessage sharedInstance] showNotificationMessage:L(@"FILE_DOWNLOAD_FINISHED_MESSAGE")];
#endif
        return;
    }
    
#if DEBUG
    if ([filePath.pathExtension isEqualToString:@"log"]) {
        CZLogPreviewViewController *logPreviewViewController = [[CZLogPreviewViewController alloc] initWithLogFile:filePath];
        [self.navigationController pushViewController:logPreviewViewController animated:YES];
        return;
    }
#endif
    
    NSString *fileName = filePath.lastPathComponent;
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:fileName.pathExtension];
    switch (fileFormat) {
        case kCZFileFormatCZI:
        case kCZFileFormatJPEG:
        case kCZFileFormatTIF: {
            multitaskManager.imageTask.filePath = filePath;
            [multitaskManager switchToImageTask];
            break;
        }
        case kCZFileFormatPDF:
        case kCZFileFormatCSV: {
            CZPreviewController *previewController = [[CZPreviewController alloc] init];
            previewController.previewFileURL = [NSURL fileURLWithPath:filePath];
            previewController.navigationItem.title = [NSString stringWithFormat:L(@"FILES_PREVIEW_TITLE"), fileName];
            [self.navigationController pushViewController:previewController animated:YES];
            break;
        }
        case kCZFileFormatMP4: {
            NSURL *url = [NSURL fileURLWithPath:filePath];
            CZMoviePlayerViewController *player = [[CZMoviePlayerViewController alloc] initWithContentURL:url];
            [self.navigationController pushViewController:player animated:YES];
            break;
        }
        case kCZFileFormatCZFTP: {
            CZFileNameTemplateConfigurationViewController *viewController = [[CZFileNameTemplateConfigurationViewController alloc] initWithPath:filePath];
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Action

- (void)sortAction:(CZButton *)sender {
    if (self.sortingOrderPopoverController == nil) {
        @weakify(self);
        CZFileSortingOrderController *fileSortingOrderController = [[CZFileSortingOrderController alloc] initWithTitle:L(@"SORT_BY")
                                                                                                         sortingOrders:@[@(CZSortByNameAscend),
                                                                                                                         @(CZSortByNameDescend),
                                                                                                                         @(CZSortByTimeAscend),
                                                                                                                         @(CZSortByTimeDescend)]];
        fileSortingOrderController.selectedSortingOrder = self.viewModel.sortOrder;
        [fileSortingOrderController didSelectSortOrder:^(NSUInteger sortOrder) {
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.viewModel.sortOrder = sortOrder;
                [self.viewModel synchronizeSort];
                [self.sortingOrderPopoverController dismissViewControllerAnimated:YES completion:nil];
            });
        }];
        self.sortingOrderPopoverController = [[CZPopoverController alloc] initWithContentViewController:fileSortingOrderController];
        self.sortingOrderPopoverController.arrowDirection = CZPopoverArrowDirectionUp;
        self.sortingOrderPopoverController.sourceRect = CGRectOffset(sender.bounds, 0, 10.0f);
        self.sortingOrderPopoverController.sourceView = sender;
        self.sortingOrderPopoverController.borderWidth = 1.0f;
        self.sortingOrderPopoverController.borderColor = [UIColor cz_gs120];
        self.sortingOrderPopoverController.backgroundColor = [UIColor cz_gs105];
        self.sortingOrderPopoverController.delegate = self;
    }
    [self.sortingOrderPopoverController presentAnimated:YES completion:nil];
}

- (void)searchAction:(CZButton *)sender {
    sender.selected = !sender.selected;

    @weakify(self);
    if (sender.selected) {
        self.filterSearchView.filterSearchTextField.text = self.viewModel.keyword;
        [self.filterSearchView showWithCompletion:^() {
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.filterSearchViewHeightConstant.constant = kDefaultSearchFilterViewHeight;
            });
        }];
    } else {
        [self.filterSearchView dismissWithCompletion:^() {
            @strongify(self);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.filterSearchViewHeightConstant.constant = 0;
                self.viewModel.keyword = nil;
            });
        }];
    }
    [self.view updateConstraintsIfNeeded];
}

- (void)splitAction:(CZButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(localFilesController:didEnterFullScreen:)]) {
        [self.delegate localFilesController:self didEnterFullScreen:YES];
    }
}

#pragma mark - Key-Value Observing
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self.viewModel) {
        if ([keyPath isEqualToString:@"shouldSelectedAllFileEntities"]) {
            BOOL newValue = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
            BOOL oldValue = [[change objectForKey:NSKeyValueChangeOldKey] boolValue];
            if (newValue != oldValue) {
                [self updateMultiSelectionItemState];
            }
        }
    }
}

#pragma mark - Getters

- (CZFileListToolBar *)fileListToolBar {
    if (_fileListToolBar == nil) {
        _fileListToolBar = [[CZFileListToolBar alloc] init];
        _fileListToolBar.delegate = self;
    }
    return _fileListToolBar;
}

- (CZFilterSearchView *)filterSearchView {
    if (_filterSearchView == nil) {
        _filterSearchView = [[CZFilterSearchView alloc] initWithFilters:CZFileFilterTypeVideo |
                                                                        CZFileFilterTypeTemplate |
                                                                        CZFileFilterTypeReport |
                                                                        CZFileFilterTypeImage];
        _filterSearchView.delegate = self;
        _filterSearchView.hidden = YES;
        _filterSearchView.selectedFilterOptions = self.viewModel.filterType;
    }
    return _filterSearchView;
}

- (CZLocalGridViewController *)fileGridController {
    if (_fileGridController == nil) {
        _fileGridController = [[CZLocalGridViewController alloc] initWithViewModel:_viewModel.localList];
        _fileGridController.delegate = self;
    }
    return _fileGridController;
}

- (CZLocalFileListController *)fileListController {
    if (_fileListController == nil) {
        _fileListController = [[CZLocalFileListController alloc] initWithViewModel:_viewModel.localList];
        _fileListController.delegate = self;
    }
    return _fileListController;
}

@end
