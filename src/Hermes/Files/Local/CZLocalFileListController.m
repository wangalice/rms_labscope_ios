//
//  CZLocalFileListController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLocalFileListController.h"
#import "CZFilesListViewModel.h"
#import "CZFileListCell.h"
#import "CZLocalFileOpenProtocol.h"
#import "CZFileEntity+Icon.h"

static NSString *kDefaultCellIdentifier = @"kDefaultCellIdentifier";

@interface CZLocalFileListController ()<
CZFileListCellDelegate,
CZFilesListViewControllerDelegate
>

@property (nonatomic, strong) CZFilesListViewModel *viewModel;

@end

@implementation CZLocalFileListController

- (instancetype)initWithViewModel:(CZFilesListViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[CZFileListCell class] forCellReuseIdentifier:kDefaultCellIdentifier];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.indicatorStyle =  UIScrollViewIndicatorStyleWhite;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.viewModel.viewControllerDelegate = self;
    [self.tableView reloadData];
    [self.tableView scrollsToTop];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.viewModel.viewControllerDelegate = nil;
}

#pragma mark - Public method

- (void)scrollSelectedRowToVisible {
    //Scroll the selection to visible zone
    for (CZFileEntity *fileEntity in self.viewModel.files) {
        if ([self.viewModel isFileEntitySelected:fileEntity] == YES) {
            NSUInteger index = [self.viewModel.files indexOfObject:fileEntity];
            if (index != NSNotFound) {
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                      atScrollPosition:UITableViewScrollPositionMiddle
                                              animated:NO];
            }
            break;
        }
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileListCell *cell = [tableView dequeueReusableCellWithIdentifier:kDefaultCellIdentifier];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    if (self.viewModel.files.count > indexPath.row) {
        CZFileEntity *fileEntity = self.viewModel.files[indexPath.row];
        [cell setCellWithFileEntity:fileEntity
                           selected:[self.viewModel isFileEntitySelected:fileEntity]
                          thumbnail:[self.viewModel thumbnailFrom:fileEntity]];
        NSString *fileInfoText = [self.viewModel newFileInfoLabel:fileEntity.fileSize
                                                     modifiedDate:fileEntity.modificationDate];
        cell.fileInfoLabel.text = fileInfoText;
    } else {
        cell.fileLabel.text = @"";
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.files.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileEntity *fileEntity = self.viewModel.files[indexPath.row];
    NSString *filePath = fileEntity.filePath;
    if ([self.delegate respondsToSelector:@selector(controller:didRequestOpeningFileAtPath:)]){
        [self.delegate controller:self didRequestOpeningFileAtPath:filePath];
    }
}

#pragma mark - CZFileListCellDelegate

- (void)tableViewCellDidSelectButton:(CZFileListCell *)tableViewCell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:tableViewCell];
    if (indexPath.row < self.viewModel.files.count) {
        [self.viewModel selectFileEntity:self.viewModel.files[indexPath.row] isSelected:tableViewCell.selectButton.selected];
    }
}

- (void)tableViewCellDidClickAccessoryButton:(CZFileListCell *)tableViewCell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:tableViewCell];
    if (indexPath.row < self.viewModel.files.count) {
        [self.viewModel uploadFileEntity:self.viewModel.files[indexPath.row]];
    }
}

#pragma mark - CZFilesListViewControllerDelegate

- (void)fileListDidUpdate {
    if (self.isActive == YES) {
        [self.tableView reloadData];
    }
}

- (void)fileListViewModel:(CZFilesListViewModel *)viewModel didUpdateThumbnail:(UIImage *)thumbnail filPath:(NSString *)filePath {
    if (filePath) {
        for (CZFileListCell *cell in self.tableView.visibleCells) {
            if ([filePath isEqualToString:cell.fileEntity.filePath]) {
                cell.thumbnailView.image = thumbnail;  // CAUTION: do not use reloadRowsAtIndexPaths, which is bad performance.
                break;
            }
        }
    }
}

@end
