//
//  CZLocalGridViewController.m
//  Hermes
//
//  Created by Sun, Shaoge on 2019/5/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZLocalGridViewController.h"
#import "CZFilesListViewModel.h"
#import "CZFileGridCell.h"
#import "CZLocalFileOpenProtocol.h"

static NSString * const kDefaultLocalReuseIdentifier = @"kDefaultLocalReuseIdentifier";
static NSInteger  const kDefaultFileItemsInLine = 4;
static CGFloat    const kDefaultHorizontalMargin = 16.0f;
static CGFloat    const kDefaultVerticalMargin = 16.0f;
static CGFloat    const kDefaultCollectionViewCellSpacing = 16.0f;
static CGFloat    const kDefaultCollecitonViewLineSpacing = 16.0f;
static CGFloat    const kDefualtCollectionViewCellThumbnailAspectRatio = 16.0 / 9.0;
static CGFloat    const kDefaultFileInfoHeight = 56.0f;

@interface CZLocalGridViewController () <
CZFileGridCellDelegate,
CZFilesListViewControllerDelegate
>

@property (nonatomic, strong) CZFilesListViewModel *viewModel;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@end

@implementation CZLocalGridViewController

- (instancetype)initWithViewModel:(CZFilesListViewModel *)viewModel {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = kDefaultCollectionViewCellSpacing;
    flowLayout.minimumLineSpacing = kDefaultCollecitonViewLineSpacing;
    CZLocalGridViewController *localGridViewController = [[CZLocalGridViewController alloc] initWithCollectionViewLayout:flowLayout];
    localGridViewController.viewModel = viewModel;
    return localGridViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.collectionView.backgroundColor = [UIColor cz_gs105];
    self.collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    [self.collectionView registerClass:[CZFileGridCell class] forCellWithReuseIdentifier:kDefaultLocalReuseIdentifier];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.viewModel.viewControllerDelegate = self;
    [self.collectionView reloadData];
    [self.collectionView scrollsToTop];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.viewModel.viewControllerDelegate = nil;
}

#pragma mark - Public method

- (void)scrollSelectedRowToVisible {
    //scroll the first selection cell to the visible zone
    for (CZFileEntity *fileEntity in self.viewModel.files) {
        if ([self.viewModel isFileEntitySelected:fileEntity] == YES) {
            NSUInteger index = [self.viewModel.files indexOfObject:fileEntity];
            if (index != NSNotFound) {
                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                                            atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                                    animated:NO];
            }
            break;
        }
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.files.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZFileGridCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDefaultLocalReuseIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    if (indexPath.row < self.viewModel.files.count) {
        CZFileEntity *fileEntity = self.viewModel.files[indexPath.row];
        [cell setCellWithFileEntity:fileEntity
                           selected:[self.viewModel isFileEntitySelected:fileEntity]
                          thumbnail:[self.viewModel thumbnailFrom:fileEntity]];
        NSString *fileInfoText = [self.viewModel newFileInfoLabel:fileEntity.fileSize
                                                     modifiedDate:fileEntity.modificationDate];
        cell.fileInfoLabel.text = fileInfoText;
    } else {
        cell.fileInfoLabel.text = @"";
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CZFileEntity *fileEntity = self.viewModel.files[indexPath.row];
    NSString *filePath = fileEntity.filePath;
    if ([self.delegate respondsToSelector:@selector(controller:didRequestOpeningFileAtPath:)]){
        [self.delegate controller:self didRequestOpeningFileAtPath:filePath];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (self.collectionView.bounds.size.width - kDefaultHorizontalMargin * 2 - kDefaultCollectionViewCellSpacing * (kDefaultFileItemsInLine - 1)) / kDefaultFileItemsInLine;
    CGFloat height = width / kDefualtCollectionViewCellThumbnailAspectRatio + kDefaultFileInfoHeight;
    return CGSizeMake(floorf(width), floorf(height));
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(kDefaultVerticalMargin, kDefaultHorizontalMargin, kDefaultVerticalMargin, kDefaultHorizontalMargin);
}

#pragma mark - CZFileGridCellDelegate

- (void)collectionCellDidSelectButton:(CZFileGridCell *)collectionCell {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:collectionCell];
    if (indexPath.row < self.viewModel.files.count) {
        [self.viewModel selectFileEntity:self.viewModel.files[indexPath.row] isSelected:collectionCell.selectButton.selected];
    }
}

#pragma mark - CZFilesListViewControllerDelegate

- (void)fileListDidUpdate {
    if (self.isActive) {
        [self.collectionView reloadData];
    }
}

- (void)fileListViewModel:(CZFilesListViewModel *)viewModel didUpdateThumbnail:(UIImage *)thumbnail filPath:(NSString *)filePath {
    if (filePath) {
        for (CZFileGridCell *cell in self.collectionView.visibleCells) {
            if ([filePath isEqualToString:cell.fileEntity.filePath]) {
                cell.thumbnailView.image = thumbnail;  // CAUTION: do not use reloadRowsAtIndexPaths, which is bad performance.
                break;
            }
        }
    }
}

@end
