//
//  CZMicroscopeAutoScaleController.h
//  Matscope
//
//  Created by Mike Wang on 10/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@class CZMicroscopeAutoScaleController;
@class CZCameraView;
@class CZMicroscopeModel;

@protocol CZMicroscopeAutoScaleControllerDelegate <NSObject>

- (void)microscopeAutoScaleControllerDidFinishScaling:(CZMicroscopeAutoScaleController *)controller;

@end

@interface CZMicroscopeAutoScaleController : UIViewController

@property (nonatomic, assign) id<CZMicroscopeAutoScaleControllerDelegate> delegate;

- (id)initWithCameraView:(CZCameraView *)cameraView
         microscopeModel:(CZMicroscopeModel *)model
                position:(NSUInteger)position;

@end
