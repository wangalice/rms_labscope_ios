//
//  CZShadingCorrectionSetupViewController.m
//  Matscope
//
//  Created by Mike Wang on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZShadingCorrectionSetupViewController.h"

#import <GPUImage/GPUImage.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "MBProgressHUD+Hide.h"

#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZImageProcessing/CZImageProcessing.h>
#import "CZBulletedListView.h"
#import "CZGridView.h"
#import "CZCameraViewController.h"
#import "CZLiveViewController.h"
#import "CZAlertController.h"

@interface CZShadingCorrectionSetupInfo : NSObject

@property (nonatomic, assign) NSUInteger position;
@property (nonatomic, strong) UIImage *maskImage;
@property (nonatomic, strong) NSArray *maxMaskData; // array of NSNumber, (maxMaskX, maxMaskY, maxMaskZ...)

@end

@implementation CZShadingCorrectionSetupInfo

@end

@interface CZShadingCorrectionResetInfo : NSObject

@property (nonatomic, assign) NSUInteger position;

@end

@implementation CZShadingCorrectionResetInfo

@end

@interface CZShadingCorrectionSetupViewController () <CZCameraViewControllerDelegate, CZCameraShadingCorrectionDelegate>

@property (nonatomic, readonly, copy) NSDictionary<NSNumber *, CZObjective *> *objectives;
@property (nonatomic, readonly, copy) NSDictionary<NSNumber *, CZZoomLevel *> *zoomLevels;
@property (nonatomic, assign) NSUInteger selectedPosition;

@property (nonatomic, readonly, strong) CZBarButtonItem *doneButtonItem;
@property (nonatomic, readonly, strong) UILabel *messageLabel;
@property (nonatomic, readonly, strong) CZBulletedListView *stepsView;
@property (nonatomic, readonly, strong) CZGridView *gridView;
@property (nonatomic, readonly, strong) UILabel *objectiveTitleLabel;
@property (nonatomic, readonly, strong) UILabel *objectiveLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *objectiveDropdown;
@property (nonatomic, readonly, strong) UILabel *zoomTitleLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *zoomDropdown;
@property (nonatomic, readonly, strong) UILabel *reflectorTitleLabel;
@property (nonatomic, readonly, strong) UILabel *reflectorLabel;
@property (nonatomic, readonly, strong) UILabel *lightPathTitleLabel;
@property (nonatomic, readonly, strong) UILabel *lightPathLabel;
@property (nonatomic, readonly, strong) UIImageView *appliedIcon;
@property (nonatomic, readonly, strong) UILabel *appliedLabel;
@property (nonatomic, readonly, strong) CZButton *setupButton;
@property (nonatomic, readonly, strong) CZButton *resetButton;
@property (nonatomic, readonly, strong) MBProgressHUD *loadingHUD;

@property (nonatomic, readonly, strong) NSOperationQueue *queue;
@property (nonatomic, readonly, strong) NSMutableArray<CZShadingCorrectionSetupInfo *> *setupInfos;
@property (nonatomic, readonly, strong) NSMutableArray<CZShadingCorrectionResetInfo *> *resetInfos;

@property (nonatomic, copy) void (^cameraViewControllerCompletionHandler)(BOOL success);

@end

@implementation CZShadingCorrectionSetupViewController

@synthesize objectives = _objectives;
@synthesize zoomLevels = _zoomLevels;

@synthesize doneButtonItem = _doneButtonItem;
@synthesize messageLabel = _messageLabel;
@synthesize stepsView = _stepsView;
@synthesize gridView = _gridView;
@synthesize objectiveTitleLabel = _objectiveTitleLabel;
@synthesize objectiveLabel = _objectiveLabel;
@synthesize objectiveDropdown = _objectiveDropdown;
@synthesize zoomTitleLabel = _zoomTitleLabel;
@synthesize zoomDropdown = _zoomDropdown;
@synthesize reflectorTitleLabel = _reflectorTitleLabel;
@synthesize reflectorLabel = _reflectorLabel;
@synthesize lightPathTitleLabel = _lightPathTitleLabel;
@synthesize lightPathLabel = _lightPathLabel;
@synthesize appliedIcon = _appliedIcon;
@synthesize appliedLabel = _appliedLabel;
@synthesize setupButton = _setupButton;
@synthesize resetButton = _resetButton;
@synthesize loadingHUD = _loadingHUD;

- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _camera = camera;
        _camera.shadingCorrectionDelegate = self;
        _microscopeModel = microscopeModel;
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 1;
        _setupInfos = [NSMutableArray array];
        _resetInfos = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(geminiCameraStatusDidChange:) name:CZGeminiCameraStatusDidChangeNotification object:self.camera];
    }
    return self;
}

- (void)dealloc {
    _camera.shadingCorrectionDelegate = nil;
    [_queue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZGeminiCameraStatusDidChangeNotification object:self.camera];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
    self.title = L(@"MIC_SHADING_CORRECTION_SETUP_TITLE");
    self.navigationItem.rightBarButtonItem = self.doneButtonItem;
    self.view.backgroundColor = [UIColor cz_gs105];
    
    [self.view addSubview:self.messageLabel];
    [self.view addSubview:self.stepsView];
    [self.view addSubview:self.setupButton];
    [self.view addSubview:self.resetButton];
    [self.view addSubview:self.gridView];
    [self.view addSubview:self.appliedIcon];
    [self.view addSubview:self.appliedLabel];
    [self.view addSubview:self.setupButton];
    [self.view addSubview:self.resetButton];
    
    [self.messageLabel.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:128.0].active = YES;
    [self.messageLabel.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-128.0].active = YES;
    [self.messageLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:32.0].active = YES;
    
    [self.stepsView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:128.0].active = YES;
    [self.stepsView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-128.0].active = YES;
    [self.stepsView.topAnchor constraintEqualToAnchor:self.messageLabel.bottomAnchor constant:22.0].active = YES;
    
    [self.gridView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:128.0].active = YES;
    [self.gridView.topAnchor constraintEqualToAnchor:self.stepsView.bottomAnchor constant:64.0].active = YES;
    
    [self.appliedIcon.leadingAnchor constraintEqualToAnchor:self.gridView.trailingAnchor constant:9.0].active = YES;
    [self.appliedIcon.centerYAnchor constraintEqualToAnchor:self.gridView.topAnchor constant:16.0].active = YES;
    
    [self.appliedLabel.leadingAnchor constraintEqualToAnchor:self.appliedIcon.trailingAnchor constant:9.0].active = YES;
    [self.appliedLabel.centerYAnchor constraintEqualToAnchor:self.appliedIcon.centerYAnchor].active = YES;
    
    [self.setupButton.trailingAnchor constraintEqualToAnchor:self.resetButton.leadingAnchor constant:-16.0].active = YES;
    [self.setupButton.topAnchor constraintEqualToAnchor:self.stepsView.bottomAnchor constant:144.0].active = YES;
    [self.setupButton.widthAnchor constraintEqualToConstant:160.0].active = YES;
    [self.setupButton.heightAnchor constraintEqualToConstant:48.0].active = YES;
    
    [self.resetButton.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-128.0].active = YES;
    [self.resetButton.topAnchor constraintEqualToAnchor:self.stepsView.bottomAnchor constant:144.0].active = YES;
    [self.resetButton.widthAnchor constraintEqualToConstant:160.0].active = YES;
    [self.resetButton.heightAnchor constraintEqualToConstant:48.0].active = YES;
    
    if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
        self.doneButtonItem.enabled = YES;
        
        [self.gridView addContentView:self.objectiveTitleLabel];
        [self.gridView addContentView:self.objectiveLabel];
        
        CZObjective *currentObjective = [self.microscopeModel.nosepiece objectiveAtCurrentPosition];
        self.objectiveLabel.text = currentObjective.displayName;
        
        if ([self.microscopeModel hasReflector]) {
            [self.gridView addContentView:self.reflectorTitleLabel];
            [self.gridView addContentView:self.reflectorLabel];
            
            CZFilterSet *currentFilterSet = [self.microscopeModel.reflector filterSetAtCurrentPosition];
            self.reflectorLabel.text = currentFilterSet.displayName;
            
            self.setupButton.enabled = currentObjective != nil && !currentObjective.isNone && currentFilterSet != nil;
        } else {
            self.setupButton.enabled = currentObjective != nil && !currentObjective.isNone;
        }
        
        [self.gridView addContentView:self.lightPathTitleLabel];
        [self.gridView addContentView:self.lightPathLabel];
        
        CZCameraLightPath lightPath = [self.camera lightPath];
        switch (lightPath) {
            case CZCameraLightPathUnknown:
                self.lightPathLabel.text = L(@"NONE");
                break;
            case CZCameraLightPathReflected:
                self.lightPathLabel.text = L(@"LIVE_CAMERA_LIGHT_RL");
                break;
            case CZCameraLightPathTransmitted:
                self.lightPathLabel.text = L(@"LIVE_CAMERA_LIGHT_TL");
                break;
        }
    } else if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        self.selectedPosition = self.microscopeModel.currentZoomClickStopPosition;
        [self.gridView addContentView:self.zoomTitleLabel];
        [self.gridView addContentView:self.zoomDropdown];
    } else {
        self.selectedPosition = self.microscopeModel.nosepiece.currentPosition;
        [self.gridView addContentView:self.objectiveTitleLabel];
        [self.gridView addContentView:self.objectiveDropdown];
    }
    
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)backBarButtonItemAction:(id)sender {
    if (self.setupInfos.count == 0 && self.resetInfos.count == 0) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_ASK_ON_SAVE_MSG"), self.microscopeModel.name];
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:message level:CZAlertLevelWarning];
    @weakify(self);
    [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDestructive handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self doneButtonItemAction:nil];
    }];
    [alert presentAnimated:YES completion:nil];
}

- (void)updateShadingCorrectionIsAppliedStatus {
    if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
        @weakify(self);
        [self.queue cancelAllOperations];
        [self.queue addOperationWithBlock:^{
            @strongify(self);
            BOOL applied = [self.camera isShadingCorrectionApplied];
            dispatch_async(dispatch_get_main_queue(), ^{
                @strongify(self);
                self.appliedIcon.hidden = !applied;
                self.appliedLabel.hidden = !applied;
                self.resetButton.enabled = applied;
            });
        }];
    } else {
        BOOL applied = YES;
        
        for (CZShadingCorrectionResetInfo *resetInfo in self.resetInfos) {
            if (resetInfo.position == self.selectedPosition) {
                applied = NO;
                break;
            }
        }
        
        if (applied == YES) {
            applied = NO;
            if ([self.microscopeModel hasShadingCorrectionDataAtPosition:self.selectedPosition]) {
                applied = YES;
            } else {
                for (CZShadingCorrectionSetupInfo *setupInfo in self.setupInfos) {
                    if (setupInfo.position == self.selectedPosition) {
                        applied = YES;
                        break;
                    }
                }
            }
        }
        
        self.appliedIcon.hidden = !applied;
        self.appliedLabel.hidden = !applied;
        self.resetButton.enabled = applied;
    }
}

#pragma mark - Models

- (NSDictionary<NSNumber *, CZObjective *> *)objectives {
    if (_objectives == nil) {
        NSMutableDictionary<NSNumber *, CZObjective *> *objectives = [NSMutableDictionary dictionary];
        for (NSUInteger position = 0; position < self.microscopeModel.nosepiece.positions; position++) {
            CZObjective *objective = [self.microscopeModel.nosepiece objectiveAtPosition:position];
            if (objective.matID.length > 0) {
                objectives[@(position)] = objective;
            }
        }
        _objectives = [objectives copy];
    }
    return _objectives;
}

- (NSDictionary<NSNumber *, CZZoomLevel *> *)zoomLevels {
    if (_zoomLevels == nil) {
        NSMutableDictionary<NSNumber *, CZZoomLevel *> *zoomLevels = [NSMutableDictionary dictionary];
        for (NSUInteger position = 0; position < self.microscopeModel.zoomClickStopPositions; position++) {
            CZZoomLevel *zoomLevel = [self.microscopeModel zoomClickStopAtPosition:position];
            zoomLevels[@(position)] = zoomLevel;
        }
        _zoomLevels = [zoomLevels copy];
    }
    return _zoomLevels;
}

#pragma mark - Views

- (CZBarButtonItem *)doneButtonItem {
    if (_doneButtonItem == nil) {
        _doneButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"DONE") target:self action:@selector(doneButtonItemAction:)];
        _doneButtonItem.enabled = NO;
    }
    return _doneButtonItem;
}

- (UILabel *)messageLabel {
    if (_messageLabel == nil) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.font = [UIFont cz_body1];
        _messageLabel.text = L(@"MIC_SHADING_CORRECTION_MESSAGE");
        _messageLabel.textColor = [UIColor cz_gs50];
        _messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _messageLabel.numberOfLines = 0;
    }
    return _messageLabel;
}

- (CZBulletedListView *)stepsView {
    if (_stepsView == nil) {
        _stepsView = [[CZBulletedListView alloc] init];
        _stepsView.translatesAutoresizingMaskIntoConstraints = NO;
        if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
            _stepsView.contents = @[L(@"MIC_SHADING_CORRECTION_STEP_1_STEREO"),
                                    L(@"MIC_SHADING_CORRECTION_STEP_2"),
                                    L(@"MIC_SHADING_CORRECTION_STEP_3"),
                                    L(@"MIC_SHADING_CORRECTION_STEP_4")];
        } else {
            _stepsView.contents = @[L(@"MIC_SHADING_CORRECTION_STEP_2"),
                                    L(@"MIC_SHADING_CORRECTION_STEP_3"),
                                    L(@"MIC_SHADING_CORRECTION_STEP_4")];
        }
    }
    return _stepsView;
}

- (CZGridView *)gridView {
    if (_gridView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:96.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:160.0 alignment:CZGridColumnAlignmentFill];
        
        _gridView = [[CZGridView alloc] initWithRows:@[row0, row1, row2] columns:@[column0, column1]];
        _gridView.translatesAutoresizingMaskIntoConstraints = NO;
        _gridView.rowSpacing = 16.0;
        _gridView.columnSpacing = 16.0;
    }
    return _gridView;
}

- (UILabel *)objectiveTitleLabel {
    if (_objectiveTitleLabel == nil) {
        _objectiveTitleLabel = [[UILabel alloc] init];
        _objectiveTitleLabel.font = [UIFont cz_label1];
        _objectiveTitleLabel.text = L(@"OBJECTIVE");
        _objectiveTitleLabel.textColor = [UIColor cz_gs80];
    }
    return _objectiveTitleLabel;
}

- (UILabel *)objectiveLabel {
    if (_objectiveLabel == nil) {
        _objectiveLabel = [[UILabel alloc] init];
        _objectiveLabel.font = [UIFont cz_body1];
        _objectiveLabel.textColor = [UIColor cz_gs50];
    }
    return _objectiveLabel;
}

- (CZDropdownMenu *)objectiveDropdown {
    if (_objectiveDropdown == nil) {
        NSString *title = nil;
        NSMutableArray<NSString *> *titles = [NSMutableArray array];
        NSArray<NSNumber *> *sortedPositions = [self.objectives.allKeys sortedArrayUsingSelector:@selector(compare:)];
        for (NSNumber *position in sortedPositions) {
            CZObjective *objective = self.objectives[position];
            [titles addObject:objective.displayName];
        }
        if (![sortedPositions containsObject:@(self.selectedPosition)]) {
            self.selectedPosition = sortedPositions.firstObject.unsignedIntegerValue;
        }
        title = self.objectives[@(self.selectedPosition)].displayName;
        _objectiveDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:title imagePicker:nil expandTitles:titles];
        _objectiveDropdown.enabled = titles.count > 0;
        [_objectiveDropdown addTarget:self action:@selector(objectiveDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _objectiveDropdown;
}

- (UILabel *)zoomTitleLabel {
    if (_zoomTitleLabel == nil) {
        _zoomTitleLabel = [[UILabel alloc] init];
        _zoomTitleLabel.font = [UIFont cz_label1];
        _zoomTitleLabel.text = L(@"MIC_CONFIG_ZOOM_CLICK_STOPS");
        _zoomTitleLabel.textColor = [UIColor cz_gs80];
    }
    return _zoomTitleLabel;
}

- (CZDropdownMenu *)zoomDropdown {
    if (_zoomDropdown == nil) {
        NSString *title = nil;
        NSMutableArray<NSString *> *titles = [NSMutableArray array];
        NSArray<NSNumber *> *sortedPositions = [self.zoomLevels.allKeys sortedArrayUsingSelector:@selector(compare:)];
        for (NSNumber *position in sortedPositions) {
            CZZoomLevel *zoomLevel = self.zoomLevels[position];
            [titles addObject:zoomLevel.displayMagnification];
        }
        if (![sortedPositions containsObject:@(self.selectedPosition)]) {
            self.selectedPosition = sortedPositions.firstObject.unsignedIntegerValue;
        }
        title = self.zoomLevels[@(self.selectedPosition)].displayName;
        _zoomDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:title imagePicker:nil expandTitles:titles];
        _zoomDropdown.enabled = titles.count > 0;
        [_zoomDropdown addTarget:self action:@selector(zoomDropdownAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _zoomDropdown;
}

- (UILabel *)reflectorTitleLabel {
    if (_reflectorTitleLabel == nil) {
        _reflectorTitleLabel = [[UILabel alloc] init];
        _reflectorTitleLabel.font = [UIFont cz_label1];
        _reflectorTitleLabel.text = L(@"REFLECTOR");
        _reflectorTitleLabel.textColor = [UIColor cz_gs80];
    }
    return _reflectorTitleLabel;
}

- (UILabel *)reflectorLabel {
    if (_reflectorLabel == nil) {
        _reflectorLabel = [[UILabel alloc] init];
        _reflectorLabel.font = [UIFont cz_body1];
        _reflectorLabel.textColor = [UIColor cz_gs50];
    }
    return _reflectorLabel;
}

- (UILabel *)lightPathTitleLabel {
    if (_lightPathTitleLabel == nil) {
        _lightPathTitleLabel = [[UILabel alloc] init];
        _lightPathTitleLabel.font = [UIFont cz_label1];
        _lightPathTitleLabel.text = L(@"LIVE_CAMERA_LIGHTPATH");
        _lightPathTitleLabel.textColor = [UIColor cz_gs80];
    }
    return _lightPathTitleLabel;
}

- (UILabel *)lightPathLabel {
    if (_lightPathLabel == nil) {
        _lightPathLabel = [[UILabel alloc] init];
        _lightPathLabel.font = [UIFont cz_body1];
        _lightPathLabel.textColor = [UIColor cz_gs50];
    }
    return _lightPathLabel;
}

- (UIImageView *)appliedIcon {
    if (_appliedIcon == nil) {
        _appliedIcon = [[UIImageView alloc] init];
        _appliedIcon.translatesAutoresizingMaskIntoConstraints = NO;
        _appliedIcon.hidden = YES;
        
        CZIconRenderingOptions *options = [CZIconRenderingOptions alwaysOriginalRenderingOptions];
        options.size = CZIconSizeSmall;
        CZIcon *icon = [CZIcon iconNamed:@"alert-level-info"];
        _appliedIcon.image = [icon imageWithOptions:options];
    }
    return _appliedIcon;
}

- (UILabel *)appliedLabel {
    if (_appliedLabel == nil) {
        _appliedLabel = [[UILabel alloc] init];
        _appliedLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _appliedLabel.hidden = YES;
        _appliedLabel.font = [UIFont cz_label1];
        _appliedLabel.text = L(@"MIC_SHADING_CORRECTION_SETTINGS_EXIST");
        _appliedLabel.textColor = [UIColor cz_gs85];
    }
    return _appliedLabel;
}

- (CZButton *)setupButton {
    if (_setupButton == nil) {
        _setupButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        _setupButton.translatesAutoresizingMaskIntoConstraints = NO;
        if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
            _setupButton.enabled = YES;
        } else if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
            _setupButton.enabled = self.zoomLevels.count > 0;
        } else {
            _setupButton.enabled = self.objectives.count > 0;
        }
        [_setupButton setTitle:L(@"SETUP") forState:UIControlStateNormal];
        [_setupButton addTarget:self action:@selector(setupButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _setupButton;
}

- (CZButton *)resetButton {
    if (_resetButton == nil) {
        _resetButton = [[CZButton alloc] initWithLevel:CZControlEmphasisDefault];
        _resetButton.translatesAutoresizingMaskIntoConstraints = NO;
        _resetButton.enabled = NO;
        [_resetButton setTitle:L(@"RESET") forState:UIControlStateNormal];
        [_resetButton addTarget:self action:@selector(resetButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _resetButton;
}

- (MBProgressHUD *)loadingHUD {
    if (_loadingHUD == nil) {
        _loadingHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [_loadingHUD setColor:kSpinnerBackgroundColor];
        [_loadingHUD setLabelText:L(@"BUSY_INDICATOR_PLEASE_WAIT")];
    }
    return _loadingHUD;
}

#pragma mark - Actions

- (void)doneButtonItemAction:(id)sender {
    if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
        
    } else {
        for (CZShadingCorrectionSetupInfo *setupInfo in self.setupInfos) {
            [self.microscopeModel setShadingCorrectionMask:setupInfo.maskImage atPosition:setupInfo.position imageResolution:setupInfo.maskImage.size];
            
            if (setupInfo.maxMaskData.count >= 3) {
                CZShadingCorrectionData *data = [[CZShadingCorrectionData alloc] initWithMaxMaskX:[setupInfo.maxMaskData[0] floatValue]
                                                                                         maxMaskY:[setupInfo.maxMaskData[1] floatValue]
                                                                                         maxMaskZ:[setupInfo.maxMaskData[2] floatValue]];
                [self.microscopeModel setShadingCorrectionData:data atPosition:setupInfo.position imageResolution:setupInfo.maskImage.size];
            }
        }
        for (CZShadingCorrectionResetInfo *resetInfo in self.resetInfos) {
            [self.microscopeModel removeShadingCorrectionDataAndMaskAtPosition:resetInfo.position];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)objectiveDropdownAction:(id)sender {
    NSArray<NSNumber *> *sortedPositions = [self.objectives.allKeys sortedArrayUsingSelector:@selector(compare:)];
    self.selectedPosition = sortedPositions[self.objectiveDropdown.selectedIndex].integerValue;
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)zoomDropdownAction:(id)sender {
    NSArray<NSNumber *> *sortedPositions = [self.zoomLevels.allKeys sortedArrayUsingSelector:@selector(compare:)];
    self.selectedPosition = sortedPositions[self.zoomDropdown.selectedIndex].integerValue;
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)setupButtonAction:(id)sender {
    if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
        CZCameraViewController *cameraViewController = [[CZCameraViewController alloc] initWithCamera:self.camera action:CZCameraViewControllerActionCustom];
        cameraViewController.delegate = self;
        cameraViewController.autoCameraModeEnabled = YES;
        [self.navigationController pushViewController:cameraViewController animated:YES];
    } else {
        CZCameraViewController *cameraViewController = [[CZCameraViewController alloc] initWithCamera:self.camera action:CZCameraViewControllerActionSnapAllResolutions];
        cameraViewController.delegate = self;
        cameraViewController.autoCameraModeEnabled = YES;
        [self.navigationController pushViewController:cameraViewController animated:YES];
    }
}

- (void)resetButtonAction:(id)sender {
    if ([self.camera hasCapability:CZCameraCapabilityShadingCorrection]) {
        [self.loadingHUD show:YES];
        [self.camera disableShadingCorrection];
    } else {
        self.doneButtonItem.enabled = YES;
        
        for (CZShadingCorrectionSetupInfo *setupInfo in self.setupInfos) {
            if (setupInfo.position == self.selectedPosition) {
                [self.setupInfos removeObject:setupInfo];
                break;
            }
        }
        
        CZShadingCorrectionResetInfo *resetInfo = [[CZShadingCorrectionResetInfo alloc] init];
        resetInfo.position = self.selectedPosition;
        [self.resetInfos addObject:resetInfo];
        
        [self updateShadingCorrectionIsAppliedStatus];
    }
}

#pragma mark - Notifications

- (void)geminiCameraStatusDidChange:(NSNotification *)note {
    if (![self.camera hasCapability:CZCameraCapabilityEncoding]) {
        return;
    }
    
    CZGeminiCameraStatus *status = note.userInfo[CZGeminiCameraStatusKey];
    CZGeminiCameraStatusChangeMask changes = [note.userInfo[CZGeminiCameraStatusChangesKey] unsignedIntegerValue];
    
    if (((changes & CZGeminiCameraStatusChangeMaskObjectivePosition) != 0) ||
        ((changes & CZGeminiCameraStatusChangeMaskReflectorPosition) != 0) ||
        ((changes & CZGeminiCameraStatusChangeMaskRLTLPosition) != 0)) {
        CZObjective *currentObjective = [self.microscopeModel.nosepiece objectiveAtPosition:status.objectivePosition];
        self.objectiveLabel.text = currentObjective.displayName;
        
        if ([self.microscopeModel hasReflector]) {
            CZFilterSet *currentFilterSet = [self.microscopeModel.reflector filterSetAtPosition:status.reflectorPosition];
            self.reflectorLabel.text = currentFilterSet.displayName;
            
            self.setupButton.enabled = currentObjective != nil && !currentObjective.isNone && currentFilterSet != nil;
        } else {
            self.setupButton.enabled = currentObjective != nil && !currentObjective.isNone;
        }
        
        if ((changes & CZGeminiCameraStatusChangeMaskRLTLPosition) != 0) {
            CZCameraLightPath lightPath = status.rltlPosition;
            switch (lightPath) {
                case CZCameraLightPathUnknown:
                    self.lightPathLabel.text = L(@"NONE");
                    break;
                case CZCameraLightPathReflected:
                    self.lightPathLabel.text = L(@"LIVE_CAMERA_LIGHT_RL");
                    break;
                case CZCameraLightPathTransmitted:
                    self.lightPathLabel.text = L(@"LIVE_CAMERA_LIGHT_TL");
                    break;
            }
        }
        
        [self updateShadingCorrectionIsAppliedStatus];
    }
}

#pragma mark - CZCameraViewControllerDelegate

- (void)cameraViewController:(CZCameraViewController *)cameraViewController didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {
    if (snappingInfo.image == nil || error) {
        return;
    }
    
    self.doneButtonItem.enabled = YES;
    
    for (CZShadingCorrectionResetInfo *resetInfo in self.resetInfos) {
        if (resetInfo.position == self.selectedPosition) {
            [self.resetInfos removeObject:resetInfo];
            break;
        }
    }
    
    CZShadingCorrectionSetupInfo *setupInfo = [[CZShadingCorrectionSetupInfo alloc] init];
    setupInfo.position = self.selectedPosition;
    
    CZShadingCorrectionMaskDenoiser *denoiser = [[CZShadingCorrectionMaskDenoiser alloc] init];
    setupInfo.maskImage = [denoiser denoiseShadingCorrectionMaskImage:snappingInfo.image];
    
    NSArray *dataArray = [denoiser maxMaskDataArrayFromDenoisedImage:setupInfo.maskImage];
    setupInfo.maxMaskData = [dataArray mutableCopy];
    
    [self.setupInfos addObject:setupInfo];
    
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)cameraViewController:(CZCameraViewController *)cameraViewController performCustomActionWithCompletionHandler:(void (^)(BOOL))completionHandler {
    self.cameraViewControllerCompletionHandler = completionHandler;
    [self.camera calibrateShadingCorrection];
}

#pragma mark - CZCameraShadingCorrectionDelegate

- (void)cameraDidCalibrateShadingCorrection:(CZCamera *)camera {
    [self.camera enableShadingCorrection];
}

- (void)cameraDidFailToCalibrateShadingCorrection:(CZCamera *)camera {
    if (self.cameraViewControllerCompletionHandler) {
        self.cameraViewControllerCompletionHandler(NO);
        self.cameraViewControllerCompletionHandler = nil;
    }
    
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"MIC_SHADING_CORRECTION_ERR_GEMINI_FAIL") level:CZAlertLevelError];
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
    [alert presentAnimated:YES completion:nil];
}

- (void)cameraDidEnableShadingCorrection:(CZCamera *)camera {
    if (self.cameraViewControllerCompletionHandler) {
        self.cameraViewControllerCompletionHandler(YES);
        self.cameraViewControllerCompletionHandler = nil;
    }
    
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)cameraDidFailToEnableShadingCorrection:(CZCamera *)camera {
    if (self.cameraViewControllerCompletionHandler) {
        self.cameraViewControllerCompletionHandler(NO);
        self.cameraViewControllerCompletionHandler = nil;
    }
    
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"MIC_SHADING_CORRECTION_ERR_GEMINI_FAIL") level:CZAlertLevelError];
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
    [alert presentAnimated:YES completion:nil];
}

- (void)cameraDidDisableShadingCorrection:(CZCamera *)camera {
    [self.loadingHUD hide:YES];
    
    [self updateShadingCorrectionIsAppliedStatus];
}

- (void)cameraDidFailToDisableShadingCorrection:(CZCamera *)camera {
    [self.loadingHUD hide:YES];
    
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"MIC_SHADING_CORRECTION_ERR_GEMINI_FAIL") level:CZAlertLevelError];
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
    [alert presentAnimated:YES completion:nil];
}

@end
