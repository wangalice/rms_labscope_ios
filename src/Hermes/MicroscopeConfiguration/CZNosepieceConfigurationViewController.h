//
//  CZNosepieceConfigurationViewController.h
//  Hermes
//
//  Created by Li, Junlin on 2/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMicroscopeModel;
@class CZNosepieceConfigurationViewController;

@interface CZNosepieceConfigurationViewController : UIViewController

@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel;

- (void)reloadData;
- (void)setCurrentPosition:(NSUInteger)position;

- (void)lockSelection;
- (void)unlockSelection;

@end
