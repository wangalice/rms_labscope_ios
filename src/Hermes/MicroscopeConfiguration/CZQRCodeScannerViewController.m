//
//  CZQRCodeScannerViewController.m
//  Matscope
//
//  Created by Ralph Jin on 9/15/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZQRCodeScannerViewController.h"
#import <AVFoundation/AVFoundation.h>

const static CGFloat kLableHeight = 44;
const static CGFloat kScanIndicatorWidth = 450;
const static CGFloat kScanIndicatorWidthIphone = 250;
static NSString * const kScanLineAnimation = @"scanLineAnimation";

@interface CZQRCodeScannerViewController ()<AVCaptureMetadataOutputObjectsDelegate> {
    BOOL _gotValidString;
}

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@property (nonatomic, weak) UILabel *infoLabel;
@property (nonatomic, weak) UIImageView *successImageView;
@property (nonatomic, weak) UIImageView *scanIndicatorImageView;
@property (nonatomic, weak) UIImageView *scanLineImageView;
@property (nonatomic, weak) UIView *downView;
@property (nonatomic, weak) UIView *upView;
@property (nonatomic, weak) UIView *leftView;
@property (nonatomic, weak) UIView *rightView;

- (void)loadBeepSound;

@end

@implementation CZQRCodeScannerViewController

+ (CABasicAnimation *)moveYTime:(CGFloat)time fromY:(NSNumber *)fromY toY:(NSNumber *)toY repeat:(NSUInteger)repeatCount {
    CABasicAnimation *animationMove = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    [animationMove setFromValue:fromY];
    [animationMove setToValue:toY];
    animationMove.duration = time;
    animationMove.repeatCount  = repeatCount;
    animationMove.fillMode = kCAFillModeForwards;
    animationMove.removedOnCompletion = NO;
    animationMove.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animationMove;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *checkMark = [UIImage imageNamed:A(@"green-check-mark-large")];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:checkMark];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:imageView];
    imageView.alpha = 0.7;
    
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:imageView
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.view
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1
                                                                constant:0];
    
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:imageView
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.view
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0];

    [self.view addConstraint:centerX];
    [self.view addConstraint:centerY];
    imageView.hidden = YES;
    self.successImageView = imageView;
    
    [self loadBeepSound];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (interfaceOrientation != UIInterfaceOrientationUnknown) {
        _videoPreviewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)interfaceOrientation;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self stopReading];

    [super viewWillDisappear:animated];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if (deviceOrientation != UIDeviceOrientationUnknown) {
       _videoPreviewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)deviceOrientation;
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    _videoPreviewLayer.frame = self.view.bounds;
    
    CGRect frame = self.view.layer.bounds;
    frame.size.height -= kLableHeight;
    _videoPreviewLayer.frame = frame;
    
    CGFloat insetLeft = (frame.size.width - self.scanIndicatorImageView.frame.size.width) / 2.0;
    CGFloat insetTop = (frame.size.height - self.scanIndicatorImageView.frame.size.width) / 2.0;
    
    CGRect leftFrame = CGRectMake(0, 0, insetLeft, frame.size.height);
    self.leftView.frame = leftFrame;
    
    CGRect rightFrame = CGRectMake(frame.size.width - insetLeft, 0, insetLeft, frame.size.height);
    self.rightView.frame = rightFrame;
    
    CGRect upFrame = CGRectMake(insetLeft, 0, frame.size.width - 2 * insetLeft, insetTop);
    self.upView.frame = upFrame;
    
    CGRect downFrame = CGRectMake(insetLeft, frame.size.height - insetTop, frame.size.width - 2 * insetLeft, insetTop);
    self.downView.frame  = downFrame;
    
    frame = CGRectMake(0, frame.size.height, frame.size.width, kLableHeight);
    self.infoLabel.frame = frame;
    
    frame = self.scanIndicatorImageView.frame;
    frame.size.height = 2;
    self.scanLineImageView.frame = frame;
}

- (BOOL)startReading {
    if (_captureSession) {
        return NO;
    }
    
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        CZLogv(@"%@", [error localizedDescription]);
        return NO;
    }

    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("com.zeiss.qrcodescanner", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the self.view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (interfaceOrientation != UIInterfaceOrientationUnknown) {
        _videoPreviewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)interfaceOrientation;
    }
    
    CGRect frame = self.view.layer.bounds;
    frame.size.height -= 44;
    _videoPreviewLayer.frame = frame;
    [self.view.layer addSublayer:_videoPreviewLayer];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height, frame.size.width, 44)];
    infoLabel.backgroundColor = [UIColor clearColor];
    infoLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    infoLabel.textColor = kDefaultLabelColor;
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        infoLabel.numberOfLines = 2;
    } else {
        infoLabel.numberOfLines = 1;
    }
    [self.view addSubview:infoLabel];
    infoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.infoLabel = infoLabel;
    
    CGFloat scanIndicatorWidth = kScanIndicatorWidth;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        scanIndicatorWidth = kScanIndicatorWidthIphone;
    }
    
    CGFloat insetLeft = (frame.size.width - scanIndicatorWidth) / 2.0;
    CGFloat insetTop = (frame.size.height - scanIndicatorWidth) / 2.0;
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, insetLeft, frame.size.height)];
    leftView.alpha = 0.5;
    leftView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:leftView];
    self.leftView = leftView;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width - insetLeft, 0, insetLeft, frame.size.height)];
    rightView.alpha = 0.5;
    rightView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:rightView];
    self.rightView = rightView;
    
    UIView *upView = [[UIView alloc] initWithFrame:CGRectMake(insetLeft, 0, frame.size.width - 2 * insetLeft, insetTop)];
    upView.alpha = 0.5;
    upView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:upView];
    self.upView = upView;
    
    UIView *downView = [[UIView alloc] initWithFrame:CGRectMake(insetLeft, frame.size.height - insetTop, frame.size.width - 2 * insetLeft, insetTop)];
    downView.alpha = 0.5;
    downView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:downView];
    self.downView = downView;
    
    UIImage *scanIndicator = [UIImage imageNamed:A(@"scanOutline.png")];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:scanIndicator];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:imageView];
    self.scanIndicatorImageView = imageView;
    
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:imageView
                                                               attribute:NSLayoutAttributeCenterX
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.view
                                                               attribute:NSLayoutAttributeCenterX
                                                              multiplier:1
                                                                constant:0];
    
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:imageView
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.view
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:-22];
    
    [self.view addConstraint:centerX];
    [self.view addConstraint:centerY];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:scanIndicatorWidth]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:scanIndicatorWidth]];
    [self.view setNeedsUpdateConstraints];
    
    UIImageView *line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:A(@"scan-line-icon")]];
    line.contentMode = UIViewContentModeScaleToFill;
    line.backgroundColor = [UIColor clearColor];
    [self.view addSubview:line];
    self.scanLineImageView = line;

    // Start video capture.
    [_captureSession startRunning];
    
    _gotValidString = NO;

    if (_audioPlayer == nil) {
        [self loadBeepSound];
    }
    
    self.successImageView.hidden = YES;
    [self.view bringSubviewToFront:self.successImageView];
    self.infoLabel.hidden = NO;
    self.scanIndicatorImageView.hidden = NO;
    [self addAnimation];

    return YES;
}

- (void)stopReading {
    [_captureSession stopRunning];
    _captureSession = nil;
    [self removeAnimation];
}

- (void)showSuccessView {
    self.successImageView.hidden = NO;
    self.infoLabel.hidden = YES;
    self.scanIndicatorImageView.hidden = NO;
}

- (void)addAnimation {
    self.scanLineImageView.hidden = NO;
    CGFloat scanIndicatorWidth = kScanIndicatorWidth;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        scanIndicatorWidth = kScanIndicatorWidthIphone;
    }

    CABasicAnimation *animation = [CZQRCodeScannerViewController moveYTime:3 fromY:[NSNumber numberWithFloat:0] toY:[NSNumber numberWithFloat:scanIndicatorWidth - 2] repeat:OPEN_MAX];
    [self.scanLineImageView.layer addAnimation:animation forKey:kScanLineAnimation];
}

- (void)removeAnimation {
    [self.scanLineImageView.layer removeAnimationForKey:kScanLineAnimation];
    self.scanLineImageView.hidden = YES;
}

- (void)loadBeepSound {
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3" inDirectory:@"Assets"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        CZLogv(@"Could not play beep file.");
        CZLogv(@"%@", [error localizedDescription]);
    } else {
        // If the audio player was successfully initialized then load it in memory.
        [_audioPlayer prepareToPlay];
    }
}

- (void)getString:(NSString *)string {
    [self stopReading];
    
    if (_audioPlayer && !_audioPlayer.isPlaying) {
        [_audioPlayer play];
    }
    
    if ([_delegate respondsToSelector:@selector(codeScannerViewController:didGetString:)]) {
        [_delegate codeScannerViewController:self didGetString:string];
    }
}

- (void)getWrongString:(NSString *)string {
    self.infoLabel.text = L(@"MIC_CONFIG_QRCODE_INVALIDE_CONTENT");
    self.infoLabel.textColor = [UIColor yellowColor];
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    if (_gotValidString) {
        return;
    }
    
    for (AVMetadataMachineReadableCodeObject *metadataObj in metadataObjects) {
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            NSString *string = [metadataObj stringValue];
            
            BOOL validString = NO;
            if (self.scanMagicNumber.length == 0) {
                validString = YES;
            } else {
                if ([string hasPrefix:self.scanMagicNumber]) {
                    validString = YES;
                }
            }
            
            if (validString) {
                _gotValidString = YES;
                [self performSelectorOnMainThread:@selector(getString:) withObject:string waitUntilDone:NO];
            } else {
                [self performSelectorOnMainThread:@selector(getWrongString:) withObject:string waitUntilDone:NO];
            }
            
            break;
        }
    }
}

@end
