//
//  CZMicroscopeModelSelectionViewCell.h
//  Hermes
//
//  Created by Li, Junlin on 2/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZMicroscopeModelSelectionViewCell : UICollectionViewCell

@property (nonatomic, readonly, strong) UIImageView *microscopeModelImageView;
@property (nonatomic, readonly, strong) UILabel *microscopeModelNameLabel;

@end
