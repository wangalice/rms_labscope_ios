//
//  CZMicroscopeAutoScaleController.mm
//  Matscope
//
//  Created by Mike Wang on 10/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAutoScaleController.h"

#import <GPUImage/GPUImage.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "MBProgressHUD+Hide.h"
#import "CZAlertController.h"
#import "CZCameraView.h"
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZUIDefinition.h"
#import "CZFunctionBar.h"
#import "CZToastManager.h"
#import "CZLiveViewController.h"
#import <CZImageProcessing/UIImage+CVMat.h>
#import "CZImageDocument+CZCameraSnappingInfo.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#if DCM_REFACTORED
#import "CZFileListManager.h"
#endif

#define kAutoScalingPanelKeyboardOffset 380

static NSString *kCZMicroscopeConfigurationMeasureTipIdentiter = @"kMicroscopeConfigurationMeasureTipIdentiter";

using namespace cv;

@interface CZMicroscopeAutoScaleController () <UITextFieldDelegate,
                                               CZCameraDelegate,
                                               CZCameraViewDelegate>

@property (nonatomic, retain) CZMicroscopeModel *model;
@property (nonatomic, assign) NSUInteger position;
@property (atomic, assign) float fieldOfView;
@property (nonatomic, retain) CZCameraView *cameraView;
@property (nonatomic, retain) CZBarButtonItem *modeButton;
@property (nonatomic, retain) CZBarButtonItem *doneButton;
@property (nonatomic, retain) UIButton *calibrateButton;
@property (nonatomic, retain) MBProgressHUD *calibrateHUD;
@property (nonatomic, retain) UIView *manualPanel;
@property (nonatomic, retain) UITextField *manualInputTextField;
@property (nonatomic, assign) BOOL isInAutoMode;
@property (atomic, retain) UIImage *calibrateFrame;
@property (nonatomic, readonly, assign) CZElementUnitStyle originalUnitStyle;
@property (nonatomic, readonly, assign) CZElementUnitStyle unitStyle;
@property (nonatomic, readonly, assign) BOOL prefersBigUnit;
@property (nonatomic, assign) BOOL isFirstFrame;
@property (nonatomic, assign) CZScaleBarPosition originalPosition;

@end

@implementation CZMicroscopeAutoScaleController

- (id)initWithCameraView:(CZCameraView *)cameraView
         microscopeModel:(CZMicroscopeModel *)model
                position:(NSUInteger)position {
    self = [super init];
    if (self) {
        _cameraView = [cameraView retain];
        _cameraView.delegate = self;
        _model = [model retain];
        _position = position;
        _fieldOfView = [model fovWidthAtPosition:position];
        _isInAutoMode = (_model.type == kPrimotech);
        _isFirstFrame = YES;
        
        _prefersBigUnit = [CZMicroscopeModel shouldPreferBigUnitByModelName:[model modelName]];
        _originalUnitStyle = [CZElement unitStyle];
        _unitStyle = CZElementUnitStyleMicrometer;
        [CZElement setUnitStyle:_unitStyle];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
	
    self.view.backgroundColor = kDefaultBackGroundColor;
    
    [self.view addSubview:self.cameraView];
    
    NSString *currentMagnificationName = [self.model displayMagnificationAtPosition:self.position localized:YES];
    NSString *currentCameraName = self.cameraView.camera.displayName;
    
    self.navigationItem.title = [NSString stringWithFormat:L(@"MIC_CONFIG_CALIBRATE_TITLE_TEMPLATE"),
                                 currentMagnificationName,
                                 currentCameraName];
    
    _modeButton = [[CZBarButtonItem alloc] initWithTitle:L(@"MIC_CONFIG_CALIBRATE_MANUAL")
                                                  target:self
                                                  action:@selector(autoScalingModeAction:)];
    
    _doneButton = [[CZBarButtonItem alloc] initWithTitle:L(@"DONE")
                                                  target:self
                                                  action:@selector(autoScalingDoneAction:)];
    _doneButton.enabled = NO;
    
    if (_isInAutoMode) {
        self.navigationItem.rightBarButtonItems = @[_doneButton, _modeButton];
    } else {
        self.navigationItem.rightBarButtonItem = _doneButton;
    }
    
    _calibrateButton = [[UIButton alloc] init];
    _calibrateButton.exclusiveTouch = YES;
    _calibrateButton.hidden = !_isInAutoMode;
    [_calibrateButton setImage:[UIImage imageNamed:A(@"capture-button-scaling")] forState:UIControlStateNormal];
    [_calibrateButton setImage:[UIImage imageNamed:A(@"capture-button-scaling-pressed")] forState:UIControlStateHighlighted];
    [_calibrateButton addTarget:self action:@selector(calibrateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_calibrateButton];
    
    _manualPanel = [[UIView alloc] initWithFrame:CGRectMake(300, 600, 424, 50)];
    _manualPanel.userInteractionEnabled = NO;
    _manualPanel.hidden = _isInAutoMode;
    [self.view addSubview:_manualPanel];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:A(@"camera-control-background")]];
    [backgroundView setAlpha:0.8];
    [backgroundView setFrame:_manualPanel.bounds];
    [_manualPanel addSubview:backgroundView];
    [backgroundView release];
    
    UILabel *manualInputLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
    [manualInputLabel setBackgroundColor:[UIColor clearColor]];
    [manualInputLabel setText:L(@"MIC_CONFIG_CALIBRATE_MANUAL_INPUT")];
    manualInputLabel.textColor = kDefaultLabelColor;
    [_manualPanel addSubview:manualInputLabel];
    [manualInputLabel release];
    
    // There is a reason why manualInputTextField is not a subview of manualPanel.
    // We need to set userInteractionEnabled of manualPanel to NO to make it
    // transparent so that user still can select or move annotation beneath it.
    // But the text field needs userInteractionEnabled to be YES, so we cannot
    // let it be a subview of the panel.
    
    _manualInputTextField = [[UITextField alloc] initWithFrame:CGRectMake(590, 610, 124, 30)];
    _manualInputTextField.hidden = _isInAutoMode;
    _manualInputTextField.borderStyle = UITextBorderStyleRoundedRect;
    _manualInputTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _manualInputTextField.returnKeyType = UIReturnKeyDone;
    _manualInputTextField.delegate = self;
    [self.view addSubview:_manualInputTextField];
}

- (void)dealloc {
    [CZElement setUnitStyle:_originalUnitStyle];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];

    _cameraView.delegate = nil;
    _cameraView.camera = nil;
    [_cameraView release];
    
    [_model release];
    [_calibrateButton release];
    [_modeButton release];
    [_doneButton release];
    [_calibrateFrame release];
    [_calibrateHUD release];
    [_manualPanel release];
    [_manualInputTextField release];
    
    [super dealloc];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.modeButton.enabled = NO;
    
    CGRect frame = self.view.frame;
    self.calibrateButton.frame = CGRectMake(frame.size.width - kUISnapButtonDiameter - kUISnapButtonXPadding,
                                            (frame.size.height - kUISnapButtonDiameter) / 2.0,
                                            kUISnapButtonDiameter,
                                            kUISnapButtonDiameter);
    self.calibrateButton.enabled = NO;
    
    [self.cameraView setCameraViewFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.cameraView setDelegate:self];
    [self.cameraView enableAdvancedView:YES];
    [self.cameraView forceAutoCameraMode];
    [self.cameraView play];

    [self.view sendSubviewToBack:self.cameraView];
    
    [self.cameraView updateFOV:self.fieldOfView];
    [self.cameraView setScaleBarHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.cameraView endMeasuring];
    [self.cameraView restoreCameraMode];
    [self.cameraView stop];
    self.cameraView.camera = nil;
    [self restoreLiveView];
    
    [self.cameraView setScaleBarHidden:NO];
    [[CZToastManager sharedManager] dismissToastMessageWithIdentifier:kCZMicroscopeConfigurationMeasureTipIdentiter];
}

- (MBProgressHUD *)calibrateHUD {
    if (!_calibrateHUD) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        self.calibrateHUD = hud;
    }
    
    return _calibrateHUD;
}

- (void)autoScalingModeAction:(id)sender {
    if (!self.isInAutoMode) {
        [self refreshFieldOfView];
    }

    self.isInAutoMode = !self.isInAutoMode;
    self.calibrateButton.hidden = !self.isInAutoMode;
    self.manualPanel.hidden = self.isInAutoMode;
    self.manualInputTextField.hidden = self.isInAutoMode;

    self.manualInputTextField.text = nil;
    [self.manualInputTextField resignFirstResponder];

    if (self.isInAutoMode) {
        [self.modeButton setTitle:L(@"MIC_CONFIG_CALIBRATE_MANUAL")];
        
        _unitStyle = CZElementUnitStyleMicrometer;
        [CZElement setUnitStyle:_unitStyle];
    } else {
        [self.modeButton setTitle:L(@"MIC_CONFIG_CALIBRATE_AUTO")];
        
        [self manualScalingModeAction];
    }

    self.doneButton.enabled = (self.fieldOfView != kNonCalibratedValue);
}

- (void)manualScalingModeAction {
    if (!self.isInAutoMode) {
        _unitStyle = [CZElement determineUnitStyle:_originalUnitStyle prefersBigUnit:_prefersBigUnit];
        [CZElement setUnitStyle:_unitStyle];
        [self refreshInputTextField];
    }
}

- (void)autoScalingDoneAction:(id)sender {
    // Calculate the FOV when working in manual scaling mode.
    if (!self.isInAutoMode) {
        [self refreshFieldOfView];
    }
    
    [self.model setFOVWidth:self.fieldOfView atPosition:self.position];
    
    if ([self.delegate respondsToSelector:@selector(microscopeAutoScaleControllerDidFinishScaling:)]) {
        [self.delegate microscopeAutoScaleControllerDidFinishScaling:self];
    }
    
    [self.cameraView.camera beginSnappingFromVideoStream];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)calibrateButtonClicked:(id)sender {
    @synchronized (_calibrateFrame) {
        if (!self.calibrateFrame) {
            return;
        }
        
        [self.calibrateHUD setColor:kSpinnerBackgroundColor];
        [self.calibrateHUD setLabelText:L(@"BUSY_INDICATOR_MEASURING")];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            __block float fieldOfView = [self fieldOfViewFromImage:self.calibrateFrame];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.calibrateHUD dismiss:YES];
                self.calibrateHUD = nil;
                
                double defaultFieldOfView = [self.model defaultFOVWidthAtPosition:self.position];
                if (defaultFieldOfView != kNonCalibratedValue) {
                    float ratio = fieldOfView / defaultFieldOfView;
                    if (ratio > 1.2 || ratio < 0.83) { // 20% tolerance
                        fieldOfView = kNonCalibratedValue;
                    }
                }

                if (fieldOfView != kNonCalibratedValue) {
                    self.fieldOfView = fieldOfView;
                    [self.cameraView updateFOV:fieldOfView];
                    [[CZToastManager sharedManager] showToastMessage:L(@"MIC_CONFIG_MEASURE_SUCCESS")
                                                          identifier:kCZMicroscopeConfigurationMeasureTipIdentiter
                                                        dismissAfter:10.0
                                                          sourceRect:CGRectOffset(self.view.frame, 0, CGRectGetMidY(self.view.frame))
                                                            animated:YES];
                                                                                                                                                                                                        
                    self.doneButton.enabled = YES;
                    [self.cameraView beginMeasuringWithMeasurementHidden:YES];
                } else {
                    CZAlertController *alertController = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"ERROR_MEASURING_FAILED") level:CZAlertLevelError];
                    [alertController addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:NULL];
                    [alertController presentAnimated:YES completion:nil];
                }
            });
        });
    }
}

- (void)restoreLiveView {
    self.cameraView.camera = nil;
    [self.cameraView removeFromSuperview];
    self.cameraView = nil;
}

- (CGFloat)fieldOfViewFromImage:(UIImage *)image {
    // Definition of constants and algorithm parameters.
    
    const double CV_HALF_PI = CV_PI / 2.0;
    const double CV_QUARTER_PI = CV_PI / 4.0;
    
    const size_t kPreTestSampleCount = 100;
    const double kPreTestMinBlackRatio = 33.0;
    const double kPreTestMaxBlackRatio = 100.0 - kPreTestMinBlackRatio;
    const double kPreTestDeviationThreshold = 1.5;
    const double kEnlargingScale = 4.0;
    const int kEnlargingLengthThreshold = 20;
    const double kAngleTolerance = 2.0 * CV_PI / 180.0;
    const double kLeaningThresholdRatio = 0.0025;
    const int kHoughThreshold = 80;
    const double kDeviationThreshold = 0.06;
    const double kCellWidthInMicrometer = 25.0;
    
    if (image == nil) {
        CZLogv(@"Scaling error: source image is nil.");
        return kNonCalibratedValue;
    }
    
    Mat matSource = [image mat];
    
    int width = matSource.cols;
    int height = matSource.rows;
    
    Mat matGray;
    cvtColor(matSource, matGray, CV_BGR2GRAY);
    matSource.release();
    
    // Step 1: Roughly estimate the length of pixel cycle. This will
    // calculate the length of continous white or black pixels, except the
    // first length and the last since they could be incomplete shape.
    
    int meanGray = ceil(mean(matGray).val[0]);
    
    vector<int> lengths;
    int lengthCounts[2] = {0, 0};
    int blackLengthSums[2] = {0, 0};
    int whiteLengthSums[2] = {0, 0};
    double averageLengths[2] = {0.0, 0.0};
    double blackRatioValues[2] = {0.0, 0.0};
    
    // Two phases, first horizontal, then vertical.
    for (size_t i = 0; i < 2; ++i) {
        for (size_t j = 0; j < kPreTestSampleCount; ++j) {
            Mat matSegment;
            if (i == 0) {
                int rowNumber = j * floor((double)height / kPreTestSampleCount);
                threshold(matGray.row(rowNumber), matSegment, meanGray, 255, CV_THRESH_BINARY);
            } else {
                int colNumber = j * floor((double)width / kPreTestSampleCount);
                threshold(matGray.col(colNumber), matSegment, meanGray, 255, CV_THRESH_BINARY);
            }
            
            int lastValue = -1;
            int currentLength = 0;
            bool isFirst = true;
            
            int segmentSize = (i == 0) ? matSegment.cols : matSegment.rows;
            for (int k = 0; k < segmentSize; ++k) {
                int value = (i == 0) ? matSegment.at<uchar>(0, k) : matSegment.at<uchar>(k, 0);
                if (value != lastValue) {
                    if (!isFirst) {
                        lengths.push_back(currentLength);
                        ++lengthCounts[i];
                        
                        if (lastValue == 0) {
                            blackLengthSums[i] += currentLength;
                        } else {
                            whiteLengthSums[i] += currentLength;
                        }
                    }
                    
                    isFirst = false;
                    lastValue = value;
                    currentLength = 1;
                } else {
                    ++currentLength;
                }
            }
        }
        
        double lengthSum = blackLengthSums[i] + whiteLengthSums[i];
        averageLengths[i] = lengthSum / lengthCounts[i];
        blackRatioValues[i] = 100.0 * blackLengthSums[i] / lengthSum;
        
        CZLogv(@"Scaling Pre-test #%zu: estimated distance = %lf, black = %lf%%",
              i + 1, averageLengths[i], blackRatioValues[i]);
        
        if (blackRatioValues[i] < kPreTestMinBlackRatio ||
            blackRatioValues[i] > kPreTestMaxBlackRatio) {
            CZLogv(@"Scaling error: pre-calculation shows invalid pattern for the image.");
            return kNonCalibratedValue;
        }
    }
    
    // Combine the results.
    
    double accurateAverageLength = (averageLengths[0] + averageLengths[1]) / 2.0;
    int averageLength = ceil(accurateAverageLength);
    
    CZLogv(@"Scaling #1: overall estimated distance = %d, black = %lf%%",
          averageLength, (blackRatioValues[0] + blackRatioValues[1]) / 2.0);
    
    if (averageLength <= 0) {
        CZLogv(@"Scaling error: average length is invalid.");
        return kNonCalibratedValue;
    }
    
    // Calculate the deviation of all length samples, if the deviation is too
    // high, abandon the results and fail the process.
    
    double deviation = 0.0;
    for (size_t i = 0; i < lengths.size(); ++i) {
        double diff = lengths[i] - accurateAverageLength;
        deviation += diff * diff;
    }
    deviation = sqrt(deviation / lengths.size()) / accurateAverageLength;
    
    if (deviation > kPreTestDeviationThreshold) {
        CZLogv(@"Scaling error: pre-calculation shows invalid pattern for the image.");
        return kNonCalibratedValue;
    }
    
    // Step 1.5: Enlarge the image if the predicted average length is very small.
    
    BOOL isScaled = (averageLength <= kEnlargingLengthThreshold);
    Mat matScaled;
    if (isScaled) {
        int newWidth = width / kEnlargingScale;
        int newHeight = height / kEnlargingScale;
        
        Mat matTemp = matGray(cv::Rect((width - newWidth) / 2,
                                       (height - newHeight) / 2,
                                       newWidth, newHeight)).clone();
        resize(matTemp, matScaled, cv::Size(), kEnlargingScale, kEnlargingScale);
        matTemp.release();
        
        averageLength *= kEnlargingScale;
        
        CZLogv(@"Scaling #1.5: image is enlarged because of low magnification");
    } else {
        matScaled = matGray.clone();
    }
    matGray.release();
    
    // Step 2: Use Otsu's thresholding algorithm and adaptive thresholding method
    // to get binary edge image.
    
    Mat matThreshold;
    threshold(matScaled, matThreshold, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);
    matScaled.release();
    
    Mat matAdaptive;
    adaptiveThreshold(matThreshold, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 3, 5);
    matThreshold.release();
    
    CZLogv(@"Scaling #2: edge detection complete");
    
    // Step 3: Find lines and calculate the skewing angle of the image.
    
    int minLineLength = sqrt(averageLength) * 2;
    
    vector<Vec4i> lines;
    HoughLinesP(matAdaptive,        // 8-bit single channel binary source image
                lines,              // output vector of lines
                1,                  // distance resolution in pixels
                CV_PI / 180,        // angle resolution in radians
                kHoughThreshold,    // accumulator threshold
                minLineLength,      // minimum line length
                0);                 // maximum allowed gap between points to link
    
    double averageAngle = 0.0;
    if (lines.empty()) {
        CZLogv(@"Scaling warning: cannot get correct skewing angle.");
    } else {
        double weightSum = 0.0;
        double angleSum = 0.0;
        for (size_t i = 0; i < lines.size(); i++) {
            double x = lines[i][2] - lines[i][0];
            double y = lines[i][3] - lines[i][1];
            double angle = atan2(y, x);
            double distance = hypot(x, y);
            
            // Normalize angle into [0, PI)
            if (angle < 0.0) {
                angle += CV_PI;
            } else if (angle == CV_PI) {
                angle = 0.0;
            }
            
            // Normalize angle into [0, PI/2)
            if (angle >= CV_HALF_PI) {
                angle -= CV_HALF_PI;
            }
            
            // Normalize angle into (-PI/4, PI/4]
            if (angle >= CV_QUARTER_PI) {
                angle -= CV_HALF_PI;
            }
            
            // If the angle is very close to -PI/4, consider it PI/4
            if (angle < - CV_QUARTER_PI + kAngleTolerance) {
                angle = CV_QUARTER_PI;
            }
            
            double weight = pow(distance, 10) / 100.0;
            angleSum += angle * weight;
            weightSum += weight;
        }
        
        averageAngle = angleSum / weightSum * 180.0 / CV_PI;
    }
    
    CZLogv(@"Scaling #3: average angle = %lf", averageAngle);
    
    // Step 4: De-skew the original image.
    // Currently no tolerance, if it is necessary, change the angle to a
    // reasonable threshold value.
    
    if (averageAngle != 0.0) {
        Point2f centerPoint(width / 2.0f, height / 2.0f);
        Mat rotation = getRotationMatrix2D(centerPoint, averageAngle, 1);
        warpAffine(matAdaptive, matAdaptive, rotation, matAdaptive.size(), INTER_CUBIC);
        CZLogv(@"Scaling #4: rotation complete");
    } else {
        CZLogv(@"Scaling #4: no need to rotate");
    }
    
    // Step 5: Find lines in the image and group by horizontal / vertical.
    
    double leaningThreshold = width * kLeaningThresholdRatio;
    
    lines.clear();
    HoughLinesP(matAdaptive, lines, 1, CV_PI / 180, kHoughThreshold,
                minLineLength, minLineLength / 5);
    
    matAdaptive.release();
    
    vector<double> values[2]; // 0 is X-axis, 1 is Y-axis
    
    for (size_t i = 0; i < lines.size(); i++) {
        int offsetX = abs(lines[i][0] - lines[i][2]);
        int offsetY = abs(lines[i][1] - lines[i][3]);
        
        if (offsetX < leaningThreshold) {
            // Vertical line, get X value
            double value = (lines[i][0] + lines[i][2]) * 0.5;
            values[0].push_back(value);
        } else if (offsetY < leaningThreshold) {
            // Horizontal line
            double value = (lines[i][1] + lines[i][3]) * 0.5;
            values[1].push_back(value);
        }
    }
    
    for (size_t axis = 0; axis < 2; ++axis) {
        sort(values[axis].begin(), values[axis].end());
    }
    
    CZLogv(@"Scaling #5: reference lines found");
    
    // Step 6: Do clustering on both horizontal and vertical values.
    
    double groupingThreshold = sqrt(averageLength);
    vector<double> clusterValues[2];
    
    for (size_t axis = 0; axis < 2; ++axis) {
        double groupSum = 0.0;
        int groupCount = 0;
        
        for (size_t i = 0; i < values[axis].size(); ++i) {
            if (i == 0 || values[axis][i] - values[axis][i-1] < groupingThreshold) {
                groupSum += values[axis][i];
                ++groupCount;
            } else {
                clusterValues[axis].push_back(groupSum / groupCount);
                
                groupSum = values[axis][i];
                groupCount = 1;
            }
        }
        clusterValues[axis].push_back(groupSum / groupCount);
    }
    
    CZLogv(@"Scaling #6: coordinate data clustering done");
    
    // Step 7: Calculate the scaling value by the distance among lines.
    
    double distanceSum = 0.0;
    int distanceCount = 0;
    
    for (size_t axis = 0; axis < 2; ++axis) {
        if (clusterValues[axis].size() > 1) {
            vector<char> clusterFilter;
            clusterFilter.resize(clusterValues[axis].size());
            
            double distanceSubSum = 0.0;
            for (size_t i = 0; i < clusterValues[axis].size() - 1; ++i) {
                distanceSubSum += clusterValues[axis][i+1] - clusterValues[axis][i];
            }
            double distanceSubAverage = distanceSubSum / (clusterValues[axis].size() - 1);
            
            for (int i = 0; i < clusterValues[axis].size() - 1; ++i) {
                double distance = clusterValues[axis][i+1] - clusterValues[axis][i];
                double deviation = fabs(distance - distanceSubAverage) / distanceSubAverage;
                if (deviation < kDeviationThreshold) {
                    distanceSum += distance;
                    ++distanceCount;
                } else {
                    ++clusterFilter[i];
                    ++clusterFilter[i+1];
                }
            }
            
            for (long i = clusterValues[axis].size() - 1; i >= 0; --i) {
                if (clusterFilter[i] == 2) {
                    clusterValues[axis].erase(clusterValues[axis].begin() + i);
                }
            }
        } else {
            clusterValues[axis].clear();
        }
    }
    
    if (clusterValues[0].empty() || clusterValues[1].empty()) {
        CZLogv(@"Scaling error: result is not reliable if only horizontal or vertical lines detected.");
        return kNonCalibratedValue;
    }
    
    if (distanceCount > 0) {
        double averageDistance = distanceSum / distanceCount;
        if (isScaled) {
            averageDistance /= kEnlargingScale;
        }
        
        double fieldOfView = kCellWidthInMicrometer / averageDistance * image.size.width;
        
        CZLogv(@"Scaling #7: FOV = %lf, average distance = %lf pixels", fieldOfView, averageDistance);
        
        return fieldOfView;
    } else {
        CZLogv(@"Scaling error: average distance cannot be determined");
        return kNonCalibratedValue;
    }
}

#pragma mark - Private methods

- (void)appDidBecomeActive:(NSNotification *)notification {
    _originalUnitStyle = [[CZDefaultSettings sharedInstance] unitStyle];

    if (self.isInAutoMode) {
        [CZElement setUnitStyle:_unitStyle];
    } else {
        _unitStyle = [CZElement determineUnitStyle:_originalUnitStyle prefersBigUnit:self.prefersBigUnit];
        [CZElement setUnitStyle:_unitStyle];
        [self refreshInputTextField];
    }
}

- (void)refreshInputTextField {
    UILabel *unitLabel = (UILabel *)_manualInputTextField.rightView;
    if (unitLabel == nil) {
        unitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        _manualInputTextField.rightView = unitLabel;
        _manualInputTextField.rightViewMode = UITextFieldViewModeAlways;
        [unitLabel release];
    }

    NSString *unitStringKey = [CZElement distanceUnitStringByStyle:_unitStyle prefersBigUnit:self.prefersBigUnit];
    if (unitStringKey) {
        unitLabel.text = L(unitStringKey);
    }
    CGSize fitSize = [unitLabel sizeThatFits:CGSizeMake(1024, 24)];
    unitLabel.frame = CGRectMake(0, 0, fitSize.width, 24);

    if (self.fieldOfView <= 0) {
        switch (_unitStyle) {
            case CZElementUnitStyleInch:
                _manualInputTextField.text = @"1";
                break;
            default:
                _manualInputTextField.text = @"100";
                break;
        }
    } else {
        CGFloat lengthInPixel = [self.cameraView measuredValue];
        CGSize liveResolution = [self.cameraView.camera liveResolution];
        CGFloat lengthInMicrometer = lengthInPixel * self.fieldOfView / liveResolution.width;
        CGFloat length = lengthInMicrometer;
        NSUInteger precision = 0;
        switch (_unitStyle) {
            case CZElementUnitStyleMil:
                length = lengthInMicrometer * kCZUMtoMil;
                break;
            case CZElementUnitStyleMM:
                length = lengthInMicrometer * kCZUMtoMM;
                precision = 3;
                break;
            case CZElementUnitStyleInch:
                length = lengthInMicrometer * kCZUMtoInch;
                precision = 3;
                break;
            default:
                break;
        }
        _manualInputTextField.text = [CZCommonUtils stringFromFloat:length precision:precision];
    }
}

- (void)refreshFieldOfView {
    float length = [[CZCommonUtils numberFromLocalizedString:self.manualInputTextField.text] floatValue];
    float lengthInMicrometer = length;
    switch (_unitStyle) {
        case CZElementUnitStyleMicrometer:
            lengthInMicrometer = length;
            break;
        case CZElementUnitStyleMil:
            lengthInMicrometer = length / kCZUMtoMil;
            break;
        case CZElementUnitStyleMM:
            lengthInMicrometer = length / kCZUMtoMM;
            break;
        case CZElementUnitStyleInch:
            lengthInMicrometer = length / kCZUMtoInch;
            break;
        default:
            break;
    }
    float lengthInPixel = [self.cameraView measuredValue];

    CGSize liveResolution = [self.cameraView.camera liveResolution];
    float fieldOfView = lengthInMicrometer / lengthInPixel * liveResolution.width;
    if (fieldOfView <= 0.0) {
        self.fieldOfView = kNonCalibratedValue;
    } else {
        self.fieldOfView = fieldOfView;
    }
    [self.cameraView updateFOV:self.fieldOfView];
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.manualInputTextField) {
        CGRect frame = self.manualPanel.frame;
        frame.origin.y -= kAutoScalingPanelKeyboardOffset;
        self.manualPanel.frame = frame;
        
        frame = self.manualInputTextField.frame;
        frame.origin.y -= kAutoScalingPanelKeyboardOffset;
        self.manualInputTextField.frame = frame;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldDidChangeText:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    if (textField == self.manualInputTextField) {
        return [CZCommonUtils numericTextField:textField shouldAcceptChange:textEntered];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.manualInputTextField) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UITextFieldTextDidChangeNotification
                                                      object:textField];
        
        CGRect frame = self.manualPanel.frame;
        frame.origin.y += kAutoScalingPanelKeyboardOffset;
        self.manualPanel.frame = frame;
        
        frame = self.manualInputTextField.frame;
        frame.origin.y += kAutoScalingPanelKeyboardOffset;
        self.manualInputTextField.frame = frame;

        [self refreshFieldOfView];
        self.doneButton.enabled = (self.fieldOfView != kNonCalibratedValue);
    }
}

- (void)textFieldDidChangeText:(NSNotification *)notification {
    UITextField *textField = (UITextField *)notification.object;
    if (textField == self.manualInputTextField) {
        float lengthInMicrometer = [[CZCommonUtils numberFromLocalizedString:self.manualInputTextField.text] floatValue];
        self.doneButton.enabled = (lengthInMicrometer > 0.0f);
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.manualInputTextField) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - CZCameraDelegate methods

- (void)camera:(CZCamera *)camera didGetVideoFrame:(UIImage *)frame {
    @synchronized (_calibrateFrame) {
        self.calibrateFrame = frame;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.modeButton.enabled = YES;
            self.calibrateButton.enabled = YES;
            if (self.isInAutoMode) {
                if (self.fieldOfView != kNonCalibratedValue) {
                    [self.cameraView beginMeasuringWithMeasurementHidden:NO];
                } else {
                    [self.cameraView endMeasuring];
                }
            } else {
                [self.cameraView beginMeasuringWithMeasurementHidden:YES];
                if (_isFirstFrame) {
                    [self manualScalingModeAction];
                    [self refreshFieldOfView];
                    self.doneButton.enabled = (self.fieldOfView != kNonCalibratedValue);
                }
            }
            _isFirstFrame = NO;
        });
    }
}

- (void)camera:(CZCamera *)camera didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error {
    if (!error && snappingInfo.image) {
        UIImage *scalingImage = [[snappingInfo.image retain] autorelease];
        
        NSString *magnificationString = [self.model displayMagnificationAtPosition:self.position localized:NO];
        NSString *macAddress = _cameraView.camera.macAddress;
        macAddress = [macAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
        NSString *imageFileName = [NSString stringWithFormat:@"%@_%@_scaling_%lu.jpg", macAddress, magnificationString, (unsigned long)_position];
        NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:imageFileName];
       
        // update metadata info for scaling image
        CZElementLayer *layer = [[self.cameraView.videoTool.docManager.elementLayer copy] autorelease];
        CZElement *line = [layer firstElementOfClass:[CZElementLine class]];
        line.measurementHidden = NO;
        CZElement *scaleBar = [layer firstElementOfClass:[CZElementScaleBar class]];
        [scaleBar removeFromParent];
#if DCM_REFACTORED
        NSString *directory = [CZLocalFileList sharedInstance].documentPath;
#else
        NSString *directory = [NSFileManager defaultManager].cz_documentPath;
#endif
        CZImageDocument *document = [[CZImageDocument alloc] initFromSnappingInfo:snappingInfo];
        CZDocManager *newDocManager = [[CZDocManager alloc] initWithDirectory:directory document:document elementLayer:layer];
        [newDocManager updateImagePropertyFrom:self.model updateFilePath:NO];

        newDocManager.calibratedScaling = [self.model fovWidthAtPosition:_position] / snappingInfo.image.size.width;
        NSAssert(layer.scaling > 0, @"invalid calibrated scaling <= 0");
        
        //save burn in scaling image to the tmp dir
        scalingImage = [newDocManager imageWithBurninAnnotations];
        NSData *scalingImageData = UIImageJPEGRepresentation(scalingImage, 1.0);
        [scalingImageData writeToFile:filePath atomically:YES];
        
        // save measurement to the xml file
        NSString *xmlFileName = [NSString stringWithFormat:@"%@_%@_scaling_%lu.xml", macAddress, magnificationString, (unsigned long)_position];
        [newDocManager writeMetaDataToFilePath:[NSTemporaryDirectory() stringByAppendingPathComponent:xmlFileName]];
        [newDocManager release];
        [document release];
    }
}

#pragma mark - CZCameraViewDelegate methods

- (void)cameraView:(CZCameraView *)cameraView reachabilityDidChange:(BOOL)isReachable {
    if (!isReachable) {
        if (!self.isInAutoMode) {
            [self autoScalingModeAction:nil];
        }
        
        self.modeButton.enabled = NO;
        self.calibrateButton.enabled = NO;
    }
}

@end
