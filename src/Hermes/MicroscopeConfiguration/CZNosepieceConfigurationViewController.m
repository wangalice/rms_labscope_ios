//
//  CZNosepieceConfigurationViewController.m
//  Hermes
//
//  Created by Li, Junlin on 2/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepieceConfigurationViewController.h"
#import "CZGridView.h"
#import "CZAlertController.h"
#import "CZUppercaseLabel.h"
#import "CZNosepiecePositionButton.h"
#import "CZPickerViewController.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@interface CZNosepieceConfigurationViewController ()

@property (nonatomic, readonly, strong) CZGridView *nosepiecePositionButtonGridView;
@property (nonatomic, readonly, strong) CZUppercaseLabel *zoomClickStopsLabel;
@property (nonatomic, readonly, strong) CZGridView *zoomClickStopCheckBoxGridView;
@property (nonatomic, readonly, strong) UIStackView *objectiveNumberStackView;
@property (nonatomic, readonly, strong) UILabel *objectiveNumberLabel;
@property (nonatomic, readonly, strong) CZDropdownMenu *objectiveNumberDropdown;

@end

@implementation CZNosepieceConfigurationViewController

@synthesize nosepiecePositionButtonGridView = _nosepiecePositionButtonGridView;
@synthesize zoomClickStopsLabel = _zoomClickStopsLabel;
@synthesize zoomClickStopCheckBoxGridView = _zoomClickStopCheckBoxGridView;
@synthesize objectiveNumberStackView = _objectiveNumberStackView;
@synthesize objectiveNumberLabel = _objectiveNumberLabel;
@synthesize objectiveNumberDropdown = _objectiveNumberDropdown;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _microscopeModel = microscopeModel;
        self.title = L(@"MIC_CONFIG_OBJECTIVE_SELECTION");
    }
    return self;
}

- (void)setMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    _microscopeModel = microscopeModel;
    [self reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.nosepiecePositionButtonGridView];
    [self.view addSubview:self.zoomClickStopsLabel];
    [self.view addSubview:self.zoomClickStopCheckBoxGridView];
    
    [self.view addSubview:self.objectiveNumberStackView];
    [self.objectiveNumberStackView addArrangedSubview:self.objectiveNumberLabel];
    [self.objectiveNumberStackView addArrangedSubview:self.objectiveNumberDropdown];
    
    [self.objectiveNumberStackView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:32.0].active = YES;
    [self.objectiveNumberStackView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-32.0].active = YES;
    [self.objectiveNumberStackView.heightAnchor constraintEqualToConstant:32.0].active = YES;
}

- (void)reloadData {
    [self reloadNosepiecePositions];
    [self reloadZoomClickStops];
    
    self.objectiveNumberStackView.hidden = (self.microscopeModel.type != kAxiocamCompound);
    self.objectiveNumberDropdown.selectedItemTitle = @(self.microscopeModel.nosepiece.positions).stringValue;
}

- (void)reloadNosepiecePositions {
    [self.nosepiecePositionButtonGridView removeAllContentViews];
    
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        CZZoomLevel *zoomLevel = [self.microscopeModel zoomForType:kZoomTypeStereoObjective];
        
        CZNosepiecePositionButton *button = [[CZNosepiecePositionButton alloc] initWithPosition:0 magnification:zoomLevel.magnification displayMagnification:zoomLevel.displayName];
        button.tag = 0;
        [button addTarget:self action:@selector(nosepiecePositionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"ObjectiveSelectionButton";
        
        [self.nosepiecePositionButtonGridView addContentView:button];
    } else {
        for (NSUInteger position = 0; position < self.microscopeModel.nosepiece.positions; position++) {
            CZObjective *objective = [self.microscopeModel.nosepiece objectiveAtPosition:position];
            
            CZNosepiecePositionButton *button = [[CZNosepiecePositionButton alloc] initWithPosition:position magnification:objective.magnification displayMagnification:objective.displayMagnification];
            button.tag = position;
            [button addTarget:self action:@selector(nosepiecePositionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            button.isAccessibilityElement = YES;
            button.accessibilityIdentifier = @"ObjectiveSelectionButton";
            
            [self.nosepiecePositionButtonGridView addContentView:button];
        }
        [self setCurrentPosition:self.microscopeModel.nosepiece.currentPosition];
    }
    
    if ([self.microscopeModel isKindOfClass:[CZMicroscopePrimoStar class]]) {
        CZMicroscopePrimoStar *primoStar = (CZMicroscopePrimoStar *)self.microscopeModel;
        if (primoStar.package != kCZPrimoStarPackageManualMode) {
            [self lockSelection];
        } else {
            [self unlockSelection];
        }
    }
}

- (void)reloadZoomClickStops {
    [self.zoomClickStopCheckBoxGridView removeAllContentViews];
    
    BOOL isStereo = [self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]];
    BOOL isGeneric = (self.microscopeModel.type == kAxiocamCompound || self.microscopeModel.type == kAxiocamStereo);
    
    self.zoomClickStopsLabel.hidden = !isStereo || !isGeneric;
    self.zoomClickStopCheckBoxGridView.hidden = !isStereo || !isGeneric;
    
    if (!isStereo || !isGeneric) {
        return;
    }
    
    NSMutableArray *zoomLevelIndexMap = [NSMutableArray array];
    
    CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
    for (NSUInteger i = 0; i < [zoomLevelSet zoomLevelCountOfType:kZoomTypeZoomClickStop]; ++i) {
        CZZoomLevel *zoomLevel = [zoomLevelSet zoomLevelAtIndex:i ofType:kZoomTypeZoomClickStop];
        if (i > 0 && zoomLevel.magnification == 0) {
            continue;
        }
        
        [zoomLevelIndexMap addObject:@(i)];
    }
    
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeAxiocamStereo class]]) {
        [zoomLevelIndexMap sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSUInteger i1 = [(NSNumber *)obj1 unsignedIntegerValue];
            NSUInteger i2 = [(NSNumber *)obj2 unsignedIntegerValue];
            CZZoomLevel *zoomLevel1 = [zoomLevelSet zoomLevelAtIndex:i1 ofType:kZoomTypeZoomClickStop];
            CZZoomLevel *zoomLevel2 = [zoomLevelSet zoomLevelAtIndex:i2 ofType:kZoomTypeZoomClickStop];
            return [zoomLevel1 compare:zoomLevel2];
        }];
    }
    
    for (NSNumber *index in zoomLevelIndexMap) {
        NSUInteger i = [index unsignedIntegerValue];
        CZZoomLevel *zoomLevel = [zoomLevelSet zoomLevelAtIndex:i ofType:kZoomTypeZoomClickStop];
        
        CZCheckBox *checkBox = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
        checkBox.tag = i;
        [checkBox setTitle:zoomLevel.displayName forState:UIControlStateNormal];
        [checkBox addTarget:self action:@selector(zoomClickStopCheckBoxAction:) forControlEvents:UIControlEventTouchUpInside];
        
        checkBox.isAccessibilityElement = YES;
        checkBox.accessibilityIdentifier = @"ZoomClickStopCheckBox";
        
        for (NSUInteger position = 0; position < self.microscopeModel.zoomClickStopPositions; position++) {
            NSString *zoomName = [[self.microscopeModel zoomClickStopAtPosition:position] displayName];
            if ([zoomName isEqualToString:zoomLevel.displayName]) {
                checkBox.selected = YES;
                break;
            }
        }
        
        [self.zoomClickStopCheckBoxGridView addContentView:checkBox];
    }
}

- (void)setCurrentPosition:(NSUInteger)position {
    for (UIButton *button in self.nosepiecePositionButtonGridView.contentViews) {
        button.selected = (button.tag == position);
    }
}

- (void)lockSelection {
    for (UIButton *button in self.nosepiecePositionButtonGridView.contentViews) {
        button.enabled = NO;
    }
}

- (void)unlockSelection {
    for (UIButton *button in self.nosepiecePositionButtonGridView.contentViews) {
        button.enabled = YES;
    }
}

#pragma mark - Views

- (CZGridView *)nosepiecePositionButtonGridView {
    if (_nosepiecePositionButtonGridView == nil) {
        CZGridRow *row = [[CZGridRow alloc] initWithHeight:CZNosepiecePositionButtonSize alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = @[row];
        
        CZGridColumn *column = [[CZGridColumn alloc] initWithWidth:CZNosepiecePositionButtonSize alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = [NSArray cz_arrayWithRepeatedObject:column count:7];
        
        _nosepiecePositionButtonGridView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0, 704.0, CZNosepiecePositionButtonSize) rows:rows columns:columns];
        _nosepiecePositionButtonGridView.columnSpacing = 16.0;
    }
    return _nosepiecePositionButtonGridView;
}

- (CZUppercaseLabel *)zoomClickStopsLabel {
    if (_zoomClickStopsLabel == nil) {
        _zoomClickStopsLabel = [[CZUppercaseLabel alloc] initWithFrame:CGRectMake(32.0, 32.0 + CZNosepiecePositionButtonSize, 704.0, 70.0)];
        _zoomClickStopsLabel.text = L(@"MIC_CONFIG_ZOOM_CLICK_STOPS");
        _zoomClickStopsLabel.textAlignment = NSTextAlignmentCenter;
        
        _zoomClickStopsLabel.isAccessibilityElement = YES;
        _zoomClickStopsLabel.accessibilityIdentifier = @"ZoomClickStopsLabel";
    }
    return _zoomClickStopsLabel;
}

- (CZGridView *)zoomClickStopCheckBoxGridView {
    if (_zoomClickStopCheckBoxGridView == nil) {
        CZGridRow *row = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = [NSArray cz_arrayWithRepeatedObject:row count:2];
        
        CZGridColumn *column = [[CZGridColumn alloc] initWithWidth:72.0 alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = [NSArray cz_arrayWithRepeatedObject:column count:10];
        
        _zoomClickStopCheckBoxGridView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0 + CZNosepiecePositionButtonSize + 70.0, 704.0, 70.0) rows:rows columns:columns];
        _zoomClickStopCheckBoxGridView.rowSpacing = 6.0;
    }
    return _zoomClickStopCheckBoxGridView;
}

- (UIStackView *)objectiveNumberStackView {
    if (_objectiveNumberStackView == nil) {
        _objectiveNumberStackView = [[UIStackView alloc] init];
        _objectiveNumberStackView.translatesAutoresizingMaskIntoConstraints = NO;
        _objectiveNumberStackView.axis = UILayoutConstraintAxisHorizontal;
        _objectiveNumberStackView.distribution = UIStackViewDistributionFill;
        _objectiveNumberStackView.alignment = UIStackViewAlignmentFill;
        _objectiveNumberStackView.spacing = 8.0;
    }
    return _objectiveNumberStackView;
}

- (UILabel *)objectiveNumberLabel {
    if (_objectiveNumberLabel == nil) {
        _objectiveNumberLabel = [[UILabel alloc] init];
        _objectiveNumberLabel.font = [UIFont cz_body1];
        _objectiveNumberLabel.text = L(@"MIC_CONFIG_OBJECTIVE_NUMBER");
        _objectiveNumberLabel.textColor = [UIColor cz_gs50];
        
        _objectiveNumberLabel.isAccessibilityElement = YES;
        _objectiveNumberLabel.accessibilityIdentifier = @"ObjectiveNumberLabel";
    }
    return _objectiveNumberLabel;
}

- (CZDropdownMenu *)objectiveNumberDropdown {
    if (_objectiveNumberDropdown == nil) {
        _objectiveNumberDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:nil imagePicker:nil expandTitles:@[@"4", @"5", @"6", @"7"]];
        _objectiveNumberDropdown.selectedItemTitle = @(self.microscopeModel.nosepiece.positions).stringValue;
        _objectiveNumberDropdown.animationDirection = CZDropdownAnimationDirectionUp;
        [_objectiveNumberDropdown addTarget:self action:@selector(objectiveNumberDropdownAction:) forControlEvents:UIControlEventValueChanged];
        
        _objectiveNumberDropdown.isAccessibilityElement = YES;
        _objectiveNumberDropdown.accessibilityIdentifier = @"ObjectiveNumberDropdown";
        
        [_objectiveNumberDropdown.widthAnchor constraintEqualToConstant:48.0].active = YES;
    }
    return _objectiveNumberDropdown;
}

#pragma mark - Actions

- (void)nosepiecePositionButtonAction:(CZButton *)button {
    NSInteger position = button.tag;
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        NSMutableArray *contents = [[NSMutableArray alloc] init];
        CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
        NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeStereoObjective];
        
        for (CZZoomLevel *zoomLevel in zoomLevels) {
            [contents addObject:zoomLevel.displayName];
        }
        
        CZZoomLevel *currentLevel = [self.microscopeModel zoomForType:kZoomTypeStereoObjective];
        
        CZPickerViewController *picker = [[CZPickerViewController alloc] initWithSelectedItem:currentLevel.displayName selectableItems:contents];
        picker.selectedItemTitleLabel.text = [NSString stringWithFormat:L(@"MIC_CONFIG_CURRENT_OBJECTIVE"), 0];
        picker.selectableItemsTitleLabel.text = L(@"MIC_CONFIG_AVAILABLE_OBJECTIVE");
        
        @weakify(self);
        [picker setHandler:^(CZPickerViewController *picker, CZDialogAction *action) {
            @strongify(self);
            CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
            NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeStereoObjective];
            if (picker.selectedItemIndex < zoomLevels.count) {
                CZZoomLevel *zoomLevel = zoomLevels[picker.selectedItemIndex];
                [self.microscopeModel setZoom:zoomLevel forType:kZoomTypeStereoObjective];
            }
            
            [self reloadNosepiecePositions];
        } forActionWithIdentifier:CZPickerViewControllerSaveActionIdentifier];
        
        [self presentViewController:picker animated:YES completion:nil];
    } else {
        NSArray<CZObjective *> *availableObjectives = self.microscopeModel.nosepiece.availableObjectives;
        CZObjective *selectedObjective = [self.microscopeModel.nosepiece objectiveAtPosition:position];
        
        CZPickerViewController<CZObjective *> *picker = [[CZPickerViewController alloc] initWithSelectedItem:selectedObjective selectableItems:availableObjectives];
        picker.selectedItemTitleLabel.text = [NSString stringWithFormat:L(@"MIC_CONFIG_CURRENT_OBJECTIVE"), position + 1];
        picker.selectableItemsTitleLabel.text = L(@"MIC_CONFIG_AVAILABLE_OBJECTIVE");
        
        picker.titleForItem = ^NSString *(CZPickerViewController<CZObjective *> *picker, CZObjective *item) {
            return item.displayName;
        };
        
        @weakify(self);
        [picker setHandler:^(CZPickerViewController<CZObjective *> *picker, CZDialogAction *action) {
            @strongify(self);
            [self.microscopeModel.nosepiece setObjective:picker.selectedItem atPosition:position];
            [self reloadData];
        } forActionWithIdentifier:CZPickerViewControllerSaveActionIdentifier];
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)zoomClickStopCheckBoxAction:(CZCheckBox *)checkBox {
    if (checkBox.isSelected) {
        checkBox.selected = NO;
        [self.microscopeModel removeZoomClickStopForID:checkBox.tag];
    } else {
        if (self.microscopeModel.zoomClickStopPositions >= 12) {
            CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:L(@"MIC_CONFIG_ZOOM_CLICK_STOPS_WARNING") level:CZAlertLevelError];
            [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
            [alert presentAnimated:YES completion:nil];
        } else {
            checkBox.selected = YES;
            [self.microscopeModel addZoomClickStopID:checkBox.tag];
        }
    }
}

- (void)objectiveNumberDropdownAction:(id)sender {
    self.microscopeModel.nosepiece.positions = self.objectiveNumberDropdown.selectedIndex + 4;
    [self reloadData];
}

@end
