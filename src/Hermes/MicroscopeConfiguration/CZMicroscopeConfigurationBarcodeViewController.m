//
//  CZMicroscopeConfigurationBarcodeViewController.m
//  Hermes
//
//  Created by Li, Junlin on 2/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeConfigurationBarcodeViewController.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@interface CZMicroscopeConfigurationBarcodeViewController ()

@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;
@property (nonatomic, copy) NSString *magicNumber;

@end

@implementation CZMicroscopeConfigurationBarcodeViewController

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel magicNumber:(NSString *)magicNumber {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _microscopeModel = microscopeModel;
        _magicNumber = [magicNumber copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:L(@"MIC_QR_VIEW_TITLE"), self.microscopeModel.name];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = kDefaultBackGroundColor;
    
    NSString *message = [self.magicNumber stringByAppendingString:[self.microscopeModel saveToJsonContent]];
    UIImage *logo = [UIImage imageNamed:A(@"zeiss-logo-blue")];
    CGImageRef logoWithoutEdge = CGImageCreateWithImageInRect(logo.CGImage, CGRectMake(125, 125, 250, 250));
    UIImage *qrCodeImage = [UIImage qrCodeImageWithMessage:message icon:[UIImage imageWithCGImage:logoWithoutEdge]];
    CGImageRelease(logoWithoutEdge);
    
    // create QR code image view
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = qrCodeImage;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.backgroundColor = kDefaultLabelColor;
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // create information label
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.textColor = kDefaultLabelColor;
    infoLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.numberOfLines = 0;
    infoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // create caption label
    UILabel *captionLabel = [[UILabel alloc] init];
    captionLabel.textColor = kDefaultLabelColor;
    captionLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    NSString *scanTip = [NSString stringWithFormat:L(@"MIC_CONFIG_QRCODE_SCANBY_TIP"), self.microscopeModel.name];
    captionLabel.text = scanTip;
    captionLabel.textAlignment = NSTextAlignmentCenter;
    captionLabel.numberOfLines = 0;
    captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    // generate html string
    NSString *bullet1 = [NSString stringWithFormat:L(@"MIC_QR_INFO_LABEL_BULLET_1"), [[UIApplication sharedApplication] cz_name]];
    NSString *bullet2 = [NSString stringWithFormat:L(@"MIC_QR_INFO_LABEL_BULLET_2"), [[UIApplication sharedApplication] cz_name], [[UIApplication sharedApplication] cz_name]];
    NSString *bullet3 = [NSString stringWithFormat:L(@"MIC_QR_INFO_LABEL_BULLET_3"), [[UIApplication sharedApplication] cz_name]];
    NSArray *bulletPoints = @[bullet1,
                              bullet2,
                              bullet3];
    
    NSMutableString *bulletPointsHtml = [[NSMutableString alloc] init];
    if (bulletPoints.count > 0) {
        [bulletPointsHtml appendString:@"<ul>"];
        for (id bulletPoint in bulletPoints) {
            [bulletPointsHtml appendString:[NSString stringWithFormat:@"<li>%@</li>", bulletPoint]];
        }
        [bulletPointsHtml appendString:@"</ul>"];
    }
    
    NSString *introductoryText = [NSString stringWithFormat:L(@"MIC_QR_INFO_LABEL_HEAD"), [[UIApplication sharedApplication] cz_name]];;
    
    NSString *colorString = @"rgb(203,206,212)";
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head>\
                            <style type=\"text/css\">body{font-family:HelveticaNeue;font-size:16px;background-color:transparent;color:%@}</style>\
                            </head><body><h3>%@</h3>%@</body></html>", colorString, introductoryText, bulletPointsHtml];
    
    // assign attributed string to label
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                 documentAttributes:nil
                                                                              error:NULL];
    infoLabel.attributedText = attributedString;
    
    [self.view addSubview:imageView];
    [self.view addSubview:infoLabel];
    [self.view addSubview:captionLabel];
    
    // generate auto layout constraints
    NSDictionary *views = NSDictionaryOfVariableBindings(imageView, infoLabel, captionLabel);
    NSDictionary *metrics = @{@"padding":@15, @"edge":@32, @"infoImageGap": @50};
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:imageView
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1
                                                          constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:captionLabel
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationLessThanOrEqual
                                                            toItem:imageView
                                                         attribute:NSLayoutAttributeWidth
                                                        multiplier:1
                                                          constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:captionLabel
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationLessThanOrEqual
                                                            toItem:imageView
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1
                                                          constant:0]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-edge-[infoLabel]-infoImageGap-[imageView(==500)]-edge-|"
                                                                     options:0
                                                                     metrics:metrics
                                                                       views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-edge-[infoLabel]"
                                                                     options:0
                                                                     metrics:metrics
                                                                       views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-edge-[imageView]-padding-[captionLabel]"
                                                                     options:0
                                                                     metrics:metrics
                                                                       views:views]];
}

@end
