//
//  CZMicroscopeModelSelectionViewCell.m
//  Hermes
//
//  Created by Li, Junlin on 2/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModelSelectionViewCell.h"

static const CGFloat kMicroscopeModelNameLabelXPadding = 8.0;
static const CGFloat kMicroscopeModelNameLabelYPadding = 3.0;
static const CGFloat kMicroscopeModelNameLabelLineHeight = 20.0;
static const CGFloat kSelectionIndicatorViewHeight = 8.0;

@interface CZMicroscopeModelSelectionViewCell ()

@property (nonatomic, readonly, strong) UIView *selectionIndicatorView;

@end

@implementation CZMicroscopeModelSelectionViewCell

@synthesize microscopeModelImageView = _microscopeModelImageView;
@synthesize microscopeModelNameLabel = _microscopeModelNameLabel;
@synthesize selectionIndicatorView = _selectionIndicatorView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.microscopeModelImageView];
        [self.contentView addSubview:self.microscopeModelNameLabel];
        [self.contentView addSubview:self.selectionIndicatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect microscopeModelNameLabelFrame = self.microscopeModelNameLabel.frame;
    microscopeModelNameLabelFrame.size.height = kMicroscopeModelNameLabelLineHeight * self.microscopeModelNameLabel.numberOfLines;
    microscopeModelNameLabelFrame.origin.y = self.contentView.bounds.size.height - microscopeModelNameLabelFrame.size.height - kMicroscopeModelNameLabelYPadding;
    self.microscopeModelNameLabel.frame = microscopeModelNameLabelFrame;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.selectionIndicatorView.hidden = !selected;
}

- (UIImageView *)microscopeModelImageView {
    if (_microscopeModelImageView == nil) {
        _microscopeModelImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _microscopeModelImageView.backgroundColor = [UIColor clearColor];
    }
    return _microscopeModelImageView;
}

- (UILabel *)microscopeModelNameLabel {
    if (_microscopeModelNameLabel == nil) {
        CGRect frame = CGRectMake(kMicroscopeModelNameLabelXPadding,
                                  self.contentView.bounds.size.height - kMicroscopeModelNameLabelLineHeight - kMicroscopeModelNameLabelYPadding,
                                  self.contentView.bounds.size.width - 2 * kMicroscopeModelNameLabelXPadding,
                                  kMicroscopeModelNameLabelLineHeight);
        _microscopeModelNameLabel = [[UILabel alloc] initWithFrame:frame];
        _microscopeModelNameLabel.backgroundColor = [UIColor clearColor];
        _microscopeModelNameLabel.font = [UIFont cz_body1];
        _microscopeModelNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _microscopeModelNameLabel.textAlignment = NSTextAlignmentCenter;
        _microscopeModelNameLabel.textColor = [UIColor cz_gs120];
    }
    return _microscopeModelNameLabel;
}

- (UIView *)selectionIndicatorView {
    if (_selectionIndicatorView == nil) {
        _selectionIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.contentView.bounds.size.width, kSelectionIndicatorViewHeight)];
        _selectionIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _selectionIndicatorView.backgroundColor = [UIColor cz_pb100];
        _selectionIndicatorView.hidden = YES;
    }
    return _selectionIndicatorView;
}

@end
