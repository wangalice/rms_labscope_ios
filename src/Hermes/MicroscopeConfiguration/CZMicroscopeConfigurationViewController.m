//
//  CZMicroscopeConfigurationViewController.m
//  Hermes
//
//  Created by Mike Wang on 3/13/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeConfigurationViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import <ReportFramework/CZGlobal.h>
#import <CZCameraInterface/CZCameraInterface.h>
#import <CZMicroscopeManager/CZMicroscopeManager.h>

#import "CZMicroscopeModelSelectionViewCell.h"
#import "CZAccessabilityHelper.h"
#import "CZAppDelegate.h"
#import "CZCameraView.h"
#import "CZHelpOverlayView.h"
#import "CZLiveViewController.h"
#import "CZMicroscopeAutoScaleController.h"
#import "CZShadingCorrectionSetupViewController.h"
#import "CZQRCodeScannerViewController.h"
#import "CZUIDefinition.h"
#import "CZWebBrowserViewController.h"
#import "CZButtonGroup.h"
#import "CZGridView.h"
#import "CZTabViewController.h"
#import "CZNosepieceConfigurationViewController.h"
#import "CZReflectorConfigurationViewController.h"
#import "CZMicroscopeConfigurationBarcodeViewController.h"
#import "CZUppercaseLabel.h"
#import "CZPickerViewController.h"
#import "CZMicroscopeNameValidator.h"
#import "CZAlertController.h"

#if REPORT_REFACTORED
#import "CZScalingReportManager.h"
#import "CZReportEditViewController.h"
#endif

#if DCM_REFACTORED
//DCM Dependence
#import "CZDCMManager.h"
#import "CZDCMManager+DefaultSettings.h"
#import "CZFileListManager.h"
#endif

#define kUIButtonGenenalPadding 20.0
#define kMNAInfoRetrievingInterval 2.0

static const CGFloat kMicroscopeModelSelectionViewWidth = 192.0;
static const CGFloat kMicroscopeModelSelectionViewLineSpacing = 8.0;
static const CGFloat kToolbarHeight = 48.0;
static const NSInteger kUnlockConfigurationButtonIndex = 0;
static const NSInteger kLockConfigurationButtonIndex = 1;

static NSString * const kStemi508MNAMacAddressPrefix = @"00:20:0D:F9:6";
static NSString * const kQRCodeMagicNumber = @"mna";

@interface CZMicroscopeConfigurationViewController () <UICollectionViewDataSource, UICollectionViewDelegate, CZTextFieldDelegate, CZMicroscopeAutoScaleControllerDelegate, CZMicroscopeModelUpdateInfoFromMNADelegate, CZMNAParingManagerDelegate, CZQRCodeScannerViewControllerDelegate>

// Data related
@property (atomic, strong) CZCameraControlLock *cameraControlLock;
@property (nonatomic, readwrite, copy) CZMicroscopeModel *microscopeModel;
@property (nonatomic, copy) CZMicroscopeModel *originalMicroscopeModel;
@property (nonatomic, strong) NSMutableDictionary *cachedMicroscopeModels;
@property (nonatomic, copy) NSArray<NSNumber *> *supportedMicroscopeModelTypes;
@property (atomic, copy) CZMNA *pairedMNA;
@property (nonatomic, strong, readonly) CZMNAParingManager *mnaPairingManager;
@property (nonatomic, copy) NSArray<CZMNA *> *mnaList;
@property (nonatomic, copy) NSDictionary<NSString *, CZMicroscopeModel *> *pairedModelList;
@property (nonatomic, copy) NSString *lockPIN;
@property (nonatomic, assign) BOOL shouldRememberPIN;
@property (nonatomic, assign) BOOL shouldCompareDataWithMNA;
@property (nonatomic, assign) CZPrimoStarPackage package;
@property (nonatomic, weak) CZAlertController *pinInvalidAlert;

// UI elements and controllers

@property (nonatomic, strong, readonly) UICollectionView *microscopeModelSelectionView;
@property (nonatomic, strong, readonly) UIScrollView *microscopeModelDetailView;
@property (nonatomic, strong, readonly) UILabel *selectModelLabel;

@property (nonatomic, strong, readonly) CZGridView *basicConfigurationView;
@property (nonatomic, strong, readonly) CZUppercaseLabel *lockConfigurationLabel;
@property (nonatomic, strong, readonly) CZToggleButton *lockConfigurationToggleButton;
@property (nonatomic, strong, readonly) CZUppercaseLabel *pinLabel;
@property (nonatomic, strong, readonly) CZTextField *pinTextField;
@property (nonatomic, strong, readonly) CZUppercaseLabel *confirmPinLabel;
@property (nonatomic, strong, readonly) CZTextField *confirmPinTextField;
@property (nonatomic, strong, readonly) CZUppercaseLabel *microscopeNameLabel;
@property (nonatomic, strong, readonly) CZTextField *microscopeNameTextField;
@property (nonatomic, strong, readonly) CZMicroscopeNameValidator *microscopeNameValidator;
@property (nonatomic, strong, readonly) CZUppercaseLabel *shadingCorrectionLabel;
@property (nonatomic, strong, readonly) CZButton *shadingCorrectionSetupButton;
@property (nonatomic, strong, readonly) CZUppercaseLabel *scalingCalibrationLabel;
@property (nonatomic, strong, readonly) CZButton *scalingCalibrationSetupButton;
@property (nonatomic, strong, readonly) CZUppercaseLabel *cameraAdapterLabel;
@property (nonatomic, strong, readonly) CZDropdownMenu *cameraAdapterDropdown;
@property (nonatomic, strong, readonly) CZUppercaseLabel *networkAdapterLabel;
@property (nonatomic, strong, readonly) CZButton *networkAdapterButton;

@property (nonatomic, strong, readonly) CZGridView *packageSelectionView;
@property (nonatomic, strong, readonly) CZUppercaseLabel *selectPackageLabel;
@property (nonatomic, strong, readonly) UILabel *selectPackageHintLabel;
@property (nonatomic, strong, readonly) CZDropdownMenu *selectPackageDropdown;

@property (nonatomic, strong, readonly) UIActivityIndicatorView *mnaSearchingIndicator;

@property (nonatomic, strong, readonly) CZTabViewController *selectionTabViewController;
@property (nonatomic, strong, readonly) CZNosepieceConfigurationViewController *nosepieceConfigurationViewController;
@property (nonatomic, strong, readonly) CZReflectorConfigurationViewController *reflectorConfigurationViewController;
@property (nonatomic, strong) CZQRCodeScannerViewController *qrCodeScanner;

@property (nonatomic, strong, readonly) CZToolbar *toolbar;
@property (nonatomic, strong, readonly) CZToolbarItem *showBarcodeItem;
@property (nonatomic, strong, readonly) CZToolbarItem *onlyUseThisMicroscopeItem;
@property (nonatomic, strong, readonly) CZToolbarItem *productWebsiteItem;
@property (nonatomic, strong, readonly) CZToolbarItem *onlineShopItem;
@property (nonatomic, strong, readonly) CZToolbarItem *scalingReportItem;

// Misc

@property (nonatomic, strong) NSTimer *mnaInfoRetrievingTimer;

@end

@implementation CZMicroscopeConfigurationViewController

// Synthesize lazy-load objects
@synthesize mnaPairingManager = _mnaPairingManager;
@synthesize microscopeModelSelectionView = _microscopeModelSelectionView;
@synthesize microscopeModelDetailView = _microscopeModelDetailView;
@synthesize selectModelLabel = _selectModelLabel;

@synthesize basicConfigurationView = _basicConfigurationView;
@synthesize lockConfigurationLabel = _lockConfigurationLabel;
@synthesize lockConfigurationToggleButton = _lockConfigurationToggleButton;
@synthesize pinLabel = _pinLabel;
@synthesize pinTextField = _pinTextField;
@synthesize confirmPinLabel = _confirmPinLabel;
@synthesize confirmPinTextField = _confirmPinTextField;
@synthesize microscopeNameLabel = _microscopeNameLabel;
@synthesize microscopeNameTextField = _microscopeNameTextField;
@synthesize microscopeNameValidator = _microscopeNameValidator;
@synthesize shadingCorrectionLabel = _shadingCorrectionLabel;
@synthesize shadingCorrectionSetupButton = _shadingCorrectionSetupButton;
@synthesize scalingCalibrationLabel = _scalingCalibrationLabel;
@synthesize scalingCalibrationSetupButton = _scalingCalibrationSetupButton;
@synthesize cameraAdapterLabel = _cameraAdapterLabel;
@synthesize cameraAdapterDropdown = _cameraAdapterDropdown;
@synthesize networkAdapterLabel = _networkAdapterLabel;
@synthesize networkAdapterButton = _networkAdapterButton;

@synthesize packageSelectionView = _packageSelectionView;
@synthesize selectPackageLabel = _selectPackageLabel;
@synthesize selectPackageHintLabel = _selectPackageHintLabel;
@synthesize selectPackageDropdown = _selectPackageDropdown;

@synthesize mnaSearchingIndicator = _mnaSearchingIndicator;

@synthesize selectionTabViewController = _selectionTabViewController;
@synthesize nosepieceConfigurationViewController = _nosepieceConfigurationViewController;
@synthesize reflectorConfigurationViewController = _reflectorConfigurationViewController;

@synthesize toolbar = _toolbar;
@synthesize showBarcodeItem = _showBarcodeItem;
@synthesize onlyUseThisMicroscopeItem = _onlyUseThisMicroscopeItem;
@synthesize productWebsiteItem = _productWebsiteItem;
@synthesize onlineShopItem = _onlineShopItem;
@synthesize scalingReportItem = _scalingReportItem;

#pragma mark - View controller lifecycle

- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel isReadOnlyMode:(BOOL)isReadOnlyMode {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        NSAssert(camera != nil, @"Camera is nil, please initialize it before show microscope configure view");
        _camera = camera;
        
        _microscopeModel = [microscopeModel copy];
        if (_microscopeModel == nil) {
            _microscopeModel = [CZMicroscopeModel newMicroscopeModelWithCamera:_camera];
        }
        
        _isReadOnlyMode = isReadOnlyMode;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nosepieceDidUpdate:) name:CZNosepieceDidUpdateNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomKnobDidUpdate:) name:CZZoomKnobDidUpdateNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    _mnaPairingManager.delegate = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZNosepieceDidUpdateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CZZoomKnobDidUpdateNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cz_gs105];
    
    self.navigationItem.title = L(@"MIC_CONFIG_TITLE");
    
    CZBarButtonItem *applyButtonItem = [[CZBarButtonItem alloc] initWithTitle:L(@"APPLY") target:self action:@selector(applyButtonAction:)];
    applyButtonItem.isAccessibilityElement = YES;
    applyButtonItem.accessibilityIdentifier = @"ApplyButton";
    self.navigationItem.rightBarButtonItems = @[applyButtonItem];
    
    [self.view addSubview:self.microscopeModelSelectionView];
    [self.view addSubview:self.microscopeModelDetailView];
    
    [self.microscopeModelDetailView addSubview:self.basicConfigurationView];
    
#if MICCONFIG_REFACTORED
    [self.basicConfigurationView addContentView:self.lockConfigurationLabel atRowIndex:0 columnIndex:2];
    [self.basicConfigurationView addContentView:self.lockConfigurationToggleButton atRowIndex:0 columnIndex:3];
    [self.basicConfigurationView addContentView:self.pinLabel atRowIndex:1 columnIndex:2];
    [self.basicConfigurationView addContentView:self.pinTextField atRowIndex:1 columnIndex:3];
    [self.basicConfigurationView addContentView:self.confirmPinLabel atRowIndex:2 columnIndex:2];
    [self.basicConfigurationView addContentView:self.confirmPinTextField atRowIndex:2 columnIndex:3];
#endif
    
    [self.basicConfigurationView addContentView:self.microscopeNameLabel atRowIndex:0 columnIndex:0];
    [self.basicConfigurationView addContentView:self.microscopeNameTextField atRowIndex:0 columnIndex:1];
    [self.basicConfigurationView addContentView:self.shadingCorrectionLabel atRowIndex:1 columnIndex:0];
    [self.basicConfigurationView addContentView:self.shadingCorrectionSetupButton atRowIndex:1 columnIndex:1];
    
#if MICCONFIG_REFACTORED
    [self.basicConfigurationView addContentView:self.scalingCalibrationLabel atRowIndex:2 columnIndex:0];
    [self.basicConfigurationView addContentView:self.scalingCalibrationSetupButton atRowIndex:2 columnIndex:1];
    [self.basicConfigurationView addContentView:self.cameraAdapterLabel atRowIndex:3 columnIndex:0];
    [self.basicConfigurationView addContentView:self.cameraAdapterDropdown atRowIndex:3 columnIndex:1];
    [self.basicConfigurationView addContentView:self.networkAdapterLabel atRowIndex:3 columnIndex:0];
    [self.basicConfigurationView addContentView:self.networkAdapterButton atRowIndex:3 columnIndex:1];
#else
    [self.basicConfigurationView addContentView:self.cameraAdapterLabel atRowIndex:2 columnIndex:0];
    [self.basicConfigurationView addContentView:self.cameraAdapterDropdown atRowIndex:2 columnIndex:1];
    [self.basicConfigurationView addContentView:self.networkAdapterLabel atRowIndex:2 columnIndex:0];
    [self.basicConfigurationView addContentView:self.networkAdapterButton atRowIndex:2 columnIndex:1];
#endif
    
    [self.microscopeModelDetailView addSubview:self.packageSelectionView];
    [self.packageSelectionView addContentView:self.selectPackageLabel atRowIndex:0 columnIndex:0];
    [self.packageSelectionView addContentView:self.selectPackageHintLabel atRowIndex:1 columnIndex:1];
    [self.packageSelectionView addContentView:self.selectPackageDropdown atRowIndex:0 columnIndex:1];
    
    self.selectModelLabel.hidden = YES;
    CGRect frame = self.selectModelLabel.frame;
    frame.origin.x = self.microscopeModelDetailView.frame.origin.x + kUIButtonGenenalPadding;
    frame.origin.y = self.microscopeModelDetailView.frame.origin.y;
    self.selectModelLabel.frame = frame;
    [self.view addSubview:self.selectModelLabel];
    
    [self updateLockConfigControlStatus];
    
    [self addChildViewController:self.selectionTabViewController];
    self.selectionTabViewController.view.frame = CGRectMake(32.0, 320.0, 768.0, 336.0);
    self.selectionTabViewController.view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.microscopeModelDetailView addSubview:self.selectionTabViewController.view];
    [self.selectionTabViewController didMoveToParentViewController:self];
    
#if MICCONFIG_REFACTORED
    [self.view addSubview:self.toolbar];
    self.toolbar.items = @[
        self.showBarcodeItem,
        [CZToolbarItem fixedSpaceItemWithWidth:16.0],
        self.onlyUseThisMicroscopeItem,
        [CZToolbarItem flexibleSpaceItem],
        self.scalingReportItem,
        self.onlineShopItem,
        self.productWebsiteItem,
        [CZToolbarItem fixedSpaceItemWithWidth:8.0]
    ];
    
    self.onlyUseThisMicroscopeItem.hidden = YES;
    self.productWebsiteItem.hidden = YES;
    self.onlineShopItem.hidden = YES;
    self.scalingReportItem.hidden = YES;
#endif
    
    self.shouldCompareDataWithMNA = YES;
    
    _cachedMicroscopeModels = [[NSMutableDictionary alloc] init];
    
    [self.microscopeModel loadSlotsFromDefaults];
    self.originalMicroscopeModel = self.microscopeModel;
    [self refreshMicrosopeModelList];
    [self updateWithCurrentMicroscopeModel];
    CZMicroscopeTypes type = self.microscopeModel.type;
    if (type != kUnknownModelType && !self.isReadOnlyMode) {
        [self showHelpOverlay];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
#if MICCONFIG_REFACTORED
    CZAppDelegate *delegate = [CZAppDelegate get];
    [delegate.tabBar updateLaserPointerWithControllerIndex:kCZMicroscopeController];
#endif
    
#if DCM_REFACTORED
    //FIXME: CM is Connection, MIC_CONFIG_SINGLE_MICROSCOPE is not available
    if (![CZDCMManager defaultManager].hasConnectedToDCMServer) {
        _defaultToggleButton.enabled = YES;
        _defaultToggleButton.alpha = 1.0;
    } else {
        _defaultToggleButton.enabled = NO;
        _defaultToggleButton.alpha = 0.5;
    }
    
    [self updatePINStateWithDCMConnectedState:[CZDCMManager defaultManager].hasConnectedToDCMServer];
#endif
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.microscopeModel.isPairedWithMNA) {
        [self.mnaInfoRetrievingTimer invalidate];
        self.mnaInfoRetrievingTimer = [NSTimer scheduledTimerWithTimeInterval:kMNAInfoRetrievingInterval
                                                                       target:self
                                                                     selector:@selector(mnaInfoRetrievingTimerHandler:)
                                                                     userInfo:nil
                                                                      repeats:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[CZHelpOverlayView current] dismiss];
    
    [self.mnaInfoRetrievingTimer invalidate];
    self.mnaInfoRetrievingTimer = nil;
    
    [self.mnaSearchingIndicator stopAnimating];
    
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark - Prepare & Finalize Configuration

- (void)prepareForConfigurationWithCompletionHandler:(void (^)(BOOL))completionHandler {
    @weakify(self);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @strongify(self);
        BOOL prefetched = [self.camera prefetchRequiredCameraInfo];
        
        // Get the control access from camera.
        if ([self.camera hasCapability:CZCameraCapabilityEncoding]) {
            CZCameraControlLock *cameraLock = [[CZCameraControlLock alloc] initWithType:CZCameraControlTypeConfiguration camera:self.camera retained:YES];
            self.cameraControlLock = cameraLock;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self);
            if ([self.camera isKindOfClass:[CZGeminiCamera class]] && [self.camera hasCapability:CZCameraCapabilityEncoding]) {
                [self.camera beginEventListening];
                [self.microscopeModel updateDataFromCamera:self.camera];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(geminiCameraStatusDidChange:) name:CZGeminiCameraStatusDidChangeNotification object:self.camera];
            }
            
            BOOL prepared = [self.camera hasCapability:CZCameraCapabilityEncoding] ? (prefetched && (self.cameraControlLock != nil)) : (prefetched);
            if (completionHandler) {
                completionHandler(prepared);
            }
        });
    });
}

- (void)finalizeConfiguration {
    if ([self.camera isKindOfClass:[CZGeminiCamera class]] && [self.camera hasCapability:CZCameraCapabilityEncoding]) {
        [self.camera endEventListening];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:CZGeminiCameraStatusDidChangeNotification object:self.camera];
    }
    
    self.cameraControlLock = nil;
}

#pragma mark - Lazy-load objects

- (CZMNAParingManager *)mnaPairingManager {
    if (_mnaPairingManager == nil) {
        _mnaPairingManager = [[CZMNAParingManager alloc] init];
    }
    _mnaPairingManager.delegate = self;
    
    return _mnaPairingManager;
}

- (UICollectionView *)microscopeModelSelectionView {
    if (_microscopeModelSelectionView == nil) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = kMicroscopeModelSelectionViewLineSpacing;
        layout.minimumInteritemSpacing = 0.0;
        layout.itemSize = CGSizeMake(kMicroscopeModelSelectionViewWidth, kMicroscopeModelSelectionViewWidth);
        
        _microscopeModelSelectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0, 0.0, kMicroscopeModelSelectionViewWidth, self.view.bounds.size.height - kToolbarHeight) collectionViewLayout:layout];
        _microscopeModelSelectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
        _microscopeModelSelectionView.backgroundColor = [UIColor clearColor];
        _microscopeModelSelectionView.showsVerticalScrollIndicator = NO;
        _microscopeModelSelectionView.dataSource = self;
        _microscopeModelSelectionView.delegate = self;
        [_microscopeModelSelectionView registerClass:[CZMicroscopeModelSelectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([CZMicroscopeModelSelectionViewCell class])];
    }
    return _microscopeModelSelectionView;
}

- (UIScrollView *)microscopeModelDetailView {
    if (_microscopeModelDetailView == nil) {
        _microscopeModelDetailView = [[UIScrollView alloc] initWithFrame:CGRectMake(kMicroscopeModelSelectionViewWidth, 0.0, self.view.bounds.size.width - kMicroscopeModelSelectionViewWidth, self.view.bounds.size.height - kToolbarHeight)];
        _microscopeModelDetailView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _microscopeModelDetailView.backgroundColor = [UIColor clearColor];
        _microscopeModelDetailView.scrollEnabled = NO;
        _microscopeModelDetailView.hidden = YES;
        _microscopeModelDetailView.contentSize = _microscopeModelDetailView.frame.size;
    }
    return _microscopeModelDetailView;
}

- (UILabel *)selectModelLabel {
    if (_selectModelLabel == nil) {
        _selectModelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 100)];
        _selectModelLabel.backgroundColor = [UIColor clearColor];
        _selectModelLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        _selectModelLabel.text = L(@"MIC_CONFIG_SELECT_MODEL_MESSAGE");
        _selectModelLabel.textColor = kDefaultLabelColor;
        _selectModelLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _selectModelLabel;
}

- (CZGridView *)basicConfigurationView {
    if (_basicConfigurationView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row2 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row3 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = @[row0, row1, row2, row3];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:120.0 alignment:CZGridColumnAlignmentLeading];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:240.0 alignment:CZGridColumnAlignmentFill];
        CZGridColumn *column2 = [[CZGridColumn alloc] initWithWidth:120.0 alignment:CZGridColumnAlignmentLeading];
        CZGridColumn *column3 = [[CZGridColumn alloc] initWithWidth:240.0 alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = @[column0, column1, column2, column3];
        
        _basicConfigurationView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0, 768.0, 256.0) rows:rows columns:columns];
        _basicConfigurationView.rowSpacing = 16.0;
        _basicConfigurationView.columnSpacing = 8.0;
        [_basicConfigurationView setCustomSpacing:32.0 afterColumnAtIndex:1];
    }
    return _basicConfigurationView;
}

- (CZUppercaseLabel *)lockConfigurationLabel {
    if (_lockConfigurationLabel == nil) {
        _lockConfigurationLabel = [[CZUppercaseLabel alloc] init];
        _lockConfigurationLabel.text = L(@"MIC_CONFIG_LOCK_SWITCH_LABEL");
        
        _lockConfigurationLabel.isAccessibilityElement = YES;
        _lockConfigurationLabel.accessibilityIdentifier = @"LockConfigurationLabel";
    }
    return _lockConfigurationLabel;
}

- (CZToggleButton *)lockConfigurationToggleButton {
    if (_lockConfigurationToggleButton == nil) {
        _lockConfigurationToggleButton = [[CZToggleButton alloc] initWithItems:@[L(@"MIC_CONFIG_LOCK_TOGGLE_UNLOCK"), L(@"MIC_CONFIG_LOCK_TOGGLE_LOCK")]];
        _lockConfigurationToggleButton.selectedIndex = kUnlockConfigurationButtonIndex;
        [_lockConfigurationToggleButton addTarget:self
                                           action:@selector(lockConfigurationToggleButtonAction:)
                                 forControlEvents:UIControlEventValueChanged];
        
        _lockConfigurationToggleButton.isAccessibilityElement = YES;
        _lockConfigurationToggleButton.accessibilityIdentifier = @"LockConfigurationToggleButton";
    }
    return _lockConfigurationToggleButton;
}

- (CZUppercaseLabel *)pinLabel {
    if (_pinLabel == nil) {
        _pinLabel = [[CZUppercaseLabel alloc] init];
        _pinLabel.text = L(@"MIC_CONFIG_INPUT_PIN_LABEL_1");
        
        _pinLabel.isAccessibilityElement = YES;
        _pinLabel.accessibilityIdentifier = @"PINLabel";
    }
    return _pinLabel;
}

- (CZTextField *)pinTextField {
    if (_pinTextField == nil) {
        _pinTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightImagePicker:nil];
        _pinTextField.secureTextEntry = YES;
        _pinTextField.delegate = self;
        _pinTextField.keyboardType = UIKeyboardTypeNumberPad;
        
        _pinTextField.isAccessibilityElement = YES;
        _pinTextField.accessibilityIdentifier = @"PINTextBox";
    }
    return _pinTextField;
}

- (CZUppercaseLabel *)confirmPinLabel {
    if (_confirmPinLabel == nil) {
        _confirmPinLabel = [[CZUppercaseLabel alloc] init];
        _confirmPinLabel.text = L(@"MIC_CONFIG_REINPUT_PIN_LABEL");
        
        _confirmPinLabel.isAccessibilityElement = YES;
        _confirmPinLabel.accessibilityIdentifier = @"ConfirmPINLabel";
    }
    return _confirmPinLabel;
}

- (CZTextField *)confirmPinTextField {
    if (_confirmPinTextField == nil) {
        _confirmPinTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightImagePicker:nil];
        _confirmPinTextField.secureTextEntry = YES;
        _confirmPinTextField.delegate = self;
        _confirmPinTextField.keyboardType = UIKeyboardTypeNumberPad;
        
        _confirmPinTextField.isAccessibilityElement = YES;
        _confirmPinTextField.accessibilityIdentifier = @"ConfirmPINTextBox";
    }
    return _confirmPinTextField;
}

- (CZUppercaseLabel *)microscopeNameLabel {
    if (_microscopeNameLabel == nil) {
        _microscopeNameLabel = [[CZUppercaseLabel alloc] init];
        _microscopeNameLabel.text = L(@"MIC_CONFIG_NAME");
        
        _microscopeNameLabel.isAccessibilityElement = YES;
        _microscopeNameLabel.accessibilityIdentifier = @"MicroscopeNameLabel";
    }
    return _microscopeNameLabel;
}

- (CZTextField *)microscopeNameTextField {
    if (_microscopeNameTextField == nil) {
        _microscopeNameTextField = [[CZTextField alloc] initWithStyle:CZTextFieldStyleDefault unit:nil rightImagePicker:nil];
        _microscopeNameTextField.delegate = self;
        _microscopeNameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
        _microscopeNameTextField.isAccessibilityElement = YES;
        _microscopeNameTextField.accessibilityIdentifier = @"MicroscopeNameTextBox";
    }
    return _microscopeNameTextField;
}

- (CZMicroscopeNameValidator *)microscopeNameValidator {
    if (_microscopeNameValidator == nil) {
        _microscopeNameValidator = [[CZMicroscopeNameValidator alloc] initWithMaxSupportedNameLength:[self.camera maxSupportedNameLength]];
    }
    return _microscopeNameValidator;
}

- (CZUppercaseLabel *)shadingCorrectionLabel {
    if (_shadingCorrectionLabel == nil) {
        _shadingCorrectionLabel = [[CZUppercaseLabel alloc] init];
        _shadingCorrectionLabel.text = L(@"MIC_SHADING_CORRECTION_BUTTON_LABEL");
        
        _shadingCorrectionLabel.isAccessibilityElement = YES;
        _shadingCorrectionLabel.accessibilityIdentifier = @"ShadingCorrectionLabel";
    }
    return _shadingCorrectionLabel;
}

- (CZButton *)shadingCorrectionSetupButton {
    if (_shadingCorrectionSetupButton == nil) {
        _shadingCorrectionSetupButton = [[CZButton alloc] init];
        [_shadingCorrectionSetupButton setTitle:L(@"SETUP") forState:UIControlStateNormal];
        [_shadingCorrectionSetupButton addTarget:self
                                          action:@selector(shadingCorrectionSetupButtonAction:)
                                forControlEvents:UIControlEventTouchUpInside];
        
        if ([self.camera hasCapability:CZCameraCapabilityEncoding]) {
            CZObjective *currentObjective = [self.microscopeModel.nosepiece objectiveAtCurrentPosition];
            _shadingCorrectionSetupButton.enabled = currentObjective != nil && !currentObjective.isNone;
        } else if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
            _shadingCorrectionSetupButton.enabled = self.microscopeModel.zoomClickStopPositions > 0;
        } else {
            _shadingCorrectionSetupButton.enabled = [self.microscopeModel hasObjectivesAssigned];
        }
        
        _shadingCorrectionSetupButton.isAccessibilityElement = YES;
        _shadingCorrectionSetupButton.accessibilityIdentifier = @"ShadingCorrectionSetupButton";
    }
    return _shadingCorrectionSetupButton;
}

- (CZUppercaseLabel *)scalingCalibrationLabel {
    if (_scalingCalibrationLabel == nil) {
        _scalingCalibrationLabel = [[CZUppercaseLabel alloc] init];
        _scalingCalibrationLabel.text = L(@"MIC_CONFIG_SCALING_CALIBRATION");
        
        _scalingCalibrationLabel.isAccessibilityElement = YES;
        _scalingCalibrationLabel.accessibilityIdentifier = @"ScalingCalibrationLabel";
    }
    return _scalingCalibrationLabel;
}

- (CZButton *)scalingCalibrationSetupButton {
    if (_scalingCalibrationSetupButton == nil) {
        _scalingCalibrationSetupButton = [[CZButton alloc] init];
        [_scalingCalibrationSetupButton setTitle:L(@"SETUP") forState:UIControlStateNormal];
        [_scalingCalibrationSetupButton addTarget:self
                                           action:@selector(scalingCalibrationSetupButtonAction:)
                                 forControlEvents:UIControlEventTouchUpInside];
        
        _scalingCalibrationSetupButton.isAccessibilityElement = YES;
        _scalingCalibrationSetupButton.accessibilityIdentifier = @"ScalingCalibrationSetupButton";
    }
    return _scalingCalibrationSetupButton;
}

- (CZUppercaseLabel *)cameraAdapterLabel {
    if (_cameraAdapterLabel == nil) {
        _cameraAdapterLabel = [[CZUppercaseLabel alloc] init];
        _cameraAdapterLabel.text = L(@"MIC_CONFIG_CAMERA_ADAPTER");
        
        _cameraAdapterLabel.isAccessibilityElement = YES;
        _cameraAdapterLabel.accessibilityIdentifier = @"CameraAdapterLabel";
    }
    return _cameraAdapterLabel;
}

- (CZDropdownMenu *)cameraAdapterDropdown {
    if (_cameraAdapterDropdown == nil) {
        NSMutableArray<CZDropdownItem *> *items = [NSMutableArray array];
        CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
        NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeCameraAdapter];
        
        for (CZZoomLevel *zoomLevel in zoomLevels) {
            CZDropdownItem *item = [CZDropdownItem itemWithTitle:zoomLevel.displayName];
            [items addObject:item];
        }
        
        CZZoomLevel *currentLevel = [self.microscopeModel zoomForType:kZoomTypeCameraAdapter];
        
        _cameraAdapterDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:L(@"CHOOSE") imagePicker:nil expandItems:items];
        _cameraAdapterDropdown.selectedItemTitle = currentLevel.displayName;
        [_cameraAdapterDropdown addTarget:self
                                   action:@selector(cameraAdapterDropdownAction:)
                         forControlEvents:UIControlEventValueChanged];
        
        _cameraAdapterDropdown.isAccessibilityElement = YES;
        _cameraAdapterDropdown.accessibilityIdentifier = @"CameraAdapterDropdown";
    }
    return _cameraAdapterDropdown;
}

- (CZUppercaseLabel *)networkAdapterLabel {
    if (_networkAdapterLabel == nil) {
        _networkAdapterLabel = [[CZUppercaseLabel alloc] init];
        _networkAdapterLabel.text = L(@"MIC_CONFIG_MNA");
        
        _networkAdapterLabel.isAccessibilityElement = YES;
        _networkAdapterLabel.accessibilityIdentifier = @"NetworkAdapterLabel";
    }
    return _networkAdapterLabel;
}

- (CZButton *)networkAdapterButton {
    if (_networkAdapterButton == nil) {
        _networkAdapterButton = [[CZButton alloc] init];
        _networkAdapterButton.enabled = NO;
        [_networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-pending-icon")] forState:UIControlStateNormal];
        [_networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-not-found-icon")] forState:UIControlStateDisabled];
        [_networkAdapterButton setTitle:L(@"CHOOSE") forState:UIControlStateNormal];
        [_networkAdapterButton addTarget:self
                                  action:@selector(networkAdapterButtonAction:)
                        forControlEvents:UIControlEventTouchUpInside];
        
        _networkAdapterButton.isAccessibilityElement = YES;
        _networkAdapterButton.accessibilityIdentifier = @"NetworkAdapterButton";
    }
    return _networkAdapterButton;
}

- (CZGridView *)packageSelectionView {
    if (_packageSelectionView == nil) {
        CZGridRow *row0 = [[CZGridRow alloc] initWithHeight:32.0 alignment:CZGridRowAlignmentFill];
        CZGridRow *row1 = [[CZGridRow alloc] initWithHeight:24.0 alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = @[row0, row1];
        
        CZGridColumn *column0 = [[CZGridColumn alloc] initWithWidth:120.0 alignment:CZGridColumnAlignmentLeading];
        CZGridColumn *column1 = [[CZGridColumn alloc] initWithWidth:640.0 alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = @[column0, column1];
        
        _packageSelectionView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 231.0, 768.0, 56.0) rows:rows columns:columns];
        _packageSelectionView.columnSpacing = 8.0;
    }
    return _packageSelectionView;
}

- (CZUppercaseLabel *)selectPackageLabel {
    if (_selectPackageLabel == nil) {
        _selectPackageLabel = [[CZUppercaseLabel alloc] init];
        _selectPackageLabel.text = L(@"MIC_CONFIG_OBJECTIVES_FOR_PACKAGES");
        
        _selectPackageLabel.isAccessibilityElement = YES;
        _selectPackageLabel.accessibilityIdentifier = @"SelectPackageLabel";
    }
    return _selectPackageLabel;
}

- (UILabel *)selectPackageHintLabel {
    if (_selectPackageHintLabel == nil) {
        _selectPackageHintLabel = [[UILabel alloc] init];
        _selectPackageHintLabel.backgroundColor = [UIColor clearColor];
        _selectPackageHintLabel.font = [UIFont cz_caption];
        _selectPackageHintLabel.text = L(@"MIC_CONFIG_CHOOSE_CUSTOM_PACKAGES");
        _selectPackageHintLabel.textColor = [UIColor cz_gs70];
        
        _selectPackageHintLabel.isAccessibilityElement = YES;
        _selectPackageHintLabel.accessibilityIdentifier = @"SelectPackageHintLabel";
    }
    return _selectPackageHintLabel;
}

- (CZDropdownMenu *)selectPackageDropdown {
    if (_selectPackageDropdown == nil) {
        NSArray *packageNames = [CZNosepieceManager packageNames];
        int packageDisplay = [[CZNosepieceManager displayFromPackages][self.package] intValue];
        
        _selectPackageDropdown = [[CZDropdownMenu alloc] initWithStyle:kCZDropdownStyleMain title:L(@"CHOOSE") imagePicker:nil expandTitles:packageNames];
        _selectPackageDropdown.selectedIndex = packageDisplay;
        [_selectPackageDropdown addTarget:self
                                   action:@selector(selectPackageDropdownAction:)
                         forControlEvents:UIControlEventValueChanged];
        
        _selectPackageDropdown.isAccessibilityElement = YES;
        _selectPackageDropdown.accessibilityIdentifier = @"SelectPackageDropdown";
    }
    return _selectPackageDropdown;
}

- (UIActivityIndicatorView *)mnaSearchingIndicator {
    if (_mnaSearchingIndicator == nil) {
        _mnaSearchingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _mnaSearchingIndicator.color = [UIColor darkGrayColor];
    }
    return _mnaSearchingIndicator;
}

- (CZTabViewController *)selectionTabViewController {
    if (_selectionTabViewController == nil) {
        _selectionTabViewController = [[CZTabViewController alloc] init];
        _selectionTabViewController.viewControllers = @[self.nosepieceConfigurationViewController];
    }
    return _selectionTabViewController;
}

- (CZNosepieceConfigurationViewController *)nosepieceConfigurationViewController {
    if (_nosepieceConfigurationViewController == nil) {
        _nosepieceConfigurationViewController = [[CZNosepieceConfigurationViewController alloc] initWithMicroscopeModel:self.microscopeModel];
    }
    return _nosepieceConfigurationViewController;
}

- (CZReflectorConfigurationViewController *)reflectorConfigurationViewController {
    if (_reflectorConfigurationViewController == nil) {
        _reflectorConfigurationViewController = [[CZReflectorConfigurationViewController alloc] initWithMicroscopeModel:self.microscopeModel];
    }
    return _reflectorConfigurationViewController;
}

- (CZToolbar *)toolbar {
    if (_toolbar == nil) {
        _toolbar = [[CZToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.bounds.size.height - kToolbarHeight, self.view.bounds.size.width, kToolbarHeight)];
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _toolbar.backgroundColor = [UIColor clearColor];
        _toolbar.insets = UIEdgeInsetsMake(8.0, 8.0, 8.0, 8.0);
    }
    return _toolbar;
}

- (CZToolbarItem *)showBarcodeItem {
    if (_showBarcodeItem == nil) {
        CZButton *button = [[CZButton alloc] init];
        [button setTitle:L(@"MIC_CONFIG_SHOW_QRCODE") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(showBarCodeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"ShowBarCodeButton";
        
        _showBarcodeItem = [CZToolbarItem anonymousItem];
        _showBarcodeItem.view = button;
        _showBarcodeItem.width = 185.0;
    }
    return _showBarcodeItem;
}

- (CZToolbarItem *)onlyUseThisMicroscopeItem {
    if (_onlyUseThisMicroscopeItem == nil) {
        CZCheckBox *checkBox = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
        [checkBox setTitle:L(@"MIC_CONFIG_SINGLE_MICROSCOPE") forState:UIControlStateNormal];
        [checkBox addTarget:self
                     action:@selector(onlyUseThisMicroscopeCheckBoxAction:)
           forControlEvents:UIControlEventTouchUpInside];
        
        checkBox.isAccessibilityElement = YES;
        checkBox.accessibilityIdentifier = @"OnlyUseThisMicroscopeCheckBox";
        
        _onlyUseThisMicroscopeItem = [CZToolbarItem anonymousItem];
        _onlyUseThisMicroscopeItem.view = checkBox;
    }
    return _onlyUseThisMicroscopeItem;
}

- (CZToolbarItem *)productWebsiteItem {
    if (_productWebsiteItem == nil) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:A(@"website-icon")] forState:UIControlStateNormal];
        [button addTarget:self
                   action:@selector(productWebsiteButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"ProductWebsiteButton";
        
        _productWebsiteItem = [CZToolbarItem anonymousItem];
        _productWebsiteItem.view = button;
        _productWebsiteItem.width = 32.0;
    }
    return _productWebsiteItem;
}

- (CZToolbarItem *)onlineShopItem {
    if (_onlineShopItem == nil) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:A(@"buy-icon")] forState:UIControlStateNormal];
        [button addTarget:self
                   action:@selector(onlineShopButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"OnlineShopButton";
        
        _onlineShopItem = [CZToolbarItem anonymousItem];
        _onlineShopItem.view = button;
        _onlineShopItem.width = 32.0;
    }
    return _onlineShopItem;
}

- (CZToolbarItem *)scalingReportItem {
    if (_scalingReportItem == nil) {
        CZButton *button = [[CZButton alloc] init];
        [button setTitle:L(@"MIC_CONFIG_SCALING_REPORT") forState:UIControlStateNormal];
        [button addTarget:self
                   action:@selector(scalingReportButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"ScalingReportButton";
        
        _scalingReportItem = [CZToolbarItem anonymousItem];
        _scalingReportItem.view = button;
        _scalingReportItem.width = 180.0;
    }
    return _scalingReportItem;
}

#pragma mark - IBAction handlers

- (void)backBarButtonItemAction:(id)sender {
    [self.view endEditing:YES];
    
    if (self.pinInvalidAlert) {
        return;
    }
    
    if ([self.originalMicroscopeModel isEqualToMicroscopeModel:self.microscopeModel]) {
        [self finalizeConfiguration];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    self.originalMicroscopeModel = nil;
    
    NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_ASK_ON_SAVE_MSG"), self.microscopeModel.name];
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:message level:CZAlertLevelWarning];
    @weakify(self);
    [alert addActionWithTitle:L(@"DISCARD") style:CZDialogActionStyleDestructive handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self.microscopeModel deleteTemporarilyScalingImages];
        [self finalizeConfiguration];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
        @strongify(self);
        [self saveMicroscopeModelWithCompletionHandler:^{
            @strongify(self);
            [self finalizeConfiguration];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
    [alert presentAnimated:YES completion:nil];
}

- (void)applyButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    if (self.lockConfigurationToggleButton.selectedIndex == kLockConfigurationButtonIndex) {
        if (![self validateLockPINText] ||
            ![self.pinTextField.text isEqualToString:self.confirmPinTextField.text]) {
            [self showPinMismatchAlert];
            return;
        }
    }
    
    NSString *pin = self.pinTextField.text;
    self.lockPIN = pin;
    
    @weakify(self);
    [self saveMicroscopeModelWithCompletionHandler:^{
        @strongify(self);
        self.mnaPairingManager.delegate = nil;
        self.originalMicroscopeModel = nil;
        [self finalizeConfiguration];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)showBarCodeButtonAction:(id)sender {
    if ([self.originalMicroscopeModel isEqualToMicroscopeModel:self.microscopeModel]) {
        [self showQRCodeView];
    } else {
        NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_ASK_ON_SAVE_MSG"), self.microscopeModel.name];
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:message level:CZAlertLevelWarning];
        @weakify(self);
        [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            [self saveMicroscopeModelWithCompletionHandler:^{
                @strongify(self);
                self.originalMicroscopeModel = self.microscopeModel;
                [self showQRCodeView];
            }];
        }];
        [alert presentAnimated:YES completion:nil];
    }
}

- (void)showQRCodeView {
    CZMicroscopeConfigurationBarcodeViewController *barcodeViewController = [[CZMicroscopeConfigurationBarcodeViewController alloc] initWithMicroscopeModel:self.microscopeModel magicNumber:kQRCodeMagicNumber];
    [self.navigationController pushViewController:barcodeViewController animated:YES];
}

- (void)refreshMicrosopeModelList {
    self.microscopeModelSelectionView.allowsSelection = self.isReadOnlyMode == NO;
#if MICCONFIG_REFACTORED
    self.supportedMicroscopeModelTypes = self.camera.supportedMicroscopeModelTypes;
#else
    self.supportedMicroscopeModelTypes = self.camera.supportedMicroscopeModelTypes;
    if ([self.supportedMicroscopeModelTypes containsObject:@(kScanBarCodeModel)]) {
        NSMutableArray<NSNumber *> *supportedMicroscopeModelTypes = [self.supportedMicroscopeModelTypes mutableCopy];
        [supportedMicroscopeModelTypes removeObject:@(kScanBarCodeModel)];
        self.supportedMicroscopeModelTypes = supportedMicroscopeModelTypes;
    }
#endif
    [self.microscopeModelSelectionView reloadData];
}

- (void)lockConfigurationToggleButtonAction:(id)sender {
    if (self.lockConfigurationToggleButton.selectedIndex == kLockConfigurationButtonIndex) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"MIC_CONFIG_LOCK_PIN_WARNING_MESSAGE") level:CZAlertLevelInfo];
        [alert addCheckBoxWithConfigurationHandler:^(CZCheckBox *checkBox) {
            NSString *title = [NSString stringWithFormat:L(@"MIC_CONFIG_LOCK_PIN_REMEMBER_LABEL"), [[UIApplication sharedApplication] cz_name]];
            [checkBox setTitle:title forState:UIControlStateNormal];
        }];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            BOOL isPINRemembered = alert.checkBoxes.firstObject.isSelected;
            self.shouldRememberPIN = isPINRemembered;
        }];
        [alert presentAnimated:YES completion:nil];
    } else {
        self.shouldRememberPIN = NO;
        self.microscopeModel.pin = nil;
    }
    
    [self updateLockConfigControlStatus];
}

- (void)shadingCorrectionSetupButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    CZShadingCorrectionSetupViewController *shadingCorrectionSetupViewController = [[CZShadingCorrectionSetupViewController alloc] initWithCamera:self.camera microscopeModel:self.microscopeModel];
    [self.navigationController pushViewController:shadingCorrectionSetupViewController animated:YES];
}

- (void)scalingCalibrationSetupButtonAction:(id)sender {
    [self.view endEditing:YES];
    
    CZCameraView *cameraView = [[CZCameraView alloc] init];
    cameraView.camera = self.camera;
    [cameraView setAnnotationHidden:YES];
    
    CZMicroscopeAutoScaleController *autoScaleControler = [[CZMicroscopeAutoScaleController alloc] initWithCameraView:cameraView
                                                                                                      microscopeModel:self.microscopeModel
                                                                                                                 position:0];
    autoScaleControler.delegate = self;
    
    [self.navigationController pushViewController:autoScaleControler animated:YES];
}

- (void)cameraAdapterDropdownAction:(id)sender {
    CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
    NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeCameraAdapter];
    CZZoomLevel *zoomLevel = zoomLevels[self.cameraAdapterDropdown.selectedIndex];
    [self.microscopeModel setZoom:zoomLevel forType:kZoomTypeCameraAdapter];
}

- (void)networkAdapterButtonAction:(id)sender {
    [self updateMNAListControllerContents];
    
    CZMNA *pairedMNA = self.pairedMNA;
    NSDictionary<NSString *, CZMicroscopeModel *> *modelList = [self.pairedModelList copy];
    
    CZPickerViewController<CZMNA *> *picker = [[CZPickerViewController alloc] initWithSelectedItem:pairedMNA selectableItems:self.mnaList];
    
    picker.titleForItem = ^NSString *(CZPickerViewController<CZMNA *> *picker, CZMNA *item) {
        CZMicroscopeModel *model = modelList[item.macAddress];
        NSString *macAddressDisplayName = [item.macAddress substringFromIndex:kStartIndexOfLastThreeOctet];
        if (model.name.length > 0) {
            return [NSString stringWithFormat:@"%@ (%@)", macAddressDisplayName, model.name];
        } else {
            return macAddressDisplayName;
        }
    };
    
    picker.saveButtonTitleForItem = ^NSString *(CZPickerViewController<CZMNA *> *picker, CZMNA *item) {
        CZMicroscopeModel *model = modelList[item.macAddress];
        if (item == nil) {
            return L(@"MIC_CONFIG_MNA_ASSOCIATE");
        } else if (model.name.length > 0 || [pairedMNA.macAddress isEqualToString:item.macAddress]) {
            return L(@"RESET");
        } else {
            return L(@"MIC_CONFIG_MNA_ASSOCIATE");
        }
    };
    
    @weakify(self);
    [picker setHandler:^(CZPickerViewController<CZMNA *> *picker, CZDialogAction *action) {
        @strongify(self);
        CZMicroscopeModel *model = modelList[picker.selectedItem.macAddress];
        if (model.name.length > 0 || [pairedMNA.macAddress isEqualToString:picker.selectedItem.macAddress]) {
            [self resetMNA:picker.selectedItem];
            [self updateMNAListControllerContents];
        } else {
            CZMicroscopeModel *model = modelList[picker.selectedItem.macAddress];
            
            BOOL isEqualMACAddress = [model.cameraMACAddress isEqualComparisionWithCaseInsensitive:self.microscopeModel.cameraMACAddress];
            if (model.cameraMACAddress && !isEqualMACAddress) {
                return;
            }
            
            [self associateWithMNA:picker.selectedItem];
        }
    } forActionWithIdentifier:CZPickerViewControllerSaveActionIdentifier];
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)selectPackageDropdownAction:(id)sender {
    self.package = [[CZNosepieceManager packagesFromDisplay][self.selectPackageDropdown.selectedIndex] intValue];
    
    if ([self.microscopeModel isKindOfClass:[CZMicroscopePrimoStar class]]) {
        CZMicroscopePrimoStar *primoStar = (CZMicroscopePrimoStar *)self.microscopeModel;
        primoStar.package = self.package;
        primoStar.nosepiece.currentPosition = NSNotFound;
        
        for (NSUInteger position = 0; position < self.microscopeModel.nosepiece.positions; position++) {
            [self.microscopeModel.nosepiece setObjective:nil atPosition:position];
        }
        
        NSUInteger position = 0;
        NSArray<CZObjective *> *objectives = [CZMicroscopePrimoStar objectivesInPackage:primoStar.package];
        for (CZObjective *objective in objectives) {
            [primoStar.nosepiece setObjective:objective atPosition:position];
            position++;
        }
        
        [self.nosepieceConfigurationViewController reloadData];
    }
}

- (void)onlyUseThisMicroscopeCheckBoxAction:(id)sender {
    self.onlyUseThisMicroscopeItem.selected = !self.onlyUseThisMicroscopeItem.isSelected;
    self.microscopeModel.isDefault = self.onlyUseThisMicroscopeItem.isSelected;
}

- (void)productWebsiteButtonAction:(id)sender {
    NSString *address = [self.microscopeModel localedWebsiteAddress];
    [self browseURLStringInApp:address];
}

- (void)onlineShopButtonAction:(id)sender {
    NSString *address = [self.microscopeModel localedMicroShopAddress];
    [self browseURLStringInApp:address];
}

- (void)scalingReportButtonAction:(id)sender {
    if ([self.originalMicroscopeModel isEqualToMicroscopeModel:self.microscopeModel]) {
        [self doReport];
    } else {
        NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_ASK_ON_SAVE_MSG"), self.microscopeModel.name];
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:message level:CZAlertLevelWarning];
        @weakify(self);
        [alert addActionWithTitle:L(@"CANCEL") style:CZDialogActionStyleCancel handler:nil];
        [alert addActionWithTitle:L(@"SAVE") style:CZDialogActionStyleDefault handler:^(CZAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            [self saveMicroscopeModelWithCompletionHandler:^{
                @strongify(self);
                self.originalMicroscopeModel = self.microscopeModel;
                [self doReport];
            }];
        }];
        [alert presentAnimated:YES completion:nil];
    }
}

- (void)doReport {
#if REPORT_REFACTORED
    NSString *templateFilePath = nil;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kEnableMNALevelSimulator]) {
//        templateFilePath = [[[CZAppDelegate get] documentPath] stringByAppendingPathComponent:@"ScalingReportTemplate.czrtj"];
        // Fix bug 187000
        templateFilePath = [[CZLocalFileList sharedInstance].documentPath stringByAppendingPathComponent:@"ScalingReportTemplate.czrtj"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:templateFilePath]) {
            templateFilePath = nil;
        }
    }
    if (templateFilePath == nil) {
        templateFilePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"ScalingReportTemplate.czrtj"];
    }
    
    CZScalingReportManager *reportManager = [[CZScalingReportManager alloc] init];
    reportManager.imageRenderMode = kImageRenderModeMultipage;
    reportManager.microscopeModel = self.microscopeModel;
    reportManager.cameraSnapshotWidth = [self.currentCamera snapResolution].width;
    reportManager.templateFilePath = templateFilePath;
    [reportManager loadCachedUserInputData];
    
    CZReportEditViewController *controller = [[CZReportEditViewController alloc] initWithStyle:UITableViewStyleGrouped];
    controller.reportManager = reportManager;
    [reportManager release];

    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
#endif
}

#pragma mark - Private methods

- (void)updateWithCurrentMicroscopeModel {
    [self updateUIWithCurrentMicroscopeModel];
    
    BOOL supportsMNA = [self.microscopeModel canSupportMNA];
    if (supportsMNA) {
        if (!self.mnaSearchingIndicator.isAnimating) {
            [self.mnaSearchingIndicator startAnimating];
        }
        
        [self.mnaPairingManager searchMNAPairedWithModelAsync:self.microscopeModel];
    }
    
    [self checkShadingCorrectionDataFormat];
}

- (void)updateUIWithCurrentMicroscopeModel {
    CZMicroscopeTypes type = self.microscopeModel.type;
    
    [self highlightModelButtonWithMicroscopeType:type];
    
#if DCM_REFACTORED
    self.onlyUseThisMicroscopeItem.selected = self.microscopeModel.isDefault && ![[CZDCMManager defaultManager] enableMultiMicroscopes];
#else
    self.onlyUseThisMicroscopeItem.selected = self.microscopeModel.isDefault;
#endif
    
    if (type == kUnknownModelType) {
        self.microscopeModelDetailView.hidden = YES;
        self.selectModelLabel.hidden = NO;
        self.onlyUseThisMicroscopeItem.hidden = !self.isReadOnlyMode;
        self.lockConfigurationToggleButton.selectedIndex = kUnlockConfigurationButtonIndex;
        self.pinTextField.text = nil;
        self.confirmPinTextField.text = nil;
        self.lockPIN = nil;
        self.showBarcodeItem.enabled = NO;
        return;
    } else if (self.isReadOnlyMode) {
        self.microscopeModelDetailView.hidden = YES;
        self.selectModelLabel.hidden = YES;
        self.onlyUseThisMicroscopeItem.hidden = NO;
        if ([self.camera hasPIN]) {
            NSString *pin = [self.camera pin];
            self.pinTextField.text = pin;
            self.confirmPinTextField.text = pin;
            self.lockPIN = pin;
            self.lockConfigurationToggleButton.selectedIndex = kLockConfigurationButtonIndex;
        } else {
            self.pinTextField.text = nil;
            self.confirmPinTextField.text = nil;
            self.lockPIN = @"";
            self.lockConfigurationToggleButton.selectedIndex = kUnlockConfigurationButtonIndex;
        }
        self.showBarcodeItem.enabled = NO;
        return;
    }
    self.onlyUseThisMicroscopeItem.hidden = NO;
    
    self.microscopeModelDetailView.hidden = NO;
    self.selectModelLabel.hidden = YES;
    self.showBarcodeItem.enabled = YES;
    
    if (![self.camera.mockDisplayName isEqualToString:self.camera.displayName]) {
        self.microscopeNameTextField.text = self.camera.displayName;
    }
    self.microscopeNameTextField.placeholder = self.camera.mockDisplayName;
    
    self.lockConfigurationToggleButton.selectedIndex = [self.camera hasPIN] ? kLockConfigurationButtonIndex : kUnlockConfigurationButtonIndex;
    if (self.lockConfigurationToggleButton.selectedIndex == kLockConfigurationButtonIndex) {
        NSString *pin = [self.camera pin];
        self.pinTextField.text = pin;
        self.confirmPinTextField.text = pin;
        
        self.lockPIN = pin;
        
        self.shouldRememberPIN = (self.microscopeModel.cachedCameraPIN.length > 0);
        self.microscopeModel.pin = pin;
    } else {
        self.shouldRememberPIN = NO;
        self.microscopeModel.pin = nil;
    }
    [self updateLockConfigControlStatus];
    
    [self updateShadingCorrectionSetupButtonStatus];
    
    BOOL isGeneric = (self.microscopeModel.type == kAxiocamCompound || self.microscopeModel.type == kAxiocamStereo);
    BOOL isAxioline = (self.microscopeModel.type == kAxioscope) || (self.microscopeModel.type == kAxiolab);
    self.cameraAdapterLabel.hidden = !isGeneric && !isAxioline;
    self.cameraAdapterDropdown.hidden = !isGeneric && !isAxioline;
    
    if (!self.cameraAdapterLabel.hidden) {
        [self.cameraAdapterDropdown removeAllItems];
        
        CZZoomLevelSet *zoomLevelSet = [self.microscopeModel availableZoomLevelSet];
        NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeCameraAdapter];
        for (CZZoomLevel *zoomLevel in zoomLevels) {
            CZDropdownItem *item = [CZDropdownItem itemWithTitle:zoomLevel.displayName];
            [self.cameraAdapterDropdown addItem:item];
        }
        
        CZZoomLevel *cameraAdapterZoomLevel = [self.microscopeModel zoomForType:kZoomTypeCameraAdapter];
        self.cameraAdapterDropdown.selectedItemTitle = cameraAdapterZoomLevel.displayName;
    }

    BOOL supportsMNA = [self.microscopeModel canSupportMNA];
    self.networkAdapterLabel.hidden = !supportsMNA;
    self.networkAdapterButton.hidden = !supportsMNA;
    self.mnaSearchingIndicator.hidden = !supportsMNA;
    
    self.packageSelectionView.hidden = YES;
    BOOL hasNoWebsiteAddress = ([self.microscopeModel localedWebsiteAddress].length == 0);
    self.productWebsiteItem.hidden = hasNoWebsiteAddress;
    self.onlineShopItem.hidden = YES;
    
    //TODO: Advanced features. Pls check it.
    if (type == kAxiocamCompound || type == kPrimotech) {
        self.scalingReportItem.hidden = NO;
    } else {
        self.scalingReportItem.hidden = YES;
    }
    
    if (type == kPrimoStar) {
        NSAssert([self.microscopeModel isKindOfClass:[CZMicroscopePrimoStar class]], @"");
        CZMicroscopePrimoStar *primoStar = (CZMicroscopePrimoStar *)self.microscopeModel;
        
        self.packageSelectionView.hidden = NO;
        self.package = primoStar.package;
        if (self.package == kCZPrimoStarPackageManualMode) {
            self.selectPackageDropdown.selectedIndex = CZDropdownMenuNoSelectedItem;
        } else {
            int packageDisplay = [[CZNosepieceManager displayFromPackages][self.package] intValue];
            self.selectPackageDropdown.selectedItemTitle = [CZNosepieceManager packageNames][packageDisplay];
        }
        
        self.onlineShopItem.hidden = ![self.microscopeModel hasLocaledMicroShop];
    }
    
    self.nosepieceConfigurationViewController.microscopeModel = self.microscopeModel;
    
    if ([self.microscopeModel hasReflector] && self.selectionTabViewController.viewControllers.count == 1) {
        [self.selectionTabViewController addViewController:self.reflectorConfigurationViewController];
        self.reflectorConfigurationViewController.microscopeModel = self.microscopeModel;
    } else if (![self.microscopeModel hasReflector] && self.selectionTabViewController.viewControllers.count == 2) {
        [self.selectionTabViewController removeViewController:self.reflectorConfigurationViewController];
    }
}

- (void)saveMicroscopeModelWithCompletionHandler:(void (^)(void))completionHandler {
    if (self.microscopeModel.type == kUnknownModelType) {
        if (completionHandler) {
            completionHandler();
        }
        return;
    }
    
    if (self.shouldRememberPIN) {
        self.microscopeModel.cachedCameraPIN = self.lockPIN;
    } else {
        self.microscopeModel.cachedCameraPIN = @"";
    }
    
    // switch compound microscope to valid position
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelCompound class]]) {
        CZNosepiece *nosepiece = self.microscopeModel.nosepiece;
        CZObjective *objective = [nosepiece objectiveAtCurrentPosition];
        if (objective.magnification <= 0.0) {
            for (NSUInteger position = 0; position < nosepiece.positions; position++) {
                CZObjective *objective = [nosepiece objectiveAtPosition:position];
                if (objective.magnification > 0.0) {
                    nosepiece.currentPosition = position;
                    break;
                }
            }
        }
    }
    
    // switch stereo microscope to valid position
    if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        if (self.microscopeModel.currentZoomClickStopPosition == NSNotFound && self.microscopeModel.zoomClickStopPositions > 0) {
            self.microscopeModel.currentZoomClickStopPosition = 0;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(microscopeConfigurationViewControllerWillSaveConfiguration:)]) {
        [self.delegate microscopeConfigurationViewControllerWillSaveConfiguration:self];
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = L(@"BUSY_INDICATOR_SAVING");
    hud.color = kSpinnerBackgroundColor;
    
    [self.microscopeModel saveSlotsToDefaults];
    [self.microscopeModel saveToDefaults];
    
    if ([self.microscopeModel canSupportMNA]) {
        [self.microscopeModel saveToMNA:NULL];
    }
    
#if DCM_REFACTORED
    if (![CZDCMManager defaultManager].hasConnectedToDCMServer) {
        BOOL originIsSingleMode = ![[CZDCMManager defaultManager] enableMultiMicroscopes];
        if (originIsSingleMode != self.microscopeModel.isDefault) {
            [[CZDefaultSettings sharedInstance] setEnableMultiMicroscopes:!self.microscopeModel.isDefault];
        }
    }
#endif
    
    [self.microscopeModel moveScalingImages];
    
    @weakify(self);
    [self.microscopeModel saveToCamera:self.camera withCompletionHandler:^{
        @strongify(self);
        [hud hide:YES];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(microscopeConfigurationViewControllerDidSaveConfiguration:)]) {
            [self.delegate microscopeConfigurationViewControllerDidSaveConfiguration:self];
        }
        
        if (completionHandler) {
            completionHandler();
        }
    }];
}

- (void)highlightModelButtonWithMicroscopeType:(CZMicroscopeTypes)type {
    NSInteger item = [self.supportedMicroscopeModelTypes indexOfObject:@(type)];
    if (self.supportedMicroscopeModelTypes && item != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        if (![self.microscopeModelSelectionView.indexPathsForVisibleItems containsObject:indexPath]) {
            [self.microscopeModelSelectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionCenteredVertically];
        }
    }
}

- (void)updateLockConfigControlStatus {
    if (self.lockConfigurationToggleButton.selectedIndex == kLockConfigurationButtonIndex) {
        self.pinLabel.alpha = 1.0;
        self.confirmPinLabel.alpha = 1.0;
        self.pinTextField.enabled = YES;
        self.pinTextField.alpha = 1.0;
        self.confirmPinTextField.enabled = YES;
        self.confirmPinTextField.alpha = 1.0;
    } else {
        self.pinLabel.alpha = 0.5;
        self.confirmPinLabel.alpha = 0.5;
        self.pinTextField.alpha = 0.5;
        self.pinTextField.enabled = NO;
        self.confirmPinTextField.enabled = NO;
        self.confirmPinTextField.alpha = 0.5;
        self.lockPIN = nil;
        self.pinTextField.text = nil;
        self.confirmPinTextField.text = nil;
    }
}

- (BOOL)validateLockPINText {
    NSString *pin = self.pinTextField.text;
    if (!pin || pin.length != 4) {
        return NO;
    }
    return YES;
}

- (BOOL)validatePINTextField {
    if (![self validateLockPINText] && self.pinInvalidAlert == nil) {
        self.pinTextField.text = nil;
        
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"MIC_CONFIG_LOCK_PIN_LABEL_2") level:CZAlertLevelInfo];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        self.pinInvalidAlert = alert;
        
        return NO;
    } else {
        self.confirmPinTextField.text = nil;
        return YES;
    }
}

- (BOOL)validatePINRetypeTextField {
    if (![self.confirmPinTextField.text isEqualToString:self.pinTextField.text]) {
        self.confirmPinTextField.text = nil;
        [self showPinMismatchAlert];
        return NO;
    } else {
        self.lockPIN = self.pinTextField.text;
        return YES;
    }
}

- (void)showPinMismatchAlert {
    if (self.pinInvalidAlert == nil) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"MIC_CONFIG_LOCK_PIN_MISMATCH") level:CZAlertLevelInfo];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
        [alert presentAnimated:YES completion:nil];
        self.pinInvalidAlert = alert;
    }
}

- (void)updateMNAListControllerContents {
    CZMicroscopeTypes type = self.microscopeModel.type;
    NSDictionary *mnaList = [self.mnaPairingManager newMNAList];
    NSArray *macAddresses = mnaList.allKeys;
    NSMutableArray *stemiMNAList = [[NSMutableArray alloc] init];
    NSMutableArray *primotechMNAList = [[NSMutableArray alloc] init];
    for (NSString *string in macAddresses) {
        if ([string hasPrefix:kStemi508MNAMacAddressPrefix]) {
            CZMNA *mna = [mnaList objectForKey:string];
            [stemiMNAList addObject:mna];
        } else {
            CZMNA *mna = [mnaList objectForKey:string];
            [primotechMNAList addObject:mna];
        }
    }
    
    if (type == kStemi508) {
        self.mnaList = stemiMNAList;
    } else {
        self.mnaList = primotechMNAList;
    }
    
    NSDictionary *modelList = [self.mnaPairingManager newPairedModelList];
    self.pairedModelList = modelList;
    
    if (self.mnaList.count <= 0) {
        self.networkAdapterButton.enabled = NO;
    } else {
        self.networkAdapterButton.enabled = YES;
    }
}

- (void)associateWithMNA:(CZMNA *)mna {
    if (!mna) {
        [self.networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-pending-icon")] forState:UIControlStateNormal];
        [self.networkAdapterButton setTitle:L(@"CHOOSE") forState:UIControlStateNormal];
        return;
    }
    
    self.pairedMNA = mna;
    self.microscopeModel.mnaMACAddress = mna.macAddress;
    self.microscopeModel.mnaIPAddress = mna.ipAddress;
    [self saveToMNA:self.microscopeModel];
    
    [self.mnaPairingManager pairMNA:mna microscope:self.microscopeModel];
    
    [self.mnaInfoRetrievingTimer invalidate];
    self.mnaInfoRetrievingTimer = [NSTimer scheduledTimerWithTimeInterval:kMNAInfoRetrievingInterval
                                                                   target:self
                                                                 selector:@selector(mnaInfoRetrievingTimerHandler:)
                                                                 userInfo:nil
                                                                  repeats:YES];
    
    [self.nosepieceConfigurationViewController lockSelection];
    
    [self.networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-paired-icon")] forState:UIControlStateNormal];
#if MICCONFIG_REFACTORED
    [self.networkAdapterButton setTitle:L(@"SETTINGS") forState:UIControlStateNormal];
#endif
}

- (void)mnaInfoRetrievingTimerHandler:(NSTimer *)timer {
    if (self.microscopeModel && self.microscopeModel.canSupportMNA && self.pairedMNA) {
        [self.microscopeModel retrieveCurrentObjectiveFromMNAAsync:self];
        [self.microscopeModel newMicroscopeInfoFromMNAAsync:self.pairedMNA delegate:self];
    }
}

- (void)browseURLStringInApp:(NSString *)urlString {
    CZWebBrowserViewController *webViewController = [[CZWebBrowserViewController alloc] init];
    webViewController.urlString = urlString;
    
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)scanQRCode {
    if (self.qrCodeScanner == nil) {
        CZQRCodeScannerViewController *scanner = [[CZQRCodeScannerViewController alloc] init];
        scanner.delegate = self;
        scanner.view.backgroundColor = kDefaultBackGroundColor;
        CGRect frame = self.microscopeModelDetailView.frame;
        frame = CGRectInset(frame, 6, 6);
        scanner.view.frame = frame;
        
        [self addChildViewController:scanner];
        [self.view addSubview:scanner.view];
        
        self.qrCodeScanner = scanner;
    }

    self.showBarcodeItem.enabled = NO;

    [self.qrCodeScanner startReading];
    self.qrCodeScanner.infoLabel.text = [NSString stringWithFormat:L(@"MIC_CONFIG_QRCODE_SCANBY_TIP"), self.microscopeModel.name];
}

- (void)stopScanQRCode {
    if (self.qrCodeScanner) {
        [self.qrCodeScanner.view removeFromSuperview];
        [self.qrCodeScanner removeFromParentViewController];
        self.qrCodeScanner = nil;
        self.showBarcodeItem.enabled = YES;
    }
}

- (void)showHelpOverlay {
#if MICCONFIG_REFACTORED
    BOOL hide = [[NSUserDefaults standardUserDefaults] boolForKey:kHideInfoScreenMicConfig];
    if (!hide) {
        CZHelpOverlayView *helpOverlayer = [[CZHelpOverlayView alloc] init];
        helpOverlayer.hideNextTimeKey = kHideInfoScreenMicConfig;

        [helpOverlayer addBalloonText:L(@"MIC_HELP_LOCK")
                              atFrame:CGRectMake(600, 160, 220, 68)
                               toView:self.lockConfigButton
                            direction:CZBalloonDirectionLeft];
        
        [helpOverlayer addBalloonText:L(@"MIC_HELP_MIC_NAME")
                              atFrame:CGRectMake(480, 56, 500, 68)
                               toView:self.microscopeNameTextField
                            direction:CZBalloonDirectionNone];
        
        NSUInteger buttonId = kHelpoverlayMicroscopeType;

        [helpOverlayer addBalloonText:L(@"MIC_HELP_PRIMO_STAR")
                              atFrame:CGRectMake(60, 56, 400, 180)
                               toView:self.modelButtons[buttonId]
                            direction:CZBalloonDirectionNone];
        
        [helpOverlayer addBalloonText:L(@"MIC_HELP_DEFAULT_BUTTON")
                              atFrame:CGRectMake(40, 350, 360, 300)
                               toView:self.defaultToggleButton
                            direction:CZBalloonDirectionDown];
        
        if (!self.websiteItem.isHidden) {
            [helpOverlayer addBalloonText:L(@"MIC_HELP_WEBSITE")
                                  atFrame:CGRectMake(260, 600, 220, 60)
                                   toView:self.websiteButton
                                direction:CZBalloonDirectionDown];
        }
        
        if (!self.shopItem.isHidden) {
            [helpOverlayer addBalloonText:L(@"MIC_HELP_WEBSHOP")
                                  atFrame:CGRectMake(480, 680, 180, 60)
                                   toView:self.buyButton
                                direction:CZBalloonDirectionLeft];
        }
        
        [helpOverlayer release];
    }
#endif
}

- (void)checkShadingCorrectionDataFormat {
    NSString *maskPath = [[[NSFileManager defaultManager] cz_cachePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", self.microscopeModel.cameraMACAddress]];
    BOOL maskExists = [[NSFileManager defaultManager] fileExistsAtPath:maskPath];
    if (maskExists && [self.microscopeModel isKindOfClass:[CZMicroscopeModelCompound class]]) {
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:L(@"MIC_SHADING_CORRECTION_UPGRADE") level:CZAlertLevelInfo];
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            [[NSFileManager defaultManager] removeItemAtPath:maskPath error:nil];
        }];
        [alert presentAnimated:YES completion:nil];
    }
}

- (void)saveToMNA:(CZMicroscopeModel *)microscopeModel {
    NSString *mnaMacAddress = microscopeModel.mnaIPAddress;
    
    [self.microscopeModel saveToMNA:^(NSError *connectionError){
        if (connectionError && mnaMacAddress.length > 0) {
            [self.microscopeModel saveToMNA:^(NSError *connectionError){  // try once more
                if (connectionError) {
                    [self showMNACommunicationErrorAlert:mnaMacAddress];
                    [self unpairCurrentMNA];
                }
            }];
        }
    }];
}

- (void)resetMNA:(CZMNA *)mna {
    NSString *mnaMacAddress = mna.macAddress;
    [self.mnaPairingManager resetMNA:mna completionHandler:^(NSError *connectionError){
        if (connectionError && mnaMacAddress.length > 0) {
            [self showMNACommunicationErrorAlert:mnaMacAddress];
        }
    }];
}

- (void)showMNACommunicationErrorAlert:(NSString *)mnaMacAddress {
    NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_CONNECTING_MNA_ERROR"), mnaMacAddress, [[UIApplication sharedApplication] cz_name]];
    CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"ERROR") message:message level:CZAlertLevelError];
    [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:nil];
    [alert presentAnimated:YES completion:nil];
}

- (void)unpairCurrentMNA {
    if (self.pairedMNA) {
        [self.mnaPairingManager unpairMNA:self.pairedMNA];
    }
    self.pairedMNA = nil;
    self.microscopeModel.mnaMACAddress = kNULLMACAddress;
    self.microscopeModel.mnaIPAddress = kNULLIPAddress;
    
    [self.mnaInfoRetrievingTimer invalidate];
    self.mnaInfoRetrievingTimer = nil;
    [self.nosepieceConfigurationViewController unlockSelection];
    
    [self updateMNAListControllerContents];
    [self.networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-pending-icon")] forState:UIControlStateNormal];
    [self.networkAdapterButton setTitle:L(@"CHOOSE") forState:UIControlStateNormal];
}

//Disable the PIN & microscope name update function
- (void)updatePINStateWithDCMConnectedState:(BOOL)connected {
    if (connected) {
        //Disable the lock configuration
        self.lockConfigurationLabel.alpha = 0.5;
        self.lockConfigurationToggleButton.enabled = NO;
        self.lockConfigurationToggleButton.selectedIndex = kUnlockConfigurationButtonIndex;
        
        //Disable the microscope name
        self.microscopeNameLabel.alpha = 0.5;
        self.microscopeNameTextField.userInteractionEnabled = NO;
        self.microscopeNameTextField.alpha = 0.5;
    } else {
        //Enable the lock configuration
        self.lockConfigurationLabel.alpha = 0.5;
        self.lockConfigurationToggleButton.enabled = YES;
        
        //Enbale the microscope name & textField
        self.microscopeNameLabel.alpha = 1.0;
        self.microscopeNameTextField.userInteractionEnabled = YES;
        self.microscopeNameTextField.alpha = 1.0;
    }
}

- (void)updateShadingCorrectionSetupButtonStatus {
    if ([self.camera hasCapability:CZCameraCapabilityEncoding]) {
        CZObjective *currentObjective = [self.microscopeModel.nosepiece objectiveAtCurrentPosition];
        self.shadingCorrectionSetupButton.enabled = currentObjective != nil && !currentObjective.isNone;
    } else if ([self.microscopeModel isKindOfClass:[CZMicroscopeModelStereo class]]) {
        self.shadingCorrectionSetupButton.enabled = self.microscopeModel.zoomClickStopPositions > 0;
    } else {
        self.shadingCorrectionSetupButton.enabled = [self.microscopeModel hasObjectivesAssigned];
    }
}

#pragma mark - Notifications

- (void)nosepieceDidUpdate:(NSNotification *)note {
    if (note.object != self.microscopeModel.nosepiece) {
        return;
    }
    
    [self updateShadingCorrectionSetupButtonStatus];
}

- (void)zoomKnobDidUpdate:(NSNotification *)note {
    if (note.object != self.microscopeModel) {
        return;
    }
    
    [self updateShadingCorrectionSetupButtonStatus];
}

- (void)geminiCameraStatusDidChange:(NSNotification *)note {
    CZGeminiCameraStatus *status = note.userInfo[CZGeminiCameraStatusKey];
    CZGeminiCameraStatusChangeMask changes = [note.userInfo[CZGeminiCameraStatusChangesKey] unsignedIntegerValue];
    
    if ((changes & CZGeminiCameraStatusChangeMaskObjectivePosition) != 0) {
        self.microscopeModel.nosepiece.currentPosition = status.objectivePosition;
        [self.nosepieceConfigurationViewController setCurrentPosition:self.microscopeModel.nosepiece.currentPosition];
    }
    
    if ((changes & CZGeminiCameraStatusChangeMaskReflectorPosition) != 0) {
        self.microscopeModel.reflector.currentPosition = status.reflectorPosition;
        [self.reflectorConfigurationViewController setCurrentPosition:self.microscopeModel.reflector.currentPosition];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.supportedMicroscopeModelTypes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CZMicroscopeModelSelectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CZMicroscopeModelSelectionViewCell class]) forIndexPath:indexPath];
    CZMicroscopeTypes type = self.supportedMicroscopeModelTypes[indexPath.item].unsignedIntegerValue;
    
    switch (type) {
        case kAxiocamCompound:
        case kAxiocamStereo:
            cell.microscopeModelNameLabel.text = [NSString stringWithFormat:@"%@\n%@", self.camera.cameraModelName, [CZMicroscopeModel modelNameOfType:type]];
            cell.microscopeModelNameLabel.numberOfLines = 2;
            cell.microscopeModelImageView.image = self.camera.cameraModelIcon;
            break;
        case kAxioscope:
        case kAxiolab:
            cell.microscopeModelNameLabel.text = [self.microscopeModel modelName];
            cell.microscopeModelNameLabel.numberOfLines = 1;
            cell.microscopeModelImageView.image = [self.microscopeModel modelIcon];
            break;
        case kScanBarCodeModel:
            cell.microscopeModelNameLabel.text = [CZMicroscopeModel modelNameOfType:type];
            cell.microscopeModelNameLabel.numberOfLines = 2;
            cell.microscopeModelImageView.image = [CZMicroscopeModel modelIconOfType:type];
            break;
        default:
            cell.microscopeModelNameLabel.text = [CZMicroscopeModel modelNameOfType:type];
            cell.microscopeModelNameLabel.numberOfLines = 1;
            cell.microscopeModelImageView.image = [CZMicroscopeModel modelIconOfType:type];
            break;
    }
    
    if (self.isReadOnlyMode) {
        cell.microscopeModelImageView.alpha = 0.5;
        cell.microscopeModelNameLabel.enabled = NO;
    } else {
        cell.microscopeModelImageView.alpha = 1.0;
        cell.microscopeModelNameLabel.enabled = YES;
    }
    
    cell.isAccessibilityElement = YES;
    cell.accessibilityIdentifier = @"MicroscopeModelView";
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CZMicroscopeTypes type = self.supportedMicroscopeModelTypes[indexPath.item].unsignedIntegerValue;
    if (type == kScanBarCodeModel) {
        NSIndexPath *previousSelectedIndexPath = collectionView.indexPathsForSelectedItems.firstObject;
        [CZAccessabilityHelper requestAccessCameraWithCompletion:^(BOOL success) {
            if (success) {
                [self scanQRCode];
            } else {
                // revert to last selected button.
                [collectionView selectItemAtIndexPath:previousSelectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
                [collectionView deselectItemAtIndexPath:indexPath animated:YES];
            }
        }];
    }
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CZMicroscopeTypes type = self.supportedMicroscopeModelTypes[indexPath.item].unsignedIntegerValue;
    if (type != kScanBarCodeModel) {
        [self stopScanQRCode];
        
        if (self.microscopeModel == nil || self.microscopeModel.type != type) {
            CZMicroscopeModel *currentModel = [self.microscopeModel copy];
            self.cachedMicroscopeModels[@(self.microscopeModel.type)] = currentModel;
            
            CZMicroscopeModel *cachedModel = self.cachedMicroscopeModels[@(type)];
            if (cachedModel != nil) {
                self.microscopeModel = cachedModel;
            } else {
                self.microscopeModel = [self.microscopeModel modelBySwitchingToType:type];
                if ([self.microscopeModel isKindOfClass:[CZMicroscopeAxioline class]] && [self.camera isKindOfClass:[CZGeminiCamera class]]) {
                    [self.microscopeModel updateDataFromCamera:self.camera];
                }
                
                if ([self.microscopeModel canSupportMNA] && self.pairedMNA) {
                    [self updateMNAListControllerContents];
                    [self.nosepieceConfigurationViewController lockSelection];
                }
            }
            
            if (![self.microscopeModel canSupportMNA]) {
                [self.nosepieceConfigurationViewController unlockSelection];
            }
            
            [self updateWithCurrentMicroscopeModel];
        }
    }
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.microscopeNameTextField) {
        [self.microscopeNameValidator textFieldDidEndEditing:textField];
        if (self.microscopeNameTextField.text.length > 0) {
            self.microscopeModel.name = self.microscopeNameTextField.text;
        } else {
            self.microscopeModel.name = self.camera.displayName;
        }
    } else if (textField == self.pinTextField) {
        [self validatePINTextField];
    } else if (textField == self.confirmPinTextField) {
        BOOL isValid = [self validatePINRetypeTextField];
        if (isValid) {
            self.microscopeModel.pin = self.lockPIN;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string {
    if (textField == self.pinTextField || textField == self.confirmPinTextField) {
        if (textField.text.length + string.length - range.length > 4) {
            return NO;
        }
        
        return [CZCommonUtils numericPINField:textField shouldAcceptChange:string];
    } else if (textField == self.microscopeNameTextField) {
        return [self.microscopeNameValidator textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.microscopeNameTextField) {
        [self.microscopeNameTextField resignFirstResponder];
    } else if (textField == self.pinTextField) {
        if ([self validatePINTextField]) {
            [self.confirmPinTextField becomeFirstResponder];
        }
    } else if (textField == self.confirmPinTextField) {
        [self validatePINRetypeTextField];
        [self.confirmPinTextField resignFirstResponder];
    }
    return YES;
}

#pragma mark - CZMicroscopeAutoScaleControllerDelegate methods

- (void)microscopeAutoScaleControllerDidFinishScaling:(CZMicroscopeAutoScaleController *)controller {
    [self.nosepieceConfigurationViewController reloadData];
}

#pragma mark - CZMicroscopeModelUpdateInfoFromMNADelegate methods

- (void)microscopeModel:(CZMicroscopeModel *)queryModel
          hasUpdateInfo:(CZMicroscopeModel *)newModel
                fromMNA:(CZMNA *)mna {
    if (![self.microscopeModel.mnaMACAddress isEqualToString:newModel.mnaMACAddress]) {
        [self unpairCurrentMNA];
    }
}

- (void)microscopeModel:(CZMicroscopeModel *)myModel
   retrieveNewObjective:(NSUInteger)objective {
    if (!myModel) {
        return;
    }
    
    self.microscopeModel.nosepiece.currentPosition = myModel.nosepiece.currentPosition;
    
    self.nosepieceConfigurationViewController.microscopeModel = self.microscopeModel;
    [self.nosepieceConfigurationViewController setCurrentPosition:self.microscopeModel.nosepiece.currentPosition];
}

#pragma mark - CZMNAParingManagerDelegate methods

- (void)mnaPairingManager:(CZMNAParingManager *)pairingManager
         didFindPairedMNA:(CZMNA *)pairedMNA {
    self.pairedMNA = pairedMNA;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mnaSearchingIndicator stopAnimating];
        
        if (pairedMNA) {
            NSDictionary *modelList = [self.mnaPairingManager newPairedModelList];
            CZMicroscopeModel *modelFromMNA = modelList[pairedMNA.macAddress];
            
            self.microscopeModel.mnaMACAddress = pairedMNA.macAddress;
            self.microscopeModel.mnaIPAddress = pairedMNA.ipAddress;
            
            if (_shouldCompareDataWithMNA &&
                ![self.microscopeModel hasEqualMNAInfoWithMicroscopeModel:modelFromMNA]) {
                NSString *message = [NSString stringWithFormat:L(@"MIC_CONFIG_IF_UPDATE_FROM_MNA"), [[UIApplication sharedApplication] cz_name]];
                CZAlertController *alert = [CZAlertController alertControllerWithTitle:L(@"NOTIFICATION") message:message level:CZAlertLevelInfo];
                @weakify(self);
                [alert addActionWithTitle:L(@"MIC_CONFIG_USE_MNA") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
                    @strongify(self);
                    [self.microscopeModel updateDataFromMNAModel:modelFromMNA];
                    [self.microscopeModel deleteCachedScalingImages];
                }];
                NSString *useLocalActionTitle = [NSString stringWithFormat:L(@"MIC_CONFIG_USE_LOCAL"), [[UIApplication sharedApplication] cz_name]];
                [alert addActionWithTitle:useLocalActionTitle style:CZDialogActionStyleCancel handler:nil];
                [alert presentAnimated:YES completion:nil];
            }
            
            _shouldCompareDataWithMNA = NO;
            
            [self.mnaInfoRetrievingTimer invalidate];
            self.mnaInfoRetrievingTimer = [NSTimer scheduledTimerWithTimeInterval:kMNAInfoRetrievingInterval
                                                                           target:self
                                                                         selector:@selector(mnaInfoRetrievingTimerHandler:)
                                                                         userInfo:nil
                                                                          repeats:YES];
            [self.mnaInfoRetrievingTimer fire];
            
            [self.nosepieceConfigurationViewController lockSelection];
            
            self.networkAdapterButton.enabled = YES;
            [self.networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-paired-icon")] forState:UIControlStateNormal];
#if MICCONFIG_REFACTORED
            [self.networkAdapterButton setTitle:L(@"SETTINGS") forState:UIControlStateNormal];
#endif
        } else {
            self.microscopeModel.mnaMACAddress = kNULLMACAddress;
            self.microscopeModel.mnaIPAddress = kNULLIPAddress;
            
            [self.nosepieceConfigurationViewController unlockSelection];
            
            [self updateMNAListControllerContents];
            [self.networkAdapterButton setImage:[UIImage imageNamed:A(@"network-adpater-pending-icon")] forState:UIControlStateNormal];
            [self.networkAdapterButton setTitle:L(@"CHOOSE") forState:UIControlStateNormal];
        }
    });
}

#pragma mark - CZQRCodeScannerViewControllerDelegate

- (void)codeScannerViewController:(CZQRCodeScannerViewController *)codeScanner didGetString:(NSString *)string {
    if (codeScanner != self.qrCodeScanner) {
        return;
    }
    
    BOOL isCorrectContent = NO;
    NSString *errorMessage = nil;
    
    if ([string hasPrefix:kQRCodeMagicNumber]) {
        string = [string substringFromIndex:3];
        NSError *error = nil;
        CZMicroscopeModel *microscopeModel = [self.microscopeModel newMicroscopeModelWithJsonContent:string error:&error];
        
        if (microscopeModel) {
            // check whether type is support by current App
            NSSet *supportedTypes = [NSSet setWithArray:self.supportedMicroscopeModelTypes];
            if ([supportedTypes containsObject:@(microscopeModel.type)]) {
                self.cachedMicroscopeModels[@(microscopeModel.type)] = microscopeModel;
                self.microscopeModel = microscopeModel;
                
                _shouldCompareDataWithMNA = YES;
                
                isCorrectContent = YES;
            }
        } else if (error != nil) {
            if (error.code == -1001) {
                errorMessage = L(@"MIC_CONFIG_QRCODE_NOT_MATCH");
            }
        }
    }

    if (!isCorrectContent) {
        if (errorMessage == nil) {
            errorMessage = L(@"MIC_CONFIG_QRCODE_INVALIDE_CONTENT");
        }
        
        [self.qrCodeScanner stopReading];
        
        CZAlertController *alert = [CZAlertController alertControllerWithTitle:nil message:errorMessage level:CZAlertLevelInfo];
        @weakify(self);
        [alert addActionWithTitle:L(@"OK") style:CZDialogActionStyleCancel handler:^(CZAlertController *alert, CZDialogAction *action) {
            @strongify(self);
            [self stopScanQRCode];
            [self updateWithCurrentMicroscopeModel];
            [self.view setNeedsDisplay];
        }];
        [alert presentAnimated:YES completion:nil];
    } else {
        [self.qrCodeScanner showSuccessView];
        [NSTimer scheduledTimerWithTimeInterval:1
                                         target:self
                                       selector:@selector(scanQRCodeTimer:)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (void)scanQRCodeTimer:(NSTimer *)timer {
    [timer invalidate];
    
    [self stopScanQRCode];
    [self updateWithCurrentMicroscopeModel];
    [self.view setNeedsDisplay];
}

@end
