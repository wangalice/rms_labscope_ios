//
//  CZShadingCorrectionSetupViewController.h
//  Matscope
//
//  Created by Mike Wang on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZCamera;
@class CZMicroscopeModel;

@interface CZShadingCorrectionSetupViewController : UIViewController

@property (nonatomic, readonly, strong) CZCamera *camera;
@property (nonatomic, readonly, strong) CZMicroscopeModel *microscopeModel;

- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel;

@end
