//
//  CZMicroscopeConfigurationViewController.h
//  Hermes
//
//  Created by Mike Wang on 3/13/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZCamera;
@class CZMicroscopeModel;
@class CZMicroscopeConfigurationViewController;

@protocol CZMicroscopeConfigurationViewControllerDelegate <NSObject>

@optional
- (void)microscopeConfigurationViewControllerWillSaveConfiguration:(CZMicroscopeConfigurationViewController *)microscopeConfigurationViewController;
- (void)microscopeConfigurationViewControllerDidSaveConfiguration:(CZMicroscopeConfigurationViewController *)microscopeConfigurationViewController;

@end

@interface CZMicroscopeConfigurationViewController : UIViewController

@property (nonatomic, weak) id<CZMicroscopeConfigurationViewControllerDelegate> delegate;
@property (nonatomic, readonly, strong) CZCamera *camera;
@property (nonatomic, readonly, copy) CZMicroscopeModel *microscopeModel;
@property (nonatomic, readonly, assign) BOOL isReadOnlyMode;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithCamera:(CZCamera *)camera microscopeModel:(CZMicroscopeModel *)microscopeModel isReadOnlyMode:(BOOL)isReadOnlyMode NS_DESIGNATED_INITIALIZER;

- (void)prepareForConfigurationWithCompletionHandler:(void (^)(BOOL prepared))completionHandler;
- (void)finalizeConfiguration;

@end
