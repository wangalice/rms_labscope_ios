//
//  CZReflectorConfigurationViewController.h
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMicroscopeModel;

@interface CZReflectorConfigurationViewController : UIViewController

@property (nonatomic, strong) CZMicroscopeModel *microscopeModel;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel;

- (void)reloadData;
- (void)setCurrentPosition:(NSUInteger)position;

@end
