//
//  CZQRCodeScannerViewController.h
//  Matscope
//
//  Created by Ralph Jin on 9/15/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZQRCodeScannerViewController;

@protocol CZQRCodeScannerViewControllerDelegate <NSObject>
@optional
- (void)codeScannerViewController:(CZQRCodeScannerViewController *)codeScanner
                     didGetString:(NSString *)string;

@end

@interface CZQRCodeScannerViewController : UIViewController

@property (nonatomic, weak) id<CZQRCodeScannerViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *scanMagicNumber;
@property (nonatomic, weak, readonly) UILabel *infoLabel;

- (BOOL)startReading;
- (void)stopReading;

/// show success view with a green check mark in center.
- (void)showSuccessView;

@end
