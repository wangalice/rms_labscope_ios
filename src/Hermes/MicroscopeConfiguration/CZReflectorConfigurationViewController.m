//
//  CZReflectorConfigurationViewController.m
//  Hermes
//
//  Created by Li, Junlin on 2/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReflectorConfigurationViewController.h"
#import "CZGridView.h"
#import "CZPickerViewController.h"
#import "CZNumberedButton.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@interface CZReflectorConfigurationViewController ()

@property (nonatomic, readonly, strong) CZGridView *reflectorPositionButtonGridView;

@end

@implementation CZReflectorConfigurationViewController

@synthesize reflectorPositionButtonGridView = _reflectorPositionButtonGridView;

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _microscopeModel = microscopeModel;
        self.title = L(@"MIC_CONFIG_FILTER_SET_SELECTION");
    }
    return self;
}

- (void)setMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    _microscopeModel = microscopeModel;
    [self reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.reflectorPositionButtonGridView];
}

- (void)reloadData {
    [self.reflectorPositionButtonGridView removeAllContentViews];
    
    for (NSInteger position = 0; position < self.microscopeModel.reflector.positions; position++) {
        CZFilterSet *filterSet = [self.microscopeModel.reflector filterSetAtPosition:position];
        CZNumberedButton *button = [[CZNumberedButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 344.0, 48.0) number:position + 1];
        button.tag = position;
        [button setTitle:filterSet.filterSetName forState:UIControlStateNormal];
        [button addTarget:self action:@selector(reflectorPositionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        button.isAccessibilityElement = YES;
        button.accessibilityIdentifier = @"FilterSetSelectionButton";
        
        [self.reflectorPositionButtonGridView addContentView:button];
    }
    
    [self setCurrentPosition:self.microscopeModel.reflector.currentPosition];
}

- (void)setCurrentPosition:(NSUInteger)position {
    for (UIButton *button in self.reflectorPositionButtonGridView.contentViews) {
        button.selected = (button.tag == position);
    }
}

#pragma mark - Views

- (CZGridView *)reflectorPositionButtonGridView {
    if (_reflectorPositionButtonGridView == nil) {
        CZGridRow *row = [[CZGridRow alloc] initWithHeight:48.0 alignment:CZGridRowAlignmentFill];
        NSArray<CZGridRow *> *rows = [NSArray cz_arrayWithRepeatedObject:row count:4];
        
        CZGridColumn *column = [[CZGridColumn alloc] initWithWidth:344.0 alignment:CZGridColumnAlignmentFill];
        NSArray<CZGridColumn *> *columns = [NSArray cz_arrayWithRepeatedObject:column count:2];
        
        _reflectorPositionButtonGridView = [[CZGridView alloc] initWithFrame:CGRectMake(32.0, 32.0, 704.0, 240.0) rows:rows columns:columns];
        _reflectorPositionButtonGridView.rowSpacing = 16.0;
        _reflectorPositionButtonGridView.columnSpacing = 16.0;
    }
    return _reflectorPositionButtonGridView;
}

#pragma mark - Actions

- (void)reflectorPositionButtonAction:(CZButton *)button {
    NSInteger position = button.tag;
    NSArray<CZFilterSet *> *availableFilterSets = self.microscopeModel.reflector.availableFilterSets;
    CZFilterSet *selectedFilterSet = [self.microscopeModel.reflector filterSetAtPosition:position];
    
    CZPickerViewController<CZFilterSet *> *picker = [[CZPickerViewController alloc] initWithSelectedItem:selectedFilterSet selectableItems:availableFilterSets];
    picker.selectedItemTitleLabel.text = [NSString stringWithFormat:L(@"MIC_CONFIG_CURRENT_FILTER_SET"), position + 1];
    picker.selectableItemsTitleLabel.text = L(@"MIC_CONFIG_AVAILABLE_FILTER_SET");
    
    picker.titleForItem = ^NSString *(CZPickerViewController<CZFilterSet *> *picker, CZFilterSet *item) {
        return item.displayName;
    };
    
    @weakify(self);
    [picker setHandler:^(CZPickerViewController<CZFilterSet *> *picker, CZDialogAction *action) {
        @strongify(self);
        [self.microscopeModel.reflector setFilterSet:picker.selectedItem atPosition:position];
        [self reloadData];
    } forActionWithIdentifier:CZPickerViewControllerSaveActionIdentifier];
    
    [self presentViewController:picker animated:YES completion:nil];
}

@end
