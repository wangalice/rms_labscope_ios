//
//  CZMicroscopeConfigurationBarcodeViewController.h
//  Hermes
//
//  Created by Li, Junlin on 2/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMicroscopeModel;

@interface CZMicroscopeConfigurationBarcodeViewController : UIViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel magicNumber:(NSString *)magicNumber NS_DESIGNATED_INITIALIZER;

@end
