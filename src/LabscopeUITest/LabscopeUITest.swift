//
//  LabscopeUITest.swift
//  LabscopeUITest
//
//  Created by eva on 11/27/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

import XCTest

class LabscopeUITest: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWisionSnapping() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCUIDevice.shared().orientation = .faceUp
        
        let app = XCUIApplication()
        var index : Int32 = 0
        
        app.buttons["Wision"].tap()
        
        for _ in (0 ..< 500) {
            autoreleasepool {
                app.buttons["snap"].tap()
                print("Snapping time: \(index)")
                app.buttons["liveTab"].tap()
            }
            
            index += 1
        }
        XCTAssertEqual(index, 500, "Snapping 500 times failed!")
    }
    
    func testShowQRCode() {
        let app = XCUIApplication()
        // launch camera and switch to live tab
        app.buttons["Wision"].tap()

        print("begin pressure test of showing QR code")
                
        var index : Int32 = 0
        for _ in (0 ..< 500) {
            autoreleasepool {
                app.buttons["live.button.config"].tap()
                
                let configureMicroscopeNavigationBar = app.navigationBars["Configure Microscope"]
                configureMicroscopeNavigationBar.buttons["Show configuration bar code"].tap()
                
                // back 1
                app.navigationBars["Configuration bar code - Wision"].buttons["Configure Microscope"].tap()
                
                // back 2
                configureMicroscopeNavigationBar.children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
                
                print("loop time: \(index)\n")
            }
            
            index += 1
        }
        
        XCTAssertEqual(index, 500, "Pressure test of showing QR code failed!")
    }
}
