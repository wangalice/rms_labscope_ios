#!/usr/bin/python
# -*- coding: utf-8 -*-

# usage: go to source code directory and run: python .../scanObjcPropertyLeak.py

import re
import os

Const_Objc_Ext = ['.m', '.mm']
Const_Exclude_list = ['CZFFmpegCamera.m', 'CZTemplatePreviewManager.m',
                      'GCDAsyncUdpSocket.m']


def scanPropertyInFile(filepath):
    pattern = re.compile(r'[\s]*@property[^\(]*\(([^\)]*)\)[^\*]*\*[\s]*(\w+)')

    notAssignPattern = re.compile(r'[^\(]*assign')

    arr = []

    with open(filepath, 'r') as f:
        line = f.readline()
        while line:
            match = pattern.match(line)
            if match:
                match2 = notAssignPattern.match(match.groups()[0])
                if not match2:
                    arr.append(match.groups()[1])
            line = f.readline()

    return arr


def scanReleaseProperty(property, filepath):
    deallocPattern = re.compile(r'[^\*]*dealloc')

    regStr = r"[^\*]*" + property + r" (release|invalidate|= nil)"
    pattern = re.compile(regStr)

    regStr = r"[^\*]*FreeObj\([_]{0,1}" + property + r"\)"
    pattern2 = re.compile(regStr)

    with open(filepath, 'r') as f:
        line = f.readline()
        while line:
            match = pattern.match(line)
            if match:
                f.close()
                return
            else:
                match = pattern2.match(line)
                if match:
                    f.close()
                    return
            line = f.readline()

    print property + ' might leak.'


def scanFile(filename, ext):
    arr2 = []
    if os.path.isfile(filename + '.h'):
        arr2 = scanPropertyInFile(filename + '.h')
    arr = scanPropertyInFile(filename + ext)
    arr.extend(arr2)

    arr = list(set(arr))

    print '------ begin scan properties in' + filename + ext + ':'
    print arr
    for p in arr:
        scanReleaseProperty(p, filename + ext)
    print '------ end scan properties.'


def scanDir(curDir):
    for item in os.listdir(curDir):
        subpath = os.path.join(curDir, item)
        if os.path.isfile(subpath):
            if subpath and os.path.splitext(subpath)[1] \
                in Const_Objc_Ext and item not in Const_Exclude_list:
                scanFile(os.path.splitext(subpath)[0],
                         os.path.splitext(subpath)[1])
        elif os.path.isdir(subpath):
            scanDir(subpath)


scanDir(os.getcwd())

