#!/usr/bin/python
# -*- coding: utf-8 -*-

# usage: go to destination directory and run: python .../scanExposureTime.py
#                                     or run: python scanExposureTime.py [Destination Directory]
# You need install exiv2 to run this script

import re
import os
import sys

Const_JPEG_Ext = ['.jpg', '.jpeg', '.JPG', '.JPEG']

def scanFile(filename, ext):
    command = 'exiv2 -K Exif.Photo.ExposureTime pr ' + filename + ext
    print(command)
    os.system(command)

def scanDir(curDir):
    for item in os.listdir(curDir):
        subpath = os.path.join(curDir, item)
        if os.path.isfile(subpath):
            if subpath and os.path.splitext(subpath)[1] \
                in Const_JPEG_Ext:
                scanFile(os.path.splitext(subpath)[0],
                         os.path.splitext(subpath)[1])
        # deep scan
        # elif os.path.isdir(subpath):
        #     scanDir(subpath)

if len(sys.argv) >= 1:
    scanDir(sys.argv[1])
else:
	scanDir(os.getcwd())

