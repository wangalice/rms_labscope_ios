//
//  main.m
//  Matscope iPhone
//
//  Created by Sherry Xu on 5/7/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
