//
//  AppDelegate.m
//  Matscope iPhone
//
//  Created by Sherry Xu on 5/7/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window;

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

@end
