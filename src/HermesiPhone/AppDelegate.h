//
//  AppDelegate.h
//  Matscope iPhone
//
//  Created by Sherry Xu on 5/7/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@end
