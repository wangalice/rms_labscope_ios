//
//  CZLocalFileListViewControllerIPhone.m
//  Matscope iPhone
//
//  Created by Ralph Jin on 5/13/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZLocalFileListViewControllerIPhone.h"

#import "CZCommonUtils.h"
#import "CZFileEntity.h"
#import "CZFileListManager.h"
#import "CZFileTableViewCell.h"
#import "CZFileThumbnail.h"

#import "CZImageViewControllerIPhone.h"

@interface CZLocalFileListViewControllerIPhone () <CZFileThumbnailDelegate, CZFileOperations>

@property (nonatomic, weak) CZLocalFileListManager *localFileListManager;
@property (nonatomic, weak) UIView *backgroundView;
@property (nonatomic, assign) BOOL hideAccessoryButton;

@end

static NSString * const kTextCellIdentifier = @"LocalFileCell";

@implementation CZLocalFileListViewControllerIPhone

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.localFileListManager = [CZFileListManager sharedInstance].localList;
    self.localFileListManager.delegate = self;
    [self.localFileListManager loadFiles];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerClass:[CZFileTableViewCell class] forCellReuseIdentifier:kTextCellIdentifier];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    self.backgroundView = backgroundView;
    [self.backgroundView setBackgroundColor:[UIColor lightGrayColor]];
    self.backgroundView.alpha = 0.0;
    [self.view addSubview:self.backgroundView];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:L(@"FILES_PROGRESS_REFRESH_TIP")];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private

- (void)refreshView:(UIRefreshControl *)refreshControl {
    [self.localFileListManager loadFiles];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.bounces = (scrollView.contentOffset.y < 1); // only allow bouncing at top
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.localFileListManager.visibleFiles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileEntity *fileEntity = self.localFileListManager.visibleFiles[indexPath.row];
    CZFileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTextCellIdentifier forIndexPath:indexPath];
    
    [cell setWithFileEntity:fileEntity thumbnailBlock:^(CZFileEntity *fileEntiy) {
        return [self.localFileListManager thumbnailFrom:fileEntity];
    }];
    
    NSString *fileInfoText = [CZFileTabCommon newFileInfoLabel:fileEntity.fileSize
                                                  modifiedDate:fileEntity.modificationDate];
    cell.fileInfoLabel.text = fileInfoText;
    cell.accessoryButton.hidden = self.hideAccessoryButton;
    cell.backgroundColor = self.view.backgroundColor;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CZTableViewCellHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileEntity *fileEntity = self.localFileListManager.visibleFiles[indexPath.row];
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[fileEntity.fileName pathExtension]];
    if (fileFormat == kCZFileFormatCZI ||
        fileFormat == kCZFileFormatJPEG ||
        fileFormat == kCZFileFormatPDF ||
        fileFormat == kCZFileFormatCSV ||
        fileFormat == kCZFileFormatMP4 ||
        fileFormat == kCZFileFormatTIF) {
//        NSString *filePath = fileEntity.filePath;
        
//        if ([self.delegate respondsToSelector:@selector(controller:didRequestOpeningFileAtPath:)]) {
//            [self.delegate controller:self didRequestOpeningFileAtPath:filePath];
//        }
        UIViewController *parentVC = self.parentViewController;
        if ([parentVC isKindOfClass:[UITabBarController class]]) {
            [(UITabBarController *)parentVC setSelectedIndex:2];
            UIViewController *imageViewController = [(UITabBarController *)parentVC viewControllers][2];
            
            [(CZImageViewControllerIPhone *)imageViewController presentImageFile:fileEntity.filePath];
        }
    } else if (fileFormat == kCZFileFormatCZFTP) {
//        CZFileNameTemplateController *controller = [[CZFileNameTemplateController alloc] init];
//        controller.selectedFileName = fileEntity.fileName;
//        
//        CZAppDelegate *app = [CZAppDelegate get];
//        [app.navigationController pushViewController:controller animated:YES];
//        
//        [controller release];
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CZFileThumbnailDelegate

- (void)thumbnail:(UIImage *)thumbnail didGenerateFrom:(NSString *)path {
    for (CZFileTableViewCell *cell in self.tableView.visibleCells) {
        if ([path isEqualToString:cell.fileEntity.filePath]) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            if (indexPath) {
                NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
                [self.tableView beginUpdates];
                [self.tableView reloadRowsAtIndexPaths:@[indexPath2]
                                      withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
            }
            break;
        }
    }
}

#pragma mark - CZFileOperations methods

- (void)setSelected:(BOOL)isSelected forFile:(CZFileEntity *)fileEntity {
    [self.localFileListManager setSelected:isSelected forFile:fileEntity];
    [[CZFileListManager sharedInstance] selectFile:fileEntity];
    
//    self.hideAccessoryButton = TRUE;
    [self.tableView reloadData];
    
//    if ([self.delegate respondsToSelector:@selector(controller:didChangeSelectionWithFiles:)]) {
//        [self.delegate controller:self didChangeSelectionWithFiles:self.localFileListManager.pendingFiles];
//    }
}

- (void)transferSingleFile:(CZFileEntity *)fileEntity andOpen:(BOOL)opensAfterFinish {
//    if ([self.delegate respondsToSelector:@selector(controller:didRequestTransferForFile:andOpen:)]) {
//        [self.delegate controller:self didRequestTransferForFile:fileEntity andOpen:opensAfterFinish];
//    }
    [[CZFileListManager sharedInstance] transferFilesRemovingSource:NO];
}

- (void)copyFile:(CZFileEntity *)source to:(CZFileEntity *)target {
//    if (source.storageType != kLocal) {
//        return;
//    }
    
//    self.state = kBusy;
    
    [self.localFileListManager copyFile:source to:target];
}

- (void)moveFile:(CZFileEntity *)source to:(CZFileEntity *)target {
//    if (source.storageType != kLocal) {
//        return;
//    }
    
//    self.state = kBusy;
    
    [self.localFileListManager moveFile:source to:target];
}

- (void)removeFile:(CZFileEntity *)fileEntity {
    [self.localFileListManager removeFile:fileEntity];
}

- (void)setSelectedForAllFiles:(BOOL)isSelected {
    [self.localFileListManager setSelectedForAllFiles:isSelected];
    if (isSelected) {
        [[CZFileListManager sharedInstance] selectFiles:self.localFileListManager.visibleFiles];
    } else {
        [[CZFileListManager sharedInstance] clearSelected];
    }
    [self.tableView reloadData];
    
//    if ([self.delegate respondsToSelector:@selector(controller:didChangeSelectionWithFiles:)]) {
//        [self.delegate controller:self didChangeSelectionWithFiles:self.localFileListManager.pendingFiles];
//    }
}

@end
