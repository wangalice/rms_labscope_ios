//
//  CZLocalFileListViewControllerIPhone.m
//  Matscope iPhone
//
//  Created by Ralph Jin on 5/13/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZRemoteFileListViewControllerIPhone.h"

#import <WNFSFramework/CZWNFSManagerDelegate.h>

#import "CZCommonUtils.h"
#import "CZFileEntity.h"
#import "CZFileListManager.h"
#import "CZFileTableViewCell.h"
#import "CZFileThumbnail.h"
#import "CZDefaultSettings.h"

@interface CZRemoteFileListViewControllerIPhone () <CZFileThumbnailDelegate, CZWNFSManagerDelegate, CZFileOperations>

@property (nonatomic, weak) CZRemoteFileListManager *remoteFileListManager;
@property (nonatomic, weak) UIView *backgroundView;
@property (nonatomic, assign) BOOL hideAccessoryButton;

@end

static NSString * const kTextCellIdentifier = @"LocalFileCell";

@implementation CZRemoteFileListViewControllerIPhone

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsServerSetup];
    
    self.remoteFileListManager = [CZFileListManager sharedInstance].remoteList;
    self.remoteFileListManager.delegate = self;
    [self.remoteFileListManager setupServerPath];
    [self.remoteFileListManager loadFilesFromServer];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerClass:[CZFileTableViewCell class] forCellReuseIdentifier:kTextCellIdentifier];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    self.backgroundView = backgroundView;
    [self.backgroundView setBackgroundColor:[UIColor lightGrayColor]];
    self.backgroundView.alpha = 0.0;
    [self.view addSubview:self.backgroundView];
    
    UIRefreshControl *serverRefreshControl = [[UIRefreshControl alloc] init];
    serverRefreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:L(@"FILES_PROGRESS_REFRESH_TIP")];
    [serverRefreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = serverRefreshControl;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.bounces = (scrollView.contentOffset.y < 1); // only allow bouncing at top
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.remoteFileListManager.visibleFiles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTextCellIdentifier forIndexPath:indexPath];
    CZRemoteFileEntity *fileEntity = self.remoteFileListManager.visibleFiles[indexPath.row];
    
    [cell setWithFileEntity:fileEntity thumbnailBlock:^UIImage *(CZFileEntity *fileEntiy) {
        UIImage *thumbnail = [self.remoteFileListManager thumbnailFrom:fileEntity];
        return thumbnail;
    }];
    
    if (fileEntity.isFolder) {
        cell.thumbnailView.image = [UIImage imageNamed:A(@"Icons/folder.png")];
        cell.selectImageView.image = nil;
        cell.fileLabel.text = fileEntity.fileName;
        cell.fileInfoLabel.text = nil;
        cell.selectButton.hidden = YES;
        cell.accessoryButton.hidden = YES;
    } else {
        cell.fileLabel.text = fileEntity.fileName;
        
        NSString *fileInfoText = [CZFileTabCommon newFileInfoLabel:fileEntity.fileSize
                                                      modifiedDate:fileEntity.modificationDate];
        
        cell.fileInfoLabel.text = fileInfoText;
        cell.selectButton.hidden = NO;
        cell.accessoryButton.hidden = self.hideAccessoryButton;
    }
    
    cell.fileLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = self.view.backgroundColor;
    
    return cell;
}

#pragma mark - private

- (void)refreshView:(UIRefreshControl *)refresh {
@autoreleasepool {
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:L(@"FILES_PROGRESS_REFRESH")];
    
    [self.remoteFileListManager loadFilesFromServer];
    
    NSDate *lastUpdatedDate = self.remoteFileListManager.lastUpdateDate;
    
    NSString *lastUpdateTime = [NSDateFormatter localizedStringFromDate:lastUpdatedDate
                                                              dateStyle:NSDateFormatterShortStyle
                                                              timeStyle:NSDateFormatterShortStyle];
    NSString *lastUpdated = [NSString stringWithFormat:L(@"FILES_PROGRESS_REFRESH_STATUS"), lastUpdateTime];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
}
}

// workaround for go back to parent folder.
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if ( event.subtype == UIEventSubtypeMotionShake ) {
        [self.remoteFileListManager navigateToParentFolder];
        [self.remoteFileListManager loadFilesFromServer];
    }
    
    [super motionEnded:motion withEvent:event];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark - table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CZTableViewCellHeight;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CZFileEntity *fileEntity = self.remoteFileListManager.visibleFiles[indexPath.row];
    if (fileEntity.isFolder) {
        NSString *newPath = [NSString stringWithFormat:@"%@%@/", self.remoteFileListManager.sharePath, fileEntity.fileName];
        self.remoteFileListManager.sharePath = newPath;
        [self.remoteFileListManager loadFilesFromServer];
    } else {
        [self.remoteFileListManager navigateToParentFolder];
        [self.remoteFileListManager loadFilesFromServer];
    }
    
//    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:[fileEntity.fileName pathExtension]];
//    if (fileFormat == kCZFileFormatCZI ||
//        fileFormat == kCZFileFormatJPEG ||
//        fileFormat == kCZFileFormatPDF ||
//        fileFormat == kCZFileFormatCSV ||
//        fileFormat == kCZFileFormatMP4 ||
//        fileFormat == kCZFileFormatTIF) {
//    } else if (fileFormat == kCZFileFormatCZFTP) {
//    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark - CZFileThumbnailDelegate

- (void)thumbnail:(UIImage *)thumbnail didGenerateFrom:(NSString *)path {
    for (CZFileTableViewCell *cell in self.tableView.visibleCells) {
        if ([path isEqualToString:cell.fileEntity.filePath]) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            if (indexPath) {
                [self.tableView beginUpdates];
                [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                                      withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
            }
            break;
        }
    }
}

#pragma mark - CZWNFSManagerDelegate
- (void)operateDidFinishItem:(NSDictionary *)item type:(CZOperateType) type {
    [self.refreshControl endRefreshing];
    
    if (type != CZOperateTypeListDirectory) {
        return;
    }
    
//    self.state = kFree;
//    self.canEdit = TRUE;
//    [self.loadingActivity stopAnimating];
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    self.backgroundView.alpha = 0.0;
    [self.view sendSubviewToBack:self.backgroundView];
    
    if (item == nil || [[item allValues] count] < 1) {
        return;
    }
    
    self.hideAccessoryButton = FALSE;
    [self.tableView reloadData];
    
    if (self.remoteFileListManager.prevSharePath &&
        ![self.remoteFileListManager.prevSharePath isEqualToString:self.remoteFileListManager.sharePath] &&
        self.remoteFileListManager.visibleFiles.count > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
    
//    if ([self.delegate respondsToSelector:@selector(controller:didChangeRemotePath:)]) {
//        [self.delegate controller:self didChangeRemotePath:self.remoteFileListManager.sharePath];
//    }
//    
//    if ([self.delegate respondsToSelector:@selector(controller:didUpdateRemoteStatus:errorDescription:)]) {
//        [self.delegate controller:self didUpdateRemoteStatus:YES errorDescription:nil];
//    }
}

- (void)operateDidFailWithError:(NSError *)error {
//    self.state = kFree;
//    self.canEdit = TRUE;
//    [self.loadingActivity stopAnimating];
//    self.backgroundView.alpha = 0.0;
//    [self.view sendSubviewToBack:self.backgroundView];
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    
    [self.tableView reloadData];
}

#pragma mark - CZFileOperations methods

- (void)setSelected:(BOOL)isSelected forFile:(CZFileEntity *)fileEntity {
    [self.remoteFileListManager setSelected:isSelected forFile:fileEntity];
    
    [self.tableView reloadData];
}

- (void)transferSingleFile:(CZFileEntity *)fileEntity andOpen:(BOOL)opensAfterFinish {
    [[CZFileListManager sharedInstance] selectFile:fileEntity];
    [[CZFileListManager sharedInstance] transferFilesRemovingSource:NO];
}

- (void)copyFile:(CZFileEntity *)source to:(CZFileEntity *)target {
    if (source.storageType != kRemote || source.isFolder) {
        return;
    }

    [self.remoteFileListManager copyFile:source to:target];
}

- (void)moveFile:(CZFileEntity *)source to:(CZFileEntity *)target {
    if (source.storageType != kRemote || source.isFolder) {
        return;
    }

    [self.remoteFileListManager moveFile:source to:target];
}

- (void)removeFile:(CZFileEntity *)fileEntity {
    [self.remoteFileListManager removeFile:fileEntity];
}

- (void)setSelectedForAllFiles:(BOOL)isSelected {
    [self.remoteFileListManager setSelectedForAllFiles:isSelected];
    
    [self.tableView reloadData];
}

@end
