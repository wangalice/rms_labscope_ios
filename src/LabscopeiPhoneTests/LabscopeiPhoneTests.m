//
//  LabscopeiPhoneTests.m
//  LabscopeiPhoneTests
//
//  Created by Sherry Xu on 10/8/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LabscopeiPhoneTests : XCTestCase

@end

@implementation LabscopeiPhoneTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
