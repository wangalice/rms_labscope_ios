//
//  CZSMBBrowser.h
//  FileKit
//
//  Created by Halley Gu on 7/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZSMBBrowser;

@protocol CZSMBBrowserDelegate <NSObject>

@optional
- (void)smbBrowser:(CZSMBBrowser *)browser didFindServerWithIpAddress:(NSString *)ipAddress name:(NSString *)name;

@end

@interface CZSMBBrowser : NSObject

@property (nonatomic, assign) id<CZSMBBrowserDelegate> delegate;

- (void)startBrowsing;
- (void)stopBrowsing;

@end
