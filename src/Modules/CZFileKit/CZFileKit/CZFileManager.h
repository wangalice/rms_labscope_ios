//
//  CZFileManager.h
//  FileKit
//
//  Created by Ralph Jin on 3/31/14.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/** file update or new created notification*/
extern NSString * const kCZFileUpdateNotification;
extern NSString * const kCZFileUpdateNotificationFileList;
extern NSString * const kCZFileUpdateNotificationIsImageNew;

extern NSString * const kCZFileRemoveNotification;
extern NSString * const kCZFileRemoveNotificationFileList;

/** A wrapper class of NSFileManager
 * When modify or delete file in success, it will boardcast notification.
 */
@interface CZFileManager : NSObject

+ (CZFileManager *)defaultManager;

- (BOOL)moveItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error;

- (BOOL)removeItemAtPath:(NSString *)path error:(NSError **)error;

// TODO: add similar methods as NSFileManager

/** deeply copy all the files and sub-directories from source directory to destination directory without override.*/
- (BOOL)mergeDirectory:(NSString *)srcDirectory
           toDirectory:(NSString *)destDirectory
             recursive:(BOOL)recursive
                 error:(NSError **)error;

@end
