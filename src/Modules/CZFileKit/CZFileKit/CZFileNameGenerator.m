//
//  CZFileNameGenerator.m
//  FileKit
//
//  Created by Ralph Jin on 5/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameGenerator.h"
#import <CZToolbox/CZToolbox.h>

const static int kDefaultDigitCount = 3;  // default digits count for auto number field
const static int kDefaultAutoDigitCount = 2;  // default digits count when there's no auto number field

const static NSString * const kDefaultDateFormat = @"yyyy-MM-dd";

// TODO: consider the situation auto number doesn't exist.

@implementation CZFileNameGenerator

+ (NSString *)uniqueFilePath:(NSString *)filePath {
    NSString *parentPath = [filePath stringByDeletingLastPathComponent];
    NSString *extension = [filePath pathExtension];
    NSString *fileName = [[filePath lastPathComponent] stringByDeletingPathExtension];
    
    CZFileNameTemplateField *field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeFreeText value:fileName];
    
    CZFileNameTemplate *template = [[CZFileNameTemplate alloc] init];
    [template addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:template];
    generator.extension = extension;
    [template release];
    
    NSString *newfileName = [generator uniqueFileNameInDirectory:parentPath];
    [generator release];
    
    return [parentPath stringByAppendingPathComponent:newfileName];
}

+ (NSString *)uniqueFilePath:(NSString *)filePath from:(NSArray *)fileNames {
    NSString *parentPath = [filePath stringByDeletingLastPathComponent];
    NSString *extension = [filePath pathExtension];
    NSString *fileName = [[filePath lastPathComponent] stringByDeletingPathExtension];
    
    CZFileNameTemplateField *field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeFreeText value:fileName];
    
    CZFileNameTemplate *template = [[CZFileNameTemplate alloc] init];
    [template addField:field];
    [field release];
    
    CZFileNameGenerator *generator = [[CZFileNameGenerator alloc] initWithTemplate:template];
    generator.extension = extension;
    [template release];
    
    NSString *newfileName = [generator uniqueFileNameFromNames:fileNames];
    [generator release];
    
    return [parentPath stringByAppendingPathComponent:newfileName];

}

+ (NSArray *)dateTimeFormatOptions {
    return @[kDefaultDateFormat,
             L(@"FNT_DATE_TIME_FORMAT_1"),
             L(@"FNT_DATE_TIME_FORMAT_2"),
             L(@"FNT_DATE_TIME_FORMAT_3"),
             L(@"FNT_DATE_TIME_FORMAT_4")];
}

- (id)init {
    return [self initWithTemplate:nil];
}

- (id)initWithTemplate:(CZFileNameTemplate *)fileNameTemplate {
    self = [super init];
    if (self) {
        _fileNameTemplate = [fileNameTemplate retain];
    }
    
    return self;
}

- (void)dealloc {
    [_fileNameTemplate release];
    [_operatorName release];
    [_microscope release];
    [_objective release];
    [_captureDate release];
    [_extension release];
    [super dealloc];
}

- (NSString *)uniqueFileNameInDirectory:(NSString *)path {
    return [self uniqueFileNameInDirectory:path excludedName:nil];
}

- (NSString *)uniqueFileNameInDirectory:(NSString *)path excludedName:(NSString *)excludedName {
    if (self.fileNameTemplate == nil) {
        return nil;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    excludedName = [excludedName stringByDeletingPathExtension];
    
    char utf8str[4096];
    if ([excludedName getFileSystemRepresentation:utf8str maxLength:4096]) {
        excludedName = [fileManager stringWithFileSystemRepresentation:utf8str length:strlen(utf8str)];
    }
    
    NSMutableArray *fileNames = [NSMutableArray array];
    
    
    NSDirectoryEnumerator *dirEnum = [fileManager enumeratorAtPath:path];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        NSString *fileNameWithoutExtension = [file stringByDeletingPathExtension];
        if (! [fileNameWithoutExtension isEqualToString:excludedName]) {
            [fileNames addObject:file];
        }
    }
    
    return [self uniqueFileNameFromNames:fileNames];
}

- (NSString *)uniqueFileNameFromNames:(NSArray *)fileNames {
    if (self.fileNameTemplate == nil) {
        return @"";
    }
    
    NSUInteger digitCount = [self scanAutoNumberDigitCount];
    BOOL hasAutoNumberField = digitCount != NSNotFound;

    if (digitCount == NSNotFound) {
        digitCount = kDefaultAutoDigitCount;
    }

    NSInteger digitNumber = 0;
    
    if (!hasAutoNumberField) {
        BOOL found = NO;
        NSString *fileName = [self formatOfFileName:@"" forRegex:NO];
        if (fileName.length) {
            for (NSString *sampleFile in fileNames) {
                sampleFile = [sampleFile stringByDeletingPathExtension];
                if ([fileName isEqualToString:sampleFile]) {
                    found = YES;
                    break;
                }
            }
            
            if (!found) {
                return (self.extension.length > 0) ? [fileName stringByAppendingPathExtension:self.extension] : fileName;
            }
            
            digitNumber = 1;
        }
    }
    
    // create regular expression for match file name
    NSRegularExpression *regex = nil;
    NSString *tempString = [self formatOfFileName:@"(\\d{1,})" forRegex:YES];
    
    NSMutableString *expressionString = [NSMutableString stringWithString:tempString];
    
    [expressionString insertString:@"^" atIndex:0];
    [expressionString appendString:@"$"];
    
    NSError *error = NULL;
    regex = [NSRegularExpression regularExpressionWithPattern:expressionString
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    if (error) {
        CZLogv(@"Filename generator regular expression error: %@", [error localizedDescription]);
    }

    if (regex) {
        // scan each file name and calculate max index
        for (NSString *sampleFile in fileNames) {
            sampleFile = [sampleFile stringByDeletingPathExtension];
            NSTextCheckingResult *match = [regex firstMatchInString:sampleFile
                                                            options:0
                                                              range:NSMakeRange(0, [sampleFile length])];
            if (match && [match numberOfRanges] > 1) {
                NSRange matchRange = [match rangeAtIndex:1];
                NSString *matchNumber = [sampleFile substringWithRange:matchRange];
                NSInteger i = [matchNumber integerValue];
                digitNumber = MAX(i, digitNumber);
            }
        }
        
        // generate file name with next number to maxIndex
        NSString *number;
        NSString *numberFomat = [NSString stringWithFormat:@"%%0%lud", (unsigned long)digitCount];  // for exmaple "%3d", if digitCount == 3
        number = [NSString stringWithFormat:numberFomat, digitNumber + 1];
        
        NSString *newFileName = [self formatOfFileName:number forRegex:NO];
        return (self.extension.length > 0) ? [newFileName stringByAppendingPathExtension:self.extension] : newFileName;
    }
    
    return nil;
}

/**
 * @return a sample file name, for example "FreeText_2013-05-07_Foo_Bar_###"
 */
- (NSString *)sampleFileName {
    if (self.fileNameTemplate == nil) {
        return @"";
    }
    
    NSUInteger digitCount = [self scanAutoNumberDigitCount];
    BOOL hasAutoNumberField = digitCount != NSNotFound;
    
    NSMutableString *autoNumberString = [[NSMutableString alloc] init];
    if (hasAutoNumberField) {
        for (int i = 0; i < digitCount; ++i) {
            [autoNumberString appendString:@"#"];
        }
    }
    
    NSString *formatString = [self formatOfFileName:autoNumberString forRegex:NO];
    [autoNumberString release];
    
    return formatString;
}

- (void)useDefaultValueWhenEmpty {
    if (self.operatorName.length == 0) {
        self.operatorName = @"UnknownOperator";
    }
    
    if (self.microscope.length == 0) {
        self.microscope = @"UnknownMicroscope";
    }
    
    if (self.objective.length == 0) {
        self.objective = @"0x";
    }
}

#pragma mark - Private methods

/**
 Combine all the fields to a string, except auto number field.
 Auto number field will be leaved as "%@" for formatting.
 @param autoNumberFormatString append string for auto number part; e.g. ###
 @return a format string, for example "FreeText_2013-05-07_Foo_Bar_###"
 */
- (NSString *)formatOfFileName:(NSString *)autoNumberFormatString forRegex:(BOOL)forRegex {
    BOOL autoNumberAppended = NO;
    NSMutableString *formatString = [NSMutableString string];
    for (NSUInteger i = 0; i < [_fileNameTemplate fieldCount]; ++i) {
        CZFileNameTemplateField *field = [_fileNameTemplate fieldAtIndex:i];
        BOOL appendSeperator = (i != [_fileNameTemplate fieldCount] - 1);
        
        switch (field.type) {
            case CZFileNameTemplateFieldTypeAutoNumber: {
                if (!autoNumberAppended) {
                    autoNumberAppended = YES;
                    [formatString appendString:autoNumberFormatString];
                } else {
                    appendSeperator = NO;
                }
                break;
            }
            case CZFileNameTemplateFieldTypeFreeText: {
                if ([field.value length]) {
                    NSString *value = field.value;
                    value = [CZFileNameGenerator stringByRemovingIllegalChar:value];
                    if (forRegex) {
                        value = [NSRegularExpression escapedPatternForString:value];
                    }
                    [formatString appendString:value];
                } else {
                    appendSeperator = NO;
                }
                break;
            }
            case CZFileNameTemplateFieldTypeDate: {
                NSString *dateString = nil;
                if (self.captureDate) {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    NSUInteger i = [field.value integerValue];
                    NSArray *options = [CZFileNameGenerator dateTimeFormatOptions];
                    NSUInteger maxIndex = options.count - 1;
                    i = MIN(i, maxIndex);
                    NSString *formatStr = options[i];
                    [formatter setDateFormat:formatStr];
                    dateString = [formatter stringFromDate:self.captureDate];
                    [formatter release];
                } else {
                    dateString = @"UnknownDate";
                }
                
                if (dateString.length) {
                    if (forRegex) {
                        dateString = [NSRegularExpression escapedPatternForString:dateString];
                    }
                    [formatString appendString:dateString];
                } else {
                    appendSeperator = NO;
                }
                
                break;
            }
            case CZFileNameTemplateFieldTypeVariableText: {
                if ([@"Operator" isEqualToString:field.value]) {
                    if (self.operatorName.length) {
                        NSString *value = [CZFileNameGenerator stringByRemovingIllegalChar:self.operatorName];
                        if (forRegex) {
                            value = [NSRegularExpression escapedPatternForString:value];
                        }
                        
                        [formatString appendString:value];
                    } else {
                        appendSeperator = NO;
                    }
                } else if ([@"Microscope" isEqualToString:field.value]) {
                    if (self.microscope.length) {
                        NSString *value = [CZFileNameGenerator stringByRemovingIllegalChar:self.microscope];
                        if (forRegex) {
                            value = [NSRegularExpression escapedPatternForString:value];
                        }
                        
                        [formatString appendString:value];
                    } else {
                        appendSeperator = NO;
                    }
                } else if ([@"Objective" isEqualToString:field.value]) {
                    NSString *value = (self.objective.length == 0) ? @"0x" : self.objective;
                    value = [CZFileNameGenerator stringByRemovingIllegalChar:value];
                    if (forRegex) {
                        value = [NSRegularExpression escapedPatternForString:value];
                    }
                    
                    [formatString appendString:value];
                }
                break;
            }
            default:
                break;
        }
        
        // append separator
        if (appendSeperator) {
            [formatString appendString:_fileNameTemplate.separator];
        }
    }
    
    NSCharacterSet *seperatorSet = [NSCharacterSet characterSetWithCharactersInString:_fileNameTemplate.separator];
    NSString *string = [formatString stringByTrimmingCharactersInSet:seperatorSet];
    
    if (!autoNumberAppended) {
        if (autoNumberFormatString.length) {
            if (forRegex) {
                [formatString setString:@"-"];
                [formatString appendString:autoNumberFormatString];
            } else {
                [formatString setString:@"-"];
                [formatString appendString:autoNumberFormatString];
            }
            
            string = [string stringByAppendingString:formatString];
        }
    }
    
    char utf8str[4096];
    if ([string getFileSystemRepresentation:utf8str maxLength:4096]) {
        string = [[NSFileManager defaultManager] stringWithFileSystemRepresentation:utf8str length:strlen(utf8str)];
    }

    return string;
}

/**
 * @return NSNotFound if there's no number field.
 */
- (NSUInteger)scanAutoNumberDigitCount {
    NSUInteger digitCount = NSNotFound;
    
    for (NSUInteger i = 0; i < [_fileNameTemplate fieldCount]; ++i) {
        CZFileNameTemplateField *field = [_fileNameTemplate fieldAtIndex:i];
        if (field.type == CZFileNameTemplateFieldTypeAutoNumber) {
            NSInteger i = [field.value integerValue];
            if (i > 0) {
                digitCount = i;
            } else {
                digitCount = kDefaultDigitCount;
            }
            break;
        }
    }
    
    return digitCount;
}

+ (NSString *)stringByRemovingIllegalChar:(NSString *)string {
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"?*:|<>/\"\\"];
    return [[string componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString:@""];
}

@end
