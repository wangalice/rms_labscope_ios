//
//  UIImage+EXIFThumbnail.h
//  FileKit
//
//  Created by Ralph Jin on 5/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (EXIFThumbnail)

- (id)initThumbWithEXIFOfJPEGFile:(NSString *)path;

@end
