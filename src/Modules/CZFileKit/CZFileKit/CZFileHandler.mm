//
//  CZFileHandler.mm
//  FileKit
//
//  Created by Mike Wang on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZFileHandler.h"
#import <CZToolbox/CZToolbox.h>
#import <WNFSFramework/CZFileDescriptor.h>
#import <WNFSFramework/CZSMBAdapter.h>
#import <WNFSFramework/CZWNFSManager.h>

typedef NS_ENUM(NSUInteger, CZFileType) {
    kLocal,
    kRemote,
    kUnknown
};

class CZFileHandler::CZFileHandlerWrap
{
public:
    CZFileHandlerWrap() : _handler(nil), _type(kUnknown) {};
    ~CZFileHandlerWrap();
    
    id _handler;
    CZFileType _type;
};

CZFileHandler::CZFileHandlerWrap::~CZFileHandlerWrap()
{
    if (_type == kRemote) {
        CZFileDescriptor *fileDescriptor = (CZFileDescriptor *)_handler;
        [fileDescriptor release];
    }
}

CZFileHandler::CZFileHandler() : _adapter(nil)
{
    _fileHandler = new CZFileHandlerWrap();
}

CZFileHandler::~CZFileHandler()
{
    delete _fileHandler;
    [_adapter release];
}

long CZFileHandler::openFile(const char *filePath, CZFileAttributeFlag fileOpenFlag)
{
    if (!filePath)
        return kInvalidArg;
    
#ifdef TARGET_OS_IPHONE
    NSString *path = [NSString stringWithUTF8String:filePath];
    if (![[path substringToIndex:2] isEqualToString:@"//"]) {
        NSFileHandle *handler = nil;
        switch (fileOpenFlag) {
            case kRDONLY:
                handler = [NSFileHandle fileHandleForReadingAtPath:path];
                break;
                
            case kWRONLY: {
                [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
                
                handler = [NSFileHandle fileHandleForWritingAtPath:path];
                break;
            }
                
            case kRDWR:
                handler = [NSFileHandle fileHandleForUpdatingAtPath:path];
                break;
                
            default:
                break;
        }
        
        if (handler == nil) {
            return kFailed;
        }
        else {
            _fileHandler->_type = kLocal;
            _fileHandler->_handler = handler;
            
            return kSuccess;
        }
    } else {
        CZSMBErrorCode code = kErrorCodeHostUnreachable;
        CZFileDescriptor *fileDesc = [[[CZWNFSManager sharedInstance] sharedSMBAdapter] openFile:[NSString stringWithUTF8String:filePath]
                                                                                         options:fileOpenFlag
                                                                                           error:&code];
        
        if ([CZSMBErrorHandler isError:code]) {
            CZLogv(@"openFile: %@", [CZSMBErrorHandler getErrorMessage:code]);
            return kFailed;
        }
        
        _fileHandler->_type = kRemote;
        [fileDesc retain];
        _fileHandler->_handler = fileDesc;
        
        if (fileDesc.endOfFile == 0) {
            CZLogv(@"error file size:%llu bytes", fileDesc.endOfFile);
            return kInvalidData;
        } else {
            CZLogv(@"file size:%llu bytes", fileDesc.endOfFile);
            return kSuccess;
        }
    }
#else
    return kInvalidPlatform;
#endif
}

long CZFileHandler::readFile(void *dataBuffer, size_t bufferSize) {
    if (!dataBuffer)
        return kInvalidArg;
    
#ifdef TARGET_OS_IPHONE
    static uint32_t kBlockBufferSize = 64 * 1024;  // 64 KB
    if (_fileHandler->_type == kRemote) {
        //refere CZSMBConsistent.h
        //kMaxReadDataBlockSize is set to 63K total for data not including header
        //with header will be 64K
        kBlockBufferSize = 63 * 1024;
    }
    
    @autoreleasepool {
        NSFileHandle *handler = (NSFileHandle *)(_fileHandler->_handler);
        NSUInteger remainSize = bufferSize;
        unsigned char *pDataBufferOffset = (unsigned char*)dataBuffer;
        while (remainSize) {
            @autoreleasepool {
                uint32_t blockSize = (remainSize > kBlockBufferSize) ? kBlockBufferSize : (uint32_t)remainSize;
                NSUInteger readLength = 0;
                if (_fileHandler->_type == kLocal) {
                    NSData *data = [handler readDataOfLength:blockSize];
                    if (data.length != 0) {
                        [data getBytes:pDataBufferOffset length:data.length];
                    }
                    
                    readLength = data.length;
                } else if (_fileHandler->_type == kRemote) {
                    CZSMBErrorCode code = kErrorCodeHostUnreachable;
                    CZFileDescriptor *fileDesc = (CZFileDescriptor *)_fileHandler->_handler;
                    CZSMBAdapter *smbAdapter = [[CZWNFSManager sharedInstance] sharedSMBAdapter];
                    if (smbAdapter) {
                        code = [smbAdapter readFile:fileDesc
                                             buffer:pDataBufferOffset
                                               size:blockSize];
                    }
                    
                    if ([CZSMBErrorHandler isError:code]) {
                        CZLogv(@"readFile: %@", [CZSMBErrorHandler getErrorMessage:code]);
                        break;
                    }
                    
                    fileDesc.offset += code;
                    readLength = code;
                } else {
                    return 0;
                }
                pDataBufferOffset += readLength;
                remainSize -= readLength;
                
                if (readLength != blockSize) {
                    CZLogv(@"readFile error: block size:%lu, read length:%ld", (unsigned long)blockSize, (long)readLength);
                    break;
                }
            }
        }
        return (pDataBufferOffset - (unsigned char*)dataBuffer);
    }
#else
    return kInvalidPlatform;
#endif
}

void CZFileHandler::writeFile(const void *inputBuffer, size_t bufferSize)
{
    if (!inputBuffer)
        return;
    
#ifdef TARGET_OS_IPHONE
    @autoreleasepool {
        if (_fileHandler->_type == kLocal) {
            NSFileHandle *handler = (NSFileHandle *)(_fileHandler->_handler);
            NSData *data = [NSData dataWithBytes:inputBuffer length:bufferSize];
            [handler writeData:data];
        } else if (_fileHandler->_type == kRemote) {
            CZSMBErrorCode code = kErrorCodeHostUnreachable;
            CZFileDescriptor *fileDesc = (CZFileDescriptor *)_fileHandler->_handler;
            CZSMBAdapter *smbAdapter = [[CZWNFSManager sharedInstance] sharedSMBAdapter];
            code = [smbAdapter writeFile:fileDesc
                                  buffer:(Byte *)inputBuffer
                                    size:bufferSize];
            
            if ([CZSMBErrorHandler isError:code]) {
                CZLogv(@"writeFile: %@", [CZSMBErrorHandler getErrorMessage:code]);
            }else {
                CZLogv(@"writeFile: have write %lu bytes to file", (unsigned long)code);
            }
        }
    }
#else
    
#endif
}

uint64_t CZFileHandler::seekFile(int64_t fileOffset, CZIFileSeekMode seekMode)
{
#ifdef TARGET_OS_IPHONE
    if (_fileHandler->_type == kLocal) {
        NSFileHandle *handler = (NSFileHandle *)(_fileHandler->_handler);
        
        switch (seekMode) {
            case kSet:
                [handler seekToFileOffset:fileOffset];
                break;
                
            case kCurrent: {
                uint64_t pos = fileOffset + handler.offsetInFile;
                [handler seekToFileOffset:pos];
                break;
            }
                
            case kEnd:
                [handler seekToEndOfFile];
                break;
                
            default:
                break;
        }
        
        return handler.offsetInFile;
    } else if (_fileHandler->_type == kRemote) {
        CZSMBErrorCode code = kErrorCodeHostUnreachable;
        CZSMBFileSeekMode smbSeekMode;
        switch (seekMode) {
            case kCurrent:
                smbSeekMode = CZSMBFileSeekModeCur;
                break;
            case kSet:
                smbSeekMode = CZSMBFileSeekModeSet;
                break;
            case kEnd:
                smbSeekMode = CZSMBFileSeekModeEnd;
                break;
        }
        CZFileDescriptor *fileDesc = (CZFileDescriptor *)_fileHandler->_handler;
        CZSMBAdapter *smbAdapter = [[CZWNFSManager sharedInstance] sharedSMBAdapter];
        if (smbAdapter) {
            code = [smbAdapter seekFile:fileDesc
                                 offset:fileOffset
                                   mode:smbSeekMode];
        }
        
        if ([CZSMBErrorHandler isError:code]) {
            CZLogv(@"seekFile: %@", [CZSMBErrorHandler getErrorMessage:code]);
        }else {
            CZLogv(@"seekFile: have seek %lld bytes to file", fileDesc.offset);
        }
        
        return fileDesc.offset;
    } else {
        return 0;
    }
#else
    return kInvalidPlatform;
#endif
}

int CZFileHandler::closeFile()
{
#ifdef TARGET_OS_IPHONE
    if (_fileHandler->_type == kLocal) {
        NSFileHandle *handler = (NSFileHandle *)(_fileHandler->_handler);
        [handler closeFile];
        return kSuccess;
    } else if (_fileHandler->_type == kRemote) {
        CZFileDescriptor *fileDesc = (CZFileDescriptor *)_fileHandler->_handler;
        CZSMBErrorCode code = kErrorCodeHostUnreachable;
        CZSMBAdapter *smbAdapter = [[CZWNFSManager sharedInstance] sharedSMBAdapter];
        if (smbAdapter) {
            code = [smbAdapter closeFile:fileDesc];
        }
        if ([CZSMBErrorHandler isError:code]) {
            CZLogv(@"closeFile: %@", [CZSMBErrorHandler getErrorMessage:code]);
            return kFailed;
        }
        
        return kSuccess;
    } else {
        return kSuccess;
    }
#else
    return kInvalidPlatform;
#endif
}

uint64_t CZFileHandler::fileOffset()
{
#ifdef TARGET_OS_IPHONE
    if (_fileHandler->_type == kLocal) {
        NSFileHandle *handler = (NSFileHandle *)(_fileHandler->_handler);
        return [handler offsetInFile];
    } else if (_fileHandler->_type == kRemote) {
        CZFileDescriptor *fileDesc = (CZFileDescriptor *)_fileHandler->_handler;
        return fileDesc.offset;
    } else {
        return 0;
    }
#else
    return kInvalidPlatform;
#endif
}
