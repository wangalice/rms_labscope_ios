//
//  UIImage+ReadTIFF.h
//  FileKit
//
//  Created by Li, Junlin on 12/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <tiffio.h>

@interface UIImage (ReadTIFF)

- (id)initWithContentsOfTifFile:(NSString *)path;

/**
 * read one frame image from tiff handler.
 * @param subFileType demand sub file type to read. 0 for main image; FILETYPE_REDUCEDIMAGE for thumbnail.
 */
+ (CGImageRef)readImageFromTif:(TIFF *)tif subFileType:(uint32)subFileType imageOrientation:(UIImageOrientation *)imageOrientation CF_RETURNS_RETAINED;

@end
