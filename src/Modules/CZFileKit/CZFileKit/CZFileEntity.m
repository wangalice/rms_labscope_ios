//
//  CZFileEntity.m
//  FileKit
//
//  Created by Mike Wang on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileEntity.h"
#import <WNFSFramework/CZFileDescriptor.h>

@interface CZFileEntity ()

// sub-class visible properties
@property (nonatomic, assign, readwrite) BOOL isFolder;
@property (nonatomic, assign, readwrite) unsigned long long fileSize;
@property (nonatomic, retain, readwrite) NSDate *creationDate;
@property (nonatomic, retain, readwrite) NSDate *modificationDate;

@end

@implementation CZFileEntity

- (id)copyWithZone:(NSZone *)zone {
    CZFileEntity *entity = [[[self class] allocWithZone:zone] init];
    entity->_filePath = [self->_filePath copy];
    entity->_modificationDate = [self->_modificationDate retain];
    entity->_isFolder = self->_isFolder;
    entity->_fileSize = self->_fileSize;
    return entity;
}

- (void)dealloc {
    [_filePath release];
    [_creationDate release];
    [_modificationDate release];
    
    [super dealloc];
}

- (CZFileStorageType)storageType {
    return kUnknownStorage;
}

- (NSString *)fileName {
    return [self.filePath lastPathComponent];
}

- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[CZFileEntity class]]) {
        CZFileEntity *other = (CZFileEntity *)object;
        return [_filePath isEqualToString:other->_filePath];
    } else {
        return NO;
    }
}

- (NSUInteger)hash {
    return _filePath.hash;
}

@end

@implementation CZLocalFileEntity

- (id)initWithFilePath:(NSString *)filePath {
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    return [self initWithFilePath:filePath andFileAttributes:fileAttributes];
}

- (id)initWithFilePath:(NSString *)filePath andFileAttributes:(NSDictionary *)fileAttributes {
    self = [super init];
    if (self) {
        self.filePath = filePath;
        self.fileSize = [fileAttributes[NSFileSize] unsignedLongLongValue];
        self.creationDate = fileAttributes[NSFileCreationDate];
        self.modificationDate = fileAttributes[NSFileModificationDate];
        self.isFolder = [fileAttributes[NSFileType] isEqualToString:NSFileTypeDirectory];
    }
    return self;
}

- (CZFileStorageType)storageType {
    return kLocal;
}

@end

@implementation CZRemoteFileEntity

- (id)initWithFileDescriptor:(CZFileDescriptor *)fileDescriptor {
    self = [super init];
    if (self) {
        self.filePath = fileDescriptor.filePath;
        self.fileSize = fileDescriptor.fileDataSize;
        self.creationDate = fileDescriptor.createDatetime;
        self.modificationDate = fileDescriptor.lastWriteDatetime;
        self.isFolder = fileDescriptor.isDirectory;
    }
    return self;
}

- (CZFileStorageType)storageType {
    return kRemote;
}

@end
