//
//  CZFileNameGenerator.h
//  FileKit
//
//  Created by Ralph Jin on 5/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZFileNameTemplate.h"

@interface CZFileNameGenerator : NSObject

@property (nonatomic, retain) CZFileNameTemplate *fileNameTemplate;
@property (nonatomic, copy) NSString *operatorName;
@property (nonatomic, copy) NSString *microscope;
@property (nonatomic, copy) NSString *objective;
@property (nonatomic, retain) NSDate *captureDate;
@property (nonatomic, copy) NSString *extension;

/** generator unique file path by append auto number, if conflict.
 * for example, /usr/a.png -> /usr/a(02).png
 */
+ (NSString *)uniqueFilePath:(NSString *)filePath;

+ (NSString *)uniqueFilePath:(NSString *)filePath from:(NSArray *)fileNames;

+ (NSArray *)dateTimeFormatOptions;

- (id)initWithTemplate:(CZFileNameTemplate *)fileNameTemplate;

- (NSString *)uniqueFileNameInDirectory:(NSString *)path;
- (NSString *)uniqueFileNameInDirectory:(NSString *)path excludedName:(NSString *)excludedName;

// TODO: return file name without extension
/*! generate unique file name among exists file names.
 @param fileNames array of file name, can be nil;
 */
- (NSString *)uniqueFileNameFromNames:(NSArray *)fileNames;

/*! generate sample file name, auto number field will be ###*/
- (NSString *)sampleFileName;

/*! operatorName, microscope, objective fallback to default version when empty.*/
- (void)useDefaultValueWhenEmpty;

@end
