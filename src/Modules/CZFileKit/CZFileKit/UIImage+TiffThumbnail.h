//
//  UIImage+TiffThumbnail.h
//  FileKit
//
//  Created by Ralph Jin on 12/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TiffThumbnail)

- (id)initThumbWithTiffFile:(NSString *)filePath;

@end
