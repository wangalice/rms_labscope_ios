//
//  UIImage+EXIFThumnail.m
//  FileKit
//
//  Created by Ralph Jin on 5/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "UIImage+EXIFThumbnail.h"
#import <CZToolbox/CZToolbox.h>
#include <libexif/exif-loader.h>
#include "CZFileHandler.h"

@implementation UIImage (EXIFThumbnail)

- (id)initThumbWithEXIFOfJPEGFile:(NSString *)path {
    BOOL success = NO;
    
    ExifLoader *loader = exif_loader_new();
    if (loader) {
        CZFileHandler file;
        long err = file.openFile([path UTF8String], kRDONLY);
        
        if (err >= 0) {
            const int kBlockSize = 2048;
            unsigned char buff[kBlockSize];
            while (true) {
                long size = file.readFile(buff, sizeof(buff));
                if (size <= 0) {
                    break;
                }
                if (!exif_loader_write(loader, buff, (uint32_t)size)) {
                    break;
                }
            }
            
            file.closeFile();
        } else {
            CZLogv(@"open file %@ failed!", path);
        }
        
        ExifData *exif = exif_loader_get_data(loader);
        exif_loader_unref(loader);
        
        if (exif) {
            if (exif->data && exif->size) {
                NSData *data = [[NSData alloc] initWithBytes:exif->data length:exif->size];
                self = [self initWithData:data];
                [data release];
                success = YES;
            }
            
            exif_data_unref(exif);
        }
    }
    
    if (!success) {
        [self release];
        self = nil;
    }
    
    return self;
}

@end
