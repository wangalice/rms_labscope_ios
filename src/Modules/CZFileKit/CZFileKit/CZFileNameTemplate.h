//
//  CZFileNameTemplate.h
//  FileKit
//
//  Created by Ralph Jin on 5/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMaxFileNameFieldsCount 6U

typedef NS_ENUM(NSInteger, CZFileNameTemplateFieldType) {
    CZFileNameTemplateFieldTypeUnknown      = -1,
    CZFileNameTemplateFieldTypeFreeText     = 0,
    CZFileNameTemplateFieldTypePreText API_UNAVAILABLE(ios) = 1,
    CZFileNameTemplateFieldTypeVariableText = 2,
    CZFileNameTemplateFieldTypeAutoNumber   = 3,
    CZFileNameTemplateFieldTypeDate         = 4
};

extern NSString * const CZFileNameTemplateFieldValueMicroscopeName;
extern NSString * const CZFileNameTemplateFieldValueObjectiveMagnification;

@interface CZFileNameTemplateField : NSObject <NSCopying>

@property (nonatomic, assign, readonly) CZFileNameTemplateFieldType type;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, assign, getter = isUserInput) BOOL userInput;

- (instancetype)initWithType:(CZFileNameTemplateFieldType)type;
- (instancetype)initWithType:(CZFileNameTemplateFieldType)type value:(NSString *)value;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (BOOL)isEqualToField:(CZFileNameTemplateField *)otherField;

@end

@interface CZFileNameTemplate : NSObject <NSCopying>

@property (nonatomic, copy) NSString *path;
@property (nonatomic, copy) NSString *separator;
@property (nonatomic, assign, getter=isEditable) BOOL editable;
@property (nonatomic, readonly, assign, getter=isUpdated) BOOL updated;

- (instancetype)initWithContentsOfFile:(NSString *)path;

- (BOOL)isEqualToTemplate:(CZFileNameTemplate *)otherTemplate;

- (BOOL)writeToFile:(NSString *)path;

- (void)addField:(CZFileNameTemplateField *)field;
- (CZFileNameTemplateField *)fieldAtIndex:(NSUInteger)index;
- (void)replaceFieldAtIndex:(NSUInteger)index withField:(CZFileNameTemplateField *)field;
- (void)removeFieldAtIndex:(NSUInteger)index;
- (void)insertField:(CZFileNameTemplateField *)field atIndex:(NSUInteger)index;
- (NSUInteger)fieldCount;

- (BOOL)isValid;

@end
