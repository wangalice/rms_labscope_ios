//
//  CZFileThumbnail.m
//  FileKit
//
//  Created by Mike Wang on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileThumbnail.h"
#import <ImageIO/ImageIO.h>
#import <CZIKit/CZIKit.h>
#import <CZToolbox/CZToolbox.h>
#import "UIImage+EXIFThumbnail.h"
#import "UIImage+TiffThumbnail.h"

static const NSUInteger kGenerateThumbnailRetryCount = 3;
static const NSUInteger kGenerateThumbnailMaxCacheSize = 1024 * 1024 * 4;

@interface CZFileThumbnailTask : NSObject
@property (nonatomic, copy, readonly) NSString *filePath;
@property (nonatomic, strong) NSDate *modifiedDate;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic) NSInteger errorCount;
@end

@implementation CZFileThumbnailTask

- (id)initWithPath:(NSString *)filePath modifiedDate:(NSDate *)modifiedDate {
    self = [super init];
    if (self) {
        _filePath = [filePath copy];
        _modifiedDate = modifiedDate;
    }
    return self;
}

@end

@interface CZThumbnailEntry : NSObject

@property (nonatomic, strong, readonly) UIImage *thumbnail;
@property (nonatomic, strong, readonly) NSDate *modifiedDate;

@end

@implementation CZThumbnailEntry

- (id)initWithThumbnail:(UIImage *)thumnail modifiedDate:(NSDate *)modifiedDate {
    self = [super init];
    if (self) {
        _thumbnail = thumnail;
        _modifiedDate = modifiedDate;
    }
    return self;
}

@end

static NSCache *sharedThumbnailCache() {
    static NSCache *sharedThumbnailCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedThumbnailCache = [[NSCache alloc] init];
        [sharedThumbnailCache setTotalCostLimit:kGenerateThumbnailMaxCacheSize];
    });
    
    return sharedThumbnailCache;
}

@interface CZFileThumbnail ()

@property (nonatomic, unsafe_unretained) NSCache *cachedThumbnails;  // container of CZThumbnailEntry
@property (nonatomic, strong) NSMutableArray *pendingTasks;
@property (atomic, strong) CZFileThumbnailTask *currentThumbnailTask;

@end



@implementation CZFileThumbnail

+ (void)clearCache {
    [sharedThumbnailCache() removeAllObjects];
}

+ (UIImage *)newThumbnailFromImageFile:(NSString *)filePath {
    UIImage *thumbnail = nil;
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:filePath], NULL);
    if (imageSource) {
        NSDictionary *options = @{(id)kCGImageSourceCreateThumbnailWithTransform: @YES,
                                  (id)kCGImageSourceCreateThumbnailFromImageIfAbsent: @YES,
                                  (id)kCGImageSourceThumbnailMaxPixelSize: @160};
        
        CGImageRef imageRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (CFDictionaryRef)options);
        CFRelease(imageSource);
        
        if (imageRef) {
            thumbnail = [[UIImage alloc] initWithCGImage:imageRef];
            CGImageRelease(imageRef);
        }
    }
    
    return thumbnail;
}

- (id)init {
    self = [super init];
    if (self) {
        _pendingTasks = [[NSMutableArray alloc] init];
        _cachedThumbnails = sharedThumbnailCache();
        _delegate = nil;
    }
    return self;
}

- (UIImage *)newThumbnailFrom:(NSString *)filePath modifiedDate:(NSDate *)modifiedDate {
    UIImage *thumbnail = nil;
    
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:filePath.pathExtension];
    if (fileFormat == kCZFileFormatCZI || fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
        BOOL foundInCache = FALSE;
        
        CZThumbnailEntry *thumbnailEntry = [_cachedThumbnails objectForKey:filePath];
        foundInCache = (thumbnailEntry != nil);
        
        if (foundInCache) {
            NSDate *dateFromCacheFile = thumbnailEntry.modifiedDate;
            if ([dateFromCacheFile isEqualToDate:modifiedDate]) {
                thumbnail = thumbnailEntry.thumbnail;
            } else {
                foundInCache = NO;
                [_cachedThumbnails removeObjectForKey:filePath];  // remove old thumbnail
            }
        }

        if (!foundInCache) {
            [self scheduleFor:filePath modifiedDate:modifiedDate];
        }
    }

    return thumbnail;
}

- (BOOL)isValidThumbnailTask:(CZFileThumbnailTask *)task {
    BOOL isRemote = [task.filePath hasPrefix:@"//"];
    if (isRemote) {
        CZDefaultSettings *defaultSettings = [CZDefaultSettings sharedInstance];
        NSString *currentConnectedServerIP = [NSString stringWithFormat:@"//%@/", [defaultSettings serverURL]];
        BOOL isServerSetup = [[NSUserDefaults standardUserDefaults] boolForKey:kIsServerSetup];
        if (isServerSetup && [task.filePath hasPrefix:currentConnectedServerIP]) {
            return YES;
        } else  {
            return NO;
        }
    } else {
        return YES;
    }
}

- (void)willGenerateThumbnailFor:(CZFileThumbnailTask *)task {
    self.currentThumbnailTask = task;
        
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        if (![self isValidThumbnailTask:task]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self thumbnailTaskDidFinish];
            });
            return;
        }
        
        UIImage *thumbnail = nil;
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:self.currentThumbnailTask.filePath.pathExtension];
        if (fileFormat == kCZFileFormatCZI) {
            CZIDocument *czi = [[CZIDocument alloc] initWithFile:self.currentThumbnailTask.filePath];
            thumbnail = [[UIImage alloc] initWithData:[czi thumbnail]];
        } else if (fileFormat == kCZFileFormatJPEG) {
            thumbnail = [[UIImage alloc] initThumbWithEXIFOfJPEGFile:self.currentThumbnailTask.filePath];
            if (thumbnail == nil) {
                thumbnail = [CZFileThumbnail newThumbnailFromImageFile:self.currentThumbnailTask.filePath];
            }
        } else if (fileFormat == kCZFileFormatTIF) {
            thumbnail = [[UIImage alloc] initThumbWithTiffFile:self.currentThumbnailTask.filePath];
            if (thumbnail == nil) {
                thumbnail = [CZFileThumbnail newThumbnailFromImageFile:self.currentThumbnailTask.filePath];
            }
        } else {
            return;
        }
        
        if (!thumbnail) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self thumbnailTaskDidFinish];
            });
            return;
        }
        
        self.currentThumbnailTask.thumbnail = thumbnail;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self thumbnailTaskDidFinish];
        });
    });
}

- (void)scheduleFor:(NSString *)filePath modifiedDate:(NSDate *)modifiedDate {
    for (CZFileThumbnailTask *task in self.pendingTasks) {
        if ([task.filePath isEqualToString:filePath]) {
            task.modifiedDate = modifiedDate;
            return;
        }
    }
    
    CZFileThumbnailTask *task = [[CZFileThumbnailTask alloc] initWithPath:filePath
                                                             modifiedDate:modifiedDate];
    [self.pendingTasks addObject:task];
    
    if (self.pendingTasks.count <= 1) {
        [self willGenerateThumbnailFor:task];
    }
}

- (void)thumbnailTaskDidFinish {
    CZFileThumbnailTask *taskFinish = self.currentThumbnailTask;
    
    if (self.currentThumbnailTask.thumbnail == nil) {
        CZLogv(@"Generate thumbnail failed for %@", self.currentThumbnailTask.filePath);
        if (++taskFinish.errorCount > kGenerateThumbnailRetryCount) {
            CZLogv(@"Generate thumbnail failed more than %lu times for %@", (unsigned long)kGenerateThumbnailRetryCount,
                  self.currentThumbnailTask.filePath);
            [self.pendingTasks removeObject:taskFinish];
        }
    } else {
        [self.pendingTasks removeObject:taskFinish];

        CZThumbnailEntry *thumbnailEntry = [[CZThumbnailEntry alloc] initWithThumbnail:taskFinish.thumbnail
                                                                          modifiedDate:taskFinish.modifiedDate];
        
        CGSize imageSize = [taskFinish.thumbnail size];
        NSUInteger cost = imageSize.width * imageSize.height;
        
        [_cachedThumbnails setObject:thumbnailEntry forKey:taskFinish.filePath cost:cost];
        
        [self.delegate thumbnail:taskFinish.thumbnail
                 didGenerateFrom:taskFinish.filePath];
    }
    
    while (self.pendingTasks.count > 0) {
        CZFileThumbnailTask *nextTask = [_pendingTasks lastObject];
        if (![self isValidThumbnailTask:nextTask]) {
            [_pendingTasks removeObject:nextTask];
            continue;
        }
        
        [self willGenerateThumbnailFor:nextTask];
        break;
    }
}

- (void)removeThumbnailFor:(NSString *)filePath {
    CZFileThumbnailTask *taskWillBeRemove = nil;
    for (CZFileThumbnailTask *task in self.pendingTasks) {
        if ([task.filePath isEqualToString:filePath]) {
            taskWillBeRemove = task;
            break;
        }
    }
    
    if (taskWillBeRemove) {
        [self.pendingTasks removeObject:taskWillBeRemove];
    }
    [_cachedThumbnails removeObjectForKey:filePath];
}

@end
