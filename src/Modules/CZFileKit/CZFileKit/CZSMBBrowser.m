//
//  CZSMBBrowser.m
//  FileKit
//
//  Created by Halley Gu on 7/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZSMBBrowser.h"
#import <WNFSFramework/CZWNFSManager.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import <CocoaAsyncSocket/GCDAsyncSocket.h>
#import <CZToolbox/CZToolbox.h>

#define UNUSED __attribute__((unused))

static const int kTcpConnectionTimeout = 2;

typedef NS_ENUM(uint8_t, nbt_name_type) {
    NBT_NAME_CLIENT   = 0x00,
    NBT_NAME_MS       = 0x01,
    NBT_NAME_USER     = 0x03,
    NBT_NAME_SERVER   = 0x20,
    NBT_NAME_PDC      = 0x1B,
    NBT_NAME_LOGON    = 0x1C,
    NBT_NAME_MASTER   = 0x1D,
    NBT_NAME_BROWSER  = 0x1E
};

typedef NS_ENUM(uint16_t, nbt_qclass) {
    NBT_QCLASS_IP = 0x0001
};

typedef NS_ENUM(uint16_t, nbt_qtype) {
    NBT_QTYPE_ADDRESS     = 0x0001,
    NBT_QTYPE_NAMESERVICE = 0x0002,
    NBT_QTYPE_NULL        = 0x000A,
    NBT_QTYPE_NETBIOS     = 0x0020,
    NBT_QTYPE_STATUS      = 0x0021
};

typedef NS_ENUM(uint16_t, nb_flags) {
    NBT_NM_PERMANENT        = 0x0200,
    NBT_NM_ACTIVE           = 0x0400,
    NBT_NM_CONFLICT         = 0x0800,
    NBT_NM_DEREGISTER       = 0x1000,
    NBT_NM_OWNER_TYPE       = 0x6000,
    NBT_NM_GROUP            = 0x8000
};

@interface CZSMBBrowser () <GCDAsyncSocketDelegate, GCDAsyncUdpSocketDelegate, CZWNFSManagerDelegate> {
    dispatch_queue_t _socketDelegateQueue;
}

@property (nonatomic, retain) GCDAsyncUdpSocket *broadcastSocket;
@property (nonatomic, retain) GCDAsyncUdpSocket *nameQuerySocket;
@property (nonatomic, retain) NSMutableDictionary *hostNameMap;
@property (nonatomic, retain) NSMutableSet *subnetsSearched;

@end

@implementation CZSMBBrowser

- (id)init {
    self = [super init];
    if (self) {
        _socketDelegateQueue = dispatch_queue_create("com.zeisscn.smbbrowser", NULL);
        _hostNameMap = [[NSMutableDictionary alloc] init];
        _subnetsSearched = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_broadcastSocket close];
    [_broadcastSocket release];
    
    [_nameQuerySocket close];
    [_nameQuerySocket release];
    
    [_hostNameMap release];
    [_subnetsSearched release];
    
    dispatch_release(_socketDelegateQueue);
    
    [super dealloc];
}

- (void)startBrowsing {
    [self stopBrowsing];
    
    [self sendBroadcast];
    [self sendNameQueriesToLocalSubnet];
}

- (void)stopBrowsing {
    [_broadcastSocket close];
    self.broadcastSocket = nil;
    
    [_nameQuerySocket close];
    self.nameQuerySocket = nil;
    
    [self.hostNameMap removeAllObjects];
    [self.subnetsSearched removeAllObjects];
}

#pragma mark - Lazy-load properties

- (GCDAsyncUdpSocket *)broadcastSocket {
    if (_broadcastSocket == nil) {
        _broadcastSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                         delegateQueue:_socketDelegateQueue];
        [_broadcastSocket setIPv4Enabled:YES];
        [_broadcastSocket setIPv6Enabled:YES];
        
        NSError *error = nil;
        do {
            if (![_broadcastSocket enableBroadcast:YES error:&error]) {
                CZLogv(@"CZSMBBrowser: failed to enable broadcast for socket.");
            }
            
            if (![_broadcastSocket beginReceiving:&error]) {
                CZLogv(@"CZSMBBrowser: failed to begin receiving data from broadcast socket.");
                break;
            }
        } while (NO);
        
        if (error != nil) {
            [_broadcastSocket release];
            _broadcastSocket = nil;
        }
    }
    return _broadcastSocket;
}

- (GCDAsyncUdpSocket *)nameQuerySocket {
    if (_nameQuerySocket == nil) {
        _nameQuerySocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                         delegateQueue:_socketDelegateQueue];
        [_nameQuerySocket setIPv4Enabled:YES];
        [_nameQuerySocket setIPv6Enabled:YES];
        
        NSError *error = nil;
        do {
            if (![_nameQuerySocket bindToPort:137 error:&error]) {
                CZLogv(@"CZSMBBrowser: failed to bind port 137 for socket.");
                if (![_nameQuerySocket bindToPort:0 error:&error]) {
                    CZLogv(@"CZSMBBrowser: failed to bind port for socket.");
                    break;
                }
            }
            
            if (![_nameQuerySocket beginReceiving:&error]) {
                CZLogv(@"CZSMBBrowser: failed to begin receiving data from name query socket.");
                break;
            }
        } while (NO);
        
        if (error != nil) {
            [_nameQuerySocket release];
            _nameQuerySocket = nil;
        }
    }
    return _nameQuerySocket;
}

#pragma mark - Private methods

- (void)sendBroadcast {
    GCDAsyncUdpSocket *socket = [self.broadcastSocket retain];
    
    char packet[] = {0x36, 0x26, 0x01, 0x10, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x43,
        0x4b, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x00,
        0x00, 0x20, 0x00, 0x01};  // SMB browse packet
    const size_t packetLength = sizeof(packet);
    
    NSData *packetData = [[NSData alloc] initWithBytes:packet
                                                length:packetLength];
    
    [socket sendData:packetData
              toHost:@"255.255.255.255" // Broadcast
                port:137
         withTimeout:5
                 tag:0];
    
    [packetData release];
    [socket release];
}

- (NSArray *)ipAddressesFromBroadcastResponseReader:(CZBufferProcessor *)reader {
    UNUSED uint16_t name_trn_id = [reader readShortInHostOrder];   // Transaction ID: 0x3626
    UNUSED uint16_t operation = [reader readShortInHostOrder];
    UNUSED uint16_t qdcount = [reader readShortInHostOrder];
    uint16_t ancount = [reader readShortInHostOrder];
    UNUSED uint16_t nscount = [reader readShortInHostOrder];
    UNUSED uint16_t arcount = [reader readShortInHostOrder];
    uint8_t stringLength = [reader readByte];
    UNUSED NSString *nameString = [reader readStringWithLength:stringLength];
    UNUSED nbt_name_type nameType = [reader readByte];
    nbt_qtype type = [reader readShortInHostOrder];
    nbt_qclass class = [reader readShortInHostOrder];
    UNUSED uint32_t timeToLive = [reader readLongInHostOrder];
    uint16_t dataLength = [reader readShortInHostOrder];
    
    if ((ancount != 1) || (type != NBT_QTYPE_NETBIOS) || (class != NBT_QCLASS_IP) || (dataLength % 6 != 0)) {
        return nil;
    }
    
    NSMutableArray *result = [[[NSMutableArray alloc] init] autorelease];
    
    for (int i = 0; i < dataLength / 6; ++i) {
        UNUSED uint16_t nameFlag = [reader readShortInHostOrder];
        NSString *ipAddress = [reader readIpAddressString];
        [result addObject:ipAddress];
    }
    
    return result;
}

- (void)sendNameQueriesToSubnetOfIPAddress:(NSString *)ipAddress {
    NSArray *components = [ipAddress componentsSeparatedByString:@"."];
    if (components.count != 4) {
        return;
    }
    
    NSString *prefix = [NSString stringWithFormat:@"%@.%@.%@",
                        components[0], components[1], components[2]];
    if ([self.subnetsSearched containsObject:prefix]) {
        return; // Already searched
    } else {
        [self.subnetsSearched addObject:prefix];
    }
    
    int localLastComponent = [[components lastObject] intValue];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        for (int i = 1; i < 255; i++) {
            if (i == localLastComponent) {
                // Skip passed IP address.
                continue;
            }
            
            NSString *target = [NSString stringWithFormat:@"%@.%d", prefix, i];
            
            [self sendNameQueryToIpAddress:target];
            
            usleep(0.005 * USEC_PER_SEC);
            
            if (i == 1) {
                [self searchNetBiosSSNToIpAddress:target];
            }
        }
    });
}

- (void)sendNameQueriesToLocalSubnet {
    NSString *localIpAddress = [CZCommonUtils localIpAddress];
    [self sendNameQueriesToSubnetOfIPAddress:localIpAddress];
}

- (void)sendNameQueryToIpAddress:(NSString *)ipAddress {
    GCDAsyncUdpSocket *socket = [self.nameQuerySocket retain];
    
    char packet[] = {0x7a, 0x03, 0x00, 0x00, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x43,
        0x4b, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x00,
        0x00, 0x21, 0x00, 0x01};  // SMB name query packet
    const size_t packetLength = sizeof(packet);
    
    NSData *packetData = [[NSData alloc] initWithBytes:packet
                                                length:packetLength];
    
    [socket sendData:packetData
              toHost:ipAddress
                port:137
         withTimeout:5
                 tag:0];
    
    [packetData release];
    [socket release];
}

- (void)searchNetBiosSSNToIpAddress:(NSString *)ipAddress {
    GCDAsyncSocket *socket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                                  delegateQueue:_socketDelegateQueue];
    [socket connectToHost:ipAddress
                   onPort:139
              withTimeout:kTcpConnectionTimeout
                    error:NULL];
    [socket release];
}

- (NSString *)hostNameFromNameQueryResponseReader:(CZBufferProcessor *)reader {
    UNUSED uint16_t name_trn_id = [reader readShortInHostOrder];
    UNUSED uint16_t operation = [reader readShortInHostOrder];
    UNUSED uint16_t qdcount = [reader readShortInHostOrder];
    uint16_t ancount = [reader readShortInHostOrder];
    UNUSED uint16_t nscount = [reader readShortInHostOrder];
    UNUSED uint16_t arcount = [reader readShortInHostOrder];
    uint8_t stringLength = [reader readByte];
    UNUSED NSString *nameString = [reader readStringWithLength:stringLength];
    UNUSED nbt_name_type nameType = [reader readByte];
    nbt_qtype type = [reader readShortInHostOrder];
    nbt_qclass class = [reader readShortInHostOrder];
    UNUSED uint32_t timeToLive = [reader readLongInHostOrder];
    UNUSED uint16_t dataLength = [reader readShortInHostOrder];
    uint8_t num_names = [reader readByte];
    
    if ((ancount != 1) || (type != NBT_QTYPE_STATUS) || (class != NBT_QCLASS_IP) || (num_names <= 0)) {
        return nil;
    }
    
    for (NSUInteger i = 0; i < num_names; i++) {
        NSString *name = [reader readStringWithLength:15];
        nbt_name_type nameType = [reader readByte];
        nb_flags nameFlag = [reader readShortInHostOrder];
        
        if (nameType == NBT_NAME_CLIENT && !(nameFlag & NBT_NM_GROUP)) {
            return name;
        }
    }
    
    return nil;
}

#pragma mark - GCDAsyncUdpSocketDelegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    
    uint16_t transactionId = [reader readShortInHostOrder];
    [reader seekToOffset:0];
    
    if (transactionId == 0x3626 && sock == self.broadcastSocket) {
        NSArray *ipAddresses = [self ipAddressesFromBroadcastResponseReader:reader];
        
        NSString *localIpAddress = [CZCommonUtils localIpAddress];
        NSString *localIpMask = nil;
        if ([localIpAddress rangeOfString:@"."].location != NSNotFound) {
            NSArray *components = [localIpAddress componentsSeparatedByString:@"."];
            if (components.count == 4) {
                localIpMask = [NSString stringWithFormat:@"%@.%@.%@.", components[0], components[1], components[2]];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(){
            for (NSString *ipAddress in ipAddresses) {
                if (localIpMask != nil && [ipAddress hasPrefix:localIpMask]) {
                    // Skip IP addresses in the same subnet.
                    continue;
                }
                
                [self sendNameQueryToIpAddress:ipAddress];
                [self sendNameQueriesToSubnetOfIPAddress:ipAddress];
            }
        });
    } else if (transactionId == 0x7a03 && sock == _nameQuerySocket) {
        CZBufferProcessor *addressReader = [[CZBufferProcessor alloc] initWithBuffer:address.bytes
                                                                              length:address.length];
        NSString *ipAddress = nil;
        if ([addressReader seekToOffset:4]) {
            ipAddress = [addressReader readIpAddressString];
        }
        [addressReader release];
        
        NSString *name = [self hostNameFromNameQueryResponseReader:reader];
        
        if (ipAddress.length > 0) {
            if (name != nil) {
                self.hostNameMap[ipAddress] = name;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(){
                [[CZWNFSManager sharedInstance] isServerReachable:ipAddress timeout:2 delegate:self];
            });
        }
    }
    
    [reader release];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    if (sock == _broadcastSocket) {
        self.broadcastSocket = nil;
    } else if (sock == _nameQuerySocket) {
        self.nameQuerySocket = nil;
    }
}

#pragma mark - CZWNFSManagerDelegate methods

- (void)reachingDidSuccess:(NSString *)server {
    NSString *hostName = self.hostNameMap[server];
    if ([self.delegate respondsToSelector:@selector(smbBrowser:didFindServerWithIpAddress:name:)]) {
        [self.delegate smbBrowser:self didFindServerWithIpAddress:server name:hostName];
    }
}

- (void)reachingDidFailWithError:(NSError *)error {
    // Do nothing.
}

#pragma mark - GCDAsyncSocketDelegate methods

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSString *ipAddress = host;
    if (ipAddress.length > 0) {
        NSString *oldName = self.hostNameMap[ipAddress];
        if (oldName == nil) {
            self.hostNameMap[ipAddress] = ipAddress;
            
            dispatch_async(dispatch_get_main_queue(), ^(){
                [[CZWNFSManager sharedInstance] isServerReachable:ipAddress timeout:2 delegate:self];
            });
        }
    }
    
    [sock disconnect];
}

@end
