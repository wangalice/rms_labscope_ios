//
//  UIImage+ReadTIFF.m
//  FileKit
//
//  Created by Li, Junlin on 12/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "UIImage+ReadTIFF.h"
#import <Accelerate/Accelerate.h>
#import <CZToolbox/CZToolbox.h>

@implementation UIImage (ReadTIFF)

- (id)initWithContentsOfTifFile:(NSString *)filePath {
    UIImageOrientation imageOrientation;
    CGImageRef imageRef = [[self class] readImageFromTifFile:filePath imageOrientation:&imageOrientation];
    if (imageRef) {
        self = [self initWithCGImage:imageRef scale:1.0 orientation:imageOrientation];
        CGImageRelease(imageRef);
    } else {
        [self release];
        self = nil;
    }
    return self;
}

+ (CGImageRef)readImageFromTifFile:(NSString *)filePath imageOrientation:(UIImageOrientation *)imageOrientation CF_RETURNS_RETAINED {
    TIFF *tif = NULL;
    tif = TIFFOpen([filePath fileSystemRepresentation], "r");
    if (!tif) {
        CZLogv(@"Can't create TIFF file at path: %@.\n", filePath);
        return NULL;
    } else {
        CGImageRef image = [self readImageFromTif:tif subFileType:0 imageOrientation:imageOrientation];
        TIFFClose(tif);
        return image;
    }
}

+ (CGImageRef)readImageFromTif:(TIFF *)tif subFileType:(uint32)subFileType imageOrientation:(UIImageOrientation *)imageOrientation CF_RETURNS_RETAINED {
    uint32 *raster = NULL;
    CGImageRef newCGImage = NULL;
    do {
        if (!tif) {
            break;
        }
        
        const uint16 framesCount = TIFFNumberOfDirectories(tif);
        
        BOOL found = NO;
        if (framesCount > 1) {
            for (uint16 frameID = 0; frameID < framesCount; frameID++) {
                if (TIFFSetDirectory(tif, frameID)) {
                    uint32 realSubFileType = 0;
                    if (TIFFGetField(tif, TIFFTAG_SUBFILETYPE, &realSubFileType)) {
                        if (realSubFileType == subFileType) {
                            found = YES;
                            break;
                        }
                    }
                }
            }
        } else if (framesCount == 1) {
            found = (subFileType == 0);  // if there's only 1 frame, then it must be main image frame.
        }
        
        if (!found) {
            CZLogv(@"Can't read image, can't find the sub file with type %d.", subFileType);
            break;
        }
        
        if (TIFFIsTiled(tif)) {
            CZLogv(@"Can't read image, it is tiled.");
            break;
        }
        
        uint16 photometric = -1;
        TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
        if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_MINISBLACK) {
            break;
        }
        
        uint32 width = 0;
        if (!TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width)) {
            break;
        }
        
        if (width == 0) {
            break;
        }
        
        uint32 height = 0;
        if (!TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height)) {
            break;
        }
        
        
        if (height == 0) {
            break;
        }
        
        if (imageOrientation) {
            uint16 tiffOrientation = ORIENTATION_TOPLEFT;
            if (!TIFFGetField(tif, TIFFTAG_ORIENTATION, &tiffOrientation)) {
                tiffOrientation = ORIENTATION_TOPLEFT;
            }
            
            switch (tiffOrientation) {
                    case ORIENTATION_TOPLEFT:
                    *imageOrientation = UIImageOrientationUp;
                    break;
                    case ORIENTATION_BOTRIGHT:
                    *imageOrientation = UIImageOrientationDown;
                    break;
                    case ORIENTATION_LEFTBOT:
                    *imageOrientation = UIImageOrientationLeft;
                    break;
                    case ORIENTATION_RIGHTTOP:
                    *imageOrientation = UIImageOrientationRight;
                    break;
                    case ORIENTATION_TOPRIGHT:
                    *imageOrientation = UIImageOrientationUpMirrored;
                    break;
                    case ORIENTATION_BOTLEFT:
                    *imageOrientation = UIImageOrientationDownMirrored;
                    break;
                    case ORIENTATION_LEFTTOP:
                    *imageOrientation = UIImageOrientationLeftMirrored;
                    break;
                    case ORIENTATION_RIGHTBOT:
                    *imageOrientation = UIImageOrientationRightMirrored;
                    break;
                default:
                    *imageOrientation = UIImageOrientationUp;
                    break;
            }
        }
        
        uint16 bitspersample = 0;
        if (!TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample) || bitspersample != 8) {
            break;
        }
        
        uint16 samplePerPixel = 0;
        if (!TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samplePerPixel)) {
            CZLogv(@"Can't get SamplesPerPixel tag.\n");
            break;
        }
        
        const BOOL isGray = samplePerPixel == 1;
        uint32 rowBytes = width * samplePerPixel;
        raster = (uint32 *)_TIFFmalloc(rowBytes * height);
        
        BOOL readSuccess = YES;
        void *buf = raster;
        for (uint32 row = 0; row < height; ++row) {
            if (!TIFFReadScanline(tif, buf, row, 0)){
                readSuccess = NO;
                break;
            }
            
            buf += rowBytes;
        }
        
        if (!readSuccess) {
            CZLogv(@"failed to read strip");
            break;
        }
        
        void *rawData = raster;
        if (samplePerPixel == 3) { // RGB to RGBA
            rawData = malloc(width * 4 * height);
            vImage_Buffer srcBuffer = {
                raster,
                (vImagePixelCount)height,
                (vImagePixelCount)width,
                3 * width
            };
            
            vImage_Buffer dstBuffer = {
                rawData,
                (vImagePixelCount)height,
                (vImagePixelCount)width,
                4 * width
            };
            
            vImageConvert_RGB888toRGBA8888(&srcBuffer, NULL, 255, &dstBuffer, NO, kvImageNoFlags);
            rowBytes = 4 * width;
            
            if (raster) {
                _TIFFfree(raster);
                raster = NULL;
            }
        }
        
        CGColorSpaceRef colorSpace = isGray ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
        CGBitmapInfo bitmapInfo = isGray ? kCGImageAlphaNone : (kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        CGContextRef context = CGBitmapContextCreate(rawData, width, height, 8, rowBytes, colorSpace, bitmapInfo);
        CGColorSpaceRelease(colorSpace);
        newCGImage = CGBitmapContextCreateImage(context);
        CGContextRelease(context);
        
        if (samplePerPixel == 3) {
            free(rawData);
        }
        
        if (raster) {
            _TIFFfree(raster);
            raster = NULL;
        }
    } while (0);
    
    if (raster) {
        _TIFFfree(raster);
    }
    
    return newCGImage;
}

@end
