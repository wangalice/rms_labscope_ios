//
//  UIImage+TiffThumbnail.mm
//  FileKit
//
//  Created by Ralph Jin on 12/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "UIImage+TiffThumbnail.h"
#include <tiffio.h>
#import "CZFileHandler.h"
#import "UIImage+ReadTIFF.h"

static tmsize_t
_tiffisReadProc(thandle_t fd, void *buf, tmsize_t size) {
    CZFileHandler *data = reinterpret_cast<CZFileHandler *>(fd);
    
    // Verify that type does not overflow.
    long request_size = size;
    if (static_cast<tmsize_t>(request_size) != size) {
        return static_cast<tmsize_t>(-1);
    }
    
    long readBytes = data->readFile(buf, request_size);
        
    return static_cast<tmsize_t>(readBytes);
}

static tmsize_t
_tiffisWriteProc(thandle_t, void *, tmsize_t) {
    return 0;
}

static uint64
_tiffisSeekProc(thandle_t fd, uint64 off, int whence) {
    CZFileHandler *data = reinterpret_cast<CZFileHandler *>(fd);
    
    switch(whence) {
        case SEEK_SET: {
            // Compute 64-bit offset
            uint64 new_offset = off;
            
            // Verify that value does not overflow
            int64 offset = static_cast<int64>(new_offset);
            if (static_cast<uint64>(offset) != new_offset) {
                return static_cast<uint64>(-1);
            }
            
            data->seekFile(offset, kSet);
            break;
        }
        case SEEK_CUR: {
            // Verify that value does not overflow
            int64 offset = static_cast<int64>(off);
            if (static_cast<uint64>(offset) != off) {
                return static_cast<uint64>(-1);
            }
            
            data->seekFile(offset, kCurrent);
            break;
        }
        case SEEK_END: {
            // Verify that value does not overflow
            int64_t offset = static_cast<int64_t>(off);
            if (static_cast<uint64>(offset) != off) {
                return static_cast<uint64>(-1);
            }
            
            data->seekFile(offset, kEnd);
            break;
        }
    }

    return (uint64)data->fileOffset();
}

static uint64
_tiffisSizeProc(thandle_t fd) {
    CZFileHandler *data = reinterpret_cast<CZFileHandler *>(fd);
    uint64 pos = data->fileOffset();
    uint64 len;

    data->seekFile(0, kEnd);
    len = data->fileOffset();
    data->seekFile(pos, kSet);

    return (uint64) len;
}

static int
_tiffisCloseProc(thandle_t fd) {
    CZFileHandler *data = reinterpret_cast<CZFileHandler *>(fd);
    data->closeFile();
    delete data;
    return 0;
}

static int
_tiffDummyMapProc(thandle_t , void **base, toff_t *size) {
    return (0);
}

static void
_tiffDummyUnmapProc(thandle_t , void *base, toff_t size) {
}

static TIFF *
tiffCZFileHandlerOpen(const char *name) {
    CZFileHandler *fileHandler = new CZFileHandler;
    if (fileHandler->openFile(name, kRDONLY) != kSuccess) {
        return NULL;
    }

    // Open for reading.
    TIFF *tif = TIFFClientOpen(name, "r",
                               reinterpret_cast<thandle_t>(fileHandler),
                               _tiffisReadProc,
                               _tiffisWriteProc,
                               _tiffisSeekProc,
                               _tiffisCloseProc,
                               _tiffisSizeProc,
                               _tiffDummyMapProc,
                               _tiffDummyUnmapProc);
    return tif;
}

@implementation UIImage (TiffThumbnail)

- (id)initThumbWithTiffFile:(NSString *)filePath {
    CGImageRef imageRef = NULL;
    TIFF *tif = tiffCZFileHandlerOpen([filePath fileSystemRepresentation]);
    if (tif) {
        imageRef = [[self class] readImageFromTif:tif subFileType:FILETYPE_REDUCEDIMAGE imageOrientation:NULL];
        TIFFClose(tif);
    }

    if (imageRef) {
        self = [self initWithCGImage:imageRef];
        CGImageRelease(imageRef);
    } else {
        [self release];
        self = nil;
    }
    return self;
}

@end
