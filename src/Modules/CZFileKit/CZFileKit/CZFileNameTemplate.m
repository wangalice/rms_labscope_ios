//
//  CZFileNameTemplate.m
//  FileKit
//
//  Created by Ralph Jin on 5/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileNameTemplate.h"

NSString * const CZFileNameTemplateFieldTypeUnknownString = @"Unknown";
NSString * const CZFileNameTemplateFieldTypeFreeTextString = @"FreeText";
NSString * const CZFileNameTemplateFieldTypePreTextString = @"PreText";
NSString * const CZFileNameTemplateFieldTypeVariableTextString = @"VariableText";
NSString * const CZFileNameTemplateFieldTypeAutoNumberString = @"AutoNumber";
NSString * const CZFileNameTemplateFieldTypeDateString = @"Date";

NSString * const CZFileNameTemplateFieldValueMicroscopeName = @"Microscope";
NSString * const CZFileNameTemplateFieldValueOperatorName = @"Operator";
NSString * const CZFileNameTemplateFieldValueObjectiveMagnification = @"Objective";

@implementation CZFileNameTemplateField

+ (NSString *)stringFromType:(CZFileNameTemplateFieldType)type {
    switch (type) {
        case CZFileNameTemplateFieldTypeFreeText:
            return CZFileNameTemplateFieldTypeFreeTextString;
        case CZFileNameTemplateFieldTypeVariableText:
            return CZFileNameTemplateFieldTypeVariableTextString;
        case CZFileNameTemplateFieldTypeAutoNumber:
            return CZFileNameTemplateFieldTypeAutoNumberString;
        case CZFileNameTemplateFieldTypeDate:
            return CZFileNameTemplateFieldTypeDateString;
        default:
            return CZFileNameTemplateFieldTypeUnknownString;
    }
}

- (instancetype)initWithType:(CZFileNameTemplateFieldType)type {
    return [self initWithType:type value:nil];
}

- (instancetype)initWithType:(CZFileNameTemplateFieldType)type value:(NSString *)value {
    self = [super init];
    if (self) {
        _type = type;
        _value = [value copy];
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    NSString *type = dictionary[@"Type"];
    NSString *value = dictionary[@"Value"];
    
    if ([type isEqualToString:CZFileNameTemplateFieldTypeFreeTextString]) {
        self = [self initWithType:CZFileNameTemplateFieldTypeFreeText value:value];
    } else if ([type isEqualToString:CZFileNameTemplateFieldTypePreTextString]) {
        NSArray<NSString *> *candidates = dictionary[@"Candidates"];
        if ([candidates isKindOfClass:[NSArray class]]) {
            value = candidates.firstObject;
        }
        self = [self initWithType:CZFileNameTemplateFieldTypeFreeText value:value];
    } else if ([type isEqualToString:CZFileNameTemplateFieldTypeVariableTextString] && [value isEqualToString:CZFileNameTemplateFieldValueOperatorName]) {
        self = [self initWithType:CZFileNameTemplateFieldTypeFreeText value:value];
    } else if ([type isEqualToString:CZFileNameTemplateFieldTypeVariableTextString]) {
        self = [self initWithType:CZFileNameTemplateFieldTypeVariableText value:value];
    } else if ([type isEqualToString:CZFileNameTemplateFieldTypeAutoNumberString]) {
        self = [self initWithType:CZFileNameTemplateFieldTypeAutoNumber value:value];
    } else if ([type isEqualToString:CZFileNameTemplateFieldTypeDateString]) {
        self = [self initWithType:CZFileNameTemplateFieldTypeDate value:value];
    } else {
        self = [self initWithType:CZFileNameTemplateFieldTypeUnknown value:value];
    }
    
    NSNumber *userInput = dictionary[@"isUserInput"];
    if ([userInput isKindOfClass:[NSNumber class]]) {
        self.userInput = userInput.boolValue;
    }
    
    return self;
}

- (void)dealloc {
    [_value release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZFileNameTemplateField *field = [[[self class] allocWithZone:zone] initWithType:self.type value:self.value];
    field.userInput = self.isUserInput;
    return field;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZFileNameTemplateField class]]) {
        return NO;
    } else {
        return [self isEqualToField:object];
    }
}

- (BOOL)isEqualToField:(CZFileNameTemplateField *)otherField {
    return self.type == otherField.type && [self.value isEqualToString:otherField.value] && self.isUserInput == otherField.isUserInput;
}

- (NSDictionary *)toDictionary {
    NSString *typeString = [CZFileNameTemplateField stringFromType:_type];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{ @"Type" : typeString, @"isUserInput" : (self.isUserInput ? @YES : @NO)}];
    
    if (_value != nil) {
        [dict setObject:_value forKey:@"Value"];
    }
    
    return dict;
}

@end

@interface CZFileNameTemplate ()

@property (nonatomic, retain) NSMutableArray<CZFileNameTemplateField *> *fields;
@property (nonatomic, retain) NSArray<CZFileNameTemplateField *> *originalFields;

- (BOOL)fieldsToArray:(NSMutableArray *)array;
- (BOOL)fieldsFromArray:(NSArray *)array;

@end

@implementation CZFileNameTemplate

- (instancetype)init {
    self = [super init];
    if (self) {
        _fields = [[NSMutableArray alloc] init];
        _originalFields = [[NSMutableArray alloc] init];
        _separator = @"_";
    }
    return self;
}

- (instancetype)initWithContentsOfFile:(NSString *)path {
    self = [super init];
    if (self) {
        _path = [path copy];
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
        NSArray *array = dictionary[@"Fields"];
        if (!(array && [self fieldsFromArray:array])) {
            [self release];
            return nil;
        }
        
        _originalFields = [[NSArray alloc] initWithArray:_fields copyItems:YES];
        id obj = dictionary[@"Separator"];
        if ([obj isKindOfClass:[NSString class]]) {
            _separator = [obj copy];
        }
        _editable = YES;
    }

    return self;
}

- (void)dealloc {
    [_path release];
    [_fields release];
    [_originalFields release];
    [_separator release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZFileNameTemplate *template = [[[self class] allocWithZone:zone] init];
    template.path = self.path;
    [template.fields addObjectsFromArray:self.fields];
    return template;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZFileNameTemplate class]]) {
        return NO;
    } else {
        return [self isEqualToTemplate:object];
    }
}

- (BOOL)isEqualToTemplate:(CZFileNameTemplate *)otherTemplate {
    return [self.path isEqualToString:otherTemplate.path] && [self.fields isEqualToArray:otherTemplate.fields];
}

- (BOOL)isUpdated {
    return ![self.fields isEqualToArray:self.originalFields];
}

- (void)addField:(CZFileNameTemplateField *)field {
    [self.fields addObject:field];
}

- (NSUInteger)fieldCount {
    return [self.fields count];
}

- (CZFileNameTemplateField *)fieldAtIndex:(NSUInteger)index {
    return [self.fields objectAtIndex:index];
}

- (void)replaceFieldAtIndex:(NSUInteger)index withField:(CZFileNameTemplateField *)field {
    [self.fields replaceObjectAtIndex:index withObject:field];
}

- (void)removeFieldAtIndex:(NSUInteger)index {
    [self.fields removeObjectAtIndex:index];
}

- (void)insertField:(CZFileNameTemplateField *)field atIndex:(NSUInteger)index {
    [self.fields insertObject:field atIndex:index];
}

- (BOOL)writeToFile:(NSString *)path {
    NSMutableArray *array = [NSMutableArray array];
    if ([self fieldsToArray:array]) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        dictionary[@"Fields"] = array;
        
        if ([self.separator length] != 0) {
            dictionary[@"Separator"] = _separator;
        }
        
        return [dictionary writeToFile:path atomically:YES];
    } else {
        return NO;
    }
}

- (BOOL)isValid {
    for (CZFileNameTemplateField *field in self.fields) {
        if (field.type == CZFileNameTemplateFieldTypeFreeText ||
            field.type == CZFileNameTemplateFieldTypeDate ||
            field.type == CZFileNameTemplateFieldTypeVariableText ||
            field.type == CZFileNameTemplateFieldTypeAutoNumber) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Private methods

- (BOOL)fieldsToArray:(NSMutableArray *)array {
    if ([self.fields count] == 0) {
        return NO;
    }
    
    [array removeAllObjects];
    
    for (CZFileNameTemplateField *field in self.fields) {
        [array addObject:[field toDictionary]];
    }

    return YES;
}

- (BOOL)fieldsFromArray:(NSArray *)array {
    self.fields = [NSMutableArray array];  // remove all fields
    
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)obj;
            CZFileNameTemplateField *field = [[CZFileNameTemplateField alloc] initWithDictionary:dict];
            if (field) {
                [_fields addObject:field];
                [field release];
            }
        }
    }
    return YES;
}

@end
