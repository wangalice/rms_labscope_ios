//
//  CZFileEntity.h
//  FileKit
//
//  Created by Mike Wang on 4/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CZFileStorageType) {
    kLocal,
    kRemote,
    kUnknownStorage
};

@class CZFileDescriptor;

@interface CZFileEntity : NSObject <NSCopying>

@property (nonatomic, readonly) CZFileStorageType storageType;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, copy) NSString *filePath;  // TODO: make filePath to readonly
@property (nonatomic, assign, readonly) unsigned long long fileSize;
@property (nonatomic, retain, readonly) NSDate *creationDate;
@property (nonatomic, retain, readonly) NSDate *modificationDate;
@property (nonatomic, assign, readonly) BOOL isFolder;

@end

@interface CZLocalFileEntity : CZFileEntity

- (id)initWithFilePath:(NSString *)filePath;
- (id)initWithFilePath:(NSString *)filePath andFileAttributes:(NSDictionary *)fileAttributes;

@end

@interface CZRemoteFileEntity : CZFileEntity

- (id)initWithFileDescriptor:(CZFileDescriptor *)fileDescriptor;

@end
