//
//  CZFileHandler.h
//  FileKit
//
//  Created by Mike Wang on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef Hermes_CZFileHandler_h
#define Hermes_CZFileHandler_h

#include <iostream>

#define kSuccess 0
#define kFailed -1
#define kInvalidArg -2 //invalid input argument
#define kInvalidFileHeader -3 //invalid file header
#define kInvalidPlatform -4
#define kInvalidData -5 //invalid raw data
#define kNOMemory -6
#define kNotSupported -7
#define kNOFound -8

typedef enum
{
    kSet,
    kCurrent,
    kEnd
} CZIFileSeekMode;

typedef enum
{
    kRDONLY     = 0x0001,           //open for reading only
    kWRONLY     = 0x0002,           //open for writing only
    kRDWR       = kRDONLY|kWRONLY,  //open for reading and writing
    kCREAT      = 0x0008,           //create file if it does not exist
    kAPPEND     = 0x0010,           //append on each write
    kTRUNC      = 0x0020,           //truncate size to 0
    kEXCL       = 0x0040,           //error if O_CREAT and the file exists
    kSHLOCK     = 0x0080,           //atomically obtain a shared lock
    kEXLOCK     = 0x0100,           //atomically obtain an exclusive lock
} CZFileAttributeFlag;


#if defined (__OBJC__)
@class CZSMBAdapter;
#else
class CZSMBAdapter;
#endif

//TODO: need to refactor for local and remote file manager later
class CZFileHandler
{
public:
    CZFileHandler();
    ~CZFileHandler();
    
    class CZFileHandlerWrap;
    
    //If successful, returns a non-negative integer, termed a file descriptor.  It returns minus with error information.
    long openFile(const char *filePath, CZFileAttributeFlag fileOpenFlag);
    
    //Upon successful completion, return the number of bytes actually read and  placed in the buffer.  Minus for error code.
    long readFile(void *dataBuffer, size_t bufferSize);
    
    //Upon successful completion, return the number of bytes actually written and placed in the buffer.  Otherwise, Minus for error code
    void writeFile(const void *inputBuffer, size_t bufferSize);
    
    //seekFile repositions the offset of the file descriptor fileDescriptor to the argument offset,according to the directive seekMode.
    //Upon successful completion, seekFile returns the resulting offset location as measured in bytes from the beginning of the file.  Otherwise, a value of minus is returned to indicate the error.
    uint64_t seekFile(int64_t fileOffset, CZIFileSeekMode seekMode);
    
    int closeFile();
    
    uint64_t fileOffset();
    
private:
    CZSMBAdapter *_adapter;
    CZFileHandlerWrap *_fileHandler;
};

#endif
