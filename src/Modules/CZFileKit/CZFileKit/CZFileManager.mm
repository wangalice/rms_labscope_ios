//
//  CZFileManager.mm
//  FileKit
//
//  Created by Ralph Jin on 3/31/14.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFileManager.h"

NSString * const kCZFileUpdateNotification = @"kCZFileUpdateNotification";
NSString * const kCZFileUpdateNotificationFileList = @"kCZFileUpdateNotificationFileList";
NSString * const kCZFileUpdateNotificationIsImageNew = @"kCZFileUpdateNotificationIsImageNew";

NSString * const kCZFileRemoveNotification = @"kCZFileRemoveNotification";
NSString * const kCZFileRemoveNotificationFileList = @"kCZFileRemoveNotificationFileList";

@interface CZFileManager () {
    NSFileManager *_fileManager;  // assign
}

@end

@implementation CZFileManager

+ (CZFileManager *)defaultManager {
    static CZFileManager *defaultManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultManager = [[CZFileManager alloc] init];
    });
    
    return defaultManager;
}

- (id)init {
    self = [super init];
    if (self) {
        _fileManager = [NSFileManager defaultManager];
    }
    
    return self;
}

- (BOOL)moveItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error {
    BOOL success = [_fileManager moveItemAtPath:srcPath toPath:dstPath error:error];
    if (success) {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:kCZFileUpdateNotification
                                          object:nil
                                        userInfo:@{kCZFileUpdateNotificationFileList : @[dstPath]}];
        [notificationCenter postNotificationName:kCZFileRemoveNotification
                                          object:nil
                                        userInfo:@{kCZFileRemoveNotificationFileList : @[srcPath]}];
    }
    return success;
}

- (BOOL)removeItemAtPath:(NSString *)path error:(NSError **)error {
    BOOL success = [_fileManager removeItemAtPath:path error:error];
    if (success) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:kCZFileRemoveNotification
                              object:nil
                            userInfo:@{kCZFileRemoveNotificationFileList : @[path]}];
    }
    return success;
}

- (BOOL)mergeDirectory:(NSString *)srcDirectory
           toDirectory:(NSString *)destDirectory
             recursive:(BOOL)recursive
                 error:(NSError **)error {
    // TODO: consider error case when srcDirectory or destDirectory is a file.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = NO;
    if ([fileManager fileExistsAtPath:destDirectory isDirectory:&isDir]) {
        BOOL result = YES;
        NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:srcDirectory error:nil];
        for (NSString *content in contents) {
            NSString *srcPath = [srcDirectory stringByAppendingPathComponent:content];
            NSString *destPath = [destDirectory stringByAppendingPathComponent:content];
            
            if ([fileManager fileExistsAtPath:destPath isDirectory:&isDir]) {
                if (isDir && recursive) {
                    BOOL success = [self mergeDirectory:srcPath toDirectory:destPath recursive:YES error:error];
                    if (!success) {
                        result = NO;
                        break;
                    }
                } // else { do not override exist file }
            } else {
                [fileManager copyItemAtPath:srcPath toPath:destPath error:error];
            }
        }
        return result;
    } else {
        return [fileManager copyItemAtPath:srcDirectory toPath:destDirectory error:error];
    }
}

@end
