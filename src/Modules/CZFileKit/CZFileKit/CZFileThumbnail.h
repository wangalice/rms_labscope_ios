//
//  CZFileThumbnail.h
//  FileKit
//
//  Created by Mike Wang on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CZFileThumbnailDelegate <NSObject>

- (void)thumbnail:(UIImage *)thumbnail didGenerateFrom:(NSString *)path;

@end

@interface CZFileThumbnail : NSObject

@property (nonatomic, weak) id<CZFileThumbnailDelegate> delegate;

+ (void)clearCache;

/** create thumbnail of file.
 * @param filePath The full path of file.
 * @param modifiedDate The uptodate modified date of file.
 */
- (UIImage *)newThumbnailFrom:(NSString *)filePath modifiedDate:(NSDate *)modifiedDate;

/** remove thumbnail of file in the cache, for example the file is deleted. */
- (void)removeThumbnailFor:(NSString *)filePath;

@end

