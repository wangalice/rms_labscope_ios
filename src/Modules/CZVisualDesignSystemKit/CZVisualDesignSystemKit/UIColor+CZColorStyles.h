//  UIColor+CZColorStyles.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CZColorStyles)

// grayscale
@property (class, nonatomic, readonly, strong) UIColor *cz_gs10;    ///< #FFFFFF
@property (class, nonatomic, readonly, strong) UIColor *cz_gs20;    ///< #F5F7FA
@property (class, nonatomic, readonly, strong) UIColor *cz_gs30;    ///< #EDF2F7
@property (class, nonatomic, readonly, strong) UIColor *cz_gs35;    ///< #E4EBF2
@property (class, nonatomic, readonly, strong) UIColor *cz_gs40;    ///< #DDE3ED
@property (class, nonatomic, readonly, strong) UIColor *cz_gs50;    ///< #D1D9E5
@property (class, nonatomic, readonly, strong) UIColor *cz_gs60;    ///< #C1CBD9
@property (class, nonatomic, readonly, strong) UIColor *cz_gs70;    ///< #A5B1C2
@property (class, nonatomic, readonly, strong) UIColor *cz_gs80;    ///< #828D9E
@property (class, nonatomic, readonly, strong) UIColor *cz_gs85;    ///< #626C7A
@property (class, nonatomic, readonly, strong) UIColor *cz_gs90;    ///< #555E68
@property (class, nonatomic, readonly, strong) UIColor *cz_gs95;    ///< #404952
@property (class, nonatomic, readonly, strong) UIColor *cz_gs100;   ///< #353D45
@property (class, nonatomic, readonly, strong) UIColor *cz_gs105;   ///< #262E36
@property (class, nonatomic, readonly, strong) UIColor *cz_gs110;   ///< #1A2329
@property (class, nonatomic, readonly, strong) UIColor *cz_gs115;   ///< #171B24
@property (class, nonatomic, readonly, strong) UIColor *cz_gs120;   ///< #07090D

// primary blue
@property (class, nonatomic, readonly, strong) UIColor *cz_pb60;    ///< #A6C9FF
@property (class, nonatomic, readonly, strong) UIColor *cz_pb70;    ///< #85B6FF
@property (class, nonatomic, readonly, strong) UIColor *cz_pb80;    ///< #5C9DFF
@property (class, nonatomic, readonly, strong) UIColor *cz_pb90;    ///< #2E7AF5
@property (class, nonatomic, readonly, strong) UIColor *cz_pb100;   ///< #0050F2
@property (class, nonatomic, readonly, strong) UIColor *cz_pb110;   ///< #003ECD
@property (class, nonatomic, readonly, strong) UIColor *cz_pb120;   ///< #0029AF

// secondary colors - red
@property (class, nonatomic, readonly, strong) UIColor *cz_sr90;    ///< #FF3564
@property (class, nonatomic, readonly, strong) UIColor *cz_sr100;   ///< #ED2654
@property (class, nonatomic, readonly, strong) UIColor *cz_sr110;   ///< #DB1442

// secondary colors - pink
@property (class, nonatomic, readonly, strong) UIColor *cz_sp90;    ///< #FF52C0
@property (class, nonatomic, readonly, strong) UIColor *cz_sp100;   ///< #EE2FA8
@property (class, nonatomic, readonly, strong) UIColor *cz_sp110;   ///< #E7189B

// secondary colors - blue
@property (class, nonatomic, readonly, strong) UIColor *cz_sb90;    ///< #39CFFF
@property (class, nonatomic, readonly, strong) UIColor *cz_sb100;   ///< #13C6FF
@property (class, nonatomic, readonly, strong) UIColor *cz_sb110;   ///< #08B8EF

// secondary colors - green
@property (class, nonatomic, readonly, strong) UIColor *cz_sg90;    ///< #5FE8DA
@property (class, nonatomic, readonly, strong) UIColor *cz_sg100;   ///< #3ED2C3
@property (class, nonatomic, readonly, strong) UIColor *cz_sg110;   ///< #27C6B6

// secondary colors - yellow
@property (class, nonatomic, readonly, strong) UIColor *cz_sy90;    ///< #FFE669
@property (class, nonatomic, readonly, strong) UIColor *cz_sy100;   ///< #FFE148
@property (class, nonatomic, readonly, strong) UIColor *cz_sy110;   ///< #FCD510

// secondary colors - orange
@property (class, nonatomic, readonly, strong) UIColor *cz_so90;    ///< #FFAC4D
@property (class, nonatomic, readonly, strong) UIColor *cz_so100;   ///< #FFA33A
@property (class, nonatomic, readonly, strong) UIColor *cz_so110;   ///< #FF951C

@property (nonatomic, readonly, assign) NSInteger cz_hexIntValue;

+ (instancetype)cz_colorWithHexInt:(NSInteger)hexInt;
+ (instancetype)cz_colorWithRed:(UInt8)red green:(UInt8)green blue:(UInt8)blue;

@end
