//
//  UIFont+CZFontStyles.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIFont+CZFontStyles.h"
#import "CZButton.h"
#import <CoreText/CoreText.h>

@implementation UIFont (CZFontStyles)

+ (void)load {
    NSBundle *bundle = [NSBundle bundleForClass:[CZButton class]];
    [self _cz_registerFontWithFile:@"SourceSansPro-Light.ttf" inBundle:bundle];
    [self _cz_registerFontWithFile:@"SourceSansPro-Regular.ttf" inBundle:bundle];
    [self _cz_registerFontWithFile:@"SourceSansPro-Semibold.ttf" inBundle:bundle];
    [self _cz_registerFontWithFile:@"SourceSansPro-Bold.ttf" inBundle:bundle];
}

+ (void)_cz_registerFontWithFile:(NSString *)file inBundle:(NSBundle *)bundle {
    NSString *path = [bundle pathForResource:file ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:path];
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    CGFontRef font = CGFontCreateWithDataProvider(dataProvider);
    CFErrorRef error = NULL;
    CTFontManagerRegisterGraphicsFont(font, &error);
    CGFontRelease(font);
    CGDataProviderRelease(dataProvider);
}

+ (UIFont *)cz_h0 {
    return [self fontWithName:@"SourceSansPro-Light" size:52.0];
}

+ (UIFont *)cz_h1 {
    return [self fontWithName:@"SourceSansPro-Light" size:35.0];
}

+ (UIFont *)cz_h2 {
    return [self fontWithName:@"SourceSansPro-Light" size:26.0];
}

+ (UIFont *)cz_h3 {
    return [self fontWithName:@"SourceSansPro-Light" size:21.0];
}

+ (UIFont *)cz_h4 {
    return [self fontWithName:@"SourceSansPro-Regular" size:18.0];
}

+ (UIFont *)cz_subtitle1 {
    return [self fontWithName:@"SourceSansPro-Regular" size:15.0];
}

+ (UIFont *)cz_subtitle2 {
    return [self fontWithName:@"SourceSansPro-Bold" size:13.0];
}

+ (UIFont *)cz_body1 {
    return [self fontWithName:@"SourceSansPro-Regular" size:15.0];
}

+ (UIFont *)cz_caption {
    return [self fontWithName:@"SourceSansPro-Regular" size:13.0];
}

+ (UIFont *)cz_label1 {
    return [self fontWithName:@"SourceSansPro-Semibold" size:12.0f];
}

+ (UIFont *)cz_label2 {
    return [self fontWithName:@"SourceSansPro-Bold" size:11.0];
}

@end
