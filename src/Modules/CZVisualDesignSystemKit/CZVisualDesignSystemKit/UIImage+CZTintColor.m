//
//  UIImage+CZTintColor.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImage+CZTintColor.h"

/**
 refer to https://github.com/filipstefansson/UITintedButton
 */
@implementation UIImage (CZTintColor)

- (UIImage *)cz_imageWithTintColor:(UIColor *)tintColor {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(context, 0.0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGRect rect = CGRectMake(0.0, 0.0, self.size.width, self.size.height);
    
    // draw alpha-mask
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextDrawImage(context, rect, self.CGImage);
    
    // draw tint color, preserving alpha values of original image
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return coloredImage;
}

@end
