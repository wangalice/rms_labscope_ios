//
//  UIImage+CZIcon.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZIcon;

@interface UIImage (CZIcon)

+ (UIImage *)cz_imageWithIcon:(CZIcon *)icon;

@end
