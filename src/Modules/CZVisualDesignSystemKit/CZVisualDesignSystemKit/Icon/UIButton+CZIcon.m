//
//  UIButton+CZIcon.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIButton+CZIcon.h"
#import "CZICon+Theme.h"
#import "UIButton+CZTheme.h"

@implementation UIButton (CZIcon)

- (void)cz_setImageWithIcon:(CZIcon *)icon {
    [self cz_setImagePicker:^UIImage *(CZTheme theme) {
        return [icon imageForControlWithTheme:theme state:UIControlStateNormal];
    } forState:UIControlStateNormal];
    
    [self cz_setImagePicker:^UIImage *(CZTheme theme) {
        return [icon imageForControlWithTheme:theme state:UIControlStateSelected];
    } forState:UIControlStateSelected];
}

@end
