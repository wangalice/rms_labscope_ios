//
//  UIImage+CZIcon.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImage+CZIcon.h"
#import "CZThemeManager.h"
#import "CZICon+Theme.h"

@implementation UIImage (CZIcon)

+ (UIImage *)cz_imageWithIcon:(CZIcon *)icon {
    CZTheme theme = [[CZThemeManager sharedManager] currentTheme];
    UIImage *image = [icon imageForControlWithTheme:theme state:UIControlStateNormal];
    return image;
}

@end
