//
//  UIImageView+CZIcon.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImageView+CZIcon.h"
#import "UIImage+CZIcon.h"

@implementation UIImageView (CZIcon)

- (void)cz_setImageWithIcon:(CZIcon *)icon {
    self.image = [UIImage cz_imageWithIcon:icon];
}

@end
