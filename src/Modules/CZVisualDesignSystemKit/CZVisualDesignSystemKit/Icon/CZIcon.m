//
//  CZIcon.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 5/31/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIcon.h"
#import "UIColor+CZColorStyles.h"
#import <SVGKit/SVGKit.h>

const CGSize CZIconSizeSmall = {16.0, 16.0};
const CGSize CZIconSizeMedium = {24.0, 24.0};
const CGSize CZIconSizeLarge = {32.0, 32.0};

@interface CZIconTintColor : NSObject

@property (nonatomic, readonly, strong) UIColor *color;

@end

@implementation CZIconTintColor

- (instancetype)initWithColor:(UIColor *)color {
    self = [super init];
    if (self) {
        _color = color;
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZIconTintColor class]]) {
        return NO;
    } else {
        return [self isEqualToTintColor:object];
    }
}

- (BOOL)isEqualToTintColor:(CZIconTintColor *)otherTintColor {
    if (self.color == nil && otherTintColor.color == nil) {
        return YES;
    } else {
        return [self.color isEqual:otherTintColor.color];
    }
}

@end

@interface CZIconRenderingOptions ()

@property (nonatomic, strong) NSMutableDictionary<NSString *, CZIconTintColor *> *tintColors;

@end

@implementation CZIconRenderingOptions

+ (instancetype)alwaysOriginalRenderingOptions {
    CZIconRenderingOptions *options = [[self alloc] init];
    return options;
}

+ (instancetype)alwaysTemplateRenderingOptionsWithTintColor:(UIColor *)tintColor {
    CZIconRenderingOptions *options = [[self alloc] init];
    [options setTintColor:tintColor forElementIdentifier:nil];
    return options;
}

+ (instancetype)automaticRenderingOptionsWithTintColor:(UIColor *)tintColor {
    return [self automaticRenderingOptionsWithOriginalElementIdentifier:@"original" templateElementIdentifier:@"template" tintColor:tintColor];
}

+ (instancetype)automaticRenderingOptionsWithOriginalElementIdentifier:(NSString *)originalElementIdentifier templateElementIdentifier:(NSString *)templateElementIdentifier tintColor:(UIColor *)tintColor {
    CZIconRenderingOptions *options = [[self alloc] init];
    [options setTintColor:nil forElementIdentifier:originalElementIdentifier];
    [options setTintColor:tintColor forElementIdentifier:templateElementIdentifier];
    [options setTintColor:tintColor forElementIdentifier:nil];
    return options;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _size = CGSizeZero;
        _tintColors = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSUInteger)hash {
    return [NSStringFromCGSize(self.size) hash] + [self.tintColors hash];
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZIconRenderingOptions class]]) {
        return NO;
    } else {
        return [self isEqualToOptions:object];
    }
}

- (BOOL)isEqualToOptions:(CZIconRenderingOptions *)otherOptions {
    return CGSizeEqualToSize(self.size, otherOptions.size) && [self.tintColors isEqualToDictionary:otherOptions.tintColors];
}

- (void)setTintColor:(UIColor *)tintColor forElementIdentifier:(NSString *)elementIdentifier {
    elementIdentifier = elementIdentifier ?: @"";
    self.tintColors[elementIdentifier] = [[CZIconTintColor alloc] initWithColor:tintColor];
}

- (UIColor *)tintColorForElementIdentifier:(NSString *)elementIdentifier {
    elementIdentifier = elementIdentifier ?: @"";
    CZIconTintColor *tintColor = self.tintColors[elementIdentifier];
    return tintColor.color;
}

@end

@interface CZIcon ()

@property (nonatomic, strong) NSCache<CZIconRenderingOptions *, UIImage *> *images;

@end

@implementation CZIcon

+ (NSCache<NSString *, CZIcon *> *)cache {
    static NSCache<NSString *, CZIcon *> *cache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [[NSCache alloc] init];
    });
    return cache;
}

+ (instancetype)iconNamed:(NSString *)name {
    if (name.length == 0) {
        return nil;
    }
    
    CZIcon *icon = [[self cache] objectForKey:name];
    if (icon) {
        return icon;
    }
    
    icon = [[CZIcon alloc] initWithName:name];
    [[self cache] setObject:icon forKey:name];
    return icon;
}

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = [name copy];
        _images = [[NSCache alloc] init];
    }
    return self;
}

- (UIImage *)image {
    return [self imageWithOptions:nil];
}

- (UIImage *)imageWithOptions:(CZIconRenderingOptions *)options {
    if (options == nil) {
        options = [CZIconRenderingOptions alwaysOriginalRenderingOptions];
    }
    
    UIImage *image = [self.images objectForKey:options];
    if (image == nil) {
        NSDataAsset *asset = [[NSDataAsset alloc] initWithName:self.name];
        SVGKSource *source = [SVGKSourceNSData sourceFromData:asset.data URLForRelativeLinks:nil];
        if (source == nil) {
            source = [SVGKSourceLocalFile internalSourceAnywhereInBundleUsingName:self.name];
        }
        
        SVGKImage *svg = [SVGKImage imageWithSource:source];
        if (!CGSizeEqualToSize(options.size, CGSizeZero) && [svg hasSize]) {
            svg.size = options.size;
        }
        [self renderLayer:svg.CALayerTree options:options];
        image = svg.UIImage;
        
        [self.images setObject:image forKey:options];
    }
    return image;
}

- (void)renderLayer:(CALayer *)layer options:(CZIconRenderingOptions *)options {
    if (layer == nil || options.tintColors.count == 0) {
        return;
    }
    
    [self traverseLayer:layer usingBlock:^(CAShapeLayer *layer) {
        NSString *elementIdentifier = [layer valueForKey:kSVGElementIdentifier];
        UIColor *color = [options tintColorForElementIdentifier:elementIdentifier];
        if (color) {
            layer.fillColor = color.CGColor;
        }
    }];
}

- (void)traverseLayer:(CALayer *)layer usingBlock:(void (^)(CAShapeLayer *layer))block {
    NSMutableArray<CALayer *> *layers = [NSMutableArray array];
    [layers addObject:layer];
    do {
        CALayer *layer = layers.firstObject;
        if ([layer isKindOfClass:[CAShapeLayer class]] && block) {
            block((CAShapeLayer *)layer);
        }
        [layers removeObjectAtIndex:0];
        [layers addObjectsFromArray:layer.sublayers];
    } while (layers.count > 0);
}

@end
