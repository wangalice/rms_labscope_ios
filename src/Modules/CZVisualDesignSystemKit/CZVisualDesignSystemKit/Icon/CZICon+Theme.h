//
//  CZIcon+Theme.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIcon.h"
#import "CZVirturalDesignMacro.h"

@interface CZIconRenderingOptions (Theme)

+ (instancetype)renderingOptionsForControlWithSize:(CGSize)size;
+ (instancetype)renderingOptionsForControlWithSize:(CGSize)size theme:(CZTheme)theme state:(UIControlState)state;

@end

@interface CZIcon (Theme)

- (UIImage *)imageForControlWithSize:(CGSize)size;
- (UIImage *)imageForControlWithState:(UIControlState)state;
- (UIImage *)imageForControlWithTheme:(CZTheme)theme state:(UIControlState)state;

/// @param size CGSizeZero as default
/// @param theme [[CZThemeManager sharedManager] currentTheme] as default
/// @param state UIControlStateNormal as default
- (UIImage *)imageForControlWithSize:(CGSize)size theme:(CZTheme)theme state:(UIControlState)state;

@end
