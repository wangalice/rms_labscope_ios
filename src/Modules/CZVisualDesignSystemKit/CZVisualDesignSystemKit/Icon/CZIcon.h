//
//  CZIcon.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 5/31/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGSize CZIconSizeSmall;
extern const CGSize CZIconSizeMedium;
extern const CGSize CZIconSizeLarge;

@interface CZIconRenderingOptions : NSObject

/// CGSizeZero as default, render as its original size
@property (nonatomic, assign) CGSize size;

+ (instancetype)alwaysOriginalRenderingOptions;

+ (instancetype)alwaysTemplateRenderingOptionsWithTintColor:(UIColor *)tintColor;

/// Use "original" as originalElementIdentifier, "template" as templateElementIdentifier
+ (instancetype)automaticRenderingOptionsWithTintColor:(UIColor *)tintColor;

/// An original element in the svg image will keep its original fill color.
/// A template element in the svg image will be rendered using tint color.
/// If an element has not an identifier, it will be treated as a template element.
+ (instancetype)automaticRenderingOptionsWithOriginalElementIdentifier:(NSString *)originalElementIdentifier
                                             templateElementIdentifier:(NSString *)templateElementIdentifier
                                                             tintColor:(UIColor *)tintColor;

/// @param tintColor If tintColor is nil, keep the original fill color of the element
/// @param elementIdentifier If elementIdentifier is nil, use tintColor for all elements that have not identifier
- (void)setTintColor:(UIColor *)tintColor forElementIdentifier:(NSString *)elementIdentifier;

- (UIColor *)tintColorForElementIdentifier:(NSString *)elementIdentifier;

@end

/// Represent a svg image whose template element's fill color can by replaced by tint color.
@interface CZIcon : NSObject

@property (nonatomic, readonly, copy) NSString *name;

+ (instancetype)iconNamed:(NSString *)name;

/// Use alwaysOriginalRenderingOptions as options
- (UIImage *)image;

/// If options is nil, use alwaysOriginalRenderingOptions as default
- (UIImage *)imageWithOptions:(CZIconRenderingOptions *)options;

@end
