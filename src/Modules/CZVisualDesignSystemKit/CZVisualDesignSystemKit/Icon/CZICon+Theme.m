//
//  CZIcon+Theme.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 6/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZICon+Theme.h"
#import "UIColor+CZColorStyles.h"
#import "CZThemeManager.h"

@implementation CZIconRenderingOptions (Theme)

+ (instancetype)renderingOptionsForControlWithSize:(CGSize)size {
    CZTheme theme = [[CZThemeManager sharedManager] currentTheme];
    return [self renderingOptionsForControlWithSize:size theme:theme state:UIControlStateNormal];
}

+ (instancetype)renderingOptionsForControlWithSize:(CGSize)size theme:(CZTheme)theme state:(UIControlState)state {
    UIColor *tintColor = nil;
    switch (state) {
        case UIControlStateNormal:
        case UIControlStateHighlighted:
        case UIControlStateDisabled:
        case UIControlStateFocused:
            switch (theme) {
                case CZThemeDark:
                    tintColor = [UIColor cz_gs50];
                    break;
                case CZThemeLight:
                    tintColor = [UIColor cz_gs100];
                    break;
                case CZThemeBlue:
                    break;
            }
            break;
        case UIControlStateSelected:
            switch (theme) {
                case CZThemeDark:
                    tintColor = [UIColor cz_gs10];
                    break;
                case CZThemeLight:
                    tintColor = [UIColor cz_gs10];
                    break;
                case CZThemeBlue:
                    break;
            }
            break;
        default:
            break;
    }
    
    CZIconRenderingOptions *options = [self automaticRenderingOptionsWithTintColor:tintColor];
    options.size = size;
    return options;
}

@end

@implementation CZIcon (Theme)

- (UIImage *)imageForControlWithSize:(CGSize)size {
    CZTheme theme = [[CZThemeManager sharedManager] currentTheme];
    return [self imageForControlWithSize:size theme:theme state:UIControlStateNormal];
}

- (UIImage *)imageForControlWithState:(UIControlState)state {
    CZTheme theme = [[CZThemeManager sharedManager] currentTheme];
    return [self imageForControlWithSize:CGSizeZero theme:theme state:state];
}

- (UIImage *)imageForControlWithTheme:(CZTheme)theme state:(UIControlState)state {
    return [self imageForControlWithSize:CGSizeZero theme:theme state:state];
}

- (UIImage *)imageForControlWithSize:(CGSize)size theme:(CZTheme)theme state:(UIControlState)state {
    CZIconRenderingOptions *options = [CZIconRenderingOptions renderingOptionsForControlWithSize:size theme:theme state:state];
    return [self imageWithOptions:options];
}

@end
