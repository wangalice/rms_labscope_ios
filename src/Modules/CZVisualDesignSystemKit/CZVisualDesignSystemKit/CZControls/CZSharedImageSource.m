//
//  CZSharedImageSource.m
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/3/6.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSharedImageSource.h"
#import "CZShapeImage.h"
#import "UIColor+CZColorStyles.h"

@interface CZSharedImageSource ()

@property (nonatomic, strong) NSArray <NSString *> *imageSource;

@end

@implementation CZSharedImageSource

+ (instancetype)sharedImageSource {
    static CZSharedImageSource *shared= nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[CZSharedImageSource allocWithZone:NULL] init];
    });
    return shared;
}

- (instancetype)init {
    if (self = [super init]) {
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, 11.0, 11.0) cornerRadius:3.0];
        path.lineWidth = 1.0;

        // TODO: Change the light theme color when the UI design is finished
        
        //default
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs110]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-default-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_gs110] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_gs120] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-default-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs110]
                         strokeColor:[UIColor cz_gs80]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-default-button-focused"];

        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs120]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-default-button-pressed"];

        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs30]
                         strokeColor:[UIColor cz_gs60]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-default-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_gs30] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_gs60] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-default-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs30]
                         strokeColor:[UIColor cz_gs80]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-default-button-focused"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs50]
                         strokeColor:[UIColor cz_gs60]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-default-button-pressed"];

        //Active
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs90]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-active-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_gs90] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_gs120] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-active-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs90]
                         strokeColor:[UIColor cz_gs50]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-active-button-focused"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs105]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-active-button-pressed"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs80]
                         strokeColor:[UIColor cz_gs90]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-active-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_gs80] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_gs90] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-active-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs80]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-active-button-focused"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_gs90]
                         strokeColor:[UIColor cz_gs90]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-active-button-pressed"];
        
        //ActivePrimary
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb100]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-activePrimary-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_pb100] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_gs120] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-activePrimary-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb100]
                         strokeColor:[UIColor cz_gs10]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-activePrimary-button-focused"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb110]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"dark-activePrimary-button-pressed"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb100]
                         strokeColor:[UIColor cz_pb120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-activePrimary-button-enabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[[UIColor cz_pb100] colorWithAlphaComponent:0.5]
                         strokeColor:[[UIColor cz_pb120] colorWithAlphaComponent:0.5]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-activePrimary-button-disabled"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb100]
                         strokeColor:[UIColor cz_gs120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-activePrimary-button-focused"];
        
        [[CZShapeImage imageWithPath:path
                           fillColor:[UIColor cz_pb110]
                         strokeColor:[UIColor cz_pb120]
                           capInsets:UIEdgeInsetsMake(3.0, 3.0, 3.0, 3.0)]
         setName:@"light-activePrimary-button-pressed"];
        
    }
    return self;
}

- (UIImage *)imageWithName:(NSString *)imageName {
    if ([self.imageSource containsObject:imageName]) {
        // Resizing the image to fix the distortion when the picture is tiled
        return [[[CZShapeImage imageNamed:imageName] UIImage] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)
                                                                             resizingMode:UIImageResizingModeStretch];
    }
    return nil;
}

#pragma mark - Getters
- (NSArray<NSString *> *)imageSource {
    return @[@"dark-default-button-enabled",
             @"dark-default-button-disabled",
             @"dark-default-button-focused",
             @"dark-default-button-pressed",
             @"dark-active-button-enabled",
             @"dark-active-button-disabled",
             @"dark-active-button-focused",
             @"dark-active-button-pressed",
             @"dark-activePrimary-button-enabled",
             @"dark-activePrimary-button-disabled",
             @"dark-activePrimary-button-focused",
             @"dark-activePrimary-button-pressed",
             @"light-default-button-enabled",
             @"light-default-button-disabled",
             @"light-default-button-focused",
             @"light-default-button-pressed",
             @"light-active-button-enabled",
             @"light-active-button-disabled",
             @"light-active-button-focused",
             @"light-active-button-pressed",
             @"light-activePrimary-button-enabled",
             @"light-activePrimary-button-disabled",
             @"light-activePrimary-button-focused",
             @"light-activePrimary-button-pressed",
             ];
}

@end
