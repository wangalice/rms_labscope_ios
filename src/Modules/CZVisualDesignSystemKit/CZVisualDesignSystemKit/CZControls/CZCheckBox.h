//
//  CZCheckBox.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CZCheckBoxType) {
    kCZCheckBoxTypeMixed,
    kCZCheckBoxTypeActive
};

@interface CZCheckBox : UIButton

- (instancetype)init;

- (instancetype)initWithType:(CZCheckBoxType)checkBoxType;

@end

NS_ASSUME_NONNULL_END
