//
//  CZHorizontalMenu.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"
#import "CZThemeImage.h"

typedef NS_ENUM(NSInteger, CZHorizontalMenuStyle) {
    kCZHorizontalMenuStyleActive,
    kCZHorizontalMenuStyleActivePrimary
};

NS_ASSUME_NONNULL_BEGIN

@interface CZHorizontalMenu : UIControl

@property (nonatomic, readonly) NSUInteger numberOfItems;

@property (nonatomic) NSUInteger selectedIndex;

@property (nonatomic) CZControlState cz_state;

// items can be NSStrings or UIImages. Control is automatically sized to fit content;
- (instancetype)initWithItems:(nullable NSArray *)items style:(CZHorizontalMenuStyle)style;

//TODO: add automated Test configurtion
// use this property accessibilityIdentifier to support
// accessibilityIdentifier: A string that succinctly identifies the accessibility element.

- (void)setItemWithImagePicker:(CZThemeImagePicker)imagePicker atIndex:(NSUInteger)index forCZState:(CZControlState)state;

- (UIButton *)itemAtIndex:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
