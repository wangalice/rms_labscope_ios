//
//  CZDropdownMenu.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDropdownMenu.h"
#import "UIButton+CZTheme.h"
#import "UIFont+CZFontStyles.h"
#import "CZImageAssert.h"
#import "UIView+CZTheme.h"
#import "CZColorTable.h"
#import "UIColor+CZColorStyles.h"
#import "CALayer+CZTheme.h"
#import "UIImageView+CZTheme.h"
#import "CZThemeImage.h"

static float const kCZDropdownItemExpandAnimationDuration = 0.1f;

static NSString * const kCZDropdownExpandControllerDismissNotification = @"CZDropdownExpandControllerDimissNotification";

@interface CZDropdownExpandController : UIViewController

@property (nonatomic, assign) CGRect sourceRect;
/* Default is 32.0 */
@property (nonatomic, assign) CGFloat itemHeight;
/* Default is CZSizeZero, and default is not required. */
@property (nonatomic, assign) CGSize contentSize;
/* Default is CZDropdownAnimationDirectionDown */
@property (nonatomic, assign) CZDropdownAnimationDirection animationDirection;
@property (nonatomic, assign, readonly) BOOL isShown;

- (instancetype)initWithExpandItems:(NSArray <CZDropdownItem *>*)items;

+ (UIViewController *)currentWindowPresentedViewController;

@end

@interface CZDropdownPresentationController : UIPresentationController

@property (nonatomic, assign) CGRect sourceRect;

@end

@interface CZDropdownMenu ()

@property (nonatomic, strong) NSMutableArray <CZDropdownItem *> *items;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) CZDropdownAlignButton *button;
@property (nonatomic, copy)   NSString *title;
@property (nonatomic, strong) CZThemeImagePicker imagePicker;
@property (nonatomic, strong) CZDropdownMenuSelectedAction block;
@property (nonatomic, assign) CGFloat originalHeight;
@property (nonatomic, strong) CZDropdownExpandController *expandController;

@end

@implementation CZDropdownMenu

- (instancetype)initWithStyle:(CZDropdownStyle)style
                        title:(NSString *)title
                  imagePicker:(CZThemeImagePicker)imagePicker
                 expandTitles:(NSArray<NSString *> *)items {

    NSMutableArray *itemsArray = [NSMutableArray array];
    for (NSString *string in items) {
        CZDropdownItem *item = [CZDropdownItem itemWithTitle:string];
        [itemsArray addObject:item];
    }
    
    return [self initWithStyle:style
                         title:title
                   imagePicker:imagePicker
                   expandItems:itemsArray];
}

- (instancetype)initWithStyle:(CZDropdownStyle)style
                        title:(NSString *)title
                  imagePicker:(CZThemeImagePicker)imagePicker
                  expandItems:(NSArray<CZDropdownItem *> *)items {
    if (self = [super init]) {
        _items = [items mutableCopy];
        _style = style;
        _scrollEnable = NO;
        _contentSize = CGSizeZero;
        _title = [title copy];
        _imagePicker = [imagePicker copy];
        _animationDirection = CZDropdownAnimationDirectionDown;
        _itemHeight = 32.0f;
        _selectedIndex = CZDropdownMenuNoSelectedItem;
        _automaticallyTriggersPrimaryAction = YES;
        
        self.layer.cornerRadius = kControlDefaultBorderRadius;
        self.layer.borderWidth = kControlDefaultBorderWidth;
        self.layer.masksToBounds = YES;
        [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        
        //TODO: Add image to support theme change
        CZThemeImagePicker statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-main-down-enable"),
                                                                      Assert(@"dark-dropdown-main-down-enable"));;
        [self setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
        if (_style == kCZDropdownStyleAlternative) {
            statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-alternative-down-enable"),
                                                       Assert(@"dark-dropdown-alternative-down-enable"));
            self.backgroundColor = [UIColor clearColor];
            self.layer.borderWidth = kControlDefaultNoneBorderWidth;
        }
        [self.imageView setCz_imagePicker:statesImagePicker];
        self.button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.button setTitle:_title forState:UIControlStateNormal];
        if (imagePicker) {
            [self.button cz_setImagePicker:imagePicker forState:UIControlStateNormal];
        }
        
        [self addSubview:self.button];
        [self addSubview:self.imageView];

        //Auto layout
        [self addLayoutConstraints];
        
        UITapGestureRecognizer *tapGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(menuSelectEvent:)];
        [self addGestureRecognizer:tapGestureRec];
        
        for (CZDropdownItem *item in self.items) {
            [item addTarget:self action:@selector(itemSelectEvent:) forControlEvents:UIControlEventTouchUpInside];
        }

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(dropdownExpandViewControllerDismiss:)
                                                     name:kCZDropdownExpandControllerDismissNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kCZDropdownExpandControllerDismissNotification
                                                  object:nil];
    self.items = nil;
    self.contentSize = CGSizeZero;
    self.itemHeight = 0;
    self.selectedIndex = CZDropdownMenuNoSelectedItem;
    self.scrollEnable = NO;
}

#pragma mark - Public methods

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    NSParameterAssert(selectedIndex < (NSInteger)self.items.count);
    if (selectedIndex == _selectedIndex) return;
    if (selectedIndex != CZDropdownMenuNoSelectedItem) {
        CZDropdownItem *item = [self.items objectAtIndex:selectedIndex];
        item.selected = YES;
        self.title = [item.titleLabel.text copy];
        [self.button setTitle:self.title forState:UIControlStateNormal];
    }
    _selectedIndex = selectedIndex;
}

- (void)setSelectedItemTitle:(NSString *)selectedItemTitle {
    for (CZDropdownItem *item in self.items) {
        if ([item.titleLabel.text isEqualToString:selectedItemTitle]) {
            self.title = selectedItemTitle;
            [self.button setTitle:selectedItemTitle forState:UIControlStateNormal];
            NSInteger index = [self.items indexOfObject:item];
            _selectedIndex = index;
        }
    }
}

- (NSArray<CZDropdownItem *> *)expandItems {
    return [self.items copy];
}

- (void)setTitle:(NSString *)title index:(NSUInteger)index forState:(UIControlState)state {
    NSParameterAssert(index < self.items.count);  
    CZDropdownItem *item = self.items[index];
    [item setTitle:title forState:state];
}

- (void)setImagePicker:(CZThemeImagePicker)picker index:(NSUInteger)index forState:(UIControlState)state {
    NSParameterAssert(index < self.items.count);
    CZDropdownItem *item = self.items[index];
    [item setImagePicker:picker forState:state];
}

- (void)expandMenu {
    // show in superview
    if (self.isExpanded) return;
    
    CGRect rect = [self convertRect:self.bounds toView:[UIApplication sharedApplication].keyWindow];
    self.originalHeight = CGRectGetHeight(rect);
    
    if (!_expandController) {
        _expandController = [[CZDropdownExpandController alloc] initWithExpandItems:self.items];
    }
    _expandController.itemHeight = _itemHeight;
    _expandController.contentSize = CGSizeEqualToSize(_contentSize, CGSizeZero) ? CGSizeMake(rect.size.width, _itemHeight * _items.count) : CGSizeMake(rect.size.width, _contentSize.height);
    _expandController.animationDirection = _animationDirection;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kCZDropdownItemExpandAnimationDuration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        __strong typeof(weakSelf) strongSelf = self;
        CZThemeImagePicker imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-main-up"),
                                                                      Assert(@"dark-dropdown-main-up"));;
        if (weakSelf.style == kCZDropdownStyleAlternative) {
            imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-alternative-up"),
                                                       Assert(@"dark-dropdown-alternative-up"));
        }
        [strongSelf.imageView setCz_imagePicker:imagePicker];
        
        switch (weakSelf.animationDirection) {
            case CZDropdownAnimationDirectionUp: {
                strongSelf.expandController.sourceRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, 0);
            }
                break;
            case CZDropdownAnimationDirectionDown: {
                strongSelf.expandController.sourceRect = CGRectMake(rect.origin.x, rect.origin.y + rect.size.height, rect.size.width, 0);
            }
                break;
            default:
                break;
        }
        if (!strongSelf.expandController.isShown) {
            [[CZDropdownExpandController currentWindowPresentedViewController] presentViewController:strongSelf.expandController
                                                                                            animated:YES
                                                                                          completion:nil];
        }
    } completion:^(BOOL finished) {
        weakSelf.expanded = YES;
    }];
}

- (void)collapseMenu {
    if (!self.isExpanded) return;
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:kCZDropdownItemExpandAnimationDuration
                     animations:^{
                         __strong typeof(weakSelf) strongSelf = weakSelf;
                         CZThemeImagePicker statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-main-down-enable"),
                                                                                             Assert(@"dark-dropdown-main-down-enable"));;
                         if (weakSelf.style == kCZDropdownStyleAlternative) {
                             statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-alternative-down-enable"),
                                                                              Assert(@"dark-dropdown-alternative-down-enable"));
                         }
                         [strongSelf.imageView setCz_imagePicker:statesImagePicker];
                         switch (weakSelf.animationDirection) {
                             case CZDropdownAnimationDirectionUp: {
                                 
                             }
                                 break;
                             case CZDropdownAnimationDirectionDown: {
                                 
                             }
                                 break;
                             default:
                                 break;
                         }
                         if (weakSelf.expandController.isShown) {
                             [weakSelf.expandController dismissViewControllerAnimated:YES
                                                                       completion:nil];
                             strongSelf.expandController = nil;
                         }
                     } completion:^(BOOL finished) {
                         weakSelf.expanded = NO;
                     }];
}


- (void)invalidateMenu {
    if (self.expandController) {
        [self.expandController dismissViewControllerAnimated:YES completion:nil];
        self.expandController = nil;
    }
}

- (BOOL)isExpanded {
    return self.expandController.isShown;
}

- (void)selectItemAction:(CZDropdownMenuSelectedAction)action {
    self.block = action;
}

- (void)insertItem:(CZDropdownItem *)item atIndex:(NSInteger)index {
    NSParameterAssert(item);
    [self.items insertObject:item atIndex:index];
    [item addTarget:self action:@selector(itemSelectEvent:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addItem:(CZDropdownItem *)item {
    NSParameterAssert(item);
    [self.items addObject:item];
    [item addTarget:self action:@selector(itemSelectEvent:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeItem:(CZDropdownItem *)item {
    NSParameterAssert([self.items containsObject:item]);
    [self.items removeObject:item];
}

- (void)removeAllItems {
    [self.items removeAllObjects];
}

#pragma mark - Action
- (void)itemSelectAction:(CZDropdownItem *)sender {
    [self collapseMenu];
}

- (void)selectButtonEvent:(UIButton *)sender {
    if (self.automaticallyTriggersPrimaryAction) {
        [self expandMenu];
    }
    [self sendActionsForControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)itemSelectEvent:(CZDropdownItem *)item {
    [self collapseMenu];
    [self.button setTitle:[item.titleLabel.text copy] forState:UIControlStateNormal];
    NSInteger index = [self.items indexOfObject:item];
    self.selectedIndex = index;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    if (self.block) {
        self.block(index);
    }
}

- (void)menuSelectEvent:(UITapGestureRecognizer *)sender {
    if (self.isExpanded) {
        [self collapseMenu];
    } else {
        [self expandMenu];
    }
}

#pragma mark - Notification
- (void)dropdownExpandViewControllerDismiss:(NSNotification *)notif {
    CZThemeImagePicker statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-main-down-enable"),
                                                                        Assert(@"dark-dropdown-main-down-enable"));;
    if (self.style == kCZDropdownStyleAlternative) {
        statesImagePicker = CZThemeImagePickerWithImages(Assert(@"dark-dropdown-alternative-down-enable"),
                                                         Assert(@"dark-dropdown-alternative-down-enable"));
    }
    [self.imageView setCz_imagePicker:statesImagePicker];
}

#pragma mark - Private method
- (void)addLayoutConstraints {
    self.button.translatesAutoresizingMaskIntoConstraints = false;
    self.imageView.translatesAutoresizingMaskIntoConstraints = false;

    [self.button.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
    [self.button.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
    [self.button.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    [self.button.trailingAnchor constraintEqualToAnchor:self.imageView.leadingAnchor].active = YES;
    
    NSLayoutConstraint *imageRightLayoutContraint = [self.imageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-8.0];
    NSLayoutConstraint *imageWidthLayoutContraint = [self.imageView.widthAnchor constraintEqualToConstant:8.0];
    NSLayoutConstraint *imageHeightLayoutContraint = [self.imageView.heightAnchor constraintEqualToConstant:5.0];
    NSLayoutConstraint *imageCenterYLayoutContraint = [self.imageView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor];

    if (_style == kCZDropdownStyleAlternative) {
        imageRightLayoutContraint = [self.imageView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-6];
        imageWidthLayoutContraint = [self.imageView.widthAnchor constraintEqualToConstant:6.0];
        imageHeightLayoutContraint = [self.imageView.heightAnchor constraintEqualToConstant:3.0];
        imageCenterYLayoutContraint = [self.imageView.centerYAnchor constraintEqualToAnchor:self.bottomAnchor constant:-6.0];
    }
    imageRightLayoutContraint.active = YES;
    imageWidthLayoutContraint.active = YES;
    imageHeightLayoutContraint.active = YES;
    imageCenterYLayoutContraint.active = YES;
}

#pragma mark - Getters
- (NSMutableArray<CZDropdownItem *> *)items {
    if (!_items) {
        _items = [NSMutableArray array];
    }
    return _items;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

- (CZDropdownAlignButton *)button {
    if (!_button) {
        _button = [CZDropdownAlignButton buttonWithType:UIButtonTypeCustom];
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _button.adjustsImageWhenHighlighted = NO;
        _button.titleLabel.font = [UIFont cz_body1];
        [_button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                               forState:UIControlStateNormal];
        [_button addTarget:self
                    action:@selector(selectButtonEvent:)
          forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}

- (CGFloat)itemHeight {
    return _itemHeight;
}

@end

@interface CZDropdownItem ()

@end

@implementation CZDropdownItem

- (instancetype)initWithTitle:(NSString *)title imagePicker:(nullable CZThemeImagePicker)imagePicker {
    if (self = [super init]) {
        if (title) {
            [self setTitle:title forState:UIControlStateNormal];
        }
        
        if (imagePicker) {
            [self cz_setImagePicker:imagePicker forState:UIControlStateNormal];
        }
        
        CGFloat padding = 8;
        self.titleEdgeInsets = UIEdgeInsetsMake(0, padding, 0, -padding);
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.titleLabel.font = [UIFont cz_body1];
        self.adjustsImageWhenDisabled = NO;
        [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                            forState:UIControlStateNormal];
        [self setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
    }
    return self;
}

+ (instancetype)itemWithTitle:(nullable NSString *)title imagePicker:(nullable CZThemeImagePicker)imagePicker {
    CZDropdownItem *item = [[CZDropdownItem alloc] initWithTitle:title imagePicker:imagePicker];
    return item;
}

+ (instancetype)itemWithImagePicker:(CZThemeImagePicker)imagePicker {
    CZDropdownItem *item = [[CZDropdownItem alloc] initWithTitle:nil imagePicker:imagePicker];
    return item;
}

+ (instancetype)itemWithTitle:(NSString *)title {
    CZDropdownItem *item = [[CZDropdownItem alloc] initWithTitle:title imagePicker:NULL];
    return item;
}

- (void)setImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self cz_setImagePicker:imagePicker forState:state];
}

@end

@interface CZDropdownExpandController ()<
UIViewControllerTransitioningDelegate
>

@property (nonatomic, strong) NSArray <CZDropdownItem *>* items;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation CZDropdownExpandController

- (instancetype)initWithExpandItems:(NSArray <CZDropdownItem *>*)items {
    if (self = [super init]) {
        _items = [items copy];
        _itemHeight = 32;
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.frame = self.view.bounds;

    CGFloat contentWidth = MAX(_contentSize.width, [self maxLabelWidth]);
    CGFloat contentHeight = self.items.count * self.itemHeight;
    self.scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
    self.contentSize = CGSizeMake(contentWidth, contentHeight);
    [self.view addSubview:self.scrollView];

    [self drawBorder];
    
    for (CZDropdownItem *item in _items) {
        NSInteger index = [self.items indexOfObject:item];
        CGFloat seperatorLineHeight = 1.0;
        item.frame = CGRectMake(kControlDefaultBorderWidth,
                                index * _itemHeight + seperatorLineHeight,
                                _contentSize.width - kControlDefaultBorderWidth * 2,
                                _itemHeight - seperatorLineHeight);
        [self.scrollView addSubview:item];
        if (index == self.items.count - 1) {
            item.frame = CGRectMake(kControlDefaultBorderWidth,
                                    index * _itemHeight + seperatorLineHeight,
                                    _contentSize.width - kControlDefaultBorderWidth * 2,
                                    _itemHeight - seperatorLineHeight * 2);
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] postNotificationName:kCZDropdownExpandControllerDismissNotification
                                                        object:nil];
}

- (void)drawBorder {

    CGFloat borderRadius = kControlDefaultBorderRadius;
    
    CGPoint p1 = CGPointZero;
    CGPoint p2 = CGPointMake(0, _contentSize.height - kControlDefaultBorderWidth);;
    CGPoint p3 = CGPointMake(_contentSize.width, _contentSize.height - kControlDefaultBorderWidth);
    CGPoint p4 = CGPointMake(_contentSize.width, 0);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    if (_animationDirection == CZDropdownAnimationDirectionUp) {
        [path moveToPoint:p3];
        [path addLineToPoint:CGPointMake(p4.x, p4.y - borderRadius)];
        [path addArcWithCenter:CGPointMake(p4.x - borderRadius, p4.y + borderRadius)
                        radius:borderRadius
                    startAngle:0
                      endAngle:M_PI * 3/2
                     clockwise:NO];
        [path addLineToPoint:CGPointMake(p4.x - borderRadius, p4.y)];
        [path addLineToPoint:CGPointMake(p1.x + borderRadius, p1.y)];
        [path addArcWithCenter:CGPointMake(p1.x + borderRadius, p1.y + borderRadius)
                        radius:borderRadius
                    startAngle:M_PI * 3/2
                      endAngle:M_PI
                     clockwise:NO];
        [path addLineToPoint:CGPointMake(p1.x, p1.y + borderRadius)];
        [path addLineToPoint:p2];
    } else {
        [path moveToPoint:p1];
        [path addLineToPoint:CGPointMake(p2.x, p2.y - borderRadius)];
        [path addArcWithCenter:CGPointMake(p2.x + borderRadius, p2.y - borderRadius)
                        radius:borderRadius
                    startAngle:M_PI
                      endAngle:M_PI/2
                     clockwise:NO];
        [path addLineToPoint:CGPointMake(p2.x + borderRadius, p2.y)];
        [path addLineToPoint:CGPointMake(p3.x - borderRadius, p3.y)];
        [path addArcWithCenter:CGPointMake(p3.x - borderRadius, p3.y - borderRadius)
                        radius:borderRadius
                    startAngle:M_PI/2
                      endAngle:0
                     clockwise:NO];
        [path addLineToPoint:CGPointMake(p3.x, p3.y - borderRadius)];
        [path addLineToPoint:p4];
    }
    [path closePath];
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.path = path.CGPath;
    layer.strokeColor = [UIColor cz_gs120].CGColor;
    [self.scrollView.layer addSublayer:layer];
}

#pragma mark - Public
+ (UIViewController *)currentWindowPresentedViewController {
    UIViewController *viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [CZDropdownExpandController visiableViewController:viewController];
}

+ (UIViewController *)visiableViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        return [CZDropdownExpandController visiableViewController:[((UINavigationController *)viewController) visibleViewController]];
    } if ([viewController isKindOfClass:[UITabBarController class]]) {
        return [CZDropdownExpandController visiableViewController:[((UITabBarController *)viewController) selectedViewController]];
    } else if ([viewController isKindOfClass:[UIViewController class]]) {
        if (viewController.presentedViewController) {
            return [viewController presentedViewController];
        } else {
            return viewController;
        }
    }
    return viewController;
}

#pragma mark - Private

#pragma mark - Setters
- (void)setSourceRect:(CGRect)sourceRect {
    _sourceRect = sourceRect;
    _sourceRect.size.height = _contentSize.height == 0 ? _itemHeight * _items.count : _contentSize.height;
    if (_animationDirection == CZDropdownAnimationDirectionUp) {
        _sourceRect.origin.y -= _sourceRect.size.height - kControlDefaultBorderRadius;
    } else {
        _sourceRect.origin.y -= kControlDefaultBorderRadius;
    }
}

- (void)setContentSize:(CGSize)contentSize {
    _contentSize = contentSize;
    self.scrollView.contentSize = contentSize;
    self.scrollView.showsVerticalScrollIndicator = _contentSize.height < _itemHeight * _items.count ? YES : NO;
}

- (CGFloat)maxLabelWidth {
    CGFloat labelWidth = 0.0;
    for (CZDropdownItem *item in self.items) {
        CGFloat imageBtnMaxY = CGRectGetMaxY(item.imageView.frame) != 0 ? : 10.0;
        if (item.titleLabel.text != nil) {
            CGFloat contentWidth = [item.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: item.titleLabel.font}].width;
            labelWidth = MAX(labelWidth, contentWidth + imageBtnMaxY + 10.0);
        }
    }
    return labelWidth;
}

#pragma mark - Getters
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (BOOL)isShown {
    return [[self class] currentWindowPresentedViewController] == self ? YES : NO;
}

#pragma mark - UIViewControllerTransitioningDelegate
- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    CZDropdownPresentationController *presentationController = [[CZDropdownPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    presentationController.sourceRect = _sourceRect;
    return presentationController;
}

@end

@interface CZDropdownPresentationController ()

@property (nonatomic, strong)  UITapGestureRecognizer *tapGestureRec;

@end

@implementation CZDropdownPresentationController

- (CGRect)frameOfPresentedViewInContainerView {
    return _sourceRect;
}

- (void)presentationTransitionWillBegin {
    [self.containerView addGestureRecognizer:self.tapGestureRec];
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
    if (completed) {
        [self.containerView removeGestureRecognizer:self.tapGestureRec];
    }
}

#pragma mark - Getters
- (UITapGestureRecognizer *)tapGestureRec {
    if (!_tapGestureRec) {
       _tapGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(dismiss)];
    }
    return _tapGestureRec;
}

#pragma mark - Action
- (void)dismiss {
    if (self.presentedViewController) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

@end

@implementation CZDropdownAlignButton

- (UIEdgeInsets)contentEdgeInsets {
    CGFloat padding = 8;
    return UIEdgeInsetsMake(0, padding, 0, 0);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    CGRect rect = [super titleRectForContentRect:contentRect];
    rect.origin.x = CGRectGetMaxY(self.imageView.frame) + 10;
    return rect;
}

@end
