//
//  CZHorizontalMenu.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZHorizontalMenu.h"
#import "UIButton+CZTheme.h"
#import "CZColorTable.h"
#import "CALayer+CZTheme.h"
#import "UIView+CZTheme.h"

@interface CZHorizontalMenu ()

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) UIButton       *selectButton;
@property (nonatomic, strong) NSMutableArray <UIButton *> *buttons;
@property (nonatomic, assign) CZHorizontalMenuStyle style;

@end

@implementation CZHorizontalMenu

- (instancetype)initWithItems:(nullable NSArray *)items style:(CZHorizontalMenuStyle)style {
    if (self = [super init]) {
        _items = [items mutableCopy];
        _style = style;
        [self setup];
    }
    return self;
}

- (void)dealloc {
    NSLog(@"%s work well", __func__);
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat height = CGRectGetHeight(self.frame);
    CGFloat y = 0;
    CGFloat width = self.items.count ? (CGRectGetWidth(self.frame) - kControlDefaultBorderWidth * (self.items.count - 1))/self.items.count : 0;
    
    for (int i = 0; i < self.items.count; i++) {
        @autoreleasepool {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            id subObject = [self.items objectAtIndex:i];
            
            if ([subObject isKindOfClass:[NSString class]]) {
                NSString *title = (NSString *)subObject;
                [button setTitle:title forState:UIControlStateNormal];
            } else if ([subObject isKindOfClass:[UIImage class]]) {
                UIImage *image = (UIImage *)subObject;
                [button setImage:image forState:UIControlStateNormal];
            }
            
            [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                                  forState:UIControlStateNormal];
            [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                                  forState:UIControlStateDisabled];
            [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                                  forState:UIControlStateFocused];
            [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                                  forState:UIControlStateSelected];
            
            button.layer.cornerRadius = kControlDefaultBorderRadius;
            button.layer.borderWidth = kControlDefaultNoneBorderWidth;
            button.layer.masksToBounds = YES;
            [button.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
            button.frame = CGRectMake((width + kControlDefaultBorderWidth) * i, y, width, height);
            [button addTarget:self action:@selector(buttonSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            [self.buttons addObject:button];
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    switch (self.cz_state) {
        case CZControlStateEnable: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateDisable: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateFocused: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStatePressed: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateHover:
        default:
            break;
    }
}

#pragma mark - Public methods

- (void)setItemWithImagePicker:(CZThemeImagePicker)imagePicker atIndex:(NSUInteger)index forCZState:(CZControlState)state {
    NSParameterAssert(index < self.numberOfItems);
    UIButton *button = [self.buttons objectAtIndex:index];
    UIControlState controlState = UIControlStateNormal;
    switch (state) {
        case CZControlStateEnable: {
            controlState = UIControlStateNormal;
        }
            break;
        case CZControlStateDisable: {
            controlState = UIControlStateDisabled;
        }
            break;
        case CZControlStateFocused: {
            controlState = UIControlStateFocused;
        }
            break;
        case CZControlStatePressed: {
            controlState  = UIControlStateSelected;
        }
            break;
        case CZControlStateHover:
        default:
            break;
    }
    [button cz_setImagePicker:imagePicker forState:controlState];
}

- (UIButton *)itemAtIndex:(NSUInteger)index {
    NSParameterAssert(index < self.numberOfItems);
    return [self.buttons objectAtIndex:index];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    NSParameterAssert(selectedIndex < self.numberOfItems);
    self.selectedIndex = selectedIndex;
    UIButton *button = [self.buttons objectAtIndex:selectedIndex];
    button.selected = YES;
    self.selectButton = button;
}

- (NSUInteger)numberOfItems {
    return self.items.count;
}

#pragma mark - Action
- (void)buttonSelectedAction:(UIButton *)sender {
    if (self.selectButton != sender) {
        self.selectButton.selected = NO;
        switch (self.style) {
            case kCZHorizontalMenuStyleActive: {
                [self.selectButton setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
            }
                break;
            case kCZHorizontalMenuStyleActivePrimary: {
                [self.selectButton setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
            }
                break;
            default:
                break;
        }
        self.selectButton.layer.borderWidth = kControlDefaultNoneBorderWidth;
        self.selectButton = sender;
        self.selectButton.selected = YES;
        self.selectButton.layer.borderWidth = kControlDefaultBorderWidth;
        switch (self.style) {
            case kCZHorizontalMenuStyleActive: {
                [self.selectButton setCz_backgroundColorPicker:[CZColorTable defaultSelectedBackgroundColor]];
            }
                break;
            case kCZHorizontalMenuStyleActivePrimary: {
                [self.selectButton setCz_backgroundColorPicker:[CZColorTable defaultActivePrimaryBlueColor]];
            }
                break;
            default:
                break;
        }
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark - Private methods

- (void)setup {
    if (nil == self.buttons) {
        self.buttons = [NSMutableArray array];
    }
    [self.buttons removeAllObjects];
    
    [self setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
    [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
    self.layer.cornerRadius = kControlDefaultBorderRadius;
    self.layer.borderWidth = kControlDefaultBorderWidth;
    self.layer.masksToBounds = YES;
}


#pragma mark - Getters

- (NSMutableArray<UIButton *> *)buttons {
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}

@end

