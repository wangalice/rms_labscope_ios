//
//  CZToggleButton.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToggleButton.h"
#import "UIButton+CZTheme.h"
#import "CZColorTable.h"
#import "CALayer+CZTheme.h"
#import "UIView+CZTheme.h"
#import "UIFont+CZFontStyles.h"
#import "CZIcon.h"
#import "UIButton+CZIcon.h"

static NSString const *placeholder = @"placeholder";

@interface CZToggleButton ()

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) NSMutableArray *buttons;

@end

@implementation CZToggleButton

#pragma mark - Init methods

- (instancetype)initWithItems:(NSArray *)items {
    if (self = [super init]) {
        _items = [items mutableCopy];
//        _level = CZControlEmphasisDefault;
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat height = CGRectGetHeight(self.frame);
    CGFloat y = 0;
     CGFloat width = self.items.count ? (CGRectGetWidth(self.frame) - kControlDefaultBorderWidth * (self.items.count - 1))/self.items.count : 0;
    for (int i = 0; i < self.buttons.count; i++) {
        UIButton *button = [self.buttons objectAtIndex:i];
        button.frame = CGRectMake((width + kControlDefaultBorderWidth) * i, y, width, height);

    }

    switch (self.czState) {
        case CZControlStateEnable: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateDisable: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateFocused: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStatePressed: {
            [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
        }
            break;
        case CZControlStateHover:
        default:
            break;
    }
}

#pragma mark - Public methods
- (void)setSelectedIndex:(NSInteger)selectedIndex {
    NSParameterAssert(selectedIndex < (NSInteger)self.numberOfItems);
    if (selectedIndex == CZToggleButtonNoSelectedButton) {
        self.selectButton.selected = NO;
        [self updateButtonSelectedState:self.selectButton];
        self.selectButton = nil;
    } else {
        if (selectedIndex == _selectedIndex) return;
        if (_selectButton) {
            _selectButton.selected = NO;
            [self updateButtonSelectedState:_selectButton];
        }
        _selectedIndex = selectedIndex;
        UIButton *button = [self.buttons objectAtIndex:selectedIndex];
        button.selected = YES;
        self.selectButton = button;
        [self updateButtonSelectedState:button];
    }
}

- (void)setEnabled:(BOOL)enabled forToggleAtIndex:(NSUInteger)index {
    NSParameterAssert(index < self.numberOfItems);
    UIButton *button = [self.buttons objectAtIndex:index];
    button.enabled = enabled;
    if (enabled) {
        [button setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(1.0f, 1.0f)];
    } else {
        [button setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(0.5f, 0.5f)];
    }
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    
    if (enabled) {
        [self setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(1.0f, 1.0f)];
    } else {
        [self setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(0.5f, 0.5f)];
    }
}

- (void)setLevel:(CZControlEmphasisLevel)style {
    _level = style;
    [self layoutIfNeeded];
}

- (void)setCzState:(CZControlState)czState {
    _czState = czState;
    [self layoutIfNeeded];
}

- (void)setTitle:(NSAttributedString *)title index:(NSInteger)index state:(CZControlState)state {
    NSParameterAssert(index < self.numberOfItems);
}

- (void)setTitleTextAttributes:(NSDictionary<NSAttributedStringKey,id> *)attributes forState:(UIControlState)state {
    
}

- (NSDictionary<NSAttributedStringKey,id> *)titleTextAttributesForState:(UIControlState)state {
    switch (state) {
        case UIControlStateNormal: {
            
        }
            break;
        case UIControlStateSelected: {
            
        }
            break;
        case UIControlStateHighlighted: {
            
        }
            break;
        case UIControlStateDisabled: {
            
        }
            break;
        case UIControlStateFocused: {
            
        }
            break;
        case UIControlStateReserved: {
           
        }
            break;
        case UIControlStateApplication: {
            
        }
            break;
    }
    return nil;
}

- (NSUInteger)numberOfItems {
    return self.items.count;
}

- (void)insertButtonItemWithTitle:(nullable NSString *)title atIndex:(NSUInteger)index animated:(BOOL)animated {
    [self insertButtonItemWithObject:title atIndex:index animated:animated];
}

- (void)insertButtonItemWithImage:(nullable UIImage *)image atIndex:(NSUInteger)index animated:(BOOL)animated {
    [self insertButtonItemWithObject:image atIndex:index animated:animated];
}

- (void)insertButtonItemWithCZIcon:(nullable CZIcon *)icon atIndex:(NSUInteger)index animated:(BOOL)animated {
    [self insertButtonItemWithObject:icon atIndex:index animated:animated];
}

- (void)removeButtonItemIndex:(NSUInteger)index animated:(BOOL)animated {
    NSParameterAssert(index < self.items.count);

    UIButton *button  = [self.buttons objectAtIndex:index];
    [button removeFromSuperview];
    [self.buttons removeObjectAtIndex:index];
    [self.items removeObjectAtIndex:index];
    
    [self updateViewWithAnimated:animated];
}

- (void)removeAllButtonItems {
    for (UIButton *button in self.buttons) {
        [button removeFromSuperview];
    }
    [self.buttons removeAllObjects];
    [self.items removeAllObjects];

    [self updateViewWithAnimated:YES];
}

#pragma mark - Private methods
- (void)setup {
    
    if (nil == self.buttons) {
        self.buttons = [NSMutableArray array];
    }
    [self.buttons removeAllObjects];

    for (int i = 0; i < self.items.count; i++) {
        @autoreleasepool {
            id subObject = self.items[i];
            UIButton *button = [self buttonItemWithObject:subObject];
            [self addSubview:button];
            
            [self.buttons addObject:button];
        }
    }

    _selectedIndex = CZToggleButtonNoSelectedButton;
    [self setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
    [self.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
    self.layer.cornerRadius = kControlDefaultBorderRadius;
    self.layer.borderWidth = kControlDefaultBorderWidth;
    self.layer.masksToBounds = YES;
}

- (void)updateButtonSelectedState:(UIButton *)button {
    if (button) {
        if (button.selected) {
            switch (self.level) {
                case CZControlEmphasisDefault: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultSelectedBackgroundColor]];
                }
                    break;
                case CZControlEmphasisActive: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultSelectedBackgroundColor]];
                }
                case CZControlEmphasisActivePrimary: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultActivePrimaryBlueColor]];
                }
                    break;
                default:
                    break;
            }
            self.selectButton.layer.borderWidth = kControlDefaultBorderWidth;
            self.selectButton.titleLabel.font = [UIFont cz_subtitle1];
        } else {
            switch (self.level) {
                case CZControlEmphasisDefault: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
                }
                    break;
                case CZControlEmphasisActive: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
                }
                    break;
                case CZControlEmphasisActivePrimary: {
                    [button setCz_backgroundColorPicker:[CZColorTable defaultBackgroundColor]];
                }
                    break;
                default:
                    break;
            }
            button.layer.borderWidth = kControlDefaultNoneBorderWidth;
            self.selectButton.titleLabel.font = [UIFont cz_body1];
        }
    }
}

- (void)updateViewWithAnimated:(BOOL)animated {
    [self layoutIfNeeded];
}

- (void)insertButtonItemWithObject:(id)object atIndex:(NSUInteger)index animated:(BOOL)animated {
    NSParameterAssert(index < self.items.count);
    
    object = object ? : placeholder;
    UIButton *button = [self buttonItemWithObject:object];
    [self.buttons insertObject:button atIndex:index];
    [self.items insertObject:object atIndex:index];
    
    [self updateViewWithAnimated:animated];
}

- (UIButton *)buttonItemWithObject:(id)object {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([object isKindOfClass:[NSString class]]) {
        NSString *title = (NSString *)object;
        [button setTitle:title forState:UIControlStateNormal];
    } else if ([object isKindOfClass:[UIImage class]]) {
        UIImage *image = (UIImage *)object;
        [button setImage:image forState:UIControlStateNormal];
    } else if ([object isKindOfClass:[CZIcon class]]) {
        CZIcon *icon = (CZIcon *)object;
        [button cz_setImageWithIcon:icon];
    }
    [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                          forState:UIControlStateNormal];
    [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                          forState:UIControlStateDisabled];
    [button cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                          forState:UIControlStateFocused];
    [button cz_setTitleColorPicker:[CZColorTable defaultActiveTextColor]
                          forState:UIControlStateSelected];
    
    button.layer.cornerRadius = kControlDefaultBorderRadius;
    button.layer.borderWidth = kControlDefaultNoneBorderWidth;
    button.layer.masksToBounds = YES;
    [button.layer setCz_borderColorPicker:[CZColorTable defaultNormalBorderColor]];
    button.titleLabel.font = [UIFont cz_body1];
    [button addTarget:self action:@selector(buttonSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

#pragma mark - Action
- (void)buttonSelectedAction:(UIButton *)sender {
    if (sender == self.selectButton) return;
    
    if (self.selectButton != sender) {
        self.selectButton.selected = NO;
        [self updateButtonSelectedState:self.selectButton];
        self.selectButton = sender;
        self.selectButton.selected = YES;
        [self updateButtonSelectedState:self.selectButton];
    }
    self.selectedIndex = [self.buttons indexOfObject:sender];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end
