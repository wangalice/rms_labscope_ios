//
//  CZCheckBox.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCheckBox.h"
#import "UIButton+CZTheme.h"
#import "CZColorTable.h"
#import "UIFont+CZFontStyles.h"
#import "UIView+CZTheme.h"
#import "CZThemeImage.h"
#import "UIImage+CZTintColor.h"
#import "CZImageAssert.h"

@interface CZCheckBox ()

@property (nonatomic, assign) CZCheckBoxType checkBoxType;

@end

@implementation CZCheckBox

- (instancetype)init {
    return [self initWithType:kCZCheckBoxTypeActive];
}

- (instancetype)initWithType:(CZCheckBoxType)checkBoxType {
    if (self = [super init]) {
        _checkBoxType = checkBoxType;
        [self setup];
    }
    return self;
}

- (void)setup {
    CGFloat padding = 8;
    self.titleEdgeInsets = UIEdgeInsetsMake(0, padding, 0, -padding);
    
    NSString *typeString = @"mixed";
    if (_checkBoxType == kCZCheckBoxTypeActive) {
        typeString = @"active";
    }

    if (self.selected) {
        [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-enable", typeString]),
                                                                  Assert([NSString stringWithFormat:@"light-checkbox-%@-enable", typeString])]]
                       forState:UIControlStateNormal];
    } else {
        typeString = @"default";
        [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-enable", typeString]),
                                                                  Assert([NSString stringWithFormat:@"light-checkbox-%@-enable", typeString])]]
                       forState:UIControlStateNormal];
    }
    
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-pressed", typeString]),
                                                              Assert([NSString stringWithFormat:@"light-checkbox-%@-pressed", typeString])]]
                   forState:UIControlStateSelected];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-disabled", typeString]),
                                                              Assert([NSString stringWithFormat:@"light-checkbox-%@-disabled", typeString])]]
                   forState:UIControlStateDisabled];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-focused", typeString]),
                                                              Assert([NSString stringWithFormat:@"light-checkbox-%@-focused", typeString])]]
                   forState:UIControlStateFocused];
    
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateNormal];
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateFocused];
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateDisabled];

    self.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont cz_body1];
    self.adjustsImageWhenDisabled = NO;
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];

    NSString *typeString = @"mixed";
    if (_checkBoxType == kCZCheckBoxTypeActive) {
        typeString = @"active";
    }

    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert([NSString stringWithFormat:@"dark-checkbox-%@-pressed", typeString]),
                                                              Assert([NSString stringWithFormat:@"light-checkbox-%@-pressed", typeString])]]
                   forState:UIControlStateSelected];
    
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateSelected];
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];

    if (enabled) {
        [self setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(1.0f, 1.0f)];
    } else {
        [self setCz_alphaPicker:CZThemeAlphaPickerWithAlphas(0.5f, 0.5f)];
    }
}

@end
