//
//  CZSlider.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSlider.h"
#import "CZComponentState.h"
#import "UISlider+CZTheme.h"
#import "CZImageAssert.h"
#import "UIFont+CZFontStyles.h"
#import "UIColor+CZColorStyles.h"

@interface CZSlider ()

@property (nonatomic, strong) UILabel *minimumValueTextLabel;
@property (nonatomic, strong) UILabel *maximumValueTextLabel;

@end

@implementation CZSlider

- (instancetype)initWithType:(CZSliderType)type {
    if (self = [super init]) {
        _type = type;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame type:(CZSliderType)type {
    return [self initWithType:CZSliderHorizontalOneHandle];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    return [self initWithType:CZSliderHorizontalOneHandle];
}

- (instancetype)init {
    return [self initWithType:CZSliderHorizontalOneHandle];
}

- (void)setup {

    switch (_type) {
        case CZSliderHorizontalOneHandle: {
            //Thumb
            [self cz_setThumbImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-thumb-enable"),
                                                                      Assert(@"light-slider-thumb-enable"))
                                forState:UIControlStateNormal];
            
            [self cz_setThumbImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-thumb-disable"),
                                                                      Assert(@"light-slider-thumb-disable"))
                                forState:UIControlStateDisabled];
            [self cz_setThumbImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-thumb-pressed"),
                                                                      Assert(@"light-slider-thumb-pressed"))
                                forState:UIControlStateHighlighted];
            
            //Minimum
            [self cz_setMinimumTrackImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-horizontal-minimumTrack-enable"),
                                                                             Assert(@"light-slider-horizontal-minimumTrack-enable"))
                                       forState:UIControlStateNormal];
            [self cz_setMinimumTrackImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-horizontal-minimumTrack-disable"),
                                                                             Assert(@"light-slider-horizontal-minimumTrack-disable"))
                                       forState:UIControlStateDisabled];
            //Maximum
            [self cz_setMaximumTrackImagePicker:CZThemeImagePickerWithImages(Assert(@"dark-slider-horizontal-maximumTrack"),
                                                                             Assert(@"light-slider-horizontal-maximumTrack"))
                                       forState:UIControlStateNormal];
        }
            break;
        case CZSliderVerticalOneHandle: {
            //TODO: Support here
        }
            break;
        case CZSliderVerticalTwoHandle: {
            //TODO: Support here
        }
            break;
        case CZSliderHorizontalTwoHandle: {
            //TODO: Support here
        }
            break;
        default:
            break;
    }
}

- (void)setMinimumValueText:(NSString *)minimumValueText {
    _minimumValueText = [minimumValueText.uppercaseString copy];
    
    if (self.minimumValueTextLabel == nil) {
        self.minimumValueTextLabel = [[UILabel alloc] init];
        self.minimumValueTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.minimumValueTextLabel.font = [UIFont cz_label2];
        self.minimumValueTextLabel.textColor = [UIColor cz_gs80];
        self.minimumValueTextLabel.textAlignment = NSTextAlignmentLeft;
        
        [self addSubview:self.minimumValueTextLabel];
        
        [self.minimumValueTextLabel.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:2.0].active = YES;
        [self.minimumValueTextLabel.topAnchor constraintEqualToAnchor:self.centerYAnchor constant:10.0].active = YES;
        [self.minimumValueTextLabel.widthAnchor constraintEqualToAnchor:self.widthAnchor multiplier:0.5 constant:-4.0].active = YES;
        [self.minimumValueTextLabel.heightAnchor constraintEqualToConstant:8.0].active = YES;
    }
    
    self.minimumValueTextLabel.text = _minimumValueText;
}

- (void)setMaximumValueText:(NSString *)maximumValueText {
    _maximumValueText = [maximumValueText.uppercaseString copy];
    
    if (self.maximumValueTextLabel == nil) {
        self.maximumValueTextLabel = [[UILabel alloc] init];
        self.maximumValueTextLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.maximumValueTextLabel.font = [UIFont cz_label2];
        self.maximumValueTextLabel.textColor = [UIColor cz_gs80];
        self.maximumValueTextLabel.textAlignment = NSTextAlignmentRight;
        
        [self addSubview:self.maximumValueTextLabel];
        
        [self.maximumValueTextLabel.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-2.0].active = YES;
        [self.maximumValueTextLabel.topAnchor constraintEqualToAnchor:self.centerYAnchor constant:10.0].active = YES;
        [self.maximumValueTextLabel.widthAnchor constraintEqualToAnchor:self.widthAnchor multiplier:0.5 constant:-4.0].active = YES;
        [self.maximumValueTextLabel.heightAnchor constraintEqualToConstant:8.0].active = YES;
    }
    
    self.maximumValueTextLabel.text = _maximumValueText;
}

@end
