//
//  CZComponentState.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

UIKIT_EXTERN NSArray<NSNumber *> *UIControlStatesFromCZControlState(CZControlState state);

@interface  CZComponentState : NSObject

@end

