//
//  CZToggleButton.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

NS_ASSUME_NONNULL_BEGIN

enum {
    CZToggleButtonNoSelectedButton = -1 // dropdown item index for no selected item
};

@class CZIcon;

@interface CZToggleButton : UIControl <NSCoding>

// items can be NSStrings or UIImages or CZIcons. Control is automatically sized to fit content;
- (instancetype)initWithItems:(nullable NSArray *)items;

// ignored in momentary mode. returns last segment pressed. default is -1 until a toggle button is pressed
// the UIControlEventValueChanged action is invoked when the segment changes via a user event. set to -1 to turn off selection
@property (nonatomic) NSInteger selectedIndex;

// Default is CZControlEmphasisDefault. If you want to emphasize this control, set the style to CZControlEmphasisActivePrimary;
@property (nonatomic) CZControlEmphasisLevel level;

@property (nonatomic) CZControlState czState;

@property (nonatomic, readonly) NSUInteger numberOfItems;

- (void)setEnabled:(BOOL)enabled forToggleAtIndex:(NSUInteger)index;

- (void)setTitle:(NSAttributedString *)title index:(NSInteger)index state:(CZControlState)state;

/* You may specify the font, text color, and shadow properties for the title in the text attributes dictionary, using the keys found in NSAttributedString.h.
 */
- (void)setTitleTextAttributes:(nullable NSDictionary<NSAttributedStringKey,id> *)attributes forState:(UIControlState)state ;
- (nullable NSDictionary<NSAttributedStringKey,id> *)titleTextAttributesForState:(UIControlState)state;

// insert before segment number. 0..#segments. value pinned
- (void)insertButtonItemWithTitle:(nullable NSString *)title atIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)insertButtonItemWithImage:(nullable UIImage *)image atIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)insertButtonItemWithCZIcon:(nullable CZIcon *)icon atIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)removeButtonItemIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)removeAllButtonItems;

@end

NS_ASSUME_NONNULL_END
