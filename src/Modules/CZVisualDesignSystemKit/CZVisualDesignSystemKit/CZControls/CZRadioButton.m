//
//  CZRadioButton.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZRadioButton.h"
#import "UIButton+CZTheme.h"
#import "CZColorTable.h"
#import "UIFont+CZFontStyles.h"
#import "UILabel+CZTheme.h"
#import "UIView+CZTheme.h"
#import "CZImageAssert.h"

@implementation CZRadioButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (void)setup {
    CGFloat padding = 8;
    self.titleEdgeInsets = UIEdgeInsetsMake(0, padding, 0, -padding);
    
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert(@"dark-radio-default-enable"), Assert(@"light-radio-default-enable")]] forState:UIControlStateNormal];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert(@"dark-radio-active-pressed"), Assert(@"light-radio-active-pressed")]] forState:UIControlStateSelected];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert(@"dark-radio-default-disabled"), Assert(@"light-radio-default-disabled")]] forState:UIControlStateDisabled];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert(@"dark-radio-active-disable"), Assert(@"light-radio-active-disable")]] forState:UIControlStateSelected | UIControlStateDisabled];
    [self cz_setImagePicker:[CZThemeImage imageWithUIImages:@[Assert(@"dark-radio-default-focused"), Assert(@"light-radio-default-focused")]] forState:UIControlStateFocused];
    
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateNormal];
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateFocused];
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateDisabled];
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateSelected];

    self.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont cz_body1];
    self.adjustsImageWhenDisabled = NO;
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;

}

@end
