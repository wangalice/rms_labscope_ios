//
//  CZSharedImageSource.h
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/3/6.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZSharedImageSource : NSObject

+ (instancetype)sharedImageSource;

- (UIImage *)imageWithName:(NSString *)imageName;

@end

NS_ASSUME_NONNULL_END
