//
//  CZButton.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+CZTheme.h"

@interface CZButton : UIButton

// Only support default, activePrimary; set active is equal to defult
+ (instancetype)buttonWithLevel:(CZControlEmphasisLevel)level;

- (instancetype)initWithLevel:(CZControlEmphasisLevel)level;

// Default is CZControlEmphasisDefault. If you want to emphasize this control, set the style to CZControlEmphasisActivePrimary;
@property (nonatomic) CZControlEmphasisLevel level;

@property (nonatomic) CZControlState czState;

@end
