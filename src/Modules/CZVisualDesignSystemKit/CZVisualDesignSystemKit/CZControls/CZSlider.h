//
//  CZSlider.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 Support the slider type as UX design;
 Reference:
 https://www.figma.com/file/vMeLQZQBMU0gKnghKd23PI/1.0-General-Component-Library?node-id=1818%3A413

 - CZSliderHorizontalOneHandle: horizontal slider with one handle to chang value;
 - CZSliderHorizontalTwoHandle: horizontal slider with two handle to change value;
 - CZSliderVerticalOneHandle: vertical slider with one handle to change value;
 - CZSliderVerticalTwoHandle: vertical slider with two handle to change vlaue;
 */
typedef NS_ENUM(NSUInteger, CZSliderType) {
    CZSliderHorizontalOneHandle,
    CZSliderHorizontalTwoHandle,
    CZSliderVerticalOneHandle,
    CZSliderVerticalTwoHandle
};

@interface CZSlider : UISlider


@property (nonatomic) CZSliderType type;

@property (nonatomic, copy) NSString *minimumValueText;
@property (nonatomic, copy) NSString *maximumValueText;

- (instancetype)initWithType:(CZSliderType)type;

- (instancetype)initWithFrame:(CGRect)frame type:(CZSliderType)type;

@end
