//
//  CZShapeImage.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZShapeImage.h"

@implementation CZShapeImage

@synthesize UIImage = _UIImage;

+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor {
    return [[[self class] alloc] initWithPath:path fillColor:fillColor strokeColor:nil capInsets:UIEdgeInsetsZero];
}

+ (instancetype)imageWithPath:(UIBezierPath *)path strokeColor:(UIColor *)strokeColor {
    return [[[self class] alloc] initWithPath:path fillColor:nil strokeColor:strokeColor capInsets:UIEdgeInsetsZero];
}

+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor {
    return [[[self class] alloc] initWithPath:path fillColor:fillColor strokeColor:strokeColor capInsets:UIEdgeInsetsZero];
}

+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor capInsets:(UIEdgeInsets)capInsets {
    return [[[self class] alloc] initWithPath:path fillColor:fillColor strokeColor:strokeColor capInsets:capInsets];
}

- (instancetype)initWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor capInsets:(UIEdgeInsets)capInsets {
    self = [super init];
    if (self) {
        _path = path;
        _fillColor = fillColor;
        _strokeColor = strokeColor;
        _capInsets = capInsets;
    }
    return self;
}

+ (NSMutableDictionary<NSString *, CZShapeImage *> *)cache {
    static NSMutableDictionary<NSString *, CZShapeImage *> *cache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [[NSMutableDictionary alloc] init];
    });
    return cache;
}

- (void)setName:(NSString *)name {
    NSAssert([[[self class] cache] objectForKey:name] == nil, @"A shape image with the same name is already in the cache.");
    [[[self class] cache] setObject:self forKey:name];
}

+ (instancetype)imageNamed:(NSString *)name {
    return [[self cache] objectForKey:name];
}

- (UIImage *)UIImage {
    if (_UIImage == nil) {
        CGFloat lineWidth = self.path.lineWidth;
        if (self.strokeColor == nil) {
            lineWidth = 0.0;
        }
        
        CGFloat width = self.path.bounds.size.width + lineWidth;
        CGFloat height = self.path.bounds.size.height + lineWidth;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, [[UIScreen mainScreen] scale]);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM(context, lineWidth / 2, lineWidth / 2);
        
        if (self.fillColor) {
            [self.fillColor setFill];
            [self.path fill];
        }
        
        if (self.strokeColor) {
            [self.strokeColor setStroke];
            [self.path stroke];
        }
        
        _UIImage = UIGraphicsGetImageFromCurrentImageContext();
        if (!UIEdgeInsetsEqualToEdgeInsets(self.capInsets, UIEdgeInsetsZero)) {
            _UIImage = [_UIImage resizableImageWithCapInsets:self.capInsets];
        }
        UIGraphicsEndImageContext();
    }
    return _UIImage;
}

@end
