//
//  CZSwitch.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSwitch.h"
#import "UIColor+CZColorStyles.h"
#import "UISwitch+CZTheme.h"
#import "CALayer+CZTheme.h"
#import "UIView+CZTheme.h"

@interface CZSwitch ()

@property (nonatomic, assign) CZControlEmphasisLevel level;
@property (nonatomic, assign) CZSwitchStyle style;

@end

@implementation CZSwitch

- (instancetype)initWithStyle:(CZSwitchStyle)style {
    if (self = [super init]) {
        _level = CZControlEmphasisDefault;
        _style = style;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _level = CZControlEmphasisDefault;
        _style = CZSwitchStyleLarge;
        [self setup];
    }
    return self;
}

- (instancetype)init {
    return [self initWithStyle:CZSwitchStyleLarge];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _level = CZControlEmphasisDefault;
        _style = CZSwitchStyleLarge;
        [self setup];
    }
    return self;
}

#pragma mark - Public method

- (void)setLevel:(CZControlEmphasisLevel)level {
    _level = level;
    [self updateUIWithLevel:_level];
}

#pragma mark - Private method

- (void)setup {
    self.cz_backgroundColorPicker = CZThemeColorWithColors([UIColor cz_gs110], [UIColor cz_gs60]);
    self.cz_tintColorPicker = CZThemeColorWithColors([UIColor cz_gs120], [UIColor cz_gs70]);
    self.cz_thumbTintColorPicker = CZThemeColorWithColors([UIColor cz_gs10], [UIColor cz_gs10]);
    self.layer.borderWidth = kControlDefaultBorderWidth;
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    self.layer.masksToBounds = YES;
    [self updateUIWithLevel:_level];
}

- (void)updateUIWithLevel:(CZControlEmphasisLevel)level {
    switch (level) {
        case CZControlEmphasisDefault:
        case CZControlEmphasisActive:
        {
            self.cz_onTintColorPicker = CZThemeColorWithColors([UIColor cz_gs90], [UIColor cz_gs80]);
            self.cz_tintColorPicker = CZThemeColorWithColors([UIColor cz_gs120], [UIColor cz_gs90]);
        }
            break;
        case CZControlEmphasisActivePrimary : {
            self.cz_onTintColorPicker = CZThemeColorWithColors([UIColor cz_pb100], [UIColor cz_pb100]);
            self.cz_tintColorPicker = CZThemeColorWithColors([UIColor cz_gs120], [UIColor cz_pb120]);
        }
            break;
    }
}

@end
