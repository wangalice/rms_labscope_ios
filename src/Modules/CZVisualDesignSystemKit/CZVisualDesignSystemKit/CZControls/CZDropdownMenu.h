//
//  CZDropdownMenu.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeImage.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CZDropdownStyle) {
    kCZDropdownStyleMain,
    kCZDropdownStyleAlternative
};

typedef NS_ENUM(NSInteger, CZDropdownAnimationDirection) {
    CZDropdownAnimationDirectionDown,
    CZDropdownAnimationDirectionUp
};

enum {
    CZDropdownMenuNoSelectedItem = -1 // dropdown item index for no selected item
};


typedef void(^CZDropdownMenuSelectedAction)(NSUInteger index);

@interface CZDropdownAlignButton : UIButton

@end

@interface CZDropdownItem : CZDropdownAlignButton

+ (instancetype)itemWithTitle:(nullable NSString *)title imagePicker:(nullable CZThemeImagePicker)imagePicker;

+ (instancetype)itemWithTitle:(NSString *)title;

+ (instancetype)itemWithImagePicker:(CZThemeImagePicker)imagePicker;

- (void)setImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;

@end

@interface CZDropdownMenu : UIControl

@property (nonatomic) CZDropdownStyle style;

@property (nonatomic) CZDropdownAnimationDirection animationDirection;

@property (nonatomic) CGFloat itemHeight;

@property (nonatomic) NSInteger selectedIndex;

@property (nonatomic, copy, nullable) NSString *selectedItemTitle;

@property (nonatomic, copy, readonly) NSArray<CZDropdownItem *> *expandItems;

/** Set scrollEnable to YES to enabled the drop-down list scrolled while the conntent size smaller than the auto-sized bounds. Default is NO. */
@property (nonatomic) BOOL scrollEnable;

/** Set drop-down list content size, the width is not work. Default is CGSizeZero. */
@property (nonatomic) CGSize contentSize;

@property (nonatomic, getter=isExpanded) BOOL expanded;

/** If the value is YES, menu will be expanded after clicked. */
@property (nonatomic, assign) BOOL automaticallyTriggersPrimaryAction;

- (instancetype)initWithStyle:(CZDropdownStyle)style
                        title:(nullable NSString *)title
                  imagePicker:(nullable CZThemeImagePicker)imagePicker
                  expandItems:(NSArray<CZDropdownItem *> *)items;

- (instancetype)initWithStyle:(CZDropdownStyle)style
                        title:(nullable NSString *)title
                  imagePicker:(nullable CZThemeImagePicker)imagePicker
                  expandTitles:(NSArray<NSString *> *)items;


- (void)setTitle:(nullable NSString *)title
           index:(NSUInteger)index
        forState:(UIControlState)state;

- (void)setImagePicker:(nullable CZThemeImagePicker)picker
                 index:(NSUInteger)index
              forState:(UIControlState)state;

- (void)expandMenu;

- (void)collapseMenu;

- (void)invalidateMenu;

- (void)selectItemAction:(CZDropdownMenuSelectedAction)action;

- (void)insertItem:(CZDropdownItem *)item atIndex:(NSInteger)index;

- (void)addItem:(CZDropdownItem *)item;

- (void)removeItem:(CZDropdownItem *)item;

- (void)removeAllItems;

@end

NS_ASSUME_NONNULL_END
