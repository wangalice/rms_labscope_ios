//
//  CZShapeImage.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZShapeImage : NSObject

@property (nonatomic, readonly, strong) UIBezierPath *path;
@property (nonatomic, readonly, strong) UIColor *fillColor;
@property (nonatomic, readonly, strong) UIColor *strokeColor;
@property (nonatomic, readonly, assign) UIEdgeInsets capInsets;

+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor;
+ (instancetype)imageWithPath:(UIBezierPath *)path strokeColor:(UIColor *)strokeColor;
+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor;
+ (instancetype)imageWithPath:(UIBezierPath *)path fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor capInsets:(UIEdgeInsets)capInsets;

- (void)setName:(NSString *)name;
+ (instancetype)imageNamed:(NSString *)name;

@property (nonatomic, readonly, strong) UIImage *UIImage;

@end
