//
//  CZRadioButton.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZRadioButton : UIButton

@end

NS_ASSUME_NONNULL_END
