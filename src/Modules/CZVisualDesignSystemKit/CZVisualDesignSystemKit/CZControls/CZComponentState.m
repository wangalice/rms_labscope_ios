//
//  CZComponentState.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZComponentState.h"

@implementation CZComponentState

NSArray<NSNumber *> *UIControlStatesFromCZComponentState(CZControlState state) {
    NSMutableArray<NSNumber *> *states = [NSMutableArray array];
    
    if ((state & CZControlStateEnable) != 0) {
        [states addObject:@(UIControlStateNormal)];
    }
    
    if ((state & CZControlStateDisable) != 0) {
        [states addObject:@(UIControlStateDisabled)];
    }
    
    if ((state & CZControlStateFocused) != 0) {
        [states addObject:@(UIControlStateFocused)];
    }
    
    if ((state & CZControlStatePressed) != 0) {
        [states addObject:@(UIControlStateHighlighted)];
        [states addObject:@(UIControlStateSelected)];
    }
    
    return [states copy];
};

@end
