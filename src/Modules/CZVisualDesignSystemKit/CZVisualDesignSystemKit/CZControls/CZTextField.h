//
//  CZTextField.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeImage.h"
#import "CZVirturalDesignMacro.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CZTextFieldStyle) {
    CZTextFieldStyleDefault,
    CZTextFieldStyleReadOnly
};

@class CZIcon;
@class CZTextField;

@protocol CZTextFieldDelegate <UITextFieldDelegate>

@optional
- (BOOL)shouldShowErrorMessageWhileTextFieldEditing:(CZTextField *)textField;
- (BOOL)shouldShowErrorMessageWhileTextFieldEndEditing:(CZTextField *)textField;

@end

@interface CZTextField : UITextField

@property (nullable, nonatomic, weak) id<CZTextFieldDelegate> delegate;
@property (nonatomic, assign) BOOL showClearButtonWhileEditing;

// convenience instance method
- (instancetype)initWithStyle:(CZTextFieldStyle)style;

// if you want to show the unit, set rightViewMode to UITextFieldModelAlways. Otherwise, set rightViewMode to UITextFieldModelNever
- (instancetype)initWithStyle:(CZTextFieldStyle)style
                         unit:(nullable NSString *)unit
             rightImagePicker:(nullable CZThemeImagePicker)imagePicker;

- (instancetype)initWithStyle:(CZTextFieldStyle)style
                         unit:(nullable NSString *)unit
                    rightIcon:(nullable CZIcon *)icon;

// if you want show the right image, set rightViewMode to UITextFieldViewModeAlways
- (void)setRightImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;

- (void)setRightImagePicker:(CZThemeImagePicker)imagePicker forCZState:(CZControlState)state;

@end

// TextField + CZTextFieldSupportError
@interface CZTextField ()

/** Default is nil. */
@property (nonatomic, copy) NSString *errorMessage;
/** Default is nil. */
@property (nonatomic, strong) UIView *errorContentView;
/** Default is CGRectZero. */
@property (nonatomic, assign) CGRect errorContentRect;
/** Default is nil. */
@property (nonatomic, strong) UILabel *errorMessageLabel;
/** Default is nil. */
@property (nonatomic, strong) UIImageView *errorImageView;

@end

NS_ASSUME_NONNULL_END
