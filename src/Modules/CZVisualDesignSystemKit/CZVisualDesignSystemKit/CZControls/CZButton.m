//
//  CZButton.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZButton.h"
#import "CZComponentState.h"
#import "UIColor+CZColorStyles.h"
#import "UIFont+CZFontStyles.h"
#import "CZColorTable.h"
#import "UIButton+CZTheme.h"
#import "UIImage+CZTintColor.h"
#import "UIView+CZTheme.h"
#import "CALayer+CZTheme.h"
#import "CZSharedImageSource.h"

@implementation CZButton

+ (instancetype)buttonWithLevel:(CZControlEmphasisLevel)level {
    CZButton *button = [CZButton buttonWithType:UIButtonTypeCustom];
    button.level = level;
    return button;
}

- (instancetype)initWithLevel:(CZControlEmphasisLevel)level {
    if (self = [super init]) {
        _level = level;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (instancetype)init {
    return [self initWithLevel:CZControlEmphasisActive];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

#pragma mark - Private Methods
- (void)setup {
    //Text
    self.titleLabel.font = [UIFont cz_body1];
    self.adjustsImageWhenHighlighted = NO;
    [self cz_setTitleColorPicker:[CZColorTable defaultTextColor]
                        forState:UIControlStateNormal];
    [self cz_setTitleColorPicker:[CZColorTable defaultActiveTextColor]
                        forState:UIControlStateSelected];
    [self cz_setTitleColorPicker:[CZColorTable defaultDisabledTextColor]
                        forState:UIControlStateDisabled];

    //background
    [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-default-button-enabled"],
                                                                   [[CZSharedImageSource sharedImageSource] imageWithName:@"light-default-button-enabled"])
                             forState:UIControlStateNormal];
}

#pragma mark - Public Methods

- (void)setCzState:(CZControlState)czState {
    _czState = czState;
    //TODO: implement this methods if u need this to replace the UI display and interact;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.titleLabel.font = [UIFont cz_subtitle1];
        [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-active-button-enabled"],
                                                                       [[CZSharedImageSource sharedImageSource] imageWithName:@"light-active-button-enabled"])
                                 forState:UIControlStateSelected];
        if (_level == CZControlEmphasisActivePrimary) {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-activePrimary-button-enabled"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-activePrimary-button-enabled"])
                                     forState:UIControlStateSelected];
        }
    } else {
        self.titleLabel.font = [UIFont cz_body1];
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];

    if (highlighted && self.selected) {
        [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-active-button-pressed"],
                                                                       [[CZSharedImageSource sharedImageSource] imageWithName:@"light-active-button-pressed"])
                                 forState:UIControlStateNormal];
        if (_level == CZControlEmphasisActivePrimary) {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-activePrimary-button-pressed"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-activePrimary-button-pressed"])
                                     forState:UIControlStateNormal];
        }
    } else {
        if (highlighted) {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-default-button-pressed"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-default-button-pressed"])
                                     forState:UIControlStateNormal];
        } else {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-default-button-enabled"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-default-button-enabled"])
                                     forState:UIControlStateNormal];
        }
    }
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];

    if (!enabled && self.selected) {
        [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-active-button-disabled"],
                                                                       [[CZSharedImageSource sharedImageSource] imageWithName:@"light-active-button-disabled"])
                                 forState:UIControlStateNormal];
        if (_level == CZControlEmphasisActivePrimary) {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-activePrimary-button-disabled"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-activePrimary-button-disabled"])
                                     forState:UIControlStateNormal];
        }
    } else {
        if (enabled) {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-default-button-enabled"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-default-button-enabled"])
                                     forState:UIControlStateNormal];
        } else {
            [self cz_setBackgroundImagePicker:CZThemeImagePickerWithImages([[CZSharedImageSource sharedImageSource] imageWithName:@"dark-default-button-disabled"],
                                                                           [[CZSharedImageSource sharedImageSource] imageWithName:@"light-default-button-disabled"])
                                     forState:UIControlStateNormal];
        }
    }
}

@end
