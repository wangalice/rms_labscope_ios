//
//  CZSwitch.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

NS_ASSUME_NONNULL_BEGIN

 typedef NS_ENUM(NSInteger, CZSwitchStyle) {
     CZSwitchStyleSmall, // No support template
     CZSwitchStyleLarge // Default is large
 };

@interface CZSwitch : UISwitch

// default is CZControlEmphasisDefault
- (instancetype)initWithStyle:(CZSwitchStyle)style;

// default is CZControlEmphasisDefault
- (void)setLevel:(CZControlEmphasisLevel)level;

@end

NS_ASSUME_NONNULL_END
