//
//  CZTextField.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTextField.h"
#import "CZThemeImage.h"
#import "UIButton+CZTheme.h"
#import "CZImageAssert.h"
#import "UIImageView+CZTheme.h"
#import "UITextField+CZTheme.h"
#import "CZColorTable.h"
#import "UIFont+CZFontStyles.h"
#import "UIColor+CZColorStyles.h"
#import "UIView+CZTheme.h"
#import "CAShapeLayer+CZTheme.h"
#import "CZIcon+Theme.h"
#import "UIButton+CZIcon.h"

@interface CZErrorBackgroundView : UIView

@end

@interface CZTextField ()

@property (nonatomic, assign) CZTextFieldStyle style;

@property (nonatomic, strong) UIImageView *bottomLine;

@property (nonatomic, strong) UIButton *rightButton;

@property (nonatomic, strong) NSString *unit;

@property (nonatomic, copy) CZThemeImagePicker imagePicker;

@property (nonatomic, strong) CZErrorBackgroundView *errorContentBackground;

@end

@implementation CZTextField
@dynamic delegate;

- (instancetype)initWithStyle:(CZTextFieldStyle)style {
    return [self initWithStyle:style unit:nil rightImagePicker:NULL];
}

- (instancetype)initWithStyle:(CZTextFieldStyle)style
                         unit:(nullable NSString *)unit
             rightImagePicker:(nullable CZThemeImagePicker)imagePicker {
    if (self = [super init]) {
        _style = style;
        _unit = unit;
        _imagePicker = [imagePicker copy];
        [self setup];
    }
    return self;
}

- (instancetype)initWithStyle:(CZTextFieldStyle)style
                         unit:(NSString *)unit
                    rightIcon:(CZIcon *)icon {
    return [self initWithStyle:style unit:unit rightImagePicker:^UIImage *(CZTheme theme) {
        return [icon imageForControlWithTheme:theme state:UIControlStateNormal];
    }];
}

- (instancetype)init {
    return [self initWithStyle:CZTextFieldStyleDefault unit:nil rightImagePicker:NULL];
}

#pragma mark - Public Methods

- (void)setRightImagePicker:(CZThemeImagePicker)imagePicker forCZState:(CZControlState)state {
    UIControlState controlState = UIControlStateNormal;
    switch (state) {
        case CZControlStateEnable: {
            controlState = UIControlStateNormal;
        }
            break;
            
        case CZControlStateFocused: {
            controlState = UIControlStateFocused;
        }
            break;
        case CZControlStateDisable: {
            controlState = UIControlStateDisabled;
        }
            break;
        case CZControlStatePressed: {
            controlState = UIControlStateSelected;
        }
            break;
        case CZControlStateHover:
        default:
            break;
    }
    [self setRightImagePicker:imagePicker forState:controlState];
}

- (void)setRightImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self.rightButton cz_setImagePicker:imagePicker forState:state];
}

#pragma mark - Private Methods
- (void)setup {
    
    if (self.unit) {
        [self.rightButton setTitle:self.unit forState:UIControlStateNormal];
        CGSize size = [self.unit sizeWithAttributes:@{NSFontAttributeName : [UIFont cz_body1]}];
        self.rightButton.frame = CGRectMake(0, 0, ceilf(size.width), 32);
        self.rightButton.userInteractionEnabled = NO;
    } else if (self.imagePicker) {
        [self.rightButton cz_setImagePicker:self.imagePicker forState:UIControlStateNormal];
        self.rightButton.frame = CGRectMake(0, 0, 32, 21);
        self.rightButton.userInteractionEnabled = NO;
    }
    
    switch (self.style) {
        case CZTextFieldStyleDefault: {
            self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-active-enable-line"),
                                                                          Assert(@"light-input-active-enable-line"));
        }
            break;
        case CZTextFieldStyleReadOnly: {
            self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-readonly-enable-line"),
                                                                          Assert(@"light-input-readonly-enable-line"));
        }
            break;
        default:
            break;
    }

    // subviews
    [self addSubview:self.bottomLine];
    [self addSubview:self.errorContentBackground];
    self.rightView = self.rightButton;
    self.errorContentBackground.translatesAutoresizingMaskIntoConstraints = NO;
    [self.errorContentBackground.bottomAnchor constraintEqualToAnchor:self.topAnchor constant:-4.0].active = YES;
    [self.errorContentBackground.leftAnchor constraintEqualToAnchor:self.leftAnchor].active = YES;
    [self.errorContentBackground.heightAnchor constraintEqualToConstant:26.0].active = YES;
    
    self.font = [UIFont cz_body1];
    self.cz_textColorPicker = [CZColorTable defaultTextColor];
    self.cz_tintColorPicker = [CZColorTable defaultTextColor];
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.spellCheckingType = UITextSpellCheckingTypeNo;
    self.rightViewMode = UITextFieldViewModeAlways;
    
    // add textField editing
    [self addTarget:self action:@selector(textFieldDidBeginEvent:) forControlEvents:UIControlEventEditingDidBegin];
    [self addTarget:self action:@selector(textFieldDidEndEditingEvent:) forControlEvents:UIControlEventEditingDidEnd];
    [self addTarget:self action:@selector(textFieldDidChangeEditingEvent:) forControlEvents:UIControlEventEditingChanged];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    self.bottomLine.frame = CGRectMake(0, CGRectGetHeight(self.frame) - 1, CGRectGetWidth(self.frame), 1.0f);
}

- (void)setPlaceholder:(NSString *)placeholder {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSFontAttributeName:[UIFont cz_body1],
                                                                                                     NSForegroundColorAttributeName:[UIColor cz_gs80]
                                                                                                     }];
}

- (void)showError {
    self.errorContentBackground.hidden = NO;
}

- (void)hideError {
    self.errorContentBackground.hidden = YES;
}

#pragma mark - Action
- (void)rightButtonSelectEvent:(UIButton *)sender {
    self.text = nil;

    [self sendActionsForControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldDidBeginEvent:(UITextField *)sender {
    switch (self.style) {
        case CZTextFieldStyleDefault: {
            self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-active-editing-line"),
                                                                          Assert(@"light-input-active-editing-line"));
        }
            break;
        case CZTextFieldStyleReadOnly: {
            self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-readonly-editing-line"),
                                                                          Assert(@"light-input-readonly-editing-line"));
        }
            break;
        default:
            break;
    }
    
    if (_showClearButtonWhileEditing) {
        if (sender.text.length != 0) {
            [self.rightButton cz_setImageWithIcon:[CZIcon iconNamed:@"close"]];
        }
        self.rightButton.userInteractionEnabled = YES;
    }
}

- (void)textFieldDidEndEditingEvent:(UITextField *)sender {
    BOOL shouldShowErrorMessage = NO;
    if ([self.delegate respondsToSelector:@selector(shouldShowErrorMessageWhileTextFieldEndEditing:)]) {
        shouldShowErrorMessage = [self.delegate shouldShowErrorMessageWhileTextFieldEndEditing:self];
    }
    if (shouldShowErrorMessage) {
        [self showError];
        self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-active-enable-line"), Assert(@"dark-input-active-enable-line"));
    } else {
        [self hideError];
        switch (self.style) {
            case CZTextFieldStyleDefault: {
                self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-active-enable-line"),
                                                                              Assert(@"light-input-active-enable-line"));
            }
                break;
            case CZTextFieldStyleReadOnly: {
                self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-readonly-enable-line"),
                                                                              Assert(@"light-input-readonly-enable-line"));
            }
                break;
            default:
                break;
        }
    }
    
    if (_showClearButtonWhileEditing) {
        if (self.imagePicker) {
            [self.rightButton cz_setImagePicker:self.imagePicker forState:UIControlStateNormal];
        }
        self.rightButton.hidden = NO;
        self.rightButton.userInteractionEnabled = NO;
    }
}

- (void)textFieldDidChangeEditingEvent:(UITextField *)sender {
    BOOL shouldShowErrorMessage = NO;
    if ([self.delegate respondsToSelector:@selector(shouldShowErrorMessageWhileTextFieldEditing:)]) {
        shouldShowErrorMessage = [self.delegate shouldShowErrorMessageWhileTextFieldEditing:self];
    }
    if (shouldShowErrorMessage) {
        [self showError];
        self.bottomLine.image = Assert(@"input-error-line");
    } else {
        [self hideError];
        switch (self.style) {
            case CZTextFieldStyleDefault: {
                self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-active-editing-line"),
                                                                              Assert(@"light-input-active-editing-line"));
            }
                break;
            case CZTextFieldStyleReadOnly: {
                self.bottomLine.cz_imagePicker = CZThemeImagePickerWithImages(Assert(@"dark-input-readonly-editing-line"),
                                                                              Assert(@"light-input-readonly-editing-line"));
            }
                break;
            default:
                break;
        }
    }

    if (_showClearButtonWhileEditing) {
        if (self.text.length == 0) {
            if (self.imagePicker) {
                [self.rightButton cz_setImagePicker:self.imagePicker forState:UIControlStateNormal];
            } else {
                self.rightButton.hidden = YES;
            }
        } else {
            self.rightButton.hidden = NO;
            [self.rightButton cz_setImageWithIcon:[CZIcon iconNamed:@"close"]];
        }
    }
}

#pragma mark - Setters
- (void)setErrorContentView:(UIView *)errorContentView {
    if (errorContentView == nil) return;
    
    _errorContentView = errorContentView;
    [self.errorContentBackground addSubview:self.errorContentView];
    self.errorImageView.hidden = YES;
    self.errorMessageLabel.hidden = YES;
}

- (void)setErrorContentRect:(CGRect)errorContentRect {
    if (CGRectEqualToRect(errorContentRect, CGRectZero)) return;

    _errorContentRect = errorContentRect;
    self.errorContentBackground.frame = errorContentRect;
    self.errorImageView.hidden = YES;
    self.errorMessageLabel.hidden = YES;
    [self.errorContentBackground layoutIfNeeded];
}

- (void)setErrorMessage:(NSString *)errorMessage {
    if (errorMessage == nil) return;
    
    _errorMessage = [errorMessage copy];
    
    self.errorMessageLabel.text = _errorMessage;
    self.errorMessageLabel.hidden = NO;
    self.errorImageView.hidden = NO;
    [self.errorContentBackground layoutIfNeeded];
}

- (void)setShowClearButtonWhileEditing:(BOOL)showClearButtonWhileEditing {
    _showClearButtonWhileEditing = showClearButtonWhileEditing;

    if (_showClearButtonWhileEditing) {
        self.rightButton.frame = CGRectMake(0, 0, 32, 32);
    }
}

#pragma mark - Getters

- (UIImageView *)bottomLine {
    if (_bottomLine == nil) {
        _bottomLine = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    return _bottomLine;
}

- (UIButton *)rightButton {
    if (_rightButton == nil) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.backgroundColor = [UIColor clearColor];
        _rightButton.titleLabel.font = [UIFont cz_body1];
        _rightButton.contentMode = UIViewContentModeScaleAspectFit;
        _rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_rightButton cz_setTitleColorPicker:[CZColorTable defaultTextColor] forState:UIControlStateNormal];
        [_rightButton addTarget:self
                         action:@selector(rightButtonSelectEvent:)
               forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

- (CZErrorBackgroundView *)errorContentBackground {
    if (_errorContentBackground == nil) {
        _errorContentBackground = [[CZErrorBackgroundView alloc] init];
        _errorContentBackground.hidden = YES;
        [_errorContentBackground addSubview:self.errorImageView];
        [_errorContentBackground addSubview:self.errorMessageLabel];

        self.errorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.errorMessageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.errorImageView.leftAnchor constraintEqualToAnchor:_errorContentBackground.leftAnchor constant:8.0].active = YES;
        [self.errorImageView.centerYAnchor constraintEqualToAnchor:_errorContentBackground.centerYAnchor].active = YES;
        [self.errorImageView.widthAnchor constraintEqualToConstant:16.0].active = YES;
        [self.errorImageView.heightAnchor constraintEqualToConstant:16.0].active = YES;
        
        [self.errorMessageLabel.leftAnchor constraintEqualToAnchor:self.errorImageView.rightAnchor constant:8.0].active = YES;
        [self.errorMessageLabel.rightAnchor constraintEqualToAnchor:_errorContentBackground.rightAnchor constant:-8.0].active = YES;
        [self.errorMessageLabel.heightAnchor constraintEqualToConstant:17.0].active = YES;
        [self.errorMessageLabel.centerYAnchor constraintEqualToAnchor:_errorContentBackground.centerYAnchor].active = YES;
    }
    return _errorContentBackground;
}

- (UILabel *)errorMessageLabel {
    if (_errorMessageLabel == nil) {
        _errorMessageLabel = [[UILabel alloc] init];
        _errorMessageLabel.cz_tintColorPicker = CZThemeColorWithColors([UIColor cz_gs100], [UIColor cz_gs50]);
        _errorMessageLabel.font = [UIFont cz_caption];
        _errorMessageLabel.numberOfLines = 1;
        _errorMessageLabel.hidden = YES;
    }
    return _errorMessageLabel;
}

- (UIImageView *)errorImageView {
    if (_errorImageView == nil) {
        _errorImageView = [[UIImageView alloc] init];
        _errorImageView.image = Assert(@"inputbox-error-icon");
        _errorImageView.hidden = YES;
    }
    return _errorImageView;
}

@end

@implementation CZErrorBackgroundView

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    UIBezierPath *path = [self borderPath];
    CAShapeLayer *shapeLayer = (CAShapeLayer *)self.layer;
    shapeLayer.path = path.CGPath;
    shapeLayer.cz_fillColorPicker = CZThemeColorWithColors([UIColor cz_gs30], [UIColor cz_gs100]);
    shapeLayer.cz_strokeColorPicker = CZThemeColorWithColors([UIColor cz_gs40], [UIColor cz_gs110]);
}


///     E                   D
///      -------------------
///     |                   |
///     | G  B              |
///      --  ---------------
///    F   \/                C
///        A

- (UIBezierPath *)borderPath {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat arrowWidth = 16.0;
    CGFloat arrowHeight = arrowWidth / 2;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGPoint point = CGPointMake(0, 0);
    
    // Move to A
    point.x = 16.0;
    point.y = height + arrowHeight;
    [path moveToPoint:point];
    
    // Add line from A to B
    point.x += arrowWidth / 2;
    point.y -= arrowHeight;
    [path addLineToPoint:point];
    
    // Add line from B to C
    point.x = width;
    [path addLineToPoint:point];
    
    // Add arc from C to D
    point.y -= height;
    [path addLineToPoint:point];
    
    // Add line from D to E
    point.x -= width;
    [path addLineToPoint:point];
    
    // Add line from E to F
    point.y += height;
    [path addLineToPoint:point];
    
    // Add line from F to G
    point.x = 8.0;
    [path addLineToPoint:point];
    
    // Add arc from G to A
    [path closePath];
    return path;
}

@end
