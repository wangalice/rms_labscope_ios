//
//  UIColor+CZColorStyles.m
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIColor+CZColorStyles.h"

@implementation UIColor (CZColorStyles)

+ (UIColor *)cz_gs10 {
    return [self cz_colorWithHexInt:0xFFFFFF];
}

+ (UIColor *)cz_gs20 {
    return [self cz_colorWithHexInt:0xF5F7FA];
}

+ (UIColor *)cz_gs30 {
    return [self cz_colorWithHexInt:0xEDF2F7];
}

+ (UIColor *)cz_gs35 {
    return [self cz_colorWithHexInt:0xE4EBF2];
}

+ (UIColor *)cz_gs40 {
    return [self cz_colorWithHexInt:0xDDE3ED];
}

+ (UIColor *)cz_gs50 {
    return [self cz_colorWithHexInt:0xD1D9E5];
}

+ (UIColor *)cz_gs60 {
    return [self cz_colorWithHexInt:0xC1CBD9];
}

+ (UIColor *)cz_gs70 {
    return [self cz_colorWithHexInt:0xA5B1C2];
}

+ (UIColor *)cz_gs80 {
    return [self cz_colorWithHexInt:0x828D9E];
}

+ (UIColor *)cz_gs85 {
    return [self cz_colorWithHexInt:0x626C7A];
}

+ (UIColor *)cz_gs90 {
    return [self cz_colorWithHexInt:0x555E68];
}

+ (UIColor *)cz_gs95 {
    return [self cz_colorWithHexInt:0x404952];
}

+ (UIColor *)cz_gs100 {
    return [self cz_colorWithHexInt:0x353D45];
}

+ (UIColor *)cz_gs105 {
    return [self cz_colorWithHexInt:0x262E36];
}

+ (UIColor *)cz_gs110 {
    return [self cz_colorWithHexInt:0x1A2329];
}

+ (UIColor *)cz_gs115 {
    return [self cz_colorWithHexInt:0x171B24];
}

+ (UIColor *)cz_gs120 {
    return [self cz_colorWithHexInt:0x07090D];
}

+ (UIColor *)cz_pb60 {
    return [self cz_colorWithHexInt:0xA6C9FF];
}

+ (UIColor *)cz_pb70 {
    return [self cz_colorWithHexInt:0x85B6FF];
}

+ (UIColor *)cz_pb80 {
    return [self cz_colorWithHexInt:0x5C9DFF];
}

+ (UIColor *)cz_pb90 {
    return [self cz_colorWithHexInt:0x2E7AF5];
}

+ (UIColor *)cz_pb100 {
    return [self cz_colorWithHexInt:0x0050F2];
}

+ (UIColor *)cz_pb110 {
    return [self cz_colorWithHexInt:0x003ECD];
}

+ (UIColor *)cz_pb120 {
    return [self cz_colorWithHexInt:0x0029AF];
}

+ (UIColor *)cz_sr90 {
    return [self cz_colorWithHexInt:0xFF3564];
}

+ (UIColor *)cz_sr100 {
    return [self cz_colorWithHexInt:0xED2654];
}

+ (UIColor *)cz_sr110 {
    return [self cz_colorWithHexInt:0xDB1442];
}

+ (UIColor *)cz_sp90 {
    return [self cz_colorWithHexInt:0xFF52C0];
}

+ (UIColor *)cz_sp100 {
    return [self cz_colorWithHexInt:0xEE2FA8];
}

+ (UIColor *)cz_sp110 {
    return [self cz_colorWithHexInt:0xE7189B];
}

+ (UIColor *)cz_sb90 {
    return [self cz_colorWithHexInt:0x39CFFF];
}

+ (UIColor *)cz_sb100 {
    return [self cz_colorWithHexInt:0x13C6FF];
}

+ (UIColor *)cz_sb110 {
    return [self cz_colorWithHexInt:0x08B8EF];
}

+ (UIColor *)cz_sg90 {
    return [self cz_colorWithHexInt:0x5FE8DA];
}

+ (UIColor *)cz_sg100 {
    return [self cz_colorWithHexInt:0x3ED2C3];
}

+ (UIColor *)cz_sg110 {
    return [self cz_colorWithHexInt:0x27C6B6];
}

+ (UIColor *)cz_sy90 {
    return [self cz_colorWithHexInt:0xFFE669];
}

+ (UIColor *)cz_sy100 {
    return [self cz_colorWithHexInt:0xFFE148];
}

+ (UIColor *)cz_sy110 {
    return [self cz_colorWithHexInt:0xFCD510];
}

+ (UIColor *)cz_so90 {
    return [self cz_colorWithHexInt:0xFFAC4D];
}

+ (UIColor *)cz_so100 {
    return [self cz_colorWithHexInt:0xFFA33A];
}

+ (UIColor *)cz_so110 {
    return [self cz_colorWithHexInt:0xFF951C];
}

- (NSInteger)cz_hexIntValue {
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    
    NSInteger red = r * 255;
    NSInteger green = g * 255;
    NSInteger blue = b * 255;
    NSInteger alpha = a * 255;
    
    return (alpha << 24) + (red << 16) + (green << 8) + blue;
}

+ (instancetype)cz_colorWithHexInt:(NSInteger)hexInt {
    UInt8 red = ((hexInt & 0xFF0000) >> 16);
    UInt8 green = ((hexInt & 0xFF00) >> 8);
    UInt8 blue = (hexInt & 0xFF);
    return [self colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:1.0];
}

+ (instancetype)cz_colorWithRed:(UInt8)red green:(UInt8)green blue:(UInt8)blue {
    return [self colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:1.0];
}

@end
