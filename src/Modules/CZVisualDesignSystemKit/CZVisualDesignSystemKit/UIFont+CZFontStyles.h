//
//  UIFont+CZFontStyles.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

// TODO: Add new style font;

@interface UIFont (CZFontStyles)

@property (class, nonatomic, readonly, strong) UIFont *cz_h0;                               ///< SourceSansPro-Light 52 (SegoeUI-Light 48)
@property (class, nonatomic, readonly, strong) UIFont *cz_h1;                               ///< SourceSansPro-Light 35 (SegoeUI-Light 32)
@property (class, nonatomic, readonly, strong) UIFont *cz_h2;                               ///< SourceSansPro-Light 26 (SegoeUI-Light 24)
@property (class, nonatomic, readonly, strong) UIFont *cz_h3;                               ///< SourceSansPro-Light 21 (SegoeUI-Light 20)
@property (class, nonatomic, readonly, strong) UIFont *cz_h4;                               ///< SourceSansPro-Regular 18 (SegoeUI-Regular 16)

@property (class, nonatomic, readonly, strong) UIFont *cz_subtitle1;                        ///< SourceSansPro-Regular 15 (SegoeUI-Semibold 14)
@property (class, nonatomic, readonly, strong) UIFont *cz_subtitle2;                        ///< SourceSansPro-Bold 13 (SegoeUI-Bold 12)

@property (class, nonatomic, readonly, strong) UIFont *cz_body1;                            ///< SourceSansPro-Regular 15 (SegoeUI-Regular 14)

@property (class, nonatomic, readonly, strong) UIFont *cz_caption;                          ///< SourceSansPro-Regular 13 (SegoeUI-Regular 12)

//New added label1: placed before or upon controls or Controlgroups are using "Label1"
@property (class, nonatomic, readonly, strong) UIFont *cz_label1 NS_AVAILABLE_IOS(9.0);     ///< SourceSansPro-Semibold 12 (SegoeUI-Semibold 12)

//New added label2: is still used for Tab menueitems, SectionDeviders, SubAccordions and Tableheaders
@property (class, nonatomic, readonly, strong) UIFont *cz_label2;                           ///< SourceSansPro-Bold 11 (SegoeUI-Bold 10)

@end
