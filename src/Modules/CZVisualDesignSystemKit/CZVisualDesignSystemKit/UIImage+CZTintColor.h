//
//  UIImage+CZTintColor.h
//  CZVisualDesignSystemKit
//
//  Created by Li, Junlin on 1/21/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CZTintColor)

- (UIImage *)cz_imageWithTintColor:(UIColor *)tintColor;

@end
