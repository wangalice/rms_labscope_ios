//
//  CZColorTable.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/3.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"

// Color: Design as style guide in the figma

//Reference UI Design: https://www.figma.com/file/CtDBCRfwa8id6rOzSCR13T19/0.0-Design-System-StyleGuide?node-id=62%3A44

NS_ASSUME_NONNULL_BEGIN

// This abstract class target: use the general theme with color
@interface CZColorTable : NSObject

+ (instancetype)sharedColorTable;

+ (CZThemeColorPicker)defaultBackgroundColor;

+ (CZThemeColorPicker)defaultActiveBackgroundColor;

+ (CZThemeColorPicker)defaultActivePrimaryBlueColor;

+ (CZThemeColorPicker)defaultSelectedBackgroundColor;

+ (CZThemeColorPicker)defaultDisbaledBackgroundColor;

+ (CZThemeColorPicker)defaultHighlightTextColor;

+ (CZThemeColorPicker)defaultTextColor;

+ (CZThemeColorPicker)defaultDisabledTextColor;

+ (CZThemeColorPicker)defaultActiveTextColor;

+ (CZThemeColorPicker)defaultActiveDisabledTextColor;

+ (CZThemeColorPicker)defaultNormalBorderColor;

@end

NS_ASSUME_NONNULL_END
