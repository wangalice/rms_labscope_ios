//
//  CZThemeImage.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

NS_ASSUME_NONNULL_BEGIN

typedef UIImage *(^CZThemeImagePicker)(CZTheme theme);

CZThemeImagePicker CZThemeImagePickerWithImages(UIImage * primaryImage,...);

@interface CZThemeImage : NSObject

+ (CZThemeImagePicker)imageWithUIImages:(NSArray <UIImage *> *)images;

+ (CZThemeImagePicker)imageWithUIImageNames:(NSArray <NSString *> *)imageNames;

@end

NS_ASSUME_NONNULL_END
