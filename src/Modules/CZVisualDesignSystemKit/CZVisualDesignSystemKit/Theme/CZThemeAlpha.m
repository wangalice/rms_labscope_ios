//
//  CZThemeAlpha.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZThemeAlpha.h"
#import "CZThemeManager.h"

@implementation CZThemeAlpha

CZThemeAlphaPicker CZThemeAlphaPickerWithAlphas(CGFloat primaryAlpha,...) {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSMutableArray <NSNumber *> *alphasArray = [NSMutableArray array];
    
    [alphasArray addObject:[NSNumber numberWithFloat:primaryAlpha]];
    
    NSUInteger num_args = themes.count - 1;
    va_list alphas;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvarargs"
    va_start(alphas, num_args);
#pragma clang diagnostic pop
    for (NSUInteger i = 0; i < num_args; i++) {
        CGFloat secondaryAlpha = va_arg(alphas, CGFloat);
        [alphasArray addObject:[NSNumber numberWithFloat:secondaryAlpha]];
    }
    va_end(alphas);
    
    return ^(CZTheme theme) {
        NSInteger index = [themes indexOfObject:@(theme)];
        return [alphasArray[index] floatValue];
    };
}

+ (CZThemeAlphaPicker)alphaWithThemeAlpha:(CGFloat)primaryAlpha {
    return CZThemeAlphaPickerWithAlphas(primaryAlpha);
}

+ (CZThemeAlphaPicker)alphaWithThemeAlphas:(NSArray <NSNumber *>*)alphas {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSParameterAssert(alphas.count == themes.count);
    return ^(CZTheme theme) {
        NSInteger index = [themes indexOfObject:@(theme)];
        if (index < alphas.count) {
            return [alphas[index] floatValue];
        } else {
            NSLog(@"Array crossover expection: %@", alphas);
            return [alphas[0] floatValue];
        }
    };
}

@end
