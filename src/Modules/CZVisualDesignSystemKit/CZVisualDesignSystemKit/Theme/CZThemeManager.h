//
//  CZThemeManager.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shagoe on 2019/1/31.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZVirturalDesignMacro.h"

extern NSString * const kCZThemeChangeEventNotification;
extern NSString * const kCZApplicationShouldChangeStatusBarStyle;

@interface CZThemeManager : NSObject

// Get current theme;
@property (nonatomic, readonly) CZTheme currentTheme;

@property (nonatomic, copy, readonly) NSArray <NSNumber *> *themes;

// If you set this value to YES, u must register the kCZApplicationShouldChangeStatusBarStyle notification in the AppDelegte.m file to change statusbar style;
@property (nonatomic, assign) BOOL shouldUpdateStatusBarStyle;

+ (instancetype)sharedManager;

// Switch the theme with this method, u should register the kCZThemeChangeEventNotification notification when you want to change the theme in the controls;
- (void)switchThemeTo:(CZTheme)theme;

@end
