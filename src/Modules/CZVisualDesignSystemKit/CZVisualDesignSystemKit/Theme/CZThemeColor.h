//
//  CZThemeColor.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

//NS_ASSUME_NONNULL_BEGIN

typedef UIColor *(^CZThemeColorPicker)(CZTheme theme);

CZThemeColorPicker CZThemeColorWithColors(UIColor *primaryColor,...);
CZThemeColorPicker CZThemeColorWithRGBs(NSInteger primaryRGB,...);

@interface CZThemeColor : NSObject

+ (CZThemeColorPicker)themeColorWithColors:(NSArray <UIColor *> *)colors;

+ (CZThemeColorPicker)themeColorWithColor:(UIColor *)color;

@end

//NS_ASSUME_NONNULL_END
