//
//  CZThemeColor.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZThemeColor.h"
#import "CZThemeManager.h"

@implementation CZThemeColor

CZThemeColorPicker CZThemeColorWithColors(UIColor *primaryColor,...) {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    
    NSMutableArray <UIColor *>*colors = [NSMutableArray array];
    [colors addObject:primaryColor];
    
    NSUInteger num_args = themes.count - 1;
    va_list rgbas;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvarargs"
    va_start(rgbas, num_args);
#pragma clang diagnostic pop
    for (NSUInteger i = 0; i < num_args; i++) {
        UIColor *secondaryColor = va_arg(rgbas, UIColor *);
        [colors addObject:secondaryColor];
    }
    va_end(rgbas);
    
    return ^ (CZTheme theme) {
        NSUInteger index = [themes indexOfObject:[NSNumber numberWithInteger:theme]];
        return colors[index];
    };
}

CZThemeColorPicker CZThemeColorWithRGBs(NSInteger primaryRGB,...) {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSMutableArray <UIColor *>*colors = [NSMutableArray array];
    
    UIColor *primaryColor = [UIColor colorWithRed:((float)((primaryRGB & 0xFF0000) >> 16))/255.0 green:((float)((primaryRGB & 0xFF00) >> 8))/255.0 blue:((float)(primaryRGB & 0xFF))/255.0 alpha:1.0];
    [colors addObject:primaryColor];
    
    NSUInteger num_args = themes.count - 1;
    va_list rgbas;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvarargs"
    va_start(rgbas, num_args);
#pragma clang diagnostic pop
    for (NSUInteger i = 0; i < num_args; i++) {
        NSInteger secondaryRGBA = va_arg(rgbas, NSInteger);
        UIColor *secondaryColor = [UIColor colorWithRed:((float)((secondaryRGBA & 0xFF0000) >> 16))/255.0 green:((float)((secondaryRGBA & 0xFF00) >> 8))/255.0 blue:((float)(secondaryRGBA & 0xFF))/255.0 alpha:1.0];
        [colors addObject:secondaryColor];
    }
    va_end(rgbas);
    
    return ^ (CZTheme theme) {
        NSUInteger index = [themes indexOfObject:[NSNumber numberWithInteger:theme]];
        return colors[index];
    };
}

+ (CZThemeColorPicker)themeColorWithColors:(NSArray <UIColor *> *)colors {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSParameterAssert(colors.count == themes.count);
    return ^ (CZTheme theme) {
        NSUInteger index = [themes indexOfObject:[NSNumber numberWithInteger:theme]];
        if (index < colors.count) {
            return colors[index];
        } else {
            return colors[0];// As placeholder color to handle this case
        }
    };
}

+ (CZThemeColorPicker)themeColorWithColor:(UIColor *)color {
    return CZThemeColorWithColors(color);
}

@end
