//
//  NSObject+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"
#import "CZThemeColor.h"
#import <objc/runtime.h>
#import "CZVirturalDesignMacro.h"

@interface NSObject ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, CZThemeColorPicker> *pickers;

@end

@implementation NSObject (CZTheme)

- (NSDictionary<NSString *,CZThemeColorPicker> *)pickers {
    NSDictionary <NSString *, CZThemeColorPicker> *pickers = objc_getAssociatedObject(self, @selector(pickers));
    if (pickers == nil) {
        @autoreleasepool {
            //TODO: Need to remove observer when delloc
        }
        pickers = [NSMutableDictionary dictionary];
        objc_setAssociatedObject(self, @selector(pickers), pickers, OBJC_ASSOCIATION_RETAIN);
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kCZThemeChangeEventNotification
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(switchTheme)
                                                     name:kCZThemeChangeEventNotification
                                                   object:nil];
    }
    return pickers;
}

- (CZThemeManager *)themeManager {
    return [CZThemeManager sharedManager];
}

- (void)switchTheme {
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, CZThemeColorPicker  _Nonnull picker, BOOL * _Nonnull stop) {
        SEL sel = NSSelectorFromString(selector);
        id result = picker(self.themeManager.currentTheme);
        
        [UIView animateWithDuration:CZThemeAnimationDuration
                         animations:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                             [self performSelector:sel withObject:result];
#pragma clang diagnostic pop
                         }];

    }];
}

@end
