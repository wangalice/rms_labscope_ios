//
//  UIView+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"
#import "CZThemeAlpha.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIView (CZTheme)

@property (nonatomic, copy) CZThemeColorPicker cz_backgroundColorPicker;
@property (nonatomic, copy) CZThemeColorPicker cz_tintColorPicker;
@property (nonatomic, copy) CZThemeAlphaPicker cz_alphaPicker;

@end

NS_ASSUME_NONNULL_END
