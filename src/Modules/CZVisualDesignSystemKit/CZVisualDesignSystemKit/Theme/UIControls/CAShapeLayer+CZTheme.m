//
//  CAShapeLayer+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/4/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CAShapeLayer+CZTheme.h"
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"
#import <objc/runtime.h>

@interface CAShapeLayer ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, CZThemeColorPicker>*pickers;

@end

@implementation CAShapeLayer (CZTheme)

- (CZThemeColorPicker)cz_fillColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_fillColorPicker));
}

- (CZThemeColorPicker)cz_strokeColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_strokeColorPicker));
}

- (void)setCz_fillColorPicker:(CZThemeColorPicker)cz_fillColorPicker {
    objc_setAssociatedObject(self, @selector(cz_fillColorPicker), cz_fillColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.fillColor = cz_fillColorPicker(self.themeManager.currentTheme).CGColor;
    [self.pickers setObject:[cz_fillColorPicker copy] forKey:NSStringFromSelector(@selector(setFillColor:))];
}

- (void)setCz_strokeColorPicker:(CZThemeColorPicker)cz_strokeColorPicker {
    objc_setAssociatedObject(self, @selector(cz_strokeColorPicker), cz_strokeColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.strokeColor = cz_strokeColorPicker(self.themeManager.currentTheme).CGColor;
    [self.pickers setValue:[cz_strokeColorPicker copy] forKey:NSStringFromSelector(@selector(setStrokeColor:))];
}

- (void)switchTheme {
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, CZThemeColorPicker  _Nonnull themeColorPicker, BOOL * _Nonnull stop) {
        CGColorRef result = themeColorPicker(self.themeManager.currentTheme).CGColor;
        [UIView animateWithDuration:kControlDefaultBorderRadius
                         animations:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                             if ([selector isEqualToString:NSStringFromSelector(@selector(setFillColor:))]) {
                                 [self setFillColor:result];
                             } else if ([selector isEqualToString:NSStringFromSelector(@selector(setStrokeColor:))]) {
                                 [self setStrokeColor:result];
                             }
#pragma clang diagnostic pop
                         }];
    }];
}

@end
