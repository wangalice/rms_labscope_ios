//
//  UIButton+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"
#import "CZThemeImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (CZTheme)

- (void)cz_setTitleColorPicker:(CZThemeColorPicker)colorPicker forState:(UIControlState)state;

- (void)cz_setImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;

- (void)cz_setImageNamePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;

- (void)cz_setBackgroundImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;

@end

NS_ASSUME_NONNULL_END
