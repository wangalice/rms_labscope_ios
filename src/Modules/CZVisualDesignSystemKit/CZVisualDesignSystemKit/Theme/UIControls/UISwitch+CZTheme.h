//
//  UISwitch+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface UISwitch (CZTheme)

@property (nonatomic, strong) CZThemeColorPicker cz_tintColorPicker;

@property (nonatomic, strong) CZThemeColorPicker cz_onTintColorPicker;

@property (nonatomic, strong) CZThemeColorPicker cz_thumbTintColorPicker;


@end

NS_ASSUME_NONNULL_END
