//
//  UITextField+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (CZTheme)

@property (nonatomic, copy) CZThemeColorPicker cz_textColorPicker;

@end

NS_ASSUME_NONNULL_END
