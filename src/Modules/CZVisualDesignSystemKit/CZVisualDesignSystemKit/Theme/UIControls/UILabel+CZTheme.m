//
//  UILabel+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/20.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UILabel+CZTheme.h"
#import <objc/runtime.h>
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"

@interface UILabel ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, CZThemeColorPicker> *pickers;

@end

@implementation UILabel (CZTheme)

- (CZThemeColorPicker)cz_textColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_textColorPicker));
}

- (void)setCz_textColorPicker:(CZThemeColorPicker)cz_titleColorPicker {
    objc_setAssociatedObject(self, @selector(cz_textColorPicker), cz_titleColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.textColor = cz_titleColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_titleColorPicker copy] forKey:@"setTextColor:"];
}

@end
