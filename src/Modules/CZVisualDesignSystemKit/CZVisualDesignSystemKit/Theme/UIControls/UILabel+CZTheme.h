//
//  UILabel+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/20.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (CZTheme)

@property (nonatomic, copy) CZThemeColorPicker cz_textColorPicker;

@end

NS_ASSUME_NONNULL_END
