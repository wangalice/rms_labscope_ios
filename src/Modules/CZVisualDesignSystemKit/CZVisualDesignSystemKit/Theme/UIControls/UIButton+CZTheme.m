//
//  UIButton+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIButton+CZTheme.h"
#import "NSObject+CZTheme.h"
#import <objc/runtime.h>
#import "CZThemeManager.h"
#import "CZVirturalDesignMacro.h"

@interface UIButton ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, id>*pickers;

@end

@implementation UIButton (CZTheme)

- (void)cz_setTitleColorPicker:(CZThemeColorPicker)colorPicker forState:(UIControlState)state {
    [self setTitleColor:colorPicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[colorPicker copy] forKey:NSStringFromSelector(@selector(setTitleColor:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)cz_setImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)cz_setImageNamePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)cz_setBackgroundImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setBackgroundImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setBackgroundImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)switchTheme {
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary<NSString *, CZThemeColorPicker> *dictionary = (NSDictionary *)obj;
            [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, id  _Nonnull picker, BOOL * _Nonnull stop) {
                UIControlState state = [key integerValue];
                [UIView animateWithDuration:CZThemeAnimationDuration
                                 animations:^{
                                     if ([selector isEqualToString:NSStringFromSelector(@selector(setTitleColor:forState:))]) {
                                         UIColor *resultColor = ((CZThemeColorPicker)picker)(self.themeManager.currentTheme);
                                         [self setTitleColor:resultColor forState:state];
                                     } else if ([selector isEqualToString:NSStringFromSelector(@selector(setBackgroundImage:forState:))]) {
                                         UIImage *resultImage = ((CZThemeImagePicker)picker)(self.themeManager.currentTheme);
                                         [self setBackgroundImage:resultImage forState:state];
                                     } else if ([selector isEqualToString:NSStringFromSelector(@selector(setImage:forState:))]) {
                                         UIImage *resultImage = ((CZThemeImagePicker)picker)(self.themeManager.currentTheme);
                                         [self setImage:resultImage forState:state];
                                     }
                                 }];
            }];
        } else {
            SEL sel = NSSelectorFromString(key);
            CZThemeColorPicker picker = (CZThemeColorPicker)obj;
            UIColor *resultColor = picker(self.themeManager.currentTheme);
            [UIView animateWithDuration:CZThemeAnimationDuration
                             animations:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                 [self performSelector:sel withObject:resultColor];
#pragma clang diagnostic pop
                             }];
            
        }
    }];
}

@end
