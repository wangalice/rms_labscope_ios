//
//  UIImageView+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImageView+CZTheme.h"
#import "CZThemeImage.h"
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"
#import <objc/runtime.h>

@interface UIImageView ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, CZThemeImagePicker> *pickers;

@end

@implementation UIImageView (CZTheme)

- (CZThemeImagePicker)cz_imagePicker {
    return objc_getAssociatedObject(self, @selector(cz_imagePicker));
}

- (void)setCz_imagePicker:(CZThemeImagePicker)cz_imagePicker {
    objc_setAssociatedObject(self, @selector(cz_imagePicker), cz_imagePicker, OBJC_ASSOCIATION_RETAIN);
    self.image = cz_imagePicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_imagePicker copy] forKey:@"setImage:"];
}

@end
