//

//  CALayer+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CALayer+CZTheme.h"
#import <objc/runtime.h>
#import "NSObject+CZTheme.h"
#import "CZVirturalDesignMacro.h"
#import "CZThemeManager.h"

@interface CALayer ()

@property (nonatomic, strong) NSMutableDictionary<NSString *, CZThemeColorPicker> *pickers;

@end

@implementation CALayer (CZTheme)

- (CZThemeColorPicker)cz_shadowColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_shadowColorPicker));
}

- (void)setCz_shadowColorPicker:(CZThemeColorPicker)cz_shadowColorPicker {
    objc_setAssociatedObject(self, @selector(cz_shadowColorPicker), cz_shadowColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.shadowColor = cz_shadowColorPicker(self.themeManager.currentTheme).CGColor;
    [self.pickers setValue:[cz_shadowColorPicker copy] forKey:NSStringFromSelector(@selector(setShadowColor:))];
}

- (CZThemeColorPicker)cz_borderColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_borderColorPicker));
}

- (void)setCz_borderColorPicker:(CZThemeColorPicker)cz_borderColorPicker {
    objc_setAssociatedObject(self, @selector(cz_borderColorPicker), cz_borderColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.borderColor = cz_borderColorPicker(self.themeManager.currentTheme).CGColor;
    [self.pickers setValue:[cz_borderColorPicker copy] forKey:NSStringFromSelector(@selector(setBorderColor:))];
}

- (CZThemeColorPicker)cz_backgroundColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_backgroundColorPicker));
}

- (void)setCz_backgroundColorPicker:(CZThemeColorPicker)cz_backgroundColorPicker {
    objc_setAssociatedObject(self, @selector(cz_backgroundColorPicker), cz_backgroundColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.backgroundColor = cz_backgroundColorPicker(self.themeManager.currentTheme).CGColor;
    [self.pickers setValue:[cz_backgroundColorPicker copy] forKey:NSStringFromSelector(@selector(setBackgroundColor:))];
}

- (void)switchTheme {
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, CZThemeColorPicker  _Nonnull themeColorPicker, BOOL * _Nonnull stop) {
        CGColorRef result = themeColorPicker(self.themeManager.currentTheme).CGColor;
        [UIView animateWithDuration:kControlDefaultBorderRadius
                         animations:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                             if ([selector isEqualToString:NSStringFromSelector(@selector(setShadowColor:))]) {
                                 [self setShadowColor:result];
                             } else if ([selector isEqualToString:NSStringFromSelector(@selector(setBorderColor:))]) {
                                 [self setBorderColor:result];
                             } else if ([selector isEqualToString:NSStringFromSelector(@selector(setBackgroundColor:)) ]) {
                                 [self setBackgroundColor:result];
                             }
#pragma clang diagnostic pop
                         }];
    }];
}


@end
