//
//  NSObject+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class CZThemeManager;

@interface NSObject (CZTheme)

@property (nonatomic, strong, readonly) CZThemeManager *themeManager;

@end

NS_ASSUME_NONNULL_END
