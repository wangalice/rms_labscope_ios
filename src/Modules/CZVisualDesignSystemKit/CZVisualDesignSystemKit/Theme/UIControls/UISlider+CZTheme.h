//
//  UISlider+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZThemeImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface UISlider (CZTheme)

- (void)cz_setMinimumTrackImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;
- (void)cz_setMaximumTrackImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;
- (void)cz_setThumbImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state;


@end

NS_ASSUME_NONNULL_END
