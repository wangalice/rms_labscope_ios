//
//  CALayer+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CZThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (CZTheme)

@property (nonatomic, copy) CZThemeColorPicker cz_shadowColorPicker;
@property (nonatomic, copy) CZThemeColorPicker cz_borderColorPicker;
@property (nonatomic, copy) CZThemeColorPicker cz_backgroundColorPicker;

@end

NS_ASSUME_NONNULL_END
