//
//  UIView+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIView+CZTheme.h"
#import <objc/runtime.h>
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"

@interface UIView ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, CZThemeColorPicker> *pickers;

@end

@implementation UIView (CZTheme)

- (CZThemeColorPicker)cz_backgroundColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_backgroundColorPicker));
}

- (void)setCz_backgroundColorPicker:(CZThemeColorPicker)cz_backgroundColorPicker {
    objc_setAssociatedObject(self, @selector(cz_backgroundColorPicker), cz_backgroundColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.backgroundColor = cz_backgroundColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_backgroundColorPicker copy] forKey:@"setBackgroundColor:"];
}

- (CZThemeColorPicker)cz_tintColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_tintColorPicker));
}

- (void)setCz_tintColorPicker:(CZThemeColorPicker)cz_tinterColorPicker {
    objc_setAssociatedObject(self, @selector(cz_tintColorPicker), cz_tinterColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.tintColor = cz_tinterColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_tinterColorPicker copy] forKey:@"setTintColor:"];
}

- (CZThemeAlphaPicker)cz_alphaPicker {
    return objc_getAssociatedObject(self, @selector(cz_alphaPicker));
}

- (void)setCz_alphaPicker:(CZThemeAlphaPicker)cz_alphaPicker {
    objc_setAssociatedObject(self, @selector(cz_alphaPicker), cz_alphaPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.alpha = cz_alphaPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_alphaPicker copy] forKey:@"setAlpha:"];
}

@end
