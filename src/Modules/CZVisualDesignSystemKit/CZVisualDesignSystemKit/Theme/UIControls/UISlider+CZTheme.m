//
//  UISlider+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UISlider+CZTheme.h"
#import "NSObject+CZTheme.h"
#import <objc/runtime.h>
#import "CZThemeManager.h"
#import "CZVirturalDesignMacro.h"

@interface UISlider ()

@property (nonatomic, strong) NSMutableDictionary <NSString *, id> *pickers;

@end

@implementation UISlider (CZTheme)

- (void)cz_setMinimumTrackImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setMinimumTrackImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setMinimumTrackImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)cz_setMaximumTrackImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setMaximumTrackImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setMaximumTrackImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)cz_setThumbImagePicker:(CZThemeImagePicker)imagePicker forState:(UIControlState)state {
    [self setThumbImage:imagePicker(self.themeManager.currentTheme) forState:state];
    NSString *key = [NSString stringWithFormat:@"%@", @(state)];
    NSMutableDictionary *dictionary = [self.pickers valueForKey:key];
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[imagePicker copy] forKey:NSStringFromSelector(@selector(setThumbImage:forState:))];
    [self.pickers setValue:dictionary forKey:key];
}

- (void)switchTheme {
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary<NSString *, CZThemeImagePicker> *dictionary = (NSDictionary *)obj;
            [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, CZThemeImagePicker _Nonnull picker, BOOL * _Nonnull stop) {
                UIControlState state = [key integerValue];
                [UIView animateWithDuration:CZThemeAnimationDuration
                                 animations:^{
                                     if ([selector isEqualToString:NSStringFromSelector(@selector(setMaximumTrackImage:forState:))]) {
                                         UIImage *resultImage = picker(self.themeManager.currentTheme);
                                         [self setMaximumTrackImage:resultImage forState:state];
                                     } else if ([selector isEqualToString:NSStringFromSelector(@selector(setMinimumTrackImage:forState:))]) {
                                         UIImage *resultImage = picker(self.themeManager.currentTheme);
                                         [self setMinimumTrackImage:resultImage forState:state];
                                     } else if ([selector isEqualToString:NSStringFromSelector(@selector(setThumbImage:forState:))]) {
                                         UIImage *resultImage = picker(self.themeManager.currentTheme);
                                         [self setThumbImage:resultImage forState:state];
                                     }
                                 }];
            }];
        }
    }];
}

@end
