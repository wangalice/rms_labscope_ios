//
//  UISwitch+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UISwitch+CZTheme.h"
#import <objc/runtime.h>
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"

@interface UISwitch ()

@property (nonatomic, strong) NSMutableDictionary *pickers;

@end

@implementation UISwitch (CZTheme)

- (CZThemeColorPicker)cz_onTintColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_onTintColorPicker));
}

- (void)setCz_onTintColorPicker:(CZThemeColorPicker)cz_onTintColorPicker {
    objc_setAssociatedObject(self, @selector(cz_onTintColorPicker), cz_onTintColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.onTintColor = cz_onTintColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_onTintColorPicker copy] forKey:@"setOnTintColor:"];
}

- (CZThemeColorPicker)cz_tintColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_tintColorPicker));
}

- (void)setCz_tintColorPicker:(CZThemeColorPicker)cz_tintColorPicker {
    objc_setAssociatedObject(self, @selector(cz_tintColorPicker), cz_tintColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.tintColor = cz_tintColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_tintColorPicker copy] forKey:@"setTintColor:"];
}

- (CZThemeColorPicker)cz_thumbTintColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_thumbTintColorPicker));
}

- (void)setCz_thumbTintColorPicker:(CZThemeColorPicker)cz_thumbTintColorPicker {
    objc_setAssociatedObject(self, @selector(cz_thumbTintColorPicker), cz_thumbTintColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.thumbTintColor = cz_thumbTintColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_thumbTintColorPicker copy] forKey:@"setThumbTintColor:"];
}

@end
