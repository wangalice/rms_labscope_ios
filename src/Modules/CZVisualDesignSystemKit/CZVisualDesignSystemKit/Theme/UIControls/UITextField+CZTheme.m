//
//  UITextField+CZTheme.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UITextField+CZTheme.h"
#import <objc/runtime.h>
#import "NSObject+CZTheme.h"
#import "CZThemeManager.h"

@interface UITextField ()

@property (nonatomic, strong) NSDictionary <NSString *, CZThemeColorPicker> *pickers;

@end

@implementation UITextField (CZTheme)

- (CZThemeColorPicker)cz_textColorPicker {
    return objc_getAssociatedObject(self, @selector(cz_textColorPicker));
}

- (void)setCz_textColorPicker:(CZThemeColorPicker)cz_textColorPicker {
    objc_setAssociatedObject(self, @selector(cz_textColorPicker), cz_textColorPicker, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.textColor = cz_textColorPicker(self.themeManager.currentTheme);
    [self.pickers setValue:[cz_textColorPicker copy] forKey:@"setTextColor:"];
}

@end
