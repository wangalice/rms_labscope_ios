//
//  CAShapeLayer+CZTheme.h
//  CZVisualDesignSystemKit
//
//  Created by Carl Zeiss on 2019/4/26.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CZThemeColor.h"

NS_ASSUME_NONNULL_BEGIN

@interface CAShapeLayer (CZTheme)

@property (nonatomic, copy) CZThemeColorPicker cz_fillColorPicker;
@property (nonatomic, copy) CZThemeColorPicker cz_strokeColorPicker;

@end

NS_ASSUME_NONNULL_END
