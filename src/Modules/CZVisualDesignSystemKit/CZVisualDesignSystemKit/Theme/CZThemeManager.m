//
//  CZThemeManager.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/31.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZThemeManager.h"

NSString * const kCZThemeChangeEventNotification = @"kCZThemeChangeEventNotification";
NSString * const kCZApplicationShouldChangeStatusBarStyle = @"kCZApplicationShouldChangeStatusBarStyle";

@interface CZThemeManager ()

@property (nonatomic, assign) CZTheme currentTheme;

@end

@implementation CZThemeManager

#pragma mark - Initial methods

+ (instancetype)sharedManager {
    static CZThemeManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CZThemeManager alloc] init];
    });
    return manager;
}

- (instancetype)init {
    if (self = [super init]) {
        //Set current default theme to the dark;
        _currentTheme = CZThemeDark;
    }
    return self;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [super allocWithZone:zone];
}

- (void)dealloc {
    NSLog(@"dealloc method run succeed");
}

- (void)switchThemeTo:(CZTheme)theme {
    if ([self supportedTheme:theme] && self.currentTheme != theme) {
        self.currentTheme = theme;
        [[NSNotificationCenter defaultCenter] postNotificationName:kCZThemeChangeEventNotification
                                                             object:nil];
        if (self.shouldUpdateStatusBarStyle) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kCZThemeChangeEventNotification
                                                                object:nil];
        }
    } else {
        NSLog(@"Switch to the same theme, so we do nothing with it.");
    }
}

- (BOOL)supportedTheme:(CZTheme)theme {
    switch (theme) {
        case CZThemeDark:
        case CZThemeLight:
        case CZThemeBlue:
            return YES;
        default:
            break;
    }
    return NO;
}

- (NSArray<NSNumber *> *)themes {
    return [NSArray arrayWithObjects:@(CZThemeDark), @(CZThemeLight), nil];
}

@end
