//
//  CZColorTable.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/3.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZColorTable.h"
#import "CZThemeAlpha.h"
#import "UIColor+CZColorStyles.h"
#import "CZVirturalDesignMacro.h"

@implementation CZColorTable

+ (instancetype)sharedColorTable {
    static CZColorTable *colorTable;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        colorTable = [[CZColorTable allocWithZone:NULL] init];
    });
    return colorTable;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [super allocWithZone:zone];
}

+ (void)dealloc {
    NSLog(@"CZColorTable dealloc enter #1");
}

+ (CZThemeColorPicker)defaultBackgroundColor {
    return CZThemeColorWithColors([UIColor cz_gs110], [UIColor cz_gs30]);
}

+ (CZThemeColorPicker)defaultActiveBackgroundColor {
    return CZThemeColorWithColors([UIColor cz_gs90], [UIColor cz_gs80]);
}

+ (CZThemeColorPicker)defaultActivePrimaryBlueColor {
    return CZThemeColorWithColors([UIColor cz_pb100], [UIColor cz_pb100]);
}

+ (CZThemeColorPicker)defaultSelectedBackgroundColor {
    return CZThemeColorWithColors([UIColor cz_gs90], [UIColor cz_gs80]);
}

+ (CZThemeColorPicker)defaultDisbaledBackgroundColor {
    return CZThemeColorWithColors([UIColor cz_gs10], [UIColor cz_gs10]);
}


+ (CZThemeColorPicker)defaultHighlightTextColor {
    return CZThemeColorWithColors([UIColor cz_gs10], [UIColor cz_gs120]);
}

+ (CZThemeColorPicker)defaultTextColor {
    return CZThemeColorWithColors([UIColor cz_gs50], [UIColor cz_gs100]);
}

+ (CZThemeColorPicker)defaultActiveTextColor {
    return CZThemeColorWithColors([UIColor cz_gs10], [UIColor cz_gs10]);
}

+ (CZThemeColorPicker)defaultDisabledTextColor {
    return CZThemeColorWithColors([[UIColor cz_gs50] colorWithAlphaComponent:0.5],
                                  [[UIColor cz_gs100] colorWithAlphaComponent:0.5]);
}

+ (CZThemeColorPicker)defaultActiveDisabledTextColor {
    return CZThemeColorWithColors([[UIColor cz_gs10] colorWithAlphaComponent:0.5],
                                  [[UIColor cz_gs10] colorWithAlphaComponent:0.5]);
}

+ (CZThemeColorPicker)defaultNormalBorderColor {
    return CZThemeColorWithColors([UIColor cz_gs120], [UIColor cz_gs60]);
}

@end
