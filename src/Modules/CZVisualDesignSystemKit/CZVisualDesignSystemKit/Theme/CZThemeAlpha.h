//
//  CZThemeAlpha.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZVirturalDesignMacro.h"

NS_ASSUME_NONNULL_BEGIN
typedef float (^CZThemeAlphaPicker)(CZTheme theme);

CZThemeAlphaPicker CZThemeAlphaPickerWithAlphas(CGFloat primaryAlpha,...);

@interface CZThemeAlpha : NSObject

+ (CZThemeAlphaPicker)alphaWithThemeAlpha:(CGFloat)primaryAlpha;

+ (CZThemeAlphaPicker)alphaWithThemeAlphas:(NSArray <NSNumber *>*)alphas;

@end

NS_ASSUME_NONNULL_END
