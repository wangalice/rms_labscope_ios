//
//  CZThemeImage.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/2.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZThemeImage.h"
#import "CZThemeManager.h"


@implementation CZThemeImage

CZThemeImagePicker CZThemeImagePickerWithImages(UIImage * primaryImage,...) {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSMutableArray <UIImage *> *imagesArray = [NSMutableArray array];
    
    [imagesArray addObject:primaryImage];
    
    NSUInteger num_args = themes.count - 1;
    va_list images;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvarargs"
    va_start(images, num_args);
#pragma clang diagnostic pop
    for (NSUInteger i = 0; i < num_args; i++) {
        UIImage *secondaryImage = va_arg(images, UIImage *);
        [imagesArray addObject:secondaryImage];
    }
    va_end(images);
    
    return ^(CZTheme theme) {
        NSInteger index = [themes indexOfObject:@(theme)];
        return imagesArray[index];
    };
}

+ (CZThemeImagePicker)imageWithUIImages:(NSArray <UIImage *> *)images {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSParameterAssert(themes.count == images.count);
    return ^(CZTheme theme) {
        NSInteger index = [themes indexOfObject:@(theme)];
        if (index < images.count) {
            return images[index];
        } else {
            NSLog(@"Array crossover expection: %@", images);
            return images[0];
        }
    };
}

+ (CZThemeImagePicker)imageWithUIImageNames:(NSArray <NSString *> *)imageNames {
    NSArray *themes = [CZThemeManager sharedManager].themes;
    NSParameterAssert(themes.count == imageNames.count);
    return ^(CZTheme theme) {
        NSInteger index = [themes indexOfObject:@(theme)];
        if (index < imageNames.count) {
            return [UIImage imageNamed:imageNames[index]];
        } else {
            NSLog(@"Array crossover expection: %@", imageNames);
            return [UIImage imageNamed:imageNames[0]];
        }
    };
}

@end
