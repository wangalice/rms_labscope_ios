//
//  CZImageAssert.m
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageAssert.h"
#import <SVGKit/SVGKit.h>

UIImage *Assert(NSString *imageName) {
    NSBundle *bundle = [NSBundle bundleForClass:[CZImageAssert class]];
    NSDataAsset *asset = [[NSDataAsset alloc] initWithName:imageName bundle:bundle];
    SVGKSource *source = [SVGKSourceNSData sourceFromData:asset.data URLForRelativeLinks:nil];
    if (source == nil) {
        source = [SVGKSourceLocalFile internalSourceAnywhereInBundleUsingName:imageName];
    }
    SVGKImage *image = [SVGKImage imageWithSource:source];
    return image.UIImage;
}

@implementation CZImageAssert

@end
