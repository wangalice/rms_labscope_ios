//
//  CZImageAssert.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/2/21.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef __cplusplus
extern "C" {
#endif
    
    UIImage *Assert(NSString *imageName);
#ifdef __cplusplus
}
#endif

@interface CZImageAssert : NSObject

@end
