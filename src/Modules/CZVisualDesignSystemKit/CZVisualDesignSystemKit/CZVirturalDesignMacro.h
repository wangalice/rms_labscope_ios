//
//  CZVirturalDesignMacro.h
//  CZVisualDesignSystemKit
//
//  Created by Sun, Shaoge on 2019/1/31.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#ifndef CZVirturalDesignMacro_h
#define CZVirturalDesignMacro_h

// State & level

//Reference UI Design: https://www.figma.com/file/CtDBCRfwa8id6rOzSCR13T19/0.0-Design-System-StyleGuide?node-id=44%3A36

/**
 States are visual representations used to communicate the status of a component or interactive element.
 
 - CZControlStateEnable: An enabled state communicates an interactive component or element;
 - CZControlStateDisable: A disabled state communicates when a component or element isn’t interactive, and should be deemphasized in a UI;
 - CZControlStateHover: Hover states are initiated by the user pausing over an interactive element using a cursor;
 - CZControlStateFocused: A focused state communicates when a user has highlighted an element using a keyboard or voice;
 - CZControlStatePressed: A pressed state communicates a user-initiated tap or click by a cursor, keyboard, or voice input method;
 */
typedef NS_ENUM(NSUInteger, CZControlState) {
    CZControlStateEnable = 0,
    CZControlStateDisable,
    CZControlStateHover, //Not support for the iOS Platform for now
    CZControlStateFocused,
    CZControlStatePressed
};

/**
 The emphasis determines the priority level of a component and how much it stands out from it’s surrounding.
 
 - CZControlEmphasisDefault: Default is the not manipulated state of a component, the original version. It has a medium emphasis, since it shouldn’t gain too much attention;
 - CZControlEmphasisActive: Active states emphasize components from their surrounding. It can be initiated by the user or by default;
 - CZControlEmphasisActivePrimary: Active primary states highly emphasize the most important components, so they stand out. It can be initiated by the user or by default;
 */

typedef NS_ENUM(NSUInteger, CZControlEmphasisLevel) {
    CZControlEmphasisDefault = 0,
    CZControlEmphasisActive,
    CZControlEmphasisActivePrimary
};

/**
 CZTheme
 
 - CZThemeDark: Dark theme, the Default theme
 - CZThemeLight: Light theme
 */
typedef NS_ENUM(NSUInteger, CZTheme) {
    CZThemeDark = 0,
    CZThemeLight,
    CZThemeBlue // TODO: As the test theme 
};

//Animation Duration
#define CZThemeAnimationDuration 1.0f

//border radius
static float const kControlDefaultBorderRadius = 3.0f;
static float const kControlDefaultBorderWidth = 1.0f;
static float const kControlDefaultNoneBorderWidth = 0.0f;

//default control height
static float const kControlDefaultSmallHeight = 32.0f;
static float const kControlDefaultLargeHeight = 48.0f;

//default alpha
static float const kControlDefaultNormalAlpha = 1.0f;
static float const kControlDefaultDisableAlpha = 0.5f;

#endif /* CZVirturalDesignMacro_h */
