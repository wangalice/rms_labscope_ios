//
//  CZWisionImageTuneClient.m
//  Hermes
//
//  Created by Halley Gu on 5/20/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZWisionImageTuneClient.h"
#import <CZToolbox/CZToolbox.h>

static const uint16_t kImageTunePort = 5000;
static const NSTimeInterval kImageTuneTimeout = 12;

// Message packet header structure
typedef struct {
    uint32_t headerMagicNum;        // Header magic number to identify as image tuning tool message
    uint32_t checkSum;              // Check sum for message validation
    uint32_t commandId;             // Command ID
    uint32_t commandFlags;          // Command flags to identify the type the command
    uint32_t paramSize;             // Size of parameters
} CZWisionImageTunePacketHeader;

// Message packet footer structure
typedef struct {
    uint32_t footerMagicNum;        // footer magic number to confirm end of parameters
} CZWisionImageTunePacketFooter;

// YUV/RAW data file header format
typedef struct {
    uint32_t validDataStartOffset;  // Offset in bytes from start of file where actual data is stored in file
    uint32_t magicNum;              // To indicate if this is a valid header
    uint32_t version;               // Header format version number, 0x00010000, is v1.0
    uint32_t dataFormat;            // 0: raw data, 1: YUV422 data, 2: YUV420 data
    uint32_t dataWidth;             // In pixels
    uint32_t dataHeight;            // In lines
    uint32_t dataBytesPerLines;     // In bytes
    uint32_t yuv420ChormaOffset;    // In bytes from start of valid data, only valid for YUV420 data, file offset is (validDataStartOffset+dataChormaOffset)
    uint32_t rawDataStartPhase;     // 0: R, 1: Gr, 2: Gb, 3: B
    uint32_t rawDataBitsPerPixel;   // 8..14 bits
    uint32_t rawDataFormat;         // 0: Normal 1pixel in 16-bits, no compression, 1: Alaw compressed, 2: Dpcm compressed
    uint32_t H3aRegs[32];           // H3A Register Dump  - AWB
    uint32_t AwbNumWinH;            // Number of AWB windows in H direction
    uint32_t AwbNumWinV;            // Number of AWB windows in V direction
    uint32_t AwbRgbDataOffset;      // Offset in bytes from start of file where RGB H3A data is present
    uint32_t AwbYuvDataOffset;      // Offset in bytes from start of file where YUV H3A data is present
    uint32_t AwbMiscData[16];       // AWB algorithm specific data
    uint32_t AwbRgbData[3072];      // Valid data is 4x3xN bytes where N = AwbNumWinH x AwbNumWinV
    uint32_t AwbYuvData[3072];      // Valid data is 4x3xN bytes where N = AwbNumWinH x AwbNumWinV
} CZWisionImageTuneRawFileHeader;

static const long TAG_SNAP = 0;
static const long TAG_READ_HEAD = 1;
static const long TAG_READ_RAW_DATA = 2;

@interface CZWisionImageTuneClient () {
    uint32_t _rawImageLength;
    int32_t _framesToSnap;
    dispatch_queue_t _imageTuneQueue;
}

@property (nonatomic, copy) NSString *cameraIPAddress;
@property (atomic, retain) NSMutableData *rawImageData;

- (void)endSnappingWithImage:(UIImage *)image;
+ (UIImage *)newImageFromYUVRawImageData:(NSData *)rawImageData;

@end

@implementation CZWisionImageTuneClient

- (id)init {
    self = [super init];
    if (self) {
        _imageTuneQueue = dispatch_queue_create("com.zeisscn.wision.imagetune", NULL);
        _rawImageLength = 0;
    }
    return self;
}

- (void)dealloc {
    [_rawImageData release];
    dispatch_release(_imageTuneQueue);
    
    [_cameraIPAddress release];
    
    [super dealloc];
}

- (void)beginSnappingRawImageFromCamera:(NSString *)cameraIPAddress frameCount:(NSUInteger)frameCount {
    if (self.rawImageData) {
        // Do nothing if a snapping operation is in process.
        [self endSnappingWithImage:nil];
        return;
    }

    _framesToSnap = (int32_t)frameCount;
    
    self.cameraIPAddress = cameraIPAddress;
    
    [self beginTCPConnecting];
}

#pragma mark - Private methods

- (void)endSnappingWithImage:(UIImage *)image {
    id delegate = self.delegate;
    dispatch_async(dispatch_get_main_queue(), ^() {
        if ([delegate respondsToSelector:@selector(imageTuneClient:didGetRawImage:)]) {
            [delegate imageTuneClient:self didGetRawImage:image];
        }
    });
    
    self.rawImageData = nil;
    _rawImageLength = 0;
}

+ (UIImage *)newImageFromYUVRawImageData:(NSData *)rawImageData {
    NSRange headRange = NSMakeRange(sizeof(CZWisionImageTunePacketHeader), sizeof(CZWisionImageTuneRawFileHeader));
    if (rawImageData.length < NSMaxRange(headRange)) {
        return nil;
    }
    
    CZWisionImageTuneRawFileHeader info;
    [rawImageData getBytes:&info
                      range:headRange];
    
    const void *data = [rawImageData bytes];
    if (!data) {
        return nil;
    }
    const NSUInteger bufferOffset = info.validDataStartOffset + sizeof(CZWisionImageTunePacketHeader);
    if (bufferOffset > [rawImageData length]) {
        return nil;
    }
    
    const void *buffer = data + bufferOffset;
    NSUInteger remainLength = [rawImageData length] - bufferOffset;
    
    UIImage *result = nil;
    @autoreleasepool {
        result = [[CZCommonUtils imageFromYUV420spNV12Buffer:buffer
                                                      length:remainLength
                                                       width:info.dataWidth
                                                      height:info.dataHeight] retain];
    }
    return result;
}

- (void)beginTCPConnecting {
    NSMutableData *mutableData = [[NSMutableData alloc] init];
    self.rawImageData = mutableData;
    [mutableData release];
    
    // Create TCP socket to connect to server
    GCDAsyncSocket *socket = [[GCDAsyncSocket alloc] initWithDelegate:self
                                                        delegateQueue:_imageTuneQueue];
    
    NSError *error = nil;
    [socket connectToHost:self.cameraIPAddress
                   onPort:kImageTunePort
              withTimeout:kImageTuneTimeout
                    error:&error];
    if (error != nil) {
        CZLogv(@"Failed to connect to camera while getting YUV image.");
        [self endSnappingWithImage:nil];
    }
    
    [socket release];
}

- (void)socketSendBeginSnapPacket:(GCDAsyncSocket *)sock {
    // Prepare the header and footer for the packet
    CZWisionImageTunePacketHeader header;
    header.headerMagicNum = 0xABCD1234;
    header.commandId      = 0x0D; // IMAGE_TUNE_SYS_SEND_YUV_DATA
    header.commandFlags   = 0x01;
    header.paramSize      = 0;
    header.checkSum       = header.commandId + header.commandFlags + header.paramSize;
    
    CZWisionImageTunePacketFooter footer;
    footer.footerMagicNum = 0x5678EF09;
    
    // Make the packet for getting RAW image from camera
    size_t packetSize = sizeof(CZWisionImageTunePacketHeader) + sizeof(CZWisionImageTunePacketFooter);
    uint32_t *packet = (uint32_t *)malloc(packetSize);
    
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:packet length:packetSize];
    [writer writeLong:header.headerMagicNum];
    [writer writeLong:header.checkSum];
    [writer writeLong:header.commandId];
    [writer writeLong:header.commandFlags];
    [writer writeLong:header.paramSize];
    [writer writeLong:footer.footerMagicNum];
    [writer release];
    
    // Send the packet
    NSData *packetData = [[NSData alloc] initWithBytes:packet length:packetSize];
    [sock writeData:packetData withTimeout:kImageTuneTimeout tag:TAG_SNAP];
    [packetData release];
    
    free(packet);
}

#pragma mark - GCDAsyncSocketDelegate methods

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    CZLogv(@"Wision ImageTune: connected");
    
    [self socketSendBeginSnapPacket:sock];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    CZLogv(@"Wision ImageTune: didDisconnect");
    
    // Only call delegate when disconnected by accident
    if (err) {
        [self endSnappingWithImage:nil];
    }
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    if (tag == TAG_READ_HEAD) {
        CZWisionImageTunePacketHeader header;
        [data getBytes:&header length:sizeof(CZWisionImageTunePacketHeader)];
        _rawImageLength = header.paramSize + sizeof(CZWisionImageTunePacketHeader) + sizeof(CZWisionImageTunePacketFooter);

        if (_rawImageLength > sizeof(CZWisionImageTunePacketHeader)) {
            [sock readDataToLength:_rawImageLength - sizeof(CZWisionImageTunePacketHeader)
                       withTimeout:kImageTuneTimeout
                            buffer:_rawImageData
                      bufferOffset:sizeof(CZWisionImageTunePacketHeader)
                               tag:TAG_READ_RAW_DATA];
        } else {
            [self endSnappingWithImage:nil];
            _framesToSnap = 0;
            
            [sock disconnect];
        }
    } else if (tag == TAG_READ_RAW_DATA) {
        if (_rawImageData.length >= _rawImageLength) {
            CZLogv(@"Wision ImageTune: didFinishSnapping, length = %lu", (unsigned long)_rawImageData.length);
            
            UIImage *image = [[self class] newImageFromYUVRawImageData:self.rawImageData];
            if (image) {
                [self endSnappingWithImage:image];
                [image release];

                _framesToSnap--;
            }
        } else {
            [self endSnappingWithImage:nil];
            _framesToSnap = 0;
        }
        
        [sock disconnect];
    
        if (_framesToSnap > 0) {  // start next snapping
            [self beginTCPConnecting];
        }
    } else {
        NSAssert(false, @"Unknown tag ID");
    }
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag {
    CZLogv(@"Wision ImageTune: didWriteData");
    
    if (tag == TAG_SNAP) {
        [sock readDataToLength:sizeof(CZWisionImageTunePacketHeader)
                   withTimeout:kImageTuneTimeout
                        buffer:_rawImageData
                  bufferOffset:0
                           tag:TAG_READ_HEAD];
    } else {
        NSAssert(false, @"Unknown tag ID");
    }
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length {
    CZLogv(@"Wision ImageTune: readTimeout, elapsed: %lf", elapsed);
    return 0.0;
}

- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
                 elapsed:(NSTimeInterval)elapsed
               bytesDone:(NSUInteger)length {
    CZLogv(@"Wision ImageTune: writeTimeout, elapsed: %lf", elapsed);
    return 0.0;
}

@end
