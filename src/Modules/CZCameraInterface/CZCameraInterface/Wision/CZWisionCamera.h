//
//  CZWisionCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZRTSPCamera.h"
#import "CZWisionImageTuneClient.h"

/*!
 * Represents a Wision-manufactured local IP camera.
 */
@interface CZWisionCamera : CZRTSPCamera<CZWisionImageTuneClientDelegate>

@end
