//
//  CZWisionCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZWisionCamera.h"
#import "CZCameraSubclass.h"
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFImageRequestOperation.h>
#import <CZToolbox/CZToolbox.h>
#import "CZWisionImageTuneClient.h"

static const NSTimeInterval kWisionDefaultTimeout = 20;
static const NSUInteger kWisionGainLevels[] = {
    80, 269, 457, 646, 834, 1023
};
static const NSUInteger kWisionGainLevelsCount = sizeof(kWisionGainLevels) / sizeof(kWisionGainLevels[0]);

@interface CZWisionCamera ()

@property (nonatomic, retain) CZWisionImageTuneClient *imageTuneClient;
@property (atomic, copy) NSString *version;

@property (nonatomic, retain) NSNumber *actualExposureTime;
@property (nonatomic, retain) NSNumber *originalExposureTime;

- (NSString *)getParameterFromIniHtm:(NSString *)param;
- (NSString *)getParameterFromVbHtm:(NSString *)param;
- (void)setParameters:(NSDictionary *)params;

- (void)beginSnappingJPEGImage;
- (void)beginSnappingRAWImage:(NSUInteger)frameCount;

+ (NSString *)urlEncodedStringFromObject:(id)object;
+ (NSString *)urlEncodedStringWithParameters:(NSDictionary *)params;
+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain;

@end

@implementation CZWisionCamera

- (id)init {
    self = [super init];
    if (self) {
        _imageTuneClient = [[CZWisionImageTuneClient alloc] init];
        [_imageTuneClient setDelegate:self];
    }
    return self;
}

- (void)dealloc {
    [_imageTuneClient setDelegate:nil];
    [_imageTuneClient release];
    [_version release];
    [_actualExposureTime release];
    [_originalExposureTime release];
    
    [super dealloc];
}

#pragma mark - Overridden methods

- (CZCameraCapability)capabilities {
    return (CZCameraCapabilityResetAll |
            CZCameraCapabilityExposureMode |
            CZCameraCapabilityExposureTime |
            CZCameraCapabilityExtraExposureTime |
            CZCameraCapabilityGain |
            CZCameraCapabilityExposureTimeAutoAdjust |
            CZCameraCapabilityWhiteBalanceMode |
            CZCameraCapabilityWhiteBalanceAutoAdjust |
            CZCameraCapabilityStreamingQuality |
            CZCameraCapabilityImageOrientation |
            CZCameraCapabilitySharpening |
            CZCameraCapabilityGrayscaleMode |
            CZCameraCapabilityIFrameInterval);
}

- (NSString *)firmwareVersion {
    if (self.version == nil) {
        NSString *version = [self getParameter:@"version" fromPath:@"ipcam_id.htm"];
        if ([version hasSuffix:@"\";"]) {
            version = [version stringByReplacingOccurrencesOfString:@"\";" withString:@""];
        }
        if (version.length > 0) {
            self.version = version;
        }
    }
    return self.version;
}

- (BOOL)isFirmwareUpdated {
    NSString *firmwareVersion = [self firmwareVersion];
    if (firmwareVersion.length > 0) {
        NSRange zeissRange = [firmwareVersion rangeOfString:@"Zeiss"];
        if (zeissRange.location != NSNotFound && zeissRange.location > 0) {
            NSString *subVersion = [[firmwareVersion substringWithRange:NSMakeRange(zeissRange.location - 1, 1)] uppercaseString];
            unichar subVersionLetter = [subVersion characterAtIndex:0];
            if (subVersionLetter < 'K') { // Hardcoded latest version number
                return NO;
            }
        }
    }
    return YES;
}

- (void)updateFirmware {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"cramfsImage_ipnc_DM368_S1" ofType:@""];
    NSData *fileData = [[NSData alloc] initWithContentsOfFile:filePath];
    CZLogv(@"Firmware binary length = %lu", (unsigned long)fileData.length);
    if (fileData) {
        NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://%@:8080/", self.ipAddress];
        NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
        if (baseUrl == nil) {
            [fileData release];
            [baseUrlString release];
            return;
        }
        
        NSString *cookie = [NSString stringWithFormat:@"zyxcookie=IP=%@$port=8080$; items=0,%@,8080,admin,9999,0,0,%@,0",
                            self.ipAddress, self.ipAddress, self.version];
        self.version = nil;
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
        [httpClient setAuthorizationHeaderWithUsername:@"admin" password:@"9999"];
        [httpClient setDefaultHeader:@"Cookie" value:cookie];
        httpClient.parameterEncoding = AFFormURLParameterEncoding;
        
        void (^formDataBlock)(id<AFMultipartFormData> formData) = ^(id<AFMultipartFormData> formData) {
            [formData setBoundary:@"---------------------------7de8a2d60602"];
            
            [formData appendPartWithFileData:fileData
                                        name:@"upfile_tx"
                                    fileName:@"cramfsImage_ipnc_DM368_S1"
                                    mimeType:@"application/octet-stream"];
            
            // "软件升级" -_-b
            char special[12] = {0xe8, 0xbd, 0xaf, 0xe4, 0xbb, 0xb6, 0xe5, 0x8d, 0x87, 0xe7, 0xba, 0xa7};
            NSData *specialData = [NSData dataWithBytes:special length:12];
            [formData appendPartWithFormData:specialData name:@"upSoft"]; // "submit"
        };
        
        NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                             path:@"update.cgi"
                                                                       parameters:nil
                                                        constructingBodyWithBlock:formDataBlock];
        [request setTimeoutInterval:300];
        
        CZLogv(@"Sending firmware to the camera:\n\n%@", request.allHTTPHeaderFields);
        
        CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
        if (result.error) {
            CZLogv(@"Failed to send the request to update firmware of Wision camera: %@", [result.error localizedDescription]);
        } else {
            NSMutableData *dataWithoutZero = [[NSMutableData alloc] init];
            const unsigned char *rawData = result.data.bytes;
            for (NSUInteger i = 0; i < result.data.length; ++i) {
                if (rawData[i] > 0) {
                    [dataWithoutZero appendBytes:(rawData + i) length:1];
                }
            }
            
            NSMutableString *body = [[NSMutableString alloc] initWithData:dataWithoutZero encoding:NSUTF8StringEncoding];
            if (body == nil) {
                body = [[NSMutableString alloc] initWithData:dataWithoutZero encoding:NSASCIIStringEncoding];
            }
            
            if (body) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)result.response;
                CZLogv(@"Got response from Wision camera:\n\n%@\n\n%@", httpResponse.allHeaderFields, body);
                [body release];
                
                // TODO: Better error handling.
            }
            
            [dataWithoutZero release];
        }
        
        [httpClient release];
        [baseUrl release];
        [baseUrlString release];
    }
    [fileData release];
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    [super setName:name andUpload:shouldUpload];
    if (shouldUpload) {
        if (name.length == 0) {
            name = @" ";
        }

        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                name, @"title",
                                nil];
        [self setParameters:params];
        [params release];
    }
}

- (NSUInteger)maxSupportedNameLength {
    NSString *firmwareVersion = [self firmwareVersion];
    if (firmwareVersion.length > 0) {
        NSRange zeissRange = [firmwareVersion rangeOfString:@"Zeiss"];
        if (zeissRange.location != NSNotFound && zeissRange.location > 0) {
            NSString *subVersion = [[firmwareVersion substringWithRange:NSMakeRange(zeissRange.location - 1, 1)] uppercaseString];
            unichar subVersionLetter = [subVersion characterAtIndex:0];
            if (subVersionLetter > 'H' && subVersionLetter <= 'Z') {
                return 12;
            }
        }
    }
    return 7;
}

- (void)beginContinuousSnapping:(NSUInteger)frameCount {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        CZLogv(@"Failed to snap - no Wi-Fi available.");
        
        NSError *timeoutError = [NSError errorWithDomain:CZCameraErrorDomain
                                                    code:kCZCameraTimeoutError
                                                userInfo:nil];
        
        id delegate = self.delegate;
        dispatch_async(dispatch_get_main_queue(), ^() {
            if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                [delegate camera:self didFinishSnapping:nil error:timeoutError];
            }
        });
    } else {
        [self beginSnappingRAWImage:frameCount];
    }
}

- (CGSize)liveResolution {
    return CGSizeMake(640, 480);
}

- (void)restart {
    [self setParameters:@{@"ipcamrestartcmd": [NSNull null]}];
}

- (void)performReset {
    [self setGain:[self maxGain]];
    [super performReset];
}

- (NSURL *)rtspUrl {
    NSString *urlString = [[NSString alloc] initWithFormat:@"rtsp://admin:9999@%@/PSIA/Streaming/channels/h264cif", self.ipAddress];
    NSURL *url = [NSURL URLWithString:urlString];
    [urlString release];
    return url;
}

#pragma mark - Exposure Mode

- (CZCameraExposureMode)exposureMode {
    NSString *modeString = [self getParameterFromIniHtm:@"exposuremod"];
    if (!modeString) {
        return [super exposureMode]; // Fallback to default implementation
    }
    NSInteger modeValue = [modeString integerValue];
    return (modeValue == 1) ? kCZCameraExposureManual : kCZCameraExposureAutomatic;
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)exposureMode];
    [params setObject:value forKey:@"setexpmod"];
    [value release];
    
    [self setParameters:params];
    [params release];
}

- (void)beginExposureTimeAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self setExposureMode:kCZCameraExposureAutomatic];
        sleep(3); // Wait for the camera to finish auto exposure-time adjust.
        [self setExposureTime:[self exposureTime]];
        [self setExposureMode:kCZCameraExposureManual];
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishExposureTimeAutoAdjust:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                [self.controlDelegate cameraDidFinishExposureTimeAutoAdjust:self];
            });
        }
    });
}

#pragma mark - Exposure Time

- (NSArray<NSNumber *> *)supportedExposureTimeValues {
    return @[@0.02f, @0.05f, @0.1f, @0.2f, @0.5f, @1.f, @2.f, @4.f, @5.f, @8.f, @8.33333f, @10.f,
             @16.6666f, @20.f, @25.f, @30.30303f, @33.33333f, @40.f, @50.f, @66.66666f, @76.923f, @100.f, @200.f];
}

- (float)exposureTime {
    CZCameraExposureMode exposureMode = [self exposureMode];
    if (exposureMode == kCZCameraExposureAutomatic) {
        NSString *exposureString = [self getParameterFromVbHtm:@"getexptime"];
        if (exposureString) {
            return [exposureString floatValue] / 1000.0f;
        }
    } else {
        NSString *exposureString = [self getParameterFromIniHtm:@"fixedexposure"];
        if (exposureString) {
            NSArray *supportedValues = [self supportedExposureTimeValues];
            NSInteger exposureIndex = [exposureString integerValue] - 1;
            if (exposureIndex < 0) {
                exposureIndex = 0;
            }
            if (exposureIndex >= supportedValues.count) {
                exposureIndex = supportedValues.count - 1;
            }
            return [supportedValues[exposureIndex] floatValue];
        }
    }
    
    return [super exposureTime]; // Fallback to default implementation
}

- (void)setExposureTime:(float)milliseconds {
    NSUInteger index = [self nearestValidExposureTimeIndex:milliseconds];
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)index + 1];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"fixedexptime",
                            @"1", @"setexpmod",
                            nil];
    [self setParameters:params];
    [value release];
    [params release];
}

#pragma mark - Gain

- (NSUInteger)minGain {
    return 1;
}

- (NSUInteger)maxGain {
    return 6;
}

- (NSUInteger)gain {
    NSString *gainString = [self getParameterFromIniHtm:@"maxgain"];
    if (!gainString) {
        return [super gain]; // Fallback to default implementation
    }
    NSInteger gain = [gainString integerValue];
    NSUInteger gainLevel = [CZWisionCamera nearestValidGainLevelIndex:gain];
    return gainLevel + 1;
}

- (void)setGain:(NSUInteger)gain {
    if (gain - 1 >= kWisionGainLevelsCount) {
        return;
    }
    
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)kWisionGainLevels[gain - 1]];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"maxgain", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

#pragma mark - White Balance Mode

- (BOOL)isWhiteBalanceLocked {
    NSString *statusString = [self getParameterFromIniHtm:@"getWBLock"];
    if (!statusString) {
        return [super isWhiteBalanceLocked]; // Fallback to default implementation
    }
    NSInteger status = [statusString integerValue];
    return (status == 1);
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
    NSString *value = [[NSString alloc] initWithFormat:@"%u", (locked ? 1 : 0)];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"lockwb", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (void)beginWhiteBalanceAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self setIsWhiteBalanceLocked:NO];
        sleep(3); // Wait for the camera to finish auto white-balance.
        [self setIsWhiteBalanceLocked:YES];
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust:userInfo:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlDelegate cameraDidFinishWhiteBalanceAutoAdjust:self userInfo:nil];
            });
        }
    });
}

#pragma mark - Streaming Quality

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    CGFloat bitRateValue = 0.0;
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateValue = 6.0;
            break;
            
        case kCZCameraBitRateLow:
            bitRateValue = 1.5;
            break;
            
        case kCZCameraBitRateMedium:
        default:
            bitRateValue = 3.0;
            break;
    }
    return bitRateValue;
}

- (CZCameraBitRate)defaultBitRate {
    return kCZCameraBitRateMedium;
}

- (CZCameraBitRate)bitRate {
    NSString *bitRateString = [self getParameterFromIniHtm:@"bitrate2"];
    
    if (!bitRateString) {
        return [super bitRate]; // Fallback to default implementation
    }
    
    NSInteger bitRateInMegabytes = [bitRateString integerValue];
    if (bitRateInMegabytes >= 4608) { // >= 4.5 Mbps
        return kCZCameraBitRateHigh;
    } else if (bitRateInMegabytes < 2048) { // < 2 Mbps
        return kCZCameraBitRateLow;
    }
    return kCZCameraBitRateMedium;
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
    if (bitRate == [self bitRate]) {
        return;
    }
    
    NSInteger bitRateInMegabytes = 3072; // 3 Mbps
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateInMegabytes = 6144; // 6 Mbps
            break;
            
        case kCZCameraBitRateLow:
            bitRateInMegabytes = 1536; // 1.5 Mbps
            break;
            
        default:
            break;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)bitRateInMegabytes];
    [params setObject:value forKey:@"bitrate2"];
    [value release];
    
    [self setParameters:params];
    [params release];
}

#pragma mark - Image Orientation

- (CZCameraImageOrientation)defaultImageOrientation {
    return kCZCameraImageOrientationOriginal;
}

- (CZCameraImageOrientation)imageOrientation {
    NSString *orientationString = [self getParameterFromIniHtm:@"mirctrl"];
    
    if (!orientationString) {
        return [super imageOrientation]; // Fallback to default implementation
    }
    
    return [orientationString integerValue];
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)orientation];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"mirctrl", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

#pragma mark - Snap Resolution

- (CGSize)lowSnapResolution {
    return CGSizeMake(2048, 1536);
}

- (CGSize)highSnapResolution {
    return CGSizeMake(2048, 1536);
}

#pragma mark - Sharpening

- (uint32_t)defaultSharpness {
    return 0;
}

- (uint32_t)sharpnessStepCount {
    return 10; // 0-9
}

- (uint32_t)sharpness {
    NSString *sharpnessString = [self getParameterFromIniHtm:@"sharpness"];
    if (!sharpnessString) {
        return [super sharpness];
    }
    
    uint32_t sharpness = [sharpnessString intValue];
    uint32_t sharpnessStep = round(sharpness / 25.5);
    uint32_t maxStep = [self sharpnessStepCount] - 1;
    
    return MIN(maxStep, MAX(0, sharpnessStep));
}

- (void)setSharpness:(uint32_t)sharpness {
    if (sharpness >= [self sharpnessStepCount]) {
        return;
    }
    
    sharpness = round(25.5 * sharpness); // Convert steps to real value in range [0, 255]
    NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)sharpness];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"sharpness", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

#pragma mark - Grayscale Mode

- (BOOL)isGrayscaleModeEnabledByDefault {
    return NO;
}

- (BOOL)isGrayscaleModeEnabled {
    NSString *grayscaleString = [self getParameterFromIniHtm:@"irdaynigmode"];
    if (!grayscaleString) {
        return [super isGrayscaleModeEnabled]; // Fallback to default implementation
    }
    NSInteger grayscaleValue = [grayscaleString integerValue];
    return (grayscaleValue == 1);
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
    NSString *value = enabled ? @"1" : @"0";
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"setirdaynight", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

#pragma mark - IFrame Interval

- (CZCameraIFrameRatio)IFrameRatio {
    NSString *intervalString = [self getParameterFromIniHtm:@"ipratio2"];
    int32_t interval = [intervalString intValue];
    return (interval >= 30) ? kCZCameraIFrameRatioHigh : kCZCameraIFrameRatioLow;
}

- (void)setIFrameRatio:(CZCameraIFrameRatio)iFrameRatio {
    uint32_t iFrameInterval;
    switch (iFrameRatio) {
        case kCZCameraIFrameRatioLow:
            iFrameInterval = 3;
            break;
        case kCZCameraIFrameRatioHigh:
            iFrameInterval = 30;
    }
    
    NSString *value = [[NSString alloc] initWithFormat:@"%u", iFrameInterval];
    [self setParameters:@{@"iframeinterval": value}];
    [value release];
}

#pragma mark - Private methods

- (NSString *)getParameter:(NSString *)param fromPath:(NSString *)urlPath {
    NSString *value = nil;
    
    if (param == nil || [param length] == 0) {
        return value;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/", self.ipAddress];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    if (!baseUrl) {
        [baseUrlString release];
        return nil;
    }
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setTimeoutInterval:10]; // Shorter timeout for GET requests
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    if (result.error) {
        CZLogv(@"Failed to send the request to get parameters from Wision camera: %@", [result.error localizedDescription]);
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:result.data encoding:NSUTF8StringEncoding];
        if (body == nil) {
            body = [[NSMutableString alloc] initWithData:result.data encoding:NSASCIIStringEncoding];
        }
        
        if (body) {
            [body replaceOccurrencesOfString:@"<br>" withString:@"\n" options:0 range:NSMakeRange(0, [body length])];
            
            NSString *searchPattern = [[NSString alloc] initWithFormat:@"%@=(.*)", param];
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:searchPattern options:0 error:NULL];
            NSTextCheckingResult *match = [regex firstMatchInString:body options:0 range:NSMakeRange(0, [body length])];
            
            value = [body substringWithRange:[match rangeAtIndex:1]];
            
            CZLogv(@"Got parameter from %@ of Wision camera: %@ = %@", urlPath, param, value);
            
            [searchPattern release];
            [body release];
        }
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return value;
}

- (NSString *)getParameterFromIniHtm:(NSString *)param {
    NSString *value = nil;
    
    if (param == nil || [param length] == 0) {
        return value;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/", self.ipAddress];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    if (baseUrl == nil) {
        [baseUrlString release];
        return nil;
    }
    NSString *urlPath = @"ini.htm";
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:10]; // Shorter timeout for GET requests
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    if (result.error) {
        CZLogv(@"Failed to send the request to get parameters from Wision camera: %@", [result.error localizedDescription]);
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:result.data encoding:NSUTF8StringEncoding];
        if (body == nil) {
            body = [[NSMutableString alloc] initWithData:result.data encoding:NSASCIIStringEncoding];
        }
        
        if (body) {
            NSArray *lines = [body componentsSeparatedByString:@"<br>"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            for (NSString *line in lines) {
                NSArray *components = [line componentsSeparatedByString:@"="];
                if (components.count == 2) {
                    dict[components[0]] = components[1];
                }
            }
            
            value = dict[param];
            
            CZLogv(@"Got parameter from %@ of Wision camera: %@ = %@", urlPath, param, value);
            
            [dict release];
            [body release];
        }
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return value;
}

- (NSString *)getParameterFromVbHtm:(NSString *)param {
    NSString *urlPath = [NSString stringWithFormat:@"vb.htm?%@", param];
    return [self getParameter:param fromPath:urlPath];
}

- (void)setParameters:(NSDictionary *)params {
    if (params == nil || [params count] == 0) {
        return;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return;
    }
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/vb.htm?%@",
                           self.ipAddress,
                           [CZWisionCamera urlEncodedStringWithParameters:params]];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setTimeoutInterval:kWisionDefaultTimeout];
    [url release];
    [urlString release];
    
    void (^successBlock)(AFHTTPRequestOperation *, id) =
    ^(AFHTTPRequestOperation *operation, id responseObject) {
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *, NSError *) =
    ^(AFHTTPRequestOperation *operation, NSError *error) {
        CZLogv(@"Failed to send the request to set parameters onto Wision camera: %@", [error localizedDescription]);
    };
    
    AFHTTPRequestOperation *operation;
    operation = [[[AFHTTPRequestOperation alloc] initWithRequest:request] autorelease];
    [operation setCompletionBlockWithSuccess:successBlock failure:failureBlock];
    [operation start];
}

- (void)beginSnappingJPEGImage {
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://%@:8080/ipcam/dms.cgi", self.ipAddress];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setTimeoutInterval:kWisionDefaultTimeout];
    [url release];
    [urlString release];
    
    id delegate = self.delegate;
    
    UIImage *(^imageProcessingBlock)(UIImage *) = ^UIImage *(UIImage *image) {
        // Since the snapped image from Wision camera is JPEG compressed,
        // convert to raw bitmap in memory to get better performance during
        // post-processing in the app.
        return [[CZCommonUtils newDecompressedImageFromJPEGImage:image] autorelease];
    };
    
    void (^successBlock)(NSURLRequest *, NSHTTPURLResponse *, UIImage *) =
    ^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        float exposure = [self exposureTime];
        NSUInteger gain = [self gain];
        BOOL grayscale = [self isGrayscaleModeEnabled];
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                snappingInfo.image = image;
                snappingInfo.grayscale = grayscale;
                snappingInfo.exposureTimeInMilliseconds = @(exposure);
                snappingInfo.gain = @(gain);
                [delegate camera:self didFinishSnapping:snappingInfo error:nil];
                [snappingInfo release];
            }
        });
    };
    
    void (^failureBlock)(NSURLRequest *, NSHTTPURLResponse *, NSError *) =
    ^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        CZLogv(@"Failed to snap: %@", [error localizedDescription]);
        
        NSError *timeoutError = [NSError errorWithDomain:CZCameraErrorDomain
                                                    code:kCZCameraTimeoutError
                                                userInfo:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                [delegate camera:self didFinishSnapping:nil error:timeoutError];
            }
        });
    };
    
    AFImageRequestOperation *operation;
    operation = [AFImageRequestOperation imageRequestOperationWithRequest:request
                                                     imageProcessingBlock:imageProcessingBlock
                                                                  success:successBlock
                                                                  failure:failureBlock];
    [operation setImageScale:1.0];
    [operation start];
}

- (void)beginSnappingRAWImage:(NSUInteger)frameCount {
    // Check whether needs to use extra long exposure time for snapping.
    float snappingExposure = [self snappingExposureTime];
    if (snappingExposure > 0) {
        self.originalExposureTime = @([self exposureTime]);
        
        NSUInteger index;
        if (snappingExposure < 1000.0f) {
            index = 24; // 500 ms
            self.actualExposureTime = @500.0f;
        } else if (snappingExposure < 2000.0f) {
            index = 25; // 1000 ms
            self.actualExposureTime = @1000.0f;
        } else {
            index = 26; // 2000 ms
            self.actualExposureTime = @2000.0f;
        }
        
        NSString *value = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)index];
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                value, @"fixedexptime",
                                @"1", @"setexpmod",
                                nil];
        [self setParameters:params];
        [value release];
        [params release];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        // Wait for a while so that extra long exposure time can be applied.
        if (self.originalExposureTime != nil) {
            usleep((snappingExposure / 1000.0f + 0.5) * USEC_PER_SEC);
        }
        
        [_imageTuneClient beginSnappingRawImageFromCamera:self.ipAddress
                                               frameCount:frameCount];
    });
}

+ (NSString *)urlEncodedStringFromObject:(id)object {
    NSString *string = [NSString stringWithFormat:@"%@", object];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

+ (NSString *)urlEncodedStringWithParameters:(NSDictionary *)params {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (id key in params) {
        id value = [params objectForKey:key];
        NSString *keyEncoded = [CZWisionCamera urlEncodedStringFromObject:key];
        
        if ([value isKindOfClass:[NSString class]]) {
            NSString *valueEncoded = [CZWisionCamera urlEncodedStringFromObject:value];
            NSString *part = [NSString stringWithFormat:@"%@=%@", keyEncoded, valueEncoded];
            [parts addObject:part];
        } else if ([value isKindOfClass:[NSNull class]]) {
            [parts addObject:keyEncoded];
        }
    }
    
    NSString *result = [parts componentsJoinedByString:@"&"];
    [parts release];
    
    return result;
}

+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain {
    NSInteger leftIndex = -1;
    for (NSUInteger i = 0; i < kWisionGainLevelsCount; ++i) {
        if (kWisionGainLevels[i] < gain) {
            leftIndex = i;
        } else {
            break;
        }
    }
    
    if (leftIndex == -1) {
        return 0;
    } else if (leftIndex == kWisionGainLevelsCount - 1) {
        return leftIndex;
    }
    
    CGFloat leftValue = kWisionGainLevels[leftIndex];
    CGFloat rightValue = kWisionGainLevels[leftIndex + 1];
    CGFloat average = (leftValue + rightValue) / 2;
    return (gain < average) ? leftIndex : leftIndex + 1;
}

#pragma mark - CZWisionImageTuneClientDelegate methods

- (void)imageTuneClient:(CZWisionImageTuneClient *)client didGetRawImage:(UIImage *)image {
    float exposure;
    if (self.actualExposureTime != nil) {
        exposure = [self.actualExposureTime floatValue];
        
        [self setExposureTime:self.originalExposureTime.floatValue];
        
        self.actualExposureTime = nil;
        self.originalExposureTime = nil;
    } else {
        exposure = [self exposureTime];
    }
    
    NSUInteger gain = [self gain];
    
    BOOL grayscale = NO;
    if (image) {
        grayscale = [self isGrayscaleModeEnabled];
    }
    
    id delegate = self.delegate;
    dispatch_async(dispatch_get_main_queue(), ^() {
        @autoreleasepool {
            NSError *error = nil;
            if (image == nil) {
                error = [NSError errorWithDomain:CZCameraErrorDomain
                                            code:kCZCameraTimeoutError
                                        userInfo:nil];
            }
            
            if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                snappingInfo.image = image;
                snappingInfo.grayscale = grayscale;
                snappingInfo.exposureTimeInMilliseconds = @(exposure);
                snappingInfo.gain = @(gain);
                [delegate camera:self didFinishSnapping:snappingInfo error:error];
                [snappingInfo release];
            }
        }
    });
}

@end
