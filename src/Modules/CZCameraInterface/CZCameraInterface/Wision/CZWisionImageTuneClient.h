//
//  CZWisionImageTuneClient.h
//  Hermes
//
//  Created by Halley Gu on 5/20/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CocoaAsyncSocket/GCDAsyncSocket.h>

@class CZWisionImageTuneClient;

@protocol CZWisionImageTuneClientDelegate <NSObject>

- (void)imageTuneClient:(CZWisionImageTuneClient *)client didGetRawImage:(UIImage *)image;

@end

@interface CZWisionImageTuneClient : NSObject<GCDAsyncSocketDelegate>

@property (nonatomic, assign) id<CZWisionImageTuneClientDelegate> delegate;

/**
 * Asynchronously retrieve the raw YUV420 image data from camera, and return
 * as a UIImage object.
 *
 * \param frameCount if frameCount > 1 then it's continous snapping.
 * \warning Not thread-safe, only call this method from main thread.
 */
- (void)beginSnappingRawImageFromCamera:(NSString *)cameraIPAddress
                             frameCount:(NSUInteger)frameCount;

@end
