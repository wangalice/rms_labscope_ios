//
//  CZCameraInformation.m
//  Hermes
//
//  Created by Ding Yu on 12/12/2017.
//  Copyright © 2017 Carl Zeiss. All rights reserved.
//

#import "CZCameraInformation.h"

@implementation CZCameraInformation

- (instancetype) initWithName:(NSString *)name
                    ipAddress:(NSString *)ipAddress
                   macAddress:(NSString *)macAddress
                  connectable:(BOOL)connectable{
    
    self = [super init];
    if(self){
        self.cameraName = name;
        self.ipAddress = ipAddress;
        self.macAddress = macAddress;
        self.connectable = connectable;
    }
    
    return self;
}

- (void) dealloc{
    
    [self.cameraName release];
    [self.ipAddress release];
    [self.macAddress release];
    [super dealloc];
}

@end
