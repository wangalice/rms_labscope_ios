//
//  CZDefaultSettings+FluorescenceSnapping.m
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+FluorescenceSnapping.h"
#import "CZCamera.h"
#import "CZFluorescenceSnapping.h"

@implementation CZDefaultSettings (FluorescenceSnapping)

- (CZFluorescenceSnappingTemplate *)fluorescenceSnappingTemplateForCamera:(CZCamera *)camera {
    NSString *key = [NSString stringWithFormat:kFluorescenceSnappingTemplateKey, camera.macAddress.uppercaseString];
    NSData *data = [[NSUserDefaults standardUserDefaults] dataForKey:key];
    CZFluorescenceSnappingTemplate *flTemplate = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return flTemplate;
}

- (void)setFluorescenceSnapTemplate:(CZFluorescenceSnappingTemplate *)flTemplate forCamera:(CZCamera *)camera {
    NSString *key = [NSString stringWithFormat:kFluorescenceSnappingTemplateKey, camera.macAddress.uppercaseString];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:flTemplate];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
}

@end
