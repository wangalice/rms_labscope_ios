//
//  CZCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCamera.h"
#import "CZCameraSubclass.h"
#import <CZToolbox/CZToolbox.h>
#import <CZVideoEngine/CZVideoEngine.h>

NSString * const CZCameraErrorDomain = @"CameraSnappingError";

NSString * const CZCameraStreamingStartNotification = @"CameraStreamingStartNotification";
NSString * const CZCameraStreamingStopNotification = @"CameraStreamingStopNotification";

NSString * const CZCameraExposureTimeKey = @"CZCameraExposureTimeKey";
NSString * const CZCameraGainKey = @"CZCameraGainKey";
NSString * const CZCameraWhiteBalanceValueKey = @"CZCameraWhiteBalanceValueKey";

@implementation CZCameraLightSource

- (instancetype)initWithPosition:(NSUInteger)position wavelength:(NSUInteger)wavelength isOn:(BOOL)isOn canOn:(BOOL)canOn {
    self = [super init];
    if (self) {
        _position = position;
        _wavelength = wavelength;
        _isOn = isOn;
        _canOn = canOn;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] alloc] initWithPosition:self.position wavelength:self.wavelength isOn:self.isOn canOn:self.canOn];
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.position forKey:@"position"];
    [aCoder encodeInteger:self.wavelength forKey:@"wavelength"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _position = [aDecoder decodeIntegerForKey:@"position"];
        _wavelength = [aDecoder decodeIntegerForKey:@"wavelength"];
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZCameraLightSource class]]) {
        return NO;
    } else {
        return [self isEqualToLightSource:object];
    }
}

- (BOOL)isEqualToLightSource:(CZCameraLightSource *)otherLightSource {
    return self.position == otherLightSource.position && self.wavelength == otherLightSource.wavelength;
}

@end

@implementation CZCameraSnappingInfo

- (instancetype)snappingInfoByTransformingImageToImage:(UIImage *)image {
    CZCameraSnappingInfo *snappingInfo = [self copy];
    snappingInfo.image = image;
    return [snappingInfo autorelease];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _currentSnappingNumber = 1;
        _totalSnappingNumber = 1;
    }
    return self;
}

- (void)dealloc {
    [_image release];
    [_gain release];
    [_lightSource release];
    [_filterSetID release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZCameraSnappingInfo *snappingInfo = [[[self class] alloc] init];
    snappingInfo.currentSnappingNumber = self.currentSnappingNumber;
    snappingInfo.totalSnappingNumber = self.totalSnappingNumber;
    snappingInfo.image = self.image;
    snappingInfo.grayscale = self.grayscale;
    snappingInfo.exposureMode = self.exposureMode;
    snappingInfo.exposureTimeInMilliseconds = self.exposureTimeInMilliseconds;
    snappingInfo.gain = self.gain;
    snappingInfo.exposureIntensity = self.exposureIntensity;
    snappingInfo.lightSource = self.lightSource;
    snappingInfo.lightSourceType = self.lightSourceType;
    snappingInfo.lightIntensity = self.lightIntensity;
    snappingInfo.filterSetID = self.filterSetID;
    return snappingInfo;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.exposureMode forKey:@"exposureMode"];
    [aCoder encodeObject:self.exposureTimeInMilliseconds forKey:@"exposureTime"];
    [aCoder encodeObject:self.gain forKey:@"gain"];
    [aCoder encodeInteger:self.exposureIntensity forKey:@"exposureIntensity"];
    [aCoder encodeObject:self.lightSource forKey:@"lightSource"];
    [aCoder encodeInteger:self.lightSourceType forKey:@"lightSourceType"];
    [aCoder encodeInteger:self.lightIntensity forKey:@"lightIntensity"];
    [aCoder encodeObject:self.filterSetID forKey:@"filterSetID"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.exposureMode = [aDecoder decodeIntegerForKey:@"exposureMode"];
        self.exposureTimeInMilliseconds = [aDecoder decodeObjectOfClass:[NSNumber class] forKey:@"exposureTime"];
        self.gain = [aDecoder decodeObjectOfClass:[NSNumber class] forKey:@"gain"];
        self.exposureIntensity = [aDecoder decodeIntegerForKey:@"exposureIntensity"];
        self.lightSource = [aDecoder decodeObjectOfClass:[CZCameraLightSource class] forKey:@"lightSource"];
        self.lightSourceType = [aDecoder decodeIntegerForKey:@"lightSourceType"];
        self.lightIntensity = [aDecoder decodeIntegerForKey:@"lightIntensity"];
        self.filterSetID = [aDecoder decodeObjectOfClass:[NSString class] forKey:@"filterSetID"];
    }
    return self;
}

- (void)updateImage:(UIImage *)image {
    self.image = image;
}

@end

@interface CZCamera () {
    NSString *_displayName;
    NSString *_pin;
    NSObject *_localCameraSettingsLock;
    NSObject *_nameLock;
}

+ (NSArray *)emptyArray;

- (void)updateName;
- (NSString *)mockDisplayName;

- (id)readLocalCameraSettingForKey:(NSString *)key;
- (void)writeLocalCameraSetting:(id)setting forKey:(NSString *)key;

@end

@implementation CZCamera (CZCameraProtected) 

@dynamic timelapseRecorder;

#pragma mark - CZTimelapseRecorderDelegate

- (void)recorderDidEndRecording:(CZTimelapseRecorder *)recorder {
    if ([self.delegate respondsToSelector:@selector(cameraDidFinishRecording:)]) {
        [self.delegate cameraDidFinishRecording:self];
    }
}

@end

@interface CZCamera ()

@property (atomic, retain, readwrite) CZTimelapseRecorder *timelapseRecorder;

@end

@interface CZCameraControlLock ()

- (void)resetCamera;

@end

@implementation CZCamera

- (id)init {
    self = [super init];
    if (self) {
        _lastActiveTime = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
        _localCameraSettingsLock = [[NSObject alloc] init];
        _nameLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    _timelapseRecorder.delegate = nil;
    [_timelapseRecorder endRecording];
    [_timelapseRecorder release];
    
    [_exclusiveLock resetCamera];
    [_exclusiveLock release];
    
    [_displayName release];
    [_pin release];
    [_ipAddress release];
    [_macAddress release];
    [_lastActiveTime release];
    [_lastFrameTime release];
    [_localCameraSettingsLock release];
    [_nameLock release];
    
    [super dealloc];
}

#pragma mark - Default implementations

- (BOOL)isEqual:(CZCamera *)otherCamera {
    if (self.macAddress || self.macAddress.length <= 0 ||
        otherCamera.macAddress || otherCamera.macAddress.length <= 0) {
        return NO;
    }
    return [self.macAddress isEqualToString:otherCamera.macAddress];
}

- (CZCameraCapability)capabilities {
    return CZCameraCapabilityNone;
}

- (BOOL)hasCapability:(CZCameraCapability)capability {
    return ((capability & [self capabilities]) == capability);
}

- (NSString *)firmwareVersion {
    return @"";
}

- (BOOL)isFirmwareUpdated {
    return YES;
}

- (void)updateFirmware {
}

- (BOOL)isFirmwareCompatibleForSnapping:(NSError **)error {
    return YES;
}

- (NSString *)displayName {
    @synchronized (_nameLock) {
        if (_displayName && _displayName.length > 0) {
            return [[_displayName copy] autorelease];
        } else {
            return [self mockDisplayName];
        }
    }
}

- (NSString *)mockDisplayName {
    // Get last 3 segments of MAC address as display name.
    return [self.macAddress substringFromIndex:9];
}

- (void)setDisplayName:(NSString *)displayName andUpload:(BOOL)shouldUpload {
    if (displayName == nil || [displayName isEqualToString:[self mockDisplayName]]) {
        return;
    }
    
    @synchronized (_nameLock) {
        [_displayName release];
        _displayName = [displayName copy];
        
        if (shouldUpload) {
            [self updateName];
        }
    }
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    @synchronized (_nameLock) {
        NSString *newName = [name copy];
        [_pin release];
        _pin = nil;
        
        [_displayName release];
        _displayName = nil;
        
        // Ignore name which equals to the default name (second-half of MAC address)
        if ([newName isEqualToString:[self mockDisplayName]]) {
            [newName release];
            return;
        }
        
        // Scan for all the decimal digit characters as the PIN
        NSString *pinPart = nil;
        NSScanner *scanner = [[NSScanner alloc] initWithString:newName];
        BOOL hasPin = [scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet] intoString:&pinPart];
        if (hasPin) {
            _pin = [pinPart copy];
        }
        
        // The left part is the actual display name
        NSString *displayName = [newName substringFromIndex:[scanner scanLocation]];
        displayName = [displayName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        _displayName = [displayName copy];
        
        [scanner release];
        [newName release];
    }
}

- (NSUInteger)maxSupportedNameLength {
    return 12;
}

- (UIImage *)thumbnail {
    return nil;
}

- (UIImage *)liveThumbnail {
    return nil;
}

- (void)setLiveThumbnail:(UIImage *)liveThumbnail {
}

- (void)beginThumbnailUpdating:(CGSize)thumbnailSize {
}

- (BOOL)prefetchRequiredCameraInfo {
    return YES;
}

- (void)play {
}

- (void)stop {
}

- (void)pause {
}

- (void)resume {
}

- (BOOL)isPaused {
    return NO;
}

- (void)startRelayToURL:(NSURL *)relayURL {
}

- (void)endRelay {
}

- (void)beginSnapping {
    [self beginContinuousSnapping:1];
}

- (void)beginContinuousSnapping:(NSUInteger)frameCount {
    [self beginContinuousSnappingFromVideoStream:frameCount];
}

- (void)beginSnappingFromVideoStream {
    [self beginContinuousSnappingFromVideoStream:1];
}

- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount {
    CZLogv(@"Camera %@ doesn't support continuous snapping.", self);
    
    if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
        [self.delegate camera:self didFinishSnapping:nil error:nil];
    }
}

- (void)beginRecordingToFile:(NSString *)filePath {
}

- (void)endRecording {
}

- (void)beginTimelapseRecordingToFile:(NSString *)filePath
                          inFrameRate:(NSUInteger)frameRate
                             interval:(NSTimeInterval)interval
                            overlayer:(CALayer *)overlayer {
    // sub-class need override this method
}

- (void)endTimelapseRecording {
    [self.timelapseRecorder endRecording];
    self.timelapseRecorder = nil;
}

- (void)handoverTimelapseRecorderFromCamera:(CZCamera *)camera {
    self.timelapseRecorder = camera.timelapseRecorder;
    self.timelapseRecorder.delegate = self;
    camera.timelapseRecorder = nil;
}

- (CGSize)liveResolution {
    return CGSizeZero;
}

- (BOOL)getControl:(CZCameraControlType)type {
    return YES;
}

- (BOOL)releaseControl:(CZCameraControlType)type {
    return YES;
}

- (void)beginEventListening {
}

- (void)endEventListening {
}

- (void)beginReset {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self performReset];
        
        sleep(3); // Wait for the camera to finish resetting.
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishReset:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlDelegate cameraDidFinishReset:self];
            });
        }
    });
}

- (void)performReset {
    NSUInteger exposureIntensity = [self defaultExposureIntensity];
    [self setExposureIntensity:exposureIntensity];
    
    [self setExposureMode:kCZCameraExposureAutomatic];
    [self setIsWhiteBalanceLocked:NO];
    
    CZCameraBitRate bitRate = [self defaultBitRate];
    CZCameraImageOrientation imageOrientation = [self defaultImageOrientation];
    CZCameraSnapResolutionPreset snapResolutionPreset = [self defaultSnapResolutionPreset];
    uint32_t sharpness = [self defaultSharpness];
    BOOL isDenoiseEnabled = [self isDenoiseEnabledByDefault];
    BOOL isPixelCorrectionEnabled = [self isPixelCorrectionEnabledByDefault];
    BOOL isHDREnabled = [self isHDREnabledByDefault];
    CZCameraBitDepth bitDepth = [self defaultBitDepth];
    BOOL isGrayscaleModeEnabled = [self isGrayscaleModeEnabledByDefault];
    float gamma = [self defaultGamma];
    
    [self setBitRate:bitRate];
    [self setImageOrientation:imageOrientation];
    [self setSnapResolutionPreset:snapResolutionPreset];
    [self setSharpness:sharpness];
    [self setDenoiseEnabled:isDenoiseEnabled];
    [self setPixelCorrectionEnabled:isPixelCorrectionEnabled];
    [self setHDREnabled:isHDREnabled];
    [self setBitDepth:bitDepth];
    [self setGrayscaleModeEnabled:isGrayscaleModeEnabled];
    [self setGamma:gamma];
}

- (NSString *)pin {
    @synchronized (_nameLock) {
        return [[_pin copy] autorelease];
    }
}

- (BOOL)hasPIN {
    @synchronized (_nameLock) {
        if (_pin && _pin.length > 0) {
            return YES;
        }
        return NO;
    }
}

- (BOOL)validatePIN:(NSString *)pin {
    @synchronized (_nameLock) {
        if (_pin == nil) {
            if (pin == nil || pin.length == 0) {
                return YES;
            } else {
                return NO;
            }
        } else {
            BOOL result = [_pin isEqualToString:pin];
            
            // Implement fallback for PIN here - use a hardcoded master pin instead from v2.5
            if (!result) {
//                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//                NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth
//                                                           fromDate:[NSDate date]];
                
//                NSString *masterText = [[NSString alloc] initWithFormat:@"Hermes-Master-PIN-%04li-%02li",
//                                        (long)components.year,
//                                        (long)components.month];
//                NSString *masterPin = [[CZCommonUtils md5FromString:masterText] substringToIndex:12];
//                
//                result = [[pin lowercaseString] isEqualToString:[masterPin lowercaseString]];
//                
//                [masterText release];
//                [calendar release];
                
                NSString *masterText = @"h!%jPYtt34";
                NSString *masterPin = [CZCommonUtils md5FromString:masterText];
                
                result = [[CZCommonUtils md5FromString:pin] isEqualToString:masterPin];
            }
            
            return result;
        }
    }
}

- (void)setPIN:(NSString *)pin andUpload:(BOOL)shouldUpload {
    @synchronized (_nameLock) {
        [_pin release];
        _pin = [pin copy];
        
        if (shouldUpload) {
            [self updateName];
        }
    }
}

- (void)restart {
}

#pragma mark - Private methods

+ (NSArray *)emptyArray {
    static NSArray *emptyArray = nil;
    if (emptyArray == nil) {
        emptyArray = [NSArray array];
    }
    return emptyArray;
}

- (void)updateName {
    @synchronized (_nameLock) {
        if (_pin == nil || _pin.length == 0) {
            [self setName:_displayName andUpload:YES];
        } else {
            NSString *name = nil;
            if (_displayName.length) {
                name = [_pin stringByAppendingString:_displayName];
            } else {
                name = _pin;
            }
            [self setName:name andUpload:YES];
        }
    }
}

- (id)readLocalCameraSettingForKey:(NSString *)key {
    @synchronized (_localCameraSettingsLock) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *allCameraSettings = [defaults valueForKey:@"local_camera_settings"];
        NSDictionary *cameraSettings = [allCameraSettings g_valueWithInsensitiveKeys:self.macAddress];//allCameraSettings[self.macAddress];
        return cameraSettings[key];
    }
}

- (void)writeLocalCameraSetting:(id)setting forKey:(NSString *)key {
    @synchronized (_localCameraSettingsLock) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *allCameraSettings = [[defaults valueForKey:@"local_camera_settings"] mutableCopy];
        NSMutableDictionary *cameraSettings = [[allCameraSettings g_valueWithInsensitiveKeys:self.macAddress] mutableCopy];//[allCameraSettings[self.macAddress] mutableCopy];
        
        if (allCameraSettings == nil) {
            allCameraSettings = [[NSMutableDictionary alloc] init];
        }
        if (cameraSettings == nil) {
            cameraSettings = [[NSMutableDictionary alloc] init];
        }
        
        cameraSettings[key] = setting;

        //remove the values stored in the dictionary of the local_camera_settings;
        [allCameraSettings removeObjectForKey:self.macAddress];
        [allCameraSettings removeObjectForKey:[self.macAddress uppercaseString]];
        [allCameraSettings removeObjectForKey:[self.macAddress lowercaseString]];
        
        allCameraSettings[[self.macAddress uppercaseString]] = cameraSettings;
        
        [defaults setValue:allCameraSettings forKey:@"local_camera_settings"];
        
        [cameraSettings release];
        [allCameraSettings release];
    }
}

@end

@implementation CZCamera (ExposureMode)

- (CZCameraExposureMode)exposureMode {
    return kCZCameraExposureAutomatic;
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
}

- (void)beginExposureTimeAutoAdjust {
    if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishExposureTimeAutoAdjust:)]) {
        [self.controlDelegate cameraDidFinishExposureTimeAutoAdjust:self];
    }
}

- (void)beginSwitchToManualExposureMode {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self setExposureMode:kCZCameraExposureManual];
        
        sleep(1);
        
        float exposureTime = [self exposureTime];
        NSUInteger gain = [self gain];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.controlDelegate && [self.controlDelegate respondsToSelector:@selector(cameraDidFinishSwitchToManualExposureMode:userInfo:)]) {
                NSDictionary *userInfo = @{CZCameraExposureTimeKey : @(exposureTime), CZCameraGainKey : @(gain)};
                [self.controlDelegate cameraDidFinishSwitchToManualExposureMode:self userInfo:userInfo];
            }
        });
    });
}

@end

@implementation CZCamera (ExposureTime)

- (float)minExposureTime {
    return [[[self supportedExposureTimeValues] firstObject] floatValue];
}

- (float)maxExposureTime {
    return [[[self supportedExposureTimeValues] lastObject] floatValue];
}

- (NSArray<NSNumber *> *)supportedExposureTimeValues {
    return @[];
}

- (float)nearestValidExposureTime:(float)milliseconds {
    NSArray *supportedValues = [self supportedExposureTimeValues];
    NSUInteger index = [self nearestValidExposureTimeIndex:milliseconds];
    if (index < supportedValues.count) {
        NSNumber *value = supportedValues[index];
        return value.floatValue;
    }
    return milliseconds;
}

- (NSUInteger)nearestValidExposureTimeIndex:(float)milliseconds {
    NSInteger leftIndex = NSNotFound;
    NSArray *supportedValues = [self supportedExposureTimeValues];
    for (NSUInteger i = 0; i < supportedValues.count; ++i) {
        NSNumber *value = supportedValues[i];
        if (value.floatValue < milliseconds) {
            leftIndex = i;
        } else {
            break;
        }
    }
    
    if (leftIndex == NSNotFound) {
        return 0;
    } else if (leftIndex == supportedValues.count - 1) {
        return leftIndex;
    }
    
    float leftValue = [supportedValues[leftIndex] floatValue];
    float rightValue = [supportedValues[leftIndex + 1] floatValue];
    float average = (leftValue + rightValue) / 2;
    return (milliseconds < average) ? leftIndex : leftIndex + 1;
}

- (float)exposureTime {
    return 0.0f;
}

- (void)setExposureTime:(float)milliseconds {
}

- (float)snappingExposureTime {
    if ([self hasCapability:CZCameraCapabilityExtraExposureTime] &&
        [self exposureMode] == kCZCameraExposureManual) {
        NSNumber *value = [self readLocalCameraSettingForKey:@"snap_exposure_time"];
        if (value) {
            float exposure = [value floatValue];
            if (exposure > 2000.0f) {
                return 2000.0f;
            } else if (exposure > 200.0f) {
                return exposure;
            }
        }
    }
    return 0.0f;
}

- (void)setSnappingExposureTime:(float)milliseconds {
    if ([self hasCapability:CZCameraCapabilityExtraExposureTime]) {
        float exposure = milliseconds;
        if (exposure <= 200.0f) {
            exposure = 0.0f;
        } else if (exposure > 2000.0f) {
            exposure = 2000.0f;
        }
        [self writeLocalCameraSetting:@(exposure) forKey:@"snap_exposure_time"];
    }
}

@end

@implementation CZCamera (Gain)

- (NSUInteger)minGain {
    return 1;
}

- (NSUInteger)maxGain {
    return 1;
}

- (NSUInteger)gain {
    return 1;
}

- (NSUInteger)nearestValidGain:(float)gain {
    return roundf(gain);
}

- (void)setGain:(NSUInteger)gain {
}

@end

@implementation CZCamera (ExposureIntensity)

- (NSUInteger)minExposureIntensity {
    return 0;
}

- (NSUInteger)maxExposureIntensity {
    return 0;
}

- (NSUInteger)defaultExposureIntensity {
    return 0;
}

- (NSUInteger)exposureIntensity {
    return 0;
}

- (void)setExposureIntensity:(NSUInteger)exposureIntensity {
}

@end

@implementation CZCamera (WhiteBalanceMode)

- (CZCameraWhiteBalanceMode)whiteBalanceMode {
    return CZCameraWhiteBalanceModeAutomatic;
}

- (void)setWhiteBalanceMode:(CZCameraWhiteBalanceMode)whiteBalanceMode {
}

- (BOOL)isWhiteBalanceLocked {
    return NO;
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
}

- (void)beginWhiteBalanceAutoAdjust {
    if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust: userInfo:)]) {
        [self.controlDelegate cameraDidFinishWhiteBalanceAutoAdjust:self userInfo:nil];
    }
}

@end

@implementation CZCamera (ColorTemperature)

- (NSUInteger)coldestWhiteBalanceValue {
    return 0;
}

- (NSUInteger)warmestWhiteBalanceValue {
    return 0;
}

- (NSUInteger)whiteBalanceValue {
    return 0;
}

- (NSUInteger)nearestValidWhiteBalanceValue:(float)value {
    return roundf(value);
}

- (void)setWhiteBalanceWithValue:(NSUInteger)value {
}

@end

@implementation CZCamera (LightIntensity)

- (BOOL)hasLightPath:(CZCameraLightPath)lightPath {
    return NO;
}

- (CZCameraLightPath)lightPath {
    return CZCameraLightPathUnknown;
}

- (CZCameraLightSourceType)reflectedLightSourceType {
    return CZCameraLightSourceTypeUnknown;
}

- (void)setLightPath:(CZCameraLightPath)lightPath {
}

- (NSUInteger)minLightIntensityAtPath:(CZCameraLightPath)lightPath {
    return 0;
}

- (NSUInteger)maxLightIntensityAtPath:(CZCameraLightPath)lightPath {
    return 0;
}

- (NSUInteger)lightIntensityAtPath:(CZCameraLightPath)lightPath {
    return 0;
}

- (void)setLightIntensity:(NSUInteger)lightIntensity atPath:(CZCameraLightPath)lightPath {
}

@end

@implementation CZCamera (StreamingQuality)

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    return 0.0;
}

- (CZCameraBitRate)defaultBitRate {
    return kCZCameraBitRateMedium;
}

- (CZCameraBitRate)bitRate {
    return kCZCameraBitRateMedium;
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
}

@end

@implementation CZCamera (ImageOrientation)

- (CZCameraImageOrientation)defaultImageOrientation {
    return kCZCameraImageOrientationOriginal;
}

- (CZCameraImageOrientation)imageOrientation {
    return kCZCameraImageOrientationOriginal;
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
}

@end

@implementation CZCamera (SnapResolution)

- (CGSize)lowSnapResolution {
    return CGSizeZero;
}

- (CGSize)highSnapResolution {
    return CGSizeZero;
}

- (CGSize)snapResolution {
    switch (self.snapResolutionPreset) {
        case kCZCameraSnapResolutionHigh:
            return [self highSnapResolution];
        case kCZCameraSnapResolutionLow:
            return [self lowSnapResolution];
        default:
            return CGSizeZero;
    }
}

- (CZCameraSnapResolutionPreset)defaultSnapResolutionPreset {
    return kCZCameraSnapResolutionHigh;
}

- (CZCameraSnapResolutionPreset)snapResolutionPreset {
    NSNumber *preset = [self readLocalCameraSettingForKey:@"snap_resolution_preset"];
    if (preset) {
        NSInteger presetValue = [preset integerValue];
        if (presetValue >= 0 && presetValue < kCZCameraSnapResolutionCount) {
            return (CZCameraSnapResolutionPreset)presetValue;
        }
    }
    return kCZCameraSnapResolutionHigh;
}

- (void)setSnapResolutionPreset:(CZCameraSnapResolutionPreset)preset {
    [self writeLocalCameraSetting:[NSNumber numberWithInteger:preset]
                           forKey:@"snap_resolution_preset"];
}

@end

@implementation CZCamera (Sharpening)

- (uint32_t)defaultSharpness {
    return 0;
}

- (uint32_t)sharpnessStepCount {
    return 0;
}

- (uint32_t)sharpness {
    return 0;
}

- (void)setSharpness:(uint32_t)sharpness {
}

@end

@implementation CZCamera (Denoise)

- (BOOL)isDenoiseEnabledByDefault {
    return NO;
}

- (BOOL)isDenoiseEnabled {
    return NO;
}

- (void)setDenoiseEnabled:(BOOL)enabled {
}

@end

@implementation CZCamera (PixelCorrection)

- (BOOL)isPixelCorrectionEnabledByDefault {
    return NO;
}

- (BOOL)isPixelCorrectionEnabled {
    return NO;
}

- (void)setPixelCorrectionEnabled:(BOOL)enabled {
}

@end

@implementation CZCamera (HDR)

- (BOOL)isHDREnabledByDefault {
    return NO;
}

- (BOOL)isHDREnabled {
    return NO;
}

- (void)setHDREnabled:(BOOL)enabled {
}

@end

@implementation CZCamera (BitDepth)

- (CZCameraBitDepth)defaultBitDepth {
    return CZCameraBitDepth8Bit;
}

- (CZCameraBitDepth)bitDepth {
    NSNumber *bitDepth = [self readLocalCameraSettingForKey:@"bit_depth"];
    if (bitDepth) {
        NSInteger bitDepthValue = bitDepth.unsignedIntegerValue;
        if (bitDepthValue >= 0 && bitDepthValue < CZCameraBitDepthCount) {
            return (CZCameraBitDepth)bitDepthValue;
        }
    }
    return CZCameraBitDepth8Bit;
}

- (void)setBitDepth:(CZCameraBitDepth)bitDepth {
    [self writeLocalCameraSetting:@(bitDepth) forKey:@"bit_depth"];
}

@end

@implementation CZCamera (GrayscaleMode)

- (BOOL)isGrayscaleModeEnabledByDefault {
    return NO;
}

- (BOOL)isGrayscaleModeEnabled {
    return NO;
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
}

@end

@implementation CZCamera (Gamma)

- (float)minGamma {
    return 0.0;
}

- (float)maxGamma {
    return 0.0;
}

- (float)defaultGamma {
    return 0.0;
}

- (float)gamma {
    return 0.0;
}

- (void)setGamma:(float)gamma {
}

@end

@implementation CZCamera (WebConfiguration)

- (NSURL *)webInterfaceURL {
    return nil;
}

@end

@implementation CZCamera (IFrameInterval)

- (CZCameraIFrameRatio)IFrameRatio {
    return kCZCameraIFrameRatioLow;
}

- (void)setIFrameRatio:(CZCameraIFrameRatio)iFrameRatio {
}

@end

@implementation CZCamera (ShadingCorrection)

- (BOOL)isShadingCorrectionApplied {
    return NO;
}

- (void)calibrateShadingCorrection {
}

- (void)enableShadingCorrection {
}

- (void)disableShadingCorrection {
}

@end

@implementation CZCameraControlLock

- (instancetype)init {
    return [self initWithType:CZCameraControlTypeSnap camera:nil retained:NO];
}

- (instancetype)initWithCamera:(CZCamera *)camera {
    return [self initWithType:CZCameraControlTypeSnap camera:camera retained:YES];
}

- (instancetype)initWithType:(CZCameraControlType)type camera:(CZCamera *)camera retained:(BOOL)retained {
    self = [super init];
    if (self) {
        BOOL lockSuccess = [camera getControl:type];
        if (lockSuccess) {
            _type = type;
            _retained = retained;
            if (_retained) {
                _camera = [camera retain];
            } else {
                _camera = camera;
            }
        } else {
            [self release];
            self = nil;
        }
    }
    
    return self;
}

- (void)resetCamera {
    [_camera releaseControl:_type];
    if (_retained) {
        [_camera release];
    }
    _camera = nil;
}

- (void)dealloc {
    [_camera releaseControl:_type];
    if (_retained) {
        [_camera release];
    }
    [super dealloc];
}

@end
