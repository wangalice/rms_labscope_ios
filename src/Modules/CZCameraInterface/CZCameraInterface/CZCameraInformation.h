//
//  CZCameraInformation.h
//  Hermes
//
//  Created by Ding Yu on 12/12/2017.
//  Copyright © 2017 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZCameraInformation : NSObject

- (instancetype) initWithName:(NSString *)name
                    ipAddress:(NSString *)ipAddress
                   macAddress:(NSString *)macAddress
                  connectable:(BOOL)connectable;

@property (nonatomic, readwrite, copy) NSString *macAddress;
@property (nonatomic, readwrite, copy) NSString *ipAddress;
@property (nonatomic, readwrite, copy) NSString *cameraName;
@property (nonatomic, readwrite, assign) BOOL connectable;

@end
