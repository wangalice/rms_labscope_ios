//
//  CZCameraBrowser.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCameraBrowser.h"
#include <arpa/inet.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import <CZToolbox/CZToolbox.h>
#import <CZVideoEngine/CZVideoEngine.h>
#import "CZCamera.h"
#import "CZGeminiCamera.h"
#import "CZGEVClient.h"
#import "CZGVCPDiscoveryAck.h"
#import "CZKappaCamera.h"
#import "CZLocalFileCamera.h"
#import "CZMoticCamera.h"
#import "CZWisionCamera.h"
#import "CZRemoteStreamCamera.h"
#import "CZPlusCamera.h"
#import "CZCameraInformation.h"
//IP Address
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>

#define kNumOfMockCameras 2

#if REC_STREAM_ENABLED
    #define kNumOfRemoteCameras 1
#else
    #define kNumOfRemoteCameras 0
#endif

#define kNumOfPlusCameras 1

NSString * const kCZCameraBrowserNotificationFound = @"CameraBrowserNotificationFound";
NSString * const kCZCameraBrowserNotificationLost = @"CameraBrowserNotificationLost";
NSString * const kCZCameraBrowserNotificationUpdate = @"CameraBrowserNotificationUpdate";
NSString * const kCZCameraBrowserNotificationRemoved = @"CameraBrowserNotificationRemove";
NSString * const kCZCameraBrowserNotificationReplace = @"CZCameraBrowserNotificationReplace";
NSString * const kCZCameraBrowserNotificationOldCamera = @"CZCameraBrowserNotificationOldCamera";
NSString * const kCZCameraBrowserNotificationNewCamera = @"CZCameraBrowserNotificationNewCamera";
NSString * const kCZCameraBrowserNotificationCameraList = @"CameraBrowserNotificationCameraList";

static const uint16_t kVendorMagicNumberZeiss = 0x1B46;
static const uint16_t kVendorMagicNumberWision = 0xAA55;
static const uint16_t kVendorMagicNumberMotic = 0x33BB;

// This is a time interval compares with "now", so it's negative.
static const NSTimeInterval kLostTime = -15.0;

@interface CZCameraBrowser () <CZGEVClientDelegate, GCDAsyncUdpSocketDelegate> {
    dispatch_group_t _browsingGroup;
    dispatch_queue_t _browsingQueue;
    NSObject *_browsingLock;
    NSObject *_cameraListLock;
    CZGEVClient *_gevClient;
    NSObject *_udpDiscoveryLock;
    dispatch_queue_t _udpDiscoveryQueue;
    GCDAsyncUdpSocket *_browsingSocket;
}

@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, retain) NSMutableDictionary<NSString *, CZCamera *> *cameraList;

@property (atomic, copy) NSString *pauseRefreshingCameraKey;
@property (atomic, retain) NSDate *pauseRefreshingExpireDate;

@end

@implementation CZCameraBrowser

static CZCameraBrowser *uniqueInstance = nil;

#pragma mark - Singleton Design Pattern

+ (CZCameraBrowser *)sharedInstance {
    @synchronized ([CZCameraBrowser class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (id)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

#pragma mark - Lifecycle management

- (id)init {
    self = [super init];
    if (self) {
        _cameraList = [[NSMutableDictionary alloc] init];
        
        _browsingGroup = dispatch_group_create();
        _browsingQueue = dispatch_queue_create("com.zeisscn.browsing", NULL);
        _browsingLock = [[NSObject alloc] init];
        _cameraListLock = [[NSObject alloc] init];
        
        _gevClient = [[CZGEVClient alloc] init];
        [_gevClient setDelegate:self];
        
        _udpDiscoveryQueue = dispatch_queue_create("com.zeisscn.discovery.udp", NULL);
        _udpDiscoveryLock = [[NSObject alloc] init];
        
        [self addObserver:self
               forKeyPath:@"thumbnailRefreshingEnabled"
                  options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                  context:NULL];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self
              forKeyPath:@"thumbnailRefreshingEnabled"
                 context:NULL];
    
    [_timer invalidate];
    [_timer release];
    
    dispatch_release(_udpDiscoveryQueue);
    dispatch_release(_browsingQueue);
    dispatch_release(_browsingGroup);
    
    [_browsingLock release];
    [_cameraListLock release];
    [_cameraList release];
    [_gevClient release];
    [_udpDiscoveryLock release];
    
    _browsingSocket.delegate = nil;
    [_browsingSocket close];
    [_browsingSocket release];
    
    [_pauseRefreshingCameraKey release];
    [_pauseRefreshingExpireDate release];
    
    [super dealloc];
}

#pragma mark - Public interfaces

- (NSArray *)cameras {
    @synchronized (_cameraListLock) {
        return [_cameraList allValues];
    }
}

- (void)beginBrowsing {
    @synchronized (_browsingLock) {
        if (_timer == nil) {
            [self invalidateCameras];
            [self handleTimer:nil];
            
            const NSTimeInterval kBrowsingInterval = 5.0;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:kBrowsingInterval
                                                          target:self
                                                        selector:@selector(handleTimer:)
                                                        userInfo:nil
                                                         repeats:YES];
        }
    }
}

- (void)stopBrowsing {
    @synchronized (_browsingLock) {
        @synchronized (_udpDiscoveryLock) {
            [_browsingSocket close];
        }

        [_timer invalidate];
        self.timer = nil;
        
        // Wait for up to 5 seconds before invalidate the camera list.
        dispatch_group_wait(_browsingGroup, dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC));
        
        [self invalidateCameras];
    }
}

- (BOOL)isBrowsing {
    @synchronized (_browsingLock) {
        return _timer != nil;
    }
}

- (void)removeCameraForKey:(NSString *)cameraKey {
    @synchronized (_cameraListLock) {
        [_cameraList removeObjectForKey:cameraKey];
    }
}

- (void)removeAllCameras {
    @synchronized (_cameraListLock) {
        [_cameraList removeAllObjects];
    }
}

- (void)forceRefresh {
    [self handleTimer:nil];
}

- (void)pauseCameraRefreshing:(CZCamera *)camera duration:(NSTimeInterval)duration {
    self.pauseRefreshingCameraKey = camera.macAddress;
    self.pauseRefreshingExpireDate = [NSDate dateWithTimeIntervalSinceNow:duration];
}

#pragma mark - Private methods

- (void)handleTimer:(NSTimer *)timer {
    //CZLogv(@"Camera discovery timer fired.");
    
    dispatch_group_async(_browsingGroup, _browsingQueue, ^(){
        [self checkLostCameras];
        
        [self browseMockCamerasAsync];
        
        // Do not try to discover real cameras if no Wi-Fi is available.
        if ([[CZWifiNotifier sharedInstance] isReachable]) {
            [self browseGeminiCameraAsync];
            [self browseGVCPCamerasAsync];
            [self browseWisionCamerasAsync];
            [self browseMoticCamerasAsync];
            [self browseManuallyAddCamerasAsnyc];
        } else {
            CZLogv(@"Camera discovery not executed - no Wi-Fi available.");
        }
    });
}

- (void)invalidateCameras {
    @synchronized (_cameraListLock) {
        NSArray *cameras = [_cameraList allValues];
        for (CZCamera *camera in cameras) {
            [camera setThumbnailDelegate:nil];
            
            // Special handling for virtual camera since they don't have persistent
            // storage for security PINs. With this fix, the PIN for virtual camera
            // can be presisted during the app is alive.
            if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
                if (camera) {
                    [camera setDisplayName:[camera displayName] andUpload:NO];
                    [camera setPIN:[camera pin] andUpload:YES];
                }
            }
        }
    }
}

- (void)checkLostCameras {
    @synchronized (_cameraListLock) {
        
        
        NSMutableArray *lostCameras = [[NSMutableArray alloc] init];
        
        // Drain autorelease pool for the two NSArray created by "allValues",
        // or the camera objects will be retained longer than expected.
        @autoreleasepool {
            // Check living cameras.
            NSArray *cameras = [_cameraList allValues];
            for (CZCamera *camera in cameras) {
                NSTimeInterval timeInterval = [[camera lastActiveTime] timeIntervalSinceNow];
                if (timeInterval <= kLostTime) {
                    [lostCameras addObject:camera];
                } else {
                    // Refresh thumbnail for connectable cameras only.
                    [self refreshThumbnailOfCamera:camera forced:NO];
                }
            }
        }
        
        if ([lostCameras count] > 0) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                [notificationCenter postNotificationName:kCZCameraBrowserNotificationLost
                                                  object:nil
                                                userInfo:@{kCZCameraBrowserNotificationCameraList: lostCameras}];
            });
        }
        
        [lostCameras release];
    }
}

- (void)browseGVCPCamerasAsync {
    [_gevClient beginDiscovery];
}

- (void)browseMoticCamerasAsync {
    [self browseUDPBasedCamerasAsyncWithVendor:kVendorMagicNumberMotic];
}

- (void)browseWisionCamerasAsync {
    [self browseUDPBasedCamerasAsyncWithVendor:kVendorMagicNumberWision];
}

- (void)browseGeminiCameraAsync{
    [self browseUDPBasedCamerasAsyncWithVendor:kVendorMagicNumberZeiss];
}

- (void)browseUDPBasedCamerasAsyncWithVendor:(uint16_t)vendorMagicNumber {
    const uint16_t kDiscoveryPort = 8995;
    const NSTimeInterval kDiscoveryTimeout = 5;
    const size_t kPacketLength = 544;
    
    @synchronized (_udpDiscoveryLock) {
        GCDAsyncUdpSocket *socket = [self browsingSocket];
        
        do {
            NSError *error = nil;
            
            if (![socket enableBroadcast:YES error:&error]) {
                CZLogv(@"Wision/Motic/Gemini discovery: failed to enable broadcast for the socket.");
                break;
            }
            
            if (![socket beginReceiving:&error]) {
                CZLogv(@"Wision/Motic/Gemini discovery: failed to begin receiving data from socket.");
                break;
            }
            
            time_t now = time(NULL);
            struct tm *timeNow = localtime(&now);
            
            void *dataBuffer = malloc(kPacketLength);
            memset(dataBuffer, 0, kPacketLength);
            
            CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:dataBuffer
                                                                           length:kPacketLength];
            [writer writeShort:vendorMagicNumber];                  // magic_number
            [writer writeShortInNetworkOrder:0];                    // type
            [writer writeShortInNetworkOrder:0];                    // port
            [writer writeShort:0];                                  // device_code
            [writer writeLongInNetworkOrder:0];                     // serial_num
            [writer writeLong:0];                                   // eventcd
            [writer writeLong:0];                                   // arg, 0 or 3600
            [writer writeLong:0];                                   // reserved, 0
            [writer writeShort:(1900 + timeNow->tm_year)];          // year, 0-2099
            [writer writeByte:(timeNow->tm_mon + 1)];               // mon, 1-12
            [writer writeByte:timeNow->tm_mday];                    // day, 1-31
            [writer writeByte:timeNow->tm_hour];                    // hr, 0-23
            [writer writeByte:timeNow->tm_min];                     // min, 0-59
            [writer writeByte:timeNow->tm_sec];                     // sec, 0-59
            [writer writeByte:0xFF];                                // tz
            [writer release];
            
            NSData *packetData = [[NSData alloc] initWithBytes:dataBuffer
                                                        length:kPacketLength];
            
            [socket sendData:packetData
                      toHost:@"255.255.255.255" // Broadcast
                        port:kDiscoveryPort
                 withTimeout:kDiscoveryTimeout
                         tag:0];
            
            [packetData release];
            free(dataBuffer);
        } while (0);
    }
}

- (void)browseManuallyAddCamerasAsnyc {
    NSArray *microscopesSettings = [[CZDefaultSettings sharedInstance] manuallyAddMicroscopesSettings];
    if (microscopesSettings.count <= 0) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^() {
        for (NSDictionary *settings in microscopesSettings) {
            CZCamera *foundCamera = nil;
            NSString *macAddress = settings[kManuallyAddMicroscopeMacAddress];
            NSString *ipAddress = settings[kManuallyAddMicroscopeIPAddress];
            NSString *displayName = settings[kManuallyAddMicroscopeName];
            
            CZWisionCamera *wisionCamera = nil;
            if (foundCamera == nil) {
                wisionCamera = [[CZWisionCamera alloc] init];
                [wisionCamera setIpAddress:ipAddress];
                [wisionCamera setMacAddress:macAddress];
                [wisionCamera setDisplayName:displayName andUpload:NO];
                
                NSString *firmware = [wisionCamera firmwareVersion];
                if (firmware.length == 0) {
                    [wisionCamera release];
                    wisionCamera = nil;
                } else {
                    foundCamera = wisionCamera;
                }
            }
            
            CZMoticCamera *moticCamera = nil;
            if (foundCamera == nil) {
                moticCamera = [[CZMoticCamera alloc] init];
                [moticCamera setIpAddress:ipAddress];
                [moticCamera setMacAddress:macAddress];
                [moticCamera setDisplayName:displayName andUpload:NO];
                
                NSString *firmware = [moticCamera firmwareVersion];
                if (firmware.length == 0) {
                    [moticCamera release];
                    moticCamera = nil;
                } else {
                    foundCamera = moticCamera;
                }
            }
            
            CZKappaCamera *kappaCamera = nil;
            if (foundCamera == nil) {
                kappaCamera = [[CZKappaCamera alloc] init];
                [kappaCamera setIpAddress:ipAddress];
                [kappaCamera setMacAddress:macAddress];
                [kappaCamera setDisplayName:displayName andUpload:NO];
                
                // TODO:try to get lock of Kappa camera
                CZCameraControlLock *controlLock = [[CZCameraControlLock alloc] initWithCamera:kappaCamera];
                if (controlLock) {
                    foundCamera = kappaCamera;
                    [controlLock release];
                } else {
                    [kappaCamera release];
                    kappaCamera = nil;
                }
            }
            
            CZGeminiCamera *geminiCamera = nil;
            if (foundCamera == nil) {
                geminiCamera = [[CZGeminiCamera alloc] init];
                [geminiCamera setIpAddress:ipAddress];
                [geminiCamera setMacAddress:macAddress];
                [geminiCamera setDisplayName:displayName andUpload:NO];
                
                CZCameraControlLock *controlLock = [[CZCameraControlLock alloc] initWithCamera:geminiCamera];
                if (controlLock) {
                    foundCamera = geminiCamera;
                    [controlLock release];
                } else {
                    [geminiCamera release];
                    geminiCamera = nil;
                }
            }
            
            if (foundCamera && [[CZDefaultSettings sharedInstance] containsManuallyMicroscopeSettingsAtMacAddress:macAddress]) {
                [self foundCamera:foundCamera];
                
                CZCamera *existCamera = _cameraList[foundCamera.macAddress];
                if (existCamera) {
                    // Begin getting thumbnail for new cameras.
                    [self refreshThumbnailOfCamera:existCamera forced:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^() {
                        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                        [notificationCenter postNotificationName:kCZCameraBrowserNotificationFound
                                                          object:nil
                                                        userInfo:@{kCZCameraBrowserNotificationCameraList: @[existCamera]}];
                    });
                }
            }
            
            [wisionCamera release];
            [kappaCamera release];
            [moticCamera release];
            [geminiCamera release];
        }
    });
}

- (void)browseMockCamerasAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        @synchronized (_cameraListLock) {
            NSMutableArray *found = [[NSMutableArray alloc] init];
            
            if (self.virtualCameraEnabled) {
                for (int i = 0; i < kNumOfMockCameras; i++) {
                    NSString *mockMacAddress = [[NSString alloc] initWithFormat:@"00:00:00:00:00:0%d", i+1];
                    if ([_cameraList objectForKey:mockMacAddress] == nil) {
                        CZLocalFileCamera *camera = [[CZLocalFileCamera alloc] init];
                        NSString *displayName = camera.displayName;
                        [camera setDisplayName:[NSString stringWithFormat:@"%@%d", displayName, i+1] andUpload:NO];
                        [camera setMacAddress:mockMacAddress];
                        [found addObject:camera];
                        [camera release];
                    }
                    [mockMacAddress release];
                }
            } else {
                // remove all virtual cameras
                NSArray<NSString *> *allCameraKeys = [_cameraList.allKeys retain];
                for (NSString *cameraKey in allCameraKeys) {
                    CZCamera *camera = _cameraList[cameraKey];
                    if ([camera isKindOfClass:[CZLocalFileCamera class]]) {
                        [_cameraList removeObjectForKey:cameraKey];
                    }
                }
                
                [allCameraKeys release];
            }

            for (int i = 0; i < kNumOfRemoteCameras; i++) {
                NSString *remoteMacAddress = [[NSString alloc] initWithFormat:@"11:00:00:00:00:0%d", i+1];
                if ([_cameraList objectForKey:remoteMacAddress] == nil) {
                    CZRemoteStreamCamera *camera = [[CZRemoteStreamCamera alloc] init];
                    NSString *displayName = camera.displayName;
                    [camera setDisplayName:[NSString stringWithFormat:@"%@", displayName] andUpload:NO];
                    [camera setMacAddress:remoteMacAddress];
                    [found addObject:camera];
                    [camera release];
                }
                [remoteMacAddress release];
            }
            
            if (self.plusCameraEnabled) {
                for (int i = 0; i < kNumOfPlusCameras; i++) {
                    NSString *plusMacAddress = [[NSString alloc] initWithFormat:@"22:00:00:00:00:0%d", i+1];
                    if ([_cameraList objectForKey:plusMacAddress] == nil) {
                        CZPlusCamera *camera = [[CZPlusCamera alloc] init];
                        NSString *displayName = camera.displayName;
                        [camera setDisplayName:[NSString stringWithFormat:@"%@", displayName] andUpload:NO];
                        [camera setMacAddress:plusMacAddress];
                        [found addObject:camera];
                        [camera release];
                    }
                    [plusMacAddress release];
                }
            } else {
                // remove plus camera
                NSArray<NSString *> *allCameraKeys = [_cameraList.allKeys retain];
                for (NSString *cameraKey in allCameraKeys) {
                    CZCamera *camera = _cameraList[cameraKey];
                    if ([camera isKindOfClass:[CZPlusCamera class]]) {
                        [_cameraList removeObjectForKey:cameraKey];
                    }
                }
                
                [allCameraKeys release];
            }

            if ([found count] > 0) {
                NSMutableArray *existCameras = [NSMutableArray array];
                for (CZCamera *camera in found) {
                    [self foundCamera:camera];
                    
                    CZCamera *existCamera = self.cameraList[camera.macAddress];
                    if (existCamera) {
                        // Begin getting thumbnail for new cameras.
                        [self refreshThumbnailOfCamera:existCamera forced:YES];
                        [existCameras addObject:existCamera];
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^() {
                    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                    [notificationCenter postNotificationName:kCZCameraBrowserNotificationFound
                                                      object:nil
                                                    userInfo:@{kCZCameraBrowserNotificationCameraList: existCameras}];
                });
            }
            
            [found release];
        }
    });
}

- (GCDAsyncUdpSocket *)browsingSocket {
    if (!_browsingSocket) {
        GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                  delegateQueue:_udpDiscoveryQueue];
        [socket setIPv4Enabled:YES];
        [socket setIPv6Enabled:YES];
        _browsingSocket = socket;
    }
    
    return _browsingSocket;
}

- (void)foundCamera:(CZCamera *)camera {
    @synchronized (_cameraListLock) {
        NSString *cameraKey = camera.macAddress;
        CZCamera *existCamera = _cameraList[cameraKey];
        if ([camera class] == [existCamera class]) {
            // hand over informations
            [existCamera setDisplayName:camera.displayName andUpload:NO];
            [existCamera setPIN:camera.pin andUpload:NO];
            [existCamera setIpAddress:camera.ipAddress];
        } else {
            BOOL result = [camera prefetchRequiredCameraInfo];
            if (result) {
                [_cameraList setObject:camera forKey:cameraKey];
            }
        }
    }
}

- (CZCamera *)manuallyAddedCameraForIPAddress:(NSString *)cameraIPAddress {
    NSArray *microscopesSettings = [[CZDefaultSettings sharedInstance] manuallyAddMicroscopesSettings];
    for (NSDictionary *settings in microscopesSettings) {
        NSString *macAddress = settings[kManuallyAddMicroscopeMacAddress];
        NSString *ipAddress = settings[kManuallyAddMicroscopeIPAddress];
        NSString *displayName = settings[kManuallyAddMicroscopeName];
        if ([ipAddress isEqualToString:cameraIPAddress]) {
            CZCamera *existCamera = self.cameraList[macAddress];
            if (existCamera == nil) {
                existCamera = [[[CZCamera alloc] init] autorelease];
                existCamera.macAddress = macAddress;
                existCamera.ipAddress = ipAddress;
                [existCamera setDisplayName:displayName andUpload:NO];
            }
            return existCamera;
        }
    }
    return nil;
}

- (void)refreshThumbnailOfCamera:(CZCamera *)camera forced:(BOOL)forced {
    // Do not refresh thumbnail if no Wi-Fi is available.
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return;
    }
    
    if (self.thumbnailRefreshingEnabled) {
        if ([_delegate respondsToSelector: @selector(cameraBrowserShouldRefreshThumbnailForCamera:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                BOOL shouldUpdate = forced ? YES : [_delegate cameraBrowserShouldRefreshThumbnailForCamera:camera];
                if (shouldUpdate) {
                    [camera beginThumbnailUpdating:self.thumbnailSize];
                }
            });
        }
    }
}

- (BOOL)isCameraPausedRefreshing:(NSString *)cameraKey {
    if (_pauseRefreshingCameraKey && [cameraKey isEqualToString:_pauseRefreshingCameraKey]) {
        if (_pauseRefreshingExpireDate) {
            if ([_pauseRefreshingExpireDate timeIntervalSinceNow] >= 0) {
                return YES;
            } else {
                self.pauseRefreshingExpireDate = nil;
                self.pauseRefreshingCameraKey = nil;
            }
        }
    }
    
    return NO;
}

#pragma mark - CZGEVClientDelegate methods

- (void)gevClient:(CZGEVClient *)client didGetDiscoveryAck:(CZGVCPDiscoveryAck *)acknowlege {
    @synchronized (_cameraListLock) {
        CZKappaCamera *camera = (CZKappaCamera *)[_cameraList objectForKey:[acknowlege deviceMacAddress]];
        if (![camera isKindOfClass:[CZKappaCamera class]]) {
            camera = nil;
        }
        
        NSString *name = [acknowlege userDefinedName];
        NSString *macAddress = [acknowlege deviceMacAddress];
        NSString *ipAddress = [acknowlege currentIpAddress];
        NSString *firmwareVersion = [acknowlege deviceVersion];
        
        if (camera == nil) {
            if ([CZCommonUtils isMacAddressFromCarlZeissChina:macAddress]) {
                camera = [[CZKappaCamera alloc] init];
                
                [camera setIpAddress:ipAddress];
                [camera setMacAddress:macAddress];
                [camera setFirmwareVersion:firmwareVersion];
                
                if (name != nil) {
                    [camera setName:name andUpload:NO];
                }
                
                CZCamera *manuallyAddedCamera = [self manuallyAddedCameraForIPAddress:camera.ipAddress];
                
                [self foundCamera:camera];
                [camera release];
                
                // Begin getting thumbnail for new cameras.
                CZCamera *existCamera = self.cameraList[macAddress];
                if (existCamera) {
                    [self refreshThumbnailOfCamera:existCamera forced:YES];
                    
                    if (manuallyAddedCamera) {
                        [[CZDefaultSettings sharedInstance] removeManuallyMicroscopeSettingsAtMacAddress:manuallyAddedCamera.macAddress];
                        [self removeCameraForKey:manuallyAddedCamera.macAddress];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                            [notificationCenter postNotificationName:kCZCameraBrowserNotificationReplace
                                                              object:nil
                                                            userInfo:@{kCZCameraBrowserNotificationOldCamera: manuallyAddedCamera,
                                                                       kCZCameraBrowserNotificationNewCamera: existCamera}];
                        });
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                            [notificationCenter postNotificationName:kCZCameraBrowserNotificationFound
                                                              object:nil
                                                            userInfo:@{kCZCameraBrowserNotificationCameraList: @[existCamera]}];
                        });
                    }
                }
            }
        } else {
            // Update name for PIN refreshing.
            if (name != nil && ![self isCameraPausedRefreshing:macAddress]) {
                [camera setName:name andUpload:NO];
                [camera setIpAddress:ipAddress];
                [camera setMacAddress:macAddress];
                [camera setFirmwareVersion:firmwareVersion];
                
                dispatch_async(dispatch_get_main_queue(), ^() {
                    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                    [notificationCenter postNotificationName:kCZCameraBrowserNotificationUpdate
                                                      object:nil
                                                    userInfo:@{kCZCameraBrowserNotificationCameraList: @[camera]}];
                });
            }
            
            NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
            [camera setLastActiveTime:now];
            [now release];
        }
    }
}

#pragma mark - GCDAsyncUdpSocketDelegate methods

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    // pre-process: filter out invalid(zero) bytes after header
    const NSUInteger kHeaderLength = 32;
    NSMutableData *filteredData = [[NSMutableData alloc] initWithCapacity:data.length];
    const uint8_t *pData = data.bytes;
    [filteredData appendBytes:pData length:kHeaderLength]; // append header
    pData += kHeaderLength;
    for (NSUInteger i = kHeaderLength; i < data.length; i++) {
        if (*pData) {
            [filteredData appendBytes:pData length:1];
        }
        ++pData;
    }
    uint8_t stringEnd = 0;
    [filteredData appendBytes:&stringEnd length:1];
    data = [NSData dataWithData:filteredData];
    [filteredData release];
    
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];

    @autoreleasepool {
        uint16_t vendor = [reader readShort];
        
        [reader seekToOffset:kHeaderLength];
        
        NSString *response = [reader readStringWithLength:(data.length - kHeaderLength)];
        
        // The format of the response:
        // Name;Model;ID;IP address;Mask;Gateway;HTTP port;MAC address;...
        // xxxx;xxxx;eth0;xxx;xxx;xxx;80;xxxx;wlan0;ip;mask;gate;http;mac;
        
        // The response may contains two network chip information. One is 
        CZCameraInformation *cameraInformation = [self parseUdpResponse:response];
        
        if (cameraInformation.connectable) {
            NSString *name = cameraInformation.cameraName;
            NSString *ipAddress = cameraInformation.ipAddress ;
            NSString *macAddress = cameraInformation.macAddress ;
            CZLogv(@"ip : %@, mac : %@", ipAddress, macAddress);
            if (ipAddress.length > 0 && macAddress.length > 0) {
                @synchronized (_cameraListLock) {
                    if ([CZCommonUtils isMacAddressFromCarlZeissChina:macAddress]) {
                        CZCamera *camera = [_cameraList objectForKey:macAddress];
                        
                        Class expectCameraClass;
                        if(vendor == kVendorMagicNumberZeiss){
                            expectCameraClass = [CZGeminiCamera class];
                        }
                        else if (vendor == kVendorMagicNumberMotic) {
                            expectCameraClass = [CZMoticCamera class];
                        } else if (vendor == kVendorMagicNumberWision) {
                            expectCameraClass = [CZWisionCamera class];
                        } else {
                            [reader release];
                            return;
                        }
                        
                        if (camera && ![camera isKindOfClass:expectCameraClass]) {
                            camera = nil;
                        }
                        
                        if (camera == nil) {
                            camera = [[expectCameraClass alloc] init];
                            
                            [camera setIpAddress:ipAddress];
                            [camera setMacAddress:macAddress];
                            
                            if (name != nil) {
                                if (vendor == kVendorMagicNumberMotic ||
                                    (![name isEqualToString:@"IPCam"] && ![name isEqualToString:@"(null)"])) {
                                    [camera setName:name andUpload:NO];
                                }
                            }
                            
                            CZCamera *manuallyAddedCamera = [self manuallyAddedCameraForIPAddress:camera.ipAddress];
                            
                            [self foundCamera:camera];
                            [camera release];
                            
                            // Begin getting thumbnail for new cameras.
                            CZCamera *existCamera = self.cameraList[macAddress];
                            if (existCamera) {
                                [self refreshThumbnailOfCamera:existCamera forced:YES];
                                
                                if (manuallyAddedCamera) {
                                    [[CZDefaultSettings sharedInstance] removeManuallyMicroscopeSettingsAtMacAddress:manuallyAddedCamera.macAddress];
                                    [self removeCameraForKey:manuallyAddedCamera.macAddress];
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                                        [notificationCenter postNotificationName:kCZCameraBrowserNotificationReplace
                                                                          object:nil
                                                                        userInfo:@{kCZCameraBrowserNotificationOldCamera: manuallyAddedCamera,
                                                                                   kCZCameraBrowserNotificationNewCamera: existCamera}];
                                    });
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                                        [notificationCenter postNotificationName:kCZCameraBrowserNotificationFound
                                                                          object:nil
                                                                        userInfo:@{kCZCameraBrowserNotificationCameraList: @[existCamera]}];
                                    });
                                }
                            }
                        } else {
                            // Update name for PIN refreshing.
                            if (name != nil && ![self isCameraPausedRefreshing:camera.macAddress]) {
                                if (vendor == kVendorMagicNumberMotic ||
                                    (![name isEqualToString:@"IPCam"] && ![name isEqualToString:@"(null)"])) {
                                    [camera setName:name andUpload:NO];
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^() {
                                        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                                        [notificationCenter postNotificationName:kCZCameraBrowserNotificationUpdate
                                                                          object:nil
                                                                        userInfo:@{kCZCameraBrowserNotificationCameraList: @[camera]}];
                                    });
                                }
                            }
                            
                            // Update IP address.
                            [camera setIpAddress:ipAddress];
                            
                            // Update camera info.
                            [camera prefetchRequiredCameraInfo];
                            
                            NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
                            [camera setLastActiveTime:now];
                            [now release];
                        }
                    }
                }
            }
        }
        
        [cameraInformation release];
    }
    
    [reader release];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    @synchronized (_udpDiscoveryLock) {
        if (_browsingSocket == sock) {
            [_browsingSocket release];
            _browsingSocket = nil;
        }
    }
}

#pragma mark - IP Address handler
- (CZCameraInformation *)parseUdpResponse:(NSString *)response {
   
    NSArray<NSString *> *fields = [response componentsSeparatedByString:@";"];
    // fields format:
    // Name;Model;ID;IP address;Mask;Gateway;HTTP port;MAC address;...
    // xxxx;xxxx;eth0;xxx;xxx;xxx;80;xxxx;wlan0;ip;mask;gate;http;mac;

    NSInteger ethIndex = [fields indexOfObject:@"eth0"];
    NSInteger wlanIndex = [fields indexOfObject:@"wlan0"];
    
    if ((fields.count > 7 && fields.count < 14) || (ethIndex == NSNotFound && wlanIndex == NSNotFound)) {
        
        return [self parseUdpResponse:fields indexOfId:2];
        
    } else if (fields.count >= 14) {
        
        CZCameraInformation* information = [self parseUdpResponse:fields indexOfId:ethIndex];
        
        if (information.connectable) {
            return information;
        } else {
            NSString* macAddress = [information.macAddress copy];
            [information release];
            information = [self parseUdpResponse:fields indexOfId:wlanIndex];
            information.macAddress = macAddress;
            return information;
        }
    }
    
    return [[CZCameraInformation alloc] init];
}

- (CZCameraInformation *)parseUdpResponse:(NSArray<NSString*>*)response
                                        indexOfId:(NSInteger)index {
    if (index == NSNotFound || response.count <= index) {
        return [[CZCameraInformation alloc] init];
    }
    
    // Respnose format:
    // Name;Model;ID;IP address;Mask;Gateway;HTTP port;MAC address;...
    // xxxx;xxxx;eth0;xxx;xxx;xxx;80;xxxx;wlan0;ip;mask;gate;http;mac;
    NSString *ipAddress = response[index + 1];
   
    NSString *macAddress = response[index + 5];
#if TARGET_IPHONE_SIMULATOR
    // We cannot get local ip address and subnet mask when using simulator.
    // So we always choose to connect to Ethernet, no matter iPad is in the
    // same subnet or not.
    return [[CZCameraInformation alloc] initWithName:response[0]
                                           ipAddress:ipAddress
                                          macAddress:macAddress
                                         connectable:YES];
#else
    NSString *subnetMask = response[index + 2];
    NSString *localSubnetMask = [CZCommonUtils localSubnetMask];
    NSString *localIPAddress = [CZCommonUtils localIpAddress];

    NSString *ipAddressWithMask = [CZCommonUtils andOperationIPAddress:ipAddress
                                                            subnetMask:subnetMask];
    NSString *localIpAddressWithMask = [CZCommonUtils andOperationIPAddress:localIPAddress
                                                                 subnetMask:localSubnetMask];
    if (ipAddressWithMask && localIpAddressWithMask) {
        return [[CZCameraInformation alloc] initWithName:response[0]
                                                ipAddress:ipAddress
                                               macAddress:macAddress
                                              connectable:[ipAddressWithMask isEqualToString:localIpAddressWithMask]] ;
    }
    
    return [[CZCameraInformation alloc] init];
#endif
}


#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self && [keyPath isEqualToString:@"thumbnailRefreshingEnabled"]) {
        NSNumber *oldValue = (NSNumber *)[change objectForKey:NSKeyValueChangeOldKey];
        NSNumber *newValue = (NSNumber *)[change objectForKey:NSKeyValueChangeNewKey];
        if (![oldValue isEqualToNumber:newValue]) {
            BOOL enabled = [newValue boolValue];
            if (enabled) {
                [self.timer fire];
            } else {
                [[CZRTSPThumbnailQueue sharedInstance] clearQueue];
            }
        }
    }
}

@end
