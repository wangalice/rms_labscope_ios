//
//  CZCameraBrowser.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

extern NSString * const kCZCameraBrowserNotificationFound;
extern NSString * const kCZCameraBrowserNotificationLost;
extern NSString * const kCZCameraBrowserNotificationUpdate;
extern NSString * const kCZCameraBrowserNotificationRemoved;
extern NSString * const kCZCameraBrowserNotificationReplace;
extern NSString * const kCZCameraBrowserNotificationOldCamera;
extern NSString * const kCZCameraBrowserNotificationNewCamera;
extern NSString * const kCZCameraBrowserNotificationCameraList;

@class CZCamera;

@protocol CZCameraBrowserDelegate <NSObject>

@optional
- (BOOL)cameraBrowserShouldRefreshThumbnailForCamera:(CZCamera *)camera;

@end

/*!
 * A singleton pattern implementation that provides a unified way to discover
 * all kinds of cameras in the same local network.
 */
@interface CZCameraBrowser : NSObject

@property (atomic, assign) id<CZCameraBrowserDelegate> delegate;
@property (atomic, assign) CGSize thumbnailSize;
@property (atomic, assign) BOOL thumbnailRefreshingEnabled;
@property (atomic, assign) BOOL virtualCameraEnabled;
@property (atomic, assign) BOOL plusCameraEnabled;

/**
 * The one and only way to get the shared instance.
 * \return The global shared instance of CZCameraBrowser
 */
+ (CZCameraBrowser *)sharedInstance;

/*! Returns cached list which contains all the discovered cameras. */
- (NSArray *)cameras;

/*! Starts browsing cameras. */
- (void)beginBrowsing;
 
/*! Abandons the current browsing action. */
- (void)stopBrowsing;

- (BOOL)isBrowsing;

- (void)removeCameraForKey:(NSString *)cameraKey;
- (void)removeAllCameras;

- (void)forceRefresh;
- (void)pauseCameraRefreshing:(CZCamera *)camera duration:(NSTimeInterval)duration;

@end
