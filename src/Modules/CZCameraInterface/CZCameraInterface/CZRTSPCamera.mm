//
//  CZRTSPCamera.mm
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRTSPCamera.h"
#import "CZCameraSubclass.h"
#import <CZToolbox/CZToolbox.h>
#import <CZVideoEngine/CZVideoEngine.h>

@interface CZRTSPCamera () <CZRTSPThumbnailAcquirer> {
    NSObject *_thumbnailLock;
    CGSize _thumbnailSize;
}

@property (atomic, retain) UIImage *lastThumbnail;
@property (atomic, retain) CZRTSPClient *client;
@property (atomic, retain) id<CZHardwareDecoder> decoder;

- (void)stopObject:(id)object;

@end

@implementation CZRTSPCamera

+ (Class)clientClass {
    return [CZRTSPClient class];
}

- (id)init {
    self = [super init];
    if (self) {
        _client = nil;
        _thumbnailLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [[CZRTSPThumbnailQueue sharedInstance] removeAcquirer:self];
    
    [self stopObject:_client];
    [self stopObject:_decoder];
    
    [_client release];
    [_decoder release];
    [_lastThumbnail release];
    [_thumbnailLock release];

    [super dealloc];
}

#pragma mark - Overridden methods

- (UIImage *)thumbnail {
    return self.lastThumbnail;
}

- (UIImage *)liveThumbnail {
    return self.lastThumbnail;
}

- (void)setLiveThumbnail:(UIImage *)liveThumbnail {
    if (liveThumbnail.size.width > _thumbnailSize.width &&
        liveThumbnail.size.height > _thumbnailSize.height) {
        liveThumbnail = [CZCommonUtils scaleImage:liveThumbnail
                                         intoSize:_thumbnailSize];
    }
    
    if (liveThumbnail) {
        self.lastThumbnail = liveThumbnail;
    }
}

- (void)beginThumbnailUpdating:(CGSize)thumbnailSize {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        @synchronized (_thumbnailLock) {
            _thumbnailSize = thumbnailSize;
            
            if (self.decoder) {
                // If the live video is being played, directly fetch a frame
                // to generate thumbnail.
                UIImage *thumbnail = [CZCommonUtils scaleImage:self.decoder.lastDecodedFrame
                                                      intoSize:thumbnailSize];
                if (thumbnail) {
                    self.lastThumbnail = thumbnail;
                
                    if ([self.thumbnailDelegate respondsToSelector:@selector(camera:didUpdateThumbnail:)]) {
                        dispatch_async(dispatch_get_main_queue(), ^() {
                            [self.thumbnailDelegate camera:self didUpdateThumbnail:_lastThumbnail];
                        });
                    }
                }
            } else {
                [[CZRTSPThumbnailQueue sharedInstance] appendAcquirer:self];
            }
        }
    });
}

- (void)play {
    @synchronized (self) {
        if (self.ipAddress == nil) {
            return;
        }
        
        if (![[CZWifiNotifier sharedInstance] isReachable]) {
            CZLogv(@"Camera playback not started - no Wi-Fi available.");
            return;
        }
        
        if (self.client == nil) {
            [self stopObject:self.decoder];
            
            NSURL *rtspUrl = [self rtspUrl];
            CZRTSPClient *newClient = [[[[self class] clientClass] alloc] initWithURL:rtspUrl];
            
            [newClient setDelegate:self];
            [newClient setResolution:[self liveResolution]];
            self.client = newClient;
            [newClient release];
            
            id<CZHardwareDecoder> newDecoder = [newClient needsFileWrapping] ? [[CZHardwareFileDecoder alloc] init] : [[CZHardwareStreamDecoder alloc] init];
            [newDecoder setDelegate:self];
            self.decoder = newDecoder;
            [newDecoder release];
            
            [self.decoder start];
            [self.client play];
        }
    }
}

- (void)stop {
    @synchronized (self) {
        if (self.client || self.decoder) {
            CZRTSPClient *client = self.client;
            id<CZHardwareDecoder> decoder = self.decoder;
            
            __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
            bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"StopRTSPCamera"
                                                                  expirationHandler:^{
                                                                      if (bgTask != UIBackgroundTaskInvalid) {
                                                                          [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                                                                          bgTask = UIBackgroundTaskInvalid;
                                                                      }
                                                                  }];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
                [self stopObject:client];
                [self stopObject:decoder];
                
                if (bgTask != UIBackgroundTaskInvalid) {
                    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                    bgTask = UIBackgroundTaskInvalid;
                }
            });
        }

        self.client = nil;
        self.decoder = nil;
    }
}

- (void)pause {
    @synchronized (self) {
        [self.client pause];
        [self.decoder pause];
    }
}

- (void)resume {
    @synchronized (self) {
        [self.client resume];
        [self.decoder resume];
    }
}

- (BOOL)isPaused {
    return [self.client isPaused];
}

- (void)startRelayToURL:(NSURL *)relayURL {
    @synchronized (self) {
        [self.client startRelayToURL:relayURL];
    }
}

- (void)endRelay {
    @synchronized (self) {
        [self.client endRelay];
    }
}

- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount {
    if (frameCount != 1) {
        return [super beginContinuousSnappingFromVideoStream:frameCount];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                @autoreleasepool {
                    NSError *error = nil;
                    BOOL grayscale = NO;
                    CZCameraControlLock *lock = [[CZCameraControlLock alloc] initWithCamera:self];
                    if (lock) {
                        grayscale = [self isGrayscaleModeEnabled];
                        [lock release];
                    }
                    UIImage *frame = [[self.decoder lastDecodedFrame] retain];
                    if (frame == nil) {
                        error = [NSError errorWithDomain:CZCameraErrorDomain
                                                    code:kCZCameraTimeoutError
                                                userInfo:nil];
                    }
                    CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                    snappingInfo.image = frame;
                    snappingInfo.grayscale = grayscale;
                    [self.delegate camera:self didFinishSnapping:snappingInfo error:error];
                    [snappingInfo release];
                    [frame release];
                }
            });
        }
    });
}

- (void)beginRecordingToFile:(NSString *)filePath {
    [_client beginRecordingToFile:filePath];
}

- (void)endRecording {
    [_client endRecording];
}

- (void)beginTimelapseRecordingToFile:(NSString *)filePath
                          inFrameRate:(NSUInteger)frameRate
                             interval:(NSTimeInterval)interval
                            overlayer:(CALayer *)overlayer {
    if (![self.timelapseRecorder isRecording]) {
        CZTimelapseRecorder *recorder = [[CZTimelapseRecorder alloc] init];
        recorder.interval = interval;
        recorder.frameRate = frameRate;
        recorder.videoSize = [self liveResolution];
        recorder.overlayer = overlayer;
        recorder.delegate = self;

        self.timelapseRecorder = recorder;
        [self.timelapseRecorder beginRecordingToFile:filePath];
        [recorder release];
    }
}

#pragma mark - Default implementations

- (NSURL *)rtspUrl {
    return nil;
}

#pragma mark - Private methods

- (void)stopObject:(id)object {
    if ([object respondsToSelector:@selector(stop)]) {
        [object stop];
    }
    if ([object respondsToSelector:@selector(setDelegate:)]) {
        [object setDelegate:nil];
    }
}

#pragma mark - CZRTSPClientDelegate methods

- (void)rtspClient:(CZRTSPClient *)client didGetVideoSegment:(id)segment {
    if (client == self.client) {
        if (segment == nil) {
            [self stop];
        } else {
            self.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
            [self.decoder decodeVideoSegment:segment];
        
            // The camera is alive, update the last active time.
            NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
            [self setLastActiveTime:now];
            [now release];
        }
    }
}

- (void)rtspClientDidGetVideoFrame:(CZRTSPClient *)client {
    if (client == self.client) {
        self.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
    }
}

- (void)rtspClientDidEndRecording:(CZRTSPClient *)client {
    if (client == self.client) {
        if ([self.delegate respondsToSelector:@selector(cameraDidFinishRecording:)]) {
            [self.delegate cameraDidFinishRecording:self];
        }
    }
}

#pragma mark - CZHardwareDecoderDelegate methods

- (void)hardwareDecoder:(id<CZHardwareDecoder>)decoder didGetFrame:(UIImage *)frame {
    if (decoder == self.decoder && frame != nil) {
        self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
        
        [self.timelapseRecorder recordVideoFrame:frame];
        
        if ([self.delegate respondsToSelector:@selector(camera:didGetVideoFrame:)]) {
            [self.delegate camera:self didGetVideoFrame:frame];
        }
    }
}

#pragma mark - CZRTSPThumbnailAcquirer

- (void)thumbnailQueue:(CZRTSPThumbnailQueue *)queue didGetThumbnail:(UIImage *)thumbnail {
    if (thumbnail == nil) {
        return;
    }
    
    self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
    self.lastThumbnail = [CZCommonUtils scaleImage:thumbnail intoSize:_thumbnailSize];

    dispatch_async(dispatch_get_main_queue(), ^() {
        if ([self.thumbnailDelegate respondsToSelector:@selector(camera:didUpdateThumbnail:)]) {
            [self.thumbnailDelegate camera:self didUpdateThumbnail:_lastThumbnail];
        }
    });
}

@end
