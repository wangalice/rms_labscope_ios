//
//  CZBuiltInCamera.h
//  Hermes
//
//  Created by Halley Gu on 6/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCamera.h"

@class AVCaptureSession;

@interface CZBuiltInCamera : CZCamera

@property (nonatomic, readonly) AVCaptureSession *session;
@property (nonatomic, assign) UIInterfaceOrientation orientation;

- (void)setPreferredResolution:(CGSize)resolution;
- (void)setPointOfInterest:(CGPoint)point;

@end
