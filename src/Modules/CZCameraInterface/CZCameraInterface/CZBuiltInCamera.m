//
//  CZBuiltInCamera.m
//  Hermes
//
//  Created by Halley Gu on 6/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZBuiltInCamera.h"
#import "CZCameraSubclass.h"
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <CZToolbox/CZToolbox.h>

@interface CZBuiltInCamera () <AVCaptureVideoDataOutputSampleBufferDelegate> {
    CGSize _preferredResolution;
    dispatch_queue_t _snapQueue;
}

@property (nonatomic, retain) AVCaptureDevice *device;
@property (nonatomic, retain) AVCaptureSession *session;
@property (nonatomic, retain) AVCaptureStillImageOutput *output;

@end

@implementation CZBuiltInCamera

- (id)init {
    self = [super init];
    if (self) {
        _preferredResolution = CGSizeZero;
        _orientation = UIInterfaceOrientationLandscapeLeft;
        _snapQueue = dispatch_queue_create("com.zeisscn.builtincamera.snap", NULL);
        
        NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        for (AVCaptureDevice *device in devices) {
            if ([device position] == AVCaptureDevicePositionBack) {
                _device = [device retain];
                break;
            }
        }
    }
    return self;
}

- (void)dealloc {
    [self stop];
    
    [_device release];
    [_session release];
    [_output release];
    
    if (_snapQueue != NULL) {
        dispatch_release(_snapQueue);
    }
    
    [super dealloc];
}

#pragma mark - Overridden methods

- (NSDate *)lastActiveTime {
    return [NSDate date]; // As embedded hardware, it's always online
}

- (void)play {
    if (self.session != nil) {
        return;
    }
    
    @synchronized (self) {
        NSError *error = nil;
        AVCaptureSession *session = [[AVCaptureSession alloc] init];
        AVCaptureStillImageOutput *output = nil;
        BOOL successful = NO;
        
        do {
            session.sessionPreset = [self actualSessionPreset];
            
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:&error];
            if (input == nil || error != nil || ![session canAddInput:input]) {
                break;
            }
            
            [session addInput:input];
            
            output = [[[AVCaptureStillImageOutput alloc] init] autorelease];
            [output setOutputSettings:@{AVVideoCodecKey: AVVideoCodecJPEG}];
            
            if (![session canAddOutput:output]) {
                break;
            }
            
            [session addOutput:output];
            
            successful = YES;
        } while (NO);
        
        if (!successful) {
            if (error != nil) {
                CZLogv(@"Error happened during start playing built-in camera: %@", error);
            }
            
            [session release];
            return;
        }
        
        self.session = session;
        self.output = output;
        
        [session startRunning];
        [session release];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(subjectAreaDidChange:)
                                                     name:AVCaptureDeviceSubjectAreaDidChangeNotification
                                                   object:self.device];
    }
}

- (void)stop {
    if (self.session == nil) {
        return;
    }
    
    @synchronized (self) {
        [self.session stopRunning];
        self.session = nil;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVCaptureDeviceSubjectAreaDidChangeNotification
                                                      object:self.device];
    }
}

- (CGSize)snapResolution {
    return [self liveResolution];
}

- (CGSize)liveResolution {
    NSString *preset = [self actualSessionPreset];
    if ([preset isEqualToString:AVCaptureSessionPreset640x480]) {
        return CGSizeMake(640, 480);
    } else if ([preset isEqualToString:AVCaptureSessionPreset1280x720]) {
        return CGSizeMake(1280, 720);
    } else if ([preset isEqualToString:AVCaptureSessionPresetPhoto]) {
        if ([CZCommonUtils iPadModelNumber] >= 500) { // iPad air 2 and later
            return CGSizeMake(3264, 2448);
        } else {
            return CGSizeMake(2592, 1936);
        }
    }
    return CGSizeZero;
}

- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount {
    if (frameCount != 1) {
        return [super beginContinuousSnappingFromVideoStream:frameCount];
    }
    
    dispatch_async(_snapQueue, ^() {
        AVCaptureConnection *connection = [self.output connectionWithMediaType:AVMediaTypeVideo];
        [connection setVideoOrientation:self.orientation];
        
        [self.output captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            @autoreleasepool {
//                [self stop]; // NOTICE: stop after snap to save memory.

                UIImage *image = nil;
                NSNumber *exposureTime = nil;
                if (imageDataSampleBuffer != nil) {
                    CFDictionaryRef exifDictRef = CMGetAttachment(imageDataSampleBuffer, kCGImagePropertyExifDictionary, NULL);
                    NSDictionary *exifDict = (NSDictionary *)exifDictRef;
                    exposureTime = exifDict[(id)kCGImagePropertyExifExposureTime];
                    
                    NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                    UIImage *jpegImage = [[UIImage alloc] initWithData:imageData];
                    if (jpegImage.size.width > 3072) {  // iPad air 2 and later size
                        image = [[CZCommonUtils scaleImage:jpegImage intoSize:CGSizeMake(3072, 2304)] retain];
                    } else {
                        image = [CZCommonUtils newDecompressedImageFromJPEGImage:jpegImage];
                    }
                    [jpegImage release];
                }
                
                if (image == nil) {
                    error = [NSError errorWithDomain:CZCameraErrorDomain
                                                code:kCZCameraTimeoutError
                                            userInfo:nil];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                        CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                        snappingInfo.image = image;
                        snappingInfo.grayscale = NO;
                        snappingInfo.exposureTimeInMilliseconds = exposureTime;
                        [self.delegate camera:self didFinishSnapping:snappingInfo error:error];
                        [snappingInfo release];
                    }
                });
                
                [image release];
            }
        }];
    });
}

#pragma mark - Public methods

- (void)setPreferredResolution:(CGSize)resolution {
    if (!CGSizeEqualToSize(_preferredResolution, resolution)) {
        _preferredResolution = resolution;
        
        if (self.session != nil) {
            // Restart playback when resolution is changed.
            [self stop];
            [self play];
        }
    }
}

- (void)setPointOfInterest:(CGPoint)point {
    dispatch_async(_snapQueue, ^{
        NSError *error = nil;
        if ([self.device lockForConfiguration:&error]) {
            if ([self.device isFocusPointOfInterestSupported] &&
                [self.device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
                [self.device setFocusMode:AVCaptureFocusModeAutoFocus];
                [self.device setFocusPointOfInterest:point];
            }
            
            if ([self.device isExposurePointOfInterestSupported] &&
                [self.device isExposureModeSupported:AVCaptureExposureModeAutoExpose]) {
                [self.device setExposureMode:AVCaptureExposureModeAutoExpose];
                [self.device setExposurePointOfInterest:point];
            }
            
            [self.device setSubjectAreaChangeMonitoringEnabled:YES];
            [self.device unlockForConfiguration];
        }
    });
}

#pragma mark - Private methods

- (void)subjectAreaDidChange:(NSNotification *)notification {
    [self setPointOfInterest:CGPointMake(0.5, 0.5)];
}

- (NSString *)actualSessionPreset {
    int preferredWidth = round(_preferredResolution.width);
    int preferredHeight = round(_preferredResolution.height);
    
    if (CGSizeEqualToSize(_preferredResolution, CGSizeZero)) {
        return AVCaptureSessionPresetPhoto;
    } else if (preferredWidth * 3 == preferredHeight * 4) { // 4:3
        return AVCaptureSessionPreset640x480;
    } else { // 16:9, etc.
        return AVCaptureSessionPreset1280x720;
    }
}

@end
