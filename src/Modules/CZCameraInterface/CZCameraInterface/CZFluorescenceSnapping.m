//
//  CZFluorescenceSnapping.m
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFluorescenceSnapping.h"
#import "CZFluorescenceSnappingPrivate.h"
#import "CZCamera.h"

NSString * const CZFluorescenceSnappingTemplateDidSaveNotification = @"CZFluorescenceSnappingTemplateDidSaveNotification";

@interface CZFluorescenceSnappingTemplate ()

@property (nonatomic, readwrite, copy) NSArray<CZCameraSnappingInfo *> *snappingInfos;

@end

@implementation CZFluorescenceSnappingTemplate

+ (instancetype)emptyTemplate {
    return [[[self alloc] init] autorelease];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _snappingInfos = [[NSArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_snappingInfos release];
    [super dealloc];
}

- (void)updateWithSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    if (snappingInfo == nil) {
        return;
    }
    
    NSMutableArray<CZCameraSnappingInfo *> *snappingInfos = [[self.snappingInfos mutableCopy] autorelease];
    [snappingInfos enumerateObjectsUsingBlock:^(CZCameraSnappingInfo *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.lightSource isEqualToLightSource:snappingInfo.lightSource]) {
            [snappingInfos removeObjectAtIndex:idx];
            *stop = YES;
        }
    }];
    [snappingInfos addObject:snappingInfo];
    self.snappingInfos = [[snappingInfos copy] autorelease];
}

- (CZCameraSnappingInfo *)snappingInfoForLightSource:(CZCameraLightSource *)lightSource {
    for (CZCameraSnappingInfo *snappingInfo in self.snappingInfos) {
        if ([snappingInfo.lightSource isEqualToLightSource:lightSource]) {
            return snappingInfo;
        }
    }
    return nil;
}

#pragma mark - Copying

- (id)copyWithZone:(NSZone *)zone {
    CZFluorescenceSnappingTemplate *flTemplate = [[CZFluorescenceSnappingTemplate alloc] init];
    flTemplate.snappingInfos = [[self.snappingInfos copy] autorelease];
    return flTemplate;
}

#pragma mark - Coding

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.snappingInfos forKey:@"snappingInfos"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _snappingInfos = [[aDecoder decodeObjectOfClass:[NSArray class] forKey:@"snappingInfos"] copy] ?: [[NSArray alloc] init];
    }
    return self;
}

@end
