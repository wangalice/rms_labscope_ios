//
//  CZRemoteStreamCamera.h
//  Matscope
//
//  Created by Sherry Xu on 5/29/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZRTSPCamera.h"

@interface CZRemoteStreamCamera : CZRTSPCamera

/** binding port, so that relay server should send UDP packet to this port.*/
@property (nonatomic, assign, readonly) uint16_t bindingPorts;

@end
