//
//  CZPlusCamera.h
//  Matscope
//
//  Created by Sherry Xu on 4/15/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import "CZCamera.h"

extern NSString * const CZPlusCameraMACAddressPrefix;

/*!
* A mock camera which is used to add fixed IP address camera.
*/
@interface CZPlusCamera : CZCamera

@end
