//
//  CZLocalFileCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLocalFileCamera.h"
#import "CZCameraSubclass.h"
#import <CZToolbox/CZToolbox.h>
#import <CZVideoEngine/CZVideoEngine.h>

//TODO: Support the Vistual camera from the Matscope
//#define kSampleVideoWidth 960
//#define kSampleVideoHeight 720

#define kSampleVideoWidth 1280
#define kSampleVideoHeight 720


@interface CZLocalFileCamera () {
    CZHardwareFileDecoder *_decoder;
}

@property (nonatomic, retain) NSURL *localFileURL;

- (void)playLocalFile;

@end

@implementation CZLocalFileCamera

- (id)init {
    NSURL *sampleURL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"mp4"];
    return [self initWithURL:sampleURL];
}

- (id)initWithURL:(NSURL *)url {
    self = [super init];
    if (self) {
        [self setName:@"Virtual" andUpload:NO];
        self.localFileURL = url;
        
        _decoder = [[CZHardwareFileDecoder alloc] init];
        [_decoder setDelegate:self];
    }
    return self;
}

- (void)dealloc {
    [_localFileURL release];
    
    [_decoder stop];
    [_decoder release];
    [super dealloc];
}

#pragma mark - Overridden methods

/*! Override the getter to make mock camera never lost. */
- (NSDate *)lastActiveTime {
    return [NSDate date];
}

- (UIImage *)thumbnail {
    return [UIImage imageNamed:@"sample-thumbnail"];
}

- (void)play {
    [_decoder start];
    [self playLocalFile];
}

- (void)stop {
    [_decoder stop];
}

- (void)pause {
    [_decoder pause];
}

- (void)resume {
    [_decoder resume];
}

- (BOOL)isPaused {
    return _decoder.isPaused;
}

- (CGSize)snapResolution {
    return CGSizeMake(kSampleVideoWidth, kSampleVideoHeight);
}

- (CGSize)liveResolution {
    return CGSizeMake(kSampleVideoWidth, kSampleVideoHeight);
}

- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        // Simulate real snapping progress.
        sleep(0.5);
        
        if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                @autoreleasepool {
                    NSError *error = nil;
                    UIImage *frame = [[_decoder lastDecodedFrame] retain];
                    if (frame == nil) {
                        error = [NSError errorWithDomain:CZCameraErrorDomain
                                                    code:kCZCameraTimeoutError
                                                userInfo:nil];
                    }
                    for (NSUInteger i = 0; i < frameCount; ++i) {
                        CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                        snappingInfo.image = frame;
                        snappingInfo.grayscale = NO;
                        [self.delegate camera:self didFinishSnapping:snappingInfo error:error];
                        [snappingInfo release];
                    }
                    [frame release];
                }
            });
        }
    });
}

#pragma mark - Private methods

- (void)playLocalFile {
    CZFileVideoSegment *segment = [[CZFileVideoSegment alloc] initWithURL:_localFileURL];
    [segment switchToStatus:kCZVideoSegmentReadyForReading];
    [_decoder decodeVideoSegment:segment];
    [segment release];
}

#pragma mark - CZHardwareDecoderDelegate methods

- (void)hardwareDecoder:(id<CZHardwareDecoder>)decoder didGetFrame:(UIImage *)frame {
    self.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
    
    if ([self.delegate respondsToSelector:@selector(camera:didGetVideoFrame:)]) {
        [self.delegate camera:self didGetVideoFrame:frame];
    }
}

- (void)hardwareDecoderDidFinishDecodingVideoSegment:(id<CZHardwareDecoder>)decoder {
    [self playLocalFile];
}

@end
