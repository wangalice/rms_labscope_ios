//
//  CZDefaultSettings+FluorescenceSnapping.h
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZToolbox/CZToolbox.h>

/*! Fluorescence snapping template. since v14 [CZFluorescenceSnappingTemplate] */
#define kFluorescenceSnappingTemplateKey @"fluorescence_snapping_template_%@"

@class CZCamera;
@class CZFluorescenceSnappingTemplate;

@interface CZDefaultSettings (FluorescenceSnapping)

- (CZFluorescenceSnappingTemplate *)fluorescenceSnappingTemplateForCamera:(CZCamera *)camera;
- (void)setFluorescenceSnapTemplate:(CZFluorescenceSnappingTemplate *)flTemplate forCamera:(CZCamera *)camera;

@end
