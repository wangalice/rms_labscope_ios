//
//  CZPlusCamera.m
//  Matscope
//
//  Created by Sherry Xu on 4/15/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import "CZPlusCamera.h"
#import <CZToolbox/CZToolbox.h>

NSString * const CZPlusCameraMACAddressPrefix = @"00:20:0D:F9:00";

@implementation CZPlusCamera

- (id)init {
    self = [super init];
    if (self) {
        [self setName:@"Manually add a microscope" andUpload:NO];
    }
    return self;
}

- (UIImage *)thumbnail {
    return nil;
}

- (NSDate *)lastActiveTime {
    return [NSDate date];
}

@end
