//
//  CZRemoteStreamCamera.m
//  Matscope
//
//  Created by Sherry Xu on 5/29/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZRemoteStreamCamera.h"
#import <CZVideoEngine/CZVideoEngine.h>

@implementation CZRemoteStreamCamera

+ (Class)clientClass {
    return [CZRTPFrameReceiver class];
}

- (id)init {
    self = [super init];
    if (self) {
        [self setName:@"Receive stream" andUpload:NO];
        self.ipAddress = @"localhost"; // TODO: placehold for hacking
    }
    return self;
}

- (UIImage *)thumbnail {
    return nil;
}

- (uint16_t)bindingPorts {
    return ((CZRTPFrameReceiver *)self.client).bindingPorts;
}

- (void)startRelayToURL:(NSURL *)relayURL {
    // disable relay mode, since it's already receiver of relay sender.
}

- (NSDate *)lastActiveTime {
    return [NSDate date];
}

@end
