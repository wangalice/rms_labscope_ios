//
//  CZLocalFileCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCamera.h"
#import <CZVideoEngine/CZVideoEngine.h>

/*!
 * Represents a mock camera which actually plays a local video file according to
 * the provided URL when initialized. This is for demo purpose only.
 */
@interface CZLocalFileCamera : CZCamera<CZHardwareDecoderDelegate>

/*! Initializes the local file camera object with a certain video file URL. */
- (id)initWithURL:(NSURL *)url;

@end
