//
//  CZCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, CZCameraCapability) {
    CZCameraCapabilityNone                      = 0,
    CZCameraCapabilityResetAll                  = 1 << 0,
    
    CZCameraCapabilityExposureMode              = 1 << 1,
    CZCameraCapabilityExposureTime              = 1 << 2,
    CZCameraCapabilityExtraExposureTime         = 1 << 3,
    CZCameraCapabilityGain                      = 1 << 4,
    CZCameraCapabilityExposureIntensity         = 1 << 5,
    CZCameraCapabilityExposureTimeAutoAdjust    = 1 << 6,
    CZCameraCapabilityWhiteBalanceMode          = 1 << 7,
    CZCameraCapabilityColorTemperature          = 1 << 8,
    CZCameraCapabilityWhiteBalanceAutoAdjust    = 1 << 9,
    CZCameraCapabilityIllumination              = 1 << 10,
    CZCameraCapabilityLightIntensity            = 1 << 11,
    
    CZCameraCapabilityStreamingQuality          = 1 << 12,
    CZCameraCapabilityImageOrientation          = 1 << 13,
    CZCameraCapabilitySnapResolution            = 1 << 14,
    CZCameraCapabilitySharpening                = 1 << 15,
    CZCameraCapabilityDenoise                   = 1 << 16,
    CZCameraCapabilityPixelCorrection           = 1 << 17,
    CZCameraCapabilityHDR                       = 1 << 18,
    CZCameraCapabilityBitDepth                  = 1 << 19,
    CZCameraCapabilityGrayscaleMode             = 1 << 20,
    CZCameraCapabilityGamma                     = 1 << 21,
    CZCameraCapabilityWebConfiguration          = 1 << 22,
    
    CZCameraCapabilityExclusiveLock             = 1 << 23,
    CZCameraCapabilityIFrameInterval            = 1 << 24,
    
    CZCameraCapabilityEncoding                  = 1 << 25,
    CZCameraCapabilityShadingCorrection         = 1 << 26
};

typedef NS_ENUM(NSUInteger, CZCameraExposureMode) {
    kCZCameraExposureAutomatic = 0,
    kCZCameraExposureManual = 1,
    kCZCameraExposureMixed = 2
};

typedef NS_ENUM(NSUInteger, CZCameraWhiteBalanceMode) {
    CZCameraWhiteBalanceModeAutomatic = 0,
    CZCameraWhiteBalanceModeManual = 1,
    CZCameraWhiteBalanceModeEyepieces = 2
};

typedef NS_ENUM(NSUInteger, CZCameraBitRate) {
    kCZCameraBitRateHigh = 0,
    kCZCameraBitRateMedium = 1,
    kCZCameraBitRateLow = 2
};

typedef NS_ENUM(NSUInteger, CZCameraImageOrientation) {
    kCZCameraImageOrientationOriginal = 0,
    kCZCameraImageOrientationFlippedHorizontally = 1,
    kCZCameraImageOrientationFlippedVertically = 2,
    kCZCameraImageOrientationRotated180 = 3
};

typedef NS_ENUM(NSUInteger, CZCameraSnapResolutionPreset) {
    kCZCameraSnapResolutionHigh = 0,
    kCZCameraSnapResolutionLow = 1,
    
    kCZCameraSnapResolutionCount
};

typedef NS_ENUM(NSUInteger, CZCameraBitDepth) {
    CZCameraBitDepth8Bit,
    CZCameraBitDepth12Bit,
    CZCameraBitDepthCount
};

typedef NS_ENUM(NSUInteger, CZCameraIFrameRatio) {
    kCZCameraIFrameRatioLow = 0,
    kCZCameraIFrameRatioHigh = 1,
};

typedef NS_ENUM(NSUInteger, CZCameraLightPath) {
    CZCameraLightPathUnknown = 0,
    CZCameraLightPathReflected = 1,
    CZCameraLightPathTransmitted = 2
};

typedef NS_ENUM(NSUInteger, CZCameraLightSourceType) {
    CZCameraLightSourceTypeUnknown,
    CZCameraLightSourceTypeColibri3,
    CZCameraLightSourceTypeFL_LED,
    CZCameraLightSourceTypeRL_LED,
    CZCameraLightSourceTypeTL_LED,
    CZCameraLightSourceTypeRL_Halogen,
    CZCameraLightSourceTypeTL_Halogen,
    CZCameraLightSourceTypeRL_External,
    CZCameraLightSourceTypeTL_External
};

typedef NS_ENUM(NSInteger, CZCameraControlType) {
    CZCameraControlTypeSnap,
    CZCameraControlTypeConfiguration
};

extern NSString * const CZCameraErrorDomain;

extern NSString * const CZCameraStreamingStartNotification;
extern NSString * const CZCameraStreamingStopNotification;

extern NSString * const CZCameraExposureTimeKey;
extern NSString * const CZCameraGainKey;
extern NSString * const CZCameraWhiteBalanceValueKey;

typedef NS_ENUM(NSUInteger, CZCameraSnappingPauseReason) {
    CZCameraNeedSwitchLightSourceByUser
};

typedef NS_ENUM(NSUInteger, CZCameraErrorCode) {
    kCZCameraTimeoutError = 1,
    kCZCameraNoControlError = 2,
    kCZCameraUserCancelledError = 3,
    kCZCameraInterrupttedError = 4,
    kCZCameraIncompatibleFirmwareVersionError = 5
};

@class CZCamera;
@class CZCameraControlLock;
@class CZTimelapseRecorder;

@interface CZCameraLightSource : NSObject <NSCopying, NSSecureCoding>

@property (nonatomic, readonly, assign) NSUInteger position;
@property (nonatomic, readonly, assign) NSUInteger wavelength;
@property (nonatomic, readonly, assign) BOOL isOn;
@property (nonatomic, readonly, assign) BOOL canOn;

- (instancetype)initWithPosition:(NSUInteger)position wavelength:(NSUInteger)wavelength isOn:(BOOL)isOn canOn:(BOOL)canOn;
- (BOOL)isEqualToLightSource:(CZCameraLightSource *)otherLightSource;

@end

@interface CZCameraSnappingInfo : NSObject <NSCopying, NSSecureCoding>

@property (nonatomic, readonly, assign) NSUInteger currentSnappingNumber; // Start from 1
@property (nonatomic, readonly, assign) NSUInteger totalSnappingNumber;

@property (nonatomic, readonly, retain) UIImage *image;
@property (nonatomic, readonly, assign) BOOL grayscale;

@property (nonatomic, readonly, assign) CZCameraExposureMode exposureMode;
@property (nonatomic, readonly, retain) NSNumber *exposureTimeInMilliseconds;
@property (nonatomic, readonly, retain) NSNumber *gain;
@property (nonatomic, readonly, assign) NSUInteger exposureIntensity;

@property (nonatomic, readonly, retain) CZCameraLightSource *lightSource;
@property (nonatomic, readonly, assign) CZCameraLightSourceType lightSourceType;
@property (nonatomic, readonly, assign) NSUInteger lightIntensity;

@property (nonatomic, readonly, copy)   NSString *filterSetID;

- (instancetype)snappingInfoByTransformingImageToImage:(UIImage *)image;

- (void)updateImage:(UIImage *)image;

@end

@protocol CZCameraDelegate <NSObject>

@optional
- (BOOL)cameraShouldDeliverVideoFrame:(CZCamera *)camera;
- (void)camera:(CZCamera *)camera didGetControlBeforeSnapping:(CZCameraControlLock *)lock;
- (void)camera:(CZCamera *)camera didGetVideoFrame:(UIImage *)frame;
- (void)camera:(CZCamera *)camera didPauseSnapping:(CZCameraSnappingInfo *)snappingInfo reason:(CZCameraSnappingPauseReason)reason;
- (void)camera:(CZCamera *)camera didFinishSnapping:(CZCameraSnappingInfo *)snappingInfo error:(NSError *)error;
- (void)cameraDidFinishRecording:(CZCamera *)camera;

@end

@protocol CZCameraThumbnailDelegate <NSObject>

@optional
- (void)camera:(CZCamera *)camera didUpdateThumbnail:(UIImage *)thumbnail;

@end

@protocol CZCameraControlDelegate <NSObject>

@optional
- (void)cameraDidFinishExposureTimeAutoAdjust:(CZCamera *)camera;
- (void)cameraDidFinishSwitchToManualExposureMode:(CZCamera *)camera userInfo:(NSDictionary *)userInfo; // CZCameraExposureTimeKey and CZCameraGainKey in userInfo.
- (void)cameraDidFinishWhiteBalanceAutoAdjust:(CZCamera *)camera userInfo:(NSDictionary *)userInfo; // CZCameraWhiteBalanceValueKey in userInfo if there is.
- (void)cameraDidFinishReset:(CZCamera *)camera;

@end

@protocol CZCameraShadingCorrectionDelegate <NSObject>

@optional
- (void)cameraDidCalibrateShadingCorrection:(CZCamera *)camera;
- (void)cameraDidFailToCalibrateShadingCorrection:(CZCamera *)camera;
- (void)cameraDidEnableShadingCorrection:(CZCamera *)camera;
- (void)cameraDidFailToEnableShadingCorrection:(CZCamera *)camera;
- (void)cameraDidDisableShadingCorrection:(CZCamera *)camera;
- (void)cameraDidFailToDisableShadingCorrection:(CZCamera *)camera;

@end

/*!
 * CZCamera is the abstract model for all kinds of camera objects. It defines
 * the common interfaces for the camera-related requirements in the project,
 * hides the difference among cameras and provides default implementations.
 */
@interface CZCamera : NSObject

@property (atomic, assign) id<CZCameraDelegate> delegate;
@property (atomic, assign) id<CZCameraThumbnailDelegate> thumbnailDelegate;
@property (atomic, assign) id<CZCameraControlDelegate> controlDelegate;
@property (atomic, assign) id<CZCameraShadingCorrectionDelegate> shadingCorrectionDelegate;

@property (atomic, copy) NSString *ipAddress;
@property (atomic, copy) NSString *macAddress;

@property (atomic, retain) NSDate *lastActiveTime;
/** Time of recent video frame received. Use this property to check if the camera's vedio stream is alive.*/
@property (atomic, retain) NSDate *lastFrameTime;

/** Exclusive camera control lock.
 * This is avaiable when camera has CZCameraCapabilityExclusiveLock capability.
 */
@property (atomic, retain) CZCameraControlLock *exclusiveLock;

@property (atomic, retain, readonly) CZTimelapseRecorder *timelapseRecorder;

/*! Determines whether the two objects refer to the same physical camera. */
- (BOOL)isEqual:(CZCamera *)otherCamera;

/*! A combined mask of camera capability options. Subclasses should override this. */
- (CZCameraCapability)capabilities;

/*! Easy way to check whether the camera has a specific capability. */
- (BOOL)hasCapability:(CZCameraCapability)capability;

/*! Gets the firmware version of the camera. */
- (NSString *)firmwareVersion;

/*! Checks whether the firmware version is latest. */
- (BOOL)isFirmwareUpdated;

/*! Updates the firmware to latest built-in version. */
- (void)updateFirmware;

/*! Checks whether the firmware version is compatible for snapping. */
- (BOOL)isFirmwareCompatibleForSnapping:(NSError **)error;

/*! Gets the display name of the camera. */
- (NSString *)displayName;

/*! Gets the mock display name which is extracted from MAC address. */
- (NSString *)mockDisplayName;

/*! Changes the display name of the camera. */
- (void)setDisplayName:(NSString *)displayName andUpload:(BOOL)shouldUpload;

/*! Sets the full name of the camera, which consists both PIN and display name. */
- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload;

/*! Gets the maximum supported length for the camera name (excluding PIN). */
- (NSUInteger)maxSupportedNameLength;

/*! Gets the thumbnail of camera. */
- (UIImage *)thumbnail;

/*! Gets the thumbnail from the live stream. */
- (UIImage *)liveThumbnail;
- (void)setLiveThumbnail:(UIImage *)liveThumbnail;

/*! Refreshes the thumbnail of camera. */
- (void)beginThumbnailUpdating:(CGSize)thumbnailSize;

/*! Prefetch required camera info, return No if failed. */
- (BOOL)prefetchRequiredCameraInfo;

/*! Starts playing the video stream from camera. */
- (void)play;

/*! Stops playing the video stream from camera. */
- (void)stop;

/*! Pauses playing the video stream from camera. */
- (void)pause;

/*! Resumes playing the video stream from camera. */
- (void)resume;

/*! Tells whether the video stream playing is paused at the moment. */
- (BOOL)isPaused;

/*! start relay to URL with UDP socket.
 * @param relayURL destination URL, e.g. // rtsp://192.168.1.100:554
 */
- (void)startRelayToURL:(NSURL *)relayURL;
- (void)endRelay;

/*! Asynchronously snaps a still image. */
- (void)beginSnapping;

/*! Asynchronously snaps n still images. */
- (void)beginContinuousSnapping:(NSUInteger)frameCount;

/*! Asynchronously snaps a single frame from live video. */
- (void)beginSnappingFromVideoStream;

/*! Asynchronously snaps n frames from live video. */
- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount;

/*! Starts recording the video stream from camera to a certain file. */
- (void)beginRecordingToFile:(NSString *)filePath;

/*! Ends recording the video stream. */
- (void)endRecording;

/*! Starts time lapse recording the video stream from camera to a certain file.
 * @param frameRate the frame rate of recorded video, the lowest frame rate is 5.
 * @param interval the time interval between each frame captured from source, minmum is 1 second.
 * @param overlayer the additional overlay to burn-in to video. can be nil;
 */
- (void)beginTimelapseRecordingToFile:(NSString *)filePath
                          inFrameRate:(NSUInteger)frameRate
                             interval:(NSTimeInterval)interval
                            overlayer:(CALayer *)overlayer;

/*! Ends time lapse recording the video stream. */
- (void)endTimelapseRecording;

- (void)handoverTimelapseRecorderFromCamera:(CZCamera *)camera;

/*! Returns the current video resolution of camera. */
- (CGSize)liveResolution;

/*! Tries to retrieve control privilege from the camera.
 * NOTICE: don't call this method directly, use CZCameraControlLock for safety.
 */
- (BOOL)getControl:(CZCameraControlType)type;

/*! Tries to abandon the current control privilege.
 * NOTICE: don't call this method directly, use CZCameraControlLock for safety.
 */
- (BOOL)releaseControl:(CZCameraControlType)type;

/*! Start handling the event messages sent by camera. */
- (void)beginEventListening;

/*! No more handles the event messages sent by camera. */
- (void)endEventListening;

/*! Tells the camera to begin resetting for exposure time, gain, etc. */
- (void)beginReset;

/*! Gets the current security PIN stored on camera. */
- (NSString *)pin;

/*! Checks whether the camera has a PIN. */
- (BOOL)hasPIN;

/*! Checks whether the security PIN matches the one stored on camera. */
- (BOOL)validatePIN:(NSString *)pin;

/*! Changes the security PIN to a new value. */
- (void)setPIN:(NSString *)pin andUpload:(BOOL)shouldUpload;

/*! Tells the camera to restart itself. */
- (void)restart;

@end

@interface CZCamera (ExposureMode)

/*! Gets the current exposure mode. */
- (CZCameraExposureMode)exposureMode;

/*! Tells the camera to switch to a certain exposure mode. */
- (void)setExposureMode:(CZCameraExposureMode)exposureMode;

/*! Tells the camera to begin exposure time auto-adjust. */
- (void)beginExposureTimeAutoAdjust;

/*! Tells the camera to begin switch exposure mode from auto to manual. */
- (void)beginSwitchToManualExposureMode;

@end

@interface CZCamera (ExposureTime)

/*! Gets the minimum exposure time. */
- (float)minExposureTime;

/*! Gets the maximum exposure time. */
- (float)maxExposureTime;

/*! Gets all supported exposure time values between min and max. If all values are valid, return empty. */
- (NSArray<NSNumber *> *)supportedExposureTimeValues;

/*! Gets the nearest supported exposure time given preferred value. */
- (float)nearestValidExposureTime:(float)milliseconds;

/*! Gets the index of nearest supported exposure time given preferred value. */
- (NSUInteger)nearestValidExposureTimeIndex:(float)milliseconds;

/*! Gets the current exposure time. */
- (float)exposureTime;

/*! Sets the exposure time on camera. */
- (void)setExposureTime:(float)milliseconds;

/*! Gets the current snapping exposure time. */
- (float)snappingExposureTime;

/*! Sets the exposure time for snapping locally. */
- (void)setSnappingExposureTime:(float)milliseconds;

@end

@interface CZCamera (Gain)

/*! Gets the minimum gain. */
- (NSUInteger)minGain;

/*! Gets the maximum gain. */
- (NSUInteger)maxGain;

/*! Gets the nearest supported gain given preferred value. */
- (NSUInteger)nearestValidGain:(float)gain;

/*! Gets the current gain. */
- (NSUInteger)gain;

/*! Sets the gain on camera. */
- (void)setGain:(NSUInteger)gain;

@end

@interface CZCamera (ExposureIntensity)

/*! Gets the minimum exposure intensity. */
- (NSUInteger)minExposureIntensity;

/*! Gets the maximum exposure intensity. */
- (NSUInteger)maxExposureIntensity;

/*! Gets the default exposure intensity. */
- (NSUInteger)defaultExposureIntensity;

/*! Gets the current exposure intensity. */
- (NSUInteger)exposureIntensity;

/*! Sets the exposure intensity on camera. */
- (void)setExposureIntensity:(NSUInteger)exposureIntensity;

@end

@interface CZCamera (WhiteBalanceMode)

/*! Gets the current white balance mode. */
- (CZCameraWhiteBalanceMode)whiteBalanceMode;

/*! Sets the white balance mode on camera. */
- (void)setWhiteBalanceMode:(CZCameraWhiteBalanceMode)whiteBalanceMode;

/*! Returns whether the white-balance setting on camera is currently locked. */
- (BOOL)isWhiteBalanceLocked;

/*! Changes the status of the white-balance lock on camera. */
- (void)setIsWhiteBalanceLocked:(BOOL)locked;

/*! Tells the camera to begin white-balance auto-adjust. */
- (void)beginWhiteBalanceAutoAdjust;

@end

@interface CZCamera (ColorTemperature)

/*! Gets the coldest white balance value. */
- (NSUInteger)coldestWhiteBalanceValue;

/*! Gets the warmest white balance value. */
- (NSUInteger)warmestWhiteBalanceValue;

/*! Gets the nearest supported white balance value given preferred value. */
- (NSUInteger)nearestValidWhiteBalanceValue:(float)value;

/*! Gets the current white balance value. */
- (NSUInteger)whiteBalanceValue;

/*! Sets the white balance on camera. */
- (void)setWhiteBalanceWithValue:(NSUInteger)value;

@end

@interface CZCamera (LightIntensity)

/*! Gets whether has the light path. */
- (BOOL)hasLightPath:(CZCameraLightPath)lightPath;

/*! Gets the current light path. */
- (CZCameraLightPath)lightPath;

/*! Gets the type of reflected light. */
- (CZCameraLightSourceType)reflectedLightSourceType;

/*! Sets the current light path. */
- (void)setLightPath:(CZCameraLightPath)lightPath;

/*! Gets the minimum light intensity at the given light path. */
- (NSUInteger)minLightIntensityAtPath:(CZCameraLightPath)lightPath;

/*! Gets the maximum light intensity at the given light path. */
- (NSUInteger)maxLightIntensityAtPath:(CZCameraLightPath)lightPath;

/*! Gets the light intensity at the given light path. */
- (NSUInteger)lightIntensityAtPath:(CZCameraLightPath)lightPath;

/*! Sets the light intensity at the given light path. */
- (void)setLightIntensity:(NSUInteger)lightIntensity atPath:(CZCameraLightPath)lightPath;

@end

@interface CZCamera (StreamingQuality)

/*! Gets the actual number for a certain bit rate preset. */
+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate;

/*! Gets the default bit rate. */
- (CZCameraBitRate)defaultBitRate;

/*! Gets the current bit rate. */
- (CZCameraBitRate)bitRate;

/*! Sets the bit rate to the chosen mode. */
- (void)setBitRate:(CZCameraBitRate)bitRate;

@end

@interface CZCamera (ImageOrientation)

/*! Gets the default image orientation. */
- (CZCameraImageOrientation)defaultImageOrientation;

/*! Gets the current image orientation. */
- (CZCameraImageOrientation)imageOrientation;

/*! Sets the image orientation to a certain mode. */
- (void)setImageOrientation:(CZCameraImageOrientation)orientation;

@end

@interface CZCamera (SnapResolution)

/*! Returns the low snapshot resolution of camera. */
- (CGSize)lowSnapResolution;

/*! Returns the high snapshot resolution of camera. */
- (CGSize)highSnapResolution;

/*! Returns the current snapshot resolution of camera. */
- (CGSize)snapResolution;

/*! Gets the default resolution preset for snapping. */
- (CZCameraSnapResolutionPreset)defaultSnapResolutionPreset;

/*! Gets the resolution preset for snapping. */
- (CZCameraSnapResolutionPreset)snapResolutionPreset;

/*! Sets the resolution for still image to certain preset. */
- (void)setSnapResolutionPreset:(CZCameraSnapResolutionPreset)preset;

@end

@interface CZCamera (Sharpening)

/*! Gets the default sharpness step. */
- (uint32_t)defaultSharpness;

/*! Gets the number of possible sharpness step values. */
- (uint32_t)sharpnessStepCount;

/*! Gets the current sharpness step. */
- (uint32_t)sharpness;

/*! Sets the sharpness by step on camera. */
- (void)setSharpness:(uint32_t)sharpness;

@end

@interface CZCamera (Denoise)

/*! Gets the default value of denoise. */
- (BOOL)isDenoiseEnabledByDefault;

/*! Gets whether denoise is enabled currently for the camera. */
- (BOOL)isDenoiseEnabled;

/*! Sets whether the camera is denoise enabled. */
- (void)setDenoiseEnabled:(BOOL)enabled;

@end

@interface CZCamera (PixelCorrection)

/*! Gets the default value of pixel correction. */
- (BOOL)isPixelCorrectionEnabledByDefault;

/*! Gets whether pixel correction is enabled currently for the camera. */
- (BOOL)isPixelCorrectionEnabled;

/*! Sets whether the camera is pixel correction enabled. */
- (void)setPixelCorrectionEnabled:(BOOL)enabled;

@end

@interface CZCamera (HDR)

/*! Gets the default value of HDR. */
- (BOOL)isHDREnabledByDefault;

/*! Gets whether HDR is enabled currently for the camera. */
- (BOOL)isHDREnabled;

/*! Sets whether the camera is HDR enabled. */
- (void)setHDREnabled:(BOOL)enabled;

@end

@interface CZCamera (BitDepth)

/*! Gets the default bit depth. */
- (CZCameraBitDepth)defaultBitDepth;

/*! Gets the current bit depth. */
- (CZCameraBitDepth)bitDepth;

/*! Sets the bit depth locally. */
- (void)setBitDepth:(CZCameraBitDepth)bitDepth;

@end

@interface CZCamera (GrayscaleMode)

/*! Gets the default value of grayscale mode. */
- (BOOL)isGrayscaleModeEnabledByDefault;

/*! Gets whether grayscale mode is enabled currently for the camera. */
- (BOOL)isGrayscaleModeEnabled;

/*! Sets whether the camera should be in grayscale mode. */
- (void)setGrayscaleModeEnabled:(BOOL)enabled;

@end

@interface CZCamera (Gamma)

/*! Gets the minimum gamma. */
- (float)minGamma;

/*! Gets the maximum gamma. */
- (float)maxGamma;

/*! Gets the default gamma. */
- (float)defaultGamma;

/*! Gets the current gamma. */
- (float)gamma;

/*! Sets the gamma on camera. */
- (void)setGamma:(float)gamma;

@end

@interface CZCamera (WebConfiguration)

/*! The URL for the camera's web interface. */
- (NSURL *)webInterfaceURL;

@end

@interface CZCamera (IFrameInterval)

/*! Gets the IDR Frame intervel ratio. This parameter only affect with H.264 camera.*/
- (CZCameraIFrameRatio)IFrameRatio;

/*! Sets the IDR Frame intervel ratio. This parameter only affect with H.264 camera.*/
- (void)setIFrameRatio:(CZCameraIFrameRatio)iFrameRatio;

@end

@interface CZCamera (ShadingCorrection)

- (BOOL)isShadingCorrectionApplied;
- (void)calibrateShadingCorrection;
- (void)enableShadingCorrection;
- (void)disableShadingCorrection;

@end

/** camera control lock helper class.
 * This class call camera's |getControl| in |init|, and |releaseControl| in |dealloc|.
 */
@interface CZCameraControlLock : NSObject {
@private
    CZCameraControlType _type;
    CZCamera *_camera;
    BOOL _retained;
}

- (instancetype)initWithCamera:(CZCamera *)camera;

/**
 * @param retained if YES, camera is retained by CZCameraControlLock. Default is YES.
 */
- (instancetype)initWithType:(CZCameraControlType)type camera:(CZCamera *)camera retained:(BOOL)retained;

@end
