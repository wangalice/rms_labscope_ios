//
//  CZGeminiCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/2/17.
//  Copyright (c) 2017 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCamera.h"
#import "CZCameraSubclass.h"
#import "CZFluorescenceSnappingPrivate.h"
#import "CZDefaultSettings+FluorescenceSnapping.h"
#import <AFNetworking/AFHTTPClient.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <CZToolbox/CZToolbox.h>

NSString * const CZGeminiCameraMinFirmwareVersionForSnapping = @"1.3.5";

NSString * const CZGeminiCameraStatusDidChangeNotification = @"CZGeminiCameraStatusDidChangeNotification";
NSString * const CZGeminiCameraStatusKey = @"CZGeminiCameraStatusKey";
NSString * const CZGeminiCameraStatusChangesKey = @"CZGeminiCameraStatusChangesKey";

NSString * const CZGeminiCameraDidReceiveEventNotification = @"CZGeminiCameraDidReceiveEventNotification";
NSString * const CZGeminiCameraEventKey = @"CZGeminiCameraEventKey";
NSString * const CZGeminiCameraClientNumberKey = @"CZGeminiCameraClientNumberKey";

@interface CZGeminiCamera () <CZGeminiWebSocketClientDelegate> {
    NSUInteger _controlRetainCount[CZCameraControlTypeConfiguration + 1];
    NSObject *_controlRetainCountLock;
    
    dispatch_semaphore_t _snapSemaphore;
    dispatch_semaphore_t _exposureTimeAutoAdjustSemaphore;
    
    NSOperationQueue *_onekeyFluorescenceSnapQueue;
    dispatch_semaphore_t _onekeyFluorescenceSnapSemaphore;
}

@property (nonatomic, readwrite, retain) CZGeminiAPISystemInfo *systemInfo;
@property (nonatomic, retain) NSTimer *heartBeatTimer;
@property (nonatomic, retain) CZCameraControlLock *fluorescenceSnapLock;
@property (nonatomic, retain) CZCameraControlLock *fluorescenceConfigurationLock;
@property (nonatomic, assign) CZFluorescenceSnappingMode fluorescenceSnappingMode;
@property (nonatomic, assign) id<CZFluorescenceSnappingDelegate> fluorescenceSnappingDelegate;
@property (nonatomic, retain) CZFluorescenceSnappingTemplate *temporaryFluorescenceSnappingTemplate;
@property (nonatomic, assign, getter=isOnekeyFluorescenceSnapCancelled) BOOL onekeyFluorescenceSnapCancelled;
@property (nonatomic, retain) NSArray<CZCameraLightSource *> *cachedLightSources;
@property (nonatomic, retain) NSMutableDictionary<NSNumber *, NSNumber *> *cachedLightIntensities;

@end

@implementation CZGeminiCamera

@synthesize apiClient = _apiClient;
@synthesize webSocketClient = _webSocketClient;
@synthesize manualFluorescenceSnappingTemplate = _manualFluorescenceSnappingTemplate;
@synthesize onekeyFluorescenceSnappingTemplate = _onekeyFluorescenceSnappingTemplate;

- (instancetype)init {
    self = [super init];
    if (self) {
        _controlRetainCount[CZCameraControlTypeSnap] = 0;
        _controlRetainCount[CZCameraControlTypeConfiguration] = 0;
        _controlRetainCountLock = [[NSObject alloc] init];
        
        _onekeyFluorescenceSnapQueue = [[NSOperationQueue alloc] init];
        _onekeyFluorescenceSnapQueue.maxConcurrentOperationCount = 1;
        
        _fluorescenceSnappingMode = CZFluorescenceSnappingModeNone;
        _cachedLightIntensities = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_apiClient release];
    [_webSocketClient release];
    [_manualFluorescenceSnappingTemplate release];
    [_onekeyFluorescenceSnappingTemplate release];
    [_temporaryFluorescenceSnappingTemplate release];
    [_systemInfo release];
    [_controlRetainCountLock release];
    if (_snapSemaphore) {
        dispatch_release(_snapSemaphore);
    }
    if (_exposureTimeAutoAdjustSemaphore) {
        dispatch_release(_exposureTimeAutoAdjustSemaphore);
    }
    [_heartBeatTimer invalidate];
    [_heartBeatTimer release];
    [_fluorescenceSnapLock release];
    [_fluorescenceConfigurationLock release];
    [_onekeyFluorescenceSnapQueue release];
    if (_onekeyFluorescenceSnapSemaphore) {
        dispatch_release(_onekeyFluorescenceSnapSemaphore);
    }
    [_cachedLightSources release];
    [_cachedLightIntensities release];
    
    [super dealloc];
}

#pragma mark - API Client & Web Socket Client

- (CZGeminiAPIClient *)apiClient {
    if (_apiClient == nil) {
        _apiClient = [[CZGeminiAPIClient alloc] initWithIPAddress:self.ipAddress];
    }
    return _apiClient;
}

- (CZGeminiWebSocketClient *)webSocketClient {
    if (_webSocketClient == nil) {
        _webSocketClient = [[CZGeminiWebSocketClient alloc] initWithIPAddress:self.ipAddress delegate:self];
    }
    return _webSocketClient;
}

#pragma mark - Overridden methods

- (void)setIpAddress:(NSString *)ipAddress {
    [super setIpAddress:ipAddress];
    
    // Invalidate the client if IP address changed.
    if (_apiClient && ![_apiClient.ipAddress isEqualToString:ipAddress]) {
        [_apiClient release];
        _apiClient = nil;
    }
    if (_webSocketClient && ![_webSocketClient.ipAddress isEqualToString:ipAddress]) {
        [_webSocketClient release];
        _webSocketClient = nil;
    }
}

- (CZCameraCapability)capabilities {
    CZCameraCapability capabilities = (CZCameraCapabilityResetAll |
                                       CZCameraCapabilityExposureMode |
                                       CZCameraCapabilityExposureTime |
                                       CZCameraCapabilityGain |
                                       CZCameraCapabilityExposureIntensity |
                                       CZCameraCapabilityExposureTimeAutoAdjust |
                                       CZCameraCapabilityStreamingQuality |
                                       CZCameraCapabilityImageOrientation |
                                       CZCameraCapabilitySharpening |
                                       CZCameraCapabilityDenoise |
                                       CZCameraCapabilityPixelCorrection |
                                       CZCameraCapabilityGamma |
                                       CZCameraCapabilityExclusiveLock);
    if ([self.systemInfo.camera modelType] == CZGeminiAPICameraModelTypeAxiocam202) {
        capabilities |= CZCameraCapabilityBitDepth;
    }
    if ([self.systemInfo.camera modelType] == CZGeminiAPICameraModelTypeAxiocam208) {
        capabilities |= CZCameraCapabilityWhiteBalanceMode;
        capabilities |= CZCameraCapabilityColorTemperature;
        capabilities |= CZCameraCapabilityWhiteBalanceAutoAdjust;
        capabilities |= CZCameraCapabilitySnapResolution;
        capabilities |= CZCameraCapabilityHDR;
        capabilities |= CZCameraCapabilityGrayscaleMode;
    }
    if ([self.systemInfo.microscope.microscopeName hasPrefix:@"Axioscope"] || [self.systemInfo.microscope.microscopeName hasPrefix:@"Axiolab"]) {
        capabilities |= CZCameraCapabilityLightIntensity;
        capabilities |= CZCameraCapabilityEncoding;
        capabilities |= CZCameraCapabilityShadingCorrection;
    }
    return capabilities;
}

- (NSString *)firmwareVersion {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetFirmwareVersionInfo];
    CZGeminiAPIFirmwareVersionInfo *firmwareVersionInfo = [self.apiClient callAPI:api];
    return firmwareVersionInfo.firmwareVersion;
}

- (BOOL)isFirmwareCompatibleForSnapping:(NSError **)error {
    NSString *currentFirmwareVersion = [self firmwareVersion];
    if ([CZGeminiCameraMinFirmwareVersionForSnapping compare:currentFirmwareVersion options:NSNumericSearch] == NSOrderedDescending) {
        if (error) {
            *error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraIncompatibleFirmwareVersionError userInfo:nil];
        }
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)prefetchRequiredCameraInfo {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetSystemInfo];
    self.systemInfo = [self.apiClient callAPI:api];
    return self.systemInfo != nil;
}

- (void)play {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetResolution:1];
    [self.apiClient callAPIAsync:api];
    [super play];
}

- (void)beginContinuousSnapping:(NSUInteger)frameCount {
    id delegate = self.delegate;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            CZCameraSnappingInfo *snappingInfo = nil;
            NSError *error = nil;
            CZCameraControlLock *lock = [[CZCameraControlLock alloc] initWithCamera:self];
            if (lock) {
                if ([delegate respondsToSelector:@selector(camera:didGetControlBeforeSnapping:)]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [delegate camera:self didGetControlBeforeSnapping:lock];
                    });
                }
                
                CZCameraExposureMode exposureMode = [self exposureMode];
                float exposureTime = 0.0;
                NSUInteger gain = 0;
                if (exposureMode == kCZCameraExposureManual) {
                    exposureTime = [self exposureTime];
                    gain = [self gain];
                }
                snappingInfo = [self snapWithExposureMode:exposureMode exposureTimeInMilliseconds:exposureTime gain:gain monochrome:[self isGrayscaleModeEnabled] error:&error];
                snappingInfo.lightSource = nil;
                
                [lock release];
            } else {
                error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraNoControlError userInfo:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                    [delegate camera:self didFinishSnapping:snappingInfo error:error];
                }
            });
        }
    });
}

- (CGSize)liveResolution {
    return CGSizeMake(1920, 1080);
}

- (void)beginEventListening {
    if ([self.webSocketClient connect]) {
        [self heartBeatTimerFired:nil];
        self.heartBeatTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:self
                                                             selector:@selector(heartBeatTimerFired:)
                                                             userInfo:nil
                                                              repeats:YES];
    }
}

- (void)endEventListening {
    if ([self.webSocketClient disconnect]) {
        [self.heartBeatTimer invalidate];
        self.heartBeatTimer = nil;
    }
}

- (NSURL *)rtspUrl {
    NSString *urlString = [[NSString alloc] initWithFormat:@"rtsp://%@:8554/axiocam", self.ipAddress];
    NSURL *url = [NSURL URLWithString:urlString];
    [urlString release];
    return url;
}

- (BOOL)getControl:(CZCameraControlType)type {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount[type] == 0) {
            if ([self startControl:type]) {
                _controlRetainCount[type] = 1;
                return YES;
            } else {
                return NO;
            }
        }
        
        ++_controlRetainCount[type];
        return YES;
    }
}

- (BOOL)releaseControl:(CZCameraControlType)type {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount[type] > 0) {
            --_controlRetainCount[type];
        } else {
            return NO;
        }
        
        if (_controlRetainCount[type] == 0) {
            [self endControl:type];
        }
        
        return YES;
    }
}

- (BOOL)startControl:(CZCameraControlType)type {
    switch (type) {
        case CZCameraControlTypeSnap: {
            CZGeminiAPI *api = [CZGeminiAPI apiForLockSnap];
            CZGeminiAPILockInfo *lockInfo = [self.apiClient callAPI:api];
            return lockInfo.lockStatus.integerValue == 1;
        }
        case CZCameraControlTypeConfiguration: {
            CZGeminiAPI *api = [CZGeminiAPI apiForLockConfiguration];
            CZGeminiAPILockInfo *lockInfo = [self.apiClient callAPI:api];
            return lockInfo.lockStatus.integerValue == 1;
        }
    }
}

- (void)endControl:(CZCameraControlType)type {
    switch (type) {
        case CZCameraControlTypeSnap: {
            CZGeminiAPI *api = [CZGeminiAPI apiForUnlockSnap];
            [self.apiClient callAPIAsync:api];
            break;
        }
        case CZCameraControlTypeConfiguration: {
            CZGeminiAPI *api = [CZGeminiAPI apiForUnlockConfiguration];
            [self.apiClient callAPIAsync:api];
            break;
        }
    }
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    [super setName:name andUpload:shouldUpload];
    if (shouldUpload && (name || name.length > 0)) {
        if ([CZWifiNotifier sharedInstance].isReachable) {
            CZGeminiAPI *api = [CZGeminiAPI apiForSetCameraName:name];
            [self.apiClient callAPIAsync:api];
        }
    }
}

#pragma mark - Exposure Mode

- (CZCameraExposureMode)exposureMode {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetExposureType];
    CZGeminiAPIExposureType exposureType = [[self.apiClient callAPI:api] integerValue];
    switch (exposureType) {
        case CZGeminiAPIExposureTypeAuto:
            return kCZCameraExposureAutomatic;
        case CZGeminiAPIExposureTypeManual:
            return kCZCameraExposureManual;
    }
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
    CZGeminiAPIExposureType exposureType = (exposureMode == kCZCameraExposureAutomatic) ? CZGeminiAPIExposureTypeAuto : CZGeminiAPIExposureTypeManual;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetExposureType:exposureType once:NO];
    [self.apiClient callAPIAsync:api];
}

- (void)beginExposureTimeAutoAdjust {
    if (_exposureTimeAutoAdjustSemaphore) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPI *aeTypeAPI = [CZGeminiAPI apiForSetExposureType:CZGeminiAPIExposureTypeManual once:YES];
        [self.apiClient callAPI:aeTypeAPI];
        
        CZGeminiAPI *aeStableAPI = [CZGeminiAPI apiForGetExposureStabilizationStatus];
        [self.apiClient callAPI:aeStableAPI];
        
        _exposureTimeAutoAdjustSemaphore = dispatch_semaphore_create(0);
        dispatch_semaphore_wait(_exposureTimeAutoAdjustSemaphore, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(CZGeminiAPITimeoutInterval * NSEC_PER_SEC)));
        dispatch_release(_exposureTimeAutoAdjustSemaphore);
        _exposureTimeAutoAdjustSemaphore = nil;
        
        CZGeminiCameraAutoExposureStableEvent *event = self.webSocketClient.latestEvent;
        if ([event isKindOfClass:[CZGeminiCameraAutoExposureStableEvent class]]) {
            [self setExposureTime:event.exposureTime.floatValue / 1000];
            [self setGain:event.gain.unsignedIntegerValue];
        }
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishExposureTimeAutoAdjust:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlDelegate cameraDidFinishExposureTimeAutoAdjust:self];
            });
        }
    });
}

- (void)beginSwitchToManualExposureMode {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPI *api = nil;
        
        api = [CZGeminiAPI apiForSetExposureType:CZGeminiAPIExposureTypeManual once:NO];
        [self.apiClient callAPI:api];
        
        api = [CZGeminiAPI apiForGetRealExposureTimeInfo];
        CZGeminiAPIRealExposureTimeInfo *realExposureTimeInfo = [self.apiClient callAPI:api];
        
        api = [CZGeminiAPI apiForSetExposureTime:realExposureTimeInfo.exposureTime.floatValue];
        [self.apiClient callAPI:api];
        
        api = [CZGeminiAPI apiForSetGain:realExposureTimeInfo.gain.unsignedIntegerValue];
        [self.apiClient callAPI:api];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.controlDelegate && [self.controlDelegate respondsToSelector:@selector(cameraDidFinishSwitchToManualExposureMode:userInfo:)]) {
                NSDictionary *userInfo = @{CZCameraExposureTimeKey : @(realExposureTimeInfo.exposureTime.floatValue / 1000),
                                           CZCameraGainKey : @(realExposureTimeInfo.gain.unsignedIntegerValue)};
                [self.controlDelegate cameraDidFinishSwitchToManualExposureMode:self userInfo:userInfo];
            }
        });
    });
}

#pragma mark - Exposure Time

- (float)minExposureTime {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
            return [super minExposureTime];
        case CZGeminiAPICameraModelTypeAxiocam202:
            return 1.0; // 0.296;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return 1.0; // 0.061;
    }
}

- (float)maxExposureTime {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
            return [super maxExposureTime];
        case CZGeminiAPICameraModelTypeAxiocam202:
            return 2000.0;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return 1000.0;
    }
}

- (float)nearestValidExposureTime:(float)milliseconds {
    float exposureTime = milliseconds;
    exposureTime = MAX(exposureTime, [self minExposureTime]);
    exposureTime = MIN(exposureTime, [self maxExposureTime]);
    return roundf(exposureTime);
}

- (float)exposureTime {
    CZCameraExposureMode exposureMode = [self exposureMode];
    if (exposureMode == kCZCameraExposureAutomatic) {
        return 0; // TODO: Currently not supported by camera.
    } else {
        CZGeminiAPI *api = [CZGeminiAPI apiForGetExposureTimeInfo];
        CZGeminiAPIExposureTimeInfo *exposureTimeInfo = [self.apiClient callAPI:api];
        return exposureTimeInfo.exposureTime.floatValue / 1000;
    }
    
    return [super exposureTime]; // Fallback to default implementation
}

- (void)setExposureTime:(float)milliseconds {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetExposureTime:milliseconds * 1000];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Gain

- (NSUInteger)minGain {
    return 1;
}

- (NSUInteger)maxGain {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
            return [super maxGain];
        case CZGeminiAPICameraModelTypeAxiocam202:
            return 16;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return 22;
    }
}

- (NSUInteger)gain {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetGainInfo];
    CZGeminiAPIGainInfo *gainInfo = [self.apiClient callAPI:api];
    return gainInfo.gain.unsignedIntegerValue;
}

- (void)setGain:(NSUInteger)gain {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetGain:gain];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Exposure Intensity

- (NSUInteger)minExposureIntensity {
    return CZGeminiAPIMinExposureCompenation;
}

- (NSUInteger)maxExposureIntensity {
    return CZGeminiAPIMaxExposureCompenation;
}

- (NSUInteger)defaultExposureIntensity {
    // 22% as default
    return CZGeminiAPIMinExposureCompenation + (CZGeminiAPIMaxExposureCompenation - CZGeminiAPIMinExposureCompenation) * 0.22;
}

- (NSUInteger)exposureIntensity {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetExposureCompenation];
    return [[self.apiClient callAPI:api] unsignedIntegerValue];
}

- (void)setExposureIntensity:(NSUInteger)exposureIntensity {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetExposureCompenation:exposureIntensity];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - White Balance Mode

- (CZCameraWhiteBalanceMode)whiteBalanceMode {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetWhiteBalanceMode];
    CZGeminiAPIWhiteBalanceMode geminiWhiteBalanceMode = [[self.apiClient callAPI:api] integerValue];
    switch (geminiWhiteBalanceMode) {
        case CZGeminiAPIWhiteBalanceModeAuto:
            return CZCameraWhiteBalanceModeAutomatic;
        case CZGeminiAPIWhiteBalanceModeManual:
            return CZCameraWhiteBalanceModeManual;
        case CZGeminiAPIWhiteBalanceModeAsEyepieces:
            return CZCameraWhiteBalanceModeEyepieces;
    }
}

- (void)setWhiteBalanceMode:(CZCameraWhiteBalanceMode)whiteBalanceMode {
    CZGeminiAPI *api = nil;
    switch (whiteBalanceMode) {
        case CZCameraWhiteBalanceModeAutomatic:
            api = [CZGeminiAPI apiForSetWhiteBalanceMode:CZGeminiAPIWhiteBalanceModeAuto once:NO];
            break;
        case CZCameraWhiteBalanceModeManual:
            api = [CZGeminiAPI apiForSetWhiteBalanceMode:CZGeminiAPIWhiteBalanceModeManual once:NO];
            break;
        case CZCameraWhiteBalanceModeEyepieces:
            api = [CZGeminiAPI apiForSetWhiteBalanceMode:CZGeminiAPIWhiteBalanceModeAsEyepieces once:NO];
            break;
    }
    [self.apiClient callAPIAsync:api];
}

- (BOOL)isWhiteBalanceLocked {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetWhiteBalanceMode];
    CZGeminiAPIWhiteBalanceMode whiteBalanceMode = [[self.apiClient callAPI:api] integerValue];
    return whiteBalanceMode == CZGeminiAPIWhiteBalanceModeManual;
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
    CZGeminiAPIWhiteBalanceMode whiteBalanceMode = locked ? CZGeminiAPIWhiteBalanceModeManual : CZGeminiAPIWhiteBalanceModeAuto;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetWhiteBalanceMode:whiteBalanceMode once:NO];
    [self.apiClient callAPIAsync:api];
}

- (void)beginWhiteBalanceAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPI *api = [CZGeminiAPI apiForSetWhiteBalanceMode:CZGeminiAPIWhiteBalanceModeManual once:YES];
        CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [self.apiClient callAPI:api];
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust:userInfo:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *userInfo = whiteBalanceInfo.colorTemperature ? @{CZCameraWhiteBalanceValueKey : whiteBalanceInfo.colorTemperature} : nil;
                [self.controlDelegate cameraDidFinishWhiteBalanceAutoAdjust:self userInfo:userInfo];
            });
        }
    });
}

#pragma mark - Color Temperature

- (NSUInteger)coldestWhiteBalanceValue {
    return 1500;
}

- (NSUInteger)warmestWhiteBalanceValue {
    return 15000;
}

- (NSUInteger)whiteBalanceValue {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetColorTemperature];
    return [[self.apiClient callAPI:api] integerValue];
}

- (void)setWhiteBalanceWithValue:(NSUInteger)value {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetColorTemperature:value];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Ligth Intensity

- (BOOL)hasLightPath:(CZCameraLightPath)lightPath {
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            return NO;
        case CZCameraLightPathReflected:
            return self.systemInfo.microscope.hasReflectedLightSwitch;
        case CZCameraLightPathTransmitted:
            return self.systemInfo.microscope.hasTransmittedLightSwitch;
    }
}

- (CZCameraLightPath)lightPath {
    CZGeminiAPI *rlAPI = [CZGeminiAPI apiForGetReflectedLightInfo];
    CZGeminiAPILightInfo *reflectedLightInfo = [self.apiClient callAPI:rlAPI];
    if ([reflectedLightInfo isLightOn]) {
        return CZCameraLightPathReflected;
    }
    
    CZGeminiAPI *tlAPI = [CZGeminiAPI apiForGetTransmittedLightInfo];
    CZGeminiAPILightInfo *transmittedLightInfo = [self.apiClient callAPI:tlAPI];
    if ([transmittedLightInfo isLightOn]) {
        return CZCameraLightPathTransmitted;
    }
    
    return CZCameraLightPathUnknown;
}

- (CZCameraLightSourceType)reflectedLightSourceType {
    if (self.systemInfo.microscope.hasColibri) {
        return CZCameraLightSourceTypeColibri3;
    } else if (self.systemInfo.microscope.hasFluorescenceLEDs) {
        return CZCameraLightSourceTypeFL_LED;
    } else {
        return CZCameraLightSourceTypeUnknown;
    }
}

- (void)setLightPath:(CZCameraLightPath)lightPath {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.status = @"on";
    
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            break;
        case CZCameraLightPathReflected: {
            CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
            [self.apiClient callAPIAsync:api];
            break;
        }
        case CZCameraLightPathTransmitted: {
            CZGeminiAPI *api = [CZGeminiAPI apiForSetTransmittedLightInfo:lightInfo];
            [self.apiClient callAPIAsync:api];
            break;
        }
    }
    
    [lightInfo release];
}

- (NSUInteger)minLightIntensityAtPath:(CZCameraLightPath)lightPath {
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            return 0;
        case CZCameraLightPathReflected: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetReflectedLightInfo];
            CZGeminiAPILightInfo *reflectedLightInfo = [self.apiClient callAPI:api];
            return reflectedLightInfo.brightnessMin.unsignedIntegerValue;
        }
        case CZCameraLightPathTransmitted: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetTransmittedLightInfo];
            CZGeminiAPILightInfo *transmittedLightInfo = [self.apiClient callAPI:api];
            return transmittedLightInfo.brightnessMin.unsignedIntegerValue;
        }
    }
}

- (NSUInteger)maxLightIntensityAtPath:(CZCameraLightPath)lightPath {
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            return 0;
        case CZCameraLightPathReflected: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetReflectedLightInfo];
            CZGeminiAPILightInfo *reflectedLightInfo = [self.apiClient callAPI:api];
            return reflectedLightInfo.brightnessMax.unsignedIntegerValue;
        }
        case CZCameraLightPathTransmitted: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetTransmittedLightInfo];
            CZGeminiAPILightInfo *transmittedLightInfo = [self.apiClient callAPI:api];
            return transmittedLightInfo.brightnessMax.unsignedIntegerValue;
        }
    }
}

- (NSUInteger)lightIntensityAtPath:(CZCameraLightPath)lightPath {
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            return 0;
        case CZCameraLightPathReflected: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetReflectedLightInfo];
            CZGeminiAPILightInfo *reflectedLightInfo = [self.apiClient callAPI:api];
            return reflectedLightInfo.brightness.unsignedIntegerValue;
        }
        case CZCameraLightPathTransmitted: {
            CZGeminiAPI *api = [CZGeminiAPI apiForGetTransmittedLightInfo];
            CZGeminiAPILightInfo *transmittedLightInfo = [self.apiClient callAPI:api];
            return transmittedLightInfo.brightness.unsignedIntegerValue;
        }
    }
}

- (void)setLightIntensity:(NSUInteger)lightIntensity atPath:(CZCameraLightPath)lightPath {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.brightness = @(lightIntensity);
    
    switch (lightPath) {
        case CZCameraLightPathUnknown:
            break;
        case CZCameraLightPathReflected: {
            CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
            [self.apiClient callAPIAsync:api];
            break;
        }
        case CZCameraLightPathTransmitted: {
            CZGeminiAPI *api = [CZGeminiAPI apiForSetTransmittedLightInfo:lightInfo];
            [self.apiClient callAPIAsync:api];
            break;
        }
    }
    
    [lightInfo release];
}

#pragma mark - Streaming Quality

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    CGFloat bitRateValue = 0.0;
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateValue = 8.0;
            break;
        case kCZCameraBitRateMedium:
            bitRateValue = 4.0;
            break;
        case kCZCameraBitRateLow:
            bitRateValue = 1.0;
            break;
    }
    return bitRateValue;
}

- (CZCameraBitRate)defaultBitRate {
    return kCZCameraBitRateMedium;
}

- (CZCameraBitRate)bitRate {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetBitRate];
    NSUInteger bitRate = [[self.apiClient callAPI:api] unsignedIntegerValue];
    if (bitRate == 8) {
        return kCZCameraBitRateHigh;
    } else if (bitRate == 1) {
        return kCZCameraBitRateLow;
    } else {
        return kCZCameraBitRateMedium;
    }
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
    if (bitRate == [self bitRate]) {
        return;
    }
    
    CZGeminiAPI *api = [CZGeminiAPI apiForSetBitRate:[self.class bitRateValueForLevel:bitRate]];
    [self.apiClient callAPI:api];
}

#pragma mark - Image Orientation

- (CZCameraImageOrientation)defaultImageOrientation {
    return kCZCameraImageOrientationOriginal;
}

- (CZCameraImageOrientation)imageOrientation {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetMirrorInfo];
    CZGeminiAPIMirrorInfo *mirrorInfo = [self.apiClient callAPI:api];
    if (mirrorInfo.horizontal.integerValue == 1 && mirrorInfo.vertical.integerValue == 1) {
        return kCZCameraImageOrientationRotated180;
    } else if (mirrorInfo.horizontal.integerValue == 1 && mirrorInfo.vertical.integerValue == 0) {
        return kCZCameraImageOrientationFlippedHorizontally;
    } else if (mirrorInfo.horizontal.integerValue == 0 && mirrorInfo.vertical.integerValue == 1) {
        return kCZCameraImageOrientationFlippedVertically;
    } else {
        return kCZCameraImageOrientationOriginal;
    }
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
    CZGeminiAPIMirrorInfo *mirrorInfo = [[CZGeminiAPIMirrorInfo alloc] init];
    switch (orientation) {
        case kCZCameraImageOrientationOriginal:
            mirrorInfo.horizontal = @0;
            mirrorInfo.vertical = @0;
            break;
        case kCZCameraImageOrientationFlippedHorizontally:
            mirrorInfo.horizontal = @1;
            mirrorInfo.vertical = @0;
            break;
        case kCZCameraImageOrientationFlippedVertically:
            mirrorInfo.horizontal = @0;
            mirrorInfo.vertical = @1;
            break;
        case kCZCameraImageOrientationRotated180:
            mirrorInfo.horizontal = @1;
            mirrorInfo.vertical = @1;
            break;
    }
    CZGeminiAPI *api = [CZGeminiAPI apiForSetMirrorInfo:mirrorInfo];
    [self.apiClient callAPIAsync:api];
    [mirrorInfo release];
}

#pragma mark - Snap Resolution

- (CGSize)lowSnapResolution {
    return CGSizeMake(1920, 1080);
}

- (CGSize)highSnapResolution {
    return CGSizeMake(3840, 2160);
}

- (CZCameraSnapResolutionPreset)defaultSnapResolutionPreset {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
        case CZGeminiAPICameraModelTypeAxiocam202:
            return kCZCameraSnapResolutionLow;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return kCZCameraSnapResolutionHigh;
    }
}

- (CZCameraSnapResolutionPreset)snapResolutionPreset {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
        case CZGeminiAPICameraModelTypeAxiocam202:
            return kCZCameraSnapResolutionLow;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return [super snapResolutionPreset];
    }
}

#pragma mark - Sharpening

- (uint32_t)defaultSharpness {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
        case CZGeminiAPICameraModelTypeAxiocam202:
            return 0;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return 1;
    }
}

- (uint32_t)sharpnessStepCount {
    return 2; // On and off
}

- (uint32_t)sharpness {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetSharpness];
    NSUInteger sharpness = [[self.apiClient callAPI:api] unsignedIntegerValue];
    return (sharpness == 3) ? 1 : 0;
}

- (void)setSharpness:(uint32_t)sharpness {
    int value = (sharpness == 0) ? 0 : 3;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetSharpness:value];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Denoise

- (BOOL)isDenoiseEnabledByDefault {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
        case CZGeminiAPICameraModelTypeAxiocam202:
            return NO;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return YES;
    }
}

- (BOOL)isDenoiseEnabled {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetDenoiseMode];
    CZGeminiAPIDenoiseMode denoiseMode = [[self.apiClient callAPI:api] integerValue];
    return denoiseMode == CZGeminiAPIDenoiseModeOn;
}

- (void)setDenoiseEnabled:(BOOL)enabled {
    CZGeminiAPIDenoiseMode denoiseMode = enabled ? CZGeminiAPIDenoiseModeSetOn : CZGeminiAPIDenoiseModeOff;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetDenoiseMode:denoiseMode];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Pixel Correction

- (BOOL)isPixelCorrectionEnabledByDefault {
    return NO;
}

- (BOOL)isPixelCorrectionEnabled {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetDynamicDefectPixel];
    CZGeminiAPIDynamicDefectPixel dynamicDefectPixel = [[self.apiClient callAPI:api] integerValue];
    return dynamicDefectPixel == CZGeminiAPIDynamicDefectPixelOpen;
}

- (void)setPixelCorrectionEnabled:(BOOL)enabled {
    CZGeminiAPIDynamicDefectPixel dynamicDefectPixel = enabled ? CZGeminiAPIDynamicDefectPixelOpen: CZGeminiAPIDynamicDefectPixelClose;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetDynamicDefectPixel:dynamicDefectPixel];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - HDR

- (BOOL)isHDREnabledByDefault {
    return NO;
}

- (BOOL)isHDREnabled {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetHDRMode];
    CZGeminiAPIHDRMode hdrMode = [[self.apiClient callAPI:api] integerValue];
    return hdrMode == CZGeminiAPIHDRModeOn;
}

- (void)setHDREnabled:(BOOL)enabled {
    CZGeminiAPIHDRMode hdrMode = enabled ? CZGeminiAPIHDRModeOn : CZGeminiAPIHDRModeOff;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetHDRMode:hdrMode];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Bit Depth

- (CZCameraBitDepth)defaultBitDepth {
    return CZCameraBitDepth8Bit;
}

#pragma mark - Grayscale Mode

- (BOOL)isGrayscaleModeEnabledByDefault {
    return NO;
}

- (BOOL)isGrayscaleModeEnabled {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetColorMode];
    CZGeminiAPIColorMode colorMode = [[self.apiClient callAPI:api] integerValue];
    return colorMode == CZGeminiAPIColorModeGray;
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
    CZGeminiAPIColorMode colorMode = enabled ? CZGeminiAPIColorModeGray : CZGeminiAPIColorModeColor;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetColorMode:colorMode];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - Gamma

- (float)minGamma {
    return 0.1;
}

- (float)maxGamma {
    return 3.0;
}

- (float)defaultGamma {
    switch ([self.systemInfo.camera modelType]) {
        case CZGeminiAPICameraModelTypeUnknown:
            return [super defaultGamma];
        case CZGeminiAPICameraModelTypeAxiocam202:
            return 1.2;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return 0.45;
    }
}

- (float)gamma {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetGammaInfo];
    CZGeminiAPIGammaInfo *gammaInfo = [self.apiClient callAPI:api];
    return gammaInfo.gamma.floatValue;
}

- (void)setGamma:(float)gamma {
    CZGeminiAPI *api = [CZGeminiAPI apiForSetGamma:gamma];
    [self.apiClient callAPI:api];
}

#pragma mark - Shading Correction

- (BOOL)isShadingCorrectionApplied {
    CZGeminiAPIObjectivesInfo *objectivesInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetObjectivesInfo]];
    CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetReflectorsInfo]];
    CZCameraLightPath ligthPath = [self lightPath];
    
    CZGeminiAPIReflectorInfo *reflectorInfo = [reflectorsInfo reflectorInfoAtCurrentPosition];
    CZGeminiAPIEffectInfo *effectInfo = [reflectorInfo effectInfoAtPosition:objectivesInfo.position];
    
    switch (ligthPath) {
        case CZCameraLightPathUnknown:
            return NO;
        case CZCameraLightPathReflected:
            return [effectInfo isRlShadingCorrect];
        case CZCameraLightPathTransmitted:
            return [effectInfo isTlShadingCorrect];
    }
}

- (void)calibrateShadingCorrection {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPIObjectivesInfo *objectivesInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetObjectivesInfo]];
        CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetReflectorsInfo]];
        CZCameraLightPath ligthPath = [self lightPath];
        
        CZGeminiAPIShadingCorrectionInfo *shadingCorrectionInfo = [[CZGeminiAPIShadingCorrectionInfo alloc] init];
        shadingCorrectionInfo.objectivePosition = objectivesInfo.position - 1;
        shadingCorrectionInfo.reflectorPosition = reflectorsInfo.position - 1;
        shadingCorrectionInfo.rltlPosition = ligthPath;
        shadingCorrectionInfo.action = @"calibrate";
        CZGeminiAPI *api = [CZGeminiAPI apiForSetShadingCorrection:shadingCorrectionInfo];
        [self.apiClient callAPI:api];
        [shadingCorrectionInfo release];
    });
}

- (void)enableShadingCorrection {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPIObjectivesInfo *objectivesInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetObjectivesInfo]];
        CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetReflectorsInfo]];
        CZCameraLightPath ligthPath = [self lightPath];
        
        CZGeminiAPIShadingCorrectionInfo *shadingCorrectionInfo = [[CZGeminiAPIShadingCorrectionInfo alloc] init];
        shadingCorrectionInfo.objectivePosition = objectivesInfo.position - 1;
        shadingCorrectionInfo.reflectorPosition = reflectorsInfo.position - 1;
        shadingCorrectionInfo.rltlPosition = ligthPath;
        shadingCorrectionInfo.on = @YES;
        CZGeminiAPI *api = [CZGeminiAPI apiForSetShadingCorrection:shadingCorrectionInfo];
        [self.apiClient callAPI:api];
        [shadingCorrectionInfo release];
    });
}

- (void)disableShadingCorrection {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZGeminiAPIObjectivesInfo *objectivesInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetObjectivesInfo]];
        CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:[CZGeminiAPI apiForGetReflectorsInfo]];
        CZCameraLightPath ligthPath = [self lightPath];
        
        CZGeminiAPIShadingCorrectionInfo *shadingCorrectionInfo = [[CZGeminiAPIShadingCorrectionInfo alloc] init];
        shadingCorrectionInfo.objectivePosition = objectivesInfo.position - 1;
        shadingCorrectionInfo.reflectorPosition = reflectorsInfo.position - 1;
        shadingCorrectionInfo.rltlPosition = ligthPath;
        shadingCorrectionInfo.on = @NO;
        CZGeminiAPI *api = [CZGeminiAPI apiForSetShadingCorrection:shadingCorrectionInfo];
        [self.apiClient callAPI:api];
        [shadingCorrectionInfo release];
    });
}

#pragma mark - Private methods

- (CZCameraSnappingInfo *)snapWithExposureMode:(CZCameraExposureMode)exposureMode exposureTimeInMilliseconds:(float)exposureTimeInMilliseconds gain:(NSUInteger)gain monochrome:(BOOL)monochrome error:(NSError **)error {
    if ([self isFirmwareCompatibleForSnapping:error] == NO) {
        return nil;
    }
    
    void (^timeoutErrorHandler)(void) = ^{
        if (error) {
            *error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraTimeoutError userInfo:nil];
        }
    };
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        CZLogv(@"Gemini camera failed to snap: no Wi-Fi available.");
        timeoutErrorHandler();
        return nil;
    }
    
    CZGeminiAPISnapInfo *snapInfo = [[CZGeminiAPISnapInfo alloc] init];
    if (exposureMode == kCZCameraExposureManual) {
        snapInfo.exposureType = CZGeminiAPIExposureTypeManual;
        snapInfo.exposureTimeInMicroseconds = @(exposureTimeInMilliseconds * 1e3);
        snapInfo.gain = @(gain);
    } else {
        snapInfo.exposureType = CZGeminiAPIExposureTypeAuto;
    }
    snapInfo.format = [self bitDepth] == CZCameraBitDepth12Bit ? CZGeminiAPISnapFormatGrayBayer : CZGeminiAPISnapFormatYUV;
//    snapInfo.resolution = [self snapResolutionPreset] == kCZCameraSnapResolutionHigh ? CZGeminiAPISnapResolution4K : CZGeminiAPISnapResolution1080p;
    CZGeminiAPI *api = [CZGeminiAPI apiForSnap:snapInfo];
    CZURLSessionTaskResult *result = [self.apiClient callAPI:api];
    [snapInfo release];
    
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)result.response;
    if (response.statusCode != 200) {
        CZLogv(@"Gemini camera failed to snap: HTTP status code = %ld", (long)response.statusCode);
        timeoutErrorHandler();
        return nil;
    }
    
    _snapSemaphore = dispatch_semaphore_create(0);
    long semaresult = dispatch_semaphore_wait(_snapSemaphore, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(CZGeminiAPITimeoutInterval * NSEC_PER_SEC)));
    dispatch_release(_snapSemaphore);
    _snapSemaphore = nil;
    
    // Timeout occurred.
    if (semaresult != 0) {
        timeoutErrorHandler();
        return nil;
    }
    
    if (self.onekeyFluorescenceSnapCancelled == YES) {
        timeoutErrorHandler();
        return nil;
    }
    
    CZGeminiCameraHTTPSnapEvent *event = [self.webSocketClient.latestEvent retain];
    if (![event isKindOfClass:[CZGeminiCameraHTTPSnapEvent class]]) {
        timeoutErrorHandler();
        return nil;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/image/thumbnail/%@", self.ipAddress, event.imageURL]];
    NSData *imageData = [[[NSURLSession sharedSession] cz_startSynchronousDataTaskWithURL:url timeoutInterval:CZGeminiAPITimeoutInterval] data];
    
    NSString *imageType = event.imageType;
    if ([imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeYUV420] && monochrome) {
        imageType = CZGeminiCameraHTTPSnapEventImageTypeYUV400;
    }
    
    UIImage *image = nil;
    @autoreleasepool {
        if ([imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeYUV400]) {
            image = [[CZCommonUtils imageFromYUV400Buffer:imageData.bytes
                                                   length:imageData.length
                                                    width:event.imageWidth.intValue
                                                   height:event.imageHeight.intValue] retain];
        } else if ([imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeYUV420]) {
            image = [[CZCommonUtils imageFromYUV420spNV21Buffer:imageData.bytes
                                                         length:imageData.length
                                                          width:event.imageWidth.intValue
                                                         height:event.imageHeight.intValue] retain];
        } else if ([imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeGrayBayer]) {
            image = [[CZCommonUtils imageFromGrayBayerBuffer:imageData.bytes
                                                      length:imageData.length
                                                       width:event.imageWidth.intValue
                                                      height:event.imageHeight.intValue] retain];
        }
    }
    
    if (image == nil) {
        timeoutErrorHandler();
        return nil;
    }
    
    CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
    snappingInfo.image = image;
    snappingInfo.grayscale = ([imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeYUV400] || [imageType isEqualToString:CZGeminiCameraHTTPSnapEventImageTypeGrayBayer]);
    snappingInfo.exposureTimeInMilliseconds = @(event.exposureTime.floatValue / 1000);
    snappingInfo.gain = @(event.gain.unsignedIntegerValue / 1024);
    snappingInfo.lightSource = [[[CZCameraLightSource alloc] initWithPosition:event.position.unsignedIntegerValue wavelength:event.wavelength.unsignedIntegerValue isOn:YES canOn:YES] autorelease];
    
    // Set light source type
    CZCameraLightSourceType lightSourceType = CZCameraLightSourceTypeUnknown;
    if ([self.systemInfo.microscope.microscopeName hasPrefix:@"Axiolab"]) {
        CZCameraLightPath lightPath = [self lightPath];
        switch (lightPath) {
            case CZCameraLightPathReflected:
                if (self.systemInfo.microscope.hasFluorescenceLEDs) {
                    lightSourceType = CZCameraLightSourceTypeFL_LED;
                } else if ([self.systemInfo.microscope.illumination.reflectedLight.type.lowercaseString isEqualToString:@"led"]) {
                    lightSourceType = CZCameraLightSourceTypeRL_LED;
                } else {
                    lightSourceType = CZCameraLightSourceTypeRL_Halogen;
                }
                break;
            case CZCameraLightPathTransmitted:
                if ([self.systemInfo.microscope.illumination.transmittedLight.type.lowercaseString isEqualToString:@"led"]) {
                    lightSourceType = CZCameraLightSourceTypeTL_LED;
                } else {
                    lightSourceType = CZCameraLightSourceTypeTL_Halogen;
                }
                break;
            default:
                break;
        }
    } else if ([self.systemInfo.microscope.microscopeName hasPrefix:@"Axioscope"]) {
        CZCameraLightPath lightPath = [self lightPath];
        switch (lightPath) {
            case CZCameraLightPathReflected:
                if (self.systemInfo.microscope.hasColibri) {
                    lightSourceType = CZCameraLightSourceTypeColibri3;
                } else if ([self.systemInfo.microscope.illumination.reflectedLight.type.lowercaseString isEqualToString:@"led"]) {
                    lightSourceType = CZCameraLightSourceTypeRL_LED;
                } else {
                    lightSourceType = CZCameraLightSourceTypeRL_External;
                }
                break;
            case CZCameraLightPathTransmitted:
                if ([self.systemInfo.microscope.illumination.transmittedLight.type.lowercaseString isEqualToString:@"led"]) {
                    lightSourceType = CZCameraLightSourceTypeTL_LED;
                } else if (!self.systemInfo.microscope.hasReflectedLightSwitch) {
                    lightSourceType = CZCameraLightSourceTypeTL_Halogen;
                } else {
                    lightSourceType = CZCameraLightSourceTypeTL_External;
                }
                break;
            default:
                break;
        }
    }
    snappingInfo.lightSourceType = lightSourceType;
    
    // Set filter set
    CZGeminiAPI *reflectorAPI = [CZGeminiAPI apiForGetReflectorsInfo];
    CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:reflectorAPI];
    snappingInfo.filterSetID = [[reflectorsInfo reflectorInfoAtCurrentPosition] matID];
    
    [image release];
    [event release];
    return [snappingInfo autorelease];
}

#pragma mark - Timer

- (void)heartBeatTimerFired:(NSTimer *)timer {
    CZGeminiAPI *api = [CZGeminiAPI apiForSendHeartBeat];
    [self.apiClient callAPIAsync:api];
}

#pragma mark - CZFluorescenceSnapping

- (CZFluorescenceSnappingTemplate *)onekeyFluorescenceSnappingTemplate {
    if (_onekeyFluorescenceSnappingTemplate == nil) {
        _onekeyFluorescenceSnappingTemplate = [[[CZDefaultSettings sharedInstance] fluorescenceSnappingTemplateForCamera:self] retain];
    }
    return _onekeyFluorescenceSnappingTemplate;
}

- (BOOL)enterFluorescenceSnappingMode:(CZFluorescenceSnappingMode)mode delegate:(id<CZFluorescenceSnappingDelegate>)delegate {
    if (mode == CZFluorescenceSnappingModeNone) {
        return NO;
    }
    
    CZCameraControlLock *snapLock = self.fluorescenceSnapLock;
    if (snapLock == nil) {
        snapLock = [[CZCameraControlLock alloc] initWithType:CZCameraControlTypeSnap camera:self retained:YES];
        self.fluorescenceSnapLock = snapLock;
        [snapLock release];
    }
    
    CZCameraControlLock *configurationLock = self.fluorescenceConfigurationLock;
    if (configurationLock == nil) {
        configurationLock = [[CZCameraControlLock alloc] initWithType:CZCameraControlTypeConfiguration camera:self retained:YES];
        self.fluorescenceConfigurationLock = configurationLock;
        [configurationLock release];
    }
    
    if (snapLock == nil || configurationLock == nil) {
        return NO;
    }
    
    self.fluorescenceSnappingMode = mode;
    self.fluorescenceSnappingDelegate = delegate;
    
    [self turnOnReflectedLight];
    [self initializeLightSources];
    
    self.manualFluorescenceSnappingTemplate = [[self.onekeyFluorescenceSnappingTemplate copy] autorelease] ?: [CZFluorescenceSnappingTemplate emptyTemplate];
    
    return YES;
}

- (void)switchToFluorescenceSnappingMode:(CZFluorescenceSnappingMode)mode {
    self.fluorescenceSnappingMode = mode;
}

- (void)exitFluorescenceSnappingMode {
    self.fluorescenceSnappingMode = CZFluorescenceSnappingModeNone;
    self.fluorescenceSnappingDelegate = nil;
    
    self.fluorescenceSnapLock = nil;
    self.fluorescenceConfigurationLock = nil;
    
    self.manualFluorescenceSnappingTemplate = nil;
    self.onekeyFluorescenceSnappingTemplate = nil;
}

- (BOOL)isFluorescenceSnappingMode {
    return self.fluorescenceSnapLock && self.fluorescenceConfigurationLock;
}

- (BOOL)isManualFluorescenceSnappingMode {
    return [self isFluorescenceSnappingMode] && self.fluorescenceSnappingMode == CZFluorescenceSnappingModeManual;
}

- (BOOL)isOnekeyFluorescenceSnappingMode {
    return [self isFluorescenceSnappingMode] && self.fluorescenceSnappingMode == CZFluorescenceSnappingModeOnekey;
}

- (void)toggleLightSourceAtPosition:(NSUInteger)position {
    if (position >= self.cachedLightSources.count) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        switch ([self reflectedLightSourceType]) {
            case CZCameraLightSourceTypeFL_LED: {
                CZCameraLightSource *lightSource = self.cachedLightSources[position];
                if (lightSource.isOn) {
                    [self cacheLightIntensityForLightSourceAtPosition:position];
                    [self turnOffReflectedLight];
                } else {
                    CZCameraSnappingInfo *snappingInfo = [self.manualFluorescenceSnappingTemplate snappingInfoForLightSource:lightSource];
                    if (snappingInfo) {
                        [self turnOnReflectedLightWithSnappingInfo:snappingInfo];
                        [self adjustExposureWithSnappingInfo:snappingInfo];
                    } else {
                        [self turnOnReflectedLightWithCachedLightIntensityForLightSourceAtPosition:position];
                    }
                }
                [self toggleLocalLightSourceAtPosition:position];
                break;
            }
            case CZCameraLightSourceTypeColibri3: {
                // Switch to reflected light if needed.
                CZCameraLightPath lightPath = [self lightPath];
                if (lightPath != CZCameraLightPathReflected) {
                    [self turnOnReflectedLight];
                }
                
                NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
                for (CZCameraLightSource *lightSource in lightSources) {
                    if (lightSource.position == position && !lightSource.isOn) {
                        [self turnOnLightSourceAtPosition:lightSource.position withSnappingInfo:nil];
                    } else if (lightSource.isOn) {
                        [self turnOffLightSourceAtPosition:lightSource.position];
                    }
                }
                [lightSources release];
                break;
            }
            default:
                break;
        }
    });
}

- (void)beginOrContinueManualFluorescenceSnapping {
    if (![self isManualFluorescenceSnappingMode]) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            CZCameraSnappingInfo *snappingInfo = nil;
            NSError *error = nil;
            
            if (self.fluorescenceSnapLock && self.fluorescenceConfigurationLock) {
                CZCameraExposureMode exposureMode = [self exposureMode];
                float exposureTime = 0.0;
                NSUInteger gain = 0;
                if (exposureMode == kCZCameraExposureManual) {
                    exposureTime = [self exposureTime];
                    gain = [self gain];
                }
                
                snappingInfo = [self snapWithExposureMode:exposureMode exposureTimeInMilliseconds:exposureTime gain:gain monochrome:YES error:&error];
                snappingInfo.exposureMode = exposureMode;
                snappingInfo.exposureIntensity = [self exposureIntensity];
                snappingInfo.lightIntensity = [self lightIntensityAtPath:CZCameraLightPathReflected];
                
                [self.manualFluorescenceSnappingTemplate updateWithSnappingInfo:snappingInfo];
                
                if (self.temporaryFluorescenceSnappingTemplate == nil) {
                    self.temporaryFluorescenceSnappingTemplate = [CZFluorescenceSnappingTemplate emptyTemplate];
                }
                [self.temporaryFluorescenceSnappingTemplate updateWithSnappingInfo:snappingInfo];
            } else {
                error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraNoControlError userInfo:nil];
            }
            
            switch ([self reflectedLightSourceType]) {
                case CZCameraLightSourceTypeFL_LED:
                    [self cacheLightIntensityForLightSourceAtPosition:snappingInfo.lightSource.position];
                    [self turnOffReflectedLight];
                    [self turnOffAllLocalLightSources];
                    break;
                case CZCameraLightSourceTypeColibri3:
                    [self turnOffAllLightSources];
                    break;
                default:
                    break;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.delegate && [self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                    [self.delegate camera:self didFinishSnapping:snappingInfo error:error];
                }
            });
        }
    });
}

- (void)endManualFluorescenceSnapping:(CZFluorescenceSnappingEndAction)action {
    if (![self isManualFluorescenceSnappingMode]) {
        return;
    }
    
    switch (action) {
        case CZFluorescenceSnappingEndActionDone:
            self.onekeyFluorescenceSnappingTemplate = [[self.temporaryFluorescenceSnappingTemplate copy] autorelease];
            self.manualFluorescenceSnappingTemplate = [[self.onekeyFluorescenceSnappingTemplate copy] autorelease];
            self.temporaryFluorescenceSnappingTemplate = nil;
            
            [[CZDefaultSettings sharedInstance] setFluorescenceSnapTemplate:self.onekeyFluorescenceSnappingTemplate forCamera:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:CZFluorescenceSnappingTemplateDidSaveNotification object:self];
            break;
        case CZFluorescenceSnappingEndActionUndone:
            break;
    }
}

- (BOOL)canBeginOnekeyFluorescenceSnapping {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return NO;
    }
    
    CZGeminiAPI *api = [CZGeminiAPI apiForGetReflectorsInfo];
    CZGeminiAPIReflectorsInfo *reflectorsInfo = [self.apiClient callAPI:api];
    NSString *filterSetID = [[reflectorsInfo reflectorInfoAtCurrentPosition] matID];
    
    for (CZCameraSnappingInfo *snappingInfo in self.onekeyFluorescenceSnappingTemplate.snappingInfos) {
        if (![snappingInfo.filterSetID isEqualToString:filterSetID]) {
            return NO;
        }
    }
    
    return YES;
}

- (void)beginOnekeyFluorescenceSnapping {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return;
    }
    
    if (_onekeyFluorescenceSnapQueue.operationCount > 0) {
        return;
    }
    
    self.onekeyFluorescenceSnapCancelled = NO;
    
    [_onekeyFluorescenceSnapQueue addOperationWithBlock:^{
        if ([self reflectedLightSourceType] == CZCameraLightSourceTypeFL_LED) {
            [self turnOnAllLocalLightSources];
        }
        
        CZCameraSnappingInfo *snappingInfoForCurrentLightSource = nil;
        NSArray<CZCameraSnappingInfo *> *sortedSnappingInfos = [self sortSnappingInfos:self.onekeyFluorescenceSnappingTemplate.snappingInfos snappingInfoForCurrentLightSource:&snappingInfoForCurrentLightSource];
        
        if ([self checkOnekeyFluorescenceSnappingIsCancelled]) {
            return;
        }
        
        float previousExposureTime = [self realExposureTime];
        
        for (NSUInteger index = 0; index < sortedSnappingInfos.count; index++) {
            CZCameraSnappingInfo *snappingInfo = sortedSnappingInfos[index];
            
            switch ([self reflectedLightSourceType]) {
                case CZCameraLightSourceTypeFL_LED:
                    if (snappingInfo == snappingInfoForCurrentLightSource) {
                        // Set light intensity
                        [self setLightIntensityForSnappingInfo:snappingInfo];
                    } else {
                        _onekeyFluorescenceSnapSemaphore = dispatch_semaphore_create(0);
                        
                        if (self.delegate && [self.delegate respondsToSelector:@selector(camera:didPauseSnapping:reason:)]) {
                            [self.delegate camera:self didPauseSnapping:snappingInfo reason:CZCameraNeedSwitchLightSourceByUser];
                        }
                        
                        if (_onekeyFluorescenceSnapSemaphore) {
                            long semaresult = dispatch_semaphore_wait(_onekeyFluorescenceSnapSemaphore, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(CZGeminiAPITimeoutInterval * NSEC_PER_SEC)));
                            dispatch_release(_onekeyFluorescenceSnapSemaphore);
                            _onekeyFluorescenceSnapSemaphore = nil;
                            
                            // Timeout occurred.
                            if (semaresult != 0) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                                        NSError *error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraTimeoutError userInfo:nil];
                                        [self.delegate camera:self didFinishSnapping:nil error:error];
                                    }
                                });
                                return;
                            }
                            
                            if ([self checkOnekeyFluorescenceSnappingIsCancelled]) {
                                return;
                            }
                            
                            // Set light intensity
                            [self setLightIntensityForSnappingInfo:snappingInfo];
                        }
                    }
                    break;
                case CZCameraLightSourceTypeColibri3:
                    [self switchToLightSourceForSnappingInfo:snappingInfo];
                    break;
                default:
                    break;
            }
            
            if ([self checkOnekeyFluorescenceSnappingIsCancelled]) {
                return;
            }
            
            NSError *error = nil;
            
            [self adjustExposureWithSnappingInfo:snappingInfo];
            // If the next exposure mode is manual, we should wait for some time, the time algorithm is provided by Camera Team.
            if (snappingInfo.exposureMode == kCZCameraExposureManual) {
                float waitTime = [self waitTimeForPreviousExposureTime:previousExposureTime targetExposureTime:snappingInfo.exposureTimeInMilliseconds.floatValue];
                usleep(waitTime * 1e3);
            }
            
            snappingInfo = [self snapWithExposureMode:snappingInfo.exposureMode exposureTimeInMilliseconds:snappingInfo.exposureTimeInMilliseconds.floatValue gain:snappingInfo.gain.unsignedIntegerValue monochrome:YES error:&error];
            snappingInfo.currentSnappingNumber = index + 1;
            snappingInfo.totalSnappingNumber = sortedSnappingInfos.count;
            
            previousExposureTime = snappingInfo.exposureTimeInMilliseconds.floatValue;
            
            if ([self checkOnekeyFluorescenceSnappingIsCancelled]) {
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                    [self.delegate camera:self didFinishSnapping:snappingInfo error:error];
                }
            });
        }
        
        switch ([self reflectedLightSourceType]) {
            case CZCameraLightSourceTypeFL_LED:
                [self turnOffReflectedLight];
                [self turnOffAllLocalLightSources];
                break;
            case CZCameraLightSourceTypeColibri3:
                [self turnOffAllLightSources];
                break;
            default:
                break;
        }
    }];
}

- (BOOL)isOnekeyFluorescenceSnappingPaused {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return NO;
    }
    
    return _onekeyFluorescenceSnapSemaphore != nil;
}

- (void)continueOnekeyFluorescenceSnapping {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return;
    }
    
    if (_onekeyFluorescenceSnapSemaphore) {
        dispatch_semaphore_signal(_onekeyFluorescenceSnapSemaphore);
    }
}

- (void)cancelOnekeyFluorescenceSnapping {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return;
    }
    
    self.onekeyFluorescenceSnapCancelled = YES;
    if (_onekeyFluorescenceSnapSemaphore) {
        dispatch_semaphore_signal(_onekeyFluorescenceSnapSemaphore);
    }
    if (_snapSemaphore) {
        dispatch_semaphore_signal(_snapSemaphore);
    }
}

- (void)endOnekeyFluorescenceSnapping:(CZFluorescenceSnappingEndAction)action {
    if (![self isOnekeyFluorescenceSnappingMode]) {
        return;
    }
}

#pragma mark - CZFluorescenceSnapping Helpers

- (void)turnOnReflectedLight {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.status = @"on";
    CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
    [self.apiClient callAPI:api];
    [lightInfo release];
}

- (void)turnOnReflectedLightWithSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.status = @"on";
    if (snappingInfo) {
        lightInfo.brightness = @(snappingInfo.lightIntensity);
    }
    CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
    [self.apiClient callAPI:api];
    [lightInfo release];
}

- (void)turnOnReflectedLightWithCachedLightIntensityForLightSourceAtPosition:(NSUInteger)position {
    CZCameraLightSource *currentLightSource = [self currentLightSource];
    NSNumber *lightIntensity = self.cachedLightIntensities[@(currentLightSource.wavelength)];
    
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.status = @"on";
    lightInfo.brightness = lightIntensity;
    CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
    [self.apiClient callAPI:api];
    [lightInfo release];
}

- (void)turnOffReflectedLight {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.status = @"off";
    CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
    [self.apiClient callAPI:api];
    [lightInfo release];
}

- (void)turnOnLightSourceAtPosition:(NSUInteger)position withSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    CZGeminiAPILEDInfo *ledInfo = [[CZGeminiAPILEDInfo alloc] init];
    ledInfo.status = @"on";
    if (snappingInfo) {
        ledInfo.brightness = @(snappingInfo.lightIntensity);
    }
    CZGeminiAPI *api = [CZGeminiAPI apiForSetLEDInfo:ledInfo atPosition:position + 1];
    [self.apiClient callAPI:api];
    [ledInfo release];
}

- (void)adjustLightSourceAtPosition:(NSUInteger)position withSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    if (snappingInfo == nil) {
        return;
    }
    
    CZGeminiAPILEDInfo *ledInfo = [[CZGeminiAPILEDInfo alloc] init];
    ledInfo.brightness = @(snappingInfo.lightIntensity);
    CZGeminiAPI *api = [CZGeminiAPI apiForSetLEDInfo:ledInfo atPosition:position + 1];
    [self.apiClient callAPI:api];
    [ledInfo release];
}

- (void)turnOffLightSourceAtPosition:(NSUInteger)position {
    CZGeminiAPILEDInfo *ledInfo = [[CZGeminiAPILEDInfo alloc] init];
    ledInfo.status = @"off";
    CZGeminiAPI *api = [CZGeminiAPI apiForSetLEDInfo:ledInfo atPosition:position + 1];
    [self.apiClient callAPI:api];
    [ledInfo release];
}

- (void)turnOffAllLightSources {
    NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
    for (CZCameraLightSource *lightSource in lightSources) {
        if (lightSource.isOn) {
            [self turnOffLightSourceAtPosition:lightSource.position];
        }
    }
    [lightSources release];
}

- (void)toggleLocalLightSourceAtPosition:(NSUInteger)position {
    CZCameraLightSource *lightSource = self.cachedLightSources[position];
    lightSource.isLocal = YES;
    lightSource.isOn = !lightSource.isOn;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.fluorescenceSnappingDelegate && [self.fluorescenceSnappingDelegate respondsToSelector:@selector(camera:didUpdateLightSources:)]) {
            [self.fluorescenceSnappingDelegate camera:self didUpdateLightSources:self.cachedLightSources];
        }
    });
}

- (void)turnOnAllLocalLightSources {
    NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
    for (CZCameraLightSource *lightSource in lightSources) {
        if (lightSource.canOn) {
            lightSource.isLocal = YES;
            lightSource.isOn = YES;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.fluorescenceSnappingDelegate && [self.fluorescenceSnappingDelegate respondsToSelector:@selector(camera:didUpdateLightSources:)]) {
            [self.fluorescenceSnappingDelegate camera:self didUpdateLightSources:lightSources];
        }
    });
    
    [lightSources release];
}

- (void)turnOffAllLocalLightSources {
    NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
    for (CZCameraLightSource *lightSource in lightSources) {
        if (lightSource.isOn) {
            lightSource.isLocal = YES;
            lightSource.isOn = NO;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.fluorescenceSnappingDelegate && [self.fluorescenceSnappingDelegate respondsToSelector:@selector(camera:didUpdateLightSources:)]) {
            [self.fluorescenceSnappingDelegate camera:self didUpdateLightSources:lightSources];
        }
    });
    
    [lightSources release];
}

- (NSArray<CZCameraLightSource *> *)lightSources {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetLEDsInfo];
    CZGeminiAPILEDsInfo *ledsInfo = [self.apiClient callAPI:api];
    
    NSMutableArray<CZCameraLightSource *> *lightSources = [NSMutableArray array];
    for (NSInteger index = 0; index < ledsInfo.leds.count; index++) {
        CZGeminiAPILEDInfo *ledInfo = ledsInfo.leds[index];
        BOOL canOn = [self reflectedLightSourceType] == CZCameraLightSourceTypeFL_LED ? ledInfo.isLightOn : YES;
        CZCameraLightSource *lightSource = [[CZCameraLightSource alloc] initWithPosition:index
                                                                              wavelength:ledInfo.waveLength.unsignedIntegerValue
                                                                                    isOn:ledInfo.isLightOn
                                                                                   canOn:canOn];
        [lightSources addObject:lightSource];
        [lightSource release];
    }
    
    return [[lightSources copy] autorelease];
}

- (CZCameraLightSource *)currentLightSource {
    NSArray<CZCameraLightSource *> *lightSources = [self lightSources];
    NSUInteger currentPosition = [[self.apiClient callAPI:[CZGeminiAPI apiForGetCurrentLEDPosition]] unsignedIntegerValue];
    if (currentPosition < lightSources.count) {
        return lightSources[currentPosition];
    } else {
        return nil;
    }
}

- (void)initializeLightSources {
    CZCameraLightPath lightPath = [self lightPath];
    NSArray<CZCameraLightSource *> *lightSources = [self lightSources];
    for (CZCameraLightSource *lightSource in lightSources) {
        lightSource.isOn = lightPath == CZCameraLightPathReflected ? lightSource.isOn : NO;
        lightSource.canOn = lightPath == CZCameraLightPathReflected ? lightSource.canOn : NO;
    }
    
    self.cachedLightSources = lightSources;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.fluorescenceSnappingDelegate && [self.fluorescenceSnappingDelegate respondsToSelector:@selector(camera:didGetLightSources:)]) {
            [self.fluorescenceSnappingDelegate camera:self didGetLightSources:lightSources];
        }
    });
}

- (void)updateLightSourcesWithStatus:(CZGeminiCameraStatus *)status {
    if (![self isFluorescenceSnappingMode]) {
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray<CZCameraLightSource *> *lightSources = [self lightSources];
        for (CZCameraLightSource *lightSource in lightSources) {
            lightSource.isOn = status.rltlPosition == 1 ? lightSource.isOn : NO;
            lightSource.canOn = status.rltlPosition == 1 ? lightSource.canOn : NO;
        }
        
        if (lightSources.count != self.cachedLightSources.count) {
            return;
        }
        
        // Light source position and wavelength must be same.
        if (![lightSources isEqualToArray:self.cachedLightSources]) {
            return;
        }
        
        BOOL lightSourcesUpdated = NO;
        NSUInteger positions = self.cachedLightSources.count;
        for (NSUInteger position = 0; position < positions; position++) {
            CZCameraLightSource *oldLightSource = self.cachedLightSources[position];
            CZCameraLightSource *newLightSource = lightSources[position];
            if (newLightSource.canOn != oldLightSource.canOn) {
                lightSourcesUpdated = YES;
                break;
            }
            if (newLightSource.isOn != oldLightSource.isOn && !oldLightSource.isLocal) {
                lightSourcesUpdated = YES;
                break;
            }
        }
        
        if (lightSourcesUpdated == NO) {
            return;
        }
        
        self.cachedLightSources = lightSources;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.fluorescenceSnappingDelegate && [self.fluorescenceSnappingDelegate respondsToSelector:@selector(camera:didUpdateLightSources:)]) {
                [self.fluorescenceSnappingDelegate camera:self didUpdateLightSources:lightSources];
            }
        });
        
        if ([self isManualFluorescenceSnappingMode] || _onekeyFluorescenceSnapQueue.operationCount == 0) {
            NSMutableArray<CZCameraLightSource *> *activeLightSources = [NSMutableArray arrayWithCapacity:lightSources.count];
            for (CZCameraLightSource *lightSource in lightSources) {
                if (lightSource.isOn) {
                    [activeLightSources addObject:lightSource];
                }
            }
            
            if (activeLightSources.count == 1) {
                CZCameraLightSource *activeLightSource = activeLightSources[0];
                CZCameraSnappingInfo *snappingInfo = [self.manualFluorescenceSnappingTemplate snappingInfoForLightSource:activeLightSource];
                [self adjustLightSourceAtPosition:activeLightSource.position withSnappingInfo:snappingInfo];
                [self adjustExposureWithSnappingInfo:snappingInfo];
            }
        }
    });
}

- (void)cacheLightIntensityForLightSourceAtPosition:(NSUInteger)position {
    CZCameraLightSource *currentLightSource = [self currentLightSource];
    NSUInteger currentBrightness = [self lightIntensityAtPath:CZCameraLightPathReflected];
    self.cachedLightIntensities[@(currentLightSource.wavelength)] = @(currentBrightness);
}

- (void)adjustExposureWithSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    if (snappingInfo == nil) {
        return;
    }
    
    [self setExposureMode:snappingInfo.exposureMode];
    [self setExposureIntensity:snappingInfo.exposureIntensity];
    if (snappingInfo.exposureMode == kCZCameraExposureManual) {
        [self setExposureTime:snappingInfo.exposureTimeInMilliseconds.floatValue];
        [self setGain:snappingInfo.gain.unsignedIntegerValue];
    }
}

- (NSArray<CZCameraSnappingInfo *> *)sortSnappingInfos:(NSArray<CZCameraSnappingInfo *> *)snappingInfos snappingInfoForCurrentLightSource:(CZCameraSnappingInfo **)snappingInfoForCurrentLightSource {
    snappingInfos = [snappingInfos sortedArrayUsingComparator:^NSComparisonResult(CZCameraSnappingInfo *si1, CZCameraSnappingInfo *si2) {
        return [@(si1.lightSource.position) compare:@(si2.lightSource.position)];
    }];
    
    switch ([self reflectedLightSourceType]) {
        case CZCameraLightSourceTypeFL_LED: {
            CZCameraLightSource *currentLightSource = nil;
            NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
            for (CZCameraLightSource *lightSource in lightSources) {
                if (lightSource.isOn) {
                    currentLightSource = lightSource;
                    break;
                }
            }
            [lightSources release];
            
            if (currentLightSource == nil) {
                return snappingInfos;
            }
            
            NSMutableArray<CZCameraSnappingInfo *> *sortedSnappingInfos = [snappingInfos mutableCopy];
            for (CZCameraSnappingInfo *snappingInfo in sortedSnappingInfos) {
                if ([snappingInfo.lightSource isEqualToLightSource:currentLightSource]) {
                    [sortedSnappingInfos removeObject:snappingInfo];
                    [sortedSnappingInfos insertObject:snappingInfo atIndex:0];
                    if (snappingInfoForCurrentLightSource) {
                        *snappingInfoForCurrentLightSource = snappingInfo;
                    }
                    break;
                }
            }
            return [sortedSnappingInfos autorelease];
        }
        case CZCameraLightSourceTypeColibri3: {
            return snappingInfos;
        }
        default: {
            return snappingInfos;
        }
    }
}

- (void)switchToLightSourceForSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    NSArray<CZCameraLightSource *> *lightSources = [self.cachedLightSources retain];
    for (CZCameraLightSource *lightSource in lightSources) {
        if (lightSource.position == snappingInfo.lightSource.position && !lightSource.isOn) {
            [self turnOnLightSourceAtPosition:lightSource.position withSnappingInfo:snappingInfo];
        } else if (lightSource.position != snappingInfo.lightSource.position && lightSource.isOn) {
            [self turnOffLightSourceAtPosition:lightSource.position];
        }
    }
    [lightSources release];
}

- (void)setLightIntensityForSnappingInfo:(CZCameraSnappingInfo *)snappingInfo {
    CZGeminiAPILightInfo *lightInfo = [[CZGeminiAPILightInfo alloc] init];
    lightInfo.brightness = @(snappingInfo.lightIntensity);
    CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectedLightInfo:lightInfo];
    [self.apiClient callAPI:api];
    [lightInfo release];
}

- (BOOL)checkOnekeyFluorescenceSnappingIsCancelled {
    if (self.onekeyFluorescenceSnapCancelled) {
        self.onekeyFluorescenceSnapCancelled = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                NSError *error = [NSError errorWithDomain:CZCameraErrorDomain code:kCZCameraUserCancelledError userInfo:nil];
                [self.delegate camera:self didFinishSnapping:nil error:error];
            }
        });
        return YES;
    }
    return NO;
}

/// Real exposure time in milliseconds.
- (float)realExposureTime {
    CZGeminiAPI *api = [CZGeminiAPI apiForGetRealExposureTimeInfo];
    CZGeminiAPIRealExposureTimeInfo *realExposureTimeInfo = [self.apiClient callAPI:api];
    return realExposureTimeInfo.exposureTime.floatValue / 1000;
}

/// All the times are in milliseconds.
- (float)waitTimeForPreviousExposureTime:(float)previousExposureTime targetExposureTime:(float)targetExposureTime {
    if (fabsf(targetExposureTime - previousExposureTime) < 50) {
        return 0;
    } else if (previousExposureTime < 300 && targetExposureTime < 300) {
        return 300;
    } else {
        return fmaxf(previousExposureTime, targetExposureTime) * 3;
    }
}

#pragma mark - CZGeminiWebSocketClientDelegate

- (void)webSocketClient:(CZGeminiWebSocketClient *)webSocketClient statusDidChange:(CZGeminiCameraStatus *)status changes:(CZGeminiCameraStatusChangeMask)changes {
    if (((changes & CZGeminiCameraStatusChangeMaskRLTLPosition) != 0) || ((changes & CZGeminiCameraStatusChangeMaskLEDsStatus) != 0)) {
        [self updateLightSourcesWithStatus:status];
    }
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    if (status) {
        userInfo[CZGeminiCameraStatusKey] = status;
    }
    userInfo[CZGeminiCameraStatusChangesKey] = @(changes);
    [[NSNotificationCenter defaultCenter] postNotificationName:CZGeminiCameraStatusDidChangeNotification object:self userInfo:userInfo];
}

- (void)webSocketClient:(CZGeminiWebSocketClient *)webSocketClient didReceiveEvent:(CZGeminiCameraEvent *)event {
    if ([event.eventType isEqualToString:CZGeminiCameraEventTypeAutoExposureStable] && _exposureTimeAutoAdjustSemaphore) {
        dispatch_semaphore_signal(_exposureTimeAutoAdjustSemaphore);
    }
    
    if ([event.eventType isEqualToString:CZGeminiCameraEventTypeHTTPSnap] && _snapSemaphore) {
        dispatch_semaphore_signal(_snapSemaphore);
    }
    
    if ([event.eventType isEqualToString:CZGeminiCameraEventTypeShadingCorrectionInfo]) {
        CZGeminiCameraShadingCorrectionInfoEvent *shadingCorrectionInfoEvent = (CZGeminiCameraShadingCorrectionInfoEvent *)event;
        CZGeminiCameraShadingCorrectionInfoType infoType = shadingCorrectionInfoEvent.value.integerValue;
        switch (infoType) {
            case CZGeminiCameraShadingCorrectionInfoGetTableSuccess:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidCalibrateShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidCalibrateShadingCorrection:self];
                }
                break;
            case CZGeminiCameraShadingCorrectionInfoGetTableError:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidFailToCalibrateShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidFailToCalibrateShadingCorrection:self];
                }
                break;
            case CZGeminiCameraShadingCorrectionInfoOpenSuccess:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidEnableShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidEnableShadingCorrection:self];
                }
                break;
            case CZGeminiCameraShadingCorrectionInfoOpenError:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidFailToEnableShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidFailToEnableShadingCorrection:self];
                }
                break;
            case CZGeminiCameraShadingCorrectionInfoCloseSuccess:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidDisableShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidDisableShadingCorrection:self];
                }
                break;
            case CZGeminiCameraShadingCorrectionInfoCloseError:
                if (self.shadingCorrectionDelegate && [self.shadingCorrectionDelegate respondsToSelector:@selector(cameraDidFailToDisableShadingCorrection:)]) {
                    [self.shadingCorrectionDelegate cameraDidFailToDisableShadingCorrection:self];
                }
                break;
        }
    }
    
    if ([event.eventType isEqualToString:CZGeminiCameraEventTypeHardwareSnap]) {
        CZGeminiAPI *api = [CZGeminiAPI apiForGetClientNumber];
        [self.apiClient callAPIAsync:api completionHandler:^(NSNumber *clientNumber) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            if (event) {
                userInfo[CZGeminiCameraEventKey] = event;
            }
            if (clientNumber) {
                userInfo[CZGeminiCameraClientNumberKey] = clientNumber;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:CZGeminiCameraDidReceiveEventNotification object:self userInfo:userInfo];
        }];
    } else {
        NSDictionary *userInfo = event ? @{CZGeminiCameraEventKey : event} : nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:CZGeminiCameraDidReceiveEventNotification object:self userInfo:userInfo];
    }
}

@end
