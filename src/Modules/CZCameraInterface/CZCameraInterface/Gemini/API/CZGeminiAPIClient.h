//
//  CZGeminiAPIClient.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/22/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGeminiAPI.h"
#import "CZGeminiAPISession.h"

extern const NSTimeInterval CZGeminiAPITimeoutInterval;

@interface CZGeminiAPIClient : NSObject

@property (nonatomic, readonly, copy) NSString *ipAddress;

- (instancetype)initWithIPAddress:(NSString *)ipAddress;

- (id)callAPI:(CZGeminiAPI *)api;
- (void)callAPIAsync:(CZGeminiAPI *)api;
- (void)callAPIAsync:(CZGeminiAPI *)api completionHandler:(void (^)(id result))completionHandler;

- (void)commitAPISession:(CZGeminiAPISession *)session withCompletionHandler:(void (^)(void))completionHandler;
- (void)commitAPISession:(CZGeminiAPISession *)session withCompletionHandler:(void (^)(void))completionHandler afterDelay:(NSTimeInterval)delay;

@end
