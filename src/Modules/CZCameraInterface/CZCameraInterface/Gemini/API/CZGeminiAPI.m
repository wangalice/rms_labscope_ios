//
//  CZGeminiAPI.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPI.h"

@implementation CZGeminiAPI

#pragma mark - Snap

+ (CZGeminiAPI *)apiForSnap:(CZGeminiAPISnapInfo *)snapInfo {
    NSDictionary *parameters = [snapInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"setHttpSnap" parameters:parameters mapper:nil];
}

+ (CZGeminiAPI *)apiForSetResolution:(NSUInteger)resolution {
    NSDictionary *parameters = @{@"resolution" : @(resolution)};
    return [self apiWithMethod:@"PUT" path:@"rtsp" parameters:parameters mapper:nil];
}

#pragma mark - System Info APIs

+ (CZGeminiAPI<CZGeminiAPISystemInfo *> *)apiForGetSystemInfo {
    return [self apiWithMethod:@"GET" path:@"system" parameters:nil mapper:^CZGeminiAPISystemInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPISystemInfo *systemInfo = [CZGeminiAPISystemInfo yy_modelWithJSON:result.data];
        return systemInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetCameraName:(NSString *)cameraName {
    NSDictionary *parameters = @{@"name" : cameraName ?: @""};
    return [self apiWithMethod:@"PUT" path:@"camera" parameters:parameters mapper:nil];
}

#pragma mark - Reflector Info APIs

+ (CZGeminiAPI<CZGeminiAPIReflectorsInfo *> *)apiForGetReflectorsInfo {
    return [self apiWithMethod:@"GET" path:@"reflector" parameters:nil mapper:^CZGeminiAPIReflectorsInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIReflectorsInfo *reflectorsInfo = [CZGeminiAPIReflectorsInfo yy_modelWithJSON:result.data];
        return reflectorsInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPIReflectorInfo *> *)apiForGetReflectorInfoAtPosition:(NSInteger)position {
    NSString *path = [NSString stringWithFormat:@"reflector/%ld", (long)position];
    return [self apiWithMethod:@"GET" path:path parameters:nil mapper:^CZGeminiAPIReflectorInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIReflectorInfo *reflectorInfo = [CZGeminiAPIReflectorInfo yy_modelWithJSON:result.data];
        return reflectorInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetReflectorInfo:(CZGeminiAPIReflectorInfo *)reflectorInfo atPosition:(NSInteger)position {
    NSDictionary *parameters = [reflectorInfo yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"reflector/%ld", (long)position];
    return [self apiWithMethod:@"PUT" path:path parameters:parameters mapper:nil];
}

#pragma mark - Objective Info APIs

+ (CZGeminiAPI<CZGeminiAPIObjectivesInfo *> *)apiForGetObjectivesInfo {
    return [self apiWithMethod:@"GET" path:@"objective" parameters:nil mapper:^CZGeminiAPIObjectivesInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIObjectivesInfo *objectivesInfo = [CZGeminiAPIObjectivesInfo yy_modelWithJSON:result.data];
        return objectivesInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPIObjectiveInfo *> *)apiForGetObjectiveInfoAtPosition:(NSInteger)position {
    NSString *path = [NSString stringWithFormat:@"objective/%ld", (long)position];
    return [self apiWithMethod:@"GET" path:path parameters:nil mapper:^CZGeminiAPIObjectiveInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIObjectiveInfo *objectiveInfo = [CZGeminiAPIObjectiveInfo yy_modelWithJSON:result.data];
        return objectiveInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetObjectiveInfo:(CZGeminiAPIObjectiveInfo *)objectiveInfo atPosition:(NSInteger)position {
    NSDictionary *parameters = [objectiveInfo yy_modelToJSONObject];
    NSString *path = [NSString stringWithFormat:@"objective/%ld", (long)position];
    return [self apiWithMethod:@"PUT" path:path parameters:parameters mapper:nil];
}

#pragma mark - Camera Adapter APIs

+ (CZGeminiAPI *)apiForSetCameraAdapter:(float)cameraAdapter {
    NSDictionary *parameters = @{@"cameraAdapter" : @(cameraAdapter)};
    return [self apiWithMethod:@"PUT" path:@"cameraAdapter" parameters:parameters mapper:nil];
}

#pragma mark - Mirror Info APIs

+ (CZGeminiAPI<CZGeminiAPIMirrorInfo *> *)apiForGetMirrorInfo {
    return [self apiWithMethod:@"GET" path:@"mirror" parameters:nil mapper:^CZGeminiAPIMirrorInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIMirrorInfo *mirrorInfo = [CZGeminiAPIMirrorInfo yy_modelWithJSON:result.data];
        return mirrorInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetMirrorInfo:(CZGeminiAPIMirrorInfo *)mirrorInfo {
    NSDictionary *parameters = [mirrorInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"mirror" parameters:parameters mapper:nil];
}

#pragma mark - Firmware Version Info APIs

+ (CZGeminiAPI<CZGeminiAPIFirmwareVersionInfo *> *)apiForGetFirmwareVersionInfo {
    return [self apiWithMethod:@"GET" path:@"firmwareVersion" parameters:nil mapper:^CZGeminiAPIFirmwareVersionInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIFirmwareVersionInfo *firmwareVersionInfo = [CZGeminiAPIFirmwareVersionInfo yy_modelWithJSON:result.data];
        return firmwareVersionInfo;
    }];
}

#pragma mark - Sharpness Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetSharpness {
    return [self apiWithMethod:@"GET" path:@"sharpness" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPISharpnessInfo *sharpnessInfo = [CZGeminiAPISharpnessInfo yy_modelWithJSON:result.data];
        return sharpnessInfo.sharpness;
    }];
}

+ (CZGeminiAPI *)apiForSetSharpness:(NSUInteger)sharpness {
    CZGeminiAPISharpnessInfo *sharpnessInfo = [[CZGeminiAPISharpnessInfo alloc] init];
    sharpnessInfo.sharpness = @(sharpness);
    NSDictionary *parameters = [sharpnessInfo yy_modelToJSONObject];
    [sharpnessInfo release];
    return [self apiWithMethod:@"PUT" path:@"sharpness" parameters:parameters mapper:nil];
}

#pragma mark - Denoise Mode Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetDenoiseMode {
    return [self apiWithMethod:@"GET" path:@"denoise" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIDenoiseModeInfo *denoiseModeInfo = [CZGeminiAPIDenoiseModeInfo yy_modelWithJSON:result.data];
        return denoiseModeInfo.denoiseMode;
    }];
}

+ (CZGeminiAPI *)apiForSetDenoiseMode:(CZGeminiAPIDenoiseMode)denoiseMode {
    CZGeminiAPIDenoiseModeInfo *denoiseModeInfo = [[CZGeminiAPIDenoiseModeInfo alloc] init];
    denoiseModeInfo.denoiseMode = @(denoiseMode);
    NSDictionary *parameters = [denoiseModeInfo yy_modelToJSONObject];
    [denoiseModeInfo release];
    return [self apiWithMethod:@"PUT" path:@"denoise" parameters:parameters mapper:nil];
}

#pragma mark - Exposure Info APIs

+ (CZGeminiAPI<CZGeminiAPIExposureInfo *> *)apiForGetExposureInfo {
    return [self apiWithMethod:@"GET" path:@"exposure" parameters:nil mapper:^CZGeminiAPIExposureInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureInfo *exposureInfo = [CZGeminiAPIExposureInfo yy_modelWithJSON:result.data];
        return exposureInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetExposureInfo:(CZGeminiAPIExposureInfo *)exposureInfo {
    NSDictionary *parameters = [exposureInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"exposure" parameters:parameters mapper:nil];
}

#pragma mark - Exposure Stabilization APIs

+ (CZGeminiAPI *)apiForGetExposureStabilizationStatus {
    return [self apiWithMethod:@"GET" path:@"exposure/stabilization" parameters:nil mapper:nil];
}

#pragma mark - Gain Info APIs

+ (CZGeminiAPI<CZGeminiAPIGainInfo *> *)apiForGetGainInfo {
    return [self apiWithMethod:@"GET" path:@"exposure/gain" parameters:nil mapper:^CZGeminiAPIGainInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureInfo *exposureInfo = [CZGeminiAPIExposureInfo yy_modelWithJSON:result.data];
        return exposureInfo.gain;
    }];
}

+ (CZGeminiAPI *)apiForSetGain:(NSUInteger)gain {
    NSDictionary *parameters = @{@"gain" : @(gain)};
    return [self apiWithMethod:@"PUT" path:@"exposure/gain" parameters:parameters mapper:nil];
}

#pragma mark - Exposure Time Info APIs

+ (CZGeminiAPI<CZGeminiAPIExposureTimeInfo *> *)apiForGetExposureTimeInfo {
    return [self apiWithMethod:@"GET" path:@"exposure/exptime" parameters:nil mapper:^CZGeminiAPIExposureTimeInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureInfo *exposureInfo = [CZGeminiAPIExposureInfo yy_modelWithJSON:result.data];
        return exposureInfo.exposureTime;
    }];
}

+ (CZGeminiAPI *)apiForSetExposureTime:(float)exposureTime {
    NSDictionary *parameters = @{@"exptime" : @(exposureTime)};
    return [self apiWithMethod:@"PUT" path:@"exposure/exptime" parameters:parameters mapper:nil];
}

#pragma mark - Exposure Type APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetExposureType {
    return [self apiWithMethod:@"GET" path:@"exposure/exptype" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureTypeInfo *exposureTypeInfo = [CZGeminiAPIExposureTypeInfo yy_modelWithJSON:result.data];
        return exposureTypeInfo.exposureType;
    }];
}

+ (CZGeminiAPI<CZGeminiAPIExposureTypeInfo *> *)apiForSetExposureType:(CZGeminiAPIExposureType)exposureType once:(BOOL)once {
    CZGeminiAPIExposureTypeInfo *exposureTypeInfo = [[CZGeminiAPIExposureTypeInfo alloc] init];
    exposureTypeInfo.exposureType = @(exposureType);
    if (once) {
        exposureTypeInfo.action = @"once";
    }
    NSDictionary *parameters = [exposureTypeInfo yy_modelToJSONObject];
    [exposureTypeInfo release];
    return [self apiWithMethod:@"PUT" path:@"exposure/exptype" parameters:parameters mapper:^CZGeminiAPIExposureTypeInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureTypeInfo *exposureTypeInfo = [CZGeminiAPIExposureTypeInfo yy_modelWithJSON:result.data];
        return exposureTypeInfo;
    }];
}

#pragma mark - Exposure Compenation Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetExposureCompenation {
    return [self apiWithMethod:@"GET" path:@"exposure/compenation" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIExposureCompenationInfo *exposureCompenationInfo = [CZGeminiAPIExposureCompenationInfo yy_modelWithJSON:result.data];
        return exposureCompenationInfo.compenation;
    }];
}

+ (CZGeminiAPI *)apiForSetExposureCompenation:(NSInteger)exposureCompenation {
    CZGeminiAPIExposureCompenationInfo *exposureCompenationInfo = [[CZGeminiAPIExposureCompenationInfo alloc] init];
    exposureCompenationInfo.compenation = @(exposureCompenation);
    NSDictionary *parameters = [exposureCompenationInfo yy_modelToJSONObject];
    [exposureCompenationInfo release];
    return [self apiWithMethod:@"PUT" path:@"exposure/compenation" parameters:parameters mapper:nil];
}

#pragma mark - White Balance Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetWhiteBalanceMode {
    return [self apiWithMethod:@"GET" path:@"awb" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [CZGeminiAPIWhiteBalanceInfo yy_modelWithJSON:result.data];
        return whiteBalanceInfo.whiteBalanceMode;
    }];
}

+ (CZGeminiAPI<CZGeminiAPIWhiteBalanceInfo *> *)apiForSetWhiteBalanceMode:(CZGeminiAPIWhiteBalanceMode)whiteBalanceMode once:(BOOL)once {
    CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [[CZGeminiAPIWhiteBalanceInfo alloc] init];
    whiteBalanceInfo.whiteBalanceMode = @(whiteBalanceMode);
    if (once) {
        whiteBalanceInfo.action = @"once";
    }
    NSDictionary *parameters = [whiteBalanceInfo yy_modelToJSONObject];
    [whiteBalanceInfo release];
    return [self apiWithMethod:@"PUT" path:@"awb" parameters:parameters mapper:^CZGeminiAPIWhiteBalanceInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [CZGeminiAPIWhiteBalanceInfo yy_modelWithJSON:result.data];
        return whiteBalanceInfo;
    }];
}

+ (CZGeminiAPI<NSNumber *> *)apiForGetColorTemperature {
    return [self apiWithMethod:@"GET" path:@"colorTemp" parameters:nil mapper:^id(CZURLSessionTaskResult *result) {
        CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [CZGeminiAPIWhiteBalanceInfo yy_modelWithJSON:result.data];
        return whiteBalanceInfo.colorTemperature;
    }];
}

+ (CZGeminiAPI *)apiForSetColorTemperature:(NSUInteger)colorTemperature {
    CZGeminiAPIWhiteBalanceInfo *whiteBalanceInfo = [[CZGeminiAPIWhiteBalanceInfo alloc] init];
    whiteBalanceInfo.colorTemperature = @(colorTemperature);
    NSDictionary *parameters = [whiteBalanceInfo yy_modelToJSONObject];
    [whiteBalanceInfo release];
    return [self apiWithMethod:@"PUT" path:@"colorTemp" parameters:parameters mapper:nil];
}

#pragma mark - Real Exposure Time Info APIs

+ (CZGeminiAPI<CZGeminiAPIRealExposureTimeInfo *> *)apiForGetRealExposureTimeInfo {
    return [self apiWithMethod:@"GET" path:@"realExposure" parameters:nil mapper:^CZGeminiAPIRealExposureTimeInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIRealExposureTimeInfo *realExposureTimeInfo = [CZGeminiAPIRealExposureTimeInfo yy_modelWithJSON:result.data];
        return realExposureTimeInfo;
    }];
}

#pragma mark - Illumination Info APIs

+ (CZGeminiAPI<CZGeminiAPILightInfo *> *)apiForGetReflectedLightInfo {
    return [self apiWithMethod:@"GET" path:@"illumination/rl" parameters:nil mapper:^CZGeminiAPILightInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILightInfo *lightInfo = [CZGeminiAPILightInfo yy_modelWithJSON:result.data];
        return lightInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPILightInfo *> *)apiForGetTransmittedLightInfo {
    return [self apiWithMethod:@"GET" path:@"illumination/tl" parameters:nil mapper:^CZGeminiAPILightInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILightInfo *lightInfo = [CZGeminiAPILightInfo yy_modelWithJSON:result.data];
        return lightInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetReflectedLightInfo:(CZGeminiAPILightInfo *)lightInfo {
    NSDictionary *parameters = [lightInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"illumination/rl" parameters:parameters mapper:nil];
}

+ (CZGeminiAPI *)apiForSetTransmittedLightInfo:(CZGeminiAPILightInfo *)lightInfo {
    NSDictionary *parameters = [lightInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"illumination/tl" parameters:parameters mapper:nil];
}

#pragma mark - LED Info APIs

+ (CZGeminiAPI<CZGeminiAPILEDsInfo *> *)apiForGetLEDsInfo {
    return [self apiWithMethod:@"GET" path:@"led" parameters:nil mapper:^CZGeminiAPILEDsInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILEDsInfo *ledsInfo = [CZGeminiAPILEDsInfo yy_modelWithJSON:result.data];
        return ledsInfo;
    }];
}

+ (CZGeminiAPI<NSNumber *> *)apiForGetCurrentLEDPosition {
    return [self apiWithMethod:@"GET" path:@"led/position" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPILEDsInfo *ledsInfo = [CZGeminiAPILEDsInfo yy_modelWithJSON:result.data];
        return @(ledsInfo.position);
    }];
}

+ (CZGeminiAPI<CZGeminiAPILEDInfo *> *)apiForGetLEDInfoAtPosition:(NSInteger)position {
    NSString *path = [NSString stringWithFormat:@"led/%ld", (long)position];
    return [self apiWithMethod:@"GET" path:path parameters:nil mapper:^CZGeminiAPILEDInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILEDInfo *ledInfo = [CZGeminiAPILEDInfo yy_modelWithJSON:result.data];
        return ledInfo;
    }];
}

+ (CZGeminiAPI *)apiForSetLEDInfo:(CZGeminiAPILEDInfo *)ledInfo atPosition:(NSInteger)position {
    NSString *path = [NSString stringWithFormat:@"led/%ld", (long)position];
    NSDictionary *parameters = [ledInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:path parameters:parameters mapper:nil];
}

#pragma mark - Bit Rate Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetBitRate {
    return [self apiWithMethod:@"GET" path:@"bitRate" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIBitRateInfo *bitRateInfo = [CZGeminiAPIBitRateInfo yy_modelWithJSON:result.data];
        return bitRateInfo.bitRate;
    }];
}

+ (CZGeminiAPI *)apiForSetBitRate:(NSUInteger)bitRate {
    CZGeminiAPIBitRateInfo *bitRateInfo = [[CZGeminiAPIBitRateInfo alloc] init];
    bitRateInfo.bitRate = @(bitRate);
    NSDictionary *parameters = [bitRateInfo yy_modelToJSONObject];
    [bitRateInfo release];
    return [self apiWithMethod:@"PUT" path:@"bitRate" parameters:parameters mapper:nil];
}

#pragma mark - Color Mode Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetColorMode {
    return [self apiWithMethod:@"GET" path:@"color2Gray" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIColorModeInfo *colorModeInfo = [CZGeminiAPIColorModeInfo yy_modelWithJSON:result.data];
        return colorModeInfo.colorMode;
    }];
}

+ (CZGeminiAPI *)apiForSetColorMode:(CZGeminiAPIColorMode)colorMode {
    CZGeminiAPIColorModeInfo *colorModeInfo = [[CZGeminiAPIColorModeInfo alloc] init];
    colorModeInfo.colorMode = @(colorMode);
    NSDictionary *parameters = [colorModeInfo yy_modelToJSONObject];
    [colorModeInfo release];
    return [self apiWithMethod:@"PUT" path:@"color2Gray" parameters:parameters mapper:nil];
}

#pragma mark - Colibri LED Status APIs

+ (CZGeminiAPI *)apiForSetColibriLEDStatus:(BOOL)status enabled:(BOOL)enabled {
    NSDictionary *parameters = @{@"status" : @(status), @"enable" : @(enabled)};
    return [self apiWithMethod:@"PUT" path:@"colibri" parameters:parameters mapper:nil];
}

#pragma mark - Lock APIs

+ (NSString *)lockID {
    static NSString *lockID = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lockID = [[[NSUUID UUID] UUIDString] retain];
    });
    return lockID;
}

+ (CZGeminiAPI *)apiForSendHeartBeat {
    CZGeminiAPILockInfo *lockInfo = [[CZGeminiAPILockInfo alloc] init];
    lockInfo.clientID = [self lockID];
    NSDictionary *parameters = [lockInfo yy_modelToJSONObject];
    [lockInfo release];
    return [self apiWithMethod:@"PUT" path:@"heartbeat" parameters:parameters mapper:nil];
}

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForLockConfiguration {
    CZGeminiAPILockInfo *lockInfo = [[CZGeminiAPILockInfo alloc] init];
    lockInfo.clientID = [self lockID];
    lockInfo.lockStatus = @1;
    NSDictionary *parameters = [lockInfo yy_modelToJSONObject];
    [lockInfo release];
    return [self apiWithMethod:@"PUT" path:@"requestConfigLock" parameters:parameters mapper:^CZGeminiAPILockInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILockInfo *lockInfo = [CZGeminiAPILockInfo yy_modelWithJSON:result.data];
        return lockInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForUnlockConfiguration {
    CZGeminiAPILockInfo *lockInfo = [[CZGeminiAPILockInfo alloc] init];
    lockInfo.clientID = [self lockID];
    lockInfo.lockStatus = @0;
    NSDictionary *parameters = [lockInfo yy_modelToJSONObject];
    [lockInfo release];
    return [self apiWithMethod:@"PUT" path:@"releaseConfigLock" parameters:parameters mapper:^CZGeminiAPILockInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILockInfo *lockInfo = [CZGeminiAPILockInfo yy_modelWithJSON:result.data];
        return lockInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForLockSnap {
    CZGeminiAPILockInfo *lockInfo = [[CZGeminiAPILockInfo alloc] init];
    lockInfo.clientID = [self lockID];
    lockInfo.lockStatus = @1;
    NSDictionary *parameters = [lockInfo yy_modelToJSONObject];
    [lockInfo release];
    return [self apiWithMethod:@"PUT" path:@"requestSnapLock" parameters:parameters mapper:^CZGeminiAPILockInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILockInfo *lockInfo = [CZGeminiAPILockInfo yy_modelWithJSON:result.data];
        return lockInfo;
    }];
}

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForUnlockSnap {
    CZGeminiAPILockInfo *lockInfo = [[CZGeminiAPILockInfo alloc] init];
    lockInfo.clientID = [self lockID];
    lockInfo.lockStatus = @0;
    NSDictionary *parameters = [lockInfo yy_modelToJSONObject];
    [lockInfo release];
    return [self apiWithMethod:@"PUT" path:@"releaseSnapLock" parameters:parameters mapper:^CZGeminiAPILockInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPILockInfo *lockInfo = [CZGeminiAPILockInfo yy_modelWithJSON:result.data];
        return lockInfo;
    }];
}

#pragma mark - Client Number APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetClientNumber {
    return [self apiWithMethod:@"GET" path:@"clientNum" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIClientNumberInfo *clientNumberInfo = [CZGeminiAPIClientNumberInfo yy_modelWithJSON:result.data];
        return clientNumberInfo.clientNumber;
    }];
}

#pragma mark - HDR Mode Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetHDRMode {
    return [self apiWithMethod:@"GET" path:@"hdr" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIHDRModeInfo *hdrModeInfo = [CZGeminiAPIHDRModeInfo yy_modelWithJSON:result.data];
        return hdrModeInfo.hdrMode;
    }];
}

+ (CZGeminiAPI *)apiForSetHDRMode:(CZGeminiAPIHDRMode)hdrMode {
    CZGeminiAPIHDRModeInfo *hdrModeInfo = [[CZGeminiAPIHDRModeInfo alloc] init];
    hdrModeInfo.hdrMode = @(hdrMode);
    NSDictionary *parameters = [hdrModeInfo yy_modelToJSONObject];
    [hdrModeInfo release];
    return [self apiWithMethod:@"PUT" path:@"hdr" parameters:parameters mapper:nil];
}

#pragma mark - Shading Correction APIs

+ (CZGeminiAPI *)apiForSetShadingCorrection:(CZGeminiAPIShadingCorrectionInfo *)shadingCorrectionInfo {
    NSDictionary *parameters = [shadingCorrectionInfo yy_modelToJSONObject];
    return [self apiWithMethod:@"PUT" path:@"shading" parameters:parameters mapper:nil];
}

#pragma mark - Dynamic Defect Pixel Info APIs

+ (CZGeminiAPI<NSNumber *> *)apiForGetDynamicDefectPixel {
    return [self apiWithMethod:@"GET" path:@"dynamicDefectPixel" parameters:nil mapper:^NSNumber *(CZURLSessionTaskResult *result) {
        CZGeminiAPIDynamicDefectPixelInfo *dynamicDefectPixelInfo = [CZGeminiAPIDynamicDefectPixelInfo yy_modelWithJSON:result.data];
        return dynamicDefectPixelInfo.dynamicDefectPixel;
    }];
}

+ (CZGeminiAPI *)apiForSetDynamicDefectPixel:(CZGeminiAPIDynamicDefectPixel)dynamicDefectPixel {
    CZGeminiAPIDynamicDefectPixelInfo *dynamicDefectPixelInfo = [[CZGeminiAPIDynamicDefectPixelInfo alloc] init];
    dynamicDefectPixelInfo.dynamicDefectPixel = @(dynamicDefectPixel);
    NSDictionary *parameters = [dynamicDefectPixelInfo yy_modelToJSONObject];
    [dynamicDefectPixelInfo release];
    return [self apiWithMethod:@"PUT" path:@"dynamicDefectPixel" parameters:parameters mapper:nil];
}

#pragma mark - Gamma Info APIs

+ (CZGeminiAPI<CZGeminiAPIGammaInfo *> *)apiForGetGammaInfo {
    return [self apiWithMethod:@"GET" path:@"gamma" parameters:nil mapper:^CZGeminiAPIGammaInfo *(CZURLSessionTaskResult *result) {
        CZGeminiAPIGammasInfo *gammasInfo = [CZGeminiAPIGammasInfo yy_modelWithJSON:result.data];
        return gammasInfo.gamma;
    }];
}

+ (CZGeminiAPI *)apiForSetGamma:(float)gamma {
    CZGeminiAPIGammaInfo *gammaInfo = [[CZGeminiAPIGammaInfo alloc] init];
    gammaInfo.gamma = @(gamma);
    NSDictionary *parameters = [gammaInfo yy_modelToJSONObject];
    [gammaInfo release];
    return [self apiWithMethod:@"PUT" path:@"gamma" parameters:parameters mapper:nil];
}

#pragma mark - API Lifecycle

+ (instancetype)apiWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters mapper:(id (^)(CZURLSessionTaskResult *))mapper {
    return [[[self alloc] initWithMethod:method path:path parameters:parameters mapper:mapper] autorelease];
}

- (instancetype)initWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters mapper:(id (^)(CZURLSessionTaskResult *))mapper {
    self = [super init];
    if (self) {
        _method = [method copy];
        _path = [path copy];
        _parameters = [parameters copy];
        _mapper = [mapper copy];
    }
    return self;
}

- (void)dealloc {
    [_method release];
    [_path release];
    [_parameters release];
    [_mapper release];
    [super dealloc];
}

- (id)mapResult:(CZURLSessionTaskResult *)result {
    if (self.mapper) {
        return self.mapper(result);
    } else {
        return result;
    }
}

@end
