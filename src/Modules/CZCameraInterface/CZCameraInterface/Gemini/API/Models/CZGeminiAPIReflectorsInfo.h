//
//  CZGeminiAPIReflectorsInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIReflectorInfo.h"

@interface CZGeminiAPIReflectorsInfo : NSObject <YYModel>

@property (nonatomic, copy)   NSArray<CZGeminiAPIReflectorInfo *> *reflectors;
@property (nonatomic, assign) NSInteger positionCount;
@property (nonatomic, assign) NSInteger position;

- (CZGeminiAPIReflectorInfo *)reflectorInfoAtCurrentPosition;
- (CZGeminiAPIReflectorInfo *)reflectorInfoAtPosition:(NSInteger)position;

@end
