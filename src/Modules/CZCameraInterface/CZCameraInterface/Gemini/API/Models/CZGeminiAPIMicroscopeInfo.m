//
//  CZGeminiAPIMicroscopeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIMicroscopeInfo.h"

@implementation CZGeminiAPIMicroscopeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"hasFluorescence" : @"hasFl",
             @"isMultiFluorescence" : @"isMultiFl",
             @"hasFluorescenceLEDs" : @"hasFL_LEDs",
             @"hasReflectedLightSwitch" : @"hasRLSwitch",
             @"hasTransmittedLightSwitch" : @"hasTLSwitch",
             @"microscopeName" : @"micName"};
}

- (void)dealloc {
    [_microscopeName release];
    [_reflector release];
    [_led release];
    [_objective release];
    [_illumination release];
    [super dealloc];
}

@end
