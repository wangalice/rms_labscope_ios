//
//  CZGeminiAPIScaleBarInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIScaleBarInfo.h"

@implementation CZGeminiAPIScaleBarInfo

- (void)dealloc {
    [_show release];
    [super dealloc];
}

@end
