//
//  CZGeminiAPIReflectorInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIReflectorsInfo.h"

@implementation CZGeminiAPIReflectorsInfo

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"reflectors" : [CZGeminiAPIReflectorInfo class]};
}

- (void)dealloc {
    [_reflectors release];
    [super dealloc];
}

- (CZGeminiAPIReflectorInfo *)reflectorInfoAtCurrentPosition {
    return [self reflectorInfoAtPosition:self.position];
}

- (CZGeminiAPIReflectorInfo *)reflectorInfoAtPosition:(NSInteger)position {
    NSUInteger index = position - 1;
    if (index >= 0 && index < self.reflectors.count) {
        return self.reflectors[index];
    } else {
        return nil;
    }
}

@end
