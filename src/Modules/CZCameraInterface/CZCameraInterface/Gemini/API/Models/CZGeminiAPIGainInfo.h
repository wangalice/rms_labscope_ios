//
//  CZGeminiAPIGainInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIGainInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *gainMax; // NSUInteger
@property (nonatomic, retain) NSNumber *gainMin; // NSUInteger
@property (nonatomic, retain) NSNumber *gain;    // NSUInteger

@end
