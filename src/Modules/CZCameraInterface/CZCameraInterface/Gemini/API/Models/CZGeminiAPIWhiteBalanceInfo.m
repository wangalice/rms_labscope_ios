//
//  CZGeminiAPIWhiteBalanceInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIWhiteBalanceInfo.h"

@implementation CZGeminiAPIWhiteBalanceInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"whiteBalanceMode" : @"awb",
             @"colorTemperature" : @"colorTemp"};
}

- (void)dealloc {
    [_whiteBalanceMode release];
    [_action release];
    [_colorTemperature release];
    [super dealloc];
}

@end
