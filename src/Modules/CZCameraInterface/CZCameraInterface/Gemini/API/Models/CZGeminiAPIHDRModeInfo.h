//
//  CZGeminiAPIHDRModeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

typedef NS_ENUM(NSInteger, CZGeminiAPIHDRMode) {
    CZGeminiAPIHDRModeOff = 0,
    CZGeminiAPIHDRModeOn = 3
};

@interface CZGeminiAPIHDRModeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *hdrMode; // CZGeminiAPIHDRMode

@end
