//
//  CZGeminiAPILockInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPILockInfo : NSObject <YYModel>
/* The client identifier */
@property (nonatomic, copy) NSString *clientID;
/* lockStatus is 0: not to lock, otherwise lock */
@property (nonatomic, retain) NSNumber *lockStatus; // NSInteger

@end
