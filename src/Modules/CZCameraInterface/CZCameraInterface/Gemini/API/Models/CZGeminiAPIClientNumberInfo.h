//
//  CZGeminiAPIClientNumberInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIClientNumberInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *clientNumber;   // NSUInteger

@end
