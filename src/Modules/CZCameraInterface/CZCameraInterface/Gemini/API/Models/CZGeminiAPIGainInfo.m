//
//  CZGeminiAPIGainInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIGainInfo.h"

@implementation CZGeminiAPIGainInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"gain" : @"value"};
}

- (void)dealloc {
    [_gainMax release];
    [_gainMin release];
    [_gain release];
    [super dealloc];
}

@end
