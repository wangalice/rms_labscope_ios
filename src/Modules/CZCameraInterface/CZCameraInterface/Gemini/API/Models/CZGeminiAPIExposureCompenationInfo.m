//
//  CZGeminiAPIExposureCompenationInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIExposureCompenationInfo.h"

const NSInteger CZGeminiAPIMinExposureCompenation = 1;
const NSInteger CZGeminiAPIMaxExposureCompenation = 250;

@implementation CZGeminiAPIExposureCompenationInfo

- (void)dealloc {
    [_compenation release];
    [super dealloc];
}

@end
