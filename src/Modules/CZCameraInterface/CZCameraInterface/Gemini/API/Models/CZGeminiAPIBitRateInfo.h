//
//  CZGeminiAPIBitRateInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIBitRateInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *bitRate; // NSUInteger

@end
