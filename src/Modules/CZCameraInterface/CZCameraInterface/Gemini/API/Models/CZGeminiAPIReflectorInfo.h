//
//  CZGeminiAPIReflectorInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIEffectInfo.h"

@interface CZGeminiAPIReflectorInfo : NSObject <YYModel>

@property (nonatomic, copy) NSString *matID;
@property (nonatomic, copy) NSArray<CZGeminiAPIEffectInfo *> *objectives;

- (CZGeminiAPIEffectInfo *)effectInfoAtPosition:(NSInteger)position;

@end
