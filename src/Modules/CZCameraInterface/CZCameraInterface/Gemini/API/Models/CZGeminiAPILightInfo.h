//
//  CZGeminiAPILightInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPILightInfo : NSObject <YYModel>

@property (nonatomic, copy)   NSString *status;               ///< "on" or "off"
@property (nonatomic, retain) NSNumber *brightness;
@property (nonatomic, retain) NSNumber *brightnessMin;
@property (nonatomic, retain) NSNumber *brightnessMax;
@property (nonatomic, copy)   NSString *type;

- (BOOL)isLightOn;

@end
