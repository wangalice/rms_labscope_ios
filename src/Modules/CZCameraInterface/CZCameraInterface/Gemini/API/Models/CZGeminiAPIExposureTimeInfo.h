//
//  CZGeminiAPIExposureTimeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIExposureTimeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *exposureTimeMax; // float
@property (nonatomic, retain) NSNumber *exposureTimeMin; // float
@property (nonatomic, retain) NSNumber *exposureTime;    // float

@end
