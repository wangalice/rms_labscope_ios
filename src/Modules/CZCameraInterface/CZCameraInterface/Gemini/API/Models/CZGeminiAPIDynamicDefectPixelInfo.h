//
//  CZGeminiAPIDynamicDefectPixelInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

typedef NS_ENUM(NSInteger, CZGeminiAPIDynamicDefectPixel) {
    CZGeminiAPIDynamicDefectPixelClose = 0,
    CZGeminiAPIDynamicDefectPixelOpen = 1
};

@interface CZGeminiAPIDynamicDefectPixelInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *dynamicDefectPixel; // CZGeminiAPIDynamicDefectPixel

@end
