//
//  CZGeminiAPISnapInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIEnumerations.h"

typedef NS_ENUM(NSInteger, CZGeminiAPISnapFormat) {
    CZGeminiAPISnapFormatYUV = 0,
    CZGeminiAPISnapFormatGrayBayer = 1
};

typedef NS_ENUM(NSInteger, CZGeminiAPISnapResolution) {
    CZGeminiAPISnapResolution4K = 0,
    CZGeminiAPISnapResolution1080p = 1
};

@interface CZGeminiAPISnapInfo : NSObject <YYModel>

@property (nonatomic, assign) CZGeminiAPIExposureType exposureType;
@property (nonatomic, retain) NSNumber *exposureTimeInMicroseconds;
@property (nonatomic, retain) NSNumber *gain;
@property (nonatomic, assign) CZGeminiAPISnapFormat format;

@end
