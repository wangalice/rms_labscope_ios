//
//  CZGeminiAPIObjectiveInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIObjectivesInfo.h"

@implementation CZGeminiAPIObjectivesInfo

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"objectives" : [CZGeminiAPIObjectiveInfo class]};
}

- (void)dealloc {
    [_type release];
    [_objectives release];
    [super dealloc];
}

@end
