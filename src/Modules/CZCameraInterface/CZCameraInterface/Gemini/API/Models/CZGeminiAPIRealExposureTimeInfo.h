//
//  CZGeminiAPIRealExposureTimeInfo.h
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIRealExposureTimeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *exposureTime;   // float
@property (nonatomic, retain) NSNumber *gain;           // NSUInteger
@property (nonatomic, retain) NSNumber *ispGain;        // NSUInteger
@property (nonatomic, retain) NSNumber *digitalGain;    // NSUInteger

@end
