//
//  CZGeminiAPIEnumerations.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CZGeminiAPICameraModelType) {
    CZGeminiAPICameraModelTypeUnknown,
    CZGeminiAPICameraModelTypeAxiocam202,
    CZGeminiAPICameraModelTypeAxiocam208
};

typedef NS_ENUM(NSInteger, CZGeminiAPIExposureType) {
    CZGeminiAPIExposureTypeAuto,
    CZGeminiAPIExposureTypeManual
};

typedef NS_ENUM(NSInteger, CZGeminiAPIWhiteBalanceMode) {
    CZGeminiAPIWhiteBalanceModeAuto,
    CZGeminiAPIWhiteBalanceModeManual,
    CZGeminiAPIWhiteBalanceModeAsEyepieces,
    CZGeminiAPIWhiteBalanceModeNormalWhite = CZGeminiAPIWhiteBalanceModeAuto,
};
