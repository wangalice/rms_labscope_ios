//
//  CZGeminiAPILightInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPILightInfo.h"

@implementation CZGeminiAPILightInfo

- (void)dealloc {
    [_status release];
    [_brightness release];
    [_brightnessMin release];
    [_brightnessMax release];
    [_type release];
    [super dealloc];
}

- (BOOL)isLightOn {
    return [self.status.lowercaseString isEqualToString:@"on"];
}

@end
