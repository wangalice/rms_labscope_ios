//
//  CZGeminiAPIScaleBarInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIScaleBarInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *show; // BOOL

@end
