//
//  CZGeminiAPISharpnessInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPISharpnessInfo.h"

@implementation CZGeminiAPISharpnessInfo

- (void)dealloc {
    [_sharpness release];
    [super dealloc];
}

@end
