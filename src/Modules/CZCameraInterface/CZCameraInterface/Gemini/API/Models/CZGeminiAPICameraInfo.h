//
//  CZGeminiAPICameraInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIEnumerations.h"
#import "CZGeminiAPIScaleBarInfo.h"
#import "CZGeminiAPIMirrorInfo.h"
#import "CZGeminiAPIFilenameTemplateInfo.h"

@interface CZGeminiAPICameraInfo : NSObject <YYModel>

@property (nonatomic, assign) double gain;
@property (nonatomic, assign) NSInteger exposureTime;
@property (nonatomic, assign) NSInteger colorTemperature;
@property (nonatomic, assign) NSInteger rGain;
@property (nonatomic, assign) NSInteger grGain;
@property (nonatomic, assign) NSInteger gbGain;
@property (nonatomic, assign) NSInteger bGain;
@property (nonatomic, assign) NSInteger saturation;
@property (nonatomic, assign) NSInteger whiteBalance;
@property (nonatomic, assign) NSInteger hdr;
@property (nonatomic, assign) NSInteger denoise;
@property (nonatomic, assign) NSInteger resolution;
@property (nonatomic, assign) NSInteger rtspResolution;
@property (nonatomic, assign) NSInteger sharpness;
@property (nonatomic, assign) NSInteger snapFormat;
@property (nonatomic, assign) NSInteger exposureType;
@property (nonatomic, assign) NSInteger channel;
@property (nonatomic, assign) NSInteger captureMode;
@property (nonatomic, assign) BOOL showFullScreen;
@property (nonatomic, assign) NSInteger aeCompensation;
@property (nonatomic, assign) NSInteger colorMode;
@property (nonatomic, assign) NSInteger dynamicDefectPixel;
@property (nonatomic, assign) NSInteger bitRate;
@property (nonatomic, assign) NSInteger standAloneShading;
@property (nonatomic, assign) float gamma;
@property (nonatomic, assign) NSInteger aePercent;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *wifiMode;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *reflectorVersion;
@property (nonatomic, copy) NSString *objectiveVersion;
@property (nonatomic, retain) CZGeminiAPIScaleBarInfo *scaleBar;
@property (nonatomic, retain) CZGeminiAPIMirrorInfo *mirror;
@property (nonatomic, retain) CZGeminiAPIFilenameTemplateInfo *filenameTemplate;
@property (nonatomic, copy) NSString *firmwareVersion;
@property (nonatomic, copy) NSString *modelName;
@property (nonatomic, copy) NSString *firmwareBuild;

- (CZGeminiAPICameraModelType)modelType;

@end
