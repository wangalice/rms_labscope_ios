//
//  CZGeminiAPIGammaInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIGammaInfo.h"

@implementation CZGeminiAPIGammaInfo

- (void)dealloc {
    [_gamma release];
    [_gammaMin release];
    [_gammaMax release];
    [_gammaRes release];
    [_gammaDef release];
    [super dealloc];
}

@end

@implementation CZGeminiAPIGammasInfo

- (void)dealloc {
    [_gamma release];
    [super dealloc];
}

@end
