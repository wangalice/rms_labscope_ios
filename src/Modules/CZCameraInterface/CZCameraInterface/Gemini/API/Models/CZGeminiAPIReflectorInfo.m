//
//  CZGeminiAPIReflectorInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIReflectorInfo.h"

@implementation CZGeminiAPIReflectorInfo

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"objectives" : [CZGeminiAPIEffectInfo class]};
}

- (void)dealloc {
    [_matID release];
    [_objectives release];
    [super dealloc];
}

- (CZGeminiAPIEffectInfo *)effectInfoAtPosition:(NSInteger)position {
    NSUInteger index = position - 1;
    if (index >= 0 && index < self.objectives.count) {
        return self.objectives[index];
    } else {
        return nil;
    }
}

@end
