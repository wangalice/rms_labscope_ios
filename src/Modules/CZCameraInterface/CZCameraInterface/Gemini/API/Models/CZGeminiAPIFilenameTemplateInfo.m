//
//  CZGeminiAPIFilenameTemplateInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIFilenameTemplateInfo.h"

@implementation CZGeminiAPIFilenameTemplateInfo

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"customText" : [NSString class]};
}

- (void)dealloc {
    [_customText release];
    [super dealloc];
}

@end
