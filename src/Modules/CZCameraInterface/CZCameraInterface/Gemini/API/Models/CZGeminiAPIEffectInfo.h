//
//  CZGeminiAPIEffectInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIEffectInfo : NSObject <YYModel>

@property (nonatomic, assign) BOOL isRlShadingCorrect;
@property (nonatomic, assign) BOOL isTlShadingCorrect;
@property (nonatomic, assign) BOOL hasRlShadingTable;
@property (nonatomic, assign) BOOL hasTlShadingTable;
@property (nonatomic, assign) NSInteger tlAwbType;
@property (nonatomic, assign) NSInteger rlAwbType;
@property (nonatomic, assign) NSInteger tlColorTemp;
@property (nonatomic, assign) NSInteger RlColorTemp;
@property (nonatomic, assign) NSInteger tlRgain;
@property (nonatomic, assign) NSInteger tlGrgain;
@property (nonatomic, assign) NSInteger tlGbgain;
@property (nonatomic, assign) NSInteger tlGgain;
@property (nonatomic, assign) NSInteger rlRgain;
@property (nonatomic, assign) NSInteger rlGrgain;
@property (nonatomic, assign) NSInteger rlGbgain;
@property (nonatomic, assign) NSInteger rlGgain;

@end
