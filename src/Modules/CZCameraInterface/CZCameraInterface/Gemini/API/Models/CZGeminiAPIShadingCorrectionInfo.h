//
//  CZGeminiAPIShadingCorrectionInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 7/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIShadingCorrectionInfo : NSObject <YYModel>

@property (nonatomic, assign) NSInteger objectivePosition;
@property (nonatomic, assign) NSInteger reflectorPosition;
@property (nonatomic, assign) NSInteger rltlPosition;
@property (nonatomic, retain) NSNumber *on; // BOOL
@property (nonatomic, copy) NSString *action;

@end
