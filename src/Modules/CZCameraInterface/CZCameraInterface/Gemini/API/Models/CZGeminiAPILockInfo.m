//
//  CZGeminiAPILockInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPILockInfo.h"

@implementation CZGeminiAPILockInfo

- (void)dealloc {
    [_clientID release];
    [_lockStatus release];
    [super dealloc];
}

@end
