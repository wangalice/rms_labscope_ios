//
//  CZGeminiAPIExposureInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIExposureInfo.h"

@implementation CZGeminiAPIExposureInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureTime" : @"exptime"};
}

- (void)dealloc {
    [_exposureTime release];
    [_gain release];
    [_aePercent release];
    [super dealloc];
}

@end
