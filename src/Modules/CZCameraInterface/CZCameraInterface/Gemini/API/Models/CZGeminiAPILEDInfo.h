//
//  CZGeminiAPILEDInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPILEDInfo : NSObject <YYModel>

@property (nonatomic, strong) NSNumber *ledIndex;
@property (nonatomic, strong) NSNumber *brightnessMax;
@property (nonatomic, copy)   NSString *matID;
@property (nonatomic, strong) NSNumber *brightness;
@property (nonatomic, strong) NSNumber *brightnessMin;
@property (nonatomic, strong) NSNumber *waveLength;
@property (nonatomic, strong) NSNumber *oneKeyFluorescenceEnabled;
@property (nonatomic, copy)   NSString *status;

- (BOOL)isLightOn;
- (BOOL)doesLEDExist;

@end
