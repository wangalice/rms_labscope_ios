//
//  CZGeminiAPIWhiteBalanceInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIEnumerations.h"

@interface CZGeminiAPIWhiteBalanceInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *whiteBalanceMode; // CZGeminiAPIWhiteBalanceMode
@property (nonatomic, copy)   NSString *action;
@property (nonatomic, retain) NSNumber *colorTemperature; // NSUInteger

@end
