//
//  CZGeminiAPIRealExposureTimeInfo.m
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/26/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIRealExposureTimeInfo.h"

@implementation CZGeminiAPIRealExposureTimeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureTime" : @"exptime"};
}

- (void)dealloc {
    [_exposureTime release];
    [_gain release];
    [_ispGain release];
    [_digitalGain release];
    [super dealloc];
}

@end
