//
//  CZGeminiAPIExposureInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIExposureTimeInfo.h"
#import "CZGeminiAPIGainInfo.h"

@interface CZGeminiAPIExposureInfo : NSObject <YYModel>

@property (nonatomic, retain) CZGeminiAPIExposureTimeInfo *exposureTime;
@property (nonatomic, retain) CZGeminiAPIGainInfo *gain;
@property (nonatomic, retain) NSNumber *aePercent; // NSInteger

@end
