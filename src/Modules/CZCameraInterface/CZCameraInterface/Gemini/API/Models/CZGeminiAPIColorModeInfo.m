//
//  CZGeminiAPIColorModeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIColorModeInfo.h"

@implementation CZGeminiAPIColorModeInfo

- (void)dealloc {
    [_colorMode release];
    [super dealloc];
}

@end
