//
//  CZGeminiAPISnapInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPISnapInfo.h"

@implementation CZGeminiAPISnapInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureType" : @"expType",
             @"exposureTimeInMicroseconds" : @"expTime",
             @"gain" : @"expGain"};
}

- (void)dealloc {
    [_exposureTimeInMicroseconds release];
    [_gain release];
    [super dealloc];
}

@end
