//
//  CZGeminiAPIGammaInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIGammaInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *gamma;      // float
@property (nonatomic, retain) NSNumber *gammaMin;   // float
@property (nonatomic, retain) NSNumber *gammaMax;   // float
@property (nonatomic, retain) NSNumber *gammaRes;   // float
@property (nonatomic, retain) NSNumber *gammaDef;   // float

@end

@interface CZGeminiAPIGammasInfo : NSObject <YYModel>

@property (nonatomic, retain) CZGeminiAPIGammaInfo *gamma;

@end
