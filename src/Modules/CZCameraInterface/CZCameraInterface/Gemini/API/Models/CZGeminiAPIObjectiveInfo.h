//
//  CZGeminiAPIObjectiveInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIObjectiveInfo : NSObject <YYModel>

@property (nonatomic, copy)   NSString *matID;
@property (nonatomic, copy)   NSString *objectiveClass;
@property (nonatomic, retain) NSNumber *magnification; // float

@end
