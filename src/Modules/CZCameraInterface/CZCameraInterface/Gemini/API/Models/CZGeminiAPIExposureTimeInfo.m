//
//  CZGeminiAPIExposureTimeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIExposureTimeInfo.h"

@implementation CZGeminiAPIExposureTimeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureTimeMax" : @"exptimeMax",
             @"exposureTimeMin" : @"exptimeMin",
             @"exposureTime"    : @"value"};
}

- (void)dealloc {
    [_exposureTimeMax release];
    [_exposureTimeMin release];
    [_exposureTime release];
    [super dealloc];
}

@end
