//
//  CZGeminiAPIClientNumberInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIClientNumberInfo.h"

@implementation CZGeminiAPIClientNumberInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"clientNumber" : @"clientNum"};
}

- (void)dealloc {
    [_clientNumber release];
    [super dealloc];
}

@end
