//
//  CZGeminiAPIFilenameTemplateInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIFilenameTemplateInfo : NSObject <YYModel>

@property (nonatomic, copy) NSArray<NSString *> *customText;
@property (nonatomic, assign) BOOL date;
@property (nonatomic, assign) BOOL time;
@property (nonatomic, assign) NSInteger mode;
@property (nonatomic, assign) NSInteger dateFormat;

@end
