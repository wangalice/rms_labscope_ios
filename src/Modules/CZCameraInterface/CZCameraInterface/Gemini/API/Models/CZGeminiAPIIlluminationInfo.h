//
//  CZGeminiAPIIlluminationInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPILightInfo.h"

@interface CZGeminiAPIIlluminationInfo : NSObject <YYModel>

@property (nonatomic, retain) CZGeminiAPILightInfo *reflectedLight;
@property (nonatomic, retain) CZGeminiAPILightInfo *transmittedLight;

@end
