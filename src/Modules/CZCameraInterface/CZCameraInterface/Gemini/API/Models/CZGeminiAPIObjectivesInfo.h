//
//  CZGeminiAPIObjectivesInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIObjectiveInfo.h"

@interface CZGeminiAPIObjectivesInfo : NSObject <YYModel>

@property (nonatomic, assign) NSInteger positionCount;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, copy)   NSString *type;
@property (nonatomic, copy)   NSArray<CZGeminiAPIObjectiveInfo *> *objectives;

@end
