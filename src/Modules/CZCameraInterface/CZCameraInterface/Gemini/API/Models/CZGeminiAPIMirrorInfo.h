//
//  CZGeminiAPIMirrorInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIMirrorInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *horizontal; // NSInteger 0: original; 1: flipped
@property (nonatomic, retain) NSNumber *vertical;   // NSInteger 0: original; 1: flipped

@end
