//
//  CZGeminiAPIDynamicDefectPixelInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIDynamicDefectPixelInfo.h"

@implementation CZGeminiAPIDynamicDefectPixelInfo

- (void)dealloc {
    [_dynamicDefectPixel release];
    [super dealloc];
}

@end
