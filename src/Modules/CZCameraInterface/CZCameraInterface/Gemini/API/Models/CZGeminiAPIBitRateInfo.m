
//
//  CZGeminiAPIBitRateInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIBitRateInfo.h"

@implementation CZGeminiAPIBitRateInfo

- (void)dealloc {
    [_bitRate release];
    [super dealloc];
}

@end
