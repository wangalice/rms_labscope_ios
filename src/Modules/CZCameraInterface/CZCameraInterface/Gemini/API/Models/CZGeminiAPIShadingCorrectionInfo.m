//
//  CZGeminiAPIShadingCorrectionInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 7/17/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIShadingCorrectionInfo.h"

@implementation CZGeminiAPIShadingCorrectionInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"objectivePosition" : @"objPosition",
             @"reflectorPosition" : @"refPosition",
             @"rltlPosition"      : @"RLTLPosition"};
}

- (void)dealloc {
    [_on release];
    [_action release];
    [super dealloc];
}

@end
