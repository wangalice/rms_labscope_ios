//
//  CZGeminiAPIHDRModeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIHDRModeInfo.h"

@implementation CZGeminiAPIHDRModeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"hdrMode" : @"hdr"};
}

- (void)dealloc {
    [_hdrMode release];
    [super dealloc];
}

@end
