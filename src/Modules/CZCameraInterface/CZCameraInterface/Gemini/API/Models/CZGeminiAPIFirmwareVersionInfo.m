//
//  CZGeminiAPIFirmwareVersionInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIFirmwareVersionInfo.h"

@implementation CZGeminiAPIFirmwareVersionInfo

- (void)dealloc {
    [_firmwareVersion release];
    [_firmwareBuild release];
    [_firmwareFullVersion release];
    [super dealloc];
}

@end
