//
//  CZGeminiAPISystemInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPISystemInfo.h"

@implementation CZGeminiAPISystemInfo

- (void)dealloc {
    [_camera release];
    [_microscope release];
    [_timestamp release];
    [super dealloc];
}

@end
