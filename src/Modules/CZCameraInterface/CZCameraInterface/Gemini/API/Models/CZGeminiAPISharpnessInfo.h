//
//  CZGeminiAPISharpnessInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPISharpnessInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *sharpness; // NSUInteger

@end
