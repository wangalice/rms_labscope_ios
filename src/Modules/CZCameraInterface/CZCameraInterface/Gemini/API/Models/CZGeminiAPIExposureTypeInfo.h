//
//  CZGeminiAPIExposureTypeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIEnumerations.h"

@interface CZGeminiAPIExposureTypeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *exposureType; // CZGeminiAPIExposureType
@property (nonatomic, copy)   NSString *action;
@property (nonatomic, retain) NSNumber *exposureTime; // float
@property (nonatomic, retain) NSNumber *gain;         // NSUInteger

@end
