//
//  CZGeminiAPIColorModeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

typedef NS_ENUM(NSInteger, CZGeminiAPIColorMode) {
    CZGeminiAPIColorModeColor = 0,
    CZGeminiAPIColorModeGray = 1
};

@interface CZGeminiAPIColorModeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *colorMode; // CZGeminiAPIColorMode

@end
