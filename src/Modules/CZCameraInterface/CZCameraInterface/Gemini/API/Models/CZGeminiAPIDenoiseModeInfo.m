//
//  CZGeminiAPIDenoiseModeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIDenoiseModeInfo.h"

@implementation CZGeminiAPIDenoiseModeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"denoiseMode" : @"denoise"};
}

- (void)dealloc {
    [_denoiseMode release];
    [super dealloc];
}

@end
