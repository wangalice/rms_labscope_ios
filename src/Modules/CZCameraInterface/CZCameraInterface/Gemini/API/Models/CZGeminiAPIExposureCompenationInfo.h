//
//  CZGeminiAPIExposureCompenationInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/20/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

extern const NSInteger CZGeminiAPIMinExposureCompenation;
extern const NSInteger CZGeminiAPIMaxExposureCompenation;

@interface CZGeminiAPIExposureCompenationInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *compenation; // NSInteger

@end
