//
//  CZGeminiAPIExposureTypeInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIExposureTypeInfo.h"

@implementation CZGeminiAPIExposureTypeInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureType" : @"exptype",
             @"exposureTime" : @"exptime"};
}

- (void)dealloc {
    [_exposureType release];
    [_action release];
    [_exposureTime release];
    [_gain release];
    [super dealloc];
}

@end
