//
//  CZGeminiAPIIlluminationInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIIlluminationInfo.h"

@implementation CZGeminiAPIIlluminationInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"reflectedLight" : @"rl",
             @"transmittedLight" : @"tl"};
}

- (void)dealloc {
    [_reflectedLight release];
    [_transmittedLight release];
    [super dealloc];
}

@end
