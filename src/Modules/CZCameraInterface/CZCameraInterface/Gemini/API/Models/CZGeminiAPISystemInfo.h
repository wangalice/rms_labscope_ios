//
//  CZGeminiAPISystemInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPICameraInfo.h"
#import "CZGeminiAPIMicroscopeInfo.h"

@interface CZGeminiAPISystemInfo : NSObject <YYModel>

@property (nonatomic, retain) CZGeminiAPICameraInfo *camera;
@property (nonatomic, retain) CZGeminiAPIMicroscopeInfo *microscope;
@property (nonatomic, retain) NSDate *timestamp;

@end
