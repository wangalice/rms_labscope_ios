//
//  CZGeminiAPIObjectiveInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIObjectiveInfo.h"

@implementation CZGeminiAPIObjectiveInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"objectiveClass" : @"class"};
}

- (void)dealloc {
    [_matID release];
    [_objectiveClass release];
    [_magnification release];
    [super dealloc];
}

@end
