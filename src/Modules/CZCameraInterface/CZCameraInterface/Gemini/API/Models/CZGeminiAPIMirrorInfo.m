//
//  CZGeminiAPIMirrorInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIMirrorInfo.h"

@implementation CZGeminiAPIMirrorInfo

- (void)dealloc {
    [_horizontal release];
    [_vertical release];
    [super dealloc];
}

@end
