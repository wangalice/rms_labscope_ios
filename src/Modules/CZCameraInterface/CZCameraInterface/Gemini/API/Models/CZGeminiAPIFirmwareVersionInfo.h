//
//  CZGeminiAPIFirmwareVersionInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@interface CZGeminiAPIFirmwareVersionInfo : NSObject <YYModel>

@property (nonatomic, copy) NSString *firmwareVersion;
@property (nonatomic, copy) NSString *firmwareBuild;
@property (nonatomic, copy) NSString *firmwareFullVersion;

@end
