//
//  CZGeminiAPIMicroscopeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPIReflectorsInfo.h"
#import "CZGeminiAPILEDsInfo.h"
#import "CZGeminiAPIObjectivesInfo.h"
#import "CZGeminiAPIIlluminationInfo.h"

@interface CZGeminiAPIMicroscopeInfo : NSObject <YYModel>

@property (nonatomic, assign) float cameraAdapter;
@property (nonatomic, assign) BOOL hasFluorescence;
@property (nonatomic, assign) BOOL isMultiFluorescence;
@property (nonatomic, assign) BOOL hasReflector;
@property (nonatomic, assign) BOOL hasObjecive;
@property (nonatomic, assign) BOOL hasFluorescenceLEDs;
@property (nonatomic, assign) BOOL hasZAxis;
@property (nonatomic, assign) BOOL hasKey;
@property (nonatomic, assign) BOOL hasReflectedLightSwitch;
@property (nonatomic, assign) BOOL hasTransmittedLightSwitch;
@property (nonatomic, assign) BOOL hasXAxis;
@property (nonatomic, assign) BOOL hasYAxis;
@property (nonatomic, assign) BOOL hasHalogenLEDIntensity;
@property (nonatomic, assign) BOOL hasColibri;
@property (nonatomic, assign) BOOL ledSetable;
@property (nonatomic, assign) BOOL hasMCB;
@property (nonatomic, copy)   NSString *microscopeName;
@property (nonatomic, retain) CZGeminiAPIReflectorsInfo *reflector;
@property (nonatomic, retain) CZGeminiAPILEDsInfo *led;
@property (nonatomic, retain) CZGeminiAPIObjectivesInfo *objective;
@property (nonatomic, retain) CZGeminiAPIIlluminationInfo *illumination;

@end
