//
//  CZGeminiAPIDenoiseModeInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 4/18/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

typedef NS_ENUM(NSInteger, CZGeminiAPIDenoiseMode) {
    CZGeminiAPIDenoiseModeOff = 0,
    CZGeminiAPIDenoiseModeOn = 1,
    CZGeminiAPIDenoiseModeSetOn = 3
};


@interface CZGeminiAPIDenoiseModeInfo : NSObject <YYModel>

@property (nonatomic, retain) NSNumber *denoiseMode; // CZGeminiAPIDenoiseMode

@end


