//
//  CZGeminiAPILEDInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPILEDInfo.h"

@implementation CZGeminiAPILEDInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"oneKeyFluorescenceEnabled" : @"chosen"};
}

- (void)dealloc {
    [_ledIndex release];
    [_brightnessMax release];
    [_matID release];
    [_brightness release];
    [_brightnessMin release];
    [_waveLength release];
    [_oneKeyFluorescenceEnabled release];
    [_status release];
    [super dealloc];
}

- (BOOL)isLightOn {
    return [self.status.lowercaseString isEqualToString:@"on"];
}

- (BOOL)doesLEDExist {
    return self.waveLength.integerValue > 0;
}

@end
