//
//  CZGeminiAPICameraInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPICameraInfo.h"

@implementation CZGeminiAPICameraInfo

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"exposureTime" : @"expTime",
             @"colorTemperature" : @"colorTemp",
             @"whiteBalance" : @"awb",
             @"exposureType" : @"exptype",
             @"showFullScreen" : @"showfullScreen",
             @"dynamicDefectPixel" : @"DynamicDefectPixel",
             @"language" : @"lang",
             @"scaleBar" : @"scalebar",
             @"modelName" : @"model"};
}

- (void)dealloc {
    [_language release];
    [_wifiMode release];
    [_name release];
    [_reflectorVersion release];
    [_objectiveVersion release];
    [_scaleBar release];
    [_mirror release];
    [_filenameTemplate release];
    [_firmwareVersion release];
    [_modelName release];
    [_firmwareBuild release];
    [super dealloc];
}

- (CZGeminiAPICameraModelType)modelType {
    if ([self.modelName isEqualToString:@"Axiocam202"]) {
        return CZGeminiAPICameraModelTypeAxiocam202;
    } else if ([self.modelName isEqualToString:@"Axiocam208"]) {
        return CZGeminiAPICameraModelTypeAxiocam208;
    } else if ([self.modelName.lowercaseString isEqualToString:@"Integrated smart 8 MP color camera".lowercaseString]) {
        return CZGeminiAPICameraModelTypeAxiocam208;
    } else {
        return CZGeminiAPICameraModelTypeUnknown;
    }
}

@end
