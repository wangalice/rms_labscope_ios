//
//  CZGeminiAPILEDsInfo.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPILEDsInfo.h"

@implementation CZGeminiAPILEDsInfo

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"leds" : [CZGeminiAPILEDInfo class]};
}

- (void)dealloc {
    [_leds release];
    [super dealloc];
}

@end
