//
//  CZGeminiAPILEDInfo.h
//  CameraInterface
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>
#import "CZGeminiAPILEDInfo.h"

@interface CZGeminiAPILEDsInfo : NSObject <YYModel>

@property (nonatomic, copy)   NSArray<CZGeminiAPILEDInfo *> *leds;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, assign) NSInteger positionCount;

@end
