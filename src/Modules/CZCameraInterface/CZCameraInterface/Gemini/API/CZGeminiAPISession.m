//
//  CZGeminiAPISession.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPISession.h"

@interface CZGeminiAPISession ()

@property (nonatomic, readwrite, copy) NSArray<CZGeminiAPI *> *apis;

@end

@implementation CZGeminiAPISession

+ (instancetype)serialAPISession {
    return [[[self alloc] initWithMode:CZGeminiAPISessionModeSerial] autorelease];
}

+ (instancetype)concurrentAPISession {
    return [[[self alloc] initWithMode:CZGeminiAPISessionModeConcurrent] autorelease];
}

- (instancetype)initWithMode:(CZGeminiAPISessionMode)mode {
    self = [super init];
    if (self) {
        _mode = mode;
        _apis = [[NSArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_apis release];
    [super dealloc];
}

- (void)addAPI:(CZGeminiAPI *)api {
    self.apis = [self.apis arrayByAddingObject:api];
}

@end
