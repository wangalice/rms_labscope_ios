//
//  CZGeminiAPIClient.m
//  CameraInterface
//
//  Created by Li, Junlin on 2/22/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiAPIClient.h"
#import <AFNetworking/AFNetworking.h>
#import <YYModel/YYModel.h>
#import <CZToolbox/CZToolbox.h>

const NSTimeInterval CZGeminiAPITimeoutInterval = 20.0;

@interface CZGeminiAPIClient ()

@property (nonatomic, retain) AFHTTPClient *httpClient;
@property (nonatomic, retain) dispatch_queue_t apiQueue;

@end

@implementation CZGeminiAPIClient

- (instancetype)initWithIPAddress:(NSString *)ipAddress {
    self = [super init];
    if (self) {
        _ipAddress = [ipAddress copy];
        NSString *baseURLString = [NSString stringWithFormat:@"http://%@/api/", ipAddress];
        NSURL *baseURL = [NSURL URLWithString:baseURLString];
        _httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
        _apiQueue = dispatch_queue_create("com.zeisscn.camera.gemini.api", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)dealloc {
    [_ipAddress release];
    [_httpClient release];
    dispatch_release(_apiQueue);
    [super dealloc];
}

#pragma mark - Call API

- (id)callAPI:(CZGeminiAPI *)api {
    CZURLSessionTaskResult *result = [self resultForAPI:api];
    return [api mapResult:result];
}

- (void)callAPIAsync:(CZGeminiAPI *)api {
    [self callAPIAsync:api completionHandler:nil];
}

- (void)callAPIAsync:(CZGeminiAPI *)api completionHandler:(void (^)(id))completionHandler {
    dispatch_async(self.apiQueue, ^{
        CZURLSessionTaskResult *result = [self resultForAPI:api];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler([api mapResult:result]);
            }
        });
    });
}

#pragma mark - API Session

- (void)commitAPISession:(CZGeminiAPISession *)session withCompletionHandler:(void (^)(void))completionHandler {
    [self commitAPISession:session withCompletionHandler:completionHandler afterDelay:0.0];
}

- (void)commitAPISession:(CZGeminiAPISession *)session withCompletionHandler:(void (^)(void))completionHandler afterDelay:(NSTimeInterval)delay {
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_queue_t queue = nil;
    switch (session.mode) {
        case CZGeminiAPISessionModeSerial:
            queue = dispatch_queue_create("com.zeisscn.camera.gemini.api.session.serial", DISPATCH_QUEUE_SERIAL);
            break;
        case CZGeminiAPISessionModeConcurrent:
            queue = dispatch_queue_create("com.zeisscn.camera.gemini.api.session.concurrent", DISPATCH_QUEUE_CONCURRENT);
            break;
    }
    
    for (CZGeminiAPI *api in session.apis) {
        dispatch_group_enter(group);
        dispatch_async(queue, ^{
            [self callAPI:api];
            dispatch_group_leave(group);
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        dispatch_release(queue);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler();
            }
        });
    });
    
    dispatch_release(group);
}

#pragma mark - Private

- (CZURLSessionTaskResult *)resultForAPI:(CZGeminiAPI *)api {
    if (api == nil || api.method.length == 0 || api.path.length == 0) {
        return nil;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    if ([api.method isEqualToString:@"GET"]) {
        self.httpClient.parameterEncoding = AFFormURLParameterEncoding;
    } else {
        self.httpClient.parameterEncoding = AFJSONParameterEncoding;
    }
    
    NSMutableURLRequest *request = [self.httpClient requestWithMethod:api.method path:api.path parameters:api.parameters];
    
    if ([api.method isEqualToString:@"GET"]) {
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        request.timeoutInterval = 10.0; // Shorter timeout for GET requests
    } else {
        request.cachePolicy = NSURLRequestUseProtocolCachePolicy;
        request.timeoutInterval = CZGeminiAPITimeoutInterval;
    }
    
    CZLogv(@"Gemini API client will send request: %@, method: %@, path: %@, parameters: %@", request, api.method, api.path, api.parameters);
    
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    if (result.error) {
        CZLogv(@"Failed to send the request to get parameters from Gemini camera: %@", [result.error localizedDescription]);
    } else {
#ifdef DEBUG
        CZLogv(@"Got result from Gemini camera: %@", api.path);
#endif
    }
    
    return result;
}

@end
