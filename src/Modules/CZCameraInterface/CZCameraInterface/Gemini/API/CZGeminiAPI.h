//
//  CZGeminiAPI.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>
#import "CZGeminiAPISnapInfo.h"
#import "CZGeminiAPISystemInfo.h"
#import "CZGeminiAPIFirmwareVersionInfo.h"
#import "CZGeminiAPISharpnessInfo.h"
#import "CZGeminiAPIDenoiseModeInfo.h"
#import "CZGeminiAPIExposureInfo.h"
#import "CZGeminiAPIExposureTypeInfo.h"
#import "CZGeminiAPIExposureCompenationInfo.h"
#import "CZGeminiAPIWhiteBalanceInfo.h"
#import "CZGeminiAPIRealExposureTimeInfo.h"
#import "CZGeminiAPIIlluminationInfo.h"
#import "CZGeminiAPIBitRateInfo.h"
#import "CZGeminiAPIColorModeInfo.h"
#import "CZGeminiAPILockInfo.h"
#import "CZGeminiAPIClientNumberInfo.h"
#import "CZGeminiAPIHDRModeInfo.h"
#import "CZGeminiAPIShadingCorrectionInfo.h"
#import "CZGeminiAPIDynamicDefectPixelInfo.h"
#import "CZGeminiAPIGammaInfo.h"

@interface CZGeminiAPI<ResultType> : NSObject

@property (nonatomic, readonly, copy) NSString *method;
@property (nonatomic, readonly, copy) NSString *path;
@property (nonatomic, readonly, copy) NSDictionary *parameters;
@property (nonatomic, readonly, copy) ResultType (^mapper)(CZURLSessionTaskResult *result);

+ (CZGeminiAPI *)apiForSnap:(CZGeminiAPISnapInfo *)snapInfo;
+ (CZGeminiAPI *)apiForSetResolution:(NSUInteger)resolution;

+ (CZGeminiAPI<CZGeminiAPISystemInfo *> *)apiForGetSystemInfo;
+ (CZGeminiAPI *)apiForSetCameraName:(NSString *)cameraName;

+ (CZGeminiAPI<CZGeminiAPIReflectorsInfo *> *)apiForGetReflectorsInfo;
+ (CZGeminiAPI<CZGeminiAPIReflectorInfo *> *)apiForGetReflectorInfoAtPosition:(NSInteger)position;
+ (CZGeminiAPI *)apiForSetReflectorInfo:(CZGeminiAPIReflectorInfo *)reflectorInfo atPosition:(NSInteger)position;

+ (CZGeminiAPI<CZGeminiAPIObjectivesInfo *> *)apiForGetObjectivesInfo;
+ (CZGeminiAPI<CZGeminiAPIObjectiveInfo *> *)apiForGetObjectiveInfoAtPosition:(NSInteger)position;
+ (CZGeminiAPI *)apiForSetObjectiveInfo:(CZGeminiAPIObjectiveInfo *)objectiveInfo atPosition:(NSInteger)position;

+ (CZGeminiAPI *)apiForSetCameraAdapter:(float)cameraAdapter;

+ (CZGeminiAPI<CZGeminiAPIMirrorInfo *> *)apiForGetMirrorInfo;
+ (CZGeminiAPI *)apiForSetMirrorInfo:(CZGeminiAPIMirrorInfo *)mirrorInfo;

+ (CZGeminiAPI<CZGeminiAPIFirmwareVersionInfo *> *)apiForGetFirmwareVersionInfo;

+ (CZGeminiAPI<NSNumber *> *)apiForGetSharpness;
+ (CZGeminiAPI *)apiForSetSharpness:(NSUInteger)sharpness;

+ (CZGeminiAPI<NSNumber *> *)apiForGetDenoiseMode;
+ (CZGeminiAPI *)apiForSetDenoiseMode:(CZGeminiAPIDenoiseMode)denoiseMode;

+ (CZGeminiAPI<CZGeminiAPIExposureInfo *> *)apiForGetExposureInfo;
+ (CZGeminiAPI *)apiForSetExposureInfo:(CZGeminiAPIExposureInfo *)exposureInfo;

+ (CZGeminiAPI *)apiForGetExposureStabilizationStatus;

+ (CZGeminiAPI<CZGeminiAPIGainInfo *> *)apiForGetGainInfo;
+ (CZGeminiAPI *)apiForSetGain:(NSUInteger)gain;

+ (CZGeminiAPI<CZGeminiAPIExposureTimeInfo *> *)apiForGetExposureTimeInfo;
+ (CZGeminiAPI *)apiForSetExposureTime:(float)exposureTime;

+ (CZGeminiAPI<NSNumber *> *)apiForGetExposureType;
+ (CZGeminiAPI<CZGeminiAPIExposureTypeInfo *> *)apiForSetExposureType:(CZGeminiAPIExposureType)exposureType once:(BOOL)once;

+ (CZGeminiAPI<NSNumber *> *)apiForGetExposureCompenation;
+ (CZGeminiAPI *)apiForSetExposureCompenation:(NSInteger)exposureCompenation;

+ (CZGeminiAPI<NSNumber *> *)apiForGetWhiteBalanceMode;
+ (CZGeminiAPI<CZGeminiAPIWhiteBalanceInfo *> *)apiForSetWhiteBalanceMode:(CZGeminiAPIWhiteBalanceMode)whiteBalanceMode once:(BOOL)once;

+ (CZGeminiAPI<NSNumber *> *)apiForGetColorTemperature;
+ (CZGeminiAPI *)apiForSetColorTemperature:(NSUInteger)colorTemperature;

+ (CZGeminiAPI<CZGeminiAPIRealExposureTimeInfo *> *)apiForGetRealExposureTimeInfo;

//+ (void)microscopeModelName;
//+ (void)setMicroscopeModelName;

//+ (void)illuminationInfo;
+ (CZGeminiAPI<CZGeminiAPILightInfo *> *)apiForGetReflectedLightInfo;
+ (CZGeminiAPI<CZGeminiAPILightInfo *> *)apiForGetTransmittedLightInfo;
+ (CZGeminiAPI *)apiForSetReflectedLightInfo:(CZGeminiAPILightInfo *)lightInfo;
+ (CZGeminiAPI *)apiForSetTransmittedLightInfo:(CZGeminiAPILightInfo *)lightInfo;

+ (CZGeminiAPI<CZGeminiAPILEDsInfo *> *)apiForGetLEDsInfo;
+ (CZGeminiAPI<NSNumber *> *)apiForGetCurrentLEDPosition;
+ (CZGeminiAPI<CZGeminiAPILEDInfo *> *)apiForGetLEDInfoAtPosition:(NSInteger)position;
+ (CZGeminiAPI *)apiForSetLEDInfo:(CZGeminiAPILEDInfo *)ledInfo atPosition:(NSInteger)position;

+ (CZGeminiAPI<NSNumber *> *)apiForGetBitRate;
+ (CZGeminiAPI *)apiForSetBitRate:(NSUInteger)bitRate;

+ (CZGeminiAPI<NSNumber *> *)apiForGetColorMode;
+ (CZGeminiAPI *)apiForSetColorMode:(CZGeminiAPIColorMode)colorMode;

+ (CZGeminiAPI *)apiForSetColibriLEDStatus:(BOOL)status enabled:(BOOL)enabled;

+ (CZGeminiAPI *)apiForSendHeartBeat;

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForLockConfiguration;
+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForUnlockConfiguration;

+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForLockSnap;
+ (CZGeminiAPI<CZGeminiAPILockInfo *> *)apiForUnlockSnap;

//+ (void)objectiveList;
//+ (void)setObjectiveList;

//+ (void)reflectorList;
//+ (void)setReflectorList;

+ (CZGeminiAPI<NSNumber *> *)apiForGetClientNumber;

+ (CZGeminiAPI<NSNumber *> *)apiForGetHDRMode;
+ (CZGeminiAPI *)apiForSetHDRMode:(CZGeminiAPIHDRMode)hdrMode;

+ (CZGeminiAPI *)apiForSetShadingCorrection:(CZGeminiAPIShadingCorrectionInfo *)shadingCorrectionInfo;

//+ (void)beginOneKeySnap;
//+ (void)deleteOneKeySnappedImage;
//+ (void)oneKeySnappedImage;

+ (CZGeminiAPI<NSNumber *> *)apiForGetDynamicDefectPixel;
+ (CZGeminiAPI *)apiForSetDynamicDefectPixel:(CZGeminiAPIDynamicDefectPixel)dynamicDefectPixel;

+ (CZGeminiAPI<CZGeminiAPIGammaInfo *> *)apiForGetGammaInfo;
+ (CZGeminiAPI *)apiForSetGamma:(float)gamma;

+ (instancetype)apiWithMethod:(NSString *)method
                         path:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      mapper:(ResultType (^)(CZURLSessionTaskResult * result))mapper;

- (ResultType)mapResult:(CZURLSessionTaskResult *)result;

@end
