//
//  CZGeminiAPISession.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CZGeminiAPISessionMode) {
    CZGeminiAPISessionModeSerial,
    CZGeminiAPISessionModeConcurrent
};

@class CZGeminiAPI;

@interface CZGeminiAPISession : NSObject

@property (nonatomic, readonly, assign) CZGeminiAPISessionMode mode;
@property (nonatomic, readonly, copy) NSArray<CZGeminiAPI *> *apis;

+ (instancetype)serialAPISession;
+ (instancetype)concurrentAPISession;

- (void)addAPI:(CZGeminiAPI *)api;

@end
