//
//  CZGeminiCameraEvent.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

extern NSString * const CZGeminiCameraEventTypeAutoExposureStable;
extern NSString * const CZGeminiCameraEventTypeHTTPSnap;
extern NSString * const CZGeminiCameraEventTypeHardwareSnap;
extern NSString * const CZGeminiCameraEventTypeColorModeChanged;
extern NSString * const CZGeminiCameraEventTypeDynamicDefectPixelStatusChanged;
extern NSString * const CZGeminiCameraEventTypeManualColorTemperatureChanged;
extern NSString * const CZGeminiCameraEventTypeWhiteBalanceTypeChanged;
extern NSString * const CZGeminiCameraEventTypeObjectiveVersionChanged;
extern NSString * const CZGeminiCameraEventTypeReflectorVersionChanged;
extern NSString * const CZGeminiCameraEventTypeSnapFinished;
extern NSString * const CZGeminiCameraEventTypeFirmwareUpdateError;
extern NSString * const CZGeminiCameraEventTypeFluorescenceSnapped;
extern NSString * const CZGeminiCameraEventTypeCameraConnectedToOtherDevice;
extern NSString * const CZGeminiCameraEventTypeWIFIStatusChanged;
extern NSString * const CZGeminiCameraEventTypeThumbnailReady;
extern NSString * const CZGeminiCameraEventTypeOnekeyFluorescenceSnapInfo;
extern NSString * const CZGeminiCameraEventTypeCameraAdapterChanged;
extern NSString * const CZGeminiCameraEventTypeLightSourceChanged;
extern NSString * const CZGeminiCameraEventTypeCameraNameChanged;
extern NSString * const CZGeminiCameraEventTypeShadingCorrectionInfo;
extern NSString * const CZGeminiCameraEventTypeRecordingInfo;

@interface CZGeminiCameraEvent : NSObject <YYModel>

@property (nonatomic, readonly, copy) NSString *eventType;

@end
