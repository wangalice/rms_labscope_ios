//
//  CZGeminiWebSocketClient.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiWebSocketClient.h"
#import <Jetfire/Jetfire.h>
#import <CZToolbox/CZToolbox.h>

NSString * const CZGeminiCameraWebSocketPort = @"8080";
NSString * const CZGeminiCameraWebSocketProtocolMicroscopeStatus = @"axio-microscope-status";
NSString * const CZGeminiCameraWebSocketProtocolMicroscopeEvent = @"axio-microscope-event";

@interface CZGeminiWebSocketClient () <JFRWebSocketDelegate> {
    NSUInteger _socketRetainCount;
    NSObject *_socketRetainCountLock;
}

@property (nonatomic, readwrite, retain) CZGeminiCameraStatus *previousStatus;
@property (nonatomic, readwrite, retain) CZGeminiCameraStatus *latestStatus;

@property (nonatomic, readwrite, retain) __kindof CZGeminiCameraEvent *previousEvent;
@property (nonatomic, readwrite, retain) __kindof CZGeminiCameraEvent *latestEvent;

@property (atomic, retain) JFRWebSocket *statusSocket;
@property (atomic, retain) JFRWebSocket *eventSocket;

@end

@implementation CZGeminiWebSocketClient

- (instancetype)initWithIPAddress:(NSString *)ipAddress delegate:(id<CZGeminiWebSocketClientDelegate>)delegate {
    self = [super init];
    if (self) {
        _ipAddress = [ipAddress copy];
        _delegate = delegate;
        
        _socketRetainCount = 0;
        _socketRetainCountLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_ipAddress release];
    _delegate = nil;
    [_socketRetainCountLock release];
    [_previousStatus release];
    [_latestStatus release];
    [_previousEvent release];
    [_latestEvent release];
    
    if (_statusSocket) {
        [_statusSocket disconnect];
        _statusSocket.delegate = nil;
        [_statusSocket release];
    }
    
    if (_eventSocket) {
        [_eventSocket disconnect];
        _eventSocket.delegate = nil;
        [_eventSocket release];
    }
    
    [super dealloc];
}

- (BOOL)connect {
    @synchronized (_socketRetainCountLock) {
        if (_socketRetainCount == 0) {
            NSString *urlString = [[NSString alloc] initWithFormat:@"ws://%@:%@", self.ipAddress, CZGeminiCameraWebSocketPort];
            NSURL *url = [[NSURL alloc] initWithString:urlString];
            
            self.statusSocket = [[[JFRWebSocket alloc] initWithURL:url protocols:@[CZGeminiCameraWebSocketProtocolMicroscopeStatus]] autorelease];
            self.statusSocket.delegate = self;
            [self.statusSocket connect];
            
            self.eventSocket = [[[JFRWebSocket alloc] initWithURL:url protocols:@[CZGeminiCameraWebSocketProtocolMicroscopeEvent]] autorelease];
            self.eventSocket.delegate = self;
            [self.eventSocket connect];
            
            [url release];
            [urlString release];
            
            _socketRetainCount = 1;
            
            return YES;
        }
        
        ++_socketRetainCount;
        
        return NO;
    }
}

- (BOOL)disconnect {
    @synchronized (_socketRetainCountLock) {
        if (_socketRetainCount > 0) {
            --_socketRetainCount;
        }
        
        if (_socketRetainCount == 0) {
            [self.statusSocket disconnect];
            self.statusSocket.delegate = nil;
            self.statusSocket = nil;
            
            [self.eventSocket disconnect];
            self.eventSocket.delegate = nil;
            self.eventSocket = nil;
            
            return YES;
        }
        
        return NO;
    }
}

#pragma mark - JFRWebSocketDelegate methods

- (void)websocketDidConnect:(JFRWebSocket *)socket {
    if (socket == self.statusSocket) {
        CZLogv(@"Gemini camera: status socket did connect");
    } else if (socket == self.eventSocket) {
        CZLogv(@"Gemini camera: event socket did connect");
    }
}

- (void)websocket:(JFRWebSocket *)socket didReceiveMessage:(NSString *)string {
    if (socket == self.statusSocket) {
        CZLogv(@"Gemini camera: status socket did receive message: %@", string);
        
        self.previousStatus = self.latestStatus;
        
        CZGeminiCameraStatus *status = [CZGeminiCameraStatus yy_modelWithJSON:string];
        self.latestStatus = status;
        
        CZGeminiCameraStatusChangeMask changes = 0;
        if (self.previousStatus == nil || self.latestStatus.objectivePosition != self.previousStatus.objectivePosition) {
            changes |= CZGeminiCameraStatusChangeMaskObjectivePosition;
        }
        if (self.previousStatus == nil || self.latestStatus.reflectorPosition != self.previousStatus.reflectorPosition) {
            changes |= CZGeminiCameraStatusChangeMaskReflectorPosition;
        }
        if (self.previousStatus == nil || self.latestStatus.rltlPosition != self.previousStatus.rltlPosition) {
            changes |= CZGeminiCameraStatusChangeMaskRLTLPosition;
        }
        if (self.previousStatus == nil || ![self.latestStatus.ledsStatus isEqualToArray:self.previousStatus.ledsStatus]) {
            changes |= CZGeminiCameraStatusChangeMaskLEDsStatus;
        }
        if (self.previousStatus == nil || ![self.latestStatus.ledsBrightness isEqualToArray:self.previousStatus.ledsBrightness]) {
            changes |= CZGeminiCameraStatusChangeMaskLEDsBrightness;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(webSocketClient:statusDidChange:changes:)]) {
            [self.delegate webSocketClient:self statusDidChange:status changes:changes];
        }
    } else if (socket == self.eventSocket) {
        CZLogv(@"Gemini camera: event socket did receive message: %@", string);
        
        self.previousEvent = self.latestEvent;
        
        CZGeminiCameraEvent *event = [CZGeminiCameraEvent yy_modelWithJSON:string];
        if ([event.eventType isEqualToString:CZGeminiCameraEventTypeAutoExposureStable]) {
            event = [CZGeminiCameraAutoExposureStableEvent yy_modelWithJSON:string];
        } else if ([event.eventType isEqualToString:CZGeminiCameraEventTypeHTTPSnap]) {
            event = [CZGeminiCameraHTTPSnapEvent yy_modelWithJSON:string];
        } else if ([event.eventType isEqualToString:CZGeminiCameraEventTypeHardwareSnap]) {
            event = [CZGeminiCameraHardwareEvent yy_modelWithJSON:string];
        } else if ([event.eventType isEqualToString:CZGeminiCameraEventTypeShadingCorrectionInfo]) {
            event = [CZGeminiCameraShadingCorrectionInfoEvent yy_modelWithJSON:string];
        }
        self.latestEvent = event;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(webSocketClient:didReceiveEvent:)]) {
            [self.delegate webSocketClient:self didReceiveEvent:event];
        }
    }
}

- (void)websocket:(JFRWebSocket *)socket didReceivePongData:(NSData *)data {
    if (socket == self.statusSocket) {
        CZLogv(@"Gemini camera: status socket did receive pong: %@", data);
    } else if (socket == self.eventSocket) {
        CZLogv(@"Gemini camera: event socket did receive pong: %@", data);
    }
}

- (void)websocketDidDisconnect:(JFRWebSocket *)socket error:(NSError *)error {
    if (socket == self.statusSocket) {
        CZLogv(@"Gemini camera: status socket did disconnect with error: %@", error);
        
        self.statusSocket.delegate = nil;
        self.statusSocket = nil;
    } else if (socket == self.eventSocket) {
        CZLogv(@"Gemini camera: event socket did disconnect with error: %@", error);
        
        self.eventSocket.delegate = nil;
        self.eventSocket = nil;
    }
}

@end
