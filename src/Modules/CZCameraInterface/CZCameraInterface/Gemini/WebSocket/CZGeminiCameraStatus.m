//
//  CZGeminiCameraStatus.m
//  CameraInterface
//
//  Created by Li, Junlin on 3/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraStatus.h"

@interface CZGeminiCameraStatus ()

@property (nonatomic, readwrite, assign) NSInteger objectivePosition;
@property (nonatomic, readwrite, assign) NSInteger reflectorPosition;
@property (nonatomic, readwrite, assign) NSInteger rltlPosition;
@property (nonatomic, readwrite, assign) NSInteger mode;
@property (nonatomic, readwrite, assign) NSInteger fullScreenOrThumb;
@property (nonatomic, readwrite, copy) NSArray<CZGeminiCameraLEDStatus *> *ledsStatus;
@property (nonatomic, readwrite, copy) NSArray<CZGeminiCameraLEDBrightness *> *ledsBrightness;

@end

@implementation CZGeminiCameraStatus

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"rltlPosition"   : @"RLTLPosition",
             @"ledsStatus"     : @"onOff",
             @"ledsBrightness" : @"brightness"};
}

+ (NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"ledsStatus"     : [CZGeminiCameraLEDStatus class],
             @"ledsBrightness" : [CZGeminiCameraLEDBrightness class]};
}

- (void)dealloc {
    [_ledsStatus release];
    [_ledsBrightness release];
    [super dealloc];
}

@end

@interface CZGeminiCameraLEDStatus ()

@property (nonatomic, readwrite, assign) BOOL on;

@end

@implementation CZGeminiCameraLEDStatus

- (BOOL)isEqual:(CZGeminiCameraLEDStatus *)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZGeminiCameraLEDStatus class]]) {
        return NO;
    } else {
        return object.on == self.on;
    }
}

@end

@interface CZGeminiCameraLEDBrightness ()

@property (nonatomic, readwrite, assign) NSInteger brightness;

@end

@implementation CZGeminiCameraLEDBrightness

- (BOOL)isEqual:(CZGeminiCameraLEDBrightness *)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZGeminiCameraLEDBrightness class]]) {
        return NO;
    } else {
        return object.brightness == self.brightness;
    }
}

@end
