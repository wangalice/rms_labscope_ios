//
//  CZGeminiCameraHardwareEvent.h
//  CameraInterface
//
//  Created by Sun, Shaoge on 2019/5/14.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraEvent.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, CZGeminiCameraEventStandType) {
    CZGeminiCameraEventStandSnap = 1,
    CZGeminiCameraEventStandRecording = 2
};

@interface CZGeminiCameraHardwareEvent : CZGeminiCameraEvent

@property (nonatomic, retain, readonly) NSNumber *value;

@end

NS_ASSUME_NONNULL_END
