//
//  CZGeminiCameraHTTPSnapEvent.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraHTTPSnapEvent.h"

NSString * const CZGeminiCameraHTTPSnapEventImageTypeYUV400 = @"yuv400";
NSString * const CZGeminiCameraHTTPSnapEventImageTypeYUV420 = @"yuv420sp";
NSString * const CZGeminiCameraHTTPSnapEventImageTypeGrayBayer = @"GrayBayer";

@interface CZGeminiCameraHTTPSnapEvent ()

@property (nonatomic, readwrite, copy) NSString *imageType;
@property (nonatomic, readwrite, copy) NSString *imageURL;
@property (nonatomic, readwrite, retain) NSNumber *exposureTime;
@property (nonatomic, readwrite, retain) NSNumber *gain;
@property (nonatomic, readwrite, retain) NSNumber *wavelength;
@property (nonatomic, readwrite, retain) NSNumber *imageWidth;
@property (nonatomic, readwrite, retain) NSNumber *imageHeight;
@property (nonatomic, readwrite, retain) NSNumber *position;

@end

@implementation CZGeminiCameraHTTPSnapEvent

+ (NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"imageURL"     : @"imageUrl",
             @"exposureTime" : @"expTime",
             @"gain"         : @"expGain",
             @"wavelength"   : @"waveLength",
             @"imageWidth"   : @"width",
             @"imageHeight"  : @"height"};
}

- (void)dealloc {
    [_imageType release];
    [_imageURL release];
    [_exposureTime release];
    [_gain release];
    [_wavelength release];
    [_imageWidth release];
    [_imageHeight release];
    [_position release];
    [super dealloc];
}

@end
