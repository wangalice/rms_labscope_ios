//
//  CZGeminiCameraAutoExposureStableEvent.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraEvent.h"

@interface CZGeminiCameraAutoExposureStableEvent : CZGeminiCameraEvent

@property (nonatomic, readonly, retain) NSNumber *value;
@property (nonatomic, readonly, retain) NSNumber *exposureTime;
@property (nonatomic, readonly, retain) NSNumber *gain;

@end
