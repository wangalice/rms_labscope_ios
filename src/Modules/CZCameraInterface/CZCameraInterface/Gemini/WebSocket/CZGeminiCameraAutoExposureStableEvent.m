//
//  CZGeminiCameraAutoExposureStableEvent.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraAutoExposureStableEvent.h"

@interface CZGeminiCameraAutoExposureStableEvent ()

@property (nonatomic, readwrite, retain) NSNumber *value;
@property (nonatomic, readwrite, retain) NSNumber *exposureTime;
@property (nonatomic, readwrite, retain) NSNumber *gain;

@end

@implementation CZGeminiCameraAutoExposureStableEvent

- (void)dealloc {
    [_value release];
    [_exposureTime release];
    [_gain release];
    [super dealloc];
}

@end
