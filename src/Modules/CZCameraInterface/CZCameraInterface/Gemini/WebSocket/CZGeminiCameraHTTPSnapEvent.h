//
//  CZGeminiCameraHTTPSnapEvent.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraEvent.h"

extern NSString * const CZGeminiCameraHTTPSnapEventImageTypeYUV400;
extern NSString * const CZGeminiCameraHTTPSnapEventImageTypeYUV420;
extern NSString * const CZGeminiCameraHTTPSnapEventImageTypeGrayBayer;

@interface CZGeminiCameraHTTPSnapEvent : CZGeminiCameraEvent

@property (nonatomic, readonly, copy) NSString *imageType;
@property (nonatomic, readonly, copy) NSString *imageURL;
@property (nonatomic, readonly, retain) NSNumber *exposureTime;
@property (nonatomic, readonly, retain) NSNumber *gain;
@property (nonatomic, readonly, retain) NSNumber *wavelength;
@property (nonatomic, readonly, retain) NSNumber *imageWidth;
@property (nonatomic, readonly, retain) NSNumber *imageHeight;
@property (nonatomic, readonly, retain) NSNumber *position;

@end
