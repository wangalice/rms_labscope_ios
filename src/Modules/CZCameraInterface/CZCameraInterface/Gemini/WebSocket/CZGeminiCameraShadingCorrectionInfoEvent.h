//
//  CZGeminiCameraShadingCorrectionInfoEvent.h
//  CameraInterface
//
//  Created by Li, Junlin on 7/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraEvent.h"

typedef NS_ENUM(NSInteger, CZGeminiCameraShadingCorrectionInfoType) {
    CZGeminiCameraShadingCorrectionInfoGetTableSuccess = 0,
    CZGeminiCameraShadingCorrectionInfoGetTableError = 1,
    CZGeminiCameraShadingCorrectionInfoOpenSuccess = 2,
    CZGeminiCameraShadingCorrectionInfoOpenError = 3,
    CZGeminiCameraShadingCorrectionInfoCloseSuccess = 4,
    CZGeminiCameraShadingCorrectionInfoCloseError = 5
};

@interface CZGeminiCameraShadingCorrectionInfoEvent : CZGeminiCameraEvent

@property (nonatomic, readonly, retain) NSNumber *value;
@property (nonatomic, readonly, copy) NSString *info;

@end
