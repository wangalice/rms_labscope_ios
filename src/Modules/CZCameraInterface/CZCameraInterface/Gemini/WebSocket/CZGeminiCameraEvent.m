//
//  CZGeminiCameraEvent.m
//  CameraInterface
//
//  Created by Li, Junlin on 5/7/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraEvent.h"

NSString * const CZGeminiCameraEventTypeAutoExposureStable = @"aeStable";
NSString * const CZGeminiCameraEventTypeHTTPSnap = @"httpSnap";
NSString * const CZGeminiCameraEventTypeHardwareSnap = @"snapHardware";
NSString * const CZGeminiCameraEventTypeColorModeChanged = @"colorMode";
NSString * const CZGeminiCameraEventTypeDynamicDefectPixelStatusChanged = @"dynamicDefectPixel";
NSString * const CZGeminiCameraEventTypeManualColorTemperatureChanged = @"manualColorTemp";
NSString * const CZGeminiCameraEventTypeWhiteBalanceTypeChanged = @"awbType";
NSString * const CZGeminiCameraEventTypeObjectiveVersionChanged = @"objectiveVersion";
NSString * const CZGeminiCameraEventTypeReflectorVersionChanged = @"reflectorVersion";
NSString * const CZGeminiCameraEventTypeSnapFinished = @"finishSnapping";
NSString * const CZGeminiCameraEventTypeFirmwareUpdateError = @"updateError";
NSString * const CZGeminiCameraEventTypeFluorescenceSnapped = @"flSnapping";
NSString * const CZGeminiCameraEventTypeCameraConnectedToOtherDevice = @"clientConnection";
NSString * const CZGeminiCameraEventTypeWIFIStatusChanged = @"wifiStaStatus";
NSString * const CZGeminiCameraEventTypeThumbnailReady = @"thumbnailReady";
NSString * const CZGeminiCameraEventTypeOnekeyFluorescenceSnapInfo = @"oneKeyFlSnapPath";
NSString * const CZGeminiCameraEventTypeCameraAdapterChanged = @"cameraAdapterChanged";
NSString * const CZGeminiCameraEventTypeLightSourceChanged = @"lightSourceChanged";
NSString * const CZGeminiCameraEventTypeCameraNameChanged = @"cameraNameChanged";
NSString * const CZGeminiCameraEventTypeShadingCorrectionInfo = @"shading";
NSString * const CZGeminiCameraEventTypeRecordingInfo = @"recording";

@interface CZGeminiCameraEvent ()

@property (nonatomic, readwrite, copy) NSString *eventType;

@end

@implementation CZGeminiCameraEvent

- (void)dealloc {
    [_eventType release];
    [super dealloc];
}

@end
