//
//  CZGeminiCameraHardwareEvent.m
//  CameraInterface
//
//  Created by Sun, Shaoge on 2019/5/14.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraHardwareEvent.h"

@interface CZGeminiCameraHardwareEvent ()

@property (nonatomic, retain, readwrite) NSNumber *value;

@end

@implementation CZGeminiCameraHardwareEvent

- (void)dealloc {
    [_value release];
    
    [super dealloc];
}

@end
