//
//  CZGeminiCameraStatus.h
//  CameraInterface
//
//  Created by Li, Junlin on 3/4/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYModel/YYModel.h>

@class CZGeminiCameraLEDStatus;
@class CZGeminiCameraLEDBrightness;

@interface CZGeminiCameraStatus : NSObject <YYModel>

@property (nonatomic, readonly, assign) NSInteger objectivePosition; // current objective position
@property (nonatomic, readonly, assign) NSInteger reflectorPosition; // current reflector position
@property (nonatomic, readonly, assign) NSInteger rltlPosition; // current rl tl position 1-RL, 2-TL
@property (nonatomic, readonly, assign) NSInteger mode; // 1- FNT mode free; 0- FNT mode auto
@property (nonatomic, readonly, assign) NSInteger fullScreenOrThumb; // 1- image fullscreen after snap, 0-close fullscreen
@property (nonatomic, readonly, copy) NSArray<CZGeminiCameraLEDStatus *> *ledsStatus;
@property (nonatomic, readonly, copy) NSArray<CZGeminiCameraLEDBrightness *> *ledsBrightness;

@end

@interface CZGeminiCameraLEDStatus : NSObject <YYModel>

@property (nonatomic, readonly, assign) BOOL on;

@end

@interface CZGeminiCameraLEDBrightness : NSObject <YYModel>

@property (nonatomic, readonly, assign) NSInteger brightness;

@end
