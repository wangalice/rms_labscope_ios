//
//  CZGeminiWebSocketClient.h
//  CameraInterface
//
//  Created by Li, Junlin on 5/10/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGeminiCameraStatus.h"
#import "CZGeminiCameraEvent.h"
#import "CZGeminiCameraAutoExposureStableEvent.h"
#import "CZGeminiCameraHTTPSnapEvent.h"
#import "CZGeminiCameraHardwareEvent.h"
#import "CZGeminiCameraShadingCorrectionInfoEvent.h"

typedef NS_ENUM(NSUInteger, CZGeminiCameraStatusChangeMask) {
    CZGeminiCameraStatusChangeMaskObjectivePosition = 1 << 0,
    CZGeminiCameraStatusChangeMaskReflectorPosition = 1 << 1,
    CZGeminiCameraStatusChangeMaskRLTLPosition      = 1 << 2,
    CZGeminiCameraStatusChangeMaskLEDsStatus        = 1 << 3,
    CZGeminiCameraStatusChangeMaskLEDsBrightness    = 1 << 4
};

@class CZGeminiWebSocketClient;

@protocol CZGeminiWebSocketClientDelegate <NSObject>

@optional
- (void)webSocketClient:(CZGeminiWebSocketClient *)webSocketClient statusDidChange:(CZGeminiCameraStatus *)status changes:(CZGeminiCameraStatusChangeMask)changes;
- (void)webSocketClient:(CZGeminiWebSocketClient *)webSocketClient didReceiveEvent:(CZGeminiCameraEvent *)event;

@end

@interface CZGeminiWebSocketClient : NSObject

@property (nonatomic, readonly, copy) NSString *ipAddress;
@property (nonatomic, readonly, assign) id<CZGeminiWebSocketClientDelegate> delegate;

@property (nonatomic, readonly, retain) CZGeminiCameraStatus *previousStatus;
@property (nonatomic, readonly, retain) CZGeminiCameraStatus *latestStatus;

@property (nonatomic, readonly, retain) __kindof CZGeminiCameraEvent *previousEvent;
@property (nonatomic, readonly, retain) __kindof CZGeminiCameraEvent *latestEvent;

- (instancetype)initWithIPAddress:(NSString *)ipAddress delegate:(id<CZGeminiWebSocketClientDelegate>)delegate;

- (BOOL)connect;
- (BOOL)disconnect;

@end
