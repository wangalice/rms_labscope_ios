//
//  CZGeminiCameraShadingCorrectionInfoEvent.m
//  CameraInterface
//
//  Created by Li, Junlin on 7/19/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZGeminiCameraShadingCorrectionInfoEvent.h"

@interface CZGeminiCameraShadingCorrectionInfoEvent ()

@property (nonatomic, readwrite, retain) NSNumber *value;
@property (nonatomic, readwrite, copy) NSString *info;

@end

@implementation CZGeminiCameraShadingCorrectionInfoEvent

- (void)dealloc {
    [_value release];
    [_info release];
    [super dealloc];
}

@end
