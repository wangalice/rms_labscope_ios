//
//  CZGeminiCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/2/17.
//  Copyright (c) 2017 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZRTSPCamera.h"
#import "CZFluorescenceSnapping.h"
#import "CZGeminiAPIClient.h"
#import "CZGeminiWebSocketClient.h"

extern NSString * const CZGeminiCameraStatusDidChangeNotification;
extern NSString * const CZGeminiCameraStatusKey;
extern NSString * const CZGeminiCameraStatusChangesKey;

extern NSString * const CZGeminiCameraDidReceiveEventNotification;
extern NSString * const CZGeminiCameraEventKey;
extern NSString * const CZGeminiCameraClientNumberKey;

/*!
 * Represents a smart camera developed in Project Gemini for Axio-line.
 */
@interface CZGeminiCamera : CZRTSPCamera <CZFluorescenceSnapping>

@property (nonatomic, readonly, retain) CZGeminiAPIClient *apiClient;
@property (nonatomic, readonly, retain) CZGeminiWebSocketClient *webSocketClient;
@property (nonatomic, readonly, retain) CZGeminiAPISystemInfo *systemInfo;

@end
