//
//  CZMoticCamera.m
//  Matscope
//
//  Created by Halley Gu on 6/11/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMoticCamera.h"
#import "CZCameraSubclass.h"
#import <AVFoundation/AVFoundation.h>
#import <AFNetworking/AFNetworking.h>
#import <CZToolbox/CZToolbox.h>

static NSString * const kMoticBaseURLFormat = @"http://admin:ZEISS1846@%@:8080/%@";
//static const int kMoticLatestFirmwareVersion = 20150226;

static NSString * const kMoticParameterSaturation = @"9963778";
static NSString * const kMoticParameterWhiteBalanceTemperatureAuto = @"9963788";
static NSString * const kMoticParameterWhiteBalanceTemperature = @"9963802";
static NSString * const kMoticParameterSharpness = @"9963803";
static NSString * const kMoticParameterExposureMode = @"10094849";
static NSString * const kMoticParameterManualExposureTime = @"10094850";

static const NSTimeInterval kMoticDefaultTimeout = 20;
static const NSTimeInterval kMoticFrameTimeout = 3;
static const NSTimeInterval kMoticGetTimeout = 10;

static const uint64_t kFreeSpaceLowerLimit = 200 * 1024 * 1024;

@interface CZMoticCamera () {
    dispatch_queue_t _dispatchingQueue;
    dispatch_queue_t _recordingQueue;
    NSObject *_frameBufferLock;
    NSObject *_assetLock;
    NSObject *_frameCacheLock;
    NSObject *_recordFrameCacheLock;
    CGSize _thumbnailSize;
}

@property (nonatomic, retain) UIImage *lastThumbnail;
@property (nonatomic, retain) NSOperationQueue *playbackQueue;
@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSMutableData *frameBuffer;
@property (nonatomic, retain) NSMutableArray *frameCache;
@property (nonatomic, retain) NSMutableArray *recordFrameCache;
@property (atomic, retain) AVAssetWriter *assetWriter;
@property (atomic, retain) AVAssetWriterInput *assetWriterInput;
@property (atomic, retain) AVAssetWriterInputPixelBufferAdaptor *assetWriterAdaptor;
@property (atomic, retain) NSDate *recordingStartTime;
@property (atomic, assign) BOOL isRecordingEmpty;
@property (atomic, assign, readonly) BOOL isNextFrameForSnapshot;
@property (atomic, assign) NSUInteger snappingFrameCount;  // frames count to snap
@property (atomic, assign) BOOL isNextFrameForThumbnail;
@property (atomic, assign) BOOL shouldStopAfterNextFrame;
@property (atomic, assign) BOOL isDecodingPaused;

@end

@implementation CZMoticCamera

- (id)init {
    self = [super init];
    if (self) {
        _dispatchingQueue = dispatch_queue_create("com.zeisscn.motic.dispatching", NULL);
        _recordingQueue = dispatch_queue_create("com.zeisscn.motic.recording", NULL);
        _frameBuffer = [[NSMutableData alloc] init];
        _frameCache = [[NSMutableArray alloc] init];
        _recordFrameCache = [[NSMutableArray alloc] init];
        _frameBufferLock = [[NSObject alloc] init];
        _assetLock = [[NSObject alloc] init];
        _frameCacheLock = [[NSObject alloc] init];
        _recordFrameCacheLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [self stop];
    
    if (_dispatchingQueue != NULL) {
        dispatch_release(_dispatchingQueue);
    }
    if (_recordingQueue != NULL) {
        dispatch_release(_recordingQueue);
    }
    
    [_lastThumbnail release];
    [_playbackQueue release];
    [_connection release];
    [_frameBuffer release];
    [_frameCache release];
    [_recordFrameCache release];
    [_frameBufferLock release];
    [_assetLock release];
    [_frameCacheLock release];
    [_recordFrameCacheLock release];
    [_assetWriter release];
    [_assetWriterInput release];
    [_assetWriterAdaptor release];
    [_recordingStartTime release];
    
    [super dealloc];
}

#pragma mark - Overridden methods

- (CZCameraCapability)capabilities {
    return (CZCameraCapabilityResetAll |
            CZCameraCapabilityExposureMode |
            CZCameraCapabilityExposureTime |
            CZCameraCapabilityWhiteBalanceMode |
            CZCameraCapabilityColorTemperature |
            CZCameraCapabilityIllumination |
            CZCameraCapabilitySharpening |
            CZCameraCapabilityGrayscaleMode |
            CZCameraCapabilityWebConfiguration);
}

- (NSString *)firmwareVersion {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:kMoticBaseURLFormat, self.ipAddress, @""];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    if (baseUrl == nil) {
        [baseUrlString release];
        return nil;
    }

    NSString *urlPath = @"?get_config=version";
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:kMoticGetTimeout];
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    NSString *version = nil;
    
    if (result.error || ([result.response isKindOfClass:[NSHTTPURLResponse class]] && ((NSHTTPURLResponse *)result.response).statusCode != 200)) {
        CZLogv(@"Failed to send the request to get firmware version from Motic camera: %@", [result.error localizedDescription]);
    } else {
        version = [[[NSString alloc] initWithData:result.data encoding:NSUTF8StringEncoding] autorelease];
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return version;
}

- (BOOL)isFirmwareUpdated {
    return YES; // Disable firmware update for Moticam at this moment.
    
    /*
    NSString *version = [self firmwareVersion];
    int versionDate = 0;
    NSString *temp;
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:version];
    [scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] intoString:&temp];
    [scanner scanInt:&versionDate];
    [scanner release];
    
    return (versionDate >= kMoticLatestFirmwareVersion);
     */
}

- (void)updateFirmware {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"stemi305cam_wifi_fw.bin" ofType:@""];
    NSData *fileData = [[NSData alloc] initWithContentsOfFile:filePath];
    CZLogv(@"Firmware binary length = %lu", (unsigned long)fileData.length);
    if (fileData) {
        NSString *baseUrlString = [[NSString alloc] initWithFormat:kMoticBaseURLFormat, self.ipAddress, @""];
        NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
        if (baseUrl == nil) {
            [baseUrlString release];
            [fileData release];
            return;
        }
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
        [httpClient setAuthorizationHeaderWithUsername:@"admin" password:@"ZEISS1846"];
        httpClient.parameterEncoding = AFFormURLParameterEncoding;
        
        void (^formDataBlock)(id<AFMultipartFormData> formData) = ^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:fileData
                                        name:@"romFile"
                                    fileName:@"zeiss.bin"
                                    mimeType:@"application/octet-stream"];
        };
        
        NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                             path:@"update"
                                                                       parameters:nil
                                                        constructingBodyWithBlock:formDataBlock];
        [request setTimeoutInterval:300];
        
        CZLogv(@"Sending firmware to the camera:\n\n%@", request.allHTTPHeaderFields);
        
        // No response will be available from Moticam after new firmware is received, so use
        // asynchronous request.
        [[[NSURLSession sharedSession] dataTaskWithRequest:request] resume];
        
        [httpClient release];
        [baseUrl release];
        [baseUrlString release];
    }
    [fileData release];
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    [super setName:name andUpload:shouldUpload];
    
    if (shouldUpload && name != nil) {
        if (![[CZWifiNotifier sharedInstance] isReachable]) {
            return;
        }
        
        NSString *urlString = [[NSString alloc] initWithFormat:kMoticBaseURLFormat,
                               self.ipAddress,
                               [NSString stringWithFormat:@"?set_device_name=%@", name]];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setTimeoutInterval:kMoticDefaultTimeout];
        [url release];
        [urlString release];
        
        void (^successBlock)(AFHTTPRequestOperation *, id) =
        ^(AFHTTPRequestOperation *operation, id responseObject) {
        };
        
        void (^failureBlock)(AFHTTPRequestOperation *, NSError *) =
        ^(AFHTTPRequestOperation *operation, NSError *error) {
            CZLogv(@"Failed to send the request to change name of Motic camera: %@", [error localizedDescription]);
        };
        
        AFHTTPRequestOperation *operation;
        operation = [[[AFHTTPRequestOperation alloc] initWithRequest:request] autorelease];
        [operation setCompletionBlockWithSuccess:successBlock failure:failureBlock];
        [operation start];
    }
}

- (UIImage *)thumbnail {
    return self.lastThumbnail;
}

- (UIImage *)liveThumbnail {
    return self.lastThumbnail;
}

- (void)setLiveThumbnail:(UIImage *)liveThumbnail {
    if (liveThumbnail.size.width > _thumbnailSize.width &&
        liveThumbnail.size.height > _thumbnailSize.height) {
        liveThumbnail = [CZCommonUtils scaleImage:liveThumbnail
                                         intoSize:_thumbnailSize];
    }
    
    if (liveThumbnail) {
        self.lastThumbnail = liveThumbnail;
    }
}

- (void)beginThumbnailUpdating:(CGSize)thumbnailSize {
    self.isNextFrameForThumbnail = YES;
    _thumbnailSize = thumbnailSize;
    
    if (self.playbackQueue == nil) {
        [self playForSnapping];
    }
}

- (void)play {
    [self stop];
    
    if (self.playbackQueue == nil) {
        _playbackQueue = [[NSOperationQueue alloc] init];
        
        self.shouldStopAfterNextFrame = NO;
        [self beginGettingNextFrame];
    }
}

- (void)playForSnapping {
    if (self.playbackQueue == nil) {
        _playbackQueue = [[NSOperationQueue alloc] init];
        
        self.shouldStopAfterNextFrame = YES;
        [self beginGettingNextFrame];
    }
}

- (void)stop {
    if (self.connection != nil) {
        [self.connection cancel];
        self.connection = nil;
    }
    
    NSOperationQueue *queueToWait = nil;
    if (self.playbackQueue != nil) {
        queueToWait = [self.playbackQueue retain];
        [self.playbackQueue cancelAllOperations];
        self.playbackQueue = nil;
    }
    
    @synchronized (_frameBufferLock) {
        [self.frameBuffer setLength:0];
    }
    
    if (queueToWait) {
        __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
        bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"StopMoticCamera"
                                                              expirationHandler:^{
                                                                  if (bgTask != UIBackgroundTaskInvalid) {
                                                                      [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                                                                      bgTask = UIBackgroundTaskInvalid;
                                                                  }
                                                              }];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
            [queueToWait waitUntilAllOperationsAreFinished];
            
            if (bgTask != UIBackgroundTaskInvalid) {
                [[UIApplication sharedApplication] endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
            }
        });
        
        [queueToWait release];
    }
}

- (void)pause {
    self.isDecodingPaused = YES;
}

- (void)resume {
    self.isDecodingPaused = NO;
}

- (BOOL)isPaused {
    return self.isDecodingPaused;
}

- (void)beginContinuousSnappingFromVideoStream:(NSUInteger)frameCount {
    self.snappingFrameCount = frameCount;

    if (self.playbackQueue == nil) {
        [self playForSnapping];
    }
}

- (void)beginRecordingToFile:(NSString *)filePath {
    @synchronized (_assetLock) {
        if (self.assetWriter == nil) {
            NSError *error = nil;
            AVAssetWriter *writer = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:filePath]
                                                              fileType:AVFileTypeMPEG4
                                                                 error:&error];
            
            CGSize videoSize = [self liveResolution];
            NSDictionary *videoSettings = @{AVVideoCodecKey: AVVideoCodecJPEG,
                                            AVVideoWidthKey: @(videoSize.width),
                                            AVVideoHeightKey: @(videoSize.height)};
            
            AVAssetWriterInput *writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                                                 outputSettings:videoSettings];
            [writerInput setExpectsMediaDataInRealTime:YES];
            
            AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                                                                                                             sourcePixelBufferAttributes:nil];
            
            if ([writer canAddInput:writerInput]) {
                [writer addInput:writerInput];
                
                self.assetWriter = writer;
                self.assetWriterInput = writerInput;
                self.assetWriterAdaptor = adaptor;
                self.recordingStartTime = [NSDate date];
                self.isRecordingEmpty = YES;
                
                [writer startWriting];
                [writer startSessionAtSourceTime:kCMTimeZero];
                
                CZLogv(@"Video recording started.");
            } else {
                CZLogv(@"Failed to start video recording.");
            }
            
            [writer release];
        }
    }
}

- (void)endRecording {
    @synchronized (_assetLock) {
        if (self.assetWriter != nil) {
            [self.assetWriterInput markAsFinished];
            [self.assetWriter finishWritingWithCompletionHandler:^(){
                if ([self.delegate respondsToSelector:@selector(cameraDidFinishRecording:)]) {
                    [self.delegate cameraDidFinishRecording:self];
                }
            }];
            
            if (self.assetWriter.error != nil) {
                CZLogv(@"Video recording ended with error: %@", self.assetWriter.error);
            } else {
                CZLogv(@"Video recording ended.");
            }
            
            self.assetWriter = nil;
            self.assetWriterInput = nil;
            self.assetWriterAdaptor = nil;
            self.recordingStartTime = nil;
        }
    }
}

- (void)beginTimelapseRecordingToFile:(NSString *)filePath
                          inFrameRate:(NSUInteger)frameRate
                             interval:(NSTimeInterval)interval
                            overlayer:(CALayer *)overlayer {
    if (![self.timelapseRecorder isRecording]) {
        CZTimelapseRecorder *recorder = [[CZTimelapseRecorder alloc] init];
        recorder.interval = interval;
        recorder.frameRate = frameRate;
        recorder.videoCodecKey = AVVideoCodecJPEG;
        recorder.videoSize = [self liveResolution];
        recorder.overlayer = overlayer;
        recorder.delegate = self;
        
        self.timelapseRecorder = recorder;
        [self.timelapseRecorder beginRecordingToFile:filePath];
        [recorder release];
    }
}

- (CGSize)liveResolution {
    return CGSizeMake(1280, 960);
}

- (BOOL)isNextFrameForSnapshot {
    return self.snappingFrameCount > 0;
}

#pragma mark - Exposure Mode

- (CZCameraExposureMode)exposureMode {
    NSString *value = [self getParameter:kMoticParameterExposureMode];
    if ([value intValue] == 1) {
        return kCZCameraExposureManual;
    }
    return kCZCameraExposureAutomatic;
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
    NSString *newValue = (exposureMode == kCZCameraExposureAutomatic) ? @"3" : @"1";
    [self setValue:newValue forParameter:kMoticParameterExposureMode];
    
    if (exposureMode != kCZCameraExposureAutomatic) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            NSString *value = [self getParameter:kMoticParameterManualExposureTime];
            [self setValue:value forParameter:kMoticParameterManualExposureTime];
        });
    }
}

#pragma mark - Exposure Time

- (NSArray<NSNumber *> *)supportedExposureTimeValues {
    return @[@0.5f, @1.f, @2.f, @5.f, @10.f, @16.6666f, @20.f, @30.f,
             @50.f, @66.66666f, @100.f, @140.f, @200.f];
}

- (float)exposureTime {
    float exposureTime = 0;
    CZCameraExposureMode exposureMode = [self exposureMode];
    if (exposureMode == kCZCameraExposureManual) {
        NSString *value = [self getParameter:kMoticParameterManualExposureTime];
        NSArray *exposureTimeValues = [self supportedExposureTimeValues];
        NSArray *abstractValues = [CZMoticCamera abstractValuesForExposureTime];
        
        int minDistance = INT_MAX;
        
        NSUInteger index = 0;
        
        for (NSNumber *abstractValue in abstractValues) {
            int distance = abs(abstractValue.intValue - value.intValue);
            if (distance < minDistance) {
                minDistance = distance;
                exposureTime = [exposureTimeValues[index] floatValue];
            }
            ++index;
        }
    }
    return exposureTime;
}

- (void)setExposureTime:(float)milliseconds {
    NSUInteger index = [self nearestValidExposureTimeIndex:milliseconds];
    NSNumber *value = [CZMoticCamera abstractValuesForExposureTime][index];
    [self setValue:[value stringValue] forParameter:kMoticParameterManualExposureTime];
}

#pragma mark - White Balance Mode

- (BOOL)isWhiteBalanceLocked {
    NSString *value = [self getParameter:kMoticParameterWhiteBalanceTemperatureAuto];
    int isAuto = [value intValue];
    return (isAuto != 1);
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
    if (locked) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            [self setValue:@"0" forParameter:kMoticParameterWhiteBalanceTemperatureAuto];
            
            usleep(0.6 * USEC_PER_SEC);
            
            NSUInteger value = [self whiteBalanceValue];
            [self setWhiteBalanceWithValue:value];
        });
    } else {
        [self setValue:@"1" forParameter:kMoticParameterWhiteBalanceTemperatureAuto];
    }
}

#pragma mark - Color Temperature

- (NSUInteger)coldestWhiteBalanceValue {
    return 0;
}

- (NSUInteger)warmestWhiteBalanceValue {
    return 255;
}

- (NSUInteger)nearestValidWhiteBalanceValue:(float)value {
    return roundf(value / 5.0) * 5;
}

- (NSUInteger)whiteBalanceValue {
    NSString *valueString = [self getParameter:kMoticParameterWhiteBalanceTemperature];
    int value = [valueString intValue];
    return (value < 0 || value > 255) ? 128 : value;
}

- (void)setWhiteBalanceWithValue:(NSUInteger)value {
    if (value <= 255) {
        [self setValue:[NSString stringWithFormat:@"%lu", (unsigned long)value] forParameter:kMoticParameterWhiteBalanceTemperature];
    }
}

#pragma mark - Snap Resolution

- (CGSize)lowSnapResolution {
    return CGSizeMake(1280, 960);
}

- (CGSize)highSnapResolution {
    return CGSizeMake(1280, 960);
}

#pragma mark - Sharpening

- (uint32_t)defaultSharpness {
    return 0;
}

- (uint32_t)sharpnessStepCount {
    return 16; // 0-15
}

- (uint32_t)sharpness {
    NSString *value = [self getParameter:kMoticParameterSharpness];
    int sharpness = [value intValue];
    if (sharpness < 0 || sharpness >= [self sharpnessStepCount]) {
        CZLogv(@"Invalid sharpness value returned by Motic camera.");
        return 0;
    }
    return sharpness;
}

- (void)setSharpness:(uint32_t)sharpness {
    NSString *newSharpness = [NSString stringWithFormat:@"%lu", (unsigned long)sharpness];
    [self setValue:newSharpness forParameter:kMoticParameterSharpness];
}

#pragma mark - Grayscale Mode

- (BOOL)isGrayscaleModeEnabledByDefault {
    return NO;
}

- (BOOL)isGrayscaleModeEnabled {
    NSString *value = [self getParameter:kMoticParameterSaturation];
    if (value == nil) {
        return NO;
    }
    
    int saturation = [value intValue];
    if (saturation == 0) {
        return YES;
    }
    return NO;
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
    NSString *newSaturation = enabled ? @"0" : @"16"; // 16 is default saturation, range is 0-31
    [self setValue:newSaturation forParameter:kMoticParameterSaturation];
}

#pragma mark - Web Configuration

- (NSURL *)webInterfaceURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:8080/", self.ipAddress]];
}

#pragma mark - Private methods

+ (NSArray *)abstractValuesForExposureTime {
    static NSArray *array = nil;
    if (array == nil) {
        array = [@[@1, @2, @5, @10, @20, @39, @78, @156, @312, @625, @1250, @2500, @5000] retain];
    }
    return array;
}

- (void)beginGettingNextFrame {
    if (self.playbackQueue == nil) {
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:kMoticBaseURLFormat, self.ipAddress, @"?action=image"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                         timeoutInterval:kMoticFrameTimeout];
    
    NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    [urlConnection setDelegateQueue:self.playbackQueue];
    self.connection = urlConnection;
    [urlConnection start];
    [urlConnection release];
}

- (NSString *)getParameter:(NSString *)param {
    NSString *value = nil;
    
    if (param == nil || [param length] == 0) {
        return value;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:kMoticBaseURLFormat, self.ipAddress, @""];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    if (baseUrl == nil) {
        [baseUrlString release];
        return nil;
    }
    NSString *urlPath = @"params.json";
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:kMoticGetTimeout];
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    if (result.error) {
        CZLogv(@"Failed to send the request to get parameters from Motic camera: %@", [result.error localizedDescription]);
    } else {
        NSError *error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:result.data options:NSJSONReadingMutableContainers error:&error];
        
        if (error != nil) {
            CZLogv(@"Failed to parse the response from Motic camera as JSON: %@", [error localizedDescription]);
        } else {
            NSDictionary *dict = (NSDictionary *)json;
            NSArray *params = dict[@"controls"];
            if (params) {
                for (NSDictionary *aParam in params) {
                    NSString *paramId = aParam[@"id"];
                    if ([paramId isEqualToString:param]) {
                        value = aParam[@"value"];
                        break;
                    }
                }
                
                if (value != nil) {
                    CZLogv(@"Got parameter from %@ of Motic camera: %@ = %@", urlPath, param, value);
                } else {
                    CZLogv(@"Parameter %@ not found on Motic camera.", param);
                }
            } else {
                CZLogv(@"Parameters not found on Motic camera.");
            }
        }
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return value;
}

- (void)setValue:(NSString *)value forParameter:(NSString *)param {
    if (param == nil || [param length] == 0) {
        return;
    }
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return;
    }
    
    NSString *urlString = [[NSString alloc] initWithFormat:kMoticBaseURLFormat,
                           self.ipAddress,
                           [NSString stringWithFormat:@"?action=parameter&dest=0&plugin=0&group=1&id=%@&value=%@", param, value]];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setTimeoutInterval:kMoticDefaultTimeout];
    [url release];
    [urlString release];
    
    void (^successBlock)(AFHTTPRequestOperation *, id) =
    ^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *data = responseObject;
        NSString *body = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
        CZLogv(@"Response from Motic camera: %@", body);
        [body release];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *, NSError *) =
    ^(AFHTTPRequestOperation *operation, NSError *error) {
        CZLogv(@"Failed to send the request to set parameters onto Motic camera: %@", [error localizedDescription]);
    };
    
    AFHTTPRequestOperation *operation;
    operation = [[[AFHTTPRequestOperation alloc] initWithRequest:request] autorelease];
    [operation setCompletionBlockWithSuccess:successBlock failure:failureBlock];
    [operation start];
}

- (void)recordVideoFrame:(UIImage *)image {
    @synchronized (_assetLock) {
        NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:self.recordingStartTime];
        if (self.isRecordingEmpty) {
            // Let the first incoming frame be the start of video file.
            time = 0;
            self.isRecordingEmpty = NO;
        }
        
        CMTime timestamp = CMTimeMakeWithSeconds(time, 90000);
        
        CGSize frameSize = image.size;
        NSDictionary *options = @{(id)kCVPixelBufferCGImageCompatibilityKey: @YES,
                                  (id)kCVPixelBufferCGBitmapContextCompatibilityKey: @YES};
        
        CVPixelBufferRef pixelBuffer = NULL;
        CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width, frameSize.height,
                            kCVPixelFormatType_32ARGB, (CFDictionaryRef)options, &pixelBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        void *pixelData = CVPixelBufferGetBaseAddress(pixelBuffer);
        size_t rowBytes = CVPixelBufferGetBytesPerRow(pixelBuffer);
        
        CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
        CGContextRef context = CGBitmapContextCreate(pixelData, frameSize.width, frameSize.height, 8,
                                                     rowBytes, rgbColorSpace, kCGImageAlphaNoneSkipFirst);
        
        CGContextDrawImage(context, CGRectMake(0, 0, frameSize.width, frameSize.height), image.CGImage);
        
        CGColorSpaceRelease(rgbColorSpace);
        CGContextRelease(context);
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
        
        BOOL isWritten = NO;
        int retried = 0;
        while (!isWritten && retried < 5) {
            if (self.assetWriterInput.readyForMoreMediaData) {
                isWritten = [self.assetWriterAdaptor appendPixelBuffer:pixelBuffer
                                                  withPresentationTime:timestamp];
            }
            
            if (!isWritten) {
                usleep(0.05 * NSEC_PER_USEC);
                ++retried;
            }
        }
        
        CVPixelBufferRelease(pixelBuffer);
    }
    
    if (![self hasEnoughFreeSpace]) {
        [self endRecording];
    }
}

- (BOOL)hasEnoughFreeSpace {
    BOOL hasEnoughFreeSpace = YES;
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        uint64_t totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        
        if (totalFreeSpace <= kFreeSpaceLowerLimit) {
            hasEnoughFreeSpace = NO;
        }
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return hasEnoughFreeSpace;
}

#pragma mark - NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (self.isPaused) {
        @synchronized (_frameBufferLock) {
            [self.frameBuffer setLength:0];
        }
        return;
    }
    
    UIImage *image = nil;

    BOOL delegateNeedsVideoFrame = YES;
    if ([self.delegate respondsToSelector:@selector(cameraShouldDeliverVideoFrame:)]) {
        delegateNeedsVideoFrame = [self.delegate cameraShouldDeliverVideoFrame:self];
    }

    BOOL requireImage =
        (self.assetWriter != nil) ||
        (self.timelapseRecorder != nil) ||
        self.isNextFrameForThumbnail ||
        self.isNextFrameForSnapshot ||
        (!self.shouldStopAfterNextFrame && delegateNeedsVideoFrame);

    @synchronized (_frameBufferLock) {
        if ([self.frameBuffer length] > 0) {
            if (requireImage) {
                image = [[UIImage alloc] initWithData:self.frameBuffer];
//                CZLogv(@"CZMoticCamera: decoded one frame.");
            } else {
//                CZLogv(@"CZMoticCamera: skip one frame.");
            }
            [self.frameBuffer setLength:0];
        }
    }

    if (image != nil) {
        self.lastActiveTime = [NSDate dateWithTimeIntervalSinceNow:0];
        
        if (self.assetWriter != nil || self.timelapseRecorder) {
            BOOL isTimelapseRecording = (self.timelapseRecorder != nil);
            BOOL needDispatch = NO;
            @synchronized (_recordFrameCacheLock) {
                needDispatch = self.recordFrameCache.count == 0;
                [self.recordFrameCache insertObject:image atIndex:0];
                if (self.recordFrameCache.count > 5) { // cache is full, drop 1 frame
                    [self.recordFrameCache removeLastObject];
                }
            }
            
            if (needDispatch) {
                dispatch_async(_recordingQueue, ^(){
                    do {
                        UIImage *frame = nil;
                        @synchronized (_recordFrameCacheLock) {
                            frame = [self.recordFrameCache lastObject];
                            [frame retain];
                            [self.recordFrameCache removeLastObject];
                        }
                        
                        if (frame == nil) {
                            break;
                        }
                        
                        if (isTimelapseRecording) {
                            [self.timelapseRecorder recordVideoFrame:frame];
                        } else {
                            [self recordVideoFrame:frame];
                        }
                        
                        [frame release];
                    } while (YES);
                });
            }
        }
        
        if (self.isNextFrameForThumbnail) {
            self.isNextFrameForThumbnail = NO;
            if (self.shouldStopAfterNextFrame) {
                self.shouldStopAfterNextFrame = NO;
                [self stop];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^() {
                self.lastThumbnail = [CZCommonUtils scaleImage:image intoSize:_thumbnailSize];
                
                if ([self.thumbnailDelegate respondsToSelector:@selector(camera:didUpdateThumbnail:)]) {
                    [self.thumbnailDelegate camera:self didUpdateThumbnail:image];
                }
            });
        }
        
        if (self.isNextFrameForSnapshot) {
            self.snappingFrameCount--;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                CZCameraExposureMode exposureMode = [self exposureMode];
                float exposure = [self exposureTime];
                BOOL grayscale = [self isGrayscaleModeEnabled];
                
                dispatch_async(dispatch_get_main_queue(), ^() {
                    if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                        CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                        snappingInfo.image = image;
                        snappingInfo.grayscale = grayscale;
                        if (exposureMode == kCZCameraExposureManual) {
                            snappingInfo.exposureTimeInMilliseconds = @(exposure);
                        }
                        [self.delegate camera:self didFinishSnapping:snappingInfo error:nil];
                        [snappingInfo release];
                    }
                });
            });
        }
        
        if (!self.shouldStopAfterNextFrame && delegateNeedsVideoFrame) {
            BOOL needDispatch = NO;
            @synchronized (_frameCacheLock) {
                needDispatch = self.frameCache.count == 0;
                [self.frameCache insertObject:image atIndex:0];
                if (self.frameCache.count > 5) { // cache is full, drop 1 frame
                    [self.frameCache removeLastObject];
                }
            }
            
            if (needDispatch) {
                dispatch_async(_dispatchingQueue, ^(){
                    do {
                        UIImage *frame = nil;
                        @synchronized (_frameCacheLock) {
                            frame = [self.frameCache lastObject];
                            [frame retain];
                            [self.frameCache removeLastObject];
                        }
                        
                        if (frame == nil) {
                            break;
                        }
                        
                        self.lastFrameTime = [NSDate dateWithTimeIntervalSinceNow:0];
                        
                        if ([self.delegate respondsToSelector:@selector(camera:didGetVideoFrame:)]) {
                            [self.delegate camera:self didGetVideoFrame:frame];
                        }
                        
                        [frame release];
                    } while (YES);
                });
            }
        }
        
        [image release];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (!self.isPaused) {
        @synchronized (_frameBufferLock) {
            [self.frameBuffer appendData:data];
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (self.shouldStopAfterNextFrame) {
        self.shouldStopAfterNextFrame = NO;
        [self stop];
    } else {
        [self beginGettingNextFrame];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (self.isNextFrameForThumbnail) {
        self.isNextFrameForThumbnail = NO;
        
        CZLogv(@"Failed to get thumbnail from Motic camera.");
    }
    
    if (self.isNextFrameForSnapshot) {
        self.snappingFrameCount = 0;
        
        CZLogv(@"Failed to snap: %@", [error localizedDescription]);
        
        NSError *timeoutError = [NSError errorWithDomain:CZCameraErrorDomain
                                                    code:kCZCameraTimeoutError
                                                userInfo:nil];
        
        id delegate = self.delegate;
        dispatch_async(dispatch_get_main_queue(), ^() {
            if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                [delegate camera:self didFinishSnapping:nil error:timeoutError];
            }
        });
    }
    
    if (self.shouldStopAfterNextFrame) {
        self.shouldStopAfterNextFrame = NO;
        [self stop];
    } else {
        [self beginGettingNextFrame];
    }
}

@end
