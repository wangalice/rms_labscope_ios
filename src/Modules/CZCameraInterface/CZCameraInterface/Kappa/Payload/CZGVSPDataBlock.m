//
//  CZGVSPDataBlock.m
//  Hermes
//
//  Created by Halley Gu on 7/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVSPDataBlock.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVSPDataBlock

- (id)init {
    [self release];
    return nil;
}

- (id)initWithData:(NSData *)data {
    if ([data length] <= 0) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _rawData = [data retain];
        
        CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                       length:data.length];
        
        _status = [reader readShortInHostOrder];
        uint16_t flag = [reader readShortInHostOrder];
        
        uint8_t temp = [reader readByte];
        _extendedId = (temp & 0x80);
        _packetFormat = temp & 0xF;
        
        if (_extendedId) {
            _flag = flag;
            [reader jumpByLength:3];
            uint64_t blockIdHigh = [reader readLongInHostOrder];
            uint64_t blockIdLow = [reader readLongInHostOrder];
            _blockId = (blockIdHigh << 16) + blockIdLow;
            _packetId = [reader readLongInHostOrder];
        } else {
            _flag = 0;
            _blockId = flag;
            uint32_t packetIdHigh = [reader readByte];
            uint32_t packetIdLow = [reader readShortInHostOrder];
            _packetId = (packetIdHigh << 8) + packetIdLow;
        }
        
        [reader release];
    }
    
    return self;
}

- (void)dealloc {
    [_rawData release];
    [super dealloc];
}

@end
