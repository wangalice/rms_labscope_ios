//
//  CZGVSPImagePayload.m
//  Hermes
//
//  Created by Halley Gu on 7/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVSPImagePayload.h"
#import <CZToolbox/CZToolbox.h>
#import "CZGEVCommonDefs.h"
#import "CZGVSPDataBlock.h"

@interface CZGVSPImagePayload () {
    void *_buffer;
    size_t _bufferLength;
    int _width;
    int _height;
    char *_packetFlags;
    NSUInteger _packetsRetrieved;
}

@end

@implementation CZGVSPImagePayload

- (id)init {
    self = [super init];
    if (self) {
        _buffer = NULL;
        _bufferLength = 0;
        _width = 0;
        _height = 0;
        _packetFlags = NULL;
        _packetsRetrieved = 0;
    }
    return self;
}

- (void)dealloc {
    if (_buffer) {
        free(_buffer);
    }
    if (_packetFlags) {
        free(_packetFlags);
    }
    [super dealloc];
}

- (void)addDataBlock:(CZGVSPDataBlock *)block {
    NSData *data = block.rawData;
    size_t dataOffset = block.extendedId ? 20 : 8; // Per GigEVision spec
    
    switch (block.packetFormat) {
        case kGVSPPacketFormatLeader:
        {
            self.blockId = block.blockId;
            
            if (_buffer == NULL) {
                CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                               length:data.length];
                [reader seekToOffset:dataOffset + 16];
                _width = [reader readLongInHostOrder];
                _height = [reader readLongInHostOrder];
                
                _bufferLength = _width * _height * 3 / 2; // YUV420 = 1 + 0.25 + 0.25 = 1.5
                _buffer = calloc(_bufferLength, 1);
                
                size_t packetsCount = (_bufferLength - 1) / kGVSPDataPacketSize + 3; // Including leader and tail
                if (_packetFlags == NULL) {
                    _packetFlags = (char *)calloc(packetsCount, 1);
                } else {
                    memset(_packetFlags, 0, packetsCount);
                }
                
                _packetFlags[block.packetId] = 1;
                
                [reader release];
            }
            break;
        }
            
        case kGVSPPacketFormatGenericPayload:
        {
            if (_buffer) {
                size_t bufferOffset = kGVSPDataPacketSize * (block.packetId - 1);
                size_t length = data.length - dataOffset;
                if (bufferOffset + length > _bufferLength) {
                    length = _bufferLength - bufferOffset;
                }
                memcpy(_buffer + bufferOffset, data.bytes + dataOffset, length);
                
                if (_packetFlags[block.packetId] == 0) {
                    _packetFlags[block.packetId] = 1;
                    ++_packetsRetrieved;
                } else {
                    CZLogv(@"Kappa snapping: receive duplicate block(%d)", block.packetId);
                }
            }
            break;
        }
            
        default:
            break;
    }
}

- (BOOL)hasGotAllPackets {
    size_t packetsCount;
    if (_bufferLength == 0) {  // require packet data to calculate buffer length
        return NO;
    } else {
        packetsCount = (_bufferLength - 1) / kGVSPDataPacketSize + 1;
    }
    return _packetsRetrieved >= packetsCount;
}

- (NSArray *)rangesForPacketResending {
    if (_packetFlags && ![self hasGotAllPackets]) {
        NSMutableArray *mutableRanges = [[NSMutableArray alloc] init];
        size_t packetsCount;
        if (_bufferLength == 0) {
            packetsCount = 0;
        } else {
            packetsCount = (_bufferLength - 1) / kGVSPDataPacketSize + 1;
        }
        int firstMissing = 0;
        
        for (int i = 1; i <= packetsCount; ++i) {
            if (_packetFlags[i] == 0 && firstMissing == 0) {
                firstMissing = i;
            }
            
            if (firstMissing > 0) {
                NSUInteger length = 0;
                if (_packetFlags[i] != 0) {
                    length = i - firstMissing;
                } else if (i == packetsCount) { // Last packet ID
                    length = i - firstMissing + 1;
                }
                
                if (length > 0) {
                    [mutableRanges addObject:[NSValue valueWithRange:NSMakeRange(firstMissing, length)]];
                    firstMissing = 0;
                }
            }
        }
        
        NSArray *ranges = [NSArray arrayWithArray:mutableRanges];
        [mutableRanges release];
        
        return ranges;
    } else {
        return nil;
    }
}

- (UIImage *)imageFromPayload {
    if (![self hasGotAllPackets]) {
        return nil;
    }
    
    return [CZCommonUtils imageFromYUV420spNV12Buffer:_buffer length:_bufferLength width:_width height:_height];
}

@end
