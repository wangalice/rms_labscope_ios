//
//  CZGVSPImagePayload.h
//  Hermes
//
//  Created by Halley Gu on 7/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CZGVSPDataBlock;

@interface CZGVSPImagePayload : NSObject

@property (nonatomic, assign) uint64_t blockId;

- (void)addDataBlock:(CZGVSPDataBlock *)block;

- (BOOL)hasGotAllPackets;
- (NSArray *)rangesForPacketResending;

- (UIImage *)imageFromPayload;

@end
