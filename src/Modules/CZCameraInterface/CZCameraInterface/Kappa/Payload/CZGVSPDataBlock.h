//
//  CZGVSPDataBlock.h
//  Hermes
//
//  Created by Halley Gu on 7/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGEVCommonDefs.h"

@interface CZGVSPDataBlock : NSObject

@property (nonatomic, assign, readonly) uint16_t status;
@property (nonatomic, assign, readonly) uint16_t flag;
@property (nonatomic, assign, readonly) BOOL extendedId;
@property (nonatomic, assign, readonly) CZGVSPPacketFormat packetFormat;
@property (nonatomic, assign, readonly) uint32_t packetId;
@property (nonatomic, assign, readonly) uint64_t blockId;

@property (nonatomic, retain, readonly) NSData *rawData;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

@end
