//
//  CZGEVCommonDefs.h
//  Hermes
//
//  Created by Halley Gu on 3/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef CZGigEVisionDefs_H
#define CZGigEVisionDefs_H

#define kGVCPHeaderTypeOffset       2
#define kGVCPCommandHeaderLength    8
#define kGVCPAcknowlegeHeaderLength 8
#define kGVSPDataPacketSize         1420

typedef NS_ENUM(uint16_t, CZGEVStatus) {
    kGEVStatusSuccess                           = 0x0000,
    kGEVStatusPacketResend                      = 0x0100,
    kGEVStatusNotImplemented                    = 0x8001,
    kGEVStatusInvalidParameter                  = 0x8002,
    kGEVStatusInvalidAddress                    = 0x8003,
    kGEVStatusWriteProtect                      = 0x8004,
    kGEVStatusBadAlignment                      = 0x8005,
    kGEVStatusAccessDenied                      = 0x8006,
    kGEVStatusBusy                              = 0x8007,
    kGEVStatusPacketUnavailable                 = 0x800C,
    kGEVStatusDataOverrun                       = 0x800D,
    kGEVStatusInvalidHeader                     = 0x800E,
    kGEVStatusPacketNotYetAvailable             = 0x8010,
    kGEVStatusPacketAndPrevRemovedFromMemory    = 0x8011,
    kGEVStatusPacketRemovedFromMemory           = 0x8012,
    kGEVStatusNoRefTime                         = 0x8013,
    kGEVStatusPacketTemporarilyUnavailable      = 0x8014,
    kGEVStatusOverflow                          = 0x8015,
    kGEVStatusActionLate                        = 0x8016,
    kGEVStatusError                             = 0x8FFF
};

typedef NS_ENUM(uint16_t, CZGVCPMessageType) {
    // Discovery protocol control
    kGVCPDiscoveryCmd                           = 0x0002,
    kGVCPDiscoveryAck                           = 0x0003,
    kGVCPForceIPCmd                             = 0x0004,
    kGVCPForceIPAck                             = 0x0005,
    
    // Streaming protocol control
    kGVCPPacketResendCmd                        = 0x0040,
    kGVCPPacketResendAck                        = 0x0041,
    
    // Device memory control
    kGVCPReadRegisterCmd                        = 0x0080,
    kGVCPReadRegisterAck                        = 0x0081,
    kGVCPWriteRegisterCmd                       = 0x0082,
    kGVCPWriteRegisterAck                       = 0x0083,
    kGVCPReadMemoryCmd                          = 0x0084,
    kGVCPReadMemoryAck                          = 0x0085,
    kGVCPWriteMemoryCmd                         = 0x0086,
    kGVCPWriteMemoryAck                         = 0x0087,
    kGVCPPendingAck                             = 0x0089,
    
    // Asynchronous events
    kGVCPEventCmd                               = 0x00C0,
    kGVCPEventAck                               = 0x00C1,
    kGVCPEventDataCmd                           = 0x00C2,
    kGVCPEventDataAck                           = 0x00C3,
    
    // Miscellaneous
    kGVCPActionCmd                              = 0x0100,
    kGVCPActionAck                              = 0x0101
};

typedef NS_ENUM(uint16_t, CZGVCPEventIdentifier) {
    kGVCPAcquisitionStopEvent                   = 0x9000,
    kGVCPAcquisitionStartEvent                  = 0x9001,
    kGVCPCameraControlLogoutEvent               = 0x9100,
    kGVCPCameraControlLoginEvent                = 0x9101,
};

typedef NS_ENUM(uint32_t, CZGEVRegister) {
    kGEVRegisterVersion                         = 0x0000,
    kGEVRegisterDeviceMode                      = 0x0004,
    kGEVRegisterMACAddressHigh                  = 0x0008,
    kGEVRegisterMACAddressLow                   = 0x000C,
    kGEVRegisterCurrentIPAddress                = 0x0024,
    kGEVRegisterCurrentSubnetMask               = 0x0034,
    kGEVRegisterCurrentDefaultGateway           = 0x0044,
    kGEVRegisterManufacturerName                = 0x0048,
    kGEVRegisterModelName                       = 0x0068,
    kGEVRegisterDeviceVersion                   = 0x0088,
    kGEVRegisterManufacturerInfo                = 0x00A8,
    kGEVRegisterSerialNumber                    = 0x00D8,
    kGEVRegisterUserDefinedName                 = 0x00E8,
    kGEVRegisterFirstURL                        = 0x0200,
    kGEVRegisterSecondURL                       = 0x0400,
    kGEVRegisterNumberOfNetworkInterfaces       = 0x0600,
    kGEVRegisterPersistentIPAddress             = 0x064C,
    kGEVRegisterPersistentSubnetMask            = 0x065C,
    kGEVRegisterPersistentDefaultGateway        = 0x066C,
    kGEVRegisterLinkSpeed                       = 0x0670,
    kGEVRegisterNumberOfMessageChannels         = 0x0900,
    kGEVRegisterNumberOfStreamChannels          = 0x0904,
    kGEVRegisterNumberOfActionSignals           = 0x0908,
    kGEVRegisterActionDeviceKey                 = 0x090C,
    kGEVRegisterNumberOfActiveLinks             = 0x0910,
    kGEVRegisterGVSPCapability                  = 0x092C,
    kGEVRegisterMessageChannelCapability        = 0x0930,
    kGEVRegisterGVCPCapability                  = 0x0934,
    kGEVRegisterHeartbeatTimeout                = 0x0938,
    kGEVRegisterTimestampTickFrequencyHigh      = 0x093C,
    kGEVRegisterTimestampTickFrequencyLow       = 0x0940,
    kGEVRegisterTimestampControl                = 0x0944,
    kGEVRegisterTimestampValueHigh              = 0x0948,
    kGEVRegisterTimestampValueLow               = 0x094C,
    kGEVRegisterDiscoveryACKDelay               = 0x0950,
    kGEVRegisterGVCPConfiguration               = 0x0954,
    kGEVRegisterPendingTimeout                  = 0x0958,
    kGEVRegisterControlSwitchoverKey            = 0x095C,
    kGEVRegisterGVSPConfiguration               = 0x0960,
    kGEVRegisterPhysicalLinkConfigCapability    = 0x0964,
    kGEVRegisterPhysicalLinkConfiguration       = 0x0968,
    kGEVRegisterIEEE1588Status                  = 0x096C,
    kGEVRegisterScheduledActionCommandQueueSize = 0x0970,
    kGEVRegisterCCP                             = 0x0A00,
    kGEVRegisterPrimaryApplicationPort          = 0x0A04,
    kGEVRegisterPrimaryApplicationIPAddress     = 0x0A14,
    kGEVRegisterMCP                             = 0x0B00,
    kGEVRegisterMCDA                            = 0x0B10,
    kGEVRegisterMCTT                            = 0x0B14,
    kGEVRegisterMCRC                            = 0x0B18,
    kGEVRegisterMCSP                            = 0x0B1C,
    kGEVRegisterSCP0                            = 0x0D00,
    kGEVRegisterSCPS0                           = 0x0D04,
    kGEVRegisterSCPD0                           = 0x0D08,
    kGEVRegisterSCDA0                           = 0x0D18,
    kGEVRegisterSCSP0                           = 0x0D1C,
    kGEVRegisterSCC0                            = 0x0D20,
    kGEVRegisterSCCFG0                          = 0x0D24,
    kGEVRegisterSCZ0                            = 0x0D28,
    kGEVRegisterSCZD0                           = 0x0D2C,
    
    kGEVRegisterExposureTime                    = 0x0A030000,
    kGEVRegisterSoftTrigger                     = 0x0A070000,
    kGEVRegisterBinning                         = 0x0A0B0000,
    kGEVRegisterROIWidth                        = 0x0A150000,
    kGEVRegisterROIOffsetX                      = 0x0A150004,
    kGEVRegisterROIOffsetY                      = 0x0A150008,
    kGEVRegisterGain                            = 0x14010000,
    kGEVRegisterColorBalanceRed                 = 0x15010000,
    kGEVRegisterColorBalanceGreen               = 0x15010004,
    kGEVRegisterColorBalanceBlue                = 0x15010008,
    kGEVRegisterLightSource                     = 0x15020000,
    kGEVRegisterColorSaturation                 = 0x15040000,
    kGEVRegisterColorTemperatureRG              = 0x15080000,
    kGEVRegisterColorTemperatureBG              = 0x15080004,
    kGEVRegisterVerticalFlip                    = 0x16020000,
    kGEVRegisterHorizontalFlip                  = 0x16030000,
    kGEVRegisterBrightness                      = 0x18010000,
    kGEVRegisterContrast                        = 0x18020000,
    kGEVRegisterSharpness                       = 0x18030000,
    kGEVRegisterImageCompressionQuality         = 0x1A040000,
    kGEVRegisterVideoOutput                     = 0x28070000,
    kGEVRegisterAspectRatio                     = 0x280F0000,
    kGEVRegisterStreamProtocol0                 = 0x42010000,
    kGEVRegisterImageResolutionWidth            = 0x420F0008,
    kGEVRegisterImageResolutionHeight           = 0x420F000C,
    kGEVRegisterAcquisitionState                = 0x42100000,
    kGEVRegisterImageCompressionMode0           = 0x42120000,
    kGEVRegisterEncoderBitRate                  = 0x42150004,
    kGEVRegisterEncoderReduceFPS                = 0x42150008,
    kGEVRegisterAutomaticExposureTime           = 0x46010000,
    kGEVRegisterAutomaticGainControl            = 0x46020000,
    kGEVRegisterAutomaticLevel                  = 0x46030000,
    kGEVRegisterAutomaticWhiteSet               = 0x46040000,
    kGEVRegisterLoadSettings                    = 0x51020000,
    kGEVRegisterSaveSettings                    = 0x51030000,
    kGEVRegisterStartupSettings                 = 0x51040000
} ;

typedef NS_ENUM(NSUInteger, CZGVSPPacketFormat) {
    kGVSPPacketFormatLeader                     = 1,
    kGVSPPacketFormatTrailer                    = 2,
    kGVSPPacketFormatGenericPayload             = 3,
    kGVSPPacketFormatAllIn                      = 4,
    kGVSPPacketFormatH264Payload                = 5,
    kGVSPPacketFormatMultiZonePayload           = 6
};

typedef NS_ENUM (NSUInteger, CZGVSPPayloadType) {
    kGVSPPayloadTypeImage                       = 0x0001,
    kGVSPPayloadTypeRawData                     = 0x0002,
    kGVSPPayloadTypeFile                        = 0x0003,
    kGVSPPayloadTypeChunkData                   = 0x0004,
    kGVSPPayloadTypeJPEG                        = 0x0006,
    kGVSPPayloadTypeJPEG2000                    = 0x0007,
    kGVSPPayloadTypeH264                        = 0x0008,
    kGVSPPayloadTypeMultiZoneImage              = 0x0009
};

#endif // CZGigEVisionDefs_H
