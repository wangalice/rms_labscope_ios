//
//  CZGVCPCommand.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommandHeader.h"

@protocol CZGVCPCommand <NSObject>

@property (nonatomic, retain) CZGVCPCommandHeader *header;

- (NSData *)newDataFromCommand;

@end
