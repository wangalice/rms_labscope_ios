//
//  CZGVCPReadRegisterCmd.m
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPReadRegisterCmd.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPReadRegisterCmd

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithRequestId:(uint16_t)requestId {
    self = [super init];
    if (self) {
        header = [[CZGVCPCommandHeader alloc] init];
        [header setFlag:0x01];
        [header setCommand:kGVCPReadRegisterCmd];
        [header setLength:4];
        [header setRequestId:requestId];
    }
    return self;
}

- (void)dealloc {
    [header release];
    [super dealloc];
}

- (NSData *)newDataFromCommand {
    void *buffer = malloc(header.length);
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:buffer length:header.length];
    
    [writer writeLongInNetworkOrder:_registerAddress];
    
    NSData *headerData = [header newDataFromCommandHeader];
    NSMutableData *tempData = [headerData mutableCopy];
    [tempData appendBytes:buffer length:header.length];
    
    NSData *data = [[NSData alloc] initWithData:tempData];
    
    [headerData release];
    [tempData release];
    [writer release];
    free(buffer);
    
    return data;
}

@end
