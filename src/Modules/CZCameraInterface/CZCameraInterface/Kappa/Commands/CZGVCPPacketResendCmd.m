//
//  CZGVCPPacketResendCmd.m
//  Hermes
//
//  Created by Halley Gu on 7/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPPacketResendCmd.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPPacketResendCmd

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithRequestId:(uint16_t)requestId {
    self = [super init];
    if (self) {
        header = [[CZGVCPCommandHeader alloc] init];
        [header setFlag:0x11];
        [header setCommand:kGVCPPacketResendCmd];
        [header setLength:20];
        [header setRequestId:requestId];
    }
    return self;
}

- (void)dealloc {
    [header release];
    [super dealloc];
}

- (NSData *)newDataFromCommand {
    void *buffer = malloc(header.length);
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:buffer length:header.length];
    
    [writer writeLongInNetworkOrder:0];
    [writer writeLongInNetworkOrder:_firstPacketId];
    [writer writeLongInNetworkOrder:_lastPacketId];
    
    uint32_t blockIdHigh = _blockId >> 32 & 0xFFFFFFFF;
    uint32_t blockIdLow = _blockId & 0xFFFFFFFF;
    
    [writer writeLongInNetworkOrder:blockIdHigh];
    [writer writeLongInNetworkOrder:blockIdLow];
    
    NSData *headerData = [header newDataFromCommandHeader];
    NSMutableData *tempData = [headerData mutableCopy];
    [tempData appendBytes:buffer length:header.length];
    
    NSData *data = [[NSData alloc] initWithData:tempData];
    
    [headerData release];
    [tempData release];
    [writer release];
    free(buffer);
    
    return data;
}

@end
