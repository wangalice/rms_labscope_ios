//
//  CZGVCPCommandHeader.m
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPCommandHeader.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPCommandHeader

- (id)init {
    self = [super init];
    if (self) {
        _version = 0x42;
    }
    return self;
}

- (id)initWithData:(NSData *)data {
    if ([data length] < kGVCPCommandHeaderLength) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                       length:data.length];
        
        _version = [reader readByte];
        _flag = [reader readByte];
        _command = [reader readShortInHostOrder];
        _length = [reader readShortInHostOrder];
        _requestId = [reader readShortInHostOrder];
        
        [reader release];
    }
    return self;
}

- (NSData *)newDataFromCommandHeader {
    void *buffer = malloc(kGVCPCommandHeaderLength);
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:buffer
                                                                   length:kGVCPCommandHeaderLength];
    
    [writer writeByte:_version];
    [writer writeByte:_flag];
    [writer writeShortInNetworkOrder:(uint16_t)_command];
    [writer writeShortInNetworkOrder:_length];
    [writer writeShortInNetworkOrder:_requestId];
    
    NSData *commandHeader = [[NSData alloc] initWithBytes:buffer
                                                   length:kGVCPCommandHeaderLength];
    
    [writer release];
    free(buffer);
    
    return commandHeader;
}

@end
