//
//  CZGVCPEventCmd.m
//  Hermes
//
//  Created by Halley Gu on 8/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPEventCmd.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPEventCmd

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (void)dealloc {
    [header release];
    [super dealloc];
}

- (id)initWithData:(NSData *)data {
    if ([data length] <= 0) {
        [self release];
        return nil;
    }
    
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    [reader seekToOffset:kGVCPHeaderTypeOffset];
    
    uint16_t type = [reader readShortInHostOrder];
    if (type != kGVCPEventCmd) {
        [reader release];
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        header = [[CZGVCPCommandHeader alloc] initWithData:data];
        
        [reader seekToOffset:kGVCPCommandHeaderLength];
        [reader jumpByLength:2];
        
        _eventIdentifier = [reader readShortInHostOrder];
    }
    
    [reader release];
    return self;
}

- (NSData *)newDataFromCommand {
    return nil;
}

@end
