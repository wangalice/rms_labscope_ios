//
//  CZGVCPPacketResendCmd.h
//  Hermes
//
//  Created by Halley Gu on 7/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPPacketResendCmd : NSObject<CZGVCPCommand>

@property (nonatomic, assign) uint64_t blockId;
@property (nonatomic, assign) uint32_t firstPacketId;
@property (nonatomic, assign) uint32_t lastPacketId;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRequestId:(uint16_t)requestId NS_DESIGNATED_INITIALIZER;

@end
