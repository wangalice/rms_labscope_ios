//
//  CZGVCPWriteMemoryCmd.h
//  Hermes
//
//  Created by Halley Gu on 7/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPWriteMemoryCmd : NSObject<CZGVCPCommand>

@property (nonatomic, assign) CZGEVRegister registerAddress;
@property (nonatomic, copy) NSString *registerData;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRequestId:(uint16_t)requestId NS_DESIGNATED_INITIALIZER;

@end
