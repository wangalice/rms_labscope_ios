//
//  CZGVCPCommandHeader.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGEVCommonDefs.h"

@interface CZGVCPCommandHeader : NSObject

@property (nonatomic, assign, readonly) uint8_t version;
@property (nonatomic, assign) uint8_t flag;
@property (nonatomic, assign) CZGVCPMessageType command;
@property (nonatomic, assign) uint16_t length;
@property (nonatomic, assign) uint16_t requestId;

- (id)initWithData:(NSData *)data;

- (NSData *)newDataFromCommandHeader;

@end
