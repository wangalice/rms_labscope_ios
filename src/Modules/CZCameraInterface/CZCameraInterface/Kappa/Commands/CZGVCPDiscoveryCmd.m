//
//  CZGVCPDiscoveryCmd.m
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPDiscoveryCmd.h"
#import "CZGVCPCommandHeader.h"

@implementation CZGVCPDiscoveryCmd

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithRequestId:(uint16_t)requestId {
    self = [super init];
    if (self) {
        header = [[CZGVCPCommandHeader alloc] init];
        [header setFlag:0x11];
        [header setCommand:kGVCPDiscoveryCmd];
        [header setLength:0];
        [header setRequestId:requestId];
    }
    return self;
}

- (void)dealloc {
    [header release];
    [super dealloc];
}

- (NSData *)newDataFromCommand {
    return [header newDataFromCommandHeader];
}

@end
