//
//  CZGVCPWriteMemoryCmd.m
//  Hermes
//
//  Created by Halley Gu on 7/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPWriteMemoryCmd.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPWriteMemoryCmd

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithRequestId:(uint16_t)requestId {
    self = [super init];
    if (self) {
        header = [[CZGVCPCommandHeader alloc] init];
        [header setFlag:0x01];
        [header setCommand:kGVCPWriteMemoryCmd];
        [header setLength:0];
        [header setRequestId:requestId];
    }
    return self;
}

- (void)dealloc {
    [_registerData release];
    [header release];
    [super dealloc];
}

- (NSData *)newDataFromCommand {
    NSUInteger dataLength = _registerData.length;
    NSUInteger zeroLength = (4 - dataLength % 4) % 4;
    
    // Speical fix for user defined name.
    if (_registerAddress == kGEVRegisterUserDefinedName) {
        zeroLength = 16 - dataLength;
    }
    
    [header setLength:4 + dataLength + zeroLength];
    
    const char *bytes = [_registerData UTF8String];
    
    void *buffer = malloc(header.length);
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:buffer length:header.length];
    
    [writer writeLongInNetworkOrder:_registerAddress];
    
    for (NSUInteger i = 0; i < dataLength; ++i) {
        [writer writeByte:bytes[i]];
    }
    for (NSUInteger i = 0; i < zeroLength; ++i) {
        [writer writeByte:0];
    }
    
    NSData *headerData = [header newDataFromCommandHeader];
    NSMutableData *tempData = [headerData mutableCopy];
    [tempData appendBytes:buffer length:header.length];
    
    NSData *data = [[NSData alloc] initWithData:tempData];
    
    [headerData release];
    [tempData release];
    [writer release];
    free(buffer);
    
    return data;
}

@end
