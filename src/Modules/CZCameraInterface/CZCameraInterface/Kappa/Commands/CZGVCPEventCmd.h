//
//  CZGVCPEventCmd.h
//  Hermes
//
//  Created by Halley Gu on 8/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPEventCmd : NSObject<CZGVCPCommand>

@property (nonatomic, assign) uint16_t eventIdentifier;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

@end
