//
//  CZGVCPWriteRegisterCmd.h
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPWriteRegisterCmd : NSObject<CZGVCPCommand>

@property (nonatomic, assign) CZGEVRegister registerAddress;
@property (nonatomic, assign) uint32_t registerData;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRequestId:(uint16_t)requestId NS_DESIGNATED_INITIALIZER;

@end
