//
//  CZGVCPForceIPCmd.h
//  Hermes
//
//  Created by Halley Gu on 9/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPForceIPCmd : NSObject<CZGVCPCommand>

@property (nonatomic, assign) uint16_t macAddressHigh;
@property (nonatomic, assign) uint32_t macAddressLow;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRequestId:(uint16_t)requestId NS_DESIGNATED_INITIALIZER;

@end
