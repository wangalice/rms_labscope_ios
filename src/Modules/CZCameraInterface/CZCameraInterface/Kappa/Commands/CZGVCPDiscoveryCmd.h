//
//  CZGVCPDiscoveryCmd.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPCommand.h"

@interface CZGVCPDiscoveryCmd : NSObject<CZGVCPCommand>

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRequestId:(uint16_t)requestId NS_DESIGNATED_INITIALIZER;

@end
