//
//  CZGEVClient.mm
//  Hermes
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGEVClient.h"
#import "CZGEVCommonDefs.h"
#import "CZGVCPDiscoveryCmd.h"
#import "CZGVCPDiscoveryAck.h"
#import "CZGVCPReadRegisterCmd.h"
#import "CZGVCPReadRegisterAck.h"
#import "CZGVCPWriteRegisterCmd.h"
#import "CZGVCPWriteRegisterAck.h"
#import "CZGVCPWriteMemoryCmd.h"
#import "CZGVCPWriteMemoryAck.h"
#import "CZGVCPPacketResendCmd.h"
#import "CZGVCPEventCmd.h"
#import "CZGVCPForceIPCmd.h"
#import "CZGVSPDataBlock.h"
#import "CZGVSPImagePayload.h"
#import <CZToolbox/CZToolbox.h>

static const uint16_t kGVCPPort = 3956;
static const NSTimeInterval kGVCPDefaultTimeout = 10;
static const NSUInteger kGVCPRetryTimes = 5;

@interface CZGEVClient () {
    dispatch_queue_t _gevControlQueue;
    dispatch_queue_t _gevDelegateQueue;
    dispatch_queue_t _gevHeartbeatQueue;
    dispatch_group_t _gevHeartbeatGroup;
    dispatch_semaphore_t _gevSnapshotSemaphore;
    uint16_t _requestId;
    NSObject *_connectionLock;
    NSObject *_discoverySocketLock;
    NSObject *_controlSocketLock;
    NSObject *_eventsSocketLock;
    NSObject *_readingHistoryLock;
    NSObject *_writingHistoryLock;
}

@property (atomic, retain) GCDAsyncUdpSocket *discoverySocket;
@property (atomic, retain) GCDAsyncUdpSocket *controlSocket;
@property (atomic, retain) GCDAsyncUdpSocket *snapshotSocket;
@property (atomic, retain) GCDAsyncUdpSocket *eventsSocket;
@property (atomic, retain) NSMutableDictionary *readingHistory;
@property (atomic, retain) NSMutableDictionary *writingHistory;
@property (atomic, assign) BOOL isHeartbeatEnabled;
@property (atomic, retain) CZGVSPImagePayload *imagePayload;
@property (atomic, retain) NSTimer *packetCheckingTimer;
@property (atomic, retain) NSDate *lastPacketTime;
@property (atomic, assign) NSUInteger packetRetryCount;
@property (atomic, assign) BOOL needsSaving;

- (void)handlePacketCheckingTimer:(NSTimer *)timer;
- (void)heartbeat;
- (BOOL)prepareDiscoverySocket;
- (uint16_t)nextRequestId;

+ (NSString *)ipStringFromInteger:(uint32_t)address;

@end

@implementation CZGEVClient

- (id)init {
    self = [super init];
    if (self) {
        _requestId = 0;
        
        _readingHistory = [[NSMutableDictionary alloc] init];
        _writingHistory = [[NSMutableDictionary alloc] init];
        _readingHistoryLock = [[NSObject alloc] init];
        _writingHistoryLock = [[NSObject alloc] init];
        
        _connectionLock = [[NSObject alloc] init];
        _discoverySocketLock = [[NSObject alloc] init];
        _controlSocketLock = [[NSObject alloc] init];
        _eventsSocketLock = [[NSObject alloc] init];
        
        _gevControlQueue = dispatch_queue_create("com.zeisscn.gigevision", NULL);
        _gevDelegateQueue = dispatch_queue_create("com.zeisscn.gigevision.delegate", NULL);
        _gevHeartbeatQueue = dispatch_queue_create("com.zeisscn.gigevision.heartbeat", NULL);
        _gevHeartbeatGroup = dispatch_group_create();
        
        _isHeartbeatEnabled = NO;
        _needsSaving = NO;
        
        _packetRetryCount = 0;
    }
    return self;
}

- (void)dealloc {
    [self disconnect];
    
    [_ipAddress release];
    [_macAddress release];
    [_readingHistory release];
    [_writingHistory release];
    [_readingHistoryLock release];
    [_writingHistoryLock release];
    [_discoverySocketLock release];
    [_connectionLock release];
    [_controlSocketLock release];
    [_eventsSocketLock release];
    [_imagePayload release];
    [_packetCheckingTimer release];
    [_lastPacketTime release];
    
    _discoverySocket.delegate = nil;
    [_discoverySocket close];
    [_discoverySocket release];
    
    _controlSocket.delegate = nil;
    [_controlSocket close];
    [_controlSocket release];
    
    _snapshotSocket.delegate = nil;
    [_snapshotSocket close];
    [_snapshotSocket release];
    
    _eventsSocket.delegate = nil;
    [_eventsSocket close];
    [_eventsSocket release];
    
    dispatch_release(_gevControlQueue);
    dispatch_release(_gevDelegateQueue);
    dispatch_release(_gevHeartbeatQueue);
    dispatch_release(_gevHeartbeatGroup);
    
    if (_gevSnapshotSemaphore != NULL) {
        dispatch_release(_gevSnapshotSemaphore);
    }
    
    [super dealloc];
}

- (void)beginDiscovery {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return;
    }
    
    if ([self prepareDiscoverySocket]) {
        CZGVCPDiscoveryCmd *command = [[CZGVCPDiscoveryCmd alloc] initWithRequestId:0];
        NSData *commandData = [command newDataFromCommand];
        
        [self.discoverySocket sendData:commandData
                                toHost:@"255.255.255.255" // Broadcast
                                  port:kGVCPPort
                           withTimeout:kGVCPDefaultTimeout
                                   tag:[[command header] requestId]];
        
        [commandData release];
        [command release];
    }
}

- (BOOL)connect {
    @synchronized (_connectionLock) {
        if (self.controlSocket == nil) {
            do {
                if (![[CZWifiNotifier sharedInstance] isReachable]) {
                    break;
                }
                
                if (self.ipAddress == nil || self.ipAddress.length == 0) {
                    break;
                }
                
                @synchronized (_controlSocketLock) {
                    NSError *error = nil;
                    _controlSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                   delegateQueue:_gevDelegateQueue];
                    [_controlSocket setIPv4Enabled:YES];
                    [_controlSocket setIPv6Enabled:YES];
                    
                    if (![_controlSocket bindToPort:0 error:&error]) {
                        CZLogv(@"GVCP: failed to bind to local port.");
                        break;
                    }
                    
                    if (![_controlSocket connectToHost:self.ipAddress onPort:kGVCPPort error:&error]) {
                        CZLogv(@"GVCP: failed to connect to the GVCP port of camera.");
                        break;
                    }
                    
                    if (![_controlSocket beginReceiving:&error]) {
                        CZLogv(@"GVCP: failed to begin receiving data from socket.");
                        break;
                    }
                }
                
                // Get the control privilege from camera.
                BOOL result = [self writeRegister:kGEVRegisterCCP withData:0x0002];
                if (!result) {
                    break;
                }
                
                // Dispatch to another thread to avoid UI blocking.
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
                    // Get the heartbeat timeout from camera.
                    uint32_t heartbeatTimeout = [self readRegister:kGEVRegisterHeartbeatTimeout];
                    
                    // Start the heartbeat thread.
                    if (!self.isHeartbeatEnabled) {
                        self.isHeartbeatEnabled = YES;
                        uint32_t heartbeatSeconds = (uint32_t)round(heartbeatTimeout / 2000.0);
                        if (heartbeatSeconds < 1) {
                            heartbeatSeconds = 1;
                        }
                        dispatch_group_async(_gevHeartbeatGroup, _gevHeartbeatQueue, ^() {
                            CZLogv(@"Heartbeat thread started for %@", self.ipAddress);
                            while (self.isHeartbeatEnabled) {
                                sleep(heartbeatSeconds);
                                [self heartbeat];
                            }
                            CZLogv(@"Heartbeat thread stopped for %@", self.ipAddress);
                        });
                    }
                });
                
                return YES;
            } while (0);
            
            // Close and release the created socket if error occurred.
            [self.controlSocket close];
            self.controlSocket = nil;
        }
        
        return NO;
    }
}

- (void)disconnect {
    @synchronized (_connectionLock) {
        if (self.controlSocket == nil) {  // avoid re-enter
            return;
        }
        
        if ([[CZWifiNotifier sharedInstance] isReachable]) {
            if (self.needsSaving) {
                // Let camera save the current settings to user setting slot 1.
                [self writeRegister:kGEVRegisterSaveSettings withData:1];
            }
            
            // Write CCP to zero to abandon control privilege.
            [self writeRegister:kGEVRegisterCCP withData:0x0000];
        }
        
        self.needsSaving = NO;
        
        @synchronized (_controlSocketLock) {
            [self.controlSocket close];
            self.controlSocket = nil;
        }
        
        self.isHeartbeatEnabled = NO;
        dispatch_group_wait(_gevHeartbeatGroup, DISPATCH_TIME_FOREVER);
        
        @synchronized (_readingHistoryLock) {
            [_readingHistory removeAllObjects];
        }
        @synchronized (_writingHistoryLock) {
            [_writingHistory removeAllObjects];
        }
        
        _requestId = 0;
    }
}

- (void)disconnectAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self disconnect];
    });
}

- (uint32_t)readRegister:(CZGEVRegister)registerAddress {
    __block uint32_t result = 0;
    __block BOOL successful = NO;
    
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return result;
    }
    
    for (NSUInteger attempt = 1; attempt <= kGVCPRetryTimes && !successful; ++attempt) {
        if (attempt > 1) {
            CZLogv(@"GVCP: readreg was failed, attempt #%lu", (unsigned long)attempt);
        }
        
        dispatch_sync(_gevControlQueue, ^(){
            CZGVCPReadRegisterCmd *command = [[CZGVCPReadRegisterCmd alloc] initWithRequestId:[self nextRequestId]];
            command.registerAddress = registerAddress;
            
            NSData *commandData = [command newDataFromCommand];
            BOOL isDataSent = NO;
            
            @synchronized (_controlSocketLock) {
                if (self.controlSocket != nil) {
                    [self.controlSocket sendData:commandData
                                     withTimeout:kGVCPDefaultTimeout
                                             tag:[[command header] requestId]];
                    isDataSent = YES;
                    
                    CZLogv(@"GVCP Cmd #%u (readreg): %lx", [[command header] requestId], (unsigned long)registerAddress);
                }
            }
            
            [commandData release];
            
            if (isDataSent) {
                dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                NSMutableArray *pair = [[NSMutableArray alloc] initWithObjects:
                                        semaphore,
                                        [NSNumber numberWithUnsignedLong:result],
                                        nil];
                NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:command.header.requestId];
                
                @synchronized (_readingHistoryLock) {
                    [_readingHistory setObject:pair forKey:key];
                }
                
                // Wait for the callback to store returned data and then give signal.
                long signal = dispatch_semaphore_wait((dispatch_semaphore_t)pair[0],
                                                      dispatch_time(DISPATCH_TIME_NOW, 5e+9)); // Wait for 5 secs
                
                @synchronized (_readingHistoryLock) {
                    result = [pair[1] unsignedIntValue];
                    CZLogv(@"GVCP Ack #%u (readreg): data = %u", command.header.requestId, result);
                    
                    if (signal != 0 && pair[0] == semaphore) {
                        // Timeout while getting the sempahore signal.
                        // Manually give a signal to reset the semaphore before releasing.
                        dispatch_semaphore_signal(semaphore);
                    } else {
                        successful = YES;
                    }
                    
                    dispatch_release(semaphore);
                    [_readingHistory removeObjectForKey:key];
                }
                
                [key release];
                [pair release];
            }
            
            [command release];
        });
    }
    
    return result;
}

- (BOOL)writeRegister:(CZGEVRegister)registerAddress withData:(uint32_t)data {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return NO;
    }
    
    if (registerAddress != kGEVRegisterCCP) {
        self.needsSaving = YES;
    }
    
    __block BOOL result = NO;
    
    for (NSUInteger attempt = 1; attempt <= kGVCPRetryTimes && !result; ++attempt) {
        if (attempt > 1) {
            usleep(1e+5); // Sleep for 0.1 second before retry
            CZLogv(@"GVCP: writereg was failed, attempt #%lu", (unsigned long)attempt);
        }
        
        dispatch_sync(_gevControlQueue, ^(){
            CZGVCPWriteRegisterCmd *command = [[CZGVCPWriteRegisterCmd alloc] initWithRequestId:[self nextRequestId]];
            command.registerAddress = registerAddress;
            command.registerData = data;
            
            NSData *commandData = [command newDataFromCommand];
            BOOL isDataSent = NO;
            
            @synchronized (_controlSocketLock) {
                if (self.controlSocket != nil) {
                    [self.controlSocket sendData:commandData
                                     withTimeout:kGVCPDefaultTimeout
                                             tag:[[command header] requestId]];
                    isDataSent = YES;
                    
                    CZLogv(@"GVCP Cmd #%u (writereg): %lx = %u",
                          [[command header] requestId],
                          (long)command.registerAddress,
                          command.registerData);
                }
            }
            
            [commandData release];
            
            if (isDataSent) {
                dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                NSMutableArray *pair = [[NSMutableArray alloc] initWithObjects:
                                        semaphore,
                                        [NSNumber numberWithBool:result],
                                        nil];
                NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:command.header.requestId];
                
                @synchronized (_writingHistoryLock) {
                    [_writingHistory setObject:pair forKey:key];
                }
                
                // Wait for the callback to store returned data and then give signal.
                long signal = dispatch_semaphore_wait((dispatch_semaphore_t)pair[0],
                                                      dispatch_time(DISPATCH_TIME_NOW, 5e+9)); // Wait for 5 secs
                
                @synchronized (_writingHistoryLock) {
                    result = [pair[1] boolValue];
                    CZLogv(@"GVCP Ack #%u (writereg): %@", command.header.requestId, result ? @"done" : @"failed");
                    
                    if (signal != 0 && pair[0] == semaphore) {
                        // Timeout while getting the sempahore signal.
                        // Manually give a signal to reset the semaphore before releasing.
                        dispatch_semaphore_signal(semaphore);
                    }
                    
                    dispatch_release(semaphore);
                    [_writingHistory removeObjectForKey:key];
                }
                
                [key release];
                [pair release];
            }
            
            [command release];
        });
    }
    
    return result;
}

- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withData:(uint32_t)data {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self writeRegister:registerAddress withData:data];
    });
}

- (BOOL)writeRegister:(CZGEVRegister)registerAddress withString:(NSString *)data {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return NO;
    }
    
    if (registerAddress != kGEVRegisterCCP) {
        self.needsSaving = YES;
    }
    
    __block BOOL result = NO;
    
    for (NSUInteger attempt = 1; attempt <= kGVCPRetryTimes && !result; ++attempt) {
        if (attempt > 1) {
            usleep(1e+5); // Sleep for 0.1 second before retry
            CZLogv(@"GVCP: writereg was failed, attempt #%lu", (unsigned long)attempt);
        }
        
        dispatch_sync(_gevControlQueue, ^(){
            CZGVCPWriteMemoryCmd *command = [[CZGVCPWriteMemoryCmd alloc] initWithRequestId:[self nextRequestId]];
            command.registerAddress = registerAddress;
            command.registerData = data;
            
            NSData *commandData = [command newDataFromCommand];
            BOOL isDataSent = NO;
            
            @synchronized (_controlSocketLock) {
                if (self.controlSocket != nil) {
                    [self.controlSocket sendData:commandData
                                     withTimeout:kGVCPDefaultTimeout
                                             tag:[[command header] requestId]];
                    isDataSent = YES;
                    
                    CZLogv(@"GVCP Cmd #%u (writemem): %lx = %@",
                          [[command header] requestId],
                          (long)command.registerAddress,
                          command.registerData);
                }
            }
            
            [commandData release];
            
            if (isDataSent) {
                dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
                NSMutableArray *pair = [[NSMutableArray alloc] initWithObjects:
                                        semaphore,
                                        [NSNumber numberWithBool:result],
                                        nil];
                NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:command.header.requestId];
                
                @synchronized (_writingHistoryLock) {
                    [_writingHistory setObject:pair forKey:key];
                }
                
                // Wait for the callback to store returned data and then give signal.
                long signal = dispatch_semaphore_wait((dispatch_semaphore_t)pair[0],
                                                      dispatch_time(DISPATCH_TIME_NOW, 5e+9)); // Wait for 5 secs
                
                @synchronized (_writingHistoryLock) {
                    result = [pair[1] boolValue];
                    CZLogv(@"GVCP Ack #%u (writemem): %@", command.header.requestId, result ? @"done" : @"failed");
                    
                    if (signal != 0 && pair[0] == semaphore) {
                        // Timeout while getting the sempahore signal.
                        // Manually give a signal to reset the semaphore before releasing.
                        dispatch_semaphore_signal(semaphore);
                    }
                    
                    dispatch_release(semaphore);
                    [_writingHistory removeObjectForKey:key];
                }
                
                [key release];
                [pair release];
            }
            
            [command release];
        });
    }
    
    return result;
}

- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withString:(NSString *)data {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self writeRegister:registerAddress withString:data];
    });
}

- (NSArray *)takeSnapshotWithResolution:(CZCameraSnapResolutionPreset)resolution
                            andExposure:(float *)exposureTime
                             frameCount:(NSUInteger)frameCount{
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return nil;
    }
    
    if (self.controlSocket == nil) {
        return nil;
    }
    
    NSMutableArray *imageArray = [NSMutableArray array];

    if (_gevSnapshotSemaphore == NULL) {
        _gevSnapshotSemaphore = dispatch_semaphore_create(0);
        
        //uint32_t imageCompression = [self readRegister:kGEVRegisterImageCompressionMode0];
        uint32_t streamProtocol = [self readRegister:kGEVRegisterStreamProtocol0];
        uint32_t videoOutput = [self readRegister:kGEVRegisterVideoOutput];
        
        uint32_t newVideoOutput = 0; // Full-sensor
        if (resolution == kCZCameraSnapResolutionLow) {
            newVideoOutput = 16; // 720p
        }
        
        uint32_t originalExposure = [self readRegister:kGEVRegisterExposureTime];
        uint32_t exposureMode = [self readRegister:kGEVRegisterAutomaticExposureTime];
        uint32_t gainMode = [self readRegister:kGEVRegisterAutomaticGainControl];
        float targetExposure = *exposureTime;
        
        [self writeRegister:kGEVRegisterAcquisitionState withData:0];       // Off
        
        if (targetExposure > 0) {
            CZLogv(@"Set exposure time to %f ms for snapping.", targetExposure);
            
            [self writeRegister:kGEVRegisterAutomaticExposureTime withData:0];
            [self writeRegister:kGEVRegisterAutomaticGainControl withData:0];
            
            uint32_t value = (uint32_t)round(targetExposure * 1000);
            [self writeRegister:kGEVRegisterExposureTime withData:value];
        }
        
        uint32_t actualExposure = [self readRegister:kGEVRegisterExposureTime];
        *exposureTime = (float)actualExposure / 1000.0f;
        
        // Switch to still image snapshot mode.
        [self writeRegister:kGEVRegisterImageCompressionMode0 withData:0];  // Uncompressed
        [self writeRegister:kGEVRegisterStreamProtocol0 withData:2];        // GVSP
        if (videoOutput != newVideoOutput) {
            [self writeRegister:kGEVRegisterVideoOutput withData:newVideoOutput];
        }
        [self writeRegister:kGEVRegisterAcquisitionState withData:1];       // On
        
        // Get snapshot data.
        NSError *error = nil;
        dispatch_queue_t snapshotQueue = dispatch_queue_create("com.zeisscn.gigevision.snapshot", NULL);
        
        _snapshotSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                        delegateQueue:snapshotQueue];
        [_snapshotSocket setIPv4Enabled:YES];
        [_snapshotSocket setIPv6Enabled:YES];
        [_snapshotSocket bindToPort:0 error:&error];
        [_snapshotSocket beginReceiving:&error];
        
        uint16_t port = [_snapshotSocket localPort];
        NSString *ipAddress = [CZCommonUtils localIpAddress];
        uint32_t ip = 0;
        NSArray *components = [ipAddress componentsSeparatedByString:@"."];
        if ([components count] == 4) {
            uint8_t *p = (uint8_t *)&ip;
            for (int i = 0; i < 4; ++i) {
                p[3 - i] = [components[i] intValue];
            }
        }
        [self writeRegister:kGEVRegisterSCDA0 withData:ip];
        [self writeRegister:kGEVRegisterSCP0 withData:port];
        
        for (NSUInteger i = 0; i < frameCount; i++) {
            CZGVSPImagePayload *payload = [[CZGVSPImagePayload alloc] init];
            self.imagePayload = payload;
            [payload release];
            
            @synchronized (self) {
                self.lastPacketTime = nil;
                self.packetRetryCount = 0;
                
                dispatch_async(dispatch_get_main_queue(), ^() {
                    [self.packetCheckingTimer invalidate];
                    self.packetCheckingTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                                                target:self
                                                                              selector:@selector(handlePacketCheckingTimer:)
                                                                              userInfo:nil
                                                                               repeats:YES];
                });
            }
        
        
            [self writeRegister:kGEVRegisterSoftTrigger withData:1];
            
            // Wait for the callback to store returned data and then give signal.
            long signal = dispatch_semaphore_wait(_gevSnapshotSemaphore,
                                                  dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 30)); // Wait for 30 secs
            
            [self.packetCheckingTimer invalidate];
            self.packetCheckingTimer = nil;
            
            if (signal != 0) {
                // Timeout while getting the sempahore signal.
                // Manually give a signal to reset the semaphore before releasing.
                dispatch_semaphore_signal(_gevSnapshotSemaphore);
                break;
            } else {
                UIImage *image = self.imagePayload.imageFromPayload;
                if (image) {
                    [imageArray addObject:image];
                } else {
                    break;
                }
                self.imagePayload = nil;
            }
        }
        
        [self writeRegister:kGEVRegisterSCP0 withData:0]; // Remove port setting
        
        // Recover original register values. If old value is not correct, try
        // to recover to RTSP Unicast + 720p mode.
        if (streamProtocol == 2) { // was GVSP
            streamProtocol = 0; // RTSP Unicast
        }
        if (videoOutput == 0) {
            videoOutput = 16; // 720p
        }
        
        [self writeRegister:kGEVRegisterAcquisitionState withData:0];
        [self writeRegister:kGEVRegisterImageCompressionMode0 withData:2]; // Recover to H.264 anyway
        [self writeRegister:kGEVRegisterStreamProtocol0 withData:streamProtocol];
        if (newVideoOutput != videoOutput) {
            [self writeRegister:kGEVRegisterVideoOutput withData:videoOutput];
        }
        [self writeRegister:kGEVRegisterAcquisitionState withData:1];
        
        if (targetExposure > 0) {
            [self writeRegister:kGEVRegisterAutomaticExposureTime withData:exposureMode];
            [self writeRegister:kGEVRegisterAutomaticGainControl withData:gainMode];
            [self writeRegister:kGEVRegisterExposureTime withData:originalExposure];
        }
        
        dispatch_release(_gevSnapshotSemaphore);
        dispatch_release(snapshotQueue);
        
        [_snapshotSocket close];
        [_snapshotSocket release];
        _snapshotSocket = nil;
        
        _gevSnapshotSemaphore = NULL;
        self.imagePayload = nil;
    }
    
    return [NSArray arrayWithArray:imageArray];
}

- (void)beginEventListening {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return;
    }
    
    uint32_t address = [self readRegister:kGEVRegisterMCDA];
    uint32_t port = [self readRegister:kGEVRegisterMCP];
    
    if (address == 0 || port == 0) {
        return;
    }
    
    @synchronized (_eventsSocketLock) {
        if (self.eventsSocket == nil) {
            do {
                NSError *error = nil;
                _eventsSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                              delegateQueue:_gevDelegateQueue];
                [_eventsSocket setIPv4Enabled:YES];
                [_eventsSocket setIPv6Enabled:YES];
                
                if (![_eventsSocket bindToPort:port error:&error]) {
                    CZLogv(@"GVCP: failed to bind to the specific port.");
                    break;
                }
                
                NSString *ipAddress = [CZGEVClient ipStringFromInteger:address];
                if (![_eventsSocket joinMulticastGroup:ipAddress error:&error]) {
                    CZLogv(@"GVCP: failed to join multicast group.");
                    break;
                }
                
                if (![_eventsSocket beginReceiving:&error]) {
                    CZLogv(@"GVCP: failed to begin receiving data from socket.");
                    break;
                }
                
                return;
            } while (0);
            
            // Close and release the created socket if error occurred.
            [self.eventsSocket close];
            self.eventsSocket = nil;
        }
    }
}

- (void)endEventListening {
    @synchronized (_eventsSocketLock) {
        [self.eventsSocket close];
        self.eventsSocket = nil;
    }
}

#pragma mark - Private Methods

- (void)handlePacketCheckingTimer:(NSTimer *)timer {
    @synchronized (self) {
        if (timer != self.packetCheckingTimer) {
            // The timer is obsolete.
            return;
        }
        
        if (self.imagePayload && self.lastPacketTime) {
            // Check whether timeout already happens.
            NSTimeInterval timePassed = [self.lastPacketTime timeIntervalSinceNow];
            
            // No socket came in last half second, and have sent less than 5 bunch of
            // packet resending commands.
            if (timePassed < -0.5 && self.packetRetryCount < 5) {
                self.packetRetryCount += 1;
                
                CZLogv(@"GVCP packetresend: round #%lu", (unsigned long)self.packetRetryCount);
                
                NSArray *ranges = [self.imagePayload rangesForPacketResending];
                for (NSValue *value in ranges) {
                    NSRange range = [value rangeValue];
                    
                    if (self.controlSocket != nil) {
                        CZGVCPPacketResendCmd *command = [[CZGVCPPacketResendCmd alloc] initWithRequestId:[self nextRequestId]];
                        [command setBlockId:self.imagePayload.blockId];
                        [command setFirstPacketId:(uint32_t)range.location];
                        [command setLastPacketId:(uint32_t)(range.location + range.length - 1)];
                        
                        NSData *commandData = [command newDataFromCommand];
                        
                        [self.controlSocket sendData:commandData
                                         withTimeout:kGVCPDefaultTimeout
                                                 tag:[[command header] requestId]];
                        
                        CZLogv(@"GVCP Cmd #%u (packetresend): b %llu, p %u-%u",
                              [[command header] requestId],
                              command.blockId,
                              command.firstPacketId,
                              command.lastPacketId);
                        
                        [commandData release];
                        [command release];
                    }
                }
            }
        }
    }
}

- (void)heartbeat {
    // Reading CCP is the recommended way to do heartbeat.
    CZGVCPReadRegisterCmd *command = [[CZGVCPReadRegisterCmd alloc] initWithRequestId:[self nextRequestId]];
    command.registerAddress = kGEVRegisterCCP;
    
    NSData *commandData = [command newDataFromCommand];
    
    @synchronized (_controlSocketLock) {
        [self.controlSocket sendData:commandData
                         withTimeout:kGVCPDefaultTimeout
                                 tag:[[command header] requestId]];
    }
    
    [commandData release];
    [command release];
}

- (BOOL)prepareDiscoverySocket {
    if (![[CZWifiNotifier sharedInstance] isReachable]) {
        return NO;
    }
    
    @synchronized (_discoverySocketLock) {
        if (self.discoverySocket == nil) {
            do {
                NSError *error = nil;
                _discoverySocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                 delegateQueue:_gevDelegateQueue];
                [_discoverySocket setIPv4Enabled:YES];
                [_discoverySocket setIPv6Enabled:YES];
                
                if (![_discoverySocket enableBroadcast:YES error:&error]) {
                    CZLogv(@"GVCP: failed to enable broadcast for the socket.");
                    break;
                }
                
                if (![_discoverySocket beginReceiving:&error]) {
                    CZLogv(@"GVCP: failed to begin receiving data from socket.");
                    break;
                }
                
                return YES;
            } while (0);
            
            // Close and release the created socket if error occurred.
            [self.discoverySocket close];
            self.discoverySocket = nil;
            
            return NO;
        } else {
            return YES;
        }
    }
}

- (uint16_t)nextRequestId {
    ++_requestId;
    if (_requestId == 0) {
        _requestId = 1;
    }
    return _requestId;
}

+ (NSString *)ipStringFromInteger:(uint32_t)address {
    uint8_t *p = (uint8_t *)&address;
    return [NSString stringWithFormat:@"%u.%u.%u.%u", p[3], p[2], p[1], p[0]];
}

#pragma mark - GCDAsyncUdpSocketDelegate Methods

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    if (sock == self.snapshotSocket) {
        if (self.imagePayload && ![self.imagePayload hasGotAllPackets]) {
            CZGVSPDataBlock *dataBlock = [[CZGVSPDataBlock alloc] initWithData:data];
            if (dataBlock) {
                [self.imagePayload addDataBlock:dataBlock];
                self.lastPacketTime = [NSDate date];
                
                if ([self.imagePayload hasGotAllPackets]) {
                    dispatch_semaphore_signal(_gevSnapshotSemaphore);
                }
                
                [dataBlock release];
            }
        }
    } else if (sock == self.eventsSocket) {
        CZGVCPEventCmd *event = [[CZGVCPEventCmd alloc] initWithData:data];
        if (event) {
            switch (event.eventIdentifier) {
                case kGVCPAcquisitionStopEvent:
                    CZLogv(@"GVCP Event Received: Acquisition Stop");
                    [[NSNotificationCenter defaultCenter] postNotificationName:CZCameraStreamingStopNotification
                                                                        object:nil
                                                                      userInfo:@{@"macAddress" : self.macAddress}];
                    break;
                    
                case kGVCPAcquisitionStartEvent:
                    CZLogv(@"GVCP Event Received: Acquisition Start");
                    break;
                    
                default:
                    break;
            }
        }
        [event release];
    } else {
        CZGVCPAcknowlegeHeader *header = [[CZGVCPAcknowlegeHeader alloc] initWithData:data];
        if (header) {
            switch ([header acknowledge]) {
                case kGVCPDiscoveryAck:
                {
                    CZGVCPDiscoveryAck *ack = [[[CZGVCPDiscoveryAck alloc] initWithData:data] autorelease];
                    
                    // Check whether the IP address is in link-local address range.
                    // If yes, send FORCEIP command to the camera and doesn't
                    // notify delegate about the discovery result.
                    if ([ack.currentIpAddress hasPrefix:@"169.254."]) {
                        CZLogv(@"GVCP device with link-local address found: %@", ack.currentIpAddress);
                        
                        if ([self prepareDiscoverySocket]) {
                            CZGVCPForceIPCmd *command = [[CZGVCPForceIPCmd alloc] initWithRequestId:0];
                            command.macAddressHigh = ack.deviceMacAddressHigh;
                            command.macAddressLow = ack.deviceMacAddressLow;
                            
                            NSData *commandData = [command newDataFromCommand];
                            
                            [self.discoverySocket sendData:commandData
                                                    toHost:@"255.255.255.255" // Broadcast
                                                      port:kGVCPPort
                                               withTimeout:kGVCPDefaultTimeout
                                                       tag:[[command header] requestId]];
                            
                            [commandData release];
                            [command release];
                            
                            CZLogv(@"GVCP FORCEIP sent to %@", ack.deviceMacAddress);
                        }
                    } else {
                        if ([_delegate respondsToSelector:@selector(gevClient:didGetDiscoveryAck:)]) {
                            [_delegate gevClient:self didGetDiscoveryAck:ack];
                        }
                    }
                    
                    break;
                }
                    
                case kGVCPReadRegisterAck:
                {
                    CZGVCPReadRegisterAck *ack = [[CZGVCPReadRegisterAck alloc] initWithData:data];
                    NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:ack.header.acknowlegeId];
                    
                    @synchronized (_readingHistoryLock) {
                        NSMutableArray *pair = (NSMutableArray *)_readingHistory[key];
                        if (pair) {
                            pair[1] = [NSNumber numberWithUnsignedLong:ack.registerData];
                            dispatch_semaphore_signal((dispatch_semaphore_t)pair[0]);
                        }
                    }
                    
                    [key release];
                    [ack release];
                    break;
                }
                    
                case kGVCPWriteRegisterAck:
                {
                    CZGVCPWriteRegisterAck *ack = [[CZGVCPWriteRegisterAck alloc] initWithData:data];
                    NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:ack.header.acknowlegeId];
                    
                    @synchronized (_writingHistoryLock) {
                        NSMutableArray *pair = (NSMutableArray *)_writingHistory[key];
                        if (pair) {
                            pair[1] = [NSNumber numberWithBool:(ack.index > 0)];
                            dispatch_semaphore_signal((dispatch_semaphore_t)pair[0]);
                        }
                    }
                    
                    [key release];
                    [ack release];
                    break;
                }
                    
                case kGVCPWriteMemoryAck:
                {
                    CZGVCPWriteMemoryAck *ack = [[CZGVCPWriteMemoryAck alloc] initWithData:data];
                    NSNumber *key = [[NSNumber alloc] initWithUnsignedShort:ack.header.acknowlegeId];
                    
                    @synchronized (_writingHistoryLock) {
                        NSMutableArray *pair = (NSMutableArray *)_writingHistory[key];
                        if (pair) {
                            pair[1] = [NSNumber numberWithBool:(ack.index > 0)];
                            dispatch_semaphore_signal((dispatch_semaphore_t)pair[0]);
                        }
                    }
                    
                    [key release];
                    [ack release];
                    break;
                }
                    
                default:
                    break;
            }
        }
        [header release];
    }
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    @synchronized (_discoverySocketLock) {
        if (sock == self.discoverySocket) {
            self.discoverySocket = nil;
        }
    }
    if (sock == self.controlSocket) {
        [self disconnect];
    }
    if (sock == self.snapshotSocket) {
        self.snapshotSocket = nil;
    }
    @synchronized (_eventsSocketLock) {
        if (sock == self.eventsSocket) {
            self.eventsSocket = nil;
        }
    }
}

@end
