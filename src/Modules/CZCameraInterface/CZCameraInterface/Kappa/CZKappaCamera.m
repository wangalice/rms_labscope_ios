//
//  CZKappaCamera.m
//  Hermes
//
//  Created by Halley Gu on 4/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZKappaCamera.h"
#import "CZCameraSubclass.h"
#import "CZGEVClient.h"

static const uint32_t kKappaGainLevels[] = {
    1, 9, 17, 25, 33, 41
};
static const NSUInteger kKappaLatestFirmwareVersion = 1221; //version is 1.221
static const NSUInteger kKappaGainLevelsCount = sizeof(kKappaGainLevels) / sizeof(kKappaGainLevels[0]);

@interface CZKappaCamera () {
    BOOL _eventListening;
    NSObject *_eventListeningLock;
    NSString *_firmwareVersion;
    NSUInteger _controlRetainCount;
    NSObject *_controlRetainCountLock;
}

@property (nonatomic, retain) CZGEVClient *gevClient;

+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain;

@end

@implementation CZKappaCamera

- (id)init {
    self = [super init];
    if (self) {
        _gevClient = [[CZGEVClient alloc] init];
        _eventListening = NO;
        _eventListeningLock = [[NSObject alloc] init];
        
        _controlRetainCount = 0;
        _controlRetainCountLock = [[NSObject alloc] init];
        
        [self addObserver:self forKeyPath:@"ipAddress" options:0 context:NULL];
        [self addObserver:self forKeyPath:@"macAddress" options:0 context:NULL];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [self removeObserver:self forKeyPath:@"ipAddress"];
    [self removeObserver:self forKeyPath:@"macAddress"];
    
    [_firmwareVersion release];
    [_gevClient release];
    _gevClient = nil;
    
    [_eventListeningLock release];
    
    [_controlRetainCountLock release];
    
    [super dealloc];
}

- (void)appDidEnterBackground:(NSNotification *)notification {
    // fix bug #3117: crash when switch app to background
    [self retain];

    __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"StopGEVClient" expirationHandler:^{
        if (bgTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Release camera control to prevent blocking others from controlling the camera.
        [_gevClient disconnect];
        
        self.exclusiveLock = nil;
        while ([self releaseControl:CZCameraControlTypeSnap]) {
            // Reset camera control retain count.
        }
        
        if (bgTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    });
    
    [self release];
}

#pragma mark - Overridden methods

- (CZCameraCapability)capabilities {
    return (CZCameraCapabilityResetAll |
            CZCameraCapabilityExposureMode |
            CZCameraCapabilityExposureTime |
            CZCameraCapabilityExtraExposureTime |
            CZCameraCapabilityGain |
            CZCameraCapabilityExposureIntensity |
            CZCameraCapabilityExposureTimeAutoAdjust |
            CZCameraCapabilityWhiteBalanceMode |
            CZCameraCapabilityWhiteBalanceAutoAdjust |
            CZCameraCapabilityStreamingQuality |
            CZCameraCapabilityImageOrientation |
            CZCameraCapabilitySnapResolution |
            CZCameraCapabilitySharpening |
            CZCameraCapabilityGrayscaleMode |
            CZCameraCapabilityExclusiveLock);
}

- (NSString *)firmwareVersion {
    return _firmwareVersion;
}

- (BOOL)isFirmwareUpdated {
    NSString *version = [self firmwareVersion];
    NSScanner *scanner = [[NSScanner alloc] initWithString:version];
    float versionNumber = 0.0f;
    NSString *temp;
    [scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] intoString:&temp];
    BOOL isScanSuccess = [scanner scanFloat:(&versionNumber)];
    if (!isScanSuccess) {
        [scanner release];
        return YES;
    }
    [scanner release];
    NSUInteger currentFirmwareVersion = (NSUInteger)roundf(versionNumber * 1000);
    
    return (currentFirmwareVersion >= kKappaLatestFirmwareVersion);
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    [super setName:name andUpload:shouldUpload];
    if (shouldUpload) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
            CZCameraControlLock *lock = [[CZCameraControlLock alloc] initWithCamera:self];
            if (lock) {
                [_gevClient writeRegister:kGEVRegisterUserDefinedName withString:name];
                [lock release];
            }
        });
    }
}

- (void)beginContinuousSnapping:(NSUInteger)frameCount {
    id delegate = self.delegate;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        @autoreleasepool {
            NSError *error = nil;
            NSArray *images = nil;
            float snappingExposure = 0.0f;
            BOOL isGray = NO;
            CZCameraControlLock *lock = [[CZCameraControlLock alloc] initWithCamera:self];
            if (lock) {
                if ([delegate respondsToSelector:@selector(camera:didGetControlBeforeSnapping:)]) {
                    dispatch_async(dispatch_get_main_queue(), ^() {
                        [delegate camera:self didGetControlBeforeSnapping:lock];
                    });
                }
                isGray = [self isGrayscaleModeEnabled];
                
                // Check whether needs to use extra long exposure time for snapping.
                snappingExposure = [self snappingExposureTime];
                images = [_gevClient takeSnapshotWithResolution:self.snapResolutionPreset
                                                   andExposure:&snappingExposure
                                                     frameCount:frameCount];
                
                [lock release];
            } else {
                error = [NSError errorWithDomain:CZCameraErrorDomain
                                            code:kCZCameraNoControlError
                                        userInfo:nil];
            }
            
            if (images.count < frameCount && error == nil) {
                error = [NSError errorWithDomain:CZCameraErrorDomain
                                            code:kCZCameraTimeoutError
                                        userInfo:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^() {
                if ([delegate respondsToSelector:@selector(camera:didFinishSnapping:error:)]) {
                    if (error == nil) {
                        for (UIImage *image in images) {
                            CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                            snappingInfo.image = image;
                            snappingInfo.grayscale = isGray;
                            snappingInfo.exposureTimeInMilliseconds = @(snappingExposure);
                            [delegate camera:self didFinishSnapping:snappingInfo error:error];
                            [snappingInfo release];
                        }
                    } else {
                        CZCameraSnappingInfo *snappingInfo = [[CZCameraSnappingInfo alloc] init];
                        snappingInfo.image = nil;
                        snappingInfo.grayscale = isGray;
                        snappingInfo.exposureTimeInMilliseconds = @(snappingExposure);
                        [delegate camera:self didFinishSnapping:snappingInfo error:error];
                        [snappingInfo release];
                    }
                }
            });
        }
    });
}

- (CGSize)liveResolution {
    return CGSizeMake(1280, 720);
}

- (void)beginEventListening {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized (_eventListeningLock) {
            if (_eventListening) {
                return;
            }
            _eventListening = YES;
            
            CZCameraControlLock *lock = [[CZCameraControlLock alloc] initWithCamera:self];
            if (lock) {
                [_gevClient beginEventListening];
                [lock release];
            } else {
                _eventListening = NO;
            }
        }
    });
}

- (void)endEventListening {
    __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"EndEventListening" expirationHandler:^{
        if (bgTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized (_eventListeningLock) {
            if (_eventListening) {
                _eventListening = NO;
                
                [_gevClient endEventListening];
            }
        }
        
        if (bgTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }
    });
}

- (void)performReset {
    uint32_t exposureMode = [self readDefaultValueForRegister:kGEVRegisterAutomaticExposureTime];
    uint32_t gainMode = [self readDefaultValueForRegister:kGEVRegisterAutomaticGainControl];
    uint32_t automaticLevel = [self readDefaultValueForRegister:kGEVRegisterAutomaticLevel];
    uint32_t exposureTime = [self readDefaultValueForRegister:kGEVRegisterExposureTime];
    uint32_t gain = [self readDefaultValueForRegister:kGEVRegisterGain];
    
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticExposureTime withData:exposureMode];
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticGainControl withData:gainMode];
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticLevel withData:automaticLevel];
    [_gevClient writeRegisterAsync:kGEVRegisterExposureTime withData:exposureTime];
    [_gevClient writeRegisterAsync:kGEVRegisterGain withData:gain];
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticWhiteSet withData:1];
    
    CZCameraBitRate bitRate = [self defaultBitRate];
    CZCameraImageOrientation imageOrientation = [self defaultImageOrientation];
    CZCameraSnapResolutionPreset snapResolutionPreset = [self defaultSnapResolutionPreset];
    uint32_t sharpness = [self defaultSharpness];
    BOOL isGrayscaleModeEnabled = [self isGrayscaleModeEnabledByDefault];
    
    [self setBitRate:bitRate];
    [self setImageOrientation:imageOrientation];
    [self setSnapResolutionPreset:snapResolutionPreset];
    [self setSharpness:sharpness];
    [self setGrayscaleModeEnabled:isGrayscaleModeEnabled];
}

- (NSURL *)rtspUrl {
    NSString *urlString = [[NSString alloc] initWithFormat:@"rtsp://%@:8557/PSIA/Streaming/channels/2?videoCodecType=H.264", self.ipAddress];
    NSURL *url = [NSURL URLWithString:urlString];
    [urlString release];
    return url;
}

- (BOOL)getControl:(CZCameraControlType)type {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount == 0) {
            if ([self startControl:type]) {
                _controlRetainCount = 1;
                return YES;
            } else {
                return NO;
            }
        }
        
        ++_controlRetainCount;
        return YES;
    }
}

- (BOOL)releaseControl:(CZCameraControlType)type {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount > 0) {
            --_controlRetainCount;
        } else {
            return NO;
        }
        
        if (_controlRetainCount == 0) {
            [self endControl:type];
        }
        
        return YES;
    }
}

- (BOOL)startControl:(CZCameraControlType)type {
    return [_gevClient connect];
}

- (void)endControl:(CZCameraControlType)type {
    [_gevClient disconnectAsync];
}

#pragma mark - Exposure Mode

- (CZCameraExposureMode)exposureMode {
    uint32_t exposure = [_gevClient readRegister:kGEVRegisterAutomaticExposureTime];
    uint32_t gainControl = [_gevClient readRegister:kGEVRegisterAutomaticGainControl];
    if (exposure != gainControl) {
        return kCZCameraExposureMixed;
    } else {
        return (exposure == 1) ? kCZCameraExposureAutomatic : kCZCameraExposureManual;
    }
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
    uint32_t value = (exposureMode == kCZCameraExposureAutomatic) ? 1 : 0;
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticExposureTime withData:value];
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticGainControl withData:value];
}

- (void)beginExposureTimeAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [_gevClient writeRegisterAsync:kGEVRegisterAutomaticExposureTime withData:2];
        [_gevClient writeRegisterAsync:kGEVRegisterAutomaticGainControl withData:2];
        sleep(3); // Wait for the camera to finish auto exposure-time adjust.
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishExposureTimeAutoAdjust:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                [self.controlDelegate cameraDidFinishExposureTimeAutoAdjust:self];
            });
        }
    });
}

#pragma mark - Exposure Time

- (NSArray<NSNumber *> *)supportedExposureTimeValues {
    return @[@0.03f, @0.075f, @0.16f, @0.5f, @1.f, @2.f, @4.f, @5.f, @8.0f, @8.33333f, @10.f,
             @16.6666f, @20.f, @25.f, @30.30303f, @33.33333f, @40.f, @50.f, @66.66666f, @100.f, @200.f];
}

- (float)exposureTime {
    uint32_t value = [_gevClient readRegister:kGEVRegisterExposureTime];
    return (CGFloat)value / 1000;
}

- (void)setExposureTime:(float)milliseconds {
    uint32_t value = (uint32_t)round(milliseconds * 1000);
    [_gevClient writeRegisterAsync:kGEVRegisterExposureTime withData:value];
}

#pragma mark - Gain

- (NSUInteger)minGain {
    return 1;
}

- (NSUInteger)maxGain {
    return 6;
}

- (NSUInteger)gain {
    uint32_t value = [_gevClient readRegister:kGEVRegisterGain];
    NSUInteger gainLevel = [CZKappaCamera nearestValidGainLevelIndex:value];
    return gainLevel + 1;
}

- (void)setGain:(NSUInteger)gain {
    if (gain - 1 >= kKappaGainLevelsCount) {
        return;
    }
    
    [_gevClient writeRegisterAsync:kGEVRegisterGain withData:kKappaGainLevels[gain - 1]];
}

#pragma mark - Exposure Intensity

- (NSUInteger)minExposureIntensity {
    return 1;
}

- (NSUInteger)maxExposureIntensity {
    return 200;
}

- (NSUInteger)exposureIntensity {
    uint32_t value = [_gevClient readRegister:kGEVRegisterAutomaticLevel];
    return value;
}

- (void)setExposureIntensity:(NSUInteger)exposureIntensity {
    uint32_t value = exposureIntensity;
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticLevel withData:value];
}

#pragma mark - White Balance Mode

- (BOOL)isWhiteBalanceLocked {
    uint32_t value = [_gevClient readRegister:kGEVRegisterAutomaticWhiteSet];
    return (value != 1);
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
    uint32_t value = locked ? 0 : 1;
    [_gevClient writeRegisterAsync:kGEVRegisterAutomaticWhiteSet withData:value];
}

- (void)beginWhiteBalanceAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_gevClient writeRegisterAsync:kGEVRegisterAutomaticWhiteSet withData:2];
        sleep(3); // Wait for the camera to finish auto white-balance.
        
        if ([self.controlDelegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust:userInfo:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.controlDelegate cameraDidFinishWhiteBalanceAutoAdjust:self userInfo:nil];
            });
        }
    });
}

#pragma mark - Streaming Quality

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    CGFloat bitRateValue = 0.0;
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateValue = 8.0;
            break;
            
        case kCZCameraBitRateLow:
            bitRateValue = 2.0;
            break;
            
        case kCZCameraBitRateMedium:
        default:
            bitRateValue = 4.0;
            break;
    }
    return bitRateValue;
}

- (CZCameraBitRate)defaultBitRate {
    return kCZCameraBitRateMedium;
}

- (CZCameraBitRate)bitRate {
    uint32_t bitRate = [self readRegister:kGEVRegisterEncoderBitRate];
    if (bitRate >= 6e+6) { // >= 6 Mbps
        return kCZCameraBitRateHigh;
    } else if (bitRate < 25e+5) { // < 2.5 Mbps
        return kCZCameraBitRateLow;
    }
    return kCZCameraBitRateMedium;
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
    if (bitRate == [self bitRate]) {
        return;
    }
    
    uint32_t bitRateInMegabytes = 4e+6;
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateInMegabytes = 8e+6;
            break;
            
        case kCZCameraBitRateLow:
            bitRateInMegabytes = 2e+6;
            break;
            
        default:
            break;
    }
    
    [_gevClient writeRegister:kGEVRegisterAcquisitionState withData:0];
    [_gevClient writeRegister:kGEVRegisterEncoderBitRate withData:bitRateInMegabytes];
    [_gevClient writeRegister:kGEVRegisterAcquisitionState withData:1];
}

#pragma mark - Image Orientation

- (CZCameraImageOrientation)defaultImageOrientation {
    return kCZCameraImageOrientationFlippedVertically;
}

- (CZCameraImageOrientation)imageOrientation {
    uint32_t hFlipped = [_gevClient readRegister:kGEVRegisterHorizontalFlip];
    uint32_t vFlipped = [_gevClient readRegister:kGEVRegisterVerticalFlip];
    
    if (hFlipped == 0) {
        if (vFlipped == 0) {
            return kCZCameraImageOrientationOriginal;
        } else {
            return kCZCameraImageOrientationFlippedVertically;
        }
    } else {
        if (vFlipped == 0) {
            return kCZCameraImageOrientationFlippedHorizontally;
        } else {
            return kCZCameraImageOrientationRotated180;
        }
    }
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
    uint32_t hFlipped = 0;
    uint32_t vFlipped = 0;
    
    switch (orientation) {
        case kCZCameraImageOrientationFlippedHorizontally:
            hFlipped = 1;
            break;
            
        case kCZCameraImageOrientationFlippedVertically:
            vFlipped = 1;
            break;
            
        case kCZCameraImageOrientationRotated180:
            hFlipped = 1;
            vFlipped = 1;
            break;
            
        default:
            break;
    }
    
    [_gevClient writeRegister:kGEVRegisterAcquisitionState withData:0];
    [_gevClient writeRegister:kGEVRegisterHorizontalFlip withData:hFlipped];
    [_gevClient writeRegister:kGEVRegisterVerticalFlip withData:vFlipped];
    [_gevClient writeRegister:kGEVRegisterAcquisitionState withData:1];
}

#pragma mark - Snap Resolution

- (CGSize)lowSnapResolution {
    return CGSizeMake(1280, 720);
}

- (CGSize)highSnapResolution {
    return CGSizeMake(2560, 1920);
}

- (CZCameraSnapResolutionPreset)defaultSnapResolutionPreset {
    return kCZCameraSnapResolutionHigh;
}

#pragma mark - Sharpening

- (uint32_t)defaultSharpness {
    return 0;
}

- (uint32_t)sharpnessStepCount {
    return 4; // 0-3
}

- (uint32_t)sharpness {
    uint32_t value = [_gevClient readRegister:kGEVRegisterSharpness];
    return MIN(value, [self sharpnessStepCount] - 1);
}

- (void)setSharpness:(uint32_t)sharpness {
    [_gevClient writeRegisterAsync:kGEVRegisterSharpness withData:sharpness];
}

#pragma mark - Grayscale Mode

- (BOOL)isGrayscaleModeEnabledByDefault {
    return NO;
}

- (BOOL)isGrayscaleModeEnabled {
    uint32_t saturation = [_gevClient readRegister:kGEVRegisterColorSaturation];
    return (saturation == 0);
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
    if (enabled) {
        [_gevClient writeRegisterAsync:kGEVRegisterColorSaturation withData:0];
    } else {
        uint32_t defaultSaturation = [self readDefaultValueForRegister:kGEVRegisterColorSaturation];
        [_gevClient writeRegisterAsync:kGEVRegisterColorSaturation withData:defaultSaturation];
    }
}

#pragma mark - Public methods

- (void)setFirmwareVersion:(NSString *)version {
    [_firmwareVersion release];
    _firmwareVersion = [version copy];
}

- (uint32_t)readRegister:(CZGEVRegister)registerAddress {
    return [_gevClient readRegister:registerAddress];
}

- (void)writeRegister:(CZGEVRegister)registerAddress withData:(uint32_t)data {
    [_gevClient writeRegister:registerAddress withData:data];
}

- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withData:(uint32_t)data {
    [_gevClient writeRegisterAsync:registerAddress withData:data];
}

#pragma mark - Private methods

+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain {
    NSInteger leftIndex = -1;
    for (NSUInteger i = 0; i < kKappaGainLevelsCount; ++i) {
        if (kKappaGainLevels[i] < gain) {
            leftIndex = i;
        } else {
            break;
        }
    }
    
    if (leftIndex == -1) {
        return 0;
    } else if (leftIndex == kKappaGainLevelsCount - 1) {
        return leftIndex;
    }
    
    CGFloat leftValue = kKappaGainLevels[leftIndex];
    CGFloat rightValue = kKappaGainLevels[leftIndex + 1];
    CGFloat average = (leftValue + rightValue) / 2;
    return (gain < average) ? leftIndex : leftIndex + 1;
}

- (uint32_t)readDefaultValueForRegister:(CZGEVRegister)registerAddress {
    // Offset for default value is 0x00000300.
    return [_gevClient readRegister:(registerAddress + 0x00000300)];
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self) {
        if ([keyPath isEqualToString:@"ipAddress"]) {
            [_gevClient setIpAddress:self.ipAddress];
        } else if ([keyPath isEqualToString:@"macAddress"]) {
            [_gevClient setMacAddress:self.macAddress];
        }
    }
}

@end
