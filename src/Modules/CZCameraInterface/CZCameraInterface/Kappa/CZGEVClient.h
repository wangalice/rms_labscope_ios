//
//  CZGEVClient.h
//  Hermes
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import "CZGEVCommonDefs.h"
#import "CZCamera.h"

@class CZGEVClient;
@class CZGVCPDiscoveryAck;

@protocol CZGEVClientDelegate <NSObject>

- (void)gevClient:(CZGEVClient *)client didGetDiscoveryAck:(CZGVCPDiscoveryAck *)acknowlege;

@end

@interface CZGEVClient : NSObject<GCDAsyncUdpSocketDelegate>

@property (atomic, assign) id<CZGEVClientDelegate> delegate;
@property (atomic, copy) NSString *ipAddress;
@property (atomic, copy) NSString *macAddress;

- (void)beginDiscovery;

- (BOOL)connect;
- (void)disconnect;
- (void)disconnectAsync;

- (uint32_t)readRegister:(CZGEVRegister)registerAddress;

- (BOOL)writeRegister:(CZGEVRegister)registerAddress withData:(uint32_t)data;
- (BOOL)writeRegister:(CZGEVRegister)registerAddress withString:(NSString *)data;

- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withData:(uint32_t)data;
- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withString:(NSString *)data;

- (NSArray *)takeSnapshotWithResolution:(CZCameraSnapResolutionPreset)resolution
                            andExposure:(float *)exposureTime
                             frameCount:(NSUInteger)frameCount;

- (void)beginEventListening;
- (void)endEventListening;

@end
