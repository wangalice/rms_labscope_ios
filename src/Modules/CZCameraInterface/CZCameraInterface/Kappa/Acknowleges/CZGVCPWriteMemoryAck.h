//
//  CZGVCPWriteMemoryAck.h
//  Hermes
//
//  Created by Halley Gu on 7/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPAcknowlege.h"

@interface CZGVCPWriteMemoryAck : NSObject<CZGVCPAcknowlege>

@property (nonatomic, assign, readonly) uint16_t index;

@end
