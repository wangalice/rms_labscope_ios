//
//  CZGVCPAcknowlege.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPAcknowlegeHeader.h"

@protocol CZGVCPAcknowlege <NSObject>

@property (nonatomic, retain) CZGVCPAcknowlegeHeader *header;

- (id)initWithData:(NSData *)data;

@end
