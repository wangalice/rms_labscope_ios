//
//  CZGVCPAcknowlegeHeader.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZGVCPAcknowlegeHeader : NSObject

@property (nonatomic, assign, readonly) uint16_t status;
@property (nonatomic, assign, readonly) uint16_t acknowledge;
@property (nonatomic, assign, readonly) uint16_t length;
@property (nonatomic, assign, readonly) uint16_t acknowlegeId;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

@end
