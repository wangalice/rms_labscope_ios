//
//  CZGVCPDiscoveryAck.h
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPAcknowlege.h"

@interface CZGVCPDiscoveryAck : NSObject<CZGVCPAcknowlege>

@property (nonatomic, assign, readonly) uint16_t specVersionMajor;
@property (nonatomic, assign, readonly) uint16_t specVersionMinor;
@property (nonatomic, assign, readonly) uint32_t deviceMode;
@property (nonatomic, assign, readonly) uint16_t deviceMacAddressHigh;
@property (nonatomic, assign, readonly) uint32_t deviceMacAddressLow;
@property (nonatomic, copy, readonly) NSString *deviceMacAddress;
@property (nonatomic, assign, readonly) uint32_t ipConfigOptions;
@property (nonatomic, assign, readonly) uint32_t ipConfigCurrent;
@property (nonatomic, copy, readonly) NSString *currentIpAddress;
@property (nonatomic, copy, readonly) NSString *currentSubnetMask;
@property (nonatomic, copy, readonly) NSString *defaultGateway;
@property (nonatomic, copy, readonly) NSString *manufacturerName;
@property (nonatomic, copy, readonly) NSString *modelName;
@property (nonatomic, copy, readonly) NSString *deviceVersion;
@property (nonatomic, copy, readonly) NSString *manufacturerSpecificInfo;
@property (nonatomic, copy, readonly) NSString *serialNumber;
@property (nonatomic, copy, readonly) NSString *userDefinedName;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithData:(NSData *)data NS_DESIGNATED_INITIALIZER;

@end
