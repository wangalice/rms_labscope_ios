//
//  CZGVCPAcknowlegeHeader.m
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPAcknowlegeHeader.h"
#import "CZGEVCommonDefs.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPAcknowlegeHeader

- (id)init {
    [self release];
    return nil;
}

- (id)initWithData:(NSData *)data {
    if ([data length] < kGVCPAcknowlegeHeaderLength) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                       length:data.length];
        
        _status = [reader readShortInHostOrder];
        _acknowledge = [reader readShortInHostOrder];
        _length = [reader readShortInHostOrder];
        _acknowlegeId = [reader readShortInHostOrder];
        
        [reader release];
    }
    return self;
}

@end
