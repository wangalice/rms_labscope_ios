//
//  CZGVCPReadRegisterAck.m
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPReadRegisterAck.h"
#import "CZGEVCommonDefs.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPReadRegisterAck

@synthesize header;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithData:(NSData *)data {
    if ([data length] <= 0) {
        [self release];
        return nil;
    }
    
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    [reader seekToOffset:kGVCPHeaderTypeOffset];
    
    uint16_t answer = [reader readShortInHostOrder];
    if (answer != kGVCPReadRegisterAck) {
        [reader release];
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        header = [[CZGVCPAcknowlegeHeader alloc] initWithData:data];
        
        [reader seekToOffset:kGVCPAcknowlegeHeaderLength];
        _registerData = [reader readLongInHostOrder];
    }
    
    [reader release];
    return self;
}

- (void)dealloc {
    [header release];
    [super dealloc];
}

@end
