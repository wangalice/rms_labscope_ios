//
//  CZGVCPDiscoveryAck.m
//  Hermes
//
//  Created by Halley Gu on 4/1/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGVCPDiscoveryAck.h"
#import "CZGEVCommonDefs.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZGVCPDiscoveryAck

@synthesize header;

- (instancetype)init {
    [self release];
    return nil;
}

- (instancetype)initWithData:(NSData *)data {
    if ([data length] <= 0) {
        [self release];
        return nil;
    }
    
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    [reader seekToOffset:kGVCPHeaderTypeOffset];
    
    uint16_t answer = [reader readShortInHostOrder];
    if (answer != kGVCPDiscoveryAck) {
        [reader release];
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        header = [[CZGVCPAcknowlegeHeader alloc] initWithData:data];
        
        [reader seekToOffset:kGVCPAcknowlegeHeaderLength];
        
        _specVersionMajor = [reader readShortInHostOrder];
        _specVersionMinor = [reader readShortInHostOrder];
        _deviceMode = [reader readLongInHostOrder];
        
        [reader jumpByLength:2];
        
        _deviceMacAddressHigh = [reader readShortInHostOrder];
        _deviceMacAddressLow = [reader readLongInHostOrder];
        _deviceMacAddress = [[NSString alloc] initWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                             (_deviceMacAddressHigh >> 8) & 0xFF,
                             _deviceMacAddressHigh & 0xFF,
                             (_deviceMacAddressLow >> 24) & 0xFF,
                             (_deviceMacAddressLow >> 16) & 0xFF,
                             (_deviceMacAddressLow >> 8) & 0xFF,
                             _deviceMacAddressLow & 0xFF];
        
        _ipConfigOptions = [reader readLongInHostOrder];
        _ipConfigCurrent = [reader readLongInHostOrder];
        
        [reader seekToOffset:44];
        _currentIpAddress = [[reader readIpAddressString] retain];
        
        [reader seekToOffset:60];
        _currentSubnetMask = [[reader readIpAddressString] retain];
        
        [reader seekToOffset:76];
        _defaultGateway = [[reader readIpAddressString] retain];
        
        _manufacturerName = [[reader readStringWithLength:32] retain];
        _modelName = [[reader readStringWithLength:32] retain];
        _deviceVersion = [[reader readStringWithLength:32] retain];
        _manufacturerSpecificInfo = [[reader readStringWithLength:48] retain];
        _serialNumber = [[reader readStringWithLength:16] retain];
        _userDefinedName = [[reader readStringWithLength:16] retain];
    }
    
    [reader release];
    return self;
}

- (void)dealloc {
    [header release];
    [_deviceMacAddress release];
    [_currentIpAddress release];
    [_currentSubnetMask release];
    [_defaultGateway release];
    [_manufacturerName release];
    [_modelName release];
    [_deviceVersion release];
    [_manufacturerSpecificInfo release];
    [_serialNumber release];
    [_userDefinedName release];
    [super dealloc];
}

@end
