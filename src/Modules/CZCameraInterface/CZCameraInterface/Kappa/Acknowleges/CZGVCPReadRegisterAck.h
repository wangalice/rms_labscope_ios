//
//  CZGVCPReadRegisterAck.h
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZGVCPAcknowlege.h"

@interface CZGVCPReadRegisterAck : NSObject<CZGVCPAcknowlege>

@property (nonatomic, assign, readonly) uint32_t registerData;

@end
