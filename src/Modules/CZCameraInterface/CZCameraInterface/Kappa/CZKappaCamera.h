//
//  CZKappaCamera.h
//  Hermes
//
//  Created by Halley Gu on 4/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZRTSPCamera.h"
#import "CZGEVCommonDefs.h"

/*!
 * Represents a Kappa-manufactured camera. Typically Kappa cameras conform to
 * the GigEVision protocol.
 */
@interface CZKappaCamera : CZRTSPCamera

- (void)setFirmwareVersion:(NSString *)version;

- (uint32_t)readRegister:(CZGEVRegister)registerAddress;
- (uint32_t)readDefaultValueForRegister:(CZGEVRegister)registerAddress;
- (void)writeRegister:(CZGEVRegister)registerAddress withData:(uint32_t)data;
- (void)writeRegisterAsync:(CZGEVRegister)registerAddress withData:(uint32_t)data;

@end
