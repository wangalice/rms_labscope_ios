//
//  CZFluorescenceSnapping.h
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const CZFluorescenceSnappingTemplateDidSaveNotification;

typedef NS_ENUM(NSInteger, CZFluorescenceSnappingMode) {
    CZFluorescenceSnappingModeNone,
    CZFluorescenceSnappingModeManual,
    CZFluorescenceSnappingModeOnekey
};

typedef NS_ENUM(NSInteger, CZFluorescenceSnappingEndAction) {
    CZFluorescenceSnappingEndActionDone,
    CZFluorescenceSnappingEndActionUndone
};

@class CZCamera;
@class CZCameraLightSource;
@class CZCameraSnappingInfo;
@class CZFluorescenceSnappingTemplate;
@protocol CZFluorescenceSnappingDelegate;

@protocol CZFluorescenceSnapping <NSObject>

@required
@property (nonatomic, retain) CZFluorescenceSnappingTemplate *manualFluorescenceSnappingTemplate;
@property (nonatomic, retain) CZFluorescenceSnappingTemplate *onekeyFluorescenceSnappingTemplate;

- (BOOL)enterFluorescenceSnappingMode:(CZFluorescenceSnappingMode)mode delegate:(id<CZFluorescenceSnappingDelegate>)delegate;
- (void)switchToFluorescenceSnappingMode:(CZFluorescenceSnappingMode)mode;
- (void)exitFluorescenceSnappingMode;

- (BOOL)isFluorescenceSnappingMode;
- (BOOL)isManualFluorescenceSnappingMode;
- (BOOL)isOnekeyFluorescenceSnappingMode;

- (void)toggleLightSourceAtPosition:(NSUInteger)position;

- (void)beginOrContinueManualFluorescenceSnapping;
- (void)endManualFluorescenceSnapping:(CZFluorescenceSnappingEndAction)action;

- (BOOL)canBeginOnekeyFluorescenceSnapping;
- (void)beginOnekeyFluorescenceSnapping;
- (BOOL)isOnekeyFluorescenceSnappingPaused;
- (void)continueOnekeyFluorescenceSnapping;
- (void)cancelOnekeyFluorescenceSnapping;
- (void)endOnekeyFluorescenceSnapping:(CZFluorescenceSnappingEndAction)action;

@end

@protocol CZFluorescenceSnappingDelegate <NSObject>

@optional
- (void)camera:(CZCamera<CZFluorescenceSnapping> *)camera didGetLightSources:(NSArray<CZCameraLightSource *> *)lightSources;
- (void)camera:(CZCamera<CZFluorescenceSnapping> *)camera didUpdateLightSources:(NSArray<CZCameraLightSource *> *)lightSources;

@end

@interface CZFluorescenceSnappingTemplate : NSObject <NSCopying, NSSecureCoding>

@property (nonatomic, readonly, copy) NSArray<CZCameraSnappingInfo *> *snappingInfos;

+ (instancetype)emptyTemplate;

@end
