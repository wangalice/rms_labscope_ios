//
//  CZCameraSubclass.h
//  Matscope
//
//  Created by Ralph Jin on 12/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//
#import "CZCamera.h"
#import <CZVideoEngine/CZVideoEngine.h>

@interface CZCameraLightSource ()

@property (nonatomic, readwrite, assign) BOOL isOn;
@property (nonatomic, readwrite, assign) BOOL canOn;
@property (nonatomic, assign) BOOL isLocal; // For 3 led

@end

@interface CZCameraSnappingInfo ()

@property (nonatomic, readwrite, assign) NSUInteger currentSnappingNumber;
@property (nonatomic, readwrite, assign) NSUInteger totalSnappingNumber;

@property (nonatomic, readwrite, retain) UIImage *image;
@property (nonatomic, readwrite, assign) BOOL grayscale;

@property (nonatomic, readwrite, assign) CZCameraExposureMode exposureMode;
@property (nonatomic, readwrite, retain) NSNumber *exposureTimeInMilliseconds;
@property (nonatomic, readwrite, retain) NSNumber *gain;
@property (nonatomic, readwrite, assign) NSUInteger exposureIntensity;

@property (nonatomic, readwrite, retain) CZCameraLightSource *lightSource;
@property (nonatomic, readwrite, assign) CZCameraLightSourceType lightSourceType;
@property (nonatomic, readwrite, assign) NSUInteger lightIntensity;

@property (nonatomic, readwrite, copy)   NSString *filterSetID;

@end

@interface CZCamera (CZCameraProtected) <CZTimelapseRecorderDelegate>

@property (atomic, retain, readwrite) CZTimelapseRecorder *timelapseRecorder;

- (void)performReset;

@end
