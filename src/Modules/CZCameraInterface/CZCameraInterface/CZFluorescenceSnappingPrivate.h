//
//  CZFluorescenceSnappingPrivate.h
//  CZCameraInterface
//
//  Created by Li, Junlin on 7/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZFluorescenceSnapping.h"

@class CZCameraLightSource;

@interface CZFluorescenceSnappingTemplate ()

- (void)updateWithSnappingInfo:(CZCameraSnappingInfo *)snappingInfo;
- (CZCameraSnappingInfo *)snappingInfoForLightSource:(CZCameraLightSource *)lightSource;

@end
