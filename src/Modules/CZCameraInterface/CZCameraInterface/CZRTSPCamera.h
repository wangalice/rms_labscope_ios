//
//  CZRTSPCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCamera.h"
#import <CZVideoEngine/CZVideoEngine.h>

/*!
 * Represents a camera which has the ability to serve RTSP video streaming. This
 * implementation uses GPU-accelerated decoders to support video playback.
 */
@interface CZRTSPCamera : CZCamera<CZRTSPClientDelegate, CZHardwareDecoderDelegate>

@property (atomic, retain, readonly) CZRTSPClient *client;  // for sub-class

/*!
 * Returns the URL for RTSP video streaming. Each subclass of CZRTSPCamera
 * should override this method to provide correct template to generate the URL
 * by replacing placeholder with real IP address.
 */
- (NSURL *)rtspUrl;

/** default is CZRTSPClient class. Override it should return subclass of CZRTSPClient. */
+ (Class)clientClass;

@end
