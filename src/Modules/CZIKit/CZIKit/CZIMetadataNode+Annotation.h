//
//  CZIMetadataNode+Annotation.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"

@interface CZIMetadataNode (Annotation)

- (instancetype)alignPosition;
- (instancetype)arrow;
- (instancetype)attributes;
- (instancetype)baseLine;
- (instancetype)bezier;
- (instancetype)caliper;
- (instancetype)canMeasureEnable;
- (instancetype)centerX;
- (instancetype)centerY;
- (instancetype)chord;
- (instancetype)chordPattern;
- (instancetype)circle;
- (instancetype)connectedAngle;
- (instancetype)corner;
- (instancetype)creationPoints;
- (instancetype)deltaX;
- (instancetype)deltaY;
- (instancetype)disconnectedAngle;
- (instancetype)displayUnit;
- (instancetype)distance;
- (instancetype)distances;
- (instancetype)events;
- (instancetype)expression;
- (instancetype)feature;
- (instancetype)features;
- (instancetype)fill;
- (instancetype)fillStyle;
- (instancetype)firstLine;
- (instancetype)fontSize;
- (instancetype)foreground;
- (instancetype)geometry;
- (instancetype)grainSizeNumber;
- (instancetype)graphicElementGroup;
- (instancetype)graphicLayer;
- (instancetype)groupId;
- (instancetype)height;
- (instancetype)horizontalAlignment;
- (instancetype)intersectionPoints;
- (instancetype)isDisplayEnabled;
- (instancetype)isEditable;
- (instancetype)isMeasurementNameVisible;
- (instancetype)isMeasurementVisible;
- (instancetype)isMovable;
- (instancetype)isRotateWithParent;
- (instancetype)isWrapping;
- (instancetype)keepAspectRatio;
- (instancetype)left;
- (instancetype)line;
- (instancetype)lineBeginStyle;
- (instancetype)lineEndStyle;
- (instancetype)meanDistance;
- (instancetype)measurementBackground;
- (instancetype)measurementForeground;
- (instancetype)multiCalipers;
- (instancetype)multiDistance;
- (instancetype)opacity;
- (instancetype)openBezier;
- (instancetype)orientation;
- (instancetype)point;
- (instancetype)points;
- (instancetype)pointsCircle;
- (instancetype)polygon;
- (instancetype)polyline;
- (instancetype)position;
- (instancetype)radius;
- (instancetype)rectangle;
- (instancetype)rotation;
- (instancetype)scale;
- (instancetype)scaleBar;
- (instancetype)secondLine;
- (instancetype)standard;
- (instancetype)stroke;
- (instancetype)strokeThickness;
- (instancetype)strokeZoomWithImage;
- (instancetype)text;
- (instancetype)textBox;
- (instancetype)textElement;
- (instancetype)textElements;
- (instancetype)top;
- (instancetype)uniqueName;
- (instancetype)verticalAlignment;
- (instancetype)width;
- (instancetype)x1;
- (instancetype)x2;
- (instancetype)x;
- (instancetype)y1;
- (instancetype)y2;
- (instancetype)y;

@end
