//
//  CZIMetadataNode+EXIF.h
//  CZIKit
//
//  Created by Li, Junlin on 6/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"

@interface CZIMetadataNode (EXIF)

- (instancetype)imageMetadata;

@end
