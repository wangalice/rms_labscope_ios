//
//  CZICompositor.h
//  CZIKit
//
//  Created by Li, Junlin on 12/3/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CZIMetadata.h"

@interface CZICompositor : NSObject

- (NSData *)compositeImages:(NSArray<UIImage *> *)images metadata:(CZIMetadata *)metadata;
- (void)compositeImages:(NSArray<UIImage *> *)images metadata:(CZIMetadata *)metadata toFile:(NSString *)path;

@end
