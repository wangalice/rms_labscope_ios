//
//  CZIMutableMetadataNode.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"
#import "CZIMutableMetadataNodeAttribute.h"

@interface CZIMutableMetadataNode : CZIMetadataNode

@property (nonatomic, readonly, strong) CZIMutableMetadataNodeAttribute *nodeAttribute;

@property (nonatomic, readwrite, copy) NSString *stringValue;
@property (nonatomic, readwrite, assign) BOOL boolValue;
@property (nonatomic, readwrite, assign) NSInteger integerValue;
@property (nonatomic, readwrite, assign) float floatValue;
@property (nonatomic, readwrite, assign) double doubleValue;
@property (nonatomic, readwrite, strong) NSDate *dateValue;
@property (nonatomic, readwrite, assign) CGPoint pointValue;
@property (nonatomic, readwrite, copy) NSArray<NSValue *> *pointsValue;
@property (nonatomic, readwrite, strong) UIColor *colorValue;

- (void)appendNode:(CZIMetadataNode *)node;
- (void)appendChildNode:(void (^)(CZIMutableMetadataNode *root))block;

@end

@protocol CZIMetadataNodeEncodable <NSObject>

@required
- (void)encodeToMetadataNode:(CZIMutableMetadataNode *)node;

@end
