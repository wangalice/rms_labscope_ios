//
//  CZIMetadataNode+Specification.m
//  CZIKit
//
//  Created by Li, Junlin on 11/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode+Specification.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation CZIMetadataNode (Specification)

@end

#pragma clang disgnostic pop
