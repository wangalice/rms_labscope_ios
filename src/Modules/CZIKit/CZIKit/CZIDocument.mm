//
//  CZIDocument.m
//  CZIKit
//
//  Created by Li, Junlin on 4/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIDocument.h"
#import "CZIFunctions.h"
#import "NSString+WString.h"
#include "libCZI.h"

class CZIImageInputStream : public libCZI::IStream {
private:
    NSData *data;
public:
    CZIImageInputStream(NSData *data) : data(data) {}
    
    void Read(std::uint64_t offset, void *pv, std::uint64_t size, std::uint64_t* ptrBytesRead) {
        if (offset + size > data.length) {
            return;
        }
        
        NSRange range = NSMakeRange((NSUInteger)offset, (NSUInteger)size);
        [data getBytes:pv range:range];
        *ptrBytesRead = size;
    }
};

class CZIImageOutputStream : public libCZI::IOutputStream {
private:
    NSMutableData *data = [NSMutableData data];
public:
    CZIImageOutputStream(NSMutableData *data) : data(data) {}
    
    void Write(std::uint64_t offset, const void* pv, std::uint64_t size, std::uint64_t* ptrBytesWritten) {
        std::uint64_t length = data.length;
        if (length < offset) {
            data.length = (NSUInteger)offset;
            [data appendBytes:pv length:(NSUInteger)size];
        } else if (length == offset) {
            [data appendBytes:pv length:(NSUInteger)size];
        } else if (length > offset && length < offset + size) {
            NSRange range = NSMakeRange((NSUInteger)offset, (NSUInteger)(length - offset));
            [data replaceBytesInRange:range withBytes:pv length:(NSUInteger)size];
        } else {
            NSRange range = NSMakeRange((NSUInteger)offset, (NSUInteger)size);
            [data replaceBytesInRange:range withBytes:pv length:(NSUInteger)size];
        }
        *ptrBytesWritten = size;
    }
};

typedef NS_ENUM(NSInteger, CZIDocumentSourceType) {
    CZIDocumentSourceTypeData,
    CZIDocumentSourceTypeFile
};

CZIDocumentImageRepresentationOption const CZIDocumentImageScalingOption = @"CZIDocumentImageScalingOption";

@interface CZIDocument () {
    NSMutableData *_data;
    NSString *_path;
    CZIDocumentSourceType _sourceType;
    std::shared_ptr<libCZI::ICZIReader> _reader;
    std::shared_ptr<libCZI::ICziWriter> _writer;
    std::shared_ptr<CZIImageOutputStream> _stream;
}

@end

@implementation CZIDocument

- (instancetype)init {
    self = [super init];
    if (self) {
        _data = [NSMutableData data];
        _sourceType = CZIDocumentSourceTypeData;
    }
    return self;
}

- (instancetype)initWithFile:(NSString *)path {
    self = [super init];
    if (self) {
        _path = [path copy];
        _sourceType = CZIDocumentSourceTypeFile;
    }
    return self;
}

- (instancetype)initWithData:(NSData *)data {
    self = [super init];
    if (self) {
        _data = [data mutableCopy];
        _sourceType = CZIDocumentSourceTypeData;
    }
    return self;
}

- (BOOL)createAndOpenReader {
    switch (_sourceType) {
        case CZIDocumentSourceTypeData:
            if (_data.length > 0) {
                if (_writer != nullptr) {
                    _writer->Close();
                    _writer = nullptr;
                }
                auto stream = std::make_shared<CZIImageInputStream>(_data);
                _reader = libCZI::CreateCZIReader();
                _reader->Open(stream);
                return YES;
            } else {
                return NO;
            }
        case CZIDocumentSourceTypeFile:
            if ([[NSFileManager defaultManager] fileExistsAtPath:_path]) {
                const std::wstring& wstring = [_path cz_wstring];
                wchar_t *file = (wchar_t *)(wstring.c_str());
                auto stream = libCZI::CreateStreamFromFile(file);
                _reader = libCZI::CreateCZIReader();
                _reader->Open(stream);
                return YES;
            } else {
                return NO;
            }
    }
}

- (BOOL)createWriter {
    switch (_sourceType) {
        case CZIDocumentSourceTypeData: {
            auto stream = std::make_shared<CZIImageOutputStream>(_data);
            _writer = libCZI::CreateCZIWriter();
            _writer->Create(stream, nullptr);
            return YES;
        }
        case CZIDocumentSourceTypeFile: {
            const std::wstring& wstring = [_path cz_wstring];
            wchar_t *file = (wchar_t *)(wstring.c_str());
            auto stream = libCZI::CreateOutputStreamForFile(file, true);
            _writer = libCZI::CreateCZIWriter();
            _writer->Create(stream, nullptr);
            return YES;
        }
    }
}

- (void)dealloc {
    if (_reader != nullptr) {
        _reader->Close();
        _reader = nullptr;
    }
    if (_writer != nullptr) {
        _writer->Close();
        _writer = nullptr;
    }
}

@end

@implementation CZIDocument (ReadOperations)

- (CZIMetadata *)metadata {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return nil;
    }
    
    auto mds = _reader->ReadMetadataSegment();
    auto md = mds->CreateMetaFromMetadataSegment();
    const std::string& cstring = md->GetXml();
    NSString *string = [NSString stringWithUTF8String:cstring.c_str()];
    CZIMetadata *metadata = [[CZIMetadata alloc] initWithXMLString:string];
    return metadata;
}

- (UIImage *)imageForSubBlockAtIndex:(NSInteger)index {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return nil;
    }
    
    auto subBlock = _reader->ReadSubBlock((int)index);
    auto bitmap = subBlock->CreateBitmap();
    CGImageRef cgImage = CZI_CGImageCreateWithBitmapData(bitmap);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return image;
}


- (CGSize)imageSize {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return CGSizeZero;
    }
    
    CGSize imageSize = CGSizeZero;
    _reader->EnumerateSubBlocks([&imageSize](int index, const libCZI::SubBlockInfo& info) {
        auto physicalSize = info.physicalSize;
        imageSize = CGSizeMake(physicalSize.w, physicalSize.h);
        return false;
    });
    
    return imageSize;
}

- (NSInteger)channelCount {
    CZIMetadata *metadata = self.metadata;
    CZIMetadataNode *channelsNode = metadata.root.displaySetting.channels;
    return channelsNode.childNodes.count;
}

- (NSData *)thumbnail {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return nil;
    }
    
    int thumbnailAttachmentIndex = -1;
    _reader->EnumerateAttachments([&thumbnailAttachmentIndex](int index, const libCZI::AttachmentInfo& info) -> bool {
        if (info.name == "Thumbnail") {
            thumbnailAttachmentIndex = index;
            return false;
        } else {
            return true;
        }
    });
    
    if (thumbnailAttachmentIndex == -1) {
        return nil;
    }
    
    size_t size;
    auto data = _reader->ReadAttachment(thumbnailAttachmentIndex)->GetRawData(&size);
    return [NSData dataWithBytes:data.get() length:size];
}

- (NSData *)imageAnalysis {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return nil;
    }
    
    int imageAnalysisAttachmentIndex = -1;
    _reader->EnumerateAttachments([&imageAnalysisAttachmentIndex](int index, const libCZI::AttachmentInfo& info) -> bool {
        if (info.name == "ImageAnalysis:1_t0z0c0") {
            imageAnalysisAttachmentIndex = index;
            return false;
        } else {
            return true;
        }
    });
    
    if (imageAnalysisAttachmentIndex == -1) {
        return nil;
    }
    
    size_t size;
    auto data = _reader->ReadAttachment(imageAnalysisAttachmentIndex)->GetRawData(&size);
    return [NSData dataWithBytes:data.get() length:size];
}

- (NSData *)dataRepresentation {
    if (_writer == nullptr && [self createWriter] == NO) {
        return nil;
    }
    
    _writer->Close();
    _writer = nullptr;
    return [_data copy];
}

- (UIImage *)imageRepresentation {
    return [self imageRepresentationWithOptions:nil];
}

- (UIImage *)imageRepresentationWithOptions:(NSDictionary<CZIDocumentImageRepresentationOption, id> *)options {
    if (_reader == nullptr && [self createAndOpenReader] == NO) {
        return nil;
    }
    
    auto statistics = _reader->GetStatistics();
    
    auto mds = _reader->ReadMetadataSegment();
    auto md = mds->CreateMetaFromMetadataSegment();
    auto docInfo = md->GetDocumentInfo();
    auto displaySettings = docInfo->GetDisplaySettings();
    
    std::vector<std::shared_ptr<libCZI::IChannelDisplaySetting>> activeChannels;
    displaySettings->EnumChannels([&] (int channelIndex) {
        auto channelDisplaySettings = displaySettings->GetChannelDisplaySettings(channelIndex);
        activeChannels.push_back(channelDisplaySettings);
        
        return true;
    });
    
//    if (activeChannels.size() <= 1) {
//        CGImageRef image;
//        _reader->EnumerateSubBlocks([&self, &image](int index, const libCZI::SubBlockInfo& info) {
//            auto block = _reader->ReadSubBlock(index);
//            auto bitmap = block->CreateBitmap();
//            image = CZI_CGImageCreateWithBitmapData(bitmap);
//            return false;
//        });
//        return image;
//        auto accessor = _reader->CreateSingleChannelTileAccessor();
//        libCZI::CDimCoordinate planeCoord {{ libCZI::DimensionIndex::C, 1 }};
//        auto multiTileComposit = accessor->Get(statistics.boundingBox, &planeCoord, nullptr);
//        return [self createCGImageWithBitmapData:multiTileComposit];
//    } else {
        std::vector<std::shared_ptr<libCZI::IBitmapData>> actvChBms;
        int index = 0;
        std::map<int, int> activeChNoToChIdx;
        auto accessor = _reader->CreateSingleChannelScalingTileAccessor();
        float scaling = options[CZIDocumentImageScalingOption] ? [options[CZIDocumentImageScalingOption] floatValue] : 1.0f;
        libCZI::CDisplaySettingsHelper::EnumEnabledChannels(displaySettings.get(), [&](int chIdx) -> bool {
            libCZI::CDimCoordinate planeCoord { { libCZI::DimensionIndex::C, chIdx } };
            actvChBms.emplace_back(accessor->Get(statistics.boundingBox, &planeCoord, scaling, nullptr));
            activeChNoToChIdx[chIdx] = index++;
            return true;
        });
    
        libCZI::CDisplaySettingsHelper dspHlp;
        dspHlp.Initialize(displaySettings.get(), [&](int chIdx) -> libCZI::PixelType {
            return actvChBms[activeChNoToChIdx[chIdx]]->GetPixelType();
        });
    
        if (dspHlp.GetActiveChannelsCount() == 0) {
            return nil;
        }
        auto mcComposite = libCZI::Compositors::ComposeMultiChannel_Bgr24(dspHlp.GetActiveChannelsCount(), std::begin(actvChBms), dspHlp.GetChannelInfosArray());
        CGImageRef cgImage = CZI_CGImageCreateWithBitmapData(mcComposite);
        UIImage *image = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        return image;
//    }
}

@end

@implementation CZIDocument (WriteOperations)

- (void)setMetadata:(CZIMetadata *)metadata {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    NSData *data = [metadata.XMLStringRepresentation dataUsingEncoding:NSUTF8StringEncoding];
    libCZI::WriteMetadataInfo metadataInfo{ nullptr };
    metadataInfo.szMetadata = (const char *)data.bytes;
    metadataInfo.szMetadataSize = data.length;
    _writer->SyncWriteMetadata(metadataInfo);
}

- (void)setMetadataFromFile:(NSString *)path {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    const std::wstring& wstring = [path cz_wstring];
    wchar_t *file = (wchar_t *)(wstring.c_str());
    auto stream = libCZI::CreateStreamFromFile(file);
    auto reader = libCZI::CreateCZIReader();
    reader->Open(stream);
    
    auto xml = reader->ReadMetadataSegment()->CreateMetaFromMetadataSegment()->GetXml();
}

- (void)addSubBlockWithCGImage:(CGImageRef)image atIndex:(NSInteger)index {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    libCZI::CDimCoordinate coord{ { libCZI::DimensionIndex::Z, 0 }, { libCZI::DimensionIndex::C, (int)index } };
    libCZI::AddSubBlockInfoStridedBitmap subBlock;
    subBlock.Clear();
    subBlock.coordinate = coord;
    subBlock.mIndexValid = true;
    subBlock.mIndex = 0;
    subBlock.x = 0;
    subBlock.y = 0;
    subBlock.logicalWidth = subBlock.physicalWidth = (int)CGImageGetWidth(image);
    subBlock.logicalHeight = subBlock.physicalHeight = (int)CGImageGetHeight(image);
    subBlock.PixelType = CZI_CGImageGetPixelType(image);
    subBlock.compression = libCZI::CompressionMode::UnCompressed;
    
    CFDataRef data = CZI_CFDataCopyFromCGImage(image);
    subBlock.ptrBitmap = CFDataGetBytePtr(data);
    subBlock.strideBitmap = (unsigned int)CGImageGetBytesPerRow(image);
    _writer->SyncAddSubBlock(subBlock);
    CFRelease(data);
}

- (void)addSubBlocksFromFile:(NSString *)path {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    const std::wstring& wstring = [path cz_wstring];
    wchar_t *file = (wchar_t *)(wstring.c_str());
    auto stream = libCZI::CreateStreamFromFile(file);
    auto reader = libCZI::CreateCZIReader();
    reader->Open(stream);
    auto writer = _writer;
    
    reader->EnumerateSubBlocks([&reader, &writer](int index, const libCZI::SubBlockInfo& info) {
        libCZI::AddSubBlockInfoLinewiseBitmap subBlock;
        subBlock.Clear();
        subBlock.coordinate = info.coordinate;
        subBlock.mIndexValid = true;
        subBlock.mIndex = 0;
        subBlock.x = 0;
        subBlock.y = 0;
        subBlock.logicalWidth = info.logicalRect.w;
        subBlock.logicalHeight = info.logicalRect.h;
        subBlock.physicalWidth = info.physicalSize.w;
        subBlock.physicalHeight = info.physicalSize.h;
        subBlock.PixelType = info.pixelType;
        subBlock.compression = libCZI::CompressionMode::UnCompressed;
        
        size_t size;
        auto data = reader->ReadSubBlock(index)->GetRawData(libCZI::ISubBlock::Data, &size);
        subBlock.getBitmapLine = [&subBlock, &data](int line) {
            int stride = subBlock.physicalWidth;
            return (unsigned char *)data.get() + stride * line;
        };
        
        writer->SyncAddSubBlock(subBlock);
        
        return true;
    });
    reader->Close();
}

- (void)addThumbnailAttachmentWithJPEGData:(NSData *)data {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    libCZI::AddAttachmentInfo attachment;
    attachment.contentGuid = CZI_GenerateGUID();
    attachment.SetContentFileType("JPG");
    attachment.SetName("Thumbnail");
    attachment.ptrData = data.bytes;
    attachment.dataSize = (unsigned int)data.length;
    
    _writer->SyncAddAttachment(attachment);
}

- (void)addImageAnalysisAttachmentWithData:(NSData *)data {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    libCZI::AddAttachmentInfo attachment;
    attachment.contentGuid = CZI_GenerateGUID();
    attachment.SetContentFileType("");
    attachment.SetName("ImageAnalysis:1_t0z0c0");
    attachment.ptrData = data.bytes;
    attachment.dataSize = data.length;
    
    _writer->SyncAddAttachment(attachment);
}

- (void)addAttachmentsFromFile:(NSString *)path {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    const std::wstring& wstring = [path cz_wstring];
    wchar_t *file = (wchar_t *)(wstring.c_str());
    auto stream = libCZI::CreateStreamFromFile(file);
    auto reader = libCZI::CreateCZIReader();
    reader->Open(stream);
    auto writer = _writer;
    
    reader->EnumerateAttachments([&reader, &writer](int index, const libCZI::AttachmentInfo& info) {
        size_t size;
        auto data = reader->ReadAttachment(index)->GetRawData(&size);
        
        libCZI::AddAttachmentInfo attachment;
        attachment.contentGuid = info.contentGuid;
        attachment.SetContentFileType(info.contentFileType);
        attachment.SetName(info.name.c_str());
        attachment.ptrData = data.get();
        attachment.dataSize = (int32_t)size;
        
        writer->SyncAddAttachment(attachment);
        
        return true;
    });
    reader->Close();
}

- (void)writeToFile {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    _writer->Close();
    _writer = nullptr;
}

- (void)writeToFile:(NSString *)path {
    if (_writer == nullptr && [self createWriter] == NO) {
        return;
    }
    
    _writer->Close();
    _writer = nullptr;
    NSData *data = [_data copy];
    
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path isDirectory:NO];
    [data writeToFile:url.relativePath atomically:YES];
}

@end
