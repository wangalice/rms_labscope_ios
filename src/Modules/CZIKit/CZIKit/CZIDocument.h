//
//  CZIDocument.h
//  CZIKit
//
//  Created by Li, Junlin on 4/29/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CZIMetadata.h"

typedef NSString * CZIDocumentImageRepresentationOption;
extern CZIDocumentImageRepresentationOption const CZIDocumentImageScalingOption;

@interface CZIDocument : NSObject

- (instancetype)init;
- (instancetype)initWithFile:(NSString *)path;
- (instancetype)initWithData:(NSData *)data;

@end

@interface CZIDocument (ReadOperations)

- (CZIMetadata *)metadata;

- (UIImage *)imageForSubBlockAtIndex:(NSInteger)index;

- (CGSize)imageSize;
- (NSInteger)channelCount;

- (NSData *)thumbnail;
- (NSData *)imageAnalysis;

- (NSData *)dataRepresentation;

- (UIImage *)imageRepresentation;
- (UIImage *)imageRepresentationWithOptions:(NSDictionary<CZIDocumentImageRepresentationOption, id> *)options;

@end

@interface CZIDocument (WriteOperations)

- (void)setMetadata:(CZIMetadata *)metadata;
//- (void)setMetadataFromFile:(NSString *)path;

- (void)addSubBlockWithCGImage:(CGImageRef)image atIndex:(NSInteger)index;
- (void)addSubBlocksFromFile:(NSString *)path;

- (void)addThumbnailAttachmentWithJPEGData:(NSData *)data;
- (void)addImageAnalysisAttachmentWithData:(NSData *)data;
- (void)addAttachmentsFromFile:(NSString *)path;

- (void)writeToFile;
- (void)writeToFile:(NSString *)path;

@end
