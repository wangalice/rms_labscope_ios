//
//  CZIMetadata.h
//  CZIKit
//
//  Created by Li, Junlin on 11/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZIMetadataNode.h"
#import "CZIMetadataNode+Specification.h"

@interface CZIMetadata : NSObject <NSCopying, NSMutableCopying>

@property (nonatomic, readonly, strong) CZIMetadataNode *document;
@property (nonatomic, readonly, strong) CZIMetadataNode *root;

+ (instancetype)metadata;
- (instancetype)initWithXMLString:(NSString *)xmlString;
- (instancetype)initWithJSONString:(NSString *)jsonString;

- (NSString *)XMLStringRepresentation;
- (NSString *)JSONStringRepresentation;

@end
