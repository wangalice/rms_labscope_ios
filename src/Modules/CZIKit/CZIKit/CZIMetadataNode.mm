//
//  CZIMetadataNode.m
//  CZIKit
//
//  Created by Li, Junlin on 11/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"
#import "CZIMetadataNodePrivate.h"
#import "CZIMetadataNodeAttributePrivate.h"
#import "NSString+WString.h"
#include <objc/runtime.h>
#include "pugixml.hpp"

@interface CZIMetadataNode ()

@property (nonatomic, readonly, assign) pugi::xml_node node;

@end

@implementation CZIMetadataNode

@synthesize nodeAttribute = _nodeAttribute;
@synthesize childNodes = _childNodes;

- (instancetype)initWithNode:(pugi::xml_node)node {
    self = [super init];
    if (self) {
        _node = node;
    }
    return self;
}

- (instancetype)childNodeWithName:(NSString *)name {
    return [self childNodeWithName:name index:0];
}

- (instancetype)childNodeWithName:(NSString *)name index:(NSInteger)index {
    const std::wstring& wstring = [name cz_wstring];
    auto wname = (wchar_t *)wstring.c_str();
    pugi::xml_node child;
    for (NSInteger i = 0; i <= index; i++) {
        if (i == 0) {
            child = self.node.child(wname);
        } else {
            child = child.next_sibling(wname);
        }
    }
    return [[[self class] alloc] initWithNode:child];
}

- (NSDictionary *)dictionaryRepresentation {
    if (self.isEmptyNode) {
        return nil;
    }
    
    if (self.nodeName.length == 0 && self.childNodes.count > 0) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        for (CZIMetadataNode *childNode in self.childNodes) {
            NSDictionary *childDictionary = [childNode dictionaryRepresentation];
            if (childDictionary && childDictionary.allKeys.count == 1) {
                [dictionary addEntriesFromDictionary:childDictionary];
            }
        }
        return [dictionary copy];
    }
    
    if (self.childNodes.count == 0) {
        NSString *stringValue = self.stringValue;
        return stringValue ? @{self.nodeName : stringValue} : nil;
    }
    
    BOOL usesArray = YES;
    NSString *signularNodeName = [self.nodeName substringToIndex:self.nodeName.length - 1];
    for (CZIMetadataNode *childNode in self.childNodes) {
        if (![childNode.nodeName isEqualToString:signularNodeName]) {
            usesArray = NO;
            break;
        }
    }
    
    if (usesArray) {
        NSMutableArray *array = [NSMutableArray array];
        for (CZIMetadataNode *childNode in self.childNodes) {
            NSDictionary *dictionary = [childNode dictionaryRepresentation];
            if (dictionary.allValues.count == 1 && [dictionary.allValues.firstObject isKindOfClass:[NSDictionary class]]) {
                [array addObject:dictionary.allValues.firstObject];
            }
        }
        return @{self.nodeName : [array copy]};
    } else {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        if (self.nodeAttribute.id.length > 0) {
            dictionary[@"Id"] = self.nodeAttribute.id;
        }
        if (self.nodeAttribute.name.length > 0) {
            dictionary[@"Name"] = self.nodeAttribute.name;
        }
        for (CZIMetadataNode *childNode in self.childNodes) {
            NSDictionary *childDictionary = [childNode dictionaryRepresentation];
            if (childDictionary && childDictionary.allKeys.count == 1) {
                [dictionary addEntriesFromDictionary:childDictionary];
            }
        }
        return @{self.nodeName : [dictionary copy]};
    }
}

#pragma mark - Subscripting

- (instancetype)objectAtIndexedSubscript:(NSUInteger)index {
    NSString *name = [NSString cz_stringWithWString:self.node.name()];
    if (name.length < 2) {
        return [self childNodeWithName:@"" index:index];
    }
    
    name = [name substringToIndex:name.length - 1];
    CZIMetadataNode *node = [self childNodeWithName:name index:index];
    return node;
}

#pragma mark - Fast Enumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id  _Nullable __unsafe_unretained [])buffer count:(NSUInteger)len {
    return [self.childNodes countByEnumeratingWithState:state objects:buffer count:len];
}

#pragma mark - Method Forwarding

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    Method method = class_getInstanceMethod([self class], @selector(childNodeWithName:));
    const char *encoding = method_getTypeEncoding(method);
    return [NSMethodSignature signatureWithObjCTypes:encoding];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    NSString *nodeName = NSStringFromSelector(anInvocation.selector);
    if (nodeName.length == 0) {
        return [super forwardInvocation:anInvocation];
    }
    
    nodeName = [[[nodeName substringToIndex:1] uppercaseString] stringByAppendingString:[nodeName substringFromIndex:1]];
    anInvocation.selector = @selector(childNodeWithName:);
    [anInvocation setArgument:&nodeName atIndex:2];
    [anInvocation retainArguments];
    [anInvocation invoke];
}

#pragma mark - Node Value

- (BOOL)isEmptyNode {
    return self.node.empty();
}

- (NSString *)nodeName {
    const wchar_t *wstring = self.node.name();
    return [NSString cz_stringWithWString:wstring];
}

- (CZIMetadataNodeAttribute *)nodeAttribute {
    if (_nodeAttribute == nil) {
        _nodeAttribute = [[CZIMetadataNodeAttribute alloc] initWithNode:self.node];
    }
    return _nodeAttribute;
}

- (CZIMetadataNode *)rootNode {
    return [[CZIMetadataNode alloc] initWithNode:self.node.root()];
}

- (NSArray<CZIMetadataNode *> *)childNodes {
    if (_childNodes == nil) {
        NSMutableArray<CZIMetadataNode *> *childNodes = [NSMutableArray array];
        for (pugi::xml_node child = self.node.first_child(); child; child = child.next_sibling()) {
            if (child.type() != pugi::xml_node_type::node_pcdata && child.type() != pugi::xml_node_type::node_cdata) {
                CZIMetadataNode *node = [[CZIMetadataNode alloc] initWithNode:child];
                [childNodes addObject:node];
            }
        }
        _childNodes = [childNodes copy];
    }
    return _childNodes;
}

#pragma mark - Access Value

- (NSString *)stringValue {
    const wchar_t *wstring = self.node.text().as_string();
    return [NSString cz_stringWithWString:wstring];
}

- (BOOL)boolValue {
    return self.node.text().as_bool();
}

- (NSInteger)integerValue {
    return self.node.text().as_int();
}

- (float)floatValue {
    return self.node.text().as_float();
}

- (double)doubleValue {
    return self.node.text().as_double();
}

- (NSDate *)dateValue {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:self.stringValue];
    if (date == nil) {
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZZ";
        date = [dateFormatter dateFromString:self.stringValue];
    }
    return date;
}

- (CGPoint)pointValue {
    NSArray<NSString *> *strings = [self.stringValue componentsSeparatedByString:@","];
    if (strings.count >= 2) {
        CGFloat x = strings[0].floatValue;
        CGFloat y = strings[1].floatValue;
        return CGPointMake(x, y);
    } else {
        return CGPointZero;
    }
}

- (NSArray<NSValue *> *)pointsValue {
    NSMutableArray<NSValue *> *points = [NSMutableArray array];
    NSArray<NSString *> *strings = [self.stringValue componentsSeparatedByString:@","];
    for (NSInteger index = 0; index < strings.count / 2; index++) {
        CGFloat x = strings[index * 2].floatValue;
        CGFloat y = strings[index * 2 + 1].floatValue;
        CGPoint point = CGPointMake(x, y);
        [points addObject:[NSValue valueWithCGPoint:point]];
    }
    return [points copy];
}

- (UIColor *)colorValue {
    if (self.stringValue.length != 9) {
        return nil;
    }
    
    NSString *stringValue = [self.stringValue stringByReplacingOccurrencesOfString:@"#" withString:@"0x"];
    NSScanner *scanner = [[NSScanner alloc] initWithString:stringValue];
    unsigned int hexValue;
    [scanner scanHexInt:&hexValue];
    UInt8 alpha = ((hexValue & 0xFF000000) >> 24);
    UInt8 red = ((hexValue & 0xFF0000) >> 16);
    UInt8 green = ((hexValue & 0xFF00) >> 8);
    UInt8 blue = (hexValue & 0xFF);
    return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:alpha / 255.0];
}

@end
