//
//  CZIMetadata.m
//  CZIKit
//
//  Created by Li, Junlin on 11/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadata.h"
#import "CZIMetadataPrivate.h"
#import "CZIMutableMetadata.h"
#import "CZIMetadataNodePrivate.h"
#import "NSString+WString.h"
#import "NSDictionary+XML.h"
#include <iostream>
#include "pugixml.hpp"

struct metadata_writer: pugi::xml_writer {
    std::string result;
    
    virtual void write(const void *data, size_t size) {
        result += std::string(static_cast<const char *>(data), size);
    }
};

@interface CZIMetadata () {
    pugi::xml_document _doc;
}

@end

@implementation CZIMetadata

@synthesize document = _document;
@synthesize root = _root;

+ (instancetype)metadata {
    return [[self alloc] init];
}

- (instancetype)init {
    return [self initWithXMLString:@""];
}

- (instancetype)initWithXMLString:(NSString *)xmlString {
    self = [super init];
    if (self) {
        NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
        _doc.load_buffer(data.bytes, data.length);
    }
    return self;
}

- (instancetype)initWithJSONString:(NSString *)jsonString {
    self = [super init];
    if (self) {
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if ([json isKindOfClass:[NSDictionary class]]) {
            [json cz_writeToXMLNode:_doc];
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithXMLString:self.XMLStringRepresentation];
}

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [[CZIMutableMetadata alloc] initWithXMLString:self.XMLStringRepresentation];
}

- (pugi::xml_document&)doc {
    return _doc;
}

- (CZIMetadataNode *)document {
    if (_document == nil) {
        _document = [[CZIMetadataNode alloc] initWithNode:_doc];
    }
    return _document;
}

- (CZIMetadataNode *)root {
    if (_root == nil) {
        _root = self.document.imageDocument.metadata;
    }
    return _root;
}

- (NSString *)XMLStringRepresentation {
    metadata_writer writer;
    _doc.print(writer);
    return [NSString stringWithUTF8String:writer.result.c_str()];
}

- (NSString *)JSONStringRepresentation {
    NSDictionary *dictionary = [self.document dictionaryRepresentation];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

@end
