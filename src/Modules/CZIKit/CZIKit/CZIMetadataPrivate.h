//
//  CZIMetadataPrivate.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadata.h"
#include "pugixml.hpp"

@interface CZIMetadata (CZIMetadataPrivate)

@property (nonatomic, readonly, assign) pugi::xml_document& doc;

@end
