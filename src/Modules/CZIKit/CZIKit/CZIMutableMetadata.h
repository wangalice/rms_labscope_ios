//
//  CZIMutableMetadata.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadata.h"
#import "CZIMutableMetadataNode.h"

@interface CZIMutableMetadata : CZIMetadata <NSCopying>

@property (nonatomic, readonly, strong) CZIMutableMetadataNode *document;
@property (nonatomic, readonly, strong) CZIMutableMetadataNode *root;

@end
