//
//  CZIMutableMetadataNodeAttribute.m
//  CZIKit
//
//  Created by Li, Junlin on 4/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIMutableMetadataNodeAttribute.h"
#import "CZIMetadataNodeAttributePrivate.h"
#import "NSString+WString.h"
#include "pugixml.hpp"

@implementation CZIMutableMetadataNodeAttribute

@dynamic id;
@dynamic name;
@dynamic type;

- (void)setId:(NSString *)id {
    auto attribute = self.node.attribute(L"Id");
    if (attribute == nil) {
        attribute = self.node.append_attribute(L"Id");
    }
    const std::wstring& wstring = [id cz_wstring];
    attribute.set_value((wchar_t *)wstring.c_str());
}

- (void)setName:(NSString *)name {
    auto attribute = self.node.attribute(L"Name");
    if (attribute == nil) {
        attribute = self.node.append_attribute(L"Name");
    }
    const std::wstring& wstring = [name cz_wstring];
    attribute.set_value((wchar_t *)wstring.c_str());
}

- (void)setType:(NSString *)type {
    auto attribute = self.node.attribute(L"Type");
    if (attribute == nil) {
        attribute = self.node.append_attribute(L"Type");
    }
    const std::wstring& wstring = [type cz_wstring];
    attribute.set_value((wchar_t *)wstring.c_str());
}

@end
