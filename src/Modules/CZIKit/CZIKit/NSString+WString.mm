//
//  NSString+WString.m
//  CZIKit
//
//  Created by Li, Junlin on 11/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSString+WString.h"

@implementation NSString (WString)

+ (instancetype)cz_stringWithWString:(const wchar_t *)wstring {
    auto wstringt = std::wstring(wstring);
    NSString *string = [[NSString alloc] initWithBytes:wstringt.data() length:wstringt.length() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    return string;
}

- (const std::wstring)cz_wstring {
    NSData *data = [self dataUsingEncoding:NSUTF32LittleEndianStringEncoding];
    auto wstring = std::wstring((wchar_t *)data.bytes, data.length / sizeof(wchar_t));
    return wstring;
}

@end
