//
//  CZIMetadataNodeAttribute.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZIMetadataNodeAttribute : NSObject

@property (nonatomic, readonly, copy) NSString *id;
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *type;

@end
