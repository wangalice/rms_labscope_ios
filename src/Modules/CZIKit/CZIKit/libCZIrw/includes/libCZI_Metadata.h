//******************************************************************************
// 
// libCZI is a reader for the CZI fileformat written in C++
// Copyright (C) 2017  Zeiss Microscopy GmbH
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// To obtain a commercial version please contact Zeiss Microscopy GmbH.
// 
//******************************************************************************

#pragma once

#include "libCZI_DimCoordinate.h"
#include "libCZI_Pixels.h"
#include <limits>
#include <string>
#include <vector>
#include <functional>
#include <memory>
#include <cmath>

namespace libCZI
{
	struct SubBlockStatistics;

	/// This structure specifies the information in an XSD-DateTime field (cf. https://www.w3schools.com/XML/schema_dtypes_date.asp).
	struct LIBCZI_API XmlDateTime
	{
		int sec;   ///< Seconds after the minute - [0, 60] including leap second
		int min;   ///< minutes after the hour - [0, 59]
		int hour;  ///< hours since midnight - [0, 23]
		int mday;  ///< day of the month - [1, 31]
		int mon;   ///< months since January - [0, 11]
		int year;  ///< year [-9999 - 9999]

		bool isUTC;///< True if this object is specifying the time-date in UTC.

		/// The hours of the timezone-offset. If greater than 24 or less than -24, it indicates an invalid timezone-offset.
		int offsetHours;

		/// The minutes of the timezone-offset. If greater than 60 or negative, it indicates an invalid timezone-offset.
		int offsetMinutes;

		/// Clears this object to its blank/initial state.
		void Clear()
		{
			this->sec = this->min = this->hour = this->mday = this->mon = this->year = 0;
			this->isUTC = false;
			this->offsetHours = (std::numeric_limits<int>::min)();
			this->offsetMinutes = (std::numeric_limits<int>::min)();
		}

		/// Query if this object uses a "time zone offset". This is the case of the fields offsetHours and offsetMinutes
		/// contain valid values and "isUTC" is false.
		/// \return True if a "time zone offset" is specified and it is used, false if not.
		bool HasTimeZoneOffset() const
		{
			if (this->isUTC)
			{
				return false;
			}

			if (this->offsetHours <= -24 || this->offsetHours >= 24 || this->offsetMinutes >= 60 || this->offsetMinutes < 0)
			{
				return false;
			}

			return true;
		}

		/// Converts the information into a string conforming to the XSD-specification for the DateTime data type.
		/// If this object does not contain valid information, an exception of type illegal_argument is thrown.
		/// \return This object as a string.
		std::string ToXmlString() const;

		/// Converts the information into a string conforming to the XSD-specification for the DateTime data type.
		/// If this object does not contain valid information, an exception of type illegal_argument is thrown.
		/// \return This object as a string.
		std::wstring ToXmlWstring() const;

		/// Query if this object contains valid information.
		/// \return True if valid, false if not.
		bool IsValid() const;
	};

	/// General document information - corresponding to Information/Document.
	struct GeneralDocumentInfo
	{
		/// Default constructor - all fields are intially marked "invalid".
		LIBCZI_API GeneralDocumentInfo() : name_valid(false), title_valid(false), userName_valid(false), description_valid(false), comment_valid(false), keywords_valid(false), rating_valid(false), rating(0), creationDateTime_valid(false) {}
		bool name_valid;				///< Whether the field #name is valid.		
		std::wstring name;				///< Name of the document.
		bool title_valid;				///< Whether the field #title is valid.			
		std::wstring title;				///< Title of the document.
		bool userName_valid;			///< Whether the field #userName is valid.			
		std::wstring userName;			///< Name of the user who created the document.
		bool description_valid;			///< Whether the field #description is valid.			
		std::wstring description;		///< A text describing the document.
		bool comment_valid;				///< Whether the field #comment is valid.			
		std::wstring comment;			///< A text with comments on the document.
		bool keywords_valid;			///< Whether the field #keywords is valid.			
		std::wstring keywords;			///< List of keywords (should be separated by semicolons)
		bool rating_valid;				///< Whether the field #rating is valid.			
		int rating;						///< An integer specifying a "five-star-rating" (should be between 0 and 5).
		bool creationDateTime_valid;	///< Whether the field #creationDateTime is valid.			
		std::wstring creationDateTime;	///< The creation date of the document (formatted as xml-datatype "dateTime").

		/// Sets the name and marks it valid if the specified string is non-null and non-empty. Otherwise, the name is set to invalid.
		///
		/// \param sz The string to set as name.
		void LIBCZI_API SetName(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::name, &GeneralDocumentInfo::name_valid, sz); }

		/// Sets the name and marks it valid if the specified string is non-empty. Otherwise, the name is set to invalid.
		///
		/// \param str The string to set as name.
		void LIBCZI_API SetName(const std::wstring& str) { this->SetName(str.c_str()); }

		/// Sets the title and marks it valid if the specified string is non-null and non-empty. Otherwise, the title is set to invalid.
		///
		/// \param sz The string to set as title.
		void LIBCZI_API SetTitle(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::title, &GeneralDocumentInfo::title_valid, sz); }

		/// Sets the title and marks it valid if the specified string is non-empty. Otherwise, the title is set to invalid.
		///
		/// \param str The string to set as title.
		void LIBCZI_API SetTitle(const std::wstring& str) { this->SetTitle(str.c_str()); }

		/// Sets the username and marks it valid if the specified string is non-null and non-empty. Otherwise, the username is set to invalid.
		///
		/// \param sz The string to set as username.
		void LIBCZI_API SetUserName(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::userName, &GeneralDocumentInfo::userName_valid, sz); }

		/// Sets the username and marks it valid if the specified string is non-empty. Otherwise, the username is set to invalid.
		///
		/// \param str The string to set as username.
		LIBCZI_API void SetUserName(const std::wstring& str) { this->SetUserName(str.c_str()); }

		/// Sets the description and marks it valid if the specified string is non-null and non-empty. Otherwise, the description is set to invalid.
		///
		/// \param sz The string to set as description.
		LIBCZI_API void SetDescription(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::description, &GeneralDocumentInfo::description_valid, sz); }

		/// Sets the description and marks it valid if the specified string is non-empty. Otherwise, the description is set to invalid.
		///
		/// \param str The string to set as description.
		LIBCZI_API void SetDescription(const std::wstring& str) { this->SetDescription(str.c_str()); }

		/// Sets the comment and marks it valid if the specified string is non-null and non-empty. Otherwise, the comment is set to invalid.
		///
		/// \param sz The string to set as comment.
		LIBCZI_API void SetComment(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::comment, &GeneralDocumentInfo::comment_valid, sz); }

		/// Sets the comment and marks it valid if the specified string is non-empty. Otherwise, the comment is set to invalid.
		///
		/// \param str The string to set as comment.
		LIBCZI_API void SetComment(const std::wstring& str) { this->SetComment(str.c_str()); }

		/// Sets the keywords and marks it valid if the specified string is non-null and non-empty. Otherwise, the keywords is set to invalid.
		///
		/// \param sz The string to set as keywords.
		LIBCZI_API void SetKeywords(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::keywords, &GeneralDocumentInfo::keywords_valid, sz); }

		/// Sets the keywords and marks it valid if the specified string is non-empty. Otherwise, the keywords is set to invalid.
		///
		/// \param str The string to set as keywords.
		LIBCZI_API void SetKeywords(const std::wstring& str) { this->SetKeywords(str.c_str()); }

		/// Sets the rating and marks it as valid if the specified rating is non-negative. Otherwise, the rating is set to invalid.
		/// \param rating The rating.
		LIBCZI_API void SetRating(int rating) { this->rating = rating; this->rating_valid = rating >= 0 ? true : false; }

		/// Sets the creationDateTime and marks it valid if the specified string is non-null and non-empty. Otherwise, the creationDateTime is set to invalid.
		///
		/// \param sz The string to set as creationDateTime.
		LIBCZI_API void SetCreationDate(const wchar_t* sz) { this->SetMember(&GeneralDocumentInfo::creationDateTime, &GeneralDocumentInfo::creationDateTime_valid, sz); }

		/// Sets the creationDateTime and marks it valid if the specified string is non-empty. Otherwise, the creationDateTime is set to invalid.
		///
		/// \param str The string to set as creationDateTime.
		LIBCZI_API void SetCreationDate(const std::wstring& str) { this->SetCreationDate(str.c_str()); }

		/// Sets the creationDateTime and marks it valid if the specified XmlDateTime-struct pointer is non-null and valid. Otherwise, the creationDateTime is set to invalid.
		///
		/// \param dateTime The XmlDateTime-struct to set as creationDateTime.
		LIBCZI_API void SetCreationDate(const XmlDateTime* dateTime) { if (dateTime != nullptr && dateTime->IsValid()) { this->SetCreationDate(dateTime->ToXmlWstring()); } else { this->SetCreationDate(static_cast<const wchar_t*>(nullptr)); } }

		/// Sets all fields to "invalid".
		LIBCZI_API void Clear()
		{
			this->name_valid = this->title_valid = this->userName_valid = this->description_valid =
				this->comment_valid = this->keywords_valid = this->rating_valid = this->creationDateTime_valid = false;
		}
	private:
		void SetMember(std::wstring(GeneralDocumentInfo::*strMember), bool(GeneralDocumentInfo::*validMember), const wchar_t* str)
		{
			if (str == nullptr || *str == L'\0')
			{
				(this->*strMember).clear();
				this->*validMember = false;
			}
			else
			{
				this->*strMember = str;
				this->*validMember = true;
			}
		}
	};

	/// Scaling information - gives the size of a pixel.
	struct LIBCZI_API ScalingInfo
	{
		/// Default constructor - sets all members to invalid.
		ScalingInfo() : scaleX(std::numeric_limits<double>::quiet_NaN()), scaleY(std::numeric_limits<double>::quiet_NaN()), scaleZ(std::numeric_limits<double>::quiet_NaN()) {}
		double scaleX;	///< The length of a pixel in x-direction in the unit meters. If unknown/invalid, this value is numeric_limits<double>::quiet_NaN().
		double scaleY;	///< The length of a pixel in y-direction in the unit meters. If unknown/invalid, this value is numeric_limits<double>::quiet_NaN().
		double scaleZ;	///< The length of a pixel in y-direction in the unit meters. If unknown/invalid, this value is numeric_limits<double>::quiet_NaN().

		/// Query if this object's scaleX value is valid.
		///
		/// \return True if this object's scaleX value is valid, false if not.
		bool IsScaleXValid() const { return !std::isnan(this->scaleX) && !std::isinf(this->scaleX); }

		/// Query if this object's scaleY value is valid.
		///
		/// \return True if this object's scaleY value is valid, false if not.
		bool IsScaleYValid() const { return !std::isnan(this->scaleY) && !std::isinf(this->scaleY); }

		/// Query if this object's scaleZ value is valid.
		///
		/// \return True if this object's scaleZ value is valid, false if not.
		bool IsScaleZValid() const { return !std::isnan(this->scaleZ) && !std::isinf(this->scaleZ); }

		/// Queries if the specified scale value is valid.
		///
		/// \exception std::invalid_argument Thrown when an invalid argument error condition occurs.
		///
		/// \param d Identifies the scale-value to query, can be 'x', 'y' or 'z' (or uppercase).
		///
		/// \return True if the specified scale is valid, false if not.
		bool IsScaleValid(char d) const
		{
			switch (d)
			{
			case 'x':case'X': return this->IsScaleXValid();
			case 'y':case'Y': return this->IsScaleYValid();
			case 'z':case'Z': return this->IsScaleZValid();
			default: throw std::invalid_argument("invalid dimension");
			}
		}

		/// Gets the specified scale value.
		///
		/// \exception std::invalid_argument Thrown when an invalid argument error condition occurs.
		///
		/// \param d Identifies the scale-value to query, can be 'x', 'y' or 'z' (or uppercase).
		///
		/// \return The specified scale.
		double GetScale(char d) const
		{
			switch (d)
			{
			case 'x':case'X': return this->scaleX;
			case 'y':case'Y': return this->scaleY;
			case 'z':case'Z': return this->scaleZ;
			default: throw std::invalid_argument("invalid dimension");
			}
		}
	};

	/// Extends the scaling information by a "default unit format". Note that the value itself remains to be
	/// given in meter, here we just suggest a unit to display the scale.
	struct ScalingInfoEx : ScalingInfo
	{
		std::wstring defaultUnitFormatX;	///< The default unit-format for X.
		std::wstring defaultUnitFormatY;	///< The default unit-format for Y.
		std::wstring defaultUnitFormatZ;	///< The default unit-format for Z.

		/// Gets the specified default unit-format.
		///
		/// \exception std::invalid_argument Thrown when an invalid argument error condition occurs.
		///
		/// \param d Identifies the scale-value to query, can be 'x', 'y' or 'z' (or uppercase).
		///
		/// \return The specified scale.
		LIBCZI_API std::wstring GetDefaultUnitFormat(char d) const
		{
			switch (d)
			{
			case 'x':case'X': return this->defaultUnitFormatX;
			case 'y':case'Y': return this->defaultUnitFormatY;
			case 'z':case'Z': return this->defaultUnitFormatZ;
			default: throw std::invalid_argument("invalid dimension");
			}
		}
	};

	/// Base class for information about the dimension. (not yet implemented)
	class IDimensionInfo
	{
	public:

		/// Gets the dimension index.
		/// \return The dimension index.
		virtual DimensionIndex GetDimension() const = 0;

		/// Gets the interval.
		/// \param [out] start If non-null, it will receive the start index.
		/// \param [out] end   If non-null, it will receive the end index.
		virtual void GetInterval(int* start, int* end) const = 0;

		virtual ~IDimensionInfo() {}
	};

	class IChannelDisplaySetting;

	/// The display settings.
	class IDisplaySettings
	{
	public:
		/// The (normalized) control points of a spline.
		struct SplineControlPoint
		{
			double x;	///< The normalized x-coordinate of a spline control point.
			double y;	///< The normalized y-coordinate of a spline control point.
		};

		/// Values that represent the gradation curve modes.
		enum class GradationCurveMode
		{
			Linear,		///< The gradation curve is a straight line (from white point to black point).
			Gamma,		///< The gradation curve is defined by a gamma.
			Spline		///< The gradation curve is defined by piecewise splines.
		};

		/// The coefficients of a cubic spline defined by \f$a\,x^3 + b\,x^2 + c\,x + d =y\f$.
		struct CubicSplineCoefficients
		{
			double a; ///< The coefficient of the cube.
			double b; ///< The coefficient of the square.
			double c; ///< The coefficient of the linear term.
			double d; ///< The constant.

			/// Gets the coefficients by an index (where a is 0, b is 1, c is 2 and d is 3). If the index is out-of-range, the method returns NaN.
			///
			/// \param index The index of the coefficient to get. 
			///
			/// \return The specified coefficient if the index is valid, NaN otherwise.
			double Get(int index) const
			{
				switch (index)
				{
				case 0:		return this->a;
				case 1:		return this->b;
				case 2:		return this->c;
				case 3:		return this->d;
				default:	return std::numeric_limits<double>::quiet_NaN();
				}
			}
		};

		/// The defintion of the (piecewise) spline. The spline starts at <tt>xPos</tt> which is the normalized position (between 0 and 1).
		struct SplineData
		{
			/// The (normalized) position for which this spline definition is valid.
			double xPos;

			/// The spline coefficients for this piece.
			CubicSplineCoefficients coefficients;
		};

		/// Enum all channels (which are described by the display-settings object).
		///
		/// \param func The functor to be called (passing in the channel index). If the functor returns false, the 
		/// 			enumeration is stopped.
		virtual void EnumChannels(std::function<bool(int chIndex)> func) const = 0;

		/// Gets channel display settings for the specified channel. If the channel index is not valid, then
		/// an empty shared_ptr is returned.
		///
		/// \param chIndex The channel index.
		///
		/// \return The channel display settings object (if the channel index was valid), and empty shared_ptr otherwise.
		virtual std::shared_ptr<libCZI::IChannelDisplaySetting> GetChannelDisplaySettings(int chIndex) const = 0;

		virtual ~IDisplaySettings() {}
	};

	/// The display-settings for a channel.
	class IChannelDisplaySetting
	{
	public:

		/// Gets a boolean indicating whether the corresponding channel is 'active' in the multi-channel-composition.
		///
		/// \return True if the corresponding channel is 'active', false otherwise.
		virtual bool	GetIsEnabled() const = 0;

		/// Gets the weight of the channel (for multi-channel-composition).
		///
		/// \return The weight.
		virtual float	GetWeight() const = 0;

		/// Attempts to get the RGB24-tinting color for the corresponding channel. If tinting is not enabled, then
		/// this method will return false.
		///
		/// \param [out] pColor If tinting is enabled for the corresponding channel, then (if non-null) will receive the tinting-color.
		///
		/// \return True if tinting is enabled for the corresponding channel (and in this case <tt>pColor</tt> will be set), false otherwise (and <tt>pColor</tt> will not be set).
		virtual bool	TryGetTintingColorRgb8(libCZI::Rgb8Color* pColor) const = 0;

		/// Gets the black point and the white point.
		///
		/// \param [out] pBlack If non-null, the black point will be returned.
		/// \param [out] pWhite If non-null, the white point will be returned.
		virtual void	GetBlackWhitePoint(float* pBlack, float* pWhite) const = 0;

		/// Gets gradation curve mode.
		///
		/// \return The gradation curve mode.
		virtual IDisplaySettings::GradationCurveMode GetGradationCurveMode() const = 0;

		/// Attempts to get the gamma - this will only be available if gradation curve mode is <tt>Gamma</tt>.
		///
		/// \param [out] gamma If non-null and applicable, the gamma will be returned.
		///
		/// \return True if the corresponding channel uses gradation curve mode <tt>Gamma</tt> (and a value for gamma is available), false otherwise.
		virtual bool	TryGetGamma(float* gamma)const = 0;

		/// Attempts to get spline control points - this will only be available if gradation curve mode is <tt>Spline</tt>.
		/// \remark
		/// We make no promises that both the control-points and the spline-data are always available. It might be plausible that
		/// the spline is defined in a different way (different than control-points), so in that case only the "spline-data" would
		/// be available. So - be careful is using this interface in a context different thant "CZI-metadata" where it might be
		/// the case the 'TryGetSplineControlPoints' will fail but 'TryGetSplineData' might succeed.
		/// Maybe better should remove 'TryGetSplineData' from this interface.
		///
		/// \param [in,out] ctrlPts If non-null, the control points will be written to this vector.
		///
		/// \return True if it succeeds, false if it fails.
		virtual bool	TryGetSplineControlPoints(std::vector<libCZI::IDisplaySettings::SplineControlPoint>* ctrlPts) const = 0;

		/// Attempts to get the spline data - this will only be available if gradation curve mode is <tt>Spline</tt>.
		///
		/// \param [in,out] data If non-null, the spline data will be written to this vector.
		///
		/// \return True if it the corresponding channels uses gradation curve mode <tt>Spline</tt>, false otherwise.
		virtual bool	TryGetSplineData(std::vector<libCZI::IDisplaySettings::SplineData>* data) const = 0;

		virtual ~IChannelDisplaySetting() {}
	};

	/// The top-level interface for the CZI-metadata object.
	class ICziMultiDimensionDocumentInfo
	{
	public:

		/// Gets "general document information".
		/// \return The "general document information".
		virtual GeneralDocumentInfo GetGeneralDocumentInfo() const = 0;

		/// Gets "scaling information".
		/// \return The "scaling information".
		virtual libCZI::ScalingInfo GetScalingInfo() const = 0;

		/// Enumerate the dimensions (defined in the metadata).
		/// /remark
		/// Not yet implemented.
		/// \param enumDimensions The functor which will be called for each dimension. If the functor returns false, the enumeration is cancelled.
		virtual void EnumDimensions(std::function<bool(DimensionIndex)> enumDimensions) = 0;

		/// Gets the dimension information for the specified dimension.
		/// /remark
		/// Not yet implemented.
		/// \param dim The dimension to retrieve the information for.
		/// \return The dimension information if available.
		virtual std::shared_ptr<IDimensionInfo> GetDimensionInfo(DimensionIndex dim) = 0;

		/// Gets the display settings.
		/// \remark
		/// This method may return an empty shared_ptr in case that display-settings are not present in the metadata.
		/// \return The display settings object.
		virtual std::shared_ptr<IDisplaySettings> GetDisplaySettings() const = 0;

		virtual ~ICziMultiDimensionDocumentInfo() {}

		/// Gets a vector with all dimensions (found in metadata).
		/// \return The vector containing all dimensions.
		std::vector<DimensionIndex> GetDimensions()
		{
			std::vector<DimensionIndex> vec;
			this->EnumDimensions([&](DimensionIndex i)->bool {vec.push_back(i); return true; });
			return vec;
		}
	};

	/// Representation of the CZI-metadata.
	class ICziMetadata
	{
	public:
		/// Gets the metadata as an unprocessed UTF8-encoded XML-string.
		///
		/// \return The metadata (unprocessed UTF8-encoded XML).
		virtual std::string GetXml() = 0;

		/// Gets the "document information" part of the metadata.
		///
		/// \return The "document information".
		virtual std::shared_ptr<libCZI::ICziMultiDimensionDocumentInfo> GetDocumentInfo() = 0;

		virtual ~ICziMetadata() {}
	};

	/// This interface provides read-only access to an XML-node.
	class IXmlNodeRead
	{
	public:

		/// Attempts to get the attribute with the name specified by "attributeName". If it exists, the value is stored in
		/// "attribValue" (if it is non-null) and the return value is true. Otherwise, the return value is false.
		///
		/// \param 		    attributeName Name of the attribute.
		/// \param [out] attribValue   If non-null, the attribute value will be put here (if the attribute exists).
		///
		/// \return True if it succeeds, false if it fails.
		virtual bool TryGetAttribute(const wchar_t* attributeName, std::wstring* attribValue) const = 0;

		/// Enumerate the attributes in the node. The attribute-name and their respective value will be passed to the specified functor.
		///
		/// \param enumFunc The enumeration function. If the function returns false, the enumeration will be cancelled immediately.
		virtual void EnumAttributes(const std::function<bool(const std::wstring& attribName, const std::wstring& attribValue)>& enumFunc) const = 0;

		/// Attempts to get value of the XML-node. If the specified pointer "value" is non-null, the value will be put there.
		///
		/// \param [out] value If non-null, the value of the XML-node will be put here (if successful).
		///
		/// \return True if it succeeds, false if it fails.
		virtual bool TryGetValue(std::wstring* value) const = 0;

		virtual ~IXmlNodeRead() {};
	};

	class IXmlNodeRw;

	/// This interface provides write access to an XML-node.
	class LIBCZI_API IXmlNodeWrite
	{
	public:
		/// Gets or create a child node. The path is specified as node-names separated by slashes.
		/// At path "A/B/C" selects (or creates) a node-structure like this
		/// \code{.unparsed}
		/// <A>
		///	  <B>
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// Attributes can be specified with a node, in the form 'NodeName[attr1=abc,attr2=xyz]'. This will
		/// search for nodes with the specified attributes, and if not found, create one.
		/// In this example "A/B[Id=ab,Name=xy]/C" we will get
		/// \code{.unparsed}
		/// <A>
		///	  <B Id="ab" Name="xy">
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// \param path The path  (in UTF8-encoding).
		/// \return Either an existing node or a newly created one.
		virtual std::shared_ptr<IXmlNodeRw> GetOrCreateChildNode(const char* path) = 0;

		/// Gets an existing child node. The path is specified as node-names separated by slashes.
		/// At path "A/B/C" selects (or creates) a node-structure like this
		/// \code{.unparsed}
		/// <A>
		///	  <B>
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// Attributes can be specified with a node, in the form 'NodeName[attr1=abc,attr2=xyz]'. This will
		/// search for nodes with the specified attributes, and if not found, create one.
		/// In this example "A/B[Id=ab,Name=xy]/C" we will get
		/// \code{.unparsed}
		/// <A>
		///	  <B Id="ab" Name="xy">
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// \param path The path (in UTF8-encoding).
		/// \return The existing node conforming to the path if it exists, null otherwise.
		virtual std::shared_ptr<IXmlNodeRw> GetChildNode(const char* path) = 0;

		/// Appends a child node with the specified name.
		///
		/// \param name The name of the node to add.
		///
		/// \return The newly added node.
		virtual std::shared_ptr<IXmlNodeRw> AppendChildNode(const char* name) = 0;

		/// Sets the attribute with the specified name to the specified value.
		///
		/// \param name  The name of the attribute (as UTF8-encoded string).
		/// \param value The value (as UTF8-encoded string).
		virtual void SetAttribute(const char* name, const char* value) = 0;

		/// Sets the node value - which is specified as an UTF8-string.
		///
		/// \param str The UTF8-encoded string.
		virtual void SetValue(const char* str) = 0;

		/// Sets the node value to the specified string.
		///
		/// \param str The string.
		virtual void SetValue(const wchar_t* str) = 0;

		/// Sets value of the node with the specified integer.
		///
		/// \param value The integer to write into the node.
		virtual void SetValueI32(int value) = 0;

		/// Sets value of the node with the specified unsigned integer.
		///
		/// \param value The unsigned integer to write into the node.
		virtual void SetValueUI32(unsigned int value) = 0;

		/// Sets value of the node with the specified double.
		///
		/// \param value The double to write into the node.
		virtual void SetValueDbl(double value) = 0;

		/// Sets value of the node with the specified float.
		///
		/// \param value The float to write into the node.
		virtual void SetValueFlt(float value) = 0;

		/// Sets value of the node with the specified boolean.
		///
		/// \param value The boolean to write into the node.
		virtual void SetValueBool(bool value) = 0;

		/// Sets value of the node with the specified long integer.
		///
		/// \param value The long integer to write into the node.
		virtual void SetValueI64(long long value) = 0;

		/// Sets value of the node with the specified unsigned long integer.
		///
		/// \param value The unsigned long integer to write into the node.
		virtual void SetValueUI64(unsigned long long value) = 0;

		virtual ~IXmlNodeWrite() {};

		/// Sets the node value - which is specified as an UTF8-string.
		///
		/// \param str The UTF8-encoded string.
		void SetValue(const std::string& str) { this->SetValue(str.c_str()); }

		/// Sets the node value to the specified string.
		///
		/// \param str The string.
		void SetValue(const std::wstring& str) { this->SetValue(str.c_str()); }

		/// Gets or create a child node. The path is specified as node-names separated by slashes.
		/// At path "A/B/C" selects (or creates) a node-structure like this
		/// \code{.unparsed}
		/// <A>
		///	  <B>
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// Attributes can be specified with a node, in the form 'NodeName[attr1=abc,attr2=xyz]'. This will
		/// search for nodes with the specified attributes, and if not found, create one.
		/// In this example "A/B[Id=ab,Name=xy]/C" we will get
		/// \code{.unparsed}
		/// <A>
		///	  <B Id="ab" Name="xy">
		///     <C/>
		///	  </B>
		///	</A>
		/// \endcode
		/// \param path The path.
		/// \return Either an existing node or a newly created one.
		std::shared_ptr<IXmlNodeRw> GetOrCreateChildNode(const std::string& path) { return this->GetOrCreateChildNode(path.c_str()); }

		/// Gets a child node for the specified path.
		/// \param path Path of the childnode (in UTF8-encoding).
		/// \return The child node.
		std::shared_ptr<IXmlNodeRw> GetChildNode(const std::string& path) { return this->GetChildNode(path.c_str()); }
	};

	/// This interface combines read- and write-access for an XML-node.
	class IXmlNodeRw : public IXmlNodeRead, public IXmlNodeWrite
	{
	};

	/// A utility class intended for constructing CZI-metadata.
	class ICziMetadataBuilder
	{
	public:
		/// Gets the root node.
		///
		/// \return The root node.
		virtual std::shared_ptr<IXmlNodeRw> GetRootNode() = 0;

		/// Gets the metadata as an UTF8-encoded XML-string.
		///
		/// \return The metadata (UTF8-encoded XML).
		virtual std::string GetXml(bool withIndent = false) = 0;

		virtual ~ICziMetadataBuilder() {};
	};

	/// Helper functions useful for constructing CZI-metadata.
	class LIBCZI_API MetadataUtils
	{
	public:
		/// Writes the nodes ""Metadata/Information/Image/SizeX" and ""Metadata/Information/Image/SizeY".
		///
		/// \param [in,out] builder The metadata-builder object.
		/// \param 		    width   The width (=SizeX).
		/// \param 		    height  The height (=SizeY).
		static void WriteImageSizeInformation(libCZI::ICziMetadataBuilder* builder, int width, int height);

		/// Writes the node ""Metadata/Information/Image/SizeM".
		///
		/// \param [in,out] builder The metadata-builder object.
		/// \param 		    mSize   The M-size.
		static void WriteMIndexSizeInformation(libCZI::ICziMetadataBuilder* builder, int mSize);

		/// Writes the size-information for the specified dimension (to "Metadata/Information/Image/Size?").
		///
		/// \param [in,out] builder The metadata-builder object.
		/// \param 		    dim	    The dimension.
		/// \param 		    size    The size of the dimension.
		static void WriteDimensionSize(libCZI::ICziMetadataBuilder* builder, libCZI::DimensionIndex dim, int size);

		/// Uses the specified statistics-data in order to write the "size"-information-
		///
		/// \param [in,out] builder    The metadata-builder object.
		/// \param 		    statistics The subblock-statistics.
		static void WriteFillWithSubBlockStatistics(libCZI::ICziMetadataBuilder* builder, const libCZI::SubBlockStatistics& statistics);

		/// Writes the "dimension-T"-information ("Information/Dimensions/T") - this associates a timestamp with each T-coordinate. With this
		/// function we provide the timestamps for the equidistant case (= the difference between consecutive T-indices is constant).
		/// The difference is specified as "increment". It is possible to specify an offset for the first T-index, given as startOffset.
		/// An absolute time-point may be specified as startTime. 
		/// Example: say, startOffSet is 1 and increment is 2. Then T=0 is associated with 1, T=1 with 1+2, T=2 with 1+2*2, T=3 with 1+2*3 and so on.
		/// 
		/// \param [in,out] builder	    The metadata-builder object.
		/// \param 		    startTime   If non-null, the (absolute) start time.
		/// \param 		    startOffSet The offset for the first timepoint (in units of seconds). If numeric_limits<double>::quiet_NaN() is specified, this parameter is ignored.
		/// \param 		    increment   The increment (in units of seconds). If numeric_limits<double>::quiet_NaN() is specified, this parameter is ignored.
		static void WriteDimInfoT_Interval(libCZI::ICziMetadataBuilder* builder, const libCZI::XmlDateTime* startTime, double startOffSet, double increment);

		/// Writes the "dimension-T"-information ("Information/Dimensions/T") - this associates a timestamp with each T-coordinate. With this function
		/// we provide a list of timestamps. 
		/// 
		/// \param [in,out] builder		   The metadata-builder object.
		/// \param 		    startTime	   If non-null, the (absolute) start time.
		/// \param 		    funcGetOffsets A function which is called for providing the timestamp. The integer parameter is incremented with each call,
		/// 							   starting with 0. The return value is the timestamp (in units of seconds). If numeric_limits<double>::quiet_NaN() is
		/// 							   returned, the enumeration is ended.
		static void WriteDimInfoT_List(libCZI::ICziMetadataBuilder* builder, const libCZI::XmlDateTime* startTime, std::function<double(int)> funcGetOffsets);

		/// Helper function in order to write the Dimension-Z information - as a an equal-distance sequence.
		///
		/// \param [in] builder			The metadata-builder object.
		/// \param 		    startPos    The start position.
		/// \param 		    startOffSet The offset of the first item in the sequence.
		/// \param 		    increment   The increment.
		static void WriteDimInfoZ_Interval(libCZI::ICziMetadataBuilder* builder, double startPos, double startOffSet, double increment);

		/// Helper function in order to write the Dimension-Z information - as a an explicit list.
		///
		/// \param [in] builder		   The metadata-builder object.
		/// \param 	    startPos	   The start position.
		/// \param 	    funcGetOffsets The function which is called to retrieve the offset. The argument is incremented with each call (starting with 0);
		/// 						   a return value of infinity or NaN will end the enumeration.
		static void WriteDimInfoZ_List(libCZI::ICziMetadataBuilder* builder, double startPos, std::function<double(int)> funcGetOffsets);

		/// Helper function which writes the information from the specified "GeneralDocumentInfo" into the metadata-builder object.
		///
		/// \param [in] builder		The metadata-builder object.
		/// \param 		    info    The general-document-information to be written into the metadata-builder object.
		static void WriteGeneralDocumentInfo(libCZI::ICziMetadataBuilder* builder, const libCZI::GeneralDocumentInfo& info);

		/// Helper function in order to write scaling-information into the metadata-builder object.
		///
		/// \param [in] builder		The metadata-builder object.
		/// \param 		scalingInfo The scaling  to be written into the metadata-builder object.
		static void WriteScalingInfo(libCZI::ICziMetadataBuilder* builder, const libCZI::ScalingInfo& scalingInfo);

		/// Helper function in order to write scaling-information into the metadata-builder object.
		///
		/// \param [in] builder		The metadata-builder object.
		/// \param 		scalingInfo The scaling  to be written into the metadata-builder object.
		static void WriteScalingInfoEx(libCZI::ICziMetadataBuilder* builder, const libCZI::ScalingInfoEx& scalingInfo);
	};
}
