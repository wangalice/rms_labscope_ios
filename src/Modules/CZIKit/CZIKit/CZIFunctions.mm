//
//  CZIFunctions.mm
//  CZIKit
//
//  Created by Li, Junlin on 12/3/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#include "CZIFunctions.h"
#import <Accelerate/Accelerate.h>

GUID CZI_GenerateGUID() {
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    CFUUIDBytes bytes = CFUUIDGetUUIDBytes(uuid);
    
    GUID guid = GUID();
    UInt8 *guidptr = (UInt8 *)&guid;
    guidptr[0] = bytes.byte0;
    guidptr[1] = bytes.byte1;
    guidptr[2] = bytes.byte2;
    guidptr[3] = bytes.byte3;
    guidptr[4] = bytes.byte4;
    guidptr[5] = bytes.byte5;
    guidptr[6] = bytes.byte6;
    guidptr[7] = bytes.byte7;
    guidptr[8] = bytes.byte8;
    guidptr[9] = bytes.byte9;
    guidptr[10] = bytes.byte10;
    guidptr[11] = bytes.byte11;
    guidptr[12] = bytes.byte12;
    guidptr[13] = bytes.byte13;
    guidptr[14] = bytes.byte14;
    guidptr[15] = bytes.byte15;
    
    CFRelease(uuid);
    
    return guid;
}

CGImageRef CZI_CGImageCreateWithBitmapData(std::shared_ptr<libCZI::IBitmapData> bitmapData) {
    size_t width = bitmapData->GetWidth();
    size_t height = bitmapData->GetHeight();
    size_t bitsPerComponent;
    size_t bitsPerPixel;
    size_t bytesPerPixel;
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;
    CFDataRef data;
    
    auto pixelType = bitmapData->GetPixelType();
    auto lockInfo = bitmapData->Lock();
    
    switch (pixelType) {
        case libCZI::PixelType::Gray8: {
            bitsPerComponent = 8;
            bitsPerPixel = 8;
            bytesPerPixel = 1;
            colorSpace = CGColorSpaceCreateDeviceGray();
            bitmapInfo = kCGBitmapByteOrderDefault;
            data = CFDataCreate(kCFAllocatorDefault, (UInt8 *)lockInfo.ptrDataRoi, lockInfo.size);
            break;
        }
        case libCZI::PixelType::Gray16: {
            bitsPerComponent = 16;
            bitsPerPixel = 16;
            bytesPerPixel = 2;
            colorSpace = CGColorSpaceCreateDeviceGray();
            bitmapInfo = kCGBitmapByteOrder16Little;
            data = CFDataCreate(kCFAllocatorDefault, (UInt8 *)lockInfo.ptrDataRoi, lockInfo.size);
            break;
        }
        case libCZI::PixelType::Bgr24: {
            bitsPerComponent = 8;
            bitsPerPixel = 32;
            bytesPerPixel = 4;
            colorSpace = CGColorSpaceCreateDeviceRGB();
            bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
            
            vImage_Buffer srcBuffer = {
                lockInfo.ptrDataRoi,
                (vImagePixelCount)height,
                (vImagePixelCount)width,
                3 * width
            };
            
            size_t size = 4 * width * height;
            void *rawData = malloc(size);
            vImage_Buffer dstBuffer = {
                rawData,
                (vImagePixelCount)height,
                (vImagePixelCount)width,
                4 * width
            };
            
            vImageConvert_BGR888toBGRA8888(&srcBuffer, NULL, 255, &dstBuffer, false, kvImageNoFlags);
            
            data = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (UInt8 *)rawData, size, kCFAllocatorMalloc);
            
            break;
        }
        case libCZI::PixelType::Bgra32: {
            bitsPerComponent = 8;
            bitsPerPixel = 32;
            bytesPerPixel = 4;
            colorSpace = CGColorSpaceCreateDeviceRGB();
            bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
            data = CFDataCreate(kCFAllocatorDefault, (UInt8 *)lockInfo.ptrDataRoi, lockInfo.size);
            break;
        }
        default: {
            // file read image failed. because currently only support kGray8, kBGR24 and kBGR32
            bitmapData->Unlock();
            return NULL;
        }
    }
    
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(data);
    CGImageRef image = CGImageCreate(width,
                                     height,
                                     bitsPerComponent,
                                     bitsPerPixel,
                                     bytesPerPixel * width,
                                     colorSpace,
                                     bitmapInfo,
                                     dataProvider,
                                     NULL,
                                     false,
                                     kCGRenderingIntentDefault);
    
    bitmapData->Unlock();
    CFRelease(data);
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    
    return image;
}

CFDataRef CZI_CFDataCopyFromCGImage(CGImageRef image) {
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    libCZI::PixelType pixelType = CZI_CGImageGetPixelType(image);
    
    switch (pixelType) {
        case libCZI::PixelType::Gray8: {
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
            size_t length = width * height;
            void *data = malloc(length);
            CGContextRef context = CGBitmapContextCreate(data, width, height, 8, width, colorSpace, kCGImageAlphaNone);
            CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), image);
            CGColorSpaceRelease(colorSpace);
            CGContextRelease(context);
            return CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (UInt8 *)data, length, kCFAllocatorMalloc);
        }
        case libCZI::PixelType::Gray16: {
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
            size_t length = width * height * 2;
            void *data = malloc(length);
            CGContextRef context = CGBitmapContextCreate(data, width, height, 16, width * 2, colorSpace, kCGImageAlphaNone | kCGImageByteOrder16Little);
            CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), image);
            CGColorSpaceRelease(colorSpace);
            CGContextRelease(context);
            return CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (UInt8 *)data, length, kCFAllocatorMalloc);
        }
        case libCZI::PixelType::Bgr24: {
            // TODO: To be implemented.
            return NULL;
        }
        case libCZI::PixelType::Bgra32: {
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            size_t length = width * height * 4;
            void *data = malloc(length);
            CGContextRef context = CGBitmapContextCreate(data, width, height, 8, width * 4, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
            CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), image);
            CGColorSpaceRelease(colorSpace);
            CGContextRelease(context);
            return CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (UInt8 *)data, length, kCFAllocatorMalloc);
        }
        default:
            return NULL;
    }
}

libCZI::PixelType CZI_CGImageGetPixelType(CGImageRef image) {
    size_t bitsPerComponent = CGImageGetBitsPerComponent(image);
    size_t bitsPerPixel = CGImageGetBitsPerPixel(image);
    size_t componentsPerPixel = bitsPerPixel / bitsPerComponent;
    if (componentsPerPixel == 1) { // single channel image
        if (bitsPerComponent == 8) {
            return libCZI::PixelType::Gray8;
        } else if (bitsPerComponent == 16) {
            return libCZI::PixelType::Gray16;
        }
    } else if (componentsPerPixel == 3) {
        /*
         3 channels image, maybe RBG or BRG.libCZI only support BRG byte order.
         Need red, blue byte switch if RGB->BGR
         */
        if (bitsPerComponent == 8) {
            return libCZI::PixelType::Bgr24;
        }
    } else if (componentsPerPixel == 4) {
        /*
         4 channels image, be careful about the byte order in channels. libCZI only support BGRA order.
         */
        if (bitsPerComponent == 8) {
            return libCZI::PixelType::Bgra32;
        }
    }
    
    return libCZI::PixelType::Invalid;
}
