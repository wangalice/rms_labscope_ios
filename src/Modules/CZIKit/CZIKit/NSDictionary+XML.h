//
//  NSDictionary+XML.h
//  CZIKit
//
//  Created by Li, Junlin on 6/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "pugixml.hpp"

@interface NSDictionary (XML)

- (void)cz_writeToXMLNode:(pugi::xml_node &)node;

@end
