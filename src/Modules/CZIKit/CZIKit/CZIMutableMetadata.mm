//
//  CZIMutableMetadata.m
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMutableMetadata.h"
#import "CZIMetadataPrivate.h"
#import "CZIMetadataNodePrivate.h"

@implementation CZIMutableMetadata

@synthesize document = _document;
@synthesize root = _root;

- (id)copyWithZone:(NSZone *)zone {
    return [[CZIMetadata alloc] initWithXMLString:self.XMLStringRepresentation];
}

- (CZIMutableMetadataNode *)document {
    if (_document == nil) {
        _document = [[CZIMutableMetadataNode alloc] initWithNode:self.doc];
    }
    return _document;
}

- (CZIMutableMetadataNode *)root {
    if (_root == nil) {
        CZIMutableMetadataNode *document = [[CZIMutableMetadataNode alloc] initWithNode:self.doc];
        CZIMutableMetadataNode *imageDocument = [document childNodeWithName:@"ImageDocument"];
        _root = [imageDocument childNodeWithName:@"Metadata"];
    }
    return _root;
}

@end
