//
//  NSString+WString.h
//  CZIKit
//
//  Created by Li, Junlin on 11/28/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <string>

@interface NSString (WString)

+ (instancetype)cz_stringWithWString:(const wchar_t *)wstring;
- (const std::wstring)cz_wstring;

@end
