//
//  CZIMutableMetadataNodeAttribute.h
//  CZIKit
//
//  Created by Li, Junlin on 4/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNodeAttribute.h"

@interface CZIMutableMetadataNodeAttribute : CZIMetadataNodeAttribute

@property (nonatomic, readwrite, copy) NSString *id;
@property (nonatomic, readwrite, copy) NSString *name;
@property (nonatomic, readwrite, copy) NSString *type;

@end
