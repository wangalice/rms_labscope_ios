//
//  CZIMetadataNodePrivate.h
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"
#include "pugixml.hpp"

@interface CZIMetadataNode (CZIMetadataNodePrivate)

@property (nonatomic, readonly, assign) pugi::xml_node node;

- (instancetype)initWithNode:(pugi::xml_node)node;

@end
