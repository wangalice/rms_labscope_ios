//
//  CZIMetadataNode+Specification.h
//  CZIKit
//
//  Created by Li, Junlin on 11/29/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode.h"

@interface CZIMetadataNode (Specification)

- (instancetype)acquisitionDateAndTime;
- (instancetype)adapter;
- (instancetype)additionalChannelSetting;
- (instancetype)analysisClass;
- (instancetype)analysisFeatures;
- (instancetype)analysisFrameRun;
- (instancetype)analysisFrameTeach;
- (instancetype)analysisStepCollection;
- (instancetype)appliance;
- (instancetype)appliances;
- (instancetype)application;
- (instancetype)autoScaling;
- (instancetype)bitCountRange;
- (instancetype)cameraAdapter;
- (instancetype)cameraAdapterMagnification;
- (instancetype)cameraName;
- (instancetype)channels;
- (instancetype)color;
- (instancetype)colorMode;
- (instancetype)complement;
- (instancetype)componentBitCount;
- (instancetype)creationDate;
- (instancetype)customAttributes;
- (instancetype)data;
- (instancetype)defaultUnitFormat;
- (instancetype)detector;
- (instancetype)detectorSettings;
- (instancetype)detectors;
- (instancetype)dimensions;
- (instancetype)displaySetting;
- (instancetype)distance;
- (instancetype)document;
- (instancetype)dyeName;
- (instancetype)elements;
- (instancetype)excitationWavelength;
- (instancetype)exposureGain;
- (instancetype)exposureTime;
- (instancetype)eyePiece;
- (instancetype)eyepieceSettings;
- (instancetype)frameMode;
- (instancetype)gain;
- (instancetype)grainSizeMeasurement;
- (instancetype)hardwareSetting;
- (instancetype)id;
- (instancetype)illuminationIntensity;
- (instancetype)illuminationType;
- (instancetype)image;
- (instancetype)imageAnalysisSetting;
- (instancetype)imageDocument;
- (instancetype)information;
- (instancetype)institution;
- (instancetype)instrument;
- (instancetype)isFrameMaximize;
- (instancetype)isProtected;
- (instancetype)isSelected;
- (instancetype)items;
- (instancetype)layerFlags;
- (instancetype)layer;
- (instancetype)layers;
- (instancetype)lightSource;
- (instancetype)lightSourceSettings;
- (instancetype)lightSources;
- (instancetype)lightSourcesSettings;
- (instancetype)magnification;
- (instancetype)manufacturer;
- (instancetype)maxDisplaySize;
- (instancetype)maxParticleSize;
- (instancetype)measureFrame;
- (instancetype)measurementAppData;
- (instancetype)metadata;
- (instancetype)metadataNodes;
- (instancetype)microscope;
- (instancetype)microscopeRef;
- (instancetype)microscopeSettings;
- (instancetype)microscopes;
- (instancetype)model;
- (instancetype)name;
- (instancetype)objectiveRef;
- (instancetype)objectiveSettings;
- (instancetype)objectives;
- (instancetype)optovarMagnification;
- (instancetype)pixel;
- (instancetype)pixelType;
- (instancetype)reflector;
- (instancetype)reflectorMatId;
- (instancetype)reflectorName;
- (instancetype)regionClassTemplate;
- (instancetype)scaling;
- (instancetype)script;
- (instancetype)segmenterSource;
- (instancetype)sizeX;
- (instancetype)sizeY;
- (instancetype)source;
- (instancetype)startTime;
- (instancetype)subImageDimension;
- (instancetype)t;
- (instancetype)tileSize;
- (instancetype)tilingMode;
- (instancetype)totalMagnification;
- (instancetype)type;
- (instancetype)usage;
- (instancetype)user;
- (instancetype)userName;
- (instancetype)value;
- (instancetype)version;
- (instancetype)zoomSettings;

@end
