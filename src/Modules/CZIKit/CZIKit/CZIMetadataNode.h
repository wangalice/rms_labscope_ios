//
//  CZIMetadataNode.h
//  CZIKit
//
//  Created by Li, Junlin on 11/27/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CZIMetadataNodeAttribute.h"

@interface CZIMetadataNode : NSObject <NSFastEnumeration>

@property (nonatomic, readonly, assign) BOOL isEmptyNode;
@property (nonatomic, readonly, copy) NSString *nodeName;
@property (nonatomic, readonly, strong) CZIMetadataNodeAttribute *nodeAttribute;
@property (nonatomic, readonly, strong) CZIMetadataNode *rootNode;
@property (nonatomic, readonly, copy) NSArray<CZIMetadataNode *> *childNodes;

@property (nonatomic, readonly, copy) NSString *stringValue;
@property (nonatomic, readonly, assign) BOOL boolValue;
@property (nonatomic, readonly, assign) NSInteger integerValue;
@property (nonatomic, readonly, assign) float floatValue;
@property (nonatomic, readonly, assign) double doubleValue;
@property (nonatomic, readonly, strong) NSDate *dateValue;
@property (nonatomic, readonly, assign) CGPoint pointValue;
@property (nonatomic, readonly, copy) NSArray<NSValue *> *pointsValue;
@property (nonatomic, readonly, strong) UIColor *colorValue;

- (instancetype)objectAtIndexedSubscript:(NSUInteger)index;

- (instancetype)childNodeWithName:(NSString *)name;
- (instancetype)childNodeWithName:(NSString *)name index:(NSInteger)index;

- (NSDictionary *)dictionaryRepresentation;

@end

@protocol CZIMetadataNodeDecodable <NSObject>

@required
- (instancetype)initFromMetadataNode:(CZIMetadataNode *)node;

@end
