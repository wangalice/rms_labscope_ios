//
//  UIImage+CZI.m
//  CZIKit
//
//  Created by Li, Junlin on 4/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIImage+CZI.h"
#import "CZIFunctions.h"

NSString *NSStringFromCZIPixelType(CZIPixelType pixelType) {
    switch (pixelType) {
        case CZIPixelTypeGray8:
            return @"Gray8";
        case CZIPixelTypeGray16:
            return @"Gray16";
        case CZIPixelTypeBgr24:
            return @"Bgr24";
        case CZIPixelTypeBgra32:
            return @"Bgra32";
        default:
            return nil;
    }
}

@implementation UIImage (CZI)

- (CZIComponentBitCount)czi_componentBitCount {
    libCZI::PixelType pixelType = CZI_CGImageGetPixelType(self.CGImage);
    switch (pixelType) {
        case libCZI::PixelType::Gray16:
            return CZIComponentBitCount12;
        default:
            return CZIComponentBitCount8;
    }
}

- (CZIPixelType)czi_pixelType {
    libCZI::PixelType pixelType = CZI_CGImageGetPixelType(self.CGImage);
    switch (pixelType) {
        case libCZI::PixelType::Gray8:
            return CZIPixelTypeGray8;
        case libCZI::PixelType::Gray16:
            return CZIPixelTypeGray16;
        case libCZI::PixelType::Bgr24:
            return CZIPixelTypeBgr24;
        case libCZI::PixelType::Bgra32:
            return CZIPixelTypeBgra32;
        default:
            return CZIPixelTypeUnknown;
    }
}

- (BOOL)czi_grayscale {
    libCZI::PixelType pixelType = CZI_CGImageGetPixelType(self.CGImage);
    switch (pixelType) {
        case libCZI::PixelType::Gray8:
        case libCZI::PixelType::Gray16:
            return YES;
        default:
            return NO;
    }
}

@end
