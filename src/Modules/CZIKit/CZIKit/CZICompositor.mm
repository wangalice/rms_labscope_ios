//
//  CZICompositor.m
//  CZIKit
//
//  Created by Li, Junlin on 12/3/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZICompositor.h"
#import "CZIDocument.h"

@implementation CZICompositor

- (NSData *)compositeImages:(NSArray<UIImage *> *)images metadata:(CZIMetadata *)metadata {
    CZIDocument *czi = [[CZIDocument alloc] init];
    [czi setMetadata:metadata];
    [images enumerateObjectsUsingBlock:^(UIImage *image, NSUInteger index, BOOL *stop) {
        [czi addSubBlockWithCGImage:image.CGImage atIndex:(int)index];
    }];
    return [czi dataRepresentation];
}

- (void)compositeImages:(NSArray<UIImage *> *)images metadata:(CZIMetadata *)metadata toFile:(NSString *)path {
    CZIDocument *czi = [[CZIDocument alloc] init];
    [czi setMetadata:metadata];
    [images enumerateObjectsUsingBlock:^(UIImage *image, NSUInteger index, BOOL *stop) {
        [czi addSubBlockWithCGImage:image.CGImage atIndex:(int)index];
    }];
    [czi writeToFile:path];
}

@end
