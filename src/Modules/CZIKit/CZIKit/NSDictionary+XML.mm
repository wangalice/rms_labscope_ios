//
//  NSDictionary+XML.m
//  CZIKit
//
//  Created by Li, Junlin on 6/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "NSDictionary+XML.h"
#import "NSString+WString.h"

@implementation NSDictionary (XML)

- (void)cz_writeToXMLNode:(pugi::xml_node &)node {
    [self enumerateKeysAndObjectsUsingBlock:^(NSString *key, id value, BOOL *stop) {
        if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSString class]]) {
            const std::wstring& wkeystring = [key cz_wstring];
            auto wkey = (wchar_t *)wkeystring.c_str();
            auto child = node.append_child(wkey);
            
            NSString *string = [value isKindOfClass:[NSNumber class]] ? ((NSNumber *)value).stringValue : value;
            auto data = child.append_child(pugi::node_pcdata);
            const std::wstring& wvaluestring = [string cz_wstring];
            auto wvalue = (wchar_t *)wvaluestring.c_str();
            data.set_value(wvalue);
        } else if ([value isKindOfClass:[NSArray class]] && key.length >= 2) {
            const std::wstring& wkeystring = [key cz_wstring];
            auto wkey = (wchar_t *)wkeystring.c_str();
            auto parent = node.append_child(wkey);
            
            NSString *name = [key substringToIndex:key.length - 1];
            NSArray *array = value;
            for (NSDictionary *dictionary in array) {
                if (![dictionary isKindOfClass:[NSDictionary class]]) {
                    continue;
                }
                const std::wstring& wnamestring = [name cz_wstring];
                auto wname = (wchar_t *)wnamestring.c_str();
                auto child = parent.append_child(wname);
                
                [dictionary cz_writeToXMLNode:child];
            }
        } else if ([value isKindOfClass:[NSDictionary class]]) {
            const std::wstring& wkeystring = [key cz_wstring];
            auto wkey = (wchar_t *)wkeystring.c_str();
            auto child = node.append_child(wkey);
            
            NSDictionary *dictionary = value;
            [dictionary cz_writeToXMLNode:child];
        }
    }];
}

@end
