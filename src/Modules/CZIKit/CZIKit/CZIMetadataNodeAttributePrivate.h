//
//  CZIMetadataNodeAttributePrivate.h
//  CZIKit
//
//  Created by Li, Junlin on 4/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNodeAttribute.h"
#include "pugixml.hpp"

@interface CZIMetadataNodeAttribute (CZIMetadataNodeAttributePrivate)

@property (nonatomic, readonly, assign) pugi::xml_node node;

- (instancetype)initWithNode:(pugi::xml_node)node;

@end
