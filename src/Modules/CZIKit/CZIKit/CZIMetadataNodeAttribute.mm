//
//  CZIMetadataNodeAttribute.m
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNodeAttribute.h"
#import "CZIMetadataNodeAttributePrivate.h"
#import "NSString+WString.h"

@interface CZIMetadataNodeAttribute ()

@property (nonatomic, readonly, assign) pugi::xml_node node;

@end

@implementation CZIMetadataNodeAttribute

- (instancetype)initWithNode:(pugi::xml_node)node {
    self = [super init];
    if (self) {
        _node = node;
    }
    return self;
}

- (NSString *)id {
    auto attribute = self.node.attribute(L"Id");
    const wchar_t *wstring = attribute.as_string();
    return [NSString cz_stringWithWString:wstring];
}

- (NSString *)name {
    auto attribute = self.node.attribute(L"Name");
    const wchar_t *wstring = attribute.as_string();
    return [NSString cz_stringWithWString:wstring];
}

- (NSString *)type {
    auto attribute = self.node.attribute(L"Type");
    const wchar_t *wstring = attribute.as_string();
    return [NSString cz_stringWithWString:wstring];
}

@end
