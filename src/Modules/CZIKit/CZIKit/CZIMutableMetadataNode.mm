//
//  CZIMutableMetadataNode.m
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMutableMetadataNode.h"
#import "CZIMetadataNodePrivate.h"
#import "CZIMetadataNodeAttributePrivate.h"
#import "CZIMutableMetadata.h"
#import "NSString+WString.h"
#include "pugixml.hpp"

@implementation CZIMutableMetadataNode

@synthesize nodeAttribute = _nodeAttribute;
@dynamic stringValue;
@dynamic boolValue;
@dynamic integerValue;
@dynamic floatValue;
@dynamic doubleValue;
@dynamic dateValue;
@dynamic pointValue;
@dynamic pointsValue;
@dynamic colorValue;

- (instancetype)childNodeWithName:(NSString *)name index:(NSInteger)index {
    const std::wstring& wstring = [name cz_wstring];
    auto wname = (wchar_t *)wstring.c_str();
    pugi::xml_node child;
    for (NSInteger i = 0; i <= index; i++) {
        if (i == 0) {
            child = self.node.child(wname);
        } else {
            child = child.next_sibling(wname);
        }
        if (child.empty()) {
            child = self.node.append_child(wname);
        }
    }
    return [[[self class] alloc] initWithNode:child];
}

#pragma mark - Attribute

- (CZIMutableMetadataNodeAttribute *)nodeAttribute {
    if (_nodeAttribute == nil) {
        _nodeAttribute = [[CZIMutableMetadataNodeAttribute alloc] initWithNode:self.node];
    }
    return _nodeAttribute;
}

#pragma mark - Access Value

- (void)setStringValue:(NSString *)stringValue {
    auto data = self.node.find_child([](pugi::xml_node node) {
        return node.type() == pugi::node_pcdata;
    });
    if (data.empty()) {
        data = self.node.append_child(pugi::node_pcdata);
    }
    const std::wstring& wstring = [stringValue cz_wstring];
    data.set_value((wchar_t *)wstring.c_str());
}

- (void)setBoolValue:(BOOL)boolValue {
    [self setStringValue:boolValue ? @"true" : @"false"];
}

- (void)setIntegerValue:(NSInteger)integerValue {
    [self setStringValue:[NSString stringWithFormat:@"%ld", (long)integerValue]];
}

- (void)setFloatValue:(float)floatValue {
    [self setStringValue:[NSString stringWithFormat:@"%f", floatValue]];
}

- (void)setDoubleValue:(double)doubleValue {
    [self setStringValue:[NSString stringWithFormat:@"%g", doubleValue]];
}

- (void)setDateValue:(NSDate *)dateValue {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    NSString *stringValue = [dateFormatter stringFromDate:dateValue];
    [self setStringValue:stringValue];
}

- (void)setPointValue:(CGPoint)pointValue {
    self.stringValue = [NSString stringWithFormat:@"%f,%f", pointValue.x, pointValue.y];
}

- (void)setPointsValue:(NSArray<NSValue *> *)pointsValue {
    NSMutableArray<NSString *> *pointStrings = [NSMutableArray array];
    for (NSValue *value in pointsValue) {
        CGPoint point = value.CGPointValue;
        NSString *pointString = [NSString stringWithFormat:@"%f,%f", point.x, point.y];
        [pointStrings addObject:pointString];
    }
    self.stringValue = [pointStrings componentsJoinedByString:@","];
}

- (void)setColorValue:(UIColor *)colorValue {
    CGFloat red, green, blue, alpha;
    [colorValue getRed:&red green:&green blue:&blue alpha:&alpha];
    NSString *stringValue = [NSString stringWithFormat:@"#%02X%02X%02X%02X", (UInt8)(alpha * 255), (UInt8)(red * 255), (UInt8)(green * 255),(UInt8)(blue * 255)];
    [self setStringValue:stringValue];
}

#pragma mark - Append Node

- (void)appendNode:(CZIMetadataNode *)node {
    self.node.append_copy(node.node);
}

- (void)appendChildNode:(void (^)(CZIMutableMetadataNode *))block {
    CZIMutableMetadata *metadata = [CZIMutableMetadata metadata];
    block(metadata.document);
    self.node.append_copy(metadata.document.node.first_child());
}

@end
