//
//  CZIFunctions.h
//  CZIKit
//
//  Created by Li, Junlin on 12/3/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#include "libCZI.h"

GUID CZI_GenerateGUID();

CGImageRef CZI_CGImageCreateWithBitmapData(std::shared_ptr<libCZI::IBitmapData> bitmapData);
CFDataRef CZI_CFDataCopyFromCGImage(CGImageRef image);
libCZI::PixelType CZI_CGImageGetPixelType(CGImageRef image);
