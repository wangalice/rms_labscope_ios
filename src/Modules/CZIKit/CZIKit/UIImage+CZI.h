//
//  UIImage+CZI.h
//  CZIKit
//
//  Created by Li, Junlin on 4/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CZIComponentBitCount) {
    CZIComponentBitCount8,
    CZIComponentBitCount12
};

typedef NS_ENUM(NSInteger, CZIPixelType) {
    CZIPixelTypeUnknown,
    CZIPixelTypeGray8,
    CZIPixelTypeGray16,
    CZIPixelTypeBgr24,
    CZIPixelTypeBgra32
};

#ifdef __cplusplus
extern "C" {
#endif

NSString *NSStringFromCZIPixelType(CZIPixelType pixelType);
    
#ifdef __cplusplus
}
#endif

@interface UIImage (CZI)

@property (nonatomic, readonly, assign) CZIComponentBitCount czi_componentBitCount;
@property (nonatomic, readonly, assign) CZIPixelType czi_pixelType;
@property (nonatomic, readonly, assign) BOOL czi_grayscale;

@end
