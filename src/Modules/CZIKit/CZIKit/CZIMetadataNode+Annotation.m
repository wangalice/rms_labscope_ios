//
//  CZIMetadataNode+Annotation.m
//  CZIKit
//
//  Created by Li, Junlin on 11/30/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZIMetadataNode+Annotation.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation CZIMetadataNode (Annotation)

@end

#pragma clang disgnostic pop
