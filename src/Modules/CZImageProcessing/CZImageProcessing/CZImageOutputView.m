//
//  CZImageOutputView.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageOutputView.h"
#import <GPUImage/GPUImage.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZImageTool.h"

const NSUInteger kTotalCells = 4;
const NSUInteger kGridModeColumns = 2;
const NSUInteger kGridModePadding = 10;
const NSUInteger kCrossModeMinimumPadding = 10;
const CGFloat kImageOutputLabelMaxHeight = 44;

@interface CZImageOutputView () <UIGestureRecognizerDelegate, CZAnnotationGestureRecognizerDelegate> {
    CZImageOutputViewMode _mode;
    BOOL _isAnimating;
}

@property (nonatomic, assign) BOOL forcesFourToThree;
@property (atomic, readwrite, assign) BOOL isSupplementalViewsShown;
@property (nonatomic, retain) NSMutableArray *imageButtons;

@property (nonatomic, retain) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, retain) UITapGestureRecognizer *doubleTapGestureRecognizer;
@property (nonatomic, retain) CZAnnotationGestureRecognizer *annotationRecognizer;

- (CGRect)contentRegion;
- (CGRect)contentRegionAtIndex:(NSUInteger)index;
- (CGRect)contentRegionForCrossCellAtIndex:(NSUInteger)index;
- (CGRect)contentRegionForGridCellAtX:(NSUInteger)x andY:(NSUInteger)y;
- (void)relocateControls;

@end

@implementation CZImageOutputView

- (id)init {
    [self release];
    return nil;
}

- (id)initWithMainImageViewIndex:(NSUInteger)index {
    return [self initWithMainImageViewIndex:index andForcesFourToThree:NO];
}

- (id)initWithMainImageViewIndex:(NSUInteger)index andForcesFourToThree:(BOOL)fourToThree {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.clipsToBounds = YES;
        self.multipleTouchEnabled = NO;
        self.backgroundColor = [UIColor blackColor];
        
        _mode = kImageOutputViewSingleMode;
        _forcesFourToThree = fourToThree;
        _willAutoDismiss = YES;
        _animationEnabled = YES;
        _isSupplementalViewsShown = NO;
        _isAnimating = NO;
        _animationLock = [[NSObject alloc] init];
        
        _mainImageViewIndex = index;
        _imageViews = [[NSMutableArray alloc] init];
        _imageLabels = [[NSMutableArray alloc] init];
        _imageButtons = [[NSMutableArray alloc] init];
        
        CGRect frame = [self contentRegion];
        GPUImageView *imageView = [[GPUImageView alloc] initWithFrame:frame];
        GPUImageFillModeType fillMode = _forcesFourToThree ?
        kGPUImageFillModePreserveAspectRatioAndFill : kGPUImageFillModePreserveAspectRatio;
        [imageView setFillMode:fillMode];
        [imageView setBackgroundColor:[UIColor clearColor]];
        [imageView setBackgroundColorRed:0 green:0 blue:0 alpha:0];
        imageView.autoresizingMask = UIViewAutoresizingNone;
        [self addSubview:imageView];
        
        for (NSUInteger i = 0; i < kTotalCells; ++i) {
            if (i == _mainImageViewIndex) {
                [_imageViews addObject:imageView];
            } else {
                [_imageViews addObject:[NSNull null]];
            }
            
            UILabel *label = [[UILabel alloc] init];
            [label setAlpha:0.0];
            [label setTextAlignment:NSTextAlignmentCenter];
            [label setLineBreakMode:NSLineBreakByWordWrapping];
            [label setNumberOfLines:2];
            [_imageLabels addObject:label];
            [self addSubview:label];
            [label release];
            
            if (i > 0) {
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
                [button setAlpha:0.0];
#if CZ_IMAGE_PROCESSING_REFACTORED
                [button cz_setImageWithIcon:[CZIcon iconNamed:@"settings"]];
#endif
                [button addTarget:self action:@selector(overlayGearButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [button setTag:i];
                [_imageButtons addObject:button];
                [self addSubview:button];
                [button release];
            } else {
                [_imageButtons addObject:[[[UIButton alloc] init] autorelease]];
            }
        }
        
        [imageView release];
        
        _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDoubleTapped:)];
        _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        _doubleTapGestureRecognizer.numberOfTouchesRequired = 1;
        _doubleTapGestureRecognizer.delegate = self;
        [self addGestureRecognizer:_doubleTapGestureRecognizer];
        
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        _tapGestureRecognizer.numberOfTapsRequired = 1;
        _tapGestureRecognizer.numberOfTouchesRequired = 1;
        _tapGestureRecognizer.delegate = self;
        [_tapGestureRecognizer requireGestureRecognizerToFail:_doubleTapGestureRecognizer];
        [self addGestureRecognizer:_tapGestureRecognizer];
        
        _annotationRecognizer = [[CZAnnotationGestureRecognizer alloc] initWithTarget:self action:@selector(viewHandlePanGesture:)];
        _annotationRecognizer.annotationDelegate = self;
        [self addGestureRecognizer:_annotationRecognizer];

        UISwipeGestureRecognizer *swipeRecongnizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
        swipeRecongnizer.cancelsTouchesInView = NO;
        swipeRecongnizer.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeRecongnizer.numberOfTouchesRequired = 1;
        [_annotationRecognizer requireGestureRecognizerToFail:swipeRecongnizer];
        swipeRecongnizer.delegate = self;
        [self addGestureRecognizer:swipeRecongnizer];
        [swipeRecongnizer release];
        swipeRecongnizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
        swipeRecongnizer.cancelsTouchesInView = NO;
        swipeRecongnizer.direction = UISwipeGestureRecognizerDirectionRight;
        swipeRecongnizer.numberOfTouchesRequired = 1;
        swipeRecongnizer.delegate = self;
        [self addGestureRecognizer:swipeRecongnizer];
        [_annotationRecognizer requireGestureRecognizerToFail:swipeRecongnizer];
        [swipeRecongnizer release];
    }
    return self;
}

- (void)dealloc {
    [_doubleTapGestureRecognizer release];
    [_tapGestureRecognizer release];
    [_annotationRecognizer release];
    [_imageViews release];
    [_imageLabels release];
    [_imageButtons release];
    [_animationLock release];
    
    [super dealloc];
}

#pragma mark - CZAnnotationGestureRecognizerDelegate

- (CZShouldBeginTouchState)annotationGetstureShouldBeginTouch:(UITouch *)touch {
    if ([self.delegate respondsToSelector:@selector(imageOutputView:shouldBeginAnnotationTouch:)]) {
        return [self.delegate imageOutputView:self shouldBeginAnnotationTouch:touch];
    }
    
    return CZShouldBeginTouchStateNo;
}

#pragma mark - Gesture handles

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.tapGestureRecognizer) {
        if (_isAnimating) {
            return NO;
        } else if (_isSupplementalViewsShown) {
            return YES;
        }
    } else if (gestureRecognizer == self.annotationRecognizer ||
               gestureRecognizer == self.doubleTapGestureRecognizer) {
        if (_isAnimating || _isSupplementalViewsShown) {
            return NO;
        }
    }

    if ([self.delegate respondsToSelector:@selector(imageOutputView:gesture:shouldReceiveTouch:)]) {
        BOOL shouldBegin = [self.delegate imageOutputView:self gesture:gestureRecognizer shouldReceiveTouch:touch];
        if (shouldBegin) {
            return YES;
        }
    }

    return NO;
}

- (void)viewTapped:(UIGestureRecognizer *)gestureRecognizer {
    if (_isAnimating) {
        return;
    }
    
    if (_isSupplementalViewsShown) {
        CGPoint point = [gestureRecognizer locationInView:self];
        
        for (NSUInteger i = 0; i < kTotalCells; ++i) {
            CGRect contentRegion = [self contentRegionAtIndex:i];
            if (CGRectContainsPoint(contentRegion, point)) {
                _mainImageViewIndex = i;
                
                if ([_delegate respondsToSelector:@selector(imageOutputView:didChangeMainViewIndexTo:)]) {
                    [_delegate imageOutputView:self didChangeMainViewIndexTo:_mainImageViewIndex];
                }
                
                if (self.willAutoDismiss) {
                    [self dismissSupplementalViews];
                }
                break;
            }
        }
    } else {
        if ([_delegate respondsToSelector:@selector(imageOutputView:handleTapGesture:)]) {
            [_delegate imageOutputView:self handleTapGesture:gestureRecognizer];
        }
    }
}

- (void)viewDoubleTapped:(UIGestureRecognizer *)gestureRecognizer {
    if (_isAnimating || _isSupplementalViewsShown) {
        return;
    }
    
    if ([_delegate respondsToSelector:@selector(imageOutputView:handleDoubleTapGesture:)]) {
        [_delegate imageOutputView:self handleDoubleTapGesture:gestureRecognizer];
    }
}

- (void)viewHandlePanGesture:(UIPanGestureRecognizer *)gestureRecognizer {
    if ([_delegate respondsToSelector:@selector(imageOutputView:handlePanGesture:)]) {
        [_delegate imageOutputView:self handlePanGesture:gestureRecognizer];
    }
}

- (void)swipeDetected:(UISwipeGestureRecognizer *)swipeRecognizer {
    if ([_delegate respondsToSelector:@selector(imageOutputView:handleSwipeGesture:)]) {
        [_delegate imageOutputView:self handleSwipeGesture:swipeRecognizer];
    }
}

#pragma mark - Overridden methods

/**
 * Override this method to guarantee that the image views inside will be
 * correctly resized when the container view is resized.
 */
- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    if (_isSupplementalViewsShown) {
        for (NSUInteger i = 0; i < kTotalCells; ++i) {
            CGRect subFrame = [self contentRegionAtIndex:i];
            GPUImageView *imageView = _imageViews[i];
            [imageView setFrame:subFrame];
        }
    } else {
        [[self mainImageView] setFrame:[self contentRegion]];
    }
    
    [self relocateControls];
}

#pragma mark - Public interfaces

- (GPUImageView *)mainImageView {
    return _imageViews[_mainImageViewIndex];
}

- (void)showSupplementalViewsWithMode:(CZImageOutputViewMode)mode {
    if (self.isSupplementalViewsShown || _isAnimating) {
        return;
    }
    
    _mode = mode;
    _isAnimating = YES;
     
    @synchronized (self.animationLock) {
        for (NSUInteger i = 0; i < kTotalCells; ++i) {
            if (i != _mainImageViewIndex) {
                CGRect subFrame = [self contentRegionAtIndex:i];
                
                GPUImageView *imageView = [[GPUImageView alloc] initWithFrame:subFrame];
                GPUImageFillModeType fillMode = _forcesFourToThree ?
                    kGPUImageFillModePreserveAspectRatioAndFill : kGPUImageFillModePreserveAspectRatio;
                [imageView setFillMode:fillMode];
                [imageView setBackgroundColor:[UIColor clearColor]];
                [imageView setBackgroundColorRed:0 green:0 blue:0 alpha:0];
                [self insertSubview:imageView belowSubview:[self mainImageView]];
                
                [_imageViews replaceObjectAtIndex:i withObject:imageView];
                
                [imageView release];
            }
        }
    }
    
    [self relocateControls];
    
    self.isSupplementalViewsShown = YES;
    
    if ([_delegate respondsToSelector:@selector(imageOutputViewWillShowSupplementalViews:)]) {
        [_delegate imageOutputViewWillShowSupplementalViews:self];
    }
    
    void (^animationsBlock)() = ^{
        CGRect subFrame = [self contentRegionAtIndex:_mainImageViewIndex];
        [[self mainImageView] setFrame:subFrame];
        
        for (UILabel *label in _imageLabels) {
            [self bringSubviewToFront:label];
            [label setAlpha:0.5];
        }
        for (UIButton *button in _imageButtons) {
            [self bringSubviewToFront:button];
            [button setAlpha:1.0];
        }
    };
    
    void (^completionBlock)(BOOL) = ^(BOOL finished) {
        if ([_delegate respondsToSelector:@selector(imageOutputViewDidShowSupplementalViews:)]) {
            [_delegate imageOutputViewDidShowSupplementalViews:self];
        }
        
        _isAnimating = NO;
    };
    
    if (self.animationEnabled) {
        [UIView animateWithDuration:0.4
                         animations:animationsBlock
                         completion:completionBlock];
    } else {
        [UIView animateWithDuration:0
                         animations:animationsBlock
                         completion:completionBlock];
    }
}

- (void)dismissSupplementalViews {
    [self dismissSupplementalViewsWithCompleteHandler:NULL];
}

- (void)dismissSupplementalViewsWithCompleteHandler:(void(^)())completeHandler {
    if (!self.isSupplementalViewsShown || _isAnimating) {
        if (completeHandler) {
            completeHandler();
        }
        return;
    }
    
    _isAnimating = YES;
    
    void (^animationsBlock)() = ^{
        [self bringSubviewToFront:[self mainImageView]];
        [[self mainImageView] setFrame:[self contentRegion]];
        
        for (UILabel *label in _imageLabels) {
            [label setAlpha:0.0];
        }
        for (UIButton *button in _imageButtons) {
            [button setAlpha:0.0];
        }
    };

    void (^completionBlock)(BOOL) = ^(BOOL finished) {
        @synchronized (self.animationLock) {
            if ([_delegate respondsToSelector:@selector(imageOutputViewWillDismissSupplementalViews:)]) {
                [_delegate imageOutputViewWillDismissSupplementalViews:self];
            }
            
            for (NSUInteger i = 0; i < kTotalCells; ++i) {
                if (i != _mainImageViewIndex) {
                    [_imageViews[i] removeFromSuperview];
                    [_imageViews replaceObjectAtIndex:i withObject:[NSNull null]];
                }
            }
            
            _mode = kImageOutputViewSingleMode;
            self.isSupplementalViewsShown = NO;
            
            if ([_delegate respondsToSelector:@selector(imageOutputViewDidDismissSupplementalViews:)]) {
                [_delegate imageOutputViewDidDismissSupplementalViews:self];
            }
            
            _isAnimating = NO;
            
            if (completeHandler) {
                completeHandler();
            }
        }
    };
    
    if (self.animationEnabled) {
        [UIView animateWithDuration:0.4
                         animations:animationsBlock
                         completion:completionBlock];
    } else {
        [UIView animateWithDuration:0
                         animations:animationsBlock
                         completion:completionBlock];
    }
}

- (void)setLabelsVisible:(BOOL)visible {
    @synchronized (self.animationLock) {
        for (UILabel *label in _imageLabels) {
            [label setHidden:!visible];
        }
        
        if (visible) {
            [self relocateControls];
        }
    }
}

- (BOOL)changeMainImageViewIndexTo:(NSUInteger)newIndex {
    if (self.isSupplementalViewsShown || _isAnimating) {
        return NO;
    }
    
    [_imageViews exchangeObjectAtIndex:newIndex withObjectAtIndex:_mainImageViewIndex];
    _mainImageViewIndex = newIndex;
    
    return YES;
}
    
- (void)overlayGearButtonAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    if ([self.delegate respondsToSelector:@selector(imageOutputView:didClickOverlayGearButton:)]) {
        [self.delegate imageOutputView:self didClickOverlayGearButton:button];
    }
}

#pragma mark -
#pragma mark Private Methods

- (CGRect)contentRegion {
    CGRect b = self.bounds; // alias
    if (_forcesFourToThree) {
        CGFloat actualHeight = floor(b.size.width * 3 / 4);
        CGFloat offset = (b.size.height - actualHeight) / 2;
        return CGRectMake(0, offset, b.size.width, actualHeight);
    } else {
        return b;
    }
}

- (CGRect)contentRegionAtIndex:(NSUInteger)index {
    switch (_mode) {
        case kImageOutputViewCrossMode:
            return [self contentRegionForCrossCellAtIndex:index];
            break;
            
        case kImageOutputViewGridMode:
        case kImageOutputViewGridModeWithButtons:
        {
            NSUInteger x = index % kGridModeColumns;
            NSUInteger y = index / kGridModeColumns;
            return [self contentRegionForGridCellAtX:x andY:y];
            break;
        }
            
        default:
            if (index == _mainImageViewIndex) {
                return [self contentRegion];
            }
            break;
    }
    return CGRectMake(0, 0, 0, 0);
}

- (CGRect)contentRegionForCrossCellAtIndex:(NSUInteger)index {
    CGFloat preferredWidth = floor((self.bounds.size.width - 4 * kCrossModeMinimumPadding) / 3);
    CGFloat preferredHeight = floor((self.bounds.size.height - 3 * kCrossModeMinimumPadding) / 2);
    
    CGFloat cellSize, horizontalPadding, verticalPadding;
    if (preferredWidth <= preferredHeight) {
        cellSize = preferredWidth;
        horizontalPadding = kCrossModeMinimumPadding;
        verticalPadding = floor((self.bounds.size.height - cellSize * 2) / 3);
    } else {
        cellSize = preferredHeight;
        horizontalPadding = floor((self.bounds.size.width - cellSize * 3) / 4);
        verticalPadding = kCrossModeMinimumPadding;
    }
    
    CGRect region = CGRectMake(0, 0, cellSize, cellSize);
    switch (index) {
        case 0:
            region.origin.x = horizontalPadding;
            region.origin.y = floor((self.bounds.size.height - cellSize) / 2);
            break;
            
        case 1:
            region.origin.x = cellSize + 2 * horizontalPadding;
            region.origin.y = verticalPadding;
            break;
            
        case 2:
            region.origin.x = cellSize + 2 * horizontalPadding;
            region.origin.y = cellSize + 2 * verticalPadding;
            break;
            
        case 3:
            region.origin.x = 2 * cellSize + 3 * horizontalPadding;
            region.origin.y = floor((self.bounds.size.height - cellSize) / 2);
            break;
            
        default:
            region.size.width = 0;
            region.size.height = 0;
            break;
    }
    return region;
}

- (CGRect)contentRegionForGridCellAtX:(NSUInteger)x andY:(NSUInteger)y {
    CGRect contentRegion = [self contentRegion];
    CGFloat halfWidth = contentRegion.size.width / 2;
    CGFloat halfHeight = contentRegion.size.height / 2;
    
    // Adjust the padding between cells to make outer and inner padding
    // looks the same.
    CGFloat paddingLeft = (x == 0) ? kGridModePadding : kGridModePadding / 2;
    CGFloat paddingRight = (x == 0) ? kGridModePadding / 2 : kGridModePadding;
    CGFloat paddingTop = (y == 0) ? kGridModePadding : kGridModePadding / 2;
    CGFloat paddingBottom = (y == 0) ? kGridModePadding / 2 : kGridModePadding;
    
    CGRect region = CGRectMake(x * halfWidth + paddingLeft,
                               contentRegion.origin.y + y * halfHeight + paddingTop,
                               halfWidth - paddingLeft - paddingRight,
                               halfHeight - paddingTop - paddingBottom);
    return region;
}

- (void)relocateControls {
    for (NSUInteger i = 0; i < kTotalCells; ++i) {
        CGRect subFrame = [self contentRegionAtIndex:i];
        UILabel *label = _imageLabels[i];
        UIButton *button = _imageButtons[i];
        button.hidden = YES;
        
        switch (_mode) {
            case kImageOutputViewCrossMode:
            {
                CGRect rect = [label textRectForBounds:CGRectMake(0, 0, subFrame.size.width, CGFLOAT_MAX)
                                limitedToNumberOfLines:2];
                CGFloat padding = round((kImageOutputLabelMaxHeight - rect.size.height) / 2.0);
                
                subFrame.origin.y += subFrame.size.height + padding;
                subFrame.size.height = rect.size.height;
                
                [label setBackgroundColor:[UIColor clearColor]];
#if CZ_IMAGE_PROCESSING_REFACTORED
                [label setTextColor:kDefaultLabelColor];
#endif
                break;
            }
                
            case kImageOutputViewGridModeWithButtons:
                if (i > 0) {
                    button.hidden = NO;
                    CGFloat paddingLeft = round(subFrame.size.width / 6);
                    CGRect buttonFrame = CGRectMake(CGRectGetMaxX(subFrame) - paddingLeft + 6, CGRectGetMaxY(subFrame) - 42, 32, 32);
                    [button setFrame:buttonFrame];
                }
                // fall-through
            default:
            {
                CGFloat paddingLeft = round(subFrame.size.width / 6);
                subFrame.origin.x += paddingLeft;
                subFrame.size.width -= paddingLeft * 2;
                
                CGRect rect = [label textRectForBounds:CGRectMake(0, 0, subFrame.size.width, CGFLOAT_MAX)
                                limitedToNumberOfLines:2];
                
                subFrame.origin.y += subFrame.size.height - 15 - rect.size.height;
                subFrame.size.height = rect.size.height;
                
                [label setBackgroundColor:[UIColor blackColor]];
                [label setTextColor:[UIColor whiteColor]];
                break;
            }
        }
        
        [label setFrame:subFrame];
    }
}

@end
   
