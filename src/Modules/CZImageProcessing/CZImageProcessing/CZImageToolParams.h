//
//  CZImageToolParams.h
//  Hermes
//
//  Created by Halley Gu on 1/31/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>

@interface CZImageToolParams : NSObject <NSCopying>

@property (nonatomic, assign) NSInteger rotation;
@property (nonatomic, assign) BOOL horizontalFlipping;
@property (nonatomic, assign) BOOL verticalFlipping;

@property (nonatomic, assign) CGFloat gamma;
@property (nonatomic, assign) CGFloat brightness;
@property (nonatomic, assign) CGFloat contrast;
@property (nonatomic, assign) CGFloat colorIntensity;
@property (nonatomic, assign) CGFloat sharpness;

- (void)reset;
- (BOOL)isEqualToDefaultParams:(CZImageToolParams *)defaultParams;

- (GPUImageOutput<GPUImageInput> *)compoundFilter;

@end
