//
//  CZImageAction.m
//  Hermes
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageAction.h"
#import <GPUImage/GPUImage.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZImageTool.h"
#import "CZImageToolParams.h"

@interface CZImageAction ()

@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, assign) CZImageTool *imageTool;
@property (nonatomic, copy) CZImageToolParams *params;
@property (nonatomic, retain) CZImageDocument *originalDocument;

@end

@implementation CZImageAction

- (id)initWithDocManager:(CZDocManager *)docManager
               imageTool:(CZImageTool *)imageTool
                  params:(CZImageToolParams *)params {
    self = [super init];
    if (self) {
        _docManager = docManager;
        _imageTool = imageTool;
        _params = [params copy];
    }
    return self;
}

- (void)dealloc {
    [_params release];
    [_originalDocument release];
    [super dealloc];
}

#pragma mark -
#pragma mark CZUndoAction Methods

- (NSUInteger)costMemory {
    // Rough estimation, assuming the UIImage object is 32-bit RGBA.
    CGSize size = self.originalDocument.imageSize;
    return size.width * size.height * 4;
}

- (void)do {
    [self redo];
}

- (void)undo {
    if (self.originalDocument) {
        [_imageTool.delegate imageTool:_imageTool changedImageSizeTo:self.originalDocument.imageSize];
        self.docManager.document = self.originalDocument;
    }
}

- (void)redo {
    if (self.docManager.document) {
        self.originalDocument = self.docManager.document;
        
        UIImage *result = nil;
        
        GPUImageOutput<GPUImageInput> *filter = [_params compoundFilter];
        if (filter) {
            GPUImagePicture *gpuImageSource = nil;
            @autoreleasepool {
                gpuImageSource = [[GPUImagePicture alloc] initWithImage:[self.originalDocument outputImage]];
            }
            
            CGSize targetSize = self.originalDocument.imageSize;
            if (_params.rotation % 2 == 1) {
                targetSize = CGSizeMake(targetSize.height, targetSize.width);
            }
            [filter forceProcessingAtSize:targetSize];
            
            [gpuImageSource addTarget:filter];
            [filter useNextFrameForImageCapture];
            [gpuImageSource processImage];
            
            // Explicitly pass the orientation here to avoid a bug of GPUImage. For more
            // details refer to: https://github.com/BradLarson/GPUImage/issues/343
            result = [filter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
            
            // Copy to generate a new parameter object which only contains the
            // data, so that we can safely release the GPUImageFilter objects in
            // the current parameter object.
            CZImageToolParams *params = [_params copy];
            self.params = params;
            [params release];
            
            [gpuImageSource release];
        } else {
            result = [self.originalDocument outputImage];
        }
        
        [_imageTool.delegate imageTool:_imageTool changedImageSizeTo:result.size];
        self.docManager.document = [self.originalDocument documentByReplacingImage:result];
    }
}

- (CZUndoActionType)type {
    return CZUndoActionTypeImage;
}

@end
