//
//  CZImageTool.m
//  Hermes
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageTool.h"

#import <GPUImage/GPUImage.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZImageAction.h"
#import "CZImageCropAction.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZOverExposeFilter.h"
#import "CZLookupFilter.h"
#import "UIImage+MultiphaseImage.h"
#import "CZLayerThicknessDetector.h"
#import "CZROISizePanelView.h"

@interface CZCropToolRectMeasurementTextCreator : NSObject <CZElementLayerCreating>
@end

@implementation CZCropToolRectMeasurementTextCreator

- (CALayer *)newMeasurementTextLayerOfElement:(CZElement *)element {
    if ([element isKindOfClass:[CZElementRectangle class]]) {
        CZElementRectangle *rect = (CZElementRectangle *)element;
        
        CZElementUnitStyle unitStyle = [CZElement unitStyle];
        CGFloat scaling = [element scalingOfUnit:unitStyle];
        NSString *unit1d = [element unitNameOfDistanceByStyle:unitStyle];
        CGSize rectSize = rect.rect.size;
        
        CZFeature *areaFeature = [rect.features objectAtIndex:CZElementRectangleFeatureArea];
        
        // width measuremnt
        CZFeature *widthFeature = [[CZFeature alloc] initWithValue:scaling * rectSize.width unit:unit1d type:kCZFeatureTypeWidth];
        
        // height measuremnt
        CZFeature *heightFeature = [[CZFeature alloc] initWithValue:scaling * rectSize.height unit:unit1d type:kCZFeatureTypeHeight];
        
        // combine feature string
        NSString *featureString = [areaFeature toLocalizedStringWithPrecision:[element precision]];
        featureString = [featureString stringByAppendingString:@"\n"];
        featureString = [featureString stringByAppendingString:[widthFeature toLocalizedStringWithPrecision:[element precision]]];
        [widthFeature release];
        
        featureString = [featureString stringByAppendingString:@"\n"];
        featureString = [featureString stringByAppendingString:[heightFeature toLocalizedStringWithPrecision:[element precision]]];
        [heightFeature release];

        CZNodeRectangle *measurementText = [element newMeasurementNodeWithString:featureString];
        [element putMeasurementAtDefaultPosition:measurementText];
        
        CALayer *layer = [measurementText newLayer];
        [measurementText release];
        return layer;
    } else {
        return nil;
    }
}

@end

@interface CZCropToolCircleMeasurementTextCreator : NSObject <CZElementLayerCreating>
@end

@implementation CZCropToolCircleMeasurementTextCreator

- (CALayer *)newMeasurementTextLayerOfElement:(CZElement *)element {
    if ([element isKindOfClass:[CZElementCircle class]]) {
        CZElementCircle *circle = (CZElementCircle *)element;
        
        NSUInteger precision = [circle precision];
        
        CZFeature *areaFeature = circle.features[CZElementCircleFeatureArea];
        CZFeature *perimeterFeature = circle.features[CZElementCircleFeaturePerimeter];
        CZFeature *diameterFeature = circle.features[CZElementCircleFeatureDiameter];
        NSString *featureString = [NSString stringWithFormat:@"%@\n%@\n%@",
                                   [areaFeature toLocalizedStringWithPrecision:precision],
                                   [perimeterFeature toLocalizedStringWithPrecision:precision],
                                   [diameterFeature toLocalizedStringWithPrecision:precision]];
        
        // area text
        CZNodeRectangle *measurementText = [circle newMeasurementNodeWithString:featureString];
        [circle putMeasurementAtDefaultPosition:measurementText];
        CALayer *textlayer = [measurementText newLayer];
        [measurementText release];
        
        return textlayer;
    } else {
        return nil;
    }
}
@end

@interface CZCropToolTriangleMeasurementTextCreator : NSObject <CZElementLayerCreating>
@end

@implementation CZCropToolTriangleMeasurementTextCreator

- (CALayer *)newMeasurementTextLayerOfElement:(CZElement *)element {
    if (![element isKindOfClass:[CZElementPolygon class]]) {
        return nil;
    }
    
    CZElementPolygon *polygon = (CZElementPolygon *)element;
    if (polygon.pointsCount != 3) {
        return nil;
    }
    
    CALayer *layer = [[CALayer alloc] init];
    
    CZFeature *areaFeature = [element.features objectAtIndex:CZElementPolygonFeatureArea];
    NSString *featureString = [areaFeature toLocalizedStringWithPrecision:[element precision]];
    
    // area text
    CZNodeRectangle *measurementText = [element newMeasurementNodeWithString:featureString];
    [element putMeasurementAtDefaultPosition:measurementText];
    CALayer *textlayer = [measurementText newLayer];
    [layer addSublayer:textlayer];
    [textlayer release];
    [measurementText release];
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [element scalingOfUnit:unitStyle];
    NSString *unit1d = [element unitNameOfDistanceByStyle:unitStyle];
    
    // length text
    for (NSUInteger i = 0, j = 1, k = 2; i < 3; i++, j++, k++) {
        if (j == 3) {
            j = 0;
        }
        if (k == 3) {
            k = 0;
        }
        
        CGPoint ptA = [polygon pointAtIndex:i];
        CGPoint ptB = [polygon pointAtIndex:j];
        CGPoint ptC = [polygon pointAtIndex:k];
        
        CGFloat dx = ptA.x - ptB.x;
        CGFloat dy = ptA.y - ptB.y;
        CGFloat k = dy / dx;
        if (!isnormal(k)) {
            k = 0;
        }
        CGFloat y = (ptC.x - ptA.x) * k + ptA.y;
        
        NSNumber *direction;
        if (dx * dy > 0) {
            direction = (ptC.y > y) ? @(kCZMeasurementDirectionBottomLeft) : @(kCZMeasurementDirectionTopRight);
        } else {
            direction = (ptC.y > y) ? @(kCZMeasurementDirectionBottomRight) : @(kCZMeasurementDirectionTopLeft);
        }
        
        float value = scaling * sqrt((ptA.x - ptB.x) * (ptA.x - ptB.x) + (ptA.y - ptB.y) * (ptA.y - ptB.y));
        CZFeature *distanceFeature = [[CZFeature alloc] initWithValue:value unit:unit1d type:@""];
        featureString = [distanceFeature toLocalizedStringWithPrecision:[element precision]];
        [distanceFeature release];
        
        measurementText = [element newMeasurementNodeWithString:featureString];
        
        CGPoint ptCenter = CGPointMake((ptA.x + ptB.x) * 0.5, (ptA.y + ptB.y) * 0.5);
        [element putMeasurement:measurementText
              possiblePositions:@[[NSValue valueWithCGPoint:ptCenter]]
                     directions:@[direction]];
        
        textlayer = [measurementText newLayer];
        [measurementText release];
        
        [layer addSublayer:textlayer];
        [textlayer release];
    }
    
    return layer;
}

@end

@interface CZImageTool () <CZElementLayerDelegate, CZElementDataSource, CZUndoManaging, CZROISizePanelViewDelegate> {
    CZDisplayCurvePreset _curvePreset;
    CZCroppingMode _croppingMode;
    BOOL _setCroppingModeRecursiveLock;
    BOOL _needsApplying;
    BOOL _hasSharpnessChanges;
    CZLookupFilter *_multiphaseLookupFilter;
    CGPoint _beginPoint;
}

@property (nonatomic, retain) GPUImageView *gpuImageView;
@property (nonatomic, retain) GPUImagePicture *gpuImageSource;
@property (nonatomic, retain) CZDocManager *docManager;
@property (nonatomic, retain) UIImage *sourceImage;
@property (nonatomic, retain) CZImageOutputView *rotationView;
@property (nonatomic, retain) CZImageOutputView *mirroringView;
@property (nonatomic, retain) CZImageOutputView *displayCurveView;
@property (nonatomic, retain) CZImageOutputView *croppingView;
@property (nonatomic, retain) UIView *croppingAnnotationView;
@property (nonatomic, retain) CZSelectTool *selectTool;
@property (nonatomic, retain) CALayer *thicknessEdgeLayer;
@property (nonatomic, assign) BOOL thicknessEdgeLayerHidden;
@property (nonatomic, retain) CALayer *multiphaseLayer;

- (void)invalidateAndGenerateImage:(UIImage **)output
               ignoringMinimumArea:(BOOL)ignoresMinimumArea;

- (void)showInteractiveView:(CZImageOutputView *)interactiveView
                     onView:(UIView *)view
                  withFrame:(CGRect)frame
                    andMode:(CZImageOutputViewMode)mode;

- (void)cleanUpInteractiveViewsExcept:(CZImageOutputView *)view;

- (CGSize)calculateSuitableProcessingSize:(CGSize)imageSize
                                 forParam:(CZImageToolParams *)params
                             isForInteractiveView:(BOOL)isInteractive;

- (id<CZUndoAction>)newScaleBarSetCornerAction;

- (void)updateScaleBarPosition;

- (void)updateElementOnCroppingView;

- (void)notifyImageSizeChanged;

@end

@implementation CZImageTool

@synthesize selectTool = _selectTool;
@synthesize elementLayer = _elementLayer;

+ (BOOL)multiphaseShouldUpdateWithGPU:(CZMultiphase *)multiphase {
    return !(multiphase.isParticlesMode || multiphase.hasMinimumArea);
}

- (id)initWithFrame:(CGRect)frame {
    self = [super init];
    if (self) {
        _params = [[CZImageToolParams alloc] init];
        
        // NOTICE: there's a bug in GPUImage 0.1.7 that change size of GPUImageView
        // before it shown up will cause GPUImageView show empty content. So, the
        // workaround here is set frame when init, and do not change frame.
        _gpuImageView = [[GPUImageView alloc] initWithFrame:frame];
        [_gpuImageView setFillMode:kGPUImageFillModePreserveAspectRatio];
        [_gpuImageView setBackgroundColor:[UIColor clearColor]];
        [_gpuImageView addObserver:self forKeyPath:@"frame" options:0 context:NULL];
        _gpuImageView.autoresizingMask = UIViewAutoresizingNone;
        
        _docManager = nil;
        _sourceImage = nil;
        
        _curvePreset = CZDisplayCurvePresetInvalid;
        _ignoresDisplayCurve = NO;
        
        _needsApplying = NO;
        _hasSharpnessChanges = NO;
        
        _multiphaseLayer = [[CALayer alloc] init];
        _multiphaseLayer.frame = _gpuImageView.bounds;
        _multiphaseLayer.name = @"multiphase";
        [_gpuImageView.layer addSublayer:_multiphaseLayer];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image {
    CGRect frame = CGRectZero;
    if (image) {
        CGSize imageSize = [image size];
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        frame = CGRectMake(0, 0, imageSize.width / screenScale, imageSize.height / screenScale);
    }
    
    self = [self initWithFrame:frame];
    if (self) {
        _sourceImage = [image retain];
    }
    return self;
}

- (id)initWithDocManager:(CZDocManager *)docManager {
    if (docManager == nil) {
        [self release];
        self = nil;
        return self;
    }

    CGRect frame = CGRectZero;
    if (docManager.document) {
        CGSize imageSize = docManager.document.imageSize;
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        frame = CGRectMake(0, 0, imageSize.width / screenScale, imageSize.height / screenScale);
    }

    self = [self initWithFrame:frame];
    if (self) {
        self.docManager = docManager;
        [_docManager addObserver:self forKeyPath:@"document" options:0 context:NULL];
    }
    return self;
}

- (void)dealloc {
    if (_docManager) {
        [_docManager removeObserver:self forKeyPath:@"document"];
    }
    [_docManager release];
    
    [_gpuImageView removeObserver:self forKeyPath:@"frame"];
    
    [_sourceImage release];
    [_params release];
    [_gpuImageView release];
    [_gpuImageSource release];
    
    _rotationView.delegate = nil;
    [_rotationView release];
    _mirroringView.delegate = nil;
    [_mirroringView release];
    _displayCurveView.delegate = nil;
    [_displayCurveView release];
    _croppingView.delegate = nil;
    [_croppingView release];
    
    [_multiphaseLookupFilter release];
    
    if (self == _elementLayer.delegate) {
        _elementLayer.delegate = nil;
    }
    _elementLayer.delegate = nil;
    [_elementLayer release];
    
    [_croppingAnnotationView release];
    [_selectTool release];
    [_thicknessEdgeLayer release];
    [_multiphaseLayer release];
    _roiSizePanelView.delegate = nil;
    [_roiSizePanelView release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Public Interfaces

- (UIView *)imageView {
    return _gpuImageView;
}

- (UIImage *)workingImage {
    if (_docManager) {
        return [_docManager.document outputImage];
    }
    return self.sourceImage;
}

- (void)setImage:(UIImage *)image {
    _hasSharpnessChanges = NO;
    _needsApplying = NO;
    
    if (_docManager) {
        CZImageDocument *document = [_docManager.document documentByReplacingImage:image];
        _docManager.document = document;
        
        // Invalidating will be done in the KVO handler.
    } else {
        [self setSourceImage:image];
        self.gpuImageSource = nil;
        [self invalidate];
    }
}

- (void)apply {
    // Dismiss interactive image operation views if opened.
    [self cleanUpInteractiveViewsExcept:nil];
    
    // Do not create actions when no image processing has been really done.
    if (_needsApplying && _docManager) {
        CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
        compoundAction.undoReversed = NO;
        
        CZImageAction *imageAction = [[CZImageAction alloc] initWithDocManager:_docManager
                                                                     imageTool:self
                                                                        params:_params];
        [compoundAction addAction:imageAction];
        [imageAction release];
        
        // Reset the corner position, so that it will move to the right place.
        // Only happens when the image is rotated by 90 degrees.
        if (_params.rotation % 2 == 1) {
            id<CZUndoAction> cornerAction = [self newScaleBarSetCornerAction];
            [compoundAction addAction:cornerAction];
            [cornerAction release];
        }
        
        [_params reset];
        [_gpuImageSource removeAllTargets];
        
        [_docManager pushAction:compoundAction];
        [compoundAction release];
        _needsApplying = NO;
    } else {
        [self invalidate];
    }
}

- (void)revert {
    // Dismiss interactive image operation views if opened.
    [self cleanUpInteractiveViewsExcept:nil];
    
    // Also need to revert the change on GPUImageView's frame if the image
    // has been already rotated.
    if (_params.rotation == 1 || _params.rotation == 3) {
        if ([_delegate respondsToSelector:@selector(imageTool:changedImageSizeTo:)]) {
            UIImage *image = [self workingImage];
            [_delegate imageTool:self changedImageSizeTo:image.size];
        }
        
        [self updateScaleBarPosition];
    }
    
    [_params reset];
    [_gpuImageSource removeAllTargets];
    
    _needsApplying = NO;
    [self invalidate];
}

- (void)invalidate {
    // Add autorelease pool here to release memory of filters.
    @autoreleasepool {
        // Fix a bug that the multiphase view blinks when invalidating from a
        // state with no minimum area to minimum area counted.
        
        // consider if multiphase layer will be shown after call [invalidateAndGenerateImage:NULL ignoringMinimumArea:NO];
        BOOL willShowMultiphaseLayer = (self.multiphaseEnabled &&
                                        self.multiphaseLayer.isHidden &&
                                        ![CZImageTool multiphaseShouldUpdateWithGPU:self.docManager.multiphase]);
        if (willShowMultiphaseLayer) {
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                [self invalidateAndGenerateImage:NULL ignoringMinimumArea:NO];
            }];
            self.multiphaseLayer.hidden = NO;
            [self.multiphaseLayer removeAllAnimations];
            [CATransaction commit];
        } else {
            [self invalidateAndGenerateImage:NULL ignoringMinimumArea:NO];
        }
    }
}

- (void)invalidateIgnoringMultiphaseMinimumArea {
    // Add autorelease pool here to release memory of filters.
    @autoreleasepool {
        [self invalidateAndGenerateImage:NULL ignoringMinimumArea:YES];
    }
}

- (void)drawScreenshotInRect:(CGRect)rect {
    return [self drawScreenshotInRect:rect visibleLayersName:nil];
}

- (void)drawScreenshotInRect:(CGRect)rect visibleLayersName:(NSArray *)layersName {
    UIImage *output = nil;
    @autoreleasepool {
        [self invalidateAndGenerateImage:&output ignoringMinimumArea:NO];
        [output drawInRect:rect];

        NSMutableArray *originalHiddenStatus = nil;
        if (layersName) {
            // save hidden status
            originalHiddenStatus = [NSMutableArray array];
            for (CALayer *subLayer in _gpuImageView.layer.sublayers) {
                [originalHiddenStatus addObject:@(subLayer.isHidden)];
            }
            
            for (CALayer *subLayer in _gpuImageView.layer.sublayers) {
                BOOL hidden = [layersName indexOfObject:subLayer.name] == NSNotFound;
                subLayer.hidden = hidden;
                [subLayer removeAllAnimations];
            }
        }
        
        BOOL requireDrawSublayers = NO;
        for (CALayer *subLayer in _gpuImageView.layer.sublayers) {
            if (!subLayer.isHidden) {
                requireDrawSublayers = YES;
                break;
            }
        }
        
        if (requireDrawSublayers) {
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSaveGState(context);
            
            CGContextTranslateCTM(context, rect.origin.x, rect.origin.y);
            
            CGFloat outputScaleX = rect.size.width / output.size.width;
            CGFloat outputScaleY = rect.size.height / output.size.height;
            
            CGFloat screenScale = [[UIScreen mainScreen] scale];
            CGContextScaleCTM(context, outputScaleX * screenScale, screenScale * outputScaleY);
            
            [_gpuImageView.layer renderInContext:context];
            
            CGContextRestoreGState(context);
        }
        
        // restore hidden status
        if (originalHiddenStatus) {
            NSUInteger i = 0;
            for (CALayer *subLayer in _gpuImageView.layer.sublayers) {
                BOOL hidden = [(NSNumber *)originalHiddenStatus[i] boolValue];
                subLayer.hidden = hidden;
                [subLayer removeAllAnimations];
                i++;
            }
        }
    }
}

- (void)dismissInteractiveViews {
    [self cleanUpInteractiveViewsExcept:nil];
}

- (void)showInteractiveRotationOnView:(UIView *)view withFrame:(CGRect)frame {
    if (_rotationView == nil) {
        _rotationView = [[CZImageOutputView alloc] initWithMainImageViewIndex:0];
        [self showInteractiveView:_rotationView
                           onView:view
                        withFrame:frame
                          andMode:kImageOutputViewCrossMode];
    }
}

- (void)showInteractiveMirroringOnView:(UIView *)view withFrame:(CGRect)frame {
    if (_mirroringView == nil) {
        _mirroringView = [[CZImageOutputView alloc] initWithMainImageViewIndex:0];
        [self showInteractiveView:_mirroringView
                           onView:view
                        withFrame:frame
                          andMode:kImageOutputViewCrossMode];
    }
}

- (void)rotateClockwise {
    self.params.rotation = (self.params.rotation + 5) % 4;
    
    [self notifyImageSizeChanged];
    
    _needsApplying = YES;
    [self invalidate];
    [self updateScaleBarPosition];
}

- (void)rotateCounterClockwise {
    self.params.rotation = (self.params.rotation + 3) % 4;
    
    [self notifyImageSizeChanged];
    
    _needsApplying = YES;
    [self invalidate];
    [self updateScaleBarPosition];
}

- (void)rotate180 {
    self.params.rotation = (self.params.rotation + 2) % 4;
    
    _needsApplying = YES;
    [self invalidate];
}

- (void)flipHorizontally {
    self.params.horizontalFlipping = !self.params.horizontalFlipping;
    _needsApplying = YES;
    [self invalidate];
}

- (void)flipVertically {
    self.params.verticalFlipping = !self.params.verticalFlipping;
    _needsApplying = YES;
    [self invalidate];
}

- (void)showROISizePanelWithMode:(CZCroppingMode)mode {
    [self.roiSizePanelView removeFromSuperview];
    self.roiSizePanelView = nil;
    
    CZROISizePanelView *roiSizePanelView = nil;
    if (mode == CZCroppingModeUnknown){
        return;
    } else  {
        roiSizePanelView = [[CZROISizePanelView alloc] initWithFrame:CGRectZero];
    }
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    NSString *unitString = L([_elementLayer unitNameOfDistanceByStyle:unitStyle]);
    roiSizePanelView.unit = unitString;
    
    NSUInteger precision = [_elementLayer precision];
    roiSizePanelView.precision = precision;
    
    [_croppingView addSubview:roiSizePanelView];
    CGFloat scaling = [_elementLayer scalingOfUnit:unitStyle];
    if (scaling <= 0) {
        scaling = 1.0f;
    }
    
    CGSize imageSize = self.docManager.document.imageSize;
    CGFloat diagonal = scaling * sqrt(imageSize.width * imageSize.width + imageSize.height * imageSize.height);
    
    roiSizePanelView.inputMinValue = [CZCommonUtils floatFromFloat:scaling precision:precision];
    roiSizePanelView.inputMaxValue = [CZCommonUtils floatFromFloat:diagonal precision:precision];
    
    [roiSizePanelView updateWithCroppingMode:mode];
    
    NSArray *sizeContents = [self readROISizeWithMode:mode];
    [roiSizePanelView updateTextFieldsWithContents:sizeContents];

    roiSizePanelView.delegate = self;
    self.roiSizePanelView = roiSizePanelView;
    [roiSizePanelView release];
}

- (void)showInteractiveCroppingOnView:(UIView *)view withFrame:(CGRect)frame {
    if (_croppingView == nil) {
        _croppingView = [[CZImageOutputView alloc] initWithMainImageViewIndex:0];
        [self showInteractiveView:_croppingView
                           onView:view
                        withFrame:frame
                          andMode:kImageOutputViewSingleMode];
        
        UIImage *source = nil;
        [self invalidateAndGenerateImage:&source ignoringMinimumArea:NO];
        [[source retain] autorelease];
        
        CGSize imageSize = [[self workingImage] size];
        if (self.params.rotation % 2 == 1) {
            imageSize = CGSizeMake(imageSize.height, imageSize.width);
        }
        
        GPUImagePicture *picture = nil;
        @autoreleasepool {
            picture = [[GPUImagePicture alloc] initWithImage:source];
        }
        [picture addTarget:[_croppingView mainImageView]];
        [picture processImage];
        [picture release];
        
        CGRect actualBox = [CZCommonUtils calcActualBoxByBoundBox:_croppingView.mainImageView.bounds
                                                      contentSize:imageSize];
        
        CGRect annotationViewFrame;
        annotationViewFrame.origin = actualBox.origin;
        
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        annotationViewFrame.size.width = imageSize.width / screenScale;
        annotationViewFrame.size.height = imageSize.height / screenScale;
        
        _croppingAnnotationView = [[UIView alloc] init];
        _croppingAnnotationView.transform = CGAffineTransformIdentity;
        [_croppingView addSubview:_croppingAnnotationView];
        
        if (_elementLayer == nil) {
            _elementLayer = [[CZElementLayer alloc] init];
        }
        _elementLayer.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
        _elementLayer.delegate = self;
        _elementLayer.scaling = self.docManager.elementLayer.scaling;
        _elementLayer.prefersBigUnit = self.docManager.elementLayer.prefersBigUnit;
        
        // refresh zoom of scale
        CGFloat zoomScale = actualBox.size.width / annotationViewFrame.size.width;
        _croppingAnnotationView.layer.anchorPoint = CGPointZero;
        _croppingAnnotationView.transform = CGAffineTransformMakeScale(zoomScale, zoomScale);
        
        _croppingAnnotationView.frame = annotationViewFrame;
        
        _selectTool = [[CZSelectTool alloc] initWithDocManager:self
                                                          view:_croppingAnnotationView];
        _selectTool.removeKnobShown = YES;
        [_selectTool setZoomOfView:zoomScale];
        
        if ([_elementLayer elementCount] > 0) {
            CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:_croppingAnnotationView];
            [renderer updateAllElements:_elementLayer];
            [renderer release];
            
            CZElement *element = [_elementLayer elementAtIndex:0];
            [_selectTool selectElement:element];
        }
        [self showROISizePanelWithMode:_croppingMode];
    }
}

- (void)updateCroppingModeWithROIRegion {
    CZElement *roiRegion = self.docManager.roiRegion;
    if (roiRegion) {
        CZElement *element = [roiRegion copy];
        element.measurementHidden = NO;
        element.strokeColor = kCZColorOrange;
        element.size = CZElementSizeLarge;
        element.dashLine = YES;
        element.measurementIDHidden = YES;
        element.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        
        if (_elementLayer == nil) {
            _elementLayer = [[CZElementLayer alloc] init];
        }
        
        [_elementLayer addElement:element];
        [element release];
    }
    
    if ([roiRegion isKindOfClass:[CZElementRectangle class]]) {
        _croppingMode = CZCroppingModeRectangle;
    } else if ([roiRegion isKindOfClass:[CZElementCircle class]]) {
        _croppingMode = CZCroppingModeCircle;
    } else if ([roiRegion isKindOfClass:[CZElementPolygon class]]) {
        _croppingMode = CZCroppingModeTriangle;
    } else {
        _croppingMode = CZCroppingModeUnknown;
    }
}

- (BOOL)hasDefaultROIWithCroppingMode:(CZCroppingMode)mode {
    CZElement *element = nil;
    id obj = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultROIRange];
    if ([obj isKindOfClass:[NSDictionary class]]) {
        id elementClass = [(NSDictionary *)obj objectForKey:@"class"];
        id elementShape = [(NSDictionary *)obj objectForKey:@"shape"];
        if ([elementClass isKindOfClass:[NSString class]] &&
            [elementShape isKindOfClass:[NSDictionary class]]) {
            if ([elementClass isEqualToString:NSStringFromClass([CZElementRectangle class])] && (mode == CZCroppingModeRectangle)) {
                element = [[CZElementRectangle alloc] init];
            } else if ([elementClass isEqualToString:NSStringFromClass([CZElementCircle class])] && (mode == CZCroppingModeCircle)) {
                element = [[CZElementCircle alloc] init];
            } else if ([elementClass isEqualToString:NSStringFromClass([CZElementPolygon class])] && (mode == CZCroppingModeTriangle)) {
                element = [[CZElementPolygon alloc] init];
            } else {
                return NO;
            }
            
            if ([element restoreFromShapeMemo:elementShape]) {
                CGRect imageFrame = CGRectZero;
                imageFrame.size = [[self workingImage] size];
                if (mode == CZCroppingModeRectangle) {
                    CGRect rect = [(CZElementRectangle *)element rect];
                    if (!CGRectIntersectsRect(rect, imageFrame)) {
                        [element release];
                        element = nil;
                    }
                } else if (mode == CZCroppingModeCircle) {
                    CGPoint center = [(CZElementCircle *)element center];
                    CGFloat r = [(CZElementCircle *)element radius];
                    if (!CGRectIntersectsCircle(imageFrame, center, r)) {
                        [element release];
                        element = nil;
                    }
                } else if (mode == CZCroppingModeTriangle) {
                    BOOL polygonIsInside = NO;
                    CZElementPolygon *polygon = (CZElementPolygon *)element;
                    for (NSUInteger i = 0; i < [polygon pointsCount]; i++) {
                        CGPoint pt = [polygon pointAtIndex:i];
                        if (CGRectContainsPoint(imageFrame, pt)) {
                            polygonIsInside = YES;
                            break;
                        }
                    }
                    
                    if (!polygonIsInside) {
                        [element release];
                        element = nil;
                    }
                }
            } else {
                [element release];
                element = nil;
            }
            
            if (element == nil) {
                return NO;
            }
        }
    }
  
    if (element) {
        if (mode == CZCroppingModeRectangle) {
            element.auxLayerCreater = [[[CZCropToolRectMeasurementTextCreator alloc] init] autorelease];
        } else if (mode == CZCroppingModeTriangle) {
            element.auxLayerCreater = [[[CZCropToolTriangleMeasurementTextCreator alloc] init] autorelease];
        } else {
            element.auxLayerCreater = [[[CZCropToolCircleMeasurementTextCreator alloc] init] autorelease];
        }
        
        element.strokeColor = kCZColorOrange;
        element.size = CZElementSizeLarge;
        element.dashLine = YES;
        element.measurementIDHidden = YES;
        element.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        if (_elementLayer == nil) {
            _elementLayer = [[CZElementLayer alloc] init];
        }
        [_elementLayer removeAllElements];
        [_elementLayer addElement:element];
        [_selectTool selectElement:element];
        [element release];
        
        return YES;
    }
    
    return NO;
}

- (void)updateCroppingModeWithDefaultROI {
    CZCroppingMode mode = CZCroppingModeUnknown;
    CZElement *element = nil;
    
    id obj = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultROIRange];
    if ([obj isKindOfClass:[NSDictionary class]]) {
        id elementClass = [(NSDictionary *)obj objectForKey:@"class"];
        id elementShape = [(NSDictionary *)obj objectForKey:@"shape"];
        if ([elementClass isKindOfClass:[NSString class]] &&
            [elementShape isKindOfClass:[NSDictionary class]]) {
            if ([elementClass isEqualToString:NSStringFromClass([CZElementRectangle class])]) {
                element = [[CZElementRectangle alloc] init];
                mode = CZCroppingModeRectangle;
            } else if ([elementClass isEqualToString:NSStringFromClass([CZElementCircle class])]) {
                element = [[CZElementCircle alloc] init];
                mode = CZCroppingModeCircle;
            } else if ([elementClass isEqualToString:NSStringFromClass([CZElementPolygon class])]) {
                element = [[CZElementPolygon alloc] init];
                mode = CZCroppingModeTriangle;
            }
            
            if ([element restoreFromShapeMemo:elementShape]) {
                CGRect imageFrame = CGRectZero;
                imageFrame.size = [[self workingImage] size];
                if (mode == CZCroppingModeRectangle) {
                    CGRect rect = [(CZElementRectangle *)element rect];
                    if (!CGRectIntersectsRect(rect, imageFrame)) {
                        [element release];
                        element = nil;
                    }
                } else if (mode == CZCroppingModeCircle) {
                    CGPoint center = [(CZElementCircle *)element center];
                    CGFloat r = [(CZElementCircle *)element radius];
                    if (!CGRectIntersectsCircle(imageFrame, center, r)) {
                        [element release];
                        element = nil;
                    }
                } else if (mode == CZCroppingModeTriangle) {
                    BOOL polygonIsInside = NO;
                    CZElementPolygon *polygon = (CZElementPolygon *)element;
                    for (NSUInteger i = 0; i < [polygon pointsCount]; i++) {
                        CGPoint pt = [polygon pointAtIndex:i];
                        if (CGRectContainsPoint(imageFrame, pt)) {
                            polygonIsInside = YES;
                            break;
                        }
                    }
                    
                    if (!polygonIsInside) {
                        [element release];
                        element = nil;
                    }
                }
            } else {
                [element release];
                element = nil;
            }
            
            if (element == nil) {
                mode = CZCroppingModeUnknown;
            }
        }
    }
    
    _croppingMode = mode;
    
    if (element) {        
        if (_croppingMode == CZCroppingModeRectangle) {
            element.auxLayerCreater = [[[CZCropToolRectMeasurementTextCreator alloc] init] autorelease];
        } else if (_croppingMode == CZCroppingModeTriangle) {
            element.auxLayerCreater = [[[CZCropToolTriangleMeasurementTextCreator alloc] init] autorelease];
        } else {
            element.auxLayerCreater = [[[CZCropToolCircleMeasurementTextCreator alloc] init] autorelease];
        }
        
        element.strokeColor = kCZColorOrange;
        element.size = CZElementSizeLarge;
        element.dashLine = YES;
        element.measurementIDHidden = YES;
        element.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        if (_elementLayer == nil) {
            _elementLayer = [[CZElementLayer alloc] init];
        }
        [_elementLayer addElement:element];
        [element release];
    }
}

- (void)setCroppingMode:(CZCroppingMode)mode {
    if (_setCroppingModeRecursiveLock) {
        return;
    }
    _setCroppingModeRecursiveLock = YES;
    
    if (_croppingMode != mode) {
        _croppingMode = mode;
        if (mode != CZCroppingModeUnknown) {
            BOOL hasDefaulltROI = [self hasDefaultROIWithCroppingMode:mode];
            if (!hasDefaulltROI) {
                [self updateElementOnCroppingView];
            }
        }
        if ([_delegate respondsToSelector:@selector(imageTool:didSwitchToCroppingMode:)]) {
            [_delegate imageTool:self didSwitchToCroppingMode:mode];
        }
        [self showROISizePanelWithMode:mode];
    }

    _setCroppingModeRecursiveLock = NO;
}

- (CZCroppingMode)croppingMode {
    return _croppingMode;
}

- (void)refreshROIRegion {
    CZElement *roiRegion = [self.elementLayer firstElement];
    if (roiRegion) {
        [roiRegion notifyWillChange];
        roiRegion.strokeColor = kCZColorOrange;
        roiRegion.size = CZElementSizeLarge;
        roiRegion.featureValidated = NO;
        [roiRegion notifyDidChange];
    }
}

- (void)deleteROIRegion {
    [self.elementLayer removeAllElements];
    [self setCroppingMode:CZCroppingModeUnknown];
    
    [self.roiSizePanelView removeFromSuperview];
    self.roiSizePanelView = nil;
}

- (void)dismissCroppingAndApply:(BOOL)shouldApply realCrop:(BOOL)isRealCrop {
    CZElement *element = ([self.elementLayer elementCount] > 0) ? [self.elementLayer elementAtIndex:0] : nil;
    if (element == nil && isRealCrop) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDefaultROIRange];
        shouldApply = NO;
    }
    
    if (_croppingView && shouldApply) {
        // save element as default ROI element
        NSString *elementClass = NSStringFromClass([element class]);
        NSDictionary *elementShape = [element newShapeMemo];
        if (elementClass && elementShape) {
            [[NSUserDefaults standardUserDefaults] setValue:@{@"class": elementClass, @"shape": elementShape} forKey:kDefaultROIRange];
        } else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDefaultROIRange];
        }
        [elementShape release];
        
        if (isRealCrop) {
            CGSize imageSize = [self workingImage].size;
            
            CGFloat scale = imageSize.width / _elementLayer.frame.size.width;
            if (self.params.rotation % 2 == 1) {
                scale = imageSize.height / _elementLayer.frame.size.width;
            }
            
            CZCompoundAction *compoundAction = [[CZCompoundAction alloc] init];
            compoundAction.undoReversed = NO;
            
            CZImageCropAction *cropAction = [[CZImageCropAction alloc] initWithDocManager:self.docManager
                                                                                imageTool:self
                                                                                  element:element
                                                                                    scale:scale];
            [compoundAction addAction:cropAction];
            [cropAction release];
            
            // Reset the corner position, so that it will move to the right place.
            id<CZUndoAction> cornerAction = [self newScaleBarSetCornerAction];
            [compoundAction addAction:cornerAction];
            [cornerAction release];
            
            // Apply other existing changes before going cropping.
            [self apply];
            
            [_docManager pushAction:compoundAction];
            [compoundAction release];
            
            [_docManager setRoiRegion:nil];
        } else {
            [element retain];
            [element removeFromParent];
            
            [self apply];
            
            [_docManager setRoiRegion:element];
            [_docManager.multiphase setProcessingImage:[_docManager.document outputImage]];
            [_docManager.multiphase updatePhaseAreas];
            
            [element release];
        }
    } else {
        [self cleanUpInteractiveViewsExcept:nil];
    }
}

- (void)setGamma:(CGFloat)gamma {
    if (self.params.gamma == gamma) {
        return;
    }
    
    self.params.gamma = gamma;
    _needsApplying = YES;
    [self invalidate];
}

- (void)setBrightness:(CGFloat)brightness {
    if (self.params.brightness == brightness) {
        return;
    }
    
    self.params.brightness = brightness;
    _needsApplying = YES;
    [self invalidate];
}

- (void)setContrast:(CGFloat)contrast {
    if (self.params.contrast == contrast) {
        return;
    }
    
    self.params.contrast = contrast;
    _needsApplying = YES;
    [self invalidate];
}

- (void)setColorIntensity:(CGFloat)colorIntensity {
    if (self.params.colorIntensity == colorIntensity) {
        return;
    }
    
    self.params.colorIntensity = colorIntensity;
    _needsApplying = YES;
    [self invalidate];
}

- (void)setSharpness:(CGFloat)sharpness {
    if (self.params.sharpness == sharpness) {
        return;
    }
    
    self.params.sharpness = sharpness;
    _hasSharpnessChanges = (sharpness != 1.0f);
    _needsApplying = YES;
    [self invalidate];
}

- (void)showInteractiveDisplayCurveOnView:(UIView *)view withFrame:(CGRect)frame {
    if (_displayCurveView == nil) {
        _displayCurveView = [[CZImageOutputView alloc] initWithMainImageViewIndex:_curvePreset];
        [self showInteractiveView:_displayCurveView
                           onView:view
                        withFrame:frame
                          andMode:kImageOutputViewGridMode];
    }
}

- (void)applyDisplayCurvePreset:(CZDisplayCurvePreset)preset {
    if (_curvePreset != preset) {
        _curvePreset = preset;
        [self invalidate];
    }
}

- (void)setLayerThicknessEdgeLayer:(CALayer *)layer {
    CALayer *parentLayer = self.imageView.layer;
    if (parentLayer == nil) {
        return;
    }
    
    self.thicknessEdgeLayer = layer;
    
    CALayer *oldEdgeLayer = nil;
    CALayer *annotationLayer = nil;
    
    for (CALayer *sub in [parentLayer sublayers]) {
        if ([sub.name isEqualToString:CZLayerThicknessEdgeLayerName]) {
            oldEdgeLayer = sub;
            continue;
        }
        
        if ([sub.name isEqualToString:kAnnotationLayerName]) {
            annotationLayer = sub;
            continue;
        }
    }
    
    [oldEdgeLayer removeFromSuperlayer];
    
    if (layer) {
        layer.hidden = self.thicknessEdgeLayerHidden;
        
        if (annotationLayer) {
            [parentLayer insertSublayer:layer below:annotationLayer];
        } else {
            [parentLayer addSublayer:layer];
        }
    }
}

- (void)setLayerThicknessEdgeLayerHidden:(BOOL)hidden {
    self.thicknessEdgeLayerHidden = hidden;
    self.thicknessEdgeLayer.hidden = hidden;
}

- (void)setMultiphaseLayerContentsWithImage:(UIImage *)image {
    self.multiphaseLayer.contents = (id)[image CGImage];
    [self.multiphaseLayer removeAllAnimations];
}

- (CGImageRef)multiphaseLayerImage {
    return (CGImageRef)self.multiphaseLayer.contents;
}

#pragma mark - Private methods

- (void)invalidateAndGenerateImage:(UIImage **)output
               ignoringMinimumArea:(BOOL)ignoresMinimumArea {
    UIImage *source = [[self workingImage] retain];
    
    // Do not invalidate the image view when image is not available.
    if (source == nil) {
        return;
    }
    
    if (self.gpuImageSource == nil) {
        @autoreleasepool {
            GPUImagePicture *picture = [[GPUImagePicture alloc] initWithImage:source];
            self.gpuImageSource = picture;
            [picture release];
        }
    } else {
        [self.gpuImageSource removeAllTargets];
    }
    
    GPUImagePicture *gpuImageSource = [self.gpuImageSource retain];
    
    GPUImageOutput<GPUImageInput> *compoundFilter = [_params compoundFilter];
    
    NSMutableArray *displayFilters = [[NSMutableArray alloc] init];
    
    if (_overExposureEnabled) {
        CZOverExposeFilter *overExposureFilter = [[CZOverExposeFilter alloc] init];
        [displayFilters addObject:overExposureFilter];
        [overExposureFilter release];
    }
    
    CZMultiphase *multiphase = self.docManager.multiphase;
    const BOOL showMuliphseOverlay = (self.multiphaseEnabled &&
                                      multiphase.phaseCount > 0 &&
                                      _croppingView == NULL);
    
    if (!_ignoresDisplayCurve && !showMuliphseOverlay) {
        GPUImageOutput<GPUImageInput> *curveFilter = [CZDisplayCurvePresetTool newFilterForDisplayCurvePreset:_curvePreset
                                                                                                withImageSize:source.size
                                                                                                     forVideo:NO];
        if (curveFilter != nil) {
            [displayFilters addObject:curveFilter];
            [curveFilter release];
        }
    }
    
    BOOL shouldClearMultiphaseFilter = NO;
    if (showMuliphseOverlay) {
        BOOL useLookupFilter = YES;
        if (!ignoresMinimumArea) {
            useLookupFilter = [[self class] multiphaseShouldUpdateWithGPU:multiphase];
        }

        if (useLookupFilter) {
            self.multiphaseLayer.hidden = YES;
            
            CZLookupClipType requireClipType = CZLookupClipTypeNone;
            CZElement *roiRegion = self.docManager.roiRegion;
            if (roiRegion) {
                if ([roiRegion isKindOfClass:[CZElementRectangle class]]) {
                    requireClipType = CZLookupClipTypeRectangle;
                } else if ([roiRegion isKindOfClass:[CZElementPolygon class]]) {
                    requireClipType = CZLookupClipTypeTriangle;
                } else if ([roiRegion isKindOfClass:[CZElementCircle class]]) {
                    requireClipType = CZLookupClipTypeEllipse;
                }
            }
            
            if (_multiphaseLookupFilter) {
                if (requireClipType != _multiphaseLookupFilter.lookupTwoInputFilter.clipType) {
                    [_multiphaseLookupFilter release];
                    _multiphaseLookupFilter = nil;
                }
            }
            
            if (_multiphaseLookupFilter == nil) {
                _multiphaseLookupFilter = [[CZLookupFilter alloc] initWithColorTable:[self.docManager.multiphase convertToColorTable]
                                                                            clipType:requireClipType];
            } else {
                [_multiphaseLookupFilter setColorTable:[self.docManager.multiphase convertToColorTable]];
            }
            
            CGSize imageSize = self.docManager.elementLayer.frame.size;
            CGAffineTransform transform = CGAffineTransformMakeScale(1.0 / imageSize.width, 1.0 / imageSize.height);
            
            if ([roiRegion isKindOfClass:[CZElementRectangle class]]) {
                CZElementRectangle *rectangle = (CZElementRectangle *)roiRegion;
                CGRect rect = [rectangle rect];
                CGPoint center;
                center.x = CGRectGetMidX(rect);
                center.y = CGRectGetMidY(rect);
                
                CGFloat rotateAngle = [rectangle rotateAngle];
                CGAffineTransform combineTransform = CGAffineTransformMakeRotationAtPoint(rotateAngle, center);
                combineTransform = CGAffineTransformConcat(combineTransform, transform);
                
                [_multiphaseLookupFilter.lookupTwoInputFilter setClipRect:rect
                                                              transform:combineTransform];
                
            } else if ([roiRegion isKindOfClass:[CZElementPolygon class]]) {
                if ([(CZElementPolygon *)roiRegion pointsCount] > 2) {
                    CZElementPolygon *polygon = (CZElementPolygon *)roiRegion;
                    CGPoint pt1 = [polygon pointAtIndex:0];
                    CGPoint pt2 = [polygon pointAtIndex:1];
                    CGPoint pt3 = [polygon pointAtIndex:2];
                    
                    pt1 = CGPointApplyAffineTransform(pt1, transform);
                    pt2 = CGPointApplyAffineTransform(pt2, transform);
                    pt3 = CGPointApplyAffineTransform(pt3, transform);
                    
                    [_multiphaseLookupFilter.lookupTwoInputFilter setTrianglePoint1:pt1
                                                                             point2:pt2
                                                                             point3:pt3];
                }
            } else if ([roiRegion isKindOfClass:[CZElementCircle class]]) {
                CZElementCircle *circle = (CZElementCircle *)roiRegion;
                CGPoint center = [circle center];
                center = CGPointApplyAffineTransform(center, transform);
                
                CGSize radius = CGSizeMake([circle radius], [circle radius]);
                radius = CGSizeApplyAffineTransform(radius, transform);
                
                [_multiphaseLookupFilter.lookupTwoInputFilter setEllipseCenter:center radius:radius];
            }
            
            if (_multiphaseLookupFilter) {
                [displayFilters addObject:_multiphaseLookupFilter];
            }
        } else {
            shouldClearMultiphaseFilter = YES;
            self.multiphaseLayer.hidden = NO;
            [self.multiphaseLayer removeAllAnimations];
        }
    } else {
        shouldClearMultiphaseFilter = YES;
        self.multiphaseLayer.hidden = YES;
    }
    
    if (shouldClearMultiphaseFilter) {
        [_multiphaseLookupFilter release];
        _multiphaseLookupFilter = nil;
    }

    GPUImageOutput<GPUImageInput> *compoundDisplayFilter = [CZImageTool filterFromArray:displayFilters];
    
    // Optimization for image processing.
    CGSize targetSize = source.size;
    if (_params.rotation % 2 == 1) {
        targetSize = CGSizeMake(source.size.height, source.size.width);
    }
    [compoundFilter forceProcessingAtSize:targetSize];
    [compoundDisplayFilter forceProcessingAtSize:targetSize];
    
    // Create the queue for rendering.
    GPUImageOutput *lastOutput = gpuImageSource;
    if (compoundFilter != nil) {
        [gpuImageSource addTarget:compoundFilter];
        lastOutput = compoundFilter;
    }
    if (compoundDisplayFilter != nil) {
        [lastOutput addTarget:compoundDisplayFilter];
        lastOutput = compoundDisplayFilter;
    }

    [lastOutput addTarget:_gpuImageView];
    if (output && lastOutput != gpuImageSource) {
        // imageFromCurrentFramebufferWithOrientation require set useNextFrameForImageCapture to YES
        [lastOutput useNextFrameForImageCapture];
    }
    [gpuImageSource processImage];
    
    // If output pointer is set, save the render result.
    if (output) {
        UIImage *result = nil;
        if (lastOutput == gpuImageSource) {
            result = [[source retain] autorelease];
        } else {
            result = [lastOutput imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
        }
        
        *output = result;
    }

    [displayFilters release];
    [source release];
    [gpuImageSource release];
}

- (void)showInteractiveView:(CZImageOutputView *)interactiveView
                     onView:(UIView *)view
                  withFrame:(CGRect)frame
                    andMode:(CZImageOutputViewMode)mode {
#if CZ_IMAGE_PROCESSING_REFACTORED
    [interactiveView setBackgroundColor:kDefaultBackGroundColor];
#endif
    [interactiveView setFrame:frame];
    [interactiveView setDelegate:self];
    [interactiveView setWillAutoDismiss:NO];
    [interactiveView setAnimationEnabled:NO];
    
    if (mode != kImageOutputViewSingleMode) {
        [interactiveView setAlpha:0.0];
        [interactiveView showSupplementalViewsWithMode:mode];
    }
    
    [view addSubview:interactiveView];
    [view bringSubviewToFront:interactiveView];
}

- (void)cleanUpInteractiveViewsExcept:(CZImageOutputView *)view {
    if (view != _displayCurveView) {
        [_displayCurveView dismissSupplementalViewsWithCompleteHandler:^{
            [_displayCurveView removeFromSuperview];
            self.displayCurveView.delegate = nil;
            self.displayCurveView = nil;
        }];
    }
    if (view != _rotationView) {
        [_rotationView dismissSupplementalViewsWithCompleteHandler:^{
            [_rotationView removeFromSuperview];
            self.rotationView.delegate = nil;
            self.rotationView = nil;
        }];
    }
    if (view != _mirroringView) {
        [_mirroringView dismissSupplementalViewsWithCompleteHandler:^{
            [_mirroringView removeFromSuperview];
            self.mirroringView.delegate = nil;
            self.mirroringView = nil;
        }];
    }
    if (view != _croppingView) {
        [_croppingAnnotationView removeFromSuperview];
        self.croppingAnnotationView = nil;
        
        [_croppingView dismissSupplementalViewsWithCompleteHandler:^{
            [_croppingView removeFromSuperview];
            self.croppingView.delegate = nil;
            self.croppingView = nil;
        }];
        
        [self.selectTool touchCancelled:CGPointZero];
        self.selectTool = nil;
        [_elementLayer release];
        _elementLayer = nil;
    }
}

- (NSArray *)readROISizeWithMode:(CZCroppingMode)mode {
    NSMutableArray *sizeContents = [NSMutableArray array];
    
    CZElement *element = [self.elementLayer firstElement];
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [element scalingOfUnit:unitStyle];
    
    if (mode == CZCroppingModeUnknown) {
        return nil;
    } else if (mode == CZCroppingModeRectangle) {
        if ([element isKindOfClass:[CZElementRectangle class]]) {
            
            CZElementRectangle *rect = (CZElementRectangle *)element;
            CGSize rectSize = rect.rect.size;
            
            NSString *widthString = [CZCommonUtils localizedStringFromFloat:scaling * rectSize.width precision:[element precision]];
            [sizeContents addObject:widthString];
            
            NSString *heightString = [CZCommonUtils localizedStringFromFloat:scaling * rectSize.height precision:[element precision]];
            [sizeContents addObject:heightString];
        }
    } else if (mode == CZCroppingModeTriangle) {
        if ([element isKindOfClass:[CZElementPolygon class]]) {
            
            CZElementPolygon *polygon = (CZElementPolygon *)element;
            if (polygon.pointsCount != 3) {
                return nil;
            }
            
            NSString *distanceString;
            for (NSUInteger i = 0; i < 3; i++) {
                CGPoint ptA = [polygon pointAtIndex:i];
                CGPoint ptB = [polygon pointAtIndex:(i + 1) % 3];
                
                float distanceValue = scaling * sqrt((ptA.x - ptB.x) * (ptA.x - ptB.x) + (ptA.y - ptB.y) * (ptA.y - ptB.y));
                distanceString = [CZCommonUtils localizedStringFromFloat:distanceValue precision:[element precision]];
                
                [sizeContents addObject:distanceString];
            }
        }
    } else if (mode == CZCroppingModeCircle) {
        if ([element isKindOfClass:[CZElementCircle class]]) {
            CZElementCircle *circle = (CZElementCircle *)element;
            
            CGFloat radiusValue = circle.radius;
            NSString *diameterString = [CZCommonUtils localizedStringFromFloat:scaling * radiusValue * 2 precision:[element precision]];
            [sizeContents addObject:diameterString];
        }
    }
    NSArray *roiSizeContents = [[sizeContents copy] autorelease];
    return roiSizeContents;
}

- (CGSize)calculateSuitableProcessingSize:(CGSize)imageSize
                                 forParam:(CZImageToolParams *)params
                     isForInteractiveView:(BOOL)isInteractive {
    if (params.rotation % 2 == 1) {
        // If the image has been rotated by 90 degree, swap the width/height.
        CGFloat tempForSwap = imageSize.height;
        imageSize.height = imageSize.width;
        imageSize.width = tempForSwap;
    }
    
    CGFloat zoomScale = [[UIScreen mainScreen] scale];
    if (!isInteractive && [_delegate respondsToSelector:@selector(currentZoomScaleForImageTool:)]) {
        zoomScale *= [_delegate currentZoomScaleForImageTool:self];
    }
    
    CGSize targetFrame = _gpuImageView.frame.size;
    targetFrame.width *= zoomScale;
    targetFrame.height *= zoomScale;
    
    if (isInteractive) {
        if (params.rotation % 2 == 1) {
            CGFloat tempForSwap = targetFrame.height;
            targetFrame.height = targetFrame.width;
            targetFrame.width = tempForSwap;
        }
        
        targetFrame.width /= 2;
        targetFrame.height /= 2;
    }
    
    CGFloat widthRatio = targetFrame.width / imageSize.width;
    CGFloat heightRatio = targetFrame.height / imageSize.height;
    
    if (widthRatio >= 1.0 || heightRatio >= 1.0) {
        return imageSize;
    }
    
    CGSize processingSize;
    if (widthRatio < heightRatio) {
        processingSize.width = targetFrame.width;
        processingSize.height = round(targetFrame.width * imageSize.height / imageSize.width);
    } else {
        processingSize.width = round(targetFrame.height * imageSize.width / imageSize.height);
        processingSize.height = targetFrame.height;
    }
    
    return processingSize;
}

- (id<CZUndoAction>)newScaleBarSetCornerAction {
    // Search scale bar.
    CZElementScaleBar *scaleBar = nil;
    for (NSUInteger i = 0; i < _docManager.elementLayer.elementCount; ++i) {
        CZElement *element = [_docManager.elementLayer elementAtIndex:i];
        if ([element isMemberOfClass:[CZElementScaleBar class]]) {
            scaleBar = (CZElementScaleBar *)element;
            break;
        }
    }
    
    // Reset the corner position, so that it will move to the right place.
    if (scaleBar) {
        CZScaleBarSetCornerAction *cornerAction = [[CZScaleBarSetCornerAction alloc] initWithScaleBar:scaleBar
                                                                                          newPosition:scaleBar.position];
        return cornerAction;
    } else {
        return nil;
    }
}

- (void)updateScaleBarPosition {
    // Update the scale bar's position.
    id<CZUndoAction> cornerAction = [self newScaleBarSetCornerAction];
    if (cornerAction) {
        [cornerAction do];
        [cornerAction release];
    }
}

- (void)updateElementOnCroppingView {
    if (_croppingView) {
        [_elementLayer removeAllElements];
        
        CGRect frame = [_elementLayer frame];
        CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
        CGFloat width = frame.size.width;
        CGFloat height = frame.size.height;
        CGFloat shortSize = MIN(width, height);
        
        CZElement *element = nil;
        if (_croppingMode == CZCroppingModeRectangle) {
            element = [[CZElementRectangle alloc] initWithRect:CGRectMake(width / 4.0f, height / 4.0f,
                                                                          width / 2.0f, height / 2.0f)];
        } else if (_croppingMode == CZCroppingModeTriangle) {
            CGFloat halfEdgeLength = shortSize / 4.0f;
            CGFloat halfHeight = halfEdgeLength / 2.0f * sqrt(3.0f);
            
            CZElementPolygon *triangle = [[CZElementPolygon alloc] init];
            
            [triangle appendPoint:CGPointMake(center.x + halfEdgeLength, center.y + halfHeight)];
            [triangle appendPoint:CGPointMake(center.x, center.y - halfHeight)];
            [triangle appendPoint:CGPointMake(center.x - halfEdgeLength, center.y + halfHeight)];
            element = triangle;
        } else if (_croppingMode == CZCroppingModeCircle) {
            element = [[CZElementCircle alloc] initWithCenter:center
                                                       radius:shortSize / 4.0f];
        }
        
        if (_croppingMode == CZCroppingModeRectangle) {
            element.auxLayerCreater = [[[CZCropToolRectMeasurementTextCreator alloc] init] autorelease];
        } else if (_croppingMode == CZCroppingModeTriangle) {
            element.auxLayerCreater = [[[CZCropToolTriangleMeasurementTextCreator alloc] init] autorelease];
        } else {
            element.auxLayerCreater = [[[CZCropToolCircleMeasurementTextCreator alloc] init] autorelease];
        }
        
        element.strokeColor = kCZColorOrange;
        element.size = CZElementSizeLarge;
        element.dashLine = YES;
        element.measurementIDHidden = YES;
        element.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        [_elementLayer addElement:element];
        [_selectTool selectElement:element];
        [element release];
    }
}

- (void)notifyImageSizeChanged {
    if ([_delegate respondsToSelector:@selector(imageTool:changedImageSizeTo:)]) {
        UIImage *image = [self workingImage];
        if (self.params.rotation % 2 == 1) {
            [_delegate imageTool:self changedImageSizeTo:CGSizeMake(image.size.height, image.size.width)];
        } else {
            [_delegate imageTool:self changedImageSizeTo:image.size];
        }
    }
}

+ (GPUImageOutput<GPUImageInput> *)filterFromArray:(NSArray *)filterLists {
    NSUInteger filterCount = [filterLists count];
    if (filterCount == 0) {
        return nil;
    } else if (filterCount == 1) {
        GPUImageFilter *filter = [[[filterLists objectAtIndex:0] retain] autorelease];
        return filter;
    } else {
        GPUImageFilterGroup *group = [[GPUImageFilterGroup alloc] init];
        
        for (NSUInteger i = 0; i < filterCount; ++i) {
            GPUImageFilter *filter = [filterLists objectAtIndex:i];
            [group addFilter:filter];
            
            if (i < filterCount - 1) {
                GPUImageFilter *nextFilter = [filterLists objectAtIndex:i + 1];
                [filter addTarget:nextFilter];
            }
        }
        
        GPUImageFilter *firstFilter = [filterLists objectAtIndex:0];
        GPUImageFilter *lastFilter = [filterLists objectAtIndex:filterCount - 1];
        
        [group setInitialFilters:[NSArray arrayWithObject:firstFilter]];
        [group setTerminalFilter:lastFilter];
        
        return [group autorelease];
    }
}

#pragma mark - Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (_docManager && object == _docManager) {
        if ([keyPath isEqualToString:@"document"]) {
            self.gpuImageSource = nil;
            [self invalidate];
        }
    } else if (object == _gpuImageView) {
        if ([keyPath isEqualToString:@"frame"]) {
            self.multiphaseLayer.frame = self.gpuImageView.bounds;
        }
    }
}

#pragma mark - CZImageOutputViewDelegate methods

- (void)imageOutputViewDidShowSupplementalViews:(CZImageOutputView *)view {
    @autoreleasepool {
        UIView *supplementalView = view.imageViews[0];
        CGSize outputCellSize = supplementalView.frame.size;
        NSAssert(supplementalView != nil && outputCellSize.width > 0 && outputCellSize.height > 0, @"require supplemental view exist and has proper size.");
        outputCellSize.width *= [[UIScreen mainScreen] scale];
        outputCellSize.height *= [[UIScreen mainScreen] scale];
        CGSize sourceImageSize = [[self workingImage] size];
        BOOL doesShrinkImage = (sourceImageSize.width > outputCellSize.width) || (sourceImageSize.height > outputCellSize.height);
        
        UIImage *source = nil;
        if (doesShrinkImage) {
            CGFloat scale = sourceImageSize.width / sourceImageSize.height;
            
            // processing image's width must be times of 8.
            int l = outputCellSize.width;
            l = l / 8 * 8;
            outputCellSize.width = l;
            outputCellSize.height = floor(l / scale);
            
            UIGraphicsBeginImageContextWithOptions(outputCellSize, YES, 1.0);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
            [[self workingImage] drawInRect:CGRectMake(0, 0, outputCellSize.width, outputCellSize.height)];
            source = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            [source retain];
        } else {
            source = [[self workingImage] retain];
        }
        GPUImagePicture *gpuImageSource = nil;
        @autoreleasepool {
            gpuImageSource = [[GPUImagePicture alloc] initWithImage:source];
        }
        
        for (NSUInteger i = 0; i < 4; ++i) {
            UILabel *imageLabel = [view imageLabels][i];
            [view setLabelsVisible:NO];
            
            CZImageToolParams *tempParams = [_params copy];
            CZDisplayCurvePreset curvePreset = CZDisplayCurvePresetInvalid;
            
            if (_rotationView == view) {
                switch (i) {
                    case 1:
                        // Rotate 90 CCW for the second output view.
                        tempParams.rotation = (tempParams.rotation + 3) % 4;
                        [imageLabel setText:L(@"IMAGE_ROTATE_90_CCW")];
                        break;
                        
                    case 2:
                        // Rotate 90 CW for the third output view.
                        tempParams.rotation = (tempParams.rotation + 1) % 4;
                        [imageLabel setText:L(@"IMAGE_ROTATE_90_CW")];
                        break;
                        
                    case 3:
                        // Rotate 180 for the last output view.
                        tempParams.rotation = (tempParams.rotation + 2) % 4;
                        [imageLabel setText:L(@"IMAGE_ROTATE_180")];
                        break;
                        
                    default:
                        [imageLabel setText:L(@"IMAGE_ORIGINAL")];
                        break;
                }
            } else if (_mirroringView == view) {
                switch (i) {
                    case 1:
                        // Do horizontal mirroring for the second output view.
                        tempParams.horizontalFlipping = !tempParams.horizontalFlipping;
                        [imageLabel setText:L(@"IMAGE_MIRROR_HORIZONTAL")];
                        break;
                        
                    case 2:
                        // Do vertical mirroring for the third output view.
                        tempParams.verticalFlipping = !tempParams.verticalFlipping;
                        [imageLabel setText:L(@"IMAGE_MIRROR_VERTICAL")];
                        break;
                        
                    case 3:
                        // Rotate 180 for the last output view.
                        tempParams.rotation = (tempParams.rotation + 2) % 4;
                        [imageLabel setText:L(@"IMAGE_MIRROR_BOTH")];
                        break;
                        
                    default:
                        [imageLabel setText:L(@"IMAGE_ORIGINAL")];
                        break;
                }
            } else if (_displayCurveView == view) {
                [imageLabel setText:[CZDisplayCurvePresetTool nameOfDisplayCurvePreset:i]];
                curvePreset = i;
            }
            
            [view setLabelsVisible:YES];
            
            GPUImageOutput<GPUImageInput> *compoundFilter = [tempParams compoundFilter];
            GPUImageView *imageView = [view imageViews][i];
            
            GPUImageOutput<GPUImageInput> *curveFilter = [CZDisplayCurvePresetTool newFilterForDisplayCurvePreset:curvePreset
                                                                                                    withImageSize:source.size
                                                                                                         forVideo:NO];
            
            CZOverExposeFilter *overExposeFilter = nil;
            if (self.overExposureEnabled) {
                overExposeFilter = [[CZOverExposeFilter alloc] init];
            }
            
            CGSize targetSize = source.size;
            if (curvePreset != CZDisplayCurveBestFit) {
                // Reduce the processing size since there are always 4 outputs which means
                // that each one will be smaller than 1/4 of the original size.
                targetSize = [self calculateSuitableProcessingSize:targetSize
                                                          forParam:tempParams
                                                      isForInteractiveView:YES];
            }
            [compoundFilter forceProcessingAtSize:targetSize];
            [curveFilter forceProcessingAtSize:targetSize];
            [overExposeFilter forceProcessingAtSize:targetSize];
            
            GPUImageOutput *lastOutput = gpuImageSource;
            if (compoundFilter) {
                [lastOutput addTarget:compoundFilter];
                lastOutput = compoundFilter;
            }
            if (overExposeFilter) {
                [lastOutput addTarget:overExposeFilter];
                lastOutput = overExposeFilter;
            }
            if (curveFilter) {
                [lastOutput addTarget:curveFilter];
                lastOutput = curveFilter;
            }
            [lastOutput addTarget:imageView];
            
            [tempParams release];
            [curveFilter release];
            [overExposeFilter release];
        }
        
        [gpuImageSource processImage];
        [gpuImageSource release];
        [source release];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^() {
                         [view setAlpha:1.0];
                     }
                     completion:^(BOOL finished) {
                         // Cancel running interactive operation.
                         [self cleanUpInteractiveViewsExcept:view];
                     }];
}

- (void)imageOutputView:(CZImageOutputView *)view didChangeMainViewIndexTo:(NSUInteger)index {
    if (view == _rotationView) {
        switch ([_rotationView mainImageViewIndex]) {
            case 1:
                [self rotateCounterClockwise];
                break;
                
            case 2:
                [self rotateClockwise];
                break;
                
            case 3:
                [self rotate180];
                
            default:
                break;
        }
    } else if (view == _mirroringView) {
        switch ([_mirroringView mainImageViewIndex]) {
            case 1:
                [self flipHorizontally];
                break;
                
            case 2:
                [self flipVertically];
                break;
                
            case 3:
                [self rotate180];
                break;
                
            default:
                break;
        }
    } else if (view == _displayCurveView) {
        [self applyDisplayCurvePreset:[_displayCurveView mainImageViewIndex]];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^() {
                         [view setAlpha:0.0];
                     }
                     completion:^(BOOL finished) {
                         [view dismissSupplementalViews];
                     }];
}

- (void)imageOutputViewDidDismissSupplementalViews:(CZImageOutputView *)view {
    if (view == _rotationView) {
        if ([_delegate respondsToSelector:@selector(imageToolDidDismissInteractiveRotation:)]) {
            [_delegate imageToolDidDismissInteractiveRotation:self];
        }
    } else if (view == _mirroringView) {
        if ([_delegate respondsToSelector:@selector(imageToolDidDismissInteractiveMirroring:)]) {
            [_delegate imageToolDidDismissInteractiveMirroring:self];
        }
    } else if (view == _displayCurveView) {
        if ([_delegate respondsToSelector:@selector(imageTool:didDismissInteractiveDisplayCurve:)]) {
            [_delegate imageTool:self didDismissInteractiveDisplayCurve:_curvePreset];
        }
    }
    
    [self cleanUpInteractiveViewsExcept:nil];
}

- (CZShouldBeginTouchState)imageOutputView:(CZImageOutputView *)view shouldBeginAnnotationTouch:(UITouch *)touch {
    return (view == _croppingView);
}

- (BOOL)imageOutputView:(CZImageOutputView *)view gesture:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    if ([gesture isKindOfClass:[CZAnnotationGestureRecognizer class]]) {
        if (view == _croppingView) {
            if (_croppingMode != CZCroppingModeUnknown && self.roiSizePanelView) {
                CGPoint point = [touch locationInView:self.roiSizePanelView];
                return ![self.roiSizePanelView pointInside:point withEvent:nil];
            } else {
                return YES;
            }
        }
    }

    return NO;
}

- (void)imageOutputView:(CZImageOutputView *)view handlePanGesture:(UIGestureRecognizer *)recognizer {
    if (view != _croppingView) {
        return;
    }
    
    CGPoint pointInView = [recognizer locationInView:_croppingAnnotationView];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [_selectTool touchBegin:pointInView];
        _beginPoint = pointInView;
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        [_selectTool touchMoved:pointInView];
        [self.roiSizePanelView removeFromSuperview];
        self.roiSizePanelView = nil;
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        [_selectTool touchEnd:pointInView];
        [self showROISizePanelWithMode:_croppingMode];
    } else if (recognizer.state == UIGestureRecognizerStateCancelled) {
        [_selectTool touchCancelled:pointInView];
    }
}

- (void)imageOutputView:(CZImageOutputView *)view handleTapGesture:(UIGestureRecognizer *)recognizer {
    if (view != _croppingView) {
        return;
    }
    
    CGPoint pointInView = [recognizer locationInView:_croppingAnnotationView];
    [self.selectTool tap:pointInView];
    [self showROISizePanelWithMode:_croppingMode];
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer didAddElement:(CZElement *)element {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.croppingAnnotationView];
    [renderer layer:layer didAddElement:element];
    [renderer release];
}

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.croppingAnnotationView];
    [renderer layer:layer didRemoveElement:element];
    [renderer release];
    
    if ([_selectTool respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [_selectTool layer:layer didRemoveElement:element];
    }
    
    if (layer.elementCount == 0) {
        [self setCroppingMode:CZCroppingModeUnknown];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {
    CZElementLayerRenderer *renderer = [[CZElementLayerRenderer alloc] initWithTargetView:self.croppingAnnotationView];
    [renderer layer:layer didChangeElement:element];
    [renderer hideElementOnly:self.selectTool.temporarilyHiddenElement inElementLayer:layer];
    [renderer release];
    
    if ([_selectTool respondsToSelector:@selector(layer:didChangeElement:)]) {
        [_selectTool layer:layer didChangeElement:element];
    }
}

#pragma mark - CZROISizePanelView delegate method

- (void)roiSizePanelView:(CZROISizePanelView *)view didUpdateROIRegionWithSize:(NSArray *)size {
    if (_croppingView) {
        CZElement *element = [_elementLayer firstElement];
        CZElementUnitStyle unitStyle = [CZElement unitStyle];
        CGFloat scaling = [_elementLayer scalingOfUnit:unitStyle];
        CGPoint center;
        if ([element isKindOfClass:[CZElementRectangle class]]) {
            CZElementRectangle *rect = (CZElementRectangle *) element;
            center = [rect anchorPoint];
            CGFloat width = [[CZCommonUtils numberFromLocalizedString:size[0]] floatValue] / scaling ;
            CGFloat height = [[CZCommonUtils numberFromLocalizedString:size[1]] floatValue] / scaling;
            
            rect.rect = CGRectMake(center.x - width / 2.0f , center.y - height / 2.0f, width, height);
            
        } else if ([element isKindOfClass:[CZElementPolygon class]]) {
            CZElementPolygon *triangle = (CZElementPolygon *)element;
            CGPoint ptA = [triangle pointAtIndex:0];
            CGPoint ptB = [triangle pointAtIndex:1];
            CGPoint ptC = [triangle pointAtIndex:2];
            
            center.x = (ptA.x + ptB.x + ptC.x) / 3.0f;
            center.y = (ptA.y + ptB.y + ptC.y) / 3.0f;

            CGFloat halfEdgeLength = [[CZCommonUtils numberFromLocalizedString:size[0]] floatValue] / (scaling * 2.0);
            CGFloat halfHeight = halfEdgeLength / sqrt(3.0f);
            
            [triangle setPoint:CGPointMake(center.x + halfEdgeLength, center.y + halfHeight) atIndex:0];
            [triangle setPoint:CGPointMake(center.x, center.y - halfHeight * 2) atIndex:1];
            [triangle setPoint:CGPointMake(center.x - halfEdgeLength, center.y + halfHeight) atIndex:2];
        } else if ([element isKindOfClass:[CZElementCircle class]]) {
            CZElementCircle *circle = (CZElementCircle *) element;
            float radius = [[CZCommonUtils numberFromLocalizedString:size[0]] floatValue] / (scaling * 2.0);
            circle.radius = radius;
        }
        [element notifyDidChange];
    }
}

#pragma mark - CZUndoManaging methods

- (void)pushAction:(id<CZUndoAction>)anAction {
    [anAction do];
}

- (void)undo {
}

- (void)redo {
}

- (BOOL)canUndo {
    return NO;
}

- (BOOL)canRedo {
    return NO;
}

@end
