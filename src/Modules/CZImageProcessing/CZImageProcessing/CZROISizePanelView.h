//
//  CZROISizePanelView.h
//  Matscope
//
//  Created by Sherry Xu on 7/17/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZImageTool.h"

@protocol CZROISizePanelViewDelegate;

@interface CZROISizePanelView : UIView

@property (nonatomic, assign) id<CZROISizePanelViewDelegate> delegate;
@property (nonatomic, assign) NSUInteger precision;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, assign) CGFloat inputMaxValue;
@property (nonatomic, assign) CGFloat inputMinValue;

- (id)initWithFrame:(CGRect)frame;
- (void)updateWithCroppingMode:(CZCroppingMode)mode;
- (void)updateTextFieldsWithContents:(NSArray *)contents;

@end

@protocol CZROISizePanelViewDelegate <NSObject>

@optional

- (void)roiSizePanelView:(CZROISizePanelView *)view didUpdateROIRegionWithSize:(NSArray *)size;

@end
