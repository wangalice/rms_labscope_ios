//
//  CZVideoDenoiser.h
//  Matscope
//
//  Created by Ralph Jin on 7/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//typedef NS_ENUM(NSUInteger, CZDenoiseOption) {
//    CZDenoiseOptionMedianAndAverage,
//    CZDenoiseOption3DMedian,
//    CZDenoiseOptionMedianAndSharp,
//    CZDenoiseOptionAverage
//};

/**
 * Video denoiser implemented in 3 dimension, 3x3x3 median filter.
 * NOTICE: this algorithm is bad for motion objects. Please use it in situation
 * that object doesn't move often.
 */
@interface CZVideoDenoiser : NSObject

// Default is CZDenoiseOptionMedianAndAverage
//@property (nonatomic, assign) CZDenoiseOption denoisOption;

/**
 * denoise 1 video frame. frame should input by sequence.
 * @param frame, image to denoise
 * @return denoised frame. If there are not enough images to denoise, it'll return nil;
 */
- (UIImage *)newDenoisedFrame:(UIImage *)frame;

@end

// TODO: move to new file
#pragma mark - class CZYUV420Denoiser

@interface CZYUV420Denoiser : NSObject

@property (nonatomic, assign) NSUInteger imageWidth;
@property (nonatomic, assign) NSUInteger imageHeight;

- (void)setYUVRowData:(const void *)rowData atIndex:(NSUInteger)index;

- (NSData *)denoisedRowData;

@end
