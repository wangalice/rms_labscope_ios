//
//  CZROISizePanelView.m
//  Matscope
//
//  Created by Sherry Xu on 7/17/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZROISizePanelView.h"
#import <CZToolbox/CZToolbox.h>

#define kDefaultPadding 10
#define kAutoScalingPanelKeyboardOffset 380
#define kNotAvailableTriangleLength @"n/a";

@interface CZROISizePanelView () <UITextFieldDelegate>
@property (nonatomic, retain) NSMutableArray *roiSizeLabels;
@property (nonatomic, retain) NSMutableArray *roiUnitLabels;
@property (nonatomic, retain) NSMutableArray *inputSizeTextFields;
@property (nonatomic, retain) NSMutableArray *roiSizeContents;
@property (nonatomic, assign) CZCroppingMode currentROIMode;
@property (nonatomic, retain) UIImageView *backgroundView;

@end

@implementation CZROISizePanelView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _inputSizeTextFields = [[NSMutableArray alloc] init];
        _roiSizeLabels = [[NSMutableArray alloc] init];
        _roiUnitLabels = [[NSMutableArray alloc] init];
        _roiSizeContents = [[NSMutableArray alloc] init];
        
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:A(@"camera-control-background")]];
        backgroundView.alpha = 0.8;
        backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        backgroundView.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:backgroundView];
        self.backgroundView = backgroundView;
        [backgroundView release];
    }
    return self;
}

- (void)dealloc {
    [_unit release];
    [_roiSizeLabels release];
    [_roiUnitLabels release];
    [_inputSizeTextFields release];
    [_roiSizeContents release];
    [_backgroundView release];
    
    [super dealloc];
}

- (void)updateWithCroppingMode:(CZCroppingMode)mode { // TODO:reuse UI controls instead of re-create
    for (UILabel *roiSizeLabel in self.roiSizeLabels) {
        [roiSizeLabel removeFromSuperview];
        [self.roiSizeLabels removeAllObjects];
    }
    for (UILabel *roiUnitLabel in self.roiUnitLabels) {
        [roiUnitLabel removeFromSuperview];
        [self.roiSizeLabels removeAllObjects];
    }
    for (UITextField *inputSizeTextField in self.inputSizeTextFields) {
        [inputSizeTextField removeFromSuperview];
        [self.inputSizeTextFields removeAllObjects];
    }
    
    CGFloat offset = 0.0f;
    CGFloat width = 0.0f;
    _currentROIMode = mode;
    if (mode == CZCroppingModeRectangle) {
        for (NSUInteger i = 0; i < 2; ++i) {
            NSArray *labelStrings = @[L(@"FEATURE_WIDTH"), L(@"FEATURE_HEIGHT")];
            UILabel *sizeLabel = [[UILabel alloc] init];
            sizeLabel.backgroundColor = [UIColor clearColor];
            sizeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
            sizeLabel.text = labelStrings[i];
#if CZ_IMAGE_PROCESSING_REFACTORED
            sizeLabel.textColor = kDefaultLabelColor;
#endif
            CGRect frame = [sizeLabel textRectForBounds:CGRectMake(kDefaultPadding + offset, 10, CGFLOAT_MAX, 30)
                                 limitedToNumberOfLines:1];
            frame.size.height = 30;
            sizeLabel.frame = frame;
            [self.roiSizeLabels addObject:sizeLabel];
            [self addSubview:sizeLabel];
            [sizeLabel release];
            
            // set the input textfields
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kDefaultPadding + CGRectGetMaxX(sizeLabel.frame), 10, 100, 30)];
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            textField.returnKeyType = UIReturnKeyDone;
            textField.delegate = self;
            [self.inputSizeTextFields addObject:textField];
            [self addSubview:textField];
            [textField release];
            
            UILabel *unitLabel = [[UILabel alloc] init];
            unitLabel.backgroundColor = [UIColor clearColor];
            unitLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
            unitLabel.text = self.unit;
#if CZ_IMAGE_PROCESSING_REFACTORED
            unitLabel.textColor = kDefaultLabelColor;
#endif
            CGRect unitLabelFrame = [unitLabel textRectForBounds:CGRectMake(10 + CGRectGetMaxX(textField.frame), 10, CGFLOAT_MAX, 30)
                                          limitedToNumberOfLines:1];
            unitLabelFrame.size.height = 30;
            unitLabel.frame = unitLabelFrame;
            [self.roiUnitLabels addObject:unitLabel];
            [self addSubview:unitLabel];
            [unitLabel release];
            
            offset += CGRectGetMaxX(unitLabel.frame) + kDefaultPadding * 5;
        }
        UILabel *label = [self.roiUnitLabels lastObject];
        width = CGRectGetMaxX(label.frame) + kDefaultPadding;
        self.frame = CGRectMake((self.superview.frame.size.width - width) * 0.5, 600, width, 50);
    } else if (mode == CZCroppingModeCircle) {
        UILabel *sizeLabel = [[UILabel alloc] init];
        sizeLabel.backgroundColor = [UIColor clearColor];
        sizeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        sizeLabel.text = L(@"FEATURE_DIAMETER");
#if CZ_IMAGE_PROCESSING_REFACTORED
        sizeLabel.textColor = kDefaultLabelColor;
#endif
        CGRect frame = [sizeLabel textRectForBounds:CGRectMake(kDefaultPadding + offset, 10, CGFLOAT_MAX, 30)
                             limitedToNumberOfLines:1];
        frame.size.height = 30;
        sizeLabel.frame = frame;
        [self.roiSizeLabels addObject:sizeLabel];
        [self addSubview:sizeLabel];
        [sizeLabel release];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kDefaultPadding + CGRectGetMaxX(sizeLabel.frame), 10, 100, 30)];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.returnKeyType = UIReturnKeyDone;
        textField.delegate = self;
        [self.inputSizeTextFields addObject:textField];
        [self addSubview:textField];
        [textField release];
        
        UILabel *unitLabel = [[UILabel alloc] init];
        unitLabel.backgroundColor = [UIColor clearColor];
        unitLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        unitLabel.text = self.unit;
#if CZ_IMAGE_PROCESSING_REFACTORED
        unitLabel.textColor = kDefaultLabelColor;
#endif
        CGRect unitLabelFrame = [unitLabel textRectForBounds:CGRectMake(10 + CGRectGetMaxX(textField.frame), 10, CGFLOAT_MAX, 30)
                                      limitedToNumberOfLines:1];
        unitLabelFrame.size.height = 30;
        unitLabel.frame = unitLabelFrame;
        [self.roiUnitLabels addObject:unitLabel];
        [self addSubview:unitLabel];
        [unitLabel release];
        
        width = CGRectGetMaxX(unitLabel.frame) + kDefaultPadding;
        self.frame = CGRectMake((self.superview.frame.size.width - width) * 0.5, 600, width, 50);
    } else if (mode == CZCroppingModeTriangle) {
        UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultPadding + offset, 10, 80, 30)];
        sizeLabel.backgroundColor = [UIColor clearColor];
        sizeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        sizeLabel.text = L(@"ROI_TRIANGLE_SIDE_LENGTH");
#if CZ_IMAGE_PROCESSING_REFACTORED
        sizeLabel.textColor = kDefaultLabelColor;
#endif
        CGRect frame = [sizeLabel textRectForBounds:CGRectMake(kDefaultPadding + offset, 10, CGFLOAT_MAX, 30)
                             limitedToNumberOfLines:1];
        frame.size.height = 30;
        sizeLabel.frame = frame;
        [self.roiSizeLabels addObject:sizeLabel];
        [self addSubview:sizeLabel];
        [sizeLabel release];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(kDefaultPadding + CGRectGetMaxX(sizeLabel.frame), 10, 100, 30)];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.returnKeyType = UIReturnKeyDone;
        textField.delegate = self;
        [self.inputSizeTextFields addObject:textField];
        [self addSubview:textField];
        [textField release];
        
        UILabel *unitLabel = [[UILabel alloc] init];
        unitLabel.backgroundColor = [UIColor clearColor];
        unitLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
        unitLabel.text = self.unit;
#if CZ_IMAGE_PROCESSING_REFACTORED
        unitLabel.textColor = kDefaultLabelColor;
#endif
        CGRect unitLabelFrame = [unitLabel textRectForBounds:CGRectMake(10 + CGRectGetMaxX(textField.frame), 10, CGFLOAT_MAX, 30)
                                      limitedToNumberOfLines:1];
        unitLabelFrame.size.height = 30;
        unitLabel.frame = unitLabelFrame;
        [self.roiUnitLabels addObject:unitLabel];
        [self addSubview:unitLabel];
        [unitLabel release];
        
        width = CGRectGetMaxX(unitLabel.frame) + kDefaultPadding;
        self.frame = CGRectMake((self.superview.frame.size.width - width) * 0.5, 600, width, 50);
    }
    self.backgroundView.frame = self.bounds;
}

- (void)updateTextFieldsWithContents:(NSArray *)contents {
    UITextField *inputSizeTextField;
    self.roiSizeContents = [[contents mutableCopy] autorelease];
    for (NSUInteger i = 0; i < self.inputSizeTextFields.count; i++) {
        inputSizeTextField = self.inputSizeTextFields[i];
        if (self.currentROIMode == CZCroppingModeTriangle) {
            if ([contents[0] isEqualToString:contents[1]] && [contents[1] isEqualToString:contents[2]]) {
                inputSizeTextField.text = contents[0];
            } else {
                inputSizeTextField.text = kNotAvailableTriangleLength;
            }
        } else {
           inputSizeTextField.text = contents[i];
        }
    }
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITextField *inputSizeTextField;
    for (NSUInteger i = 0; i < self.inputSizeTextFields.count; i++) {
        inputSizeTextField = self.inputSizeTextFields[i];
        if (textField == inputSizeTextField) {
            CGRect frame = self.frame;
            frame.origin.y -= kAutoScalingPanelKeyboardOffset;
            self.frame = frame;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)textEntered {
    return [CZCommonUtils numericTextField:textField shouldAcceptChange:textEntered];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UITextField *inputSizeTextField;
    NSMutableArray *roiSizeContents = self.roiSizeContents;
    for (NSUInteger i = 0; i < self.inputSizeTextFields.count; i++) {
        inputSizeTextField = self.inputSizeTextFields[i];
        if (textField == inputSizeTextField) {
            CGRect frame = self.frame;
            frame.origin.y += kAutoScalingPanelKeyboardOffset;
            self.frame = frame;
            
            NSNumber *inputNumber = [CZCommonUtils numberFromLocalizedString:inputSizeTextField.text];
            if (!inputNumber) {
                if (self.currentROIMode == CZCroppingModeTriangle) {
                    if ([roiSizeContents[0] isEqualToString:roiSizeContents[1]] && [roiSizeContents[1] isEqualToString:roiSizeContents[2]]) {
                        inputSizeTextField.text = roiSizeContents[0];
                    } else {
                        inputSizeTextField.text = kNotAvailableTriangleLength;
                    }
                } else {
                    inputSizeTextField.text = roiSizeContents[i];
                }
                return;
            }
            float inputValue = [inputNumber floatValue];
            float validValue = [CZCommonUtils floatFromFloat:inputValue precision:self.precision];
            if ((validValue < self.inputMinValue) || (validValue > self.inputMaxValue)) {
                validValue = [[CZCommonUtils numberFromLocalizedString:roiSizeContents[i]] floatValue];
            }
            NSString *validText = [CZCommonUtils localizedStringFromFloat:validValue precision:self.precision];
            inputSizeTextField.text = validText;
            if (self.currentROIMode == CZCroppingModeTriangle) {
                for (NSUInteger j = 0; j < self.roiSizeContents.count; j++) {
                    [roiSizeContents replaceObjectAtIndex:j withObject:validText];
                }
            } else {
                [roiSizeContents replaceObjectAtIndex:i withObject:validText];
            }
        }
    }
    self.roiSizeContents = roiSizeContents;
    NSArray *sizeContents = [[self.roiSizeContents copy] autorelease];
    if ([self.delegate respondsToSelector:@selector(roiSizePanelView:didUpdateROIRegionWithSize:)]) {
        [self.delegate roiSizePanelView:self didUpdateROIRegionWithSize:sizeContents];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    UITextField *inputSizeTextField;
    for (NSUInteger i = 0; i < self.inputSizeTextFields.count; i++) {
        inputSizeTextField = self.inputSizeTextFields[i];
        if (textField == inputSizeTextField) {
            [textField resignFirstResponder];
        }
    }
    return YES;
}

@end
