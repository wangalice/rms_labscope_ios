//
//  CZVideoDenoiser.mm
//  Matscope
//
//  Created by Ralph Jin on 7/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZVideoDenoiser.h"
#import <GPUImage/GPUImage.h>
#import "CZThreeImageAverageColor.h"
#include <algorithm>
#include <functional>

#if 0
template<size_t ComponentCount, typename ComponentType>
void denoise3ImagesToDstImage(size_t width, size_t height,
                              const ComponentType *imageData[3],
                              ComponentType *dstImage,
                              bool validComponent[ComponentCount]) {
    if (width < 3 || height < 3) {
        return;
    }
    
    const size_t kPixelCount = 3 * 3 * 3; // cubic of 3
    const size_t kPixelBytes = sizeof(ComponentType) * ComponentCount;
    const size_t kMedianIndex = kPixelCount / 2;
    
    ComponentType pixelsData[kPixelCount];
    ComponentType *first = pixelsData;
    ComponentType *median = first + kMedianIndex;
    ComponentType *last = first + kPixelCount;
    
    const size_t kRowBytes = ComponentCount * sizeof(ComponentType) * width;
    size_t offset0 = 0;  // 1st row
    size_t offset1 = kRowBytes;  // 2nd row
    size_t offset2 = offset1 + kRowBytes; // 3rd row
    
    // copy first row
    memcpy(dstImage, imageData[0], kRowBytes);
    
    for (size_t y = 1; y < height - 1; ++y) {
        // first pixel of row
        memcpy(dstImage + offset1, imageData[0] + offset1, ComponentCount);
        
        for (size_t x = 1; x < width - 1; x ++) {
            for (size_t i = 0; i < ComponentCount; i++) {
                if (validComponent[i]) {
                    for (size_t k = 0, j = 0; j < 3; j++) {
                        pixelsData[k++] = imageData[j][offset0];
                        pixelsData[k++] = imageData[j][offset0 + kPixelBytes];
                        pixelsData[k++] = imageData[j][offset0 + kPixelBytes + kPixelBytes];
                    
                        pixelsData[k++] = imageData[j][offset1];
                        pixelsData[k++] = imageData[j][offset1 + kPixelBytes];
                        pixelsData[k++] = imageData[j][offset1 + kPixelBytes + kPixelBytes];
                    
                        pixelsData[k++] = imageData[j][offset2];
                        pixelsData[k++] = imageData[j][offset2 + kPixelBytes];
                        pixelsData[k++] = imageData[j][offset2 + kPixelBytes + kPixelBytes];
                    }
                
                    std::nth_element(first, median, last);
            
                    // assign to center pixel
                    dstImage[offset1 + kPixelBytes] = *median;
                } else {
                    dstImage[offset1 + kPixelBytes] = imageData[0][offset1 + kPixelBytes];
                }
                
                offset0++;
                offset1++;
                offset2++;
            }
        }
        
        // last pixel of the row
        offset1 += ComponentCount;
        for (size_t i = 0; i < ComponentCount; i++) {
            dstImage[offset1 + i] = imageData[0][offset1 + i];
        }
        
        // shift to next row
        offset0 = y * kRowBytes;
        offset1 = offset0 + kRowBytes;
        offset2 = offset1 + kRowBytes;
    }
    
    size_t lastRow = kRowBytes * (height - 1);
    memcpy(&dstImage[lastRow], &imageData[0][lastRow], kRowBytes);
}

#endif  // 0

@interface CZVideoDenoiser()
@property (nonatomic, retain) NSMutableArray *frameBuffer;
@end

@implementation CZVideoDenoiser

- (instancetype)init {
    self = [super init];
    if (self) {
        _frameBuffer = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_frameBuffer release];
    [super dealloc];
}

- (UIImage *)newDenoisedFrame:(UIImage *)frame {
    if (frame == nil) {
        return nil;
    }
    
    const int width = frame.size.width;
    const int height = frame.size.height;
    if (height == 0 || width == 0) {
        return nil;
    }
    
    while (_frameBuffer.count >= 3) {
        [_frameBuffer removeLastObject];
    }
    
    [_frameBuffer insertObject:frame atIndex:0];
    
    if (_frameBuffer.count >= 3) {
        // do denoise
        UIImage *newFrame = [self newDenoisedFrameWithAvgFilter];
//        switch (self.denoisOption) {
//            case CZDenoiseOptionMedianAndAverage:
//                newFrame = [self newDenoisedFrameWithAvgBlurFilter:NO];
//                break;
//            case CZDenoiseOption3DMedian:
//                newFrame = [self newDenoisedFrameWithMediamBlurFilter];
//                break;
//            case CZDenoiseOptionMedianAndSharp:
//                newFrame = [self newDenoisedFrameWithAvgBlurFilter:YES];
//                break;
//            case CZDenoiseOptionAverage:
//                newFrame = [self newDenoisedFrameWithAvgFilter];
//                break;
//        }
        
        // free memory
        [_frameBuffer removeAllObjects];
        
        return newFrame;
    } else {
        return nil;
    }
}

#pragma mark - private methods

- (UIImage *)newDenoisedFrameWithAvgFilter {
    NSAssert(_frameBuffer.count >= 3, @"not enough frames for blending");
    
    UIImage *frame = _frameBuffer[0];
    int width = frame.size.width;
    int height = frame.size.height;
    
    GPUImagePicture *sourcePicture0 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    GPUImagePicture *sourcePicture1 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    GPUImagePicture *sourcePicture2 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    
    CZThreeImageAverageColor *averageColor = [[CZThreeImageAverageColor alloc] init];
    [averageColor forceProcessingAtSize:CGSizeMake(width, height)];
    
    [sourcePicture0 addTarget:averageColor];
    [sourcePicture1 addTarget:averageColor];
    [sourcePicture2 addTarget:averageColor];
    GPUImageFilter *lastFilter = averageColor;

    [lastFilter useNextFrameForImageCapture];
    
    [sourcePicture0 processImage];
    [sourcePicture1 processImage];
    [sourcePicture2 processImage];
    
    UIImage *filteredImage = nil;
    
    @autoreleasepool {
        filteredImage = [lastFilter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
        [filteredImage retain];
    }
    
    [sourcePicture0 release];
    [sourcePicture1 release];
    [sourcePicture2 release];
    [averageColor release];
    
    return filteredImage;
}

#if 0

- (UIImage *)newDenoisedFrameWithAvgBlurFilter:(BOOL)sharpen {
    NSAssert(_frameBuffer.count >= 3, @"not enough frames for blending");
    
    UIImage *frame = _frameBuffer[0];
    int width = frame.size.width;
    int height = frame.size.height;
    
    GPUImagePicture *sourcePicture0 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    GPUImagePicture *sourcePicture1 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    GPUImagePicture *sourcePicture2 = [[GPUImagePicture alloc] initWithImage:[_frameBuffer lastObject] smoothlyScaleOutput:NO];
    [self.frameBuffer removeLastObject];
    
    GPUImageMedianFilter *filter0 = [[GPUImageMedianFilter alloc] init];
    [filter0 forceProcessingAtSize:CGSizeMake(width, height)];
    [sourcePicture0 addTarget:filter0];
    
    GPUImageMedianFilter *filter1 = [[GPUImageMedianFilter alloc] init];
    [filter1 forceProcessingAtSize:CGSizeMake(width, height)];
    [sourcePicture1 addTarget:filter1];
    
    GPUImageMedianFilter *filter2 = [[GPUImageMedianFilter alloc] init];
    [filter2 forceProcessingAtSize:CGSizeMake(width, height)];
    [sourcePicture0 addTarget:filter2];
    
    CZThreeImageAverageColor *averageColor = [[CZThreeImageAverageColor alloc] init];
    [averageColor forceProcessingAtSize:CGSizeMake(width, height)];
    
    [filter0 addTarget:averageColor];
    [filter1 addTarget:averageColor];
    [filter2 addTarget:averageColor];
    GPUImageFilter *lastFilter = averageColor;
    
    GPUImageSharpenFilter *sharpenFilter = nil;
    if (sharpen) {
        sharpenFilter = [[GPUImageSharpenFilter alloc] init];
        CGFloat sharpness = 4.0f * 15.0 / 100.0f;
        sharpenFilter.sharpness = sharpness;
        [averageColor addTarget:sharpenFilter];
        lastFilter = sharpenFilter;
    }
    
    [lastFilter useNextFrameForImageCapture];
    
    [sourcePicture0 processImage];
    [sourcePicture1 processImage];
    [sourcePicture2 processImage];
    
    UIImage *filteredImage = nil;
    
    @autoreleasepool {
        filteredImage = [lastFilter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
        [filteredImage retain];
    }
    
    [filter0 release];
    [filter1 release];
    [filter2 release];
    [sourcePicture0 release];
    [sourcePicture1 release];
    [sourcePicture2 release];
    [averageColor release];
    [sharpenFilter release];
    
    return filteredImage;
}

- (UIImage *)newDenoisedFrameWithMediamBlurFilter {
    NSAssert(_frameBuffer.count >= 3, @"not enough frames for blending");

    UIImage *frame = [_frameBuffer lastObject];
    int width = frame.size.width;
    int height = frame.size.height;
    
    uint8_t *imageData[3] = { NULL };
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    for (int i = 0; i < 3; i++) {
        imageData[i] = (uint8_t *)malloc(width * height * 4);
        CGContextRef context = CGBitmapContextCreate(imageData[i],
                                                     width,
                                                     height,
                                                     8,
                                                     width * 4,
                                                     colorSpace,
                                                     kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault);
        CGContextSetInterpolationQuality(context, kCGInterpolationNone);
        CGContextSetShouldAntialias(context, NO);
        CGContextSetAllowsAntialiasing(context, NO);
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), ((UIImage *)[_frameBuffer lastObject]).CGImage);
        CGContextRelease(context);
        
        [_frameBuffer removeLastObject];
    }
    
    // create denoised image
    uint8_t *dstImageData = (uint8_t *)malloc(width * height * 4);
    
    const uint8_t *srcImageData[3] = {imageData[0], imageData[1], imageData[2]};
    bool rgbaMask[4] = {true, true, true, false};
    denoise3ImagesToDstImage<4>(width, height, srcImageData, dstImageData, rgbaMask);

    for (int i = 0; i < 3; i++) {
        free(imageData[i]);
    }
    
    CGContextRef context = CGBitmapContextCreate(dstImageData,
                                                 width,
                                                 height,
                                                 8,
                                                 width * 4,
                                                 colorSpace,
                                                 kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    free(dstImageData);
    
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return image;
}

#endif  // 0

@end

#if 0

// TODO: move to new file
#pragma mark - class CZYUV420Denoiser

@implementation CZYUV420Denoiser {
    const uint8_t *_imagesRowData[3];
}

- (void)setYUVRowData:(const void *)rowData atIndex:(NSUInteger)index {
    if (index < 3) {
        _imagesRowData[index] = (const uint8_t *)rowData;
    }
}

- (NSData *)denoisedRowData {
    for (int i = 0; i < 3; i++) {
        if (_imagesRowData[i] == nullptr) {
            return nil;
        }
    }
    
    const NSUInteger yDataLength = self.imageWidth * self.imageHeight;
    const NSUInteger uvDataLength = yDataLength / 2;
    const NSUInteger dataLength = yDataLength + uvDataLength;
    uint8_t *dstRowData = (uint8_t *)malloc(dataLength);
    bool yMask[1] = {true};
    
    denoise3ImagesToDstImage<1>(_imageWidth, _imageHeight, _imagesRowData, dstRowData, yMask);
    
    const uint8_t *srcUVData[3] = {
        _imagesRowData[0] + yDataLength,
        _imagesRowData[1] + yDataLength,
        _imagesRowData[2] + yDataLength
    };
    
    uint8_t *dstUVData = dstRowData + yDataLength;
    
    bool uvMask[2] = {true, true};
    denoise3ImagesToDstImage<2>(_imageWidth/2, _imageHeight/2, srcUVData, dstUVData, uvMask);
    
    return [NSData dataWithBytesNoCopy:dstRowData length:dataLength freeWhenDone:YES];
}

@end

#endif // 0
