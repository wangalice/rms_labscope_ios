//
//  CZThreeImageAverageColor.m
//  Matscope
//
//  Created by Ralph Jin on 7/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZThreeImageAverageColor.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kCZThreeImageAverageColorFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 varying highp vec2 textureCoordinate2;
 varying highp vec2 textureCoordinate3;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 uniform sampler2D inputImageTexture3;
 
 void main()
 {
     lowp vec3 color1 = texture2D(inputImageTexture, textureCoordinate).rgb;
     lowp vec3 color2 = texture2D(inputImageTexture2, textureCoordinate2).rgb;
     lowp vec3 color3 = texture2D(inputImageTexture3, textureCoordinate3).rgb;
     
     lowp float k1_3 = 0.3333333333333;
     lowp vec3 color = (color1 * k1_3 + color2 * k1_3 + color3 * k1_3);
     
     gl_FragColor = vec4(color.rgb, 1.0);
 }
 );
#else
NSString *const kCZThreeImageAverageColorFragmentShaderString = SHADER_STRING
(
 varying vec2 textureCoordinate;
 varying vec2 textureCoordinate2;
 varying vec2 textureCoordinate3;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 uniform sampler2D inputImageTexture3;
 
 void main()
 {
     vec3 color1 = texture2D(inputImageTexture, textureCoordinate).rgb;
     vec3 color2 = texture2D(inputImageTexture2, textureCoordinate2).rgb;
     vec3 color3 = texture2D(inputImageTexture3, textureCoordinate3).rgb;
     
     float k1_3 = 0.3333333333333;
     vec3 color = (color1 * k1_3 + color2 * k1_3 + color3 * k1_3);
     
     gl_FragColor = vec4(color.rgb, 1.0);
 }
 );
#endif


@implementation CZThreeImageAverageColor

- (id)init {
    return [super initWithFragmentShaderFromString:kCZThreeImageAverageColorFragmentShaderString];
}

@end
