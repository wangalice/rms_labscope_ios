//
//  CZLookupTwoInputFilter.h
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageTwoInputFilter.h>

/** filter that mapping gray image (0 ~ 255) to new color.
 * The second input image is a 16*16 color lookup table.
 */

typedef NS_ENUM(NSUInteger, CZLookupClipType) {
    CZLookupClipTypeNone,
    CZLookupClipTypeRectangle,
    CZLookupClipTypeEllipse,
    CZLookupClipTypeTriangle
};

@interface CZLookupTwoInputFilter : GPUImageTwoInputFilter

@property (nonatomic, readonly) CZLookupClipType clipType;

- (id)initWithClipType:(CZLookupClipType)clipType;

/** set clip rectangle and extra transform, like rotating. */
- (void)setClipRect:(CGRect)clipRect transform:(CGAffineTransform)transform;

- (void)setTrianglePoint1:(CGPoint)point1 point2:(CGPoint)point2 point3:(CGPoint)point3;

- (void)setEllipseCenter:(CGPoint)center radius:(CGSize)radius;

@end
