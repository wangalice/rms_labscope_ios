//
//  CZChangeMultiphaseAction.m
//  Matscope
//
//  Created by Halley Gu on 2/25/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZChangeMultiphaseAction.h"
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZChangeMultiphaseAction ()

@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, copy) CZMultiphase *multiphase;
@property (nonatomic, copy) CZMultiphase *originalMultiphase;

@end

@implementation CZChangeMultiphaseAction

- (id)initWithDocManager:(CZDocManager *)docManager
              multiphase:(CZMultiphase *)multiphase
      originalMultiphase:(CZMultiphase *)originalMultiphase {
    self = [super init];
    if (self) {
        _docManager = docManager;
        _multiphase = [multiphase copy];
        _originalMultiphase = [originalMultiphase copy];
    }
    return self;
}

- (void)dealloc {
    [_multiphase release];
    [_originalMultiphase release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark CZUndoAction Methods

- (NSUInteger)costMemory {
    return 1024U; // Rough estimation
}

- (void)do {
    [self redo];
}

- (void)undo {
    CZMultiphase *multiphase = [self.originalMultiphase copy];
    
    self.docManager.analyzer2dData = nil;
    self.docManager.analyzer2dResult = nil;
    [self.docManager setMultiphase:multiphase];
    [multiphase release];
}

- (void)redo {
    CZMultiphase *multiphase = [self.multiphase copy];
    
    self.docManager.analyzer2dData = nil;
    self.docManager.analyzer2dResult = nil;
    [self.docManager setMultiphase:multiphase];
    [multiphase release];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeMultiphase;
}

@end
