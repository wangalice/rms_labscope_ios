//
//  CZGemChords.h
//  Matscope
//
//  Created by Mike Wang on 2/28/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#ifdef __cplusplus
namespace GemMeasurerClr {
    class GemChords;
}

#endif

@interface CZGemChords : NSObject

- (id)initWithRectSize:(CGSize)size;
- (void)addPoint:(CGPoint)point;
- (void)addChordWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;
- (unsigned char)getPoint:(CGPoint)point;
- (NSUInteger)getChordCount:(NSUInteger)y;
- (CGSize)getSize;
- (void)getMinX:(long *)minX maxX:(long *)maxX minY:(long *)minY maxY:(long *)maxY;
- (void)clear;

#ifdef __cplusplus
- (GemMeasurerClr::GemChords *)gemChords;
#endif

@end
