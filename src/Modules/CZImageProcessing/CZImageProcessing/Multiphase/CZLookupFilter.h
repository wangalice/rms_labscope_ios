//
//  CZLookupFilter.h
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilterGroup.h>
#import "CZLookupTwoInputFilter.h"

/** filter that mapping gray image (0 ~ 255) to new ARGB color*/
@interface CZLookupFilter : GPUImageFilterGroup

@property (nonatomic, assign, readonly) CZLookupTwoInputFilter *lookupTwoInputFilter;

/** initialize filter with color table
 * @param colorTable, ARGB index color table, from 0~255; so the sizeof(colorTable) shall be 256*4 bytes .
 */
- (id)initWithColorTable:(NSData *)colorTable clipType:(CZLookupClipType)clipType;

/** set with new color table
 * @param colorTable, ARGB index color table, from 0~255; so the sizeof(colorTable) shall be 256*4 bytes .
 */
- (void)setColorTable:(NSData *)colorTable;


@end
