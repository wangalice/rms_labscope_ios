//
//  CZLookupTwoInputFilter.m
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLookupTwoInputFilter.h"
#import <CZToolbox/CZToolbox.h>

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

static NSString *const kCZLookupFragmentShaderStringFormat = SHADER_STRING (
 precision highp float;
 varying highp vec2 textureCoordinate;
 varying highp vec2 textureCoordinate2; // TODO: This is not used

 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2; // lookup texture

 %@

 void main() {
   vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
   if (textureColor.a == 0.0) {
     gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
     return;
   }

   // clip
   %@

   // look up
   highp float gray = floor(textureColor.r * 255.0 + 0.5);

   highp vec2 texPos2;
   texPos2.x = mod(gray, 16.0) / 16.0 + 0.5 / 16.0;
   texPos2.y = floor(gray / 16.0) / 16.0 + 0.5 / 16.0;

   vec4 newColor = texture2D(inputImageTexture2, texPos2);
   newColor = mix(textureColor, newColor, newColor.a);

   gl_FragColor = vec4(newColor.rgb, 1.0);
 }
);

static NSString *const kCZLookupRectangleClipUniformsString = SHADER_STRING (
    uniform vec2 topLeft;
    uniform vec2 bottomRight;
    uniform mat3 rotateMatrix;
);

static NSString *const kCZLookupRectangleClipProgramString = SHADER_STRING (
    vec2 xy;
    xy.x = textureCoordinate.x * rotateMatrix[0][0] + textureCoordinate.y * rotateMatrix[0][1] + rotateMatrix[0][2];
    xy.y = textureCoordinate.x * rotateMatrix[1][0] + textureCoordinate.y * rotateMatrix[1][1] + rotateMatrix[1][2];
    vec2 d = (xy - topLeft) * (bottomRight - xy);
    if (d.x < 0.0 || d.y < 0.0) {
        gl_FragColor = textureColor;
        return;
    }
);

static NSString *const kCZLookupEllipseClipUniformsString = SHADER_STRING (
 uniform vec2 origin;
 uniform vec2 radius2;  // (rx * rx, ry * ry)
);

static NSString *const kCZLookupEllipseClipProgramString = SHADER_STRING (
    float distance = (textureCoordinate.x - origin.x) * (textureCoordinate.x - origin.x) / radius2.x
        + (textureCoordinate.y - origin.y) * (textureCoordinate.y - origin.y) / radius2.y;
     
    if (distance > 1.0) {
        gl_FragColor = textureColor;
        return;
    }
);

static NSString *const kCZLookupTriangleClipUniformsString = SHADER_STRING (
    uniform vec2 origin;
    uniform vec2 v0;
    uniform vec2 v1;
);

// use Barycentric Technique to judge if point in triangle
static NSString *const kCZLookupTriangleClipProgramString = SHADER_STRING (
    vec2 v2 = textureCoordinate - origin;
     
    float dot00 = dot(v0, v0);
    float dot01 = dot(v0, v1);
    float dot02 = dot(v0, v2);
    float dot11 = dot(v1, v1);
    float dot12 = dot(v1, v2);
     
    float invertDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * invertDenom;
    float v = (dot00 * dot12 - dot01 * dot02) * invertDenom;
     
    if (u < 0.0 || v < 0.0 || (u + v) > 1.0) {
      gl_FragColor = textureColor;
      return;
    }
);
#else
  #error "not implemented yet"
#endif

static inline CGFloat dot(CGPoint p1, CGPoint p2) {
    return p1.x * p2.x + p1.y * p2.y;
}

@interface CZLookupTwoInputFilter () {
    // rectangle clip group
    GLint _topLeftUniform;
    GLint _bottomRightUniform;
    GLint _rotateMatrixUniform;
    CGRect _clipRect;
    CGFloat _rotateAngle;
    
    // triangle clip group
    GLint _vector0Uniform;
    GLint _vector1Uniform;
    CGPoint _point1;
    CGPoint _point2;
    CGPoint _point3;
    
    // ellipse clip group
    GLint _originUniform;
    GLint _radiusUniform;
    CGPoint _center;
    CGSize _radius;
}

@end

@implementation CZLookupTwoInputFilter

- (id)initWithClipType:(CZLookupClipType)clipType {
    _clipType = clipType;
    
    if (clipType == CZLookupClipTypeNone) {
        NSString *shaderString = [NSString stringWithFormat:kCZLookupFragmentShaderStringFormat, @"", @""];
        self = [super initWithFragmentShaderFromString:shaderString];
    } else if (clipType == CZLookupClipTypeRectangle) {
        NSString *shaderString = [NSString stringWithFormat:kCZLookupFragmentShaderStringFormat,
                                  kCZLookupRectangleClipUniformsString,
                                  kCZLookupRectangleClipProgramString];
        self = [super initWithFragmentShaderFromString:shaderString];
        if (self) {
            _topLeftUniform = [filterProgram uniformIndex:@"topLeft"];
            _bottomRightUniform = [filterProgram uniformIndex:@"bottomRight"];
            _rotateMatrixUniform = [filterProgram uniformIndex:@"rotateMatrix"];
            
            CGRect clipRect = CGRectMake(0, 0, 1, 1);
            [self setClipRect:clipRect transform:CGAffineTransformIdentity];
        }
    } else if (clipType == CZLookupClipTypeEllipse) {
        NSString *shaderString = [NSString stringWithFormat:kCZLookupFragmentShaderStringFormat,
                                  kCZLookupEllipseClipUniformsString,
                                  kCZLookupEllipseClipProgramString];
        self = [super initWithFragmentShaderFromString:shaderString];
        if (self) {
            _originUniform = [filterProgram uniformIndex:@"origin"];
            _radiusUniform = [filterProgram uniformIndex:@"radius2"];
            
            [self setEllipseCenter:CGPointMake(0.5, 0.5) radius:CGSizeMake(1, 1)];
        }
    } else if (clipType == CZLookupClipTypeTriangle) {
        NSString *shaderString = [NSString stringWithFormat:kCZLookupFragmentShaderStringFormat,
                                  kCZLookupTriangleClipUniformsString,
                                  kCZLookupTriangleClipProgramString];
        self = [super initWithFragmentShaderFromString:shaderString];
        if (self) {
            _originUniform = [filterProgram uniformIndex:@"origin"];
            _vector0Uniform = [filterProgram uniformIndex:@"v0"];
            _vector1Uniform = [filterProgram uniformIndex:@"v1"];
            
            [self setTrianglePoint1:CGPointMake(-1, 1) point2:CGPointMake(1, 1) point3:CGPointMake(1, -1)];
        }
    } else {
        [self release];
        self = nil;
    }
    return self;
}

- (void)setClipRect:(CGRect)clipRect transform:(CGAffineTransform)transform {
    if (_clipType != CZLookupClipTypeRectangle) {
        return;
    }
    
    _clipRect = clipRect;
    
    CGPoint pt;
    pt.x = CGRectGetMinX(_clipRect);
    pt.y = CGRectGetMinY(_clipRect);
    [self setPoint:pt forUniform:_topLeftUniform program:filterProgram];
    
    pt.x = CGRectGetMaxX(_clipRect);
    pt.y = CGRectGetMaxY(_clipRect);
    [self setPoint:pt forUniform:_bottomRightUniform program:filterProgram];

    CGAffineTransform t = CGAffineTransformInvert(transform);
    GPUMatrix3x3 mat3;
    mat3.one.one = t.a;
    mat3.one.two = t.c;
    mat3.one.three = t.tx;
    mat3.two.one = t.b;
    mat3.two.two = t.d;
    mat3.two.three = t.ty;
    mat3.three.one = 0;
    mat3.three.two = 0;
    mat3.three.three = 1.0;
    [self setMatrix3f:mat3 forUniform:_rotateMatrixUniform program:filterProgram];
}

- (void)setTrianglePoint1:(CGPoint)point1 point2:(CGPoint)point2 point3:(CGPoint)point3 {
    if (_clipType != CZLookupClipTypeTriangle) {
        return;
    }
    
    _point1 = point1;
    _point2 = point2;
    _point3 = point3;
    
    CGPoint v0, v1;
    v0.x = _point3.x - _point1.x;
    v0.y = _point3.y - _point1.y;
    
    v1.x = _point2.x - _point1.x;
    v1.y = _point2.y - _point1.y;
    
    float dot00 = dot(v0, v0);
    float dot01 = dot(v0, v1);
    float dot11 = dot(v1, v1);
    
    CGFloat denom = dot00 * dot11 - dot01 * dot01;
    denom = fabs(denom);
    
    if (denom > 1e-5) {
        [self setPoint:_point1 forUniform:_originUniform program:filterProgram];
        [self setPoint:v0 forUniform:_vector0Uniform program:filterProgram];
        [self setPoint:v1 forUniform:_vector1Uniform program:filterProgram];
    } else {
        CZLogv(@"set invalid triangle to CZLookupTwoInputFilter.");
        
        [self setTrianglePoint1:CGPointMake(0, 0) point2:CGPointMake(0, 1) point3:CGPointMake(-1, 1)];  // set triangle out of image
    }
}

- (void)setEllipseCenter:(CGPoint)center radius:(CGSize)radius {
    if (_clipType != CZLookupClipTypeEllipse) {
        return;
    }
    
    _center = center;
    _radius = radius;
    
    [self setPoint:_center forUniform:_originUniform program:filterProgram];
    
    CGSize radius2;
    radius2.width = radius.width * radius.width;
    radius2.height = radius.height * radius.height;
    
    [self setSize:radius2 forUniform:_radiusUniform program:filterProgram];
}

@end
