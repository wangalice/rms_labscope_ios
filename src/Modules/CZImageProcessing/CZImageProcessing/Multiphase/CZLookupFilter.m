//
//  CZLookupFilter.m
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLookupFilter.h"
#import <GPUImage/GPUImagePicture.h>

@interface CZLookupFilter () {
    GPUImagePicture *lookupImageSource;
}

@end

@implementation CZLookupFilter

- (id)initWithColorTable:(NSData *)colorTable clipType:(CZLookupClipType)clipType {
    if (!(self = [super init])) {
        return nil;
    }

    CZLookupTwoInputFilter *lookupFilter = [[CZLookupTwoInputFilter alloc] initWithClipType:clipType];
    [self addFilter:lookupFilter];
    
    self.initialFilters = [NSArray arrayWithObjects:lookupFilter, nil];
    self.terminalFilter = lookupFilter;
    [lookupFilter release];
    
    [self setColorTable:colorTable];
    
    return self;
}

- (void)dealloc {
    [lookupImageSource release];
    [super dealloc];
}

- (CZLookupTwoInputFilter *)lookupTwoInputFilter {
    return (CZLookupTwoInputFilter *)self.terminalFilter;
}

- (void)setColorTable:(NSData *)colorTable {
    if (lookupImageSource) {
        [lookupImageSource removeAllTargets];
        [lookupImageSource release];
        lookupImageSource = nil;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)colorTable);
    
    CGImageRef imageRef = CGImageCreate(16,
                                        16,
                                        8,
                                        32,
                                        16 * 4,
                                        colorSpace,
                                        kCGImageAlphaPremultipliedFirst| kCGBitmapByteOrderDefault,
                                        dataProvider,
                                        NULL,
                                        false,
                                        kCGRenderingIntentDefault);
    
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    
    @autoreleasepool {
        lookupImageSource = [[GPUImagePicture alloc] initWithCGImage:imageRef];
    }
    CGImageRelease(imageRef);
    
    GPUImageOutput<GPUImageInput> *lookupFilter = self.terminalFilter;
    [lookupImageSource addTarget:lookupFilter atTextureLocation:1];
    [lookupImageSource processImage];
}

@end
