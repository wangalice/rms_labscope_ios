//
//  UIImage+MultiphaseImage.mm
//  Matscope
//
//  Created by Mike Wang on 2/17/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "UIImage+MultiphaseImage.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <GPUImage/GPUImage.h>
#include <stack>

#import <CZDocumentKit/CZDocumentKit.h>
#import "CZLookupFilter.h"

struct ScanLine {
    int xs;  // start x
    int xe;  // end x
    int y;   // y
    int dy;  // -1 or +1
};

struct PhaseInfoCache {
    NSUInteger minThreshold, maxThreshold;
    NSUInteger minimumArea;
    NSUInteger areaDeducted;
    uint8_t r, g, b, a;
};

@implementation UIImage (MultiphaseImage)

static inline bool compareLuminanceWithPhaseThreshold(uint8_t luminance, uint8_t alpha, const PhaseInfoCache *phase) {
    if (alpha == 0) {
        return false;
    }
    if (luminance >= phase->minThreshold &&
        luminance < phase->maxThreshold) {
        return true;
    } else {
        return false;
    }
}

static inline bool isRevisit(const uint8_t *pMaskLineData, const int& left, const int& right) {
    const uint8_t *pEnd = pMaskLineData + right + 1;
    for (const uint8_t *pMaskData = pMaskLineData + left; pMaskData != pEnd; ++pMaskData) {
        if (*pMaskData == 0) {
            return false;
        }
    }
    
    return true;
}

static inline void pushVisitedLine(uint8_t *pMaskLineData, const int& left, const int& right) {
    memset(pMaskLineData + left, 1, (right - left + 1));
}

static inline int findLeft(int x, const uint8_t *pLineData, const uint8_t *pMaskLineData, const int& minX, const PhaseInfoCache *targetPhase) {
    const uint8_t *pData = pLineData + x * 4;
    const uint8_t *pMaskData = pMaskLineData + x;
    while (x >= minX && !pMaskData[0] && compareLuminanceWithPhaseThreshold(pData[0], pData[3], targetPhase)) {
        x--;
        pMaskData--;
        pData -= 4;
    }
    return x;
}

static inline int findRight(int x, const uint8_t *pLineData, const uint8_t *pMaskLineData, const NSUInteger& maxX, const PhaseInfoCache *targetPhase) {
    const uint8_t *pData = pLineData + x * 4;
    const uint8_t *pMaskData = pMaskLineData + x;
    while (x <= maxX && !pMaskData[0] && compareLuminanceWithPhaseThreshold(pData[0], pData[3], targetPhase)) {
        x++;
        pMaskData++;
        pData += 4;
    }
    return x;
}

static inline int skipRight(int x, const uint8_t *pLineData, const uint8_t *pMaskLineData, const NSUInteger& maxX, const PhaseInfoCache *targetPhase) {
    const uint8_t *pData = pLineData + x * 4;
    const uint8_t *pMaskData = pMaskLineData + x;
    while (x <= maxX && (pMaskData[0] || !compareLuminanceWithPhaseThreshold(pData[0], pData[3], targetPhase))) {
        x++;
        pMaskData++;
        pData += 4;
    }
    return x;
}

static inline void pushLine(std::stack<ScanLine> &stack, const int &left, const int &right, const int &y, const int &dy) {
    ScanLine scanline = { left, right, y, dy };
    stack.push(scanline);
}

static inline void popLine(std::stack<ScanLine> &stack, int &left, int &right, int &y, int &dy) {
    const ScanLine &scanLine = stack.top();

    left = scanLine.xs;
    right = scanLine.xe;
    y = scanLine.y;
    dy = scanLine.dy;
    
    stack.pop();
}

- (UIImage *)imageWithMultiphase:(CZMultiphase *)multiphase
               scanLinesCallback:(ImageWithScanLineCallback)callBack {
    UIImage *multiphaseImage = nil;
    
    if (multiphase.phaseCount == 0) {
        return multiphaseImage;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef imageRef = [self CGImage];
    int width = (int)CGImageGetWidth(imageRef);
    int height = (int)CGImageGetHeight(imageRef);
    int maxX = width - 1;
    
    const NSUInteger bytesPerPixel = 4;
    const NSUInteger bytesPerRow = width * bytesPerPixel;
    const NSUInteger bitsPerComponent = 8;
    unsigned char *imageData = (unsigned char *)calloc(height * bytesPerRow, 1);
    unsigned char *pointIsScannedList = (unsigned char *)calloc(height * width, 1);
    unsigned char *pointIsScannedListEnd = pointIsScannedList + (height * width);
    
    CGContextRef context = CGBitmapContextCreate(imageData,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 (kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast));
    CGColorSpaceRelease(colorSpace);
    
    if (multiphase.roiRegion) {
        CGAffineTransform flipTransform = CGAffineTransformMake(1, 0, 0, -1, 0, height);
        CGContextConcatCTM(context, flipTransform);
        
        CGPathRef path = [multiphase.roiRegion primaryNodePath];
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGPathRelease(path);
        
        // restore CTM
        CGContextConcatCTM(context, flipTransform);
    }

    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextSetShouldAntialias(context, NO);
    CGContextSetAllowsAntialiasing(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    // Clear existing deducted area data.
    for (CZPhase *phase in multiphase) {
        phase.areaDeducted = 0;
    }

    // prepare luminance to phase map
    NSUInteger luminanceToPhaseMap[256];
    for (int i = 0; i < 256; i++) {
        luminanceToPhaseMap[i] = [[self class] findPhaseFrom:multiphase
                                                     withRed:i
                                                       green:i
                                                        blue:i
                                                       alpha:255];
    }
    
    PhaseInfoCache phaseInfoCaches[5];  // phase count no more than 5
    NSUInteger phaseCount = MIN(5, [multiphase phaseCount]);
    for (int i = 0; i < phaseCount; ++i) {
        CZPhase *targetPhase = [multiphase phaseAtIndex:i];
        if (targetPhase) {
            CGFloat targetRed = 0.0, targetGreen = 0.0, targetBlue = 0.0, targetAlpha = 0.0;
            [targetPhase.color getRed:&targetRed
                                green:&targetGreen
                                 blue:&targetBlue
                                alpha:&targetAlpha];
            
            phaseInfoCaches[i].minThreshold = targetPhase.minThreshold;
            phaseInfoCaches[i].maxThreshold = targetPhase.maxThreshold;
            phaseInfoCaches[i].r = targetRed * 255.0;
            phaseInfoCaches[i].g = targetGreen * 255.0;
            phaseInfoCaches[i].b = targetBlue * 255.0;
            phaseInfoCaches[i].a = targetAlpha * 255.0;
            phaseInfoCaches[i].minimumArea = targetPhase.minimumArea;
            phaseInfoCaches[i].areaDeducted = 0;
        }
    }
    
    // apply cutting path to scanned mask
    if (multiphase.cuttingPaths.count > 0) {
        for(CZParticleCuttingPath *path in multiphase.cuttingPaths) {
            __block uint8_t *p = pointIsScannedList + path.startPointX + path.startPointY * width;
            __block uint8_t *pImage = &imageData[bytesPerRow * path.startPointY + path.startPointX * 4];
            if (p >= pointIsScannedList && p < pointIsScannedListEnd) {
                if ((*p) == 0) {
                    *p = 1;
                    
                    if (pImage[3] != 0) {  // alpha is not zero
                        NSUInteger targetPhaseIndex = luminanceToPhaseMap[pImage[0]];
                        PhaseInfoCache *targetPhase = &phaseInfoCaches[targetPhaseIndex];
                        if (pImage[0]) {
                            targetPhase->areaDeducted++;
                        }
                    }
                }
            }
            
            __block NSUInteger *luminanceToPhaseMapForBlock = &luminanceToPhaseMap[0];
            __block PhaseInfoCache *phaseInfoCachesForBlock = &phaseInfoCaches[0];
            
            [path enumUsingBlock: ^(uint8_t direction) {
                switch (direction) {
                    case CRACK_R:
                        p++;
                        pImage +=4 ;
                        break;
                    case CRACK_U:
                        p -= width;
                        pImage -= bytesPerRow;
                        break;
                    case CRACK_L:
                        p--;
                        pImage -= 4;
                        break;
                    case CRACK_D:
                        p += width;
                        pImage += bytesPerRow;
                        break;
                    default:
                        assert(false);
                        break;
                }
                if (p >= pointIsScannedList && p < pointIsScannedListEnd) {
                    if ((*p) == 0) {
                        *p = 1;
                        
                        if (pImage[3] != 0) {  // alpha is not zero
                            NSUInteger targetPhaseIndex = luminanceToPhaseMapForBlock[pImage[0]];
                            PhaseInfoCache *targetPhase = &phaseInfoCachesForBlock[targetPhaseIndex];
                            if (pImage[0]) {
                                targetPhase->areaDeducted++;
                            }
                        }
                    }
                }
            }];
        }
    }
    
    std::stack<ScanLine> scanLineStack;
    std::stack<ScanLine> fillLineStack;
    NSUInteger targetColorPoints = 0;
    
    CZScanLine *chords = [[CZScanLine alloc] initWithWidth:self.size.width height:self.size.height];
    
    for (int y_ = 0; y_ < height; y_++) {
        unsigned char * const pImageLine = &imageData[bytesPerRow * y_];
        unsigned char * const pMaskLine = &pointIsScannedList[width * y_];
        
        unsigned char * pImagePixel = pImageLine;
        for (int x = 0; x < width; x++, pImagePixel += bytesPerPixel) {
            
            if (pMaskLine[x]) {
                continue;
            }
            
            uint8_t sourceRed = pImagePixel[0];
            uint8_t sourceAlpha = pImagePixel[3];
            
            NSUInteger targetPhaseIndex = luminanceToPhaseMap[sourceRed];
            PhaseInfoCache *targetPhase = &phaseInfoCaches[targetPhaseIndex];
            
            if (targetPhaseIndex == NSNotFound || sourceAlpha == 0) {
                pImagePixel[0] = 0;
                pImagePixel[1] = 0;
                pImagePixel[2] = 0;
                pImagePixel[3] = 0;
                continue;
            }
            
            // quick fill algorithm, see http://www.codeproject.com/Articles/6017/QuickFill-An-efficient-flood-fill-algorithm for detail
            int parentLeft, parentRight, y = y_, dy;
            parentLeft = findLeft(x, pImageLine, pMaskLine, 0, targetPhase) + 1;
            parentRight = findRight(x, pImageLine, pMaskLine, maxX, targetPhase) - 1;
            
            ScanLine sl{parentLeft, parentRight + 1, y, 0};
            [chords addChordXStart:sl.xs xEnd:sl.xe y:y];
            fillLineStack.push(sl);
            
            pushVisitedLine(pMaskLine, parentLeft, parentRight);
            targetColorPoints += (parentRight - parentLeft + 1);

            // Push sarting point on stack
            pushLine(scanLineStack, parentLeft, parentRight, y, 1);
            pushLine(scanLineStack, parentLeft, parentRight, y, -1);

            // Start flooding
            while (!scanLineStack.empty()) {
                popLine(scanLineStack, parentLeft, parentRight, y, dy);
                y += dy;
                if (y < 0 || y >= height) {
                    continue;
                }
                
                int coverLeft = parentLeft, coverRight = parentRight;
                if (parentLeft > 0) {
                    parentLeft --;
                }
                parentRight ++;
                if (parentRight > maxX) {
                    parentRight = maxX;
                }

                unsigned char * const pMaskLineData = &pointIsScannedList[width * y];
                unsigned char * const pLineData = &imageData[bytesPerRow * y];

                if (isRevisit(pMaskLineData, parentLeft, parentRight)) {  // if is revisit
                    continue;
                }
                
                int childLeft = findLeft(parentLeft, pLineData, pMaskLineData, 0, targetPhase) + 1;
                int childRight;
                
                if (childLeft <= parentLeft) {
                    childRight = findRight(parentLeft, pLineData, pMaskLineData, maxX, targetPhase) - 1;
                    
                    ScanLine sl{childLeft, childRight + 1, y, 0};
                    [chords addChordXStart:sl.xs xEnd:sl.xe y:sl.y];
                    fillLineStack.push(sl);
                    
                    pushVisitedLine(pMaskLineData, childLeft, childRight);
                    targetColorPoints += (childRight - childLeft + 1);
                    
                    // Push unvisited lines
                    if (coverLeft <= childLeft && childRight <= coverRight) {
                        pushLine(scanLineStack, childLeft, childRight, y, dy);
                    } else {
                        pushLine(scanLineStack, childLeft, childRight, y, -dy);
                        pushLine(scanLineStack, childLeft, childRight, y, dy);
                    }
                    // Advance ChildRight end on to border
                    ++childRight;
                } else {
                    childRight = parentLeft;
                }
                
                // Fill betweens
                while (childRight < parentRight) {
                    // Skip to new ChildLeft end ( ChildRight>ParentRight on failure )
                    childRight = skipRight(childRight, pLineData, pMaskLineData, parentRight, targetPhase);
                    // If new ChildLeft end found
                    if (childRight <= parentRight) {
                        childLeft = childRight;
                        // Find ChildRight end ( this should not fail here )
                        childRight = findRight(childLeft, pLineData, pMaskLineData, maxX, targetPhase) - 1;
                        
                        // Fill line
                        ScanLine sl{childLeft, childRight + 1, y, 0};
                        [chords addChordXStart:sl.xs xEnd:sl.xe y:sl.y];
                        fillLineStack.push(sl);
                        
                        pushVisitedLine(pMaskLineData, childLeft, childRight);
                        targetColorPoints += (childRight - childLeft + 1);
                        
                        // Push unvisited lines
                        if (childRight <= coverRight) {
                            pushLine(scanLineStack, childLeft, childRight, y, dy);
                        } else {
                            pushLine(scanLineStack, childLeft, childRight, y, -dy);
                            pushLine(scanLineStack, childLeft, childRight, y, dy);
                        }
                        // Advance ChildRight end onto border
                        ++childRight;
                    }
                }
            }

            BOOL shouldFill = YES;
            if (targetColorPoints > targetPhase->minimumArea) {
                shouldFill = callBack ? callBack(chords, (uint32_t)targetPhaseIndex, targetColorPoints) : YES;
            } else {
                shouldFill = NO;
            }

            if (!shouldFill) {
                targetPhase->areaDeducted += targetColorPoints;
            }
            
            if (shouldFill) {
                while (!fillLineStack.empty()) {
                    ScanLine sl = fillLineStack.top();
                    unsigned char *pData = &imageData[bytesPerRow * sl.y + sl.xs * bytesPerPixel];
                    for (int x = sl.xs; x < sl.xe; x++, pData += bytesPerPixel) {
                        pData[0] = targetPhase->r;
                        pData[1] = targetPhase->g;
                        pData[2] = targetPhase->b;
                        pData[3] = targetPhase->a;
                    }
                    
                    fillLineStack.pop();
                };
            } else {
                std::stack<ScanLine> emptyStack;
                emptyStack.swap(fillLineStack);
            }
            
            [chords clear];
            targetColorPoints = 0;
        }
    }
    
    for (int i = 0; i < phaseCount; ++i) {
        CZPhase *targetPhase = [multiphase phaseAtIndex:i];
        if (targetPhase) {
            targetPhase.areaDeducted = phaseInfoCaches[i].areaDeducted;
        }
    }

    [chords release];
    
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    
    multiphaseImage = [UIImage imageWithCGImage:newCGImage
                                          scale:[self scale]
                                    orientation:UIImageOrientationUp];
    
    CGImageRelease(newCGImage);
    CGContextRelease(context);
    
    free(imageData);
    free(pointIsScannedList);    
    
    return multiphaseImage;
}

+ (NSUInteger)findPhaseFrom:(CZMultiphase *)multiphase
                    withRed:(uint8_t)red
                      green:(uint8_t)green
                       blue:(uint8_t)blue
                      alpha:(uint8_t)alpha {
    NSUInteger foundPhase = NSNotFound;
    if (alpha == 0) {
        return foundPhase;
    }
    
    uint8_t luminance = red;
    for (int i = 0; i < multiphase.phaseCount; i++) {
        CZPhase *singlePhase = [multiphase phaseAtIndex:i];
        if (luminance >= singlePhase.minThreshold &&
            luminance < singlePhase.maxThreshold) {
            foundPhase = i;
            break;
        }
    }
    
    return foundPhase;
}

@end
