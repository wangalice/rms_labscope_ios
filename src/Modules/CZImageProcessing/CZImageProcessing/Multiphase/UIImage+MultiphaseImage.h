//
//  UIImage+MultiphaseImage.h
//  Matscope
//
//  Created by Mike Wang on 2/17/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZMultiphase;
@class CZScanLine;

typedef BOOL (^ImageWithScanLineCallback)(CZScanLine *chords, uint32_t phaseIndex, NSUInteger chordsArea);

@interface UIImage (MultiphaseImage)

- (UIImage *)imageWithMultiphase:(CZMultiphase *)multiphase
               scanLinesCallback:(ImageWithScanLineCallback)callback;
@end
