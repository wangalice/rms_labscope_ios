//
//  CZChangeMultiphaseAction.h
//  Matscope
//
//  Created by Halley Gu on 2/25/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;
@class CZMultiphase;

@interface CZChangeMultiphaseAction : NSObject <CZUndoAction>

- (id)initWithDocManager:(CZDocManager *)docManager
              multiphase:(CZMultiphase *)multiphase
      originalMultiphase:(CZMultiphase *)originalMultiphase;

@end
