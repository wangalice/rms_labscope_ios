//
//  CZGemChords.m
//  Matscope
//
//  Created by Mike Wang on 2/28/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZGemChords.h"
#import <CZToolbox/CZToolbox.h>
#import <ZEN/GemChords.h>

using namespace GemMeasurerClr;

@interface CZGemChords () {
    GemChords *_gemChords;
}

@end

@implementation CZGemChords

- (id)init {
    self = [super init];
    if (self) {
        _gemChords = new GemChords(0, 0);
    }
    return self;
}

- (id)initWithRectSize:(CGSize)size {
    self = [self init];
    if (self) {
        if (_gemChords) {
            delete _gemChords;
        }
        _gemChords = new GemChords(size.width, size.height);
    }
    return self;
}

- (void)dealloc {
    delete _gemChords;
    
    [super dealloc];
}

- (void)addPoint:(CGPoint)point {
    _gemChords->AddPoint(point.x, point.y);
}

- (void)addChordWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    if (startPoint.y != endPoint.y) {
        CZLogv(@"Failed to add chord start point y: %f is not equal to end point y: %f", startPoint.y, endPoint.y);
        return;
    }
    
    _gemChords->AddChord(startPoint.x, endPoint.x, startPoint.y);
}

- (unsigned char)getPoint:(CGPoint)point {
    return _gemChords->GetPoint(point.x, point.y);
}

- (NSUInteger)getChordCount:(NSUInteger)y {
    return _gemChords->GetChordCount(y);
}

- (CGSize)getSize {
    return CGSizeMake(_gemChords->GetWidth(), _gemChords->GetHeight());
}

- (void)getMinX:(long *)minX maxX:(long *)maxX minY:(long *)minY maxY:(long *)maxY {
    if (minX) {
        *minX = _gemChords->GetMinX();
    }
    if (maxX) {
        *maxX = _gemChords->GetMaxX();
    }
    if (minY) {
        *minY = _gemChords->GetMinY();
    }
    if (maxY) {
        *maxY = _gemChords->GetMaxY();
    }
}

- (void)clear {
    _gemChords->Clear();
}

- (GemMeasurerClr::GemChords *)gemChords {
    return _gemChords;
}

@end
