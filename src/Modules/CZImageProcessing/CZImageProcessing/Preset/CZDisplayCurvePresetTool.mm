//
//  CZDisplayCurvePresetTool.m
//  Hermes
//
//  Created by Li, Junlin on 12/14/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZDisplayCurvePresetTool.h"
#import <GPUImage/GPUImage.h>
#import "CZBestFitFilter.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZDisplayCurvePresetTool

+ (GPUImageOutput<GPUImageInput> *)newFilterForDisplayCurvePreset:(CZDisplayCurvePreset)preset
                                                    withImageSize:(CGSize)size
                                                         forVideo:(BOOL)forVideo {
    static CGFloat optimizedGamma = 1 / 2.2;
    static NSArray *darkAreaParams = [[NSArray alloc] initWithObjects:
                                      [NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)],
                                      [NSValue valueWithCGPoint:CGPointMake(0.5019, 0.7843)],
                                      [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)],
                                      nil];
//    static NSArray *brightAreaParams = [[NSArray alloc] initWithObjects:
//                                        [NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)],
//                                        [NSValue valueWithCGPoint:CGPointMake(0.7843, 0.5019)],
//                                        [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)],
//                                        nil];
    
    GPUImageOutput<GPUImageInput> *curveFilter = nil;
    
    switch (preset) {
        case CZDisplayCurveOptimized:
            curveFilter = [[GPUImageGammaFilter alloc] init];
            [(GPUImageGammaFilter *)curveFilter setGamma:optimizedGamma];
            break;
            
        case CZDisplayCurveDetailsInDark:
            curveFilter = [[GPUImageToneCurveFilter alloc] init];
            [(GPUImageToneCurveFilter *)curveFilter setRgbCompositeControlPoints:darkAreaParams];
            break;
            
        case CZDisplayCurveBestFit:
//            curveFilter = [[CZHistogramEqualizationFilter alloc] initWithImageSize:size usesHistory:forVideo];
            curveFilter = [[CZBestFitFilter alloc] init];
            break;
            
        default:
            // Do not return an empty GPUImageFilter here for better performance.
            break;
    }
    
    return curveFilter;
}

+ (NSString *)nameOfDisplayCurvePreset:(CZDisplayCurvePreset)preset {
    NSString *name = nil;
    
    switch (preset) {
        case CZDisplayCurveOptimized:
            name = [NSString stringWithFormat:L(@"DISPLAY_CURVE_OPTIMIZED"),
                    [CZCommonUtils localizedStringFromFloat:0.45 precision:2U]];
            break;
            
        case CZDisplayCurveLinear:
            name = L(@"DISPLAY_CURVE_LINEAR");
            break;
            
        case CZDisplayCurveDetailsInDark:
            name = L(@"DISPLAY_CURVE_DETAILS_IN_DARK");
            break;
            
        case CZDisplayCurveBestFit:
            name = L(@"DISPLAY_CURVE_BEST_FIT");
            break;
            
        default:
            break;
    }
    
    return name;
}

@end
