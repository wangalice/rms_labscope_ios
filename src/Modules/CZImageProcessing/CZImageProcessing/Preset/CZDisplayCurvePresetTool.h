//
//  CZDisplayCurvePresetTool.h
//  Hermes
//
//  Created by Li, Junlin on 12/14/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class GPUImageOutput;
@protocol GPUImageInput;

typedef NS_ENUM(NSInteger, CZDisplayCurvePreset) {
    CZDisplayCurveOptimized = 0,
    CZDisplayCurveLinear,
    CZDisplayCurveDetailsInDark,
    CZDisplayCurveBestFit,
    
    CZDisplayCurvePresetCount,
    
    CZDisplayCurvePresetInvalid = -1
};

/**
 Extract from CommonUtils, needs a better class name.
 */
@interface CZDisplayCurvePresetTool : NSObject

+ (GPUImageOutput<GPUImageInput> *)newFilterForDisplayCurvePreset:(CZDisplayCurvePreset)preset
                                                    withImageSize:(CGSize)size
                                                         forVideo:(BOOL)forVideo;
+ (NSString *)nameOfDisplayCurvePreset:(CZDisplayCurvePreset)preset;

@end
