//
//  CZImageCropAction.h
//  Matscope
//
//  Created by Halley Gu on 1/7/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;
@class CZImageTool;
@class CZElement;

@interface CZImageCropAction : NSObject <CZUndoAction>

- (id)initWithDocManager:(CZDocManager *)docManager
               imageTool:(CZImageTool *)imageTool
                 element:(CZElement *)element
                   scale:(CGFloat)scale;

@end
