//
//  CZImageAction.h
//  Hermes
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;
@class CZImageTool;
@class CZImageToolParams;

@interface CZImageAction : NSObject <CZUndoAction>

- (id)initWithDocManager:(CZDocManager *)docManager
               imageTool:(CZImageTool *)imageTool
                  params:(CZImageToolParams *)params;

@end
