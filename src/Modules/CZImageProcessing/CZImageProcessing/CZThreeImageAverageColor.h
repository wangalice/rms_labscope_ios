//
//  CZThreeImageAverageColor.h
//  Matscope
//
//  Created by Ralph Jin on 7/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImage.h>

@interface CZThreeImageAverageColor : GPUImageThreeInputFilter

@end
