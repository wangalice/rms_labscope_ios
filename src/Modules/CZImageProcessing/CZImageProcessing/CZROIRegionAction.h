//
//  CZROIRegionAction.h
//  Matscope
//
//  Created by Ralph Jin on 5/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;
@class CZElement;

@interface CZROIRegionAction : NSObject <CZUndoAction>

- (id)initWithDocManager:(CZDocManager *)docManager
               roiRegion:(CZElement *)roiRegion;

@end
