//
//  CZROIRegionAction.m
//  Matscope
//
//  Created by Ralph Jin on 5/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZROIRegionAction.h"

#import <CZDocumentKit/CZDocumentKit.h>

@interface CZROIRegionAction ()

@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, retain) CZElement *roiRegion;
@end

@implementation CZROIRegionAction

- (id)initWithDocManager:(CZDocManager *)docManager
               roiRegion:(CZElement *)roiRegion {
    self = [super init];
    if (self) {
        _docManager = docManager;
        _roiRegion = [roiRegion retain];
    }
    return self;
}

- (void)dealloc {
    [_roiRegion release];
    [super dealloc];
}

#pragma mark - CZUndoAction Methods

- (NSUInteger)costMemory {
    return 128;  // Rough estimation
}

- (void)do {
    [self redo];
}

- (void)undo {
    [self redo];
}

- (void)redo {
    CZElement *oldROIRegion = [_docManager.roiRegion retain];
    _docManager.roiRegion = self.roiRegion;
    self.roiRegion = oldROIRegion;
    [oldROIRegion release];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeMultiphase;
}

@end
