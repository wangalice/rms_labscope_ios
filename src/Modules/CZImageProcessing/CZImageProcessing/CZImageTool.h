//
//  CZImageTool.h
//  Hermes
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>
#import <CZToolbox/CZToolbox.h>
#import "CZImageOutputView.h"
#import "CZImageToolParams.h"
#import "CZDisplayCurvePresetTool.h"

#define CZ_IMAGE_PROCESSING_REFACTORED 0

typedef NS_ENUM(NSUInteger, CZCroppingMode) {
    CZCroppingModeUnknown,
    CZCroppingModeRectangle,
    CZCroppingModeTriangle,
    CZCroppingModeCircle
};

@class CZDocManager;
@class CZMultiphase;
@class CZROISizePanelView;
@protocol CZImageToolDelegate;

@interface CZImageTool : NSObject<CZImageOutputViewDelegate>

@property (nonatomic, retain) CZImageToolParams *params;
@property (nonatomic, assign) id<CZImageToolDelegate> delegate;
@property (nonatomic, assign) BOOL ignoresDisplayCurve;
@property (nonatomic, assign) BOOL overExposureEnabled;
@property (nonatomic, assign) BOOL multiphaseEnabled;
@property (nonatomic, retain) CZROISizePanelView *roiSizePanelView;

+ (BOOL)multiphaseShouldUpdateWithGPU:(CZMultiphase *)multiphase;

- (id)initWithImage:(UIImage *)image;

- (id)initWithDocManager:(CZDocManager *)docManager;

/*! Gets the embedded image view object. */
- (UIView *)imageView;

/*! Gets the current working image. */
- (UIImage *)workingImage;

/*!
 * Updates the image bound to the image tool. This either updates the image in
 * relevant CZDocManager object (if exists) or updates the directly bound image.
 * \param image The new image object.
 */
- (void)setImage:(UIImage *)image;

/*! Does the actual process on the image and save the results to data source. */
- (void)apply;

/*! Removes all changes on image. */
- (void)revert;

/*! Invalidates the image view and forces a refresh. */
- (void)invalidate;

- (void)invalidateIgnoringMultiphaseMinimumArea;

/*! Render screenshot with current image parameters.
 * @param layersName, visible layers' name, can be nil which means show all.
 */
- (void)drawScreenshotInRect:(CGRect)rect;
- (void)drawScreenshotInRect:(CGRect)rect
           visibleLayersName:(NSArray *)layersName;

/*! Dismisses whatever interactive image processing UI that has been opened. */
- (void)dismissInteractiveViews;

/*! Starts WYSIWYG style, interactive image rotating. */
- (void)showInteractiveRotationOnView:(UIView *)view withFrame:(CGRect)frame;

/*! Starts WYSIWYG style, interactive image mirroring. */
- (void)showInteractiveMirroringOnView:(UIView *)view withFrame:(CGRect)frame;

/*! Rotates the image 90 degrees clockwise. */
- (void)rotateClockwise;

/*! Rotates the image 90 degrees counter-clockwise. */
- (void)rotateCounterClockwise;

/*! Rotates the image 180 degrees. */
- (void)rotate180;

/*! Flips the image horizontally. */
- (void)flipHorizontally;

/*! Flips the image vertically. */
- (void)flipVertically;

/*! Starts interactive cropping. */
- (void)showInteractiveCroppingOnView:(UIView *)view withFrame:(CGRect)frame;

- (void)updateCroppingModeWithROIRegion;
- (void)updateCroppingModeWithDefaultROI;
/*! Check if has default ROI cropping shape according to current cropping mode.If @return YES, restore it */
- (BOOL)hasDefaultROIWithCroppingMode:(CZCroppingMode)mode;

- (void)setCroppingMode:(CZCroppingMode)mode;
- (CZCroppingMode)croppingMode;

- (void)refreshROIRegion;
- (void)deleteROIRegion;
- (void)showROISizePanelWithMode:(CZCroppingMode)mode;

/*! Ends interactive cropping. */
- (void)dismissCroppingAndApply:(BOOL)shouldApply realCrop:(BOOL)isRealCrop;

/*!
 * Adjusts the gamma of the image per the value of the parameter.
 * \param gamma The new gamma value (0.0 to 3.0, with 1.0 as standard)
 */
- (void)setGamma:(CGFloat)gamma;

/*!
 * Adjusts the brightness of the image per the value of the parameter.
 * \param brightness The new brightness value (-100 to +100, with 0 as standard)
 */
- (void)setBrightness:(CGFloat)brightness;

/*!
 * Adjusts the contrast of the image per the value of the parameter.
 * \param contrast The new contrast value (-100 to +100, with 0 as standard)
 */
- (void)setContrast:(CGFloat)contrast;

/*!
 * Adjusts the color intensity of the image per the value of the parameter.
 * \param contrast The new color intensity (-100 to +100, with 0 as standard)
 */
- (void)setColorIntensity:(CGFloat)colorIntensity;

/*!
 * Adjusts the sharpness of the image per the value of the parameter.
 * \param contrast The new sharpness value (0 to +100, with 0 as standard)
 */
- (void)setSharpness:(CGFloat)sharpness;

/*! Starts WYSIWYG style, interactive display curve selection. */
- (void)showInteractiveDisplayCurveOnView:(UIView *)view withFrame:(CGRect)frame;

/*!
 * Applys a certain display curve preset on the image.
 * \param preset The display curve preset
 * \see CZDisplayCurvePreset
 */
- (void)applyDisplayCurvePreset:(CZDisplayCurvePreset)preset;

- (void)setLayerThicknessEdgeLayer:(CALayer *)layer;

- (void)setLayerThicknessEdgeLayerHidden:(BOOL)hidden;

- (void)setMultiphaseLayerContentsWithImage:(UIImage *)image;
- (CGImageRef)multiphaseLayerImage;

+ (GPUImageOutput<GPUImageInput> *)filterFromArray:(NSArray *)filterLists;

@end

@protocol CZImageToolDelegate <NSObject>

@optional
- (void)imageTool:(CZImageTool *)imageTool changedImageSizeTo:(CGSize)newSize;
- (void)imageToolDidDismissInteractiveRotation:(CZImageTool *)imageTool;
- (void)imageToolDidDismissInteractiveMirroring:(CZImageTool *)imageTool;
- (void)imageTool:(CZImageTool *)imageTool didDismissInteractiveDisplayCurve:(CZDisplayCurvePreset)preset;
- (void)imageTool:(CZImageTool *)imageTool didSwitchToCroppingMode:(CZCroppingMode)mode;
- (CGFloat)currentZoomScaleForImageTool:(CZImageTool *)imageTool;

@end
