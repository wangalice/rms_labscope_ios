//
//  CZImageToolParams.m
//  Hermes
//
//  Created by Halley Gu on 1/31/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageToolParams.h"
#import "CZImageTool.h"
#import "CZColorProcessingFilter.h"

static CGFloat CropValueIntoRange(CGFloat value, CGFloat min, CGFloat max) {
    // Normalize the value into the valid range.
    value = (value < min) ? min : value;
    value = (value > max) ? max : value;
    return value;
}

@interface CZImageToolParams ()

@property (nonatomic, retain) GPUImageFilter *rotateAndFlipFilter;
@property (nonatomic, retain) CZColorProcessingFilter *colorFilter;
@property (nonatomic, retain) GPUImageSharpenFilter *sharpenFilter;

@end

@implementation CZImageToolParams

- (id)init {
    self = [super init];
    if (self) {
        [self reset];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    CZImageToolParams *newParams = [[[self class] allocWithZone:zone] init];
    
    newParams.rotation = _rotation;
    newParams.horizontalFlipping = _horizontalFlipping;
    newParams.verticalFlipping = _verticalFlipping;
    
    newParams.gamma = _gamma;
    newParams.brightness = _brightness;
    newParams.contrast = _contrast;
    newParams.colorIntensity = _colorIntensity;
    newParams.sharpness = _sharpness;
    
    return newParams;
}

- (void)dealloc {
    [_rotateAndFlipFilter release];
    [_colorFilter release];
    [_sharpenFilter release];
    
    [super dealloc];
}

- (void)reset {
    _rotation = 0;
    _horizontalFlipping = NO;
    _verticalFlipping = NO;
    
    _gamma = 1.0f;
    _brightness = 0.0f;
    _contrast = 0.0f;
    _colorIntensity = 0.0f;
    _sharpness = 0.0f;
    
    self.rotateAndFlipFilter = nil;
    self.colorFilter = nil;
    self.sharpenFilter = nil;
}

- (BOOL)isEqualToDefaultParams:(CZImageToolParams *)defaultParams {
    if (self.rotation == defaultParams.rotation &&
        self->_horizontalFlipping == defaultParams->_horizontalFlipping &&
        self->_verticalFlipping == defaultParams->_verticalFlipping &&
        self->_gamma == defaultParams->_gamma &&
        self->_brightness == defaultParams->_brightness &&
        self->_contrast == defaultParams->_contrast &&
        self->_sharpness == defaultParams->_sharpness) {
    } else {
        return NO;
    }
    return YES;
}

- (GPUImageOutput<GPUImageInput> *)compoundFilter {
    NSMutableArray *filters = [[NSMutableArray alloc] init];
    
    // Combine rotation and flipping info into one number for switching.
    // This can save one GPUImageFilter when processing.
    NSInteger rotationNumber = _rotation;
    NSInteger flippingNumber = 0;
    
    if (_horizontalFlipping) {
        if (_verticalFlipping) { // Both flipping = rotate 180
            rotationNumber = (_rotation + 2) % 4;
        } else { // Only horizontal
            flippingNumber = 1;
        }
    } else if (_verticalFlipping) {
        flippingNumber = 2;
    }
    
    GPUImageRotationMode rotationMode = kGPUImageNoRotation;
    NSInteger combinedNumber = rotationNumber * 10 + flippingNumber;
    switch (combinedNumber) {
        case 1: // H flip
        case 22: // Rotate 180 + V flip
            rotationMode = kGPUImageFlipHorizonal;
            break;
            
        case 2: // V flip
        case 21: // Rotate 180 + H flip
            rotationMode = kGPUImageFlipVertical;
            break;
            
        case 10: // Rotate 90
            rotationMode = kGPUImageRotateRight;
            break;
            
        case 11: // Rotate 90 + H flip
        case 32: // Rotate 270 + V flip
            rotationMode = kGPUImageRotateRightFlipHorizontal;
            break;
            
        case 12: // Rotate 90 + V flip
        case 31: // Rotate 270 + H flip
            rotationMode = kGPUImageRotateRightFlipVertical;
            break;
            
        case 20: // Rotate 180
            rotationMode = kGPUImageRotate180;
            break;
            
        case 30: // Rotate 270
            rotationMode = kGPUImageRotateLeft;
            break;
            
        default:
            break;
    }
    
    if (_rotateAndFlipFilter == nil) {
        _rotateAndFlipFilter = [[GPUImageFilter alloc] init];
    }
    
    [_rotateAndFlipFilter removeAllTargets];
    [_rotateAndFlipFilter setInputRotation:rotationMode atIndex:0];
    [filters addObject:_rotateAndFlipFilter];
    
    CGFloat gamma = CropValueIntoRange(_gamma, 0.0f, 3.0f);
    CGFloat brightness = CropValueIntoRange(_brightness / 100.0f, -1.0f, 1.0f);
    CGFloat contrast = 1.0f + CropValueIntoRange(_contrast / 100.0f, -1.0f, 1.0f);
    CGFloat saturation = 1.0f + CropValueIntoRange(_colorIntensity / 100.0f, -1.0f, 1.0f);
    
    // Do color processing if any parameter is not the default value.
    if (gamma != 1.0f || brightness != 0.0f || contrast != 1.0f || saturation != 1.0f) {
        if (_colorFilter == nil) {
            _colorFilter = [[CZColorProcessingFilter alloc] init];
        }
        
        [_colorFilter removeAllTargets];
        [_colorFilter setGamma:gamma];
        [_colorFilter setBrightness:brightness];
        [_colorFilter setContrast:contrast];
        [_colorFilter setSaturation:saturation];
        [filters addObject:_colorFilter];
    }
    
    CGFloat sharpness = 4.0f * CropValueIntoRange(_sharpness / 100.0f, 0.0f, 100.0f);
    if (sharpness > 0.0f) {
        if (_sharpenFilter == nil) {
            _sharpenFilter = [[GPUImageSharpenFilter alloc] init];
        }
        
        [_sharpenFilter removeAllTargets];
        [_sharpenFilter setSharpness:sharpness];
        [filters addObject:_sharpenFilter];
    }
    
    GPUImageOutput<GPUImageInput> *compoundFilter = [CZImageTool filterFromArray:filters];
    [filters release];
    return compoundFilter;
}

@end
