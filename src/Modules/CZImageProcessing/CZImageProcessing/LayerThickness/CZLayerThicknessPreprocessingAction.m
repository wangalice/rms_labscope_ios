//
//  CZLayerThicknessPreprocessingAction.m
//  Matscope
//
//  Created by Halley Gu on 12/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLayerThicknessPreprocessingAction.h"
#if CZ_IMAGE_PROCESSING_REFACTORED
#import "MBProgressHUD+Hide.h"
#import "CZToastMessage.h"
#endif
#import "CZLayerThicknessDetector.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZImageTool.h"
#import <CZAnnotationKit/CZAnnotationKit.h>

@interface CZLayerThicknessPreprocessingAction ()

@property (nonatomic, retain) CZLayerThicknessDetector *detector;
@property (nonatomic, assign) CZImageTool *imageTool;
@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, retain) CZImageDocument *originalDocument;
#if CZ_IMAGE_PROCESSING_REFACTORED
@property (nonatomic, retain) MBProgressHUD *progressHUD;
#endif

@end

@implementation CZLayerThicknessPreprocessingAction

- (id)initWithDetector:(CZLayerThicknessDetector *)detector
          andImageTool:(CZImageTool *)imageTool {
    self = [super init];
    if (self) {
        _detector = [detector retain];
        _docManager = detector.docManager;
        _imageTool = imageTool;
    }
    return self;
}

- (void)dealloc {
    [_detector release];
    [_originalDocument release];
#if CZ_IMAGE_PROCESSING_REFACTORED
    [_progressHUD release];
#endif
    
    [super dealloc];
}

#pragma mark -
#pragma mark CZUndoAction Methods

- (NSUInteger)costMemory {
    // Rough estimation, assuming the UIImage object is 32-bit RGBA.
    CGSize size = self.originalDocument.imageSize;
    return size.width * size.height * 4;
}

- (void)do {
    // Do nothing. "redo" needs to be called manually before pushing the action
    // into undo manager since it shouldn't be pushed into if the action fails.
}

- (void)undo {
    if (self.originalDocument) {
        [self.detector reset];
        const CGSize originalImageSize = self.originalDocument.imageSize;
        const CGSize newImageSize = self.docManager.document.imageSize;
        self.docManager.document = self.originalDocument;
        if (!CGSizeEqualToSize(originalImageSize, newImageSize)) {
            [self.imageTool.delegate imageTool:self.imageTool changedImageSizeTo:originalImageSize];
        }
        
        [self.imageTool setLayerThicknessEdgeLayer:nil];
        
#if CZ_IMAGE_PROCESSING_REFACTORED
        [[CZToastMessage sharedInstance] hideNotificationMessage];
#endif
    }
}

- (void)redo {
    [self doActionWithDelegate:nil];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeImage;
}

- (void)doActionWithDelegate:(id<CZLayerThicknessPreprocessingActionDelegate>)delegate {
    if (self.progressHUDContainer) {
        // Show a spinner while image is preprocessed for PCB mode.
#if CZ_IMAGE_PROCESSING_REFACTORED
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.progressHUDContainer animated:YES];
        [hud setColor:kSpinnerBackgroundColor];
        [hud setLabelText:L(@"BUSY_INDICATOR_LAYER_THICKNESS")];
        self.progressHUD = hud;
#endif
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        NSError *error = nil;
        UIImage *image = [self.detector preprocess:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^(){
            if (image) {
                const CGSize originalImageSize = self.originalDocument.imageSize;
                const CGSize newImageSize = image.size;
                self.originalDocument = self.docManager.document;
                self.docManager.document = [self.originalDocument documentByReplacingImage:image];
                [self.docManager setImageModified:YES]; // Refresh magnifier
                if (!CGSizeEqualToSize(originalImageSize, newImageSize)) {
                    [self.imageTool.delegate imageTool:self.imageTool changedImageSizeTo:newImageSize];
                }
                CZElement *element = [self.docManager.elementLayer firstElementOfClass:[CZElementScaleBar class]];
                [element notifyDidChange];
            }
            
            if (self.progressHUDContainer) {
#if CZ_IMAGE_PROCESSING_REFACTORED
                [self.progressHUD dismiss:YES];
                self.progressHUD = nil;
#endif
            }
            
            if (image == nil && self.progressHUDContainer) {
                // Something wrong happened.
                [self.imageTool setLayerThicknessEdgeLayer:nil];
                
                if ([delegate respondsToSelector:@selector(layerThicknessPreprocessingActionDidFinishWithError:)]) {
                    [delegate layerThicknessPreprocessingActionDidFinishWithError:error];
                }
            } else {
                CALayer *newEdgeLayer = [self.detector newEdgeLayer];
                [self.imageTool setLayerThicknessEdgeLayer:newEdgeLayer];
                [newEdgeLayer release];
                
                if ([delegate respondsToSelector:@selector(layerThicknessPreprocessingActionDidFinishSuccessfully)]) {
                    [delegate layerThicknessPreprocessingActionDidFinishSuccessfully];
                }
            }
        });
    });
}

@end
