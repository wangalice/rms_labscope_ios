//
//  CZLayerThicknessDetector.mm
//  Matscope
//
//  Created by Halley Gu on 12/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLayerThicknessDetector.h"
#include <opencv2/imgproc/imgproc.hpp>
#import <CZToolbox/CZToolbox.h>
#import <GPUImage/GPUImage.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "UIImage+CVMat.h"
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZColorProcessingFilter.h"

NSString * const CZLayerThicknessEdgeLayerName = @"LayerThicknessEdgeLayer";
NSString * const CZLayerThicknessErrorDomain = @"LayerThicknessError";

using namespace cv;

const double kVerticalSlope = -1;

class CZLayerThicknessEdge {
public:
    cv::Point p1;
    cv::Point p2;
    double k; // If -1, vertical line
    double b; // If k == -1, x = b
    BOOL isVertical;
    
    CGFloat getXWithY(CGFloat y) {
        if (k == kVerticalSlope) {
            return b;
        }
        return (y - b) / k;
    }
    
    CGFloat getYWithX(CGFloat x) {
        if (k == kVerticalSlope) {
            return -1;
        }
        return k * x + b;
    }
};

struct CZLayerThicknessLineGroup {
    CGFloat minPointA;
    CGFloat minPointB;
    CGFloat maxPointA;
    CGFloat maxPointB;
    CGFloat averageOnAxisB;
    CGFloat weightForAverage;
    NSUInteger linesCount;
    BOOL isVertical;
};

void ThinSubiteration1(Mat &pSrc, Mat &pDst) {
    int rows = pSrc.rows;
    int cols = pSrc.cols;
    pSrc.copyTo(pDst);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (pSrc.at<uchar>(i, j) == 1) {
                /// get 8 neighbors
                /// calculate C(p)
                int neighbor0 = (int) pSrc.at<uchar>(i - 1, j - 1);
                int neighbor1 = (int) pSrc.at<uchar>(i - 1, j);
                int neighbor2 = (int) pSrc.at<uchar>(i - 1, j + 1);
                int neighbor3 = (int) pSrc.at<uchar>(i, j + 1);
                int neighbor4 = (int) pSrc.at<uchar>(i + 1, j + 1);
                int neighbor5 = (int) pSrc.at<uchar>(i + 1, j);
                int neighbor6 = (int) pSrc.at<uchar>(i + 1, j - 1);
                int neighbor7 = (int) pSrc.at<uchar>(i, j - 1);
                int C = int(~neighbor1 & ( neighbor2 | neighbor3)) +
                int(~neighbor3 & ( neighbor4 | neighbor5)) +
                int(~neighbor5 & ( neighbor6 | neighbor7)) +
                int(~neighbor7 & ( neighbor0 | neighbor1));
                if (C == 1) {
                    /// calculate N
                    int N1 = int(neighbor0 | neighbor1) +
                    int(neighbor2 | neighbor3) +
                    int(neighbor4 | neighbor5) +
                    int(neighbor6 | neighbor7);
                    int N2 = int(neighbor1 | neighbor2) +
                    int(neighbor3 | neighbor4) +
                    int(neighbor5 | neighbor6) +
                    int(neighbor7 | neighbor0);
                    int N = min(N1,N2);
                    if ((N == 2) || (N == 3)) {
                        /// calculate criteria 3
                        int c3 = ( neighbor1 | neighbor2 | ~neighbor4) & neighbor3;
                        if (c3 == 0) {
                            pDst.at<uchar>( i, j) = 0;
                        }
                    }
                }
            }
        }
    }
}

void ThinSubiteration2(Mat &pSrc, Mat &pDst) {
    int rows = pSrc.rows;
    int cols = pSrc.cols;
    pSrc.copyTo( pDst);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (pSrc.at<uchar>(i, j) == 1) {
                /// get 8 neighbors
                /// calculate C(p)
                
                int neighbor0 = (int) pSrc.at<uchar>(i - 1, j - 1);
                int neighbor1 = (int) pSrc.at<uchar>(i - 1, j);
                int neighbor2 = (int) pSrc.at<uchar>(i - 1, j + 1);
                int neighbor3 = (int) pSrc.at<uchar>(i, j + 1);
                int neighbor4 = (int) pSrc.at<uchar>(i + 1, j + 1);
                int neighbor5 = (int) pSrc.at<uchar>(i + 1, j);
                int neighbor6 = (int) pSrc.at<uchar>(i + 1, j - 1);
                int neighbor7 = (int) pSrc.at<uchar>(i, j - 1);
                int C = int(~neighbor1 & ( neighbor2 | neighbor3)) +
                int(~neighbor3 & ( neighbor4 | neighbor5)) +
                int(~neighbor5 & ( neighbor6 | neighbor7)) +
                int(~neighbor7 & ( neighbor0 | neighbor1));
                if (C == 1) {
                    /// calculate N
                    int n1 = int(neighbor0 | neighbor1) +
                    int(neighbor2 | neighbor3) +
                    int(neighbor4 | neighbor5) +
                    int(neighbor6 | neighbor7);
                    int n2 = int(neighbor1 | neighbor2) +
                    int(neighbor3 | neighbor4) +
                    int(neighbor5 | neighbor6) +
                    int(neighbor7 | neighbor0);
                    int n = min(n1, n2);
                    if ((n == 2) || (n == 3)) {
                        int E = (neighbor5 | neighbor6 | ~neighbor0) & neighbor7;
                        if (E == 0) {
                            pDst.at<uchar>(i, j) = 0;
                        }
                    }
                }
            }
        }
    }
}

void NormalizeLetter(Mat &inputarray, Mat &outputarray) {
    bool bDone = false;
    int rows = inputarray.rows;
    int cols = inputarray.cols;
    
    inputarray.convertTo(inputarray, CV_8UC1);
    
    /// pad source
    Mat p_enlarged_src = Mat(rows + 2, cols + 2, CV_8UC1);
    for(int i = 0; i < (rows + 2); i++) {
        p_enlarged_src.at<uchar>(i, 0) = 0;
        p_enlarged_src.at<uchar>(i, cols + 1) = 0;
    }
    for(int j = 0; j < (cols + 2); j++) {
        p_enlarged_src.at<uchar>(0, j) = 0;
        p_enlarged_src.at<uchar>(rows + 1, j) = 0;
    }
    
    inputarray.copyTo(p_enlarged_src(cv::Rect(2, 2, cols, rows)));
    threshold(p_enlarged_src, p_enlarged_src, 20, 1, THRESH_BINARY);
    
    /// start to thin
    Mat p_thinMat1 = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    Mat p_thinMat2 = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    Mat p_cmp = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    
    while (bDone != true) {
        /// sub-iteration 1
        ThinSubiteration1(p_enlarged_src, p_thinMat1);
        /// sub-iteration 2
        ThinSubiteration2(p_thinMat1, p_thinMat2);
        /// compare
        compare(p_enlarged_src, p_thinMat2, p_cmp, CV_CMP_EQ);
        /// check
        int num_non_zero = countNonZero(p_cmp);
        if (num_non_zero == (rows + 2) * (cols + 2)) {
            bDone = true;
        }
        /// copy
        p_thinMat2.copyTo(p_enlarged_src);
    }
    
    outputarray = p_enlarged_src(cv::Rect(1, 1, cols, rows));
}


@interface CZDocManager (LayerThickness)

- (BOOL)hasAnyLayerThicknessAnnotation;

@end

@implementation CZDocManager (LayerThickness)

- (BOOL)hasAnyLayerThicknessAnnotation {
    for (CZElement *element in self.elementLayer) {
        if ([element isKindOfClass:[CZElementLine class]]) {
            CZElementLine *line = (CZElementLine *)element;
            return line.measurementNameHidden;
        }
    }
    return NO;
}

@end

@interface CZLayerThicknessDetector () {
    vector<CZLayerThicknessEdge> edges;
}

@property (nonatomic, retain) CZDocManager *docManager;
@property (nonatomic, assign) BOOL isPreprocessed;

+ (double)normalizedAngle:(double)angle;

@end

@implementation CZLayerThicknessDetector

- (id)initWithDocManager:(CZDocManager *)docManager {
    if (docManager == nil) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _docManager = [docManager retain];
        _isPreprocessed = NO;
    }
    return self;
}

- (void)dealloc {
    [_docManager release];
    
    [super dealloc];
}

- (UIImage *)preprocess:(NSError **)error {
    const double CV_HALF_PI = CV_PI / 2.0;
    const double CV_QUARTER_PI = CV_PI / 4.0;
    const float kGammaValue = 1.5;
    const float kContrastValue = 2.0;
    const float kMinLineLengthRatio = 0.03125; // 1/32
    const float kExtraThresholdingRatio = 0.4;
    const int kAdaptiveBlockSize = 7;
    const double kAdaptiveCValue = 5;
    const double kMinimumContourArea = 400;
    const NSUInteger kMaxContourCount = 30;
    const int kHoughThreshold = 50;
    const NSUInteger kMaxLineCount = 200;
    const double kSkewingAngleTolerance = 2.0 * CV_PI / 180.0;
    const double kLineAngleTolerance = 3.0 * CV_PI / 180.0;
    const float kAxisAToleranceRatio = 0.5;
    const float kAxisBDefaultTolerance = 20.0;
    const float kAxisBToleranceInMicrometer = 10.0;
    const int kMinLinesInGroup = 3;
    
    UIImage *source = [self.docManager.document outputImage];
    if (source == nil) {
        *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessGeneralError userInfo:nil];
        return nil;
    }
    
    self.isPreprocessed = NO;
    edges.clear();
    
    UIImage *preprocessedSource = nil;
    @autoreleasepool {
        CZColorProcessingFilter *filter = [[CZColorProcessingFilter alloc] init];
        filter.gamma = kGammaValue;
        filter.contrast = kContrastValue;
        preprocessedSource = [[filter imageByFilteringImage:source] retain];
        [filter release];
    }
    Mat matPreprocessed = [preprocessedSource mat];
    [preprocessedSource release];
    
    int width = matPreprocessed.cols;
    int height = matPreprocessed.rows;
    Point2f centerPoint(width / 2.0, height / 2.0);
    
    int minLineLength = (int)floor(width * kMinLineLengthRatio);
    
    Mat matGray;
    cvtColor(matPreprocessed, matGray, CV_BGR2GRAY);
    
    Mat matDilated;
    dilate(matGray, matDilated, Mat());
    matGray.release();
    
    // http://stackoverflow.com/a/16047590/1241690
    // Calculate a base threshold using Otsu's algorithm.
    Mat matTemp;
    double thresholdBase = threshold(matDilated, matTemp, 0, 255, CV_THRESH_TOZERO + CV_THRESH_OTSU);
    matTemp.release();
    
    // Use a position between base threshold and 255 as new threshold, ignore
    // all the pixels which have lower grayscale value than the threshold.
    Mat matThreshold;
    double newThreshold = thresholdBase + (255 - thresholdBase) * kExtraThresholdingRatio;
    threshold(matDilated, matThreshold, newThreshold, 255, CV_THRESH_TOZERO);
    matDilated.release();
    
    // Use adaptive thresholding method to get binary edge image.
    Mat matAdaptive;
    adaptiveThreshold(matThreshold, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                      THRESH_BINARY_INV, kAdaptiveBlockSize, kAdaptiveCValue);
    matThreshold.release();
    
    // Find all the outermost contours as the target edges.
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(matAdaptive, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
    
    if (contours.size() == 0) {
        CZLogv(@"Layer thickness error: no contour is found.");
        *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessGeneralError userInfo:nil];
        return nil;
    }
    
    Mat matEdge = Mat::zeros(matAdaptive.size(), CV_8UC1);
    matAdaptive.release();
    
    NSUInteger validContourCount = 0;
    // Draw the contours on an new empty image to ignore inner edges.
    for (size_t i = 0; i< contours.size(); ++i) {
        if (contourArea(contours[i]) < kMinimumContourArea) {
            continue;
        }
        
        ++validContourCount;
        drawContours(matEdge, contours, (int)i, Scalar(255), 2, 8, hierarchy, 0, cv::Point());
    }
    
    if (validContourCount > kMaxContourCount) {
        CZLogv(@"Layer thickness error: too many contours are found.");
        *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessScalingError userInfo:nil];
        return nil;
    }
    
    vector<Vec4i> lines;
    BOOL shouldRotate = NO;
    Mat rotation;
    cv::Rect cropAfterRotation(0, 0, width, height);
    
    // Do rotation only when this image was not detected for PCB layers before.
    if (![self.docManager hasAnyLayerThicknessAnnotation]) {
        // Find long lines from the edges to calculate the skewing angle.
        HoughLinesP(matEdge, lines, 1, CV_PI/180, kHoughThreshold,
                    minLineLength * 1.5, minLineLength / 5);
        
        if (lines.size() == 0) {
            CZLogv(@"Layer thickness warning: no lines is found for rotation.");
        }
        
        if (lines.size() > kMaxLineCount) {
            CZLogv(@"Layer thickness error: too many lines are found for rotation.");
            *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessGeneralError userInfo:nil];
            return nil;
        }
        
        double averageAngle = 0.0;
        
        if (!lines.empty()) {
            double weightSum = 0.0;
            double angleSum = 0.0;
            for (size_t i = 0; i < lines.size(); i++) {
                double x = lines[i][2] - lines[i][0];
                double y = lines[i][3] - lines[i][1];
                double angle = atan2(y, x);
                double distance = hypot(x, y);
                
                // Normalize angle into [0, PI)
                if (angle < 0.0) {
                    angle += CV_PI;
                } else if (angle == CV_PI) {
                    angle = 0.0;
                }
                
                // Normalize angle into [0, PI/2)
                if (angle >= CV_HALF_PI) {
                    angle -= CV_HALF_PI;
                }
                
                // Normalize angle into (-PI/4, PI/4]
                if (angle >= CV_QUARTER_PI) {
                    angle -= CV_HALF_PI;
                }
                
                // If the angle is very close to -PI/4, consider it PI/4
                if (angle < - CV_QUARTER_PI + kSkewingAngleTolerance) {
                    angle = CV_QUARTER_PI;
                }
                
                double weight = pow(distance, 10) / 100.0;
                angleSum += angle * weight;
                weightSum += weight;
            }
            
            averageAngle = angleSum / weightSum * 180.0 / CV_PI;
            
            const float kMaxAngleAllowed = 15;
            if (fabs(averageAngle) > kMaxAngleAllowed) {
                CZLogv(@"Layer thickness error: detected rotation is greater than 15 degrees.");
                *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessRotationError userInfo:nil];
                return nil;
            }
            
            // Rotate the image so that the layers are horizontal/vertical.
            rotation = cv::getRotationMatrix2D(centerPoint, averageAngle, 1);
            warpAffine(matEdge, matEdge, rotation, matEdge.size(), cv::INTER_CUBIC);
            shouldRotate = YES;
        }
        
        if (shouldRotate) {
            // Assume center point is at (0, 0)
            CGAffineTransform aftrm = CGAffineTransformMakeRotation(averageAngle * CV_PI / 180.0);
            
            CGPoint topLeft = CGPointMake(-centerPoint.x, centerPoint.y);
            CGPoint rTopLeft = CGPointApplyAffineTransform(topLeft, aftrm);
            
            CGPoint topRight = CGPointMake(centerPoint.x, centerPoint.y);
            CGPoint rTopRight = CGPointApplyAffineTransform(topRight, aftrm);
            
            CGPoint bottomLeft = CGPointMake(-centerPoint.x, -centerPoint.y);
            CGPoint rBottomLeft = CGPointApplyAffineTransform(bottomLeft, aftrm);
            
            CGPoint bottomRight = CGPointMake(centerPoint.x, -centerPoint.y);
            CGPoint rBottomRight = CGPointApplyAffineTransform(topLeft, aftrm);
            
            CGPoint intersection[8];
            intersection[0] = CZCalcCrossPoint(topLeft, bottomRight, rTopLeft, rTopRight);
            intersection[1] = CZCalcCrossPoint(topLeft, bottomRight, rTopRight, rBottomRight);
            intersection[2] = CZCalcCrossPoint(topLeft, bottomRight, rBottomRight, rBottomLeft);
            intersection[3] = CZCalcCrossPoint(topLeft, bottomRight, rBottomLeft, rTopLeft);
            intersection[4] = CZCalcCrossPoint(topRight, bottomLeft, rTopLeft, rTopRight);
            intersection[5] = CZCalcCrossPoint(topRight, bottomLeft, rTopRight, rBottomRight);
            intersection[6] = CZCalcCrossPoint(topRight, bottomLeft, rBottomRight, rBottomLeft);
            intersection[7] = CZCalcCrossPoint(topRight, bottomLeft, rBottomLeft, rTopLeft);
            
            double minHalfWidth = centerPoint.x;
            for (int i = 0; i < 8; i++) {
                if (!isnan(intersection[i].x)) {
                    double width = fabs(intersection[i].x);
                    if (width < minHalfWidth) {
                        minHalfWidth = width;
                    }
                }
            }
            
            double minWidth = floor(minHalfWidth / 4) * 8;
            double minHeight = floor(minWidth * height / width);
            cropAfterRotation.x = floor((width - minWidth) / 2);
            cropAfterRotation.y = floor((height - minHeight) / 2);
            cropAfterRotation.width = minWidth;
            cropAfterRotation.height = minHeight;

            matEdge = cv::Mat(matEdge, cropAfterRotation);
        }
    }
    
    // Detect lines on the rotated image with a more realistic length threshold.
    lines.clear();
    HoughLinesP(matEdge, lines, 1, CV_PI/180, kHoughThreshold,
                minLineLength, minLineLength);
    
    if (lines.size() == 0) {
        CZLogv(@"Layer thickness error: no edge is recognized for layers.");
        *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessGeneralError userInfo:nil];
        return nil;
    }
    
    vector<CZLayerThicknessLineGroup> groups;
    
    // Create groups for every single line first.
    for (size_t i = 0; i < lines.size(); i++) {
        Vec4i l = lines[i];
        
        cv::Point p1(l[0], l[1]);
        cv::Point p2(l[2], l[3]);
        double w = abs(p1.x - p2.x);
        double h = abs(p1.y - p2.y);
        double angle = [CZLayerThicknessDetector normalizedAngle:atan2(h, w)];
        
        // Filter out those non-horizontal and non-vertical lines.
        if (fabs(angle) <= kLineAngleTolerance) {
            CZLayerThicknessLineGroup group;
            group.isVertical = (h > w);
            group.linesCount = 1;
            
            if (group.isVertical) {
                if (p1.y < p2.y) { // p1 is up
                    group.minPointA = p1.y;
                    group.minPointB = p1.x;
                    group.maxPointA = p2.y;
                    group.maxPointB = p2.x;
                } else {
                    group.minPointA = p2.y;
                    group.minPointB = p2.x;
                    group.maxPointA = p1.y;
                    group.maxPointB = p1.x;
                }
            } else {
                if (p1.x < p2.x) { // p1 is left
                    group.minPointA = p1.x;
                    group.minPointB = p1.y;
                    group.maxPointA = p2.x;
                    group.maxPointB = p2.y;
                } else {
                    group.minPointA = p2.x;
                    group.minPointB = p2.y;
                    group.maxPointA = p1.x;
                    group.maxPointB = p1.y;
                }
            }
            
            group.averageOnAxisB = (group.minPointB + group.maxPointB) * 0.5;
            group.weightForAverage = group.maxPointA - group.minPointA;
            
            groups.push_back(group);
        }
    }
    
    // Combine groups.
    BOOL didCombineGroups = NO;
    float axisATolerance = minLineLength * kAxisAToleranceRatio;
    float axisBTolerance = kAxisBDefaultTolerance;
    
    float scaling = self.docManager.imageProperties.scaling;
    if (scaling > 0) {
        axisBTolerance = kAxisBToleranceInMicrometer / scaling;
        if (scaling > 0.8) { // About 7x
            // Doubles the tolerance for low magnification.
            axisBTolerance += axisBTolerance;
        }
    }
    
    do {
        didCombineGroups = NO;
        
        for (int i = 0; i < (int)groups.size() - 1; ++i) {
            CZLayerThicknessLineGroup &g1 = groups[i];
            
            for (int j = i + 1; j < groups.size(); j++) {
                CZLayerThicknessLineGroup &g2 = groups[j];
                
                if (g1.isVertical != g2.isVertical) {
                    continue;
                }
                
                BOOL shouldCombine = NO;
                CGFloat distanceOnAxisB = fabs(g1.averageOnAxisB - g2.averageOnAxisB);
                if (distanceOnAxisB <= axisBTolerance) {
                    if (g1.minPointA < g2.minPointA) {
                        if (g2.minPointA <= g1.maxPointA + axisATolerance) {
                            shouldCombine = YES;
                        }
                    } else {
                        if (g1.minPointA <= g2.maxPointA + axisATolerance) {
                            shouldCombine = YES;
                        }
                    }
                }
                
                if (shouldCombine) {
                    CGFloat newMinPointA = MIN(g1.minPointA, g2.minPointA);
                    CGFloat newMaxPointA = MAX(g1.maxPointA, g2.maxPointA);
                    CGFloat totalLength = newMaxPointA - newMinPointA;
                    
                    float weightMinB1 = totalLength - (g1.minPointA - newMinPointA);
                    float weightMinB2 = totalLength - (g2.minPointA - newMinPointA);
                    CGFloat newMinPointB = (g1.minPointB * weightMinB1 + g2.minPointB * weightMinB2) / (weightMinB1 + weightMinB2);
                    
                    float weightMaxB1 = totalLength - (newMaxPointA - g1.maxPointA);
                    float weightMaxB2 = totalLength - (newMaxPointA - g2.maxPointA);
                    CGFloat newMaxPointB = (g1.maxPointB * weightMaxB1 + g2.maxPointB * weightMaxB2) / (weightMaxB1 + weightMaxB2);
                    
                    g1.minPointA = newMinPointA;
                    g1.minPointB = newMinPointB;
                    g1.maxPointA = newMaxPointA;
                    g1.maxPointB = newMaxPointB;
                    
                    g1.averageOnAxisB = (g1.averageOnAxisB * g1.weightForAverage + g2.averageOnAxisB * g2.weightForAverage) / (g1.weightForAverage + g2.weightForAverage);
                    g1.weightForAverage = newMaxPointA - newMinPointA;
                    g1.linesCount += g2.linesCount;
                    
                    didCombineGroups = YES;
                    groups.erase(groups.begin() + j);
                    break;
                }
            }
        }
    } while (didCombineGroups); // Combine until nothing can be combined anymore.
    
    vector<CZLayerThicknessEdge> horizontalEdges;
    vector<CZLayerThicknessEdge> verticalEdges;
    
    // Convert the information of groups to layer edges for later use.
    for (int i = 0; i < groups.size(); ++i) {
        CZLayerThicknessLineGroup &group = groups[i];
        
        // If the amount of lines is too low, ignore.
        if (group.linesCount < kMinLinesInGroup) {
            continue;
        }
        
        CZLayerThicknessEdge edge;
        edge.isVertical = group.isVertical;
        
        if (edge.isVertical) {
            edge.p1.x = group.minPointB;
            edge.p2.x = group.maxPointB;
            edge.p1.y = group.minPointA;
            edge.p2.y = group.maxPointA;
        } else {
            edge.p1.x = group.minPointA;
            edge.p2.x = group.maxPointA;
            edge.p1.y = group.minPointB;
            edge.p2.y = group.maxPointB;
        }
        
        double w = abs(edge.p1.x - edge.p2.x);
        double h = abs(edge.p1.y - edge.p2.y);
        
        // If the line is too short, ignore.
        if ((edge.isVertical && h < minLineLength * 2) ||
            (!edge.isVertical && w < minLineLength * 2)) {
            continue;
        }
        
        if (edge.p1.x == edge.p2.x) {
            edge.k = kVerticalSlope;
            edge.b = edge.p1.x;
        } else {
            double dy = edge.p1.y - edge.p2.y;
            double dx = edge.p1.x - edge.p2.x;
            edge.k = dy / dx;
            edge.b = edge.p1.y - edge.k * edge.p1.x;
            
            // If the angle is too big, ignore.
            double angle = [CZLayerThicknessDetector normalizedAngle:atan2(h, w)];
            if (fabs(angle) > kLineAngleTolerance) {
                continue;
            }
        }
        
        if (edge.isVertical) {
            verticalEdges.push_back(edge);
        } else {
            horizontalEdges.push_back(edge);
        }
    }
    
    if (verticalEdges.size() > 1) {
        edges.insert(edges.end(), verticalEdges.begin(), verticalEdges.end());
    }
    if (horizontalEdges.size() > 1) {
        edges.insert(edges.end(), horizontalEdges.begin(), horizontalEdges.end());
    }
    
    if (edges.empty()) {
        CZLogv(@"Layer thickness error: no edge is recognized for layers.");
        if (error) {
            *error = [NSError errorWithDomain:CZLayerThicknessErrorDomain code:kCZLayerThicknessGeneralError userInfo:nil];
        }
        return nil;
    }
    
    self.isPreprocessed = YES;
    
    if (shouldRotate) {
        Mat src = [source mat];
        warpAffine(src, src, rotation, src.size(), cv::INTER_CUBIC);
        src = Mat(src, cropAfterRotation);
        source = [UIImage imageWithMat:src];
    }
    
    return source;
}

- (void)reset {
    edges.clear();
    self.isPreprocessed = NO;
}

- (CALayer *)newEdgeLayer {
    if (!self.isPreprocessed) {
        return nil;
    }
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGSize size = self.docManager.document.imageSize;
    size.width /= scale;
    size.height /= scale;
    
    CAShapeLayer *newLayer = [[CAShapeLayer alloc] init];
    newLayer.name = CZLayerThicknessEdgeLayerName;
    newLayer.frame = CGRectMake(0, 0, size.width, size.height);
    newLayer.bounds = CGRectMake(0, 0, size.width, size.height);
    newLayer.fillColor = [[UIColor clearColor] CGColor];
    newLayer.strokeColor = [[UIColor greenColor] CGColor];
    newLayer.lineWidth = size.width * scale / 850.0f;
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    for (vector<CZLayerThicknessEdge>::iterator it = edges.begin(); it != edges.end(); ++it) {
        CGPathMoveToPoint(path, NULL, it->p1.x / scale, it->p1.y / scale);
        CGPathAddLineToPoint(path, NULL, it->p2.x / scale, it->p2.y / scale);
    }
    
    newLayer.path = path;
    CGPathRelease(path);
    
    return newLayer;
}

- (NSArray *)rectsForLineElementFromPoint:(CGPoint)point {
    if (!self.isPreprocessed) {
        return nil;
    }
    
    // Find the closest edges around the point.
    
    int leftEdgeIndex = -1;
    int rightEdgeIndex = -1;
    int upEdgeIndex = -1;
    int downEdgeIndex = -1;
    
    for (int i = 0; i < edges.size(); ++i) {
        CZLayerThicknessEdge &edge = edges[i];
        
        if (edge.isVertical) {
            if (point.y >= edge.p1.y && point.y <= edge.p2.y) {
                CGFloat x = edge.getXWithY(point.y);
                
                if (point.x > x) {
                    if (leftEdgeIndex == -1) {
                        leftEdgeIndex = i;
                    } else {
                        if (edges[leftEdgeIndex].getXWithY(point.y) < x) {
                            leftEdgeIndex = i;
                        }
                    }
                } else {
                    if (rightEdgeIndex == -1) {
                        rightEdgeIndex = i;
                    } else {
                        if (edges[rightEdgeIndex].getXWithY(point.y) > x) {
                            rightEdgeIndex = i;
                        }
                    }
                }
            }
        } else {
            if (point.x >= edge.p1.x && point.x <= edge.p2.x) {
                CGFloat y = edge.getYWithX(point.x);
                
                if (point.y > y) {
                    if (upEdgeIndex == -1) {
                        upEdgeIndex = i;
                    } else {
                        if (edges[upEdgeIndex].getYWithX(point.x) < y) {
                            upEdgeIndex = i;
                        }
                    }
                } else {
                    if (downEdgeIndex == -1) {
                        downEdgeIndex = i;
                    } else {
                        if (edges[downEdgeIndex].getYWithX(point.x) > y) {
                            downEdgeIndex = i;
                        }
                    }
                }
            }
        }
    }
    
    const float annotationPositions[3] = {0.15, 0.5, 0.85};
    
    NSMutableArray *rectsForUpDown = [[NSMutableArray alloc] init];
    CGFloat heightForUpDown = CGFLOAT_MAX;
    
    if (upEdgeIndex != -1 && downEdgeIndex != -1) {
        CZLayerThicknessEdge upEdge = edges[upEdgeIndex];
        CZLayerThicknessEdge downEdge = edges[downEdgeIndex];
        
        CGFloat minX = MAX(upEdge.p1.x, downEdge.p1.x);
        CGFloat maxX = MIN(upEdge.p2.x, downEdge.p2.x);
        CGFloat widthForUpDown = maxX - minX;
        
        CGFloat totalHeightForUpDown = 0;
        for (int i = 0; i < 3; ++i) {
            CGFloat x = round(minX + widthForUpDown * annotationPositions[i]);
            CGFloat y1 = upEdge.getYWithX(x);
            CGFloat y2 = downEdge.getYWithX(x);
            totalHeightForUpDown += y2 - y1;
            
            CGRect rect = CGRectMake(x, y1, 0, y2 - y1);
            [rectsForUpDown addObject:[NSValue valueWithCGRect:rect]];
        }
        heightForUpDown = totalHeightForUpDown / 3;
    }
    
    NSMutableArray *rectsForLeftRight = [[NSMutableArray alloc] init];
    CGFloat widthForLeftRight = CGFLOAT_MAX;
    
    if (leftEdgeIndex != -1 && rightEdgeIndex != -1) {
        CZLayerThicknessEdge leftEdge = edges[leftEdgeIndex];
        CZLayerThicknessEdge rightEdge = edges[rightEdgeIndex];
        
        CGFloat minY = MAX(leftEdge.p1.y, rightEdge.p1.y);
        CGFloat maxY = MIN(leftEdge.p2.y, rightEdge.p2.y);
        CGFloat heightForLeftRight = maxY - minY;
        
        CGFloat totalWidthForLeftRight = 0;
        for (int i = 0; i < 3; ++i) {
            CGFloat y = round(minY + heightForLeftRight * annotationPositions[i]);
            CGFloat x1 = leftEdge.getXWithY(y);
            CGFloat x2 = rightEdge.getXWithY(y);
            totalWidthForLeftRight += x2 - x1;
            
            CGRect rect = CGRectMake(x1, y, x2 - x1, 0);
            [rectsForLeftRight addObject:[NSValue valueWithCGRect:rect]];
        }
        widthForLeftRight = totalWidthForLeftRight / 3;
    }
    
    // Pick the shorter edges if both horizontal and vertical edges are detected.
    NSArray *rects = nil;
    if (heightForUpDown < widthForLeftRight) {
        rects = [[rectsForUpDown copy] autorelease];
    } else {
        rects = [[rectsForLeftRight copy] autorelease];
    }
    
    [rectsForUpDown release];
    [rectsForLeftRight release];
    
    return rects;
}

#pragma mark - Private methods

+ (double)normalizedAngle:(double)angle {
    const double CV_HALF_PI = CV_PI / 2.0;
    const double CV_QUARTER_PI = CV_PI / 4.0;
    
    // Normalize angle into [0, PI)
    if (angle < 0.0) {
        angle += CV_PI;
    } else if (angle == CV_PI) {
        angle = 0.0;
    }
    
    // Normalize angle into [0, PI/2)
    if (angle >= CV_HALF_PI) {
        angle -= CV_HALF_PI;
    }
    
    // Normalize angle into (-PI/4, PI/4]
    if (angle >= CV_QUARTER_PI) {
        angle -= CV_HALF_PI;
    }
    
    return angle;
}

@end
