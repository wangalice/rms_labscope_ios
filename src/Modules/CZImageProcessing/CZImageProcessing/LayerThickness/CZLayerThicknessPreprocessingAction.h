//
//  CZLayerThicknessPreprocessingAction.h
//  Matscope
//
//  Created by Halley Gu on 12/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;
@class CZLayerThicknessDetector;
@class CZImageTool;

@protocol CZLayerThicknessPreprocessingActionDelegate <NSObject>

- (void)layerThicknessPreprocessingActionDidFinishSuccessfully;
- (void)layerThicknessPreprocessingActionDidFinishWithError:(NSError *)error;

@end

@interface CZLayerThicknessPreprocessingAction : NSObject <CZUndoAction>

@property (nonatomic, assign) UIView *progressHUDContainer;

- (id)initWithDetector:(CZLayerThicknessDetector *)detector
          andImageTool:(CZImageTool *)imageTool;

- (void)doActionWithDelegate:(id<CZLayerThicknessPreprocessingActionDelegate>)delegate;

@end
