//
//  CZLayerThicknessDetector.h
//  Matscope
//
//  Created by Halley Gu on 12/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString * const CZLayerThicknessEdgeLayerName;
extern NSString * const CZLayerThicknessErrorDomain;

typedef NS_ENUM(NSUInteger, CZLayerThicknessErrorCode) {
    kCZLayerThicknessGeneralError = 1,
    kCZLayerThicknessScalingError = 2,
    kCZLayerThicknessRotationError = 3
};

@class CZDocManager;

@interface CZLayerThicknessDetector : NSObject

@property (nonatomic, retain, readonly) CZDocManager *docManager;

- (instancetype)init NS_UNAVAILABLE;
- (id)initWithDocManager:(CZDocManager *)docManager;

- (UIImage *)preprocess:(NSError **)error;
- (void)reset;

- (CALayer *)newEdgeLayer;
- (NSArray *)rectsForLineElementFromPoint:(CGPoint)point;

@end
