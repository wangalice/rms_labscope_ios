//
//  CZEditMultichannelAction.m
//  CZImageProcessing
//
//  Created by Li, Junlin on 4/30/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZEditMultichannelAction.h"
#import <CZDocumentKit/CZDocumentKit.h>

@interface CZEditMultichannelAction ()

@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, retain) NSMutableArray<CZImageChannelProperties *> *originalChannelProperties;
@property (nonatomic, retain) NSMutableArray<CZImageChannelProperties *> *currentChannelProperties;

@end

@implementation CZEditMultichannelAction

- (instancetype)initWithDocManager:(CZDocManager *)docManager {
    self = [super init];
    if (self) {
        _docManager = docManager;
        
        _originalChannelProperties = [[NSMutableArray alloc] init];
        _currentChannelProperties = [[NSMutableArray alloc] init];
        for (CZImageBlock *imageBlock in docManager.document.imageBlocks) {
            [_originalChannelProperties addObject:imageBlock.channelProperties];
            [_currentChannelProperties addObject:imageBlock.channelProperties];
        }
    }
    return self;
}

- (void)dealloc {
    [_originalChannelProperties release];
    [_currentChannelProperties release];
    [super dealloc];
}

- (void)setName:(NSString *)channelName forChannelAtIndex:(NSInteger)channelIndex {
    CZImageChannelProperties *channelProperties = self.currentChannelProperties[channelIndex];
    channelProperties = [channelProperties channelPropertiesByReplacingChannelName:channelName];
    [self.currentChannelProperties replaceObjectAtIndex:channelIndex withObject:channelProperties];
    self.docManager.document.imageBlocks[channelIndex].channelProperties = channelProperties;
}

- (void)setColor:(UIColor *)channelColor forChannelAtIndex:(NSInteger)channelIndex {
    CZImageChannelProperties *channelProperties = self.currentChannelProperties[channelIndex];
    channelProperties = [channelProperties channelPropertiesByReplacingChannelColor:channelColor];
    [self.currentChannelProperties replaceObjectAtIndex:channelIndex withObject:channelProperties];
    self.docManager.document.imageBlocks[channelIndex].channelProperties = channelProperties;
    
    // Trick to invalidate image tool
    self.docManager.document = self.docManager.document;
}

- (void)setSelection:(BOOL)channelSelection forChannelAtIndex:(NSInteger)channelIndex {
    CZImageChannelProperties *channelProperties = self.currentChannelProperties[channelIndex];
    channelProperties = [channelProperties channelPropertiesByReplacingChannelSelection:@(channelSelection)];
    [self.currentChannelProperties replaceObjectAtIndex:channelIndex withObject:channelProperties];
    self.docManager.document.imageBlocks[channelIndex].channelProperties = channelProperties;
    
    // Trick to invalidate image tool
    self.docManager.document = self.docManager.document;
}

#pragma mark - CZUndoAction

- (NSUInteger)costMemory {
    CGSize size = self.docManager.document.imageSize;
    return size.width * size.height * 4;
}

- (void)do {
    // Do nothing
}

- (void)undo {
    [self.docManager.document.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        imageBlock.channelProperties = self.originalChannelProperties[index];
    }];
    
    // Trick to invalidate image tool
    self.docManager.document = self.docManager.document;
}

- (void)redo {
    [self.docManager.document.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        imageBlock.channelProperties = self.currentChannelProperties[index];
    }];
    
    // Trick to invalidate image tool
    self.docManager.document = self.docManager.document;
}

- (CZUndoActionType)type {
    return CZUndoActionTypeMultichannel;
}

@end
