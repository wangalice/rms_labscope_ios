//
//  CZEditMultichannelAction.h
//  CZImageProcessing
//
//  Created by Li, Junlin on 4/30/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

@class CZDocManager;

@interface CZEditMultichannelAction : NSObject <CZUndoAction>

- (instancetype)initWithDocManager:(CZDocManager *)docManager;

- (void)setName:(NSString *)channelName forChannelAtIndex:(NSInteger)channelIndex;
- (void)setColor:(UIColor *)channelColor forChannelAtIndex:(NSInteger)channelIndex;
- (void)setSelection:(BOOL)channelSelection forChannelAtIndex:(NSInteger)channelIndex;

@end
