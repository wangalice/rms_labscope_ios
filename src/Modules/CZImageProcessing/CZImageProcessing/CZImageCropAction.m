//
//  CZImageCropAction.m
//  Matscope
//
//  Created by Halley Gu on 1/7/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZImageCropAction.h"
#import <CZToolbox/CZToolbox.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZDocumentKit/CZDocumentKit.h>
#import "CZImageTool.h"

@interface CZImageCropAction ()

@property (nonatomic, assign) CZDocManager *docManager;
@property (nonatomic, assign) CZImageTool *imageTool;
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CGFloat elementScale;
@property (nonatomic, retain) CZImageDocument *originalDocument;

@end

@implementation CZImageCropAction

- (id)initWithDocManager:(CZDocManager *)docManager
               imageTool:(CZImageTool *)imageTool
                 element:(CZElement *)element
                   scale:(CGFloat)scale {
    self = [super init];
    if (self) {
        _docManager = docManager;
        _imageTool = imageTool;
        _element = [element retain];
        _elementScale = scale;
    }
    return self;
}

- (void)dealloc {
    [_originalDocument release];
    [_element release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark CZUndoAction Methods

- (NSUInteger)costMemory {
    // Rough estimation, assuming the UIImage object is 32-bit RGBA.
    CGSize size = self.originalDocument.imageSize;
    return size.width * size.height * 4;
}

- (void)do {
    [self redo];
}

- (void)undo {
    if (self.originalDocument) {
        // TODO: Refactor so that we don't need image tool's reference here.
        [_imageTool.delegate imageTool:_imageTool changedImageSizeTo:self.originalDocument.imageSize];
        self.docManager.document = self.originalDocument;
    }
}

static CGRect ScanImageValidBounds(const void *buff, size_t imageWidth, size_t imageHeight) {
    if (buff == NULL || imageWidth == 0 || imageHeight == 0) {
        return CGRectNull;
    }
    
    size_t left = 0, right = 0, top = 0, bottom = 0;
    size_t rowBytes = imageWidth * 4;
    
    // scan top
    const uint8_t *pLineBase = buff;
    BOOL found = NO;
    for (size_t y = 0; y < imageHeight; y++) {
        const uint8_t *pPixel = pLineBase;
        for (size_t x = 0; x < imageWidth; x++) {
            if (pPixel[3] > 0) {  // check alpha channel
                found = YES;
                break;
            }
            
            pPixel += 4;
        }
        
        if (found) {
            top = y;
            break;
        }
        
        pLineBase += rowBytes;
    }
    if (!found) {
        return CGRectNull;
    }
    
    // scan bottom
    found = NO;
    pLineBase = buff + rowBytes * (imageHeight - 1);
    for (size_t y = imageHeight - 1; y != top; y--) {
        const uint8_t *pPixel = pLineBase;
        for (size_t x = 0; x < imageWidth; x++) {
            if (pPixel[3] > 0) {
                found = YES;
                break;
            }
            
            pPixel += 4;
        }
        
        if (found) {
            bottom = y;
            break;
        }
        
        pLineBase -= rowBytes;
    }
    if (!found) {
        bottom = top;
    }
    
    // scan left
    found = NO;
    pLineBase = buff;
    for (size_t x = 0; x < imageWidth; x++) {
        const uint8_t *pPixel = pLineBase + top * rowBytes;
        for (size_t y = top; y <= bottom; y++) {
            if (pPixel[3] > 0) {
                found = YES;
                break;
            }
            
            pPixel += rowBytes;
        }
        
        if (found) {
            left = x;
            break;
        }
        
        pLineBase += 4;
    }
    if (!found) {
        return CGRectNull;
    }
    
    // scan right
    found = NO;
    pLineBase = buff + 4 * (imageWidth - 1);
    for (size_t x = imageWidth - 1; x != left; x--) {
        const uint8_t *pPixel = pLineBase + top * rowBytes;
        for (size_t y = top; y <= bottom; y++) {
            if (pPixel[3] > 0) {
                found = YES;
                break;
            }
            
            pPixel += rowBytes;
        }
        
        if (found) {
            right = x;
            break;
        }
        
        pLineBase -= 4;
    }
    if (!found) {
        right = left;
    }
    
    CGRect boundary = CGRectMake(left, top, right - left + 1, bottom - top + 1);
    if (boundary.size.width == imageWidth && boundary.size.height == imageHeight) {
        return CGRectNull;
    } else {
        return boundary;
    }
}

- (void)redo {
    if (self.docManager.document) {
        self.originalDocument = self.docManager.document;
        
        UIImage *result = nil;
        
        CZNode *node = self.element.primaryNode;
        if ([node isKindOfClass:[CZNodeShape class]]) {
            CZNodeShape *nodeShape = (CZNodeShape *)node;
            CGPathRef path = [nodeShape relativePath];
            
            CGAffineTransform transformScale = CGAffineTransformMakeScale(self.elementScale, self.elementScale);
            CGPathRef scaledPath = CGPathCreateCopyByTransformingPath(path, &transformScale);
            CGRect bounds = CGRectIntegral(CGPathGetPathBoundingBox(scaledPath));
            
            // Adjust width so that the bytesPerRow can be divided by 64. This
            // is a workaround for GPUImage to correctly show the images.
            int width = bounds.size.width;
            if (width % 16 != 0) {
                if ([self.element isKindOfClass:[CZElementRectangle class]]) {
                    int diff = width % 16;
                    bounds.origin.x += floor(diff / 2.0f);
                    bounds.size.width -= diff;
                } else {
                    int diff = 16 - width % 16;
                    bounds.origin.x -= floor(diff / 2.0f);
                    bounds.size.width += diff;
                }
                
                CZLogv(@"The cropping region has been automatically reduced to have aligned width.");
            }
            
            // Crop the image into the bounding box of the element.
            if (bounds.size.width <= 0) {
                bounds.size.width = 1;
            }
            
            if (bounds.size.height <= 0) {
                bounds.size.height = 1;
            }
            
            if (bounds.size.width >= self.originalDocument.imageSize.width) {
                bounds.size.width = self.originalDocument.imageSize.width;
            }
            
            if (bounds.size.height >= self.originalDocument.imageSize.height) {
                bounds.size.height = self.originalDocument.imageSize.height;
            }
            
            size_t imageWidth = bounds.size.width;
            size_t imageHeight = bounds.size.height;
            void *buff = calloc(4 * imageWidth * imageHeight, 1);
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little;
            CGContextRef context = CGBitmapContextCreate(buff, imageWidth, imageHeight, 8, 4 * imageWidth, colorSpace, bitmapInfo);
            CGContextTranslateCTM(context, 0, imageHeight);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGColorSpaceRelease(colorSpace);
            
            UIGraphicsPushContext(context);
            
            CGContextSetShouldAntialias(context, false);
            
            CGContextTranslateCTM(context, -bounds.origin.x, -bounds.origin.y);
            
            CGContextAddPath(context, scaledPath);
            CFRelease(scaledPath);
            CGContextClip(context);
            
            // If the target shape is rotated, consider the rotation transform
            // while doing cropping.
            
            if ([self.element conformsToProtocol:@protocol(CZElementRotatable)] &&
                self.element.rotateAngle != 0.0) {
                CGPoint center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
                
                CGAffineTransform transform = CGAffineTransformIdentity;
                transform = CGAffineTransformTranslate(transform, center.x, center.y);
                transform = CGAffineTransformRotate(transform, -self.element.rotateAngle);
                transform = CGAffineTransformTranslate(transform, -center.x, -center.y);
                
                CGContextConcatCTM(context, transform);
            }

            [[self.originalDocument outputImage] drawInRect:CGRectMake(0, 0, self.originalDocument.imageSize.width, self.originalDocument.imageSize.height)];
            
            CGRect boundary = ScanImageValidBounds(buff, imageWidth, imageHeight);
            
            CGImageRef imageRef = CGBitmapContextCreateImage(context);
            result = [UIImage imageWithCGImage:imageRef];
            CGImageRelease(imageRef);
            
            UIGraphicsPopContext();
            free(buff);
            CGContextRelease(context);
            
            if (!CGRectIsNull(boundary)) {
                // Adjust width so that the bytesPerRow can be divided by 64. This
                // is a workaround for GPUImage to correctly show the images.
                size_t width = boundary.size.width;
                size_t diff = 16 - width % 16;
                boundary.size.width = width + diff;
                
                UIGraphicsBeginImageContext(boundary.size);
                context = UIGraphicsGetCurrentContext();
                CGContextTranslateCTM(context, -boundary.origin.x, -boundary.origin.y);
                [result drawInRect:CGRectMake(0, 0, imageWidth, imageHeight)];
                result = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
            }
        }
        
        [_imageTool.delegate imageTool:_imageTool changedImageSizeTo:result.size];
        if ([self.element isKindOfClass:[CZElementPolygon class]] || [self.element isKindOfClass:[CZElementCircle class]]) {
            self.docManager.document = [self.originalDocument documentByReplacingImage:result grayscale:NO];
        } else {
            self.docManager.document = [self.originalDocument documentByReplacingImage:result];
        }
    }
}

- (CZUndoActionType)type {
    return CZUndoActionTypeImage;
}

@end
