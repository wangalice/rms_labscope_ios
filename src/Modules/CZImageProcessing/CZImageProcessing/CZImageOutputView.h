//
//  CZImageOutputView.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

@class CZImageOutputView;
@class GPUImageView;

/**
 * Layouts of CZImageOutputView. Currently the amount of image view is fixed to
 * 4 views, and there are two modes of with supplemental views:
 *
 * 1) kImageOutputViewCrossMode
 *        [2]
 *    [1]     [4]
 *        [3]
 *
 * 2) kImageOutputViewGridMode
 *    [ 1 ][ 2 ]
 *    [ 3 ][ 4 ]
 */
typedef NS_ENUM(NSUInteger, CZImageOutputViewMode) {
    kImageOutputViewSingleMode,
    kImageOutputViewCrossMode,
    kImageOutputViewGridMode,
    kImageOutputViewGridModeWithButtons
};

@protocol CZImageOutputViewDelegate <NSObject>

@optional
- (void)imageOutputViewWillShowSupplementalViews:(CZImageOutputView *)view;
- (void)imageOutputViewDidShowSupplementalViews:(CZImageOutputView *)view;
- (void)imageOutputView:(CZImageOutputView *)view didChangeMainViewIndexTo:(NSUInteger)index;
- (void)imageOutputViewWillDismissSupplementalViews:(CZImageOutputView *)view;
- (void)imageOutputViewDidDismissSupplementalViews:(CZImageOutputView *)view;

- (CZShouldBeginTouchState)imageOutputView:(UIView *)image shouldBeginAnnotationTouch:(UITouch *)touch;

- (BOOL)imageOutputView:(CZImageOutputView *)view gesture:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch;
- (void)imageOutputView:(CZImageOutputView *)view handleTapGesture:(UIGestureRecognizer *)gesture;
- (void)imageOutputView:(CZImageOutputView *)view handlePanGesture:(UIGestureRecognizer *)gesture;
- (void)imageOutputView:(CZImageOutputView *)view handleDoubleTapGesture:(UIGestureRecognizer *)gesture;
- (void)imageOutputView:(CZImageOutputView *)view handleSwipeGesture:(UISwipeGestureRecognizer *)gesture;

- (void)imageOutputView:(CZImageOutputView *)view didClickOverlayGearButton:(UIButton *)button;

@end

/*! The inner view of image tool and video tool. */
@interface CZImageOutputView : UIView

@property (nonatomic, assign) id<CZImageOutputViewDelegate> delegate;
@property (nonatomic, assign) BOOL willAutoDismiss;
@property (nonatomic, assign) BOOL animationEnabled;
@property (nonatomic, retain) NSMutableArray *imageViews;
@property (nonatomic, assign, readonly) NSUInteger mainImageViewIndex;
@property (nonatomic, retain) NSMutableArray *imageLabels;
@property (atomic, readonly, assign) BOOL isSupplementalViewsShown;
@property (atomic, retain) NSObject *animationLock;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;

- (instancetype)initWithMainImageViewIndex:(NSUInteger)index;
- (instancetype)initWithMainImageViewIndex:(NSUInteger)index andForcesFourToThree:(BOOL)fourToThree NS_DESIGNATED_INITIALIZER;

- (GPUImageView *)mainImageView;

- (void)showSupplementalViewsWithMode:(CZImageOutputViewMode)mode;
- (void)dismissSupplementalViews;
- (void)dismissSupplementalViewsWithCompleteHandler:(void(^)())completeHandler;

- (void)setLabelsVisible:(BOOL)visible;
- (BOOL)changeMainImageViewIndexTo:(NSUInteger)newIndex;

@end
