//
//  CZShadingCorrectionFilter.h
//  Matscope
//
//  Created by Mike Wang on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageTwoInputFilter.h>

@interface CZShadingCorrectionFilter : GPUImageTwoInputFilter {
    GLint maxXUniform;
    GLint maxYUniform;
    GLint maxZUniform;
}
@property (nonatomic, assign) float maxX;
@property (nonatomic, assign) float maxY;
@property (nonatomic, assign) float maxZ;

@end
