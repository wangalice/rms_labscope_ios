//
//  CZShadingCorrectionFilter.m
//  Matscope
//
//  Created by Mike Wang on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZShadingCorrectionFilter.h"

static NSString *const kCZShadingCorrectionFragmentShaderString = SHADER_STRING
(
 precision highp float;
 
 varying highp vec2 textureCoordinate;
 varying highp vec2 textureCoordinate2;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 uniform float maxX;
 uniform float maxY;
 uniform float maxZ;
 
 void main()
 {
	 mediump vec4 base = texture2D(inputImageTexture, textureCoordinate);
	 mediump vec4 overlay = texture2D(inputImageTexture2, textureCoordinate2);
     overlay.r = overlay.r / maxX;
     overlay.g = overlay.g / maxY;
     overlay.b = overlay.b / maxZ;
          
	 gl_FragColor = vec4(base.r/overlay.r, base.g/overlay.g, base.b/overlay.b, base.a);
 }
 );

@implementation CZShadingCorrectionFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kCZShadingCorrectionFragmentShaderString])) {
		return nil;
    }
    
    maxXUniform = [filterProgram uniformIndex:@"maxX"];
    maxYUniform = [filterProgram uniformIndex:@"maxY"];
    maxZUniform = [filterProgram uniformIndex:@"maxZ"];
    
    self.maxX = 1.f;
    self.maxY = 1.f;
    self.maxZ = 1.f;
    
    return self;
}

- (void)setMaxX:(float)maxX {
    _maxX = maxX;
    [self setFloat:_maxX forUniform:maxXUniform program:filterProgram];
}

- (void)setMaxY:(float)maxY {
    _maxY = maxY;
    [self setFloat:_maxY forUniform:maxYUniform program:filterProgram];
}

- (void)setMaxZ:(float)maxZ {
    _maxZ = maxZ;
    [self setFloat:_maxZ forUniform:maxZUniform program:filterProgram];
}

@end


