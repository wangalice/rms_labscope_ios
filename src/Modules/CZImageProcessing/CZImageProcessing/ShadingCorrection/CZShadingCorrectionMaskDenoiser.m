//
//  CZShadingCorrectionMaskDenoiser.m
//  CZImageProcessing
//
//  Created by Li, Junlin on 7/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZShadingCorrectionMaskDenoiser.h"
#import "CZRGBYUVFilter.h"
#import <GPUImage/GPUImage.h>
#import <CZToolbox/CZToolbox.h>

@implementation CZShadingCorrectionMaskDenoiser

- (UIImage *)denoiseShadingCorrectionMaskImage:(UIImage *)maskImage {
    GPUImagePicture *maskPicture = nil;
    @autoreleasepool {
        maskPicture = [[GPUImagePicture alloc] initWithImage:maskImage];
    }
    
    // Remove noise from original image
    GPUImage3x3ConvolutionFilter *denoiseFilter = [[GPUImage3x3ConvolutionFilter alloc] init];
    [denoiseFilter setConvolutionKernel:(GPUMatrix3x3){
        {1.0/9.0f,  1.0/9.0f, 1.0/9.0f},
        {1.0/9.0f, 1.0/9.0f, 1.0/9.0f},
        {1.0/9.0f,  1.0/9.0f, 1.0/9.0f}
    }];
    [denoiseFilter forceProcessingAtSize:maskImage.size];
    [maskPicture addTarget:denoiseFilter];
    [denoiseFilter useNextFrameForImageCapture];
    [maskPicture processImage];
    
    UIImage *denoisedImage = [denoiseFilter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
    
    [denoiseFilter release];
    [maskPicture release];
    
    return denoisedImage;
}

- (NSArray *)maxMaskDataArrayFromDenoisedImage:(UIImage *)image {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    // Change RGB to YUV
    GPUImagePicture *deRGBYUVmaskPicture = nil;
    @autoreleasepool {
        deRGBYUVmaskPicture = [[GPUImagePicture alloc] initWithImage:image];
    }
    CZRGBYUVFilter *deNoiseMaskRGBToYUV = [[CZRGBYUVFilter alloc] init];
    [deNoiseMaskRGBToYUV forceProcessingAtSize:image.size];
    [deRGBYUVmaskPicture addTarget:deNoiseMaskRGBToYUV];
    [deNoiseMaskRGBToYUV useNextFrameForImageCapture];
    [deRGBYUVmaskPicture processImage];
    
    UIImage *deNoiseImage = [deNoiseMaskRGBToYUV imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
    [deNoiseMaskRGBToYUV release];
    [deRGBYUVmaskPicture release];
    
    CGBitmapInfo deNoiseBitmapInfo = CGImageGetBitmapInfo(deNoiseImage.CGImage);
    CGImageAlphaInfo deNoiseAlphaInfo = deNoiseBitmapInfo & kCGBitmapAlphaInfoMask;
    CGBitmapInfo deNoiseOrderInfo = deNoiseBitmapInfo & kCGBitmapByteOrderMask;
    
    CGImageRef deNoiseImageRef = CGImageRetain([deNoiseImage CGImage]);
    NSUInteger deNoiseImageWidth = CGImageGetWidth(deNoiseImageRef);
    NSUInteger deNoiseImageHeight = CGImageGetHeight(deNoiseImageRef);
    
    CGDataProviderRef deNoiseDataProviderRef = CGImageGetDataProvider(deNoiseImageRef);
    CFDataRef deNoiseDataRef = CGDataProviderCopyData(deNoiseDataProviderRef);
    const uint8_t *deNoiseRawData = CFDataGetBytePtr(deNoiseDataRef);
    NSUInteger deNoiseBytesPerRow = CGImageGetBytesPerRow(deNoiseImageRef);
    
    const int offset = deNoiseAlphaInfo == kCGImageAlphaNone ? 3 : 4;
    int yIndex, uIndex, vIndex;
    if (deNoiseOrderInfo == kCGBitmapByteOrder32Little) {
        yIndex = 2; uIndex = 1; vIndex = 0;
        if (deNoiseAlphaInfo == kCGImageAlphaPremultipliedLast ||
            deNoiseAlphaInfo == kCGImageAlphaNoneSkipLast ||
            deNoiseAlphaInfo == kCGImageAlphaLast) {
            yIndex++; uIndex++; vIndex++;
        }
    } else {  // kCGBitmapByteOrderDefault or kCGBitmapByteOrder32Big
        yIndex = 0; uIndex = 1; vIndex = 2;
        if (deNoiseAlphaInfo == kCGImageAlphaPremultipliedFirst ||
            deNoiseAlphaInfo == kCGImageAlphaNoneSkipFirst ||
            deNoiseAlphaInfo == kCGImageAlphaFirst) {
            yIndex++; uIndex++; vIndex++;
        }
    }
    
    // Find the pixel that has the most intensive light
    CGFloat maxL = 0.0;
    NSUInteger positionX = 0;
    NSUInteger positionY = 0;
    const NSUInteger centerX = deNoiseImageWidth / 2;
    const NSUInteger centerY = deNoiseImageHeight / 2;
    const uint8_t *pLine = deNoiseRawData;
    
    for (int j = 0; j < deNoiseImageHeight; j++) {
        const uint8_t *pPixel = pLine;
        for (int i = 0; i < deNoiseImageWidth; i++) {
            if (maxL < pPixel[yIndex]) {
                maxL = pPixel[yIndex];
                positionX = i;
                positionY = j;
            } else if (maxL == pPixel[yIndex]) {
                NSUInteger originalDistance = (positionX - centerX) * (positionX - centerX) + (positionY - centerY) * (positionY - centerX);
                NSUInteger distance = (i - centerX) * (i - centerX) + (j - centerY) * (j - centerX);
                if (distance < originalDistance) {
                    positionX = i;
                    positionY = j;
                }
            }
            
            pPixel += offset;
        }
        pLine += deNoiseBytesPerRow;
    }
    
    // Calculate the average light and color around the most intensive light pixel
    const int avgWidth = 15;
    
    if (positionX < avgWidth) {
        positionX = avgWidth;
    } else if (positionX > (deNoiseImageWidth - avgWidth - 1)) {
        positionX = deNoiseImageWidth - avgWidth - 1;
    }
    
    if (positionY < avgWidth) {
        positionY = avgWidth;
    } else if (positionY > (deNoiseImageHeight - avgWidth - 1)) {
        positionY = deNoiseImageHeight - avgWidth - 1;
    }
    
    CZLogv(@"%lu", (unsigned long)positionX);
    CZLogv(@"%lu", (unsigned long)positionY);
    
    long startPositionY = MAX(0, positionY - avgWidth);
    long endPositionY = MIN(deNoiseImageHeight - 1, positionY + avgWidth);
    long startPositionX = MAX(0, positionX - avgWidth);
    long endPositionX = MIN(deNoiseImageWidth - 1, positionX + avgWidth);
    const long avgWidthSquare = (endPositionX - startPositionX + 1) * (endPositionY - startPositionY + 1);
    
    pLine = deNoiseRawData + startPositionY * deNoiseBytesPerRow;
    
    CGFloat avgY = 0.0;
    CGFloat avgU = 0.0;
    CGFloat avgV = 0.0;
    for (long j = startPositionY; j <= endPositionY; j++) {
        const uint8_t *pPixel = pLine + startPositionX * offset;
        for (long i = startPositionX; i <= endPositionX; i++) {
            avgY += pPixel[yIndex];
            avgU += pPixel[uIndex];
            avgV += pPixel[vIndex];
            pPixel += offset;
        }
        pLine += deNoiseBytesPerRow;
    }
    CGFloat maxMaskX = avgY / avgWidthSquare;
    CGFloat maxMaskY = avgU / avgWidthSquare;
    CGFloat maxMaskZ = avgV / avgWidthSquare;
    
    maxMaskX /= 255.f;
    maxMaskY /= 255.f;
    maxMaskZ /= 255.f;
    
    maxMaskX = maxMaskX == 0.0 ? 1 : maxMaskX;
    maxMaskY = maxMaskY == 0.0 ? 1 : maxMaskY;
    maxMaskZ = maxMaskZ == 0.0 ? 1 : maxMaskZ;
    
    [tempArray addObject:@(maxMaskX)];
    [tempArray addObject:@(maxMaskY)];
    [tempArray addObject:@(maxMaskZ)];
    
    CFRelease(deNoiseDataRef);
    CGImageRelease(deNoiseImageRef);
    
    NSArray *maxMaskData = [NSArray arrayWithArray:tempArray];
    [tempArray release];
    
    return maxMaskData;
}

@end
