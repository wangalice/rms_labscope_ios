//
//  CZShadingCorrectionTool.h
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>

@interface CZShadingCorrectionTool : NSObject

- (instancetype)initWithShadingMaskFile:(NSString *)shadingMaskFile
                               maxMaskX:(float)maxMaskX
                               maxMaskY:(float)maxMaskY
                               maxMaskZ:(float)maxMaskZ
                              grayscale:(BOOL)grayscale;

- (UIImage *)shadingCorrectedImageFrom:(UIImage *)sourceImage;

- (GPUImageOutput<GPUImageInput> *)compoundFilter;

@end
