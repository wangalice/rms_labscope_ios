//
//  CZShadingCorrectionMaskDenoiser.h
//  CZImageProcessing
//
//  Created by Li, Junlin on 7/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZShadingCorrectionMaskDenoiser : NSObject

- (UIImage *)denoiseShadingCorrectionMaskImage:(UIImage *)maskImage;

- (NSArray *)maxMaskDataArrayFromDenoisedImage:(UIImage *)image;

@end
