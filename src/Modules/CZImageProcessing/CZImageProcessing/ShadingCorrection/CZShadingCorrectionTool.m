//
//  CZShadingCorrectionTool.m
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZShadingCorrectionTool.h"
#import <CZToolbox/CZToolbox.h>
#import <GPUImage/GPUImage.h>
#import "CZShadingCorrectionFilter.h"
#import "CZRGBHSLFilter.h"
#import "CZHSLRGBFilter.h"
#import "CZRGBYUVFilter.h"
#import "CZYUVRGBFilter.h"
#import "CZImageTool.h"

@interface CZShadingCorrectionTool () {
    float _maxMaskX;
    float _maxMaskY;
    float _maxMaskZ;
    BOOL _grayscale;
}

@property (nonatomic, copy) NSString *shadingMaskFile;

@end

@implementation CZShadingCorrectionTool

- (instancetype)initWithShadingMaskFile:(NSString *)shadingMaskFile
                               maxMaskX:(float)maxMaskX
                               maxMaskY:(float)maxMaskY
                               maxMaskZ:(float)maxMaskZ
                              grayscale:(BOOL)grayscale {
    self = [super init];
    if (self) {
        self.shadingMaskFile = shadingMaskFile;
        _maxMaskX = maxMaskX;
        _maxMaskY = maxMaskY;
        _maxMaskZ = maxMaskZ;
        _grayscale = grayscale;
    }
    
    return self;
}

- (void)dealloc {
    [_shadingMaskFile release];
    
    [super dealloc];
}

//TODO:need to remove this since in the future HSL filter is prefered
//Currently, compoundFilter uses YUV filter
- (GPUImageOutput<GPUImageInput> *)compoundFilter {
    if (!_shadingMaskFile) {
        return nil;
    }
    
    NSMutableArray *filters = [[NSMutableArray alloc] init];
    
    if (_grayscale) {
        GPUImageGrayscaleFilter *grayscaleFilter = [[GPUImageGrayscaleFilter alloc] init];
        [filters addObject:grayscaleFilter];
        [grayscaleFilter release];
    }
    
    //setup sample filter pipline
    CZRGBYUVFilter *pictureRGBToYUV = [[CZRGBYUVFilter alloc] init];
    [filters addObject:pictureRGBToYUV];
    [pictureRGBToYUV release];
    
    CZShadingCorrectionFilter *divideBlendFilter = [[CZShadingCorrectionFilter alloc] init];
    divideBlendFilter.maxX = _maxMaskX;
    divideBlendFilter.maxY = _maxMaskY;
    divideBlendFilter.maxZ = _maxMaskZ;
    
    [filters addObject:divideBlendFilter];
    
    CZYUVRGBFilter *outputYUVtoRGB = [[CZYUVRGBFilter alloc] init];
    [filters addObject:outputYUVtoRGB];
    [outputYUVtoRGB release];
    
    GPUImageOutput<GPUImageInput> *compoundFilter = [CZImageTool filterFromArray:filters];
    [filters release];
    
    GPUImagePicture *maskPicture = nil;
    
    //setup mask filter pipline
    CGSize maskImageSize = CGSizeZero;
    @autoreleasepool {
        UIImage *maskImage = [[UIImage alloc] initWithContentsOfFile:_shadingMaskFile];
        if (maskImage) {
            maskImageSize = maskImage.size;
            maskPicture = [[GPUImagePicture alloc] initWithImage:maskImage];
        }
        else {
            CZLogv(@"Failed to read Shading Correction reference image!");
            [divideBlendFilter release];
            return nil;
        }
        
        [maskImage release];
    }
    
    NSMutableArray *maskFilters = [[NSMutableArray alloc] init];
    
    if (_grayscale) {
        GPUImageGrayscaleFilter *maskGrayscaleFilter = [[GPUImageGrayscaleFilter alloc] init];
        [maskFilters addObject:maskGrayscaleFilter];
        [maskGrayscaleFilter release];
    }
    
    CZRGBYUVFilter *maskRGBToYUV = [[CZRGBYUVFilter alloc] init];
    [maskFilters addObject:maskRGBToYUV];
    [maskRGBToYUV release];
    
    GPUImageOutput<GPUImageInput> *maskCompoundFilter = [CZImageTool filterFromArray:maskFilters];
    [maskFilters release];
    
    [maskPicture addTarget:maskCompoundFilter];
    [maskCompoundFilter addTarget:divideBlendFilter];
    
    // Optimization for GPUImage.
    [maskCompoundFilter forceProcessingAtSize:maskImageSize];
    
    [maskPicture processImage];
    
    [maskPicture release];
    [divideBlendFilter release];
    
    return compoundFilter;
}

- (UIImage *)shadingCorrectedImageFrom:(UIImage *)sourceImage {
    GPUImagePicture *gpuImageSource = nil;
    @autoreleasepool {
        gpuImageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    }
    
    GPUImageOutput<GPUImageInput> *filter = [self compoundFilter];
    if (!filter) {
        [gpuImageSource release];
        return nil;
    }
    
    [filter forceProcessingAtSize:sourceImage.size];
    [gpuImageSource addTarget:filter];
    [filter useNextFrameForImageCapture];
    [gpuImageSource processImage];
    [gpuImageSource release];

    return [filter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
}

@end
