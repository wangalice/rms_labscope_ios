//
//  CZOverExposeFilter.m
//  Matscope
//
//  Created by Mike Wang on 11/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZOverExposeFilter.h"

static NSString *const kCZOverExposeFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 uniform highp float threshold;
 
 const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);
 
 void main()
 {
     highp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     highp float luminance = dot(textureColor.rgb, W);
     if (luminance >= threshold) {
         gl_FragColor = vec4(vec3(1.0, 0.0, 0.0), 1.0);
     } else {
         gl_FragColor = textureColor;
     }
 }
 );


@implementation CZOverExposeFilter
@synthesize threshold = _threshold;

#pragma mark -
#pragma mark Initialization

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kCZOverExposeFragmentShaderString]))
    {
		return nil;
    }
    
    thresholdUniform = [filterProgram uniformIndex:@"threshold"];
    self.threshold = 0.98f;
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (void)setThreshold:(float)newValue;
{
    _threshold = newValue;
    
    [self setFloat:_threshold forUniform:thresholdUniform program:filterProgram];
}

@end

