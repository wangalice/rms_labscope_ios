#import "CZRGBYUVFilter.h"

NSString *const kCZRGBYUVFragmentShaderString = SHADER_STRING
(
 precision highp float;
 
 varying highp vec2 textureCoordinate;

 uniform sampler2D inputImageTexture;
 
 void main()
 {
     vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     float Y = 0.299 * textureColor.r + 0.587 * textureColor.g + 0.114 * textureColor.b;
     float UX = 0.492 * (textureColor.b - Y);
     float U = (0.436 + UX) / (0.436*2.0);
     float VX = 0.877 * (textureColor.r - Y);
     float V = (0.615 + VX) / (0.615*2.0);
     
     gl_FragColor = vec4(Y, U, V, textureColor.a);
 }
 );

@implementation CZRGBYUVFilter



- (id)init
{
    if (!(self = [super initWithFragmentShaderFromString:kCZRGBYUVFragmentShaderString])) {
		return nil;
    }
    
    return self;
}


@end
