//
//  CZHSLRGBFilter.h
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilter.h>

@interface CZHSLRGBFilter : GPUImageFilter

@end
