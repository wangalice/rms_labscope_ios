//
//  CZColorProcessingFilter.h
//  Matscope
//
//  Created by Ralph Jin on 12/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilter.h>

@interface CZColorProcessingFilter : GPUImageFilter {
    GLint gammaUniform;
    GLint contrastUniform;
    GLint brightnessUniform;
    GLint saturationUniform;
}

/** Gamma ranges from 0.0 to 3.0, with 1.0 as the normal level
 */
@property(readwrite, nonatomic) CGFloat gamma;

/** Contrast ranges from 0.0 to 4.0 (max contrast), with 1.0 as the normal level
 */
@property(readwrite, nonatomic) CGFloat contrast;

/** Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
 */
@property(readwrite, nonatomic) CGFloat brightness;

/** Saturation ranges from 0.0 (fully desaturated) to 2.0 (max saturation), with 1.0 as the normal level
 */
@property(readwrite, nonatomic) CGFloat saturation;

@end
