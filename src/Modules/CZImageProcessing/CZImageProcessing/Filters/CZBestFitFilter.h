//
//  CZBestFitFilter.h
//  Matscope
//
//  Created by Jin Ralph on 12/3/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>

@interface CZBestFitFilter : GPUImageFilterGroup
@end
