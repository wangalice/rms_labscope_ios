//
//  CZHistogramEqualizationFilter.h
//  Hermes
//
//  Created by Halley Gu on 8/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GPUImage/GPUImage.h>

@interface CZHistogramEqualizationFilter : GPUImageFilter

+ (void)clearMinMaxHistory;

- (id)initWithImageSize:(CGSize)size usesHistory:(BOOL)usesHistory;

@end