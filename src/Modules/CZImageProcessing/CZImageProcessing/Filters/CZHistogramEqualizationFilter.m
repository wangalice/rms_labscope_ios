//
//  CZHistogramEqualizationFilter.m
//  Hermes
//
//  Created by Halley Gu on 8/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZHistogramEqualizationFilter.h"
#import <Accelerate/Accelerate.h>
#import <CZToolbox/CZToolbox.h>

static const size_t kDefaultHistoryMaxCount = 25;
static int minValueHistory[kDefaultHistoryMaxCount];
static int maxValueHistory[kDefaultHistoryMaxCount];
static int historyCount;
static int historyIndex;

@interface CZHistogramEqualizationFilter () {
    BOOL _usesHistory;
}

@property (nonatomic, retain) GPUImageRawDataOutput *rawOutput;
@property (nonatomic, retain) NSMutableArray *rawInputTargets;
@property (nonatomic, retain) NSMutableArray *rawInputTextureLocations;
@property (nonatomic, assign) CGSize targetSize;

@end

@implementation CZHistogramEqualizationFilter

+ (void)clearMinMaxHistory {
    @synchronized (self) {
        historyCount = 0;
        historyIndex = 0;
    }
}

- (id)initWithImageSize:(CGSize)size usesHistory:(BOOL)usesHistory {
    self = [super init];
    if (self) {
        _usesHistory = usesHistory;
        
        _rawInputTargets = [[NSMutableArray alloc] init];
        _rawInputTextureLocations = [[NSMutableArray alloc] init];
        
        _rawOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:size
                                                  resultsInBGRAFormat:YES];
        
        _targetSize = size;

        const NSUInteger kImageWidth = size.width;
        const NSUInteger kImageHeight = size.height;
        
        __block CZHistogramEqualizationFilter *selfInBlock = [self retain];
        
        [_rawOutput setNewFrameAvailableBlock:^{
            [selfInBlock.rawOutput lockFramebufferForReading];
            
            GLubyte *bytes = [selfInBlock.rawOutput rawBytesForImage];
            NSInteger bytesPerRow = [selfInBlock.rawOutput bytesPerRowInOutput];
            
            BOOL worksOnBufferDirectly = YES;
            NSInteger realBytesPerRow = kImageWidth * 4;
            if (realBytesPerRow < bytesPerRow) {
                // When bytes per row is not the same as 4 times of width,
                // GPUImage used fast texture upload to get pixel buffer, which
                // will cause incorrect result after re-render with raw data
                // input filter. Need to remove useless pixels from byte buffer
                // before any processing.
                worksOnBufferDirectly = NO;
                
                GLubyte *newBytes = malloc(kImageHeight * realBytesPerRow);
                GLubyte *rowBytes = bytes;
                GLubyte *newRowBytes = newBytes;
                for (size_t row = 0; row < kImageHeight; ++row) {
                    memcpy(newRowBytes, rowBytes, realBytesPerRow);
                    rowBytes += bytesPerRow;
                    newRowBytes += realBytesPerRow;
                    
                    if ([self shouldStopProcessing]) {
                        free(newBytes);
                        [selfInBlock.rawOutput unlockFramebufferAfterReading];
                        return;
                    }
                }
                
                bytes = newBytes;
                bytesPerRow = realBytesPerRow;
            }
            
            vImage_Error error;
            vImage_Buffer source = {bytes, kImageHeight, kImageWidth, bytesPerRow};
            
            vImagePixelCount threshold = floor(1e-4 * size.width * size.height);
            
            do {
                vImagePixelCount histogramA[256];
                vImagePixelCount histogramR[256];
                vImagePixelCount histogramG[256];
                vImagePixelCount histogramB[256];
                vImagePixelCount *histograms[4] = {histogramB, histogramG, histogramR, histogramA};
                
                error = vImageHistogramCalculation_ARGB8888(&source, histograms, 0);
                if (error != kvImageNoError) {
                    CZLogv(@"Failed to calculate histogram.");
                    break;
                }
                
                if ([self shouldStopProcessing]) {
                    break;
                }
                
                int minValue = 256; int maxValue = 0;
                
                for (int channel = 0; channel < 3; ++channel) {
                    int minForChannel = 256, maxForChannel = 0;
                    vImagePixelCount* histogram = histograms[channel];
                    for (int i = 0; i < 256; i++) {
                        if (histogram[i] > threshold) {
                            minForChannel = i;
                            break;
                        }
                    }
                    
                    for (int i = 255; i >= 0; i--) {
                        if (histogram[i] > threshold) {
                            maxForChannel = i + 1;
                            break;
                        }
                    }
                    
                    minValue = MIN(minForChannel, minValue);
                    maxValue = MAX(maxForChannel, maxValue);
                }
                
                if (selfInBlock->_usesHistory) {
                    @synchronized ([CZHistogramEqualizationFilter class]) {
                        minValueHistory[historyIndex] = minValue;
                        maxValueHistory[historyIndex] = maxValue;
                        if (historyCount < kDefaultHistoryMaxCount) {
                            historyCount++;
                        }
                        historyIndex++;
                        if (historyIndex >= kDefaultHistoryMaxCount) {
                            historyIndex = 0;
                        }
                        
                        if (historyCount > 1) {
                            double sum = 0.0;
                            for (int i = 0; i < historyCount; i++) {
                                sum += minValueHistory[i];
                            }
                            minValue = floor(sum / historyCount + 0.5);
                            
                            sum = 0.0;
                            for (int i = 0; i < historyCount; i++) {
                                sum += maxValueHistory[i];
                            }
                            maxValue = floor(sum / historyCount + 0.5);
                        }
                    }
                }
                
                if ([self shouldStopProcessing]) {
                    break;
                }
                
                //CZLogv(@"min = %d, max = %d", minValue, maxValue);
                
                if (maxValue > minValue && !(minValue == 0 && maxValue == 256)) {
                    const int32_t divisor = 256;
                    const int32_t scale = divisor * 256 / (maxValue - minValue);
                    const int16_t matrix[16] = {
                        scale, 0, 0, 0,
                        0, scale, 0, 0,
                        0, 0, scale, 0,
                        0, 0, 0, divisor
                    };
                    const int16_t preBias[4] = {
                        -minValue, -minValue, -minValue, 0
                    };
                    
                    error = vImageMatrixMultiply_ARGB8888(&source,
                                                          &source,
                                                          matrix,
                                                          divisor,
                                                          preBias,
                                                          NULL,
                                                          0);
                    if (error != kvImageNoError) {
                        CZLogv(@"Failed to do histogram equalization.");
                        break;
                    }
                }
            } while (NO);
            
            if (![self shouldStopProcessing]) {
                GPUImageRawDataInput *rawInput = [[GPUImageRawDataInput alloc] initWithBytes:bytes
                                                                                        size:size];
                
                [selfInBlock.rawOutput unlockFramebufferAfterReading];
                [rawInput forceProcessingAtSize:selfInBlock.targetSize];
                
                NSUInteger count = selfInBlock.rawInputTargets.count;
                for (NSUInteger i = 0; i < count; ++i) {
                    id<GPUImageInput> target = selfInBlock.rawInputTargets[i];
                    NSInteger textureLocation = [selfInBlock.rawInputTextureLocations[i] integerValue];
                    
                    [rawInput addTarget:target atTextureLocation:textureLocation];
                }
                
                if (![self shouldStopProcessing]) {
                    [rawInput processData];
                }
                
                [rawInput release];
            } else {
                [selfInBlock.rawOutput unlockFramebufferAfterReading];
            }
            
            if (!worksOnBufferDirectly) {
                free(bytes);
            }
            
            [selfInBlock release];
        }];
        
        NSInteger nextAvailableTextureIndex = [_rawOutput nextAvailableTextureIndex];
        [super addTarget:_rawOutput atTextureLocation:nextAvailableTextureIndex];
    }
    return self;
}

- (void)dealloc {
    [_rawOutput release];
    [_rawInputTargets release];
    [_rawInputTextureLocations release];
    
    [super dealloc];
}

#pragma mark - Overridden methods

- (void)addTarget:(id<GPUImageInput>)newTarget atTextureLocation:(NSInteger)textureLocation {
    [_rawInputTargets addObject:newTarget];
    [_rawInputTextureLocations addObject:[NSNumber numberWithInteger:textureLocation]];
}

- (void)forceProcessingAtSize:(CGSize)frameSize {
    _targetSize = frameSize;
}

#pragma mark - Private methods

- (BOOL)shouldStopProcessing {
    return [UIApplication sharedApplication].applicationState != UIApplicationStateActive;
}

@end
