//
//  CZColorProcessingFilter.m
//  Matscope
//
//  Created by Ralph Jin on 12/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZColorProcessingFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kCZImageColorProcessingFragmentShaderString = SHADER_STRING(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 uniform lowp float gamma;
 uniform lowp float contrast;
 uniform lowp float brightness;
 uniform lowp float saturation;
 
 // Values from "Graphics Shaders: Theory and Practice" by Bailey and Cunningham
 const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);

 void main() {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     // TODO: This condition statement thing is a workaround. Need to find a
     // better way to handle this.
     
     if (textureColor.a == 0.0) {
         gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
     } else {
         // gamma
         textureColor.rgb = pow(textureColor.rgb, vec3(gamma));
         
         // contrast & brightness
         textureColor.rgb = (textureColor.rgb - vec3(0.5)) * contrast + vec3(0.5 + brightness);
         
         // saturation
         lowp float luminance = dot(textureColor.rgb, luminanceWeighting);
         lowp vec3 greyScaleColor = vec3(luminance);
         gl_FragColor = vec4(mix(greyScaleColor, textureColor.rgb, saturation), textureColor.a);
     }
 }
);
#else
#error "Not implemented yet"
#endif

@implementation CZColorProcessingFilter

#pragma mark - Initialization and teardown

- (id)init {
    self = [super initWithFragmentShaderFromString:kCZImageColorProcessingFragmentShaderString];
    if (self == nil) {
		return nil;
    }
    
    gammaUniform = [filterProgram uniformIndex:@"gamma"];
    self.gamma = 1.0;
    
    contrastUniform = [filterProgram uniformIndex:@"contrast"];
    self.contrast = 1.0;
    
    brightnessUniform = [filterProgram uniformIndex:@"brightness"];
    self.brightness = 0.0;
    
    saturationUniform = [filterProgram uniformIndex:@"saturation"];
    self.saturation = 1.0;
    
    return self;
}

#pragma mark - Accessors

- (void)setGamma:(CGFloat)newValue {
    _gamma = newValue;
    [self setFloat:_gamma forUniform:gammaUniform program:filterProgram];
}

- (void)setContrast:(CGFloat)newValue {
    _contrast = newValue;
    [self setFloat:_contrast forUniform:contrastUniform program:filterProgram];
}

- (void)setBrightness:(CGFloat)newValue {
    _brightness = newValue;
    [self setFloat:_brightness forUniform:brightnessUniform program:filterProgram];
}

- (void)setSaturation:(CGFloat)newValue {
    _saturation = newValue;
    [self setFloat:_saturation forUniform:saturationUniform program:filterProgram];
}

@end
