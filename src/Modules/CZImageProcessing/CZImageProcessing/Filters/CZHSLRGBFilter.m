//
//  CZHSLRGBFilter.m
//  Matscope
//
//  Created by Mike Wang on 10/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZHSLRGBFilter.h"

NSString *const kGPUImageHSLRGBFragmentShaderString = SHADER_STRING
(
     precision highp float;
     varying highp vec2 textureCoordinate;
     uniform sampler2D inputImageTexture;
     
     float fEqual(float x, float y)
     {
         if (x+0.0000001 > y && x-0.0000001 < y) {
             return 1.0;
         } else {
             return 0.0;
         }
     }
 
     void main (void)
     {
         mediump vec4 base = texture2D(inputImageTexture, textureCoordinate);
         
         float r = 0.0;
         float g = 0.0;
         float b = 0.0;
         
         float h = base.r*360.0*2.0 - 360.0;
         float s = base.g * 2.0;
         float l = base.b * 2.0;
         
         //calculate chroma
         float chroma = (1.0 - abs(2.0*l - 1.0)) * s;
         h /= 60.0;
         float x = chroma * (1.0 - abs(mod(h, 2.0) - 1.0 ));
         
         if (h >= 0.0 && h < 1.0) {
             r = chroma;
             g = x;
             b = 0.0;
         } else if (h >= 1.0 && h < 2.0) {
             r = x;
             g = chroma;
             b = 0.0;
         } else if (h >= 2.0 && h < 3.0) {
             r = 0.0;
             g = chroma;
             b = x;
         } else if (h >= 3.0 && h < 4.0) {
             r = 0.0;
             g = x;
             b = chroma;
         } else if (h >= 4.0 && h < 5.0) {
             r = x;
             g = 0.0;
             b = chroma;
         } else if (h >= 5.0 && h < 6.0) {
             r = chroma;
             g = 0.0;
             b = x;
         } else {
             r = 0.0;
             g = 0.0;
             b = 0.0;
         }
         
         float m = l - 0.5*chroma;
         
         r += m;
         g += m;
         b += m;
         
         gl_FragColor = vec4(r, g, b, base.a);
     }
 );

@implementation CZHSLRGBFilter
#pragma mark -
#pragma mark Initialization and teardown
     
 - (id)init {
     if (!(self = [super initWithFragmentShaderFromString:kGPUImageHSLRGBFragmentShaderString])) {
         return nil;
     }
     
     return self;
 }
 
@end
