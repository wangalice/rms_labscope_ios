//
//  CZOverExposeFilter.h
//  Matscope
//
//  Created by Mike Wang on 11/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilter.h>

@interface CZOverExposeFilter : GPUImageFilter
{
    GLint thresholdUniform;
}

/** Anything above this luminance will be red. Ranges from 0.0 to 1.0, with 0.5 as the default
 */
@property(readwrite, nonatomic) float threshold;
@end
