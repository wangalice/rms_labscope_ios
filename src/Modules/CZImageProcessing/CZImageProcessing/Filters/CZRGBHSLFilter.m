//
//  CZRGBHSLFilter.m
//  Matscope
//
//  Created by Mike Wang on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRGBHSLFilter.h"

NSString *const kGPUImageRGBHSLFragmentShaderString = SHADER_STRING
(
    precision highp float;
    varying highp vec2 textureCoordinate;
    uniform sampler2D inputImageTexture;
    uniform int rgbToHSL;
 
 
    float fEqual(float x, float y)
    {
         if (x+0.0000001 > y && x-0.0000001 < y) {
             return 1.0;
         } else {
             return 0.0;
         }
    }
 
     void main (void)
     {
         mediump vec4 base = texture2D(inputImageTexture, textureCoordinate);
         
         //calculate chroma
         float r = base.r;
         float g = base.g;
         float b = base.b;
         
         float maxNum = max(r, g);
         maxNum = max(maxNum, b);
         float minNum = min(r, g);
         minNum = min(minNum, b);
         float chroma = maxNum-minNum; //calculate chroma
         float lightness = (maxNum+minNum)*0.5;
         float saturation = chroma/(1.0-abs(2.0*lightness-1.0));
     
         float hue = 0.0;
         if (fEqual(r, maxNum) == 1.0) {
             hue = mod((g - b)/chroma, 6.0);
         }
         if (fEqual(g, maxNum) == 1.0) {
             hue = ((b - r)/chroma) + 2.0;
         }
         if (fEqual(b, maxNum) == 1.0) {
             hue = ((r - g)/chroma) + 4.0;
         }
         
         hue *= 60.0;
         
         if (maxNum == minNum) {
             hue = 0.0;
             saturation = 0.0;
         }
         
         hue = (hue + 360.0) / (360.0 * 2.0);
         saturation = saturation / 2.0;
         lightness = lightness / 2.0;
     
         gl_FragColor = vec4(hue, saturation, lightness, base.a);
     }
 );

@implementation CZRGBHSLFilter


#pragma mark -
#pragma mark Initialization and teardown

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageRGBHSLFragmentShaderString])) {
		return nil;
    }
    
    return self;
}

@end
