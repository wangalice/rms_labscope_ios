//
//  CZRGBHSLFilter.h
//  Matscope
//
//  Created by Mike Wang on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilter.h>

@interface CZRGBHSLFilter : GPUImageFilter {
    GLint rgbToHSLUniform;
}
@property (nonatomic, assign) int rgbToHSL;
@end
