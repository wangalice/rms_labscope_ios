//
//  CZYUVRGBFilter.h
//  Matscope
//
//  Created by Mike Wang on 10/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage/GPUImageFilter.h>

@interface CZYUVRGBFilter : GPUImageFilter

@end
