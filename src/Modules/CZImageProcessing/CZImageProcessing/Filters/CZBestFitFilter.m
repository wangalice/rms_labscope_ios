//
//  CZBestFitFilter.m
//  Matscope
//
//  Created by Jin Ralph on 12/3/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import "CZBestFitFilter.h"
#import <GPUImage/GPUImageHistogramFilter.h>

@implementation CZBestFitFilter

- (id)init {
    if (!(self = [super init])) {
        return nil;
    }

    // First pass: reduce to luminance
    GPUImageGammaFilter *gammaFilter = [[GPUImageGammaFilter alloc] init]; // add an empty filter, so that histogramFilter can glReadPixels
    [self addFilter:gammaFilter];
    [gammaFilter release];

    GPUImageHistogramFilter *histogramFilter = [[GPUImageHistogramFilter alloc] initWithHistogramType:kGPUImageHistogramLuminance];
    [self addFilter:histogramFilter];
    [histogramFilter release];

    // Second pass: perform color matrix convert
    GPUImageColorMatrixFilter *colorMatrixFilter = [[GPUImageColorMatrixFilter alloc] init];
    [self addFilter:colorMatrixFilter];
    [colorMatrixFilter release];

    [gammaFilter addTarget:histogramFilter];

    __block CZBestFitFilter *weakSelf = self;
    __block GPUImageColorMatrixFilter *weakThreshold = colorMatrixFilter;

    [histogramFilter setFrameProcessingCompletionBlock:^(GPUImageOutput *filter, CMTime frameTime) {
        NSUInteger min = 255, max = 0;
        [weakSelf extractLuminosityAtFrameTime:frameTime min:&min max:&max];
        if (max > min) {
            CGFloat scale = 255.0 / (max - min);
            CGFloat diff = min / 255.0 * scale;

            weakThreshold.colorMatrix = (GPUMatrix4x4){
                {scale, 0.f, 0.f, -diff},
                {0.f, scale, 0.f, -diff},
                {0.f, 0.f, scale, -diff},
                {0.f, 0.f, 0.f, 1.f}
            };
        }
    }];


    self.initialFilters = [NSArray arrayWithObjects:gammaFilter, colorMatrixFilter, nil];
    self.terminalFilter = colorMatrixFilter;

    return self;
}

- (void)extractLuminosityAtFrameTime:(CMTime)frameTime min:(NSUInteger *)min max:(NSUInteger *)max {
    runSynchronouslyOnVideoProcessingQueue(^{
        // we need a normal color texture for this filter
        NSAssert(self.outputTextureOptions.internalFormat == GL_RGBA, @"The output texture format for this filter must be GL_RGBA.");
        NSAssert(self.outputTextureOptions.type == GL_UNSIGNED_BYTE, @"The type of the output texture of this filter must be GL_UNSIGNED_BYTE.");

        if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
            return;
        }

        [GPUImageContext useImageProcessingContext];

        if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
            return;
        }

        [outputFramebuffer activateFramebuffer];

        GLubyte rawImagePixels[256 * 4];
        glReadPixels(0, 1, 256, 1, GL_RGBA, GL_UNSIGNED_BYTE, rawImagePixels);

        BOOL foundMin = NO;
        NSUInteger luminanceMin = 0, luminanceMax = 0;
        NSUInteger byteIndex = 0;
        for (NSUInteger currentPixel = 0; currentPixel < 256; currentPixel++)
        {
            if (rawImagePixels[byteIndex] > 0) {
                if (!foundMin) {
                    luminanceMin = currentPixel;
                    foundMin = YES;
                }

                if (luminanceMax < currentPixel) {
                    luminanceMax = currentPixel;
                }
            }
            byteIndex += 4;
        }
        
        *min = luminanceMin;
        *max = luminanceMax;
    });
}

@end
