//
//  CZYUVRGBFilter.m
//  Matscope
//
//  Created by Mike Wang on 10/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZYUVRGBFilter.h"

NSString *const kCZYUVRGBFragmentShaderString = SHADER_STRING
(
 precision highp float;
 
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 void main()
 {
     vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     float Y = textureColor.r;
     float U = ((textureColor.g * 2.0 * 0.436) - 0.436);
     float V = ((textureColor.b * 2.0 * 0.615) - 0.615);
     
     float R = Y + V / 0.877;
     float G = Y - 0.395*U - 0.581*V;
     float B = Y + U / 0.492;
     
     gl_FragColor = vec4(R, G, B, textureColor.a);
 }
 );

@implementation CZYUVRGBFilter



- (id)init{
    if (!(self = [super initWithFragmentShaderFromString:kCZYUVRGBFragmentShaderString])) {
		return nil;
    }
    
    return self;
}


@end
