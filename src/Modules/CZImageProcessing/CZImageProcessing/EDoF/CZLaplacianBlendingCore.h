//
//  CZLaplacianBlending.hpp
//  Matscope
//
//  Created by Ralph Jin on 12/04/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//
#ifndef CZLaplacianBlendingCore_H
#define CZLaplacianBlendingCore_H

#include "opencv2/opencv.hpp"

#define LAPLACIAN_BLENDING_USE_SHORT 0
#define LAPLACIAN_BLENDING_USE_FLOAT 1

class LaplacianBlendingCore {
public:
#if LAPLACIAN_BLENDING_USE_SHORT
    typedef short pixel_t;
    typedef float mask_pixel_t;
    typedef cv::Mat_<cv::Vec4s> image_t;
#else // use float
    typedef float pixel_t;
    typedef float mask_pixel_t;
    typedef cv::Mat_<cv::Vec4f> image_t;
#endif
    
    typedef cv::Mat_<mask_pixel_t> mask_t;
    typedef cv::vector<image_t> image_array_t;
    typedef cv::vector<mask_t> mask_array_t;

public:
    explicit LaplacianBlendingCore(int _levels);
    ~LaplacianBlendingCore();
    
    cv::Mat_<cv::Vec4b> blend();
    void addImage(cv::Mat_<cv::Vec4b> &image, cv::Mat_<cv::Vec4b> &image2);

private:
    struct ImagePair {
        image_t *image;
        image_t *smallestLevel;
        image_array_t laplacianPyramid;
    };
    
    static void multiplyImage(const image_t &image, const mask_t &mask, image_t &dstImage, bool invertMask);

    void buildMask(cv::Mat_<cv::Vec4b> &image, cv::Mat_<cv::Vec4b> image2);
    void buildGaussianPyramid();
    void buildLaplacianPyramids();
    void buildLaplacianPyramid(ImagePair &pair);
    image_t reconstructImageFromLaplacianPyramid();
    void blendLaplacianPyramid();
    
    void clearImagePairs();
    
    cv::vector<ImagePair> images;
    mask_array_t maskGaussianPyramid;
    
    image_t resultSmallestLevel;
    image_array_t resultLaplacianPyramid;
    int levels;
};

#endif  // CZLaplacianBlendingCore_H