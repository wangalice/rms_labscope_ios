//
//  CZLaplacianBlending.mm
//  Matscope
//
//  Created by Ralph Jin on 12/04/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLaplacianBlending.h"
#import "UIImage+CVMat.h"
#include "CZLaplacianBlendingCore.h"

@interface CZLaplacianBlending ()

@property (nonatomic, readonly, assign) LaplacianBlendingCore *blending;

@end

@implementation CZLaplacianBlending

@synthesize blending = _blending;

- (void)dealloc {
    delete _blending;
    [super dealloc];
}

- (void)addImage:(UIImage *)image anotherImage:(UIImage *)anotherImage {
    cv::Mat_<cv::Vec4b> cvimage1 = image.mat;
    cv::Mat_<cv::Vec4b> cvimage2 = [anotherImage matInSize:image.size];
    self.blending->addImage(cvimage1, cvimage2);
}

- (UIImage *)blend {
    cv::Mat l8u;

    l8u = self.blending->blend();
    delete _blending;
    _blending = NULL;
    
    if (l8u.empty()) {
        return nil;
    }
    
    return [UIImage imageWithMat:l8u];
}

- (LaplacianBlendingCore *)blending {
    if (_blending == NULL) {
        _blending = new LaplacianBlendingCore(4);
    }
    return _blending;
}

@end
