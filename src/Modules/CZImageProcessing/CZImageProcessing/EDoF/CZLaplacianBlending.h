//
//  CZLaplacianBlending.h
//  Matscope
//
//  Created by Ralph Jin on 12/04/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIImage.h>

@interface CZLaplacianBlending : NSObject

/** add 2 images for blending.
 * If the second image's size is not same as the first image,
 * it will resize the second image to the same size.
 */
- (void)addImage:(UIImage *)image anotherImage:(UIImage *)anotherImage;

- (UIImage *)blend;

@end