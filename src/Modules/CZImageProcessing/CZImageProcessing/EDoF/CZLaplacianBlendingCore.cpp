//
//  CZLaplacianBlending.cpp
//  Matscope
//
//  Created by Ralph Jin on 12/04/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZLaplacianBlendingCore.h"
#include <climits>

#if LAPLACIAN_BLENDING_USE_SHORT
static const LaplacianBlendingCore::pixel_t kMaxPixelValue = SHRT_MIN;
//static const LaplacianBlendingCore::pixel_t kMinPixelValue = 0;
#else
static const LaplacianBlendingCore::pixel_t kMaxPixelValue = 1.0f;
//static const LaplacianBlendingCore::pixel_t kMinPixelValue = 0.0f;
#endif

using cv::vector;

template<typename Pointer>
static void inline SafeDelete(Pointer& pt) {
    delete pt;
    pt = NULL;
}

LaplacianBlendingCore::LaplacianBlendingCore(int _levels) : levels(_levels) {
}

LaplacianBlendingCore::~LaplacianBlendingCore() {
    clearImagePairs();
}

cv::Mat_<cv::Vec4b> LaplacianBlendingCore::blend() {
    if (images.size() == 2) {
        buildGaussianPyramid();
        buildLaplacianPyramids();
        blendLaplacianPyramid();
        image_t temp = reconstructImageFromLaplacianPyramid();
        
        cv::Mat_<cv::Vec4b> result;
#if LAPLACIAN_BLENDING_USE_SHORT
        temp.convertTo(result, CV_8U, 255.0 / 32767.0);
#else
        temp.convertTo(result, CV_8U, 255.0);
#endif
        return result;
    } else {
        return image_t();
    }
}

void LaplacianBlendingCore::buildMask(cv::Mat_<cv::Vec4b> &image, cv::Mat_<cv::Vec4b> image2) {
    mask_t anotherMask;
    mask_t blendMask;
    
    // calculate contrast as mask
    {
        cv::Mat_<uchar> tempImage;
        cvtColor(image, tempImage, CV_RGB2GRAY);
        tempImage.convertTo(blendMask, CV_32FC1, 1.0 / 255.0);
        Laplacian(blendMask, blendMask, -1);
        
        cvtColor(image2, tempImage, CV_RGB2GRAY);
        tempImage.convertTo(anotherMask, CV_32FC1, 1.0 / 255.0);
        Laplacian(anotherMask, anotherMask, -1);
    }
    
    // Use hard mask, max wins.
    int rows = blendMask.rows;
    int cols = blendMask.cols;
    for (int i = 0; i < rows; ++i) {
        mask_pixel_t *pRowData1 = blendMask.ptr<mask_pixel_t>(i);
        mask_pixel_t *pRowData2 = anotherMask.ptr<mask_pixel_t>(i);
        
        for (int j = 0; j < cols; ++j) {
            mask_pixel_t v1 = fabs(pRowData1[j]);
            mask_pixel_t v2 = fabs(pRowData2[j]);
            if (v1 >= v2) {
                pRowData1[j] = 1.0;
            } else {
                pRowData1[j] = 0;
            }
        }
    }
    
    maskGaussianPyramid.clear();
    maskGaussianPyramid.push_back(blendMask);
}

void LaplacianBlendingCore::addImage(cv::Mat_<cv::Vec4b> &image, cv::Mat_<cv::Vec4b> &image2) {
    clearImagePairs();
    
    if (image.size() == image2.size()) {
        buildMask(image, image2);
        
        ImagePair pair;
        pair.smallestLevel = new image_t();
        pair.image = new image_t();
#if LAPLACIAN_BLENDING_USE_SHORT
        image.convertTo(*pair.image, CV_16S, 32767.0/255.0);
#else
        image.convertTo(*pair.image, CV_32F, 1.0/255.0);
#endif
        images.push_back(pair);
        
        ImagePair pair2;
        pair2.smallestLevel = new image_t();
        pair2.image = new image_t();
#if LAPLACIAN_BLENDING_USE_SHORT
        image2.convertTo(*pair2.image, CV_16S, 32767.0/255.0);
#else
        image2.convertTo(*pair2.image, CV_32F, 1.0/255.0);
#endif
        images.push_back(pair2);
    }
}

void LaplacianBlendingCore::multiplyImage(const image_t &image, const mask_t& mask, image_t &dstImage, bool invertMask) {
    assert(image.size() == mask.size());
    
    int rows = image.rows;
    int cols = image.cols;
    
    if (dstImage.empty()) {
        dstImage.create(rows, cols);
    } else {
        assert(image.size() == dstImage.size());
    }
    
    if (invertMask) {
        for (int i = 0; i < rows; ++i) {
            const mask_pixel_t *pMask = mask.ptr<mask_pixel_t>(i);
            const pixel_t *pSrcImage = image.ptr<pixel_t>(i);
            pixel_t *pDstImage = dstImage.ptr<pixel_t>(i);
            
            for (int j = 0, k = 0; j < cols; ++j) {
                pDstImage[k] = (pixel_t)(pSrcImage[k] * (1.0 - pMask[j]));  // R
                ++k;
                
                pDstImage[k] = (pixel_t)(pSrcImage[k] * (1.0 - pMask[j]));  // G
                ++k;
                
                pDstImage[k] = (pixel_t)(pSrcImage[k] * (1.0 - pMask[j]));  // B
                ++k;
                
                pDstImage[k] = kMaxPixelValue;  // A
                ++k;
            }
        }
    } else {
        for (int i = 0; i < rows; ++i) {
            const mask_pixel_t *pMask = mask.ptr<mask_pixel_t>(i);
            const pixel_t *pSrcImage = image.ptr<pixel_t>(i);
            pixel_t *pDstImage = dstImage.ptr<pixel_t>(i);
            
            for (int j = 0, k = 0; j < cols; ++j) {
                pDstImage[k] = (pixel_t)(pSrcImage[k] * pMask[j]);  // R
                ++k;
                
                pDstImage[k] = (pixel_t)(pSrcImage[k] * pMask[j]);  // G
                ++k;
                
                pDstImage[k] = (pixel_t)(pSrcImage[k] * pMask[j]);  // B
                ++k;
                
                pDstImage[k] = kMaxPixelValue;  // A
                ++k;
            }
        }
    }
}

void LaplacianBlendingCore::buildGaussianPyramid() {
    assert(maskGaussianPyramid.size() == 1);  // which is created in |buildMasks()|
    
    mask_t currentImg = maskGaussianPyramid.front();  // highest level
    
    for (int l = 0; l < levels; ++l) {
        mask_t _down;
        pyrDown(currentImg, _down);
        maskGaussianPyramid.push_back(_down);
        currentImg = _down;
    }
}

void LaplacianBlendingCore::buildLaplacianPyramids() {
    for (vector<ImagePair>::iterator it = images.begin(); it != images.end(); ++it) {
        ImagePair &pair = *it;
        buildLaplacianPyramid(pair);
    }
}

void LaplacianBlendingCore::buildLaplacianPyramid(ImagePair &pair) {
    pair.laplacianPyramid.clear();
    
    image_t currentImg = *pair.image;
    SafeDelete(pair.image);

    for (int l = 0; l < levels; ++l) {
        image_t down,up;
        pyrDown(currentImg, down);
        pyrUp(down, up, currentImg.size());
        up = currentImg - up;
        pair.laplacianPyramid.push_back(up);
        currentImg = down;
    }
    *pair.smallestLevel = currentImg;
}

LaplacianBlendingCore::image_t LaplacianBlendingCore::reconstructImageFromLaplacianPyramid() {
    image_t currentImg = resultSmallestLevel;
    for (int l = levels - 1; l >= 0; --l) {
        image_t up;
        
        pyrUp(currentImg, up, resultLaplacianPyramid[l].size());
        currentImg = up + resultLaplacianPyramid[l];
    }
    return currentImg;
}

void LaplacianBlendingCore::blendLaplacianPyramid() {
    ImagePair &pair = images[0];
    multiplyImage(*pair.smallestLevel, maskGaussianPyramid.back(), resultSmallestLevel, false);
    SafeDelete(pair.smallestLevel);

    ImagePair &pair2 = images[1];
    multiplyImage(*pair2.smallestLevel, maskGaussianPyramid.back(), *pair2.smallestLevel, true);
    resultSmallestLevel = resultSmallestLevel + *pair2.smallestLevel;
    maskGaussianPyramid.pop_back();
    SafeDelete(pair2.smallestLevel);
    
    for (int l = levels - 1; l >= 0; --l) {
        image_t blendedLevel;
        int i = 0;
        for (vector<ImagePair>::iterator it = images.begin(); it != images.end(); ++it) {
            ImagePair &pair = *it;
            if (i == 0) {
                blendedLevel = pair.laplacianPyramid.back();
                multiplyImage(blendedLevel, maskGaussianPyramid.back(), blendedLevel, false);
            } else {
                multiplyImage(pair.laplacianPyramid.back(), maskGaussianPyramid.back(), pair.laplacianPyramid.back(), true);
                blendedLevel = pair.laplacianPyramid.back() + blendedLevel;
                maskGaussianPyramid.pop_back();
            }
            pair.laplacianPyramid.pop_back();
            ++i;
        }
        
        resultLaplacianPyramid.push_back(blendedLevel);
    }

    clearImagePairs();

    // reverse order, since we pop from back
    for (long l = 0, r = resultLaplacianPyramid.size() - 1; l < r; ++l, --r) {
        image_t temp = resultLaplacianPyramid[l];
        resultLaplacianPyramid[l] = resultLaplacianPyramid[r];
        resultLaplacianPyramid[r] = temp;
    }
}

void LaplacianBlendingCore::clearImagePairs() {
    for (vector<ImagePair>::iterator it = images.begin(); it != images.end(); ++it) {
        ImagePair &pair = *it;
        SafeDelete(pair.image);
        SafeDelete(pair.smallestLevel);
    }
    images.clear();
}