//
//  CZMicroscopeModel+MNA.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

#define kMNAMACAddress @"MNAMACAddress"
#define kMNAIPAddress @"MNAIPAddress"

@class CZMNA;

@protocol CZMicroscopeModelUpdateInfoFromMNADelegate <NSObject>

@required
- (void)microscopeModel:(CZMicroscopeModel *)myModel
          hasUpdateInfo:(CZMicroscopeModel *)queryModel
                fromMNA:(CZMNA *)mna;

- (void)microscopeModel:(CZMicroscopeModel *)myModel
   retrieveNewObjective:(NSUInteger)objective;

@optional
- (void)microscopeModelFailedToRetrieveObjective:(CZMicroscopeModel *)myModel;

@end

@interface CZMicroscopeModel (MNA)

@property (nonatomic, copy) NSString *mnaMACAddress;
@property (nonatomic, copy) NSString *mnaIPAddress;

@property (nonatomic, readonly) BOOL canSupportMNA;
@property (nonatomic, readonly) BOOL isPairedWithMNA;

- (BOOL)hasEqualMNAInfoWithMicroscopeModel:(CZMicroscopeModel *)model;
- (void)saveToMNA:(void (^)(NSError *connectionError))completionHandler;
- (CZMicroscopeModel *)newMicroscopeInfoFromMNA:(CZMNA *)mna;
- (void)newMicroscopeInfoFromMNAAsync:(CZMNA *)mna
                             delegate:(id<CZMicroscopeModelUpdateInfoFromMNADelegate>)delegate;
- (BOOL)retrieveCurrentObjectiveFromMNA;
- (void)retrieveCurrentObjectiveFromMNAAsync:(id<CZMicroscopeModelUpdateInfoFromMNADelegate>)delegate;
- (CZMicroscopeModel *)updateMNAInfoWith:(CZMicroscopeModel *)modelFromMNA;
- (void)updateDataFromMNAModel:(CZMicroscopeModel *)modelFromMNA;
- (void)resetMNA:(void (^)(NSError *connectionError))completionHandler;

@end
