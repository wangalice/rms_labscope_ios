//
//  CZReflector.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 3/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZReflector.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZFilterSet

- (instancetype)initWithMatID:(NSString *)matID {
    return [self initWithMatID:matID filterSetName:nil channelCount:-1];
}

- (instancetype)initWithMatID:(NSString *)matID filterSetName:(NSString *)filterSetName channelCount:(NSInteger)channelCount {
    self = [super init];
    if (self) {
        _matID = [matID copy];
        _filterSetName = [filterSetName copy];
        _channelCount = channelCount;
        
        if (_matID == nil) {
            _filterSetName = [L(@"NONE") retain];
            _displayName = [L(@"NONE") retain];
        } else if (_filterSetName != nil) {
            _displayName = [[NSString alloc] initWithFormat:@"%@ - %@", _matID, _filterSetName];
        }
    }
    return self;
}

- (void)dealloc {
    [_matID release];
    [_filterSetName release];
    [_displayName release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithMatID:self.matID
                                              filterSetName:self.filterSetName
                                               channelCount:self.channelCount];
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZFilterSet class]]) {
        return NO;
    } else {
        return [self isEqualToFilterSet:object];
    }
}

- (BOOL)isEqualToFilterSet:(CZFilterSet *)otherFilterSet {
    return (self.matID == nil && otherFilterSet.matID == nil) || [self.matID isEqualToString:otherFilterSet.matID];
}

- (NSComparisonResult)compare:(CZFilterSet *)otherFilterSet {
    return [self.displayName compare:otherFilterSet.displayName];
}

- (BOOL)isMultiBandpass {
    return self.channelCount > 1;
}

@end

@interface CZReflectorPositionInfo : NSObject <NSCopying>

@property (nonatomic, retain) CZFilterSet *filterSet;

- (instancetype)initWithFilterSet:(CZFilterSet *)filterSet;

- (BOOL)isEqualToPositionInfo:(CZReflectorPositionInfo *)otherPositionInfo;

@end

@implementation CZReflectorPositionInfo

- (instancetype)initWithFilterSet:(CZFilterSet *)filterSet {
    self = [super init];
    if (self) {
        _filterSet = [filterSet retain];
    }
    return self;
}

- (void)dealloc {
    [_filterSet release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithFilterSet:self.filterSet];
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZReflectorPositionInfo class]]) {
        return NO;
    } else {
        return [self isEqualToPositionInfo:object];
    }
}

- (BOOL)isEqualToPositionInfo:(CZReflectorPositionInfo *)otherPositionInfo {
    return [self.filterSet isEqualToFilterSet:otherPositionInfo.filterSet];
}

@end

@interface CZReflector ()

@property (nonatomic, retain) NSMutableArray<CZReflectorPositionInfo *> *positionInfos;

@end

@implementation CZReflector

@synthesize availableFilterSets = _availableFilterSets;

+ (NSString *)filterSetNameForMatID:(NSString *)matID {
    CZReflector *reflector = [[CZReflector alloc] init];
    for (CZFilterSet *filterSet in reflector.availableFilterSets) {
        if ([filterSet.matID isEqualToString:matID]) {
            return filterSet.filterSetName;
        }
    }
    return nil;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _currentPosition = NSNotFound;
        _positionInfos = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_availableFilterSets release];
    [_positionInfos release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZReflector *reflector = [[[self class] allocWithZone:zone] init];
    reflector->_availableFilterSets = [self.availableFilterSets copy];
    reflector->_currentPosition = self.currentPosition;
    reflector->_positionInfos = [[NSMutableArray alloc] initWithArray:self.positionInfos copyItems:YES];
    return reflector;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZReflector class]]) {
        return NO;
    } else {
        return [self isEqualToReflector:object];
    }
}

- (BOOL)isEqualToReflector:(CZReflector *)otherReflector {
    return [self.positionInfos isEqualToArray:otherReflector.positionInfos];
}

- (NSArray<CZFilterSet *> *)availableFilterSets {
    if (_availableFilterSets == nil) {
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        NSURL *url = [bundle URLForResource:@"Reflector" withExtension:@"json"];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url] options:0 error:nil];
        NSDictionary *list = json[@"value"];
        
        NSMutableArray<CZFilterSet *> *mutableFilterSets = [NSMutableArray arrayWithCapacity:list.count + 1];
        
        for (NSDictionary *value in list) {
            NSString *matID = value[@"matID"];
            NSString *matTxt = value[@"matTxt"];
            NSNumber *channelCount = value[@"channelCount"];
            
            CZFilterSet *filterSet = [[CZFilterSet alloc] initWithMatID:matID filterSetName:matTxt channelCount:channelCount.integerValue];
            [mutableFilterSets addObject:filterSet];
            [filterSet release];
        }
        
        [mutableFilterSets sortUsingSelector:@selector(compare:)];
        
        CZFilterSet *none = [[CZFilterSet alloc] initWithMatID:nil];
        [mutableFilterSets insertObject:none atIndex:0];
        [none release];
        
        _availableFilterSets = [mutableFilterSets copy];
    }
    return _availableFilterSets;
}

- (NSUInteger)positions {
    return self.positionInfos.count;
}

- (void)setPositions:(NSUInteger)positions {
    if (positions < self.positionInfos.count) {
        [self.positionInfos removeObjectsInRange:NSMakeRange(positions, self.positionInfos.count - positions)];
    } else if (positions > self.positionInfos.count) {
        for (NSInteger position = self.positionInfos.count; position < positions; position++) {
            CZReflectorPositionInfo *positionInfo = [[CZReflectorPositionInfo alloc] initWithFilterSet:[self availableFilterSets][0]];
            [self.positionInfos addObject:positionInfo];
            [positionInfo release];
        }
    }
    
    if (self.currentPosition >= positions) {
        self.currentPosition = NSNotFound;
    }
}

- (void)setCurrentPosition:(NSUInteger)currentPosition {
    if (currentPosition < self.positions) {
        _currentPosition = currentPosition;
    } else {
        _currentPosition = NSNotFound;
    }
}

- (CZFilterSet *)filterSetAtCurrentPosition {
    return [self filterSetAtPosition:self.currentPosition];
}

- (CZFilterSet *)filterSetAtPosition:(NSUInteger)position {
    if (position < self.positionInfos.count) {
        return self.positionInfos[position].filterSet;
    } else {
        return nil;
    }
}

- (void)setFilterSet:(CZFilterSet *)filterSet atPosition:(NSUInteger)position {
    if (position >= self.positionInfos.count) {
        return;
    }
    
    NSInteger index = [[self availableFilterSets] indexOfObject:filterSet];
    if (index == NSNotFound) {
        index = 0;
    }
    
    self.positionInfos[position].filterSet = [self availableFilterSets][index];
}

@end
