//
//  CZCamera+MicroscopeManagerAdditions.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCamera+MicroscopeManagerAdditions.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZCamera (MicroscopeManagerAdditions)

- (NSArray<NSNumber *> *)supportedMicroscopeModelTypes {
    return @[@(kPrimoStar), @(kPrimotech), @(kPrimovert), @(kStemi305), @(kAxiocamCompound), @(kAxiocamStereo), @(kScanBarCodeModel)];
}

- (NSString *)cameraModelName {
    return kCameraModelNameAxiocamERc5s;
}

- (UIImage *)cameraModelIcon {
    return [UIImage imageNamed:A(@"model-axiocam-erc-5s")];
}

@end

@interface CZGeminiCamera (MicroscopeManagerAdditions)

@end

@implementation CZGeminiCamera (MicroscopeManagerAdditions)

- (NSArray<NSNumber *> *)supportedMicroscopeModelTypes {
    NSString *microscopeName = self.systemInfo.microscope.microscopeName;
    if ([microscopeName hasPrefix:@"Axioscope"]) {
        return @[@(kAxioscope)];
    } else if ([microscopeName hasPrefix:@"Axiolab"]) {
        return @[@(kAxiolab)];
    } else {
        return @[@(kAxiocamCompound), @(kAxiocamStereo), @(kScanBarCodeModel)];
    }
}

- (NSString *)cameraModelName {
    CZGeminiAPICameraModelType cameraModelType = self.systemInfo.camera.modelType;
    switch (cameraModelType) {
        case CZGeminiAPICameraModelTypeUnknown:
            return nil;
        case CZGeminiAPICameraModelTypeAxiocam202:
            return kCameraModelNameAxiocam202;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return kCameraModelNameAxiocam208;
    }
}

- (UIImage *)cameraModelIcon {
    CZGeminiAPICameraModelType cameraModelType = self.systemInfo.camera.modelType;
    switch (cameraModelType) {
        case CZGeminiAPICameraModelTypeUnknown:
            return nil;
        case CZGeminiAPICameraModelTypeAxiocam202:
            return [UIImage imageNamed:A(@"model-axiocam-202")];
        case CZGeminiAPICameraModelTypeAxiocam208:
            return [UIImage imageNamed:A(@"model-axiocam-208")];
    }
}

@end

@interface CZKappaCamera (MicroscopeManagerAdditions)

@end

@implementation CZKappaCamera (MicroscopeManagerAdditions)

- (NSArray<NSNumber *> *)supportedMicroscopeModelTypes {
    return @[@(kPrimoStar), @(kPrimotech), @(kPrimovert), @(kAxiocamCompound), @(kAxiocamStereo), @(kScanBarCodeModel)];
}

- (NSString *)cameraModelName {
    return kCameraModelNameAxiocamERc5s;
}

- (UIImage *)cameraModelIcon {
    return [UIImage imageNamed:A(@"model-axiocam-erc-5s")];
}

@end

@interface CZMoticCamera (SupportedMicroscopeModelTypes)

@end

@implementation CZMoticCamera (MicroscopeManagerAdditions)

- (NSArray<NSNumber *> *)supportedMicroscopeModelTypes {
    return @[@(kStemi305), @(kScanBarCodeModel)];
}

@end

@interface CZWisionCamera (MicroscopeManagerAdditions)

@end

@implementation CZWisionCamera (SupportedMicroscopeModelTypes)

- (NSArray<NSNumber *> *)supportedMicroscopeModelTypes {
    return @[@(kPrimotech), @(kScanBarCodeModel)];
}

@end
