//
//  CZMicroscopeModel.h
//  Hermes
//
//  Created by Mike Wang on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZNosepieceManager.h"
#import "CZZoomLevelSet.h"
#import "CZNosepiece.h"
#import "CZReflector.h"

#define kMicroscopeModelVersionNumber 4
#define kMicroscopeName @"MicroscopeName"
#define kCameraMACAddress @"CameraMAC"
#define kSecurityPIN @"pin"
#define kModelType @"ModelType"
#define kPackage @"Package"
#define kIsDefault @"IsDefault"
#define kDefaultCameraMACAddress @"DefaultCameraMACAddress"
#define kCachedCameraPIN @"CachedCameraPIN"
#define kNULLMACAddress @"00:00:00:00:00:00"
#define kNULLIPAddress @"000.000.000.000"

extern NSString * const CZMicroscopeModelErrorDomain;

extern NSString * const kModelNamePrimotech;
extern NSString * const kModelNamePrimoStar;
extern NSString * const kModelNamePrimovert;
extern NSString * const kModelNameCompound;
extern NSString * const kModelNameStereo;
extern NSString * const kModelNameStemi305;
extern NSString * const kModelNameStemi508;
extern NSString * const kModelNameAxioscope;
extern NSString * const kModelNameAxiolab;
extern NSString * const kModelNameMacroImage;
extern NSString * const kCameraModelNameAxiocamERc5s;
extern NSString * const kCameraModelNameAxiocam202;
extern NSString * const kCameraModelNameAxiocam208;
extern NSString * const kCameraModelNameBuiltIn;

extern NSString * const kNotAvailableMagnification;

extern NSString * const CZZoomKnobDidUpdateNotification;

/** WARNINIG: DON'T change the following order. As these will save into files.*/
typedef NS_ENUM(NSUInteger, CZMicroscopeTypes) {
    kPrimotech = 0,
    kPrimoStar,
    kAxiocamCompound,
    kAxiocamStereo,
    kPrimovert,
    kStemi305,
    kStemi508,
    kAxioscope,
    kAxiolab,
    
    kScanBarCodeModel,
    kNumOfMicroscopeModels,
    kUnknownModelType,
    kMicroscopeTypeDefault = kUnknownModelType,

    //TODO: Advanced features. Pls check it.
//#if PROJECT_MAT
//    kHelpoverlayMicroscopeType = kPrimotech,
//#else
    kHelpoverlayMicroscopeType = kPrimoStar,
//#endif
};

@class CZCamera;
@class CZMicroscopeModel;
@class CZZoomLevel;
@class CZZoomLevelSet;

/** Abstract model class of microscope.
 * Compound microscope can directly derive from this class.
 */
@interface CZMicroscopeModel : NSObject <NSCopying>

@property (atomic, copy) NSString *name;
@property (nonatomic, copy) NSString *pin; // security PIN number
@property (nonatomic, assign) BOOL isDefault;
@property (nonatomic, readonly) BOOL hasObjectivesAssigned;  // TODO:: rename to more generic one

@property (nonatomic, copy) NSString *cachedCameraPIN;
@property (nonatomic, copy) NSString *cameraMACAddress;

@property (nonatomic, readonly, copy) CZNosepiece *nosepiece;

@property (nonatomic, readonly, assign) BOOL hasReflector;
@property (nonatomic, readonly, copy) CZReflector *reflector;

+ (CZMicroscopeModel *)newMicroscopeModelWithCamera:(CZCamera *)camera;
- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel;

+ (NSUInteger)microscopeModelClassCount;

+ (NSString *)modelNameOfType:(CZMicroscopeTypes)type;
+ (UIImage *)modelIconOfType:(CZMicroscopeTypes)type;
+ (NSString *)cameraModelNameOfType:(CZMicroscopeTypes)type;

+ (BOOL)shouldPreferBigUnitByModelName:(NSString *)modelName;

- (BOOL)isEqualToMicroscopeModel:(CZMicroscopeModel *)otherMicroscopeModel;

/** @return type of the microscope. Sub-class shall override this method.*/
- (CZMicroscopeTypes)type;
- (CZMicroscopeModel *)modelBySwitchingToType:(CZMicroscopeTypes)type;
- (NSString *)modelName;
- (UIImage *)modelIcon;
- (NSString *)cameraModelName;

/** @return return JSON format data.*/
- (NSString *)saveToJsonContent;
- (CZMicroscopeModel *)newMicroscopeModelWithJsonContent:(NSString *)content error:(NSError **)error;

- (void)saveToDefaults;
- (void)updateDataFromCamera:(CZCamera *)camera;

- (CZZoomLevelSet *)availableZoomLevelSet;

- (float)magnificationAtPosition:(NSUInteger)position;
- (NSString *)displayMagnificationAtPosition:(NSUInteger)position localized:(BOOL)localized;
- (float)cameraMagnificationAtPosition:(NSUInteger)position;

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position;
- (float)fovWidthAtPosition:(NSUInteger)position;
/** default fov width at position. */
- (float)defaultFOVWidthAtPosition:(NSUInteger)position;
/** @return YES, if fov is not set manually. */
- (BOOL)isDefaultFOVWidthAtPosition:(NSUInteger)position;
- (float)currentFOVWidth;

// other zoom type
- (void)setZoom:(CZZoomLevel *)zoom forType:(NSString *)zoomType;
- (CZZoomLevel *)zoomForType:(NSString *)zoomType;
- (void)removeZoomForType:(NSString *)zoomType;

// additional zoom
- (void)setAdditionalZoom:(float)zoom forType:(NSString *)zoomType;
- (NSNumber *)additionalZoomForType:(NSString *)zoomType;

// defaults values for slots
- (void)saveSlotsToDefaults;
- (void)loadSlotsFromDefaults;
- (void)removeSlotsFromDefaults;

- (void)readFromDictionary:(NSDictionary *)dictionary;
- (void)writeToDictionary:(NSMutableDictionary *)dictionary;

- (void)saveSlotsToDictionary:(NSDictionary *)dictionary;
- (void)loadSlotsFromDictionary:(NSDictionary *)dictionary;

- (void)saveToCamera:(CZCamera *)camera withCompletionHandler:(void (^)(void))completionHandler;

// scaling report parameters
- (void)moveScalingImages;
- (void)deleteTemporarilyScalingImages;
- (void)deleteCachedScalingImages;

// zoom click stops
@property (nonatomic, assign) NSUInteger currentZoomClickStopPosition;
@property (nonatomic, readonly, strong) CZZoomLevel *currentZoomClickStop;
@property (nonatomic, readonly, assign) NSUInteger currentZoomClickStopID;

- (NSUInteger)zoomClickStopPositions;
- (CZZoomLevel *)zoomClickStopAtPosition:(NSUInteger)position;
- (NSUInteger)zoomClickStopIDAtPosition:(NSUInteger)position;

- (void)addZoomClickStop:(CZZoomLevel *)zoomClickStop;
- (void)addZoomClickStopID:(NSUInteger)zoomClickStopID;

- (void)removeZoomClickStop:(CZZoomLevel *)zoomClickStop;
- (void)removeZoomClickStopForID:(NSUInteger)zoomClickStopID;
- (void)removeZoomClickStopAtPosition:(NSUInteger)position;
- (void)removeAllZoomClickStops;

@end

@interface CZMicroscopeModel (WebsiteAddress)

- (NSString *)localedWebsiteAddress;

- (BOOL)hasLocaledMicroShop;
- (NSString *)localedMicroShopAddress;

@end

/** Abstract model class of compound microscope. */
@interface CZMicroscopeModelCompound : CZMicroscopeModel

@end

/** Abstract model class of stereo microscope. */
@interface CZMicroscopeModelStereo : CZMicroscopeModel

+ (NSArray *)zoomLevelsByReplacing1xWithNone:(NSArray *)zoomLevels;

/** Total magnification include eyepieces for stereo. */
- (NSString *)totalMagnificationStringAtPosition:(NSUInteger)position localized:(BOOL)isLocalized;
/** Total magnification include eyepieces for stereo. */
- (float)totalMagnificationAtPosition:(NSUInteger)position;

@end

/** Mock model class for macro images generated by built-in camera. */
@interface CZMicroscopeModelMacroImage : CZMicroscopeModel

@end
