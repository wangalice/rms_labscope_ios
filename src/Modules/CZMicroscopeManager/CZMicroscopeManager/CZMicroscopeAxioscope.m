//
//  CZMicroscopeAxioscope.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAxioscope.h"
#import "CZMicroscopeModelSubclass.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZMicroscopeAxioscope

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kAxioscope];
}

- (CZMicroscopeTypes)type {
    return kAxioscope;
}

@end
