//
//  CZReflector.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 3/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZFilterSet : NSObject <NSCopying>

@property (nonatomic, readonly, copy) NSString *matID;          ///< Like `489090-9110-000`
@property (nonatomic, readonly, copy) NSString *filterSetName;  ///< Like `90 HE DAPI / GFP/ Cy3 / Cy5`
@property (nonatomic, readonly, assign) NSInteger channelCount; ///< Like `4`
@property (nonatomic, readonly, copy) NSString *displayName;    ///< Like `489090-9110-000 - 90 HE DAPI / GFP/ Cy3 / Cy5`
@property (nonatomic, readonly, assign) BOOL isMultiBandpass;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithMatID:(NSString *)matID;
- (instancetype)initWithMatID:(NSString *)matID
                filterSetName:(NSString *)filterSetName
                 channelCount:(NSInteger)channelCount NS_DESIGNATED_INITIALIZER;

- (BOOL)isEqualToFilterSet:(CZFilterSet *)otherFilterSet;
- (NSComparisonResult)compare:(CZFilterSet *)otherFilterSet;

@end

@interface CZReflector : NSObject <NSCopying>

@property (nonatomic, copy) NSArray<CZFilterSet *> *availableFilterSets;    ///< Read from Reflector.json as default
@property (nonatomic, assign) NSUInteger positions;                         ///< Change positions will change position data
@property (nonatomic, assign) NSUInteger currentPosition;                   ///< NSNotFound as default

+ (NSString *)filterSetNameForMatID:(NSString *)matID;

- (BOOL)isEqualToReflector:(CZReflector *)otherReflector;

- (CZFilterSet *)filterSetAtCurrentPosition;
- (CZFilterSet *)filterSetAtPosition:(NSUInteger)position;
- (void)setFilterSet:(CZFilterSet *)filterSet atPosition:(NSUInteger)position;

@end
