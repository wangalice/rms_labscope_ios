//
//  CZZoomLevel.m
//  Labscope
//
//  Created by Ralph Jin on 4/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZZoomLevel.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZZoomLevel

@synthesize name = _name;
@synthesize displayName = _displayName;

- (id)initWithMagnification:(float)magnification {
    return [self initWithMagnification:magnification name:nil displayName:nil];
}

- (id)initWithMagnification:(float)magnification
                       name:(NSString *)name {
    return [self initWithMagnification:magnification name:name displayName:nil];
}

- (id)initWithMagnification:(float)magnification
                       name:(NSString *)name
                displayName:(NSString *)displayName {
    self = [super init];
    if (self) {
        _name = [name copy];
        _magnification = magnification;
        _displayName = [displayName copy];
    }
    
    return self;
}

- (void)dealloc {
    [_name release];
    [_displayName release];
    [super dealloc];
}

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    
    CZZoomLevel *zoomLevel = (CZZoomLevel *)object;
    
    if (_magnification != zoomLevel->_magnification) {
        return NO;
    }
    
    if (_name != zoomLevel->_name &&
        ![_name isEqualToString:zoomLevel->_name]) {
        return NO;
    }
    
    if (_displayName != zoomLevel->_displayName &&
        ![_displayName isEqualToString:zoomLevel->_displayName]) {
        return NO;
    }
    
    return YES;
}

- (NSComparisonResult)compare:(CZZoomLevel *)zoomLevel {
    return [@(_magnification) compare:@(zoomLevel->_magnification)];
}

- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    
    result = prime * result + @(_magnification).hash;
    result = prime * result + _name.hash;
    result = prime * result + _displayName.hash;
    return result;
}

- (NSString *)displayMagnification {
    if (self.magnification > 0) {
        NSString *magnificationString = [CZCommonUtils localizedStringFromFloat:self.magnification precision:2];
        return [magnificationString stringByAppendingString:@"x"];
    } else {
        return L(@"NONE");
    }
}

- (NSString *)name {
    if (_name) {
        return _name;
    } else {
        if (_magnification > 0) {
            NSString *magnificationString = [CZCommonUtils stringFromFloat:_magnification precision:2];
            return [magnificationString stringByAppendingString:@"x"];
        } else {
            return @"";
        }
    }
}

- (NSString *)displayName {
    if (_displayName) {
        return _displayName;
    } else {
        if (_magnification > 0) {
            NSString *magnificationString = [CZCommonUtils localizedStringFromFloat:_magnification precision:2];
            return [magnificationString stringByAppendingString:@"x"];
        } else {
            return L(@"NONE");
        }
    }
}

@end
