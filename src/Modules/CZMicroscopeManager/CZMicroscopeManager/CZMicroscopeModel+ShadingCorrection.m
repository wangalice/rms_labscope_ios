//
//  CZMicroscopeModel+ShadingCorrection.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel+ShadingCorrection.h"
#import "CZMicroscopeModelPrivate.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZShadingCorrectionData

- (id)initWithMaxMaskX:(float)x maxMaskY:(float)y maxMaskZ:(float)z {
    self = [super init];
    if (self) {
        _maxMaskX = x;
        _maxMaskY = y;
        _maxMaskZ = z;
    }
    return self;
}

- (BOOL)isEqualToShadingCorrectionData:(CZShadingCorrectionData *)data {
    return (self.maxMaskX == data.maxMaskX &&
            self.maxMaskY == data.maxMaskY &&
            self.maxMaskZ == data.maxMaskZ);
}

@end

@implementation CZMicroscopeModel (ShadingCorrection)

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position {
    return nil;
}

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *positionKey = [self shadingCorrectionDataKeyForPosition:position];
    if (positionKey == nil) {
        return nil;
    }
    NSString *imageResolutionKey = [NSString stringWithFormat:@"%gx%g", imageResolution.width, imageResolution.height];
    NSString *key = [NSString stringWithFormat:@"%@_%@", positionKey, imageResolutionKey];
    return key;
}

- (BOOL)hasShadingCorrectionDataAtPosition:(NSUInteger)position {
    NSString *positionKey = [self shadingCorrectionDataKeyForPosition:position];
    if (positionKey == nil) {
        return NO;
    }
    NSArray *allKeys = self.shadingCorrectionData.allKeys;
    for (NSString *key in allKeys) {
        if ([key hasPrefix:positionKey]) {
            return YES;
        }
    }
    return NO;
}

- (CZShadingCorrectionData *)shadingCorrectionDataAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *key = [self shadingCorrectionDataKeyForPosition:position imageResolution:imageResolution];
    if (key == nil) {
        return nil;
    }
    return self.shadingCorrectionData[key];
}

- (void)setShadingCorrectionData:(CZShadingCorrectionData *)data atPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *key = [self shadingCorrectionDataKeyForPosition:position imageResolution:imageResolution];
    if (key == nil) {
        return;
    }
    self.shadingCorrectionData[key] = data;
}

- (UIImage *)shadingCorrectionMaskAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *key = [self shadingCorrectionDataKeyForPosition:position imageResolution:imageResolution];
    if (key == nil) {
        return nil;
    }
    return self.shadingCorrectionMask[key];
}

- (void)setShadingCorrectionMask:(UIImage *)mask atPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *key = [self shadingCorrectionDataKeyForPosition:position imageResolution:imageResolution];
    if (key == nil) {
        return;
    }
    self.shadingCorrectionMask[key] = mask;
}

- (NSString *)pathForShadingCorrectionMaskAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution {
    NSString *key =  [self shadingCorrectionDataKeyForPosition:position imageResolution:imageResolution];
    if (key == nil) {
        return nil;
    }
    NSString *name = [NSString stringWithFormat:@"%@_%@.jpg", self.cameraMACAddress, key];
    NSString *path = [[[NSFileManager defaultManager] cz_cachePath] stringByAppendingPathComponent:name];
    return path;
}

- (CZShadingCorrectionData *)currentShadingCorrectionDataWithImageResolution:(CGSize)imageResolution {
    return nil;
}

- (NSString *)currentPathForShadingCorrectionMaskWithImageResolution:(CGSize)imageResolution {
    return nil;
}

- (void)removeShadingCorrectionDataAndMaskAtPosition:(NSUInteger)position {
    NSString *positionKey = [self shadingCorrectionDataKeyForPosition:position];
    if (positionKey == nil) {
        return;
    }
    
    NSArray *dataKeys = self.shadingCorrectionData.allKeys;
    for (NSString *key in dataKeys) {
        if ([key hasPrefix:positionKey]) {
            [self.shadingCorrectionData removeObjectForKey:key];
        }
    }
    
    NSArray *maskKeys = self.shadingCorrectionMask.allKeys;
    for (NSString *key in maskKeys) {
        if ([key hasPrefix:positionKey]) {
            [self.shadingCorrectionMask removeObjectForKey:key];
        }
    }
    
    NSString *pathPrefix = [NSString stringWithFormat:@"%@_%@", self.cameraMACAddress, positionKey];
    NSString *path = [[NSFileManager defaultManager] cz_cachePath];
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    for (NSString *content in contents) {
        if ([content.lastPathComponent hasPrefix:pathPrefix]) {
            [[NSFileManager defaultManager] removeItemAtPath:content error:nil];
        }
    }
}

@end

@interface CZMicroscopeModelCompound (ShadingCorrection)

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position;

@end

@implementation CZMicroscopeModelCompound (ShadingCorrection)

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position {
    if (position >= self.nosepiece.positions) {
        return nil;
    }
    CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
    return objective.matID;
}

- (CZShadingCorrectionData *)currentShadingCorrectionDataWithImageResolution:(CGSize)imageResolution {
    return [self shadingCorrectionDataAtPosition:self.nosepiece.currentPosition imageResolution:imageResolution];
}

- (NSString *)currentPathForShadingCorrectionMaskWithImageResolution:(CGSize)imageResolution {
    return [self pathForShadingCorrectionMaskAtPosition:self.nosepiece.currentPosition imageResolution:imageResolution];
}

@end

@interface CZMicroscopeModelStereo (ShadingCorrection)

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position;

@end

@implementation CZMicroscopeModelStereo (ShadingCorrection)

- (NSString *)shadingCorrectionDataKeyForPosition:(NSUInteger)position {
    if (position >= self.zoomClickStopPositions) {
        return nil;
    }
    CZZoomLevel *zoomLevel = [self zoomClickStopAtPosition:position];
    return zoomLevel.displayMagnification;
}

- (CZShadingCorrectionData *)currentShadingCorrectionDataWithImageResolution:(CGSize)imageResolution {
    return [self shadingCorrectionDataAtPosition:self.currentZoomClickStopPosition imageResolution:imageResolution];
}

- (NSString *)currentPathForShadingCorrectionMaskWithImageResolution:(CGSize)imageResolution {
    return [self pathForShadingCorrectionMaskAtPosition:self.currentZoomClickStopPosition imageResolution:imageResolution];
}

@end
