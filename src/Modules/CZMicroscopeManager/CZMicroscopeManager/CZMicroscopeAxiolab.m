//
//  CZMicroscopeAxiolab.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAxiolab.h"
#import "CZMicroscopeModelSubclass.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZMicroscopeAxiolab

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kAxiolab];
}

- (CZMicroscopeTypes)type {
    return kAxiolab;
}

@end
