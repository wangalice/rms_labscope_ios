//
//  CZZoomLevelSet.h
//  Labscope
//
//  Created by Ralph Jin on 4/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZZoomLevel.h"

extern NSString * const kZoomTypeStereoObjective;
extern NSString * const kZoomTypeZoomClickStop;
extern NSString * const kZoomTypeEyepiece;
extern NSString * const kZoomTypeCameraAdapter;
extern NSString * const kZoomTypeAdditionalEyepiece;
extern NSString * const kZoomTypeAdditionalCamera;
extern NSString * const kZoomTypeAdditionalOptovar;

@interface CZZoomLevelSet : NSObject

+ (CZZoomLevelSet *)setForAxiocamCompound;
+ (CZZoomLevelSet *)setForAxiocamStereo;
+ (CZZoomLevelSet *)setForAxioline;

- (instancetype)initWithTypeZooms:(NSDictionary<NSString *, NSArray<CZZoomLevel *> *> *)typeZooms;

- (NSUInteger)zoomLevelCountOfType:(NSString *)zoomType;

- (CZZoomLevel *)zoomLevelAtIndex:(NSUInteger)index ofType:(NSString *)zoomType;

- (NSUInteger)indexForZoomLevel:(CZZoomLevel *)zoomLevel ofType:(NSString *)zoomType;

- (NSArray<NSString *> *)availableTypes;

- (NSArray<CZZoomLevel *> *)availableZoomsForType:(NSString *)zoomType;

- (CZZoomLevel *)zoomLevelType:(NSString *)type ofMagnification:(double)magnification;

@end

extern NSArray *zoomLevelsFromCArray(const double zoomSteps[], size_t count);
