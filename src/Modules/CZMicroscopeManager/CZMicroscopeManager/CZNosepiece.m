//
//  CZNosepiece.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 3/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepiece.h"
#import "CZNosepiecePrivate.h"
#import <CZToolbox/CZToolbox.h>

NSString * const CZNosepieceDidUpdateNotification = @"CZNosepieceDidUpdateNotification";

@implementation CZObjective

+ (instancetype)none {
    return [[[self alloc] initWithMatID:nil] autorelease];
}

+ (NSArray<CZObjective *> *)allObjectives {
    static NSArray<CZObjective *> *allObjectives = nil;
    if (allObjectives == nil) {
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        NSURL *url = [bundle URLForResource:@"Objective" withExtension:@"json"];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url] options:0 error:nil];
        NSDictionary *list = json[@"value"];
        
        NSMutableArray<CZObjective *> *mutableObjectives = [NSMutableArray arrayWithCapacity:list.count + 1];
        
        for (NSString *key in list.allKeys) {
            NSDictionary *value = list[key];
            NSString *cls = value[@"cls"];
            NSString *obj = value[@"obj"];
            
            CZObjective *objective = [[CZObjective alloc] initWithMatID:key objectiveClass:cls objectiveName:obj];
            [mutableObjectives addObject:objective];
            [objective release];
        }
        
        [mutableObjectives sortUsingSelector:@selector(compare:)];
        
        CZObjective *none = [[CZObjective alloc] initWithMatID:nil];
        [mutableObjectives insertObject:none atIndex:0];
        [none release];
        
        allObjectives = [mutableObjectives copy];
    }
    return allObjectives;
}

- (instancetype)initWithMatID:(NSString *)matID {
    return [self initWithMatID:matID objectiveClass:nil objectiveName:nil];
}

- (instancetype)initWithMatID:(NSString *)matID objectiveName:(NSString *)objectiveName {
    return [self initWithMatID:matID objectiveClass:nil objectiveName:objectiveName];
}

- (instancetype)initWithMatID:(NSString *)matID objectiveClass:(NSString *)objectiveClass objectiveName:(NSString *)objectiveName {
    self = [super init];
    if (self) {
        _matID = [matID copy];
        _objectiveClass = [objectiveClass copy];
        
        if (_matID == nil) {
            _objectiveName = [L(@"NONE") retain];
            _displayName = [L(@"NONE") retain];
            _magnification = 0.0;
        } else if (objectiveName != nil) {
            objectiveName = [objectiveName stringByReplacingOccurrencesOfString:@"," withString:@"."];
            
            NSRegularExpression *magRegex = [NSRegularExpression regularExpressionWithPattern:@"([\\d|\\.]+)x" options:NSRegularExpressionAllowCommentsAndWhitespace error:nil];
            NSTextCheckingResult *magMatch = [magRegex firstMatchInString:objectiveName options:0 range:NSMakeRange(0, [objectiveName length])];
            if (magMatch && magMatch.numberOfRanges > 1) {
                NSRange range = [magMatch rangeAtIndex:1];
                NSString *magnificationString = [objectiveName substringWithRange:range];
                _magnification = magnificationString.floatValue;
            }
            
            NSRegularExpression *apeRegex = [NSRegularExpression regularExpressionWithPattern:@"/([\\d|\\.]+)" options:NSRegularExpressionAllowCommentsAndWhitespace error:nil];
            NSTextCheckingResult *apeMatch = [apeRegex firstMatchInString:objectiveName options:0 range:NSMakeRange(0, [objectiveName length])];
            if (apeMatch && apeMatch.numberOfRanges > 1) {
                NSRange range = [apeMatch rangeAtIndex:1];
                NSString *apertureString = [objectiveName substringWithRange:range];
                _aperture = apertureString.floatValue;
            }
            
            _objectiveName = [objectiveName copy];
            _displayName = [[NSString alloc] initWithFormat:@"%@ - %@", _matID, _objectiveName];
        }
    }
    return self;
}

- (void)dealloc {
    [_matID release];
    [_objectiveClass release];
    [_objectiveName release];
    [_displayName release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithMatID:self.matID
                                             objectiveClass:self.objectiveClass
                                              objectiveName:self.objectiveName];
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZObjective class]]) {
        return NO;
    } else {
        return [self isEqualToObjective:object];
    }
}

- (BOOL)isEqualToObjective:(CZObjective *)otherObjective {
    return (self.matID == nil && otherObjective.matID == nil) || [self.matID isEqualToString:otherObjective.matID];
}

- (NSComparisonResult)compare:(CZObjective *)otherObjective {
    if (self.magnification != otherObjective.magnification) {
        return [@(self.magnification) compare:@(otherObjective.magnification)];
    } else {
        return [self.objectiveName compare:otherObjective.objectiveName];
    }
}

- (NSString *)displayMagnification {
    if (self.magnification > 0) {
        NSString *displayMagnification = [CZCommonUtils localizedStringFromFloat:self.magnification precision:2];
        return [displayMagnification stringByAppendingString:@"x"];
    } else {
        return L(@"NONE");
    }
}

- (BOOL)isNone {
    return self.matID.length == 0;
}

@end

@interface CZNosepiecePositionInfo : NSObject <NSCopying>

@property (nonatomic, retain) CZObjective *objective;
@property (nonatomic, retain) NSNumber *fieldOfView;

- (instancetype)initWithObjective:(CZObjective *)objective fieldOfView:(NSNumber *)fieldOfView;

- (BOOL)isEqualToPositionInfo:(CZNosepiecePositionInfo *)otherPositionInfo;

@end

@implementation CZNosepiecePositionInfo

- (instancetype)initWithObjective:(CZObjective *)objective fieldOfView:(NSNumber *)fieldOfView {
    self = [super init];
    if (self) {
        _objective = [objective retain];
        _fieldOfView = [fieldOfView retain];
    }
    return self;
}

- (void)dealloc {
    [_objective release];
    [_fieldOfView release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithObjective:self.objective fieldOfView:self.fieldOfView];
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZNosepiecePositionInfo class]]) {
        return NO;
    } else {
        return [self isEqualToPositionInfo:object];
    }
}

- (BOOL)isEqualToPositionInfo:(CZNosepiecePositionInfo *)otherPositionInfo {
    return [self.objective isEqualToObjective:otherPositionInfo.objective] && self.fieldOfView.floatValue == otherPositionInfo.fieldOfView.floatValue;
}

@end

@interface CZNosepiece ()

@property (nonatomic, retain) NSMutableArray<CZNosepiecePositionInfo *> *positionInfos;

@end

@implementation CZNosepiece

- (instancetype)init {
    self = [super init];
    if (self) {
        _availableObjectives = [[CZObjective allObjectives] retain];
        _currentPosition = NSNotFound;
        _positionInfos = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_availableObjectives release];
    [_positionInfos release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNosepiece *nosepiece = [[[self class] allocWithZone:zone] init];
    nosepiece->_availableObjectives = [self.availableObjectives copy];
    nosepiece->_currentPosition = self.currentPosition;
    nosepiece->_positionInfos = [[NSMutableArray alloc] initWithArray:self.positionInfos copyItems:YES];
    return nosepiece;
}

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZNosepiece class]]) {
        return NO;
    } else {
        return [self isEqualToNosepiece:object];
    }
}

- (BOOL)isEqualToNosepiece:(CZNosepiece *)otherNosepiece {
    return [self.positionInfos isEqualToArray:otherNosepiece.positionInfos];
}

- (NSUInteger)positions {
    return self.positionInfos.count;
}

- (void)setPositions:(NSUInteger)positions {
    if (positions < self.positionInfos.count) {
        [self.positionInfos removeObjectsInRange:NSMakeRange(positions, self.positionInfos.count - positions)];
    } else if (positions > self.positionInfos.count) {
        for (NSInteger position = self.positionInfos.count; position < positions; position++) {
            CZNosepiecePositionInfo *positionInfo = [[CZNosepiecePositionInfo alloc] initWithObjective:[self availableObjectives][0] fieldOfView:nil];
            [self.positionInfos addObject:positionInfo];
            [positionInfo release];
        }
    }
    
    if (_currentPosition >= positions) {
        _currentPosition = NSNotFound;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZNosepieceDidUpdateNotification object:self];
}

- (void)setCurrentPosition:(NSUInteger)currentPosition {
    if (currentPosition < self.positions) {
        _currentPosition = currentPosition;
    } else {
        _currentPosition = NSNotFound;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZNosepieceDidUpdateNotification object:self];
}

- (CZObjective *)objectiveAtCurrentPosition {
    return [self objectiveAtPosition:self.currentPosition];
}

- (CZObjective *)objectiveAtPosition:(NSUInteger)position {
    if (position < self.positionInfos.count) {
        return self.positionInfos[position].objective;
    } else {
        return nil;
    }
}

- (void)setObjective:(CZObjective *)objective atPosition:(NSUInteger)position {
    if (position >= self.positionInfos.count) {
        return;
    }
    
    NSInteger index = [[self availableObjectives] indexOfObject:objective];
    if (index == NSNotFound) {
        index = 0;
    }
    
    self.positionInfos[position].objective = [self availableObjectives][index];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZNosepieceDidUpdateNotification object:self];
}

- (NSNumber *)fieldOfViewAtPosition:(NSUInteger)position {
    if (position < self.positionInfos.count) {
        return self.positionInfos[position].fieldOfView;
    } else {
        return nil;
    }
}

- (void)setFieldOfView:(NSNumber *)fieldOfView atPosition:(NSUInteger)position {
    if (position >= self.positionInfos.count) {
        return;
    }
    
    self.positionInfos[position].fieldOfView = fieldOfView;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZNosepieceDidUpdateNotification object:self];
}

@end

@implementation UIColor (CZObjectiveMagnification)

+ (instancetype)colorForMagnification:(float)magnification {
    const float kEpsilon = 1.0e-3f;
    
    UIColor *brown = [UIColor colorWithRed:54.0/255.0 green:48.0/255.0 blue:36.0/255.0 alpha:1.0];
    UIColor *mud = [UIColor colorWithRed:168.0/255.0 green:132.0/255.0 blue:0.0/255.0 alpha:1.0];
    UIColor *red = [UIColor colorWithRed:178.0/255.0 green:28.0/255.0 blue:28.0/255.0 alpha:1.0];
    UIColor *orange = [UIColor colorWithRed:208.0/255.0 green:138.0/255.0 blue:0.0/255.0 alpha:1.0];
    UIColor *yellow = [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:28.0/255.0 alpha:1.0];
    UIColor *green = [UIColor colorWithRed:28.0/255.0 green:180.0/255.0 blue:100.0/255.0 alpha:1.0];
    UIColor *lightBlue = [UIColor colorWithRed:28.0/255.0 green:175.0/255.0 blue:218.0/255.0 alpha:1.0];
    UIColor *darkBlue = [UIColor colorWithRed:28.0/255.0 green:96.0/255.0 blue:219.0/255.0 alpha:1.0];
    UIColor *white = [UIColor whiteColor];
    
    UIColor *color = nil;
    
    if (fabs(magnification - 1.0f) < kEpsilon ||
        fabs(magnification - 1.25f) < kEpsilon) {
        color = brown;
    } else if (fabs(magnification - 2.5f) < kEpsilon) {
        color = mud;
    } else if (fabs(magnification - 4.0f) < kEpsilon ||
               fabs(magnification - 5.0f) < kEpsilon) {
        color = red;
    } else if (fabs(magnification - 6.3f) < kEpsilon) {
        color = orange;
    } else if (fabs(magnification - 10.0f) < kEpsilon) {
        color = yellow;
    } else if (fabs(magnification - 16.0f) < kEpsilon ||
               fabs(magnification - 20.0f) < kEpsilon ||
               fabs(magnification - 25.0f) < kEpsilon ||
               fabs(magnification - 32.0f) < kEpsilon) {
        color = green;
    } else if (fabs(magnification - 40.0f) < kEpsilon ||
               fabs(magnification - 50.0f) < kEpsilon) {
        color = lightBlue;
    } else if (fabs(magnification - 63.0f) < kEpsilon ||
               fabs(magnification - 60.0f) < kEpsilon) {
        color = darkBlue;
    } else if (fabs(magnification - 100.0f) < kEpsilon ||
               fabs(magnification - 150.0f) < kEpsilon) {
        color = white;
    }
    
    return color;
}

@end
