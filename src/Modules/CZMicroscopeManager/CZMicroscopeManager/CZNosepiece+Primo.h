//
//  CZNosepiece+Primo.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepiece.h"
#import "CZNosepieceManager.h"

@interface CZObjective (Primo)

//For Primotech
+ (instancetype)epiPlan5x;
+ (instancetype)epiPlan10x;
+ (instancetype)epiPlan20x;
+ (instancetype)epiPlan50x;
+ (instancetype)epiPlan100x;
+ (instancetype)aPlan5x;
+ (instancetype)aPlan10x;
+ (instancetype)aPlan20x;
+ (instancetype)aPlan40x;
+ (instancetype)aPlan63x;

//For Primo-Star
+ (instancetype)planAchromat4x_01;
+ (instancetype)planAchromat10x_025;
+ (instancetype)planAchromat20x_040;
+ (instancetype)planAchromat40x_065;
+ (instancetype)planAchromat100x_125_oil;
+ (instancetype)planAchromat100x_080_dry;
+ (instancetype)planAchromat20x_040_D_0;
+ (instancetype)planAchromat40x_065_D_0;
+ (instancetype)planAchromat100x_125_D_0_oil;
+ (instancetype)planAchromat10x_025_ph1;
+ (instancetype)planAchromat20x_045_ph2;
+ (instancetype)planAchromat40x_065_ph2;
+ (instancetype)planAchromat100x_125_oil_ph3;

// For Primovert
+ (instancetype)ld20x_03_Ph1;
+ (instancetype)ld40x_05_Ph1;
+ (instancetype)ld20x_03_Ph2;
+ (instancetype)ld40x_05_Ph2;
+ (instancetype)aPlan2_5x;

+ (instancetype)objectiveFromEnumeration:(CZPrimoObjective)enumeration;
- (CZPrimoObjective)enumerationValue;

@end
