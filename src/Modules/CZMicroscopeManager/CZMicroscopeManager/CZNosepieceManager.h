//
//  CZNosepieceManager.h
//  Hermes
//
//  Created by Mike Wang on 2/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//TODO: will need to use string as key
//TODO: deprecated
//Warning: if need to add more objective needs to add from the end
//Previously named CZObjective
typedef NS_ENUM(NSUInteger, CZPrimoObjective) {
    kNoneObjective = 0,
    
    //For Primotech
    kPrimotech_Start = 1,
    kEpiplan5x = kPrimotech_Start,
    kEpiplan10x = 2,
    kEpiplan20x = 3,
    kEpiplan50x = 4,
    kEpiplan100x = 5,
    kAPlan5x = 6,
    kAPlan10x = 7,
    kAPlan20x = 8,
    kAPlan40x = 9,
    kAPlan63x = 10,
    kPrimotech_End = 11,
    
    //For Primo-Star
    kPlanAchromat_Start = kPrimotech_End,
    kPlanAchromat4x_01 = kPlanAchromat_Start,
    kPlanAchromat10x_025 = 12,
    kPlanAchromat20x_040 = 13,
    kPlanAchromat40x_065 = 14,
    kPlanAchromat100x_125_oil = 15,
    kPlanAchromat100x_080_dry = 16,
    kPlanAchromat20x_040_D_0 = 17,
    kPlanAchromat40x_065_D_0 = 18,
    kPlanAchromat100x_125_D_0_oil = 19,
    kPlanAchromat10x_025_ph1 = 20,
    kPlanAchromat20x_045_ph2 = 21,
    kPlanAchromat40x_065_ph2 = 22,
    kPlanAchromat100x_125_oil_ph3 = 23,
    kPlanAchromat_End = 24,
    
    // For Primovert
    kLD20x_03_Ph1 = kPlanAchromat_End,
    kLD40x_05_Ph1 = 25,
    kLD20x_03_Ph2 = 26,
    kLD40x_05_Ph2 = 27,
    kAplan2_5x = 28,
    
    kMaxObjective = 29
};

//Add a new entry to the end
typedef NS_ENUM(NSUInteger, CZPrimoStarPackage) {
    kCZPrimoStarPackageManualMode,
    kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_Pointer,
    kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_L_FOV18,
    kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_100x_08,
    kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph2_FOV20_Photo,
    kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph1_Ph2_Ph3_FOV20,
    kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Pointer,
    kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Photo,
    kCZPrimoStarPackage_LED_Fixed_Kohler_Stage_R_FOV18_D0,
    kCZPrimoStarPackage_HD_HAL_LED_Full_Kohler_FOV20_Integrated_HD_IP_Camera_5MP,
};

#define kCurrentNosepiece @"CurrentNosepiece"
#define kDefaultNosepiece @"DefaultNosepiece"
#define kDefaultCalibratedValues @"DefaultCalibratedValues"
#define kNonCalibratedValue -1.0

@interface CZNosepieceManager : NSObject

+ (NSArray *)packageNames;
+ (NSArray *)packagesFromDisplay;
+ (NSArray *)displayFromPackages;

@end
