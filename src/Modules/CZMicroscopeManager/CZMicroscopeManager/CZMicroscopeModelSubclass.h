//
//  CZMicroscopeModelSubclass.h
//  Hermes
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

/** catagory of CZMicroscopeModel, only visible to subclass.
 */
@interface CZMicroscopeModel (ForSubclassEyesOnly)

@property (nonatomic, assign, readonly) Class cameraClass;

// subclass must override "+(void)load" and register class.

// subclass must override methods
+ (CZZoomLevelSet *)availableZoomLevelSet;

- (NSArray<CZObjective *> *)availableObjectives;
- (NSUInteger)defaultNosepiecePositions;

+ (UIImage *)modelIcon;

// subclass may override methods
- (instancetype)initWithCamera:(CZCamera *)camera;
- (void)initTypeZooms:(NSMutableDictionary *)typeZooms;
- (void)initAdditionalZooms:(NSMutableDictionary *)additionalZooms;

// open methods for subclass
+ (void)registerMicroscopeModelClass:(Class)modelClass forType:(CZMicroscopeTypes)type;

@end
