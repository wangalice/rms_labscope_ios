//
//  CZCamera+MicroscopeManagerAdditions.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <CZCameraInterface/CZCameraInterface.h>
#import "CZMicroscopeModel.h"

@interface CZCamera (MicroscopeManagerAdditions)

@property (nonatomic, readonly, copy) NSArray<NSNumber *> *supportedMicroscopeModelTypes;
@property (nonatomic, readonly, copy) NSString *cameraModelName;
@property (nonatomic, readonly, retain) UIImage *cameraModelIcon;

@end
