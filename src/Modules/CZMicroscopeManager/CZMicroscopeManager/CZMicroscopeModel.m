//
//  CZMicroscopeModel.m
//  Hermes
//
//  Created by Mike Wang on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"
#import "CZMicroscopeModelPrivate.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZMicroscopeModel+MNA.h"
#import "CZMicroscopeModel+ShadingCorrection.h"
#import "CZNosepiecePrivate.h"
#import "CZNosepiece+Primo.h"
#import "CZCamera+MicroscopeManagerAdditions.h"

#import <SBJson/SBJson.h>
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZZoomLevelSet.h"

NSString * const CZMicroscopeModelErrorDomain = @"MicroscopeModelError";

NSString * const kModelNamePrimotech = @"Primotech";
NSString * const kModelNamePrimoStar = @"Primo Star";
NSString * const kModelNamePrimovert = @"Primovert HDcam";
NSString * const kModelNameCompound = @"Compound microscope";
NSString * const kModelNameStereo = @"Stereo microscope";
NSString * const kModelNameStemi305 = @"Stemi 305 cam";
NSString * const kModelNameStemi508 = @"Stemi 508 cam";
NSString * const kModelNameAxioscope = @"Axioscope";
NSString * const kModelNameAxiolab = @"Axiolab";
NSString * const kModelNameMacroImage = @"Macro";
NSString * const kCameraModelNameAxiocamERc5s = @"AxioCam ERc 5s (Rev. 2)";
NSString * const kCameraModelNameAxiocam202 = @"Axiocam 202";
NSString * const kCameraModelNameAxiocam208 = @"Axiocam 208";
NSString * const kCameraModelNameBuiltIn = @"iPad built-in camera";

NSString * const kNotAvailableMagnification = @"?";

NSString * const CZZoomKnobDidUpdateNotification = @"CZZoomKnobDidUpdateNotification";

const float kMicroscopeModelMinZoom = 0.005;  // regard precision as 0.01

#pragma mark -

static NSMutableDictionary *subModelClasses = nil;

@interface CZMicroscopeModel ()

@property (nonatomic, assign, readonly) Class cameraClass;

@property (nonatomic, retain, readwrite) NSMutableDictionary *typeZooms;  // dictionary of CZZoomLevel
@property (nonatomic, retain, readwrite) NSMutableDictionary *additionalZooms;  // dictionary of NSNumber

@end

@implementation CZMicroscopeModel

@synthesize nosepiece = _nosepiece;
@synthesize reflector = _reflector;
@synthesize cameraClass = _cameraClass;

+ (Class)microscopeModelClassOfType:(CZMicroscopeTypes)type {
    NSNumber *key = @(type);
    return [subModelClasses objectForKey:key];
}

+ (CZMicroscopeModel *)newMicroscopeModelWithCamera:(CZCamera *)camera {
    CZMicroscopeModel *model = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *modelFromSystem = nil;
    if (camera.macAddress) {
        modelFromSystem = [defaults g_valueFromUserDefaultsWithInsensitiveKeys:camera.macAddress];
    }
    
    CZMicroscopeTypes type = kMicroscopeTypeDefault;
    
    NSMutableArray<NSNumber *> *supportedTypes = [camera.supportedMicroscopeModelTypes mutableCopy];
    [supportedTypes removeObject:@(kScanBarCodeModel)];
    
    if (modelFromSystem) {
        NSNumber *typeString = modelFromSystem[kModelType];
        type = (CZMicroscopeTypes)[typeString intValue];
        if (![supportedTypes containsObject:@(type)]) {
            type = kMicroscopeTypeDefault;
        }
    }
    
    if (type == kMicroscopeTypeDefault && supportedTypes.count == 1) {
        type = supportedTypes.firstObject.integerValue;
    }
    
    Class c = [CZMicroscopeModel microscopeModelClassOfType:type];
    if (c == nil) {
        c = [CZMicroscopeModel class];
    }
    
    model = [[c alloc] initWithCamera:camera];
    return model;
}

+ (NSUInteger)microscopeModelClassCount {
    return subModelClasses.count;
}

+ (NSString *)cameraModelName {
    return nil;
}

- (instancetype)initWithCamera:(CZCamera *)camera {
    if (camera == nil) {
        [self release];
        return nil;
    }
    
    self = [self init];
    if (self) {
        [self updateDataFromCamera:camera];
        
        if (self.type == kAxiocamStereo) {
            _currentZoomClickStopPosition = NSNotFound;
        }
        
        self.mnaMACAddress = kNULLMACAddress;
        self.mnaIPAddress = kNULLIPAddress;
        
        self.shadingCorrectionMaskPath = nil;
        self.shadingCorrectionMaskData = nil;
        self.shadingCorrectionUserInfo = [[NSMutableDictionary alloc] init];
        
        //first check if this camera is previously cached
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *modelFromSystem = [defaults g_valueFromUserDefaultsWithInsensitiveKeys:camera.macAddress];
        if (modelFromSystem) {
            [self readFromDictionary:modelFromSystem];
            [self loadSlotsFromDefaults];
        }
    }
    return self;
}

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super init];
    if (self) {
        _cameraClass = microscopeModel.cameraClass;
        
        _name = [microscopeModel.name copy];
        _pin = [microscopeModel.pin copy];
        _cameraMACAddress = [microscopeModel.cameraMACAddress copy];
        _isDefault = microscopeModel.isDefault;
        
        if ([microscopeModel canSupportMNA]) {
            _mnaMACAddress = [microscopeModel.mnaMACAddress copy];
            _mnaIPAddress = [microscopeModel.mnaIPAddress copy];
        } else {
            _mnaMACAddress = kNULLMACAddress;
            _mnaIPAddress = kNULLIPAddress;
        }
        
        _nosepiece = [microscopeModel.nosepiece copy];
        _reflector = [microscopeModel.reflector copy];
        _currentZoomClickStopPosition = microscopeModel.currentZoomClickStopPosition;
        
        _shadingCorrectionMaskPath = [microscopeModel.shadingCorrectionMaskPath copy];
        _shadingCorrectionMaskData = [microscopeModel.shadingCorrectionMaskData retain];
        _shadingCorrectionUserInfo = [microscopeModel.shadingCorrectionUserInfo mutableCopy];
        
        _typeZooms = [microscopeModel.typeZooms mutableCopy];
        _additionalZooms = [microscopeModel.additionalZooms mutableCopy];
        _shadingCorrectionData = [microscopeModel.shadingCorrectionData mutableCopy];
        _shadingCorrectionMask = [microscopeModel.shadingCorrectionMask mutableCopy];
    }
    return self;
}

- (void)dealloc {
    [_name release];
    [_pin release];
    [_mnaMACAddress release];
    [_mnaIPAddress release];
    [_cameraMACAddress release];
    [_nosepiece release];
    [_reflector release];
    [_typeZooms release];
    [_additionalZooms release];
    [_shadingCorrectionData release];
    [_shadingCorrectionMaskPath release];
    [_shadingCorrectionMaskData release];
    [_shadingCorrectionUserInfo release];
    [_shadingCorrectionMask release];
    
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithMicroscopeModel:self];
}

#pragma mark - Equatable

- (BOOL)isEqual:(id)object {
    if (object == nil) {
        return NO;
    } else if (![object isKindOfClass:[CZMicroscopeModel class]]) {
        return NO;
    } else {
        return [self isEqualToMicroscopeModel:object];
    }
}

- (BOOL)isEqualToMicroscopeModel:(CZMicroscopeModel *)otherMicroscopeModel {
    if (self.type != otherMicroscopeModel.type ||
        self.isDefault != otherMicroscopeModel.isDefault ||
        self.typeZooms.count != otherMicroscopeModel.typeZooms.count ||
        self.additionalZooms.count != otherMicroscopeModel.additionalZooms.count ||
        self.shadingCorrectionMask.count != otherMicroscopeModel.shadingCorrectionMask.count ||
        self.shadingCorrectionData.count != otherMicroscopeModel.shadingCorrectionData.count) {
        return NO;
    }
    
    if (![self.cameraMACAddress isEqualComparisionWithCaseInsensitive:otherMicroscopeModel.cameraMACAddress] ||
        ![self.name isEqualToString:otherMicroscopeModel.name]) {
        return NO;
    }
    
    if ((self.pin.length > 0 || otherMicroscopeModel.pin.length > 0) && ![self.pin isEqualToString:otherMicroscopeModel.pin]) {
        return NO;
    }
    
    if (self.nosepiece && ![self.nosepiece isEqualToNosepiece:otherMicroscopeModel.nosepiece]) {
        return NO;
    }
    
    if (self.hasReflector && ![self.reflector isEqualToReflector:otherMicroscopeModel.reflector]) {
        return NO;
    }
    
    if (self.currentZoomClickStopPosition != otherMicroscopeModel.currentZoomClickStopPosition) {
        return NO;
    }
    
    for (NSString *key in self.typeZooms) {
        CZZoomLevel *typeZoom = otherMicroscopeModel.typeZooms[key];
        if (typeZoom == nil) {
            return NO;
        }
        if (![typeZoom isEqual:self.typeZooms[key]]) {
            return NO;
        }
    }
    
    for (NSString *key in self.additionalZooms) {
        NSNumber *additionalZoom = otherMicroscopeModel.additionalZooms[key];
        if (additionalZoom == nil) {
            return NO;
        }
        if (![additionalZoom isEqualToNumber:self.additionalZooms[key]]) {
            return NO;
        }
    }
    
    BOOL equalShadingCorrectionData = ((_shadingCorrectionData.count == 0 && otherMicroscopeModel.shadingCorrectionData.count == 0) ||
                                       [self.shadingCorrectionData isEqualToDictionary:otherMicroscopeModel.shadingCorrectionData] ||
                                       [self.shadingCorrectionMask isEqualToDictionary:otherMicroscopeModel.shadingCorrectionMask]);
    if (!equalShadingCorrectionData) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Public

- (CZMicroscopeTypes)type {
    return kUnknownModelType;
}

- (CZMicroscopeModel *)modelBySwitchingToType:(CZMicroscopeTypes)type {
    if (type == [self type]) {
        return self;
    }
    
    CZMicroscopeModel *model = nil;
    Class c = [CZMicroscopeModel microscopeModelClassOfType:type];
    if (c) {
        BOOL wasStereo = [self isKindOfClass:[CZMicroscopeModelStereo class]];
        
        model = [[[c alloc] initWithMicroscopeModel:self] autorelease];
        
        // clear data which does not make sense
        model.nosepiece = nil;
        model.additionalZooms = nil;
        model.typeZooms = nil;
        
        // clear shading correction data when switching between compound and stereo,
        // because one is per-objective and another is global
        BOOL isStereo = [model isKindOfClass:[CZMicroscopeModelStereo class]];
        if (isStereo != wasStereo) {
            model.shadingCorrectionData = nil;
            model.shadingCorrectionMaskPath = nil;
            model.shadingCorrectionMaskData = nil;
            model.shadingCorrectionUserInfo = nil;
            model.shadingCorrectionMask = nil;
        }
    }
    
    return model;
}

- (NSString *)modelName {
    return nil;
}

- (UIImage *)modelIcon {
    return [[self class] modelIcon];
}

- (NSString *)cameraModelName {
    return [[self class] cameraModelName];
}

- (CZNosepiece *)nosepiece {
    if (_nosepiece == nil) {
        _nosepiece = [[CZNosepiece alloc] init];
        _nosepiece.availableObjectives = [self availableObjectives];
        _nosepiece.positions = [self defaultNosepiecePositions];
    }
    return _nosepiece;
}

- (BOOL)hasReflector {
    return NO;
}

- (CZReflector *)reflector {
    if (!self.hasReflector) {
        return nil;
    } else {
        if (_reflector == nil) {
            _reflector = [[CZReflector alloc] init];
        }
        return _reflector;
    }
}

- (CZZoomLevelSet *)availableZoomLevelSet {
    return [[self class] availableZoomLevelSet];
}

- (NSMutableDictionary *)typeZooms {
    if (_typeZooms == nil) {
        _typeZooms = [[NSMutableDictionary alloc] init];
        
        [self initTypeZooms:_typeZooms];
    }
    
    return _typeZooms;
}

- (NSMutableDictionary *)additionalZooms {
    if (_additionalZooms == nil) {
        _additionalZooms = [[NSMutableDictionary alloc] init];
        
        [self initAdditionalZooms:_additionalZooms];
    }
    
    return _additionalZooms;
}

- (NSMutableDictionary *)shadingCorrectionData {
    if (_shadingCorrectionData == nil) {
        _shadingCorrectionData = [[NSMutableDictionary alloc] init];
    }
    
    return _shadingCorrectionData;
}

- (NSMutableDictionary *)shadingCorrectionMask {
    if (_shadingCorrectionMask == nil) {
        _shadingCorrectionMask = [[NSMutableDictionary alloc] init];
    }
    
    return _shadingCorrectionMask;
}

#pragma mark - slot features

- (float)magnificationAtPosition:(NSUInteger)position {
    return 1.0;
}

- (NSString *)displayMagnificationAtPosition:(NSUInteger)position localized:(BOOL)localized {
    float magnification = [self magnificationAtPosition:position];

    if (magnification >= kMicroscopeModelMinZoom) {
        NSString *zoomString = nil;
        if (localized) {
            zoomString = [CZCommonUtils localizedStringFromFloat:magnification precision:2];
        } else {
            zoomString = [CZCommonUtils stringFromFloat:magnification precision:2];
        }
        return [zoomString stringByAppendingString:@"x"];
    } else {
        return kNotAvailableMagnification;
    }
}

- (float)cameraMagnificationAtPosition:(NSUInteger)position {
    return 1.0;
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    [self.nosepiece setFieldOfView:@(fovWidth) atPosition:position];
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    NSNumber *fieldOfView = [self.nosepiece fieldOfViewAtPosition:position];
    
    if (fieldOfView.floatValue > 0) {
        return fieldOfView.floatValue;
    } else {
        return [self defaultFOVWidthAtPosition:position];
    }
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    return kNonCalibratedValue;
}

- (BOOL)isDefaultFOVWidthAtPosition:(NSUInteger)position {
    NSNumber *fieldOfView = [self.nosepiece fieldOfViewAtPosition:position];
    
    if (fieldOfView) {
        return fieldOfView.floatValue <= 0;
    } else {
        return YES;
    }
}

#pragma mark - type zooms

- (void)setZoom:(CZZoomLevel *)zoom forType:(NSString *)zoomType {
    [self.typeZooms setObject:zoom forKey:zoomType];
}

- (CZZoomLevel *)zoomForType:(NSString *)zoomType {
    return [self.typeZooms objectForKey:zoomType];
}

- (void)removeZoomForType:(NSString *)zoomType {
    [self.typeZooms removeObjectForKey:zoomType];
}

#pragma mark - additional zooms

- (void)setAdditionalZoom:(float)zoom forType:(NSString *)zoomType {
    [self.additionalZooms setObject:@(zoom) forKey:zoomType];
}

- (NSNumber *)additionalZoomForType:(NSString *)zoomType {
    return [self.additionalZooms objectForKey:zoomType];
}

#pragma mark - current slot features

- (float)currentFOVWidth {
    return [self fovWidthAtPosition:self.nosepiece.currentPosition];
}

- (void)moveScalingImages {
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *macAddress = self.cameraMACAddress;
    macAddress = [macAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    cachePath = [cachePath stringByAppendingPathComponent:macAddress];
    cachePath = [cachePath stringByAppendingPathComponent:@"Scaling"];
    NSString *tmpPath = NSTemporaryDirectory();
    NSArray *tmpContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tmpPath error:nil];
    
    BOOL isDir = NO;
    if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:NULL];
    }
    NSError *error;
    
    for (NSString *tempFileName in tmpContents) {
        NSRange range1 = [tempFileName rangeOfString:@"scaling"];
        NSRange range2 = [tempFileName rangeOfString:macAddress];
        if (range1.location != NSNotFound && range2.location != NSNotFound) {
            
            // remove the macAddress from fileName
            NSString *fileName = [tempFileName substringFromIndex:18];
            tmpPath = [NSTemporaryDirectory() stringByAppendingPathComponent:tempFileName];
            NSString *dstPath = [cachePath stringByAppendingPathComponent:fileName];
            
            BOOL suceess;
            if ([[NSFileManager defaultManager] fileExistsAtPath:dstPath]) {
                [[NSFileManager defaultManager] removeItemAtPath:dstPath error:&error];
            }
            suceess = [[NSFileManager defaultManager] moveItemAtPath:tmpPath toPath:dstPath error:&error];
            if (!suceess) {
                CZLogv(@"%@", error);
            }
        }
    }
    // Rename scaling referance image when changing objective
    NSArray *scalingFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:nil];
    for (NSString *scalingFileName in scalingFiles) {
        NSArray *components = [scalingFileName componentsSeparatedByString:@"_"];
        NSString *magnification = components[0];
        NSUInteger position = (NSUInteger)[components[2] intValue];
        NSString *realMagnification = [[self.nosepiece objectiveAtPosition:position] displayMagnification];
        NSString *renameFileName = scalingFileName;
        if (realMagnification.length > 0 && ![realMagnification isEqualToString:magnification]) {
            renameFileName = [renameFileName stringByReplacingOccurrencesOfString:magnification withString:realMagnification];
            NSString *srcPath = [cachePath stringByAppendingPathComponent:scalingFileName];
            NSString *dstPath = [cachePath stringByAppendingPathComponent:renameFileName];
            BOOL suceess;
            suceess = [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:dstPath error:&error];
            if (!suceess) {
                CZLogv(@"%@", error);
            }
        }
    }
}

- (void)deleteTemporarilyScalingImages {
    NSString *tmpPath = NSTemporaryDirectory();
    NSString *macAddress = self.cameraMACAddress;
    macAddress = [macAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    NSArray *tmpContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tmpPath error:nil];
    for (NSString *fileName in tmpContents) {
        NSRange range = [fileName rangeOfString:macAddress];
        if (range.location != NSNotFound) {
            tmpPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
            [[NSFileManager defaultManager] removeItemAtPath:tmpPath error:nil];
        }
    }
}

- (void)deleteCachedScalingImages {
    NSString *dstPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *macAddress = self.cameraMACAddress;
    macAddress = [macAddress stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    dstPath = [dstPath stringByAppendingPathComponent:macAddress];
    dstPath = [dstPath stringByAppendingPathComponent:@"Scaling"];
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:dstPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:dstPath error:&error];
    }
}

- (void)updateDataFromCamera:(CZCamera *)camera {
    // Always use name and MAC address from camera when possible.
    if (![camera isMemberOfClass:[CZCamera class]]) { // is not placeholder camera
        self.name = camera.displayName;
        self.pin = camera.pin;
    }
    _cameraClass = [camera class];
    self.cameraMACAddress = camera.macAddress;
}

- (NSString *)cachedCameraPIN {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *pin = [defaults g_valueFromUserDefaultsWithInsensitiveKeys:[NSString stringWithFormat:@"%@+%@", self.cameraMACAddress, kCachedCameraPIN] mixedKeys:@[[NSString stringWithFormat:@"%@+%@", self.cameraMACAddress, kCachedCameraPIN],[NSString stringWithFormat:@"%@+%@", [self.cameraMACAddress uppercaseString], kCachedCameraPIN], [NSString stringWithFormat:@"%@+%@", [self.cameraMACAddress lowercaseString], kCachedCameraPIN] ]];
    return pin;
}

- (void)setCachedCameraPIN:(NSString *)cachedCameraPIN {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults g_setValue:cachedCameraPIN insensitiveKey:[NSString stringWithFormat:@"%@+%@", [self.cameraMACAddress uppercaseString], kCachedCameraPIN] mixedKeys:@[[NSString stringWithFormat:@"%@+%@", [self.cameraMACAddress uppercaseString], kCachedCameraPIN], [NSString stringWithFormat:@"%@+%@", [self.cameraMACAddress lowercaseString], kCachedCameraPIN], [NSString stringWithFormat:@"%@+%@", self.cameraMACAddress, kCachedCameraPIN]]];
}

- (NSString *)saveToJsonContent {
    NSMutableDictionary *microscopeObject = [[NSMutableDictionary alloc] init];
    [self writeToDictionary:microscopeObject];
    [self saveSlotsToDictionary:microscopeObject];
    
    NSArray *ignoreKeys = @[kSecurityPIN,  // don't leak security info into JSON content
                            kShadingCorrectionData,  // ignore shading correction data, it useless without reference image
                            kShadingCorrectionEnable,
                            kShadingCorrectMaxMaskX,
                            kShadingCorrectMaxMaskY,
                            kShadingCorrectMaxMaskZ];
    [microscopeObject removeObjectsForKeys:ignoreKeys];
    
    [microscopeObject setObject:self.cameraMACAddress forKey:kCameraMACAddress];  // add camera MAC address, which does not write into system default
    
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    NSString *string = [writer stringWithObject:microscopeObject];
    [microscopeObject release];
    [writer release];
    
    return string;
}

- (CZMicroscopeModel *)newMicroscopeModelWithJsonContent:(NSString *)content error:(NSError **)error {
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSDictionary *microscopeObject = [parser objectWithString:content];
    [parser release];
    
    CZMicroscopeModel *microscopeModel = nil;
    do {
        if (microscopeObject == nil) {
            break;
        }
        
        NSString *cameraMacAddress = microscopeObject[kCameraMACAddress];

        BOOL isCameraMacAddressEqual = [cameraMacAddress isEqualComparisionWithCaseInsensitive:self.cameraMACAddress];
        if (!isCameraMacAddressEqual) {
            if (cameraMacAddress.length == self.cameraMACAddress.length) {
                if (error) {
                    *error = [NSError errorWithDomain:CZMicroscopeModelErrorDomain code:-1001 userInfo:nil];
                }
            }
            break;
        }
        
        NSNumber *typeObj = [microscopeObject objectForKey:kModelType];
        if (typeObj == nil) {
            break;
        }

        CZMicroscopeTypes type = [typeObj unsignedIntegerValue];
        if (type < kNumOfMicroscopeModels) {
            if (type == self.type) {
                microscopeModel = [self copy];
            } else {
                microscopeModel = [[self modelBySwitchingToType:type] retain];
            }
        }
        
        [microscopeModel readFromDictionary:microscopeObject];
        [microscopeModel loadSlotsFromDictionary:microscopeObject];
    } while (0);
    
    if (microscopeModel == nil && error && *error == nil) {
        *error = [NSError errorWithDomain:CZMicroscopeModelErrorDomain code:-1000 userInfo:nil];
    }
    return microscopeModel;
}

- (void)saveToDefaults {
    if (!self.cameraMACAddress) {
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (self.isDefault) {
        //There could be a previous set default microscope camera
        //not connected, so it won't be shown in the microscopeModels
        //we need to set its default to false in NSUserDefault persistent storage
        
        NSString *previousDefaultCameraMACAddress = [CZDefaultSettings sharedInstance].defaultCameraMACAddress;
        BOOL isEqualMACAddress = [previousDefaultCameraMACAddress isEqualComparisionWithCaseInsensitive:self.cameraMACAddress];
        if (previousDefaultCameraMACAddress &&
            (previousDefaultCameraMACAddress.length != 0) &&
            !isEqualMACAddress) {
            NSDictionary *dictionary = [defaults g_valueFromUserDefaultsWithInsensitiveKeys:previousDefaultCameraMACAddress] ? : [NSDictionary dictionary];
            NSMutableDictionary *modelFromSystem = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            NSNumber *isDefault = [NSNumber numberWithBool:NO];
            modelFromSystem[kIsDefault] = isDefault;
            [defaults g_setValue:modelFromSystem insensitiveKey:previousDefaultCameraMACAddress];
            [modelFromSystem release];
            
        }
        [[CZDefaultSettings sharedInstance] setDefaultCameraMACAddress:self.cameraMACAddress];
    }

    //Set the microscope model with unique value for the insensitive key
    NSMutableDictionary *modelToSystem = [[NSMutableDictionary alloc] init];
    [self writeToDictionary:modelToSystem];
    [defaults g_setValue:modelToSystem insensitiveKey:[self.cameraMACAddress uppercaseString]];
    
    [modelToSystem release];
}

static inline NSString *defaultNosepiecesKey(NSString *key) {
    return [NSString stringWithFormat:@"%@+%@", kDefaultNosepiece, key];
}

static inline NSString *defaultCalibratedValuesKey(NSString *key) {
    return [NSString stringWithFormat:@"%@+%@", kDefaultCalibratedValues, key];
}

static inline NSString *currentNosepieceKey(NSString *key) {
    return [NSString stringWithFormat:@"%@+%@", kCurrentNosepiece, key];
}

- (void)saveSlotsToDefaults {
    NSString *macAddress = self.cameraMACAddress;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *objectivesForNosepiecesDic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *fovWidthForNosepiecesDic = [[NSMutableDictionary alloc] init];
    
    if ([self isKindOfClass:[CZMicroscopeModelCompound class]]) {
        [defaults g_setValue:@(self.nosepiece.currentPosition) insensitiveKey:currentNosepieceKey([macAddress uppercaseString]) mixedKeys:@[currentNosepieceKey([macAddress uppercaseString]), currentNosepieceKey([macAddress lowercaseString]), currentNosepieceKey(macAddress)]];
        
        for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
            NSString *keyStringValue = [NSString stringWithFormat:@"%lu", (unsigned long)position];
            
            NSNumber *fieldOfView = [self.nosepiece fieldOfViewAtPosition:position];
            [fovWidthForNosepiecesDic setObject:fieldOfView ?: @(kNonCalibratedValue) forKey:keyStringValue];
            
            CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
            [objectivesForNosepiecesDic setObject:objective.matID ?: @"" forKey:keyStringValue];
        }
    }
    
    if ([self isKindOfClass:[CZMicroscopeModelStereo class]]) {
        [defaults g_setValue:@(self.currentZoomClickStopPosition) insensitiveKey:currentNosepieceKey([macAddress uppercaseString]) mixedKeys:@[currentNosepieceKey([macAddress uppercaseString]), currentNosepieceKey([macAddress lowercaseString]), currentNosepieceKey(macAddress)]];
        
        if (self.type == kAxiocamStereo) {
            for (NSUInteger position = 0; position < [self zoomClickStopPositions]; position++) {
                NSString *keyStringValue = [NSString stringWithFormat:@"%lu", (unsigned long)position];
                
                NSUInteger zoomClickStopID = [self zoomClickStopIDAtPosition:position];
                [objectivesForNosepiecesDic setObject:@(zoomClickStopID) forKey:keyStringValue];
            }
        }
    }
    
    [defaults g_setValue:objectivesForNosepiecesDic insensitiveKey:defaultNosepiecesKey([macAddress uppercaseString]) mixedKeys:@[defaultNosepiecesKey([macAddress uppercaseString]), defaultNosepiecesKey([macAddress lowercaseString]), defaultNosepiecesKey(macAddress)]];
    [objectivesForNosepiecesDic release];

    [defaults g_setValue:fovWidthForNosepiecesDic insensitiveKey:defaultCalibratedValuesKey([macAddress uppercaseString]) mixedKeys:@[defaultCalibratedValuesKey([macAddress lowercaseString]), defaultCalibratedValuesKey([macAddress uppercaseString]), defaultCalibratedValuesKey(macAddress)]];
    
    [fovWidthForNosepiecesDic release];
    
    // clear original data
    for (NSString *type in @[kZoomTypeStereoObjective,
                             kZoomTypeCameraAdapter,
                             kZoomTypeEyepiece,
                             kZoomTypeAdditionalCamera,
                             kZoomTypeAdditionalEyepiece,
                             kZoomTypeAdditionalOptovar]) {
        NSString *combineKey = [NSString stringWithFormat:@"%@+%@", type, macAddress];
        [defaults removeObjectForKey:combineKey];
    }
    
    // save type zooms
    for (NSString *type in _typeZooms.allKeys) {
        CZZoomLevel *zoomLevel = _typeZooms[type];
        
        NSString *combineKey = [NSString stringWithFormat:@"%@+%@", type, macAddress];
        [defaults setDouble:zoomLevel.magnification forKey:combineKey];
    }
    
    // save additional zooms
    for (NSString *type in _additionalZooms.allKeys) {
        NSNumber *zoom = _additionalZooms[type];
        NSString *combineKey = [NSString stringWithFormat:@"%@+%@", type, macAddress];
        [defaults setObject:zoom forKey:combineKey];
    }
}

- (void)loadSlotsFromDefaults {
    NSString *macAddress = self.cameraMACAddress;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *defaultNosepieces = [defaults dictionaryForKey:defaultNosepiecesKey([macAddress uppercaseString])] ? : [defaults dictionaryForKey:defaultNosepiecesKey([macAddress lowercaseString])];
    
    if ([self isKindOfClass:[CZMicroscopeModelCompound class]]) {
        if (defaultNosepieces.count > 0) {
            self.nosepiece.positions = defaultNosepieces.count;
            for (NSString *keyStringValue in defaultNosepieces.allKeys) {
                NSString *objectiveMatID = defaultNosepieces[keyStringValue];
                if ([objectiveMatID isKindOfClass:[NSString class]]) {
                    CZObjective *objective = [[CZObjective alloc] initWithMatID:objectiveMatID];
                    [self.nosepiece setObjective:objective atPosition:keyStringValue.intValue];
                } else if ([objectiveMatID isKindOfClass:[NSNumber class]]) {   // backward compatible
                    CZPrimoObjective enumeration = ((NSNumber *)objectiveMatID).unsignedIntegerValue;
                    CZObjective *objective = [CZObjective objectiveFromEnumeration:enumeration];
                    [self.nosepiece setObjective:objective atPosition:keyStringValue.intValue];
                }
            }
        }
        
        if ([defaults objectForKey:currentNosepieceKey(macAddress.uppercaseString)]) {
            self.nosepiece.currentPosition = [defaults integerForKey:currentNosepieceKey(macAddress.uppercaseString)];
        } else if ([defaults objectForKey:currentNosepieceKey(macAddress.lowercaseString)]) {
            self.nosepiece.currentPosition = [defaults integerForKey:currentNosepieceKey(macAddress.lowercaseString)];
        } else {
            self.nosepiece.currentPosition = NSNotFound;
        }
    }
    
    if ([self isKindOfClass:[CZMicroscopeModelStereo class]]) {
        if (self.type == kAxiocamStereo) {
            [self removeAllZoomClickStops];
            
            for (NSString *keyStringValue in defaultNosepieces.allKeys) {
                NSUInteger zoomLevelID = [defaultNosepieces[keyStringValue] unsignedIntegerValue];
                CZZoomLevel *zoomLevel = [[self availableZoomLevelSet] zoomLevelAtIndex:zoomLevelID ofType:kZoomTypeZoomClickStop];
                if (zoomLevel) {
                    [self addZoomClickStop:zoomLevel];
                }
            }
        }
        
        self.currentZoomClickStopPosition = [defaults integerForKey:currentNosepieceKey([macAddress uppercaseString])] ? : [defaults integerForKey:currentNosepieceKey([macAddress lowercaseString])];
    }
    
    NSDictionary *defaultCalibratedValues = [defaults dictionaryForKey:defaultCalibratedValuesKey([macAddress uppercaseString])] ? : [defaults dictionaryForKey:defaultCalibratedValuesKey([macAddress lowercaseString])];
    if (defaultCalibratedValues) {
        for (NSString *keyStringValue in defaultNosepieces.allKeys) {
            NSNumber *fov = defaultCalibratedValues[keyStringValue];
            [self setFOVWidth:[fov floatValue] atPosition:[keyStringValue intValue]];
        }
    }
    else {
        for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
            [self setFOVWidth:kNonCalibratedValue atPosition:position];
        }
    }
    
    // load type zooms
    for (NSString *type in @[kZoomTypeStereoObjective,
                             kZoomTypeCameraAdapter,
                             kZoomTypeEyepiece]) {
        NSString *combineKey = [NSString stringWithFormat:@"%@+%@", type, macAddress];
        float zoom = [defaults floatForKey:combineKey];
        if (zoom > 0) {
            CZZoomLevel *zoomLevel = [[self availableZoomLevelSet] zoomLevelType:type ofMagnification:zoom];
            if (zoomLevel) {
                [self setZoom:zoomLevel forType:type];
            }
        }
    }
    
    // save additional zooms
    self.additionalZooms = nil;
    for (NSString *type in @[kZoomTypeAdditionalCamera,
                             kZoomTypeAdditionalEyepiece,
                             kZoomTypeAdditionalOptovar]) {
        NSString *combineKey = [NSString stringWithFormat:@"%@+%@", type, macAddress];
        float zoom = [defaults floatForKey:combineKey];
        if (zoom > 0) {
            [self setAdditionalZoom:zoom forType:type];
        }
    }
}

- (void)removeSlotsFromDefaults {
    NSString *macAddress = self.cameraMACAddress;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults removeObjectForKey:currentNosepieceKey([macAddress uppercaseString])];
    [defaults removeObjectForKey:defaultNosepiecesKey([macAddress uppercaseString])];
    [defaults removeObjectForKey:defaultCalibratedValuesKey([macAddress uppercaseString])];
    [defaults removeObjectForKey:currentNosepieceKey([macAddress lowercaseString])];
    [defaults removeObjectForKey:defaultNosepiecesKey([macAddress lowercaseString])];
    [defaults removeObjectForKey:defaultCalibratedValuesKey([macAddress lowercaseString])];

}

- (void)saveSlotsToDictionary:(NSMutableDictionary *)dictionary {
    [dictionary setObject:@(self.nosepiece.currentPosition) forKey:kCurrentNosepiece];
    
    NSMutableDictionary *objectivesForNosepiecesDic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *fovWidthForNosepiecesDic = [[NSMutableDictionary alloc] init];
    
    if ([self isKindOfClass:[CZMicroscopeModelCompound class]]) {
        for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
            NSString *keyStringValue = [NSString stringWithFormat:@"%lu", (unsigned long)position];
            
            NSNumber *fieldOfView = [self.nosepiece fieldOfViewAtPosition:position];
            [fovWidthForNosepiecesDic setObject:fieldOfView ?: @(kNonCalibratedValue) forKey:keyStringValue];
            
            CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
            [objectivesForNosepiecesDic setObject:objective.matID ?: @"" forKey:keyStringValue];
        }
    }
    
    if ([self isKindOfClass:[CZMicroscopeModelStereo class]] && self.type == kAxiocamStereo) {
        for (NSUInteger position = 0; position < [self zoomClickStopPositions]; position++) {
            NSString *keyStringValue = [NSString stringWithFormat:@"%lu", (unsigned long)position];
            
            NSUInteger zoomClickStopID = [self zoomClickStopIDAtPosition:position];
            [objectivesForNosepiecesDic setObject:@(zoomClickStopID) forKey:keyStringValue];
        }
    }
    
    [dictionary setObject:objectivesForNosepiecesDic forKey:kDefaultNosepiece];
    [objectivesForNosepiecesDic release];
    
    [dictionary setObject:fovWidthForNosepiecesDic forKey:kDefaultCalibratedValues];
    [fovWidthForNosepiecesDic release];
    
    // clear original data
    [dictionary removeObjectsForKeys:@[kZoomTypeStereoObjective,
                                      kZoomTypeCameraAdapter,
                                      kZoomTypeEyepiece,
                                      kZoomTypeAdditionalCamera,
                                      kZoomTypeAdditionalEyepiece,
                                       kZoomTypeAdditionalOptovar]];
    
    // save type zooms
    for (NSString *type in _typeZooms.allKeys) {
        CZZoomLevel *zoomLevel = _typeZooms[type];
        
        [dictionary setObject:@(zoomLevel.magnification) forKey:type];
    }
    
    // save additional zooms
    for (NSString *type in _additionalZooms.allKeys) {
        NSNumber *zoom = _additionalZooms[type];
        [dictionary setObject:zoom forKey:type];
    }
}

- (void)loadSlotsFromDictionary:(NSDictionary *)dictionary {
    if ([self isKindOfClass:[CZMicroscopeModelCompound class]]) {
        for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
            [self.nosepiece setObjective:nil atPosition:position];
        }
    }
    
    if ([self isKindOfClass:[CZMicroscopeModelStereo class]] && self.type == kAxiocamStereo) {
        [self removeAllZoomClickStops];
    }
    
    NSDictionary *defaultNosepieces = [dictionary objectForKey:kDefaultNosepiece];
    if ([defaultNosepieces isKindOfClass:[NSDictionary class]] && defaultNosepieces.count) {
        if ([self isKindOfClass:[CZMicroscopeModelCompound class]]) {
            for (NSString *keyStringValue in defaultNosepieces.allKeys) {
                NSString *objectiveMatID = defaultNosepieces[keyStringValue];
                if ([objectiveMatID isKindOfClass:[NSString class]]) {
                    CZObjective *objective = [[CZObjective alloc] initWithMatID:objectiveMatID];
                    [self.nosepiece setObjective:objective atPosition:keyStringValue.intValue];
                } else if ([objectiveMatID isKindOfClass:[NSNumber class]]) {   // backward compatible
                    CZPrimoObjective enumeration = ((NSNumber *)objectiveMatID).unsignedIntegerValue;
                    CZObjective *objective = [CZObjective objectiveFromEnumeration:enumeration];
                    [self.nosepiece setObjective:objective atPosition:keyStringValue.intValue];
                }
            }
            
            NSNumber *value = dictionary[kCurrentNosepiece];
            if ([value isKindOfClass:[NSNumber class]]) {
                self.nosepiece.currentPosition = [value unsignedIntegerValue];
            }
        }
        
        if ([self isKindOfClass:[CZMicroscopeModelStereo class]] && self.type == kAxiocamStereo) {
            for (NSString *keyStringValue in defaultNosepieces.allKeys) {
                NSNumber *value = defaultNosepieces[keyStringValue];
                if ([value isKindOfClass:[NSNumber class]]) {
                    NSUInteger zoomLevelID = [value unsignedIntegerValue];
                    CZZoomLevel *zoomLevel = [[self availableZoomLevelSet] zoomLevelAtIndex:zoomLevelID ofType:kZoomTypeZoomClickStop];
                    if (zoomLevel) {
                        [self addZoomClickStop:zoomLevel];
                    }
                }
            }
            
            NSNumber *value = dictionary[kCurrentNosepiece];
            if ([value isKindOfClass:[NSNumber class]]) {
                self.currentZoomClickStopPosition = [value unsignedIntegerValue];
            }
        }
    } else {
        self.nosepiece.currentPosition = NSNotFound;
    }
    
    NSDictionary *defaultCalibratedValues = [dictionary objectForKey:kDefaultCalibratedValues];
    if ([defaultCalibratedValues isKindOfClass:[NSDictionary class]]) {
        for (NSString *keyStringValue in defaultNosepieces.allKeys) {
            NSNumber *fov = defaultCalibratedValues[keyStringValue];
            if ([fov isKindOfClass:[NSNumber class]]) {
                [self setFOVWidth:[fov floatValue] atPosition:[keyStringValue intValue]];
            }
        }
    } else {
        for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
            [self setFOVWidth:kNonCalibratedValue atPosition:position];
        }
    }
    
    // load type zooms
    for (NSString *type in @[kZoomTypeStereoObjective,
                             kZoomTypeCameraAdapter,
                             kZoomTypeEyepiece]) {
        NSNumber *zoom = [dictionary objectForKey:type];
        if ([zoom isKindOfClass:[NSNumber class]] && [zoom floatValue] > 0) {
            CZZoomLevel *zoomLevel = [[self availableZoomLevelSet] zoomLevelType:type ofMagnification:[zoom floatValue]];
            if (zoomLevel) {
                [self setZoom:zoomLevel forType:type];
            }
        }
    }
    
    // save additional zooms
    self.additionalZooms = nil;
    for (NSString *type in @[kZoomTypeAdditionalCamera,
                             kZoomTypeAdditionalEyepiece,
                             kZoomTypeAdditionalOptovar]) {
        NSNumber *zoom = [dictionary objectForKey:type];
        if ([zoom isKindOfClass:[NSNumber class]] && [zoom floatValue] > 0) {
            [self setAdditionalZoom:[zoom floatValue] forType:type];
        }
    }
}

- (void)readFromDictionary:(NSDictionary *)dictionary {
    if (self.name == nil) {
        self.name = dictionary[kMicroscopeName];
    }
    self.pin = dictionary[kSecurityPIN];
    self.mnaMACAddress = dictionary[kMNAMACAddress];
    self.mnaIPAddress = dictionary[kMNAIPAddress];
    
    NSNumber *isDefault = dictionary[kIsDefault];
    self.isDefault = [isDefault boolValue];
    
    // Read per-objective shading correction settings.
    NSDictionary *shadingCorrectionData = dictionary[kShadingCorrectionData];
    for (NSString *key in shadingCorrectionData.allKeys) {
        NSDictionary *value = shadingCorrectionData[key];
        CZShadingCorrectionData *data = [[CZShadingCorrectionData alloc] initWithMaxMaskX:[value[kShadingCorrectMaxMaskX] floatValue]
                                                                                 maxMaskY:[value[kShadingCorrectMaxMaskY] floatValue]
                                                                                 maxMaskZ:[value[kShadingCorrectMaxMaskZ] floatValue]];
        self.shadingCorrectionData[key] = data;
        [data release];
    }
}

- (void)writeToDictionary:(NSMutableDictionary *)dictionary {
    dictionary[kMicroscopeName] = self.name ?: @"";
    dictionary[kSecurityPIN] = self.pin ?: @"";
    dictionary[kMNAMACAddress] = self.mnaMACAddress ?: kNULLMACAddress;
    dictionary[kMNAIPAddress] = self.mnaIPAddress ?: kNULLIPAddress;
    dictionary[kModelType] = @(self.type);
    dictionary[kIsDefault] = @(self.isDefault);
    
    NSMutableDictionary *shadingCorrectionData = [[NSMutableDictionary alloc] init];
    
    for (NSString *key in self.shadingCorrectionData.allKeys) {
        if (![key isEqualToString:[NSString stringWithFormat:@"%lu", (unsigned long)NSUIntegerMax]]) {
            CZShadingCorrectionData *data = self.shadingCorrectionData[key];
            shadingCorrectionData[key] = @{kShadingCorrectMaxMaskX: @(data.maxMaskX),
                                           kShadingCorrectMaxMaskY: @(data.maxMaskY),
                                           kShadingCorrectMaxMaskZ: @(data.maxMaskZ)};
        }
    }
    
    dictionary[kShadingCorrectionData] = shadingCorrectionData;
    [shadingCorrectionData release];
    
    for (NSString *key in self.shadingCorrectionMask.allKeys) {
        UIImage *mask = self.shadingCorrectionMask[key];
        
        key = [NSString stringWithFormat:@"%@_%@.jpg", self.cameraMACAddress, key];
        NSString *path = [[[NSFileManager defaultManager] cz_cachePath] stringByAppendingPathComponent:key];
        NSData *imageData = UIImageJPEGRepresentation(mask, 1.0);
        [imageData writeToFile:path atomically:YES];
    }
}

- (void)saveToCamera:(CZCamera *)camera withCompletionHandler:(void (^)(void))completionHandler {
    if (completionHandler) {
        completionHandler();
    }
}

- (BOOL)hasObjectivesAssigned {
    BOOL hasObjectives = NO;
    for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
        CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
        if (objective.matID.length > 0) {
            hasObjectives = YES;
            break;
        }
    }
    return hasObjectives;
}

+ (NSString *)modelNameOfType:(CZMicroscopeTypes)type {
    NSString *name = nil;
    switch (type) {
        case kPrimotech:
            name = kModelNamePrimotech;  // no need to translate
            break;
        case kPrimoStar:
            name = kModelNamePrimoStar;  // no need to translate
            break;
        case kPrimovert:
            name = kModelNamePrimovert;
            break;
        case kAxiocamCompound:
            name = kModelNameCompound;  // no need to translate
            break;
        case kAxiocamStereo:
            name = kModelNameStereo;  // no need to translate
            break;
        case kStemi305:
            name = kModelNameStemi305;
            break;
        case kStemi508:
            name = kModelNameStemi508;
            break;
        case kAxioscope:
            name = kModelNameAxioscope;
            break;
        case kAxiolab:
            name = kModelNameAxiolab;
            break;
        case kScanBarCodeModel:
            name = L(@"MIC_CONFIG_SCAN_QRCODE");
            break;
        default:
            break;
    }

    return name;
}

+ (UIImage *)modelIconOfType:(CZMicroscopeTypes)type {
    if (type == kScanBarCodeModel) {
        return [UIImage imageNamed:A(@"model-fromqrcode")];
    } else {
        Class c = [CZMicroscopeModel microscopeModelClassOfType:type];
        return [c modelIcon];        
    }
}

+ (NSString *)cameraModelNameOfType:(CZMicroscopeTypes)type {
    Class c = [CZMicroscopeModel microscopeModelClassOfType:type];
    return [c cameraModelName];
}

+ (BOOL)shouldPreferBigUnitByModelName:(NSString *)modelName {
    if ([modelName isEqualToString:kModelNameStereo] ||
        [modelName isEqualToString:kModelNameStemi305] ||
        [modelName isEqualToString:kModelNameStemi508] ||
        [modelName isEqualToString:kModelNameMacroImage]) {
        return YES;
    } else {
        return NO;
    }
}

- (CZZoomLevel *)currentZoomClickStop {
    return [self zoomClickStopAtPosition:self.currentZoomClickStopPosition];
}

- (NSUInteger)currentZoomClickStopID {
    return [self zoomClickStopIDAtPosition:self.currentZoomClickStopPosition];
}

- (NSUInteger)zoomClickStopPositions {
    NSArray<CZZoomLevel *> *zoomClickStops = [[self availableZoomLevelSet] availableZoomsForType:kZoomTypeZoomClickStop];
    return zoomClickStops.count;
}

- (CZZoomLevel *)zoomClickStopAtPosition:(NSUInteger)position {
    NSArray<CZZoomLevel *> *zoomClickStops = [[self availableZoomLevelSet] availableZoomsForType:kZoomTypeZoomClickStop];
    return position < zoomClickStops.count ? zoomClickStops[position] : nil;
}

- (NSUInteger)zoomClickStopIDAtPosition:(NSUInteger)position {
    return position;
}

- (void)addZoomClickStop:(CZZoomLevel *)zoomClickStop {
}

- (void)addZoomClickStopID:(NSUInteger)zoomClickStopID {
}

- (void)removeZoomClickStop:(CZZoomLevel *)zoomClickStop {
}

- (void)removeZoomClickStopForID:(NSUInteger)zoomClickStopID {
}

- (void)removeZoomClickStopAtPosition:(NSUInteger)position {
}

- (void)removeAllZoomClickStops {
}

@end

@implementation CZMicroscopeModel (WebsiteAddress)

- (NSString *)localedWebsiteAddress {
    NSString *locale = nil;
    switch (self.type) {
        case kPrimotech: {
            locale = @"en";
            
            NSSet *avaiableLocales = [[NSSet alloc] initWithArray:@[@"JP", @"DE", @"FR", @"ES", @"BR", @"CN"]];
            NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
            
            if ([avaiableLocales containsObject:countryCode]) {
                locale = [countryCode lowercaseString];
            } else {
                NSString *languageCode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
                languageCode = [languageCode uppercaseString];
                if ([avaiableLocales containsObject:languageCode]) {
                    locale = [languageCode lowercaseString];
                }
            }
            [avaiableLocales release];
            break;
        }
        default: {
            locale = @"en_de"; // default locale is "en_de"
            NSSet *avaiableLocales = [[NSSet alloc] initWithArray:@[@"de_de",
                                                                    @"en_gb",
                                                                    @"en_us",
                                                                    @"en_au",
                                                                    @"en_sg",
                                                                    @"en_in",
                                                                    @"en_ca",
                                                                    @"en_za",
                                                                    @"es_es",
                                                                    @"es_mx",
                                                                    @"fr_fr",
                                                                    @"fr_ca",
                                                                    @"ko_kr",
                                                                    @"ja_jp",
                                                                    @"pt_br",
                                                                    @"zh_cn"]];
            NSString * currentLocaleID = [[[NSLocale currentLocale] localeIdentifier] lowercaseString];
            if ([avaiableLocales containsObject:currentLocaleID]) {
                locale = currentLocaleID;
            }
            [avaiableLocales release];
            break;
        }
    }
    
    NSString *address = nil;
    switch (self.type) {
        case kPrimoStar: {
            address = [NSString stringWithFormat:@"http://www.zeiss.com/microscopy/%@/products/light-microscopes/primo-star.html", locale];
            break;
        }
        case kPrimovert: {
            address = [NSString stringWithFormat:@"http://www.zeiss.com/microscopy/%@/products/light-microscopes/primovert.html#primovert-hdcam", locale];
            break;
        }
        case kStemi305: {
            address = [NSString stringWithFormat:@"http://www.zeiss.com/microscopy/%@/products/stereo-zoom-microscopes/stemi-305.html", locale];
            break;
        }
        case kPrimotech: {
            address = [NSString stringWithFormat:@"http://www.zeiss.com/primotech_%@", locale];
            break;
        }
        case kAxiocamStereo:
        case kAxiocamCompound: {
            address = [NSString stringWithFormat:@"http://www.zeiss.com/microscopy/%@/products/microscope-cameras/axiocam-erc-5s.html", locale];
            break;
        }
        default: {
            address = @"";
            break;
        }
    }
    
    return address;
}

- (BOOL)hasLocaledMicroShop {
    NSSet *avaiableLocales = [[NSSet alloc]initWithArray:@[@"en_US", @"en_GB",
                                                           @"de_DE", @"de_AT", @"de_CH", @"de_LU",
                                                           @"fr_FR", @"fr_BE", @"fr_LU", @"fr_CH",
                                                           @"es_ES"]];
    
    NSString * const currentLocaleID = [[NSLocale currentLocale] localeIdentifier];
    BOOL hasLocale = [avaiableLocales containsObject:currentLocaleID];
    [avaiableLocales release];
    return hasLocale;
}

- (NSString *)localedMicroShopAddress {
    NSString *addressFormat = @"https://www.micro-shop.zeiss.com/?l=%@&p=%@&f=e&i=10128";
    
    NSString * const currentLocaleID = [[NSLocale currentLocale] localeIdentifier];
    NSString *locale = @"us";
    NSString *language = @"en";
    
    if ([currentLocaleID isEqualToString:@"en_GB"]) {
        locale = @"uk";
    } else if ([currentLocaleID isEqualToString:@"de_AT"]) {
        language = @"de";
        locale = @"at";
    } else if ([currentLocaleID isEqualToString:@"de_CH"]) {
        language = @"de";
        locale = @"ch";
    } else if ([currentLocaleID isEqualToString:@"de_LU"]) {
        language = @"de";
        locale = @"lu";
    } else if ([currentLocaleID isEqualToString:@"de_DE"]){
        language = @"de";
        locale = @"de";
    } else if ([currentLocaleID isEqualToString:@"fr_BE"]) {
        language = @"fr";
        locale = @"be";
    } else if ([currentLocaleID isEqualToString:@"fr_LU"]) {
        language = @"fr";
        locale = @"lu";
    } else if ([currentLocaleID isEqualToString:@"fr_CH"]) {
        language = @"fr";
        locale = @"ch";
    } else if ([currentLocaleID isEqualToString:@"fr_FR"]) {
        language = @"fr";
        locale = @"fr";
    } else if ([currentLocaleID isEqualToString:@"es_ES"]) {
        language = @"es";
        locale = @"es";
    }
    
    return [NSString stringWithFormat:addressFormat, language, locale];
}

@end

@implementation CZMicroscopeModel (ForSubclassEyesOnly)

@dynamic cameraClass;

+ (void)registerMicroscopeModelClass:(Class)modelClass forType:(CZMicroscopeTypes)type {
    if (subModelClasses == nil) {
        subModelClasses = [[NSMutableDictionary alloc] init];
    }
    
    [subModelClasses setObject:modelClass forKey:@(type)];
}

- (NSArray<CZObjective *> *)availableObjectives {
    return [CZObjective allObjectives];
}

- (NSUInteger)defaultNosepiecePositions {
    return 0;
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    return nil;
}

+ (UIImage *)modelIcon {
    return nil;
}

- (void)initTypeZooms:(NSMutableDictionary *)typeZooms {
}

- (void)initAdditionalZooms:(NSMutableDictionary *)additionalZooms {
}

@end

@implementation CZMicroscopeModelCompound

- (float)magnificationAtPosition:(NSUInteger)position {
    float magnification = [[self.nosepiece objectiveAtPosition:position] magnification];
    return magnification;
}

- (float)cameraMagnificationAtPosition:(NSUInteger)position {
    float magnification = [[self.nosepiece objectiveAtPosition:position] magnification];
    CZZoomLevel *cameraAdapter = [self zoomForType:kZoomTypeCameraAdapter];
    if (cameraAdapter) {
        magnification *= cameraAdapter.magnification;
    }
    
    NSNumber *additionalMagnificationFactor = [self additionalZoomForType:kZoomTypeAdditionalOptovar];
    if (additionalMagnificationFactor && [additionalMagnificationFactor floatValue] > 0) {
        magnification *= [additionalMagnificationFactor floatValue];
    }
    
    return magnification;
}

@end

@implementation CZMicroscopeModelStereo

+ (NSArray *)zoomLevelsByReplacing1xWithNone:(NSArray *)zoomLevels {
    NSUInteger index = [zoomLevels indexOfObjectPassingTest:^(id obj, NSUInteger idx, BOOL *stop) {
        CZZoomLevel *zoomLevel = (CZZoomLevel *)obj;
        if (zoomLevel.magnification == 1.0) {
            *stop = YES;
            return YES;
        } else {
            return NO;
        }
    }];
    
    if (index == NSNotFound) {
        return zoomLevels;
    }
    
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:zoomLevels];
    CZZoomLevel *noneZoomLevel = [[CZZoomLevel alloc] initWithMagnification:1.0 name:@"NONE" displayName:L(@"NONE")];
    [tempArray replaceObjectAtIndex:index withObject:noneZoomLevel];
    [noneZoomLevel release];
    
    return [NSArray arrayWithArray:tempArray];
}

- (BOOL)hasObjectivesAssigned {
    return (self.nosepiece.positions > 0);
}

- (float)currentFOVWidth {
    return [self fovWidthAtPosition:self.currentZoomClickStopPosition];
}

- (float)magnificationAtPosition:(NSUInteger)position {
    float magnification = [[self zoomClickStopAtPosition:position] magnification];
    CZZoomLevel *stereoObjective = [self zoomForType:kZoomTypeStereoObjective];
    if (stereoObjective) {
        magnification *= stereoObjective.magnification;
    }
    return magnification;
}

- (float)cameraMagnificationAtPosition:(NSUInteger)position {
    float magnification = [[self zoomClickStopAtPosition:position] magnification];
    CZZoomLevel *stereoObjective = [self zoomForType:kZoomTypeStereoObjective];
    if (stereoObjective) {
        magnification *= stereoObjective.magnification;
    }
    
    CZZoomLevel *cameraAdapter = [self zoomForType:kZoomTypeCameraAdapter];
    if (cameraAdapter) {
        magnification *= cameraAdapter.magnification;
    }
    
    NSNumber *additionalMagnification = [self additionalZoomForType:kZoomTypeAdditionalCamera];
    if (additionalMagnification) {
        magnification *= [additionalMagnification doubleValue];
    }
    
    return magnification;
}

- (float)totalMagnificationAtPosition:(NSUInteger)position {
    float totalMagnification = [[self zoomClickStopAtPosition:position] magnification];
    CZZoomLevel *stereoObjective = [self zoomForType:kZoomTypeStereoObjective];
    if (stereoObjective) {
        totalMagnification *= stereoObjective.magnification;
    }
    
    CZZoomLevel *cameraAdapter = [self zoomForType:kZoomTypeEyepiece];
    if (cameraAdapter) {
        totalMagnification *= cameraAdapter.magnification;
    }
    
    NSNumber *additionalMagnification = [self additionalZoomForType:kZoomTypeAdditionalEyepiece];
    if (additionalMagnification) {
        totalMagnification *= [additionalMagnification doubleValue];
    }
    
    return totalMagnification;
}

- (NSString *)totalMagnificationStringAtPosition:(NSUInteger)position localized:(BOOL)isLocalized {
    float zoom = [self totalMagnificationAtPosition:position];
    
    if (zoom >= kMicroscopeModelMinZoom) {
        NSString *zoomString = nil;
        if (isLocalized) {
            zoomString = [CZCommonUtils localizedStringFromFloat:zoom precision:2];
        } else {
            zoomString = [CZCommonUtils stringFromFloat:zoom precision:2];
        }
        return [zoomString stringByAppendingString:@"x"];
    } else {
        return kNotAvailableMagnification;
    }
}

@end

@implementation CZMicroscopeModelMacroImage

- (NSString *)modelName {
    return kModelNameMacroImage;
}

- (NSString *)cameraModelName {
    return kCameraModelNameBuiltIn;
}

@end
