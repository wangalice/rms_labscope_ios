//
//  CZZoomLevel.h
//  Labscope
//
//  Created by Ralph Jin on 4/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZZoomLevel : NSObject

- (id)initWithMagnification:(float)magnification;

- (id)initWithMagnification:(float)magnification
                       name:(NSString *)name;

- (id)initWithMagnification:(float)magnification
                       name:(NSString *)name
                displayName:(NSString *)displayName;

@property (nonatomic, assign, readonly) float magnification;

@property (nonatomic, copy, readonly) NSString *displayMagnification;

@property (nonatomic, copy, readonly) NSString *name;

@property (nonatomic, copy, readonly) NSString *displayName;

- (NSComparisonResult)compare:(CZZoomLevel *)zoomLevel;

@end
