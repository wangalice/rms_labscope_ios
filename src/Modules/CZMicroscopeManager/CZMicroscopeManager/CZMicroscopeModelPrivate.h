//
//  CZMicroscopeModelPrivate.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

@class CZShadingCorrectionData;

@interface CZMicroscopeModel ()

@property (nonatomic, readwrite, copy) CZNosepiece *nosepiece;

@property (nonatomic, copy) NSString *mnaMACAddress;
@property (nonatomic, copy) NSString *mnaIPAddress;

@property (nonatomic, copy) NSString *shadingCorrectionMaskPath;
@property (nonatomic, retain) CZShadingCorrectionData *shadingCorrectionMaskData;
@property (nonatomic, retain) NSMutableDictionary *shadingCorrectionUserInfo;

@property (nonatomic, retain, readwrite) NSMutableDictionary *shadingCorrectionData; // dictionary of CZShadingCorrectionData
@property (nonatomic, retain, readwrite) NSMutableDictionary *shadingCorrectionMask; // dictionary of UIImage, key is zoomLevelID

@end
