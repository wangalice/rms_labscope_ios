//
//  CZNosepiece.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 3/5/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString * const CZNosepieceDidUpdateNotification;

@interface CZObjective : NSObject <NSCopying>

@property (nonatomic, readonly, copy) NSString *matID;                  ///< Like `440020-0000-000`
@property (nonatomic, readonly, copy) NSString *objectiveClass;         ///< Like `Achroplan`
@property (nonatomic, readonly, copy) NSString *objectiveName;          ///< Like `Achroplan 4x/0.10`
@property (nonatomic, readonly, copy) NSString *displayName;            ///< Like `440020-0000-000 - Achroplan 4x/0.10`
@property (nonatomic, readonly, assign) float magnification;            ///< Like `4`
@property (nonatomic, readonly, copy) NSString *displayMagnification;   ///< Like `4x`
@property (nonatomic, readonly, assign) float aperture;                 ///< Like `0.10`
@property (nonatomic, readonly, assign) BOOL isNone;

+ (instancetype)none;
+ (NSArray<CZObjective *> *)allObjectives;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithMatID:(NSString *)matID;
- (instancetype)initWithMatID:(NSString *)matID
                objectiveName:(NSString *)objectiveName;
- (instancetype)initWithMatID:(NSString *)matID
               objectiveClass:(NSString *)objectiveClass
                objectiveName:(NSString *)objectiveName NS_DESIGNATED_INITIALIZER;

- (BOOL)isEqualToObjective:(CZObjective *)otherObjective;
- (NSComparisonResult)compare:(CZObjective *)otherObjective;

@end

@interface CZNosepiece : NSObject <NSCopying>

@property (nonatomic, copy) NSArray<CZObjective *> *availableObjectives;    ///< Read from Objective.json as default
@property (nonatomic, assign) NSUInteger positions;                         ///< Change positions will change position data
@property (nonatomic, assign) NSUInteger currentPosition;                   ///< NSNotFound as default

- (BOOL)isEqualToNosepiece:(CZNosepiece *)otherNosepiece;

- (CZObjective *)objectiveAtCurrentPosition;
- (CZObjective *)objectiveAtPosition:(NSUInteger)position;
- (void)setObjective:(CZObjective *)objective atPosition:(NSUInteger)position;

@end

@interface UIColor (CZObjectiveMagnification)

+ (instancetype)colorForMagnification:(float)magnification;

@end
