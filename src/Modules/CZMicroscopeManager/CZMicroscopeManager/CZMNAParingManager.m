//
//  CZMNAParingManager.m
//  Matscope
//
//  Created by Mike Wang on 9/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMNAParingManager.h"
#import "CZMNABrowser.h"
#import "CZMicroscopeModel.h"
#import "CZMicroscopeModel+MNA.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

@interface CZMNAParingManager () <CZMNABrowserDelegate, CZMicroscopeModelUpdateInfoFromMNADelegate> {
    NSUInteger _waitingMNACount;
    NSUInteger _searchCount;
}

@property (atomic, retain, readonly) NSMutableDictionary *mnaList;
@property (atomic, retain, readonly) NSMutableDictionary *modelList;
@property (atomic, copy) CZMicroscopeModel *requestModel;
@property (atomic, copy) CZMNA *mnaPairedWithRequestModel;

@end

@implementation CZMNAParingManager
@synthesize mnaList = _mnaList;

- (id)init {
    self = [super init];
    if (self) {
        _modelList = [[NSMutableDictionary alloc] init];
        [[CZMNABrowser sharedInstance] setDelegate:self];
    }
    
    return self;
}

- (void)dealloc {
    self.delegate = nil;
    [[CZMNABrowser sharedInstance] setDelegate:nil];
    
    @synchronized (_mnaList) {
        [_mnaList release];
        [_modelList release];
        [_requestModel release];
    }
    
    [_mnaPairedWithRequestModel release];
    
    [super dealloc];
}

- (NSMutableDictionary *)mnaList {
    if (!_mnaList) {
        NSDictionary *mnaList = [[CZMNABrowser sharedInstance] newMNAList];
        _mnaList = [[NSMutableDictionary alloc] initWithDictionary:mnaList];
        [mnaList release];
    }
    return _mnaList;
}

- (void)searchMNAPairedWithModelAsync:(CZMicroscopeModel *)model {
    self.mnaPairedWithRequestModel = nil;
    self.requestModel = model;

    @synchronized (_mnaList) {
        if (self.mnaList.count == 0) {
            if ([self.delegate respondsToSelector:@selector(mnaPairingManager:didFindPairedMNA:)]) {
                [self.delegate mnaPairingManager:self didFindPairedMNA:nil];
            }
        }
        
        @synchronized (self) {
            _waitingMNACount += self.mnaList.count;
        }
        
        for (CZMNA *mna in self.mnaList.allValues) {
            CZMicroscopeModel *queryModel = [[[model class] alloc] init];
            queryModel.mnaMACAddress = mna.macAddress;
            queryModel.mnaIPAddress = mna.ipAddress;
            [queryModel newMicroscopeInfoFromMNAAsync:mna delegate:self];
            [queryModel release];
        }
    }
}

- (NSDictionary *)newMNAList {
    return [_mnaList copy];
}

- (NSDictionary *)newPairedModelList {
    return [_modelList copy];
}

- (void)resetMNA:(CZMNA *)mna completionHandler:(void(^)(NSError *connectionError))completionHandler {
    CZMicroscopeModel *model = self.modelList[mna.macAddress];
    if (model) {
        [model resetMNA:completionHandler];
        [self.modelList removeObjectForKey:mna.macAddress];
    } else {
        NSLog(@"ERROR! Try to reset MNA, but no microscope model is paired.");
    }
}

- (void)pairMNA:(CZMNA *)mna microscope:(CZMicroscopeModel *)microscope {
    if (mna.macAddress == nil || microscope == nil) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"Pair MNA with microscope with invalid parameters! MNA macAddress is nil or microscope model is nil."
                                     userInfo:nil];
    }
    
    self.modelList[mna.macAddress] = microscope;
}

- (void)unpairMNA:(CZMNA *)mna {
    CZMicroscopeModel *model = self.modelList[mna.macAddress];
    if (model) {
        model.mnaIPAddress = nil;
        model.mnaMACAddress = nil;
        
        [self.modelList removeObjectForKey:mna.macAddress];
    } else {
        NSLog(@"ERROR! Try to unpair MNA, but no microscope model is paired.");
    }
}

- (void)tryMatchRequestModelWithMNA:(CZMNA *)mna modelForMNA:(CZMicroscopeModel *)modelForMNA{
    BOOL isCameraMACAddressEqual = [modelForMNA.cameraMACAddress isEqualComparisionWithCaseInsensitive:self.requestModel.cameraMACAddress];
    if (!isCameraMACAddressEqual) {
        //Does not match with request model
        return;
    }
    
    //Match with request model
    if (self.mnaPairedWithRequestModel &&
        ![self.mnaPairedWithRequestModel.macAddress isEqualToString:mna.macAddress]) {
        // Found one paired model before, need to check which one to keep
        // only keep the one that is consistent with the microscope model.
        
        if (![self.mnaPairedWithRequestModel.macAddress isEqualToString:self.requestModel.mnaMACAddress]) {
            [self resetMNA:self.mnaPairedWithRequestModel completionHandler:NULL];
            self.mnaPairedWithRequestModel = mna;
        } else {
            [self resetMNA:mna completionHandler:NULL];
        }
    } else {
        self.mnaPairedWithRequestModel = mna;
    }
}

- (void)updateModelListWithModel:(CZMicroscopeModel *)newModel mna:(CZMNA *)mna {
    @synchronized (_modelList) {
        if (!newModel) {
            [self.modelList removeObjectForKey:mna.macAddress];
            return;
        }
        newModel.mnaMACAddress = mna.macAddress;
        newModel.mnaIPAddress = mna.ipAddress;

        //Look up for camera name
        for (CZCamera *camera in [[CZCameraBrowser sharedInstance] cameras]) {
            BOOL isEqualMACAddress = [camera.macAddress isEqualComparisionWithCaseInsensitive:newModel.cameraMACAddress];
            if (isEqualMACAddress) {
                newModel.name = camera.displayName;
                break;
            }
        }
        
        self.modelList[mna.macAddress] = newModel;
        if (self.requestModel) {
            [self tryMatchRequestModelWithMNA:mna modelForMNA:newModel];
        }
    }
}

#pragma mark -
#pragma mark CZMNABrowserDelegate Methods
- (void)mnaBrowserDidFindMNAs:(NSArray *)mnas {
    NSMutableArray *newMNAs = [[NSMutableArray alloc] init];
    @synchronized (_mnaList) {
        if (self.mnaList.count > 0) {
            for (CZMNA *foundMNA in mnas) {
                if (![self.mnaList objectForKey:foundMNA.macAddress]) {
                    [newMNAs addObject:foundMNA];
                }
            }
        } else {
            [newMNAs release];
            newMNAs = [mnas copy];
        }
        
        for (CZMNA *mna in newMNAs) {
            CZMicroscopeModel *queryModel = [[CZMicroscopeModel alloc] init];
            queryModel.mnaMACAddress = mna.macAddress;
            queryModel.mnaIPAddress = mna.ipAddress;
            [queryModel newMicroscopeInfoFromMNAAsync:mna delegate:self];
            [queryModel release];
            self.mnaList[mna.macAddress] = mna;
        }
        
        [newMNAs release];
    }
}

- (void)mnaBrowserDidLoseMNAs:(NSArray *)mnas {
    NSMutableArray *lostMNAs = [[NSMutableArray alloc] init];
    
    @synchronized (_mnaList) {
        for (CZMNA *lostMNA in mnas) {
            if ([self.mnaList objectForKey:lostMNA.macAddress]) {
                [lostMNAs addObject:lostMNA];
            }
        }
        
        for (CZMNA *mna in lostMNAs) {
            [self.mnaList removeObjectForKey:mna.macAddress];
            [self.modelList removeObjectForKey:mna.macAddress];
        }
        
        [lostMNAs release];
    }
}

#pragma mark -
#pragma mark CZMicroscopeModelUpdateInfoFromMNADelegate Methods

- (void)microscopeModel:(CZMicroscopeModel *)queryModel
          hasUpdateInfo:(CZMicroscopeModel *)newModel
                fromMNA:(CZMNA *)mna {
    [self updateModelListWithModel:newModel mna:mna];
    
    BOOL noMoreWaiting = NO;
    @synchronized (self) {
        _searchCount++;
        if (_searchCount >= _waitingMNACount) {
            _searchCount = 0;
            _waitingMNACount = 0;
            noMoreWaiting = YES;
        }
    }
    
    if (self.requestModel && noMoreWaiting) {
        self.requestModel = nil;
        
        if ([self.delegate respondsToSelector:@selector(mnaPairingManager:didFindPairedMNA:)]) {
            [self.delegate mnaPairingManager:self didFindPairedMNA:self.mnaPairedWithRequestModel];
        }
    }
}

- (void)microscopeModel:(CZMicroscopeModel *)myModel
   retrieveNewObjective:(NSUInteger)objective {
    return;
}

@end
