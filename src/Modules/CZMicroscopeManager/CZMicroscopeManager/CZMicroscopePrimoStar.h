//
//  CZMicroscopePrimoStar.h
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

@interface CZMicroscopePrimoStar : CZMicroscopeModelCompound

@property (nonatomic, assign) CZPrimoStarPackage package;

+ (NSArray<CZObjective *> *)objectivesInPackage:(CZPrimoStarPackage)package;

@end
