//
//  CZMicroscopeAxioline.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAxioline.h"
#import "CZMicroscopeModelSubclass.h"
#import <CZCameraInterface/CZCameraInterface.h>

const double CZAxiocam202PixelSize = 5.86;  // CCD sensor for gemini 202 camera width in um.
const double CZAxiocam208PixelSize = 1.85;  // CCD sensor for gemini 208 camera width in um.
const double CZAxiocam202FOV = CZAxiocam202PixelSize * 1920;
const double CZAxiocam208FOV = CZAxiocam208PixelSize * 3840;

@interface CZMicroscopeAxioline ()

@property (nonatomic, retain) CZGeminiAPISystemInfo *systemInfo;

@end

@implementation CZMicroscopeAxioline

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithMicroscopeModel:microscopeModel];
    if (self) {
        if ([microscopeModel isKindOfClass:[CZMicroscopeAxioline class]]) {
            _systemInfo = [((CZMicroscopeAxioline *)microscopeModel).systemInfo retain];
        }
    }
    return self;
}

- (void)dealloc {
    [_systemInfo release];
    [super dealloc];
}

- (NSString *)modelName {
    NSString *modelName = nil;
    NSInteger series = 0;
    NSString *microscopeName = self.systemInfo.microscope.microscopeName;
    NSScanner *scanner = [NSScanner scannerWithString:microscopeName];
    [scanner scanUpToString:@" " intoString:&modelName];
    if ([scanner scanInteger:&series]) {
        modelName = [modelName stringByAppendingFormat:@" %ld", (long)series];
    }
    return modelName;
}

- (UIImage *)modelIcon {
    NSString *microscopeName = self.systemInfo.microscope.microscopeName;
    if ([microscopeName hasPrefix:@"Axioscope"]) {
        switch (self.systemInfo.camera.modelType) {
            case CZGeminiAPICameraModelTypeUnknown:
                return nil;
            case CZGeminiAPICameraModelTypeAxiocam202:
                return [UIImage imageNamed:A(@"model-axioscope-axiocam-202")];
            case CZGeminiAPICameraModelTypeAxiocam208:
                return [UIImage imageNamed:A(@"model-axioscope-axiocam-208")];
        }
    } else if ([microscopeName hasPrefix:@"Axiolab"]) {
        switch (self.systemInfo.camera.modelType) {
            case CZGeminiAPICameraModelTypeUnknown:
                return nil;
            case CZGeminiAPICameraModelTypeAxiocam202:
                return [UIImage imageNamed:A(@"model-axiolab-axiocam-202")];
            case CZGeminiAPICameraModelTypeAxiocam208:
                return [UIImage imageNamed:A(@"model-axiolab-axiocam-208")];
        }
    } else {
        return nil;
    }
}

- (void)updateDataFromCamera:(CZCamera *)camera {
    [super updateDataFromCamera:camera];
    
    if (![camera isKindOfClass:[CZGeminiCamera class]]) {
        return;
    }
    
    CZGeminiAPISystemInfo *systemInfo = ((CZGeminiCamera *)camera).systemInfo;
    self.systemInfo = systemInfo;
    
    CZZoomLevelSet *zoomLevelSet = [self availableZoomLevelSet];
    NSArray *zoomLevels = [zoomLevelSet availableZoomsForType:kZoomTypeCameraAdapter];
    for (CZZoomLevel *zoomLevel in zoomLevels) {
        if (zoomLevel.magnification == systemInfo.microscope.cameraAdapter) {
            [self setZoom:zoomLevel forType:kZoomTypeCameraAdapter];
            break;
        }
    }
    
    CZGeminiAPIObjectivesInfo *objectivesInfo = systemInfo.microscope.objective;
    self.nosepiece.positions = objectivesInfo.positionCount;
    self.nosepiece.currentPosition = objectivesInfo.position;
    for (NSInteger position = 0; position < objectivesInfo.objectives.count && position < objectivesInfo.positionCount; position++) {
        CZGeminiAPIObjectiveInfo *objectiveInfo = objectivesInfo.objectives[position];
        CZObjective *objective = [[CZObjective alloc] initWithMatID:objectiveInfo.matID];
        [self.nosepiece setObjective:objective atPosition:position];
    }
    
    CZGeminiAPIReflectorsInfo *reflectorsInfo = systemInfo.microscope.reflector;
    self.reflector.positions = reflectorsInfo.positionCount;
    self.reflector.currentPosition = reflectorsInfo.position;
    for (NSInteger position = 0; position < reflectorsInfo.reflectors.count && position < reflectorsInfo.positionCount; position++) {
        CZGeminiAPIReflectorInfo *reflectorInfo = reflectorsInfo.reflectors[position];
        CZFilterSet *filterSet = [[CZFilterSet alloc] initWithMatID:reflectorInfo.matID];
        [self.reflector setFilterSet:filterSet atPosition:position];
    }
}

- (BOOL)hasReflector {
    return self.systemInfo.microscope.hasReflector;
}

- (void)loadSlotsFromDefaults {
    // Only load slots from camera.
}

- (void)saveToCamera:(CZCamera *)camera withCompletionHandler:(void (^)(void))completionHandler {
    if (![camera isKindOfClass:[CZGeminiCamera class]]) {
        [super saveToCamera:camera withCompletionHandler:completionHandler];
        return;
    }
    
    CZGeminiAPISession *apiSession = [CZGeminiAPISession serialAPISession];
    
    [apiSession addAPI:[CZGeminiAPI apiForSetCameraName:self.name]];
    
    CZZoomLevel *zoomLevel = [self zoomForType:kZoomTypeCameraAdapter];
    [apiSession addAPI:[CZGeminiAPI apiForSetCameraAdapter:zoomLevel.magnification]];
    
    for (NSInteger position = 0; position < self.nosepiece.positions; position++) {
        CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
        CZGeminiAPIObjectiveInfo *objectiveInfo = [[CZGeminiAPIObjectiveInfo alloc] init];
        objectiveInfo.matID = objective.matID ?: @"0";
        objectiveInfo.objectiveClass = objective.objectiveClass ?: @"";
        objectiveInfo.magnification = @(objective.magnification);
        CZGeminiAPI *api = [CZGeminiAPI apiForSetObjectiveInfo:objectiveInfo atPosition:position + 1];
        [apiSession addAPI:api];
    }
    
    for (NSInteger position = 0; position < self.reflector.positions; position++) {
        CZFilterSet *filterSet = [self.reflector filterSetAtPosition:position];
        CZGeminiAPIReflectorInfo *reflectorInfo = [[CZGeminiAPIReflectorInfo alloc] init];
        reflectorInfo.matID = filterSet.matID ?: @"0";
        CZGeminiAPI *api = [CZGeminiAPI apiForSetReflectorInfo:reflectorInfo atPosition:position + 1];
        [apiSession addAPI:api];
    }
    
    CZGeminiAPIClient *apiClient = ((CZGeminiCamera *)camera).apiClient;
    [apiClient commitAPISession:apiSession withCompletionHandler:^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [camera prefetchRequiredCameraInfo];
            dispatch_async(dispatch_get_main_queue(), completionHandler);
        });
    } afterDelay:4.0];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet setForAxioline] retain];
    }
    return zoomLevelSet;
}

- (NSUInteger)defaultNosepiecePositions {
    return 6;
}

- (NSString *)cameraModelName {
    switch (self.systemInfo.camera.modelType) {
        case CZGeminiAPICameraModelTypeUnknown:
            return nil;
        case CZGeminiAPICameraModelTypeAxiocam202:
            return kCameraModelNameAxiocam202;
        case CZGeminiAPICameraModelTypeAxiocam208:
            return kCameraModelNameAxiocam208;
    }
}

- (void)initTypeZooms:(NSMutableDictionary *)typeZooms {
    CZZoomLevelSet *zoomSet = [self availableZoomLevelSet];
    
    // camera adapter
    NSArray *array = [zoomSet availableZoomsForType:kZoomTypeCameraAdapter];
    NSAssert(array.count == 3, @"camera adapter has 3 available zooms");
    
    CZZoomLevel *zoomLevel = array[1];
    NSAssert(zoomLevel.magnification == 0.63f, @"default zoom is 0.63");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeCameraAdapter];
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    // FOV is readonly for Axio-line.
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    return [self defaultFOVWidthAtPosition:position];
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    if (self.cameraClass != [CZGeminiCamera class]) {
        return kNonCalibratedValue;
    }
    
    float magnification = [self.nosepiece objectiveAtPosition:position].magnification;
    CZZoomLevel *cameraAdapter = [self zoomForType:kZoomTypeCameraAdapter];
    if (cameraAdapter) {
        magnification *= cameraAdapter.magnification;
    }
    if (magnification > 0.0f) {
        switch (self.systemInfo.camera.modelType) {
            case CZGeminiAPICameraModelTypeUnknown:
                return 0.0f;
            case CZGeminiAPICameraModelTypeAxiocam202:
                return CZAxiocam202FOV / magnification;
            case CZGeminiAPICameraModelTypeAxiocam208:
                return CZAxiocam208FOV / magnification;
        }
    }
    return kNonCalibratedValue;
}

@end
