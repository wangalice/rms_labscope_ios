//
//  CZMicroscopeAxiocamStereo.m
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAxiocamStereo.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZCamera+MicroscopeManagerAdditions.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

static const double kAxiocamBasicFOV = 5660;  // CCD sensor width 5700 * 0.99 um

@interface CZMicroscopeAxiocamStereo ()

@property (nonatomic, copy) NSString *cameraModelName;
@property (nonatomic, retain) NSMutableArray<NSNumber *> *zoomClickStops;

@end

@implementation CZMicroscopeAxiocamStereo

- (void)dealloc {
    [_cameraModelName release];
    [_zoomClickStops release];
    [super dealloc];
}

- (NSMutableArray<NSNumber *> *)zoomClickStops {
    if (_zoomClickStops == nil) {
        _zoomClickStops = [[NSMutableArray alloc] init];
    }
    return _zoomClickStops;
}

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithMicroscopeModel:microscopeModel];
    if (self) {
        if ([microscopeModel isKindOfClass:[CZMicroscopeAxiocamStereo class]]) {
            _zoomClickStops = [[(CZMicroscopeAxiocamStereo *)microscopeModel zoomClickStops] mutableCopy];
        }
    }
    return self;
}

- (BOOL)isEqualToMicroscopeModel:(CZMicroscopeAxiocamStereo *)otherMicroscopeModel {
    if ([otherMicroscopeModel isKindOfClass:[CZMicroscopeAxiocamStereo class]] && ![self.zoomClickStops isEqualToArray:otherMicroscopeModel.zoomClickStops]) {
        return NO;
    }
    return [super isEqualToMicroscopeModel:otherMicroscopeModel];
}

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kAxiocamStereo];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet setForAxiocamStereo] retain];
    }
    return zoomLevelSet;
}

+ (UIImage *)modelIcon {
    return nil;
}

+ (NSString *)cameraModelName {
    return nil;
}

- (instancetype)initWithCamera:(CZCamera *)camera {
    self = [super initWithCamera:camera];
    if (self) {
        _cameraModelName = [camera.cameraModelName copy];
    }
    return self;
}

- (void)initTypeZooms:(NSMutableDictionary *)typeZooms {
    CZZoomLevelSet *zoomSet = [self availableZoomLevelSet];
    
    // objective
    NSArray *array = [zoomSet availableZoomsForType:kZoomTypeStereoObjective];
    NSAssert(array.count == 12, @"objective has 12 available zooms");
    
    CZZoomLevel *zoomLevel = array[5];
    NSAssert(zoomLevel.magnification == 1.0, @"default zoom is 1.0");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeStereoObjective];
    
    // camera adapter
    array = [zoomSet availableZoomsForType:kZoomTypeCameraAdapter];
    NSAssert(array.count == 4, @"camera adapter has 4 available zooms");
    
    zoomLevel = array[1];
    NSAssert(zoomLevel.magnification == 0.5, @"default zoom is 0.5");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeCameraAdapter];
    
    // eyepiece
    array = [zoomSet availableZoomsForType:kZoomTypeEyepiece];
    NSAssert(array.count == 3, @"eyepiece has 3 available zooms");
    
    zoomLevel = array[0];
    NSAssert(zoomLevel.magnification == 10, @"default zoom is 10");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeEyepiece];
}

- (void)initAdditionalZooms:(NSMutableDictionary *)additionalZooms {
    additionalZooms[kZoomTypeAdditionalEyepiece] = @1.0;
    additionalZooms[kZoomTypeAdditionalCamera] = @1.0;
}

- (CZMicroscopeTypes)type {
    return kAxiocamStereo;
}

- (NSString *)modelName {
    return kModelNameStereo;
}

- (NSUInteger)zoomClickStopPositions {
    return self.zoomClickStops.count;
}

- (CZZoomLevel *)zoomClickStopAtPosition:(NSUInteger)position {
    if (position >= self.zoomClickStops.count) {
        return nil;
    }
    CZZoomLevelSet *levelSet = [self availableZoomLevelSet];
    NSUInteger zoomLevelID = self.zoomClickStops[position].unsignedIntegerValue;
    return [levelSet zoomLevelAtIndex:zoomLevelID ofType:kZoomTypeZoomClickStop];
}

- (NSUInteger)zoomClickStopIDAtPosition:(NSUInteger)position {
    if (position >= self.zoomClickStops.count) {
        return NSNotFound;
    }
    NSUInteger zoomLevelID = self.zoomClickStops[position].unsignedIntegerValue;
    return zoomLevelID;
}

- (void)addZoomClickStop:(CZZoomLevel *)zoomClickStop {
    NSUInteger index = [[self availableZoomLevelSet] indexForZoomLevel:zoomClickStop ofType:kZoomTypeZoomClickStop];
    if (index != NSNotFound) {
        [self addZoomClickStopID:index];
    }
}

- (void)addZoomClickStopID:(NSUInteger)zoomClickStopID {
    [self.zoomClickStops addObject:@(zoomClickStopID)];
    
    if (self.currentZoomClickStopPosition < self.zoomClickStops.count) {
        NSNumber *currentZoomClickStopID = self.zoomClickStops[self.currentZoomClickStopPosition];
        NSUInteger zoomLevelId1 = zoomClickStopID;
        NSUInteger zoomLevelId2 = currentZoomClickStopID.unsignedIntegerValue;
        if (zoomLevelId1 < zoomLevelId2) {
            self.currentZoomClickStopPosition += 1;
        }
    } else {
        self.currentZoomClickStopPosition = NSNotFound;
    }
    
    // TODO: binary search and insert
    [self.zoomClickStops sortUsingComparator: ^(NSNumber *first, NSNumber *second) {
        CZZoomLevelSet *levelSet = [self availableZoomLevelSet];
        CZZoomLevel* zoomlevel1 = [levelSet zoomLevelAtIndex:first.unsignedIntegerValue ofType:kZoomTypeZoomClickStop];
        CZZoomLevel* zoomlevel2 = [levelSet zoomLevelAtIndex:second.unsignedIntegerValue ofType:kZoomTypeZoomClickStop];
        
        return [zoomlevel1 compare:zoomlevel2];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZZoomKnobDidUpdateNotification object:self];
}

- (void)removeZoomClickStop:(CZZoomLevel *)zoomClickStop {
    // TODO: Not implemented yet.
}

- (void)removeZoomClickStopForID:(NSUInteger)zoomClickStopID {
    NSUInteger position = [self.zoomClickStops indexOfObjectPassingTest:^BOOL(NSNumber *object, NSUInteger index, BOOL *stop){
        if (object.unsignedIntegerValue == zoomClickStopID) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    
    if (position != NSNotFound) {
        [self removeZoomClickStopAtPosition:position];
    }
}

- (void)removeZoomClickStopAtPosition:(NSUInteger)position {
    if (self.currentZoomClickStopPosition < self.zoomClickStops.count) {
        if (position < self.currentZoomClickStopPosition) {
            self.currentZoomClickStopPosition -= 1;
        } else if (position == self.currentZoomClickStopPosition) {
            self.currentZoomClickStopPosition = NSNotFound;
        }
    }
    
    [self.zoomClickStops removeObjectAtIndex:position];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZZoomKnobDidUpdateNotification object:self];
}

- (void)removeAllZoomClickStops {
    [self.zoomClickStops removeAllObjects];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CZZoomKnobDidUpdateNotification object:self];
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    return [self defaultFOVWidthAtPosition:position];
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    // fov is readonly for this microscope model
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    if (self.cameraClass == [CZLocalFileCamera class]) {
        return kNonCalibratedValue;
    }
    
    double magnification = [self cameraMagnificationAtPosition:position];
    if (magnification <= 0.0f) {
        return kNonCalibratedValue;
    } else {
        return kAxiocamBasicFOV / magnification;
    }
}

@end
