//
//  CZMicroscopeModel+MNA.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/2/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel+MNA.h"
#import "CZMicroscopeModelPrivate.h"
#import "CZMNABrowser.h"
#import "CZNosepiece+Primo.h"
#import "CZNosepiecePrivate.h"
#import <CZToolbox/CZToolbox.h>
#import <AFNetworking/AFHTTPClient.h>
#import <SBJson/SBJson.h>

@implementation CZMicroscopeModel (MNA)

- (BOOL)canSupportMNA {
    return NO;
}

- (BOOL)isPairedWithMNA {
    if (self.mnaMACAddress == nil || self.mnaIPAddress == nil ||
        [self.mnaMACAddress isEqualToString:kNULLMACAddress] ||
        [self.mnaIPAddress isEqualToString:kNULLIPAddress]) {
        return NO;
    }
    return YES;
}

- (BOOL)hasEqualMNAInfoWithMicroscopeModel:(CZMicroscopeModel *)model {
    if (self.type == model.type && self.nosepiece.positions == model.nosepiece.positions) {
    } else {
        return NO;
    }
    
    //Ignore case when compare camera MAC address
    BOOL isEqualMACAddress = [self.cameraMACAddress isEqualComparisionWithCaseInsensitive:model.cameraMACAddress];
    if (self.cameraMACAddress == model.cameraMACAddress || isEqualMACAddress) {
        
    } else {
        return NO;
    }
    
    if (![self.nosepiece isEqualToNosepiece:model.nosepiece]) {
        return NO;
    }
    
    return YES;
}

- (void)setParameter:(NSString *)param body:(NSData *)body completionHandler:(void (^)(NSError *connectionError))completionHandler {
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://%@/%@", self.mnaIPAddress, param];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    if (url == nil) {
        [urlString release];
        return;
    }
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"PUT" path:[url path] parameters:nil];
    
    [request setHTTPBody:body];
    
    NSDictionary *headers = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"application/json", [NSString stringWithFormat:@"%lu", (unsigned long)body.length], nil]
                                                        forKeys:[NSArray arrayWithObjects:@"Accept", @"Content-Length", nil]];
    [request setAllHTTPHeaderFields:headers];
    
    [request setTimeoutInterval:5]; // Shorter timeout for GET requests
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            CZLogv(@"Failed to set parameters to MNAMAC: error=%@", [error localizedDescription]);
        }
        if (completionHandler) {
            // Currently ignore error -1005(connection lost),
            // iOS get this error because MNA close connection
            // too early. Hope this bug of MNA can be fixed in future.
            if (error.code == -1005) {
                completionHandler(nil);
            } else {
                completionHandler(error);
            }
        }
    }] resume];
    
    [httpClient release];
    [url release];
    [urlString release];
}

- (void)resetMNA:(void (^)(NSError *connectionError))completionHandler {
    NSData *body = [[NSData alloc] init];
    [self setParameter:@"Microscope" body:body completionHandler:completionHandler];
    [body release];
    
    self.mnaMACAddress = kNULLMACAddress;
    self.mnaIPAddress = kNULLIPAddress;
}

- (void)saveToMNA:(void (^)(NSError *connectionError))completionHandler {
    if (!self.mnaMACAddress || !self.mnaIPAddress) {
        return;
    }
    
    if (!self.isPairedWithMNA) {
        return;
    }
    
    NSMutableDictionary *microscopeObject = [[NSMutableDictionary alloc] init];
    microscopeObject[@"Version"] = @(kMicroscopeModelVersionNumber);
    microscopeObject[@"Camera"] = self.cameraMACAddress;
    microscopeObject[@"MicroscopeModelId"] = [NSNumber numberWithInt:self.type];
    microscopeObject[@"NosepiecePositions"] = [NSNumber numberWithUnsignedInteger:self.nosepiece.positions];
    
    NSMutableArray *objectives = [[NSMutableArray alloc] init];
    NSMutableArray *scalings = [[NSMutableArray alloc] init];
    
    for (NSUInteger position = 0; position < self.nosepiece.positions; position++) {
        CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
        CZPrimoObjective enumeration = [objective enumerationValue];
        objectives[position] = @(enumeration);
        
        // TODO: !!! This is incorrect now. The value being saved on MNA is the
        // width of field of view at this moment. !!!
        float fovWidth = [[self.nosepiece fieldOfViewAtPosition:position] floatValue];
        if (fovWidth > 0.0) {
            scalings[position] = @(fovWidth);
        } else {
            scalings[position] = @(kNonCalibratedValue);
        }
    }
    
    microscopeObject[@"Objectives"] = objectives;
    microscopeObject[@"Scalings"] = scalings;
    
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    NSData *body = [[writer dataWithObject:microscopeObject] copy];
    [writer release];
    
    [self setParameter:@"Microscope" body:body completionHandler:completionHandler];
    
    [objectives release];
    [scalings release];
    [microscopeObject release];
    [body release];
}

- (NSData *)getParameter:(NSURL *)url error:(NSError **)error {
    if (url == nil) {
        return nil;
    }
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:[url path] parameters:nil];
    [request setTimeoutInterval:5]; // Shorter timeout for GET requests
    CZURLSessionTaskResult *result = [[NSURLSession sharedSession] cz_startSynchronousDataTaskWithRequest:request];
    
    if (error) {
        *error = [[result.error retain] autorelease];
    }
    
    [httpClient release];
    
    return result.data;
}

- (void)newMicroscopeInfoFromMNAAsync:(CZMNA *)mna
                             delegate:(id<CZMicroscopeModelUpdateInfoFromMNADelegate>)delegate {
    [self retain];
    
    CZMNA *queryMNA = mna;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CZMicroscopeModel *modelFromMNA = [self newMicroscopeInfoFromMNA:queryMNA];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [delegate microscopeModel:self
                        hasUpdateInfo:modelFromMNA
                              fromMNA:queryMNA];
            [self release];
        });
        
        [modelFromMNA release];
    });
}

- (CZMicroscopeModel *)newMicroscopeInfoFromMNA:(CZMNA *)mna {
    if (!mna.macAddress || !mna.ipAddress) {
        return nil;
    }
    
    NSError *error = nil;
    BOOL validInfoFromMNA = YES;
    CZMicroscopeModel *modelFromMNA = nil;
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://%@/Microscope", mna.ipAddress];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSData *data = [self getParameter:url error:&error];
    if (error) {
        CZLogv(@"Failed to send the request to get parameters from MNAMAC=%@, MNAIP=%@, error=%@", mna.macAddress, mna.ipAddress, [error localizedDescription]);
        validInfoFromMNA = NO;
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        CZLogv(@"Get microsocpe info from from MNAMAC=%@, MNAIP=%@, body=%@", mna.macAddress, mna.ipAddress, body);
        
        SBJsonParser *json_parser = [[SBJsonParser alloc] init];
        NSDictionary *response_dict = [json_parser objectWithString:body];
        
        do {
            if (!response_dict) {
                CZLogv(@"Invalid body from MNAMAC=%@, MNAIP=%@", mna.macAddress, mna.ipAddress);
                if (body.length == 0) {
                    CZLogv(@"Empty body from MNAMAC=%@, MNAIP=%@", mna.macAddress, mna.ipAddress);
                    validInfoFromMNA = NO;
                } else {
                    CZLogv(@"Invalid body from MNAMAC=%@, MNAIP=%@", mna.macAddress, mna.ipAddress);
                    validInfoFromMNA = NO;
                }
                break;
            }
            
            // TODO: read version and consider update
            // NSString *version = responese_dict[@"Version"];
            
            CZMicroscopeTypes type = [response_dict[@"MicroscopeModelId"] unsignedIntegerValue];
            if (type >= kNumOfMicroscopeModels) {
                CZLogv(@"Invalid microscope type from MNAMAC=%@, MNAIP=%@, MicroscopeModelId=%lu", mna.macAddress, mna.ipAddress, (unsigned long) type);
                validInfoFromMNA = NO;
                break;
            } else {
                if (type == self.type) {
                    modelFromMNA = [self copy];
                } else {
                    modelFromMNA = [[self modelBySwitchingToType:type] retain];
                }
            }
            modelFromMNA.name = response_dict[@"Camera"];
            modelFromMNA.cameraMACAddress = response_dict[@"Camera"];
            int numOfNosepieces = [response_dict[@"NosepiecePositions"] intValue];
            if (numOfNosepieces < 0) {
                validInfoFromMNA = NO;
                CZLogv(@"Invalid number of nosepiece positions from MNAMAC=%@, MNAIP=%@, NosepiecePositions=%d", mna.macAddress, mna.ipAddress, numOfNosepieces);
                break;
            }
            
            NSArray *objectives = response_dict[@"Objectives"];
            if (objectives == nil || objectives.count != numOfNosepieces) {
                validInfoFromMNA = NO;
                CZLogv(@"Invalid number of objectives from MNAMAC=%@, MNAIP=%@, objectives count=%lu", mna.macAddress, mna.ipAddress, (unsigned long)objectives.count);
                break;
            }
            
            for (int i = 0; i < numOfNosepieces; i++) {
                CZPrimoObjective objective = [objectives[i] intValue];
                if (objective > kMaxObjective) {
                    validInfoFromMNA = NO;
                    CZLogv(@"Invalid objective from MNAMAC=%@, MNAIP=%@, Objective=%lu", mna.macAddress, mna.ipAddress, (unsigned long)objective);
                    break;
                }
                [modelFromMNA.nosepiece setObjective:[CZObjective objectiveFromEnumeration:objective] atPosition:i];
            }
            if (!validInfoFromMNA) {
                break;
            }
            
            NSArray *scalings = response_dict[@"Scalings"];
            if (scalings == nil || scalings.count != numOfNosepieces) {
                validInfoFromMNA = NO;
                CZLogv(@"Invalid number of scalings from MNAMAC=%@, MNAIP=%@, scalings count=%lu", mna.macAddress, mna.ipAddress, (unsigned long)scalings.count);
                break;
            }
            
            // TODO: !!! This is incorrect now. The value being saved on MNA is the
            // width of field of view at this moment. !!!
            for (NSUInteger position = 0; position < numOfNosepieces; position++) {
                NSString *scaling = scalings[position];
                [modelFromMNA setFOVWidth:[scaling floatValue] atPosition:position];
            }
        } while (NO);
        
        [json_parser release];
        [body release];
    }
    
    if (!validInfoFromMNA) {
        [modelFromMNA release];
    }
    
    [url release];
    [urlString release];
    return validInfoFromMNA ? modelFromMNA : nil;
}

- (void)retrieveCurrentObjectiveFromMNAAsync:(id<CZMicroscopeModelUpdateInfoFromMNADelegate>)delegate {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL canGetObjectiveFromMNA = [self retrieveCurrentObjectiveFromMNA];
        
        if (canGetObjectiveFromMNA) {
            dispatch_async(dispatch_get_main_queue(), ^{
                CZObjective *objective = [self.nosepiece objectiveAtCurrentPosition];
                [delegate microscopeModel:self
                     retrieveNewObjective:[objective enumerationValue]];
            });
        } else {
            if ([delegate respondsToSelector:@selector(microscopeModelFailedToRetrieveObjective:)]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [delegate microscopeModelFailedToRetrieveObjective:self];
                });
            }
        }
    });
}

- (BOOL)retrieveCurrentObjectiveFromMNA {
    NSError *error = nil;
    BOOL validInfoFromMNA = YES;
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://%@/CurrentNosepiecePosition", self.mnaIPAddress];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSData *data = [self getParameter:url error:&error];
    if (error) {
        validInfoFromMNA = NO;
        CZLogv(@"Failed to send the request to get parameters from MNAMAC=%@, MNAIP=%@, error=%@", self.mnaMACAddress, self.mnaIPAddress, [error localizedDescription]);
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        CZLogv(@"Get microsocpe info from from MNAMAC=%@, MNAIP=%@, body=%@", self.mnaMACAddress, self.mnaIPAddress, body);
        
        if (!body || body.length == 0) {
            CZLogv(@"Invalid body from MNAMAC=%@, MNAIP=%@", self.mnaMACAddress, self.mnaIPAddress);
            
            [url release];
            [urlString release];
            [body release];
            return NO;
        }
        
        NSUInteger position = NSNotFound;
        
        if ([body isEqualToString:@"[E]"]) {
            position = 0;
        } else if ([body isEqualToString:@"[D]"]) {
            position = 1;
        } else if ([body isEqualToString:@"[C]"]) {
            position = 2;
        } else if ([body isEqualToString:@"[B]"]) {
            position = 3;
        } else if ([body isEqualToString:@"[A]"]) {
            position = 4;
        } else if ([body isEqualToString:@"[9]"]) {
            position = 5;
        } else if ([body isEqualToString:@"[8]"]) {
            position = 6;
        } else if ([body isEqualToString:@"[7]"]) {
            position = 7;
        } else if ([body isEqualToString:@"[6]"]) {
            position = 8;
        } else if ([body isEqualToString:@"[5]"]) {
            position = 9;
        }
        
        self.nosepiece.currentPosition = position;
        
        [body release];
    }
    
    [url release];
    [urlString release];
    
    return validInfoFromMNA;
}

- (CZMicroscopeModel *)updateMNAInfoWith:(CZMicroscopeModel *)modelFromMNA {
    if (!modelFromMNA) {
        return self;
    }
    
    CZMicroscopeModel *model = self;
    if (self.type != modelFromMNA.type) {
        model = [self modelBySwitchingToType:modelFromMNA.type];
    }
    
    model.name = modelFromMNA.name;
    model.mnaIPAddress = modelFromMNA.mnaIPAddress;
    model.mnaMACAddress = modelFromMNA.mnaMACAddress;
    model.nosepiece = modelFromMNA.nosepiece;
    return model;
}


- (void)updateDataFromMNAModel:(CZMicroscopeModel *)modelFromMNA {
    if (self.type != modelFromMNA.type) {
        return;
    }
    
    //!!!: There will be some issuse when use data from microscope with the DCM Service;
    // Becasuse the mac address from DCM always is uppercase while the mac address get from
    // hareware may be lowercase, so we should take care of this issue to make the Labscope
    // work well on the latest version.
    self.mnaMACAddress = modelFromMNA.mnaMACAddress;
    self.mnaIPAddress = modelFromMNA.mnaIPAddress;
    self.nosepiece = modelFromMNA.nosepiece;
}

@end
