//
//  CZMicroscopePrimoStar.m
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopePrimoStar.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZNosepiece+Primo.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

@implementation CZMicroscopePrimoStar

+ (double)objectiveKappaFOVWidth:(NSUInteger)objective {
    const double fovWidths[] = {
        (1.1013 * 2560.0),          //kPlanAchromat4x_01
        (0.4374 * 2560.0),          //kPlanAchromat10x_025
        (0.2199 * 2560.0),          //kPlanAchromat20x_040
        (0.1095 * 2560.0),          //kPlanAchromat40x_065
        (0.0437 * 2560.0),          //kPlanAchromat100x_125_oil
        (0.0437 * 2560.0),          //kPlanAchromat100x_080_dry
        (0.2199 * 2560.0),          //kPlanAchromat20x_040_D_0
        (0.1095 * 2560.0),          //kPlanAchromat40x_065_D_0
        (0.0437 * 2560.0),          //kPlanAchromat100x_125_D_0_oil
        (0.4374 * 2560.0),          //kPlanAchromat10x_025_ph1
        (0.2199 * 2560.0),          //kPlanAchromat20x_045_ph2
        (0.1095 * 2560.0),          //kPlanAchromat40x_065_ph2
        (0.0437 * 2560.0),          //kPlanAchromat100x_125_oil_ph3
    };
    
    if (objective >= kPlanAchromat4x_01) {
        const size_t count = sizeof(fovWidths) / sizeof(fovWidths[0]);
        const size_t i = objective - kPlanAchromat4x_01;
        return i < count ? fovWidths[i] : kNonCalibratedValue;
    } else {
        return kNonCalibratedValue;
    }
}

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kPrimoStar];
}

- (NSArray<CZObjective *> *)availableObjectives {
    return @[[CZObjective none],
             [CZObjective planAchromat4x_01],
             [CZObjective planAchromat10x_025],
             [CZObjective planAchromat20x_040],
             [CZObjective planAchromat40x_065],
             [CZObjective planAchromat100x_125_oil],
             [CZObjective planAchromat100x_080_dry],
             [CZObjective planAchromat20x_040_D_0],
             [CZObjective planAchromat40x_065_D_0],
             [CZObjective planAchromat100x_125_D_0_oil],
             [CZObjective planAchromat10x_025_ph1],
             [CZObjective planAchromat20x_045_ph2],
             [CZObjective planAchromat40x_065_ph2],
             [CZObjective planAchromat100x_125_oil_ph3]];
}

- (NSUInteger)defaultNosepiecePositions {
    return 4;
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet alloc] init];
    }
    return zoomLevelSet;
}

+ (UIImage *)modelIcon {
    return [UIImage imageNamed:A(@"model-primostar")];
}

- (instancetype)initWithMicroscopeModel:(CZMicroscopeModel *)microscopeModel {
    self = [super initWithMicroscopeModel:microscopeModel];
    if (self) {
        if ([microscopeModel isKindOfClass:[CZMicroscopePrimoStar class]]) {
            _package = ((CZMicroscopePrimoStar *)microscopeModel).package;
        }
    }
    return self;
}

- (BOOL)isEqualToMicroscopeModel:(CZMicroscopeModel *)otherMicroscopeModel {
    if ([otherMicroscopeModel isKindOfClass:[CZMicroscopePrimoStar class]]) {
        CZMicroscopePrimoStar *primoStar = (CZMicroscopePrimoStar *)otherMicroscopeModel;
        return (primoStar->_package == self->_package && [super isEqualToMicroscopeModel:otherMicroscopeModel]);
    } else {
        return NO;
    }
}

- (CZMicroscopeTypes)type {
    return kPrimoStar;
}

- (NSString *)modelName {
    return kModelNamePrimoStar;
}

- (void)readFromDictionary:(NSDictionary *)dictionary {
    [super readFromDictionary:dictionary];
    
    NSNumber *package = dictionary[kPackage];
    self.package = [package intValue];
}

- (void)writeToDictionary:(NSMutableDictionary *)dictionary {
    [super writeToDictionary:dictionary];
    
    NSNumber *package = [NSNumber numberWithInt:self.package];
    dictionary[kPackage] = package;
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    return [self defaultFOVWidthAtPosition:position];
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    // fov is readonly for Primo Star
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
    NSUInteger userObjective = [objective enumerationValue];
    
    double defaultScaling = kNonCalibratedValue;
    if (self.cameraClass == [CZKappaCamera class]) {
        defaultScaling = [CZMicroscopePrimoStar objectiveKappaFOVWidth:userObjective];
    }
    
    return defaultScaling;
}

- (void)loadSlotsFromDefaults {
    [super loadSlotsFromDefaults];
    
    [self reloadSlotsDataIfNeed];
}

- (void)loadSlotsFromDictionary:(NSDictionary *)dictionary {
    [super loadSlotsFromDictionary:dictionary];
    
    [self reloadSlotsDataIfNeed];
}

- (void)reloadSlotsDataIfNeed {
    if (self.package != kCZPrimoStarPackageManualMode) {
        NSArray<CZObjective *> *objectives = [CZMicroscopePrimoStar objectivesInPackage:self.package];
        self.nosepiece.positions = objectives.count;
        NSUInteger position = 0;
        for (CZObjective *objective in objectives) {
            [self.nosepiece setObjective:objective atPosition:position];
            ++position;
        }
    }
}

+ (NSArray<CZObjective *> *)objectivesInPackage:(CZPrimoStarPackage)package {
    switch (package) {
        case kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_Pointer:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_125_oil]];
        case kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_L_FOV18:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_125_oil]];
        case kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_100x_08:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_080_dry]];
        case kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph2_FOV20_Photo:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065_ph2],
                     [CZObjective planAchromat100x_125_oil]];
        case kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph1_Ph2_Ph3_FOV20:
            return @[[CZObjective planAchromat10x_025_ph1],
                     [CZObjective planAchromat20x_045_ph2],
                     [CZObjective planAchromat40x_065_ph2],
                     [CZObjective planAchromat100x_125_oil_ph3]];
        case kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Pointer:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_125_oil]];
        case kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Photo:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_125_oil]];
        case kCZPrimoStarPackage_LED_Fixed_Kohler_Stage_R_FOV18_D0:
            return @[[CZObjective planAchromat10x_025],
                     [CZObjective planAchromat20x_040_D_0],
                     [CZObjective planAchromat40x_065_D_0],
                     [CZObjective planAchromat100x_125_D_0_oil]];
        case kCZPrimoStarPackage_HD_HAL_LED_Full_Kohler_FOV20_Integrated_HD_IP_Camera_5MP:
            return @[[CZObjective planAchromat4x_01],
                     [CZObjective planAchromat10x_025],
                     [CZObjective planAchromat40x_065],
                     [CZObjective planAchromat100x_125_oil]];
        default:
            return @[];
    }
}

@end
