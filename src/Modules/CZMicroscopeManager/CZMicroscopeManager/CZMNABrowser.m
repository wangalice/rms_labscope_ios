//
//  CZMNABrowser.m
//  Hermes
//
//  Created by Mike Wang on 6/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMNABrowser.h"
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import <CZToolbox/CZToolbox.h>

#define kNumOfMockMNAs 2

@implementation CZMNA

- (id)init {
    self = [super init];
    if (self) {
        _lastActiveTime = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    }
    return self;
}

- (void)dealloc {
    [_macAddress release];
    [_ipAddress release];
    [_lastActiveTime release];
    [_level release];
    
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZMNA *newMNA = [[[super class] allocWithZone:zone]init];
    if (newMNA) {
        newMNA.macAddress = self.macAddress;
        newMNA.ipAddress = self.ipAddress;
        newMNA.lastActiveTime = self.lastActiveTime;
        newMNA.level = self.level;
    }
    
    return newMNA;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([object class] != [CZMNA class]) {
        return NO;
    }
    
    CZMNA *mna = object;
    return [self.macAddress isEqualToString:mna.macAddress];
}

@end


@interface CZMNABrowser () {
    NSObject *_mnaDiscoveryLock;
    GCDAsyncUdpSocket *_browsingSocket;
    dispatch_queue_t _mnaDiscoveryQueue;
}
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, retain) NSMutableDictionary *mnaList;
@property (atomic, assign) BOOL isBrowsing;

- (void)handleTimer:(NSTimer *)timer;
- (void)checkLostMNAs;
- (void)browseUPnPMNAsAsync;
- (void)browseMockMNAsAsync;
- (void)foundMNA:(CZMNA *)camera;

- (GCDAsyncUdpSocket *)browsingSocket;

@end


@implementation CZMNABrowser

static CZMNABrowser *uniqueInstance = nil;
#pragma mark - Singleton Design Pattern

+ (CZMNABrowser *)sharedInstance {
    @synchronized ([CZMNABrowser class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (id)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

- (id)init {
    self = [super init];
    if (self) {
        _isBrowsing = NO;
        _mnaList = [[NSMutableDictionary alloc] init];
        
        _mnaDiscoveryQueue = dispatch_queue_create("com.zeisscn.mna.discovery", NULL);
        _mnaDiscoveryLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_timer invalidate];
    [_timer release];
    [_mnaList release];
    
    dispatch_release(_mnaDiscoveryQueue);
    [_mnaDiscoveryLock release];
    
    _browsingSocket.delegate = nil;
    [_browsingSocket close];
    [_browsingSocket release];
    
    [super dealloc];
}

#pragma mark - Public interfaces

- (NSDictionary *)newMNAList {
    @synchronized (self) {
        return [_mnaList copy];
    }
}

- (void)beginBrowsing {
    @synchronized (self) {
        self.isBrowsing = YES;
        if (_timer == nil) {
            [self handleTimer:nil];
            
            const NSTimeInterval kBrowsingInterval = 3.0;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:kBrowsingInterval
                                                          target:self
                                                        selector:@selector(handleTimer:)
                                                        userInfo:nil
                                                         repeats:YES];
        }
        
        NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
        for (CZMNA *mna in _mnaList.allValues) {
            [mna setLastActiveTime:now];
        }
        [now release];
    }
}

- (void)stopBrowsing {
    @synchronized (self) {
        @synchronized (_mnaDiscoveryLock) {
            [_browsingSocket close];
        }
        self.isBrowsing = NO;
        [_timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Private methods

- (void)handleTimer:(NSTimer *)timer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self checkLostMNAs];
        
        // Only try to discover MNA when Wi-Fi is available.
        if ([[CZWifiNotifier sharedInstance] isReachable]) {
            [self browseUPnPMNAsAsync];
        } else {
            CZLogv(@"MNA discovery not executed - no Wi-Fi available.");
        }
    });
}

- (void)checkLostMNAs {
    // This is a time interval compares with "now", so it's negative.
    const NSTimeInterval kLostTime = -15.0;
    
    NSMutableArray *mnasToRemove = [[NSMutableArray alloc] init];
    
    @synchronized (self) {
        // Drain autorelease pool for the two NSArray created by "allValues",
        // or the camera objects will be retained longer than expected.
        @autoreleasepool {
                       
            // Check living cameras.
            NSArray *mnas = [_mnaList allValues];
            for (CZMNA *mna in mnas) {
                NSTimeInterval timeInterval = [[mna lastActiveTime] timeIntervalSinceNow];
                if (timeInterval <= kLostTime) {
                    [mnasToRemove addObject:mna];
                } 
            }
            
            // Remove lost mna from the living mna list.
            for (CZMNA *mna in mnasToRemove) {
                if (self.isBrowsing) {
                    [_mnaList removeObjectForKey:[mna macAddress]];
                }
            }
        }
    }
    
    if ([mnasToRemove count] > 0) {
        if (self.isBrowsing && [_delegate respondsToSelector:@selector(mnaBrowserDidLoseMNAs:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                [_delegate mnaBrowserDidLoseMNAs:mnasToRemove];
            });
        }
    }
    
    [mnasToRemove release];
}

- (void)browseMockMNAsAsync {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        sleep(1); // Simulate asynchronous browsing.
        
        NSMutableArray *found = [[NSMutableArray alloc] init];
        
        @synchronized (self) {
            for (int i = 0; i < kNumOfMockMNAs; i++) {
                NSString *mockMacAddress = [[NSString alloc] initWithFormat:@"00:00:00:00:11:0%d", i+1];
                NSString *mockIPAddress = [[NSString alloc] initWithFormat:@"192.168.0.5%d", i];
                if ([_mnaList objectForKey:mockMacAddress] == nil) {
                    CZMNA *mna = [[CZMNA alloc] init];
                    [mna setMacAddress:mockMacAddress];
                    [mna setIpAddress:mockIPAddress];
                    [found addObject:mna];
                    [mna release];
                }
                [mockMacAddress release];
                [mockIPAddress release];
            }
        }
        
        if ([found count] > 0) {
            for (CZMNA *mna in found) {
                [self foundMNA:mna];
                NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
                [mna setLastActiveTime:now];
                [now release];
            }
            
            if (self.isBrowsing && [_delegate respondsToSelector:@selector(mnaBrowserDidFindMNAs:)]) {
                dispatch_async(dispatch_get_main_queue(), ^() {
                    [_delegate mnaBrowserDidFindMNAs:found];
                });
            }
        }
        
        [found release];
    });
}

- (void)browseUPnPMNAsAsync {
    const uint16_t kMNADiscoveryPort = 8990;
    const NSTimeInterval kMNADiscoveryTimeout = 5;
    const size_t kMNAPacketLength = 16;
    
    @synchronized (_mnaDiscoveryLock) {
        GCDAsyncUdpSocket *socket = [self browsingSocket];
        
        do {
            NSError *error = nil;
            
            if (![socket enableBroadcast:YES error:&error]) {
                CZLogv(@"MNA discovery: failed to enable broadcast for the socket.");
                break;
            }
            
            if (![socket beginReceiving:&error]) {
                CZLogv(@"MNA discovery: failed to begin receiving data from socket.");
                break;
            }
            
            void *dataBuffer = malloc(kMNAPacketLength);
            memset(dataBuffer, 0, kMNAPacketLength);
            
            CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:dataBuffer
                                                                           length:kMNAPacketLength];
            [writer writeString:"MNA_DISCOVER"];
            [writer release];
            
            NSData *packetData = [[NSData alloc] initWithBytes:dataBuffer
                                                        length:kMNAPacketLength];
            
            [socket sendData:packetData
                      toHost:@"255.255.255.255" // Broadcast
                        port:kMNADiscoveryPort
                 withTimeout:kMNADiscoveryTimeout
                         tag:0];
            
            [packetData release];
            free(dataBuffer);
        } while (0);
    }
}


- (void)foundMNA:(CZMNA *)mna {
    if (self.isBrowsing) {
        [_mnaList setObject:mna forKey:[mna macAddress]];
    }
}

- (GCDAsyncUdpSocket *)browsingSocket {
    if (!_browsingSocket) {
        GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                  delegateQueue:_mnaDiscoveryQueue];
        [socket setIPv4Enabled:YES];
        [socket setIPv6Enabled:YES];
        _browsingSocket = socket;
    }
    
    return _browsingSocket;
}

#pragma mark - GCDAsyncUdpSocketDelegate methods

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    @autoreleasepool {
        NSString *response = [reader readStringWithLength:(data.length)];
        NSArray *fields = [response componentsSeparatedByString:@";"];
        
        // The format of the response:
        // [0] ID
        // [1] IP address
        // [2] MAC address

        if ([fields count] == 3) {
            @synchronized (self) {
                
                NSString *messageID = fields[0];
                if (![messageID isEqualToString:@"MNA_ACK"]) {
                    [reader release];
                    return;
                }
                
                NSString *macAddress = fields[2];
                if (macAddress.length != 17) {
                    //invalid mac address
                    [reader release];
                    return;
                }
                
                NSString *ipAddress = fields[1];
                
                if ([CZCommonUtils isMacAddressFromCarlZeissChina:macAddress]) {
                    CZMNA *mna = [_mnaList objectForKey:macAddress];
                    if (mna == nil) {
                        mna = [[CZMNA alloc] init];
                        [mna setIpAddress:ipAddress];
                        [mna setMacAddress:macAddress];

                        //TODO: Advanced features. Pls check it.
                        if ([macAddress hasPrefix:@"00:20:0D:F9:3"]) {
                            mna.level = kMNALevelAdvanced;
                        } else if (([macAddress hasPrefix:@"00:20:0D:F9:4"]) || ([macAddress hasPrefix:@"00:20:0D:F9:6"])) {
                            mna.level = kMNALevelBasic;
                        }
                        
                        [self foundMNA:mna];
                        
                        if (self.isBrowsing && [_delegate respondsToSelector:@selector(mnaBrowserDidFindMNAs:)]) {
                            dispatch_async(dispatch_get_main_queue(), ^() {
                                [_delegate mnaBrowserDidFindMNAs:[NSArray arrayWithObject:mna]];
                            });
                        }
                        
                        [mna release];
                    } else {
                        if (ipAddress && ![mna.ipAddress isEqualToString:ipAddress]) {
                            mna.ipAddress = ipAddress;
                        }
                        
                        NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
                        [mna setLastActiveTime:now];
                        [now release];
                    }
                }
            }
        }
    }
    
    [reader release];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    @synchronized (_mnaDiscoveryLock) {
        if (_browsingSocket == sock) {
            [_browsingSocket release];
            _browsingSocket = nil;
        }
    }
}

@end
