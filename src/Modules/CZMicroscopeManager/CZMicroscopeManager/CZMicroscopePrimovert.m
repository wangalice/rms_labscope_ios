//
//  CZMicroscopePrimovert.m
//  Matscope
//
//  Created by Ralph Jin on 6/30/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopePrimovert.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZNosepiece+Primo.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

@implementation CZMicroscopePrimovert

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kPrimovert];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet alloc] init];
    }
    return zoomLevelSet;
}

- (NSArray<CZObjective *> *)availableObjectives {
    return @[[CZObjective none],
             [CZObjective planAchromat4x_01],
             [CZObjective planAchromat10x_025_ph1],
             [CZObjective ld20x_03_Ph1],
             [CZObjective ld40x_05_Ph1],
             [CZObjective ld20x_03_Ph2],
             [CZObjective ld40x_05_Ph2]];
}

- (NSUInteger)defaultNosepiecePositions {
    return 4;
}

+ (UIImage *)modelIcon {
    return [UIImage imageNamed:A(@"model-primoverthdcam")];
}

- (CZMicroscopeTypes)type {
    return kPrimovert;
}

- (NSString *)modelName {
    return kModelNamePrimovert;
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    return [self defaultFOVWidthAtPosition:position];
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    // fov is readonly for Primo Vert
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
    NSUInteger userObjective = [objective enumerationValue];

    float defaultScaling = kNonCalibratedValue;
    if (self.cameraClass == [CZKappaCamera class]) {
        switch (userObjective) {
            case kPlanAchromat4x_01:
                defaultScaling = 1.337 * 2560;
                break;
            case kPlanAchromat10x_025_ph1:
                defaultScaling = 0.5285 * 2560;
                break;
            case kLD20x_03_Ph1:
                defaultScaling = 0.2631 * 2560;
                break;
            case kLD40x_05_Ph1:
                defaultScaling = 0.1316 * 2560;
                break;
            case kLD20x_03_Ph2:
                defaultScaling = 0.2635 * 2560;
                break;
            case kLD40x_05_Ph2:
                defaultScaling = 0.1311 * 2560;
                break;
            default:
                defaultScaling = kNonCalibratedValue;
                break;
        }
    }
    
    return defaultScaling;
}

@end
