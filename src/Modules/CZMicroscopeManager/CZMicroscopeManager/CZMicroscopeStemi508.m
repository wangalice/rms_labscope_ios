//
//  CZMicroscopeStemi508.m
//  Matscope
//
//  Created by Sherry Xu on 8/29/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeStemi508.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZMicroscopeModel+MNA.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>
#import "CZZoomLevelSet.h"

static const double kWisionCameraAdaptor = 0.5;
static const double kWisionBasicFOV = 5660;  //CCD sensor width 5700 * 0.99 um

@implementation CZMicroscopeStemi508

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kStemi508];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        size_t count;
        NSMutableDictionary *typeZooms = [[NSMutableDictionary alloc] initWithCapacity:4];
        
        const double zoomClickStops[] = {0.63, 0.65, 0.8, 1.0, 1.25, 1.6, 2.0, 2.5, 3.2, 4.0, 5.0};
        count = sizeof(zoomClickStops) / sizeof(zoomClickStops[0]);
        [typeZooms setObject:zoomLevelsFromCArray(zoomClickStops, count) forKey:kZoomTypeZoomClickStop];
        
        const double cameraAdapter[] = {kWisionCameraAdaptor};
        count = sizeof(cameraAdapter) / sizeof(cameraAdapter[0]);
        [typeZooms setObject:zoomLevelsFromCArray(cameraAdapter, count) forKey:kZoomTypeCameraAdapter];
        
        const double stereoObjective[] = {0.3, 0.4, 0.5, 0.6, 0.63, 0.7, 1.0, 1.5, 2};
        count = sizeof(stereoObjective) / sizeof(stereoObjective[0]);
        NSArray *stereoObjectiveZoomLevels = zoomLevelsFromCArray(stereoObjective, count);
        stereoObjectiveZoomLevels = [self zoomLevelsByReplacing1xWithNone:stereoObjectiveZoomLevels];
        [typeZooms setObject:stereoObjectiveZoomLevels forKey:kZoomTypeStereoObjective];
        
        const double eyepieces[] = {10, 16, 25};
        count = sizeof(eyepieces) / sizeof(eyepieces[0]);
        [typeZooms setObject:zoomLevelsFromCArray(eyepieces, count) forKey:kZoomTypeEyepiece];
        
        zoomLevelSet = [[CZZoomLevelSet alloc] initWithTypeZooms:typeZooms];
        
        [typeZooms release];
    }
    return zoomLevelSet;
}

+ (UIImage *)modelIcon {
    return [UIImage imageNamed:A(@"model-default")];  // FIXME: replace image
}

- (void)initTypeZooms:(NSMutableDictionary *)typeZooms {
    CZZoomLevelSet *zoomSet = [self availableZoomLevelSet];
    
    // stereo objective
    NSArray *array = [zoomSet availableZoomsForType:kZoomTypeStereoObjective];
    NSAssert(array.count == 9, @"objective has 9 available zooms");
    
    CZZoomLevel *zoomLevel = array[6];
    NSAssert(zoomLevel.magnification == 1.0, @"default zoom is 1.0");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeStereoObjective];
    
    // camera adapter
    array = [zoomSet availableZoomsForType:kZoomTypeCameraAdapter];
    NSAssert(array.count == 1, @"camera adapter has 6 available zooms");
    
    zoomLevel = array[0];
    NSAssert(zoomLevel.magnification == kWisionCameraAdaptor, @"default zoom is 0.5");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeCameraAdapter];
    
    // eyepiece
    array = [zoomSet availableZoomsForType:kZoomTypeEyepiece];
    NSAssert(array.count == 3, @"eyepiece has 3 available zooms");
    
    zoomLevel = array[0];
    NSAssert(zoomLevel.magnification == 10, @"default zoom is 10");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeEyepiece];
}

- (CZMicroscopeTypes)type {
    return kStemi508;
}

- (NSString *)modelName {
    return kModelNameStemi508;
}

- (BOOL)canSupportMNA {
    return YES;
}

- (float)fovWidthAtPosition:(NSUInteger)position {
    return [self defaultFOVWidthAtPosition:position];
}

- (void)setFOVWidth:(float)fovWidth atPosition:(NSUInteger)position {
    // fov is readonly for this microscope model
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    if (self.cameraClass == [CZLocalFileCamera class]) {
        return kNonCalibratedValue;
    }
    
    float magnification = [self cameraMagnificationAtPosition:position];
    if (magnification <= 0.0f) {
        return kNonCalibratedValue;
    } else {
        return kWisionBasicFOV / magnification;
    }
}

- (void)setZoom:(CZZoomLevel *)zoom forType:(NSString *)zoomType {
    // type zoom is readonly in this class except objective
    if ([zoomType isEqualToString:kZoomTypeStereoObjective] ||
        [zoomType isEqualToString:kZoomTypeEyepiece]) {
        [super setZoom:zoom forType:zoomType];
    }
}

- (void)removeZoomForType:(NSString *)zoomType {
    // type zoom is readonly in this class except objective
    if ([zoomType isEqualToString:kZoomTypeStereoObjective] ||
        [zoomType isEqualToString:kZoomTypeEyepiece]) {
        [super removeZoomForType:zoomType];
    }
}

- (void)setAdditionalZoom:(float)zoom forType:(NSString *)zoomType {
    // additional zoom is readonly in this class
}

@end
