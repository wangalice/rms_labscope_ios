//
//  CZZoomLevelSet.m
//  Labscope
//
//  Created by Ralph Jin on 4/1/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZZoomLevelSet.h"
#import "CZNosepiece.h"
#import "CZNosepieceManager.h"
#import <CZToolbox/CZToolbox.h>

NSString * const kZoomTypeStereoObjective = @"StereoObjective";
NSString * const kZoomTypeZoomClickStop = @"ZoomClickStop";
NSString * const kZoomTypeEyepiece = @"Eyepiece";
NSString * const kZoomTypeCameraAdapter = @"CameraAdapter";
NSString * const kZoomTypeAdditionalEyepiece = @"AddEyepiece";
NSString * const kZoomTypeAdditionalCamera = @"AddCamera";
NSString * const kZoomTypeAdditionalOptovar = @"AddOptovar";  // additional magnification factor, currently for CZMicroscopeAxiocamCompound

@interface CZZoomLevelSet ()

@property (nonatomic, retain) NSDictionary<NSString *, NSArray<CZZoomLevel *> *> *typeZooms;  // NOTICE: zoom level shall in ascending order

@end

@implementation CZZoomLevelSet

NSArray *zoomLevelsFromCArray(const double zoomSteps[], size_t count) {
    NSMutableArray *zoomLevels = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++) {
        CZZoomLevel *level = [[CZZoomLevel alloc] initWithMagnification:zoomSteps[i]];
        [zoomLevels addObject:level];
        [level release];
    }
    
    NSArray *array = [NSArray arrayWithArray:zoomLevels];
    [zoomLevels release];
    
    return array;
}

+ (CZZoomLevelSet *)setForAxiocamCompound {
    CZZoomLevelSet *set = [[CZZoomLevelSet alloc] init];
    
    NSMutableDictionary *typeZooms = [[NSMutableDictionary alloc] initWithCapacity:4];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double cameraAdapter[] = {0.4, 0.5, 0.63, 1.0};
    size_t count = sizeof(cameraAdapter) / sizeof(cameraAdapter[0]);
    [typeZooms setObject:zoomLevelsFromCArray(cameraAdapter, count) forKey:kZoomTypeCameraAdapter];
    
    set.typeZooms = [NSDictionary dictionaryWithDictionary:typeZooms];
    [typeZooms release];
    
    return [set autorelease];
}

+ (CZZoomLevelSet *)setForAxiocamStereo {
    size_t count;
    
    CZZoomLevelSet *set = [[CZZoomLevelSet alloc] init];
    
    NSMutableDictionary *typeZooms = [[NSMutableDictionary alloc] initWithCapacity:4];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double zoomClickStops[] = {0.65, 0.7, 0.75, 0.8, 1.0, 1.25, 1.6, 2.0, 2.5, 3.0, 3.2, 4.0, 5.0, 6.3, 8.0, 10, 11.2, 12.5, 15, 0.63};
    count = sizeof(zoomClickStops) / sizeof(zoomClickStops[0]);
    [typeZooms setObject:zoomLevelsFromCArray(zoomClickStops, count) forKey:kZoomTypeZoomClickStop];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double cameraAdapter[] = {0.4, 0.5, 0.63, 1.0};
    count = sizeof(cameraAdapter) / sizeof(cameraAdapter[0]);
    [typeZooms setObject:zoomLevelsFromCArray(cameraAdapter, count) forKey:kZoomTypeCameraAdapter];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double stereoObjective[] = {0.3, 0.4, 0.5, 0.63, 0.75, 1, 1.25, 1.5, 1.6, 2, 2.3, 3.5};
    count = sizeof(stereoObjective) / sizeof(stereoObjective[0]);
    [typeZooms setObject:zoomLevelsFromCArray(stereoObjective, count) forKey:kZoomTypeStereoObjective];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double eyepieces[] = {10, 16, 25};
    count = sizeof(eyepieces) / sizeof(eyepieces[0]);
    [typeZooms setObject:zoomLevelsFromCArray(eyepieces, count) forKey:kZoomTypeEyepiece];

    set.typeZooms = [NSDictionary dictionaryWithDictionary:typeZooms];
    [typeZooms release];
    
    return [set autorelease];
}

+ (CZZoomLevelSet *)setForAxioline {
    CZZoomLevelSet *set = [[CZZoomLevelSet alloc] init];
    
    NSMutableDictionary *typeZooms = [[NSMutableDictionary alloc] initWithCapacity:3];
    
    // CAUTION: please append value at end, the index is archived in microscope model.
    double cameraAdapter[] = {0.5, 0.63, 1.0};
    size_t count = sizeof(cameraAdapter) / sizeof(cameraAdapter[0]);
    typeZooms[kZoomTypeCameraAdapter] = zoomLevelsFromCArray(cameraAdapter, count);
    
    set.typeZooms = [NSDictionary dictionaryWithDictionary:typeZooms];
    [typeZooms release];
    
    return [set autorelease];
}

+ (CZZoomLevel *)findZoomLevel:(NSArray *)array ofMagnification:(double)magnification {
    if (array.count == 0) {
        return nil;
    }
    
    CZZoomLevel *found = nil;
    NSUInteger l = 0, r = array.count - 1;
    double minDiff = FLT_MAX;
    for (NSUInteger i = 0; i <= r; i++) {
        found = array[i];
        double diff = fabs(found.magnification - magnification);
        if (diff < minDiff) {
            minDiff = diff;
            l = i;
        }
    }
    
    found = array[l];
    if (fabs(found.magnification - magnification) > 1e-3) {
        found = nil;
    }
    
    return found;
}

- (instancetype)initWithTypeZooms:(NSDictionary<NSString *, NSArray<CZZoomLevel *> *> *)typeZooms {
    self = [super init];
    if (self) {
        _typeZooms = [[NSDictionary alloc] initWithDictionary:typeZooms];
    }
    
    return self;
}

- (void)dealloc {
    [_typeZooms release];
    [super dealloc];
}

- (NSUInteger)zoomLevelCountOfType:(NSString *)zoomType {
    return [[self availableZoomsForType:zoomType] count];
}

- (CZZoomLevel *)zoomLevelAtIndex:(NSUInteger)index ofType:(NSString *)zoomType {
    if (index < [self zoomLevelCountOfType:zoomType]) {
        return [[self availableZoomsForType:zoomType] objectAtIndex:index];
    } else {
        return nil;
    }
}

- (NSUInteger)indexForZoomLevel:(CZZoomLevel *)zoomLevel ofType:(NSString *)zoomType {
    NSArray<CZZoomLevel *> *zoomLevels = [self availableZoomsForType:zoomType];
    return [zoomLevels indexOfObject:zoomLevel];
}

- (NSArray<CZZoomLevel *> *)availableZoomsForType:(NSString *)zoomType {
    return [self.typeZooms objectForKey:zoomType];
}

- (NSArray<NSString *> *)availableTypes {
    return [self.typeZooms allKeys];
}

- (CZZoomLevel *)zoomLevelType:(NSString *)type ofMagnification:(double)magnification {
    NSArray *zoomTypes = [self availableZoomsForType:type];
    return [CZZoomLevelSet findZoomLevel:zoomTypes ofMagnification:magnification];
}

@end
