//
//  CZNosepiecePrivate.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepiece.h"

@interface CZNosepiece ()

- (NSNumber *)fieldOfViewAtPosition:(NSUInteger)position;
- (void)setFieldOfView:(NSNumber *)fieldOfView atPosition:(NSUInteger)position;

@end
