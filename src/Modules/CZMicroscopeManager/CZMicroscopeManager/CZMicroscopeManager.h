//
//  CZMicroscopeManager.h
//  CZMicroscopeManager
//
//  Created by Li, Junlin on 7/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CZMicroscopeManager.
FOUNDATION_EXPORT double CZMicroscopeManagerVersionNumber;

//! Project version string for CZMicroscopeManager.
FOUNDATION_EXPORT const unsigned char CZMicroscopeManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CZMicroscopeManager/PublicHeader.h>

#import <CZMicroscopeManager/CZMicroscopeModel.h>
#import <CZMicroscopeManager/CZMicroscopeModel+MNA.h>
#import <CZMicroscopeManager/CZMicroscopeModel+ShadingCorrection.h>
#import <CZMicroscopeManager/CZMicroscopePrimoTech.h>
#import <CZMicroscopeManager/CZMicroscopePrimoStar.h>
#import <CZMicroscopeManager/CZMicroscopePrimovert.h>
#import <CZMicroscopeManager/CZMicroscopeStemi305.h>
#import <CZMicroscopeManager/CZMicroscopeStemi508.h>
#import <CZMicroscopeManager/CZMicroscopeAxiocamCompound.h>
#import <CZMicroscopeManager/CZMicroscopeAxiocamStereo.h>
#import <CZMicroscopeManager/CZMicroscopeAxiolab.h>
#import <CZMicroscopeManager/CZMicroscopeAxioscope.h>
#import <CZMicroscopeManager/CZMNABrowser.h>
#import <CZMicroscopeManager/CZMNAParingManager.h>
#import <CZMicroscopeManager/CZCamera+MicroscopeManagerAdditions.h>
