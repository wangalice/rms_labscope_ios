//
//  CZMicroscopeModel+ShadingCorrection.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

#define kShadingCorrectMaxMaskX @"ShadingCorrectionMaxMaskX"
#define kShadingCorrectMaxMaskY @"ShadingCorrectionMaxMaskY"
#define kShadingCorrectMaxMaskZ @"ShadingCorrectionMaxMaskZ"
#define kShadingCorrectionEnable @"ShadingCorrectionEnable"
#define kShadingCorrectionData @"ShadingCorrectionData"

@interface CZShadingCorrectionData : NSObject

@property (nonatomic, readonly) float maxMaskX;
@property (nonatomic, readonly) float maxMaskY;
@property (nonatomic, readonly) float maxMaskZ;

- (id)initWithMaxMaskX:(float)x maxMaskY:(float)y maxMaskZ:(float)z;
- (BOOL)isEqualToShadingCorrectionData:(CZShadingCorrectionData *)data;

@end

@interface CZMicroscopeModel (ShadingCorrection)

@property (nonatomic, copy) NSString *shadingCorrectionMaskPath;
@property (nonatomic, retain) CZShadingCorrectionData *shadingCorrectionMaskData;
@property (nonatomic, retain) NSMutableDictionary *shadingCorrectionUserInfo;   // Store additional properties like ShadingCorrectionTool

- (BOOL)hasShadingCorrectionDataAtPosition:(NSUInteger)position;
- (CZShadingCorrectionData *)shadingCorrectionDataAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution;
- (void)setShadingCorrectionData:(CZShadingCorrectionData *)data atPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution;

- (UIImage *)shadingCorrectionMaskAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution;
- (void)setShadingCorrectionMask:(UIImage *)mask atPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution;

- (NSString *)pathForShadingCorrectionMaskAtPosition:(NSUInteger)position imageResolution:(CGSize)imageResolution;

- (CZShadingCorrectionData *)currentShadingCorrectionDataWithImageResolution:(CGSize)imageResolution;
- (NSString *)currentPathForShadingCorrectionMaskWithImageResolution:(CGSize)imageResolution;

- (void)removeShadingCorrectionDataAndMaskAtPosition:(NSUInteger)position;

@end
