//
//  CZMicroscopeAxioline.h
//  MicroscopeManager
//
//  Created by Li, Junlin on 2/25/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

@class CZGeminiAPISystemInfo;

/**
 The super class of CZMicroscopeAxioscope and CZMicroscopeAxiolab
 */
@interface CZMicroscopeAxioline : CZMicroscopeModelCompound

@end
