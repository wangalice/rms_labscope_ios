//
//  CZMNABrowser.h
//  Hermes
//
//  Created by Mike Wang on 6/6/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kStartIndexOfLastThreeOctet 9
#define kMNALevelNone @"MNALevelNone"
#define kMNALevelBasic @"MNALevelBasic"
#define kMNALevelAdvanced @"MNALevelAdvanced"

@protocol CZMNABrowserDelegate <NSObject>
- (void)mnaBrowserDidFindMNAs:(NSArray *)mnas;
- (void)mnaBrowserDidLoseMNAs:(NSArray *)mnas;
@end

@interface CZMNA : NSObject <NSCopying>
@property (nonatomic, copy) NSString *macAddress;
@property (nonatomic, copy) NSString *ipAddress;
@property (nonatomic, retain) NSDate *lastActiveTime;
@property (nonatomic, copy) NSString *level;
@end

@interface CZMNABrowser : NSObject
@property (atomic, assign) id<CZMNABrowserDelegate> delegate;
+ (CZMNABrowser *)sharedInstance;
- (NSDictionary *)newMNAList;
- (void)beginBrowsing;
- (void)stopBrowsing;
@end
