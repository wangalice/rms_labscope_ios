//
//  CZNosepiece+Primo.m
//  MicroscopeManager
//
//  Created by Li, Junlin on 7/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNosepiece+Primo.h"

@implementation CZObjective (Primo)

+ (instancetype)epiPlan5x {
    return [[self alloc] initWithMatID:@"442020-9902-000" objectiveName:@"Epi-plan 5x/0,13"];
}

+ (instancetype)epiPlan10x {
    return [[self alloc] initWithMatID:@"442030-9903-000" objectiveName:@"Epi-plan 10x/0,23"];
}

+ (instancetype)epiPlan20x {
    return [[self alloc] initWithMatID:@"442040-9902-000" objectiveName:@"Epi-plan 20x/0,4"];
}

+ (instancetype)epiPlan50x {
    return [[self alloc] initWithMatID:@"442060-9901-000" objectiveName:@"Epi-plan 50x/0,65"];
}

+ (instancetype)epiPlan100x {
    return [[self alloc] initWithMatID:@"442080-9901-000" objectiveName:@"Epi-plan 100x/0,8"];
}

+ (instancetype)aPlan5x {
    return [[self alloc] initWithMatID:@"441023-9900-000" objectiveName:@"A-plan 5x/0,12 Pol"];
}

+ (instancetype)aPlan10x {
    return [[self alloc] initWithMatID:@"441033-9900-000" objectiveName:@"A-plan 10x/0,25 Pol"];
}

+ (instancetype)aPlan20x {
    return [[self alloc] initWithMatID:@"441043-9900-000" objectiveName:@"A-plan 20x/0,45 Pol"];
}

+ (instancetype)aPlan40x {
    return [[self alloc] initWithMatID:@"441053-9900-000" objectiveName:@"A-plan 40x/0,65 Pol"];
}

+ (instancetype)aPlan63x {
    return [[self alloc] initWithMatID:@"441063-9900-000" objectiveName:@"A-plan 63x/0,8 Pol"];
}

+ (instancetype)planAchromat4x_01 {
    return [[self alloc] initWithMatID:@"415500-1600-00x" objectiveName:@"Plan-Achromat 4x/0,10"];
}

+ (instancetype)planAchromat10x_025 {
    return [[self alloc] initWithMatID:@"415500-1601-00x" objectiveName:@"Plan-Achromat 10x/0,25"];
}

+ (instancetype)planAchromat20x_040 {
    return [[self alloc] initWithMatID:@"415500-1606-00x" objectiveName:@"Plan-Achromat 20x/0,40"];
}

+ (instancetype)planAchromat40x_065 {
    return [[self alloc] initWithMatID:@"415500-1602-00x" objectiveName:@"Plan-Achromat 40x/0,65"];
}

+ (instancetype)planAchromat100x_125_oil {
    return [[self alloc] initWithMatID:@"415500-1604-00x" objectiveName:@"Plan-Achromat 100x/1,25 Oil"];
}

+ (instancetype)planAchromat100x_080_dry {
    return [[self alloc] initWithMatID:@"415500-1620-00x" objectiveName:@"Plan-Achromat 100x/0,8 dry"];
}

+ (instancetype)planAchromat20x_040_D_0 {
    return [[self alloc] initWithMatID:@"415500-1610-00x" objectiveName:@"Plan-Achromat 20x/0,4 D=0"];
}

+ (instancetype)planAchromat40x_065_D_0 {
    return [[self alloc] initWithMatID:@"415500-1611-00x" objectiveName:@"Plan-Achromat 40x/0,65 D=0"];
}

+ (instancetype)planAchromat100x_125_D_0_oil {
    return [[self alloc] initWithMatID:@"415500-1612-00x" objectiveName:@"Plan-Achromat 100x/1,25 D=0 Oil"];
}

+ (instancetype)planAchromat10x_025_ph1 {
    return [[self alloc] initWithMatID:@"415500-1605-00x" objectiveName:@"Plan-Achromat 10x/0,25 Ph1"];
}

+ (instancetype)planAchromat20x_045_ph2 {
    return [[self alloc] initWithMatID:@"415500-1607-00x" objectiveName:@"Plan-Achromat 20x/0,45 Ph2"];
}

+ (instancetype)planAchromat40x_065_ph2 {
    return [[self alloc] initWithMatID:@"415500-1603-00x" objectiveName:@"Plan-Achromat 40x/0,65 Ph2"];
}

+ (instancetype)planAchromat100x_125_oil_ph3 {
    return [[self alloc] initWithMatID:@"415500-1608-00x" objectiveName:@"Plan-Achromat 100x/1,25 Oil Ph3"];
}

+ (instancetype)ld20x_03_Ph1 {
    return [[self alloc] initWithMatID:@"415500-1614-00x" objectiveName:@"LD 20x/0,3 Ph1 D=1mm"];
}

+ (instancetype)ld40x_05_Ph1 {
    return [[self alloc] initWithMatID:@"415500-1617-00x" objectiveName:@"LD 40x/0,5 Ph1 D=1mm"];
}

+ (instancetype)ld20x_03_Ph2 {
    return [[self alloc] initWithMatID:@"415500-1618-00x" objectiveName:@"LD 20x/0,3 Ph2 D=1mm"];
}

+ (instancetype)ld40x_05_Ph2 {
    return [[self alloc] initWithMatID:@"415500-1616-00x" objectiveName:@"LD 40x/0,5 Ph2 D=1mm"];
}

+ (instancetype)aPlan2_5x {
    return [[self alloc] initWithMatID:@"441010-9901-000" objectiveName:@"A-plan 2.5x/0,06 Pol"];
}

+ (instancetype)objectiveFromEnumeration:(CZPrimoObjective)enumeration {
    switch (enumeration) {
        case kNoneObjective:
            return [self none];
        case kEpiplan5x:
            return [self epiPlan5x];
        case kEpiplan10x:
            return [self epiPlan10x];
        case kEpiplan20x:
            return [self epiPlan20x];
        case kEpiplan50x:
            return [self epiPlan50x];
        case kEpiplan100x:
            return [self epiPlan100x];
        case kAPlan5x:
            return [self aPlan5x];
        case kAPlan10x:
            return [self aPlan10x];
        case kAPlan20x:
            return [self aPlan20x];
        case kAPlan40x:
            return [self aPlan40x];
        case kAPlan63x:
            return [self aPlan63x];
        case kPlanAchromat4x_01:
            return [self planAchromat4x_01];
        case kPlanAchromat10x_025:
            return [self planAchromat10x_025];
        case kPlanAchromat20x_040:
            return [self planAchromat20x_040];
        case kPlanAchromat40x_065:
            return [self planAchromat40x_065];
        case kPlanAchromat100x_125_oil:
            return [self planAchromat100x_125_oil];
        case kPlanAchromat100x_080_dry:
            return [self planAchromat100x_080_dry];
        case kPlanAchromat20x_040_D_0:
            return [self planAchromat20x_040_D_0];
        case kPlanAchromat40x_065_D_0:
            return [self planAchromat40x_065_D_0];
        case kPlanAchromat100x_125_D_0_oil:
            return [self planAchromat100x_125_D_0_oil];
        case kPlanAchromat10x_025_ph1:
            return [self planAchromat10x_025_ph1];
        case kPlanAchromat20x_045_ph2:
            return [self planAchromat20x_045_ph2];
        case kPlanAchromat40x_065_ph2:
            return [self planAchromat40x_065_ph2];
        case kPlanAchromat100x_125_oil_ph3:
            return [self planAchromat100x_125_oil_ph3];
        case kLD20x_03_Ph1:
            return [self ld20x_03_Ph1];
        case kLD40x_05_Ph1:
            return [self ld40x_05_Ph1];
        case kLD20x_03_Ph2:
            return [self ld20x_03_Ph2];
        case kLD40x_05_Ph2:
            return [self ld40x_05_Ph2];
        case kAplan2_5x:
            return [self aPlan2_5x];
        default:
            return nil;
    }
}

- (CZPrimoObjective)enumerationValue {
    if ([self isEqualToObjective:[CZObjective epiPlan5x]]) {
        return kEpiplan5x;
    } else if ([self isEqualToObjective:[CZObjective epiPlan10x]]) {
        return kEpiplan10x;
    } else if ([self isEqualToObjective:[CZObjective epiPlan20x]]) {
        return kEpiplan20x;
    } else if ([self isEqualToObjective:[CZObjective epiPlan50x]]) {
        return kEpiplan50x;
    } else if ([self isEqualToObjective:[CZObjective epiPlan100x]]) {
        return kEpiplan100x;
    } else if ([self isEqualToObjective:[CZObjective aPlan5x]]) {
        return kAPlan5x;
    } else if ([self isEqualToObjective:[CZObjective aPlan10x]]) {
        return kAPlan10x;
    } else if ([self isEqualToObjective:[CZObjective aPlan20x]]) {
        return kAPlan20x;
    } else if ([self isEqualToObjective:[CZObjective aPlan40x]]) {
        return kAPlan40x;
    } else if ([self isEqualToObjective:[CZObjective aPlan63x]]) {
        return kAPlan63x;
    } else if ([self isEqualToObjective:[CZObjective planAchromat4x_01]]) {
        return kPlanAchromat4x_01;
    } else if ([self isEqualToObjective:[CZObjective planAchromat10x_025]]) {
        return kPlanAchromat10x_025;
    } else if ([self isEqualToObjective:[CZObjective planAchromat20x_040]]) {
        return kPlanAchromat20x_040;
    } else if ([self isEqualToObjective:[CZObjective planAchromat40x_065]]) {
        return kPlanAchromat40x_065;
    } else if ([self isEqualToObjective:[CZObjective planAchromat100x_125_oil]]) {
        return kPlanAchromat100x_125_oil;
    } else if ([self isEqualToObjective:[CZObjective planAchromat100x_080_dry]]) {
        return kPlanAchromat100x_080_dry;
    } else if ([self isEqualToObjective:[CZObjective planAchromat20x_040_D_0]]) {
        return kPlanAchromat20x_040_D_0;
    } else if ([self isEqualToObjective:[CZObjective planAchromat40x_065_D_0]]) {
        return kPlanAchromat40x_065_D_0;
    } else if ([self isEqualToObjective:[CZObjective planAchromat100x_125_D_0_oil]]) {
        return kPlanAchromat100x_125_D_0_oil;
    } else if ([self isEqualToObjective:[CZObjective planAchromat10x_025_ph1]]) {
        return kPlanAchromat10x_025_ph1;
    } else if ([self isEqualToObjective:[CZObjective planAchromat20x_045_ph2]]) {
        return kPlanAchromat20x_045_ph2;
    } else if ([self isEqualToObjective:[CZObjective planAchromat40x_065_ph2]]) {
        return kPlanAchromat40x_065_ph2;
    } else if ([self isEqualToObjective:[CZObjective planAchromat100x_125_oil_ph3]]) {
        return kPlanAchromat100x_125_oil_ph3;
    } else if ([self isEqualToObjective:[CZObjective ld20x_03_Ph1]]) {
        return kLD20x_03_Ph1;
    } else if ([self isEqualToObjective:[CZObjective ld40x_05_Ph1]]) {
        return kLD40x_05_Ph1;
    } else if ([self isEqualToObjective:[CZObjective ld20x_03_Ph2]]) {
        return kLD20x_03_Ph2;
    } else if ([self isEqualToObjective:[CZObjective ld40x_05_Ph2]]) {
        return kLD40x_05_Ph2;
    } else if ([self isEqualToObjective:[CZObjective aPlan2_5x]]) {
        return kAplan2_5x;
    } else {
        return kNoneObjective;
    }
}
 @end
