//
//  CZNosepieceManager.m
//  Hermes
//
//  Created by Mike Wang on 2/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNosepieceManager.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZNosepieceManager

+ (NSArray *)packageNames {
    static NSArray *packNames = nil;
    if (packNames == nil) {
        packNames = [[NSArray alloc] initWithObjects:
                     L(@"MIC_CONFIG_SELECT_OBJECTIVES_MANUAL"),
                     @"415500-0059-000: Primo Star HD HAL/LED, full-Köhler, FOV 20, integrated HD IP Camera 5MP",
                     @"415500-0051-000: Primo Star HAL/LED microscope, fixed-Köhler, stage R, FOV 18, pointer",
                     @"415500-0052-000: Primo Star HAL/LED microscope, fixed-Köhler, stage L, FOV 18",
                     @"415500-0053-000: Primo Star HAL/LED microscope, fixed-Köhler, stage R, FOV 18, 100x/0.8",
                     @"415500-0054-000: Primo Star HAL microscope, full-Köhler, stage R, Ph2, FOV 20, photo",
                     @"415500-0055-000: Primo Star HAL microscope, full-Köhler, stage R, Ph1, Ph2, Ph3, FOV 20",
                     @"415500-0056-000: Primo Star HAL/LED microscope, full-Köhler, stage R, FOV 20, pointer",
                     @"415500-0057-000: Primo Star HAL/LED microscope, full-Köhler, stage R, FOV 20, photo",
                     @"415500-0058-000: Primo Star LED microscope, fixed-Köhler, stage R, FOV 18, D=0",
                     nil];
    }
    return packNames;
}

//Add new entry just like UI
+ (NSArray *)packagesFromDisplay {
    static NSArray *lists = nil;
    if (lists == nil) {
        lists = @[[NSNumber numberWithInt:kCZPrimoStarPackageManualMode],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HD_HAL_LED_Full_Kohler_FOV20_Integrated_HD_IP_Camera_5MP],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_Pointer],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_L_FOV18],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_LED_Fixed_Kohler_Stage_R_FOV18_100x_08],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph2_FOV20_Photo],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_Full_Kohler_Stage_R_Ph1_Ph2_Ph3_FOV20],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Pointer],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_HAL_LED_Full_Kohler_Stage_R_FOV20_Photo],
                  [NSNumber numberWithInt:kCZPrimoStarPackage_LED_Fixed_Kohler_Stage_R_FOV18_D0]];
        [lists retain];
    }
    
    return lists;
}

+ (NSArray *)displayFromPackages {
    static NSArray *lists = nil;
    if (lists == nil) {
        NSMutableArray *mutableList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [self packagesFromDisplay].count; i++) {
            [mutableList addObject:@0];
        }
        
        for (int i = 0; i < [self packagesFromDisplay].count; i++) {
            int packageValue = [[self packagesFromDisplay][i] intValue];
            mutableList[packageValue] = [NSNumber numberWithInt:i];
        }
        lists = [mutableList copy];
        [mutableList release];
    }
    
    return lists;
}

@end
