//
//  CZMicroscopeAxiocamCompound.m
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeAxiocamCompound.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZCamera+MicroscopeManagerAdditions.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

static const double kAxiocamBasicFOV = 5660;  // CCD sensor width 5700 * 0.99 um

@interface CZMicroscopeAxiocamCompound ()

@property (nonatomic, copy) NSString *cameraModelName;

@end

@implementation CZMicroscopeAxiocamCompound

- (void)dealloc {
    [_cameraModelName release];
    [super dealloc];
}

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kAxiocamCompound];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet setForAxiocamCompound] retain];
    }
    return zoomLevelSet;
}

- (NSUInteger)defaultNosepiecePositions {
    return 5;
}

+ (UIImage *)modelIcon {
    return nil;
}

+ (NSString *)cameraModelName {
    return nil;
}

- (instancetype)initWithCamera:(CZCamera *)camera {
    self = [super initWithCamera:camera];
    if (self) {
        _cameraModelName = [camera.cameraModelName copy];
    }
    return self;
}

- (void)initTypeZooms:(NSMutableDictionary *)typeZooms {
    CZZoomLevelSet *zoomSet = [self availableZoomLevelSet];
    
    // camera adapter
    NSArray *array = [zoomSet availableZoomsForType:kZoomTypeCameraAdapter];
    NSAssert(array.count == 4, @"camera adapter has 4 available zooms");
    
    CZZoomLevel *zoomLevel = array[1];
    NSAssert(zoomLevel.magnification == 0.5, @"default zoom is 0.5");
    
    [typeZooms setObject:zoomLevel forKey:kZoomTypeCameraAdapter];
}

- (void)initAdditionalZooms:(NSMutableDictionary *)additionalZooms {
    additionalZooms[kZoomTypeAdditionalOptovar] = @1.0;
}

- (CZMicroscopeTypes)type {
    return kAxiocamCompound;
}

- (NSString *)modelName {
    return kModelNameCompound;
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    if (self.cameraClass == [CZLocalFileCamera class]) {
        return kNonCalibratedValue;
    }
    
    float magnification = [self cameraMagnificationAtPosition:position];
    if (magnification <= 0.0f) {
        return kNonCalibratedValue;
    } else {
        return kAxiocamBasicFOV / magnification;
    }
}

@end
