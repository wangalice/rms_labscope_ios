//
//  CZMicroscopePrimoTech.m
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopePrimoTech.h"
#import "CZMicroscopeModelSubclass.h"
#import "CZMicroscopeModel+MNA.h"
#import "CZNosepiece+Primo.h"
#import <CZToolbox/CZToolbox.h>
#import <CZCameraInterface/CZCameraInterface.h>

@implementation CZMicroscopePrimoTech

+ (double)objectiveWisionFOVWidth:(NSUInteger)objective {
    const NSDictionary<NSNumber *, NSNumber *> *fovWidthMap = @{
                                                                @(kNoneObjective) :  @kNonCalibratedValue,        //kNoneObjective
                                                                @(kEpiplan5x)     :  @(2000.0 / 1781.0 * 2048.0), //kEpiplan5x
                                                                @(kEpiplan10x)    :  @(1000.0 / 1785.0 * 2048.0), //kEpiplan10x
                                                                @(kEpiplan20x)    :  @(500.0 / 1785.0 * 2048.0),  //kEpiplan20x
                                                                @(kEpiplan50x)    :  @(200.0 / 1781.0 * 2048.0),  //kEpiplan50x
                                                                @(kEpiplan100x)   :  @(100.0 / 1790.0 * 2048.0),  //kEpiplan100x
                                                                @(kAplan2_5x)     :  @(4000.0 / 1786.0 * 2048.0),  //kAPlan2.5x
                                                                @(kAPlan5x)       :  @(2000.0 / 1786.0 * 2048.0), //kAPlan5x
                                                                @(kAPlan10x)      :  @(1000.0 / 1780.0 * 2048.0), //kAPlan10x
                                                                @(kAPlan20x)      :  @(500.0 / 1778.0 * 2048.0),  //kAPlan20x
                                                                @(kAPlan40x)      :  @(250.0 / 1780.0 * 2048.0),  //kAPlan40x
                                                                @(kAPlan63x)      :  @(150.0 / 1681.0 * 2048.0),  //kAPlan63x
                                                                };
    
    NSNumber* value = [fovWidthMap objectForKey:[NSNumber numberWithUnsignedInteger:objective]];
    return value == nil ? kNonCalibratedValue : value.doubleValue;
}

+ (double)objectiveKappaFOVWidth:(NSUInteger)objective {
    const double kCameraAdaptiveRatio = 0.5 / 0.39;
    const NSDictionary<NSNumber *, NSNumber *> *fovWidthMap = @{
                                                               @(kNoneObjective)  :  @(kNonCalibratedValue),                     //kNoneObjective
                                                               @(kEpiplan5x)      :  @(0.8748 * 2560.0 * kCameraAdaptiveRatio),  //kEpiplan5x
                                                               @(kEpiplan10x)     :  @(0.4374 * 2560.0 * kCameraAdaptiveRatio),  //kEpiplan10x
                                                               @(kEpiplan20x)     :  @(0.2199 * 2560.0 * kCameraAdaptiveRatio),  //kEpiplan20x
                                                               @(kEpiplan50x)     :  @(0.0874 * 2560.0 * kCameraAdaptiveRatio),  //kEpiplan50x
                                                               @(kEpiplan100x)    :  @(0.0437 * 2560.0 * kCameraAdaptiveRatio),  //kEpiplan100x
                                                               @(kAplan2_5x)      :  @(1.7496 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan2.5x
                                                               @(kAPlan5x)        :  @(0.8748 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan5x
                                                               @(kAPlan10x)       :  @(0.4374 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan10x
                                                               @(kAPlan20x)       :  @(0.2199 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan20x
                                                               @(kAPlan40x)       :  @(0.1095 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan40x
                                                               @(kAPlan63x)       :  @(0.0695 * 2560.0 * kCameraAdaptiveRatio),  //kAPlan63x
                                                               };
    
    NSNumber* value = [fovWidthMap objectForKey:[NSNumber numberWithUnsignedInteger:objective]];
    return value == nil ? kNonCalibratedValue : value.doubleValue;
}

+ (void)load {
    [CZMicroscopeModel registerMicroscopeModelClass:[self class] forType:kPrimotech];
}

+ (CZZoomLevelSet *)availableZoomLevelSet {
    static CZZoomLevelSet *zoomLevelSet = nil;
    if (zoomLevelSet == nil) {
        zoomLevelSet = [[CZZoomLevelSet alloc] init];
    }
    return zoomLevelSet;
}

- (NSArray<CZObjective *> *)availableObjectives {
    return @[[CZObjective none],
             [CZObjective epiPlan5x],
             [CZObjective epiPlan10x],
             [CZObjective epiPlan20x],
             [CZObjective epiPlan50x],
             [CZObjective epiPlan100x],
             [CZObjective aPlan2_5x],
             [CZObjective aPlan5x],
             [CZObjective aPlan10x],
             [CZObjective aPlan20x],
             [CZObjective aPlan40x],
             [CZObjective aPlan63x]];
}

- (NSUInteger)defaultNosepiecePositions {
    return 5;
}

+ (UIImage *)modelIcon {
    return [UIImage imageNamed:A(@"model-primotech")];
}

- (CZMicroscopeTypes)type {
    return kPrimotech;
}

- (BOOL)canSupportMNA {
    return YES;
}

- (NSString *)modelName {
    return kModelNamePrimotech;
}

- (float)defaultFOVWidthAtPosition:(NSUInteger)position {
    CZObjective *objective = [self.nosepiece objectiveAtPosition:position];
    NSUInteger userObjective = [objective enumerationValue];
    
    float defaultScaling = kNonCalibratedValue;
    if (self.cameraClass == [CZKappaCamera class]) {
        defaultScaling = [CZMicroscopePrimoTech objectiveKappaFOVWidth:userObjective];
    } else if (self.cameraClass == [CZWisionCamera class]) {
        defaultScaling = [CZMicroscopePrimoTech objectiveWisionFOVWidth:userObjective];
    }
    
    return defaultScaling;
}

@end
