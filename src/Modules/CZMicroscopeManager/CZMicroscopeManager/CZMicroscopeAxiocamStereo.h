//
//  CZMicroscopeAxiocamStereo.h
//  Labscope
//
//  Created by Ralph Jin on 4/15/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMicroscopeModel.h"

@interface CZMicroscopeAxiocamStereo : CZMicroscopeModelStereo

@end
