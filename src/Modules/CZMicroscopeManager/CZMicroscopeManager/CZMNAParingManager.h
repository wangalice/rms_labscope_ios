//
//  CZMNAParingManager.h
//  Matscope
//
//  Created by Mike Wang on 9/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZMNA;
@class CZMicroscopeModel;
@class CZMNAParingManager;

@protocol CZMNAParingManagerDelegate <NSObject>
@optional
- (void)mnaPairingManager:(CZMNAParingManager *)pairingManager
         didFindPairedMNA:(CZMNA *)pairedMNA;
@end

@interface CZMNAParingManager : NSObject
@property (atomic, assign) id<CZMNAParingManagerDelegate> delegate;
- (void)searchMNAPairedWithModelAsync:(CZMicroscopeModel *)model;
- (NSDictionary *)newMNAList;
- (NSDictionary *)newPairedModelList;

- (void)resetMNA:(CZMNA *)mna completionHandler:(void(^)(NSError *connectionError))completionHandler;
- (void)pairMNA:(CZMNA *)mna microscope:(CZMicroscopeModel *)microscope;
/// MNA is reset by other client, so unpair MNA with microscope model without modify data on MNA device.
- (void)unpairMNA:(CZMNA *)mna;
@end
