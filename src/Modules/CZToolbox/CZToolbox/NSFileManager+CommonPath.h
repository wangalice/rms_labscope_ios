//
//  NSFileManager+CommonPath.h
//  Hermes
//
//  Created by Li, Junlin on 12/19/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (CommonPath)

@property (nonatomic, readonly) NSString *cz_documentPath;
@property (nonatomic, readonly) NSString *cz_cachePath;

@end
