//
//  CZUndoAction.h
//  Hermes
//
//  Created by Ralph Jin on 1/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, CZUndoActionType) {
    CZUndoActionTypeUnknown = 0,
    CZUndoActionTypeImage = 1 << 0,
    CZUndoActionTypeAnnotation = 1 << 1,
    CZUndoActionTypeMultichannel = 1 << 2,
    CZUndoActionTypeMultiphase = 1 << 3,
    CZUndoActionTypeGrainSize = 1 << 4,
    CZUndoActionTypeAll = CZUndoActionTypeImage | CZUndoActionTypeAnnotation | CZUndoActionTypeMultichannel | CZUndoActionTypeMultiphase | CZUndoActionTypeGrainSize
};

/*! protocol of undoable action. */
@protocol CZUndoAction<NSObject>

- (NSUInteger)costMemory;

/*! The action will call do, when it is pushed into undo manager.*/
// TODO: Rename to other name to avoid conflict with system keyword.
- (void)do;

- (void)undo;

- (void)redo;

- (CZUndoActionType)type;

@end
