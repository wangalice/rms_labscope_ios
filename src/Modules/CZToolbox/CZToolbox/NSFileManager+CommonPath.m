//
//  NSFileManager+CommonPath.m
//  Hermes
//
//  Created by Li, Junlin on 12/19/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSFileManager+CommonPath.h"

@implementation NSFileManager (CommonPath)

- (NSString *)cz_documentPath {
    // TODO: Using [self URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] is preferred.
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
    NSString *documentPath = [NSString stringWithFormat:@"%@/", path];
    return documentPath;
}

- (NSString *)cz_cachePath {
    // TODO: Using [self URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] is preferred.
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *cachePath = [NSString stringWithFormat:@"%@/", path];
    return cachePath;
}

@end
