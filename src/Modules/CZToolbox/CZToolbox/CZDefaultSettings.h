//
//  CZDefaultSettings.h
//  Hermes
//
//  Created by Mike Wang on 3/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! History of kGlobalModelVersionNumber
 *
 * Labscope 1.0 ....... v1 \n
 * Labscope 1.0.1 ..... v1 \n
 * Labscope 1.0.2 ..... v2 \n
 * Labscope 1.0.3 ..... v3 \n
 * Labscope 1.0.4 ..... v4 \n
 * Labscope 1.1 ....... v5 \n
 * Labscope 1.1.1...... v5 \n
 * Labscope 1.1.2...... v5 \n
 * Labscope 1.2 ....... v6 \n
 * Labscope 1.3 ....... v7 \n
 * Labscope 1.3.1 ..... v7 \n
 * Labscope 2.0 ....... v8 \n
 * Labscope 2.1 ....... v9 \n
 * Labscope 2.1.1...... v10 \n
 * Labscope 2.2 ....... v11 \n
 * Labscope 2.3 ....... v12 \n
 * Labscope 2.5 ....... v13 \n
 * Labscopr 3.0 ....... v14 \n
 *
 * Matscope 1.0 ....... v3 \n
 * Matscope 1.0.1 ..... v3 \n
 * Matscope 1.0.2 ..... v4 \n
 * Matscope 1.1 ....... v5 \n
 * Matscope 1.1.1 ..... v5 \n
 * Matscope 1.1.2 ..... v5 \n
 * Matscope 1.2 ....... v6 \n
 * Matscope 1.3 ....... v7 \n
 * Matscope 1.3.1 ..... v7 \n
 * Matscope 2.0 ....... v8 \n
 * Matscope 2.1 ....... v9 \n
 * Matscope 2.1.1 ..... v10 \n
 * Matscope 2.2 ....... v11 \n
 * Matscope 2.3 ....... v12 \n
 * Matscope 2.5 ....... v13 \n
 *
 * Labscope 2.0 (iPhone) ..... v8 \n
 * Labscope 2.1 (iPhone) ..... v9 \n
 * Labscope 2.1.1 (iPhone) ... v10 \n
 * Labscope 2.2 (iPhone) ..... v11 \n
 * Labscope 2.3 (iPhone) ..... v12 \n
 * Labscope 2.5 (iPhone) ..... v13 \n
 *
 * Matscope 2.5 (iPhone) ..... v13 \n
 *
*/
#define kGlobalModelVersionNumber 13
#define kCurrentShowWhatsNewVersionNumber @"2.5"
#define kCurrentShowWhatsNewVersionNumberIPhone kCurrentShowWhatsNewVersionNumber

/*! version number of showing whats new page in default user setting. since v6 [string]*/
#define kLastShowWhatsNewVersion @"last_show_whats_new_version"

/*! version number of global model which saved in default user setting. since v1 [integer]*/
#define kGlobalModelVersion @"global_model_version"

/*! version number of microscope model. since v1 [integer]*/
#define kMicroscopeModelVersion @"microscope_model_version"

/*! whether to do a factory reset on next launch of the app. since v3 [boolean]*/
#define kFactoryResetOnNextLaunch @"factory_reset"

/*! Enable/disable stay in the live after snapping; in system setting. since v5 [boolean]*/
#define kStayInLiveTabAfterSnapping @"stay_in_live_tab_after_snapping"

/*! Enable/disable macro snap in live tab*/
#define kEnableMacroSnap @"enable_macro_snap"

/*! Enable/diable multichannel snap in smart camera*/
#define kEnableMultichannelSnap @"enable_multichannel_snap"

/*! show/hide virtual sample microscopes; in system setting. since v1 [boolean]; update this toggle option to three options; in system setting. since v10 [integer]
 * 0 ... when no real microscopes are available (default) \n
 * 1 ... Always \n
 * 2 ... Never \n
 */
#define kShowVirtualMicroscopes @"show_virtual_microscopes"

typedef NS_ENUM(NSUInteger, CZVirtualMicroscopesOption) {
    kCZAutomaticShowVirtualMicroscopes,
    kCZAlwaysShowVirtualMicroscopes,
    kCZNeverShowVirtualMicroscopes
};

/*! enable/hide virtual sample microscopes; in system setting. since v1 [boolean]*/
#define kEnableSingleMicroscopeMode @"microscopes_mode"

/*! show/hide manually add microscopes; in system setting. since v10 [boolean]*/
#define kManuallyAddMicroscopes @"manually_add_microscopes"

/*! default corner position of new created scale bar; in system setting. since v1 [integer]
 *
 * 0 ... top left \n
 * 1 ... top right \n
 * 2 ... bottom left \n
 * 3 ... bootom right (default) \n
 * 4 ... hidden \n
 */

/*! URL of samba server, which doesn't contain share folder name; in system setting. since v1 [string]*/
#define kServerURL @"server_url"

/*! share folder name of samba server; in system setting. since v1 [string]*/
#define kServerSharePath @"share_path"

/*! login user name of samba server; in system setting. since v1 [string]*/
#define kUserName @"user_name"

/*! login user passcode of samba server; in system setting. since v1 [string]*/
#define kPassword @"user_password"

/*! Window domain name of samba server; in system setting. since v1 [string]*/
#define kDomain @"domain"

/*! Enable/disable direct copying to network folder after snap. in system setting. since v4 [boolean]*/
#define kCopyToServerAfterSnap @"copy_to_server_after_snap"

/*! Default playback speed of timelapse video. in system setting. since v6 [integer]
 *
 * 0 ... Low (5 fps) \n
 * 1 ... Medium (15 fps) (default) \n
 * 2 ... High (30 fps) \n
 */
#define kTimelapseVideoPalybackRate @"timelapse_video_playback_rate"

typedef NS_ENUM(NSUInteger, CZTimelapseVideoPlayRate) {
    kCZTimelapseVideoPlayRateLow = 0,
    kCZTimelapseVideoPlayRateMedium = 1, //default
    kCZTimelapseVideoPlayRateHigh = 2
};

#define kTimelapseVideoTimeInterval @"timelapse_video_time_interval"

/*! URL of remote stream. in system setting. since v7 [string]*/
#define kRemoteStreamURL @"remote_stream_url"

/*! Enable/disable sharing image by email. in system setting. since v1 [boolean]*/
#define kShareMail @"share_mail"

/*! Enable/disable sharing image to the social media website like facebook, twitte etc. in system setting. since v2 [boolean]*/
#define kShareSocial @"share_social"

/*! Enable/disable sharing image to facebook. in system setting. since v1, deprecated in v2 [boolean]*/
#define kShareFacebook @"share_facebook"

/*! Enable/disable sharing image to twitter. in system setting. since v1, deprecated in v2 [boolean]*/
#define kShareTwitter @"share_twitter"

/*! Enable/disable sharing image to sina weibo. in system setting. since v1, deprecated in v2 [boolean]*/
#define kShareSinaWeibo @"share_sina_weibo"

/*! Enable/disable log. in system setting [debug mode]. since v1 [boolean]*/
#define kEnableLog @"enable_log"

/*! Show/hide camera which MAC address is not belong to Carl Zeiss. in system setting [debug mode]. since v1 [boolean]*/
#define kDisableMacAddressFilter @"disable_mac_address_filter"

/*! Show/hide non released microscope models. in system setting [debug mode]. since v5 [boolean]*/
#define kEnableNonReleasedMicroscopes @"enable_non_released_microscopes"

/*! Show/hide laser pointer. since v6 [boolean]*/
#define kShowLaserPointer @"show_laser_pointer"

/*! URL of default file name template. since v1 [string]*/
#define kDefaultFileTemplateURL @"default_file_name_template_url"

/*! default file name of simple file name template. since v1 [string]*/
#define kDefaultFileNameTemplateText @"default_file_name_template_text"

/*! first time launch flag for copy demo files. since v1 [boolean] */
#define kFirstTimeCopyDemoFiles @"first_time_copy_demo_files"

/*! number of cameras per page shown in microscope tab [6|12|20]. since v1 [integer]*/
#define kNumOfCamerasPerPage @"num_of_cameras_per_page"

// info screen keys
/*! Enable/disable show help information overlay. in system setting. since v1[integer]
 *
 * 0 ... user defined mode \n
 * 1 ... enable all mode \n
 * 2 ... disable all mode \n
 */
#define kInfoScreen @"info_screen"

/*! Enable/disable show help information overlay in microscope tab. since v1 [boolean]*/
#define kHideInfoScreenMicTab @"hide_info_screen_mic_tab"

/*! Enable/disable show help information overlay in live stream tab. since v1 [boolean]*/
#define kHideInfoScreenLiveTab @"hide_info_screen_live_tab"

/*! Enable/disable show help information overlay in live stream tab's display curve selection mode. since v1 [boolean]*/
#define kHideInfoScreenLiveTabDisplayCurve @"hide_info_screen_live_tab_display_curve"

/*! Enable/disable show help information overlay in image tab's display curve selection mode. since v6 [boolean]*/
#define kHideInfoScreenImageTabDisplayCurve @"hide_info_screen_image_tab_display_curve"

/*! Enable/disable show help information overlay in live stream tab's fast snapping mode. since v6 [boolean]*/
#define kHideInfoScreenFastSnapping @"hide_info_screen_fast_snapping"

/*! Enable/disable show help information overlay in live stream tab's EDOF snapping mode. since v2 [boolean]*/
#define kHideInfoScreenEDOF @"hide_info_screen_edof"

/*! Enable/disable show help information overlay in live stream tab's drawing tube mode. since v5 [boolean]*/
#define kHideInfoScreenDrawingTube @"hide_info_screen_drawing_tube"

/*! Enable/disable show help information overlay in live stream tab's drawing tube mode. since v8 [boolean]*/
#define kHideInfoScreenImageTabDrawingTube @"hide_info_screen_image_tab_drawing_tube"

/*! Enable/disable show help information overlay in live stream tab's Macro photo snapping mode. since v5 [boolean]*/
#define kHideInfoScreenMacro @"hide_info_screen_macro"

/*! Enable/disable show help information overlay in live stream tab's focus indicator mode. since v6 [boolean]*/
#define kHideInfoScreenFocusIndicator @"hide_info_screen_focus_indicator"

/*! Enable/disable show help information overlay in live stream tab's overexposure indicator mode. since v6 [boolean]*/
#define kHideInfoScreenLiveTabOverexposure @"hide_info_screen_live_tab_foverexposure"

/*! Enable/disable show help information overlay in image tab's overexposure indicator mode. since v6 [boolean]*/
#define kHideInfoScreenImageTabOverexposure @"hide_info_screen_image_tab_foverexposure"

/*! Enable/disable show help information overlay in live stream tab's laser pointer mode. since v6 [boolean]*/
#define kHideInfoScreenLiveTabLaserPointer @"hide_info_screen_live_tab_laser_pointer"

/*! Enable/disable show help information overlay in image tab's laser pointer mode. since v6 [boolean]*/
#define kHideInfoScreenImageTabLaserPointer @"hide_info_screen_image_tab_laser_pointer"

/*! Enable/disable show help information overlay in image tab. since v1 [boolean]*/
#define kHideInfoScreenImageTab @"hide_info_screen_image_tab"

/*! Enable/disable show help information overlay in image tab. since v1 [boolean]*/
#define kHideInfoScreenImageTabAnnotation @"hide_info_screen_image_tab_annotation"

/*! Enable/disable show help information overlay in image tab's annotation editing mode. since v1 [boolean]*/
#define kHideInfoScreenFileTab @"hide_info_screen_file_tab"

/*! Enable/disable show help information overlay in microscope configuration view. since v1 [boolean]*/
#define kHideInfoScreenMicConfig @"hide_info_screen_mic_config"

/*! Enable/disable show help information overlay in file name template edit view. since v1 [boolean]*/
#define kHideInfoScreenFNT @"hide_info_screen_fnt"

// auto backup
/*! URL of backup image file. since v1 [string]*/
#define kURLToBeBackup @"URLToBeBackup"

/*! Boolean flag of whether the backup image is newly snapped. since v1 [boolean]*/
#define kIsImageSnapped @"kIsImageSnapped"

/*! Boolean flag of whether the backup image is new (not saved yet). since v1 [boolean]*/
#define kIsImageNew @"kIsImageNew"

/*! Boolean flag of whether the samba server is setup or not. since v1 [boolean]*/
#define kIsServerSetup @"is_server_setup"

// file tab sort order, file filter.
/*! sort order in file tab for local file list. since v1 [integer]
 *
 * 0 ... sort by name ascending \n
 * 1 ... sort by name descending \n
 * 2 ... sort by date ascending \n
 * 3 ... sort by date descending \n
 */
#define kCZLocalSortingOrderKey @"local_file_sorting_order"

/*! sort order in file tab for remote(samba server) file list. since v1 [integer]
 *
 * 0 ... sort by name ascending \n
 * 1 ... sort by name descending \n
 * 2 ... sort by date ascending \n
 * 3 ... sort by date descending \n
 */
#define kCZRemoteSortingOrderKey @"remote_file_sorting_order"

/*! file filter type in file tab for local file list. since v1 [integer]; assign filter by operator to 5. since v6 [integer];
 *
 * 0x1 << 0... filter by image, jpg, czi etc. \n
 * 0x1 << 1 ... filter by video, mp4; \n
 * 0x1 << 2 ... filter by report, RTF,PDF; \n
 * 0x1 << 3 ... filter by template, czftp, czrtj, czamt; \n
 * 0x1 << 4 ... filter by operator. \n
 * 0x1 << 5 ... don't filer, show all; \n
 */
#define kCZLocalFileFilterKey @"local_file_filter_option"

/*! file filter type in file tab for remote(samba server) file list. since v1 [integer]
 *
 * 0 ... filter by image, jpg, czi; \n
 * 1 ... filter by video, mp4; \n
 * 2 ... filter by report, RTF, PDF; \n
 * 3 ... filter by template, czftp, czrtj. czamt; \n
 * 4 ... don't filer, show all. \n
 */
#define kCZRemoteFileFilterKey @"remote_file_filter_option"

/*! tab index of last select tab. since v1 [integer]
 *
 * 0 ... file view tab; \n
 * 1 ... image view tab; \n
 * 2 ... live view tab; \n
 * 3 ... microscope view tab. \n
 */
#define kSelectedTab @"SelectedTAB"

/*! Mac address of default camera choose by user. since v1 [string]*/
#define kDefaultCameraMACAddress @"DefaultCameraMACAddress"

/*! enable MNA level simulator v1 [string]
 *
 * Default is no, yes for debug use
 */
#define kEnableMNALevelSimulator @"enable_mna_level_simulator"

/*! enable MNA level simulator v6 [boolean, debug]
 *
 * Default is no
 */
#define kEnableCurveLayerThickness @"enable_curve_layer_thickness"

/*! MNA level v1 [string]
 *
 * MNALevelNone ... There is no mna for the microscope; \n
 * MNALevelBasic ... There is basic mna for the microscope; \n
 * MNALevelAdvanced ... There is advanced mna for the microscope; \n
 */
#define kMNALevel @"mna_level"

typedef NS_ENUM(NSUInteger, CZInformationScreenOption) {
    CZInformationScreenOptionUserDefined = 0,
    CZInformationScreenOptionEnableAll = 1,
    CZInformationScreenOptionDisableAll = 2
};

// Add shortcut mode
/*! shortcut mode; in system setting. since v2 [CZShortcutMode]
 *
 * 0 ... shortcut mode none (default) \n
 * 1 ... pcb measurement shortcut enable \n
 * 2 ... continuous snapping shortcut enable \n
 */
#define kCZShortcutMode @"shortcut_mode"

typedef NS_ENUM(NSUInteger,CZShortcutMode) {
    kCZShortcutModeNone = 0,
    kCZShortcutModePCBMeasurement = 1
};

/*! debug camera IP address. since v4 [NSString] */
#define kDebugCameraIPAddress @"debug_camera_ip_address"

/*! default ROI range. since v5 [complex].
 * @{ @"class": NSStringFromClass([CZElementXXX class]),
 *    @"shape": [element newShapeMemo] }
 */
#define kDefaultROIRange @"default_roi"

/*! user last chose grain size standard. since v5 [CZGrainsStandard, same as NSUInteger]
 * see CZGrainsStandard for detail.
 */
#define kDefaultGrainsStandard @"default_grains_standard"

/*! user last chose grain size chord pattern. since v5 [CZGrainSizePattern, same as NSUInteger]
 * see CZGrainSizePattern for detail.
 */
#define kDefaultGrainsPattern @"default_grains_pattern"

/*
 * A singleton pattern implementation that provides a unified 
 * way to retrieve from system settings *
 */

#define kCZFilesDisplayViewMode @"files_display_view_mode"

/*! files display view mode. since v9 [CZFilesDisplayViewMode]
 *
 * 0 ... files list view mode for both local and remote side (default) \n
 * 1 ... files grid view mode for both local and remote side \n
 */

#define kDefaultLocalThumbnailGridViewZoomLevel @"local_thumbnail_grid_view_zoom_level"
/*! user last local thumnbnail grid view zoom level[0.5 ~ 1.0]. since v9 [float]
 */

#define kDefaultRemoteThumbnailGridViewZoomLevel @"remote_thumbnail_grid_view_zoom_level"
/*! user last remote thumnbnail grid view zoom level[0.5 ~ 1.0]. since v9 [float]
 */

/*! manually save camera settings into the default settings. since v10 [NSString] */
#define kManuallyAddMicroscopeSettings @"manually_add_microscope_settings"
#define kManuallyAddMicroscopeName  @"manually_add_microscope_name"
#define kManuallyAddMicroscopeIPAddress @"manually_add_microscope_ip_address"
#define kManuallyAddMicroscopeMacAddress @"manually_add_microscope_mac_address"

/*! Enable/disable fixed shared folder option. in system setting. since v10 [boolean]*/
#define kFixedSharedFolder @"fixed_shared_folder"

/*! Shared path list in system setting. since v10 [NSArray]*/
#define kSharedPaths @"shared_paths"
/*! Digital Classroom configuration host in system setting. since Version 2.8 */
#define kDigitalClassroomHost @"dcm_ip_address"

typedef NS_ENUM(NSUInteger, CZFilesDisplayViewMode) {
    kCZFilesListViewMode = 0,
    kCZFilesGridViewMode = 1
};

@interface CZDefaultSettings : NSObject

/*! The one and only way to get a instance of CZDefaultSettings. */
+ (CZDefaultSettings *)sharedInstance;

@property (nonatomic, readwrite, assign) BOOL factoryResetOnNextLaunch;
@property (nonatomic, readwrite, assign) BOOL enableMultiMicroscopes;
@property (nonatomic, readwrite, retain) NSString *serverURL;
@property (nonatomic, readonly) NSString *serverSharePath;
@property (nonatomic, readonly) NSString *userName;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, readonly) NSString *domain;
@property (nonatomic, readonly) BOOL enableCopyToServerAfterSnap;
@property (nonatomic, readonly) BOOL enableLog;
@property (nonatomic, readonly) BOOL disableMacAddressFilter;
@property (nonatomic, readonly) BOOL enableRTFReportGeneration;
@property (nonatomic, readwrite, assign) BOOL showLaserPointer;
@property (nonatomic, readwrite, assign) CZTimelapseVideoPlayRate videoPlayRate;
@property (nonatomic, readwrite, assign) NSTimeInterval timelapseIntervel;
@property (nonatomic, readonly) NSString *remoteStreamURL;
@property (nonatomic, readwrite) CZInformationScreenOption infoScreenOption;
@property (nonatomic, readonly) BOOL shareToMailEnabled;
@property (nonatomic, readonly) BOOL shareToScailNetworkEnabled;
@property (nonatomic, readwrite, assign) BOOL stayInLiveTabAfterSnappingEnabled;
@property (nonatomic, readonly) CZFilesDisplayViewMode filesDisplayViewMode;
@property (nonatomic, readwrite) CZVirtualMicroscopesOption virtualMicroscopesOption;
@property (nonatomic, readwrite) BOOL enableManuallyAddMicroscopes;
@property (nonatomic, readonly) BOOL enableFixedSharedFolder;
@property (nonatomic, readonly, getter=defaultCameraMACAddress) NSString *defaultCameraMACAddress;

@property (nonatomic, readonly) CZShortcutMode shortcutMode;
@property (nonatomic, readwrite) BOOL enableMacroSnap;
@property (nonatomic, readonly, assign) BOOL enableMultichannelSnap;

- (void)saveServerInfo:(NSDictionary *)info;

- (void)setFilesDisplayViewMode:(CZFilesDisplayViewMode)filesDisplayViewMode;

- (BOOL)addManuallyMicroscopeSettingsToHistory:(NSDictionary <NSString *, NSString *> *)settings;
- (void)removeManuallyMicroscopeSettingsAtMacAddress:(NSString *)macAddress;
- (void)updateManuallyMicroscopeSettingsName:(NSString *)name atMacAddress:(NSString *)macAddress;
- (BOOL)containsManuallyMicroscopeSettingsAtMacAddress:(NSString *)macAddress;
- (NSArray<NSDictionary *> *)manuallyAddMicroscopesSettings;

- (void)restoreAllSettingsWhenNeed;

- (void)setDefaultCameraMACAddress:(NSString *)macAddress;

@end
