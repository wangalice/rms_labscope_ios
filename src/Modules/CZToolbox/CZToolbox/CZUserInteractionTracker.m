//
//  CZUserInteractionTracker.m
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import "CZUserInteractionTracker.h"
#import "CZUserInteractionLog.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>

NSString * const kCZEventLogChangedNotification = @"kCZEventLogChangedNotification";
NSString * const kCZUserInteractionLog = @"kCZUserInteractionLog";
NSString * const kCZUserInteractionLogCategory = @"kCZUserInteractionLogCategory";
NSString * const kCZTimingEventCategory = @"kCZTimingEventCategory";
NSString * const kCZUIActionEventCategory = @"kCZUIActionEventCategory";

@interface CZUserInteractionTracker ()

@end

@implementation CZUserInteractionTracker

#pragma mark - Singleton Design Pattern

+ (CZUserInteractionTracker *)defaultTracker {
    static CZUserInteractionTracker *defaultTracker = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultTracker = [[CZUserInteractionTracker alloc] init];
    });
    
    return defaultTracker;
}

#pragma mark -
#pragma mark Main Methods

- (void)logEventWithCategory:(NSString *)category
                       event:(NSString *)event
                        time:(NSDate *)time
                  parameters:(NSDictionary *)parameters
            postNotification:(BOOL)enabled {
    CZUserInteractionLog *eventLog = [CZUserInteractionLog MR_createEntity];
    
    eventLog.category = category;
    eventLog.event = event;
    eventLog.time = time;
    NSDictionary *eventParameters = parameters;
    NSUInteger count = eventParameters.count;
    // Parse the parameters
    if (count > 0) {
        if (count == 1) {
            eventLog.key1 = eventParameters.allKeys[0];
            eventLog.parameter1 = eventParameters.allValues[0];
            eventLog.key2 = nil;
            eventLog.parameter2 = nil;
            eventLog.key3 = nil;
            eventLog.parameter3 = nil;
        } else if (count == 2) {
            eventLog.key1 = eventParameters.allKeys[0];
            eventLog.parameter1 = eventParameters.allValues[0];
            eventLog.key2 = eventParameters.allKeys[1];
            eventLog.parameter2 = eventParameters.allValues[1];
            eventLog.key3 = nil;
            eventLog.parameter3 = nil;
        } else if (count >= 3) {
            eventLog.key1 = eventParameters.allKeys[0];
            eventLog.parameter1 = eventParameters.allValues[0];
            eventLog.key2 = eventParameters.allKeys[1];
            eventLog.parameter2 = eventParameters.allValues[1];
            eventLog.key3 = eventParameters.allKeys[2];
            eventLog.parameter3 = eventParameters.allValues[2];
        }
    } else {
        eventLog.key1 = nil;
        eventLog.parameter1 = nil;
        eventLog.key2 = nil;
        eventLog.parameter2 = nil;
        eventLog.key3 = nil;
        eventLog.parameter3 = nil;
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    if (enabled) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kCZEventLogChangedNotification
                                                            object:nil
                                                          userInfo:@{kCZUserInteractionLog : eventLog,
                                                                     kCZUserInteractionLogCategory : category}];

    }
}

@end
