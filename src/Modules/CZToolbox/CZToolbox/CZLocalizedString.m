//
//  CZLocalizedString.m
//  Hermes
//
//  Created by Halley Gu on 8/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZLocalizedString.h"

// Fallback for unlocalized strings, to avoid showing translation keys to the
// end users. All the unlocalized strings will be replaced with English string.
NSString *L(NSString *translationKey) {
    NSString *s = NSLocalizedString(translationKey, nil);
    if (![[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"] &&
        [s isEqualToString:translationKey]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"en"
                                                         ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:path];
        s = [languageBundle localizedStringForKey:translationKey
                                            value:@""
                                            table:nil];
    }
    return s;
}

NSString *LR(NSString *translationKey) {
    NSString *mainPath = [[NSBundle mainBundle] bundlePath];
    NSString *settingsBundlePath = [mainPath stringByAppendingPathComponent:@"Settings.bundle"];
    NSBundle *settingsBundle = [NSBundle bundleWithPath:settingsBundlePath];
    NSString *s = [settingsBundle localizedStringForKey:translationKey
                                                  value:@""
                                                  table:@"Root"];
    
    if (![[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"] &&
        [s isEqualToString:translationKey]) {
        NSString *path = [settingsBundle pathForResource:@"en"
                                                  ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:path];
        s = [languageBundle localizedStringForKey:translationKey
                                            value:@""
                                            table:@"Root"];
    }
    return s;
}
