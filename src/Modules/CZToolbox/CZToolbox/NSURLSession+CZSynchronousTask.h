//
//  NSURLSession+CZSynchronousTask.h
//  Hermes
//
//  Created by Li, Junlin on 11/13/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZURLSessionTaskResult : NSObject

@property (nonatomic, readonly, retain) NSData *data;
@property (nonatomic, readonly, retain) NSURLResponse *response;
@property (nonatomic, readonly, retain) NSError *error;

- (instancetype)initWithData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error NS_DESIGNATED_INITIALIZER;

@end

@interface NSURLSession (CZSynchronousTask)

- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithURL:(NSURL *)url;
- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithURL:(NSURL *)url timeoutInterval:(NSTimeInterval)timeoutInterval;
- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithRequest:(NSURLRequest *)request;

@end
