//
//  NSDictionary+CZInsensitiveKeys.m
//  Hermes
//
//  Created by ZEISS on 2018/9/17.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSDictionary+CZInsensitiveKeys.h"

@implementation NSDictionary (CZInsensitiveKeys)

- (id _Nullable)g_valueWithInsensitiveKeys:(NSString * _Nonnull)key {
    if (self && ![self isEqual:[NSNull null]] && [self isKindOfClass:[NSDictionary class]]) {
        id result = [self objectForKey:key];
        if (result == nil || [result isEqual:[NSNull null]]) {
            result = [self objectForKey: [key uppercaseString]];
            if (result == nil || [result isEqual:[NSNull null]]) {
                result = [[NSUserDefaults standardUserDefaults] objectForKey:[key lowercaseString]];
            }
        }
        return result;
    }
    return nil;
}

@end
