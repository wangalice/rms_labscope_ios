//
//  CZCompoundAction.h
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZUndoAction.h"

@interface CZCompoundAction : NSObject<CZUndoAction>

@property (nonatomic, assign) BOOL undoReversed;
@property (nonatomic, assign, readonly) NSUInteger count;

- (void)addAction:(id<CZUndoAction>)subAction;

@end
