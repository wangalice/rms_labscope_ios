//
//  UIDevice+CZDeviceInfo.h
//  Hermes
//
//  Created by Sun, Shaoge on 12/01/2018.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (CZDeviceInfo)

+ (NSInteger)cz_deviceModel;

+ (NSString *)cz_deviceModelString;

+ (NSString *)cz_cpuType;

+ (NSString *)cz_systemVersion;

+ (NSString *)updateApplicationTime;

@end
