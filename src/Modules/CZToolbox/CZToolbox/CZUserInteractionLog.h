//
//  CZUserInteractionLog.h
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CZUserInteractionLog : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) NSString *event;
@property (nonatomic, retain) NSDate *time;
@property (nonatomic, retain) NSString *key1;
@property (nonatomic, retain) NSString *key2;
@property (nonatomic, retain) NSString *key3;
@property (nonatomic, retain) NSString *parameter1;
@property (nonatomic, retain) NSString *parameter2;
@property (nonatomic, retain) NSString *parameter3;

// TODO:add indexID for each log

@end
