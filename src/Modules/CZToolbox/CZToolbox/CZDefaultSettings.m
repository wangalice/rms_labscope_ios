//
//  CZDefaultSettings.m
//  Hermes
//
//  Created by Mike Wang on 3/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings.h"
#import "NSString+CZMNAInsensitiveCompare.h"
#import <objc/runtime.h>

@interface CZDefaultSettings () 
@property (nonatomic, assign) NSUserDefaults *userDefaults;
@end

@implementation CZDefaultSettings

static CZDefaultSettings *uniqueInstance = nil;

#pragma mark -
#pragma mark Singleton Design Pattern

+ (CZDefaultSettings *)sharedInstance {
    @synchronized ([CZDefaultSettings class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (id)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

#pragma mark -
#pragma mark Main Methods

- (id)init {
    self = [super init];
    if (self) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (void)saveServerInfo:(NSDictionary *)info {
    for (NSString *key in info.allKeys) {
        [self.userDefaults setValue:info[key] forKey:key];
    }
}

- (BOOL)factoryResetOnNextLaunch {
    return [_userDefaults boolForKey:kFactoryResetOnNextLaunch];
}

- (void)setFactoryResetOnNextLaunch:(BOOL)factoryResetOnNextLaunch {
    [_userDefaults setBool:factoryResetOnNextLaunch forKey:kFactoryResetOnNextLaunch];
}

- (BOOL)enableMultiMicroscopes {
    return ![_userDefaults boolForKey:kEnableSingleMicroscopeMode];
}

- (void)setEnableMultiMicroscopes:(BOOL)enableMultiMicroscopes {
    [_userDefaults setBool:!enableMultiMicroscopes forKey:kEnableSingleMicroscopeMode];
}

- (CZVirtualMicroscopesOption)virtualMicroscopesOption {
    CZVirtualMicroscopesOption option = kCZAutomaticShowVirtualMicroscopes; // default
    NSString *valueString = [_userDefaults stringForKey:kShowVirtualMicroscopes];
    if ([valueString isEqualToString:@"0"]) {
        option = kCZAutomaticShowVirtualMicroscopes;
    } else if ([valueString isEqualToString:@"1"]) {
        option = kCZAlwaysShowVirtualMicroscopes;
    } else if ([valueString isEqualToString:@"2"]) {
        option = kCZNeverShowVirtualMicroscopes;
    } else {
        option = kCZAutomaticShowVirtualMicroscopes;
    }
    
    return option;
}

- (void)setVirtualMicroscopesOption:(CZVirtualMicroscopesOption)virtualMicroscopesOption {
    switch (virtualMicroscopesOption) {
        case kCZAutomaticShowVirtualMicroscopes:
            [_userDefaults setObject:@"0" forKey:kShowVirtualMicroscopes];
            break;
        case kCZAlwaysShowVirtualMicroscopes:
            [_userDefaults setObject:@"1" forKey:kShowVirtualMicroscopes];
            break;
        case kCZNeverShowVirtualMicroscopes:
            [_userDefaults setObject:@"2" forKey:kShowVirtualMicroscopes];
            break;
    }
}

- (CZTimelapseVideoPlayRate)videoPlayRate {
    CZTimelapseVideoPlayRate rate = kCZTimelapseVideoPlayRateMedium; // default
    NSString *valueString = [_userDefaults stringForKey:kTimelapseVideoPalybackRate];
    if ([valueString isEqualToString:@"0"]) {
        rate = kCZTimelapseVideoPlayRateLow;
    } else if ([valueString isEqualToString:@"1"]) {
        rate = kCZTimelapseVideoPlayRateMedium;
    } else if ([valueString isEqualToString:@"2"]) {
        rate = kCZTimelapseVideoPlayRateHigh;
    } else {
        rate = kCZTimelapseVideoPlayRateMedium;
    }
    return rate;
}

- (void)setVideoPlayRate:(CZTimelapseVideoPlayRate)videoPlayRate {
    if (self.videoPlayRate == videoPlayRate) {
        return;
    }
    switch (videoPlayRate) {
        case kCZTimelapseVideoPlayRateLow:
            [_userDefaults setValue:@"0" forKey:kTimelapseVideoPalybackRate];
            break;
        case kCZTimelapseVideoPlayRateMedium:
            default:
            [_userDefaults setValue:@"1" forKey:kTimelapseVideoPalybackRate];
            break;
        case kCZTimelapseVideoPlayRateHigh:
            [_userDefaults setValue:@"2" forKey:kTimelapseVideoPalybackRate];
            break;
    }
}

- (NSTimeInterval)timelapseIntervel {
    NSString *valueString = [_userDefaults stringForKey:kTimelapseVideoTimeInterval];
    NSTimeInterval interval = [valueString doubleValue];
    if (interval < 1.0) {
        interval = 1.0;
    }
    return interval;
}

- (void)setTimelapseIntervel:(NSTimeInterval)timelapseIntervel {
    NSString *string = [NSString stringWithFormat:@"%.1f", timelapseIntervel];
    [_userDefaults setValue:string forKey:kTimelapseVideoTimeInterval];
}

- (NSString *)serverURL {
    return [_userDefaults stringForKey:kServerURL];
}

- (void)setServerURL:(NSString *)serverURL {
   [_userDefaults setValue:serverURL forKey:kServerURL];
}

- (NSString *)serverSharePath {
    NSString *sharePath = [_userDefaults stringForKey:kServerSharePath];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"/\\"];
    return [sharePath stringByTrimmingCharactersInSet:set];
}

- (NSString *)userName {
    return [_userDefaults stringForKey:kUserName];
}

- (NSString *)password {
    return [_userDefaults stringForKey:kPassword];
}

- (NSString *)domain {
    return [_userDefaults stringForKey:kDomain];
}

- (NSString *)remoteStreamURL {
    return [_userDefaults stringForKey:kRemoteStreamURL];
}

- (BOOL)enableCopyToServerAfterSnap {
    return [_userDefaults boolForKey:kCopyToServerAfterSnap];
}

- (BOOL)enableManuallyAddMicroscopes {
    return [_userDefaults boolForKey:kManuallyAddMicroscopes];
}

- (void)setEnableManuallyAddMicroscopes:(BOOL)enableManuallyAddMicroscopes {
    [_userDefaults setBool:enableManuallyAddMicroscopes forKey:kManuallyAddMicroscopes];
}

- (BOOL)enableFixedSharedFolder {
    return [_userDefaults boolForKey:kFixedSharedFolder];
}

- (NSArray *)manuallyAddMicroscopesSettings {
    return [_userDefaults arrayForKey:kManuallyAddMicroscopeSettings];
}

- (BOOL)addManuallyMicroscopeSettingsToHistory:(NSDictionary <NSString *, NSString *> *)settings {
    if (settings == nil) {
        return NO;
    }
    NSArray *settingsHistory = [_userDefaults arrayForKey:kManuallyAddMicroscopeSettings];
    
    NSUInteger originalCount = [settingsHistory count];
    
    if (settingsHistory) {
        NSString *newIpAdress = settings[kManuallyAddMicroscopeIPAddress];
        for (NSDictionary *exsistSettings in settingsHistory) {
            NSString *ipAddress = exsistSettings[kManuallyAddMicroscopeIPAddress];
           
            if ([ipAddress isEqualToString:newIpAdress]) {
                return NO;
            }
        }
        settingsHistory = [settingsHistory arrayByAddingObject:settings];
    } else {
        settingsHistory = @[settings];
    }
    
    if (originalCount < [settingsHistory count]) {
        [_userDefaults setObject:settingsHistory forKey:kManuallyAddMicroscopeSettings];
    }
    
    return YES;
}

- (void)removeManuallyMicroscopeSettingsAtMacAddress:(NSString *)macAddress {
    if (macAddress.length <= 0) {
        return;
    }
    NSArray *settingsHistory = [_userDefaults arrayForKey:kManuallyAddMicroscopeSettings];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:settingsHistory];

    for (NSUInteger index = 0; index < settingsHistory.count; index++) {
        NSDictionary *exsistSetting = settingsHistory[index];
        NSString *exsistMacAddress = exsistSetting[kManuallyAddMicroscopeMacAddress];
        BOOL isEqualMacAddress = [macAddress isEqualComparisionWithCaseInsensitive:exsistMacAddress];
        if (isEqualMacAddress) {
            [tempArray removeObjectAtIndex:index];
            settingsHistory = [NSArray arrayWithArray:tempArray];
            [_userDefaults setObject:settingsHistory forKey:kManuallyAddMicroscopeSettings];
            break;
        }
    }
}

- (void)updateManuallyMicroscopeSettingsName:(NSString *)name
                                atMacAddress:(NSString *)macAddress {
    if (macAddress.length <= 0 || name.length <= 0) {
        return;
    }
    NSArray *settingsHistory = [_userDefaults arrayForKey:kManuallyAddMicroscopeSettings];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:settingsHistory];

    for (NSUInteger index = 0; index < settingsHistory.count; index++) {
        NSDictionary *exsistSetting = settingsHistory[index];
        NSString *exsistMacAddress = exsistSetting[kManuallyAddMicroscopeMacAddress];
        BOOL isEqualMacAddress = [macAddress isEqualComparisionWithCaseInsensitive:exsistMacAddress];
        if (isEqualMacAddress) {
            NSDictionary *setting = @{kManuallyAddMicroscopeName:name,
                         kManuallyAddMicroscopeMacAddress: exsistMacAddress,
                         kManuallyAddMicroscopeIPAddress: exsistSetting[kManuallyAddMicroscopeIPAddress]};
            [tempArray replaceObjectAtIndex:index withObject:setting];
            settingsHistory = [NSArray arrayWithArray:tempArray];
            [_userDefaults setObject:settingsHistory forKey:kManuallyAddMicroscopeSettings];
            break;
        }
    }
}

- (BOOL)containsManuallyMicroscopeSettingsAtMacAddress:(NSString *)macAddress {
    if (macAddress.length <= 0) {
        return NO;
    }
    NSArray *settingsHistory = [_userDefaults arrayForKey:kManuallyAddMicroscopeSettings];
    
    for (NSUInteger index = 0; index < settingsHistory.count; index++) {
        NSDictionary *exsistSetting = settingsHistory[index];
        NSString *exsistMacAddress = exsistSetting[kManuallyAddMicroscopeMacAddress];
        BOOL isEqualMacAddress = [macAddress isEqualComparisionWithCaseInsensitive:exsistMacAddress];
        if (isEqualMacAddress) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)enableLog {
    return [_userDefaults boolForKey:kEnableLog];
}

- (BOOL)disableMacAddressFilter {
    return [_userDefaults boolForKey:kDisableMacAddressFilter];
}

- (BOOL)enableRTFReportGeneration {
    return YES;
}

- (BOOL)stayInLiveTabAfterSnappingEnabled {
    return [_userDefaults boolForKey:kStayInLiveTabAfterSnapping];
}

- (BOOL)enableMacroSnap {
    return [_userDefaults boolForKey:kEnableMacroSnap];
}

- (void)setEnableMacroSnap:(BOOL)enableMacroSnap {
    [_userDefaults setBool:enableMacroSnap forKey:kEnableMacroSnap];
}

- (BOOL)enableMultichannelSnap {
    return [_userDefaults boolForKey:kEnableMultichannelSnap];
}

- (void)setStayInLiveTabAfterSnappingEnabled:(BOOL)stayInLiveTabAfterSnappingEnabled {
    [_userDefaults setBool:stayInLiveTabAfterSnappingEnabled forKey:kStayInLiveTabAfterSnapping];
}

- (CZInformationScreenOption)infoScreenOption {
    CZInformationScreenOption option = CZInformationScreenOptionUserDefined;
    NSString *valueString = [_userDefaults stringForKey:kInfoScreen];
    if ([valueString isEqualToString:@"0"]) {
        option = CZInformationScreenOptionUserDefined;
    } else if ([valueString isEqualToString:@"1"]) {
        option = CZInformationScreenOptionEnableAll;
    } else if ([valueString isEqualToString:@"2"]) {
        option = CZInformationScreenOptionDisableAll;
    }
    return option;
}

- (void)setInfoScreenOption:(CZInformationScreenOption)infoScreenOption {
    switch (infoScreenOption) {
        case CZInformationScreenOptionUserDefined:
            [_userDefaults setObject:@"0" forKey:kInfoScreen];
            break;
        case CZInformationScreenOptionEnableAll:
            [_userDefaults setObject:@"1" forKey:kInfoScreen];
            break;
        case CZInformationScreenOptionDisableAll:
            [_userDefaults setObject:@"2" forKey:kInfoScreen];
            break;
    }
}

- (BOOL)shareToMailEnabled {
    return [_userDefaults boolForKey:kShareMail];
}

- (BOOL)shareToScailNetworkEnabled {
    return [_userDefaults boolForKey:kShareSocial];
}

- (BOOL)showLaserPointer {
    return [_userDefaults boolForKey:kShowLaserPointer];
}

- (void)setShowLaserPointer:(BOOL)showLaserPointer {
    [_userDefaults setBool:showLaserPointer forKey:kShowLaserPointer];
}

- (CZShortcutMode)shortcutMode {
    CZShortcutMode mode = kCZShortcutModeNone;
    NSString *valueString = [_userDefaults stringForKey:kCZShortcutMode];
    if ([valueString isEqualToString:@"0"]) {
        mode = kCZShortcutModeNone;
    } else if ([valueString isEqualToString:@"1"]) {
        mode = kCZShortcutModePCBMeasurement;
    }
    return mode;
}

- (CZFilesDisplayViewMode)filesDisplayViewMode {
    CZFilesDisplayViewMode mode = kCZFilesListViewMode;
    NSString *valueString = [_userDefaults stringForKey:kCZFilesDisplayViewMode];
    if ([valueString isEqualToString:@"0"]) {
        mode = kCZFilesListViewMode;
    } else if ([valueString isEqualToString:@"1"]) {
        mode = kCZFilesGridViewMode;
    }
    return mode;
}

- (void)setFilesDisplayViewMode:(CZFilesDisplayViewMode)filesDisplayViewMode {
    if (self.filesDisplayViewMode == filesDisplayViewMode) {
        return;
    }
    switch (filesDisplayViewMode) {
        case kCZFilesListViewMode:
        default:
            [_userDefaults setValue:@"0" forKey:kCZFilesDisplayViewMode];
            break;
        case kCZFilesGridViewMode:
            [_userDefaults setValue:@"1" forKey:kCZFilesDisplayViewMode];
            break;
        }
}

- (NSString *)digitalClassroomHost {
    return [_userDefaults stringForKey:kDigitalClassroomHost];
}

- (NSString *)defaultCameraMACAddress {
    return [_userDefaults stringForKey:kDefaultCameraMACAddress];
}

- (void)setDefaultCameraMACAddress:(NSString *)defaultMacAddress {
    [_userDefaults setValue:defaultMacAddress forKey:kDefaultCameraMACAddress];
}

- (void)restoreAllSettingsWhenNeed {
    NSDictionary *reset = [self allSettingsNeedReset];
    for (NSString * key in reset) {
        [_userDefaults setValue:reset[key] forKey:key];
    }
 }

- (NSDictionary *)allSettingsNeedReset {
    return @{
//             kDefaultCameraMACAddress     : @"",
//             kEnableSingleMicroscopeMode  : @(NO)
             };
}

@end
