//
//  CZUndoManager.h
//  Hermes
//
//  Created by Ralph Jin on 1/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZUndoAction.h"

@protocol CZUndoManaging <NSObject>

/*! push action into the undo manager for execution */
- (void)pushAction:(id<CZUndoAction>)anAction;

- (void)undo;

- (void)redo;

- (BOOL)canUndo;

- (BOOL)canRedo;

@end

#pragma mark -

@class CZUndoManager;

@protocol CZUndoManagerDelegate <NSObject>
@optional
- (void)didUndoAction:(id<CZUndoAction>)action;
- (void)didRedoAction:(id<CZUndoAction>)action;

- (void)undoManagerDidPushAction:(CZUndoManager *)undoManager;

@end

@interface CZUndoManager : NSObject<CZUndoManaging>

/*! the limit memory size of total actions in undo stack. Default is 0, means no limitation.*/
@property(nonatomic) NSUInteger maxMemorySize;

@property (nonatomic, assign) id<CZUndoManagerDelegate> delegate;

- (void)removeAllActions;

@end
