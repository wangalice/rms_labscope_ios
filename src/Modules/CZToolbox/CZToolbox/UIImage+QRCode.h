//
//  UIImage+QRCode.h
//  Matscope
//
//  Created by Ralph Jin on 9/16/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(QRCode)

+ (UIImage *)qrCodeImageWithMessage:(NSString *)message;
+ (UIImage *)qrCodeImageWithMessage:(NSString *)message icon:(UIImage *)icon;

@end
