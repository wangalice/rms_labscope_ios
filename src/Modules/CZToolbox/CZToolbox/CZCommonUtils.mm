//
//  CZCommonUtils.m
//  Hermes
//
//  Created by Mike Wang on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCommonUtils.h"
#import "CZAssetsName.h"
#import "CZLog.h"
#import "NSFileManager+CommonPath.h"

#import <Accelerate/Accelerate.h>
#import <CommonCrypto/CommonDigest.h>
#import <CoreGraphics/CoreGraphics.h>
#import <ImageIO/ImageIO.h>

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <sys/utsname.h>

#import "CZDefaultSettings.h"

#include <netdb.h>
#include <arpa/inet.h>
#include <err.h>

float otsuThreshold(int hist[], long total) {
    const int N = 256;
    double mu = 0, scale = 1.0 / total;
    int i;
    for (i = 0; i < N; i++) {
        mu += i * (double)hist[i];
    }
    
    mu *= scale;
    double mu1 = 0, q1 = 0;
    double max_sigma = 0, max_val = 0;
    
    for (i = 0; i < N; i++) {
        double p_i, q2, mu2, sigma;
        
        p_i = hist[i] * scale;
        mu1 *= q1;
        q1 += p_i;
        q2 = 1.0 - q1;
        
        if (std::min(q1, q2) < FLT_EPSILON || std::max(q1, q2) > 1.0 - FLT_EPSILON) {
            continue;
        }
        
        mu1 = (mu1 + i * p_i) / q1;
        mu2 = (mu - q1 * mu1) / q2;
        sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
        if (sigma > max_sigma) {
            max_sigma = sigma;
            max_val = i;
        }
    }
    
    return max_val;
}

@implementation CZCommonUtils

+ (NSNumberFormatter *)sharedNumberFormatter {
    static NSNumberFormatter *numberFormatter = nil;
    if (numberFormatter == nil) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    }
    
    return numberFormatter;
}

+ (CGFloat) distanceBetweenPoints:(CGPoint)a Point:(CGPoint)b {
    CGFloat deltaX = a.x - b.x;
    CGFloat deltaY = a.y - b.y;
    return sqrt((deltaX * deltaX) + (deltaY * deltaY));
}

+ (CGPoint)viewPointToImagePoint:(CGPoint)point {
    CGFloat scale = [[UIScreen mainScreen] scale];
    point.x = point.x * scale;
    point.y = point.y * scale;
    return point;
}

+ (CGPoint)imagePointToViewPoint:(CGPoint)point {
    CGFloat scale = [[UIScreen mainScreen] scale];
    scale = 1.0f / scale;
    point.x = point.x * scale;
    point.y = point.y * scale;
    return point;
}

+ (CGRect)viewRectToImageRect:(CGRect)rect {
    CGFloat scale = [[UIScreen mainScreen] scale];
    rect.origin.x *= scale;
    rect.origin.y *= scale;
    rect.size.width *= scale;
    rect.size.height *= scale;
    return rect;
}

+ (CGRect)imageRectToViewRect:(CGRect)rect {
    CGFloat scale = [[UIScreen mainScreen] scale];
    scale = 1.0f / scale;
    rect.origin.x *= scale;
    rect.origin.y *= scale;
    rect.size.width *= scale;
    rect.size.height *= scale;
    return rect;
}

+ (CGSize)viewSizeToImageSize:(CGSize)size {
    CGFloat scale = [[UIScreen mainScreen] scale];
    size.width *= scale;
    size.height *= scale;
    return size;
}

+ (CGSize)imageSizeToViewSize:(CGSize)size {
    CGFloat scale = [[UIScreen mainScreen] scale];
    scale = 1.0f / scale;
    size.width *= scale;
    size.height *= scale;
    return size;
}

+ (CGRect)calcActualBoxByBoundBox:(CGRect)boundBox
                      contentSize:(CGSize)contentSize {
    CGFloat originalAspectRatio = contentSize.width / contentSize.height;
    CGFloat maxAspectRatio = boundBox.size.width / boundBox.size.height;
    
    CGRect actualBox = boundBox;
    if (originalAspectRatio > maxAspectRatio) {
        actualBox.size.height = boundBox.size.width * contentSize.height / contentSize.width;
        actualBox.origin.y += (boundBox.size.height - actualBox.size.height) / 2.0;
    } else {
        actualBox.size.width = boundBox.size.height * contentSize.width / contentSize.height;
        actualBox.origin.x += (boundBox.size.width - actualBox.size.width) / 2.0;
    }
    actualBox = CGRectIntegral(actualBox);
    
    return actualBox;
}

+ (UIImage *)blankImageWithSize:(CGSize)size {
    static UIImage *blackImage = nil;
    
    if (blackImage) {
        CGSize oldSize = [blackImage size];
        if (!CGSizeEqualToSize(size, oldSize)) {
            [blackImage release];
            blackImage = nil;
        }
    }
    
    if (blackImage == nil && !(size.width != size.width || size.height != size.height) ) {
        UIGraphicsBeginImageContext(size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextClearRect(context, CGRectMake(0.0, 0.0, size.width, size.height));
        blackImage = [UIGraphicsGetImageFromCurrentImageContext() retain];
        UIGraphicsEndImageContext();
    }
    
    return blackImage;
}

+ (UIImage *)cropImageToAspectRatio:(CGFloat)ratio
                         imageSource:(UIImage *)source {
    if (!source) {
        return nil;
    }
    
    const CGFloat epsilon = 0.0001;
    
    CGSize size = [source size];
    size.width = floor(size.width);
    size.height = floor(size.height);
    
    if (size.width <= 0 || size.height <= 0) {
        return nil;
    }
    
    // If it's already taget ratio, do nothing.
    CGFloat currentRatio = size.width / size.height;
    if (fabs(currentRatio - ratio) < epsilon) {
        return source;
    }
    
    CGImageRef sourceRef = [source CGImage];
    CGImageRetain(sourceRef);
    
    // Calculate the region needs to be extracted.
    CGRect rect;
    if (currentRatio > ratio) {
        rect.size.width = round(size.height * ratio);
        rect.size.height = size.height;
        rect.origin.x = round((size.width - rect.size.width) / 2.0);
        rect.origin.y = 0.0;
    } else {
        rect.size.width = size.width;
        rect.size.height = round(size.width / ratio);
        rect.origin.x = 0.0;
        rect.origin.y = round((size.height - rect.size.height) / 2.0);
    }
    
    // Crop the image using Core Graphics.
    CGImageRef resultRef = CGImageCreateWithImageInRect(sourceRef, rect);
    UIImage *result = [[[UIImage alloc] initWithCGImage:resultRef scale:source.scale orientation:source.imageOrientation] autorelease];
    
    CGImageRelease(resultRef);
    CGImageRelease(sourceRef);
    
    return result;
}

+ (UIImage *)scaleImage:(UIImage *)source intoSize:(CGSize)maxSize {
    return [self scaleImage:source intoSize:maxSize fillColor:nil];
}

+ (UIImage *)scaleImage:(UIImage *)source intoSize:(CGSize)maxSize fillColor:(UIColor *)fillColor {
    // Return original image if the targte size if larger than source.
    if (source.size.width < maxSize.width && source.size.height < maxSize.height) {
        return source;
    }
    
    if (!source || source.size.width == 0 || source.size.height == 0 ||
        maxSize.width == 0 || maxSize.height == 0) {
        return source;
    }
    
    CGFloat widthRatio = maxSize.width / source.size.width;
    CGFloat heightRatio = maxSize.height / source.size.height;
    
    if (widthRatio < heightRatio) {
        maxSize.height = round(maxSize.width * source.size.height / source.size.width);
        maxSize.width = round(maxSize.width);
    } else {
        maxSize.width = round(maxSize.height * source.size.width / source.size.height);
        maxSize.height = round(maxSize.height);
    }
    
    UIGraphicsBeginImageContextWithOptions(maxSize, YES, 1.0);
    if (fillColor) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [fillColor CGColor]);
        CGContextFillRect(context, CGRectMake(0, 0, maxSize.width, maxSize.height));
    }
    [source drawInRect:CGRectMake(0, 0, maxSize.width, maxSize.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

+ (UIImage *)grayImageFromImage:(UIImage *)source {
    if (CGColorSpaceGetModel(CGImageGetColorSpace(source.CGImage)) == kCGColorSpaceModelMonochrome && CGImageGetBitsPerComponent(source.CGImage) == 8) {
        return source;
    }
    
    CGImageRef imageRef = [source CGImage];
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGBitmapInfo bitmapInfo = kCGImageAlphaNone;
    CGContextRef context = CGBitmapContextCreate(NULL, width, height, 8, 0,
                                                 colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *image = [UIImage imageWithCGImage:newImageRef scale:source.scale orientation:source.imageOrientation];
    CGImageRelease(newImageRef);
    
    CGContextRelease(context);
    return image;
}

+ (UIImage *)gray16ImageFromImage:(UIImage *)source {
    if (CGColorSpaceGetModel(CGImageGetColorSpace(source.CGImage)) == kCGColorSpaceModelMonochrome && CGImageGetBitsPerComponent(source.CGImage) == 16) {
        return source;
    }
    
    CGImageRef imageRef = [source CGImage];
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGBitmapInfo bitmapInfo = kCGImageAlphaNone;
    CGContextRef context = CGBitmapContextCreate(NULL, width, height, 16, 0,
                                                 colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *image = [UIImage imageWithCGImage:newImageRef scale:source.scale orientation:source.imageOrientation];
    CGImageRelease(newImageRef);
    
    CGContextRelease(context);
    return image;
}

+ (UIImage *)imageFromGrayImage:(UIImage *)grayImage {
    UIImage *outImage = nil;
    @autoreleasepool {
        UIGraphicsBeginImageContext(grayImage.size);
        [grayImage drawAtPoint:CGPointZero];
        outImage = [UIGraphicsGetImageFromCurrentImageContext() retain];
        UIGraphicsEndImageContext();
    }
    
    return outImage;
}

+ (UIImage *)newDecompressedImageFromJPEGImage:(UIImage *)originalImage {
    // Rendering the JPEG image first and then getting it from
    // the context as a new UIImage helps us to avoid decompressing while using
    // the image as the content of magnifying view. This fix the performance
    // issue of magnifying view when viewing a large JPEG file.
    //
    // Fix bug that bytePerRow should not always be 64 times, for GPUImage processing.
    UIImage *outImage = nil;
    @autoreleasepool {
        CGSize imageSize = [originalImage size];
        size_t bytesPerRow = imageSize.width * 4;
        
        CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
        CGContext *context = CGBitmapContextCreate(NULL, imageSize.width, imageSize.height, 8, bytesPerRow, space, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
        CGColorSpaceRelease(space);
        
        // Special handling for image orientation. This is initially implemented for built-in camera images.
        
        CGAffineTransform transform = CGAffineTransformIdentity;
        
        switch (originalImage.imageOrientation) { // Rotation
            case UIImageOrientationDown:
            case UIImageOrientationDownMirrored:
                transform = CGAffineTransformTranslate(transform, originalImage.size.width, originalImage.size.height);
                transform = CGAffineTransformRotate(transform, M_PI);
                break;
                
            case UIImageOrientationLeft:
            case UIImageOrientationLeftMirrored:
                transform = CGAffineTransformTranslate(transform, originalImage.size.width, 0);
                transform = CGAffineTransformRotate(transform, M_PI_2);
                imageSize = CGSizeMake(imageSize.height, imageSize.width);
                break;
                
            case UIImageOrientationRight:
            case UIImageOrientationRightMirrored:
                transform = CGAffineTransformTranslate(transform, 0, originalImage.size.height);
                transform = CGAffineTransformRotate(transform, -M_PI_2);
                imageSize = CGSizeMake(imageSize.height, imageSize.width);
                break;
                
            default:
                break;
        }
        
        switch (originalImage.imageOrientation) { // Mirroring
            case UIImageOrientationUpMirrored:
            case UIImageOrientationDownMirrored:
                transform = CGAffineTransformTranslate(transform, originalImage.size.width, 0);
                transform = CGAffineTransformScale(transform, -1, 1);
                break;
                
            case UIImageOrientationLeftMirrored:
            case UIImageOrientationRightMirrored:
                transform = CGAffineTransformTranslate(transform, originalImage.size.height, 0);
                transform = CGAffineTransformScale(transform, -1, 1);
                break;
            
            default:
                break;
        }
        
        CGContextConcatCTM(context, transform);
        CGContextDrawImage(context, CGRectMake(0, 0, imageSize.width, imageSize.height), originalImage.CGImage);
        
        CGImageRef newImageRef = CGBitmapContextCreateImage(context);
        outImage = [[UIImage imageWithCGImage:newImageRef] retain];
        CGImageRelease(newImageRef);
        CGContextRelease(context);
    }
    
    return outImage;
}

+ (UIImage *)imageFromYUV400Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height{
    if (buffer == NULL || width == 0 || height == 0) {
        return nil;
    }
    const size_t length = width * height;
    if (bufferLength < length){
        return nil;
    }
    
    Pixel_8 *pixels = (Pixel_8 *)malloc(sizeof(Pixel_8) * length);
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            size_t index = i * width + j;
            uint8_t yData = ((const uint8_t *)buffer)[index];
            pixels[index] = yData;
        }
    }
    
    return [CZCommonUtils imageFromGray8Buffer:(const UInt8 *)pixels width:width height:height];
}

+ (UIImage *)imageFromYUV420spNV12Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height {
    if (buffer == NULL || width == 0 || height == 0) {
        return nil;
    }
    
    const size_t length = width * height;
    const size_t uvLength = length / 2;
    if (bufferLength < (length + uvLength)) {
        return nil;
    }
    
    
    Pixel_8888 *pixels = (Pixel_8888 *)malloc(sizeof(Pixel_8888) * length);
    
    const uint8_t *py = (const uint8_t *)buffer;
    const uint8_t *pu = py + length;
    const uint8_t *pv = pu + 1;
    
    /**
     * The YUV420 implementation in TI solution is a bit special. Generally
     * YUV420 is packed as YYYYYYYYUUVV, but in TI solution it's YYYYYYYYUVUV.
     *
     * Example (dimensions = 6 * 4)
     *
     * Y00 Y01 Y02 Y03 Y04 Y05
     * Y06 Y07 Y08 Y09 Y10 Y11
     * Y12 Y13 Y14 Y15 Y16 Y17
     * Y18 Y19 Y20 Y21 Y22 Y23
     * U00 V00 U01 V01 U02 V02
     * U03 V03 U04 V04 U05 V05
     *
     * Y00, Y01, Y06, Y07 share U00 and V00
     * Y02, Y03, Y08, Y09 share U01 and V01
     * Y12, Y13, Y18, Y19 share U03 and V03
     * ...
     */
    
    size_t i = 0;
    for (size_t y = 0; y < height; ++y) {
        size_t j = (y / 2) * (width / 2);
        for (size_t x = 0; x < width; ++x, ++i) {
            size_t k = j + (x / 2);
            pixels[i][0] = 0;           // Alpha, ignored here
            pixels[i][1] = py[i];       // Y
            pixels[i][2] = pu[k+k];     // U
            pixels[i][3] = pv[k+k];     // V
        }
    }
    
    /*
     * YUV to RGB formula from http://en.wikipedia.org/wiki/YUV
     *
     * C = Y' - 16
     * D = U - 128
     * E = V - 128
     *
     * R = clamp((298 * C + 409 * E + 128) >> 8)
     * G = clamp((298 * C - 100 * D - 208 * E + 128) >> 8)
     * B = clamp((298 * C + 516 * D + 128) >> 8)
     */
    
    const int32_t divisor = 256; // >> 8
    const int16_t matrix[16] = {
        0, 0, 0, 0,
        0, 298, 298, 298,
        0, 0, -100, 516,
        0, 409, -208, 0
    };
    const int16_t preBias[4] = {
        0, -16, -128, -128
    };
    const int32_t postBias[4] = {
        0, 128, 128, 128
    };
    
    vImage_Buffer pixelsBuffer = {
        pixels,
        (vImagePixelCount)height,
        (vImagePixelCount)width,
        sizeof(Pixel_8888) * width
    };
    
    vImage_Error error = vImageMatrixMultiply_ARGB8888(&pixelsBuffer,
                                                       &pixelsBuffer,
                                                       matrix,
                                                       divisor,
                                                       preBias,
                                                       postBias,
                                                       0);
    
    UIImage *image = nil;
    
    if (error == kvImageNoError) {
        const size_t bitsPerComponent = 8;
        const size_t bitsPerPixel = 32;
        const size_t bytesPerRow = width * 4;
        
        CFDataRef dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc,
                                                        (const UInt8 *)pixels,
                                                        length * sizeof(Pixel_8888),
                                                        kCFAllocatorMalloc);
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);
        CGBitmapInfo bitmapInfo = kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Big;
        CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent,
                                            bitsPerPixel, bytesPerRow, colorSpace,
                                            bitmapInfo, dataProvider,
                                            NULL, true, kCGRenderingIntentDefault);
        
        image = [[[UIImage alloc] initWithCGImage:imageRef] autorelease];
        
        CGImageRelease(imageRef);
        CGDataProviderRelease(dataProvider);
        CGColorSpaceRelease(colorSpace);
        CFRelease(dataRef);
    } else {
        CZLogv(@"Failed to do the matrix multiply.");
        free(pixels);
    }
    
    // The buffer "pixels" will be freed by CFData when the image is released
    
    return image;
}

+ (UIImage *)imageFromYUV420spNV21Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height {
    if (buffer == NULL || width == 0 || height == 0) {
        return nil;
    }
    
    const size_t length = width * height;
    const size_t uvLength = length / 2;
    if (bufferLength < (length + uvLength)) {
        return nil;
    }
    
    
    Pixel_8888 *pixels = (Pixel_8888 *)malloc(sizeof(Pixel_8888) * length);
    
    const uint8_t *py = (const uint8_t *)buffer;
    const uint8_t *pu = py + length;
    const uint8_t *pv = pu + 1;
    
    /**
     * The YUV420 implementation in TI solution is a bit special. Generally
     * YUV420 is packed as YYYYYYYYUUVV, but in our solution it's YYYYYYYYVUVU.
     * Example (dimensions = 6 * 4)
     *
     * Y00 Y01 Y02 Y03 Y04 Y05
     * Y06 Y07 Y08 Y09 Y10 Y11
     * Y12 Y13 Y14 Y15 Y16 Y17
     * Y18 Y19 Y20 Y21 Y22 Y23
     * V00 U00 V01 U01 V02 U02
     * V03 U03 V04 U04 V05 U05
     *
     * Y00, Y01, Y06, Y07 share U00 and V00
     * Y02, Y03, Y08, Y09 share U01 and V01
     * Y12, Y13, Y18, Y19 share U03 and V03
     * ...
     */
    
    size_t i = 0;
    for (size_t y = 0; y < height; ++y) {
        size_t j = (y / 2) * (width / 2);
        for (size_t x = 0; x < width; ++x, ++i) {
            size_t k = j + (x / 2);
            pixels[i][0] = 0;           // Alpha, ignored here
            pixels[i][1] = py[i];       // Y
            pixels[i][2] = pv[k+k];     // V
            pixels[i][3] = pu[k+k];     // U
        }
    }
    
    /*
     * YUV to RGB formula form https://en.wikipedia.org/wiki/Ycbcr
     *
     * C = Y'
     * D = U - 128
     * E = V - 128
     *
     * R = clamp(((C << 8) + 359 * E + 128) >> 8)
     * G = clamp(((C << 8) - 88 * D - 183 * E + 128) >> 8)
     * B = clamp(((C << 8) + 454 * D + 128) >> 8)
     */
    
    const int32_t divisor = 256; // >> 8
    const int16_t matrix[16] = {
        0, 0, 0, 0,
        0, 256, 256, 256,
        0, 0, -88, 454,
        0, 359, -183, 0
    };
    
    const int16_t preBias[4] = {
        0, 0, -128, -128
    };
    
    const int32_t postBias[4] = {
        0, 128, 128, 128
    };
    
    vImage_Buffer pixelsBuffer = {
        pixels,
        (vImagePixelCount)height,
        (vImagePixelCount)width,
        sizeof(Pixel_8888) * width
    };
    
    vImage_Error error = vImageMatrixMultiply_ARGB8888(&pixelsBuffer,
                                                       &pixelsBuffer,
                                                       matrix,
                                                       divisor,
                                                       preBias,
                                                       postBias,
                                                       0);
    
    UIImage *image = nil;
    
    if (error == kvImageNoError) {
        image = [CZCommonUtils imageFromRGBBuffer:(const UInt8 *)pixels width:width height:height];
    } else {
        CZLogv(@"Failed to do the matrix multiply.");
        free(pixels);
    }
    
    // The buffer "pixels" will be freed by CFData when the image is released
    
    return image;
}

+ (UIImage *)imageFromGrayBayerBuffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height {
    if (buffer == NULL || width == 0 || height == 0) {
        return nil;
    }
    const size_t length = width * height;
    if (bufferLength < length * 2){
        return nil;
    }
    
    Pixel_88 *pixels = (Pixel_88 *)malloc(sizeof(Pixel_88) * length);
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            size_t index = i * width + j;
            pixels[index][0] = ((const uint8_t *)buffer)[index * 2];
            pixels[index][1] = ((const uint8_t *)buffer)[index * 2 + 1];
        }
    }
    
    return [CZCommonUtils imageFromGray16Buffer:(const UInt8 *)pixels width:width height:height];
}

+ (UIImage *)imageFromLayer:(CALayer *)layer {
    UIGraphicsBeginImageContext(layer.frame.size);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (CGSize)imageSizeFromFileURL:(NSURL *)fileURL {
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)fileURL, NULL);
    if (imageSource == NULL) {
        return CGSizeZero;
    }
    
    CGFloat width = 0.0f, height = 0.0f;
    CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
    if (imageProperties != NULL) {
        CFNumberRef widthNum = (CFNumberRef)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
        if (widthNum != NULL) {
            CFNumberGetValue(widthNum, kCFNumberCGFloatType, &width);
        }
        
        CFNumberRef heightNum = (CFNumberRef)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
        if (heightNum != NULL) {
            CFNumberGetValue(heightNum, kCFNumberCGFloatType, &height);
        }
        
        CFRelease(imageProperties);
    }
    
    CFRelease(imageSource);
    
    return CGSizeMake(width, height);
}

+ (CGSize)imageSizeFromFilePath:(NSString *)filePath {
    NSURL *imageFileURL = [NSURL fileURLWithPath:filePath];
    return [CZCommonUtils imageSizeFromFileURL:imageFileURL];
}

+ (UIImage *)thumnailFromImageFileURL:(NSURL *)fileURL atMinSize:(CGFloat)minSize {
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)fileURL, NULL);
    if (imageSource) {
        CGFloat width = 0.0f, height = 0.0f;
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
        if (imageProperties != NULL) {
            CFNumberRef widthNum = (CFNumberRef)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
            if (widthNum != NULL) {
                CFNumberGetValue(widthNum, kCFNumberCGFloatType, &width);
            }
            
            CFNumberRef heightNum = (CFNumberRef)CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
            if (heightNum != NULL) {
                CFNumberGetValue(heightNum, kCFNumberCGFloatType, &height);
            }
            
            CFRelease(imageProperties);
        }
        
        CGFloat maxSize;
        if (width < height) {
            maxSize = floor(minSize * height / width + 0.5f);
        } else {
            maxSize = floor(minSize * width / height + 0.5f);
        }
        
        NSDictionary *options = @{(id)kCGImageSourceCreateThumbnailWithTransform: @YES,
                                  (id)kCGImageSourceCreateThumbnailFromImageIfAbsent: @YES,
                                  (id)kCGImageSourceThumbnailMaxPixelSize: @(maxSize)};
        
        CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (CFDictionaryRef)options);
        CFRelease(imageSource);
        
        UIImage *image = [UIImage imageWithCGImage:thumbnail];
        CGImageRelease(thumbnail);
        return image;
    } else {
        return nil;
    }
}

+ (UIImage *)thumnailFromImageFilePath:(NSString *)filePath atMinSize:(CGFloat)minSize {
    NSURL *imageFileURL = [NSURL fileURLWithPath:filePath];
    return [CZCommonUtils thumnailFromImageFileURL:imageFileURL atMinSize:minSize];
}

+ (BOOL)numericTextField:(UITextField *)textField shouldAcceptChange:(NSString *)textEntered {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *decimalSeparator = [locale objectForKey:NSLocaleDecimalSeparator];
    
    NSString *originalText = [textField text];
    NSRange separatorRange = [originalText rangeOfString:decimalSeparator];
    
    NSMutableString *validCharacters = [[NSMutableString alloc] initWithString:@"0123456789"];
    
    if (separatorRange.location == NSNotFound) {
        [validCharacters appendString:decimalSeparator];
    }
    
    NSCharacterSet *numericSet = [NSCharacterSet characterSetWithCharactersInString:validCharacters];
    [validCharacters release];
    
    for (int i = 0; i < [textEntered length]; i++) {
        unichar c = [textEntered characterAtIndex:i];
        if (![numericSet characterIsMember:c]) {
            return NO;
        }
    }
    return YES;
}

+ (BOOL)numericPINField:(UITextField *)textField shouldAcceptChange:(NSString *)textEntered {
    NSCharacterSet *numericSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    for (int i = 0; i < [textEntered length]; i++) {
        unichar c = [textEntered characterAtIndex:i];
        if (![numericSet characterIsMember:c]) {
            return NO;
        }
    }
    return YES;
}

+ (float)floatFromFloat:(float)number precision:(NSUInteger)precision {
   return roundf(number * powf(10.0f, precision)) * powf(0.1f, precision);
}

+ (double)doubleFromDouble:(double)number precision:(NSUInteger)precision {
    return round(number * pow(10.0, precision)) * pow(0.1, precision);
}

+ (NSNumber *)numberFromLocalizedString:(NSString *)string {
    NSNumberFormatter *formatter = [CZCommonUtils sharedNumberFormatter];
    if (formatter.maximumFractionDigits != NSUIntegerMax) {
        [formatter setMaximumFractionDigits:NSUIntegerMax];
    }
    [formatter setDecimalSeparator:nil];
    if (!formatter.usesGroupingSeparator) {
        [formatter setUsesGroupingSeparator:YES];
    }
    NSNumber *number = [formatter numberFromString:string];
    return number;
}

+ (NSString *)stringFromNumber:(NSNumber *)number precision:(NSUInteger)precision {
    NSNumberFormatter *formatter = [CZCommonUtils sharedNumberFormatter];
    if (formatter.maximumFractionDigits != precision) {
        [formatter setMaximumFractionDigits:precision];
    }
    if (![formatter.decimalSeparator isEqualToString:@"."]) {
        [formatter setDecimalSeparator:@"."];
    }
    if (formatter.usesGroupingSeparator) {
        [formatter setUsesGroupingSeparator:NO];
    }
    return [formatter stringFromNumber:number];
}

+ (NSString *)stringFromFloat:(CGFloat)number precision:(NSUInteger)precision {
    return [CZCommonUtils stringFromNumber:@(number) precision:precision];
}

+ (NSString *)localizedStringFromNumber:(NSNumber *)number precision:(NSUInteger)precision {
    NSNumberFormatter *formatter = [CZCommonUtils sharedNumberFormatter];
    if (formatter.maximumFractionDigits != precision) {
        [formatter setMaximumFractionDigits:precision];
    }
    [formatter setDecimalSeparator:nil];
    if (!formatter.usesGroupingSeparator) {
        [formatter setUsesGroupingSeparator:YES];
    }
    return [formatter stringFromNumber:number];
}

+ (NSString *)localizedStringFromFloat:(double)number precision:(NSUInteger)precision {
    return [CZCommonUtils localizedStringFromNumber:@(number) precision:precision];
}

+ (CZFileFormat)fileFormatForExtension:(NSString *)extension {
    NSString *lowerExtension = [extension lowercaseString];
    CZFileFormat format = kCZFileFormatNotSupported;
    
    if ([lowerExtension isEqualToString:@"czi"]) {
        format = kCZFileFormatCZI;
    } else if ([lowerExtension isEqualToString:@"jpg"] ||
               [lowerExtension isEqualToString:@"jpeg"]) {
        format = kCZFileFormatJPEG;
    } else if ([lowerExtension isEqualToString:@"png"]) {
        format = kCZFileFormatPNG;
    } else if ([lowerExtension isEqualToString:@"pdf"]) {
        format = kCZFileFormatPDF;
    } else if ([lowerExtension isEqualToString:@"rtf"]) {
        format = kCZFileFormatRTF;
    } else if ([lowerExtension isEqualToString:@"csv"]) {
        format = kCZFileFormatCSV;
    } else if ([lowerExtension isEqualToString:@"mp4"]) {
        format = kCZFileFormatMP4;
    } else if ([lowerExtension isEqualToString:@"czrtj"]) {
        format = kCZFileFormatCZRTJ;
    } else if ([lowerExtension isEqualToString:@"czftp"]) {
        format = kCZFileFormatCZFTP;
    } else if ([lowerExtension isEqualToString:@"tif"] ||
               [lowerExtension isEqualToString:@"tiff"]) {
        format = kCZFileFormatTIF;
    } else if ([lowerExtension isEqualToString:@"czamt"]) {
        format = kCZFileFormatCZAMT;
    }
    
    return format;
}

+ (CZFileFormatType)fileFormatTypeForExtension:(NSString *)extension {
    NSString *lowerExtension = [extension lowercaseString];
    CZFileFormatType type = kCZFileFormatTypeNotSupported;
    
    if ([lowerExtension isEqualToString:@"czi"] ||
        [lowerExtension isEqualToString:@"jpg"] ||
        [lowerExtension isEqualToString:@"jpeg"] ||
        [lowerExtension isEqualToString:@"tif"] ||
        [lowerExtension isEqualToString:@"tiff"]) {
        type = kCZFileFormatTypeImage;
    } else if ([lowerExtension isEqualToString:@"pdf"] ||
               [lowerExtension isEqualToString:@"rtf"] ||
               [lowerExtension isEqualToString:@"csv"]) {
        type = kCZFileFormatTypeReport;
    } else if ([lowerExtension isEqualToString:@"mp4"]) {
        type = kCZFileFormatTypeVideo;
    } else if ([lowerExtension isEqualToString:@"czrtj"]) {
        type = kCZFileFormatTypeReportTemplate;
    } else if ([lowerExtension isEqualToString:@"czftp"]) {
        type = kCZFileFormatTypeFileNameTemplate;
    } else if ([lowerExtension isEqualToString:@"czamt"]) {
        type = kCZFileFormatTypeAdvanceMeasurementsTemplate;
    }
    
    return type;
}

+ (NSString *)mimeTypeStringForExtension:(NSString *)extension {
    NSString *lowerExtension = [extension lowercaseString];
    
    // Default MIME type for CZI and all unknown formats.
    NSString *mimeType = @"application/octet-stream";
    
    // Referece: http://www.iana.org/assignments/media-types
    
    if ([lowerExtension isEqualToString:@"jpg"] ||
        [lowerExtension isEqualToString:@"jpeg"]) {
        mimeType = @"image/jpeg";
    } else if ([lowerExtension isEqualToString:@"tif"]) {
        mimeType = @"image/tiff";
    } else if ([lowerExtension isEqualToString:@"png"]) {
        mimeType = @"image/png";
    } else if ([lowerExtension isEqualToString:@"pdf"]) {
        // Use "octet-stream" instead of "pdf" to avoid random lags when
        // loading PDF files as attachment.
        mimeType = @"application/pdf";
    } else if ([lowerExtension isEqualToString:@"rtf"]) {
        mimeType = @"text/rtf";
    } else if ([lowerExtension isEqualToString:@"csv"]) {
        mimeType = @"text/csv";
    } else if ([lowerExtension isEqualToString:@"mp4"]) {
        mimeType = @"video/mp4";
    } else if ([lowerExtension isEqualToString:@"czrtj"]) {
        mimeType = @"application/json";
    } else if ([lowerExtension isEqualToString:@"czftp"]) {
        mimeType = @"application/xml";
    } else if ([lowerExtension isEqualToString:@"czamt"]) {
        mimeType = @"application/xml";
    }
    
    return mimeType;
}

+ (BOOL)isStringValidFileName:(NSString *)string {
    NSCharacterSet *invalidSet = [NSCharacterSet characterSetWithCharactersInString:@"?*:|<>/\"\\"];
    
    for (NSUInteger i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([invalidSet characterIsMember:c]) {
            return NO;
        }
    }
    return YES;
}

+ (BOOL)isStringValidFileNameTemplate:(NSString *)string {
    NSCharacterSet *invalidSet = [NSCharacterSet characterSetWithCharactersInString:@"_?*:|<>/\"\\"];
    
    for (NSUInteger i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([invalidSet characterIsMember:c]) {
            return NO;
        }
    }
    return YES;
}

+ (NSMutableArray *)newLocalFiles {
    NSMutableArray *destinationFiles = [[NSMutableArray alloc] init];
    NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:[[NSFileManager defaultManager] cz_documentPath]];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        [destinationFiles addObject:file];
    }
    return destinationFiles;
}

+ (void)mergeArray:(NSMutableArray *)dic1 with:(NSArray *)dic2 {
    NSMutableArray *newItems = [[NSMutableArray alloc] init];
    if (dic1.count > 0) {
        for (NSObject *obj2 in dic2) {
            NSObject *foundObj = obj2;
            for (NSObject *obj1 in dic1) {
                if ([obj2 isEqual:obj1]) {
                    foundObj = nil;
                    break;
                }
            }
            if (foundObj) {
                [newItems addObject:foundObj];
            }
        }
        
        for (NSObject *obj in newItems) {
            [dic1 addObject:obj];
        }
    } else {
        for (NSObject *obj in dic2) {
            [dic1 addObject:obj];
        }
    }
    
    [newItems release];
}

+ (uint64_t)freeSpaceInBytes {
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

+ (float)freeSpacePercentage {
    double totalSpace = 1.0;
    double totalFreeSpace = 0.0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes doubleValue];
        totalFreeSpace = [freeFileSystemSizeInBytes doubleValue];
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace / totalSpace;
}

+ (NSString *)freeSpaceString {
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        double totalFreeSpace = [freeFileSystemSizeInBytes doubleValue];
        totalFreeSpace /= 1024 * 1024;
        if (totalFreeSpace > 1024) {
            return [NSString stringWithFormat:@"%.2fGB", (totalFreeSpace/1024.0)];
        } else {
            return [NSString stringWithFormat:@"%.2fMB", totalFreeSpace];
        }
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
        return @"";
    }
}

//for IPV6
+(NSString *)formatIPV6Address:(struct in6_addr)ipv6Addr {
    NSString *address = nil;
    
    char dstStr[INET6_ADDRSTRLEN];
    char srcStr[INET6_ADDRSTRLEN];
    memcpy(srcStr, &ipv6Addr, sizeof(struct in6_addr));
    if(inet_ntop(AF_INET6, srcStr, dstStr, INET6_ADDRSTRLEN) != NULL){
        address = [NSString stringWithUTF8String:dstStr];
    }
    
    return address;
}

//for IPV4
+(NSString *)formatIPV4Address:(struct in_addr)ipv4Addr {
    NSString *address = nil;
    
    char dstStr[INET_ADDRSTRLEN];
    char srcStr[INET_ADDRSTRLEN];
    memcpy(srcStr, &ipv4Addr, sizeof(struct in_addr));
    if(inet_ntop(AF_INET, srcStr, dstStr, INET_ADDRSTRLEN) != NULL){
        address = [NSString stringWithUTF8String:dstStr];
    }
    
    return address;
}

//+ (NSString *)iPV4ToIPV6:(NSString *)ipAddr {
//    NSString *address = nil;
//    uint32_t ip = 0;
//    NSArray *components = [ipAddr componentsSeparatedByString:@"."];
//    if ([components count] == 4) {
//        uint8_t *ipv4 = (uint8_t *)&ip;
//        for (int i = 0; i < 4; ++i) {
//            ipv4[3 - i] = [components[i] intValue]; // TODO:check it
//        }
//        
//        struct addrinfo hints, *res, *res0;
//        int error, s;
//        const char *cause = NULL;
//        char ipv4_str_buf[INET_ADDRSTRLEN] = { 0 };
//        const char *ipv4_str = inet_ntop(AF_INET, &ipv4, ipv4_str_buf, sizeof(ipv4_str_buf));
//        
//        char ipstr[INET6_ADDRSTRLEN];
//        
//        memset(&hints, 0, sizeof(hints));
//        hints.ai_family = PF_UNSPEC;
//        hints.ai_socktype = SOCK_STREAM;
//        hints.ai_flags = AI_DEFAULT;
//        error = getaddrinfo(ipv4_str, "http", &hints, &res0);
//        if (error) {
//            errx(1, "%s", gai_strerror(error));
//            /*NOTREACHED*/
//        }
//        
//        s = -1;
//        for (res = res0; res; res = res->ai_next) {
//            s = socket(res->ai_family, res->ai_socktype,
//                       res->ai_protocol);
//            if (s < 0) {
//                cause = "socket";
//                continue;
//            }
//            
//            
//            if (connect(s, res->ai_addr, res->ai_addrlen) < 0) {
//                cause = "connect";
//                close(s);
//                s = -1;
//                continue;
//            }
//            
//            switch (res->ai_family) {
//                case AF_INET: {
//                    if(inet_ntop(AF_INET,
//                                 &(((struct sockaddr_in *)(res->ai_addr))->sin_addr),
//                                 ipstr, INET_ADDRSTRLEN) != NULL) {
//                        printf("%s\n", ipstr);
//                        address = [NSString stringWithUTF8String:ipstr];
//                    };
//                    
//                } break;
//                case AF_INET6: {
//                    if(inet_ntop(AF_INET6,
//                                 &(((struct sockaddr_in6 *)(res->ai_addr))->sin6_addr),
//                                 ipstr, INET6_ADDRSTRLEN) != NULL) {
//                        printf("%s\n", ipstr);
//                        address = [NSString stringWithUTF8String:ipstr];
//                    };
//                } break;
//                default:
//                break;
//            }
//        }
//        
//        if (s < 0) {
//            err(1, "%s", cause);
//            /*NOTREACHED*/
//        }
//        freeaddrinfo(res0);
//    }
//    return address;
//}

+ (NSString *)localHostName {
    char baseHostName[256];
    int success = gethostname(baseHostName, 255);
    if (success != 0)  {
        return nil;
    }
    baseHostName[255] = '\0';
    return [NSString stringWithFormat:@"%s.local.", baseHostName];
}

+ (NSString *)localIpAddress {
    // Solution from http://blog.zachwaugh.com/post/309927273/programmatically-retrieving-ip-address-of-iphone
    // Refer to http://www.jianshu.com/p/a6bab07c4062
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *address = nil;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    // Retrieve the current interfaces - returns 0 on success
    int success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if (sa_type == AF_INET || sa_type == AF_INET6) {
                if (sa_type == AF_INET) {
                    // Get NSString from C String
                    address = [self formatIPV4Address:((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr];
                } else if (sa_type == AF_INET6){
                    address = [self formatIPV6Address:((struct sockaddr_in6 *)temp_addr->ifa_addr)->sin6_addr];
//                    if (address && ![address isEqualToString:@""] && ![address.uppercaseString hasPrefix:@"FE80"])
//                        break;
                }
                // Interface is the wifi connection on the iPhone
                if ([name isEqualToString:@"en0"]) {
                    wifiAddress = address;
                } else {
                    // Interface is the cell connection on the iPhone
                    if([name isEqualToString:@"pdp_ip0"]) {
                        cellAddress = address;
                    }
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    address = wifiAddress ? wifiAddress : cellAddress;
    return address;
}

+ (NSString *) localSubnetMask{
    NSString *subnetMask = nil;
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if (sa_type == AF_INET && [name isEqualToString:@"en0"]) {
                subnetMask = [self formatIPV4Address:((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr];
                return subnetMask;
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    return subnetMask;
}

+ (NSString *)andOperationIPAddress:(NSString *)ipAddress
                         subnetMask:(NSString *)subnetMask{
    if (ipAddress && subnetMask) {
        NSString *tempStr = @"";
        NSArray *tempIpFields = [ipAddress componentsSeparatedByString:@"."];
        NSArray *tempSubnetMaskFields  = [subnetMask componentsSeparatedByString:@"."];
        if ( tempIpFields.count && tempSubnetMaskFields.count && tempIpFields.count == tempSubnetMaskFields.count) {
            for ( int i = 0; i < tempIpFields.count; i++) {
                int a = [[tempIpFields objectAtIndex:i] intValue];
                int b = [[tempSubnetMaskFields objectAtIndex:i] intValue];
                if (tempStr.length == 0) {
                    tempStr = [tempStr stringByAppendingFormat:@"%d",a&b];
                }else{
                    tempStr = [tempStr stringByAppendingFormat:@".%d",a&b];
                }
            }
            return tempStr;
        }
    }
    return nil;
}


+ (BOOL)isIPAddress:(NSString*)ip {
    if ([ip rangeOfString:@"."].location != NSNotFound) {
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
                                      options:0
                                      error:nil];
        
        NSUInteger matches = [regex numberOfMatchesInString:ip
                                                    options:0
                                                      range:NSMakeRange(0, [ip length])];
        if (matches == 1) {
            return YES;
        }
    } else if ([ip rangeOfString:@":"].location != NSNotFound) {
        //TODO:Add method to judge if is valid IPV6 address
        return YES;
    }
    return NO;
}

+ (CZDeviceModel)deviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *modelName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    // References:
    // * http://developer.apple.com
    // * http://stackoverflow.com/q/11197509/1241690
    // * http://stackoverflow.com/a/18414094/1241690
    
    NSDictionary *modelMap = @{@"iPad2,1" : @(CZDeviceModeliPad2),      // iPad 2 (Wi-Fi)
                               @"iPad2,2" : @(CZDeviceModeliPad2),      // iPad 2 (GSM)
                               @"iPad2,3" : @(CZDeviceModeliPad2),      // iPad 2 (CDMA)
                               @"iPad2,4" : @(CZDeviceModeliPad2),      // iPad 2 (Wi-Fi Rev. A)
                               @"iPad2,5" : @(CZDeviceModeliPadMini1),  // iPad mini (Wi-Fi)
                               @"iPad2,6" : @(CZDeviceModeliPadMini1),  // iPad mini (Cellular AT&T)
                               @"iPad2,7" : @(CZDeviceModeliPadMini1),  // iPad mini (Cellular Verizon)
                               @"iPad3,1" : @(CZDeviceModeliPad3),      // iPad 3rd generation (Wi-Fi)
                               @"iPad3,2" : @(CZDeviceModeliPad3),      // iPad 3rd generation (Cellular AT&T)
                               @"iPad3,3" : @(CZDeviceModeliPad3),      // iPad 3rd generation (Cellular Verizon)
                               @"iPad3,4" : @(CZDeviceModeliPad4),      // iPad 4th generation (Wi-Fi)
                               @"iPad3,5" : @(CZDeviceModeliPad4),      // iPad 4th generation (GSM)
                               @"iPad3,6" : @(CZDeviceModeliPad4),      // iPad 4th generation (CDMA)
                               @"iPad4,1" : @(CZDeviceModeliPadAir),    // iPad Air (Wi-Fi)
                               @"iPad4,2" : @(CZDeviceModeliPadAir),    // iPad Air (Cellular)
                               @"iPad4,3" : @(CZDeviceModeliPadMini2),  // iPad mini 2nd generation (Wi-Fi)
                               @"iPad4,5" : @(CZDeviceModeliPadMini2),  // iPad mini 2 (Wi-Fi+LTE)
                               @"iPad4,6" : @(CZDeviceModeliPadMini2),  // iPad mini 2 (Rev)
                               @"iPad4,7" : @(CZDeviceModeliPadMini3),  // iPad mini 3 (Wi-Fi)
                               @"iPad4,8" : @(CZDeviceModeliPadMini3),  // iPad mini 3 (A1600)
                               @"iPad4,9" : @(CZDeviceModeliPadMini3),  // iPad mini 3 (A1601)
                               @"iPad5,3" : @(CZDeviceModeliPadAir2),   // iPad Air 2 (Wi-Fi)
                               @"iPad5,4" : @(CZDeviceModeliPadAir2),   // iPad Air 2 (Wi-Fi+LTE)
                               }; // iPad mini 2nd generation (Cellular)
    
    NSNumber *value = modelMap[modelName];
    if (value != nil) {
        return (CZDeviceModel)[value unsignedIntegerValue];
    }
    
    return CZDeviceModelOthers;
}

+ (NSUInteger)iPadModelNumber {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *modelName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if ([modelName hasPrefix:@"iPad"]) {
        modelName = [modelName substringFromIndex:4];
        NSInteger major = 0, minor = 0;
        NSArray *items = [modelName componentsSeparatedByString:@","];
        if (items.count >= 2) {
            minor = [(NSString *)items[1] integerValue];
        } else {
            CZLogv(@"Device number is invalid.");
        }
        if (items.count) {
            major = [(NSString *)items[0] integerValue];
        }
        return major * 100 + minor;
    } else {
        CZLogv(@"Device is not iPad.");
        return 0;
    }
}

// private method: convert md5 row data to NSString
+ (NSString *)md5StringOfRawData:(const unsigned char *)mdRawData {
    NSAssert(mdRawData, @"invalid md row data");
    
    NSMutableString *mutableResult = [[NSMutableString alloc] init];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; ++i) {
        NSString *hexString = [[NSString alloc] initWithFormat:@"%02x", mdRawData[i]];
        [mutableResult appendString:hexString];
        [hexString release];
    }
    
    NSString *result = [[mutableResult copy] autorelease];
    
    [mutableResult release];
    
    return result;
}

+ (NSString *)md5FromString:(NSString *)inputString {
    const char *cString = [inputString UTF8String];
    unsigned char cResult[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cString, (CC_LONG)strlen(cString), cResult);
    
    return [self md5StringOfRawData:cResult];
}

+ (NSString *)md5FromContentOfFile:(NSString *)filePath {
    NSData *fileContent = [NSData dataWithContentsOfFile:filePath];
    return [self md5FromData:fileContent];
}

+ (NSString *)md5FromData:(NSData *)data {
    if (data.length) {
        unsigned char cResult[CC_MD5_DIGEST_LENGTH];
        CC_MD5(data.bytes, (CC_LONG)data.length, cResult);
        
        return [self md5StringOfRawData:cResult];
    } else {
        return nil;
    }
}

+ (BOOL)isMacAddressFromCarlZeissChina:(NSString *)macAddress {
    // Judge whether the string has correct length for a MAC address.
    if ([macAddress length] != 17) { // 12 * alpha/numeric + 5 * ':'
        return NO;
    }
    
    // TODO: Remove this before actual release.
    //
    // If the MAC address filter is disabled in "Debug" section of global
    // settings, just return YES for all valid MAC addresses.
    if ([[CZDefaultSettings sharedInstance] disableMacAddressFilter]) {
        return YES;
    }
    
    // Judge whether the MAC address is in the range of Carl Zeiss China pool.
    return [[macAddress uppercaseString] hasPrefix:@"00:20:0D:F9"];
}

+ (void)updateThemeForView:(UIView *)view withColor:(UIColor *)color {
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            UILabel *label = (UILabel *)subView;
            [label setTextColor:color];
        }
    }
}

+ (BOOL)isOperatingSystemAtIOS9 {
    if ([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)]) {
        // check for any version >= iOS 9.0 using 'isOperatingSystemAtLeastVersion'
        NSOperatingSystemVersion ios9_0_0 = (NSOperatingSystemVersion){9, 0, 0};
        if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios9_0_0]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)isRunningOnIPhone {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

+ (BOOL)isRunningOnIPhoneXSeries {
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && MAX(screenBounds.size.width, screenBounds.size.height) >= 812){
        return YES;
    }
    return NO;
}

# pragma mark - Private methods

+ (uint8_t)clampIntToUint8 : (int) c{
    if (c < 0){
        return 0;
    }
    else if (c > UCHAR_MAX){
        return UCHAR_MAX;
    }
    else{
        return c;
    }
}

+ (UIImage *)imageFromGray8Buffer:(const UInt8 *)pixels width:(int)width height:(int)height {
    const size_t bitsPerComponent = 8;
    const size_t bitsPerPixel = 8;
    const size_t bytesPerPixel = bitsPerPixel / bitsPerComponent;
    const size_t bytesPerRow = width * bytesPerPixel;
    
    CFDataRef dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (const UInt8 *)pixels, height * width * sizeof(Pixel_8), kCFAllocatorMalloc);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent,
                                        bitsPerPixel, bytesPerRow, colorSpace,
                                        0, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    
    UIImage *image = [[[UIImage alloc] initWithCGImage:imageRef] autorelease];
    
    CGImageRelease(imageRef);
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CFRelease(dataRef);
    return image;
}

+ (UIImage *)imageFromGray16Buffer:(const UInt8 *)pixels width:(int)width height:(int)height {
    const size_t bitsPerComponent = 16;
    const size_t bitsPerPixel = 16;
    const size_t bytesPerPixel = 2;
    const size_t bytesPerRow = width * bytesPerPixel;
    
    CFDataRef dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (const UInt8 *)pixels, height * width * sizeof(Pixel_88), kCFAllocatorMalloc);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent,
                                        bitsPerPixel, bytesPerRow, colorSpace,
                                        kCGBitmapByteOrder16Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    
    UIImage *image = [[[UIImage alloc] initWithCGImage:imageRef] autorelease];
    
    CGImageRelease(imageRef);
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CFRelease(dataRef);
    return image;
}

+ (UIImage *)imageFromRGBBuffer:(const UInt8 *)pixels width:(int)width height:(int)height {
    const size_t bitsPerComponent = 8;
    const size_t bitsPerPixel = 32;
    const size_t bytesPerPixel = bitsPerPixel / bitsPerComponent;
    const size_t bytesPerRow = width * bytesPerPixel;
    
    
    CFDataRef dataRef = CFDataCreateWithBytesNoCopy(kCFAllocatorMalloc, (const UInt8 *)pixels, height * width * sizeof(Pixel_8888), kCFAllocatorMalloc);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);
    CGBitmapInfo bitmapInfo = kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Big;
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent,
                                        bitsPerPixel, bytesPerRow, colorSpace,
                                        bitmapInfo, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    
    UIImage *image = [[[UIImage alloc] initWithCGImage:imageRef] autorelease];
    
    CGImageRelease(imageRef);
    CGDataProviderRelease(dataProvider);
    CGColorSpaceRelease(colorSpace);
    CFRelease(dataRef);
    return image;
}

@end

@implementation UIImage (CommonUtils)

- (UIImage *)imageByRotatingToUp {
    if (self.imageOrientation == UIImageOrientationUp) {
        return self;
    }
    
    UIGraphicsBeginImageContextWithOptions(self.size, YES, self.scale);
    [self drawAtPoint:CGPointMake(0, 0)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
