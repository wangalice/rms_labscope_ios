//
//  CZUndoManager.m
//  Hermes
//
//  Created by Ralph Jin on 1/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZUndoManager.h"

@interface CZUndoManager()

@property (nonatomic, retain, readonly) NSUndoManager *manager;
@property (nonatomic, retain, readonly) NSMutableArray *undoStack;
@property (nonatomic, retain, readonly) NSMutableArray *redoStack;
@property (nonatomic, readonly) NSUInteger totalUndoSize;

- (void)cleanup;

- (void)didUndoAction:(id<CZUndoAction>)action;
- (void)didRedoAction:(id<CZUndoAction>)action;

@end

#pragma mark -
#pragma mark Inner class of undoable action wrapper

@interface CZActionWrapper : NSObject
@property (nonatomic, assign, readonly) CZUndoManager *owner;
@property (nonatomic, retain, readonly) id<CZUndoAction> action;

- (id)initWithAction:(id<CZUndoAction>)anAction
               owner:(CZUndoManager *)owner;

- (void) redo;
- (void) undo;
- (void) registerUndo;

@end

@implementation CZActionWrapper


- (id)initWithAction:(id<CZUndoAction>)anAction
               owner:(CZUndoManager *)owner {
    self = [super init];
    if (self) {
        _owner = owner;
        _action = [anAction retain];
    }
    return self;
}

- (void)dealloc {
    [_action release];
    [super dealloc];
}

- (void)undo {
    [_action undo];
    [[_owner.manager prepareWithInvocationTarget:self] redo];
    
    // Notify did undo
    [_owner didUndoAction:_action];
}

- (void)redo {
    [_action redo];
    [[_owner.manager prepareWithInvocationTarget:self] undo];
    
    // Notify did redo
    [_owner didRedoAction:_action];
}

- (void)registerUndo {
    [[_owner.manager prepareWithInvocationTarget:self] undo];
}

@end

#pragma mark -
#pragma mark CZUndoManager

@implementation CZUndoManager

- (id)init {
    self = [super init];
    if (self) {
        _manager = [[NSUndoManager alloc] init];
        _undoStack = [[NSMutableArray alloc] init];
        _redoStack = [[NSMutableArray alloc] init];
        _totalUndoSize = 0;
    }
    
    return self;
}

- (void)dealloc {
    [_undoStack release];
    [_redoStack release];
    [_manager release];
    [super dealloc];
}

- (void)pushAction:(id<CZUndoAction>)anAction {
    if (anAction == nil) {
        return;
    }
    
    @autoreleasepool {  // Make dead action dealloc as early as possible.
        CZActionWrapper *wrapper = [[CZActionWrapper alloc] initWithAction:anAction owner:self];
        [anAction do];
        [wrapper registerUndo];
        
        [_undoStack addObject:wrapper];
        [wrapper release];
        
        [_redoStack removeAllObjects];
        
        NSUInteger actionCostMemory = [anAction costMemory];
        _totalUndoSize += actionCostMemory;
        
        [self cleanup];
        
        if ([_delegate respondsToSelector:@selector(undoManagerDidPushAction:)]) {
            [_delegate undoManagerDidPushAction:self];
        }
    }
}

- (void)removeAllActions {
    [_manager removeAllActions];
    [_undoStack removeAllObjects];
    [_redoStack removeAllObjects];
    _totalUndoSize = 0;
}

- (void)cleanup {
    if (self.maxMemorySize == 0U || _totalUndoSize <= self.maxMemorySize) {
        return;
    }
    
    NSUInteger currentLevels = [_undoStack count];
    if (currentLevels <= 1U) {
        return;
    }
    
    // find cleanup range
    NSUInteger totalCostMemory = _totalUndoSize;
    NSUInteger cleanupLevels = 0;
    for (NSUInteger i = 0U; i < currentLevels - 1; ++i) {  // let's leave at lease one undo action
        CZActionWrapper *wrapper = [_undoStack objectAtIndex:i];
        NSUInteger costMemory = [wrapper.action costMemory];
        totalCostMemory -= costMemory;
        if (totalCostMemory <= _maxMemorySize) {
            cleanupLevels = i + 1U;
            break;
        }
    }
    
    // clean up undo memory size stack
    [_undoStack removeObjectsInRange:NSMakeRange(0U, cleanupLevels)];
    _totalUndoSize = totalCostMemory;
    
    // clean up actions in NSUndoManager
    NSUInteger remainLevels = currentLevels - cleanupLevels;
    [_manager setLevelsOfUndo:remainLevels];
    [_manager setLevelsOfUndo:0];
}

- (void)redo {
    if (![_manager canRedo]) {
        return;
    }
    
    @autoreleasepool {  // Make dead action dealloc as early as possible.
        [_manager redo];
    }
}

- (void)undo {
    if (![_manager canUndo]) {
        return;
    }
    @autoreleasepool {  // Make dead action dealloc as early as possible.
        [_manager undo];
    }
}

- (BOOL)canUndo {
    BOOL canUndo = [_manager canUndo];
    return canUndo;
}

- (BOOL)canRedo {
    BOOL canRedo = [_manager canRedo];
    return canRedo;
}

- (void)didUndoAction:(id<CZUndoAction>)action {
    // pop undo stack and push to redo stack
    CZActionWrapper *actionWrapper = [_undoStack lastObject];
    if (actionWrapper == nil) {
        return;
    }
    
    // pop undo stack and push to redo stack
    NSUInteger costMemory = [actionWrapper.action costMemory];
    _totalUndoSize -= costMemory;
    [_redoStack addObject:actionWrapper];
    [_undoStack removeLastObject];
    
    if ([_delegate respondsToSelector:@selector(didUndoAction:)]) {
        [_delegate didUndoAction:action];
    }
}

- (void)didRedoAction:(id<CZUndoAction>)action {
    CZActionWrapper *actionWrapper = [_redoStack lastObject];
    if (actionWrapper == nil) {
        return;
    }
    
    // pop redo stack and push to undo stack
    NSUInteger costMemory = [actionWrapper.action costMemory];
    _totalUndoSize += costMemory;
    [_undoStack addObject:actionWrapper];
    [_redoStack removeLastObject];
    
    if ([_delegate respondsToSelector:@selector(didRedoAction:)]) {
        [_delegate didRedoAction:action];
    }
}

@end
