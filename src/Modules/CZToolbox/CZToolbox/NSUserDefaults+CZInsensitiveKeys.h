//
//  NSUserDefaults+CZInsensitiveKeys.h
//  Hermes
//
//  Created by ZEISS on 2018/9/17.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (CZInsensitiveKeys)

/**
 Get values from the NSUserdefaults with the insensitive key

 @param key the string is made with lowercase/uppercase
 @return the value from the NSUserdefaults, may be nil
 */
- (id _Nullable)g_valueFromUserDefaultsWithInsensitiveKeys:(NSString *)key;

/**
 Set values to the NSUserdefaults with the key, and will remove the value with key which is lowsercase/upppercase not equal to current key;

 @param value the value for the key
 @param key the key
 */
- (void)g_setValue:(id _Nullable)value insensitiveKey:(NSString *)key;


- (id _Nullable)g_valueFromUserDefaultsWithInsensitiveKeys:(NSString *)key mixedKeys:(NSArray <NSString *> *)keys;

- (void)g_setValue:(id _Nullable)value insensitiveKey:(NSString *)key mixedKeys:(NSArray <NSString *> *)keys;

@end

NS_ASSUME_NONNULL_END
