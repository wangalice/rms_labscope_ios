//
//  CZWifiNotifier.m
//  Hermes
//
//  Created by Ralph Jin on 6/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZWifiNotifier.h"
#import "CZLocalizedString.h"
#import "CZLog.h"

#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <SystemConfiguration/CaptiveNetwork.h>

NSString * const kCZWifiChangedNotification = @"kCZWifiChangedNotification";
#define kShouldPrintReachabilityFlags 0

typedef enum : NSInteger {
    NotReachable = 0,
    ReachableViaWiFi,
    ReachableViaWWAN
} NetworkStatus;

@interface CZWifiNotifier () {
    SCNetworkReachabilityRef _wifiReachabilityRef;
}

@end

@implementation CZWifiNotifier

#pragma mark - Supporting functions

static void PrintReachabilityFlags(SCNetworkReachabilityFlags flags, const char* comment) {
#if kShouldPrintReachabilityFlags
    
    NSLog(@"Reachability Flag Status: %c%c %c%c%c%c%c%c%c %s\n",
          (flags & kSCNetworkReachabilityFlagsIsWWAN)				? 'W' : '-',
          (flags & kSCNetworkReachabilityFlagsReachable)            ? 'R' : '-',
          
          (flags & kSCNetworkReachabilityFlagsTransientConnection)  ? 't' : '-',
          (flags & kSCNetworkReachabilityFlagsConnectionRequired)   ? 'c' : '-',
          (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic)  ? 'C' : '-',
          (flags & kSCNetworkReachabilityFlagsInterventionRequired) ? 'i' : '-',
          (flags & kSCNetworkReachabilityFlagsConnectionOnDemand)   ? 'D' : '-',
          (flags & kSCNetworkReachabilityFlagsIsLocalAddress)       ? 'l' : '-',
          (flags & kSCNetworkReachabilityFlagsIsDirect)             ? 'd' : '-',
          comment
          );
#endif
}

static void ReachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void *info) {
#pragma unused (target, flags)
    NSCAssert(info != NULL, @"info was NULL in ReachabilityCallback");
    NSCAssert([(NSObject *)info isKindOfClass:[CZWifiNotifier class]], @"info was wrong class in ReachabilityCallback");

    // We're on the main RunLoop, so an NSAutoreleasePool is not necessary, but
    // is added defensively in case someone uses the Reachablity object on a
    // different thread.
    @autoreleasepool {
        NSString *ssid = [CZWifiNotifier copySSID];
        CZLogv(@"Wi-Fi notifier posts notification. Latest SSID is %@.", ssid);
        [ssid release];
        
        [[CZWifiNotifier sharedInstance] postNotification];
    }
}

#pragma mark - Singleton Design Pattern

static CZWifiNotifier *uniqueInstance = nil;

+ (CZWifiNotifier *)sharedInstance {
    @synchronized ([CZWifiNotifier class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (id)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

#pragma mark - Lifecycle management

- (id)init {
    self = [super init];
    if (self) {
//  Support IPV6 - only
//        Older versions of this sample included the method reachabilityForLocalWiFi. As originally designed, this method allowed apps using Bonjour to check the status of "local only" Wi-Fi (Wi-Fi without a connection to the larger internet) to determine whether or not they should advertise or browse.
//        
//        However, the additional peer-to-peer APIs that have since been added to iOS and OS X have rendered it largely obsolete.  Because of the narrow use case for this API and the large potential for misuse, reachabilityForLocalWiFi has been removed from Reachability.
//            
//            Apps that have a specific requirement can use reachabilityWithAddress to monitor IN_LINKLOCALNETNUM (that is, 169.254.0.0).
//            
//            Note: ONLY apps that have a specific requirement should be monitoring IN_LINKLOCALNETNUM.  For the overwhelming majority of apps, monitoring this address is unnecessary and potentially harmful.
        struct sockaddr_in zeroAddress;
        bzero(&zeroAddress, sizeof(zeroAddress));
        zeroAddress.sin_len = sizeof(zeroAddress);
        
        zeroAddress.sin_family = AF_INET;
        
        _wifiReachabilityRef = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault,
                                                                      (const struct sockaddr *)&zeroAddress);
    }
    return self;
}

- (void)dealloc {
    [self stopNotifier];
    
    @synchronized (self) {
        if (_wifiReachabilityRef != NULL) {
            CFRelease(_wifiReachabilityRef);
        }
    }
    
    [super dealloc];
}

#pragma mark - Public methods

+ (NSString *)copyOriginalSSID {
    return [[CZWifiNotifier sharedInstance] copyOriginalSSID];
}

- (NSString *)copyOriginalSSID {
    @synchronized ([CZWifiNotifier class]) {
        NSString *ssid = nil;
        NSArray *interfaces = (id)CNCopySupportedInterfaces();
        if ([interfaces count]) {
            NSDictionary *netWorkInfo = (id)CNCopyCurrentNetworkInfo((CFStringRef)interfaces[0]);
            ssid = netWorkInfo[(id)kCNNetworkInfoKeySSID];
            [ssid retain];
            [netWorkInfo release];
        }
        [interfaces release];
        return ssid;
    }
}

+ (NSString *)copySSID {
    return [[CZWifiNotifier sharedInstance] copySSID];
}

- (NSString *)copySSID {
    @synchronized ([CZWifiNotifier class]) {
        NSString *ssid = nil;
        NSArray *interfaces = (id)CNCopySupportedInterfaces();
        if ([interfaces count]) {
            NSDictionary *netWorkInfo = (id)CNCopyCurrentNetworkInfo((CFStringRef)interfaces[0]);
            ssid = netWorkInfo[(id)kCNNetworkInfoKeySSID];
            [ssid retain];
            [netWorkInfo release];
        }
        [interfaces release];
        
        if (ssid == nil) {
            if (self.isReachable) {
                ssid = [L(@"FILES_INFO_UNKNOWN") copy];
            } else {
                ssid = [L(@"NO_CONNECTION") copy];
            }
        }
        
        return ssid;
    }
}

- (BOOL)startNotifier {
    @synchronized (self) {
        BOOL returnValue = NO;
        SCNetworkReachabilityContext context = {0, self, NULL, NULL, NULL};
        
        if (SCNetworkReachabilitySetCallback(_wifiReachabilityRef, ReachabilityCallback, &context)) {
            if (SCNetworkReachabilityScheduleWithRunLoop(_wifiReachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode)) {
                returnValue = YES;
            }
        }
        
        return returnValue;
    }
}

- (void)stopNotifier {
    @synchronized (self) {
        if (_wifiReachabilityRef != NULL) {
            SCNetworkReachabilityUnscheduleFromRunLoop(_wifiReachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        }
    }
}

- (void)postNotification {
    // Post a notification to notify the client that the network reachability changed.
    [[NSNotificationCenter defaultCenter] postNotificationName:kCZWifiChangedNotification object:self];
}

#pragma mark - Network Flag Handling

- (NetworkStatus)networkStatusForFlags:(SCNetworkReachabilityFlags)flags {
    PrintReachabilityFlags(flags, "networkStatusForFlags");
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
    {
        // The target host is not reachable.
        return NotReachable;
    }
    
    NetworkStatus returnValue = NotReachable;
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        /*
         If the target host is reachable and no connection is required then we'll assume (for now) that you're on Wi-Fi...
         */
        returnValue = ReachableViaWiFi;
    }
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
    {
        /*
         ... and the connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs...
         */
        
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            /*
             ... and no [user] intervention is needed...
             */
            returnValue = ReachableViaWiFi;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        /*
         ... but WWAN connections are OK if the calling application is using the CFNetwork APIs.
         */
        returnValue = ReachableViaWWAN;
    }
    
    return returnValue;
}

- (BOOL)connectionRequired {
    NSAssert(_wifiReachabilityRef != NULL, @"connectionRequired called with NULL reachabilityRef");
    SCNetworkReachabilityFlags flags;
    
    if (SCNetworkReachabilityGetFlags(_wifiReachabilityRef, &flags))
    {
        return (flags & kSCNetworkReachabilityFlagsConnectionRequired);
    }
    
    return NO;
}


- (NetworkStatus)currentReachabilityStatus {
    NSAssert(_wifiReachabilityRef != NULL, @"currentNetworkStatus called with NULL SCNetworkReachabilityRef");
    NetworkStatus returnValue = NotReachable;
    SCNetworkReachabilityFlags flags;
    
    if (SCNetworkReachabilityGetFlags(_wifiReachabilityRef, &flags))
    {
        returnValue = [self networkStatusForFlags:flags];
    }
    
    return returnValue;
}

- (BOOL)isReachable {
    @synchronized (self) {
        BOOL reachable = NO;
        if (_wifiReachabilityRef == NULL) {
            CZLogv(@"isReachable called with NULL _wifiReachabilityRef - shouldn't happen");
            reachable = NO;
        }
        
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(_wifiReachabilityRef, &flags)) {
            NetworkStatus netStatus = [self currentReachabilityStatus];
            switch (netStatus) {
                case NotReachable: {
                    reachable = NO;
                    break;
                }
                    
                case ReachableViaWWAN:
                case ReachableViaWiFi: {
                    reachable = YES;
                    break;
                }
            }
        }
        
        BOOL needsConnection = [self connectionRequired];
        return (reachable && !needsConnection) ? YES : NO;
    }
}

@end
