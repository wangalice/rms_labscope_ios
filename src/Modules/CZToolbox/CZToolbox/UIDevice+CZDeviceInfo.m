//
//  UIDevice+CZDeviceInfo.m
//  Hermes
//
//  Created by Sun, Shaoge on 12/01/2018.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "UIDevice+CZDeviceInfo.h"
#import <sys/utsname.h>
#import <sys/types.h>
#import <sys/sysctl.h>
#import <mach/machine.h>

typedef NS_ENUM(NSInteger, DeviceVersion){
    UnknownDevice         = 0,
    Simulator             = 1,
    
    iPhone4               = 3,
    iPhone4S              = 4,
    iPhone5               = 5,
    iPhone5C              = 6,
    iPhone5S              = 7,
    iPhone6               = 8,
    iPhone6Plus           = 9,
    iPhone6S              = 10,
    iPhone6SPlus          = 11,
    iPhone7               = 12,
    iPhone7Plus           = 13,
    iPhone8               = 14,
    iPhone8Plus           = 15,
    iPhoneX               = 16,
    iPhoneSE              = 17,
    
    iPad1                 = 18,
    iPad2                 = 19,
    iPadMini              = 20,
    iPad3                 = 21,
    iPad4                 = 22,
    iPadAir               = 23,
    iPadMini2             = 24,
    iPadAir2              = 25,
    iPadMini3             = 26,
    iPadMini4             = 27,
    iPadPro12Dot9Inch     = 28,
    iPadPro9Dot7Inch      = 29,
    iPad5                 = 30,
    iPadPro12Dot9Inch2Gen = 31,
    iPadPro10Dot5Inch     = 32,
    
    iPodTouch1Gen         = 33,
    iPodTouch2Gen         = 34,
    iPodTouch3Gen         = 35,
    iPodTouch4Gen         = 36,
    iPodTouch5Gen         = 37,
    iPodTouch6Gen         = 38
};

typedef NS_ENUM(NSInteger, DeviceSize){
    UnknownSize     = 0,
    Screen3Dot5inch = 1,
    Screen4inch     = 2,
    Screen4Dot7inch = 3,
    Screen5Dot5inch = 4,
    Screen5Dot8inch = 5
};

@implementation UIDevice (CZDeviceInfo)

+ (NSInteger)cz_deviceModel {
    //Reference:https://github.com/sebyddd/SDVersion
    struct utsname systemInfo;
    uname(&systemInfo);

    NSString *code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    NSInteger version = [[[self deviceNamesByCode] objectForKey:code] integerValue];
    return version;
}

+ (NSString *)cz_deviceModelString {
    return [UIDevice deviceNameForVersion:[UIDevice cz_deviceModel]];
}

+ (NSDictionary*)deviceNamesByCode
{
    static NSDictionary *deviceNamesByCode = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        deviceNamesByCode = @{
                              //iPhones
                              @"iPhone3,1"  : @(iPhone4),
                              @"iPhone3,2"  : @(iPhone4),
                              @"iPhone3,3"  : @(iPhone4),
                              @"iPhone4,1"  : @(iPhone4S),
                              @"iPhone4,2"  : @(iPhone4S),
                              @"iPhone4,3"  : @(iPhone4S),
                              @"iPhone5,1"  : @(iPhone5),
                              @"iPhone5,2"  : @(iPhone5),
                              @"iPhone5,3"  : @(iPhone5C),
                              @"iPhone5,4"  : @(iPhone5C),
                              @"iPhone6,1"  : @(iPhone5S),
                              @"iPhone6,2"  : @(iPhone5S),
                              @"iPhone7,2"  : @(iPhone6),
                              @"iPhone7,1"  : @(iPhone6Plus),
                              @"iPhone8,1"  : @(iPhone6S),
                              @"iPhone8,2"  : @(iPhone6SPlus),
                              @"iPhone8,4"  : @(iPhoneSE),
                              @"iPhone9,1"  : @(iPhone7),
                              @"iPhone9,3"  : @(iPhone7),
                              @"iPhone9,2"  : @(iPhone7Plus),
                              @"iPhone9,4"  : @(iPhone7Plus),
                              @"iPhone10,1" : @(iPhone8),
                              @"iPhone10,4" : @(iPhone8),
                              @"iPhone10,2" : @(iPhone8Plus),
                              @"iPhone10,5" : @(iPhone8Plus),
                              @"iPhone10,3" : @(iPhoneX),
                              @"iPhone10,6" : @(iPhoneX),
                              @"i386"       : @(Simulator),
                              @"x86_64"     : @(Simulator),
                              
                              //iPads
                              @"iPad1,1"  : @(iPad1),
                              @"iPad2,1"  : @(iPad2),
                              @"iPad2,2"  : @(iPad2),
                              @"iPad2,3"  : @(iPad2),
                              @"iPad2,4"  : @(iPad2),
                              @"iPad2,5"  : @(iPadMini),
                              @"iPad2,6"  : @(iPadMini),
                              @"iPad2,7"  : @(iPadMini),
                              @"iPad3,1"  : @(iPad3),
                              @"iPad3,2"  : @(iPad3),
                              @"iPad3,3"  : @(iPad3),
                              @"iPad3,4"  : @(iPad4),
                              @"iPad3,5"  : @(iPad4),
                              @"iPad3,6"  : @(iPad4),
                              @"iPad4,1"  : @(iPadAir),
                              @"iPad4,2"  : @(iPadAir),
                              @"iPad4,3"  : @(iPadAir),
                              @"iPad4,4"  : @(iPadMini2),
                              @"iPad4,5"  : @(iPadMini2),
                              @"iPad4,6"  : @(iPadMini2),
                              @"iPad4,7"  : @(iPadMini3),
                              @"iPad4,8"  : @(iPadMini3),
                              @"iPad4,9"  : @(iPadMini3),
                              @"iPad5,1"  : @(iPadMini4),
                              @"iPad5,2"  : @(iPadMini4),
                              @"iPad5,3"  : @(iPadAir2),
                              @"iPad5,4"  : @(iPadAir2),
                              @"iPad6,3"  : @(iPadPro9Dot7Inch),
                              @"iPad6,4"  : @(iPadPro9Dot7Inch),
                              @"iPad6,7"  : @(iPadPro12Dot9Inch),
                              @"iPad6,8"  : @(iPadPro12Dot9Inch),
                              @"iPad6,11" : @(iPad5),
                              @"iPad6,12" : @(iPad5),
                              @"iPad7,1"  : @(iPadPro12Dot9Inch2Gen),
                              @"iPad7,2"  : @(iPadPro12Dot9Inch2Gen),
                              @"iPad7,3"  : @(iPadPro10Dot5Inch),
                              @"iPad7,4"  : @(iPadPro10Dot5Inch),
                              
                              //iPods
                              @"iPod1,1" : @(iPodTouch1Gen),
                              @"iPod2,1" : @(iPodTouch2Gen),
                              @"iPod3,1" : @(iPodTouch3Gen),
                              @"iPod4,1" : @(iPodTouch4Gen),
                              @"iPod5,1" : @(iPodTouch5Gen),
                              @"iPod7,1" : @(iPodTouch6Gen)};
#pragma clang diagnostic pop
    });
    
    return deviceNamesByCode;
}

+ (NSString *)deviceNameForVersion:(NSInteger)deviceVersion
{
    return @{
             @(iPhone4)              : @"iPhone 4",
             @(iPhone4S)             : @"iPhone 4S",
             @(iPhone5)              : @"iPhone 5",
             @(iPhone5C)             : @"iPhone 5C",
             @(iPhone5S)             : @"iPhone 5S",
             @(iPhone6)              : @"iPhone 6",
             @(iPhone6Plus)          : @"iPhone 6 Plus",
             @(iPhone6S)             : @"iPhone 6S",
             @(iPhone6SPlus)         : @"iPhone 6S Plus",
             @(iPhone7)              : @"iPhone 7",
             @(iPhone7Plus)          : @"iPhone 7 Plus",
             @(iPhone8)              : @"iPhone 8",
             @(iPhone8Plus)          : @"iPhone 8 Plus",
             @(iPhoneX)              : @"iPhone X",
             @(iPhoneSE)             : @"iPhone SE",
             
             @(iPad1)                : @"iPad 1",
             @(iPad2)                : @"iPad 2",
             @(iPadMini)             : @"iPad Mini",
             @(iPad3)                : @"iPad 3",
             @(iPad4)                : @"iPad 4",
             @(iPadAir)              : @"iPad Air",
             @(iPadMini2)            : @"iPad Mini 2",
             @(iPadAir2)             : @"iPad Air 2",
             @(iPadMini3)            : @"iPad Mini 3",
             @(iPadMini4)            : @"iPad Mini 4",
             @(iPadPro9Dot7Inch)     : @"iPad Pro 9.7 inch",
             @(iPad5)                : @"iPad 5",
             @(iPadPro12Dot9Inch)    : @"iPad Pro 12.9 inch",
             @(iPadPro10Dot5Inch)    : @"iPad Pro 10.5 inch",
             @(iPadPro12Dot9Inch2Gen): @"iPad Pro 12.9 inch",
             
             @(iPodTouch1Gen)        : @"iPod Touch 1st Gen",
             @(iPodTouch2Gen)        : @"iPod Touch 2nd Gen",
             @(iPodTouch3Gen)        : @"iPod Touch 3rd Gen",
             @(iPodTouch4Gen)        : @"iPod Touch 4th Gen",
             @(iPodTouch5Gen)        : @"iPod Touch 5th Gen",
             @(iPodTouch6Gen)        : @"iPod Touch 6th Gen",
             
             @(Simulator)            : @"Simulator",
             @(UnknownDevice)        : @"Unknown Device"
             }[@(deviceVersion)];
}

//Reference:https://stackoverflow.com/questions/19859388/how-can-i-get-the-ios-device-cpu-architecture-in-runtime
+ (NSString *)cz_cpuType {
    
    NSMutableString *cpu = [[NSMutableString alloc] init];
    size_t size;
    cpu_type_t type;
    cpu_subtype_t subtype;
    size = sizeof(type);
    sysctlbyname("hw.cputype", &type, &size, NULL, 0);
    
    size = sizeof(subtype);
    sysctlbyname("hw.cpusubtype", &subtype, &size, NULL, 0);
    
    // values for cputype and cpusubtype defined in mach/machine.h
    if (type == CPU_TYPE_X86) {
        [cpu appendString:@"x86"];

    }
    else if (type == CPU_TYPE_ARM) {
        [cpu appendString:@"ARM"];
        switch(subtype)
        {
            case CPU_SUBTYPE_ARM_V7: {
                [cpu appendString:@"V7"];
            }
                break;
            case CPU_SUBTYPE_ARM_V6: {
                [cpu appendString:@"V6"];
            }
                break;
            case CPU_SUBTYPE_ARM_V8: {
                [cpu appendString:@"V8"];
            }
                break;
            default:
                [cpu appendString:@"Others"];
                break;
        }
    }
    return cpu;
}

+ (NSString *)cz_systemVersion {
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *)updateApplicationTime {
    return nil;
}

@end
