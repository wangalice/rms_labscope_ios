//
//  NSCharacterSet+StandardCharacterSets.h
//  Common
//
//  Created by Li, Junlin on 1/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCharacterSet (StandardCharacterSets)

@property (class, nonatomic, readonly, copy) NSCharacterSet *cz_numericCharacterSet;
@property (class, nonatomic, readonly, copy) NSCharacterSet *cz_alphanumericCharacterSet;

@end
