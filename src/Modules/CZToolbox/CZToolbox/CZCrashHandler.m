//
//  CZCrashHandler.m
//  Matscope
//
//  Created by Ralph Jin on 12/30/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZCrashHandler.h"
#import <UIKit/UIKit.h>
#import "CZLog.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>

static NSString * const UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";
static NSString * const UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";

volatile int32_t UncaughtExceptionCount = 0;
const static int32_t UncaughtExceptionMaximum = 10;

@interface CZCrashHandler ()

+ (void)handleException:(NSException *)exception;

@end

void HandleException(NSException *exception) {
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    if (exceptionCount > UncaughtExceptionMaximum) {
        return;
    }
    
    if ([NSThread isMainThread]) {
        [CZCrashHandler handleException:exception];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [CZCrashHandler handleException:exception];
        });
    }
}

void SignalHandler(int signal) {
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    if (exceptionCount > UncaughtExceptionMaximum) {
        return;
    }
    
    NSException *exception = [NSException exceptionWithName:UncaughtExceptionHandlerSignalExceptionName
                                                     reason:[NSString stringWithFormat:@"Signal %d was raised.", signal]
                                                   userInfo:@{ UncaughtExceptionHandlerSignalKey:@(signal)}];
    
    if ([NSThread isMainThread]) {
        [CZCrashHandler handleException:exception];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [CZCrashHandler handleException:exception];
        });
    }
}

@implementation CZCrashHandler

+ (NSArray *)backtrace {
    void* callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);
    
    int i;
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    for (i = 0; i < frames; i++) {
        [backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
    }
    free(strs);
    
    return backtrace;
}

+ (void)handleException:(NSException *)exception {
    CZLoge(@"%@", exception);
    CZLoge(@"Backtrace: %@", [CZCrashHandler backtrace]);
    
    // notify app will terminate
    id<UIApplicationDelegate> appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate respondsToSelector:@selector(applicationWillTerminate:)]) {
        [appDelegate applicationWillTerminate:[UIApplication sharedApplication]];
    }
    
    // uninstall exception handler
    NSSetUncaughtExceptionHandler(NULL);
    signal(SIGABRT, SIG_DFL);
    signal(SIGILL, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGBUS, SIG_DFL);
    signal(SIGPIPE, SIG_IGN);
    
    if ([[exception name] isEqualToString:UncaughtExceptionHandlerSignalExceptionName]) {
        kill(getpid(), [[[exception userInfo] objectForKey:UncaughtExceptionHandlerSignalKey] intValue]);
    } else {
        [exception raise];
    }
}

+ (void)installExceptionHandler {
    NSSetUncaughtExceptionHandler(&HandleException);
    signal(SIGABRT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGFPE, SignalHandler);
    signal(SIGBUS, SignalHandler);
    signal(SIGPIPE, SignalHandler);
}

@end
