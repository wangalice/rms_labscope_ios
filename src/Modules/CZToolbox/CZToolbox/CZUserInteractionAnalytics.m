//
//  CZUserInteractionAnalytics.m
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import "CZUserInteractionAnalytics.h"
#import "CZUserInteractionLog.h"
#import "CZUserInteractionTracker.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>

NSString * const kShowDefaultSettingSwitchingMessage = @"kShowDefaultSettingSwitchingMessage";
NSString * const kShowSnapShortcutModeSwitchingMessage = @"kShowSnapShortcutModeSwitchingMessage";
NSString * const kStopShowingAskOnSaveMessage = @"kStopShowingAskOnSaveMessage";

@interface CZUserInteractionAnalytics ()

@property (nonatomic, assign) NSUInteger repeatTimes;

@end

@implementation CZUserInteractionAnalytics

- (instancetype)init {
    self = [super init];
    if (self) {
        _repeatTimes = 0;
    }
    
    return self;
}

- (void)parseLog:(CZUserInteractionLog *)log {
    
}

@end

@interface CZSnappingShortcutAnalytics ()

@property (nonatomic, assign) NSUInteger snapButtonPressedTimes;
@property (nonatomic, strong) NSString *orginalSnapShortcutStatus;

- (void)parseLog:(CZUserInteractionLog *)log;

@end


@implementation CZSnappingShortcutAnalytics

- (instancetype)init {
    self = [super init];
    if (self) {
        _snapButtonPressedTimes = 0;
        _orginalSnapShortcutStatus = nil;
    }
    
    return self;
}

- (void)parseLog:(CZUserInteractionLog *)log {
    NSString *category = log.category;
    if ([category isEqualToString:kCZTimingEventCategory]) {
        NSString *event = log.event;
        BOOL hasTabSwitched = ([event isEqualToString:@"switch_from_live_to_image"] || [event isEqualToString:@"switch_from_image_to_live"]);
        if (hasTabSwitched) {
            // Detect snap shortcut mode has changed, clear the repeat times.
            if (_orginalSnapShortcutStatus.length > 0 && ![self.orginalSnapShortcutStatus isEqualToString:log.parameter1]) {
                self.repeatTimes = 0;
                self.orginalSnapShortcutStatus = log.parameter1;
            }
        } else {
            return;
        }
    }

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category == %@", kCZTimingEventCategory];
    NSArray *filteredTimingLogs = [CZUserInteractionLog MR_findAllSortedBy:@"time" ascending:YES withPredicate:predicate];
    for (NSUInteger i = 0; i < filteredTimingLogs.count - 1; i++) {
        CZUserInteractionLog *log1 = filteredTimingLogs[i];
        CZUserInteractionLog *log2 = filteredTimingLogs[i+1];
        
        NSString *event1 = log1.event;
        NSString *event2 = log2.event;
        if ([event1 isEqualToString:@"snap_button_pressed"]) {
            _snapButtonPressedTimes++;
            if ([event2 isEqualToString:log.event]) {
                i++;
                NSDate *date1 = log1.time;
                NSDate *date2 = log.time;
                NSTimeInterval interval = [date2 timeIntervalSinceDate:date1];
                const NSTimeInterval kSwitchTabInterval = 3.0;
                if (interval < kSwitchTabInterval) {
                    self.repeatTimes++;
                }
            }
        }
    }

    if (_snapButtonPressedTimes == 0) {
        self.repeatTimes = 0;
        return;
    }

    if (self.repeatTimes > 3) {
        NSString *isCurrentSnapShortcutModeOn = log.parameter1;
        BOOL snapShortcutModeOn = NO;
        if ([isCurrentSnapShortcutModeOn isEqualToString:@"YES"]) {
            snapShortcutModeOn = YES;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowSnapShortcutModeSwitchingMessage
                                                            object:nil
                                                          userInfo:@{@"enable_snap_shortcut_mode":@(!snapShortcutModeOn)}];
        self.orginalSnapShortcutStatus = isCurrentSnapShortcutModeOn;
        
        self.repeatTimes = 0;
        
    }
}

@end

@interface CZAnnotationDefaultSettingsAnalytics ()

@property (nonatomic, assign) NSUInteger strokeColorSettingTimes;
@property (nonatomic, assign) NSUInteger fillColorSettingTimes;
@property (nonatomic, assign) NSUInteger sizeSettingTimes;
@property (nonatomic, strong) NSString *strokeColor;
@property (nonatomic, strong) NSString *fillColor;
@property (nonatomic, strong) NSString *sizeString;

@end

@implementation CZAnnotationDefaultSettingsAnalytics

- (instancetype)init {
    self = [super init];
    if (self) {
        _strokeColorSettingTimes = 0;
        _fillColorSettingTimes = 0;
        _sizeSettingTimes = 0;
        _strokeColor = nil;
        _fillColor = nil;
        _sizeString = nil;
    }
    
    return self;
}

- (void)parseLog:(CZUserInteractionLog *)log {
    // If default setting color is the same as the setting color, no need to calculate repeat times
    NSString *category = log.category;
    if ([category isEqualToString:kCZUIActionEventCategory]) {
        NSString *event = log.event;
        if ([event isEqualToString:@"annotation_setting_button_pressed"]) {
            NSString *parameter1 = log.parameter1;
            NSString *key1 = log.key1;
            NSString *parameter2 = log.parameter2;
            NSString *key2 = log.key2;
            NSString *parameter3 = log.parameter3;
            NSString *key3 = log.key3;
            // TODO:consider the order of key
            if ([key2 isEqualToString:@"stroke_color"]) {
                if (parameter2.length > 0) {
                    if (self.strokeColor.length > 0) {
                        if ([self.strokeColor isEqualToString:parameter2]) {
                            _strokeColorSettingTimes ++;
                        } else {
                            self.strokeColor = parameter2;
                            _strokeColorSettingTimes = 1;
                        }
                    } else {
                        self.strokeColor = parameter2;
                        _strokeColorSettingTimes ++;
                    }
                }
            }
            if ([key1 isEqualToString:@"fill_color"]) {
                if (parameter1.length > 0) {
                    if (_fillColor.length > 0) {
                        if ([self.fillColor isEqualToString:parameter1]) {
                            _fillColorSettingTimes ++;
                        } else {
                            self.fillColor = parameter1;
                            _fillColorSettingTimes = 1;
                        }
                    } else {
                        self.fillColor = parameter1;
                        _fillColorSettingTimes ++;
                    }
                }
            }
            
            if ([key3 isEqualToString:@"size"]) {
                if (parameter3.length > 0) {
                    if (_sizeString.length > 0) {
                        if ([self.sizeString isEqualToString:parameter3]) {
                            _sizeSettingTimes ++;
                        } else {
                            self.sizeString = parameter3;
                            _sizeSettingTimes = 1;
                        }
                    } else {
                        self.sizeString = parameter3;
                        _sizeSettingTimes ++;
                    }
                }
            }
        }
        
        // Post notification to change the default setting stroke color
        NSString *strokeColor = @"";
        NSString *fillColor = @"";
        NSString *sizeString = @"";
        
        if (self.strokeColorSettingTimes > 3 && log.parameter2.length > 0) {
            strokeColor = self.strokeColor;
        }
        
        if (self.fillColorSettingTimes > 3 && log.parameter1.length > 0) {
            fillColor = self.fillColor;
        }
        
        if (self.sizeSettingTimes > 3 && log.parameter3.length > 0) {
            sizeString = self.sizeString;
        }
        
        if (strokeColor.length > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowDefaultSettingSwitchingMessage
                                                                object:nil
                                                              userInfo:@{@"stroke_color" : strokeColor,
                                                                         @"fill_color" : fillColor,
                                                                         @"size" : sizeString}];
            self.strokeColorSettingTimes = 0;
            self.strokeColor = nil;
        }
        
        if (fillColor.length > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowDefaultSettingSwitchingMessage
                                                                object:nil
                                                              userInfo:@{@"stroke_color" : strokeColor,
                                                                         @"fill_color" : fillColor,
                                                                         @"size" : sizeString}];
            self.fillColorSettingTimes = 0;
            self.fillColor = nil;
        }
        
        if (sizeString.length > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowDefaultSettingSwitchingMessage
                                                                object:nil
                                                              userInfo:@{@"stroke_color" : strokeColor,
                                                                         @"fill_color" : fillColor,
                                                                         @"size" : sizeString}];
            self.sizeSettingTimes = 0;
            self.sizeString = nil;
        }
    }
}

@end

@interface CZFileOverwriteOptionsAnalytics ()

@property (nonatomic, assign) NSUInteger yesOptionRepeatTimes;
@property (nonatomic, assign) NSUInteger noOptionRepeatTimes;

@end

@implementation CZFileOverwriteOptionsAnalytics

- (instancetype)init {
    self = [super init];
    if (self) {
        _yesOptionRepeatTimes = 0;
        _noOptionRepeatTimes = 0;
    }
    
    return self;
}

- (void)parseLog:(CZUserInteractionLog *)log {
    NSString *category = log.category;
    if ([category isEqualToString:kCZUIActionEventCategory]) {
        NSString *event = log.event;
        if ([event isEqualToString:@"file_save_overwrite_event"]) {
            NSString *parameter1 = log.parameter1;
            NSString *key1 = log.key1;
            
            BOOL isAskOnSaveOptionSelected = [key1 isEqualToString:@"is_ask_on_save_option_selected"];
            BOOL isYesButtonSelected = [parameter1 isEqualToString:@"YES"];
        
            if (isAskOnSaveOptionSelected) {
                if (isYesButtonSelected) {
                    _yesOptionRepeatTimes++;
                    _noOptionRepeatTimes = 0;
                } else  {
                    _noOptionRepeatTimes++;
                    _yesOptionRepeatTimes = 0;
                }
            }
        }
    }

    if (_noOptionRepeatTimes > 3) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kStopShowingAskOnSaveMessage
                                                            object:nil
                                                          userInfo:@{@"enable_overwrite" : @(NO)}];
        _noOptionRepeatTimes = 0;

    }
    
    if (_yesOptionRepeatTimes > 3) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kStopShowingAskOnSaveMessage
                                                            object:nil
                                                          userInfo:@{@"enable_overwrite" : @(YES)}];
        _yesOptionRepeatTimes = 0;

    }
}

@end
