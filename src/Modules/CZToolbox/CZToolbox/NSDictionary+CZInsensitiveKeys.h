//
//  NSDictionary+CZInsensitiveKeys.h
//  Hermes
//
//  Created by ZEISS on 2018/9/17.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (CZInsensitiveKeys)

- (id _Nullable)g_valueWithInsensitiveKeys:(NSString * _Nonnull)key;

@end
