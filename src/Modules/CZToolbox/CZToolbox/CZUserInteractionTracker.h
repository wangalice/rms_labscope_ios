//
//  CZUserInteractionTracker.h
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kCZEventLogChangedNotification;
extern NSString * const kCZUserInteractionLog;
extern NSString * const kCZUserInteractionLogCategory;
extern NSString * const kCZTimingEventCategory;
extern NSString * const kCZUIActionEventCategory;

@interface CZUserInteractionTracker : NSObject

+ (CZUserInteractionTracker *)defaultTracker;

// log user interation events
- (void)logEventWithCategory:(NSString *)category
                      event:(NSString *)event
                        time:(NSDate *)time
                  parameters:(NSDictionary *)parameters
                  postNotification:(BOOL)enabled;


@end
