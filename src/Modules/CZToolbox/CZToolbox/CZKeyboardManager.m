//
//  CZKeyboardManager.m
//  Common
//
//  Created by Li, Junlin on 7/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZKeyboardManager.h"

@implementation CZKeyboardTransition

- (instancetype)initFromUserInfo:(NSDictionary *)userInfo {
    self = [self init];
    if (self) {
        _frameBegin = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        _frameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        _animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
        _animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
        _isLocal = [userInfo[UIKeyboardIsLocalUserInfoKey] boolValue];
    }
    return self;
}

@end

@interface CZKeyboardManager ()

@property (nonatomic, retain) NSHashTable<id<CZKeyboardObserver>> *observers;

@end

@implementation CZKeyboardManager

+ (instancetype)sharedManager {
    static CZKeyboardManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CZKeyboardManager alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _observers = [[NSHashTable alloc] initWithOptions:NSPointerFunctionsWeakMemory capacity:1];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [_observers release];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [super dealloc];
}

- (void)addObserver:(id<CZKeyboardObserver>)observer {
    [self.observers addObject:observer];
}

- (void)removeObserver:(id<CZKeyboardObserver>)observer {
    [self.observers removeObject:observer];
}

- (void)keyboardWillShow:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardWillShow:)]) {
            [observer keyboardWillShow:transition];
        }
    }
    [transition release];
}

- (void)keyboardDidShow:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardDidShow:)]) {
            [observer keyboardDidShow:transition];
        }
    }
    [transition release];
}

- (void)keyboardWillHide:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardWillHide:)]) {
            [observer keyboardWillHide:transition];
        }
    }
    [transition release];
}

- (void)keyboardDidHide:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardDidHide:)]) {
            [observer keyboardDidHide:transition];
        }
    }
    [transition release];
}

- (void)keyboardWillChangeFrame:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardWillChangeFrame:)]) {
            [observer keyboardWillChangeFrame:transition];
        }
    }
    [transition release];
}

- (void)keyboardDidChangeFrame:(NSNotification *)note {
    CZKeyboardTransition *transition = [[CZKeyboardTransition alloc] initFromUserInfo:note.userInfo];
    for (id<CZKeyboardObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(keyboardDidChangeFrame:)]) {
            [observer keyboardDidChangeFrame:transition];
        }
    }
    [transition release];
}

@end
