//
//  CZCompoundAction.m
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCompoundAction.h"

@interface CZCompoundAction ()

@property (nonatomic, retain) NSMutableArray *subActions;

@end

@implementation CZCompoundAction

- (id)init {
    self = [super init];
    if (self) {
        _subActions = [[NSMutableArray alloc] init];
        _undoReversed = YES;
    }
    return self;
}

- (void)dealloc {
    [_subActions release];
    [super dealloc];
}

- (void)addAction:(id<CZUndoAction>)subAction {
    if (subAction) {
        [_subActions addObject:subAction];
    }
}

- (NSUInteger)count {
    return [_subActions count];
}

#pragma mark -
#pragma mark CZUndoAction

- (NSUInteger)costMemory {
    NSUInteger sum = 0;
    for (id subAction in self.subActions) {
        NSUInteger cost = [subAction costMemory];
        sum += cost;
    }
    return sum;
}

- (void)do {
    for (id subAction in self.subActions) {
        [subAction do];
    }
}

- (void)undo {
    NSEnumerator *enumerator = nil;
    if (self.undoReversed) {
        enumerator = [self.subActions reverseObjectEnumerator];
    } else {
        enumerator = [self.subActions objectEnumerator];
    }
    
    for (id subAction in enumerator) {
        [subAction undo];
    }
}

- (void)redo {
    for (id subAction in self.subActions) {
        [subAction redo];
    }
}

- (CZUndoActionType)type {
    CZUndoActionType combineType = 0;
    for (id<CZUndoAction> subAction in self.subActions) {
        combineType = combineType | [subAction type];
    }
    return (CZUndoActionType)combineType;
}

@end
