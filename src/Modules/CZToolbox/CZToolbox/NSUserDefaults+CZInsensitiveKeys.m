//
//  NSUserDefaults+CZInsensitiveKeys.m
//  Hermes
//
//  Created by ZEISS on 2018/9/17.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSUserDefaults+CZInsensitiveKeys.h"

@implementation NSUserDefaults (CZInsensitiveKeys)

- (id)g_valueFromUserDefaultsWithInsensitiveKeys:(NSString *)key {
    return [self g_valueFromUserDefaultsWithInsensitiveKeys:key
                                                  mixedKeys:@[key, [key lowercaseString], [key uppercaseString]]];
}

- (void)g_setValue:(id)value insensitiveKey:(NSString *)key {
    [self g_setValue:value
      insensitiveKey:key
           mixedKeys:@[key, [key lowercaseString], [key uppercaseString]]];
}

- (void)g_setValue:(id)value insensitiveKey:(NSString *)key mixedKeys:(NSArray <NSString *> *)keys {
    NSDictionary *dict = [[self class] g_valuesWithInsensitiveKeys:keys];
    if (dict.count > 0) {
        for (NSString *valueKey in [dict allKeys]) {
            if (![valueKey isEqualToString:key]) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:valueKey];
            }
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)g_valueFromUserDefaultsWithInsensitiveKeys:(NSString *)key mixedKeys:(NSArray <NSString *> *)keys {
    id result = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (result == nil || [result isEqual:[NSNull null]]) {
        for (NSString *mixedKey in keys) {
            result = [[NSUserDefaults standardUserDefaults] objectForKey:mixedKey];
            if (result) return result;
        }
    }
    return result;
}

+ (NSDictionary *)g_valuesWithInsensitiveKeys:(NSArray <NSString *> *)keys {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (NSString *key in keys) {
        id value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        if (value && ![value isEqual:[NSNull null]]) {
            [dict setValue:value forKey:key];
        }
    }
    return dict;
}

@end

