//
//  UIApplication+Name.h
//  Common
//
//  Created by Li, Junlin on 1/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Name)

@property (nonatomic, readonly, copy) NSString *cz_name;

@end
