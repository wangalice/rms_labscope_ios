//
//  CZLog.m
//  Hermes
//
//  Created by Li, Junlin on 11/20/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZLog.h"

@interface CZLogFormatter : NSObject <DDLogFormatter>

@property (nonatomic, retain) NSDateFormatter *dateFormatter;

@end

@implementation CZLogFormatter

- (void)dealloc {
    [_dateFormatter release];
    [super dealloc];
}

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {
    NSString *logLevel = nil;
    switch (logMessage.level) {
        case DDLogLevelVerbose:
            logLevel = @"ⓥ";
            break;
        case DDLogLevelDebug:
            logLevel = @"ⓓ";
            break;
        case DDLogLevelInfo:
            logLevel = @"ⓘ";
            break;
        case DDLogLevelWarning:
            logLevel = @"ⓦ";
            break;
        case DDLogLevelError:
            logLevel = @"ⓔ";
            break;
        default:
            break;
    }
    
    NSString *timestamp = [self.dateFormatter stringFromDate:logMessage.timestamp];
    NSString *file = logMessage.file.lastPathComponent;
    return [NSString stringWithFormat:@"%@ %@ %@:%ld %@ %@\n", timestamp, logLevel, file, (long)logMessage.line, logMessage.function, logMessage.message];
}

- (NSDateFormatter *)dateFormatter {
    if (_dateFormatter == nil) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"HH:mm:ss.SSS";
    }
    return _dateFormatter;
}

@end

@implementation DDLog (CZLog)

+ (void)load {
    CZLogFormatter *logFormatter = [[CZLogFormatter alloc] init];
    
    DDTTYLogger *ttyLogger = [DDTTYLogger sharedInstance];
    [ttyLogger setLogFormatter:logFormatter];
    [DDLog addLogger:ttyLogger];
    
#if DEBUG
    NSString *directory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).lastObject;
    DDLogFileManagerDefault *logFileManager = [[DDLogFileManagerDefault alloc] initWithLogsDirectory:directory];
    DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:logFileManager];
    [fileLogger setLogFormatter:logFormatter];
    [DDLog addLogger:fileLogger];
    [fileLogger release];
    [logFileManager release];
#endif
    
    [logFormatter release];
}

@end


