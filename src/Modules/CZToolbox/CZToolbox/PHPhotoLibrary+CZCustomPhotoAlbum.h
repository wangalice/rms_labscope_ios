//
//  PHPhotoLibrary+CZCustomPhotoAlbum.h
//  LabscopeiPhone
//
//  Created by Sherry Xu on 10/10/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

typedef void (^CZSaveImageCompletionBlock)(BOOL success, NSError *error);

@interface PHPhotoLibrary (CZCustomPhotoAlbum)

- (PHAssetCollection *)cz_albumWithName:(NSString *)albumName;

- (void)cz_saveImageFromData:(NSData *)imageData toAlbum:(NSString *)albumName withCompletionBlock:(CZSaveImageCompletionBlock)completionBlock;

@end
