//
//  UIApplication+Name.m
//  Common
//
//  Created by Li, Junlin on 1/15/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "UIApplication+Name.h"
#import "CZLocalizedString.h"

@implementation UIApplication (Name)

- (NSString *)cz_name {
    return L(@"ABOUT_PRODUCT_BIO_NAME");
}

@end
