//
//  NSObject+NSMultiKeyValueObserverRegistration.m
//  Hermes
//
//  Created by Halley Gu on 1/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "NSObject+NSMultiKeyValueObserverRegistration.h"

@implementation NSObject (NSMultiKeyValueObserverRegistration)

- (void)addObserver:(NSObject *)observer forKeyPaths:(NSString *)firstKey, ... {
    if (firstKey == nil) {
        return;
    }
    
    [self addObserver:observer
           forKeyPath:firstKey
              options:0
              context:NULL];
    
    NSString *arg = nil;
    va_list args;
    va_start(args, firstKey);
    while ((arg = va_arg(args, NSString*)) != nil) {
        [self addObserver:observer
               forKeyPath:arg
                  options:0
                  context:NULL];
    }
    va_end(args);
}

- (void)removeObserver:(NSObject *)observer forKeyPaths:(NSString *)firstKey, ...{
    if (firstKey == nil) {
        return;
    }
    
    [self removeObserver:observer
              forKeyPath:firstKey
                 context:NULL];
    
    NSString *arg = nil;
    va_list args;
    va_start(args, firstKey);
    while ((arg = va_arg(args, NSString*)) != nil) {
        [self removeObserver:observer
                  forKeyPath:arg
                     context:NULL];
    }
    va_end(args);
}

@end
