//
//  CZAssetsName.h
//  Matscope
//
//  Created by Mike Wang on 11/12/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef __cplusplus
extern "C" {
#endif
    
    NSString *A(NSString *imageURL);
    
#ifdef __cplusplus
}
#endif