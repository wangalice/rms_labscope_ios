//
//  NSObject+NSMultiKeyValueObserverRegistration.h
//  Hermes
//
//  Created by Halley Gu on 1/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSMultiKeyValueObserverRegistration)

- (void)addObserver:(NSObject *)observer forKeyPaths:(NSString *)firstKey, ... NS_REQUIRES_NIL_TERMINATION;
- (void)removeObserver:(NSObject *)observer forKeyPaths:(NSString *)firstKey, ... NS_REQUIRES_NIL_TERMINATION;

@end
