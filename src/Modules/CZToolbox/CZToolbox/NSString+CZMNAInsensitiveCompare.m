//
//  NSString+CZMNAInsensitiveCompare.m
//  Hermes
//
//  Created by Sun, Shaoge on 2018/9/13.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSString+CZMNAInsensitiveCompare.h"

@implementation NSString (CZMNAInsensitiveCompare)

- (BOOL)isEqualComparisionWithCaseInsensitive:(NSString * _Nullable)string {
    if ([NSString isStringNotNull:self] && [NSString isStringNotNull:string]) {
        NSComparisonResult comparisonResult = [self caseInsensitiveCompare:string];
        return comparisonResult == NSOrderedSame;
    }
    return NO;
}

+ (BOOL)isStringNotNull:(NSString * _Nullable)string {
    if (string != nil && ![string isEqual:[NSNull null]]) {
        return YES;
    }
    return NO;
}

@end
