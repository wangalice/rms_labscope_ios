//
//  CZCrashHandler.h
//  Matscope
//
//  Created by Ralph Jin on 12/30/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZCrashHandler : NSObject

+ (void)installExceptionHandler;

@end
