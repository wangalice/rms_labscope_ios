//
//  NSCharacterSet+StandardCharacterSets.m
//  Common
//
//  Created by Li, Junlin on 1/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "NSCharacterSet+StandardCharacterSets.h"

@implementation NSCharacterSet (StandardCharacterSets)

+ (NSCharacterSet *)cz_numericCharacterSet {
    static NSCharacterSet *numericCharacterSet = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableCharacterSet *characterSet = [[NSMutableCharacterSet alloc] init];
        
        NSRange characterRange;
        characterRange.location = (unsigned int)'0';
        characterRange.length = 10;
        [characterSet addCharactersInRange:characterRange];
        
        numericCharacterSet = [characterSet copy];
        
        [characterSet release];
    });
    return numericCharacterSet;
}

+ (NSCharacterSet *)cz_alphanumericCharacterSet {
    static NSCharacterSet *alphanumericCharacterSet = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableCharacterSet *characterSet = [[NSMutableCharacterSet alloc] init];
        
        NSRange characterRange;
        characterRange.location = (unsigned int)'0';
        characterRange.length = 10;
        [characterSet addCharactersInRange:characterRange];
        
        characterRange.location = (unsigned int)'a';
        characterRange.length = 26;
        [characterSet addCharactersInRange:characterRange];
        
        characterRange.location = (unsigned int)'A';
        characterRange.length = 26;
        [characterSet addCharactersInRange:characterRange];
        
        alphanumericCharacterSet = [characterSet copy];
        
        [characterSet release];
    });
    return alphanumericCharacterSet;
}

@end
