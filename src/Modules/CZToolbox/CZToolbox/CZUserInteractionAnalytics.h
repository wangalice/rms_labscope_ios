//
//  CZUserInteractionAnalytics.h
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kShowSnapShortcutModeSwitchingMessage;
extern NSString * const kShowDefaultSettingSwitchingMessage;
extern NSString * const kStopShowingAskOnSaveMessage;

@class CZUserInteractionLog;

// TODO:refactor it as a protocol
@interface CZUserInteractionAnalytics : NSObject

- (void)parseLog:(CZUserInteractionLog *)log;

@end

// TODO:separate these two sub-class into two files
@interface CZSnappingShortcutAnalytics : CZUserInteractionAnalytics

@end

@interface CZAnnotationDefaultSettingsAnalytics : CZUserInteractionAnalytics

@end

@interface CZFileOverwriteOptionsAnalytics : CZUserInteractionAnalytics

@end
