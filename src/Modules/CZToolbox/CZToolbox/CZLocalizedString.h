//
//  CZLocalizedString.h
//  Hermes
//
//  Created by Halley Gu on 8/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef __cplusplus
extern "C" {
#endif

NSString *L(NSString *translationKey);
    
/** get translated string from Settings.bundle. */
NSString *LR(NSString *translationKey);

#ifdef __cplusplus
}
#endif
