//
//  NSArray+Components.h
//  Common
//
//  Created by Li, Junlin on 1/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray<ObjectType> (Components)

- (NSArray *)cz_componentsJoinedByObject:(id)separator;
- (NSArray *)cz_componentsJoinedByObject:(id)separator omittingEmptyComponent:(BOOL)omittingEmptyComponent;

- (NSArray<NSArray<ObjectType> *> *)cz_componentsSeparatedByObject:(ObjectType)separator;
- (NSArray<NSArray<ObjectType> *> *)cz_componentsSeparatedByObject:(ObjectType)separator omittingEmptyComponent:(BOOL)omittingEmptyComponent;

@end
