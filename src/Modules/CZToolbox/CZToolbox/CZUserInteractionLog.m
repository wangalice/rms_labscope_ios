//
//  CZUserInteractionLog.m
//  Matscope
//
//  Created by Sherry Xu on 2/3/16.
//  Copyright © 2016 Carl Zeiss. All rights reserved.
//

#import "CZUserInteractionLog.h"

@implementation CZUserInteractionLog

// Insert code here to add functionality to your managed object subclass
@dynamic category;
@dynamic event;
@dynamic time;
@dynamic parameter1;
@dynamic parameter2;
@dynamic parameter3;
@dynamic key1;
@dynamic key2;
@dynamic key3;

@end
