//
//  NSArray+Repeating.m
//  Common
//
//  Created by Li, Junlin on 2/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "NSArray+Repeating.h"

@implementation NSArray (Repeating)

+ (instancetype)cz_arrayWithRepeatedObject:(id)object count:(NSUInteger)count {
    return [[[self alloc] cz_initWithRepeatedObject:object count:count] autorelease];
}

- (instancetype)cz_initWithRepeatedObject:(id)object count:(NSUInteger)count {
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:count];
    for (NSUInteger i = 0; i < count; i++) {
        [objects addObject:object];
    }
    return [self initWithArray:objects];
}

@end
