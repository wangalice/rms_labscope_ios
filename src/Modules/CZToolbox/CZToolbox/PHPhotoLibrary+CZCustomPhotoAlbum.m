//
//  PHPhotoLibrary+CZCustomPhotoAlbum.m
//  LabscopeiPhone
//
//  Created by Sherry Xu on 10/10/15.
//  Copyright © 2015 Carl Zeiss. All rights reserved.
//

#import "PHPhotoLibrary+CZCustomPhotoAlbum.h"

@implementation PHPhotoLibrary (CZCustomPhotoAlbum)

- (PHAssetCollection *)cz_albumWithName:(NSString *)albumName {
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"SELF.localizedTitle == %@", albumName];
    PHFetchResult<PHCollection *> *collections = [PHAssetCollection fetchTopLevelUserCollectionsWithOptions:fetchOptions];
    [fetchOptions release];
    
    PHAssetCollection *album = nil;
    
    for (PHCollection *collection in collections) {
        if ([collection isKindOfClass:[PHAssetCollection class]]) {
            album = (PHAssetCollection *)collection;
        }
    }
    
    return album;
}

- (void)cz_saveImageFromData:(NSData *)imageData toAlbum:(NSString *)albumName withCompletionBlock:(CZSaveImageCompletionBlock)completionBlock {
    [self performChanges:^{
        PHAssetCreationRequest *request = [PHAssetCreationRequest creationRequestForAsset];
        [request addResourceWithType:PHAssetResourceTypePhoto data:imageData options:nil];
        PHObjectPlaceholder *placeholder = request.placeholderForCreatedAsset;
        [self cz_addAssetPlaceholder:placeholder toAlbum:albumName];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (completionBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(success, error);
            });
        }
    }];
}

- (void)cz_addAssetPlaceholder:(PHObjectPlaceholder *)assetPlaceholder toAlbum:(NSString *)albumName {
    if (albumName == nil) {
        return;
    }
    
    PHAssetCollection *album = [self cz_albumWithName:albumName];
    
    if (album) {
        PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:album];
        [albumChangeRequest addAssets:@[assetPlaceholder]];
    } else {
        PHAssetCollectionChangeRequest *albumCreateRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
        [albumCreateRequest addAssets:@[assetPlaceholder]];
    }
}

@end
