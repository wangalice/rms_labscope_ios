//
//  CZBufferProcessor.m
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZBufferProcessor.h"

@interface CZBufferProcessor () {
    const void *_buffer;
    uint64_t _length;
    uint64_t _offset;
    NSMutableData *_mutableData;
}

@end

@implementation CZBufferProcessor

- (id)init {
    [self release];
    return nil;
}

- (id)initWithBuffer:(const void *)buffer length:(uint64_t)length {
    if (buffer == NULL || length == 0) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _buffer = buffer;
        _length = length;
        _offset = 0;
    }
    return self;
}

- (id)initWithMutableData:(NSMutableData *)data {
    if (data == NULL) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _mutableData = [data retain];
        _buffer = [data mutableBytes];
        _length = [data length];
        _offset = 0;
    }
    return self;
}

- (void)dealloc {
    [_mutableData release];
    [super dealloc];
}

- (BOOL)seekToOffset:(uint64_t)offset {
    [self validateOffset:offset];
    
    if (offset > _length) {
        return NO;
    }
    
    _offset = offset;
    return YES;
}

- (BOOL)jumpByLength:(uint64_t)length {
    return [self seekToOffset:(_offset + length)];
}

- (uint64_t)offset {
    return _offset;
}

- (uint8_t)readByte {
    uint8_t value = *(uint8_t *)(_buffer + _offset);
    ++_offset;
    return value;
}

- (uint16_t)readShort {
    uint16_t value = *(uint16_t *)(_buffer + _offset);
    _offset += sizeof(uint16_t);
    return value;
}

- (uint16_t)readShortInHostOrder {
    uint16_t value = *(uint16_t *)(_buffer + _offset);
    _offset += sizeof(uint16_t);
    return ntohs(value);
}

- (uint32_t)readLong {
    uint32_t value = *(uint32_t *)(_buffer + _offset);
    _offset += sizeof(uint32_t);
    return value;
}

- (uint32_t)readLongInHostOrder {
    uint32_t value = *(uint32_t *)(_buffer + _offset);
    _offset += sizeof(uint32_t);
    return ntohl(value);
}

- (int64_t)readLongLong {
    int64_t value;
    [self readBytes:&value length:sizeof(uint64_t)];
    return value;
}

- (NSString *)readUTF8StringInRange:(NSRange)range {
    const char *p = (const char *)(_buffer + range.location);
    NSString *string = [[NSString alloc] initWithBytes:p length:range.length encoding:NSUTF8StringEncoding];
    return [string autorelease];
}

- (NSString *)readStringWithLength:(size_t)length {
    const char *p = (const char *)(_buffer + _offset);
    _offset += length;
    NSUInteger actualLength = strnlen(p, length);
    return [[[NSString alloc] initWithBytes:p length:actualLength encoding:NSASCIIStringEncoding] autorelease];
}

- (NSString *)readIpAddressString {
    uint32_t value = [self readLong];
    uint8_t *p = (uint8_t *)(&value);
    return [[[NSString alloc] initWithFormat:@"%u.%u.%u.%u", p[0], p[1], p[2], p[3]] autorelease];
}

- (double)readDouble {
    _Static_assert(sizeof(double) == 8, "assert double will read 8 bytes.");
    double value;
    [self readBytes:&value length:sizeof(double)];
    return value;
}

- (void)readBytes:(void *)bytes length:(uint64_t)length {
    uint64_t remain = _length - _offset;
    length = MIN(remain, length);
    
    const void *p = _buffer + _offset;
    memcpy(bytes, p, length);
    _offset += length;
}

- (void)writeByte:(uint8_t)data {
    [self validateOffset:_offset + 1];
    
    uint8_t *p = (uint8_t *)(_buffer + _offset);
    *p = data;
    ++_offset;
}

- (void)writeShort:(uint16_t)data {
    [self validateOffset:_offset + sizeof(uint16_t)];
    
    uint16_t *p = (uint16_t *)(_buffer + _offset);
    *p = data;
    _offset += sizeof(uint16_t);
}

- (void)writeShortInNetworkOrder:(uint16_t)data {
    [self validateOffset:_offset + sizeof(uint16_t)];
    
    uint16_t *p = (uint16_t *)(_buffer + _offset);
    *p = htons(data);
    _offset += sizeof(uint16_t);
}

- (void)writeLong:(uint32_t)data {
    [self validateOffset:_offset + sizeof(uint32_t)];
    
    uint32_t *p = (uint32_t *)(_buffer + _offset);
    *p = data;
    _offset += sizeof(uint32_t);
}

- (void)writeLongInNetworkOrder:(uint32_t)data {
    [self validateOffset:_offset + sizeof(uint32_t)];
    
    uint32_t *p = (uint32_t *)(_buffer + _offset);
    *p = htonl(data);
    _offset += sizeof(uint32_t);
}

- (void)writeLongLong:(int64_t)data {
    [self writeBytes:&data length:sizeof(int64_t)];
}

- (void)writeDouble:(double)data {
    _Static_assert(sizeof(double) == 8, "assert double will write 8 bytes.");
    [self writeBytes:&data length:sizeof(double)];
}

- (void)writeBytes:(const void *)bytes length:(uint64_t)length {
    [self validateOffset:_offset + length];
    
    void *p = (void *)(_buffer + _offset);
    memcpy(p, bytes, length);
    _offset += length;
}

- (void)writeString:(const char *)str {
    if (!str) {
        return;
    }
    
    int index = 0;
    while (str[index] != 0) {
        [self writeByte:str[index++]];
    }
}

- (CZBufferProcessor *)newSubBufferReaderWithLength:(NSUInteger)length {
    return [[CZBufferProcessor alloc] initWithBuffer:(_buffer + _offset) length:length];
}

#pragma mark - Privates

- (void)validateOffset:(NSUInteger)offset {
    if (_mutableData == nil) {
        return;
    }
    
    if ([_mutableData length] < offset) {
        [_mutableData setLength:offset];
        _buffer = [_mutableData mutableBytes];
        _length = [_mutableData length];
        assert(_length == offset);
    }
}

@end
