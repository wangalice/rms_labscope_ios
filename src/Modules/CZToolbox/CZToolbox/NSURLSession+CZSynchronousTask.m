//
//  NSURLSession+CZSynchronousTask.m
//  Hermes
//
//  Created by Li, Junlin on 11/13/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "NSURLSession+CZSynchronousTask.h"

@implementation CZURLSessionTaskResult

- (instancetype)init {
    return [self initWithData:nil response:nil error:nil];
}

- (instancetype)initWithData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    self = [super init];
    if (self) {
        _data = [data retain];
        _response = [response retain];
        _error = [error retain];
    }
    return self;
}

- (void)dealloc {
    [_data release];
    [_response release];
    [_error release];
    [super dealloc];
}

@end

@implementation NSURLSession (CZSynchronousTask)

- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithURL:(NSURL *)url {
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    return [self cz_startSynchronousDataTaskWithRequest:request];
}

- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithURL:(NSURL *)url timeoutInterval:(NSTimeInterval)timeoutInterval {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = timeoutInterval;
    return [self cz_startSynchronousDataTaskWithRequest:request];
}

- (CZURLSessionTaskResult *)cz_startSynchronousDataTaskWithRequest:(NSURLRequest *)request {
    __block NSData *sessionData = nil;
    __block NSURLResponse *sessionResponse = nil;
    __block NSError *sessionError = nil;
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [[self dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        sessionData = [data retain];
        sessionResponse = [response retain];
        sessionError = [error retain];
        dispatch_semaphore_signal(semaphore);
    }] resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    dispatch_release(semaphore);
    
    CZURLSessionTaskResult *result = [[CZURLSessionTaskResult alloc] initWithData:[sessionData autorelease]
                                                                         response:[sessionResponse autorelease]
                                                                            error:[sessionError autorelease]];
    return [result autorelease];
}

@end
