//
//  CZLog.h
//  Hermes
//
//  Created by Li, Junlin on 11/20/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#define LOG_LEVEL_DEF ddLogLevel
#import <CocoaLumberjack/CocoaLumberjack.h>

#if DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelWarning;
#endif

#define CZLogv(format, ...) DDLogVerbose(format, ##__VA_ARGS__)
#define CZLogd(format, ...) DDLogDebug(format, ##__VA_ARGS__)
#define CZLogi(format, ...) DDLogInfo(format, ##__VA_ARGS__)
#define CZLogw(format, ...) DDLogWarn(format, ##__VA_ARGS__)
#define CZLoge(format, ...) DDLogError(format, ##__VA_ARGS__)

@interface DDLog (CZLog)

@end
