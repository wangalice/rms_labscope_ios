//
//  CZKeyboardManager.h
//  Common
//
//  Created by Li, Junlin on 7/8/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZKeyboardTransition : NSObject

@property (nonatomic, readonly, assign) CGRect frameBegin;
@property (nonatomic, readonly, assign) CGRect frameEnd;
@property (nonatomic, readonly, assign) NSTimeInterval animationDuration;
@property (nonatomic, readonly, assign) UIViewAnimationOptions animationCurve;
@property (nonatomic, readonly, assign) BOOL isLocal;

@end

@protocol CZKeyboardObserver <NSObject>

@optional
- (void)keyboardWillShow:(CZKeyboardTransition *)transition;
- (void)keyboardDidShow:(CZKeyboardTransition *)transition;
- (void)keyboardWillHide:(CZKeyboardTransition *)transition;
- (void)keyboardDidHide:(CZKeyboardTransition *)transition;
- (void)keyboardWillChangeFrame:(CZKeyboardTransition *)transition;
- (void)keyboardDidChangeFrame:(CZKeyboardTransition *)transition;

@end

@interface CZKeyboardManager : NSObject

+ (instancetype)sharedManager;

- (void)addObserver:(id<CZKeyboardObserver>)observer;
- (void)removeObserver:(id<CZKeyboardObserver>)observer;

@end
