//
//  NSArray+Repeating.h
//  Common
//
//  Created by Li, Junlin on 2/1/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray<ObjectType> (Repeating)

+ (instancetype)cz_arrayWithRepeatedObject:(ObjectType)object count:(NSUInteger)count;
- (instancetype)cz_initWithRepeatedObject:(ObjectType)object count:(NSUInteger)count;

@end
