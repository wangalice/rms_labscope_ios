//
//  CZCommonUtils.h
//  Hermes
//
//  Created by Mike Wang on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kBackupFileExtension @"backup"
#define kBackupFileName @"data.backup"
#define kFileExtensionCZI @"czi"
#define kFileExtensionJPEG @"jpg"
#define kFileExtensionTIF @"tif"

typedef NS_ENUM(NSUInteger, CZFileFormat) {
    kCZFileFormatNotSupported,
    kCZFileFormatCZI,
    kCZFileFormatJPEG,
    kCZFileFormatPNG,
    kCZFileFormatTIF,
    kCZFileFormatMP4,
    kCZFileFormatPDF,
    kCZFileFormatRTF,
    kCZFileFormatCSV,
    kCZFileFormatCZRTJ,
    kCZFileFormatCZFTP,
    kCZFileFormatCZAMT
};

typedef NS_ENUM(NSUInteger, CZFileFormatType) {
    kCZFileFormatTypeNotSupported,
    kCZFileFormatTypeImage,
    kCZFileFormatTypeVideo,
    kCZFileFormatTypeReport,
    kCZFileFormatTypeReportTemplate,
    kCZFileFormatTypeFileNameTemplate,
    kCZFileFormatTypeAdvanceMeasurementsTemplate
};

typedef NS_ENUM(NSUInteger, CZDeviceModel) {
    CZDeviceModelOthers,
    CZDeviceModeliPad2,
    CZDeviceModeliPad3,
    CZDeviceModeliPad4,
    CZDeviceModeliPadAir,
    CZDeviceModeliPadAir2,
    CZDeviceModeliPadMini1,
    CZDeviceModeliPadMini2,
    CZDeviceModeliPadMini3
};

#define kStringDefaultSize 128

/**
 * Calculate Otsu threshold of image
 * @param hist histogram of image
 * @param total total pixels count of image
 */
extern float otsuThreshold(int hist[], long total);

@interface CZCommonUtils : NSObject

+ (CGFloat) distanceBetweenPoints:(CGPoint)a Point:(CGPoint)b;

+ (CGPoint)viewPointToImagePoint:(CGPoint)point;
+ (CGPoint)imagePointToViewPoint:(CGPoint)point;

+ (CGRect)viewRectToImageRect:(CGRect)rect;
+ (CGRect)imageRectToViewRect:(CGRect)rect;

+ (CGSize)viewSizeToImageSize:(CGSize)size;
+ (CGSize)imageSizeToViewSize:(CGSize)size;

+ (CGRect)calcActualBoxByBoundBox:(CGRect)boundBox
                      contentSize:(CGSize)contentSize;

+ (UIImage *)blankImageWithSize:(CGSize)size;
+ (UIImage *)cropImageToAspectRatio:(CGFloat)ratio
                         imageSource:(UIImage *)source;
+ (UIImage *)scaleImage:(UIImage *)source intoSize:(CGSize)maxSize;
+ (UIImage *)scaleImage:(UIImage *)source intoSize:(CGSize)maxSize fillColor:(UIColor *)fillColor;
+ (UIImage *)grayImageFromImage:(UIImage *)source;
+ (UIImage *)gray16ImageFromImage:(UIImage *)source;
+ (UIImage *)imageFromGrayImage:(UIImage *)grayImage;
+ (UIImage *)newDecompressedImageFromJPEGImage:(UIImage *)originalImage;
+ (UIImage *)imageFromYUV400Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height;
// This method is for the specific image format: YYYYYYYYUVUV, normal is for the kappa, wision camera snap
+ (UIImage *)imageFromYUV420spNV12Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height;
// This method is for the specific image format: YYYYYYYYVUVU, normal is for the gemini camera snap
+ (UIImage *)imageFromYUV420spNV21Buffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height;
+ (UIImage *)imageFromGrayBayerBuffer:(const void *)buffer length:(size_t)bufferLength width:(int)width height:(int)height;

+ (UIImage *)imageFromLayer:(CALayer *)layer;

+ (CGSize)imageSizeFromFileURL:(NSURL *)fileURL;
+ (CGSize)imageSizeFromFilePath:(NSString *)filePath;

+ (UIImage *)thumnailFromImageFileURL:(NSURL *)fileURL atMinSize:(CGFloat)minSize;
+ (UIImage *)thumnailFromImageFilePath:(NSString *)filePath atMinSize:(CGFloat)minSize;

+ (BOOL)numericTextField:(UITextField *)textField shouldAcceptChange:(NSString *)textEntered;
+ (BOOL)numericPINField:(UITextField *)textField shouldAcceptChange:(NSString *)textEntered;
+ (NSNumber *)numberFromLocalizedString:(NSString *)string;
+ (NSString *)stringFromNumber:(NSNumber *)number precision:(NSUInteger)precision;
+ (NSString *)stringFromFloat:(CGFloat)number precision:(NSUInteger)precision;
+ (NSString *)localizedStringFromNumber:(NSNumber *)number precision:(NSUInteger)precision;
+ (NSString *)localizedStringFromFloat:(double)number precision:(NSUInteger)precision;
+ (float)floatFromFloat:(float)number precision:(NSUInteger)precision;
+ (double)doubleFromDouble:(double)number precision:(NSUInteger)precision;

+ (CZFileFormat)fileFormatForExtension:(NSString *)extension;
+ (CZFileFormatType)fileFormatTypeForExtension:(NSString *)extension;
+ (NSString *)mimeTypeStringForExtension:(NSString *)extension;

+ (BOOL)isStringValidFileName:(NSString *)string;
+ (BOOL)isStringValidFileNameTemplate:(NSString *)string;

+ (NSMutableArray *)newLocalFiles;

+ (void)mergeArray:(NSMutableArray *)dic1 with:(NSArray *)dic2;

+ (uint64_t)freeSpaceInBytes;
+ (float)freeSpacePercentage;
+ (NSString *)freeSpaceString;

+ (NSString *)localIpAddress;
+ (NSString *)localSubnetMask;
+ (NSString *)localHostName;
+ (NSString *)andOperationIPAddress:(NSString *)ipAddress
                         subnetMask:(NSString *)subnetMask;
+ (BOOL)isIPAddress:(NSString*)ip;

+ (CZDeviceModel)deviceModel;
/** @return 0, if it is not an iPad. otherwise 501, if it's model name is @"iPad5,1".*/
+ (NSUInteger)iPadModelNumber;

+ (NSString *)md5FromString:(NSString *)inputString;
+ (NSString *)md5FromContentOfFile:(NSString *)filePath;
+ (NSString *)md5FromData:(NSData *)data;

+ (BOOL)isMacAddressFromCarlZeissChina:(NSString *)macAddress;

+ (void)updateThemeForView:(UIView *)view withColor:(UIColor *)color;

+ (BOOL)isOperatingSystemAtIOS9;
+ (BOOL)isRunningOnIPhone;
+ (BOOL)isRunningOnIPhoneXSeries;

@end

@interface UIImage (CommonUtils)

- (UIImage *)imageByRotatingToUp;

@end
