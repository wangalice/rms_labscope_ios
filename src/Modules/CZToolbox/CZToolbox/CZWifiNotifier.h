//
//  CZWifiNotifier.h
//  Hermes
//
//  Created by Ralph Jin on 6/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kCZWifiChangedNotification;

@interface CZWifiNotifier : NSObject

+ (CZWifiNotifier *)sharedInstance;

+ (NSString *)copyOriginalSSID;
+ (NSString *)copySSID;

- (BOOL)startNotifier;
- (void)stopNotifier;

/** post notification via notification center that wifi changed.*/
- (void)postNotification;

- (BOOL)isReachable;

@end
