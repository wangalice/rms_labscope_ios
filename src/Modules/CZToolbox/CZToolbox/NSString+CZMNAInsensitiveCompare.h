//
//  NSString+CZMNAInsensitiveCompare.h
//  Hermes
//
//  Created by Sun, Shaoge on 2018/9/13.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CZMNAInsensitiveCompare)

- (BOOL)isEqualComparisionWithCaseInsensitive:(NSString * _Nullable)string;

@end
