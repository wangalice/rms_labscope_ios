//
//  NSArray+Components.m
//  Common
//
//  Created by Li, Junlin on 1/14/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "NSArray+Components.h"

@implementation NSArray (Components)

- (NSArray *)cz_componentsJoinedByObject:(id)separator {
    return [self cz_componentsJoinedByObject:separator omittingEmptyComponent:YES];
}

- (NSArray *)cz_componentsJoinedByObject:(id)separator omittingEmptyComponent:(BOOL)omittingEmptyComponent {
    NSMutableArray *components = [NSMutableArray array];
    
    [self enumerateObjectsUsingBlock:^(NSArray *component, NSUInteger index, BOOL *stop) {
        if (omittingEmptyComponent == NO || component.count > 0) {
            [components addObjectsFromArray:component];
            if (index != self.count - 1) {
                [components addObject:separator];
            }
        }
    }];
    
    return [[components copy] autorelease];
}

- (NSArray *)cz_componentsSeparatedByObject:(id)separator {
    return [self cz_componentsSeparatedByObject:separator omittingEmptyComponent:YES];
}

- (NSArray *)cz_componentsSeparatedByObject:(id)separator omittingEmptyComponent:(BOOL)omittingEmptyComponent {
    NSMutableArray<NSArray *> *components = [NSMutableArray array];
    
    NSMutableArray *component = [NSMutableArray array];
    [self enumerateObjectsUsingBlock:^(id object, NSUInteger index, BOOL *stop) {
        if ([object isEqual:separator]) {
            if (component.count > 0 || omittingEmptyComponent == NO) {
                [components addObject:[[component copy] autorelease]];
                [component removeAllObjects];
            }
            if (index == self.count - 1 && omittingEmptyComponent == NO) {
                [components addObject:[[component copy] autorelease]];
            }
        } else {
            [component addObject:object];
            if (index == self.count - 1) {
                [components addObject:[[component copy] autorelease]];
                [component removeAllObjects];
            }
        }
    }];
    
    return [[components copy] autorelease];
}

@end
