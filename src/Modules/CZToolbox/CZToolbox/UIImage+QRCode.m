//
//  UIImage+QRCode.m
//  Matscope
//
//  Created by Ralph Jin on 9/16/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "UIImage+QRCode.h"

@implementation UIImage(QRCode)

+ (UIImage *)qrCodeImageWithMessage:(NSString *)message {
    return [self qrCodeImageWithMessage:message icon:nil];
}

+ (UIImage *)qrCodeImageWithMessage:(NSString *)message icon:(UIImage *)icon {
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setDefaults];
    
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    [filter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *outputImage = [filter outputImage];
    CGSize imageSize = outputImage.extent.size;
    const CGFloat kMaxSize = 500;
    CGFloat kScaleTime = floorf(kMaxSize / imageSize.width);
    kScaleTime = MIN(5, kScaleTime);
    imageSize.height *= kScaleTime;  // Scale by at most 5 times along both dimensions
    imageSize.width *= kScaleTime;
    
    CGFloat scale = [[UIScreen mainScreen] scale];
    UIImage *image = [UIImage imageWithCIImage:outputImage scale:scale orientation:UIImageOrientationUp];
    
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    
    [image drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    
    if (icon) {
        [[UIColor whiteColor] setFill];
        CGRect centerRect = CGRectMake(imageSize.width * 0.4, imageSize.height * 0.4,
                                       imageSize.width * 0.2, imageSize.height * 0.2);
        UIBezierPath *fillPath = [UIBezierPath bezierPathWithRoundedRect:centerRect cornerRadius:16];
        [fillPath fill];

        centerRect = CGRectInset(centerRect, 10, 10);
        CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
        [icon drawInRect:centerRect];
    }
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
