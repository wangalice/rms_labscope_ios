//
//  CZVideoEngineTests.m
//  CZVideoEngineTests
//
//  Created by Li, Junlin on 7/24/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CZVideoEngineTests : XCTestCase

@end

@implementation CZVideoEngineTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureWithMetrics:@[[[XCTCPUMetric alloc] init]] block:^{
        // Put the code whose CPU performance you want to measure here.
    }];
}

@end
