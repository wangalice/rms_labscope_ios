//
//  CZMultiFramedRTPSource.h
//  Matscope iPhone
//
//  Created by Ralph Jin on 6/1/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>

#ifdef __cplusplus
class CZRTSPFrameConsuming;
#else
@class CZRTSPFrameConsuming;
#endif

@interface CZMultiFramedRTPSource : NSObject <GCDAsyncUdpSocketDelegate> {
    uint32_t _SSRC;
    int32_t _beginFrameSeqNo;
    int32_t _endFrameSeqNo;
}

@property (nonatomic, assign) CZRTSPFrameConsuming *delegate;
@property (nonatomic, retain) GCDAsyncUdpSocket *socket;
@property (nonatomic, copy) NSString *ipAddress;
@property (nonatomic, assign, readonly) uint16_t bindingPorts;

@property (nonatomic, retain) NSMutableDictionary *cachedPackets;

- (void)start;
- (void)stop;

@end
