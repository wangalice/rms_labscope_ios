//
//  CZHardwareDecoder.h
//  Hermes
//
//  Created by Halley Gu on 4/19/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CZHardwareDecoder;

@protocol CZHardwareDecoderDelegate <NSObject>

@optional
- (void)hardwareDecoder:(id<CZHardwareDecoder>)decoder didGetFrame:(UIImage *)frame;
- (void)hardwareDecoderDidFinishDecodingVideoSegment:(id<CZHardwareDecoder>)decoder;

@end

@protocol CZHardwareDecoder <NSObject>
@required

@property (atomic, assign) id<CZHardwareDecoderDelegate> delegate;
@property (atomic, retain) UIImage *lastDecodedFrame;
@property (atomic, assign, readonly, getter = isPaused) BOOL paused;

- (void)start;
- (void)stop;

- (void)pause;
- (void)resume;

- (void)decodeVideoSegment:(id)segment;

@end

@interface CZHardwareFileDecoder : NSObject <CZHardwareDecoder>

@end

@interface CZHardwareStreamDecoder : NSObject <CZHardwareDecoder>

@end
