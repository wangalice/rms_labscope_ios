//
//  CZVideoSegment.h
//  Hermes
//
//  Created by Halley Gu on 9/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger,CZVideoSegmentStatus)  {
    kCZVideoSegmentIdle = 0,
    kCZVideoSegmentWriting = 1,
    kCZVideoSegmentReadyForReading = 2,
    kCZVideoSegmentReading = 3,
    kCZVideoSegmentFinishedReading = 4
};

@protocol CZVideoSegment <NSObject>

@property (atomic, assign, readonly) CZVideoSegmentStatus status;

- (NSURL *)url;

/**
 * @return YES, if switch successfully; otherwise NO.
 */
- (BOOL)switchToStatus:(CZVideoSegmentStatus)status;

@end

@interface CZMemoryVideoSegment : NSObject<CZVideoSegment>

- (NSString *)path;

- (void)clear;

@end

@interface CZFileVideoSegment : NSObject<CZVideoSegment>

- (instancetype)init NS_UNAVAILABLE;
- (id)initWithURL:(NSURL *)url;

@end
