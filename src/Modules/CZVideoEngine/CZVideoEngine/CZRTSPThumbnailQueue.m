//
//  CZRTSPThumbnailQueue.m
//  Matscope
//
//  Created by Ralph Jin on 10/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRTSPThumbnailQueue.h"

#import "CZHardwareDecoder.h"
#import "CZRTSPClient.h"
#import <CZToolbox/CZToolbox.h>

const static NSUInteger kMaxProcessingCount = 1;
//const static float kDelayTime = 0.15;

@interface CZRTSPThumbnailAcquirerWrapper : NSObject <CZHardwareDecoderDelegate, CZRTSPClientDelegate>

@property (nonatomic, assign) CZRTSPThumbnailQueue *owner;
@property (atomic, retain) id<CZRTSPThumbnailAcquirer> acquirer;
@property (nonatomic, retain) id<CZHardwareDecoder> decoder;
@property (nonatomic, retain) CZRTSPClient *client;

@end

@interface CZRTSPThumbnailQueue ()

@property (nonatomic, retain, readonly) NSMutableArray *penddingQueue;
@property (nonatomic, retain, readonly) NSMutableArray *processingQueue;

- (void)stopWrapper:(CZRTSPThumbnailAcquirerWrapper *)wrapper;

@end

@implementation CZRTSPThumbnailAcquirerWrapper

+ (dispatch_queue_t)sharedThreadQueue {
    static dispatch_queue_t threadQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        threadQueue = dispatch_queue_create("com.zeisscn.camera.thumbnail.updating", DISPATCH_QUEUE_SERIAL);
    });
    return threadQueue;
}

- (void)dealloc {
    [_acquirer release];
    [_decoder release];
    [_client release];
    [super dealloc];
}

- (void)start {
    if (self.acquirer && _decoder == nil && _client == nil) {
        dispatch_async([CZRTSPThumbnailAcquirerWrapper sharedThreadQueue], ^ {  // start RTSP client in queue
            // create client
            _client = [[CZRTSPClient alloc] initWithURL:[self.acquirer rtspUrl]];
            [_client setDelegate:self];
            
            // create decoder
            _decoder = [_client needsFileWrapping] ? [[CZHardwareFileDecoder alloc] init] : [[CZHardwareStreamDecoder alloc] init];
            [_decoder setDelegate:self];
            
            // play
            [_client setIsForSingleFrame:YES];
            [_client setResolution:[self.acquirer liveResolution]];
            
            double delayInSeconds = 3.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
    #ifdef _DEBUG
                @synchronized (self) {
                    if (self.acquirer) {
                        CZLogv(@"CZRTSPThumbnailUpdater: %@ is timeout!", [self.acquirer rtspUrl]);
                    }
                }
    #endif
                
                [self stop];
            });
            
            @synchronized (self) {
                [_decoder start];
                [_client play];
                
//                usleep(kDelayTime * USEC_PER_SEC);
                
//                CZLogv(@"CZRTSPThumbnailUpdater: %@ start play RTSP client.", [self.acquirer rtspUrl]);
            }
        });
    }
}

- (void)rtspClient:(CZRTSPClient *)client didGetVideoSegment:(id)segment {
    @synchronized (self) {
        if (self.client == client) {
            if (segment) {
                [self.decoder decodeVideoSegment:segment];
            } else {
                CZLogv(@"CZRTSPThumbnailUpdater: %@ failed to get segment.", [self.acquirer rtspUrl]);
                // Returning nil for RTSP client means that the RTSP stream is
                // not available. Should close client in this case.
                [self stop];
            }
        }
    }
}

#pragma mark - CZHardwareDecoderDelegate methods

- (void)hardwareDecoder:(id<CZHardwareDecoder>)decoder didGetFrame:(UIImage *)frame {
    @synchronized (self) {
        if (self.decoder == decoder) {
//            CZLogv(@"CZRTSPThumbnailUpdater: %@ success to get thumbnail.", [self.acquirer rtspUrl]);
            if ([self.acquirer respondsToSelector:@selector(thumbnailQueue:didGetThumbnail:)]) {
                [self.acquirer thumbnailQueue:self.owner didGetThumbnail:frame];
            }
            [self stop];
        }
    }
}

- (void)stop {
    dispatch_async([CZRTSPThumbnailAcquirerWrapper sharedThreadQueue], ^ {  // stop objects in queue
        CZRTSPClient *clientToClose = nil;
        id<CZHardwareDecoder> decoderToClose = nil;
        @synchronized (self) {
            if (self.acquirer || self.client || self.decoder) {
                clientToClose = [self.client retain];
                clientToClose.delegate = nil;
                
                decoderToClose = [self.decoder retain];
                decoderToClose.delegate = nil;
                
                self.decoder = nil;
                self.client = nil;
                
                self.acquirer = nil;
                [self.owner stopWrapper:self];
                
//                usleep(kDelayTime * USEC_PER_SEC);
            }
        }
        
        [self stopObject:clientToClose];
        [clientToClose release];
        
        [decoderToClose stop];
        [decoderToClose release];        
    });
}

- (void)stopObject:(id)object {
    if ([object respondsToSelector:@selector(stop)]) {
        [object stop];
    }
    
    if ([object respondsToSelector:@selector(setDelegate:)]) {
        [object setDelegate:nil];
    }
}

@end

@implementation CZRTSPThumbnailQueue

+ (CZRTSPThumbnailQueue *)sharedInstance {
    static CZRTSPThumbnailQueue *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^ {
        instance = [[CZRTSPThumbnailQueue alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _penddingQueue = [[NSMutableArray alloc] init];
        _processingQueue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_processingQueue release];
    [_penddingQueue release];
    [super dealloc];
}

- (void)appendAcquirer:(id<CZRTSPThumbnailAcquirer>)acquirer {
    if (acquirer == nil) {
        return;
    }

    @synchronized (self) {
        BOOL found = NO;
        for (CZRTSPThumbnailAcquirerWrapper *wrapper in self.penddingQueue) {
            if (wrapper.acquirer == acquirer) {
                found = YES;  // already in queue
                break;
            }
        }
        
        if (!found) {
            for (CZRTSPThumbnailAcquirerWrapper *wrapper in self.processingQueue) {
                if (wrapper.acquirer == acquirer) {
                    found = YES;  // already in queue
                    break;
                }
            }
        }
        
        if (!found) {
            CZRTSPThumbnailAcquirerWrapper *wrapper = [[CZRTSPThumbnailAcquirerWrapper alloc] init];
            wrapper.acquirer = acquirer;
            wrapper.owner = self;
            [self.penddingQueue addObject:wrapper];
            [wrapper release];
        }
    }
    
    [self process];
}

- (void)removeAcquirer:(id<CZRTSPThumbnailAcquirer>)acquirer {
    @synchronized (self) {
        NSUInteger i = 0;
        for (CZRTSPThumbnailAcquirerWrapper *wrapper in self.penddingQueue) {
            if (wrapper.acquirer == acquirer) {
                [self.penddingQueue removeObjectAtIndex:i];
                return;
            }
            ++i;
        }
        
        for (CZRTSPThumbnailAcquirerWrapper *wrapper in self.processingQueue) {
            if (wrapper.acquirer == acquirer) {
                wrapper.acquirer = nil;
                return;
            }
        }
    }
}

- (void)clearQueue {
    @synchronized (self) {
        NSArray *tempArray = [[NSArray alloc] initWithArray:self.penddingQueue];
        [self.penddingQueue removeAllObjects];
        [tempArray release];
    }
}

- (CZRTSPThumbnailAcquirerWrapper *)popWrapper {
    @synchronized (self) {
        if (self.penddingQueue.count == 0) {
            return nil;
        }
        
        CZRTSPThumbnailAcquirerWrapper *wrapper = [self.penddingQueue firstObject];
        [_processingQueue addObject:wrapper];
        
        [self.penddingQueue removeObjectAtIndex:0];
        return wrapper;
    }
}

- (void)stopWrapper:(CZRTSPThumbnailAcquirerWrapper *)wrapper {
    @synchronized (self) {
        [_processingQueue removeObject:wrapper];
    }
    
    [self process];
}

- (void)process {
    CZRTSPThumbnailAcquirerWrapper *wrapper = nil;
    @synchronized (self) {
        if (_processingQueue.count < kMaxProcessingCount) {
            wrapper = [self popWrapper];
         }
    }
    
    [wrapper start];
}

@end
