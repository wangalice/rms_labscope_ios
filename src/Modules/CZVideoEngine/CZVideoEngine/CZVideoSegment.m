//
//  CZVideoSegment.m
//  Hermes
//
//  Created by Halley Gu on 9/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZVideoSegment.h"
#include <sys/mman.h>
#include <mp4v2/mp4v2.h>

static const NSUInteger kMemoryVideoSegmentLength = 1048576;
static const NSUInteger kMemorySegmentBufferLimits = 6;

static NSUInteger sMemorySegmentCount = 0;

@interface CZMemoryVideoSegmentBuffer : NSObject {
@private
    NSString *_path;
    void *_buffer;
}

+ (CZMemoryVideoSegmentBuffer *)newBuffer;
+ (void)releaseBuffer:(CZMemoryVideoSegmentBuffer **)buffer;

+ (NSMutableArray *)pool;

- (NSString *)path;

- (void)clear;

@end

@implementation CZMemoryVideoSegmentBuffer

+ (CZMemoryVideoSegmentBuffer *)newBuffer {
    CZMemoryVideoSegmentBuffer *buffer = nil;
    @synchronized (self) {
        NSMutableArray *pool = [CZMemoryVideoSegmentBuffer pool];
        buffer = [pool lastObject];
        if (buffer) {
            [buffer retain];
            [pool removeLastObject];
        } else {
            sMemorySegmentCount++;
        }
    }
    
    if (buffer == nil) {
        buffer = [[CZMemoryVideoSegmentBuffer alloc] init];
    } else {
        [buffer clear];
    }
    return buffer;
}

+ (void)releaseBuffer:(CZMemoryVideoSegmentBuffer **)buffer {
    if (buffer == NULL) {
        return;
    }
    
    if (*buffer == nil) {
        return;
    }
    
    @synchronized (self) {
        if (sMemorySegmentCount < kMemorySegmentBufferLimits) {
            [[CZMemoryVideoSegmentBuffer pool] addObject:*buffer];
        } else {
            sMemorySegmentCount--;
        }
    }
    
    [*buffer release];
    *buffer = nil;
}

+ (NSMutableArray *)pool {
    static NSMutableArray *_bufferPool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _bufferPool = [[NSMutableArray alloc] initWithCapacity:kMemorySegmentBufferLimits];
    });
    
    return _bufferPool;
}

- (id)init {
    self = [super init];
    if (self) {
        // Generate a unique name to avoid conflicts with other instances.
        NSString *uniqueString = [[NSProcessInfo processInfo] globallyUniqueString];
        NSString *fileName = [[NSString alloc] initWithFormat:@"%@.mp4", uniqueString];
        
        _path = [[NSTemporaryDirectory() stringByAppendingPathComponent:fileName] retain];
        
        [fileName release];
        
        const char *cPath = [_path cStringUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:_path error:&error];
        
        int fileDescriptor = open(cPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        
        lseek(fileDescriptor, kMemoryVideoSegmentLength - 1, SEEK_SET);
        write(fileDescriptor, "", 1);
        lseek(fileDescriptor, 0, SEEK_SET);
        
        _buffer = mmap(NULL, kMemoryVideoSegmentLength,
                       PROT_READ | PROT_WRITE, MAP_SHARED,
                       fileDescriptor, 0);
        close(fileDescriptor);
        
        if (_buffer != MAP_FAILED) {
            memset(_buffer, 0, kMemoryVideoSegmentLength);
            MP4SetMaxBufferLength(kMemoryVideoSegmentLength);
            MP4SetBufferForFile(cPath, _buffer);
        } else {
            [self release];
            return nil;
        }
    }
    return self;
}

- (void)dealloc {
    if (_buffer != NULL) {
        const char *cPath = [_path cStringUsingEncoding:NSUTF8StringEncoding];
        MP4SetBufferForFile(cPath, NULL);
        memset(_buffer, 0, kMemoryVideoSegmentLength);
        munmap(_buffer, kMemoryVideoSegmentLength);
    }
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:_path error:&error];
    
    [_path release];
    
    [super dealloc];
}

- (NSString *)path {
    return _path;
}

- (NSURL *)url {
    return [[[NSURL alloc] initFileURLWithPath:_path] autorelease];
}

- (void)clear {
    if (_buffer) {
        memset(_buffer, 0, kMemoryVideoSegmentLength);
    }
}

@end

@interface CZMemoryVideoSegment ()

@property (nonatomic, retain) CZMemoryVideoSegmentBuffer *buffer;
@property (atomic, assign, readwrite) CZVideoSegmentStatus status;

@end

@implementation CZMemoryVideoSegment

@synthesize status = _status;

- (void)dealloc {
    [CZMemoryVideoSegmentBuffer releaseBuffer:&_buffer];
    [super dealloc];
}

- (CZMemoryVideoSegmentBuffer *)buffer {
    if (_buffer == nil) {
        @synchronized (self) {
            if (_buffer == nil) {
                _buffer = [CZMemoryVideoSegmentBuffer newBuffer];
            }
        }
    }
    return _buffer;
}

- (NSString *)path {
    return [self.buffer path];
}

- (NSURL *)url {
    return [[[NSURL alloc] initFileURLWithPath:[self.buffer path]] autorelease];
}

- (BOOL)switchToStatus:(CZVideoSegmentStatus)status {
    @synchronized (self) {
        BOOL canSwitch = NO;
        switch (_status) {
            case kCZVideoSegmentIdle:
                canSwitch = (status == kCZVideoSegmentWriting);
                break;
            case kCZVideoSegmentWriting:
                canSwitch = (status == kCZVideoSegmentReadyForReading);
                break;
            case kCZVideoSegmentReadyForReading:
                canSwitch = (status == kCZVideoSegmentIdle || status == kCZVideoSegmentReading);
                break;
            case kCZVideoSegmentReading:
                canSwitch = (status == kCZVideoSegmentFinishedReading);
                break;
            case kCZVideoSegmentFinishedReading:
                canSwitch = (status == kCZVideoSegmentIdle);
                break;
            default:
                break;
        }
        
        if (canSwitch) {
            _status = status;
        }
        
        return canSwitch;
    }
}

- (void)clear {
    [_buffer clear];
}

@end

@interface CZFileVideoSegment () {
    NSURL *_url;
}

@property (atomic, assign, readwrite) CZVideoSegmentStatus status;

@end

@implementation CZFileVideoSegment

@synthesize status = _status;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithURL:(NSURL *)url {
    self = [super init];
    if (self) {
        _url = [url retain];
    }
    return self;
}

- (void)dealloc {
    [_url release];
    [super dealloc];
}

- (NSURL *)url {
    return _url;
}

- (BOOL)switchToStatus:(CZVideoSegmentStatus)status {
    self.status = status;
    return YES;
}

@end
