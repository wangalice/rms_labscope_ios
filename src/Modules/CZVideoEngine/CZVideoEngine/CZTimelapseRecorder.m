//
//  CZTimelapseRecorder.m
//  Matscope
//
//  Created by Ralph Jin on 12/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZTimelapseRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import <CZToolbox/CZToolbox.h>

static const CGFloat kTextFontRatio = 0.04;
//static const CGFloat kNoSignalTextFontRatio = 0.1;
static const CGFloat kTextPadding = 8;
static const CGFloat kTextInsetX = 8;
static const CGFloat kTextInsetY = 2;
static const uint64_t kFreeSpaceLowerLimit = 200 * 1024 * 1024;

static NSString *sNoSignalText = nil;

@interface CZTimelapseRecorder() {
    NSObject *_assetLock;
}

@property (nonatomic, retain) AVAssetWriter *assetWriter;
@property (nonatomic, retain) AVAssetWriterInput *assetWriterInput;
@property (nonatomic, retain) AVAssetWriterInputPixelBufferAdaptor *assetWriterAdaptor;

@property (nonatomic, assign) NSUInteger recordedFrameCount;
@property (nonatomic, retain) NSDate *recordingStartTime;
@property (nonatomic, retain) NSDate *lastFrameTime;
@property (nonatomic, retain) UIImage *lastFrame;

@end

@implementation CZTimelapseRecorder

+ (NSString *)noSignalText {
    return (sNoSignalText == nil) ? @"NO SIGNAL" : sNoSignalText;
}

+ (void)setNoSignalText:(NSString *)noSignalText {
    [sNoSignalText autorelease];
    sNoSignalText = [noSignalText copy];
}

+ (CGRect)timeStampFrameInOverlayer:(CALayer *)overlayer videoSize:(CGSize)videoSize {
    if (overlayer && CGSizeEqualToSize(videoSize, CGSizeZero) == NO) {
        NSString *string = @"9999:99:99 - Video lost";
        CGFloat fontSize = ceil(videoSize.height * kTextFontRatio);
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
        CGRect timeStampFrame = CGRectZero;
        timeStampFrame.size = [string sizeWithAttributes:@{NSFontAttributeName: font}];
        timeStampFrame = CGRectIntegral(timeStampFrame);
        timeStampFrame = CGRectInset(timeStampFrame, -kTextInsetX, -kTextInsetY);
        timeStampFrame.origin.x = kTextPadding;
        timeStampFrame.origin.y = videoSize.height - timeStampFrame.size.height - kTextPadding;
        return timeStampFrame;
    }
    return CGRectZero;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _assetLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    if (_assetWriter != nil) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:@"CZTimelapseRecorder: recorder is deallocated without endRecording!"
                                     userInfo:nil];
    }
    [_lastFrame release];
    [_assetLock release];
    [_assetWriter release];
    [_assetWriterInput release];
    [_assetWriterAdaptor release];
    
    [_recordingStartTime release];
    [_lastFrameTime release];
    
    [_overlayer release];

    [super dealloc];
}

- (void)beginRecordingToFile:(NSString *)filePath {
    if (self.videoSize.height <= 0 || self.videoSize.width <= 0) {
        return;
    }
    
    if (self.frameRate < 5) {  // minimum frame rate
        return;
    }
    
    if (self.videoCodecKey == nil) {
        self.videoCodecKey = AVVideoCodecH264;
    }
    
    if (self.interval < 1) {
        self.interval = 1;
    }

    @synchronized (_assetLock) {
        if (self.assetWriter == nil) {
            NSError *error = nil;
            AVAssetWriter *writer = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:filePath]
                                                              fileType:AVFileTypeMPEG4
                                                                 error:&error];
            
            CGSize videoSize = self.videoSize;
            NSDictionary *videoSettings = @{AVVideoCodecKey: self.videoCodecKey,
                                            AVVideoWidthKey: @(videoSize.width),
                                            AVVideoHeightKey: @(videoSize.height)};
            
            AVAssetWriterInput *writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                                                 outputSettings:videoSettings];
            [writerInput setExpectsMediaDataInRealTime:NO];
            
            AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                                                                                                             sourcePixelBufferAttributes:nil];
            
            if ([writer canAddInput:writerInput]) {
                [writer addInput:writerInput];
                
                self.assetWriter = writer;
                self.assetWriterInput = writerInput;
                self.assetWriterAdaptor = adaptor;

                self.recordedFrameCount = 0;
                self.lastFrameTime = nil;
                self.recordingStartTime = nil;
                self.lastFrame = nil;
                
                [writer startWriting];
                [writer startSessionAtSourceTime:kCMTimeZero];
                
                CZLogv(@"Video recording started.");
            } else {
                CZLogv(@"Failed to start video recording.");
            }
            
            [writer release];
        }
    }
}

- (void)endRecording {
    @synchronized (_assetLock) {
        if (self.assetWriter != nil) {
            [self.assetWriterInput markAsFinished];
            
            CZTimelapseRecorder *theSelf = [self retain];
            AVAssetWriter *theAssetWriter = [self.assetWriter retain];
            [theAssetWriter finishWritingWithCompletionHandler:^(){
                if ([theSelf.delegate respondsToSelector:@selector(recorderDidEndRecording:)]) {
                    [theSelf.delegate recorderDidEndRecording:theSelf];
                }
                
                [theSelf release];
                [theAssetWriter release];
            }];
            
            if (self.assetWriter.error != nil) {
                CZLogv(@"Video recording ended with error: %@", self.assetWriter.error);
            } else {
                CZLogv(@"Video recording ended.");
            }
            
            self.assetWriter = nil;
            self.assetWriterInput = nil;
            self.assetWriterAdaptor = nil;

            self.recordedFrameCount = 0;
            self.recordingStartTime = nil;
            self.lastFrameTime = nil;
        }
    }
}

- (BOOL)isRecording {
    @synchronized (_assetLock) {
        return self.assetWriter != nil;
    }
}

- (void)recordVideoFrame:(UIImage *)image {
    @synchronized (_assetLock) {
        if (![self isRecording]) {
            return;
        }

        NSDate *now = [NSDate date];
        NSUInteger intervalCount = 1;
        if (self.lastFrameTime) {
            NSTimeInterval interval = [now timeIntervalSinceDate:self.lastFrameTime];
            intervalCount = floor(interval / self.interval);
            if (intervalCount < 1) {
                return;
            }
        }

        if (self.recordingStartTime == nil) {
            self.recordingStartTime = now;
        }
        
        const CGSize frameSize = self.videoSize;
        NSDictionary *options = @{(id)kCVPixelBufferCGImageCompatibilityKey: @YES,
                                  (id)kCVPixelBufferCGBitmapContextCompatibilityKey: @YES};
        
        CVPixelBufferRef pixelBuffer = NULL;
        CVPixelBufferCreate(kCFAllocatorDefault, frameSize.width, frameSize.height,
                            kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef)options, &pixelBuffer);
        
        for (NSUInteger i = 0; i < intervalCount; i++) {
            BOOL drawLostFrame = (i != (intervalCount - 1)) || (image == nil);
            
            CVPixelBufferLockBaseAddress(pixelBuffer, 0);
            void *pixelData = CVPixelBufferGetBaseAddress(pixelBuffer);
            size_t rowBytes = CVPixelBufferGetBytesPerRow(pixelBuffer);
            
            CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
            CGContextRef context = CGBitmapContextCreate(pixelData, frameSize.width, frameSize.height, 8,
                                                         rowBytes, rgbColorSpace, kCGImageAlphaNoneSkipFirst);
            CGColorSpaceRelease(rgbColorSpace);
            
            UIGraphicsPushContext(context);

            if (drawLostFrame) {
                if (self.lastFrame) {
                    CGContextDrawImage(context, CGRectMake(0, 0, frameSize.width, frameSize.height), self.lastFrame.CGImage);
                } else {
                    CGContextClearRect(context, CGRectMake(0, 0, frameSize.width, frameSize.height));
                }
            } else {
                CGContextDrawImage(context, CGRectMake(0, 0, frameSize.width, frameSize.height), image.CGImage);
            }

            CGContextTranslateCTM(context, 0, frameSize.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            
            // 2.a draw overlay
            if (self.overlayer) {
                [self.overlayer renderInContext:context];
            }
            
            NSTimeInterval time = (NSTimeInterval)self.recordedFrameCount / self.frameRate;
            CMTime timestamp = CMTimeMakeWithSeconds(time, 90000);

            // 3. draw time stamp
            time = [now timeIntervalSinceDate:self.recordingStartTime];
            int hour, minute, second;
            hour = floor(time / 3600);
            time = fmod(time, 3600);
            minute = floor(time / 60);
            time = fmod(time, 60);
            second = floor(time);
            NSString *string = [NSString stringWithFormat:@"%02d:%02d:%02d", hour, minute, second];
            if (drawLostFrame) {
                string = [string stringByAppendingString:@" - Video lost"];
            }

            CGRect textFrame = [self timeStampFrameOfString:string];
            [[UIColor colorWithWhite:1.0 alpha:0.5] setFill];  // semi-transparent white background      
            [[UIBezierPath bezierPathWithRoundedRect:textFrame cornerRadius:kTextPadding] fill];
            
            [[UIColor blackColor] setFill];  // black text
            CGFloat fontSize = ceil(self.videoSize.height * kTextFontRatio);
            CGPoint pos = textFrame.origin;
            pos.x += kTextInsetX;
            pos.y += kTextInsetY;
            [string drawAtPoint:pos withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:fontSize]}];
            
            // 4. finish drawing
            UIGraphicsPopContext();
            CGContextRelease(context);
            CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
            
            // 5. write frame
            BOOL isWritten = NO;
            for (int retried = 0; retried < 5; retried++) {
                if (self.assetWriterInput.readyForMoreMediaData) {
                    isWritten = [self.assetWriterAdaptor appendPixelBuffer:pixelBuffer
                                                      withPresentationTime:timestamp];
                }
                
                if (isWritten) {
                    self.recordedFrameCount++;
                    if (self.lastFrameTime == nil) {
                        self.lastFrameTime = self.recordingStartTime;
                    } else {
                        self.lastFrameTime = [NSDate dateWithTimeInterval:self.interval sinceDate:self.lastFrameTime];
                    }
                    break;
                } else {
                    usleep(0.05 * NSEC_PER_USEC);
                }
            }
        }
        
        CVPixelBufferRelease(pixelBuffer);
        
        if (image) {
            self.lastFrame = image;
        }
    }
    
    if (![self hasEnoughFreeSpace]) {
        [self endRecording];
    }
}

- (CGRect)timeStampFrameOfString:(NSString *)string {
    CGFloat fontSize = ceil(self.videoSize.height * kTextFontRatio);
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
    CGRect timeStampFrame = CGRectZero;
    timeStampFrame.size = [string sizeWithAttributes:@{NSFontAttributeName:font}];
    timeStampFrame = CGRectIntegral(timeStampFrame);
    timeStampFrame = CGRectInset(timeStampFrame, -kTextInsetX, -kTextInsetY);
    timeStampFrame.origin.x = kTextPadding;
    timeStampFrame.origin.y = self.videoSize.height - timeStampFrame.size.height - kTextPadding;

    // Fixed time stamp frame without update according to CALayers's sublayers
//    if (self.overlayer) {
//        CGRect textBox = CGRectInset(timeStampFrame, -kTextPadding, -kTextPadding);
//
//        for (CALayer *subLayer in self.overlayer.sublayers) {
//            CGRect frame = subLayer.frame;
//
//            if (CGRectIntersectsRect(frame, textBox)) {
//                timeStampFrame.origin.x = self.videoSize.width - timeStampFrame.size.width - kTextPadding;
//                break;
//            }
//        }
//    }
    
    return timeStampFrame;
}

- (BOOL)hasEnoughFreeSpace {
    BOOL hasEnoughFreeSpace = YES;

    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        uint64_t totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        
        if (totalFreeSpace <= kFreeSpaceLowerLimit) {
            hasEnoughFreeSpace = NO;
        }
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return hasEnoughFreeSpace;
}

@end
