//
//  CZRTSPClient.m
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRTPFrameReceiver.h"

#include <BasicUsageEnvironment.hh>
#include "CZRTSPClientCore.h"
#include "CZRTSPClientDelegateWrapper.h"
#include "CZRTSPEventHandler.h"
#import "CZVideoSegment.h"
#include "CZMultiFramedRTPSink.h"

#import "CZMultiFramedRTPSource.h"

@interface CZRTPFrameReceiver () {
    CZRTSPFrameConsuming *_delegateWrapper;
}

@property (atomic, assign) BOOL isPlaying;
@property (atomic, retain) CZMultiFramedRTPSource *frameSource;

@end

@implementation CZRTPFrameReceiver

- (void)dealloc {
    [self stop];
    
    [_frameSource release];
    [super dealloc];
}

-(uint16_t)bindingPorts {
    return self.frameSource.bindingPorts;
}

#pragma mark - override super class

- (void)play {
    @synchronized (self) {
        if (self.isPlaying) {
            return;
        }
        
        self.isPlaying = YES;
        self.paused = NO;
        
        CZMultiFramedRTPSource *frameSource = [[CZMultiFramedRTPSource alloc] init];
        
        if (_delegateWrapper) {
            delete _delegateWrapper;
            _delegateWrapper = nullptr;
        }

        if ([self needsFileWrapping]) {
            [self prepareVideoCache];
            _delegateWrapper = new CZRTSPFileCache(self);
        } else {
            _delegateWrapper = new CZRTSPStreamConsumer(self);
        }
        
        frameSource.delegate = _delegateWrapper;
        
        self.frameSource = frameSource;
        [frameSource start];
        [frameSource release];
    }
}

- (void)stop {
    @synchronized (self) {
        if (self.isPlaying) {
            self.frameSource.delegate = nil;
            [self.frameSource stop];
            self.frameSource = nil;

            if (_delegateWrapper) {
                delete _delegateWrapper;
                _delegateWrapper = nullptr;
            }
            
            if ([self needsFileWrapping]) {
                [self clearVideoCache];
            }
            
            self.isPlaying = NO;
            self.paused = NO;
        }
    }
}

@end
