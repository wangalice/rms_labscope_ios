//
//  CZCZRTSPEventHandler.cpp
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZRTSPEventHandler.h"
#include <pthread.h>
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include "CZRTSPClientCore.h"
#include "CZRTSPDataSink.h"

const Boolean kStreamingUsingTCP = False;
const Boolean kForceMulticast = True;

namespace {
    static pthread_mutex_t shared_mutex;
    static pthread_once_t onceToken = PTHREAD_ONCE_INIT;
    
    class CZRecursiveLock {
    public:
        CZRecursiveLock();
        ~CZRecursiveLock();
        static void init_mutex();
    };
    
    CZRecursiveLock::CZRecursiveLock() {
        pthread_once(&onceToken, CZRecursiveLock::init_mutex);
        pthread_mutex_lock(&shared_mutex);
    }
    
    CZRecursiveLock::~CZRecursiveLock() {
        pthread_mutex_unlock(&shared_mutex);
    }
    
    void CZRecursiveLock::init_mutex() {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&shared_mutex, &attr);
    }
}

void CZRTSPEventHandler::setupNextSubsession(RTSPClient *rtspClient) {
    CZRTSPClientCore *core = dynamic_cast<CZRTSPClientCore *>(rtspClient);
    if (core == NULL) {
        return;
    }
    
    UsageEnvironment &env = core->envir();
    
    core->subsession = core->iter->next();
    if (core->subsession != NULL) {
        if (!core->subsession->initiate()) {
            env << "RTSPClient: Failed to initiate subsession: " << env.getResultMsg() << "\n";
            
            // Give up on this subsession; go to the next one.
            setupNextSubsession(core);
        } else {
            //env << "RTSPClient: Initiated subsession (client ports " << core->subsession->clientPortNum() << "-"
            //    << core->subsession->clientPortNum()+1 << ")\n";
            
            // Continue setting up this subsession, by sending a RTSP
            // "SETUP" command.
            core->sendSetupCommand(*core->subsession, continueAfterSETUP, False,
                                   kStreamingUsingTCP, kForceMulticast);
        }
        return;
    }
    
    // We've finished setting up all of the subsessions. Now, send a RTSP
    // "PLAY" command to start the streaming.
    if (core->session->absStartTime() != NULL) {
        // Special case: The stream is indexed by 'absolute' time, so send
        // an appropriate "PLAY" command.
        core->sendPlayCommand(*core->session,
                              continueAfterPLAY,
                              core->session->absStartTime(),
                              core->session->absEndTime());
    } else {
        core->duration = core->session->playEndTime() - core->session->playStartTime();
        core->sendPlayCommand(*core->session, continueAfterPLAY);
    }
}

void CZRTSPEventHandler::shutdownStream(RTSPClient *rtspClient) {
    CZRTSPClientCore *core = dynamic_cast<CZRTSPClientCore *>(rtspClient);
    if (core == NULL) {
        return;
    }
    
    CZRecursiveLock guard;

//    UsageEnvironment &env = core->envir();
    
    // First, check whether any subsessions have still to be closed.
    if (core->session != NULL) {
        Boolean someSubsessionsWereActive = False;
        MediaSubsessionIterator iter(*core->session);
        MediaSubsession *subsession;
        
        while ((subsession = iter.next()) != NULL) {
            if (subsession->sink != NULL) {
                subsession->sink->stopPlaying();
                Medium::close(subsession->sink);
                subsession->sink = NULL;
                
                if (subsession->rtcpInstance() != NULL) {
                    // Avoid shutdownStream being called again, in case the server
                    // sends a RTCP "BYE" while handling "TEARDOWN".
                    subsession->rtcpInstance()->setByeHandler(NULL, NULL);
                }
                
                someSubsessionsWereActive = True;
            }
        }
        
        if (someSubsessionsWereActive) {
            // Send a RTSP "TEARDOWN" command, to tell the server to
            // shutdown the stream. Don't bother handling the response to
            // the "TEARDOWN".
            core->sendTeardownCommand(*core->session, NULL);
        }
    }
    
//    env << "RTSPClient: Closing the stream: " << (void *)core << ".\n";
    Medium::close(core);
}

void CZRTSPEventHandler::continueAfterDESCRIBE(RTSPClient *rtspClient, int resultCode, char *resultString) {
    CZRTSPClientCore *core = dynamic_cast<CZRTSPClientCore *>(rtspClient);
    if (core == NULL) {
        delete[] resultString;
        return;
    }
    
    UsageEnvironment &env = core->envir();
    
    do {
        if (resultCode != 0) {
            env << "RTSPClient: Failed to get a SDP description: " << resultString << "\n";
            break;
        }
        
        //env << "RTSPClient: Got a SDP description\n";
        
        // Create a media session object from this SDP description.
        core->session = MediaSession::createNew(env, resultString);
        delete[] resultString;
        
        if (core->session == NULL) {
            env << "RTSPClient: Failed to create a MediaSession object from the SDP description: "
                << env.getResultMsg() << "\n";
            break;
        } else if (!core->session->hasSubsessions()) {
            env << "RTSPClient: This session has no media subsessions (i.e., no \"m=\" lines)\n";
            break;
        }
        
        // Then, create and set up our data source objects for the session.
        // We do this by iterating over the session's 'subsessions', calling
        // "MediaSubsession::initiate()", and then sending a RTSP "SETUP"
        // command, on each one. (Each 'subsession' will have its own data
        // source.)
        core->iter = new MediaSubsessionIterator(*core->session);
        setupNextSubsession(core);
        return;
    } while (0);
    
    // An unrecoverable error occurred with this stream.
    shutdownStream(core);
}

void CZRTSPEventHandler::continueAfterSETUP(RTSPClient *rtspClient, int resultCode, char *resultString) {
    delete[] resultString;
    
    CZRTSPClientCore *core = dynamic_cast<CZRTSPClientCore *>(rtspClient);
    if (core == NULL) {
        return;
    }
    
    UsageEnvironment &env = core->envir();
    
    do {
        if (resultCode != 0) {
            env << "RTSPClient: Failed to set up subsession: " << env.getResultMsg() << "\n";
            break;
        }
        
        //env << "RTSPClient: Set up subsession (client ports " << core->subsession->clientPortNum() << "-"
        //    << core->subsession->clientPortNum()+1 << ")\n";
        
        // Having successfully setup the subsession, create a data sink for
        // it, and call "startPlaying()" on it.
        // (This will prepare the data sink to receive data; the actual flow
        // of data from the client won't start happening until later, after
        // we've sent a RTSP "PLAY" command.)
        
        CZRTSPDataSink *sink = CZRTSPDataSink::createNew(env, *core->subsession, core->url());
        if (sink == NULL) {
            env << "RTSPClient: Failed to create a data sink for subsession: " << env.getResultMsg() << "\n";
            break;
        }
        
        sink->setDelegate(core->delegate);
        core->subsession->sink = sink;
        
        //env << "RTSPClient: Created a data sink for subsession\n";
        
        // A hack to let subsession handle functions get the "RTSPClient"
        // from the subsession.
        core->subsession->miscPtr = core;
        
        core->subsession->sink->startPlaying(*(core->subsession->readSource()),
                                             subsessionAfterPlaying,
                                             core->subsession);
        
        // Also set a handler to be called if a RTCP "BYE" arrives for this
        // subsession.
        if (core->subsession->rtcpInstance() != NULL) {
            core->subsession->rtcpInstance()->setByeHandler(subsessionByeHandler,
                                                            core->subsession);
        }
    } while (0);
    
    // Set up the next subsession, if any.
    setupNextSubsession(core);
}

void CZRTSPEventHandler::continueAfterPLAY(RTSPClient *rtspClient, int resultCode, char *resultString) {
    CZRTSPClientCore *core = dynamic_cast<CZRTSPClientCore *>(rtspClient);
    if (core == NULL) {
        return;
    }
    
    UsageEnvironment &env = core->envir();
    
    do {
        CZRecursiveLock guard;
        
        if (resultCode != 0) {
            env << "RTSPClient: Failed to start playing session: " << resultString << "\n";
            break;
        }
        
        // Set a timer to be handled at the end of the stream's expected
        // duration (if the stream does not already signal its end using a
        // RTCP "BYE"). This is optional.
        //
        // Alternatively, if you don't want to receive the entire stream,
        // you could set this timer for some shorter value.
        
        if (core->duration > 0) {
            // Number of seconds extra to delay, after the stream's expected
            // duration. This is optional.
            const unsigned int delaySlop = 0;
            core->duration += delaySlop;
            unsigned int uSecsToDelay = (unsigned int)(core->duration * 1000000);
            core->streamTimerTask = env.taskScheduler().scheduleDelayedTask(uSecsToDelay,
                                                                            (TaskFunc *)streamTimerHandler,
                                                                            core);
        }
        
//        env << "RTSPClient: Started playing session: " << (void *)core;
//        if (core->duration > 0) {
//            env << " (for up to " << core->duration << " seconds)";
//        }
//        env << "\n";
        
        return;
    } while (0);
    
    // An unrecoverable error occurred with this stream.
    shutdownStream(core);
}

void CZRTSPEventHandler::subsessionAfterPlaying(void *clientData) {
    MediaSubsession *subsession = (MediaSubsession *)clientData;
    CZRTSPClientCore *core = (CZRTSPClientCore *)(subsession->miscPtr);
    
    // Begin by closing this subsession's stream.
    Medium::close(subsession->sink);
    subsession->sink = NULL;
    
    // Next, check whether *all* subsessions' streams have now been closed.
    MediaSession& session = subsession->parentSession();
    MediaSubsessionIterator iter(session);
    while ((subsession = iter.next()) != NULL) {
        if (subsession->sink != NULL) {
            // This subsession is still active.
            return;
        }
    }
    
    // All subsessions' streams have been closed, so shutdown the client.
    shutdownStream(core);
}

void CZRTSPEventHandler::subsessionByeHandler(void *clientData) {
    MediaSubsession *subsession = (MediaSubsession *)clientData;
    CZRTSPClientCore *core = (CZRTSPClientCore *)(subsession->miscPtr);
    UsageEnvironment &env = core->envir();
    
    env << "RTSPClient: Received RTCP \"BYE\" on subsession\n";
    
    // Now act as if the subsession had closed.
    subsessionAfterPlaying(subsession);
}

void CZRTSPEventHandler::streamTimerHandler(void *clientData) {
    CZRTSPClientCore *core = (CZRTSPClientCore *)clientData;
    
    core->streamTimerTask = NULL;
    
    // Shut down the stream.
    shutdownStream(core);
}
