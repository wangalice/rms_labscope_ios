//
//  CZRTSPClient.m
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRTSPClient.h"
#import <CZToolbox/CZToolbox.h>
#include <BasicUsageEnvironment.hh>
#include "CZRTSPClientCore.h"
#include "CZRTSPClientDelegateWrapper.h"
#include "CZRTSPEventHandler.h"
#import "CZVideoSegment.h"
#include "CZMultiFramedRTPSink.h"

@interface CZRTSPClient () {
    NSURL *_url;
    CZRTSPClientCore *_core;
    CZRTSPFrameConsuming *_delegateWrapper;
    TaskScheduler *_scheduler;
    UsageEnvironment *_env;
    dispatch_queue_t _queue;
    dispatch_group_t _queueGroup;
    char _eventLoopWatchVariable;
    NSMutableArray *_videoCache;
    NSObject *_videoCacheLock;
    CZMultiFramedRTPSink *_relayer;
}

@property (atomic, assign) BOOL isPlaying;

@property (atomic, copy) NSString *recordingPath;
@property (atomic, retain) id<CZRTSPClientDelegate> retainedDelegate;

- (void)prepareVideoCache;

@end

@implementation CZRTSPClient

@synthesize delegate = _delegate;

- (id)init {
    [self release];
    return nil;
}

- (id)initWithURL:(NSURL *)rtspUrl {
    self = [super init];
    if (self) {
        _eventLoopWatchVariable = 1;
        _queue = dispatch_queue_create("com.zeisscn.rtspclient.loop", NULL);
        _queueGroup = dispatch_group_create();
        
        _url = [rtspUrl retain];
        
        _isPlaying = NO;
        _isForSingleFrame = NO;
        _delegate = nil;
        
        _videoCacheLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_videoCache removeAllObjects];
    
    if (_queue) {
        dispatch_release(_queue);
        _queue = NULL;
    }
    if (_queueGroup) {
        dispatch_release(_queueGroup);
        _queueGroup = NULL;
    }
    
    [_url release];
    [_recordingPath release];
    [_videoCache release];
    [_videoCacheLock release];
    
    [_retainedDelegate release];
    
    delete _relayer;
    _relayer = nullptr;

    [super dealloc];
}

- (void)play {
    @synchronized (self) {
        if (self.isPlaying || _url == nil) {
            return;
        }
        
        self.isPlaying = YES;
        self.paused = NO;
        
        // For wild-pointer issue in videoSegmentCallback
        self.retainedDelegate = self.delegate;
        
        if ([self needsFileWrapping]) {
            [self prepareVideoCache];
        }
        
        // Begin by setting up usage environment.
        _scheduler = BasicTaskScheduler::createNew();
        _env = BasicUsageEnvironment::createNew(*_scheduler);
        _eventLoopWatchVariable = 0;
        
        const char *url = [[_url absoluteString] cStringUsingEncoding:NSUTF8StringEncoding];

        _core = CZRTSPClientCore::createNew(*_env, url);
        if (_core == NULL) {
            *_env << "Failed to create a RTSP client for URL \"" << url << "\": "
            << _env->getResultMsg() << "\n";
            return;
        }
        
        CZRTSPStreamConsumerGroup *consumerGroup = new CZRTSPStreamConsumerGroup;
        if ([self needsFileWrapping]) {
            consumerGroup->appendConsumer(new CZRTSPFileCache(self));
        } else {
            consumerGroup->appendConsumer(new CZRTSPStreamConsumer(self));
        }
        consumerGroup->appendConsumer(new CZRTSPFileRecorder(self));
        consumerGroup->appendConsumer(new CZRTSPFrameRelay(self));
        _delegateWrapper = consumerGroup;
        
        _core->delegate = _delegateWrapper;
        
        dispatch_group_async(_queueGroup, _queue, ^{
            @synchronized ([CZRTSPClient class]) {
                // Next, send a RTSP "DESCRIBE" command, to get a SDP description for the
                // stream. Note that this command - like all RTSP commands - is sent
                // asynchronously; we do not block, waiting for a response. Instead, the
                // following function call returns immediately, and we handle the RTSP
                // response later, from within the event loop.
                _core->sendDescribeCommand(CZRTSPEventHandler::continueAfterDESCRIBE);
            }

            // All subsequent activity takes place within the event loop:
            _env->taskScheduler().doEventLoop(&(_eventLoopWatchVariable));
        });
    }
}

- (void)stop {
    [self endRecording];
    
    // End the event loop and wait.
    _eventLoopWatchVariable = 1;
    dispatch_group_wait(_queueGroup, DISPATCH_TIME_FOREVER);
    
    @synchronized (self) {
        if (self.isPlaying) {
            if (_core) {
                @synchronized ([CZRTSPClient class]) {  // use class object as lock
                    CZRTSPEventHandler::shutdownStream(_core);
                }
                _core = NULL;
            }
            
            [self clearVideoCache];

            if (_delegateWrapper) {
                delete(_delegateWrapper);
                _delegateWrapper = NULL;
            }
            
            if (_scheduler) {
                delete(_scheduler);
                _scheduler = NULL;
            }
            
            if (_env) {
                _env->reclaim();
                _env = NULL;
            }
            
            self.retainedDelegate = nil;
            
            self.isPlaying = NO;
            self.paused = NO;
        }
    }
}

- (void)pause {
    self.paused = YES;
}

- (void)resume {
    self.paused = NO;
}

- (void)startRelayToURL:(NSURL *)url {
    @synchronized(self) {
        if (_relayer == nullptr) {
            NSString *host = url.host;
            uint16_t port = [url.port unsignedShortValue];
            _relayer = new CZMultiFramedRTPSink([host UTF8String], port);
        }
    }
}

- (void)endRelay {
    @synchronized(self) {
        if (_relayer) {
            delete _relayer;
            _relayer = nullptr;
        }
    }
}

- (BOOL)isRelaying {
    @synchronized(self) {
        return _relayer != nullptr;
    }
}

- (void)beginRecordingToFile:(NSString *)filePath {
    self.recordingPath = filePath;
}

- (void)endRecording {
    if (self.recordingPath != nil) {
        self.recordingPath = nil;
        
        dispatch_group_async(_queueGroup, _queue, ^{
            // let recorder stop
            struct timeval tm{0, 0};
            if (_delegateWrapper != nil) {
                _delegateWrapper->afterGetFrame(nullptr, 0, tm, 0);
            }
        });
    }
}

- (void)videoSegmentCallback:(const char *)segmentPath {
    if (self.isPlaying && segmentPath != NULL) {
        NSString *path = [[NSString alloc] initWithCString:segmentPath encoding:NSUTF8StringEncoding];
        CZMemoryVideoSegment *segmentToPlay = nil;
        
        @synchronized (_videoCacheLock) {
            for (CZMemoryVideoSegment *segment in _videoCache) {
                if ([path isEqualToString:segment.path]) {
                    segmentToPlay = [segment retain];
                    break;
                }
            }
        }
        [path release];
        
        if (segmentToPlay == nil) {
            return;
        }
        
        id delegate = [self.delegate retain];
        if ([delegate respondsToSelector:@selector(rtspClient:didGetVideoSegment:)]) {
            [segmentToPlay switchToStatus:kCZVideoSegmentReadyForReading];
            [delegate rtspClient:self didGetVideoSegment:segmentToPlay];
        }
        [segmentToPlay release];
        [delegate release];
        
        if (self.isForSingleFrame) {
            dispatch_async(dispatch_get_main_queue(), ^(){
                [self stop];
            });
        }
    }
}

- (void)relayFrameBuffer:(const void *)buffer length:(size_t)length timestamp:(struct timeval)timestamp {
    @synchronized(self) {
        if (_relayer) {
            _relayer->enqueueFrame(buffer, length, timestamp);
        }
    }
}

- (void)notifyGetFrame {
    if ([self.delegate respondsToSelector:@selector(rtspClientDidGetVideoFrame:)]) {
        [self.delegate rtspClientDidGetVideoFrame:self];
    }
}

- (void)decodeVideoFrame:(const void *)frameBuffer length:(size_t)length {
    NSData *bufferWrapper = [NSData dataWithBytesNoCopy:const_cast<void *>(frameBuffer)
                                                 length:length
                                           freeWhenDone:NO];
    [self.delegate rtspClient:self didGetVideoSegment:bufferWrapper];
}

- (void)streamShutdownCallback {
    _core = NULL;
    
    // This is the routine that RTSP server doesn't correctly provide streaming.
    // Should stop the stream and kill all the threads.
    if (_eventLoopWatchVariable == 0) {
        dispatch_async(dispatch_get_main_queue(), ^() {
            [self stop];
            
            if (self.isForSingleFrame) {
                // Call the delegate method to notify upstream to close related objects
                // like hardware decoder.
                id<CZRTSPClientDelegate> strongDelegate = [self.retainedDelegate retain];
                if ([strongDelegate respondsToSelector:@selector(rtspClient:didGetVideoSegment:)]) {
                    [strongDelegate rtspClient:self didGetVideoSegment:nil];
                }
                [strongDelegate release];
            }
        });
    }
}

- (const char *)nextCacheFilePath {
    @synchronized (_videoCacheLock) {
        if (self.isPaused) {
            return NULL;
        }
        
        if (!_videoCache) {
            return NULL;
        }
        
        for (CZMemoryVideoSegment *segment in _videoCache) {
            if ([segment switchToStatus:kCZVideoSegmentWriting]) {
                [segment clear];
                return [[segment path] cStringUsingEncoding:NSUTF8StringEncoding];
            }
        }
        
        return NULL;
    }
}

- (const char *)videoRecordingPath {
    return [self.recordingPath UTF8String];
}

- (BOOL)needsFileWrapping {
    // Work in MP4 file mode for iOS 7.1-; work in data mode for iOS 8.0+.
    return (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1);
}

- (void)prepareVideoCache {
    // 1 for writing, 1 for reading, 1 queued for reading and 1 more for backup.
    NSUInteger itemCount = self.isForSingleFrame ? 1 : 4;
    
    @synchronized (_videoCacheLock) {
        if (_videoCache == nil) {
            _videoCache = [[NSMutableArray alloc] init];
            // TODO: reuse memory video segment, since it's expensive
            for (NSUInteger i = 0; i < itemCount; ++i) {
                CZMemoryVideoSegment *segment = [[CZMemoryVideoSegment alloc] init];
                [_videoCache addObject:segment];
                [segment release];
            }
        }
    }
}

- (void)clearVideoCache {
    @synchronized (_videoCacheLock) {
        [_videoCache release];
        _videoCache = nil;
    }
}

@end

// Define the methods of CZRTSPClientDelegateWrapper class here to do the
// tricky part of calling Objective-C delegate from C++ directly.

enum NALUnitTypes {
    kNALUnitTypeIDR = 5,
    kNALUnitTypeSPS = 7,
    kNALUnitTypePPS = 8
};

static const size_t kSampleLengthOffset = 4;
static const int kNALUTypeMask = 0x1f;
static const uint32_t kMP4TimeScale = 90000;
static const MP4Duration kDefaultSampleDuration = 3600;

CZRTSPClientDelegateWrapper::CZRTSPClientDelegateWrapper(CZRTSPClient *client) :
    _client(client), _pps(nullptr), _sps(nullptr) {
}

CZRTSPClientDelegateWrapper::~CZRTSPClientDelegateWrapper() {
    _client = NULL;
    
    if (_pps) {
        delete[] _pps;
        _pps = nullptr;
    }
    if (_sps) {
        delete[] _sps;
        _sps = nullptr;
    }
}

void CZRTSPClientDelegateWrapper::afterGetFrame(const uint8_t *receiveBuffer, size_t frameSize, struct timeval timestamp, MP4Duration duration) {
    if (receiveBuffer == nullptr) {
        return;
    }
    
    unsigned int naluType = receiveBuffer[kSampleLengthOffset] & kNALUTypeMask;
    
    switch (naluType) {
        case kNALUnitTypeSPS:
            // Refresh SPS data.
            if (_sps) {
                delete[] _sps;
                _sps = NULL;
            }
            _spsLength = frameSize;
            _sps = new u_int8_t[_spsLength];
            memcpy(_sps, receiveBuffer + kSampleLengthOffset, _spsLength);
            break;
            
        case kNALUnitTypePPS:
            // Refresh PPS data.
            if (_pps) {
                delete[] _pps;
                _pps = NULL;
            }
            _ppsLength = frameSize;
            _pps = new u_int8_t[_ppsLength];
            memcpy(_pps, receiveBuffer + kSampleLengthOffset, _ppsLength);
            break;
        default:
            break;
    }
}

bool CZRTSPClientDelegateWrapper::shouldGetNextFrame() const {
    return true;
}

void CZRTSPClientDelegateWrapper::streamShutdownCallback() {
    [_client streamShutdownCallback];
}

const char *CZRTSPClientDelegateWrapper::getNextCacheFilePath() {
    return [_client nextCacheFilePath];
}

const char *CZRTSPClientDelegateWrapper::getVideoRecordingPath() {
    return [_client videoRecordingPath];
}

bool CZRTSPClientDelegateWrapper::isForSingleFrame() const {
    return _client.isForSingleFrame ? true : false;
}

bool CZRTSPClientDelegateWrapper::needsFileWrapping() {
    return [_client needsFileWrapping] ? true : false;
}

int CZRTSPClientDelegateWrapper::getVideoResolutionWidth() {
    return (int)floor(_client.resolution.width);
}

int CZRTSPClientDelegateWrapper::getVideoResolutionHeight() {
    return (int)floor(_client.resolution.height);
}

// TODO: move to another file
CZRTSPFrameConsuming::~CZRTSPFrameConsuming() {
}

#pragma mark - class CZRTSPStreamConsumerGroup

CZRTSPStreamConsumerGroup::CZRTSPStreamConsumerGroup() {
}

CZRTSPStreamConsumerGroup::~CZRTSPStreamConsumerGroup() {
    for (auto consumer : _consumers) {
        delete consumer;
    }
}

void CZRTSPStreamConsumerGroup::afterGetFrame(const uint8_t *receiveBuffer, size_t frameSize, struct timeval timestamp, MP4Duration duration) {
    @autoreleasepool {
        for (auto consumer : _consumers) {
            consumer->afterGetFrame(receiveBuffer, frameSize, timestamp, duration);
        }
    }
}

bool CZRTSPStreamConsumerGroup::shouldGetNextFrame() const {
    for (auto consumer : _consumers) {
        if (consumer->shouldGetNextFrame()) {
            return true;
        }
    }
    return false;
}

void CZRTSPStreamConsumerGroup::streamShutdownCallback() {
    for (auto consumer : _consumers) {
        consumer->streamShutdownCallback();
    }
}

void CZRTSPStreamConsumerGroup::appendConsumer(CZRTSPFrameConsuming *consumer) {
    _consumers.push_back(consumer);
}

#pragma mark - class CZRTSPFileCache

CZRTSPFileCache::CZRTSPFileCache(CZRTSPClient *delegate) :
CZRTSPClientDelegateWrapper(delegate),
_currentCacheFile(NULL),
_currentCacheTrack(0),
_hasGotFirstFrame(false) {
    
}

CZRTSPFileCache::~CZRTSPFileCache() {
    this->endFrameCaching();
}

void CZRTSPFileCache::afterGetFrame(const uint8_t *receiveBuffer, size_t frameSize, struct timeval timestamp, MP4Duration duration) {
    if (receiveBuffer == nullptr) {
        return;
    }
    
    [client() notifyGetFrame];
    
    CZRTSPClientDelegateWrapper::afterGetFrame(receiveBuffer, frameSize, timestamp, duration);
    unsigned int naluType = receiveBuffer[kSampleLengthOffset] & kNALUTypeMask;
    if (naluType == kNALUnitTypeIDR) {
        _hasGotFirstFrame = true;
        this->beginFrameCaching();
    }
    
    if (naluType != kNALUnitTypePPS &&
        naluType != kNALUnitTypeSPS) {
        if (duration <= 0) {
            duration = kDefaultSampleDuration;
        }
        
        if (_currentCacheFile) {
            MP4WriteSample(_currentCacheFile, _currentCacheTrack, receiveBuffer,
                           frameSize + kSampleLengthOffset, duration, 0, true);
        }
    }

    if (client() != NULL && this->isForSingleFrame() && _hasGotFirstFrame) {
        // If in single frame mode and already got the frame, notify
        // about the cached segment and do not go on getting frames.
        this->endFrameCaching();
    }
}

bool CZRTSPFileCache::shouldGetNextFrame() const {
    bool shouldStop = (this->isForSingleFrame() && _hasGotFirstFrame);
    return !shouldStop;
}

bool CZRTSPFileCache::beginFrameCaching() {
    endFrameCaching();
    
    // No delegate means no file path, so exit.
    if (client() == NULL) {
        return false;
    }
    
    // Cannot correctly initialize MP4 container if SPS/PPS is not available.
    const uint8_t *sps = this->sps();
    const uint8_t *pps = this->pps();
    if (sps == NULL || pps == NULL) {
        return false;
    }
    
    const char *filePath = this->getNextCacheFilePath();
    if (filePath == NULL) {
        return false;
    }
    
    _currentCacheFilePath = filePath;
    filePath = _currentCacheFilePath.c_str();
    assert(filePath);
    _currentCacheFile = MP4CreateEx(filePath);
    if (_currentCacheFile == MP4_INVALID_FILE_HANDLE) {
        return false;
    }
    
    MP4SetTimeScale(_currentCacheFile, kMP4TimeScale);
    
    // Get video dimensions.
    int width = this->getVideoResolutionWidth();
    int height = this->getVideoResolutionHeight();
    
    _currentCacheTrack = MP4AddH264VideoTrack(_currentCacheFile,
                                              kMP4TimeScale,            // timeScale
                                              MP4_INVALID_DURATION,     // sampleDuration
                                              width,                    // width
                                              height,                   // height
                                              sps[1],                  // AVCProfileIndication
                                              sps[2],                  // profile_compat
                                              sps[3],                  // AVCLevelIndication
                                              kSampleLengthOffset - 1); // sampleLenFieldSizeMinusOne
    
    if (_currentCacheTrack == MP4_INVALID_TRACK_ID) {
        endFrameCaching();
        return false;
    }
    
    MP4AddH264SequenceParameterSet(_currentCacheFile, _currentCacheTrack, sps, spsLength());
    MP4AddH264PictureParameterSet(_currentCacheFile, _currentCacheTrack, pps, ppsLength());
    
    return true;
}

void CZRTSPFileCache::endFrameCaching() {
    if (_currentCacheFile) {
        MP4Close(_currentCacheFile);

        [this->client() videoSegmentCallback:_currentCacheFilePath.c_str()];
        
        _currentCacheFile = NULL;
        _currentCacheTrack = NULL;
    }
}

#pragma mark - class CZRTSPFileRecorder

static const uint64_t kFreeSpaceLowerLimit = 200 * 1024 * 1024;

CZRTSPFileRecorder::CZRTSPFileRecorder(CZRTSPClient *delegate) : CZRTSPClientDelegateWrapper(delegate),
    _recordingFile(NULL),
    _recordingTrack(NULL) {
    
}

CZRTSPFileRecorder::~CZRTSPFileRecorder() {
    this->endVideoRecording();
}

void CZRTSPFileRecorder::afterGetFrame(const uint8_t *receiveBuffer, size_t frameSize, struct timeval timestamp, MP4Duration duration) {
    const char *recordingPath = getVideoRecordingPath();
    if (!recordingPath || strcmp(recordingPath, _recordingFilePath.c_str()) != 0) {
        this->endVideoRecording();
        if (recordingPath) {
            _recordingFilePath = recordingPath;
        } else {
            _recordingFilePath.erase();
        }
    }
    
    if (receiveBuffer == nullptr) {
        return;
    }
    
    CZRTSPClientDelegateWrapper::afterGetFrame(receiveBuffer, frameSize, timestamp, duration);
    
    unsigned int naluType = receiveBuffer[kSampleLengthOffset] & kNALUTypeMask;
    if (naluType == kNALUnitTypeIDR) {
        this->beginVideoRecording();
    }

    if (_recordingFile) {
        if (naluType != kNALUnitTypePPS && naluType != kNALUnitTypeSPS) {
            MP4WriteSample(_recordingFile, _recordingTrack, receiveBuffer,
                           frameSize + kSampleLengthOffset, duration, 0, true);
            
            if (!hasEnoughFreeSpace()) {
                CZRTSPClient *client = this->client();
                
               [client endRecording];
            }
        }
    }
}

bool CZRTSPFileRecorder::beginVideoRecording() {
    // If video recording is already started, return.
    if (_recordingFile) {
        return false;
    }
    
    // Cannot begin video recording without correct output path.
    if (_recordingFilePath.empty()) {
        return false;
    }
    
    // Cannot correctly initialize MP4 container if SPS/PPS is not available.
    const uint8_t *sps = this->sps();
    const uint8_t *pps = this->pps();
    if (sps == NULL || pps == NULL) {
        return false;
    }
    
    // Cannot get dimensions if no delegate is available.
    if (this->client() == NULL) {
        return false;
    }
    
    _recordingFile = MP4CreateEx(_recordingFilePath.c_str());
    if (_recordingFile == MP4_INVALID_FILE_HANDLE) {
        return false;
    }
    
    MP4SetTimeScale(_recordingFile, kMP4TimeScale);
    
    // Get video dimensions.
    int width = this->getVideoResolutionWidth();
    int height = this->getVideoResolutionHeight();
    
    _recordingTrack = MP4AddH264VideoTrack(_recordingFile,
                                           kMP4TimeScale,           // timeScale
                                           MP4_INVALID_DURATION,    // sampleDuration
                                           width,                   // width
                                           height,                  // height
                                           sps[1],                 // AVCProfileIndication
                                           sps[2],                 // profile_compat
                                           sps[3],                 // AVCLevelIndication
                                           kSampleLengthOffset - 1);// sampleLenFieldSizeMinusOne
    
    if (_recordingTrack == MP4_INVALID_TRACK_ID) {
        endVideoRecording();
        return false;
    }
    
    MP4AddH264SequenceParameterSet(_recordingFile, _recordingTrack, sps, spsLength());
    MP4AddH264PictureParameterSet(_recordingFile, _recordingTrack, pps, ppsLength());
    
    return true;
}

void CZRTSPFileRecorder::endVideoRecording() {
    if (_recordingFile) {
        MP4Close(_recordingFile);
        _recordingFile = NULL;
        _recordingTrack = NULL;
        
        CZRTSPClient *client = this->client();
        if ([client.delegate respondsToSelector:@selector(rtspClientDidEndRecording:)]) {
            [client.delegate rtspClientDidEndRecording:client];
        }
    }
}

bool CZRTSPFileRecorder::hasEnoughFreeSpace() {
    bool hasEnoughFreeSpace = true;
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        uint64_t totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        
        if (totalFreeSpace <= kFreeSpaceLowerLimit) {
            hasEnoughFreeSpace = false;
        }
    } else {
        CZLogv(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return hasEnoughFreeSpace;
}

#pragma mark - class CZRTSPStreamConsumer

CZRTSPStreamConsumer::CZRTSPStreamConsumer(CZRTSPClient *client) : CZRTSPClientDelegateWrapper(client) {
}

CZRTSPStreamConsumer::~CZRTSPStreamConsumer() {
}

void CZRTSPStreamConsumer::afterGetFrame(const uint8_t *frameBuffer,
                                         size_t frameSize,
                                         struct timeval timestamp,
                                         MP4Duration duration) {
    if (frameBuffer == nullptr) {
        return;
    }
    
    [this->client() decodeVideoFrame:frameBuffer length:frameSize + kSampleLengthOffset];
}

#pragma mark - class CZRTSPFrameRelay

CZRTSPFrameRelay::CZRTSPFrameRelay(CZRTSPClient *client) : _client(client) {
}

CZRTSPFrameRelay::~CZRTSPFrameRelay() {
}

void CZRTSPFrameRelay::afterGetFrame(const uint8_t *frameBuffer, size_t frameSize, struct timeval timestamp, MP4Duration duration) {
    if (frameBuffer == nullptr) {
        return;
    }
    
    [_client relayFrameBuffer:frameBuffer length:frameSize timestamp:timestamp];
}

bool CZRTSPFrameRelay::shouldGetNextFrame() const {
    return [_client isRelaying];
}

void CZRTSPFrameRelay::streamShutdownCallback() {
    [_client endRelay];
}




