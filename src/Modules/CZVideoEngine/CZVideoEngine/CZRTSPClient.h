//
//  CZRTSPClient.h
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZVideoSegment.h"
#import <CoreGraphics/CoreGraphics.h>

@class CZRTSPClient;

@protocol CZRTSPClientDelegate <NSObject>

- (void)rtspClient:(CZRTSPClient *)client didGetVideoSegment:(id)segment;

@optional
- (void)rtspClientDidGetVideoFrame:(CZRTSPClient *)client;

- (void)rtspClientDidEndRecording:(CZRTSPClient *)client;  // notification end of recording, because of disk nearly full.

@end

@interface CZRTSPClient : NSObject

@property (atomic, assign) id<CZRTSPClientDelegate> delegate;
@property (nonatomic, assign) CGSize resolution;
@property (atomic, assign) BOOL isForSingleFrame;

@property (atomic, assign, getter = isPaused) BOOL paused;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithURL:(NSURL *)rtspUrl;

- (void)play;
- (void)stop;

- (void)pause;
- (void)resume;

- (void)startRelayToURL:(NSURL *)url;  // "rtsp://ipaddress:port"
- (void)endRelay;
- (BOOL)isRelaying;

- (void)beginRecordingToFile:(NSString *)filePath;
- (void)endRecording;

- (void)videoSegmentCallback:(const char *)segmentPath;
- (void)streamShutdownCallback;
- (const char *)nextCacheFilePath;
- (const char *)videoRecordingPath;
- (BOOL)needsFileWrapping;

- (void)prepareVideoCache;
- (void)clearVideoCache;

@end
