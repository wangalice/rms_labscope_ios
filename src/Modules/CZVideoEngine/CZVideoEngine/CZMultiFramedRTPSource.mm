//
//  CZMultiFramedRTPSource.mm
//  Matscope iPhone
//
//  Created by Ralph Jin on 6/1/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#include "CZMultiFramedRTPSource.h"
#import <CZToolbox/CZToolbox.h>

#import <dispatch/dispatch.h>
#include "CZRTSPClientDelegateWrapper.h"
//#import <Common/CZCommonUtils.h>

@implementation CZMultiFramedRTPSource

@synthesize socket = _socket;

- (id)init {
    self = [super init];
    if (self) {
        _cachedPackets = [[NSMutableDictionary alloc] init];
        _beginFrameSeqNo = -1;
        _endFrameSeqNo = -1;
    }
    return self;
}

- (void)dealloc {
    [_ipAddress release];
    [_socket close];
    [_socket release];
    
    [_cachedPackets release];
    
    [super dealloc];
}

- (uint16_t)bindingPorts {
    return _socket.localPort;
}

- (void)checkAndCraeateSocket {
    if (_socket == nil) {
        do {
            dispatch_queue_t queue = dispatch_queue_create("com.zeisscn.rtpsource.udp", DISPATCH_QUEUE_SERIAL);
            _socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                    delegateQueue:queue];
            dispatch_release(queue);
            
            [_socket setIPv4Enabled:YES];
            [_socket setIPv6Enabled:YES];
            
            NSError *error = nil;

            if (![_socket bindToPort:554 error:&error]) {
                if (![_socket bindToPort:8554 error:&error]) {
                    CZLogv(@"RTP: failed to bind to local port.");
                    [_socket release];
                    _socket = nil;
                    break;
                }
            }
            
            if (![_socket beginReceiving:&error]) {
                CZLogv(@"RTP: failed to begin receiving data from socket.");
                [_socket release];
                _socket = nil;
                break;
            }
        } while (false);
    }
}

- (void)start {
    @synchronized(self) {
        [self checkAndCraeateSocket];
    }
}

- (void)stop {
    if (_socket) {
        [self.socket close];
    }
}

#pragma mark - private

- (void)resetSession {
    [self.cachedPackets removeAllObjects];
    _beginFrameSeqNo = -1;
    _endFrameSeqNo = -1;
}

- (void)buildFrame {
    if (_beginFrameSeqNo == _endFrameSeqNo) {
        return;
    }

    uint32_t totalLength = 0;
    NSData *packet = nil;
    for (uint16_t i = _beginFrameSeqNo; i != _endFrameSeqNo; i++) {
        packet = [self.cachedPackets objectForKey:@(i)];
        if (packet == nil) {
            return;  // failed, still missing packet
        }
        if (packet.length > 14) {
            totalLength += (packet.length - 14);
        }
    }
    totalLength += 1; //nalu header
    
    uint32_t timestamp = 0;

    NSMutableData *frameBuffer = [NSMutableData dataWithCapacity:totalLength + 4];
    for (uint16_t i = _beginFrameSeqNo; i != _endFrameSeqNo; i++) {
        NSData *packet = self.cachedPackets[@(i)];
        if (i == _beginFrameSeqNo) {
            timestamp = ntohl(*(const uint32_t *)((const uint8_t *)packet.bytes + 4));
            
            uint32_t headerLength = htonl(totalLength);
            [frameBuffer appendBytes:&headerLength length:4];
            
            // append NALU header
            uint8_t naluHeader = 0;
            [packet getBytes:&naluHeader range:NSMakeRange(13, 1)];
            naluHeader = naluHeader & 0x1F;
            naluHeader = naluHeader | (0x3 << 5);  // NRI = 3
            [frameBuffer appendBytes:&naluHeader
                              length:1];
        }
        [frameBuffer appendBytes:((const uint8_t *)packet.bytes + 14)
                          length:(packet.length - 14)];
        [self.cachedPackets removeObjectForKey:@(i)];
    }
    _beginFrameSeqNo = _endFrameSeqNo;
    
    [self removeOutdateCache];
    
    [self afterGetFrame:frameBuffer timestamp:timestamp];
}

- (void)removeOutdateCache {
    if (_beginFrameSeqNo >= 0) {
        NSMutableArray *removeKeys = [NSMutableArray array];
        for (NSNumber *number in self.cachedPackets.allKeys) {
            if ([number unsignedShortValue] <= _beginFrameSeqNo) {
                [removeKeys addObject:number];
            }
        }
        
        [self.cachedPackets removeObjectsForKeys:removeKeys];
    }
}

- (void)afterGetFrame:(NSMutableData *)frame timestamp:(uint32_t)rtpTimeStamp {
    if (_delegate) {
        struct timeval timestamp;
        const uint32_t fTimestampFrequency = 90000;
        uint64_t uSeconds = (((uint64_t)rtpTimeStamp * 2 * 1000000) / fTimestampFrequency + 1) / 2; // rounds to nearest integer
        timestamp.tv_sec = uSeconds / 1000000;
        timestamp.tv_usec = uSeconds % 1000000;
        _delegate->afterGetFrame((const uint8_t *)frame.bytes, frame.length - 4, timestamp, 0);
        
        // debug code
//        NSString *md5 = [CZCommonUtils md5FromData:frame];
//        NSLog(@"receive data: %@; timestamp:%x", md5, rtpTimeStamp);
        //
    }
}

#pragma mark - delegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error {
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    if (sock == _socket) {
        if (data.length < 16) {
            return;
        }
        
        const uint8_t * const pData = (const uint8_t *)data.bytes;
        uint8_t version = ((pData[0] & 0xC0) >> 6);
        if (version != 2) {
            return;
        }
        
        uint8_t csrcCount = ((pData[0] & 0x0F) >> 4);
        if (csrcCount != 0) {
            return;
        }
        
        uint8_t payloadType = (pData[1] & 0x7F);
        if (payloadType != 96) { // DynamicRTP Type
            return;
        }
        
        // check SSRC
        const uint32_t SSRC = ntohl(*(const uint32_t *)(pData + 8));
        if (SSRC != _SSRC) {
            [self resetSession];
            _SSRC = SSRC;
        }
        
        uint16_t currentFrameSeqNo = ntohs(*(const uint16_t *)(pData + 2));
        uint8_t naluType = (*(pData + 12) & 0x1F);
        if (naluType <= 23) {
            // build single frame from single packet
            uint32_t naluLength = data.length - 12;
            NSMutableData *frameBuffer = [NSMutableData dataWithCapacity:naluLength + 4];
            uint32_t headerLength = htonl(naluLength);
            [frameBuffer appendBytes:&headerLength length:4];
            [frameBuffer appendBytes:(pData + 12) length:naluLength];
            
            uint32_t timestamp = ntohl(*(const uint32_t *)(pData + 4));
            [self afterGetFrame:frameBuffer timestamp:timestamp];
        } else if (naluType == 28) {  // 28: FU-A
            self.cachedPackets[@(currentFrameSeqNo)] = data;
            
            BOOL isStartPU = (*(pData + 13) & 0x80) != 0;
            if (isStartPU) {
                _beginFrameSeqNo = currentFrameSeqNo;
                _endFrameSeqNo = _beginFrameSeqNo;
            } else {
                BOOL isLastPU = (*(pData + 13) & 0x40) != 0;
                BOOL isLastPacket = ((pData[1] & 0x80) != 0);
                if (isLastPU && isLastPacket) {
                    _endFrameSeqNo = (uint16_t)(currentFrameSeqNo + 1);
                }
            }
            
            [self buildFrame];
        }
    }
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    if (sock == _socket) {
        self.socket = nil;
    }
}

@end
