//
//  CZRTSPEventHandler.h
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef CZRTSPEventHandler_H
#define CZRTSPEventHandler_H

class RTSPClient;

class CZRTSPEventHandler {
public:
    /*! Used to iterate through each stream's 'subsessions', setting up each one. */
    static void setupNextSubsession(RTSPClient *rtspClient);
    
    /*! Used to shut down and close a stream. */
    static void shutdownStream(RTSPClient *rtspClient);
    
    static void continueAfterDESCRIBE(RTSPClient *rtspClient, int resultCode, char *resultString);
    static void continueAfterSETUP(RTSPClient *rtspClient, int resultCode, char *resultString);
    static void continueAfterPLAY(RTSPClient *rtspClient, int resultCode, char *resultString);
    
    static void subsessionAfterPlaying(void *clientData);
    static void subsessionByeHandler(void *clientData);
    static void streamTimerHandler(void *clientData);
    
private:
    CZRTSPEventHandler() {}
    ~CZRTSPEventHandler() {}
};

#endif // CZRTSPEventHandler_H
