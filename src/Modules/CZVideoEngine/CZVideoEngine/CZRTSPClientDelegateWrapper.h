//
//  CZRTSPClientCallback.h
//  Hermes
//
//  Created by Halley Gu on 3/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef CZRTSPClientCallback_h
#define CZRTSPClientCallback_h

#include <mp4v2/mp4v2.h>
#include <list>

#ifdef __OBJC__
@class CZRTSPClient;
#else
class CZRTSPClient;
#endif

class CZRTSPFrameConsuming {
public:
    virtual ~CZRTSPFrameConsuming() = 0;
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) = 0;
    virtual bool shouldGetNextFrame() const = 0;
    
    virtual void streamShutdownCallback() = 0;
};

class CZRTSPStreamConsumerGroup : public CZRTSPFrameConsuming {
public:
    CZRTSPStreamConsumerGroup();
    virtual ~CZRTSPStreamConsumerGroup();
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;
    virtual bool shouldGetNextFrame() const override;
    virtual void streamShutdownCallback() override;
    
    void appendConsumer(CZRTSPFrameConsuming *consumer);
    
private:
    std::list<CZRTSPFrameConsuming *> _consumers;
};

/**
 * This is the C++ "trampoline" that will be used to invoke a specific
 * Objective-C method from C++ directly.
 * Please refer to http://stackoverflow.com/a/1061576/1241690 for more info.
 */
class CZRTSPClientDelegateWrapper : public CZRTSPFrameConsuming {
public:
    // Implementations are in CZRTSPClient.mm, so that this header file can be
    // directly included in C++ source files without being compiled as
    // Objective-C++.
    
    explicit CZRTSPClientDelegateWrapper(CZRTSPClient *client);
    
    virtual ~CZRTSPClientDelegateWrapper() = 0;
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;

    virtual bool shouldGetNextFrame() const override;
    virtual void streamShutdownCallback() override;
    
    const char *getNextCacheFilePath();
    const char *getVideoRecordingPath();
    bool isForSingleFrame() const;
    bool needsFileWrapping();
    int getVideoResolutionWidth();
    int getVideoResolutionHeight();

    CZRTSPClient * client() { return _client; }
    const u_int8_t * sps() { return _sps; }
    const u_int8_t * pps() { return _pps; }
    size_t spsLength() { return _spsLength; }
    size_t ppsLength() { return _ppsLength; }

private:
    CZRTSPClient *_client;  // weak
    
    u_int8_t *_sps;
    u_int8_t *_pps;
    size_t _spsLength;
    size_t _ppsLength;
};

// TODO: move to another file
#ifdef __cplusplus

#include <mp4v2/mp4v2.h>
#include <string>
class CZRTSPFileCache : public CZRTSPClientDelegateWrapper {
public:
    explicit CZRTSPFileCache(CZRTSPClient *client);
    ~CZRTSPFileCache();
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;
    virtual bool shouldGetNextFrame() const override;

private:
    MP4FileHandle _currentCacheFile;
    std::string _currentCacheFilePath;
    MP4TrackId _currentCacheTrack;
    
    bool _hasGotFirstFrame;
    
    bool beginFrameCaching();
    void endFrameCaching();
};

// TODO: move to another file
class CZRTSPFileRecorder : public CZRTSPClientDelegateWrapper {
public:
    explicit CZRTSPFileRecorder(CZRTSPClient *client);
    ~CZRTSPFileRecorder();
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;

private:
    MP4FileHandle _recordingFile;
    std::string _recordingFilePath;
    MP4TrackId _recordingTrack;
    
    bool beginVideoRecording();
    void endVideoRecording();
    
    bool hasEnoughFreeSpace();
};

class CZRTSPStreamConsumer : public CZRTSPClientDelegateWrapper {
public:
    explicit CZRTSPStreamConsumer(CZRTSPClient *client);
    virtual ~CZRTSPStreamConsumer();
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;
};

class CZRTSPFrameRelay : public CZRTSPFrameConsuming {
public:
    explicit CZRTSPFrameRelay(CZRTSPClient *client);
    virtual ~CZRTSPFrameRelay();
    
    virtual void afterGetFrame(const uint8_t *frameBuffer,
                               size_t length,
                               struct timeval timestamp,
                               MP4Duration duration) override;
    virtual bool shouldGetNextFrame() const override;
    
    virtual void streamShutdownCallback() override;
private:
    CZRTSPClient *_client;  // weak
};

#endif // __cplusplus

#endif
