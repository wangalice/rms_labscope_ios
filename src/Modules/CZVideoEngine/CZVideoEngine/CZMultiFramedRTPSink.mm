//
//  CZMultiFramedRTPSink.mm
//  Matscope iPhone
//
//  Created by Ralph Jin on 6/1/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#include "CZMultiFramedRTPSink.h"
#import <CZToolbox/CZToolbox.h>

#include <sys/time.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>
#import <dispatch/dispatch.h>
//#import <Common/CZCommonUtils.h>

@interface CZPacketSender : NSObject <GCDAsyncUdpSocketDelegate>

@property (nonatomic, retain, readonly) GCDAsyncUdpSocket *socket;
@property (nonatomic, copy) NSString *ipAddress;

@property (nonatomic, assign) uint16_t ports;
@property (nonatomic, readonly) uint16_t bindingPorts;

- (void)sendData:(NSData *)data tag:(NSUInteger)tag;

@end

@implementation CZPacketSender

@synthesize socket = _socket;

- (void)dealloc {
    [_socket close];
    [_socket release];
    
    [_ipAddress release];
    
    [super dealloc];
}

- (uint16_t)bindingPorts {
    return _socket.localPort;
}

- (void)sendData:(NSData *)data tag:(NSUInteger)tag {
    [self.socket sendData:data
                   toHost:self.ipAddress
                     port:self.ports
              withTimeout:1.0
                      tag:tag];
}

- (id)socket {
    if (_socket == nil) {
        do {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                    delegateQueue:queue];
            
            [_socket setIPv4Enabled:YES];
            [_socket setIPv6Enabled:YES];
            
            NSError *error = nil;

            if (![_socket bindToPort:554 error:&error]) {
                CZLogv(@"RTP: failed to bind to local port.");
                break;
            }
            
            if (![_socket beginReceiving:&error]) {
                CZLogv(@"RTP: failed to begin receiving data from socket.");
                break;
            }
        } while (false);
    }
    return _socket;
}

@end

static const uint32_t kMTULength = 1498;  // for local network, for internet it shall be 576
static const uint32_t kUDPPacketLength = kMTULength - 14 - 20 - 8;  // 14 is ethernet II header, 20 is IP header, 8 is UDP header
static const uint32_t kMaxPacketLength = kUDPPacketLength - 12;   // 12 is the RTP header length.

static uint32_t our_random32() {
    long random_1 = random();
    uint32_t random16_1 = (uint32_t)(random_1 & 0x00FFFF00);
    
    long random_2 = random();
    uint32_t random16_2 = (uint32_t)(random_2 & 0x00FFFF00);
    
    return (random16_1 << 8) | (random16_2 >> 8);
}

const static uint32_t fTimestampFrequency = 90000;

static uint32_t convertToRTPTimestamp(struct timeval tv) {
    // Begin by converting from "struct timeval" units to RTP timestamp units:
    uint32_t rtpTimestamp = (fTimestampFrequency * tv.tv_sec);
    rtpTimestamp += (uint32_t)(fTimestampFrequency * (tv.tv_usec / 1000000.0) + 0.5); // note: rounding
    
    return rtpTimestamp;
}

CZMultiFramedRTPSink::CZMultiFramedRTPSink(const char *ipAddress, uint16_t ports) {
    fSSRC = our_random32();
    fSeqNo = (uint16_t)random();
    fRTPPayloadType = 96; // DynamicRTP Type 96
    fPacketSender = [[CZPacketSender alloc] init];
    fPacketSender.ports = ports;
    
    NSString *ipAddressString = [NSString stringWithUTF8String:ipAddress];
    fPacketSender.ipAddress = ipAddressString;
}

CZMultiFramedRTPSink::~CZMultiFramedRTPSink() {
    [fPacketSender release];
}

void CZMultiFramedRTPSink::enqueueFrame(const void *frameBuffer, unsigned int frameSize, struct timeval timestamp) {
    uint32_t rtpTimestamp = convertToRTPTimestamp(timestamp);
    uint8_t * const data = (uint8_t *)frameBuffer + 4;  // skip length
    
    // {{{ debug code
//    NSData *frame = [NSData dataWithBytesNoCopy:(void *)frameBuffer length:frameSize + 4 freeWhenDone:NO];
//    NSString *md5 = [CZCommonUtils md5FromData:frame];
//    NSLog(@"send data: %@; timestamp:%x", md5, rtpTimestamp);
    // }}}
    
    if (frameSize < kMaxPacketLength) {
        fIsLastPacket = false;
        fIsMultiPacket = false;
        buildAndSendPacket(data, 0, frameSize, rtpTimestamp);
    } else {
        uint32_t remainLength = frameSize - 1;  // skip nalu header
        uint32_t offset = 1;  // skip nalu header
        fIsMultiPacket = true;
        fIsFirstPacket = true;
        fIsLastPacket = false;
        const uint32_t kEachUnitLength = kMaxPacketLength - 2;  // 2 bytes of FU (Fragment Units) indicator and FU header
        for (;remainLength > 0;) {
            if (remainLength > kEachUnitLength) {
                buildAndSendPacket(data, offset, kEachUnitLength, rtpTimestamp);
                offset += kEachUnitLength;
                remainLength -= kEachUnitLength;
            } else {
                fIsLastPacket = true;
                buildAndSendPacket(data, offset, remainLength, rtpTimestamp);
                offset += remainLength;
                remainLength = 0;
            }
            fIsFirstPacket = false;
        }
    }
}

uint16_t CZMultiFramedRTPSink::bindingPort() {
    return fPacketSender.bindingPorts;
}

void CZMultiFramedRTPSink::buildAndSendPacket(const uint8_t *frameData, uint32_t offset, uint32_t length, uint32_t timestamp) {
    uint32_t totalLength = 12 + length;
    if (fIsMultiPacket) {
        totalLength += 2;
    }
    NSMutableData *nsData = [NSMutableData dataWithLength:totalLength];
    uint8_t *pData = (uint8_t *)[nsData mutableBytes];
    
    // Set up the RTP header:
    uint32_t rtpHdr = 0x80000000;
    if (fIsLastPacket) {
        rtpHdr |= 0x800000;
    }
    rtpHdr |= (fRTPPayloadType << 16);
    rtpHdr |= fSeqNo; // sequence number
    *((uint32_t *)pData) = htonl(rtpHdr);
    pData += 4;
    
    *((uint32_t *)pData) = htonl(timestamp);
    pData += 4;
    
    *((uint32_t *)pData) = htonl(fSSRC);
    pData += 4;
    
    if (fIsMultiPacket) {
        const uint8_t kFUIndicator = (28 | 0x3 << 5);  // type:28 FU-A, NRI = 3
        *pData = kFUIndicator;
        pData++;
        
        uint8_t FUHeader = (frameData[0] & 0x1F);
        if (fIsFirstPacket) {
            FUHeader = FUHeader | 0x80;
        } else if (fIsLastPacket) {
            FUHeader = FUHeader | 0x40;
        }
        *pData = FUHeader;
        pData++;
    }
    
    memcpy(pData, frameData + offset, length);
    
    [fPacketSender sendData:nsData tag:fSeqNo++];
}
