//
//  CZTimelapseRecorder.h
//  Matscope
//
//  Created by Ralph Jin on 12/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CZTimelapseRecorderDelegate;

@interface CZTimelapseRecorder : NSObject

@property (atomic, assign) id <CZTimelapseRecorderDelegate> delegate;

@property (nonatomic, assign) NSTimeInterval interval;  // time interval between each frame
@property (nonatomic, assign) NSUInteger frameRate;  // frames per second
@property (nonatomic, assign) CGSize videoSize;
@property (nonatomic, assign) NSString * const videoCodecKey; // for example, AVVideoCodecH264
@property (nonatomic, retain) CALayer *overlayer;

+ (CGRect)timeStampFrameInOverlayer:(CALayer *)overlayer videoSize:(CGSize)videoSize;

+ (NSString *)noSignalText;
+ (void)setNoSignalText:(NSString *)noSignalText;

- (void)beginRecordingToFile:(NSString *)filePath;
- (void)endRecording;
- (BOOL)isRecording;

- (void)recordVideoFrame:(UIImage *)image;

@end

@protocol CZTimelapseRecorderDelegate <NSObject>

- (void)recorderDidEndRecording:(CZTimelapseRecorder *)recorder;

@end
