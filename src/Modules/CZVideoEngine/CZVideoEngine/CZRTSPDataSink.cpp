//
//  CZRTSPDataSink.cpp
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZRTSPDataSink.h"
#include "CZRTSPClientDelegateWrapper.h"
#include "CZMultiFramedRTPSink.h"

// If you don't want to see debugging output for each received frame, then
// comment out the following line.
//#define DEBUG_PRINT_FRAME_INFO 1

static const uint32_t kMP4TimeScale = 90000;
static const MP4Duration kDefaultSampleDuration = 3600; // 90000 / 25fps = 3600
static const size_t kReceiveBufferSize = 409600;
static const size_t kSampleLengthOffset = 4;
//static const int kNALUTypeMask = 0x1f;

CZRTSPDataSink *CZRTSPDataSink::createNew(UsageEnvironment &env,
                                          MediaSubsession &subsession,
                                          char const *streamId) {
    return new CZRTSPDataSink(env, subsession, streamId);
}

CZRTSPDataSink::CZRTSPDataSink(UsageEnvironment &env, MediaSubsession &subsession,
                               char const *streamId)
: MediaSink(env), _subsession(subsession),
_lastTimestamp(0), _lastDuration(0) {
    _receiveBuffer = new u_int8_t[kReceiveBufferSize + kSampleLengthOffset];
}

CZRTSPDataSink::~CZRTSPDataSink() {
    delete[] _receiveBuffer;
}

void CZRTSPDataSink::setDelegate(CZRTSPFrameConsuming *newDelegate) {
    this->_delegate = newDelegate;
}

void CZRTSPDataSink::afterGettingFrame(void *clientData,
                                       unsigned int frameSize,
                                       unsigned int numTruncatedBytes,
                                       struct timeval presentationTime,
                                       unsigned int durationInMicroseconds) {
    CZRTSPDataSink *sink = (CZRTSPDataSink *)clientData;
    sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime,
                            durationInMicroseconds);
}

void CZRTSPDataSink::afterGettingFrame(unsigned int frameSize,
                                       unsigned int numTruncatedBytes,
                                       struct timeval presentationTime,
                                       unsigned int durationInMicroseconds) {
    unsigned int *p = (unsigned int *)_receiveBuffer;
    *p = htonl(frameSize);
    
    // Only uncomment this for debugging purpose.
    //printFrameInfo(frameSize, numTruncatedBytes, presentationTime,
    //               durationInMicroseconds);
    
    // Calculate real duration for each frame (sample).
    long millis = (presentationTime.tv_sec * 1000) + (presentationTime.tv_usec / 1000);
    MP4Duration duration = (millis - _lastTimestamp) * kMP4TimeScale / 1000;
    if (duration > kMP4TimeScale) {
        // If the calculated duration is greater than 1 second, ignore it.
        duration = kDefaultSampleDuration;
    } else if (duration == 0) {
        duration = (_lastDuration == 0) ? kDefaultSampleDuration : _lastDuration;
    }
    
    _lastDuration = duration;
    _lastTimestamp = millis;
    
    if (_delegate) {
        _delegate->afterGetFrame(_receiveBuffer, frameSize, presentationTime, duration);
    }
    
    if (_delegate && _delegate->shouldGetNextFrame()) {
        // Then continue, to request the next frame of data.
        continuePlaying();
    }
}

Boolean CZRTSPDataSink::continuePlaying() {
    if (fSource == NULL) {
        return False; // Sanity check (should not happen)
    }
    
    memset(_receiveBuffer, 0, kReceiveBufferSize + kSampleLengthOffset);
    
    // Request the next frame of data from our input source.
    fSource->getNextFrame(_receiveBuffer + kSampleLengthOffset,
                          kReceiveBufferSize,
                          afterGettingFrame, this,
                          onSourceClosure, this);
    return True;
}

void CZRTSPDataSink::printFrameInfo(unsigned int frameSize,
                                    unsigned int numTruncatedBytes,
                                    struct timeval presentationTime,
                                    unsigned int durationInMicroseconds) {
#ifdef DEBUG_PRINT_FRAME_INFO
    // Print out information.
    envir() << subsession.mediumName() << "/" << subsession.codecName()
    << ": Received " << frameSize << " bytes";
    
    if (numTruncatedBytes > 0) {
        envir() << " (with " << numTruncatedBytes << " bytes truncated)";
    }
    envir() << ". ";
    
    // Print the NAL unit type.
    unsigned int naluType = receiveBuffer[kSampleLengthOffset] & kNALUTypeMask;
    envir() << "NALU type: " << naluType << ". ";
    
    // Output the 'microseconds' part of the presentation time.
    char uSecsStr[6+1];
    sprintf(uSecsStr, "%06u", (unsigned int)presentationTime.tv_usec);
    envir() << "Presentation time: " << (int)presentationTime.tv_sec
    << "." << uSecsStr;
    
    // Mark the debugging output to indicate that this presentation time is
    // not RTCP-synchronized.
    if (subsession.rtpSource() != NULL &&
        !subsession.rtpSource()->hasBeenSynchronizedUsingRTCP()) {
        envir() << "!";
    }
    
    envir() << "\n";
#endif
}
