//
//  CZRTSPFrameReceiver.h
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRTSPClient.h"

// TODO: refactor together with CZRTSPClient

@interface CZRTPFrameReceiver : CZRTSPClient

@property (nonatomic, assign, readonly) uint16_t bindingPorts;

@end
