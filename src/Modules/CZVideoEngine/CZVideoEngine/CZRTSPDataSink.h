//
//  CZRTSPDataSink.h
//  Hermes
//
//  Created by Halley Gu on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef CZRTSPDataSink_H
#define CZRTSPDataSink_H

#include <string>
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <mp4v2/mp4v2.h>

class CZRTSPFrameConsuming;

/**
 * Define a data sink (a subclass of "MediaSink") to receive the data for each
 * subsession (i.e., each audio or video 'substream').
 */
class CZRTSPDataSink: public MediaSink {
public:
    static CZRTSPDataSink *createNew(UsageEnvironment &env,
                                     MediaSubsession &subsession,
                                     char const *streamId = NULL);
    
    void setDelegate(CZRTSPFrameConsuming *newDelegate);
    
private:
    CZRTSPDataSink(UsageEnvironment &env,
                   MediaSubsession &subsession,
                   char const *streamId);
    
    virtual ~CZRTSPDataSink();
    
    static void afterGettingFrame(void *clientData,
                                  unsigned int frameSize,
                                  unsigned int numTruncatedBytes,
                                  struct timeval presentationTime,
                                  unsigned int durationInMicroseconds);
    
    void afterGettingFrame(unsigned int frameSize,
                           unsigned int numTruncatedBytes,
                           struct timeval presentationTime,
                           unsigned int durationInMicroseconds);
    
    virtual Boolean continuePlaying();
    
    void printFrameInfo(unsigned int frameSize,
                        unsigned int numTruncatedBytes,
                        struct timeval presentationTime,
                        unsigned int durationInMicroseconds);
    
private:
    CZRTSPFrameConsuming *_delegate;
    
    u_int8_t *_receiveBuffer;
    MediaSubsession &_subsession;
    
    u_int64_t _lastTimestamp;
    MP4Duration _lastDuration;
};

#endif // CZRTSPDataSink_H
