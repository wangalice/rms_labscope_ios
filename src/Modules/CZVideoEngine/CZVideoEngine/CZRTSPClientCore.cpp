//
//  CZRTSPClientCore.cpp
//  Hermes
//
//  Created by Halley Gu on 2/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZRTSPClientCore.h"
#include "CZRTSPClientDelegateWrapper.h"

CZRTSPClientCore *CZRTSPClientCore::createNew(UsageEnvironment &env,
                                              char const *rtspURL,
                                              int verbosityLevel,
                                              char const *applicationName,
                                              portNumBits tunnelOverHTTPPortNum,
                                              int socketNumToServer) {
    return new CZRTSPClientCore(env, rtspURL, verbosityLevel,
                                applicationName, tunnelOverHTTPPortNum,
                                socketNumToServer);
}

CZRTSPClientCore::CZRTSPClientCore(UsageEnvironment &env, char const *rtspURL,
                                   int verbosityLevel, char const *applicationName,
                                   portNumBits tunnelOverHTTPPortNum, int socketNumToServer)
: RTSPClient(env, rtspURL, verbosityLevel, applicationName,
             tunnelOverHTTPPortNum, socketNumToServer),
iter(NULL), session(NULL), subsession(NULL), streamTimerTask(NULL), duration(0.0), delegate(NULL) {
}

CZRTSPClientCore::~CZRTSPClientCore() {
    delete(iter);
    if (session != NULL) {
        // Need to delete "session", and unschedule "streamTimerTask" (if set).
        UsageEnvironment &env = session->envir();
        env.taskScheduler().unscheduleDelayedTask(streamTimerTask);
        Medium::close(session);
    }
    if (delegate) {
        delegate->streamShutdownCallback();
    }
}
