//
//  CZRTSPThumbnailQueue.h
//  Matscope
//
//  Created by Ralph Jin on 10/24/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CZRTSPThumbnailQueue;

@protocol CZRTSPThumbnailAcquirer <NSObject>

@required
- (NSURL*)rtspUrl;
- (CGSize)liveResolution;

@optional
- (void)thumbnailQueue:(CZRTSPThumbnailQueue *)queue didGetThumbnail:(UIImage *)thumbnail;

@end

/*! RTSP camera thumbnail update queue.
 * Queue the RTSP client, so that avoid concurrency issue.
 */
@interface CZRTSPThumbnailQueue : NSObject

+ (CZRTSPThumbnailQueue *)sharedInstance;

- (void)appendAcquirer:(id<CZRTSPThumbnailAcquirer>)acquirer;
- (void)removeAcquirer:(id<CZRTSPThumbnailAcquirer>)acquirer;

/*! clear acquirer queue, so that the pendding acquirers won't acquire thumnail any more.
 * But, in processing acquires won't be cleared.
 */
- (void)clearQueue;

@end
