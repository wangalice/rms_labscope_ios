//
//  CZMultiFramedRTPSink.h
//  Matscope iPhone
//
//  Created by Ralph Jin on 6/1/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#ifndef __Hermes__CZMultiFramedRTPSink__
#define __Hermes__CZMultiFramedRTPSink__

#import <CoreFoundation/CoreFoundation.h>

#ifdef __OBJC__
@class CZPacketSender;
#else
class CZPacketSender;
#endif

class CZMultiFramedRTPSink {
public:
    CZMultiFramedRTPSink(const char *ipAddress, uint16_t ports);
    ~CZMultiFramedRTPSink();
    
    void enqueueFrame(const void *frameBuffer, unsigned int length, struct timeval timestamp);
    
    uint16_t bindingPort();
    
private:
    void buildAndSendPacket(const uint8_t *data, uint32_t offset, uint32_t length, uint32_t timestamp);
    
    bool fIsLastPacket;
    bool fIsFirstPacket;
    bool fIsMultiPacket;
    
    uint32_t fSSRC;
    uint8_t fRTPPayloadType;
    uint16_t fSeqNo;
    
    CZPacketSender *fPacketSender;

    // avoid copy
    CZMultiFramedRTPSink(const CZMultiFramedRTPSink &rhs);
    CZMultiFramedRTPSink& operator=(const CZMultiFramedRTPSink &rhs);
};

#endif  // !__Hermes__CZMultiFramedRTPSink__
