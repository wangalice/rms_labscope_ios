//
//  CZRTSPClientCore.h
//  Hermes
//
//  Created by Halley Gu on 2/9/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef CZRTSPClientCore_H
#define CZRTSPClientCore_H

#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>

class CZRTSPFrameConsuming;

/**
 * Extended RTSP client object which maintains its own per-stream state.
 */
class CZRTSPClientCore: public RTSPClient {
public:
    static CZRTSPClientCore *createNew(UsageEnvironment &env,
                                       char const *rtspURL,
                                       int verbosityLevel = 0,
                                       char const *applicationName = NULL,
                                       portNumBits tunnelOverHTTPPortNum = 0,
                                       int socketNumToServer = -1);
    
protected:
    CZRTSPClientCore(UsageEnvironment &env, char const *rtspURL,
                     int verbosityLevel, char const *applicationName,
                     portNumBits tunnelOverHTTPPortNum, int socketNumToServer);
    
    virtual ~CZRTSPClientCore();
    
public:
    MediaSubsessionIterator *iter;
    MediaSession *session;
    MediaSubsession *subsession;
    TaskToken streamTimerTask;
    double duration;
    CZRTSPFrameConsuming *delegate;
};

#endif // CZRTSPClientCore_H
