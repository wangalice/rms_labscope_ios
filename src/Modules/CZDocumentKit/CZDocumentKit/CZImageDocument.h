//
//  CZImageDocument.h
//  DocManager
//
//  Created by Li, Junlin on 4/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CZIKit/CZIKit.h>

extern const CGFloat CZImageDocumentThumbnailWidth;
extern const CGFloat CZImageDocumentThumbnailHeight;

@class CZImageChannelProperties;

@interface CZImageBlock : NSObject

@property (nonatomic, readonly, retain) UIImage *image;
@property (nonatomic, readonly, assign) NSInteger componentBitCount;
@property (nonatomic, readonly, copy) NSString *pixelType;
@property (nonatomic, retain) CZImageChannelProperties *channelProperties;

- (instancetype)initWithImage:(UIImage *)image channelProperties:(CZImageChannelProperties *)channelProperties;

@end

@interface CZImageDocument : NSObject

@property (nonatomic, readonly, copy) NSArray<CZImageBlock *> *imageBlocks;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithImageBlocks:(NSArray<CZImageBlock *> *)imageBlocks NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithCZIDocument:(CZIDocument *)czi;

- (instancetype)initWithImage:(UIImage *)image grayscale:(BOOL)grayscale;   // Convenience initializer with one image block
- (instancetype)documentByReplacingImage:(UIImage *)image;
- (instancetype)documentByReplacingImage:(UIImage *)image grayscale:(BOOL)grayscale;

- (CGSize)imageSize;
- (BOOL)isMultichannelImage;

- (UIImage *)outputImage;
- (UIImage *)outputThumbnail;
- (UIImage *)outputImageWithSize:(CGSize)size;

@end
