//
//  CZDocManager.m
//  Hermes
//
//  Created by Halley Gu on 1/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDocManager.h"

#import <QuartzCore/QuartzCore.h>
#import <CZToolbox/CZToolbox.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZIKit/CZIKit.h>
#import <CZFileKit/CZFileKit.h>
#import "CZElement+ROI.h"
#import "CZImageProperties.h"
#import "CZImagePropertiesPrivate.h"
#import "CZMultiphase.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZDefaultSettings+DocManager.h"
#import "CZRegions2DItemCollection.h"
#import "CZRegion2DItem.h"
#import "CZAnalyzer2dResult.h"
#import "CZScanLine.h"
#import "CZRegion2D.h"
#import "CZRegion2DInstanceItem.h"
#import "CZRegions2DItem.h"
#import "UIImage+WriteTIFF.h"
#import "UIImageCZIRepresentation.h"

NSString *const CZDocSaveDelegateKey = @"CZDocSaveDelegateKey";

@interface CZDocManager () <CZUndoManagerDelegate> {
    NSObject *_lockSaveFileObject;
}

@property (nonatomic, retain) NSTimer *backupTimer;
@property (nonatomic, retain) NSTimer *saveTimer;
@property (atomic, retain) NSData *backupData;
@property (nonatomic, assign) BOOL lockBackupImage;
@property (atomic, retain) NSMutableDictionary<NSString *, NSObject*> *saveFiles;
@property (nonatomic, assign) BOOL lockSave;
@property (nonatomic, assign) CZIFileSaveFormat saveFormat;
@property (nonatomic, readonly) CZUndoManager *undoManager;
@property (nonatomic, readwrite, copy) NSString *microscopeName;

- (void)updateModifiedByAction:(id<CZUndoAction>)anAction;

- (void)willBackup;
- (void)backupLoop:(NSTimer *)timer;

- (void)willSave;
- (void)saveLoop:(NSTimer *)timer;
- (void)didSave:(BOOL)success;

@end

@implementation CZDocManager

@synthesize multiphase = _multiphase;

+ (CZFileNameTemplate *)defaultFileNameTemplate {
    static CZFileNameTemplate *defaultFileNameTemplate = nil;
    if (defaultFileNameTemplate == nil) {
        defaultFileNameTemplate = [[CZFileNameTemplate alloc] init];
        
        NSString *defaultText = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultFileNameTemplateText];
        if ([defaultText length] == 0) {
            defaultText = @"Snap";
        }
        
        CZFileNameTemplateField *field;
        field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeFreeText
                                                        value:defaultText];
        [defaultFileNameTemplate addField:field];
        [field release];
        
        field = [[CZFileNameTemplateField alloc] initWithType:CZFileNameTemplateFieldTypeAutoNumber
                                                        value:@"3"];
        [defaultFileNameTemplate addField:field];
        [field release];
    }
    
    return defaultFileNameTemplate;
}

+ (uint32_t)phaseIDFromRegionsID:(uint32_t)regionsID {
    return regionsID / 2 - 1;  // TODO: need to be check with ZEN
}

+ (uint32_t)regionsIDFromPhaseID:(uint32_t)phaseID {
    return (phaseID + 1) * 2;  // TODO: need to be check with ZEN
}

- (instancetype)initWithDirectory:(NSString *)directory document:(CZImageDocument *)document {
    return [self initWithDirectory:directory document:document elementLayer:nil];
}

- (instancetype)initWithDirectory:(NSString *)directory document:(CZImageDocument *)document elementLayer:(CZElementLayer *)elementLayer {
    if (document == nil) {
        [self release];
        self = nil;
        return self;
    }
    
    self = [super init];
    if (self) {
        _directory = [directory copy];
        _document = [document retain];
        _elementLayer = [elementLayer retain];
        _lockSaveFileObject = [[NSObject alloc] init];
        
        // TODO: try to set _filePath = nil.
        _filePath = [[NSString alloc] initWithString:[_directory stringByAppendingPathComponent:@"sample.czi"]];
        
        _imageProperties = [[CZImageProperties alloc] init];
        _imageProperties.scaling = kNonCalibratedValue;
        
        _undoManager = [[CZUndoManager alloc] init];
        _undoManager.maxMemorySize = 128 * 1024 * 1024;  // 128 MB
        _undoManager.delegate = self;
        
        // Set the size of element layer by image size.
        if (_elementLayer == nil) {
            _elementLayer = [[CZElementLayer alloc] init];
            _elementLayer.scaling = kNonCalibratedValue;
        }
        CGSize imageSize = _document.imageSize;
        _elementLayer.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
        
        _backupTimer = nil;
        
        [self updateChannelProperties];
    }
    return self;
}

- (void)dealloc {
    [_directory release];
    [_document release];
    [_elementLayer release];
    [_undoManager release];
    [_imageProperties release];
    [_multiphase release];
    [_filePath release];
    [_originalFilePath release];
    [_fileNameGenerator release];
    [_microscopeName release];
    [_userInputFileNameText release];
    [_backupData release];
    [_saveFiles release];
    [_closeActionBlock release];
    [_lockSaveFileObject release];
    [_analyzer2dData release];
    [_analyzer2dResult release];
    [_grainSizeResult release];
    
    [self.backupTimer invalidate];
    self.backupTimer = nil;
    [self.saveTimer invalidate];
    self.saveTimer = nil;
    
    [super dealloc];
}

#pragma mark - Overridden methods

- (NSDate *)captureDate {
    return self.imageProperties.acquisitionDate;
}

- (void)setCalibratedScaling:(float)calibratedScaling {
    _imageProperties.scaling = calibratedScaling;
    _elementLayer.scaling = calibratedScaling;
    self.elementLayerModified = YES;
}

- (float)calibratedScaling {
    return _imageProperties.scaling;
}

- (void)setDocument:(CZImageDocument *)document {
    [document retain];
    [_document release];
    _document = document;
    
    CGSize imageSize = document.imageSize;
    _elementLayer.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    
    [self updateChannelProperties];
}

- (CZAnalyzer2dResult *)analyzer2dResult {
    if (_analyzer2dResult == nil) {  // lazy load |_analyzer2dResult| from |_analyzer2dData|
        if (_analyzer2dData) {
            _analyzer2dResult = [[CZAnalyzer2dResult alloc] init];
            
            CZBufferProcessor *bufferProcessor = [[CZBufferProcessor alloc] initWithMutableData:[NSMutableData dataWithData:_analyzer2dData]];
            [_analyzer2dResult read:bufferProcessor];
            [bufferProcessor release];
            
            // update multiphase area from |_analyzer2dResult|
            NSUInteger sumArea = 0;
            CZRegions2DItemCollection *regionsCollection = _analyzer2dResult.regions2dItemCollection;
            for (uint32_t i = 0; i < [self.multiphase phaseCount]; i++) {
                CZRegions2DItem *regions = [regionsCollection objectForKey:@([CZDocManager regionsIDFromPhaseID:i])];
                NSUInteger area = 0;
                for (CZRegion2DItem *regionItem in regions) {
                    area += [regionItem.region area];
                }
                CZPhase *phase = [self.multiphase phaseAtIndex:i];
                phase.area = area;
                sumArea += area;
            }
            
            if (self.multiphase.imagePixelCount > sumArea) {
                self.multiphase.remainingArea = self.multiphase.imagePixelCount - sumArea;
            }
            
            // TODO: here is workaround for wrong saved particle index, need to removed someday.
            if (self.multiphase.isParticlesMode) {
                NSArray *keys = [_analyzer2dResult.regions2dItemCollection allKeys];
                if (keys.count > 0 && [keys indexOfObject:@2] == NSNotFound) {
                    NSNumber *key = [keys firstObject];
                    CZRegions2DItem *regions = [_analyzer2dResult.regions2dItemCollection objectForKey:key];
                    [regions retain];
                    regions.regionClassID = 2;
                    [_analyzer2dResult.regions2dItemCollection removeAllObjects];
                    [_analyzer2dResult.regions2dItemCollection setObject:regions forKey:@2];
                    [regions release];
                }
            }
        }
    }
    
    return _analyzer2dResult;
}

#pragma mark - Public interfaces

- (CZMultiphase *)multiphase {
    if (_multiphase == nil) {
        _multiphase = [[CZMultiphase alloc] init];
    }
    
    return _multiphase;
}

- (CZElement *)roiRegion {
    return _multiphase.roiRegion;
}

- (void)setRoiRegion:(CZElement *)roiRegion {
    self.multiphase.roiRegion = roiRegion;
}

- (BOOL)isModified {
    return ([self isImageModified] ||
            [self isElementLayerModified] ||
            [self isMultichannelModified] ||
            [self isMultiphaseModified] ||
            [self isGrainSizeModified]);
}

- (void)clearModifiedFlag {
    self.imageModified = NO;
    self.elementLayerModified = NO;
    self.multichannelModified = NO;
    self.multiphaseModified = NO;
    self.grainSizeModified = NO;
}

- (void)updateImagePropertyFrom:(CZMicroscopeModel *)model updateFilePath:(BOOL)updateFilePath {
    BOOL firstTime = self.imageProperties.acquisitionDate == nil;
    if (firstTime) {
        NSDate *now = [NSDate date];
        self.imageProperties.microscopeManufacturer = @"ZEISS";
        self.imageProperties.microscopeName = [model name];
        self.imageProperties.microscopeModel = [model modelName];
        self.imageProperties.operatorName = [[CZDefaultSettings sharedInstance] operatorName];
        self.imageProperties.acquisitionDate = now;
        self.imageProperties.cameraModel = [model cameraModelName];
        
        CZZoomLevel *zoomLevel = [model zoomForType:kZoomTypeCameraAdapter];
        if (zoomLevel) {
            self.imageProperties.cameraAdapter = @(zoomLevel.magnification);
        } else {
            self.imageProperties.cameraAdapter = nil;
        }
        
        if ([self.imageProperties.microscopeModel isEqualToString:kModelNameMacroImage]) {
             self.microscopeName = kModelNameMacroImage;
        } else {
             self.microscopeName = model.name;
        }
        self.saveFormat = [[CZDefaultSettings sharedInstance] fileFormat];
    }

    CGFloat scaling = [model currentFOVWidth] / self.document.imageSize.width;
    self.imageProperties.scaling = scaling;
    self.elementLayer.scaling = scaling;
    
    if ([model isKindOfClass:[CZMicroscopeModelStereo class]]) {
        const NSUInteger currentPosition = model.currentZoomClickStopPosition;
        self.imageProperties.zoom = @([[model zoomClickStopAtPosition:currentPosition] magnification]);
        NSNumber *totalMagnification = @([(CZMicroscopeModelStereo *)model totalMagnificationAtPosition:currentPosition]);
        if (totalMagnification && [totalMagnification floatValue] > 0) {
            self.imageProperties.totalMagnification = totalMagnification;
        } else {
            self.imageProperties.totalMagnification = nil;
        }
        
        // calculate eyepiece magnification
        float eyepieceZoom = [[model additionalZoomForType:kZoomTypeAdditionalEyepiece] floatValue];
        if (eyepieceZoom <= 0) {
            eyepieceZoom = 1;
        }
        CZZoomLevel *zoomLevel = [model zoomForType:kZoomTypeEyepiece];
        if (zoomLevel && zoomLevel.magnification > 0) {
            eyepieceZoom *= zoomLevel.magnification;
        }
        
        self.imageProperties.eyepieceMagnification = @(eyepieceZoom);
        self.imageProperties.objectiveModel = [model zoomForType:kZoomTypeStereoObjective].name;
    } else {
        self.imageProperties.totalMagnification = nil;
        CZObjective *objective = [model.nosepiece objectiveAtCurrentPosition];
        if (objective.matID) {
            self.imageProperties.objectiveModel = objective.objectiveName;
        }
    }
    
    [self updateUnitOfElementLayer];
    
    if (updateFilePath) {
        if (firstTime) {
            NSString *finalFileName = [self uniqueFileName];
            CZLogv(@"final file name: %@", finalFileName);
            // Fix bug 18700 --Ding Yu
//            NSString *filePath = [[[CZAppDelegate get].documentPath stringByAppendingPathComponent:finalFileName] copy];
            NSString *filePath = [[self.directory stringByAppendingPathComponent:finalFileName] copy];
            self.filePath = filePath;
            [filePath release];
        } else {
            [self updateFilePath];
        }
    }
}

- (void)updateImagePropertiesMicroscopeModel:(NSString *)microscopeModel {
    self.imageProperties.microscopeModel = [microscopeModel copy];
}

- (void)updateImagePropertiesObjectiveModel:(NSString *)objectiveModel {
    self.imageProperties.objectiveModel = [objectiveModel copy];
}

- (void)updateImagePropertiesTotalMagnification:(NSNumber *)totalMagnification {
    self.imageProperties.totalMagnification = totalMagnification;
}

- (void)updateImagePropertiesEyepieceMagnification:(NSNumber *)eyepieceMagnification {
    self.imageProperties.eyepieceMagnification = eyepieceMagnification;
}

- (void)updateImagePropertiesZoom:(NSNumber *)zoom {
    self.imageProperties.zoom = zoom;
}

- (void)updateImagePropertiesCameraModel:(NSString *)cameraModel {
    self.imageProperties.cameraModel = [cameraModel copy];
}

- (void)updateImagePropertiesCameraAdapter:(NSNumber *)cameraAdapter {
    self.imageProperties.cameraAdapter = cameraAdapter;
}

#pragma mark - CZUndoManaging methods

- (void)pushAction:(id<CZUndoAction>)anAction {
    [_undoManager pushAction:anAction];
    [self updateModifiedByAction:anAction];
}

- (void)undo {
    [_undoManager undo];
}

- (void)redo {
    [_undoManager redo];
}

- (BOOL)canUndo {
    return [_undoManager canUndo];
}

- (BOOL)canRedo {
    return [_undoManager canRedo];
}

#pragma mark - Backup methods

- (void)discardBackup {
    [self.backupTimer invalidate];
    self.backupTimer = nil;
    
    NSString *backupFilePath = [self.directory stringByAppendingPathComponent:kBackupFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:backupFilePath]) {
        return;
    }
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:backupFilePath error:&error];
    if (error) {
        //TODO: will need to remove if it doesn't happen
        dispatch_async(dispatch_get_main_queue(), ^{
//            CZAlert *alert = [[CZAlert alloc] initWithTitle:nil
//                                                    message:error.localizedDescription];
//            [alert addButtonWithTitle:L(@"OK") block:NULL];
//            [alert presentInViewController:nil animated:YES];
//            [alert release];
        });
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setURL:nil forKey:kURLToBeBackup];
}

- (void)scheduleBackup {
    if (!self.backupTimer) {
        self.backupTimer = [NSTimer scheduledTimerWithTimeInterval:30
                                                            target:self
                                                          selector:@selector(backupLoop:)
                                                          userInfo:nil
                                                           repeats:NO];
    }
}

- (void)quickBackup {
    if (!_lockBackupImage) {
        _lockBackupImage = TRUE;
        
        NSData *backupData = UIImageCZIRepresentation(self);
        self.backupData = backupData;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self willBackup];
        });
    }
}

- (void)didBackup {
    _lockBackupImage = FALSE;
}

- (void)willBackup {
    @synchronized (_lockSaveFileObject) {
        if (!self.backupData || !self.filePath) {
            return;
        }
        
        [self discardBackup];
        
        if (!self.isModified) {
            return;
        }
        
        NSString *backupFilePath = [self.directory stringByAppendingPathComponent:kBackupFileName];
        NSURL *backupURL = [[NSURL alloc] initFileURLWithPath:backupFilePath isDirectory:FALSE];
        
        [self.backupData writeToFile:backupURL.relativePath atomically:YES];
        self.backupData = nil;
        [backupURL release];
        
        CZLogv(@"File path: [%@] to be backup [%@]", self.filePath, backupFilePath);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSURL *fileURLToBeBackup = [[NSURL alloc] initFileURLWithPath: self.filePath isDirectory:FALSE];
        [defaults setURL:fileURLToBeBackup forKey:kURLToBeBackup];
        [fileURLToBeBackup release];
        
        [defaults setBool:self.isImageSnapped forKey:kIsImageSnapped];
        [defaults setBool:self.isImageNew forKey:kIsImageNew];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didBackup];
        });
    }
}

- (void)backupLoop:(NSTimer *)timer {
    [self.backupTimer invalidate];
    self.backupTimer = nil;
    
    [self quickBackup];
}

+ (NSString*)filePathToBeBackupInDirectory:(NSString *)directory {
    NSString *backupFilePath = [directory stringByAppendingPathComponent:kBackupFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:backupFilePath]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setURL:nil forKey:kURLToBeBackup];
        return nil;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSURL *fileURLToBeBackup = [defaults URLForKey:kURLToBeBackup];
    return fileURLToBeBackup.relativePath;
}

#pragma mark - Saving methods

- (void)didSave:(BOOL)success {
    [self.delegate docManager:self didSaveFiles:self.saveFiles.allKeys success:success];
    
    BOOL isImageNew = self.isImageNew;
    if (success && [[NSFileManager defaultManager] fileExistsAtPath:self.filePath]) {
        // Remove original file if user change to another objective and cause file name changed.
        if (self.originalFilePath && ![self.filePath isEqualToString:self.originalFilePath]) {
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:self.originalFilePath error:&error];
            if (error) {
                CZLogv(@"CZDocManager remove original file error: %@", error);
            }
            
            if (self.saveFormat == kCZIAndJPG) {
                NSString *jpgFilePath = [self.originalFilePath stringByDeletingPathExtension];
                jpgFilePath = [jpgFilePath stringByAppendingPathExtension:@"jpg"];
                
                error = nil;
                [[NSFileManager defaultManager] removeItemAtPath:jpgFilePath error:&error];
                if (error) {
                    CZLogv(@"CZDocManager remove original file error: %@", error);
                }
            }

            self.originalFilePath = nil;
        }
        
        // reset flags
        [self.undoManager removeAllActions];
        self.isImageNew = NO;
        
        [self discardBackup];
    }
    
    NSArray <NSString *> *saveFilePaths = self.saveFiles.allKeys;
    if (saveFilePaths.count > 0) {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:kCZFileUpdateNotification
                                          object:nil
                                        userInfo:@{kCZFileUpdateNotificationFileList : saveFilePaths,
                                                   kCZFileUpdateNotificationIsImageNew : @(isImageNew)}];
    }
    
    _lockSave = NO;
}

- (void)saveFileInQueue {
    @synchronized (_lockSaveFileObject) {
        NSMutableArray <NSString *> *failedFilePaths = [NSMutableArray array];
        
        BOOL success = YES;
        for (NSString *filePath in [self.saveFiles allKeys]) {
            id data = self.saveFiles[filePath];
            
            BOOL ok = YES;
            NSError *error = nil;
            if ([data isKindOfClass:[NSData class]]) {
                ok = [data writeToFile:filePath  options:NSDataWritingAtomic error:&error];
            } else if ([data isKindOfClass:[UIImage class]]) {
                ok = [data writeTifToFile:filePath usingImageProperties:self.imageProperties];
            }
            if (!ok || error) {
                success = NO;
                [failedFilePaths addObject:filePath];
                
                CZLogv(@"Save %@ with error, %@", filePath, [error localizedDescription]);
            }
        }
        
        [self.saveFiles removeObjectsForKeys:failedFilePaths];
        
        if (success) {
            [self clearModifiedFlag];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didSave:success];
            self.saveFiles = nil;
        });
    }
}

- (void)willSave {
    if (!self.saveFiles) {
        return;
    }
    
    if (self.closeActionBlock) {
        self.closeActionBlock(self.closeActionTarget);
    }
    
    // change filePath, if user choose rename e.g.
    BOOL fileNameChanged = YES;
    for (NSString *filePath in self.saveFiles.allKeys) {
        if ([filePath isEqualToString:self.filePath]) {
            fileNameChanged = NO;
            break;
        }
    }
    
    if (fileNameChanged) {
        self.filePath = _saveFiles.allKeys[0];
    }
    
    [self.delegate docManager:self willSaveFiles:self.saveFiles.allKeys];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self saveFileInQueue];
    });
}

- (void)scheduleSave:(CZIFileSaveFormat)format withDelegate:(id<CZDocSaveDelegate>)delegate {
    _saveFormat = format;
    
    if (!self.saveTimer) {
        [self retain]; // to avoid become zombie when timer fired
        [self.saveTimer invalidate];
        
        NSMapTable *mapTable = [NSMapTable strongToWeakObjectsMapTable];
        [mapTable setObject:delegate forKey:CZDocSaveDelegateKey];
        
        self.saveTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                          target:self
                                                        selector:@selector(saveLoop:)
                                                        userInfo:mapTable
                                                         repeats:NO];
    }
}

- (void)scheduleSaveInDefaultFormatWithDelegate:(id<CZDocSaveDelegate>)delegate {
    // Save file in default file format.
    CZIFileSaveFormat format = kCZI;
    if (self.isImageSnapped && [delegate respondsToSelector:@selector(saveFormatForSnappedImage)]) {
        format = [delegate saveFormatForSnappedImage];
    } else {
        NSString *extension = [self.filePath pathExtension];
        CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:extension];
        if (fileFormat == kCZFileFormatJPEG) {
            format = kJPGWithMarkup;
        } else if (fileFormat == kCZFileFormatTIF) {
            format = kTIFWithMarkup;
        }
    }
    
    [self scheduleSave:format withDelegate:delegate];
}

- (void)checkFilesOverwriteWithDelegate:(id<CZDocSaveDelegate>)delegate {
    NSString *sourceFile = nil;
    if (self.saveFiles.allKeys.count > 1) {
        for (NSString *file in self.saveFiles.allKeys) {
            if ([[file pathExtension] isEqualToString:@"czi"]) {
                sourceFile = [file lastPathComponent];
                break;
            }
        }
    } else if (self.saveFiles.allKeys.count == 1) {
        sourceFile = self.saveFiles.allKeys[0];
        sourceFile = [sourceFile lastPathComponent];
    }
    
    if (sourceFile == nil) {
        // nothing to save
        return;
    }
    
    NSMutableArray *destinationFiles = [[NSMutableArray alloc] init];
    NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:self.directory];
    NSString *file;
    while (file = [dirEnum nextObject]) {
        [destinationFiles addObject:file];
    }
    
    CZIFileSaveOverwriteOption option = [CZDefaultSettings sharedInstance].fileOverwriteOption;
    CZUserInteractionTracker *tracker = [CZUserInteractionTracker defaultTracker];;
    void (^overwriteYesBlock)(NSDictionary *) = ^(NSDictionary *overwriteFilesDic) {
        if (option == kCZAskOnSave) {
            NSDictionary *parameters = @{@"is_ask_on_save_option_selected":@"YES"};
            [tracker logEventWithCategory:kCZUIActionEventCategory
                                    event:@"file_save_overwrite_event"
                                     time:[NSDate date]
                               parameters:parameters
                         postNotification:YES];
        }

        [self willSave];
    };
    
    void (^overwriteNoBlock)(NSDictionary *) = ^(NSDictionary *overwriteFilesDic) {
        NSMutableDictionary *newSaveFiles = [[NSMutableDictionary alloc] init];
        if (overwriteFilesDic.count > 0) {
            if (option == kCZAskOnSave) {
                NSDictionary *parameters = @{@"is_ask_on_save_option_selected":@"NO"};
                [tracker logEventWithCategory:kCZUIActionEventCategory
                                        event:@"file_save_overwrite_event"
                                         time:[NSDate date]
                                   parameters:parameters
                             postNotification:YES];
            }
            
            for (NSString *filePath in self.saveFiles.allKeys) {
                NSString *overwriteFile = overwriteFilesDic[[filePath lastPathComponent]];
                if (overwriteFile == nil && ([[filePath pathExtension] isEqualToString:@"jpg"] || [[filePath pathExtension] isEqualToString:@"tif"])) {
                    NSString *cziFilePath = [[filePath stringByDeletingPathExtension] stringByAppendingPathExtension:@"czi"];
                    overwriteFile = overwriteFilesDic[[cziFilePath lastPathComponent]];
                    if (overwriteFile == nil) {
                        continue;
                    } else {
                        if ([[filePath pathExtension] isEqualToString:@"jpg"]) {
                            overwriteFile = [[overwriteFile stringByDeletingPathExtension] stringByAppendingPathExtension:@"jpg"];
                        } else if ([[filePath pathExtension] isEqualToString:@"tif"]) {
                            overwriteFile = [[overwriteFile stringByDeletingPathExtension] stringByAppendingPathExtension:@"tif"];
                        }
                    }
                }
                
                NSString *parentPath = [filePath stringByDeletingLastPathComponent];
                if (overwriteFile == nil) {
                    [newSaveFiles setObject:self.saveFiles[filePath]
                                     forKey:filePath];
                }
                else if (overwriteFile != [filePath lastPathComponent]) {
                    [newSaveFiles setObject:self.saveFiles[filePath]
                                     forKey:[parentPath stringByAppendingPathComponent:overwriteFile]];
                } else {
                    [newSaveFiles setObject:self.saveFiles[filePath] forKey:filePath];
                }
            }
            
            self.saveFiles = newSaveFiles;
        }
        [newSaveFiles release];
        
        self.originalFilePath = nil;
        
        [self willSave];
        self.isImageSnapped = NO;
    };
    
    void (^forceOverwriteBlock)(void) = ^(void) {
        [self willSave];
    };
    
    [delegate performOverwriteWithSourceFiles:@[sourceFile] destinationFiles:destinationFiles overwirteYesBlock:overwriteYesBlock overwirteNoBlock:overwriteNoBlock forceOverwriteBlock:forceOverwriteBlock];
    
    [destinationFiles release];
}

- (void)saveLoop:(NSTimer *)timer {
    NSMapTable *mapTable = timer.userInfo;
    id<CZDocSaveDelegate> saveDelegate = [mapTable objectForKey:CZDocSaveDelegateKey];
    
    // release timer
    [self.saveTimer invalidate];
    self.saveTimer = nil;
    
    if (_lockSave) {
        CZLogv(@"save is lock");
        [self release];
        return;
    }
    
    if (self.document == nil) {
        CZLogv(@"image not set");
        [self release];
        return;
    }
    
    _lockSave = TRUE;
    
    NSMutableDictionary *saveFiles = [[NSMutableDictionary alloc] init];
    self.saveFiles = saveFiles;
    [saveFiles release];
    
    @autoreleasepool {
        CGFloat compression = 1.0;
        if (_saveFormat == kJPGMedium) {
            compression = 0.5;
        }

        if (_saveFormat == kCZI || _saveFormat == kCZIAndJPG || _saveFormat == kCZIAndTIF) {
            NSData *data = UIImageCZIRepresentation(self);
            
            //Double check to make sure the file path extension is czi
            NSString *cziPath = self.filePath;
            if (![[self.filePath pathExtension] isEqualToString:kFileExtensionCZI]) {
                cziPath = [[self.filePath stringByDeletingPathExtension] stringByAppendingPathExtension:kFileExtensionCZI];
            }
            [_saveFiles setObject:data forKey:cziPath];
        }
        
        UIImage *image = nil;
        if (_saveFormat == kCZIAndJPG || _saveFormat == kJPGWithMarkup || _saveFormat == kTIFWithMarkup || _saveFormat == kCZIAndTIF) {
            image = [self imageWithBurninAnnotations];
        } else if (_saveFormat == kJPGWithoutMarkup || _saveFormat == kJPGMedium || _saveFormat == kTIFWithoutMarkup) {
            image = [self.document outputImage];
            
            if ([self.elementLayer elementCount] > 0) {
                CZDeleteAllElementAction *action = [[CZDeleteAllElementAction alloc] init];
                action.layer = self.elementLayer;
                [self pushAction:action];
                [action release];
            }
        }
        
        NSArray<CZImageBlock *> *imageBlocks = self.document.imageBlocks;
        BOOL grayscale = imageBlocks.count == 1 && imageBlocks[0].channelProperties.channelColor == nil && imageBlocks[0].channelProperties.grayscale.boolValue == YES;
        if (_saveFormat == kCZIAndJPG ||
            _saveFormat == kJPGWithoutMarkup ||
            _saveFormat == kJPGMedium ||
            _saveFormat == kJPGWithMarkup) {
            if (grayscale && ![self.elementLayer hasNonScaleBarElements]) {
                image = [CZCommonUtils grayImageFromImage:image];
            }
            NSData *jpgImageData = [self.imageProperties JPEGRepresentationOfImage:image compression:compression];
            NSString *jpgPath = [[self.filePath stringByDeletingPathExtension] stringByAppendingPathExtension:kFileExtensionJPEG];
            
            [_saveFiles setObject:jpgImageData forKey:jpgPath];
        } else if (_saveFormat == kCZIAndTIF || _saveFormat == kTIFWithMarkup || _saveFormat == kTIFWithoutMarkup) {
            if (grayscale && ![self.elementLayer hasNonScaleBarElements]) {
                image = [CZCommonUtils grayImageFromImage:image];
            }
            NSString *tifPath = [[self.filePath stringByDeletingPathExtension] stringByAppendingPathExtension:kFileExtensionTIF];
            [_saveFiles setObject:image forKey:tifPath]; //so far only tiff is saved from UIImage
        }
        
        [self checkFilesOverwriteWithDelegate:saveDelegate];
    }
    
    [self release];
}

- (UIImage *)imageWithBurninAnnotations {
    CGSize imageSize = self.document.imageSize;
    return [self imageByBurningAnnotationsOnImage:[self.document outputImage] inSize:imageSize];
}

- (UIImage *)imageWithBurninAnnotationsInSize:(CGSize)destinationSize {
    return [self imageByBurningAnnotationsOnImage:[self.document outputImage] inSize:destinationSize];
}

- (UIImage *)imageByBurningAnnotationsOnImage:(UIImage *)image {
    CGSize imageSize = self.document.imageSize;
    return [self imageByBurningAnnotationsOnImage:image inSize:imageSize];
}

- (UIImage *)imageByBurningAnnotationsOnImage:(UIImage *)image inSize:(CGSize)destinationSize {
    if (self.elementLayer.elementCount == 0 && CGSizeEqualToSize(image.size, destinationSize)) {
        return [[image retain] autorelease];
    }

    __block UIImage *resultImage = nil;
    
    if (![[NSThread currentThread] isMainThread]) {  // Call this method always in main thread, see below also.
        dispatch_sync(dispatch_get_main_queue(), ^{
            resultImage = [self imageByBurningAnnotationsOnImage:image inSize:destinationSize];
            [resultImage retain];
        });
        return [resultImage autorelease];
    } else {
        @autoreleasepool {
            CGSize sourceSize = image.size;
            if (sourceSize.width < destinationSize.width && sourceSize.height < destinationSize.height) {
                destinationSize = sourceSize;
            }

            // Return original image if size is invalid.
            if (!image || sourceSize.width == 0 || sourceSize.height == 0 ||
                destinationSize.width == 0 || destinationSize.height == 0) {
                return image;
            }

            CGFloat widthRatio = destinationSize.width / sourceSize.width;
            CGFloat heightRatio = destinationSize.height / sourceSize.height;

            if (widthRatio < heightRatio) {
                destinationSize.height = round(destinationSize.width * sourceSize.height / sourceSize.width);
                destinationSize.width = round(destinationSize.width);
            } else {
                destinationSize.width = round(destinationSize.height * sourceSize.width / sourceSize.height);
                destinationSize.height = round(destinationSize.height);
            }

            UIGraphicsBeginImageContext(destinationSize);
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            CGContextScaleCTM(context, destinationSize.width / sourceSize.width, destinationSize.height / sourceSize.height);
            CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
            [image drawAtPoint:CGPointZero];

            if (self.elementLayer.elementCount > 0) {
                CALayer *markupLayer = [self.elementLayer newLayer];  // CALayer is not safe to render in thread besides main thread.
                [markupLayer removeAllAnimations];
                [markupLayer renderInContext:UIGraphicsGetCurrentContext()];
                [markupLayer release];
            }

            resultImage = [UIGraphicsGetImageFromCurrentImageContext() retain];
            
            UIGraphicsEndImageContext();
        }
        
        return [resultImage autorelease];
    }
}

- (void)drawMultiphaseRegionsOnContext:(CGContextRef)context {
    [self drawMultiphaseRegionsOnContext:context withVisibleIDs:nil];
}

- (void)drawMultiphaseRegionsOnContext:(CGContextRef)context withVisibleIDs:(NSSet *)visibleIDs {
    if (self.multiphase.phaseCount == 0) {
        return;
    }
    
    CGSize imageSize = self.document.imageSize;
    if (CGSizeEqualToSize(CGSizeZero, imageSize)) {
        return;
    }
    
    CZScanLine *scanline = [[CZScanLine alloc] initWithWidth:imageSize.width height:imageSize.height];
    
    CZRegions2DItemCollection *regionCollection = self.analyzer2dResult.regions2dItemCollection;
    
    // Fill the visible items.
    for (NSNumber *key in regionCollection) {
        uint32_t index = [key unsignedIntValue];
        index = [CZDocManager phaseIDFromRegionsID:index];
        
        UIColor *color = nil;
        if (index < self.multiphase.phaseCount) {
            CZPhase *phase = [self.multiphase phaseAtIndex:index];
            color = phase.color;
        }
        
        if (color) {
            CZRegions2DItem *regionsItem = [regionCollection objectForKey:key];
            for (CZRegion2DItem *regionItem in regionsItem) {
                CZRegion2D *region = regionItem.region;
                if (visibleIDs == nil || [visibleIDs containsObject:@(region.idx)]) {
                    [region fillInImage:scanline];
                }
            }
            
            CGContextSetFillColorWithColor(context, [color CGColor]);
            [scanline drawInContext:context];
            [scanline clear];
        }
    }
    
    [scanline release];
}

- (NSString *)uniqueFileName {
    return [self uniqueFileNameExcludedName:nil];
}

- (NSString *)uniqueFileNameExcludedName:(NSString *)excludedName {
    if (self.fileNameGenerator == nil) {
        _fileNameGenerator = [[CZFileNameGenerator alloc] init];
    }
    
    if (self.isImageSnapped) {
        switch (self.saveFormat) {
            case kCZI:
            case kCZIAndJPG:
            case kCZIAndTIF:
                self.fileNameGenerator.extension = @"czi";
                break;
            case kJPGWithMarkup:
            case kJPGWithoutMarkup:
            case kJPGMedium:
                self.fileNameGenerator.extension = @"jpg";
                break;
            case kTIFWithMarkup:
            case kTIFWithoutMarkup:
                self.fileNameGenerator.extension = @"tif";
                break;
        }
    } else {
        self.fileNameGenerator.extension = [self.filePath pathExtension];
    }
    
    if (self.fileNameGenerator.fileNameTemplate == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSURL *defaultTemplateURL = [defaults URLForKey:kDefaultFileTemplateURL];
        
        // load default file name template
        CZFileNameTemplate *fileNameTemplate = nil;
        if (defaultTemplateURL) {
            NSString *defaultTemplatePath = [[defaults URLForKey:kDefaultFileTemplateURL] relativePath];
            fileNameTemplate = [[CZFileNameTemplate alloc] initWithContentsOfFile:defaultTemplatePath];
        }
        
        if (fileNameTemplate == nil || ![fileNameTemplate isValid]) {
            [fileNameTemplate release];
            fileNameTemplate = [CZDocManager defaultFileNameTemplate];  // use hard code default file name template
            [fileNameTemplate retain];
        }
        
        self.fileNameGenerator.fileNameTemplate = fileNameTemplate;
        [fileNameTemplate release];
    }
    
    if (self.isImageSnapped) {
        self.fileNameGenerator.operatorName = [CZDefaultSettings sharedInstance].operatorName;
        self.fileNameGenerator.microscope = self.microscopeName;
    } else {
        self.fileNameGenerator.operatorName = self.imageProperties.operatorName;
        self.fileNameGenerator.microscope = self.imageProperties.microscopeName;
    }
    self.fileNameGenerator.captureDate = self.captureDate;
    self.fileNameGenerator.objective = self.imageProperties.magnificationFactorString;
    [self.fileNameGenerator useDefaultValueWhenEmpty];
    
    NSString *fileName = nil;
    if (excludedName) {
        fileName = [self.fileNameGenerator uniqueFileNameInDirectory:self.directory excludedName:excludedName];
    } else {
        fileName = [self.fileNameGenerator uniqueFileNameInDirectory:self.directory];
    }
    
    return fileName;
}

- (void)updateUnitOfElementLayer {
    self.elementLayer.prefersBigUnit = [CZMicroscopeModel shouldPreferBigUnitByModelName:self.imageProperties.microscopeModel];
}

/** update file path when snapped image is modified. */
- (void)updateFilePath {
    CZDocManager *docManager = self;
    if (docManager.isImageSnapped) {
        NSString *oldFilePath = docManager.filePath;
        NSString *excludedFileName;
        if (docManager.originalFilePath) {
            excludedFileName = [docManager.originalFilePath lastPathComponent];
        } else {
            excludedFileName = [oldFilePath lastPathComponent];
        }
        NSString *newFileName = [docManager uniqueFileNameExcludedName:excludedFileName];
        NSString *newFilePath = [self.directory stringByAppendingPathComponent:newFileName];
        if (! [newFilePath isEqualToString:oldFilePath]) {
            if (docManager.originalFilePath == nil) {
                docManager.originalFilePath = oldFilePath;
            } else if ([newFilePath isEqualToString:docManager.originalFilePath]) {
                docManager.originalFilePath = nil;
            }
            
            docManager.filePath = newFilePath;
        }
    }
}

#pragma mark - Other private methods

- (void)updateModifiedByAction:(id<CZUndoAction>)anAction {
    CZUndoActionType type = [anAction type];
    if (type & CZUndoActionTypeImage) {
        [self setImageModified:YES];
    }
    if (type & CZUndoActionTypeAnnotation) {
        [self setElementLayerModified:YES];
    }
    if (type & CZUndoActionTypeMultichannel) {
        [self setMultichannelModified:YES];
    }
    if (type & CZUndoActionTypeMultiphase) {
        [self setMultiphaseModified:YES];
    }
    if (type & CZUndoActionTypeGrainSize) {
        [self setGrainSizeModified:YES];
    }
}

- (void)updateChannelProperties {
    NSMutableArray<CZImageChannelProperties *> *channels = [NSMutableArray arrayWithCapacity:self.document.imageBlocks.count];
    for (CZImageBlock *imageBlock in self.document.imageBlocks) {
        CZImageChannelProperties *channelProperties = imageBlock.channelProperties;
        if (channelProperties == nil) {
            channelProperties = [[[CZImageChannelProperties alloc] init] autorelease];
        }
        [channels addObject:channelProperties];
    }
    self.imageProperties.channels = channels;
    self.imageProperties.bitDepth = @(self.document.imageBlocks.firstObject.componentBitCount);
}

#pragma mark - CZUndoManagerDelegate Methods

- (void)didUndoAction:(id<CZUndoAction>)action {
    [self updateModifiedByAction:action];
}

- (void)didRedoAction:(id<CZUndoAction>)action {
    [self updateModifiedByAction:action];
}

@end
