//
//  UIImageCZIRepresentation.mm
//  Hermes
//
//  Created by Li, Junlin on 12/17/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "UIImageCZIRepresentation.h"
#import <CZIKit/CZIKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZAnalyzer2dPersister.h"
#import "CZAnalyzer2dResult.h"
#import "CZImageProperties.h"
#import "CZElementLayer+CZIMetadata.h"
#import "CZGrainSizeResult+CZIMetadata.h"

UIKIT_EXTERN CZIMetadata *exportMetaData(CZDocManager *docManager) {
    CZIMutableMetadata *metadata = [[CZIMutableMetadata alloc] init];
    
    CZImageProperties *imageProperties = docManager.imageProperties;
    
    CZIMutableMetadataNode *hardwareSettingNode = metadata.root.hardwareSetting;
    hardwareSettingNode.microscope.nodeAttribute.name = imageProperties.microscopeName;
    hardwareSettingNode.microscope.manufacturer.stringValue = imageProperties.microscopeManufacturer;
    
    metadata.root.customAttributes.stringValue = @"";
    
    //Export Information Node
    CZIMutableMetadataNode *informationNode = metadata.root.information;
    informationNode.user.nodeAttribute.id = @"0";
    
    // set application section
    informationNode.application.name.stringValue = L(@"ABOUT_PRODUCT_BIO_NAME");
    informationNode.application.version.stringValue = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    informationNode.institution.stringValue = @"";
    
    NSDate *originalDate = imageProperties.acquisitionDate;
    if (originalDate) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZZ"];
        NSString *dateString = [formatter stringFromDate:originalDate];
        [formatter release];
        
        informationNode.document.creationDate.stringValue = dateString;
    }
    
    informationNode.document.userName.stringValue = docManager.imageProperties.operatorName;
    
    // append AcquisitionDateAndTime node
    if (originalDate) {
        informationNode.image.acquisitionDateAndTime.dateValue = originalDate;
    }
    
    informationNode.image.componentBitCount.integerValue = docManager.document.imageBlocks.firstObject.componentBitCount;
    informationNode.image.pixelType.stringValue = docManager.document.imageBlocks.firstObject.pixelType;
    informationNode.image.sizeX.integerValue = docManager.document.imageSize.width;
    informationNode.image.sizeY.integerValue = docManager.document.imageSize.height;
    
    NSMutableArray<NSString *> *lightSources = [NSMutableArray array];
    
    CZIMutableMetadataNode *dimensionsNode = informationNode.image.dimensions;
    [docManager.document.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        CZIMutableMetadataNode *channelNode = dimensionsNode.channels[index];
        channelNode.nodeAttribute.id = [NSString stringWithFormat:@"Channel:%d", index];
        channelNode.color.colorValue = imageBlock.channelProperties.channelColor ?: [UIColor whiteColor];
        channelNode.pixelType.stringValue = imageBlock.pixelType;
        channelNode.componentBitCount.integerValue = imageBlock.componentBitCount;
        
        // set exposure time
        NSNumber *exposureTime = imageBlock.channelProperties.exposureTimeInSeconds;
        if (exposureTime) {
            int exposureTimeInMicro = exposureTime.floatValue * 1e6f + 0.5f;
            NSString *exposureTimeInNano = [NSString stringWithFormat:@"%d000", exposureTimeInMicro];
            channelNode.exposureTime.stringValue = exposureTimeInNano;
        }
        
        // set gain
        NSNumber *gain = imageBlock.channelProperties.gain;
        channelNode.detectorSettings.detector.nodeAttribute.id = @"Detector:1";
        if (gain) {
            channelNode.detectorSettings.gain.integerValue = gain.integerValue;
        }
        
        channelNode.reflector.stringValue = imageBlock.channelProperties.filterSetName;
        channelNode.excitationWavelength.stringValue = imageBlock.channelProperties.wavelength;
        
        NSString *lightSource = imageBlock.channelProperties.lightSource;
        if (lightSource.length > 0) {
            NSInteger index = [lightSources indexOfObject:lightSource];
            if (index == NSNotFound) {
                [lightSources addObject:lightSource];
                index = lightSources.count;
            }
            NSString *lightSourceId = [NSString stringWithFormat:@"LightSource:%ld", (index + 1)];
            channelNode.lightSourcesSettings.lightSourceSettings.lightSource.nodeAttribute.id = lightSourceId;
            
            CZIMutableMetadataNode *lightSourceNode = informationNode.instrument.lightSources[index];
            lightSourceNode.nodeAttribute.id = lightSourceId;
            lightSourceNode.nodeAttribute.name = lightSource;
        }
    }];
    
    // add objective reference
    informationNode.image.objectiveSettings.objectiveRef.nodeAttribute.id = @"Objective:1";
    
    informationNode.image.microscopeRef.nodeAttribute.id = @"Microscope:1";
    
    // set objective
    CZIMutableMetadataNode *objectiveNode = informationNode.instrument.objectives[0];
    objectiveNode.nodeAttribute.id = @"Objective:1";
    if (imageProperties.objectiveModel) {
        objectiveNode.nodeAttribute.name = imageProperties.objectiveModel;
    }
    
    // set microscope mode
    CZIMutableMetadataNode *microscopeNode = informationNode.instrument.microscopes[0];
    microscopeNode.nodeAttribute.id = @"Microscope:1";
    if (imageProperties.microscopeModel) {
        microscopeNode.nodeAttribute.name = imageProperties.microscopeModel;
    }
    
    // set zoom and total magnification
    if (imageProperties.zoom) {
        informationNode.image.microscopeSettings.zoomSettings.magnification.doubleValue = imageProperties.zoom.doubleValue;
    }
    
    if (imageProperties.totalMagnification) {
        informationNode.image.microscopeSettings.eyepieceSettings.totalMagnification.doubleValue = imageProperties.totalMagnification.doubleValue;
    }
    
    // set camera mode
    if (imageProperties.cameraModel || imageProperties.microscopeModel) {
        CZIMutableMetadataNode *detectorNode = informationNode.instrument.detectors[0];
        detectorNode.nodeAttribute.id = @"Detector:1";
        detectorNode.nodeAttribute.name = imageProperties.cameraModel ?: imageProperties.microscopeModel;
        
        detectorNode.manufacturer.model.stringValue = imageProperties.cameraModel ?: imageProperties.microscopeModel;
        
        if (imageProperties.cameraAdapter) {
            NSString *string = [CZCommonUtils stringFromNumber:imageProperties.cameraAdapter precision:2];
            detectorNode.adapter.manufacturer.model.stringValue = [NSString stringWithFormat:@"%@x", string];
        }
    }
    
    dimensionsNode.t.startTime.stringValue = @"0001-01-01T00:00:00";
    
    //Export Scaling
    CZIMutableMetadataNode *scalingNode = metadata.root.scaling;
    if (docManager.elementLayer.isValidScaling) {
        //distance is meter/pixel, and currently our scaling is in micrometer
        NSString *distance = [NSString stringWithFormat:@"%g", docManager.elementLayer.scaling * 1e-6];
        
        CZIMutableMetadataNode *distanceXNode = CZIMutableMetadata.metadata.document.distance;
        distanceXNode.nodeAttribute.id = @"X";
        distanceXNode.value.stringValue = distance;
        distanceXNode.defaultUnitFormat.stringValue = @"\xC2\xB5m";
        [scalingNode.items appendNode:distanceXNode];
        
        CZIMutableMetadataNode *distanceYNode = CZIMutableMetadata.metadata.document.distance;
        distanceYNode.nodeAttribute.id = @"Y";
        distanceYNode.value.stringValue = distance;
        distanceYNode.defaultUnitFormat.stringValue = @"\xC2\xB5m";
        [scalingNode.items appendNode:distanceYNode];
    }
    
    // set scaling unit as pixel
    if (!docManager.elementLayer.isValidScaling) {
        scalingNode.autoScaling.type.stringValue = @"Measured";
        
        CZIMutableMetadataNode *pixelXNode = CZIMutableMetadata.metadata.document.pixel;
        pixelXNode.nodeAttribute.id = @"X";
        pixelXNode.value.stringValue = @"1";
        [scalingNode.items appendNode:pixelXNode];
        
        CZIMutableMetadataNode *pixelYNode = CZIMutableMetadata.metadata.document.pixel;
        pixelYNode.nodeAttribute.id = @"Y";
        pixelYNode.value.stringValue = @"1";
        [scalingNode.items appendNode:pixelYNode];
    }
    
    // set camera adapter
    if (imageProperties.cameraAdapter) {
        float cameraAdpater = [imageProperties.cameraAdapter floatValue];
        NSString *string = [CZCommonUtils stringFromFloat:cameraAdpater precision:2];
        string = [NSString stringWithFormat:@"CameraAdapter.%@x", string];
        scalingNode.autoScaling.cameraAdapter.stringValue = string;
        scalingNode.autoScaling.cameraAdapterMagnification.floatValue = cameraAdpater;
    }
    
    // set optovar magnification
    if (imageProperties.zoom) {
        scalingNode.autoScaling.optovarMagnification.floatValue = imageProperties.zoom.floatValue;
    }
    
    // save eyepiece
    if (imageProperties.eyepieceMagnification) {
        hardwareSettingNode.eyePiece.magnification.floatValue = imageProperties.eyepieceMagnification.floatValue;
    }
    
    //Export DisplaySettings
    CZIMutableMetadataNode *displaySettingNode = metadata.root.displaySetting;
    [docManager.document.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        CZIMutableMetadataNode *channelNode = displaySettingNode.channels[index];
        channelNode.nodeAttribute.id = [NSString stringWithFormat:@"Channel:%d", index];
        channelNode.nodeAttribute.name = imageBlock.channelProperties.channelName ?: [NSString stringWithFormat:@"C%d", index + 1];
        channelNode.dyeName.stringValue = [NSString stringWithFormat:@"Dye%d", index + 1];
        
        if (imageBlock.channelProperties.channelSelection) {
            channelNode.isSelected.boolValue = imageBlock.channelProperties.channelSelection.boolValue;
        }
        
        UIColor *color = imageBlock.channelProperties.channelColor;
        if (color) {
            channelNode.color.colorValue = color;
            channelNode.componentBitCount.integerValue = imageBlock.componentBitCount;
            channelNode.pixelType.stringValue = imageBlock.pixelType;
        } else {
            channelNode.colorMode.stringValue = @"None";
            channelNode.bitCountRange.integerValue = 8;
        }
    }];
    
    //Export Layers
    CZIMutableMetadataNode *layersNode = metadata.root.layers;
    [docManager.elementLayer encodeToLayersNode:layersNode];
    
    //Export Appliance
    if (docManager.multiphase) {
        CZIMutableMetadataNode *applianceNode = CZIMutableMetadata.metadata.document.appliance;
        applianceNode.nodeAttribute.id = @"ImageAnalysis:1";
        
        CZIMutableMetadataNode *imageAnalysisSettingNode = applianceNode.data.measurementAppData.imageAnalysisSetting;
        
        imageAnalysisSettingNode.version.stringValue = @"1.1";
        imageAnalysisSettingNode.tilingMode.stringValue = @"NoTiling";
        imageAnalysisSettingNode.tileSize.stringValue = @"0";
        imageAnalysisSettingNode.maxParticleSize.stringValue = @"0";
        imageAnalysisSettingNode.maxDisplaySize.stringValue = @"3500";
        
        imageAnalysisSettingNode.script.nodeAttribute.name = @"Builtin Multichannel";
        imageAnalysisSettingNode.script.segmenterSource.stringValue = @"SegmenterBuiltinDefault";
        
        imageAnalysisSettingNode.subImageDimension.stringValue = @"C";
        
#if METADATA_REFACTOED
        [docManager.multiphase exportMetaDataTo:&imageAnalysisSettingNode];
#endif
        
        imageAnalysisSettingNode.regionClassTemplate.stringValue = @"";
        imageAnalysisSettingNode.analysisStepCollection.stringValue = @"";
        imageAnalysisSettingNode.additionalChannelSetting.stringValue = @"";
        
        imageAnalysisSettingNode.analysisFrameTeach.source.stringValue = @"AnalysisFrameDefault";
        imageAnalysisSettingNode.analysisFrameRun.source.stringValue = @"AnalysisFrameDefault";
        imageAnalysisSettingNode.analysisFeatures.source.stringValue = @"AnalysisFeaturesDefault";
        imageAnalysisSettingNode.analysisClass.source.stringValue = @"AnalysisClassDefault";
        
        if (docManager.roiRegion) {
            CZIMutableMetadataNode *measureFrameNode = imageAnalysisSettingNode.measureFrame;
            
            measureFrameNode.frameMode.stringValue = @"Inside";
            measureFrameNode.isFrameMaximize.boolValue = NO;
            measureFrameNode.complement.boolValue = NO;
            
            CZIMutableMetadataNode *graphicLayerNode = measureFrameNode.graphicLayer;
            graphicLayerNode.usage.stringValue = @"Edit";
            graphicLayerNode.isProtected.boolValue = NO;
            graphicLayerNode.layerFlags.integerValue = 1;
            
            [docManager.roiRegion encodeROIToElementsNode:graphicLayerNode.elements withIndex:0];
        }
        
        [metadata.root.appliances appendNode:applianceNode];
    }
    
    if (docManager.grainSizeResult) {
        CZIMutableMetadataNode *applianceNode = CZIMutableMetadata.metadata.document.appliance;
        applianceNode.nodeAttribute.id = @"GrainSizeMeasurement";
        
        [docManager.grainSizeResult encodeToMetadataNode:applianceNode.data.measurementAppData.grainSizeMeasurement];
        
        [metadata.root.appliances appendNode:applianceNode];
    }
    
    metadata.root.metadataNodes.stringValue = @"";
    
    return [metadata autorelease];
}

UIKIT_EXTERN NSData *UIImageCZIRepresentation(CZDocManager *docManager) {
    CZIDocument *czi = [[[CZIDocument alloc] init] autorelease];
    
    [docManager.document.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        [czi addSubBlockWithCGImage:imageBlock.image.CGImage atIndex:index];
    }];
    
    //Write metadata
    CZIMetadata *metadata = exportMetaData(docManager);
    [czi setMetadata:metadata];
    
    //Write thumbnail
    CGSize thumbSize;
    thumbSize.width = CZImageDocumentThumbnailWidth;
    thumbSize.height = CZImageDocumentThumbnailHeight;
    UIImage *thumbImage = [docManager imageWithBurninAnnotationsInSize:thumbSize];
    NSData *thumbData = UIImageJPEGRepresentation(thumbImage, 0.7);
    [czi addThumbnailAttachmentWithJPEGData:thumbData];
    
    // write image analysis data
    if (docManager.analyzer2dData) {
        NSData *imageAnalysisBlob = docManager.analyzer2dData;
        [czi addImageAnalysisAttachmentWithData:imageAnalysisBlob];
    } else if (docManager.analyzer2dResult) {
        CZAnalyzer2dPersister *persister = [[CZAnalyzer2dPersister alloc] init];
        NSData *imageAnalysisBlob = [persister imageAnalysisBlobFromAnalyzerResult:docManager.analyzer2dResult];
        [persister release];
        
        if (imageAnalysisBlob) {
            [czi addImageAnalysisAttachmentWithData:imageAnalysisBlob];
        } else {
            CZLogv(@"failed to convert analyzer result to data blob");
        }
    }
    
    return [czi dataRepresentation];
}
