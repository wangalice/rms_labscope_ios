//
//  CZImageProperties+EXIF.h
//  DocManager
//
//  Created by Li, Junlin on 6/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties.h"
#include <libexif/exif-data.h>
#include <tiffio.h>

@interface CZImageProperties (EXIF)

- (instancetype)initFromEXIF:(ExifData *)exif makerNote:(NSString **)makerNote;
- (instancetype)initFromTIFF:(TIFF *)tif makerNote:(NSString **)makerNote;

@end
