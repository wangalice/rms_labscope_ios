//
//  CZPugiXmlHelper.h
//  Hermes
//
//  Created by Ralph Jin on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef Hermes_CZPugiXmlHelper_h
#define Hermes_CZPugiXmlHelper_h

#include <time.h>
#include <xlocale.h>
#include "pugixml.hpp"
#import <CZAnnotationKit/CZAnnotationKit.h>

#define kStringDefaultSize 128

namespace pugi {

    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, const char *value) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        nameNode.append_child(pugi::node_pcdata).set_value(value ? value : "");
    }
    
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, const double& value) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        
        char str[kStringDefaultSize];
        sprintf(str, "%g", value);
        
        nameNode.append_child(pugi::node_pcdata).set_value(str);
    }
    
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, bool value) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        nameNode.append_child(pugi::node_pcdata).set_value(value ? "true" : "false");
    }
    
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, int value) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        
        char str[kStringDefaultSize];
        sprintf(str, "%d", value);
        
        nameNode.append_child(pugi::node_pcdata).set_value(str);
    }
    
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, long value) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        
        char str[kStringDefaultSize];
        sprintf(str, "%ld", value);
        
        nameNode.append_child(pugi::node_pcdata).set_value(str);
    }
    
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, const CZColor &color) {
        pugi::xml_node nameNode = parentNode.append_child(nodeName);
        
        char str[kStringDefaultSize];
        sprintf(str, "#%02X%02X%02X%02X", color.a, color.r, color.g, color.b);
        
        nameNode.append_child(pugi::node_pcdata).set_value(str);
    }
    
    static inline void AppendTime(pugi::xml_node &parentNode, const char *nodeName, const time_t& time) {
        struct tm timeStruct;
        localtime_r(&time, &timeStruct);
        
        char buffer[kStringDefaultSize];
        strftime_l(buffer, kStringDefaultSize, "%Y-%m-%dT%H:%M:%S", &timeStruct, NULL);
        
        parentNode.append_child(nodeName).text().set(buffer);
    }
    
#ifdef __OBJC__
    static inline void AppendValue(pugi::xml_node &parentNode, const char *nodeName, NSString *v) {
        AppendValue(parentNode, nodeName, [v UTF8String]);
    }
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, NSString *&v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            v = [NSString stringWithUTF8String:valueNode.text().as_string()];
            return true;
        } else {
            return false;
        }
    }
    
    static inline bool ParseAttribute(pugi::xml_node &node, const char *attributeName, NSString *&v) {
        pugi::xml_attribute attribute = node.attribute(attributeName);
        if (attribute) {
            v = [NSString stringWithUTF8String:attribute.as_string()];
            return true;
        } else {
            return false;
        }
    }
#endif
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, bool &v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            v = valueNode.text().as_bool();
            return true;
        } else {
            return false;
        }
    }
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, int &v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            v = valueNode.text().as_int();
            return true;
        } else {
            return false;
        }
    }
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, float &v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            v = valueNode.text().as_float();
            return true;
        } else {
            return false;
        }
    }
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, double &v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            v = valueNode.text().as_double();
            return true;
        } else {
            return false;
        }
    }
    
    static inline bool ParseValue(pugi::xml_node &parentNode, const char *nodeName, CZColor &v) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            const char *vStr = valueNode.text().as_string();
            ++vStr;
            
            if (strlen(vStr) == 8) {
                unsigned long l = strtoul(vStr, 0, 16);
                v.a = (l & 0xFF000000) >> 24;
                v.r = (l & 0xFF0000) >> 16;
                v.g = (l & 0xFF00) >> 8;
                v.b = (l & 0xFF);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    static inline bool ParseTime(pugi::xml_node &parentNode, const char *nodeName, time_t& time) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            struct tm timeStruct;
            const char *stringValue = valueNode.text().as_string();
            if (strptime_l(stringValue, "%Y-%m-%dT%H:%M:%S", &timeStruct, NULL)) {
                time = mktime(&timeStruct);
                return true;
            }
        }
        
        return false;
    }

#ifdef __OBJC__
    static inline bool ParseTime(pugi::xml_node & parentNode, const char *nodeName, NSDate *&date) {
        pugi::xml_node valueNode = parentNode.child(nodeName);
        if (valueNode) {
            NSString *dateTimeStr = [NSString stringWithCString:valueNode.text().as_string() encoding:NSASCIIStringEncoding];
            static NSDateFormatter *formatter = nil;
            if (formatter == nil) {
                formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            }
            date = [formatter dateFromString:dateTimeStr];
            return date != nil ;
        }
        
        return false;
    }
#endif  // __OBJC__
    
}  // namespace pugi

#endif
