//
//  CZImageProperties+EXIF.m
//  DocManager
//
//  Created by Li, Junlin on 6/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties+EXIF.h"
#import "CZImagePropertiesPrivate.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>

@implementation CZImageProperties (JPEG)

- (instancetype)initFromEXIF:(ExifData *)exif makerNote:(NSString **)makerNote {
    self = [self init];
    if (self) {
        // read manufacturer
        ExifEntry *entry = exif_content_get_entry(exif->ifd[EXIF_IFD_0], EXIF_TAG_MAKE);
        if (entry && entry->data) {
            self.microscopeManufacturer = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
        }
        
        // read operator name
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_0], EXIF_TAG_ARTIST);
        if (entry && entry->data) {
            self.operatorName = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
        }
        
        // read original time
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_ORIGINAL);
        if (entry && entry->data) {
            NSString *dateTimeStr = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
            NSDateFormatter *formatter = [CZImageProperties defaultDateFormat];
            self.acquisitionDate = [formatter dateFromString:dateTimeStr];
        }
        
        ExifByteOrder fileByteOrder = exif_data_get_byte_order(exif);
        
        // read exposure time
        CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] init];
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_EXPOSURE_TIME);
        if (entry && entry->data) {
            ExifRational r = exif_get_rational(entry->data, fileByteOrder);
            channelProperties.exposureTimeInSeconds = @((float)r.numerator / r.denominator);
        }
        self.channels = @[channelProperties];
        [channelProperties release];
        
        // read scaling unit
        ExifShort unit;
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_FOCAL_PLANE_RESOLUTION_UNIT);
        if (entry && entry->data) {
            unit = exif_get_short(entry->data, fileByteOrder);
        } else {
            unit = 2;  // default as inch
        }
        
        // read scaling
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_FOCAL_PLANE_X_RESOLUTION);
        if (entry && entry->data) {
            ExifRational r = exif_get_rational(entry->data, fileByteOrder);
            float f = (float)r.numerator / r.denominator;
            if (f == 0.0) {
                self.scaling = -1;
            } else if (unit == 2) {  // inch
                self.scaling = kInchToUM / f;
            } else if (unit == 3) {  // cm
                self.scaling = kCMToUM / f;
            } else {
                self.scaling = -1;
            }
        } else {
            self.scaling = -1;
        }
        
        // read objective model
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], kLensModelTag);
        if (entry && entry->data) {
            self.objectiveModel = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
        } else {
            entry = exif_content_get_entry(exif->ifd[EXIF_IFD_0], EXIF_TAG_IMAGE_DESCRIPTION);
            if (entry && entry->data) {
                NSString *string = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
                [self parseFromImageDescription:string];
            }
        }
        
        // read model
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_0], EXIF_TAG_MODEL);
        if (entry && entry->data) {
            NSString *modelName = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
            [self parseFromModelName:modelName];
        }
        
        // read maker note
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_MAKER_NOTE);
        if (entry == NULL) {
            entry = exif_content_get_entry(exif->ifd[EXIF_IFD_0], EXIF_TAG_MAKER_NOTE);
        }
        if (entry && entry->data) {
            NSString *note = [NSString stringWithCString:(char *)entry->data encoding:NSUTF8StringEncoding];
            if (makerNote) {
                *makerNote = note;
            }
        }
    }
    return self;
}

- (instancetype)initFromTIFF:(TIFF *)tif makerNote:(NSString **)makerNote {
    self = [self init];
    if (self) {
        char *cstr;
        if (TIFFGetField(tif, TIFFTAG_MAKE, &cstr)) {
            self.microscopeManufacturer = [NSString stringWithUTF8String:cstr];
        }
        
        if (TIFFGetField(tif, TIFFTAG_IMAGEDESCRIPTION, &cstr)) {
            NSString *imageDescription = [NSString stringWithUTF8String:cstr];
            [self parseFromImageDescription:imageDescription];
        }
        
        if (TIFFGetField(tif, TIFFTAG_MODEL, &cstr)) {
            NSString *microscopeModel = [NSString stringWithUTF8String:cstr];
            [self parseFromModelName:microscopeModel];
        }
        
        if (TIFFGetField(tif, TIFFTAG_ARTIST, &cstr)) {
            self.operatorName = [NSString stringWithUTF8String:cstr];
        }
        
        // read EXIF IFD section, if threre is no EXIF IFD, read from IFD 0, because Windows save maker note in IFD 0
        uint64 exifOffset = 0;
        if (TIFFGetField(tif, TIFFTAG_EXIFIFD, &exifOffset)) {
            if (TIFFReadEXIFDirectory(tif, exifOffset)) {
                CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] init];
                float time;
                if (TIFFGetField(tif, EXIFTAG_EXPOSURETIME, &time)) {
                    channelProperties.exposureTimeInSeconds = @(time);
                }
                self.channels = @[channelProperties];
                [channelProperties release];
                
                if (TIFFGetField(tif, EXIFTAG_DATETIMEORIGINAL, &cstr)) {
                    NSString *dateTimeStr = [NSString stringWithUTF8String:cstr];
                    NSDateFormatter *formatter = [CZImageProperties defaultDateFormat];
                    self.acquisitionDate = [formatter dateFromString:dateTimeStr];
                }
                
                TIFF_UINT16_T unit = 0;
                if (TIFFGetField(tif, EXIFTAG_FOCALPLANERESOLUTIONUNIT, &unit)) {
                    float scaling;
                    if (TIFFGetField(tif, EXIFTAG_FOCALPLANEXRESOLUTION, &scaling) ||
                        TIFFGetField(tif, EXIFTAG_FOCALPLANEYRESOLUTION, &scaling)) {
                        if (scaling == 0.0) {
                            self.scaling = -1;
                        } else if (unit == 2) {  // inch
                            self.scaling = kInchToUM / scaling;
                        } else if (unit == 3) {  // cm
                            self.scaling = kCMToUM / scaling;
                        } else {
                            self.scaling = -1;
                        }
                    }
                }
                
                uint16 length;
                void *bytes;
                if (TIFFGetField(tif, EXIFTAG_MAKERNOTE, &length, &bytes)) {
                    NSData *data = [NSData dataWithBytes:bytes length:length];
                    NSString *note = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                    if (makerNote) {
                        *makerNote = note;
                    }
                }
            }
        } else {
            uint16 length;
            void *bytes;
            NSCharacterSet *nullCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"\0"];
            
            if (TIFFGetField(tif, EXIFTAG_DATETIMEORIGINAL, &length, &bytes)) {
                NSData *data = [NSData dataWithBytes:bytes length:length];
                NSString *dateTimeStr = [[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] autorelease];
                dateTimeStr = [dateTimeStr stringByTrimmingCharactersInSet:nullCharacterSet];
                NSDateFormatter *formatter = [CZImageProperties defaultDateFormat];
                self.acquisitionDate = [formatter dateFromString:dateTimeStr];
            }
            
            if (TIFFGetField(tif, EXIFTAG_MAKERNOTE, &length, &bytes)) {
                NSData *data = [NSData dataWithBytes:bytes length:length];
                NSString *note = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                note = [note stringByTrimmingCharactersInSet:nullCharacterSet];
                if (makerNote) {
                    *makerNote = note;
                }
            }
        }
    }
    return self;
}

@end
