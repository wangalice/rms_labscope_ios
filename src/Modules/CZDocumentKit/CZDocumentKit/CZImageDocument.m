//
//  CZImageDocument.m
//  DocManager
//
//  Created by Li, Junlin on 4/3/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageDocument.h"
#import "CZImageProperties.h"
#import "CZImagePropertiesPrivate.h"
#import <CZToolbox/CZToolbox.h>

const CGFloat CZImageDocumentThumbnailWidth = 160.0;
const CGFloat CZImageDocumentThumbnailHeight = 120.0;

@implementation CZImageBlock

- (instancetype)initWithImage:(UIImage *)image channelProperties:(CZImageChannelProperties *)channelProperties {
    self = [super init];
    if (self) {
        _image = [image retain];
        _channelProperties = [channelProperties retain];
    }
    return self;
}

- (void)dealloc {
    [_image release];
    [_channelProperties release];
    [super dealloc];
}

- (NSInteger)componentBitCount {
    switch (self.image.czi_componentBitCount) {
        case CZIComponentBitCount8:
            return 8;
        case CZIComponentBitCount12:
            return 12;
    }
}

- (NSString *)pixelType {
    CZIPixelType pixelType = self.image.czi_pixelType;
    return NSStringFromCZIPixelType(pixelType);
}

@end

@interface CZImageDocument ()

@property (nonatomic, readonly, retain) UIImage *outputImage;

@end

@implementation CZImageDocument

@synthesize outputImage = _outputImage;

- (instancetype)initWithImageBlocks:(NSArray<CZImageBlock *> *)imageBlocks {
    self = [super init];
    if (self) {
        _imageBlocks = [imageBlocks copy];
        
        for (CZImageBlock *imageBlock in imageBlocks) {
            [imageBlock addObserver:self forKeyPath:@"channelProperties" options:0 context:nil];
        }
    }
    return self;
}

- (instancetype)initWithCZIDocument:(CZIDocument *)czi {
    CZIMetadata *metadata = [czi metadata];
    NSInteger channelCount = [czi channelCount];
    NSMutableArray<CZImageBlock *> *imageBlocks = [NSMutableArray arrayWithCapacity:channelCount];
    for (NSInteger index = 0; index < channelCount; index++) {
        CZIMetadataNode *dimensionChannelNode = metadata.root.information.image.dimensions.channels[index];
        CZIMetadataNode *displayChannelNode = metadata.root.displaySetting.channels[index];
        
        UIImage *image = [czi imageForSubBlockAtIndex:index];
        CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] init];
        channelProperties.grayscale = @(image.czi_grayscale);
        channelProperties.channelName = displayChannelNode.nodeAttribute.name;
        channelProperties.channelColor = displayChannelNode.color.colorValue;
        channelProperties.channelSelection = displayChannelNode.isSelected.isEmptyNode ? @YES : @(displayChannelNode.isSelected.boolValue);
        channelProperties.filterSetName = dimensionChannelNode.reflector.stringValue;
        
        NSString *lightSourceId = dimensionChannelNode.lightSourcesSettings.lightSourceSettings.lightSource.nodeAttribute.id;
        if (lightSourceId.length > 0) {
            for (CZIMetadataNode *lightSourceNode in metadata.root.information.instrument.lightSources) {
                if ([lightSourceNode.nodeAttribute.id isEqualToString:lightSourceId]) {
                    channelProperties.lightSource = lightSourceNode.nodeAttribute.name;
                    break;
                }
            }
        }
        
        channelProperties.wavelength = dimensionChannelNode.excitationWavelength.stringValue;
        channelProperties.exposureTimeInSeconds = dimensionChannelNode.exposureTime.isEmptyNode ? nil : @(1.0e-9f * dimensionChannelNode.exposureTime.floatValue);
        
        CZIMetadataNode *gainNode = dimensionChannelNode.detectorSettings.gain;
        if (!gainNode.isEmptyNode && gainNode.integerValue >= 0) {
            channelProperties.gain = @(gainNode.integerValue);
        }
        
        CZImageBlock *imageBlock = [[CZImageBlock alloc] initWithImage:image channelProperties:channelProperties];
        [imageBlocks addObject:imageBlock];
        [channelProperties release];
        [imageBlock release];
    }
    return [self initWithImageBlocks:imageBlocks];
}

- (void)dealloc {
    for (CZImageBlock *imageBlock in _imageBlocks) {
        [imageBlock removeObserver:self forKeyPath:@"channelProperties" context:nil];
    }
    [_imageBlocks release];
    [_outputImage release];
    [super dealloc];
}

- (instancetype)initWithImage:(UIImage *)image grayscale:(BOOL)grayscale {
    CZImageChannelProperties *channelProperties = [[[CZImageChannelProperties alloc] init] autorelease];
    channelProperties.grayscale = @(grayscale);
    CZImageBlock *imageBlock = [[[CZImageBlock alloc] initWithImage:image channelProperties:channelProperties] autorelease];
    return [self initWithImageBlocks:@[imageBlock]];
}

- (instancetype)documentByReplacingImage:(UIImage *)image {
    NSMutableArray<CZImageBlock *> *imageBlocks = [NSMutableArray arrayWithCapacity:self.imageBlocks.count];
    for (CZImageBlock *imageBlock in self.imageBlocks) {
        CZImageBlock *newImageBlock = [[CZImageBlock alloc] initWithImage:image channelProperties:imageBlock.channelProperties];
        [imageBlocks addObject:newImageBlock];
    }
    return [[[[self class] alloc] initWithImageBlocks:imageBlocks] autorelease];
}

- (instancetype)documentByReplacingImage:(UIImage *)image grayscale:(BOOL)grayscale {
    NSMutableArray<CZImageBlock *> *imageBlocks = [NSMutableArray arrayWithCapacity:self.imageBlocks.count];
    for (CZImageBlock *imageBlock in self.imageBlocks) {
        CZImageChannelProperties *channelProperties = imageBlock.channelProperties;
        if (channelProperties == nil) {
            channelProperties = [[[CZImageChannelProperties alloc] init] autorelease];
        }
        channelProperties.grayscale = @(grayscale);
        CZImageBlock *newImageBlock = [[CZImageBlock alloc] initWithImage:image channelProperties:channelProperties];
        [imageBlocks addObject:newImageBlock];
    }
    return [[[[self class] alloc] initWithImageBlocks:imageBlocks] autorelease];
}

- (instancetype)documentByReplacingImageBlocks:(NSArray<CZImageBlock *> *)imageBlocks {
    return [[[[self class] alloc] initWithImageBlocks:imageBlocks] autorelease];
}

#pragma mark - Output Image

- (CGSize)imageSize {
    return self.imageBlocks.firstObject.image.size;
}

- (BOOL)isMultichannelImage {
    if (self.imageBlocks.count == 0) {
        return NO;
    }
    
    for (CZImageBlock *imageBlock in self.imageBlocks) {
        if (imageBlock.channelProperties.channelColor == nil) {
            return NO;
        }
    }
    
    return YES;
}

- (UIImage *)outputImage {
    if (_outputImage == nil) {
        _outputImage = [[self outputImageWithSize:[self imageSize]] retain];
    }
    return _outputImage;
}

- (UIImage *)outputThumbnail {
    return [self outputImageWithSize:CGSizeMake(CZImageDocumentThumbnailWidth, CZImageDocumentThumbnailHeight)];
}

- (UIImage *)outputImageWithSize:(CGSize)size {
    if (self.imageBlocks.count <= 1 && self.imageBlocks.firstObject.channelProperties.channelColor == nil) {
        return self.imageBlocks.firstObject.image;
    }
    
    CZIDocument *czi = [[CZIDocument alloc] init];
    CZIMutableMetadata *metadata = [[CZIMutableMetadata alloc] init];
    
    CZIMutableMetadataNode *information = metadata.root.information;
    information.image.componentBitCount.integerValue = self.imageBlocks.firstObject.componentBitCount;
    information.image.pixelType.stringValue = self.imageBlocks.firstObject.pixelType;
    information.image.sizeX.integerValue = self.imageSize.width;
    information.image.sizeY.integerValue = self.imageSize.height;
    
    CZIMutableMetadataNode *channels = metadata.root.displaySetting.channels;
    [self.imageBlocks enumerateObjectsUsingBlock:^(CZImageBlock *imageBlock, NSUInteger index, BOOL *stop) {
        channels[index].nodeAttribute.name = imageBlock.channelProperties.channelName;
        channels[index].color.colorValue = imageBlock.channelProperties.channelColor;
        channels[index].isSelected.boolValue = imageBlock.channelProperties.channelSelection ? imageBlock.channelProperties.channelSelection.boolValue : YES;
        channels[index].componentBitCount.integerValue = imageBlock.componentBitCount;
        channels[index].pixelType.stringValue = imageBlock.pixelType;
        [czi addSubBlockWithCGImage:imageBlock.image.CGImage atIndex:index];
    }];
    
    [czi setMetadata:metadata];
    
    CGSize imageSize = [self imageSize];
    float scaling = fmaxf(size.width / imageSize.width, size.height / imageSize.height);
    scaling = fminf(scaling, 1.0);
    float demicalPrecisonScaling = [CZCommonUtils floatFromFloat:scaling precision:1];
    UIImage *outputImage = [czi imageRepresentationWithOptions:@{CZIDocumentImageScalingOption : @(demicalPrecisonScaling)}];
    
    [metadata release];
    [czi release];
    
    return outputImage;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"channelProperties"]) {
        [_outputImage release];
        _outputImage = nil;
    }
}

@end
