//
//  CZImagePropertiesPrivate.h
//  DocManager
//
//  Created by Li, Junlin on 5/27/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties.h"
#include <libexif/exif-data.h>

extern const float kInchToUM;
extern const float kCMToUM;
extern const ExifTag kLensModelTag;

@interface CZImageChannelProperties ()

@property (nonatomic, readwrite, retain) NSNumber *grayscale;
@property (nonatomic, readwrite, copy) NSString *channelName;
@property (nonatomic, readwrite, retain) UIColor *channelColor;
@property (nonatomic, readwrite, retain) NSNumber *channelSelection;
@property (nonatomic, readwrite, copy) NSString *filterSetName;
@property (nonatomic, readwrite, copy) NSString *filterSetID;
@property (nonatomic, readwrite, copy) NSString *lightSource;
@property (nonatomic, readwrite, copy) NSString *wavelength;
@property (nonatomic, readwrite, retain) NSNumber *exposureTimeInSeconds;
@property (nonatomic, readwrite, retain) NSNumber *gain;

@end

@interface CZImageProperties ()

@property (nonatomic, readwrite, copy) NSString *microscopeManufacturer;
@property (nonatomic, readwrite, copy) NSString *microscopeName;
@property (nonatomic, readwrite, copy) NSString *microscopeModel;
@property (nonatomic, readwrite, copy) NSString *cameraModel;
@property (nonatomic, readwrite, copy) NSString *operatorName;
@property (nonatomic, readwrite, retain) NSDate *acquisitionDate;

@property (nonatomic, readwrite, copy) NSString *objectiveModel;
@property (nonatomic, readwrite, retain) NSNumber *zoom;
@property (nonatomic, readwrite, retain) NSNumber *cameraAdapter;
@property (nonatomic, readwrite, copy) NSString *displayCameraAdapter;
@property (nonatomic, readwrite, assign) float scaling;
@property (nonatomic, readwrite, retain) NSNumber *scalingInPixels;
@property (nonatomic, readwrite, retain) NSNumber *bitDepth;

@property (nonatomic, readwrite, copy) NSArray<CZImageChannelProperties *> *channels;

@property (nonatomic, readwrite, retain) NSNumber *magnificationFactor;

- (BOOL)readFromFile:(NSString *)filePath;

@end
