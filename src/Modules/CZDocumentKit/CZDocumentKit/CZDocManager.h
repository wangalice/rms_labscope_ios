//
//  CZDocManager.h
//  Hermes
//
//  Created by Halley Gu on 1/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZImageDocument.h"

#define METADATA_REFACTOED 0
#define MULTIPHASE_REFACTORED 0

typedef NS_ENUM(NSUInteger,CZIFileSaveFormat) {
    kCZI,
    kCZIAndJPG,
    kJPGWithMarkup, // For saving in default file format
    kJPGWithoutMarkup,
    kJPGMedium,
    kCZIAndTIF,
    kTIFWithMarkup, // For saving in default file format
    kTIFWithoutMarkup
};

typedef NS_ENUM(NSUInteger,CZIFileSaveOverwriteOption) {
    kCZAskOnSave,
    kCZNeverOverwrite,
    kCZAlwaysOverwrite
};

typedef void (^CZDocManagerSaveBlock)(id);

@class CZElement;
@class CZElementLayer;
@class CZUndoManager;
@class CZNosepieceManager;
@class CZImageProperties;
@class CZDocManager;
@class CZFileNameTemplate;
@class CZMicroscopeModel;
@class CZFileNameGenerator;
@class CZMultiphase;
@class CZAnalyzer2dResult;
@class CZGrainSizeResult;

@protocol CZDocManagerDelegate <NSObject>

- (void)docManager:(CZDocManager *)docManager didSaveFiles:(NSArray *)files success:(BOOL)success;
- (void)docManager:(CZDocManager *)docManager willSaveFiles:(NSArray *)files;

@end

@protocol CZDocSaveDelegate <NSObject>

@optional
- (CZIFileSaveFormat)saveFormatForSnappedImage;
- (void)performOverwriteWithSourceFiles:(NSArray *)sourceFiles
                       destinationFiles:(NSArray *)destinationFiles
                      overwirteYesBlock:(void (^)(NSDictionary *))overwirteYesBlock
                       overwirteNoBlock:(void (^)(NSDictionary *))overwirteNoBlock
                    forceOverwriteBlock:(void (^)(void))forceOverwriteBlock;

@end

@interface CZDocManager : NSObject <CZUndoManaging, CZElementDataSource>

// data model
@property (nonatomic, readonly, copy) NSString *directory;
@property (nonatomic, retain) CZImageDocument *document;
@property (nonatomic, readonly, retain) CZElementLayer *elementLayer;
@property (nonatomic, readonly, retain) CZImageProperties *imageProperties;
@property (nonatomic, retain) CZMultiphase *multiphase;
@property (nonatomic, retain) CZElement *roiRegion;
@property (nonatomic, retain) CZGrainSizeResult *grainSizeResult;

// status model
@property (atomic, assign, getter = isImageModified) BOOL imageModified;
@property (atomic, assign, getter = isElementLayerModified) BOOL elementLayerModified;
@property (atomic, assign, getter = isMultichannelModified) BOOL multichannelModified;
@property (atomic, assign, getter = isMultiphaseModified) BOOL multiphaseModified;
@property (atomic, assign, getter = isGrainSizeModified) BOOL grainSizeModified;

@property (atomic, copy) NSString *filePath;

// original file path before objective change caused file path change. This property is designed for objective change perpose.
// Additional: Change to store the original before any file name or file path change.
@property (nonatomic, copy) NSString *originalFilePath;

@property (nonatomic, retain) CZFileNameGenerator *fileNameGenerator;

@property (nonatomic, assign) float calibratedScaling;

//It's used for checking if image displayed in the imageviewtab is snapped or opened from file tab
@property (nonatomic, assign) BOOL isImageSnapped;

@property (nonatomic, assign) BOOL isImageNew;

@property (nonatomic, readonly, copy) NSString *microscopeName;

@property (nonatomic, copy) NSString *userInputFileNameText;

@property (nonatomic, retain, readonly) NSDate *captureDate;

@property (nonatomic, copy) CZDocManagerSaveBlock closeActionBlock;

@property (nonatomic, assign) id closeActionTarget;

@property (nonatomic, assign) id<CZDocManagerDelegate> delegate;

@property (nonatomic, assign) CZShortcutMode shortcutMode;

/** multiphase analyzer result */
@property (nonatomic, retain) CZAnalyzer2dResult *analyzer2dResult;
/** multiphase analyzer result in CZI blob format. */
@property (nonatomic, retain) NSData *analyzer2dData;

+ (NSString *)filePathToBeBackupInDirectory:(NSString *)directory;
+ (CZFileNameTemplate *)defaultFileNameTemplate;

/** convert from region collection ID to phase index */
+ (uint32_t)phaseIDFromRegionsID:(uint32_t)regionsID;

/** convert from phase index to region collection ID */
+ (uint32_t)regionsIDFromPhaseID:(uint32_t)phaseID;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithDirectory:(NSString *)directory document:(CZImageDocument *)document;
- (instancetype)initWithDirectory:(NSString *)directory document:(CZImageDocument *)document elementLayer:(CZElementLayer *)elementLayer NS_DESIGNATED_INITIALIZER;

- (BOOL)isModified;
- (void)clearModifiedFlag;
- (void)discardBackup;
- (void)scheduleBackup;
- (void)quickBackup;

- (void)scheduleSave:(CZIFileSaveFormat)format withDelegate:(id<CZDocSaveDelegate>)delegate;
- (void)scheduleSaveInDefaultFormatWithDelegate:(id<CZDocSaveDelegate>)delegate;

- (void)updateImagePropertyFrom:(CZMicroscopeModel *)model updateFilePath:(BOOL)updateFilePath;

- (UIImage *)imageWithBurninAnnotations;
- (UIImage *)imageWithBurninAnnotationsInSize:(CGSize)destinationSize;

/** return a image with annotations burn on the input image*/
- (UIImage *)imageByBurningAnnotationsOnImage:(UIImage *)image;

/** draw multiphase regions on the context, and exclude invisible regions.
 * @param context graphic context to draw on.
 * @param visibleIDs visible region indexes; if nil, means all regions are visible.
 * @param highlightedID index of the region needs to be highlighted; if nil, means no region will be highlighted.
 */
- (void)drawMultiphaseRegionsOnContext:(CGContextRef)context;
- (void)drawMultiphaseRegionsOnContext:(CGContextRef)context withVisibleIDs:(NSSet *)visibleIDs;

- (NSString *)uniqueFileName;
- (NSString *)uniqueFileNameExcludedName:(NSString *)excludedName;

- (void)updateUnitOfElementLayer;

//TODO: Refactor these methods
- (void)updateImagePropertiesMicroscopeModel:(NSString *)microscopeModel;
- (void)updateImagePropertiesObjectiveModel:(NSString *)objectiveModel;
- (void)updateImagePropertiesTotalMagnification:(NSNumber *)totalMagnification;
- (void)updateImagePropertiesEyepieceMagnification:(NSNumber *)eyepieceMagnification;
- (void)updateImagePropertiesZoom:(NSNumber *)zoom;
- (void)updateImagePropertiesCameraModel:(NSString *)cameraModel;
- (void)updateImagePropertiesCameraAdapter:(NSNumber *)cameraAdapter;

@end
