//
//  CZDefaultSettings+DocManager.m
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+DocManager.h"

@implementation CZDefaultSettings (DocManager)

- (NSString *)operatorName {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kOperatorName];
}

- (void)setOperatorName:(NSString *)operatorName {
    if (operatorName) {
        NSString *name = [operatorName copy];
        [[NSUserDefaults standardUserDefaults] setValue:name forKey:kOperatorName];
        [self addOperatorNameToHistory:name];
        [name release];
    } else {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kOperatorName];
    }
}

- (NSArray *)operatorNameHistory {
    NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:kOperatorNameHistory];
    NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = (NSString *)a;
        NSString *second = (NSString *)b;
        return [first compare:second];
    }];
    return sortedArray;
}

- (void)addOperatorNameToHistory:(NSString *)operatorName {
    if ([operatorName length] == 0) {
        return;
    }
    
    NSArray *history = [[NSUserDefaults standardUserDefaults] arrayForKey:kOperatorNameHistory];
    NSUInteger originalCount = [history count];
    
    if (history) {
        history = [history arrayByAddingObject:operatorName];
    } else {
        history = @[operatorName];
    }
    NSSet *uniqueHistory = [NSSet setWithArray:history];
    history = [uniqueHistory allObjects];
    
    if (originalCount < [history count]) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"" ascending:YES];
        history = [history sortedArrayUsingDescriptors:@[sortDescriptor]];
        [[NSUserDefaults standardUserDefaults] setObject:history forKey:kOperatorNameHistory];
    }
}

- (BOOL)removeOperatorNameFromHistory:(NSString *)operatorName {
    if ([operatorName length] == 0) {
        return NO;
    }
    
    NSArray *history = [[NSUserDefaults standardUserDefaults] arrayForKey:kOperatorNameHistory];
    
    NSUInteger index = [history indexOfObject:operatorName];
    if (index != NSNotFound) {
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:history];
        [tempArray removeObjectAtIndex:index];
        [[NSUserDefaults standardUserDefaults] setObject:tempArray forKey:kOperatorNameHistory];
        return YES;
    } else {
        return NO;
    }
}

- (CZIFileSaveFormat)fileFormat {
    CZIFileSaveFormat fileFormat = kCZI;
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultFileFormat];
    if ([valueString isEqualToString:@"0"]) {
        fileFormat = kCZI;
    } else if ([valueString isEqualToString:@"1"]) {
        fileFormat = kCZIAndJPG;
    } else if ([valueString isEqualToString:@"2"]) {
        fileFormat = kJPGWithMarkup;
    } else if ([valueString isEqualToString:@"3"]) {
        fileFormat = kCZIAndTIF;
    } else if ([valueString isEqualToString:@"4"]) {
        fileFormat = kTIFWithMarkup;
    }
    return fileFormat;
}

- (void)setFileFormat:(CZIFileSaveFormat)fileFormat {
    switch (fileFormat) {
        case kCZI:
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:kDefaultFileFormat];
            break;
        case kCZIAndJPG:
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:kDefaultFileFormat];
            break;
        case kJPGWithMarkup:
            [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:kDefaultFileFormat];
            break;
        case kCZIAndTIF:
            [[NSUserDefaults standardUserDefaults] setObject:@"3" forKey:kDefaultFileFormat];
            break;
        case kTIFWithMarkup:
            [[NSUserDefaults standardUserDefaults] setObject:@"4" forKey:kDefaultFileFormat];
            break;
        default:
            break;
    }
}

- (CZIFileSaveOverwriteOption)fileOverwriteOption {
    CZIFileSaveOverwriteOption fileOverwriteOption = kCZAskOnSave;
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kOverwriteImageOnSave];
    if ([valueString isEqualToString:@"0"]) {
        fileOverwriteOption = kCZAskOnSave;
    } else if ([valueString isEqualToString:@"1"]) {
        fileOverwriteOption = kCZNeverOverwrite;
    } else if ([valueString isEqualToString:@"2"]) {
        fileOverwriteOption = kCZAlwaysOverwrite;
    }
    return fileOverwriteOption;
}

- (void)setFileOverwriteOption:(CZIFileSaveOverwriteOption)fileOverwriteOption {
    switch (fileOverwriteOption) {
        case kCZAskOnSave:
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kOverwriteImageOnSave];
            break;
        case kCZNeverOverwrite:
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kOverwriteImageOnSave];
            break;
        case kCZAlwaysOverwrite:
            [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:kOverwriteImageOnSave];
            break;
    }
}

@end
