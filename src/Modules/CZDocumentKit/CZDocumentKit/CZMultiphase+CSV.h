//
//  CZMultiphase+CSV.h
//  Matscope
//
//  Created by Ralph Jin on 4/16/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMultiphase.h"

@class CZDocManager;

@interface CZMultiphase (CSV)

- (BOOL)writeMeasurementsToCSVFile:(NSString *)filePath
                    withDocManager:(CZDocManager *)docManager
                             error:(NSError **)error;

- (BOOL)writeParticlesToCSVFile:(NSString *)filePath
                 withDocManager:(CZDocManager *)docManager
                          error:(NSError **)error;

@end
