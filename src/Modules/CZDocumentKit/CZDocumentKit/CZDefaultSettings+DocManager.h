//
//  CZDefaultSettings+DocManager.h
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <CZToolbox/CZToolbox.h>
#import "CZDocManager.h"

/*! operator name for reporting or file name template; in system setting. since v1 [string]*/
#define kOperatorName @"operator_name"

/*! operator name history. since v5 [string array]*/
#define kOperatorNameHistory @"operator_name_history"

/*! Default file format for snapped image, such as CZI, JPG etc.; in system setting. since v1 [integer];Add file format TIF.; in system setting.since v6 [integer]
 *
 * 0 ... CZI (default) \n
 * 1 ... CZI and JPG \n
 * 2 ... JPG without annotation \n
 * 3 ... CZI and TIF (v6) \n
 * 4 ... TIF (v6) \n
 */
#define kDefaultFileFormat @"default_file_format"

/*! Default mode of overwrite exist image file on saving. in system setting. since v1 [integer]
 *
 * 0 ... ask on save (default) \n
 * 1 ... never overwrite \n
 * 2 ... always overwrite \n
 */
#define kOverwriteImageOnSave @"overwrite_image_on_save"

@interface CZDefaultSettings (DocManager)

@property (nonatomic, retain) NSString *operatorName;
@property (nonatomic, readonly, retain) NSArray *operatorNameHistory;
@property (nonatomic, readwrite) CZIFileSaveFormat fileFormat;
@property (nonatomic, readwrite) CZIFileSaveOverwriteOption fileOverwriteOption;

- (BOOL)removeOperatorNameFromHistory:(NSString *)operatorName;

@end
