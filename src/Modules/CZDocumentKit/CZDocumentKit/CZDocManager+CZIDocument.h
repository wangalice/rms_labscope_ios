//
//  CZDocManager+CZIDocument.h
//  DocManager
//
//  Created by Li, Junlin on 4/30/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDocManager.h"
#import <CZIKit/CZIKit.h>

@interface CZDocManager (CZIDocument)

- (instancetype)initWithContentsOfCZIFile:(NSString *)filePath;
- (instancetype)initWithContentsOfCZIFile:(NSString *)filePath withDummyImage:(UIImage *)dummyImage imageSize:(CGSize)imageSize;

@end
