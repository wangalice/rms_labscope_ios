//
//  CZImageProperties+MakerNote.m
//  DocManager
//
//  Created by Li, Junlin on 6/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties+MakerNote.h"
#import "CZImagePropertiesPrivate.h"
#import <CZIKit/CZIKit.h>

@implementation CZImageProperties (MakerNote)

- (instancetype)initFromMakerNote:(NSString *)makerNote {
    self = [self init];
    if (self) {
        if (makerNote.length > 0) {
            CZIMetadata *metadata = [[CZIMetadata alloc] initWithJSONString:makerNote];
            
            CZIMetadataNode *hardwareSettingNode = metadata.document.imageMetadata.hardwareSetting;
            self.microscopeName = hardwareSettingNode.microscope.name.stringValue;
            self.microscopeManufacturer = hardwareSettingNode.microscope.manufacturer.stringValue;
            
            CZIMetadataNode *informationNode = metadata.document.imageMetadata.information;
            self.operatorName = informationNode.user.userName.stringValue;
            self.acquisitionDate = informationNode.image.acquisitionDateAndTime.dateValue;
            self.microscopeModel = informationNode.instrument.microscopes[0].name.stringValue;
            self.objectiveModel = informationNode.instrument.objectives[0].name.stringValue;
            self.cameraModel = informationNode.instrument.detectors[0].name.stringValue;
            
            CZIMetadataNode *zoomMagnificationNode = informationNode.image.microscopeSettings.zoomSettings.magnification;
            self.zoom = zoomMagnificationNode.isEmptyNode ? nil : @(zoomMagnificationNode.doubleValue);
            
            CZIMetadataNode *scalingNode = metadata.document.imageMetadata.scaling;
            self.cameraAdapter = @(scalingNode.autoScaling.cameraAdapterMagnification.floatValue);
            self.scaling = scalingNode.items[0].value.isEmptyNode ? -1 : scalingNode.items[0].value.doubleValue;
            
            CZIMetadataNode *channelsNode = informationNode.image.dimensions.channels;
            CZIMetadataNode *componentBitCountNode = channelsNode.componentBitCount;
            if (componentBitCountNode.isEmptyNode || componentBitCountNode.integerValue == 0) {
                componentBitCountNode = informationNode.image.componentBitCount;
            }
            self.bitDepth = componentBitCountNode.isEmptyNode ? nil : @(componentBitCountNode.integerValue);
            
            CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] init];
            channelProperties.filterSetID = channelsNode.reflectorMatId.stringValue;
            channelProperties.filterSetName = channelsNode.reflectorName.stringValue;
            channelProperties.exposureTimeInSeconds = channelsNode.exposureTime.isEmptyNode ? nil : @(channelsNode.exposureTime.floatValue * 1e-3f);
            
            CZIMetadataNode *exposureGainNode = channelsNode.exposureGain;
            if (!exposureGainNode.isEmptyNode && exposureGainNode.integerValue >= 0) {
                channelProperties.gain = @(exposureGainNode.integerValue);
            }
            
            channelProperties.lightSource = channelsNode.lightSource.stringValue;
            channelProperties.wavelength = channelsNode.excitationWavelength.stringValue;
            self.channels = @[channelProperties];
            
            [channelProperties release];
            [metadata release];
        }
    }
    return self;
}

- (NSString *)makerNoteRepresentation {
    CZIMutableMetadata *metadata = [CZIMutableMetadata metadata];
    
    CZIMutableMetadataNode *hardwareSettingNode = metadata.document.imageMetadata.hardwareSetting;
    hardwareSettingNode.microscope.name.stringValue = self.microscopeName;
    hardwareSettingNode.microscope.manufacturer.stringValue = self.microscopeManufacturer;
    
    CZIMutableMetadataNode *informationNode = metadata.document.imageMetadata.information;
    informationNode.document.creationDate.dateValue = self.acquisitionDate;
    informationNode.user.userName.stringValue = self.operatorName;
    informationNode.image.acquisitionDateAndTime.dateValue = self.acquisitionDate;
    
    if (self.zoom) {
        informationNode.image.microscopeSettings.zoomSettings.magnification.doubleValue = self.zoom.doubleValue;
    }
    
    CZIMutableMetadataNode *channelsNode = informationNode.image.dimensions.channels;
    
    if (self.bitDepth) {
        channelsNode.componentBitCount.integerValue = self.bitDepth.integerValue;
    }
    
    NSString *filterSetID = self.channels.firstObject.filterSetID;
    NSString *filterSetName = self.channels.firstObject.filterSetName;
    for (CZImageChannelProperties *channelProperties in self.channels) {
        if (![channelProperties.filterSetID isEqualToString:filterSetID]) {
            filterSetID = nil;
            filterSetName = nil;
            break;
        }
    }
    channelsNode.reflectorMatId.stringValue = filterSetID;
    channelsNode.reflectorName.stringValue = filterSetName;
    
    if (self.channels.count == 1 && self.channels[0].channelColor == nil) {
        if (self.channels[0].exposureTimeInSeconds) {
            channelsNode.exposureTime.floatValue = self.channels[0].exposureTimeInSeconds.floatValue * 1e3f;
        }
        if (self.channels[0].gain) {
            channelsNode.exposureGain.integerValue = self.channels[0].gain.integerValue;
        }
        channelsNode.lightSource.stringValue = self.channels[0].lightSource;
        channelsNode.excitationWavelength.stringValue = self.channels[0].wavelength;
    }
    
    informationNode.instrument.microscopes[0].name.stringValue = self.microscopeModel;
    informationNode.instrument.objectives[0].name.stringValue = self.objectiveModel;
    informationNode.instrument.detectors[0].name.stringValue = self.cameraModel ?: self.microscopeModel;
    CZIMutableMetadataNode *scalingNode = metadata.document.imageMetadata.scaling;
    scalingNode.autoScaling.type.stringValue = @"Theoretic";
    scalingNode.autoScaling.cameraAdapterMagnification.floatValue = self.cameraAdapter.floatValue;
    scalingNode.items[0].type.stringValue = @"Distance";
    scalingNode.items[0].id.stringValue = @"X";
    scalingNode.items[0].value.doubleValue = self.scaling;
    scalingNode.items[0].defaultUnitFormat.stringValue = @"um";
    scalingNode.items[1].type.stringValue = @"Distance";
    scalingNode.items[1].id.stringValue = @"Y";
    scalingNode.items[1].value.doubleValue = self.scaling;
    scalingNode.items[1].defaultUnitFormat.stringValue = @"um";
    return [metadata JSONStringRepresentation];
}

@end
