//
//  CZDocManager+CZIMetaData.h
//  Hermes
//
//  Created by Ralph Jin on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDocManager.h"

@interface CZDocManager (CZIMetaData)

+ (CZDocManager *)newDocManagerFromFilePath:(NSString *)filePath;

/** new a doc manager with dummy image, so that it won't load image from disk into memory.*/
+ (CZDocManager *)newDocManagerWithDummyImageFromFilePath:(NSString *)filePath;

- (void)writeMetaDataToFilePath:(NSString *)path;
@end
