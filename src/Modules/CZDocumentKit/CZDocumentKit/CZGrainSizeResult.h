//
//  CZGrainSizeResult.h
//  Matscope
//
//  Created by Ralph Jin on 8/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSUInteger, CZGrainSizePattern) {
    CZGrainSizePatternCombined,
    CZGrainSizePatternHorizontalLines,
    CZGrainSizePatternVerticalLines,
    CZGrainSizePatternXCrossLines,
    CZGrainSizePatternConcentricCircles,
    CZGrainSizePatternLast = CZGrainSizePatternConcentricCircles
};

typedef NS_ENUM(NSUInteger, CZGrainsStandard) {
    CZGrainsStandardATSM_E_112,
    CZGrainsStandardISO_643,
    CZGrainsStandardGBT_6394_2002,
    CZGrainsStandardLast = CZGrainsStandardGBT_6394_2002
};

extern NSString * const CZStandardNameATSM_E_112;
extern NSString * const CZStandardNameISO_643;
extern NSString * const CZStandardNameGBT_6394_2002;

@interface CZGrainSizeResult : NSObject

@property (nonatomic, readonly, assign) CZGrainsStandard standard;
@property (nonatomic, readonly, assign) CZGrainSizePattern pattern;
@property (nonatomic, readonly, assign) float patternZoom;
@property (nonatomic, readonly, assign) float grainSize;  // grain size number
@property (nonatomic, readonly, assign) float roundedGrainSize;  // rounded grain size number, step is 0.5
@property (nonatomic, readonly, assign) float meanDistance;
@property (nonatomic, readonly, retain) NSArray *chords;  // array of CZGrainsChord

+ (NSString *)nameFromPattern:(CZGrainSizePattern)pattern;
+ (CZGrainSizePattern)patternFromName:(NSString *)patternName;
+ (NSString *)localizedNameFromPattern:(CZGrainSizePattern)pattern;

+ (NSString *)nameFromStandard:(CZGrainsStandard)standard;
+ (CZGrainsStandard)standardFromName:(NSString *)standardName;

/** designate initializer. */
- (id)initWithStandard:(CZGrainsStandard)standard
             grainSize:(float)grainSize
          meanDistance:(float)meanDistance
           patternType:(CZGrainSizePattern)patternType
           patternZoom:(float)patternZoom
                chords:(NSArray *)chords;

- (NSUInteger)intersectionPointsCount;
- (BOOL)isIntersectionPointsCountSuitable;

- (NSString *)standardName;
- (NSString *)patternName;
- (NSString *)localizedPatternName;

- (CAShapeLayer *)newLayerWithFrame:(CGRect)frame lineWidth:(CGFloat)lineWidth;

@end

typedef void(^ CZGrainSizeDDABlock)(int x, int y);

@interface CZGrainsChord : NSObject

+ (CZGrainsChord *)lineChordWithStartPoint:(CGPoint)startPoint
                                  endPoint:(CGPoint)endPoint
                        intersectionPoints:(NSArray *)intersectionPoints;

+ (CZGrainsChord *)circleChordWithCenter:(CGPoint)center
                                  radius:(CGFloat)radius
                      intersectionPoints:(NSArray *)intersectionPoints;

@property (nonatomic, readonly, retain) NSArray *intersectionPoints;

- (id)initWithIntersectionPoints:(NSArray *)intersectionPoints;

- (NSUInteger)intersectionPointsCount;
- (CGPoint)intersectionPointAtIndex:(NSUInteger)index;

- (NSUInteger)distanceCount;
/** total distance between intersection points, in pixels */
- (CGFloat)totalValidDistance;

/** length of chord in pixel*/
- (CGFloat)length;

/** @return the closest point on chord from point |p|. */
- (CGPoint)closestPoint:(CGPoint)p;

/** enum points on chord
 * DDA - Digital differential analyzer (graphics algorithm)
 * @see http://en.wikipedia.org/wiki/Digital_differential_analyzer_%28graphics_algorithm%29
 */
- (void)ddaWithBlock:(CZGrainSizeDDABlock)block inBounds:(CGRect)bounds;

- (void)renderChordOnPath:(CGMutablePathRef)path;

- (CZGrainsChord *)chordWithIntersectionPoints:(NSArray *)intersectionPoints;
- (CZGrainsChord *)chordByAppendingPoint:(CGPoint)point;
- (CZGrainsChord *)chordByRemovingPoint:(CGPoint)point;

@end

@interface CZGrainsChordLine : CZGrainsChord

@property (nonatomic, readonly, assign) CGPoint startPoint;
@property (nonatomic, readonly, assign) CGPoint endPoint;

- (id)initWithStartPoint:(CGPoint)startPoint
                endPoint:(CGPoint)endPoint
      intersectionPoints:(NSArray *)intersectionPoints;

@end

@interface CZGrainsChordCircle : CZGrainsChord

@property (nonatomic, readonly, assign) CGPoint center;
@property (nonatomic, readonly, assign) CGFloat radius;

- (id)initWithCenter:(CGPoint)startPoint
              radius:(CGFloat)radius
  intersectionPoints:(NSArray *)intersectionPoints;;


@end
