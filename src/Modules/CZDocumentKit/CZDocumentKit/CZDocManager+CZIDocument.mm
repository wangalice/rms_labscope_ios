//
//  CZDocManager+CZIDocument.m
//  DocManager
//
//  Created by Li, Junlin on 4/30/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDocManager+CZIDocument.h"
#import <CZToolbox/CZToolbox.h>
#import "pugixml.hpp"
#import "CZPugiXmlHelper.h"
#import "CZImagePropertiesPrivate.h"
#import "CZImageProperties+CZIMetadata.h"
#import "CZElementLayer+CZIMetadata.h"
#import "CZMultiphase+CZIMetaData.h"
#import "CZElement+ROI.h"

@implementation CZDocManager (CZIDocument)

- (instancetype)initWithContentsOfCZIFile:(NSString *)filePath {
    return [self initWithContentsOfCZIFile:filePath withDummyImage:nil imageSize:CGSizeZero];
}

- (instancetype)initWithContentsOfCZIFile:(NSString *)filePath withDummyImage:(UIImage *)dummyImage imageSize:(CGSize)imageSize {
    @autoreleasepool {
        CZIDocument *czi = [[CZIDocument alloc] initWithFile:filePath];
        CZIMetadata *metadata = [czi metadata];
        
        NSString *directory = [filePath stringByDeletingLastPathComponent];
        CZImageDocument *document = nil;
        
        if (dummyImage == nil) {
            document = [[CZImageDocument alloc] initWithCZIDocument:czi];
            imageSize = [czi imageSize];
        } else {  // Do not read image, if use dummy image.
            document = [[CZImageDocument alloc] initWithImage:dummyImage grayscale:NO];
        }
        
        CZElementLayer *elementLayer = [[CZElementLayer alloc] initFromMetadata:metadata imageSize:imageSize];
        
        self = [self initWithDirectory:directory document:document elementLayer:elementLayer];
        if (self) {
            self.filePath = filePath;
            self.analyzer2dData = [czi imageAnalysis];
            [self.imageProperties readFromCZIMetadata:metadata];
            [self updateUnitOfElementLayer];
            elementLayer.scaling = self.imageProperties.scaling;
            
#if MULTIPHASE_REFACTORED
            CZMultiphase *multiphase = [[CZMultiphase alloc] initWithMetaData:&doc imageSize:imageSize];
            multiphase.imagePixelCount = imageSize.width * imageSize.height;
            if (!isGrayImage) {
                // recalculate image counts for cutting(circle/triangle) image
                if (!CGSizeEqualToSize(imageSize, CGSizeMake(1, 1))) { // ignore dummy image size for report generation
                    multiphase.imagePixelCount = [self calculateRealImageCount:image];
                }
            }
            self.multiphase = multiphase;
            [multiphase release];
#endif
            
#if METADATA_REFACTOED
            // read ROI region
            pugi::xml_node elementsNode = doc.select_single_node("/ImageDocument/Metadata/Appliances/Appliance[@Id='ImageAnalysis:1']/Data/MeasurementAppData/ImageAnalysisSetting/MeasureFrame/GraphicLayer/Elements").node();
            if (elementsNode) {
                for (pugi::xml_node_iterator it = elementsNode.begin(); it != elementsNode.end(); ++it) {
                    pugi::xml_node &elementNode = *it;
                    CZElement *roiRegion = [CZElement roiElementFromNode:&elementNode];
                    roiRegion.dashLine = YES;
                    self.roiRegion = roiRegion;
                    break;  // Hermes only support 1 ROI
                }
            }
#endif
            
#if MULTIPHASE_REFACTORED
            if (self.roiRegion) {
                multiphase.imagePixelCount = [self.roiRegion roiAreaInBoundary:CGRectMake(0, 0, imageSize.width, imageSize.height)];
            }
#endif
            
#if METADATA_REFACTOED
            // read grain size measurement
            pugi::xml_node grainSizeNode = doc.select_single_node("/ImageDocument/Metadata/Appliances/Appliance[@Id='GrainSizeMeasurement']/Data/MeasurementAppData/GrainSizeMeasurement").node();
            if (grainSizeNode) {
                CZGrainSizeResult *result = [[CZGrainSizeResult alloc] initWithCZIMetadata:&grainSizeNode];
                self.grainSizeResult = result;
                [result release];
            }
#endif
        }
        
#if MULTIPHASE_REFACTORED
        if ([self.multiphase hasOverlappingPhases]) {
            // Images with overlapping phases are not supported.
            CZLogv(@"There are overlapping phases in the multiphase data.");
            [self release];
            return nil;
        }
#endif
        
        [document release];
        [czi release];
        [elementLayer release];
        
        return self;
    }
}

- (NSUInteger)calculateRealImageCount:(UIImage *)image {
    if (CGSizeEqualToSize(image.size, CGSizeZero)) {
        return 0;
    }
    
    size_t width = image.size.width;
    size_t height = image.size.height;
    
    size_t count = width * height;
    void *imageData = calloc(height * width, sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(imageData, width, height, 8, width * sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextSetShouldAntialias(context, NO);
    CGContextSetAllowsAntialiasing(context, NO);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [image CGImage]);
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    uint8_t *pData = (uint8_t *)imageData;
    
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            if (pData[0] == 0 && count > 0) {
                count--;
            }
            pData += 4;
        }
    }
    
    free(imageData);
    
    return count;
}

@end
