//
//  CZElement+ROI.h
//  Matscope
//
//  Created by Ralph Jin on 6/6/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <CZAnnotationKit/CZAnnotationKit.h>

@interface CZElement (ROI)

- (NSUInteger)roiAreaInBoundary:(CGRect)boundary;

@end


