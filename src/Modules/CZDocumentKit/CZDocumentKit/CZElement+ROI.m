//
//  CZElement+ROI.m
//  Matscope
//
//  Created by Ralph Jin on 6/6/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZElement+ROI.h"
#import <CZAnnotationKit/CZAnnotationKit.h>

@implementation CZElement (ROI)

- (NSUInteger)roiAreaInBoundary:(CGRect)boundary {
    boundary = CGRectIntegral(boundary);
    
    CGRect elementBoundary = [self boundary];
    elementBoundary = CGRectIntegral(elementBoundary);
    
    CGRect intersectRect = CGRectIntersection(elementBoundary, boundary);
    
    if (CGRectIsNull(intersectRect)) {
        return 0;
    } else {
        size_t width = intersectRect.size.width;
        size_t height = intersectRect.size.height;
        void *imageData = calloc(width * height, sizeof(uint8_t));
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
        CGContextRef context = CGBitmapContextCreate(imageData, width, height, 8, width, colorSpace, kCGImageAlphaNone);
        CGContextSetInterpolationQuality(context, kCGInterpolationNone);
        CGContextSetShouldAntialias(context, NO);
        CGContextSetAllowsAntialiasing(context, NO);
        CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
        
        CGPathRef path = [self primaryNodePath];
        
        CGContextTranslateCTM(context, -intersectRect.origin.x, -intersectRect.origin.y);
        CGContextAddPath(context, path);
        CGContextFillPath(context);
        CGPathRelease(path);

        CGContextRelease(context);
        CGColorSpaceRelease(colorSpace);
        
        uint8_t *pData = imageData;
        size_t count = 0;
        for (size_t y = 0; y < height; ++y) {
            for (size_t x = 0; x < width; ++x) {
                if (*pData && *pData != 0) {
                    ++count;
                }
                ++pData;
            }
        }
        
        free(imageData);
        
        return count;
    }
}

@end
