//
//  CZImageProperties+MakerNote.h
//  DocManager
//
//  Created by Li, Junlin on 6/11/19.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties.h"

@interface CZImageProperties (MakerNote)

- (instancetype)initFromMakerNote:(NSString *)makerNote;
- (NSString *)makerNoteRepresentation;

@end
