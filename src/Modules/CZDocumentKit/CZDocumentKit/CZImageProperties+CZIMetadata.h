//
//  CZImageProperties+CZIMetadata.h
//  Hermes
//
//  Created by Ralph Jin on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties.h"

@class CZIMetadata;

@interface CZImageProperties (CZIMetadata)

- (void)readFromCZIMetadata:(CZIMetadata *)metadata;

@end
