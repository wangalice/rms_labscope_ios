//
//  CZElementLayer+CZIMetadata.h
//  Hermes
//
//  Created by Mike Wang on 3/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <CZAnnotationKit/CZAnnotationKit.h>
#import <CZIKit/CZIKit.h>

@interface CZElementLayer (CZIMetadata)

/**
 @param metadata the root node of the CZI file's meta.
 @param imageSize the size of the image, this is for scaling and line width calculation.
 */
- (instancetype)initFromMetadata:(CZIMetadata *)metadata imageSize:(CGSize)imageSize;
- (void)encodeToLayersNode:(CZIMutableMetadataNode *)layersNode;

@end

@interface CZElement (CZIMetadata)

+ (CZElement *)elementFromNode:(CZIMetadataNode *)elementNode;
+ (CZElement *)roiElementFromNode:(CZIMetadataNode *)elementNode;

/** Abstract method, subclass should override it. */
- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i;

/** Abstract method, subclass should override it. */
- (NSUInteger)encodeROIToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i;

@end
