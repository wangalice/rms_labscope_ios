//
//  CZElementXMLFile.h
//  Matscope
//
//  Created by Sherry Xu on 4/21/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import <CZAnnotationKit/CZAnnotationKit.h>

@interface CZElementLayer (CZIMetadata)

+ (CZElementLayer *)readElementXMLFile:(NSData *)elementData;
- (void)exportElementToXMLFilePath:(NSString *)path;

@end

