//
//  CZMultiphase+CZIMetaData.h
//  Matscope
//
//  Created by Mike Wang on 2/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "CZMultiphase.h"
#import "pugixml.hpp"

@interface CZMultiphase (CZIMetaData)
/**
 @param rootNode the root node of the CZI file's meta.
 @param imageSize the size of the image, this is for scaling and line width calculation.
 */
- (id)initWithMetaData:(pugi::xml_node *)rootNode imageSize:(CGSize)imageSize;

- (void)exportMetaDataTo:(pugi::xml_node &)imageAnalysisSettingNode;
@end
