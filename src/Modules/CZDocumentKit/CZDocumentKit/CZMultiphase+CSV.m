//
//  CZMultiphase+CSV.m
//  Matscope
//
//  Created by Ralph Jin on 4/16/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMultiphase+CSV.h"
#import <CZToolbox/CZToolbox.h>
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZDocManager.h"
#import "CZAnalyzer2dResult.h"
#import "CZParticles2DItem.h"
#import "CZParticle2DItem.h"

@implementation CZMultiphase (CSV)

- (BOOL)writeMeasurementsToCSVFile:(NSString *)filePath
                    withDocManager:(CZDocManager *)docManager
                             error:(NSError **)error {
    if ([self phaseCount] <= 0) {
        return NO;
    }
    
    CZAnalyzer2dResult *analyzer2dResult = [docManager analyzer2dResult];
    
    // Append title of csv
    NSMutableString *csvContent = [[NSMutableString alloc] initWithCapacity:4096U];
    [csvContent appendString:@"\xEF\xBB\xBF"];  // append BOM
    [csvContent appendString:@"\"ID::ID!!I\""];
    [csvContent appendString:@",\"ImageID::Img.ID!!I\""];
    [csvContent appendString:@",\"PhaseID::Ph.ID!!I\""];
    [csvContent appendString:@",\"PhaseName::Phase!!\""];
    [csvContent appendString:@",\"VolPerc::Area Percentage!!R\""];
    [csvContent appendString:@",\"Vol::Area!!R\""];
    [csvContent appendString:@",\"ParticleCount::Object Count!!I\""];
    [csvContent appendString:@"\n"];
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    NSString *unitName = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
    double scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    NSUInteger precision = [docManager.elementLayer precision];

    // Append unit row
    [csvContent appendFormat:@",,,,%%,%@,\n", unitName];

    for (NSUInteger i = 0; i < [self phaseCount]; ++i) {
        CZPhase *phase = [self phaseAtIndex:i];
        NSString *index = [NSString stringWithFormat:@"%lu", (unsigned long)(i + 1)];
        [csvContent appendString:index];
        
        // image id
        [csvContent appendString:@",1"];
        
        // phase id
        [csvContent appendString:@","];
        [csvContent appendString:index];
        
        // phase name
        [csvContent appendString:@","];
        if ([phase.name length]) {
            [csvContent appendString:@"\""];
            [csvContent appendString:phase.name];
            [csvContent appendString:@"\""];
        }
        
        // percentage
        [csvContent appendString:@","];
        float percent = [self percentOfPhaseAtIndex:i];
        percent = MAX(percent, 0);
        [csvContent appendString:@"\""];
        [csvContent appendString:[NSString stringWithFormat:@"%.1f", (percent * 100)]];
        [csvContent appendString:@"\""];
        
        // area
        [csvContent appendString:@","];
        [csvContent appendString:@"\""];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * scaling * phase.area  precision:precision]];
        [csvContent appendString:@"\""];
        
        // partical count
        NSUInteger regionsID = (i + 1) * 2;  // convert phase ID to regions ID
        CZRegions2DItem *regions = [analyzer2dResult.regions2dItemCollection objectForKey:@(regionsID)];
        NSUInteger particalCount = [regions count];
        [csvContent appendString:@","];
        [csvContent appendString:@"\""];
        [csvContent appendString:[NSString stringWithFormat:@"%lu", (unsigned long)particalCount]];
        [csvContent appendString:@"\""];
        
        [csvContent appendString:@"\n"];
    }
    
    if (self.isRemainingEnabled) {
        NSString *index = [NSString stringWithFormat:@"%lu", (unsigned long)([self phaseCount] + 1)];
        [csvContent appendString:index];
        
        // image id
        [csvContent appendString:@",1"];
        
        // phase id
        [csvContent appendString:@","];
        [csvContent appendString:index];
        
        // phase name
        [csvContent appendString:@","];
        NSString *name = L(@"REPORT_REMAINING_PHASE");
        if (name.length) {
            [csvContent appendString:@"\""];
            [csvContent appendString:name];
            [csvContent appendString:@"\""];
        }
        
        // percentage
        [csvContent appendString:@","];
        float percent = [self remainingPercent];
        percent = MAX(percent, 0);
        [csvContent appendString:@"\""];
        [csvContent appendString:[NSString stringWithFormat:@"%.1f", (percent * 100)]];
        [csvContent appendString:@"\""];
        
        // area
        [csvContent appendString:@","];
        [csvContent appendString:@"\""];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * scaling * self.remainingArea  precision:precision]];
        [csvContent appendString:@"\""];
        
        // partical count, leave as empty
        [csvContent appendString:@","];

        [csvContent appendString:@"\n"];
    }
    
    BOOL success = [csvContent writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:error];
    [csvContent release];
    
    return success;
}

- (BOOL)writeParticlesToCSVFile:(NSString *)filePath
                 withDocManager:(CZDocManager *)docManager
                          error:(NSError **)error {
    if ([self phaseCount] <= 0) {
        return NO;
    }
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    NSString *unitName1d = L([docManager.elementLayer unitNameOfDistanceByStyle:unitStyle]);
    NSString *unitName2d = L([docManager.elementLayer unitNameOfAreaByStyle:unitStyle]);
    double scaling = [docManager.elementLayer scalingOfUnit:unitStyle];
    NSUInteger precision = [docManager.elementLayer precision];
    
    NSMutableString *csvContent = [[NSMutableString alloc] initWithCapacity:4096U];

    [csvContent appendString:@"\xEF\xBB\xBF"];  // append BOM
    
    // Append title of csv
    [csvContent appendString:@"ID::ID!!I,Diameter::Diameter!!R,Perimeter::Perimeter!!R,Area::Area!!R,FeretMax::Feret Maximum!!R,FeretMin::Feret Minimum!!R,FeretRatio::Feret Ratio!!R\n"];

    // Append unit row
    [csvContent appendFormat:@",%@,%@,%@,%@,%@,\n", unitName1d, unitName1d, unitName2d, unitName1d, unitName1d];
    
    // Append particle items
    CZParticles2DItem *particles = [[CZParticles2DItem alloc] initWithAnalysisResult:docManager.analyzer2dResult];
    [particles setCriteria:self.particleCriteria];
    for (CZParticle2DItem *particle in particles.visibleItems) {
        [csvContent appendFormat:@"%@", particle.index];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * [particle.diameter floatValue] precision:precision]];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * [particle.perimeter floatValue] precision:precision]];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * scaling * [particle.area floatValue] precision:precision]];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * [particle.maxFeret floatValue] precision:precision]];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromFloat:scaling * [particle.minFeret floatValue] precision:precision]];
        
        [csvContent appendString:@","];
        [csvContent appendString:[CZCommonUtils stringFromNumber:particle.feretRatio precision:kCZFeretRatioPrecision]];
        
        [csvContent appendString:@"\n"];
    }
    
    [particles release];
    
    BOOL success = [csvContent writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:error];
    [csvContent release];
    
    return success;
}

@end
