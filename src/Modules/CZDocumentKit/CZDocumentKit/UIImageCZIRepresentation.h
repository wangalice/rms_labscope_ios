//
//  UIImageCZIRepresentation.h
//  Hermes
//
//  Created by Li, Junlin on 12/17/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIkit.h>
#import "CZDocManager.h"

UIKIT_EXTERN NSData *UIImageCZIRepresentation(CZDocManager *docManager);
