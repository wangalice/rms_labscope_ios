//
//  CZGrainSizeResult.m
//  Matscope
//
//  Created by Ralph Jin on 8/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZGrainSizeResult.h"
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

NSString * const CZStandardNameATSM_E_112 = @"ASTM E112";
NSString * const CZStandardNameISO_643 = @"ISO 643";
NSString * const CZStandardNameGBT_6394_2002 = @"GB/T 6394-2002";

static NSString * const CZGrainSizePatternNameCombined = @"Combined";
static NSString * const CZGrainSizePatternNameHorizontalLines = @"HorizontalLines";
static NSString * const CZGrainSizePatternNameVerticalLines = @"VerticalLines";
static NSString * const CZGrainSizePatternNameXCrossLines = @"XCross";
static NSString * const CZGrainSizePatternNameConcentricCircles = @"ConcentricCircles";

static inline NSUInteger quadrantOfVector(CGPoint v) {
    if (v.x > 0) {
        return (v.y > 0) ? 0 : 3;
    } else {
        return (v.y > 0) ? 1 : 2;
    }
}

@implementation CZGrainSizeResult

+ (NSString *)nameFromPattern:(CZGrainSizePattern)pattern {
    NSString *name;
    switch (pattern) {
        case CZGrainSizePatternCombined:
            name = CZGrainSizePatternNameCombined;
            break;
        case CZGrainSizePatternHorizontalLines:
            name = CZGrainSizePatternNameHorizontalLines;
            break;
        case CZGrainSizePatternVerticalLines:
            name = CZGrainSizePatternNameVerticalLines;
            break;
        case CZGrainSizePatternXCrossLines:
            name = CZGrainSizePatternNameXCrossLines;
            break;
        case CZGrainSizePatternConcentricCircles:
            name = CZGrainSizePatternNameConcentricCircles;
            break;
    }
    return name;
}

+ (CZGrainSizePattern)patternFromName:(NSString *)patternName {
    if ([patternName isEqualToString:CZGrainSizePatternNameCombined]) {
        return CZGrainSizePatternCombined;
    } else if ([patternName isEqualToString:CZGrainSizePatternNameHorizontalLines]) {
        return CZGrainSizePatternHorizontalLines;
    } else if ([patternName isEqualToString:CZGrainSizePatternNameVerticalLines]) {
        return CZGrainSizePatternVerticalLines;
    } else if ([patternName isEqualToString:CZGrainSizePatternNameXCrossLines]) {
        return CZGrainSizePatternXCrossLines;
    } else if ([patternName isEqualToString:CZGrainSizePatternNameConcentricCircles]) {
        return CZGrainSizePatternConcentricCircles;
    } else {
        return CZGrainSizePatternCombined;
    }
}

+ (NSString *)localizedNameFromPattern:(CZGrainSizePattern)pattern {
    NSString *key;
    switch (pattern) {
        case CZGrainSizePatternCombined:
            key = @"GRAINS_PATTERN_COMBINED";
            break;
        case CZGrainSizePatternHorizontalLines:
            key = @"GRAINS_PATTERN_HORIZONTAL";
            break;
        case CZGrainSizePatternVerticalLines:
            key = @"GRAINS_PATTERN_VERTICAL";
            break;
        case CZGrainSizePatternXCrossLines:
            key = @"GRAINS_PATTERN_XCROSS";
            break;
        case CZGrainSizePatternConcentricCircles:
            key = @"GRAINS_PATTERN_CONCENTRIC_CIRCLES";
            break;
        default:
            return @"--";
            break;
    }
    return L(key);
}

+ (NSString *)nameFromStandard:(CZGrainsStandard)standard {
    NSString *name;
    switch (standard) {
        case CZGrainsStandardATSM_E_112:
            name = CZStandardNameATSM_E_112;
            break;
        case CZGrainsStandardISO_643:
            name = CZStandardNameISO_643;
            break;
        case CZGrainsStandardGBT_6394_2002:
            name = CZStandardNameGBT_6394_2002;
            break;
    }
    return name;
}

+ (CZGrainsStandard)standardFromName:(NSString *)standardName {
    if ([standardName isEqualToString:CZStandardNameATSM_E_112]) {
        return CZGrainsStandardATSM_E_112;
    } else if ([standardName isEqualToString:CZStandardNameISO_643]) {
        return CZGrainsStandardISO_643;
    } else if ([standardName isEqualToString:CZStandardNameGBT_6394_2002]) {
        return CZGrainsStandardGBT_6394_2002;
    } else {
        return CZGrainsStandardATSM_E_112;
    }
}

- (id)init {
    return [self initWithStandard:CZGrainsStandardATSM_E_112
                        grainSize:0
                     meanDistance:0
                      patternType:CZGrainSizePatternCombined
                      patternZoom:1
                           chords:nil];
}

- (id)initWithStandard:(CZGrainsStandard)standard
             grainSize:(float)grainSize
          meanDistance:(float)meanDistance
           patternType:(CZGrainSizePattern)patternType
           patternZoom:(float)patternZoom
    chords:(NSArray *)chords {
    self = [super init];
    if (self) {
        _standard = standard;
        _grainSize = grainSize;
        _meanDistance = meanDistance;
        _pattern = patternType;
        _patternZoom = patternZoom;
        _chords = [[NSArray alloc] initWithArray:chords];
    }
    
    return self;
}

- (void)dealloc {
    [_chords release];
    [super dealloc];
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([self class] != [object class]) {
        return NO;
    }
    
    CZGrainSizeResult *otherResult = object;
    if (_standard != otherResult.standard) {
        return NO;
    }
    
    if (_pattern != otherResult.pattern) {
        return NO;
    }
    
    if (fabsf(_patternZoom - otherResult.patternZoom) >= 1e-6) {
        return NO;
    }
    
    if (fabsf(_grainSize - otherResult.grainSize) >= 1e-6) {
        return NO;
    }
    
    if (fabsf(_meanDistance - otherResult.meanDistance) >= 1e-6) {
        return NO;
    }
    
    if (![_chords isEqual:otherResult.chords]) {
        return NO;
    }
    
    return YES;
}

- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    
    result = prime * result + @(_standard).hash;
    result = prime * result + @(_pattern).hash;
    result = prime * result + @(_patternZoom).hash;
    result = prime * result + @(_grainSize).hash;
    result = prime * result + @(_meanDistance).hash;
    result = prime * result + _chords.hash;
    return result;
}

- (float)roundedGrainSize {
    return round(self.grainSize * 2.0f) * 0.5f;
}

- (NSUInteger)intersectionPointsCount {
    NSUInteger count = 0;
    for (CZGrainsChord *chord in self.chords) {
        count += [chord intersectionPointsCount];
    }
    
    return count;
}

- (BOOL)isIntersectionPointsCountSuitable {
    NSUInteger pointCountOnLines = 0, pointCountOnCircles = 0;
    NSUInteger lowBoundary = self.standard == CZGrainsStandardGBT_6394_2002 ? 50 : 40;
    switch (_pattern) {
        case CZGrainSizePatternCombined: {
            int i = 0;
            for (CZGrainsChord *chord in self.chords) {
                if (i < 4) {
                    pointCountOnLines += [chord intersectionPointsCount];
                } else {
                    pointCountOnCircles += [chord intersectionPointsCount];
                }
                i++;
            }
            
            if (pointCountOnLines < 50) {
                return NO;
            }
            
            if (pointCountOnCircles < lowBoundary ||
                pointCountOnCircles > 100) {
                return NO;
            }
            break;
        }
        case CZGrainSizePatternHorizontalLines:
        case CZGrainSizePatternVerticalLines:
        case CZGrainSizePatternXCrossLines: {
            pointCountOnLines = [self intersectionPointsCount];
            if (pointCountOnLines < 50) {
                return NO;
            }
            break;
        }
        case CZGrainSizePatternConcentricCircles: {
            pointCountOnCircles = [self intersectionPointsCount];
            if (pointCountOnCircles < lowBoundary ||
                pointCountOnCircles > 100) {
                return NO;
            }
            break;
        }
        default:
            break;
    }
    
    return YES;
}

- (NSString *)standardName {
    return [CZGrainSizeResult nameFromStandard:self.standard];
}

- (NSString *)patternName {
    return [CZGrainSizeResult nameFromPattern:self.pattern];
}

- (NSString *)localizedPatternName {
    return [CZGrainSizeResult localizedNameFromPattern:self.pattern];
}

- (CAShapeLayer *)newLayerWithFrame:(CGRect)frame lineWidth:(CGFloat)lineWidth {
    CGRect imageRect = frame;

    // create chord layer
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.position = CGPointZero;
    layer.anchorPoint = CGPointZero;
    layer.frame = imageRect;
    layer.bounds = imageRect;
    layer.contentsRect = imageRect;
    layer.fillColor = nil;
    layer.strokeColor = [[UIColor redColor] CGColor];
    layer.lineWidth = lineWidth;
    CGMutablePathRef path = CGPathCreateMutable();
    for (CZGrainsChord *chord in self.chords) {
        [chord renderChordOnPath:path];
    }
    CGPathRef tempPath = CGPathCreateCopy(path);
    CGPathRelease(path);
    layer.path = tempPath;
    CGPathRelease(tempPath);
    
    // create intersection points layer
    CAShapeLayer *intersectionPointLayer = [[CAShapeLayer alloc] init];
    intersectionPointLayer.position = CGPointZero;
    intersectionPointLayer.anchorPoint = CGPointZero;
    intersectionPointLayer.frame = imageRect;
    intersectionPointLayer.bounds = imageRect;
    intersectionPointLayer.contentsRect = imageRect;
    intersectionPointLayer.strokeColor = nil;
    intersectionPointLayer.fillColor = [[UIColor yellowColor] CGColor];
    path = CGPathCreateMutable();
    CGFloat pointSize = lineWidth * 1.5;
    pointSize = MAX(0.5, pointSize);
    
    for (CZGrainsChord *chord in self.chords) {
        for (NSUInteger i = 0; i < chord.intersectionPointsCount; ++i) {
            CGPoint point = [chord intersectionPointAtIndex:i];
            CGPathAddRect(path, NULL, CGRectMake(point.x - pointSize, point.y - pointSize, pointSize * 2, pointSize * 2));
        }
    }
    tempPath = CGPathCreateCopy(path);
    CGPathRelease(path);
    intersectionPointLayer.path = tempPath;
    CGPathRelease(tempPath);
    
    [layer addSublayer:intersectionPointLayer];
    [intersectionPointLayer release];
    
    return layer;
}

@end

@implementation CZGrainsChord

+ (CZGrainsChord *)lineChordWithStartPoint:(CGPoint)startPoint
                                  endPoint:(CGPoint)endPoint
                        intersectionPoints:(NSArray *)intersectionPoints {
    return [[[CZGrainsChordLine alloc] initWithStartPoint:startPoint
                                                 endPoint:endPoint
                                       intersectionPoints:intersectionPoints] autorelease];
}

+ (CZGrainsChord *)circleChordWithCenter:(CGPoint)center
                                  radius:(CGFloat)radius
                      intersectionPoints:(NSArray *)intersectionPoints {
    return [[[CZGrainsChordCircle alloc] initWithCenter:center
                                                 radius:radius
                                     intersectionPoints:intersectionPoints] autorelease];
}

- (id)initWithIntersectionPoints:(NSArray *)intersectionPoints {
    self  = [super init];
    if (self) {
        _intersectionPoints = [[NSArray alloc] initWithArray:intersectionPoints];
    }
    
    return self;
}

- (void)dealloc {
    [_intersectionPoints release];
    [super dealloc];
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([self class] != [object class]) {
        return NO;
    }
    
    CZGrainsChord *otherChord = (CZGrainsChord *)object;
    return [_intersectionPoints isEqualToArray:otherChord.intersectionPoints];
}

- (NSUInteger)hash {
    return [_intersectionPoints hash];
}

- (NSUInteger)intersectionPointsCount {
    return [_intersectionPoints count];
}

- (CGPoint)intersectionPointAtIndex:(NSUInteger)index {
    NSValue *value = _intersectionPoints[index];
    return [value CGPointValue];
}

- (NSUInteger)distanceCount {
    // sub-class shall override
    return 0;
}

- (CGFloat)totalValidDistance {
    // sub-class shall override
    return 0;
}

- (CGFloat)length {
    // sub-class shall override
    return 0;
}

- (CGPoint)closestPoint:(CGPoint)p {
    // sub-class shall override
    return p;
}

- (void)ddaWithBlock:(CZGrainSizeDDABlock)block inBounds:(CGRect)bounds {
    // sub-class shall override
}

- (void)renderChordOnPath:(CGMutablePathRef)path {
    // sub-class shall override
}

- (CZGrainsChord *)chordWithIntersectionPoints:(NSArray *)intersectionPoints {
    // sub-class shall override
    return self;
}

- (CZGrainsChord *)chordByAppendingPoint:(CGPoint)point {
    NSValue *value = [NSValue valueWithCGPoint:point];
    NSArray *intersectionPoints = [self.intersectionPoints arrayByAddingObject:value];
    
    return [self chordWithIntersectionPoints:intersectionPoints];
}

- (CZGrainsChord *)chordByRemovingPoint:(CGPoint)point {
    NSValue *value = [NSValue valueWithCGPoint:point];
    NSMutableArray *intersectionPoints = [NSMutableArray arrayWithArray:self.intersectionPoints];
    [intersectionPoints removeObject:value];
    
    if ([intersectionPoints count] != [self.intersectionPoints count]) {
        return [self chordWithIntersectionPoints:intersectionPoints];
    } else {
        return self;
    }
}

@end

@implementation CZGrainsChordLine

- (id)initWithStartPoint:(CGPoint)startPoint
                endPoint:(CGPoint)endPoint
      intersectionPoints:(NSArray *)intersectionPoints {
    self = [super initWithIntersectionPoints:intersectionPoints];
    if (self) {
        _startPoint = startPoint;
        _endPoint = endPoint;
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([self class] != [object class]) {
        return NO;
    }
    
    CZGrainsChordLine *otherChord = (CZGrainsChordLine *)object;
    if (!CGPointEqualToPoint(_startPoint, otherChord->_startPoint)) {
        return NO;
    }
    
    if (!CGPointEqualToPoint(_endPoint, otherChord->_endPoint)) {
        return NO;
    }
    
    return [super isEqual:object];
}

- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    
    result = prime * result + @(_startPoint.x).hash;
    result = prime * result + @(_startPoint.y).hash;
    result = prime * result + @(_endPoint.x).hash;
    result = prime * result + @(_endPoint.y).hash;
    result = prime * result + [super hash];
    return result;
}

- (NSUInteger)distanceCount {
    NSUInteger count = [self intersectionPointsCount];
    if (count == 0) {
        return 0;
    } else {
        return count - 1;
    }
}

- (CGFloat)totalValidDistance {
    NSUInteger count = [self intersectionPointsCount];
    if (count > 1) {
        CGPoint p0 = [self intersectionPointAtIndex:0];
        CGPoint p1 = [self intersectionPointAtIndex:count - 1];
        CGFloat dx = p0.x - p1.x;
        CGFloat dy = p0.y - p1.y;
        if (dx == 0) {
            return fabs(dy);
        } else if (dy == 0) {
            return fabs(dx);
        } else {
            return M_SQRT2 * fabs(dx);
        }
    } else {
        return 0;
    }
}

- (CGFloat)length {
    CGFloat dx = _startPoint.x - _endPoint.x;
    CGFloat dy = _startPoint.y - _endPoint.y;
    if (dx == 0) {
        return fabs(dy);
    } else if (dy == 0) {
        return fabs(dx);
    } else {
        return M_SQRT2 * fabs(dx);
    }
}

- (CGPoint)closestPoint:(CGPoint)p {
    CGPoint v0, v1;
    v0.x = p.x - self.startPoint.x;
    v0.y = p.y - self.startPoint.y;
    v1.x = self.endPoint.x - self.startPoint.x;
    v1.y = self.endPoint.y - self.startPoint.y;
    
    CGFloat v0DotV1 = v0.x * v1.x + v0.y * v1.y;
    CGFloat v1Distance2 = v1.x * v1.x + v1.y * v1.y;
    CGFloat ratio = v0DotV1 / v1Distance2;
    if (ratio < 0) {
        return self.startPoint;
    } else if (ratio > 1) {
        return self.endPoint;
    } else {
        p = self.startPoint;
        p.x += ratio * v1.x;
        p.y += ratio * v1.y;
        return p;
    }
}

- (void)ddaWithBlock:(CZGrainSizeDDABlock)block inBounds:(CGRect)bounds {
    if (!block) {
        return;
    }
    
    int dx = round(_endPoint.x - _startPoint.x);
    int dy = round(_endPoint.y - _startPoint.y);
    if (dx == 0) {
        int x = _startPoint.x;
        if (x >= 0 && x < bounds.size.width) {
            int startY = MAX(0, _startPoint.y);
            int endY = MIN(bounds.size.height - 1, _endPoint.y);
            if (startY > endY) {
                int temp = startY; startY = endY; endY = temp;
            }
            for (int y = startY; y < endY; ++y) {
                block(x, y);
            }
        }
    } else if (dy == 0) {
        int y = _startPoint.y;
        if (y >=0 && y < bounds.size.height) {
            int startX = MAX(0, _startPoint.x);
            int endX = MIN(bounds.size.width - 1, _endPoint.x);
            if (startX > endX) {
                int temp = startX; startX = endX; endX = temp;
            }
            for (int x = startX; x < endX; ++x) {
                block(x, y);
            }
        }
    } else { // 45 degrees slope line
        int startX = _startPoint.x;
        int endX = _endPoint.x;
        int startY = _startPoint.y;
        int endY = _endPoint.y;
        if (startX > endX) {
            int temp = startX; startX = endX; endX = temp;
            temp = startY; startY = endY; endY = temp;
        }
        int yStep = startY <= endY ? 1 : -1;
        
        for (int x = startX, y = startY; x < endX; x++, y += yStep) {
            block(x, y);
        }
    }
}

- (void)renderChordOnPath:(CGMutablePathRef)path {
    CGPathMoveToPoint(path, NULL, _startPoint.x, _startPoint.y);
    CGPathAddLineToPoint(path, NULL, _endPoint.x, _endPoint.y);
}

- (CZGrainsChord *)chordWithIntersectionPoints:(NSArray *)intersectionPoints {
    return [CZGrainsChord lineChordWithStartPoint:_startPoint
                                         endPoint:_endPoint
                               intersectionPoints:intersectionPoints];
}

- (CZGrainsChord *)chordByAppendingPoint:(CGPoint)point {
    CGPoint startPoint = self.startPoint;
    BOOL isHorizontal = startPoint.y == self.endPoint.y;
    CGFloat eToS = isHorizontal ? (self.endPoint.x - startPoint.x) : (self.endPoint.y - startPoint.y);
    NSValue *newValue = [NSValue valueWithCGPoint:point];
    
    NSUInteger insertionIndex = [self.intersectionPoints indexOfObject:newValue
                                                         inSortedRange:NSMakeRange(0, [self.intersectionPoints count])
                                                               options:NSBinarySearchingInsertionIndex
                                                       usingComparator:^(id lhs, id rhs) {
                                                           CGPoint leftPt = [lhs CGPointValue];
                                                           CGFloat leftPtToS = isHorizontal ? (leftPt.x - startPoint.x) : (leftPt.y - startPoint.y);
                                                           CGFloat leftT = leftPtToS / eToS;
                                                           
                                                           CGPoint rightPt = [rhs CGPointValue];
                                                           CGFloat rightPtToS = isHorizontal ? (rightPt.x - startPoint.x) : (rightPt.y - startPoint.y);
                                                           CGFloat rightT = rightPtToS / eToS;
                                                           
                                                           if (leftT < rightT) {
                                                               return NSOrderedAscending;
                                                           } else if (leftT == rightT) {
                                                               return NSOrderedSame;
                                                           } else {
                                                               return NSOrderedDescending;
                                                           }
                                                       }];
    
    NSMutableArray *intersectionPoints = [NSMutableArray arrayWithArray:self.intersectionPoints];
    [intersectionPoints insertObject:newValue atIndex:insertionIndex];
    return [self chordWithIntersectionPoints:intersectionPoints];
}

@end

@implementation CZGrainsChordCircle

- (id)initWithCenter:(CGPoint)center radius:(CGFloat)radius intersectionPoints:(NSArray *)intersectionPoints {
    self = [super initWithIntersectionPoints:intersectionPoints];
    if (self) {
        _center = center;
        _radius = radius;
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([self class] != [object class]) {
        return NO;
    }
    
    CZGrainsChordCircle *otherChord = (CZGrainsChordCircle *)object;
    if (_radius != otherChord->_radius) {
        return NO;
    }
    
    if (!CGPointEqualToPoint(_center, otherChord->_center)) {
        return NO;
    }
    
    return [super isEqual:object];
}

- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    
    result = prime * result + @(_radius).hash;
    result = prime * result + @(_center.x).hash;
    result = prime * result + @(_center.y).hash;
    result = prime * result + [super hash];
    return result;
}

- (NSUInteger)distanceCount {
    return [self intersectionPointsCount];
}

- (CGFloat)totalValidDistance {
    return [self length];
}

- (CGFloat)length {
    return 2 * M_PI * _radius;
}

- (CGPoint)closestPoint:(CGPoint)p {
    CGPoint v = CGPointMake(p.x - self.center.x, p.y - self.center.y);
    CGFloat distanceV = sqrt(v.x * v.x + v.y * v.y);
    CGFloat ratio = self.radius / distanceV;
    
    return CGPointMake(_center.x + v.x * ratio, _center.y + v.y * ratio);
}

- (void)ddaWithBlock:(CZGrainSizeDDABlock)block inBounds:(CGRect)bounds {
    if (!block) {
        return;
    }
    
    int x = _radius, y = 0;
    int radiusError = 1 - x;
    
    NSMutableArray *pointsCache = [NSMutableArray array];
    CGPoint pt;
    
    // Brehesom circle algorithm, 1/8 circle
    while(x >= y) {
        pt.x = x;
        pt.y = y;
        [pointsCache addObject:[NSValue valueWithCGPoint:pt]];
        block(x + _center.x, y + _center.y);
        
        y++;
        if (radiusError < 0) {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError += 2 * (y - x + 1);
        }
    }
    
    if ([pointsCache count] == 0) {
        return;
    }

    int i = 0;
    for (NSValue *value in [pointsCache reverseObjectEnumerator]) { // skip first point
        if (i != 0) {
            pt = [value CGPointValue];
            block(pt.y + _center.x, pt.x + _center.y);
        }
        i++;
    }
    
    for (NSValue *value in pointsCache) {
        pt = [value CGPointValue];
        block(-pt.y + _center.x, pt.x + _center.y);
    }
    
    i = 0;
    for (NSValue *value in [pointsCache reverseObjectEnumerator]) { // skip first point
        if (i != 0) {
            pt = [value CGPointValue];
            block(-pt.x + _center.x, pt.y + _center.y);
        }
        i++;
    }
    
    for (NSValue *value in pointsCache) {
        pt = [value CGPointValue];
        block(-pt.x + _center.x, -pt.y + _center.y);
    }
    
    i = 0;
    for (NSValue *value in [pointsCache reverseObjectEnumerator]) { // skip first point
        if (i != 0) {
            pt = [value CGPointValue];
            block(-pt.y + _center.x, -pt.x + _center.y);
        }
        i++;
    }
    
    for (NSValue *value in pointsCache) {
        pt = [value CGPointValue];
        block(pt.y + _center.x, -pt.x + _center.y);
    }
    
    i = 0;
    for (NSValue *value in [pointsCache reverseObjectEnumerator]) { // skip first point
        if (i != 0) {
            pt = [value CGPointValue];
            block(pt.x + _center.x, -pt.y + _center.y);
        }
        i++;
    }
}

- (void)renderChordOnPath:(CGMutablePathRef)path {
    CGPathMoveToPoint(path, NULL, _center.x + _radius, _center.y);
    CGPathAddArc(path, NULL, _center.x, _center.y, _radius, 2.0f * M_PI, 0.0f, true);
}

- (CZGrainsChord *)chordWithIntersectionPoints:(NSArray *)intersectionPoints {
    return [CZGrainsChord circleChordWithCenter:_center
                                         radius:_radius
                             intersectionPoints:intersectionPoints];
}

- (CZGrainsChord *)chordByAppendingPoint:(CGPoint)point {
    CGPoint center = self.center;
    NSValue *newValue = [NSValue valueWithCGPoint:point];
    
    NSUInteger insertionIndex = [self.intersectionPoints indexOfObject:newValue
                                                         inSortedRange:NSMakeRange(0, [self.intersectionPoints count])
                                                               options:NSBinarySearchingInsertionIndex
                                                       usingComparator:^(id lhs, id rhs) {
                                                           CGPoint lPt = [lhs CGPointValue];
                                                           CGPoint lV = CGPointMake(lPt.x - center.x, lPt.y - center.y);
                                                           NSUInteger lQ = quadrantOfVector(lV);
                                                           
                                                           CGPoint rPt = [lhs CGPointValue];
                                                           CGPoint rV = CGPointMake(rPt.x - center.x, rPt.y - center.y);
                                                           NSUInteger rQ = quadrantOfVector(rV);
                                                           
                                                           if (lQ < rQ) {
                                                               return NSOrderedAscending;
                                                           } else if (lQ == rQ) {
                                                               CGFloat lT = lPt.y / lPt.x;
                                                               CGFloat rT = rPt.y / rPt.x;
                                                               if (lT < rT) {
                                                                   return NSOrderedAscending;
                                                               } else if (lT == rT) {
                                                                   return NSOrderedSame;
                                                               } else {
                                                                   return NSOrderedDescending;
                                                               }
                                                           } else {
                                                               return NSOrderedDescending;
                                                           }
                                                       }];
    
    NSMutableArray *intersectionPoints = [NSMutableArray arrayWithArray:self.intersectionPoints];
    [intersectionPoints insertObject:newValue atIndex:insertionIndex];
    return [self chordWithIntersectionPoints:intersectionPoints];
}

@end
