//
//  CZElementLayer+CZIMetadata.m
//  Hermes
//
//  Created by Mike Wang on 3/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayer+CZIMetadata.h"
#import "CZElementXMLFile.h"
#import <CZToolbox/CZToolbox.h>

static const CGFloat kDefaultSnapTolerance = 88;

@interface CZElementGroup (CZIMetadata)
- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i;
@end

@interface CZFeature (CZIMetadata)
- (void)encodeToFeaturesNode:(CZIMutableMetadataNode *)featuresNode;
@end

@interface CZElementAngle (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementDisconnectedAngle (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementArrow (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementCircle (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementLine (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementPolygon (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementPolyline (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementSpline (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementSplineContour (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementCount (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementRectangle (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementCalipers (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementMultiCalipers (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementScaleBar (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@interface CZElementText (CZIMetadata)
- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode;
@end

@implementation CZFeature (CZIMetadata)

- (void)encodeToFeaturesNode:(CZIMutableMetadataNode *)featuresNode {
    NSString *featureName = nil;
    NSString *featureExpression = nil;
    NSString *displayUnit = nil;
    BOOL visible = YES;
    
    if ([self.type isEqualToString:kCZFeatureTypeArea]) {
        featureName = @"Area";
        featureExpression = @"Feature.Area()";
        displayUnit = @"GeomUnit\xC2\xB2";
    } else if ([self.type isEqualToString:kCZFeatureTypeDistance]) {
        featureName = @"Distance";
        featureExpression = @"Feature.Distance()";
        displayUnit = @"DistanceUnit";
    } else if ([self.type isEqualToString:kCZFeatureTypeCenterX]) {
        featureName = @"CenterX";
        featureExpression = @"Feature.CenterX()";
        displayUnit = @"CenterUnit";
        visible = NO;
    } else if ([self.type isEqualToString:kCZFeatureTypeCenterY]) {
        featureName = @"CenterY";
        featureExpression = @"Feature.CenterY()";
        displayUnit = @"CenterUnit";
        visible = NO;
    } else if ([self.type isEqualToString:kCZFeatureTypeAngle]) {
        featureName = @"Angle";
        featureExpression = @"Feature.Angel()";
        displayUnit = @"AngelUnit";
    } else if ([self.type isEqualToString:kCZFeatureTypeDiameter]) {
        featureName = @"Diameter";
        featureExpression = @"Feature.Diameter()";
        displayUnit = @"DiameterUnit";
    } else if ([self.type isEqualToString:kCZFeatureTypePerimeter]) {
        featureName = @"Perimeter";
        featureExpression = @"Feature.Perimeter()";
        displayUnit = @"PerimeterUnit";
    } else if ([self.type isEqualToString:kCZFeatureTypeNumber]) {
        featureName = @"Number";
    } else {
        CZLogv(@"Unknown feature type: %@!!", self.type);
        assert(false);
        return;
    }
    
    [featuresNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *featureNode = root.feature;
        featureNode.name.stringValue = featureName;
        // Deprecated by ZIS framework
        if (featureExpression) {
            featureNode.expression.stringValue = featureExpression;
        }
        if (displayUnit) {
            featureNode.displayUnit.stringValue = displayUnit;
        }
        featureNode.isDisplayEnabled.boolValue = visible;
    }];
}

@end

#pragma mark - CZElementGroup

@implementation CZElementGroup (CZIMetadata)

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *groupNode = root.graphicElementGroup;
        groupNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = groupNode.attributes;
        attributesNode.groupId.integerValue = [self index];
        attributesNode.canMeasureEnable.boolValue = NO;
    }];
    return ++index;
}

@end

#pragma mark - CZElement

@implementation CZElement (CZIMetadata)

+ (CZElement *)elementFromNode:(CZIMetadataNode *)elementNode {
    CZElement *element = nil;
    BOOL hasFillColor = NO;
    
    CZIMutableMetadataNode *placeholder = CZIMutableMetadata.metadata.document;
    if ([elementNode.nodeName isEqualToString:placeholder.rectangle.nodeName]) {
        element = [[CZElementRectangle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.circle.nodeName] ||
               [elementNode.nodeName isEqualToString:placeholder.pointsCircle.nodeName]) {
        element = [[CZElementCircle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.scaleBar.nodeName]) {
        element = [[CZElementScaleBar alloc] initFromElementNode:elementNode];
        hasFillColor = YES;
    } else if ([elementNode.nodeName isEqualToString:placeholder.line.nodeName]) {
        element = [[CZElementLine alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.connectedAngle.nodeName]) {
        element = [[CZElementAngle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.disconnectedAngle.nodeName]) {
        element = [[CZElementDisconnectedAngle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.arrow.nodeName]) {
        element = [[CZElementArrow alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.textBox.nodeName]) {
        element = [[CZElementText alloc] initFromElementNode:elementNode];
        hasFillColor = YES;
    } else if ([elementNode.nodeName isEqualToString:placeholder.polygon.nodeName]) {
        element = [[CZElementPolygon alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.polyline.nodeName]) {
        element = [[CZElementPolyline alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.openBezier.nodeName]) {
        element = [[CZElementSpline alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.bezier.nodeName]) {
        element = [[CZElementSplineContour alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.events.nodeName]) {
        element = [[CZElementCount alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.distance.nodeName] ||
               [elementNode.nodeName isEqualToString:placeholder.caliper.nodeName]) {
        element = [[CZElementCalipers alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.multiCalipers.nodeName] ||
               [elementNode.nodeName isEqualToString:placeholder.multiDistance.nodeName]) {
        element = [[CZElementMultiCalipers alloc] initFromElementNode:elementNode];
    }
    
    if (!hasFillColor) {
        element.fillColor = kCZColorTransparent;
    }

    return element;
}

+ (CZElement *)roiElementFromNode:(CZIMetadataNode *)elementNode {
    CZElement *element = nil;
    
    CZIMutableMetadataNode *placeholder = CZIMutableMetadata.metadata.document;
    if ([elementNode.nodeName isEqualToString:placeholder.rectangle.nodeName]) {
        element = [[CZElementRectangle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.circle.nodeName]) {
        element = [[CZElementCircle alloc] initFromElementNode:elementNode];
    } else if ([elementNode.nodeName isEqualToString:placeholder.polygon.nodeName]) {
        element = [[CZElementPolygon alloc] initFromElementNode:elementNode];
    }
    
    return element;
}

+ (NSArray<NSValue *> *)pointsFromMultiPointElement:(id<CZElementMultiPointKind>)element {
    NSUInteger pointsCount = [element pointsCount];
    NSMutableArray<NSValue *> *points = [NSMutableArray arrayWithCapacity:pointsCount];
    for (NSUInteger index = 0; index < pointsCount; index++) {
        CGPoint point = [element pointAtIndex:index];
        NSValue *value = [NSValue valueWithCGPoint:point];
        [points addObject:value];
    }
    return [[points copy] autorelease];
}

+ (void)appendPoints:(NSArray<NSValue *> *)points toMultiPointElement:(id<CZElementMultiPointKind>)element {
    for (NSValue *value in points) {
        CGPoint point = value.CGPointValue;
        [element appendPoint:point];
    }
}

- (void)decodeAttributesFromElementNode:(CZIMetadataNode *)elementNode {
    NSString *idAttribute = elementNode.nodeAttribute.id;
    if (idAttribute.length > 0) {
        self.identifier = idAttribute;
    }
    
    CZIMetadataNode *attributesNode = elementNode.attributes;
    if (attributesNode.isEmptyNode) {
        return;
    }
    
    if ([self conformsToProtocol:@protocol(CZElementRotatable)]) {
        id<CZElementRotatable> rotatable = (id<CZElementRotatable>)self;
        if (!attributesNode.rotation.isEmptyNode) {
            float angle = attributesNode.rotation.floatValue;
            [rotatable setRotateAngle:angle / 180.0 * M_PI];
        }
    }
    
    UIColor *color = nil;
    if ((color = attributesNode.stroke.colorValue)) {
        self.strokeColor = CZColorFromUIColor(color);
    } else if ((color = attributesNode.foreground.colorValue)) {
        self.strokeColor = CZColorFromUIColor(color);
    } else {
        self.strokeColor = kCZColorRed;
    }
    
    if ((color = attributesNode.fill.colorValue)) {
        self.fillColor = CZColorFromUIColor(color);
    } else {
        self.fillColor = kCZColorTransparent;
    }
    
    if ((color = attributesNode.measurementBackground.colorValue)) {
        self.measurementBackgroundColor = CZColorFromUIColor(color);
    } else {
        self.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
    }
    
    if ([self hasMeasurement]) {
        BOOL measurementVisible = YES;
        if (!attributesNode.isMeasurementVisible.isEmptyNode) {
            measurementVisible = attributesNode.isMeasurementVisible.boolValue;
            self.measurementHidden = !measurementVisible;
        }
        
        BOOL nameVisible = YES;
        if (!attributesNode.isMeasurementNameVisible.isEmptyNode) {
            nameVisible = attributesNode.isMeasurementNameVisible.boolValue;
            self.measurementNameHidden = !nameVisible;
        }
    }
    
    if ([self canKeepAspect]) {
        BOOL keepAspect = NO;
        if (!attributesNode.keepAspectRatio.isEmptyNode) {
            keepAspect = attributesNode.keepAspectRatio.boolValue;
            self.keepAspect = keepAspect;
        }
    }
}

- (void)decodeROIAttributesFromElementNode:(CZIMetadataNode *)elementNode {
    CZIMetadataNode *attributesNode = elementNode.attributes;
    if (attributesNode.isEmptyNode) {
        return;
    }
    
    if ([self conformsToProtocol:@protocol(CZElementRotatable)]) {
        id<CZElementRotatable> rotatable = (id<CZElementRotatable>)self;
        if (!attributesNode.rotation.isEmptyNode) {
            float angle = attributesNode.rotation.floatValue;
            [rotatable setRotateAngle:angle / 180.0 * M_PI];
        }
    }
    
    UIColor *color = nil;
    if ((color = attributesNode.stroke.colorValue)) {
        self.strokeColor = CZColorFromUIColor(color);
    } else {
        self.strokeColor = kCZColorTransparent;
    }
    
    if ((color = attributesNode.measurementBackground.colorValue)) {
        self.measurementBackgroundColor = CZColorFromUIColor(color);
    } else {
        self.measurementBackgroundColor = kCZColorWhite;
    }
    
    self.fillColor = kCZColorTransparent;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    return i;
}

- (NSUInteger)encodeROIToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    return i;
}

- (void)encodeToAttributesNode:(CZIMutableMetadataNode *)attributesNode withIndex:(NSUInteger)index {
    attributesNode.uniqueName.integerValue = index;
    
    if (self.parentGroup) {
        NSUInteger groupId = [self.parentGroup index];
        attributesNode.groupId.integerValue = groupId;
    }
    
    if (self.rotateAngle != 0.0f) {
        CGFloat angle = self.rotateAngle * 180.0 / M_PI;
        attributesNode.rotation.floatValue = angle;
    }
    
    attributesNode.foreground.stringValue = NSStringFromColor(self.strokeColor);
    
    int fontSize = round(self.suggestFontSize);
    attributesNode.fontSize.integerValue = fontSize;
    
    attributesNode.stroke.stringValue = NSStringFromColor(self.strokeColor);
    
    if (self.fillColor.a != 0) {
        attributesNode.fill.stringValue = NSStringFromColor(self.fillColor);
        attributesNode.fillStyle.stringValue = @"Solid";
    }
    
    int lineWidth = self.suggestLineWidth + 0.5f;
    attributesNode.strokeThickness.integerValue = lineWidth;
    
    attributesNode.strokeZoomWithImage.boolValue = YES;
    
    BOOL measurementVisible = [self hasMeasurement] && ![self isMeasurementHidden];
    attributesNode.isMeasurementVisible.boolValue = measurementVisible;
    CZColor background = self.measurementBackgroundColor;
    if (background.a > 0) {
        attributesNode.measurementForeground.stringValue = NSStringFromColor(kCZColorBlack);
        attributesNode.measurementBackground.stringValue = NSStringFromColor(self.measurementBackgroundColor);
    }
    
    BOOL nameVisible = [self hasMeasurement] && ![self isMeasurementNameHidden];
    attributesNode.isMeasurementNameVisible.boolValue = nameVisible;
    
    if ([self canKeepAspect]) {
        attributesNode.keepAspectRatio.boolValue = self.keepAspect;
    }
}

- (void)encodeROIToAttributesNode:(CZIMutableMetadataNode *)attributesNode {
    if (self.rotateAngle != 0.0f) {
        float angle = self.rotateAngle * 180.0 / M_PI;
        attributesNode.rotation.floatValue = angle;
    }
    
    attributesNode.opacity.floatValue = 0.3;
    attributesNode.fontSize.integerValue = 32;
    attributesNode.fillStyle.stringValue = @"HorizontalLine";
    attributesNode.fill.stringValue = NSStringFromColor(self.strokeColor);
}

- (void)encodeLineEndStyleToAttributesNode:(CZIMutableMetadataNode *)attributesNode {
    BOOL measurementVisible = [self hasMeasurement] && ![self isMeasurementHidden];
    if (measurementVisible) {
        attributesNode.lineBeginStyle.stringValue = @"LargeBar";
        attributesNode.lineEndStyle.stringValue = @"LargeBar";
    }
}

- (void)encodeFeaturesToNode:(CZIMutableMetadataNode *)parentNode {
    [parentNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *featuresNode = root.features;
        for (CZFeature *feature in self.features) {
            [feature encodeToFeaturesNode:featuresNode];
        }
    }];
}

- (void)encodeTextElementsToNode:(CZIMutableMetadataNode *)parentNode {
    [parentNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *textElementsNode = root.textElements;
        CZIMutableMetadataNode *textElementNode = textElementsNode.textElement;
        textElementNode.nodeAttribute.id = @"Annotation";
        textElementNode.nodeAttribute.type = @"AlignPosition";
    }];
}

- (void)encodeTextElementsToNode:(CZIMutableMetadataNode *)parentNode alignType:(NSString *)alignType {
    [parentNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *textElementsNode = root.textElements;
        CZIMutableMetadataNode *textElementNode = textElementsNode.textElement;
        textElementNode.nodeAttribute.id = @"Annotation";
        textElementNode.nodeAttribute.type = alignType;
        textElementNode.position.pointValue = self.measurementTextPosition;
    }];
}

@end

#pragma mark - CZElementAngle

@implementation CZElementAngle (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [super init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        CZIMetadataNode *geometryNode = elementNode.geometry;
        CZIMetadataNode *firstLineNode = geometryNode.firstLine;
        CZIMetadataNode *secondLineNode = geometryNode.secondLine;
        self.ptP = CGPointMake(firstLineNode.geometry.x1.floatValue, firstLineNode.geometry.y1.floatValue);
        self.ptA = CGPointMake(firstLineNode.geometry.x2.floatValue, firstLineNode.geometry.y2.floatValue);
        self.ptB = CGPointMake(secondLineNode.geometry.x2.floatValue, secondLineNode.geometry.y2.floatValue);
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *angleNode = root.connectedAngle;
        angleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = angleNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:angleNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = angleNode.geometry;
        CZIMutableMetadataNode *firstLineNode = geometryNode.firstLine;
        CZIMutableMetadataNode *secondLineNode = geometryNode.secondLine;
        
        // encode first line
        attributesNode = firstLineNode.attributes;
        ++index;
        firstLineNode.nodeAttribute.id = @(index).stringValue;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        geometryNode = firstLineNode.geometry;
        geometryNode.x1.floatValue = self.ptP.x;
        geometryNode.y1.floatValue = self.ptP.y;
        geometryNode.x2.floatValue = self.ptA.x;
        geometryNode.y2.floatValue = self.ptA.y;
        
        // encode second line
        attributesNode = secondLineNode.attributes;
        ++index;
        secondLineNode.nodeAttribute.id = @(index).stringValue;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        geometryNode = secondLineNode.geometry;
        geometryNode.x1.floatValue = self.ptP.x;
        geometryNode.y1.floatValue = self.ptP.y;
        geometryNode.x2.floatValue = self.ptB.x;
        geometryNode.y2.floatValue = self.ptB.y;
        
        [self encodeFeaturesToNode:angleNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementDisconnectedAngle

@implementation CZElementDisconnectedAngle (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        CZIMetadataNode *geometryNode = elementNode.geometry;
        CZIMetadataNode *firstLineNode = geometryNode.firstLine;
        CZIMetadataNode *secondLineNode = geometryNode.secondLine;
        self.ptB = CGPointMake(firstLineNode.geometry.x1.floatValue, firstLineNode.geometry.y1.floatValue);
        self.ptA = CGPointMake(firstLineNode.geometry.x2.floatValue, firstLineNode.geometry.y2.floatValue);
        self.ptD = CGPointMake(secondLineNode.geometry.x1.floatValue, secondLineNode.geometry.y1.floatValue);
        self.ptC = CGPointMake(secondLineNode.geometry.x2.floatValue, secondLineNode.geometry.y2.floatValue);
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *angleNode = root.disconnectedAngle;
        angleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = angleNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:angleNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = angleNode.geometry;
        CZIMutableMetadataNode *firstLineNode = geometryNode.firstLine;
        CZIMutableMetadataNode *secondLineNode = geometryNode.secondLine;
        
        // encode first line
        attributesNode = firstLineNode.attributes;
        ++index;
        firstLineNode.nodeAttribute.id = @(index).stringValue;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        geometryNode = firstLineNode.geometry;
        geometryNode.x1.floatValue = self.ptB.x;
        geometryNode.y1.floatValue = self.ptB.y;
        geometryNode.x2.floatValue = self.ptA.x;
        geometryNode.y2.floatValue = self.ptA.y;
        
        // encode second line
        attributesNode = secondLineNode.attributes;
        ++index;
        secondLineNode.nodeAttribute.id = @(index).stringValue;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        geometryNode = secondLineNode.geometry;
        geometryNode.x1.floatValue = self.ptD.x;
        geometryNode.y1.floatValue = self.ptD.y;
        geometryNode.x2.floatValue = self.ptC.x;
        geometryNode.y2.floatValue = self.ptC.y;
        
        [self encodeFeaturesToNode:angleNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementArrow

@implementation CZElementArrow (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGPoint pt1 = CGPointMake(elementNode.geometry.x1.floatValue, elementNode.geometry.y1.floatValue);
    CGPoint pt2 = CGPointMake(elementNode.geometry.x2.floatValue, elementNode.geometry.y2.floatValue);
    self = [self initWithPoint:pt2 anotherPoint:pt1];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *arrowNode = root.arrow;
        arrowNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = arrowNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        // The arrow's direction in Hermes is opposite to the arrow's in CZI, so the points are swapped here.
        CZIMutableMetadataNode *geometryNode = arrowNode.geometry;
        geometryNode.x1.floatValue = self.p2.x;
        geometryNode.y1.floatValue = self.p2.y;
        geometryNode.x2.floatValue = self.p1.x;
        geometryNode.y2.floatValue = self.p1.y;
    }];
    return ++index;
}

@end

#pragma mark - CZElementLine

@implementation CZElementLine (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGPoint pt1 = CGPointMake(elementNode.geometry.x1.floatValue, elementNode.geometry.y1.floatValue);
    CGPoint pt2 = CGPointMake(elementNode.geometry.x2.floatValue, elementNode.geometry.y2.floatValue);
    self = [self initWithPoint:pt1 anotherPoint:pt2];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *lineNode = root.line;
        lineNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = lineNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        [self encodeLineEndStyleToAttributesNode:attributesNode];
        attributesNode.horizontalAlignment.stringValue = @"Center";
        attributesNode.verticalAlignment.stringValue = @"Top";
        
        [self encodeTextElementsToNode:lineNode];
        
        CZIMutableMetadataNode *geometryNode = lineNode.geometry;
        geometryNode.x1.floatValue = self.p1.x;
        geometryNode.y1.floatValue = self.p1.y;
        geometryNode.x2.floatValue = self.p2.x;
        geometryNode.y2.floatValue = self.p2.y;
        
        [self encodeFeaturesToNode:lineNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementPolygon

@implementation CZElementPolygon (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeROIAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *polygonNode = root.polygon;
        polygonNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = polygonNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:polygonNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = polygonNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
        
        [self encodeFeaturesToNode:polygonNode];
    }];
    return ++index;
}

- (NSUInteger)encodeROIToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *polygonNode = root.polygon;
        polygonNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = polygonNode.attributes;
        [self encodeROIToAttributesNode:attributesNode];
        
        CZIMutableMetadataNode *geometryNode = polygonNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
    }];
    return ++index;
}

@end

#pragma mark - CZElementPolyline

@implementation CZElementPolyline (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *polylineNode = root.polyline;
        polylineNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = polylineNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        [self encodeLineEndStyleToAttributesNode:attributesNode];
        
        [self encodeTextElementsToNode:polylineNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = polylineNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
        
        [self encodeFeaturesToNode:polylineNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementSpline

@implementation CZElementSpline (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *splineNode = root.openBezier;
        splineNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = splineNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        [self encodeLineEndStyleToAttributesNode:attributesNode];
        
        [self encodeTextElementsToNode:splineNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = splineNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
        
        [self encodeFeaturesToNode:splineNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementSplineContour

@implementation CZElementSplineContour (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *splineContourNode = root.bezier;
        splineContourNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = splineContourNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:splineContourNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = splineContourNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
        
        [self encodeFeaturesToNode:splineContourNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementCount

@implementation CZElementCount (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        [CZElement appendPoints:elementNode.geometry.points.pointsValue toMultiPointElement:self];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *countNode = root.events;
        countNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = countNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:countNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = countNode.geometry;
        geometryNode.points.pointsValue = [CZElement pointsFromMultiPointElement:self];
        
        [self encodeFeaturesToNode:countNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementRectangle

@implementation CZElementRectangle (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGRect rect = CGRectMake(elementNode.geometry.left.floatValue,
                             elementNode.geometry.top.floatValue,
                             elementNode.geometry.width.floatValue,
                             elementNode.geometry.height.floatValue);
    self = [self initWithRect:rect];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
    }
    return self;
}

- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode {
    CGRect rect = CGRectMake(elementNode.geometry.left.floatValue,
                             elementNode.geometry.top.floatValue,
                             elementNode.geometry.width.floatValue,
                             elementNode.geometry.height.floatValue);
    self = [self initWithRect:rect];
    if (self) {
        [self decodeROIAttributesFromElementNode:elementNode];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *rectangleNode = root.rectangle;
        rectangleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = rectangleNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        attributesNode.horizontalAlignment.stringValue = @"Left";
        attributesNode.verticalAlignment.stringValue = @"Top";
        
        [self encodeTextElementsToNode:rectangleNode];
        
        CZIMutableMetadataNode *geometryNode = rectangleNode.geometry;
        geometryNode.left.floatValue = self.rect.origin.x;
        geometryNode.top.floatValue = self.rect.origin.y;
        geometryNode.width.floatValue = self.rect.size.width;
        geometryNode.height.floatValue = self.rect.size.height;
        
        [self encodeFeaturesToNode:rectangleNode];
    }];
    return ++index;
}

- (NSUInteger)encodeROIToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *rectangleNode = root.rectangle;
        rectangleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = rectangleNode.attributes;
        [self encodeROIToAttributesNode:attributesNode];
        
        CZIMutableMetadataNode *geometryNode = rectangleNode.geometry;
        geometryNode.left.floatValue = self.rect.origin.x;
        geometryNode.top.floatValue = self.rect.origin.y;
        geometryNode.width.floatValue = self.rect.size.width;
        geometryNode.height.floatValue = self.rect.size.height;
    }];
    return ++index;
}

@end

#pragma mark - CZElementCalipers

@implementation CZElementCalipers (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CZIMetadataNode *geometryNode = elementNode.geometry;
    CZIMetadataNode *baseLineNode = geometryNode.baseLine;
    CGPoint pt1 = CGPointMake(baseLineNode.geometry.x1.floatValue, baseLineNode.geometry.y1.floatValue);
    CGPoint pt2 = CGPointMake(baseLineNode.geometry.x2.floatValue, baseLineNode.geometry.y2.floatValue);
    CGPoint delta = CGPointMake(geometryNode.deltaX.floatValue, geometryNode.deltaY.floatValue);
    
    CGFloat vecX = pt2.x - pt1.x;
    CGFloat vecY = pt2.y - pt1.y;
    CGAffineTransform t = CGAffineTransformMakeRotationFromVector(vecX, vecY);
    
    delta = CGPointApplyAffineTransform(delta, t);
    CGPoint pt3 = CGPointZero;
    pt3.x = pt1.x + delta.x;
    pt3.y = pt1.y + delta.y;
    
    self = [self initWithPoint1:pt1 point2:pt2 point3:pt3];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *calipersNode = root.caliper;
        calipersNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = calipersNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        CZIMutableMetadataNode *geometryNode = calipersNode.geometry;
        CZIMutableMetadataNode *baseLineNode = geometryNode.baseLine;
        ++index;
        baseLineNode.nodeAttribute.id = @(index).stringValue;
        
        // encode base line
        attributesNode = baseLineNode.attributes;
        int fontSize = round(self.suggestFontSize);
        attributesNode.fontSize.integerValue = fontSize;
        attributesNode.strokeZoomWithImage.boolValue = YES;
        bool measurementVisible = [self hasMeasurement] && ![self isMeasurementHidden];
        attributesNode.isMeasurementVisible.boolValue = measurementVisible;
        
        [self encodeTextElementsToNode:baseLineNode alignType:@"AlignPosition"];
        CZIMutableMetadataNode *subGeometryNode = baseLineNode.geometry;
        subGeometryNode.x1.floatValue = self.p1.x;
        subGeometryNode.y1.floatValue = self.p1.y;
        subGeometryNode.x2.floatValue = self.p2.x;
        subGeometryNode.y2.floatValue = self.p2.y;
        
        // calculate rotate (-a) degree affine transform
        CGFloat vecX = self.p2.x - self.p1.x;
        CGFloat vecY = self.p2.y - self.p1.y;
        vecY = -vecY;
        CGAffineTransform t = CGAffineTransformMakeRotationFromVector(vecX, vecY);
        
        // encode deltaX/deltaY
        CGPoint delta;
        delta.x = self.p3.x - self.p1.x;
        delta.y = self.p3.y - self.p1.y;
        delta = CGPointApplyAffineTransform(delta, t);
        geometryNode.deltaX.floatValue = delta.x;
        geometryNode.deltaY.floatValue = delta.y;
        
        [self encodeFeaturesToNode:calipersNode];
    }];
    return ++index;
}

@end

#pragma mark - CZElementMultiCalipers

@implementation CZElementMultiCalipers (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    self = [self init];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        CZIMetadataNode *geometryNode = elementNode.geometry;
        CZIMetadataNode *baseLineNode = geometryNode.baseLine;
        CGPoint pt1 = CGPointMake(baseLineNode.geometry.x1.floatValue, baseLineNode.geometry.y1.floatValue);
        CGPoint pt2 = CGPointMake(baseLineNode.geometry.x2.floatValue, baseLineNode.geometry.y2.floatValue);
        [self appendPoint:pt1];
        [self appendPoint:pt2];
        
        CGFloat vecX = pt2.x - pt1.x;
        CGFloat vecY = pt2.y - pt1.y;
        CGAffineTransform t = CGAffineTransformMakeRotationFromVector(vecX, vecY);
        
        CZIMetadataNode *distancesNode = geometryNode.distances;
        for (CZIMetadataNode *distanceNode in distancesNode) {
            CGPoint delta = CGPointMake(distanceNode.deltaX.floatValue, distanceNode.deltaY.floatValue);
            delta = CGPointApplyAffineTransform(delta, t);
            CGPoint pt3 = CGPointZero;
            pt3.x = pt1.x + delta.x;
            pt3.y = pt1.y + delta.y;
            [self appendPoint:pt3];
        }
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *multiCalipersNode = root.multiCalipers;
        multiCalipersNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = multiCalipersNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        CZIMutableMetadataNode *geometryNode = multiCalipersNode.geometry;
        CZIMutableMetadataNode *baseLineNode = geometryNode.baseLine;
        ++index;
        baseLineNode.nodeAttribute.id = @(index).stringValue;
        
        // encode base line
        attributesNode = baseLineNode.attributes;
        int fontSize = round(self.suggestFontSize);
        attributesNode.fontSize.integerValue = fontSize;
        attributesNode.strokeZoomWithImage.boolValue = YES;
        bool measurementVisible = [self hasMeasurement] && ![self isMeasurementHidden];
        attributesNode.isMeasurementVisible.boolValue = measurementVisible;
        attributesNode.horizontalAlignment.stringValue = @"Center";
        attributesNode.verticalAlignment.stringValue = @"Top";
        
        [self encodeTextElementsToNode:baseLineNode alignType:@"AlignPosition"];
        CZIMutableMetadataNode *subGeometryNode = baseLineNode.geometry;
        CGPoint p1,p2;
        p1 = [self pointAtIndex:0];
        p2 = [self pointAtIndex:1];
        
        subGeometryNode.x1.floatValue = p1.x;
        subGeometryNode.y1.floatValue = p1.y;
        subGeometryNode.x2.floatValue = p2.x;
        subGeometryNode.y2.floatValue = p2.y;
        
        // calculate rotate (-a) degree affine transform
        CGFloat vecX = p2.x - p1.x;
        CGFloat vecY = p2.y - p1.y;
        vecY = -vecY;
        CGAffineTransform t = CGAffineTransformMakeRotationFromVector(vecX, vecY);
        
        // encode deltaX/deltaY
        CZIMutableMetadataNode *distancesNode = geometryNode.distances;
        for (NSUInteger j = 2; j < [self pointsCount]; ++j) {
            CGPoint p3 = [self pointAtIndex:j];
            CGPoint delta;
            delta.x = p3.x - p1.x;
            delta.y = p3.y - p1.y;
            delta = CGPointApplyAffineTransform(delta, t);
            
            [distancesNode appendChildNode:^(CZIMutableMetadataNode *root) {
                root.distance.deltaX.floatValue = delta.x;
                root.distance.deltaY.floatValue = delta.y;
            }];
        }
        
        [self encodeFeaturesToNode:multiCalipersNode];
    }];
    return ++index;
}

- (void)encodeFeaturesToNode:(CZIMutableMetadataNode *)parentNode {
    [parentNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *featuresNode = root.features;
        featuresNode[0].name.stringValue = @"Distances";
        featuresNode[0].isDisplayEnabled.boolValue = YES;
    }];
}

@end

#pragma mark - CZElementScaleBar

@implementation CZElementScaleBar (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGPoint pt1 = CGPointMake(elementNode.geometry.x1.floatValue, elementNode.geometry.y1.floatValue);
    CGPoint pt2 = CGPointMake(elementNode.geometry.x2.floatValue, elementNode.geometry.y2.floatValue);
    CZIMetadataNode *imageNode = elementNode.rootNode.imageDocument.metadata.information.image;
    self = [self initWithPoint:pt1 length:pt2.x - pt1.x bound:CGRectMake(0.0, 0.0, imageNode.sizeX.floatValue, imageNode.sizeY.floatValue)];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        if (!elementNode.corner.isEmptyNode) {
            self.position = (CZScaleBarPosition)elementNode.corner.integerValue;
        } else {
            self.position = kCZScaleBarPositionFree;
        }
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *scaleBarNode = root.scaleBar;
        scaleBarNode.nodeAttribute.id = @"0";
        
        CZIMutableMetadataNode *attributesNode = scaleBarNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        attributesNode.lineBeginStyle.stringValue = @"LargeBar";
        attributesNode.lineEndStyle.stringValue = @"LargeBar";
        
        [self encodeTextElementsToNode:scaleBarNode];
        
        CZIMutableMetadataNode *geometryNode = scaleBarNode.geometry;
        geometryNode.x1.floatValue = self.point.x;
        geometryNode.y1.floatValue = self.point.y;
        geometryNode.x2.floatValue = self.point.x + self.length;
        geometryNode.y2.floatValue = self.point.y;
        geometryNode.orientation.stringValue = @"Horizontal";
        
        scaleBarNode.corner.integerValue = self.position;
    }];
    return ++index;
}

@end

#pragma mark - CZElementText

@implementation CZElementText (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGRect frame = CGRectMake(elementNode.geometry.left.floatValue,
                              elementNode.geometry.top.floatValue,
                              elementNode.geometry.width.floatValue,
                              elementNode.geometry.height.floatValue);
    self = [self initWithFrame:frame];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
        
        self.string = elementNode.textElements[0].text.stringValue;
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *textBoxNode = root.textBox;
        textBoxNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = textBoxNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        attributesNode.horizontalAlignment.stringValue = @"Left";
        attributesNode.verticalAlignment.stringValue = @"Top";
        
        [self encodeTextElementsToNode:textBoxNode];
        
        CZIMutableMetadataNode *geometryNode = textBoxNode.geometry;
        geometryNode.left.floatValue = self.frame.origin.x;
        geometryNode.top.floatValue = self.frame.origin.y;
        geometryNode.width.floatValue = self.frame.size.width;
        geometryNode.height.floatValue = self.frame.size.height;
    }];
    return ++index;
}

- (void)encodeTextElementsToNode:(CZIMutableMetadataNode *)parentNode {
    [parentNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *textElementsNode = root.textElements;
        CZIMutableMetadataNode *textElementNode = textElementsNode.textElement;
        textElementNode.nodeAttribute.id = @"Custom";
        textElementNode.nodeAttribute.type = @"AlignPosition";
        textElementNode.text.stringValue = self.string;
        textElementNode.isEditable.boolValue = YES;
        textElementNode.isMovable.boolValue = NO;
        textElementNode.isWrapping.boolValue = YES;
        textElementNode.isRotateWithParent.boolValue = YES;
        textElementNode.position.pointValue = self.frame.origin;
    }];
}

@end

#pragma mark - CZElementCircle

@implementation CZElementCircle (CZIMetadata)

- (instancetype)initFromElementNode:(CZIMetadataNode *)elementNode {
    CGPoint center = CGPointMake(elementNode.geometry.centerX.floatValue, elementNode.geometry.centerY.floatValue);
    CGFloat radius = elementNode.geometry.radius.floatValue;
    self = [self initWithCenter:center radius:radius];
    if (self) {
        [self decodeAttributesFromElementNode:elementNode];
    }
    return self;
}

- (instancetype)initFromROIElementNode:(CZIMetadataNode *)elementNode {
    CGPoint center = CGPointMake(elementNode.geometry.centerX.floatValue, elementNode.geometry.centerY.floatValue);
    CGFloat radius = elementNode.geometry.radius.floatValue;
    self = [self initWithCenter:center radius:radius];
    if (self) {
        [self decodeROIAttributesFromElementNode:elementNode];
    }
    return self;
}

- (NSUInteger)encodeToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *circleNode = root.pointsCircle;
        circleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = circleNode.attributes;
        [self encodeToAttributesNode:attributesNode withIndex:index];
        
        [self encodeTextElementsToNode:circleNode alignType:@"RelativePosition.TopLeft"];
        
        CZIMutableMetadataNode *geometryNode = circleNode.geometry;
        geometryNode.centerX.floatValue = self.center.x;
        geometryNode.centerY.floatValue = self.center.y;
        geometryNode.radius.floatValue = self.radius;
        
        CZIMutableMetadataNode *creationPointsNode = geometryNode.creationPoints;
        [creationPointsNode appendChildNode:^(CZIMutableMetadataNode *root) {
            root.point.pointValue = [self pointAtIndex:0];
        }];
        [creationPointsNode appendChildNode:^(CZIMutableMetadataNode *root) {
            root.point.pointValue = [self pointAtIndex:1];
        }];
        [creationPointsNode appendChildNode:^(CZIMutableMetadataNode *root) {
            root.point.pointValue = [self pointAtIndex:2];
        }];
        
        [self encodeFeaturesToNode:circleNode];
    }];
    return ++index;
}

- (NSUInteger)encodeROIToElementsNode:(CZIMutableMetadataNode *)elementsNode withIndex:(NSUInteger)i {
    __block NSUInteger index = i;
    [elementsNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *circleNode = root.pointsCircle;
        circleNode.nodeAttribute.id = @(index).stringValue;
        
        CZIMutableMetadataNode *attributesNode = circleNode.attributes;
        [self encodeROIToAttributesNode:attributesNode];
        
        CZIMutableMetadataNode *geometryNode = circleNode.geometry;
        geometryNode.centerX.floatValue = self.center.x;
        geometryNode.centerY.floatValue = self.center.y;
        geometryNode.radius.floatValue = self.radius;
    }];
    return ++index;
}

@end

#pragma mark - CZElementLayer

@implementation CZElementLayer (CZIMetadata)

- (instancetype)initFromMetadata:(CZIMetadata *)metadata imageSize:(CGSize)imageSize {
    self = [self init];
    if (self) {
        if (CGSizeEqualToSize(imageSize, CGSizeZero)) {
            CZIMetadataNode *imageNode = metadata.root.information.image;
            imageSize.width = imageNode.sizeX.floatValue;
            imageSize.height = imageNode.sizeY.floatValue;
        }
        
        self.frame = CGRectMake(0.0, 0.0, imageSize.width, imageSize.height);
        
        CZIMetadataNode *elementsNode = metadata.root.layers[0].elements;
        for (CZIMetadataNode *elementNode in elementsNode) {
            CZElement *element = [CZElement elementFromNode:elementNode];
            if (element == nil) {
                continue;
            }
            
            CZIMetadataNode *attributesNode = elementNode.attributes;
            if (!attributesNode.fontSize.isEmptyNode) {
                float fontSize = attributesNode.fontSize.floatValue;
                CZElementSize size = [self elementSizeFromFontSize:fontSize];
                element.size = size;
            } else if (!attributesNode.strokeThickness.isEmptyNode) {
                float lineWidth = attributesNode.strokeThickness.floatValue;
                CZElementSize size = [self elementSizeFromLineWidth:lineWidth];
                element.size = size;
            }
            
            [self addElement:element];
            
            CZIMetadataNode *groupIdNode = attributesNode.groupId;
            if (!groupIdNode.isEmptyNode) {
                NSInteger groupID = groupIdNode.integerValue;
                
                if (groupID >= 0 && groupID < 1024) {
                    while (groupID >= self.groupCount) {
                        CZElementGroup *tempGroup = [[CZElementGroup alloc] init];
                        [self addGroup:tempGroup];
                        [tempGroup release];
                    }
                    
                    CZElementGroup *group = [self groupAtIndex:groupID];
                    [group addElement:element];
                }
            }
            
            if ([element isKindOfClass:[CZElementScaleBar class]] && elementNode.corner.isEmptyNode) {
                // force scaleBar update, so that the measurement label size becomes valid.
                CALayer *tempLayer = [element newLayer];
                [tempLayer release];
                
                CZElementScaleBar *scaleBar = (CZElementScaleBar *)element;
                [scaleBar snapToDefaultPosition:kDefaultSnapTolerance];
            }
        }
        
        [self groupNormalize];
    }
    return self;
}

+ (CZElementLayer *)readElementXMLFile:(NSData *)elementData {
    NSString *xmlString = [[NSString alloc] initWithData:elementData encoding:NSUTF8StringEncoding];
    CZIMetadata *metadata = [[CZIMetadata alloc] initWithXMLString:xmlString];
    CZElementLayer *elementLayer = [[CZElementLayer alloc] initFromMetadata:metadata imageSize:CGSizeZero];
    [metadata release];
    [xmlString release];
    return [elementLayer autorelease];
}

- (void)exportElementToXMLFilePath:(NSString *)path {
    CZIMutableMetadata *metadata = [[CZIMutableMetadata alloc] init];
    metadata.root.information.image.sizeX.floatValue = self.frame.size.width;
    metadata.root.information.image.sizeY.floatValue = self.frame.size.height;
    [self encodeToLayersNode:metadata.root.layers];
    [[metadata XMLStringRepresentation] writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    [metadata release];
}

- (void)encodeToLayersNode:(CZIMutableMetadataNode *)layersNode {
    [layersNode appendChildNode:^(CZIMutableMetadataNode *root) {
        CZIMutableMetadataNode *layerNode = root.layer;
        layerNode.nodeAttribute.name = @"Layer";
        layerNode.usage.stringValue = @"Annotation";
        layerNode.isProtected.boolValue = NO;
        layerNode.layerFlags.integerValue = 1;
        
        CZIMutableMetadataNode *elementsNode = layerNode.elements;
        
        NSUInteger index = 1;
        for (NSUInteger i = 0; i < self.elementCount; i++) {
            CZElement *element = [self elementAtIndex:i];
            
            // reserve index 0 for scale bar
            if ([element isKindOfClass:[CZElementScaleBar class]]) {
                [element encodeToElementsNode:elementsNode withIndex:0];
            } else {
                index = [element encodeToElementsNode:elementsNode withIndex:index];
            }
        }
        
        for (NSUInteger j = 0; j < self.groupCount; j++) {
            CZElementGroup *group = [self groupAtIndex:j];
            index = [group encodeToElementsNode:elementsNode withIndex:index];
        }
    }];
}

@end
