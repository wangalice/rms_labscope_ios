//
//  CZImageProperties.h
//  Hermes
//
//  Created by Ralph Jin on 3/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CZImageChannelProperties : NSObject <NSCopying>

@property (nonatomic, readonly, retain) NSNumber *grayscale;
@property (nonatomic, readonly, copy) NSString *channelName;
@property (nonatomic, readonly, retain) UIColor *channelColor;
@property (nonatomic, readonly, retain) NSNumber *channelSelection;
@property (nonatomic, readonly, copy) NSString *filterSetID;
@property (nonatomic, readonly, copy) NSString *filterSetName;
@property (nonatomic, readonly, copy) NSString *lightSource;
@property (nonatomic, readonly, copy) NSString *wavelength;
@property (nonatomic, readonly, retain) NSNumber *exposureTimeInSeconds;
@property (nonatomic, readonly, retain) NSNumber *gain;

- (instancetype)initWithGrayscale:(NSNumber *)grayscale
                      channelName:(NSString *)channelName
                     channelColor:(UIColor *)channelColor
                  channelSelected:(NSNumber *)channelSelected
                      filterSetID:(NSString *)filterSetID
                    filterSetName:(NSString *)filterSetName
                      lightSource:(NSString *)lightSource
                       wavelength:(NSString *)wavelength
            exposureTimeInSeconds:(NSNumber *)exposureTimeInSeconds
                             gain:(NSNumber *)gain;

- (instancetype)channelPropertiesByReplacingChannelName:(NSString *)channelName;
- (instancetype)channelPropertiesByReplacingChannelColor:(UIColor *)channelColor;
- (instancetype)channelPropertiesByReplacingChannelSelection:(NSNumber *)channelSelection;

@end

@interface CZImageProperties : NSObject

+ (NSDateFormatter *)defaultDateFormat;

@property (nonatomic, readonly, copy) NSString *microscopeManufacturer;
@property (nonatomic, readonly, copy) NSString *microscopeName;
@property (nonatomic, readonly, copy) NSString *microscopeModel;
@property (nonatomic, readonly, copy) NSString *cameraModel;
@property (nonatomic, readonly, copy) NSString *operatorName;
@property (nonatomic, readonly, retain) NSDate *acquisitionDate;

@property (nonatomic, readonly, copy) NSString *objectiveModel;  // microscope model and magnification
@property (nonatomic, readonly, retain) NSNumber *zoom; // e.g. zoom click stop; or optovar of compound AxioCam ERc 5s
@property (nonatomic, readonly, retain) NSNumber *cameraAdapter;    // camera adpter magnification
@property (nonatomic, readonly, copy) NSString *displayCameraAdapter;   // First use this string value instead of number value
@property (nonatomic, readonly, assign) float scaling;  // in um/pixel unit.
@property (nonatomic, readonly, retain) NSNumber *scalingInPixels;
@property (nonatomic, readonly, retain) NSNumber *bitDepth;

@property (nonatomic, readonly, copy) NSArray<CZImageChannelProperties *> *channels;

@property (nonatomic, retain, readonly) NSNumber *magnificationFactor; //equal to the totalMagnification
@property (nonatomic, retain, readonly) NSString *magnificationFactorString;
@property (nonatomic, retain) NSNumber *totalMagnification;
@property (nonatomic, retain) NSNumber *eyepieceMagnification;

/*! save metaData for saving to camera roll in Labscope iPhone. */
- (NSDictionary *)metadataOfImage:(UIImage *)image;

- (BOOL)readMetadataFromImageRepresentation:(NSData *)imageRepresentation;

- (NSData *)JPEGRepresentationOfImage:(UIImage *)jpgImage compression:(CGFloat)compression;

- (BOOL)isStereoMicroscopeMode;
- (BOOL)isMacroImageMicroscopeMode;

- (NSString *)combineImageDescription;
- (void)parseFromImageDescription:(NSString *)imageDescription;
- (void)parseFromModelName:(NSString *)modelName;

- (BOOL)readMetadataFromDict:(NSDictionary *)dict;

- (void)updateScaling:(float)scaling;

@end
