//
//  CZRegion2DInstanceItem.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZBufferProcessor;

@interface CZRegion2DInstanceItem : NSObject
@property (nonatomic, assign) uint32_t regionInstanceID;
@property (nonatomic, assign) uint32_t regionClassID;
@property (nonatomic, assign) int32_t left;
@property (nonatomic, assign) int32_t top;
@property (nonatomic, assign) uint32_t width;
@property (nonatomic, assign) uint32_t height;
@property (nonatomic, assign) uint32_t regionID;

+ (NSUInteger)getByteCount;

- (id)initWith:(uint32_t)regionInstanceID
 regionClassID:(uint32_t)regionClassID;

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
