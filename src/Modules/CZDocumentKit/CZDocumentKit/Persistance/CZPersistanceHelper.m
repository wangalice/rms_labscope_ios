//
//  CZPersistanceHelper.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZPersistanceHelper.h"
#import <CZToolbox/CZToolbox.h>
#import "CZAnalyzer2dResult.h"

static const int FixedSizePrefix = 12;
static const int IndexChunkType = 0;
static const int IndexByteCountBehind = 4;

typedef CZBufferProcessor BinaryWriter;
typedef CZBufferProcessor BinaryReader;

@implementation CZPersistanceHelper

+ (int64_t)write:(BinaryWriter *)writer chunk:(Analyzer2dChunkType)chunkType {
    return [self write:writer chunk:chunkType byteCountBehind:0];
}

+ (int64_t)write:(BinaryWriter *)writer chunk:(Analyzer2dChunkType)chunk byteCountBehind:(int64_t)byteCountBehind {
    int8_t bytesPrefix[FixedSizePrefix];
    memset(bytesPrefix, 0, FixedSizePrefix);
    
    BinaryWriter *bw = [[BinaryWriter alloc] initWithBuffer:bytesPrefix length:FixedSizePrefix];
    
    [bw seekToOffset:IndexChunkType];
    [bw writeLong:chunk];
    
    [bw seekToOffset:IndexByteCountBehind];
    [bw writeLongLong:byteCountBehind];
    [bw release];
    
    [writer writeBytes:bytesPrefix length:FixedSizePrefix];
    
    int64_t behindPosition = writer.offset;
    return behindPosition;
}

+ (void)write:(BinaryWriter *)writer byteCountOfPrefix:(int64_t)startPos {
    int ofs = -FixedSizePrefix + IndexByteCountBehind;
    // behind chunkType: + 4
    
    int64_t curPos = [writer offset];
    
    int64_t byteCount = curPos - startPos;
    [writer seekToOffset:(curPos - byteCount + ofs)];
    [writer writeLongLong:byteCount];
    [writer seekToOffset:curPos];
}

+ (void)write:(BinaryWriter *)writer position:(int)startPos length:(int)bufferLength string:(NSString*)text {
    [writer seekToOffset:startPos];
    
    if (text) {
        const char *utf8Text = [text cStringUsingEncoding:NSUTF8StringEncoding];
        size_t length = strlen(utf8Text) + 1;
        if (length > bufferLength) {
            length = bufferLength - 1;
        }
        [writer writeBytes:utf8Text length:length];
    }
    
    [writer writeByte:0];  // string terminal
}

+ (int64_t)readChunkPrefix:(BinaryReader *)reader {
    Analyzer2dChunkType chunkType;
    return [self readChunkPrefix:reader chunk:&chunkType];
}

+ (int64_t)readChunkPrefix:(BinaryReader *)reader chunk:(Analyzer2dChunkType *)chunkType {
    int8_t bytesPrefix[FixedSizePrefix];
    [reader readBytes:bytesPrefix length:FixedSizePrefix];
    
    BinaryReader *br = [[BinaryReader alloc] initWithBuffer:bytesPrefix length:FixedSizePrefix];
    
    [br seekToOffset:IndexChunkType];
    *chunkType = [br readLong];
    
    [br seekToOffset:IndexByteCountBehind];
    int64_t byteCountBehind = [br readLongLong];
    [br release];
    
    return byteCountBehind;
}

@end
