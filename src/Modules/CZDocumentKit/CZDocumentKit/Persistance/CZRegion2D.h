//
//  CZRegion2D.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CZPackedCrack.h"

@class CZScanLine;

@interface CZRegion2D : CZPackedCrack

@property (nonatomic, assign) uint32_t classID;
@property (nonatomic, assign) uint32_t idx;
@property (nonatomic, assign, readonly) NSMutableArray *subRegions;

- (id)initWithScanLines:(CZScanLine *)chords
                classID:(uint32_t)classID;

/** Fill region into scan line image. */
- (void)fillInImage:(CZScanLine *)scanLine;
- (BOOL)removeFromImage:(CZScanLine *)scanLine;

/** go through with outline of region. */
- (void)enumOutlineUsingBlock:(void(^)(uint8_t))block;

- (NSUInteger)area;

- (NSUInteger)subRegionCount;
- (CZRegion2D *)subRegionAtIndex:(NSUInteger)index;
- (void)addSubRegion:(CZRegion2D *)subRegion;

@end
