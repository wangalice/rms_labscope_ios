//
//  CZScanLine.mm
//  Matscope
//
//  Created by Jin Ralph on 3/8/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZScanLine.h"
#import <UIKit/UIKit.h>

#include <vector>
#include <algorithm>

namespace {
    class CZVector {
    public:
        typedef CZScanLineCord value_type;
        typedef value_type *iterator;
        
    public:
        CZVector() : _data (NULL), _size(0), _capacity(32 / sizeof(value_type)) {
            _data = (value_type *)malloc(32);
        }
        
        CZVector(const CZVector& rhs) : _data(NULL), _size(rhs._size), _capacity(rhs._capacity) {
            _data = (value_type *)malloc(_capacity * sizeof(value_type));
            memcpy(_data, rhs._data, _size * sizeof(value_type));
        }
        
        ~CZVector() {
            free(_data);
        }
        
        void clear() {
            _size = 0;
        }
        
        size_t size() const {
            return _size;
        }
        
        bool empty() const {
            return _size == 0;
        }
        
        void reserve(size_t capacity) {
            if (capacity > _capacity) {
                _capacity = capacity;
                _data = (value_type *)realloc(_data, capacity * sizeof(value_type));
            }
        }
        
        void erase(iterator it);
        void insert(iterator it, const value_type &v);
        
        void push_back(const value_type &v);
        
        iterator begin() const {
            return (iterator)_data;
        }
        
        iterator end() const {
            return ((iterator)_data) + _size;
        }
        
        const value_type& back() const {
            iterator it = end() - 1;
            return *it;
        }
        
    private:
        value_type *_data;
        size_t _size;
        size_t _capacity;
    };
    
    void CZVector::erase(CZVector::iterator it) {
        assert(it >= begin());
        assert(it < end());
        
        size_t distance = end() - it;
        if (distance > 1) {
            iterator next = it + 1;
            memcpy(it, next, sizeof(value_type) * (distance - 1));
        }
        
        _size--;
    }
    
    void CZVector::insert(CZVector::iterator it, const value_type &v) {
        assert(it >= begin());
        assert(it <= end());
        
        size_t distance;
        
        if (_size >= _capacity) {
            distance = it - begin();
            reserve(_capacity * 2);
            it = begin() + distance;
        }
        
        distance = end() - it;
        if (distance) {
            iterator next = it + 1;
            memcpy(next, it, sizeof(value_type) * distance);
        }
        
        *it = v;
        _size++;
    }
    
    void CZVector::push_back(const value_type& v) {
        if (_size >= _capacity) {
            reserve(_capacity * 2);
        }
        
        iterator it = end();
        *it = v;
        _size++;
    }
}

static const uint8_t kHeaderData[] = {
    0b00000000,
    0b10000000,
    0b11000000,
    0b11100000,
    0b11110000,
    0b11111000,
    0b11111100,
    0b11111110,
};

static const uint8_t kTailData[] = {
    0b00000000,
    0b01111111,
    0b00111111,
    0b00011111,
    0b00001111,
    0b00000111,
    0b00000011,
    0b00000001,
};

typedef CZScanLineCord cord_t;
typedef CZVector xlist_t;

@interface CZScanLine () {
    std::vector<xlist_t *> _lines;
}

@end

@implementation CZScanLine

- (id)init {
    [self release];
    return nil;
}

- (id)initWithWidth:(uint32_t)width height:(uint32_t)height {
    self = [super init];
    if (self) {
        _width = width;
        _height = height;
        
        _minX = _width;
        _maxX = 0;
        _minY = _height;
        _maxY = 0;
        
        _lines.resize(_height, NULL);
    }
    
    return self;
}

- (void)dealloc {
    for (std::vector<xlist_t *>::const_iterator it = _lines.begin(); it != _lines.end(); ++it ) {
        xlist_t *list = *it;
        delete list;
    }

    [super dealloc];
}

- (void)clear {
    for (size_t y = _minY; y < _maxY; y++) {
        xlist_t *list = _lines[y];
        if (list) {
            list->clear();
        }
    }

    _minX = _width;
    _maxX = 0;
    _minY = _height;
    _maxY = 0;
}

- (void)addEdgePointX:(CZScanLineCord)x y:(CZScanLineCord)y {
    if (y >= _height || y < 0) {
        return;
    }
    
    xlist_t *list = _lines[y];
    if (list == NULL) {
        list = new xlist_t;
        _lines[y] = list;
        
        list->push_back(x);
    } else {
        xlist_t::iterator x_it = std::lower_bound(list->begin(), list->end(), x);
        if (x_it == list->end()) {
            list->push_back(x);
        } else {
            if (*x_it == x) {
                list->erase(x_it);
            } else {
                list->insert(x_it, x);
            }
        }
    }

    _minX = std::min(x, _minX);
    _maxX = std::max(x, _maxX);
    _minY = std::min(y, _minY);
    y++;
    _maxY = std::max(y, _maxY);
}

- (void)addChordXStart:(CZScanLineCord)xs xEnd:(CZScanLineCord)xe y:(CZScanLineCord)y {
    NSAssert(xs < xe, @"end always bigger than start");
    
    if (xs < xe) {
        [self addEdgePointX:xs y:y];
        [self addEdgePointX:xe y:y];
    }
}

- (BOOL)getPointX:(CZScanLineCord)x y:(CZScanLineCord)y {
    xlist_t *list = NULL;
    if (y >= 0 && y < _lines.size()) {
        list = _lines[y];
    }

    if (list == NULL) {
        return NO;
    } else {
        xlist_t::iterator xit = std::lower_bound(list->begin(), list->end(), x);
        if (xit == list->end()) {
            return NO;
        } else {
            size_t i = std::distance(list->begin(), xit);
            if (i % 2) {  // right edge
                return x != (*xit);
            } else {     // left edge
                return x == (*xit);
            }
        }
    }
    
    return NO;
}

- (void)setPointX:(CZScanLineCord)x y:(CZScanLineCord)y white:(BOOL)white {
    BOOL isOriginWhite = [self getPointX:x y:y];
    if (isOriginWhite != white) {
        [self addEdgePointX:x y:y];
        [self addEdgePointX:x+1 y:y];
    }
}

- (BOOL)removePointX:(CZScanLineCord)x y:(CZScanLineCord)y {
    xlist_t *list = NULL;
    if (y >= 0 && y < _lines.size()) {
        list = _lines[y];
    }
    
    if (list) {
        xlist_t::iterator xit = std::lower_bound(list->begin(), list->end(), x);
        if (xit != list->end() && *xit == x) {
            list->erase(xit);
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

- (BOOL)isContainsPointX:(CZScanLineCord)x y:(CZScanLineCord)y {
    xlist_t *list = NULL;
    if (y >= 0 && y < _lines.size()) {
        list = _lines[y];
    }
    
    if (list) {
        xlist_t::iterator xit = std::lower_bound(list->begin(), list->end(), x);
        return (xit != list->end() && *xit == x);
    } else {
        return NO;
    }
}

- (CZScanLineCord)maxXonY:(CZScanLineCord)y {
    xlist_t *list = NULL;
    if (y >= 0 && y < _lines.size()) {
        list = _lines[y];
    }
    
    if (list == NULL) {
        return -1;
    } else {
        if (list->empty()) {
            return -1;
        } else {
            return list->back() - 1;
        }
    }
}

- (void)draw {
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self drawInContext:context];
}

- (void)drawInContext:(CGContextRef)context {
    if (_minX >= _maxX || _minY >= _maxY) {
        return;
    }
    
    cord_t width = _maxX - _minX;
    cord_t height = _maxY - _minY;
    
    size_t rowBytes = (width + 7) / 8;
    uint8_t *dataBlock = (uint8_t *)malloc(rowBytes * height);
    memset(dataBlock, 0b11111111, rowBytes * height);
    
    uint8_t *pLine = dataBlock + (height - 1) * rowBytes;
    for (cord_t y = _minY; y < _maxY; ++y) {
        xlist_t *list = _lines[y];
        if (list) {
            bool isLeft = true;
            cord_t left = 0, right;
            for (xlist_t::iterator xit = list->begin(); xit != list->end(); ++xit) {
                if (isLeft) {
                    left = *xit - _minX;
                } else {
                    // fill from left -> right
                    right = *xit - _minX;
                    right = std::min(width, right);
                    
                    size_t leftByteOffset = left / 8;
                    size_t leftBinOffset = left % 8;
                    size_t rightByteOffset = right / 8;
                    size_t rightBinOffset = right % 8;
                    
                    if (leftByteOffset == rightByteOffset) {
                        *(pLine + leftByteOffset) &= (kTailData[rightBinOffset] | kHeaderData[leftBinOffset]);
                    } else {
                        if (leftBinOffset > 0) {
                            *(pLine + leftByteOffset) &= kHeaderData[leftBinOffset];
                            leftByteOffset++;
                        }
                        
                        for (size_t offset = leftByteOffset; offset < rightByteOffset; offset++) {
                            *(pLine + offset) = 0x00;
                        }
                        
                        if (rightBinOffset) {
                            *(pLine + rightByteOffset) &= kTailData[rightBinOffset];
                        }
                    }
                }
                
                isLeft = !isLeft;
            }
            
            CGContextStrokePath(context);
        }
        pLine -= rowBytes;
    }

    CGContextSaveGState(context);
    CGContextSetShouldAntialias(context, false);
    CGContextSetAllowsAntialiasing(context, false);
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    
    CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, dataBlock, rowBytes * height, kCFAllocatorDefault);
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(data);
    CFRelease(data);
    
    CGImageRef mask = CGImageMaskCreate(width, height, 1, 1, rowBytes, dataProvider, NULL, true);
    CGDataProviderRelease(dataProvider);
    
    CGContextClipToMask(context, CGRectMake(_minX, _minY, width, height), mask);
    CGContextFillRect(context, CGRectMake(_minX, _minY, width, height));
    CGContextRestoreGState(context);
    
    CGImageRelease(mask);
}

- (void)enumerateLinesUsingBlock:(void (^)(CZScanLineCord xStart, CZScanLineCord xEnd, CZScanLineCord y))block {
    if (block) {
        for (int y = _minY; y < _maxY; y++) {
            xlist_t *list = _lines[y];
            if (list) {
                bool isLeft = true;
                cord_t left = 0;
                for (xlist_t::iterator xit = list->begin(); xit != list->end(); ++xit) {
                    if (isLeft) {
                        left = *xit;
                    } else {
                        block(left, *xit, y);
                    }
                    
                    isLeft = !isLeft;
                }
            }
        }
    }
}

- (BOOL)isValid {
    for (std::vector<xlist_t *>::iterator it = _lines.begin(); it != _lines.end(); ++it ) {
        xlist_t *list = *it;
        if (list && list->size() % 2) {
            return NO;
        }
    }
    return YES;
}

// Bresenham's line algorithm. see wiki for detail
// Octants:
// \2|1/
// 3\|/0
// --+--
// 4/|\7
// /5|6\

+ (void)lineStart:(CZScanLinePoint)start end:(CZScanLinePoint)end ddaUsingBlock:(BOOL(^)(int x, int y))block {
    if (!block) {
        return;
    }
    
    if (start.x > end.x) {
        std::swap(start, end);
    }
    
    int dx = end.x - start.x;
    int dy = end.y - start.y;
    int octant;
    if (dy >= 0) {
        octant = 0;
    } else {
        octant = 7;
    }
    
    dx = abs(dx);
    dy = abs(dy);
    
    if (dx < dy) {
        switch (octant) {
            case 0:
                octant = 1;
                break;
            case 7:
                octant = 6;
                break;
            default:
                break;
        }
        
        std::swap(dx, dy);
    }
    
    BOOL keepGoing = block(start.x, start.y);
    
    int delta = 2 * dy - dx;
    for (int x = 0, y = 0; x < dx && keepGoing; x++) {
        if (delta > 0) {
            switch (octant) {
                case 0:
                    keepGoing = block(start.x + x, start.y + y);
                    break;
                case 1:
                    keepGoing = block(start.x + y, start.y + x);
                    break;
                case 6:
                    keepGoing = block(start.x + y, start.y - x);
                    break;
                case 7:
                    keepGoing = block(start.x + x, start.y - y);
                    break;
                default:
                    break;
            }
            
            y++;
            delta += (dy * 2 - dx * 2);
        } else {
            delta += dy * 2;
        }
        
        switch (octant) {
            case 0:
                keepGoing = block(start.x + x, start.y + y);
                break;
            case 1:
                keepGoing = block(start.x + y, start.y + x);
                break;
            case 6:
                keepGoing = block(start.x + y, start.y - x);
                break;
            case 7:
                keepGoing = block(start.x + x, start.y - y);
                break;
            default:
                break;
        }
    }
}

- (void)cutWithLineStartPoint:(CZScanLinePoint)start
                     endPoint:(CZScanLinePoint)end
                   usingBlock:(void(^)(int x, int y, BOOL isFirstPoint))block {
    __block BOOL lastIsWhite = NO;
    
    [CZScanLine lineStart:start end:end ddaUsingBlock:^(int x, int y) {
        BOOL isWhite = [self getPointX:x y:y];
        if (isWhite) {
            [self setPointX:x y:y white:NO];
            if (block) {
                block(x, y, lastIsWhite != isWhite);
            }
        }

        lastIsWhite = isWhite;
        return YES;
    }];
}

- (BOOL)cutWithLineStartPoint:(CZScanLinePoint)start
                     endPoint:(CZScanLinePoint)end
                    tolerance:(CZScanLineCord)tolerance {
    return [self cutWithLineStartPoint:start
                              endPoint:end
                             tolerance:tolerance
                            usingBlock:NULL];
}

- (BOOL)cutWithLineStartPoint:(CZScanLinePoint)start
                     endPoint:(CZScanLinePoint)end
                    tolerance:(CZScanLineCord)tolerance
                   usingBlock:(void(^)(int x, int y, BOOL isFirstPoint))block {
    __block int sumDistance;
    __block CZScanLinePoint lastIntersection;
    __block int lastPointIsWhite;  // 0: black; 1: white; -1: unknown
    
    const int dx = abs(end.x - start.x);
    const int dy = abs(end.y - start.y);
    const BOOL vertical = dy > dx;
    
    BOOL(^ddaBlock)(int x, int y) = ^(int x, int y) {
        int isPointWhite = [self getPointX:x y:y] ? 1 : 0;
        
        if (lastPointIsWhite == -1) {
            if (isPointWhite == 0) {
                lastPointIsWhite = 0;
            }
        } else {
            if (isPointWhite != lastPointIsWhite) {  // if it's intersection
                if (lastPointIsWhite == 1) {  // if it's the second intersection
                    int distance = vertical ? abs(y - lastIntersection.y) : abs(x - lastIntersection.x);
                    sumDistance += distance;
                } else {  // if it's the first intersection
                    lastIntersection.x = x;
                    lastIntersection.y = y;
                }
                
                lastPointIsWhite = isPointWhite;
            }
        }
        
        return YES;
    };

    int minDistance = -1;
    int minT = 0;
    tolerance = tolerance < 1 ? 1 :tolerance;
    for (int t = -tolerance; t < tolerance; t++) {
        CZScanLinePoint tryStart = start, tryEnd = end;

        if (vertical) {
            tryStart.x += t;
            tryEnd.x += t;
        } else {
            tryStart.y += t;
            tryEnd.y += t;
        }

        // reset block variables
        sumDistance = 0;
        lastPointIsWhite = -1;
        
        [CZScanLine lineStart:tryStart end:tryEnd ddaUsingBlock:ddaBlock];
        
        if (sumDistance > 0) {
            if (minDistance < 0) {
                minDistance = sumDistance;
                minT = t;
            } else if (sumDistance < minDistance) {
                minDistance = sumDistance;
                minT = t;
            } else if (sumDistance == minDistance) {
                if (t == 0) {
                    minT = 0;
                }
            }
        }
    }

    if (minDistance > 0) {
        if (vertical) {
            start.y = start.y + minT;
        } else {
            start.x = start.x + minT;
        }

        [self cutWithLineStartPoint:start endPoint:end usingBlock:block];
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Flood fill

#include <stack>

struct FFScanLine {
    int xs;  // start x
    int xe;  // end x
    int y;   // y
    int dy;  // -1 or +1
};

static inline bool isRevisit(CZScanLine *pImage, const int &left, const int &right, const int &y) {
    for (int x = left; x <= right; ++x) {
        if ([pImage getPointX:x y:y]) {
            return false;
        }
    }
    
    return true;
}

static inline void pushVisitedLine(CZScanLine *pImage, const int& left, const int& right, const int &y) {
    [pImage removePointX:left y:y];
    [pImage removePointX:right + 1 y:y];
}

static inline int findLeft(int x, int y, CZScanLine *pImage, const int& minX) {
    while (x >= minX && [pImage getPointX:x y:y]) {
        x--;
    }
    return x;
}

static inline int findRight(int x, int y, CZScanLine *pImage, const int& maxX) {
    while (x <= maxX && [pImage getPointX:x y:y]) {
        x++;
    }
    return x;
}

static inline int skipRight(int x, int y, CZScanLine *pImage, const NSUInteger& maxX) {
    while (x <= maxX && ![pImage getPointX:x y:y]) {
        x++;
    }
    return x;
}

static inline void pushLine(std::stack<FFScanLine> &stack, const int &left, const int &right, const int &y, const int &dy) {
    FFScanLine scanline = { left, right, y, dy };
    stack.push(scanline);
}

static inline void popLine(std::stack<FFScanLine> &stack, int &left, int &right, int &y, int &dy) {
    const FFScanLine &scanLine = stack.top();
    
    left = scanLine.xs;
    right = scanLine.xe;
    y = scanLine.y;
    dy = scanLine.dy;
    
    stack.pop();
}

- (void)scanRegionsUsingCallback:(void(^)(CZScanLine *))callback {
    if (!callback) {
        return;
    }
    
    uint32_t maxX = self.width - 1;
    
    std::stack<FFScanLine> candidates;
    CZScanLine *subScanLine = [[CZScanLine alloc] initWithWidth:self.width height:self.height];
    
    for (int y_ = _minY; y_ < _maxY; y_++) {
        for (int x = _minX; x < _maxX; x++) {
            if (![self getPointX:x y:y_]) {
                continue;
            }
            
            int y = y_;
            int parentLeft = findLeft(x, y, self, 0) + 1;
            int parentRight = findRight(x, y, self, maxX) - 1;
            
            [subScanLine addChordXStart:parentLeft xEnd:parentRight + 1 y:y];
            pushVisitedLine(self, parentLeft, parentRight, y);
            
            // Push sarting point on stack
            pushLine(candidates, parentLeft, parentRight, y, 1);
            pushLine(candidates, parentLeft, parentRight, y, -1);
            
            // Start flooding
            while (!candidates.empty()) {
                int dy;
                popLine(candidates, parentLeft, parentRight, y, dy);
                y += dy;
                if (y < 0 || y >= _height) {
                    continue;
                }
                
                int coverLeft = parentLeft, coverRight = parentRight;
                if (parentLeft > 0) {
                    parentLeft --;
                }
                parentRight ++;
                if (parentRight > maxX) {
                    parentRight = maxX;
                }
                
                if (isRevisit(self, parentLeft, parentRight, y)) {  // if is revisit
                    continue;
                }
                
                int childLeft = findLeft(parentLeft, y, self, 0) + 1;
                int childRight;
                
                if (childLeft <= parentLeft) {
                    // Find ChildRight end
                    childRight = findRight(parentLeft, y, self, maxX) - 1;
                    
                    // Fill line
                    [subScanLine addChordXStart:childLeft xEnd:childRight + 1 y:y];
                    pushVisitedLine(self, childLeft, childRight, y);
                    
                    // Push unvisited lines
                    if (coverLeft <= childLeft && childRight <= coverRight) {
                        pushLine(candidates, childLeft, childRight, y, dy);
                    } else {
                        pushLine(candidates, childLeft, childRight, y, -dy);
                        pushLine(candidates, childLeft, childRight, y, dy);
                    }
                    // Advance ChildRight end on to border
                    ++childRight;
                } else {
                    childRight = parentLeft;
                }
                
                // Fill betweens
                while (childRight < parentRight) {
                    // Skip to new ChildLeft end ( ChildRight>ParentRight on failure )
                    childRight = skipRight(childRight, y,  self, parentRight);
                    // If new ChildLeft end found
                    if (childRight <= parentRight) {
                        childLeft = childRight;
                        // Find ChildRight end ( this should not fail here )
                        childRight = findRight(childLeft, y, self, maxX) - 1;
                        
                        // Fill line
                        [subScanLine addChordXStart:childLeft xEnd:childRight + 1 y:y];
                        pushVisitedLine(self, childLeft, childRight, y);
                        
                        // Push unvisited lines
                        if (childRight <= coverRight) {
                            pushLine(candidates, childLeft, childRight, y, dy);
                        } else {
                            pushLine(candidates, childLeft, childRight, y, -dy);
                            pushLine(candidates, childLeft, childRight, y, dy);
                        }
                        // Advance ChildRight end onto border
                        ++childRight;
                    }
                }
            }
            
            callback(subScanLine);
            
            [subScanLine clear];
        }
    }
}

@end
