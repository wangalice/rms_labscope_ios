//
//  CZPackedCrack.mm
//  Matscope
//
//  Created by Carl Zeiss on 12/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZPackedCrack.h"
#import <objc/runtime.h>
#include <vector>
#import <CZToolbox/CZToolbox.h>

#define CZCrackSizeInBit 2

static void enumCracksUsingBlock(const std::vector<PackedCrackEight>& cracks, uint32_t crackOffset, void (^block)(uint8_t direction)) {
    std::vector<PackedCrackEight>::const_iterator it = cracks.begin(), end = cracks.end();
    
    if (crackOffset == 0) {
        for (; it != end; ++it) {
            block(it->c1);
            block(it->c2);
            block(it->c3);
            block(it->c4);
            block(it->c5);
            block(it->c6);
            block(it->c7);
            block(it->c8);
        }
    } else {
        if (it == end) {
            return;
        }
        
        --end;
        for (; it != end; ++it) {
            block(it->c1);
            block(it->c2);
            block(it->c3);
            block(it->c4);
            block(it->c5);
            block(it->c6);
            block(it->c7);
            block(it->c8);
        }
        
        switch (crackOffset) {
            case 1:
                block(it->c1);
                break;
            case 2:
                block(it->c1);
                block(it->c2);
                break;
            case 3:
                block(it->c1);
                block(it->c2);
                block(it->c3);
                break;
            case 4:
                block(it->c1);
                block(it->c2);
                block(it->c3);
                block(it->c4);
                break;
            case 5:
                block(it->c1);
                block(it->c2);
                block(it->c3);
                block(it->c4);
                block(it->c5);
                break;
            case 6:
                block(it->c1);
                block(it->c2);
                block(it->c3);
                block(it->c4);
                block(it->c5);
                block(it->c6);
                break;
            case 7:
                block(it->c1);
                block(it->c2);
                block(it->c3);
                block(it->c4);
                block(it->c5);
                block(it->c6);
                block(it->c7);
                break;
            default:
                break;
        }
    }
}


@interface CZPackedCrack () {
    std::vector<PackedCrackEight> _cracksData;
    uint32_t _crackOffset;
}
@end

@implementation CZPackedCrack

+ (uint32_t)crackCount2CrackCountPacked:(uint32_t)crackCount {
    static const uint32_t packedBits = 2;
    static const uint32_t packedGroupCount = sizeof(uint16_t) * 8 / packedBits;
    
    return (crackCount + packedGroupCount - 1) / packedGroupCount;
}

- (id)init {
    return [self initWithStartPointX:0 y:0];
}

- (id)initWithStartPointX:(int32_t)x y:(int32_t)y {
    return [self initWithStartPointX:x y:y crackCount:0 crackPacked:NULL carackCountPacked:0];
}

- (id)initWithStartPointX:(int32_t)x
                        y:(int32_t)y
               crackCount:(size_t)crackCount
              crackPacked:(const uint16_t *)cracksPacked
        carackCountPacked:(size_t)crackCountPacked {
    self = [super init];
    if (self) {
        _startPointX = x;
        _startPointY = y;
        _cracksData.reserve(crackCountPacked);
        const PackedCrackEight *pDataEnd = (const PackedCrackEight *)cracksPacked + crackCountPacked;
        for (const PackedCrackEight *pData = (const PackedCrackEight *)cracksPacked; pData != pDataEnd; ++pData) {
            _cracksData.push_back(*pData);
        }
        _crackOffset = crackCount % 8;
    }
    return self;
}

- (size_t)crackCount {
    if (_crackOffset == 0) {
        return _cracksData.size() * 8;
    } else {
        return (_cracksData.size() - 1) * 8 + _crackOffset;
    }
}

- (const uint16_t *)cracksPacked {
    return (const uint16_t *)(&_cracksData[0]);
}

- (size_t)crackCountPacked {
    return _cracksData.size();
}

- (void)enumUsingBlock:(void (^)(uint8_t))block {
    enumCracksUsingBlock(_cracksData, _crackOffset, block);
}

- (void)addDirection:(uint8_t)direction {
    if (_crackOffset) {
        PackedCrackEight &packData = _cracksData.back();
        switch (_crackOffset) {
            case 1:
                packData.c2 = direction;
                ++_crackOffset;
                break;
            case 2:
                packData.c3 = direction;
                ++_crackOffset;
                break;
            case 3:
                packData.c4 = direction;
                ++_crackOffset;
                break;
            case 4:
                packData.c5 = direction;
                ++_crackOffset;
                break;
            case 5:
                packData.c6 = direction;
                ++_crackOffset;
                break;
            case 6:
                packData.c7 = direction;
                ++_crackOffset;
                break;
            case 7:
                packData.c8 = direction;
                _crackOffset = 0;
                break;
            default:
                break;
        }
    } else {
        PackedCrackEight packData;
        packData.c1 = direction;
        _cracksData.push_back(packData);
        ++_crackOffset;
    }
}

- (void)readCracksPacked:(CZBufferProcessor *)reader
              crackCount:(size_t)crackCount
        crackCountPacked:(size_t)crackCountPacked {
    _cracksData.reserve(_cracksData.size() + crackCountPacked);
    PackedCrackEight packData;
    for (size_t i = 0; i < crackCountPacked; ++i) {
        [reader readBytes:&packData length:sizeof(PackedCrackEight)];
        _cracksData.push_back(packData);
    }
    _crackOffset = crackCount % 8;
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    memorySize += _cracksData.size() * sizeof(PackedCrackEight);
    return memorySize;
}

@end
