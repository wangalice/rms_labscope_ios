//
//  CZRegion2DInstanceItemCollection.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCollection.h"

@class CZBufferProcessor;

@interface CZRegion2DInstanceItemCollection : CZCollection

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
