//
//  CZAnalysisRelation.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalysisRelation.h"
#include <objc/runtime.h>
#import <CZToolbox/CZToolbox.h>
#import "CZPersistanceHelper.h"

static const int FixedSize = 388;

static const int IndexRelationType = 0; // + 32
static const int IndexName = 32; // + 32
static const int IndexDescription = 96; // + 64
static const int IndexId = 124; // + 4
static const int IndexRoleArg1 = 132; // + 32
static const int IndexRoleArg2 = 164; // + 32
static const int IndexSuperIdArg1 = 196; // + 4
static const int IndexSuperIdArg2 = 200; // + 4

@interface CZAnalysisRelation () {
    CZAnalysisRelationArgumentCollection *_arguments;
}

@end

@implementation CZAnalysisRelation

+ (size_t)getByteCount {
    return FixedSize;
}

- (void)dealloc {
    [_relationDesc release];
    [_roleArg1 release];
    [_roleArg2 release];
    [_relationType release];
    [_name release];
    
    [_arguments release];
    
    [super dealloc];
}

- (void)write:(CZBufferProcessor *)writer {
    int8_t bytes [FixedSize];
    memset(bytes, 0, FixedSize);
    
    CZBufferProcessor *bw = [[CZBufferProcessor alloc] initWithBuffer:bytes length:FixedSize];

    [CZPersistanceHelper write:bw position:IndexRelationType length:IndexName - IndexRelationType string:self.relationType];
    [CZPersistanceHelper write:bw position:IndexName length:IndexDescription - IndexName string:self.name];
    [CZPersistanceHelper write:bw position:IndexDescription length:IndexId - IndexDescription string:self.relationDesc];
    
    [bw seekToOffset:IndexId];
    [bw writeLong:self.index];
    
    [CZPersistanceHelper write:bw position:IndexRoleArg1 length:IndexRoleArg2 - IndexRoleArg1 string:self.roleArg1];
    [CZPersistanceHelper write:bw position:IndexRoleArg2 length:IndexSuperIdArg1 - IndexRoleArg2 string:self.roleArg2];
    
    [bw seekToOffset:IndexSuperIdArg1];
    [bw writeLong:self.superIdArg1];
    
    [bw seekToOffset:IndexSuperIdArg2];
    [bw writeLong:self.superIdArg2];
    [bw release];
    
    [writer writeBytes:bytes length:FixedSize];
    
    [self.arguments write:writer];
}

- (CZAnalysisRelationArgumentCollection *)arguments {
    if (!_arguments) {
        _arguments = [[CZAnalysisRelationArgumentCollection alloc] init];
    }
    return _arguments;
}

- (void)read:(CZBufferProcessor *)reader {
    CZBufferProcessor *br = [reader newSubBufferReaderWithLength:FixedSize];
    [reader jumpByLength:FixedSize];
    
    self.relationType = [br readUTF8StringInRange:NSMakeRange(IndexRelationType, FixedSize - IndexRelationType)];
    self.name = [br readUTF8StringInRange:NSMakeRange(IndexName, FixedSize - IndexName)];
    self.relationDesc = [br readUTF8StringInRange:NSMakeRange(IndexDescription, FixedSize - IndexDescription)];
    
    [br seekToOffset:IndexId];
    self.index = [br readLong];
    
    self.roleArg1 = [br readUTF8StringInRange:NSMakeRange(IndexRoleArg1, FixedSize - IndexRoleArg1)];
    self.roleArg2 = [br readUTF8StringInRange:NSMakeRange(IndexRoleArg2, FixedSize - IndexRoleArg2)];
    
    [br seekToOffset:IndexSuperIdArg1];
    self.superIdArg1 = [br readLong];
    
    [br seekToOffset:IndexSuperIdArg2];
    self.superIdArg2 = [br readLong];
    [br release];
    
    [self.arguments read:reader];
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    memorySize += _relationDesc.length * 2;
    memorySize += _roleArg1.length * 2;
    memorySize += _roleArg2.length * 2;
    memorySize += _relationType.length * 2;
    memorySize += _name.length * 2;

    return memorySize;
}

@end
