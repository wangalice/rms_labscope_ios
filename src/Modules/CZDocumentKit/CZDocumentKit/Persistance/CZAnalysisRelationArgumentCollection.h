//
//  CZAnalysisRelationArgumentCollection.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCollection.h"

@class CZBufferProcessor;

@interface CZAnalysisRelationArgumentCollection : CZList

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

@end
