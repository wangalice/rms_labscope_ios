//
//  CZParticle2DItem.m
//  Matscope
//
//  Created by Ralph Jin on 5/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZParticle2DItem.h"
#include <Accelerate/Accelerate.h>

// see also GemMeasurerClr.h
#define CRACK_R     0  // right
#define CRACK_U     1  // up
#define CRACK_L     2  // left
#define CRACK_D     3  // down

const static vFloat kSinAngle[4] = {
    {0,                 0.195090322016128, 0.38268343236509,  0.555570233019602},
    {0.707106781186548, 0.831469612302545, 0.923879532511287, 0.98078528040323},
    {1,                 0.980785280403233, 0.923879532511287, 0.831469612302545},
    {0.707106781186548, 0.555570233019602, 0.38268343236509,  0.195090322016128}
};  // sin(0), sin(1/16*PI), ... sin(15/16*PI)

const static vFloat kCosAngle[4] = {
    {1,                  0.98078528040323,   0.923879532511287,  0.831469612302545},
    {0.707106781186548,  0.555570233019602,  0.38268343236509,   0.195090322016128},
    {0,                  -0.195090322016128, -0.38268343236509,  -0.555570233019602},
    {-0.707106781186548, -0.831469612302545, -0.923879532511287, -0.98078528040323}
};  // cos(0), cos(1/16*PI), ... cos(15/16*PI)

NSString * const kCZParticle2DKeyID = @"index";
NSString * const kCZParticle2DKeyArea = @"area";
NSString * const kCZParticle2DKeyDiameter = @"diameter";
NSString * const kCZParticle2DKeyPerimeter = @"perimeter";
NSString * const kCZParticle2DKeyMinFeret = @"minFeret";
NSString * const kCZParticle2DKeyMaxFeret = @"maxFeret";
NSString * const kCZParticle2DKeyFeretRatio = @"feretRatio";
const NSUInteger kCZFeretRatioPrecision = 2;

@implementation CZParticle2DItem

+ (void)calculateRegion:(CZRegion2D *)region
               diameter:(float *)diameter
                   area:(float *)area
              perimeter:(float *)perimeter
               feretMin:(float *)feretMin
               feretMax:(float *)feretMax {
    long x = 0;
    long y = 0;
    float top[16];
    float bottom[16];
    float left[16];
    float right[16];
    long area_ = 0;
    long nhv = 0;  // horizontal or vertical move
    long nd = 0;   // change direction move
    float fx, fy;
    
    for (int i = 0; i < 16; ++i) {
        top[i] = FLT_MAX;
        bottom[i] = -FLT_MAX;
        left[i] = FLT_MAX;
        right[i] = -FLT_MAX;
    }
    
    const PackedCrackFour *crack = (const PackedCrackFour *)[region cracksPacked];
    
    size_t lastID = [region crackCount] / 4;
    uint8_t lastCode = 0;
    if (crack) {
        switch ([region crackCount] % 4) {
            case 0:
                lastCode = crack[lastID].c4;
                break;
            case 1:
                lastCode = crack[lastID].c1;
                break;
            case 2:
                lastCode = crack[lastID].c2;
                break;
            case 3:
                lastCode = crack[lastID].c3;
                break;
            default:
                lastCode = crack[lastID].c4;
                break;
        }
    }
    
    for (NSUInteger i = 0, c = 0; c < [region crackCount]; i++, c += 4) {
        uint8_t code[4] = {crack[i].c1, crack[i].c2, crack[i].c3, crack[i].c4};
        
        NSUInteger remainCount = MIN(4, ([region crackCount] - c));
        for (NSUInteger j = 0; j < remainCount; j++) {
            if (lastCode == code[j]) {
                nhv++;
            } else {
                nd++;
            }
            
            switch (code[j]) {
                case CRACK_R:
                    x++;
                    break;
                case CRACK_U:
                    y--;
                    break;
                case CRACK_L:
                    x--;
                    break;
                case CRACK_D:
                    y++;
                    break;
                default:
                    assert(false);
                    break;
            }
            
            // calculate feret boundary in 16 directions
            fx = x;
            fy = y;
            
            vFloat vx = {fx, fx, fx, fx};
            vFloat vy = {fy, fy, fy, fy};
            
            vFloat tempVX[4], tempVY[4];
            for (int i = 0; i < 4; ++i) { // rotate fx fy
                tempVX[i] = vx * kCosAngle[i] - vy * kSinAngle[i];
                tempVY[i] = vx * kSinAngle[i] + vy * kCosAngle[i];
            }

            float *pfx = (float *)&tempVX;
            float *pfy = (float *)&tempVY;
            
            for (int i = 0; i < 16; ++i) {
                top[i] = MIN(top[i], pfy[i]);
                bottom[i] = MAX(bottom[i], pfy[i]);
                left[i] = MIN(left[i], pfx[i]);
                right[i] = MAX(right[i], pfx[i]);
            }
            
            lastCode = code[j];
        }
    }

    area_ = [region area];
    
    float width = right[0] - left[0];
    float height = bottom[0] - top[0];
    *feretMin = *feretMax = width;
    if (height < *feretMin) {
        *feretMin = height;
    } else if (height > *feretMax) {
        *feretMax = height;
    }
    for (int i = 1; i < 16; i++) {
        float width = right[i] - left[i];
        float height = bottom[i] - top[i];
        
        if (width < *feretMin) {
            *feretMin = width;
        } else if (width > *feretMax) {
            *feretMax = width;
        }
        if (height < *feretMin) {
            *feretMin = height;
        } else if (height > *feretMax) {
            *feretMax = height;
        }
    }
    
    *perimeter = nhv + nd * M_SQRT1_2;
    *area = fabs((float)area_);
    *diameter = sqrt(*area / M_PI) * 2;
}

- (id)initWithRegion:(CZRegion2D *)region {
    if (region == nil) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _index = [[NSNumber alloc] initWithInteger:region.idx];
        
        float diameter, area, perimeter, minFeret, maxFeret;
        [CZParticle2DItem calculateRegion:region
                                 diameter:&diameter
                                     area:&area
                                perimeter:&perimeter
                                 feretMin:&minFeret
                                 feretMax:&maxFeret];
        
        _diameter = [[NSNumber alloc] initWithFloat:diameter];
        _area = [[NSNumber alloc] initWithFloat:area];
        _perimeter = [[NSNumber alloc] initWithFloat:perimeter];
        _minFeret = [[NSNumber alloc] initWithFloat:minFeret];
        _maxFeret = [[NSNumber alloc] initWithFloat:maxFeret];
        _feretRatio = [[NSNumber alloc] initWithFloat:(minFeret / maxFeret)];
    }
    
    return self;
}

- (void)dealloc {
    [_index release];
    [_diameter release];
    [_area release];
    [_perimeter release];
    [_minFeret release];
    [_maxFeret release];
    [_feretRatio release];
    
    [super dealloc];
}

@end
