//
//  CZTraceScanner.h
//  Matscope
//
//  Created by Ralph Jin on 2/25/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//
#ifndef CZ_TRACE_SCANNER_H
#define CZ_TRACE_SCANNER_H

#import "CZScanLine.h"

@class CZRegion2D;

class CZTraceScanner {
public:
    static CZTraceScanner *shareInstance();
    
    CZTraceScanner();
    ~CZTraceScanner();
    
    bool ScanRegions(CZScanLine *ppLines, CZRegion2D *region);

private:
    // avoid copy
    CZTraceScanner(const CZTraceScanner& rhs);
    CZTraceScanner & operator=(const CZTraceScanner &rhs);
    
    /// Scan for out contour, and save it to region
    bool Scan(int32_t width, int32_t height, CZRegion2D* region, bool isSolid);

    CZScanLine *_currentImage;
    
    typedef BOOL (*GetPointIMP)(id, SEL, CZScanLineCord, CZScanLineCord);
    GetPointIMP _getPointMethod;
    SEL _getPointSelector;
};

#endif  // CZ_TRACE_SCANNER_H