//
//  CZAnalysisRelationCollection.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalysisRelationCollection.h"
#include <objc/runtime.h>
#import "CZPersistanceHelper.h"

static const int FixedSize = 64;
static const int IndexCount = 0;


@implementation CZAnalysisRelationCollection

- (void)write:(CZBufferProcessor *)writer {
    int64_t byteCountBehindPosition = [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeAnalysisRelationCollection];
    
    uint32_t byteCountAnalysisRelation = (uint32_t)[CZAnalysisRelation getByteCount];
    uint32_t byteCountAnalysisRelationCollection = FixedSize + byteCountAnalysisRelation * (uint32_t)[self count];
    
    int8_t bytes [FixedSize];
    memset(bytes, 0, FixedSize);
    
    CZBufferProcessor *bw = [[CZBufferProcessor alloc] initWithBuffer:bytes length:FixedSize];
    [bw seekToOffset:IndexCount];
    [bw writeLong:byteCountAnalysisRelationCollection];
    [bw release];
    
    [writer writeBytes:bytes length:FixedSize];
    
    for (CZAnalysisRelation *rel in self) {
        [rel write:writer];
    }
    
    [CZPersistanceHelper write:writer byteCountOfPrefix:byteCountBehindPosition];
}

- (void)read:(CZBufferProcessor *)reader {
    [self removeAllObjects];
    
    [CZPersistanceHelper readChunkPrefix:reader];
    
    CZBufferProcessor *br = [reader newSubBufferReaderWithLength:FixedSize];
    [reader jumpByLength:FixedSize];

    [br seekToOffset:IndexCount];
    int byteCountAnalysisRelationCollection = [br readLong];
    [br release];
    
    byteCountAnalysisRelationCollection -= FixedSize;
    size_t byteCountAnalysisRelation = [CZAnalysisRelation getByteCount];
    int countAnalysisRelation = (int)(byteCountAnalysisRelationCollection / byteCountAnalysisRelation);
    
    for (int i = 0; i < countAnalysisRelation; i++) {
        CZAnalysisRelation *rel = [[CZAnalysisRelation alloc] init];
        [rel read:reader];
        [self addObject:rel];
        [rel release];
    }
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    for (CZAnalysisRelation *rel in self) {
        memorySize += [rel memorySize];
    }
    return memorySize;
}

@end
