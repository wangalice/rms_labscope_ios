//
//  CZCollection.m
//  Matscope
//
//  Created by Ralph Jin on 2/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZCollection.h"

@interface CZCollection () {
    NSMutableDictionary *_dict;
}
@end

@implementation CZCollection

- (void)dealloc {
    [_dict release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZCollection *newCollection = [[[self class] alloc] init];
    if (newCollection) {
        newCollection->_dict = [self->_dict mutableCopyWithZone:zone];
    }
    return newCollection;
}

- (NSMutableDictionary *)dict {
    if (_dict == nil) {
        _dict = [[NSMutableDictionary alloc] init];
    }
    
    return _dict;
}

- (NSUInteger)count {
    return [self.dict count];
}

- (NSArray *)allKeys {
    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:self.dict];
    return [dict allKeys];
}

- (NSArray *)allValues {
    return [self.dict allValues];
}

- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey {
    [self.dict setObject:anObject forKey:aKey];
}

- (id)objectForKey:(id)aKey {
    return [self.dict objectForKey:aKey];
}

- (void)addValues:(NSDictionary *)values {
    [self.dict setValuesForKeysWithDictionary:values];
}

- (void)removeAllObjects {
    [self.dict removeAllObjects];
}

#pragma mark - NSFastEnumeration protocol

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id [])buffer count:(NSUInteger)len {
    return [self.dict countByEnumeratingWithState:state objects:buffer count:len];
}

@end


@interface CZList () {
    NSMutableArray *_array;
}
@end

@implementation CZList

- (id)initWithCapacity:(NSUInteger)capacity {
    self = [super init];
    if (self) {
        _array = [[NSMutableArray alloc] initWithCapacity:capacity];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    CZList *newList = [[[self class] allocWithZone:zone] init];
    if (newList) {
        newList->_array = [self->_array mutableCopyWithZone:zone];
    }
    
    return newList;
}

- (void)dealloc {
    [_array release];
    [super dealloc];
}

- (id)lastObject {
    return [self.array lastObject];
}

- (NSMutableArray *)array {
    if (_array == nil) {
        _array = [[NSMutableArray alloc] init];
    }
    
    return _array;
}

- (void)addObject:(id)object {
    [self.array addObject:object];
}

- (NSUInteger)count {
    return self.array.count;
}

- (id)objectAtIndex:(NSUInteger)index {
    return self.array[index];
}

- (NSUInteger)indexOfObject:(id)obj
              inSortedRange:(NSRange)r
                    options:(NSBinarySearchingOptions)opts
            usingComparator:(NSComparator)cmp {
    return [self.array indexOfObject:obj
                       inSortedRange:r
                             options:opts
                     usingComparator:cmp];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    [self.array replaceObjectAtIndex:index withObject:anObject];
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index {
    [self.array insertObject:object atIndex:index];
}

- (void)removeAllObjects {
    [self.array removeAllObjects];
}

#pragma mark - KVC

- (id)valueForKey:(NSString *)key {
    return [self.array valueForKey:key];
}

- (id)valueForKeyPath:(NSString *)keyPath {
    return [self.array valueForKeyPath:keyPath];
}

- (id)valueForUndefinedKey:(NSString *)key {
    return [self.array valueForUndefinedKey:key];
}

#pragma mark - NSFastEnumeration protocol

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id [])buffer count:(NSUInteger)len {
    return [self.array countByEnumeratingWithState:state objects:buffer count:len];
}

@end
