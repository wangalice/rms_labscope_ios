//
//  CZTraceScanner.m
//  Matscope
//
//  Created by Ralph Jin on 2/25/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#include "CZTraceScanner.h"
#include <assert.h>
#import "CZScanLine.h"
#import "CZRegion2D.h"
#import <ZEN/GemMeasurerClr.h>

#pragma mark - class CZTraceScanner

// c8 c4
// c2 c1
static inline int MakeDirectionCode(const int &c8, const int &c4, const int &c2, const int &c1) {
    return (c8 << 3) | (c4 << 2) | (c2 << 1) | (c1 << 0);
}

CZTraceScanner * CZTraceScanner::shareInstance() {
    static CZTraceScanner *sharedInstance = NULL;
    if (sharedInstance == NULL) {
        sharedInstance = new CZTraceScanner();
    }
    return sharedInstance;
}

CZTraceScanner::CZTraceScanner() {
}

CZTraceScanner::~CZTraceScanner() {
}

bool CZTraceScanner::ScanRegions(CZScanLine *ppLines, CZRegion2D *region) {
    this->_currentImage = ppLines;
    this->_getPointSelector = @selector(getPointX:y:);
    this->_getPointMethod = (CZTraceScanner::GetPointIMP)[_currentImage methodForSelector:_getPointSelector];
    
    const int32_t minX = [ppLines minX];
    const int32_t minY = [ppLines minY];
    const int32_t maxX = [ppLines maxX];
    const int32_t maxY = [ppLines maxY];
    
    if (minX > maxX || minY > maxY) {
        return false;
    }
    
    int32_t y = maxY - 1;

    if (y >= 0) {
        int32_t x = [_currentImage maxXonY:y];
        
        if (Scan(x, y, region, true)) {
            if (![region removeFromImage:_currentImage]) {  // fill the region, then inside holes appear
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
    
    // scan holes
    for (y = maxY - 1; y >= minY; --y) {
        for (int32_t x = [_currentImage maxXonY:y]; x > minX; x = [_currentImage maxXonY:y]) {
            CZRegion2D *rgn = [[CZRegion2D alloc] init];
            if (Scan(x, y, rgn, false)) {
                if (![rgn removeFromImage:_currentImage]) {
                    [rgn release];
                    return false;
                }
                [region addSubRegion:rgn];
                [rgn release];
            } else {
                [rgn release];
                return false;
            }
        }
    }
        
    return true;
}

/** scan cracks code from bottom right side
 * @param isSolid, true for scan solid region; false for scan holes in solid region;
 *                 Also, true for 8 directions scan; false for 4 directions scan.
 */
bool CZTraceScanner::Scan(int32_t xseed, int32_t yseed, CZRegion2D* region, bool isSolid) {
    const uint32_t xsize = _currentImage.width;
	const uint32_t ysize = _currentImage.height;
    
	if ( xseed < 0 || xsize <= xseed || yseed < 0 || ysize <= yseed) {
		return false;
	}

	int32_t startX = 0, startY = 0;
	
    int c8 = 0, c4 = 0, c2 = 0, c1 = 0;
    c8 = _getPointMethod(_currentImage, _getPointSelector, xseed, yseed);
    
    if (xseed + 1 < xsize) {
        c4 = _getPointMethod(_currentImage, _getPointSelector, xseed + 1, yseed);
        if (yseed + 1 < ysize) {
            c1 = _getPointMethod(_currentImage, _getPointSelector, xseed + 1, yseed + 1);
        }
    } else if (yseed + 1 < ysize) {
        c2 = _getPointMethod(_currentImage, _getPointSelector, xseed, yseed + 1);
    }
    
    if (c8 != 1 || c4 != 0) {
        return false;
    }

    int code = MakeDirectionCode(c8, c4, c2, c1);
    if ( code == 9 ) {  // ignore hole
        if (isSolid) {
            code = 13;
        } else {
            code = 8;
        }
    }
    
    int32_t x = startX = xseed + 1;
    int32_t y = startY = yseed + 1;
    int32_t y1 = y;
    int32_t y2 = yseed;
    
    while ( 1 )
    {
        // arche point at c1
        // c8 c4    y2
        // c2 c1    y1
        switch( code )
        {
            case 1: // 00
                    // 01

            case 5: // 01
                    // 01
                
            case 13:// 11
                    // 01
                [region addDirection:CRACK_D];
                y++;
                y2 = y1;
                if ( y != ysize ) {
                    y1 = y;
                    if ( x != 0 ) {
                        c2 = _getPointMethod(_currentImage, _getPointSelector, x - 1, y1);
                    } else {
                        c2 = 0;
                    }
                    c1 = _getPointMethod(_currentImage, _getPointSelector, x, y1);
                } else {
                    c2 = c1 = 0;
                }
                
                code = MakeDirectionCode(0, 1, c2, c1);
                
                if (code == 6) {
                    if (isSolid) {  // ignore hole
                        code = 7;
                    } else {  // scan hole
                        code = 4;
                    }
                }
                break;
                
            case 2: // 00
                    // 10
                
            case 3:	// 00
                    // 11
                
            case 7: // 01
                    // 11
                [region addDirection:CRACK_L];
                x--;
                
                if ( x != 0 ) {
                    if ( y != 0 ) {
                        c8 = _getPointMethod(_currentImage, _getPointSelector, x - 1, y2);
                    } else {
                        c8 = 0;
                    }
                    c2 = _getPointMethod(_currentImage, _getPointSelector, x - 1, y1);
                } else {
                    c8 = c2 = 0;
                }
                
                code = MakeDirectionCode(c8, 0, c2, 1);
                
                if ( code == 9 ) {
                    if (isSolid) {
                        code = 11;  // ignore holes
                    } else {
                        code = 1;   // scan hole
                    }
                }
                break;
                
            case 4: // 01
                    // 00
                
            case 12:// 11
                    // 00
                
            case 14:// 11
                    // 10
                [region addDirection:CRACK_R];
                x++;
                
                if ( x != xsize ) {
                    if ( y != ysize ) {
                        c1 = _getPointMethod(_currentImage, _getPointSelector, x, y1);
                    } else {
                        c1 = 0;
                    }
                    c4 = _getPointMethod(_currentImage, _getPointSelector, x, y2);
                } else {
                    c4 = c1 = 0;
                }
                
                code = MakeDirectionCode(1, c4, 0, c1);
                
                if ( code == 9 ) {
                    if (isSolid) {
                        code = 13;
                    } else {
                        code = 8;
                    }
                }
                break;
            case 8:  // 10
                     // 00
                
            case 10: // 10
                     // 10
                
            case 11: // 10
                     // 11
                [region addDirection:CRACK_U];
                y--;
                y1 = y2;
                
                if ( y != 0 )
                {
                    y2 = y-1;
                    if ( x != xsize ) {
                        c4 = _getPointMethod(_currentImage, _getPointSelector, x, y2);
                    } else {
                        c4 = 0;
                    }
                    c8 = _getPointMethod(_currentImage, _getPointSelector, x - 1, y2);
                } else {
                    c8 = c4 = 0;
                }
                
                code = MakeDirectionCode(c8, c4, 1, 0);
                
                if ( code == 6 ) {
                    if (isSolid) {  // ignore holes
                        code = 14;
                    } else {
                        code = 2;
                    }
                }
                break;
                
            case 6: // 01
                    // 10
                break;

            case 9: // 10
                    // 01
                break;
            
            case 0: // 00
                    // 00
                break;
            
            case 15:// 11
                    // 11
                break;
        }
        if ( x == startX && y == startY ) {
            break;
        }
    }
    
    region.startPointX = startX;
    region.startPointY = startY;
    
    return true; 
}
