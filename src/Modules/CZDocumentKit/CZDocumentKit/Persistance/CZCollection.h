//
//  CZCollection.h
//  Matscope
//
//  Created by Ralph Jin on 2/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CZCollection : NSObject<NSFastEnumeration, NSCopying>

- (NSUInteger)count;

- (NSArray *)allKeys;
- (NSArray *)allValues;

- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey;
- (id)objectForKey:(id)aKey;

- (void)addValues:(NSDictionary *)values;
- (void)removeAllObjects;

@end

@interface CZList : NSObject<NSFastEnumeration, NSCopying>

@property (nonatomic, readonly) id lastObject;

- (id)initWithCapacity:(NSUInteger)capacity;

- (void)addObject:(id)object;
- (NSUInteger)count;
- (id)objectAtIndex:(NSUInteger)index;

- (NSUInteger)indexOfObject:(id)obj
              inSortedRange:(NSRange)r
                    options:(NSBinarySearchingOptions)opts
            usingComparator:(NSComparator)cmp;

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject;
- (void)insertObject:(id)object atIndex:(NSUInteger)index;

- (void)removeAllObjects;
@end
