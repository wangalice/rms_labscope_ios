//
//  CZPersistanceHelper.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnalyzer2dResult.h"

@interface CZPersistanceHelper : NSObject

+ (void)write:(CZBufferProcessor *)writer position:(int)startPos length:(int)bufferLength string:(NSString*)text;

+ (int64_t)write:(CZBufferProcessor *)writer chunk:(Analyzer2dChunkType)chunk;
+ (int64_t)write:(CZBufferProcessor *)writer chunk:(Analyzer2dChunkType)chunk byteCountBehind:(int64_t)byteCountBehind;
+ (void)write:(CZBufferProcessor *)writer byteCountOfPrefix:(int64_t)startPos;

+ (int64_t)readChunkPrefix:(CZBufferProcessor *)reader;

@end
