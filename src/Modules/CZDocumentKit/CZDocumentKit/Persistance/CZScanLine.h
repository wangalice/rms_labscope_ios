//
//  CZScanLine.h
//  Matscope
//
//  Created by Jin Ralph on 3/8/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef short CZScanLineCord;

typedef struct _CZScanLinePoint {
    CZScanLineCord x;
    CZScanLineCord y;
} CZScanLinePoint;

@interface CZScanLine : NSObject

@property (nonatomic, assign, readonly) uint32_t width;
@property (nonatomic, assign, readonly) uint32_t height;
@property (nonatomic, assign, readonly) CZScanLineCord minX;
@property (nonatomic, assign, readonly) CZScanLineCord maxX;
@property (nonatomic, assign, readonly) CZScanLineCord minY;
@property (nonatomic, assign, readonly) CZScanLineCord maxY;

- (instancetype)init NS_UNAVAILABLE;
- (id)initWithWidth:(uint32_t)width height:(uint32_t)height;
- (void)clear;

- (void)addEdgePointX:(CZScanLineCord)x y:(CZScanLineCord)y;

/** add chord [xs, xe), which xe is excluded from filling, according to even-odd fill mode. */
- (void)addChordXStart:(CZScanLineCord)xs xEnd:(CZScanLineCord)xe y:(CZScanLineCord)y;

/** determine if point(x, y) shall be filled in even-odd fill mode*/
- (BOOL)getPointX:(CZScanLineCord)x y:(CZScanLineCord)y;
- (void)setPointX:(CZScanLineCord)x y:(CZScanLineCord)y white:(BOOL)white;

- (BOOL)removePointX:(CZScanLineCord)x y:(CZScanLineCord)y;

- (CZScanLineCord)maxXonY:(CZScanLineCord)y;

// fill scan lines on current CG context
- (void)draw;
- (void)drawInContext:(CGContextRef)context;

- (void)enumerateLinesUsingBlock:(void (^)(CZScanLineCord xStart, CZScanLineCord xEnd, CZScanLineCord y))block;

#if 0
- (BOOL)isValid;
- (BOOL)isContainsPointX:(CZScanLineCord)x y:(CZScanLineCord)y;
#endif

/**
 * @param block call back block to tell caller which points are cutted.
 * @return YES, if line can cut the area; otherwise NO.
 */
- (BOOL)cutWithLineStartPoint:(CZScanLinePoint)start
                     endPoint:(CZScanLinePoint)end
                    tolerance:(CZScanLineCord)tolerance;

- (BOOL)cutWithLineStartPoint:(CZScanLinePoint)start
                     endPoint:(CZScanLinePoint)end
                    tolerance:(CZScanLineCord)tolerance
                   usingBlock:(void(^)(int x, int y, BOOL isFirstPoint))block;

/**
 * use flood fill algorithm to find sub region.
 * Once a sub-region is found, it will call the callback block.
 * After finished, the scan line will be erased.
 */
- (void)scanRegionsUsingCallback:(void(^)(CZScanLine *))callback;

@end
