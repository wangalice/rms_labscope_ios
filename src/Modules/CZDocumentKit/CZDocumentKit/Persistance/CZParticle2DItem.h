//
//  CZParticle2DItem.h
//  Matscope
//
//  Created by Ralph Jin on 5/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZRegion2D.h"

extern NSString * const kCZParticle2DKeyID;
extern NSString * const kCZParticle2DKeyArea;
extern NSString * const kCZParticle2DKeyDiameter;
extern NSString * const kCZParticle2DKeyPerimeter;
extern NSString * const kCZParticle2DKeyMinFeret;
extern NSString * const kCZParticle2DKeyMaxFeret;
extern NSString * const kCZParticle2DKeyFeretRatio;
extern const NSUInteger kCZFeretRatioPrecision;

/** particle analysis result, unit is all in pixels*/
@interface CZParticle2DItem : NSObject

@property (nonatomic, retain, readonly) NSNumber *index;
@property (nonatomic, retain, readonly) NSNumber *diameter;
@property (nonatomic, retain, readonly) NSNumber *area;
@property (nonatomic, retain, readonly) NSNumber *perimeter;
@property (nonatomic, retain, readonly) NSNumber *minFeret;
@property (nonatomic, retain, readonly) NSNumber *maxFeret;
@property (nonatomic, retain, readonly) NSNumber *feretRatio;

- (id)initWithRegion:(CZRegion2D *)region;

@end
