//
//  CZParticles2DItem.h
//  Matscope
//
//  Created by Ralph Jin on 5/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZCollection.h"
#import "CZAnalyzer2dResult.h"

@class CZRegion2D;
@class CZParticle2DItem;

@interface CZParticleCriteria : NSObject<NSCopying>

@property (nonatomic, retain, readonly) NSDictionary *filters;
@property (nonatomic, copy, readonly) NSString *sortKey;
@property (nonatomic, assign, readonly) BOOL sortAscending;

- (instancetype)init NS_UNAVAILABLE;
- (id)initWithFilters:(NSDictionary *)filters sortKey:(NSString *)sortKey ascending:(BOOL)ascending;

- (float)filterMinValueForKey:(NSString *)key;
- (float)filterMaxValueForKey:(NSString *)key;

- (BOOL)hasFilter;
- (BOOL)hasFilterForKey:(NSString *)key;

- (BOOL)isValidRegion:(CZRegion2D *)region;

- (instancetype)criteriaByRemovingFilters;

@end

@interface CZParticles2DItem : CZList

@property (nonatomic, copy, readonly) NSString *sortKey;
@property (nonatomic, assign, readonly) BOOL sortAscending;

- (id)initWithAnalysisResult:(CZAnalyzer2dResult *)analyzerResult;

- (void)replaceItemWithItem:(CZParticle2DItem *)item;

- (float)minValueForKey:(NSString *)key;
- (float)maxValueForKey:(NSString *)key;

- (void)setFilterMin:(float)min max:(float)max forKey:(NSString *)key;
- (float)filterMinValueForKey:(NSString *)key;
- (float)filterMaxValueForKey:(NSString *)key;

- (void)clearFilterForKey:(NSString *)key;

- (void)clearAllFilters;
- (BOOL)hasFilter;
- (BOOL)hasFilterForKey:(NSString *)key;

- (void)sortByKey:(NSString *)key ascending:(BOOL)ascending;
- (void)clearSortKey;

- (CZParticleCriteria *)criteria;
- (void)setCriteria:(CZParticleCriteria *)criteria;

/** @return Still visible CZParticle2DItem(s) after filtering. */
- (NSArray *)visibleItems;
/** Invalidate visible items, so that they can be re-generated later.*/
- (void)invalidate;

@end
