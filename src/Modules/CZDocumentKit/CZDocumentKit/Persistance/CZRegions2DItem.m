//
//  CZRegions2DItem.m
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegions2DItem.h"
#import <objc/runtime.h>
#import <CZToolbox/CZToolbox.h>
#import "CZRegion2DItem.h"
#import "CZPersistanceHelper.h"

#define kRegions2DItemFixedSize 64
@implementation CZRegions2DItem

- (id)initWith:(uint32_t)regionInstanceID
 regionClassID:(uint32_t)regionClassID {
    self = [super init];
    if (self) {
        _regionClassID = regionClassID;
    }
    return self;
}

- (void)write:(CZBufferProcessor *)writer {
    int64_t byteCountBehindPosition = [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeRegions2dItem];
    
    NSMutableData *data = [NSMutableData dataWithCapacity:kRegions2DItemFixedSize];
    
    [data appendBytes:&_regionClassID length:1];
    
    uint32_t count = (uint32_t)self.count;
    [data appendBytes:&count length:sizeof(uint32_t)];
    
    [data setLength:kRegions2DItemFixedSize];
    
    [writer writeBytes:data.mutableBytes length:data.length];
    
    for (CZRegion2DItem *regionItem in self) {
        [regionItem write:writer];
    }
    
    [CZPersistanceHelper write:writer byteCountOfPrefix:byteCountBehindPosition];
}
    
- (void)read:(CZBufferProcessor *)reader {
    [CZPersistanceHelper readChunkPrefix:reader];
    
    uint64_t offset = reader.offset;
    
    self.regionClassID = [reader readByte];
    int argCount = [reader readLong];
    
    [reader seekToOffset:(offset + kRegions2DItemFixedSize)];
    
    for (int i = 0; i < argCount; i++) {
        CZRegion2DItem *item = [[CZRegion2DItem alloc] init];
        [item read:reader];
        [self addObject:item];
        [item release];
    }
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    for (CZRegion2DItem *regionItem in self) {
        memorySize += [regionItem memorySize];
    }
    return memorySize;
}

@end
