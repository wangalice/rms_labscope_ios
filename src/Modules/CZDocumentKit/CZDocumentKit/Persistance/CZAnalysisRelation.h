//
//  CZAnalysisRelation.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnalysisRelationArgumentCollection.h"

@class CZBufferProcessor;

@interface CZAnalysisRelation : NSObject

@property (nonatomic, copy) NSString *relationDesc;
@property (nonatomic, copy) NSString *roleArg1;
@property (nonatomic, copy) NSString *roleArg2;
@property (nonatomic, copy) NSString *relationType;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) int index;
@property (nonatomic, assign) int superIdArg1;
@property (nonatomic, assign) int superIdArg2;

@property (nonatomic, readonly, retain) CZAnalysisRelationArgumentCollection *arguments;

+ (size_t)getByteCount;

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
