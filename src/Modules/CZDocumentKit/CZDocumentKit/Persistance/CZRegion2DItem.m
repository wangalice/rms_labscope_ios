//
//  CZRegion2DItem.m
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegion2DItem.h"
#include <objc/runtime.h>
#import "CZRegion2D.h"
#import <CZToolbox/CZToolbox.h>

@interface CZRegion2DItem ()
@property (nonatomic, retain, readwrite) CZRegion2D *region;
@end

@implementation CZRegion2DItem

- (id)initWithRegion2D:(CZRegion2D *)region{
    self = [super init];
    if (self) {
        _region = [region retain];
        _id = region.idx;
    }
    return self;
}

- (void)dealloc {
    [_region release];
    
    [super dealloc];
}

- (NSData *)toByteArray {
    NSUInteger idx = self.region.idx;
    NSUInteger innerCount = self.region.subRegionCount;
    NSUInteger integerCount = 1 + 3 + 3 * innerCount + 1; // + classid
    int wordCountCrackPacked = 0;
    CZRegion2D *reg1 = nil;
    for (int ic = 0; ic <= innerCount; ic++) {
        reg1 = (ic == 0) ? self.region : [self.region subRegionAtIndex:ic - 1];
        wordCountCrackPacked += reg1.crackCountPacked;
    }
    
    NSInteger bufferSize = integerCount * sizeof(int32_t) + wordCountCrackPacked * sizeof(uint16_t);
    NSMutableData *buffer = [NSMutableData dataWithCapacity:bufferSize];
    
    [buffer appendBytes:&idx length:sizeof(int)];
    [buffer appendBytes:&innerCount length:sizeof(int)];
    
    for (int ic = 0; ic <= innerCount; ic++) {
        reg1 = (ic == 0) ? self.region : [self.region subRegionAtIndex:ic - 1];
        
        int32_t crackCount = (int32_t)reg1.crackCount;
        [buffer appendBytes:&crackCount length:sizeof(int32_t)];
        
        int32_t startX = reg1.startPointX;
        [buffer appendBytes:&startX length:sizeof(int32_t)];
        
        int32_t startY = reg1.startPointY;
        [buffer appendBytes:&startY length:sizeof(int32_t)];
        
        NSUInteger crackCountPacked = reg1.crackCountPacked;
        const uint16_t *cracks = reg1.cracksPacked;
        [buffer appendBytes:cracks length:(crackCountPacked * sizeof(uint16_t))];
    }
    
    assert(buffer.length == bufferSize);
    
    return buffer;
}

- (void)fromByteArray:(CZBufferProcessor *)reader length:(NSUInteger)length region:(CZRegion2D *)reg {
    int idx = [reader readLong];
    reg.idx = idx;
    
    int innerCount = [reader readLong];
    
    CZRegion2D *reg1;
    for (int ic = 0; ic <= innerCount; ic++)
    {
        if ([reader offset] >= length) {
            CZLogv(@"read region error: less data than expect.");
            break;
        }
        
        int crackCount = [reader readLong];
        
        int startX = [reader readLong];
        
        int startY = [reader readLong];
        
        if (ic == 0) {
            reg1 = reg;
        } else {
            reg1 = [[CZRegion2D alloc] init];
            [reg addSubRegion:reg1];
            [reg1 release];
        }
        
        uint32_t crackCountPacked = [CZRegion2D crackCount2CrackCountPacked:crackCount];
        uint64_t nextOffset = [reader offset] + crackCountPacked * sizeof(ushort);
        if (nextOffset > length) {
            CZLogv(@"read region error: less data than expect.");
            break;
        }
        
        [reg1 readCracksPacked:reader crackCount:crackCount crackCountPacked:crackCountPacked];
        reg1.startPointX = startX;
        reg1.startPointY = startY;
    }
}

- (void)write:(CZBufferProcessor *)writer {
    [writer writeLong:self.id];
    
    if (self.region) {
        NSData *data = [self toByteArray];
        [writer writeLong:(uint32_t)data.length];
        [writer writeBytes:data.bytes
                    length:data.length];
    } else {
        [writer writeLong:0];
    }
}

- (void)read:(CZBufferProcessor *)reader {
    self.id = [reader readLong];
    
    int byteCount = [reader readLong];
    
    CZBufferProcessor *br = [reader newSubBufferReaderWithLength:byteCount];
    [reader jumpByLength:byteCount];
    
    if (byteCount > 0) {
        CZRegion2D *reg = [[CZRegion2D alloc] init];
        [self fromByteArray:br length:byteCount region:reg];
        self.region = reg;
        [reg release];
    }
    
    [br release];
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    memorySize += [self.region memorySize];
    return memorySize;
}

@end
