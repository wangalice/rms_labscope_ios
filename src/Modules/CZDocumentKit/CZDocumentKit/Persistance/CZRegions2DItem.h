//
//  CZRegions2DItem.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCollection.h"

@class CZBufferProcessor;

@interface CZRegions2DItem : CZList
@property (nonatomic, assign) uint32_t regionClassID;

- (id)initWith:(uint32_t)regionInstanceID
 regionClassID:(uint32_t)regionClassID;
- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;
- (NSUInteger)memorySize;
@end
