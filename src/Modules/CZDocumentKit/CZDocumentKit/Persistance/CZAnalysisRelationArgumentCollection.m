//
//  CZAnalysisRelationArgumentCollection.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalysisRelationArgumentCollection.h"
#import <CZToolbox/CZToolbox.h>
#import "CZAnalysisRelationArgument.h"

@implementation CZAnalysisRelationArgumentCollection

- (void)write:(CZBufferProcessor *)writer {
    uint32_t argumentsCount = (uint32_t)self.count;
    [writer writeLong:argumentsCount];
    
    for (CZAnalysisRelationArgument *relArg in self) {
        [relArg write:writer];
    }
}

- (void)read:(CZBufferProcessor *)reader {
    int argumentsCount = [reader readLong];
    
    for (int i = 0; i < argumentsCount; i++) {
        CZAnalysisRelationArgument *relArg = [[CZAnalysisRelationArgument alloc] init];
        [relArg read:reader];
        [self addObject:relArg];
        [relArg release];
    }
}

@end
