//
//  CZAnalyzer2dResult.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CZToolbox/CZToolbox.h>
#import "CZRegion2DInstanceItemCollection.h"
#import "CZRegions2DItemCollection.h"
#import "CZAnalysisRelationCollection.h"

typedef NS_ENUM(NSUInteger, Analyzer2dChunkType) {
    Analyzer2dChunkTypeEndOfData = 0,
    Analyzer2dChunkTypeAnalysisRelationCollection = 1,
    Analyzer2dChunkTypeRegion2dInstanceItemCollection = 2,
    Analyzer2dChunkTypeDataVectorsCollectionList = 3,
    Analyzer2dChunkTypeDataVectorsCollection = 4,
    Analyzer2dChunkTypeDataVectors = 5,
    Analyzer2dChunkTypeDataVectorsDescription = 6,
    Analyzer2dChunkTypeRegions2dItem = 7,
    Analyzer2dChunkTypeRegions2dItemCollection = 8,
    Analyzer2dChunkTypeShapeItemsCollectionList = 9,
    Analyzer2dChunkTypeHeader = 10,
};

@class CZAnalysisRelationCollection;
@class CZRegion2D;

@interface CZAnalyzer2dResult : NSObject <NSCopying>

@property (nonatomic, assign) CGPoint imageOffset;
@property (nonatomic, assign) CGPoint imageOffsetW;

@property (nonatomic, retain) CZAnalysisRelationCollection *analysisRelationCollection;
@property (nonatomic, retain) CZRegion2DInstanceItemCollection *region2dInstanceItemCollection;
@property (nonatomic, retain) CZRegions2DItemCollection *regions2dItemCollection;

+ (void)addDefaultAnalysisRelation:(CZAnalysisRelationCollection *)analysisRelationCollection;

+ (void)writeHeader:(CZBufferProcessor *)writer imageOffset:(CGPoint)imageOffset imageOffsetW:(CGPoint)imageOffsetW;

+ (void)writeEndOfData:(CZBufferProcessor *)writer;

- (void)addRegions:(NSArray *)regions regionInstances:(NSArray *)regionIntances atIndex:(NSUInteger)index;

- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
