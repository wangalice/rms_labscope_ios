//
//  CZRegion2DInstanceItem.m
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegion2DInstanceItem.h"
#import <CZToolbox/CZToolbox.h>
#include <objc/runtime.h>

static const int FixedSize = 30;
static const int IndexRegionInstanceID = 0; // + 4
static const int IndexRegionClassID = 4; // + 1
static const int IndexLeft = 5; // + 4
static const int IndexTop = 9; // + 4
static const int IndexWidth = 13; // + 4
static const int IndexHeight = 17; // + 4
static const int IndexRegionID = 21; // + 4

@implementation CZRegion2DInstanceItem

- (id)initWith:(uint32_t)regionInstanceID
 regionClassID:(uint32_t)regionClassID {
    self = [super init];
    if (self) {
        _regionInstanceID = regionInstanceID;
        _regionClassID = regionClassID;
        _regionID = -1;
    }
    return self;
}

+ (NSUInteger)getByteCount {
    return FixedSize;
}

- (void)write:(CZBufferProcessor *)writer {
    int8_t bytes[FixedSize];
    memset(bytes, 0, FixedSize);
    
    CZBufferProcessor *bw = [[CZBufferProcessor alloc] initWithBuffer:bytes length:FixedSize];
    
    [bw writeLong:_regionInstanceID];
    [bw writeByte:_regionClassID];
    [bw writeLong:_left];
    [bw writeLong:_top];
    [bw writeLong:_width];
    [bw writeLong:_height];
    [bw writeLong:_regionID];
    
    [writer writeBytes:bytes length:FixedSize];
    [bw release];
}

- (void)read:(CZBufferProcessor *)reader {
    uint64_t offset = reader.offset;
    
    [reader seekToOffset:(offset + IndexRegionInstanceID)];
    self.regionInstanceID = [reader readLong];
    
    [reader seekToOffset:(offset + IndexRegionClassID)];
    self.regionClassID = [reader readByte];
    
    [reader seekToOffset:(offset + IndexLeft)];
    self.left = [reader readLong];
    
    [reader seekToOffset:(offset + IndexTop)];
    self.top = [reader readLong];
    
    [reader seekToOffset:(offset + IndexWidth)];
    self.width = [reader readLong];
    
    [reader seekToOffset:(offset + IndexHeight)];
    self.height = [reader readLong];
    
    [reader seekToOffset:(offset + IndexRegionID)];
    self.regionID = [reader readLong];
    
    [reader seekToOffset:(offset + FixedSize)];
}

- (NSUInteger)memorySize {
    return class_getInstanceSize([self class]);
}
@end
