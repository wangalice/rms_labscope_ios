//
//  CZRegions2DItemCollection.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCollection.h"

@class CZBufferProcessor;
@class CZRegions2DItem;

@interface CZRegions2DItemCollection : CZCollection

- (NSNumber *)getKeyForItem:(CZRegions2DItem *) item;
- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
