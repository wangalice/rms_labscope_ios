//
//  CZParticles2DItem.m
//  Matscope
//
//  Created by Ralph Jin on 5/5/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZParticles2DItem.h"
#import "CZRegion2DInstanceItem.h"
#import "CZRegions2DItem.h"
#import "CZRegion2DItem.h"
#import "CZRegions2DItemCollection.h"
#import "CZParticle2DItem.h"

@implementation CZParticleCriteria

- (id)init {
    [self release];
    return nil;
}

- (id)initWithFilters:(NSDictionary *)filters sortKey:(NSString *)sortKey ascending:(BOOL)ascending {
    if (filters.count == 0 && sortKey.length == 0) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _filters = [[NSDictionary alloc] initWithDictionary:filters];
        _sortKey = [sortKey copy];
        _sortAscending = ascending;
    }
    return self;
}

- (void)dealloc {
    [_filters release];
    [_sortKey release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithFilters:_filters
                                                      sortKey:_sortKey
                                                    ascending:_sortAscending];
}

- (BOOL)isEqual:(id)object {
    if ([object isMemberOfClass:[CZParticleCriteria class]]) {
        return [self isEqualToCriteria:(CZParticleCriteria *)object];
    } else {
        return NO;
    }
}

- (BOOL)isEqualToCriteria:(CZParticleCriteria *)criteria {
    if (self.sortKey) {
        if (![self.sortKey isEqualToString:criteria.sortKey]) {
            return NO;
        }
        
        if (self.sortAscending != criteria.sortAscending) {
            return NO;
        }
    } else {
        if (criteria.sortKey) {
            return NO;
        }
    }
    
    return [_filters isEqualToDictionary:criteria.filters];
}

- (float)filterMinValueForKey:(NSString *)key {
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSNumber *number = [_filters objectForKey:minKey];
    if (number == nil) {
        return -FLT_MAX;
    }
    return [number floatValue];
}

- (float)filterMaxValueForKey:(NSString *)key {
    NSString *maxKey = [key stringByAppendingString:@".max"];
    NSNumber *number = [_filters objectForKey:maxKey];
    if (number == nil) {
        return FLT_MAX;
    }
    return [number floatValue];
}

- (BOOL)hasFilter {
    return (self.filters.count > 0);
}

- (BOOL)hasFilterForKey:(NSString *)key {
    if (key == nil) {
        return NO;
    }
    
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSString *maxKey = [key stringByAppendingString:@".max"];
    return ([_filters objectForKey:minKey] != nil || [_filters objectForKey:maxKey] != nil);
}

- (BOOL)isValidRegion:(CZRegion2D *)region {
    CZParticle2DItem *item = [[[CZParticle2DItem alloc] initWithRegion:region] autorelease];
    if (item) {
        NSArray *properties = @[@"area", @"diameter", @"perimeter", @"minFeret", @"maxFeret", @"feretRatio"];
        for (NSString *property in properties) {
            NSString *minKey = [property stringByAppendingString:@".min"];
            NSNumber *minThreshold = [_filters objectForKey:minKey];
            NSNumber *value = nil;
            
            if (minThreshold) {
                value = [item valueForKey:property];
                if ([value compare:minThreshold] == NSOrderedAscending) {
                    return NO;
                }
            }
            
            NSString *maxKey = [property stringByAppendingString:@".max"];
            NSNumber *maxThreshold = [_filters objectForKey:maxKey];
            if (maxThreshold) {
                if (value == nil) {
                    value = [item valueForKey:property];
                }
                if ([value compare:maxThreshold] == NSOrderedDescending) {
                    return NO;
                }
            }
        }
        
        return YES;
    } else {
        return NO;
    }
}

- (instancetype)criteriaByRemovingFilters {
    CZParticleCriteria *criteria = [[CZParticleCriteria alloc] initWithFilters:nil
                                                                       sortKey:self.sortKey
                                                                     ascending:self.sortAscending];
    return [criteria autorelease];
}

@end

@interface CZParticles2DItem () {
    BOOL _requireSort;
}

@property (nonatomic, retain) NSArray *visibleItems;  // array of CZParticle2DItem
@property (nonatomic, retain) NSMutableDictionary *filters;
@property (nonatomic, retain) NSMutableDictionary *values;

@property (nonatomic, copy) NSString *sortKey;
@property (nonatomic, assign) BOOL sortAscending;

@end

@implementation CZParticles2DItem

- (id)initWithAnalysisResult:(CZAnalyzer2dResult *)analyzerResult {
    self = [super init];
    if (self) {
        NSArray *orderedKeys = [analyzerResult.regions2dItemCollection.allKeys sortedArrayUsingComparator:^(id a, id b) {
            return [(NSNumber *)a compare:(NSNumber *)b];
        }];
        
        for (id key in orderedKeys) {
            CZRegions2DItem *regionsItem = [analyzerResult.regions2dItemCollection objectForKey:key];
            for (CZRegion2DItem *regionItem in regionsItem) {
                CZRegion2D *region = regionItem.region;
                CZParticle2DItem *particle = [[CZParticle2DItem alloc] initWithRegion:region];
                if (particle) {
                    [super addObject:particle];
                    [particle release];
                }
            }
        }
    }
    
    return self;
}

- (void)dealloc {
    [_visibleItems release];
    [_filters release];
    [_values release];
    [_sortKey release];
    [super dealloc];
}

- (void)replaceItemWithItem:(CZParticle2DItem *)newItem {
    NSUInteger foundIndex = [self indexOfObject:newItem
                                  inSortedRange:NSMakeRange(0, [self count])
                                        options:NSBinarySearchingFirstEqual
                                usingComparator:^(id lhs, id rhs) {
                                    CZParticle2DItem *left = lhs;
                                    CZParticle2DItem *right = rhs;
                                    return [left.index compare:right.index];
                                }];

    if (foundIndex != NSNotFound) {
        CZParticle2DItem *oldItem = [self objectAtIndex:foundIndex];
        [oldItem retain];
        
        [self replaceObjectAtIndex:foundIndex withObject:newItem];
        
        if (_visibleItems) {
            NSMutableArray *tempArray = [NSMutableArray arrayWithArray:_visibleItems];
            foundIndex = [tempArray indexOfObject:oldItem];
            if (foundIndex != NSNotFound) {
                [tempArray replaceObjectAtIndex:foundIndex withObject:newItem];
            } else {
                [tempArray addObject:newItem];
            }
            self.visibleItems = [NSArray arrayWithArray:tempArray];
            self.values = nil;
            _requireSort = YES;
        }
        
        [oldItem release];
    }
}

- (void)property:(float (^)(CZParticle2DItem *particle))propertyBlock min:(float *)min max:(float *)max {
    float minValue, maxValue;
    if ([self count] && propertyBlock) {
        CZParticle2DItem *particle = [self objectAtIndex:0];
        
        float value;
        minValue = maxValue = propertyBlock(particle);
        for (particle in self) {
            value = propertyBlock(particle);
            if (value < minValue) {
                minValue = value;
            } else if (value > maxValue) {
                maxValue = value;
            }
        }
    } else {
        minValue = maxValue = 0;
    }
    
    if (min) {
        *min = minValue;
    }
    
    if (max) {
        *max = maxValue;
    }
}

- (float)minValueForKey:(NSString *)key {
    NSString *minKey = [NSString stringWithFormat:@"@min.%@", key];
    NSNumber *minValue = [self.values objectForKey:minKey];
    
    if (minValue == nil) {
        minValue = [self valueForKeyPath:minKey];

        if (minValue) {
            if (strcmp([minValue objCType], @encode(float)) == 0) {
                minValue = @(floor([minValue floatValue] * 1e4f) * 1e-4f);
            } else if (strcmp([minValue objCType], @encode(double)) == 0) {
                minValue = @(floor([minValue doubleValue] * 1e4f) * 1e-4f);
            }
            [self.values setObject:minValue forKey:minKey];
        }
    }
    
    return (minValue != nil) ? [minValue floatValue] : 0;
}

- (float)maxValueForKey:(NSString *)key {
    NSString *maxKey = [NSString stringWithFormat:@"@max.%@", key];
    NSNumber *maxValue = [self.values objectForKey:maxKey];
    
    if (maxValue == nil) {
        maxValue = [self valueForKeyPath:maxKey];
        if (maxValue) {
            if (strcmp([maxValue objCType], @encode(float)) == 0) {
                maxValue = @(ceil([maxValue floatValue] * 1e4f) * 1e-4f);
            } else if (strcmp([maxValue objCType], @encode(double)) == 0) {
                maxValue = @(ceil([maxValue doubleValue] * 1e4f) * 1e-4f);
            }
            
            [self.values setObject:maxValue forKey:maxKey];
        }
    }
    
    return (maxValue != nil) ? [maxValue floatValue] : 0;
}

- (void)setFilterMin:(float)min max:(float)max forKey:(NSString *)key {
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSString *maxKey = [key stringByAppendingString:@".max"];
    [self.filters setObject:@(min) forKey:minKey];
    [self.filters setObject:@(max) forKey:maxKey];
    self.visibleItems = nil;
}

- (float)filterMinValueForKey:(NSString *)key {
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSNumber *number = [_filters objectForKey:minKey];
    if (number == nil) {
        return -DBL_MAX;
    }
    return [number floatValue];
}

- (float)filterMaxValueForKey:(NSString *)key {
    NSString *maxKey = [key stringByAppendingString:@".max"];
    NSNumber *number = [_filters objectForKey:maxKey];
    if (number == nil) {
        return DBL_MAX;
    }
    return [number floatValue];
}

- (void)clearFilterForKey:(NSString *)key {
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSString *maxKey = [key stringByAppendingString:@".max"];
    
    [_filters removeObjectForKey:minKey];
    [_filters removeObjectForKey:maxKey];
    self.visibleItems = nil;
}

- (void)clearAllFilters {
    self.visibleItems = nil;
    self.filters = nil;
}

- (BOOL)hasFilter {
    if (self.filters.count > 0) {
        if (self.visibleItems.count == [self count]) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return NO;
    }
}

- (BOOL)hasFilterForKey:(NSString *)key {
    if (key == nil) {
        return NO;
    }
    
    NSString *minKey = [key stringByAppendingString:@".min"];
    NSString *maxKey = [key stringByAppendingString:@".max"];
    return ([_filters objectForKey:minKey] != nil || [_filters objectForKey:maxKey] != nil);
}

- (void)sortByKey:(NSString *)key ascending:(BOOL)ascending {
    self.sortKey = key;
    self.sortAscending = ascending;
    _requireSort = YES;
}

- (void)clearSortKey {
    [self sortByKey:nil ascending:YES];
    self.visibleItems = nil;
}

- (CZParticleCriteria *)criteria {
    return [[[CZParticleCriteria alloc] initWithFilters:self.filters
                                                sortKey:self.sortKey
                                              ascending:self.sortAscending] autorelease];
}

- (void)setCriteria:(CZParticleCriteria *)criteria {
    self.sortKey = criteria.sortKey;
    self.sortAscending = criteria.sortAscending;
    if (criteria.filters) {
        self.filters = [NSMutableDictionary dictionaryWithDictionary:criteria.filters];
    } else {
        self.filters = nil;
    }
    
    _requireSort = self.sortKey != nil;
    self.visibleItems = nil;
}

- (NSArray *)visibleItems {
    if (_visibleItems == nil) {
        _requireSort = YES;
        _visibleItems = [[self filterResult:self] retain];
    }

    if (_requireSort) {
        if (_sortKey) {
            NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:_sortKey ascending:_sortAscending];
            self.visibleItems = [_visibleItems sortedArrayUsingDescriptors:@[sortDescriptor]];
        }
        _requireSort = NO;
    }
    
    return _visibleItems;
}

- (void)invalidate {
    self.visibleItems = nil;
    self.values = nil;
    _requireSort = YES;
}

- (NSMutableDictionary *)filters {
    if (_filters == nil) {
        _filters = [[NSMutableDictionary alloc] init];
    }
    return _filters;
}

- (NSMutableDictionary *)values {
    if (_values == nil) {
        _values = [[NSMutableDictionary alloc] init];
    }
    return _values;
}

#pragma mark - override methods

- (void)addObject:(id)object {
    NSAssert([object isKindOfClass:[CZParticle2DItem class]], @"add object which is not CZParticle2DItem");
    
    CZParticle2DItem *newItem = object;
    CZParticle2DItem *lastItem = [self lastObject];
    if (lastItem && [newItem.index compare:lastItem.index] != NSOrderedDescending) {
        NSAssert(NO, @"Add item at end, but its index is not greater than the last one. Maybe you should use \"replaceItemWithItem\" method.");
        return;
    }
    
    [super addObject:object];
    
    if (_visibleItems) {
        self.visibleItems = [_visibleItems arrayByAddingObject:object];
        self.values = nil;
        _requireSort = YES;
    }
}

#pragma mark - Private methods

- (NSArray *)filterResult:(NSObject<NSFastEnumeration> *)sourceItems {
    NSMutableArray *filterResult = [NSMutableArray array];
    
    if (_filters) {
        NSArray *properties = @[@"area", @"diameter", @"perimeter", @"minFeret", @"maxFeret", @"feretRatio"];
        NSMutableArray *minFilters = [NSMutableArray arrayWithCapacity:[properties count]];
        NSMutableArray *maxFilters = [NSMutableArray arrayWithCapacity:[properties count]];
        
        for (NSString *property in properties) {
            NSString *minKey = [property stringByAppendingString:@".min"];
            NSNumber *minThreshold = [_filters objectForKey:minKey];
            if (minThreshold) {
                [minFilters addObject:minThreshold];
            } else {
                [minFilters addObject:[NSNull null]];
            }
            
            NSString *maxKey = [property stringByAppendingString:@".max"];
            NSNumber *maxThreshold = [_filters objectForKey:maxKey];
            if (maxThreshold) {
                [maxFilters addObject:maxThreshold];
            } else {
                [maxFilters addObject:[NSNull null]];
            }
        }

        for (CZParticle2DItem *item in sourceItems) {
            BOOL isValidValue = YES;
            NSUInteger i = 0;
            for (NSString *property in properties) {
                NSNumber *value = nil;
                id minThreshold = minFilters[i];
                id maxThreshold = maxFilters[i];
                
                if (minThreshold != [NSNull null]) {
                    value = [item valueForKey:property];
                    if ([value compare:minThreshold] == NSOrderedAscending) {
                        isValidValue = NO;
                        break;
                    }
                }
                
                if ((maxThreshold != [NSNull null])) {
                    if (value == nil) {
                        value = [item valueForKey:property];
                    }
                    
                    if ([value compare:maxThreshold] == NSOrderedDescending) {
                        isValidValue = NO;
                        break;
                    }
                }
                
                i++;
            }
            
            if (isValidValue) {
                [filterResult addObject:item];
            }
        }
    } else {
        for (CZParticle2DItem *item in sourceItems) {
            [filterResult addObject:item];
        }
    }
    
    return [NSArray arrayWithArray:filterResult];
}

@end
