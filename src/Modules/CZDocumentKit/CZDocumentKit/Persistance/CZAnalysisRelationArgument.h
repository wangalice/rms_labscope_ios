//
//  CZAnalysisRelationArgument.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZBufferProcessor;

@interface CZAnalysisRelationArgument : NSObject

@property (nonatomic, readonly) uint32_t idx1;
@property (nonatomic, readonly) uint32_t idx2;

- (id)initWithIdx1:(uint32_t)idx1 idx2:(uint32_t)idx2;

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

@end
