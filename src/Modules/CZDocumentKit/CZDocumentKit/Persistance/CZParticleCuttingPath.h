//
//  CZParticleCuttingPath.h
//  Matscope
//
//  Created by Carl Zeiss on 12/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZPackedCrack.h"

typedef CZPackedCrack CZParticleCuttingPath;
