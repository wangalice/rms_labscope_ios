//
//  CZAnalyzer2dPersister.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalyzer2dPersister.h"
#import "CZAnalyzer2dResult.h"
#import "CZAnalysisRelationCollection.h"
#import "CZAnalysisRelationArgument.h"
#import "CZRegion2DInstanceItemCollection.h"
#import "CZRegion2DItem.h"
#import "CZRegions2DItem.h"
#import "CZRegions2DItemCollection.h"
#import "CZRegion2DInstanceItem.h"
#import "CZRegion2D.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZAnalyzer2dPersister

- (NSData *)setAnalyzer2dResultTest {
    NSMutableData *blob = [[NSMutableData alloc] initWithCapacity:20000];
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithMutableData:blob];
    [self writeTest:writer];
    
    // TODO remove test code here
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"blob.data"];
    [blob writeToFile:filePath atomically:YES];
    CZLogv(@"write to blob to file: %@", filePath);
    
    [writer release];
    return [blob autorelease];
}

- (NSData *)imageAnalysisBlobFromAnalyzerResult:(CZAnalyzer2dResult *)result {
    NSMutableData *blob = [[NSMutableData alloc] initWithCapacity:102400];
    CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithMutableData:blob];
    
    [CZAnalyzer2dResult writeHeader:writer imageOffset:result.imageOffset imageOffsetW:result.imageOffsetW];
    
    CZAnalysisRelationCollection *analysisRelationCollection = result.analysisRelationCollection;
    CZRegion2DInstanceItemCollection *region2dInstanceItemCollection = result.region2dInstanceItemCollection;
    CZRegions2DItemCollection *regions2dItemCollection = result.regions2dItemCollection;
    
    [analysisRelationCollection write:writer];
    [region2dInstanceItemCollection write:writer];
    [regions2dItemCollection write:writer];
    
    [CZAnalyzer2dResult writeEndOfData:writer];
    
    [writer release];
    return [blob autorelease];
}

- (CZAnalyzer2dResult *)analyzerResultFromImageAnalysisBlob:(NSData *)blob {
    // TODO: ...
    return nil;
}


- (void)read:(CZBufferProcessor *)reader {
    
}

// TODO: remove this method
- (void)writeTest:(CZBufferProcessor *)writer {
    CGPoint imageOffset = CGPointZero;
    CGPoint imageOffsetW = CGPointZero;
    
    [CZAnalyzer2dResult writeHeader:writer imageOffset:imageOffset imageOffsetW:imageOffsetW];
    
    CZAnalysisRelationCollection *analysisRelationCollection = [[CZAnalysisRelationCollection alloc] init];
    [CZAnalyzer2dResult addDefaultAnalysisRelation:analysisRelationCollection];
    
    CZAnalysisRelation *firstRelation = [analysisRelationCollection objectAtIndex:0];

    CZRegion2DInstanceItemCollection *region2dInstanceItemCollection = [[CZRegion2DInstanceItemCollection alloc] init];
    CZRegions2DItemCollection *regions2dItemCollection = [[CZRegions2DItemCollection alloc] init];
    
    int regionInstanceID = 1;
    
    int drect = 30;
    int start = 10;
    
    for (int ic = 0; ic < 2; ic++, start += 15)
    {
        int8_t regionsClassID = 1;
        int8_t regionClassID = 2;
        switch (ic)
        {
            case 0:
                regionsClassID = 3;
                regionClassID = 4;
                break;
            default:
                regionsClassID = 1;
                regionClassID = 2;
                break;
        }
        
        int riID = regionInstanceID;
        
        // Root node
        {
            CZAnalysisRelationArgument *arg = [[CZAnalysisRelationArgument alloc] initWithIdx1:0 idx2:riID];
            [firstRelation.arguments addObject:arg];
            [arg release];
            
            CZRegion2DInstanceItem *rsi = [[CZRegion2DInstanceItem alloc] initWith:riID regionClassID:regionsClassID];
            [region2dInstanceItemCollection setObject:rsi forKey:@(riID)];
            [rsi release];
        }
        
        regionInstanceID++;
        
        uint8_t crackPacked[10] = {0x55, 0x55, 0xA5, 0xAA, 0xAA, 0xFF, 0xFF, 0x0F, 0x00, 0x00};
        uint8_t crackPacked2[4] = {0x55, 0xAA, 0xFF, 0x00};
        
        const int width = 10;
        const int height = 10;
        
        for (int y = start; y < 300; y += drect)
        {
            for (int x = start; x < 300; x += drect)
            {
                CZAnalysisRelationArgument *arg1 = [[CZAnalysisRelationArgument alloc] initWithIdx1:riID idx2:regionInstanceID];
                [firstRelation.arguments addObject:arg1];
                [arg1 release];

                CZRegion2D *reg = [[CZRegion2D alloc] initWithStartPointX:(x + width)
                                                                        y:(y + height)
                                                               crackCount:40
                                                              crackPacked:(ushort *)crackPacked
                                                        carackCountPacked:5];
                reg.classID = regionClassID;
                reg.idx = regionInstanceID;
                int left = x, top = y;
                
                CZRegion2D *regHole = [[CZRegion2D alloc] initWithStartPointX:(x + 7)
                                                                            y:(y + 7)
                                                                   crackCount:16
                                                                  crackPacked:(ushort *)crackPacked2
                                                            carackCountPacked:2];
                [reg addSubRegion:regHole];
                regHole.classID = regionClassID;
                regHole.idx = 0;
                
                CZRegions2DItem *regsItem = [regions2dItemCollection objectForKey:@(regionClassID)];
                if (regsItem == nil) {
                    regsItem = [[CZRegions2DItem alloc] init];
                    regsItem.regionClassID = regionClassID;
                    [regions2dItemCollection setObject:regsItem forKey:@(regionClassID)];
                    [regsItem release];
                }

                CZRegion2DInstanceItem *regionInstanceItem = [[CZRegion2DInstanceItem alloc] initWith:regionInstanceID regionClassID:regionClassID];
                regionInstanceItem.regionID = (uint32_t)regsItem.count;
                regionInstanceItem.left = left;
                regionInstanceItem.top = top;
                regionInstanceItem.width = width;
                regionInstanceItem.height = height;
                [region2dInstanceItemCollection setObject:regionInstanceItem forKey:@(regionInstanceID)];
                
                CZRegion2DItem *regItem = [[CZRegion2DItem alloc] initWithRegion2D:reg];
                [regsItem addObject:regItem];
                [regItem release];
                
                regionInstanceID++;
            }
        }
    }
    
    [analysisRelationCollection write:writer];
    [region2dInstanceItemCollection write:writer];
    [regions2dItemCollection write:writer];
    
    
    [CZAnalyzer2dResult writeEndOfData:writer];
    
    [analysisRelationCollection release];
    [regions2dItemCollection release];
    [region2dInstanceItemCollection release];
}


@end
