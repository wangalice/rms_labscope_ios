//
//  CZAnalysisRelationArgument.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalysisRelationArgument.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZAnalysisRelationArgument

- (id)initWithIdx1:(uint32_t)idx1 idx2:(uint32_t)idx2 {
    self = [super init];
    if (self) {
        _idx1 = idx1;
        _idx2 = idx2;
    }
    return self;
}

- (void)write:(CZBufferProcessor *)writer {
    [writer writeLong:self.idx1];
    [writer writeLong:self.idx2];
}

- (void)read:(CZBufferProcessor *)reader {
    _idx1 = [reader readLong];
    _idx2 = [reader readLong];
}


@end
