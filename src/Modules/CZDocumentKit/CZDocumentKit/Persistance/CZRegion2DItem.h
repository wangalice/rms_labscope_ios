//
//  CZRegion2DItem.h
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZBufferProcessor;
@class CZRegion2D;

@interface CZRegion2DItem : NSObject
@property (nonatomic, assign) uint32_t id;
@property (nonatomic, readonly, retain) CZRegion2D *region;

- (id)initWithRegion2D:(CZRegion2D *)region;
- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;
- (NSUInteger)memorySize;
@end
