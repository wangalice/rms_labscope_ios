//
//  CZRegions2DItemCollection.m
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegions2DItemCollection.h"
#include <objc/runtime.h>
#import <CZToolbox/CZToolbox.h>
#import "CZRegions2DItem.h"
#import "CZPersistanceHelper.h"

#define kRegions2DItemCollectionFixedSize 64
@implementation CZRegions2DItemCollection

- (id)copyWithZone:(NSZone *)zone {
    CZRegions2DItemCollection *newCollection = [super copyWithZone:zone];
    if (newCollection) {
        NSArray *allKeys = [newCollection.allKeys copy];
        for (id<NSCopying> key in allKeys) {
            CZRegions2DItem *item = [newCollection objectForKey:key];
            CZRegions2DItem *newItem = [item copyWithZone:zone];
            if (newItem) {
                [newCollection setObject:newItem forKey:key];
                [newItem release];
            }
        }
        
        [allKeys release];
    }
    
    return newCollection;
}

- (NSNumber *)getKeyForItem:(CZRegions2DItem *) item {
    return [NSNumber numberWithUnsignedInteger:item.regionClassID];
}

- (void)write:(CZBufferProcessor *)writer {
    @autoreleasepool {
        int64_t byteCountBehindPosition = [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeRegions2dItemCollection];

        NSMutableData *data = [NSMutableData dataWithCapacity:kRegions2DItemCollectionFixedSize];
        NSUInteger count = self.count;
        [data appendBytes:&count length:sizeof(int)];
        [data setLength:kRegions2DItemCollectionFixedSize];
        
        [writer writeBytes:data.mutableBytes
                    length:data.length];
        
        NSArray *orderedKeys = [self.allKeys sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            return [a longValue] <= [b longValue] ? NSOrderedAscending : NSOrderedDescending;
        }];
        
        for (id key in orderedKeys) {
            CZRegions2DItem *regionsItem = [self objectForKey:key];
            [regionsItem write:writer];
        }

        [CZPersistanceHelper write:writer byteCountOfPrefix:byteCountBehindPosition];
    }
}

- (void)read:(CZBufferProcessor *)reader {
    [CZPersistanceHelper readChunkPrefix:reader];
    
    uint64_t offset = [reader offset];
    int count = [reader readLong];
    [reader seekToOffset:(offset + kRegions2DItemCollectionFixedSize)];
    
    for (int i = 0; i < count; i++) {
        CZRegions2DItem *item = [[CZRegions2DItem alloc] init];
        [item read:reader];
        [self setObject:item forKey:@(item.regionClassID)];
        [item release];
    }
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    for (id key in self.allKeys) {
        CZRegions2DItem *regionsItem = [self objectForKey:key];
        memorySize += [regionsItem memorySize];
    }
    return memorySize;
}

@end
