//
//  CZAnalyzer2dResult.m
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAnalyzer2dResult.h"
#include <objc/runtime.h>
#import "CZPersistanceHelper.h"
#import "CZAnalysisRelationArgument.h"
#import "CZRegion2DInstanceItem.h"
#import "CZRegion2D.h"
#import "CZRegions2DItem.h"
#import "CZRegion2DItem.h"
#import "CZRegion2DInstanceItem.h"
#import "CZRegion2DInstanceItemCollection.h"

static const int FixedSize = 256;
static const int IndexVersionMajor = 0;
static const int IndexVersionMinor = 1;
static const int IndexAnalyzer2dResultFlags = 2; // Int32
static const int IndexImageOffsetX = 6; // double
static const int IndexImageOffsetY = 14; // double
static const int IndexImageOffsetWX = 22; // double
static const int IndexImageOffsetWY = 30; // double

typedef NS_ENUM(NSUInteger, Analyzer2dResultFlags) {
    RegionCountGreater2_31 = 1,
};

typedef CZBufferProcessor BinaryWriter;
typedef CZBufferProcessor BinaryReader;

@implementation CZAnalyzer2dResult

+ (void)addDefaultAnalysisRelation:(CZAnalysisRelationCollection *)analysisRelationCollection {
    assert(analysisRelationCollection);
    if (analysisRelationCollection == nil)
    {
        return;
    }
    
    CZAnalysisRelation *anarel = [[CZAnalysisRelation alloc] init];
    anarel.relationDesc = @"Defines the PartOf relation of the region instance hierarchy";
    anarel.roleArg1 = @"Parent";
    anarel.roleArg2 = @"Child";
    [analysisRelationCollection addObject:anarel];
    [anarel release];
}

+ (void)writeHeader:(CZBufferProcessor *)writer imageOffset:(CGPoint)imageOffset imageOffsetW:(CGPoint)imageOffsetW {
    int64_t byteCountBehindPosition = [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeHeader];
    
    int8_t versionMajor = 1;
    int8_t versionMinor = 0;
    
    Analyzer2dResultFlags analyzer2dResultFlags = 0; // RegionCountGreater2_31;
    
    int8_t bytes[FixedSize];
    memset(bytes, 0, FixedSize);
    
    BinaryWriter *bw = [[BinaryWriter alloc] initWithBuffer:bytes length:FixedSize];
    [bw seekToOffset:IndexVersionMajor];
    [bw writeByte:versionMajor];
    
    [bw seekToOffset:IndexVersionMinor];
    [bw writeByte:versionMinor];
    
    [bw seekToOffset:IndexAnalyzer2dResultFlags];
    [bw writeLong:analyzer2dResultFlags];
    
    [bw seekToOffset:IndexImageOffsetX];
    [bw writeDouble:(double)imageOffset.x];
    
    [bw seekToOffset:IndexImageOffsetY];
    [bw writeDouble:(double)imageOffset.y];
    
    [bw seekToOffset:IndexImageOffsetWX];
    [bw writeDouble:(double)imageOffsetW.x];
    
    [bw seekToOffset:IndexImageOffsetWY];
    [bw writeDouble:(double)imageOffsetW.y];
    [bw release];

    [writer writeBytes:bytes length:FixedSize];
    
    [CZPersistanceHelper write:writer byteCountOfPrefix:byteCountBehindPosition];
}

+ (void)writeEndOfData:(BinaryWriter *)writer {
    [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeEndOfData byteCountBehind:0];
}

+ (void)readEndOfData:(BinaryReader *)reader {
    [CZPersistanceHelper readChunkPrefix:reader];
}

- (void)dealloc {
    [_analysisRelationCollection release];
    [_region2dInstanceItemCollection release];
    [_regions2dItemCollection release];
    
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZAnalyzer2dResult *newResult = [[[self class] allocWithZone:zone] init];
    if (newResult) {
        newResult->_imageOffset = self->_imageOffset;
        newResult->_imageOffsetW = self->_imageOffsetW;
        newResult->_analysisRelationCollection = [self->_analysisRelationCollection copyWithZone:zone];
        newResult->_region2dInstanceItemCollection = [self->_region2dInstanceItemCollection copyWithZone:zone];
        newResult->_regions2dItemCollection = [self->_regions2dItemCollection copyWithZone:zone];
    }
    return newResult;
}

- (void)read:(CZBufferProcessor *)reader {
    bool formatok = [self readHeader:reader];
    if (!formatok)
    {
        return;
    }
    
    [self.analysisRelationCollection read:reader];
    [self.region2dInstanceItemCollection read:reader];
    [self.regions2dItemCollection read:reader];
    
    // ignore reading data and shapes, since we only interest analysis result
//  this.DataVectorsCollectionList.Read(reader);
//  this.ReadShapes(reader);
    
    [CZAnalyzer2dResult readEndOfData:reader];
    
//   TODO: this.ConvertFromRead(ias);
    
    [self clearBuffers];
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    memorySize += [self.analysisRelationCollection memorySize];
    memorySize += [self.region2dInstanceItemCollection memorySize];
    memorySize += [self.regions2dItemCollection memorySize];
    return memorySize;
}

- (BOOL)readHeader:(CZBufferProcessor *)reader {
    //long byteCountBehind = PersistanceHelper.ReadChunkPrefix(reader);
    [CZPersistanceHelper readChunkPrefix:reader];
    
    const uint64_t offset = reader.offset;

    [reader seekToOffset:(IndexVersionMajor + offset)];
    int versionMajor = [reader readByte];

    [reader seekToOffset:(IndexAnalyzer2dResultFlags + offset)];
    Analyzer2dResultFlags analyzer2dResultFlags = (Analyzer2dResultFlags)[reader readLong];
    if ((analyzer2dResultFlags & RegionCountGreater2_31) != 0)
    {
        return NO;
    }
    
    if (versionMajor > 1)
    {
        return NO;
    }
    
    [reader seekToOffset:(IndexImageOffsetX + offset)];
    double ofsX = [reader readDouble];
    [reader seekToOffset:(IndexImageOffsetY + offset)];
    double ofsY = [reader readDouble];
    self.imageOffset = CGPointMake(ofsX, ofsY);
    
    [reader seekToOffset:(IndexImageOffsetWX + offset)];
    double ofsWX = [reader readDouble];
    [reader seekToOffset:(IndexImageOffsetWY + offset)];
    double ofsWY = [reader readDouble];
    self.imageOffsetW = CGPointMake(ofsWX, ofsWY);
    
    [reader seekToOffset:(offset + FixedSize)];
    
    return true;
}

- (void)clearBuffers {
    // TODO:
}

- (CZAnalysisRelationCollection *)analysisRelationCollection {
    if (_analysisRelationCollection == nil) {
        _analysisRelationCollection = [[CZAnalysisRelationCollection alloc] init];
        [CZAnalyzer2dResult addDefaultAnalysisRelation:_analysisRelationCollection];
    }
    return _analysisRelationCollection;
}

- (CZRegions2DItemCollection *)regions2dItemCollection {
    if (_regions2dItemCollection == nil) {
        _regions2dItemCollection = [[CZRegions2DItemCollection alloc] init];
    }
    return _regions2dItemCollection;
}

- (CZRegion2DInstanceItemCollection *)region2dInstanceItemCollection {
    if (_region2dInstanceItemCollection == nil) {
        _region2dInstanceItemCollection = [[CZRegion2DInstanceItemCollection alloc] init];
    }
    return _region2dInstanceItemCollection;
}

- (void)addRegions:(NSArray *)regions regionInstances:(NSArray *)regionIntances atIndex:(NSUInteger)index {
    if (regions.count == 0) {
        return;// skip illegal region
    }
    
    CZAnalysisRelation *firstRelation = [self.analysisRelationCollection objectAtIndex:0];
    
    const uint32_t regionsClassID = (uint32_t)index * 2 + 1;
    const uint32_t regionClassID = regionsClassID + 1;
    uint32_t regionInstanceID = (uint32_t)self.region2dInstanceItemCollection.count;
    uint32_t rootRegionID = regionInstanceID;
    
    // find root arg
    CZAnalysisRelationArgument *arg = nil;
    CZRegion2DInstanceItem *rootRSI = nil;
    NSUInteger foundTimes = 0;
    for (CZAnalysisRelationArgument *tempArg in firstRelation.arguments) {
        if (tempArg.idx1 == 0) {
            if (foundTimes == index) {
                rootRegionID = tempArg.idx2;
                arg = tempArg;
                break;
            }
            foundTimes++;
        }
    }

    if (arg == nil) {
        // Add root node
        arg = [[CZAnalysisRelationArgument alloc] initWithIdx1:0 idx2:regionInstanceID];
        [firstRelation.arguments addObject:arg];
        [arg release];
        
        rootRSI = [[CZRegion2DInstanceItem alloc] initWith:regionInstanceID regionClassID:regionsClassID];
        regionInstanceID++;
    }
    
    // create CZRegions2DItem if not exist
    CZRegions2DItem *regsItem = [self.regions2dItemCollection objectForKey:@(regionClassID)];
    if (regsItem == nil) {
        regsItem = [[CZRegions2DItem alloc] initWithCapacity:regions.count];
        regsItem.regionClassID = regionClassID;
        [self.regions2dItemCollection setObject:regsItem forKey:@(regionClassID)];
        [regsItem release];
    }
    
    uint32_t rgnID = regionInstanceID;
    NSUInteger i = 0;
    for (CZRegion2D *region in regions) {
        CZRegion2DInstanceItem *rsi = [regionIntances objectAtIndex:i];
        rsi.regionInstanceID = rgnID;
        rsi.regionClassID = regionClassID;
        rsi.regionID = (uint32_t)[regsItem count];
        
        CZAnalysisRelationArgument *arg1 = [[CZAnalysisRelationArgument alloc] initWithIdx1:rootRegionID idx2:rgnID];
        [firstRelation.arguments addObject:arg1];
        [arg1 release];
        
        region.idx = rgnID;
        region.classID = regionClassID;
        
        uint32_t holeID = 0;
        for (CZRegion2D *hole in region.subRegions) {
            hole.idx = holeID;
            hole.classID = regionClassID;
            ++holeID;
        }
        
        CZRegion2DItem *regItem = [[CZRegion2DItem alloc] initWithRegion2D:region];
        [regsItem addObject:regItem];
        [regItem release];
        
        rgnID++;
        i++;
    }
    
    NSMutableArray *indexArray = [NSMutableArray arrayWithCapacity:regionIntances.count];
    for (CZRegion2DInstanceItem *rsi in regionIntances) {
        [indexArray addObject:@(rsi.regionInstanceID)];
    }
    
    NSMutableDictionary *collection = [NSMutableDictionary dictionaryWithObjects:regionIntances forKeys:indexArray];
    
    if (rootRSI) {
        [collection setObject:rootRSI forKey:@(rootRSI.regionInstanceID)];
        [rootRSI release];
    }
    
    [self.region2dInstanceItemCollection addValues:collection];
}

@end
