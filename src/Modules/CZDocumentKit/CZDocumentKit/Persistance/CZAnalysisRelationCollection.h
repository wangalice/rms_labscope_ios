//
//  CZAnalysisRelationCollection.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnalysisRelation.h"
#import <CZToolbox/CZToolbox.h>
#import "CZCollection.h"

@interface CZAnalysisRelationCollection : CZList

- (void)write:(CZBufferProcessor *)writer;
- (void)read:(CZBufferProcessor *)reader;

- (NSUInteger)memorySize;

@end
