//
//  CZRegion2DInstanceItemCollection.m
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegion2DInstanceItemCollection.h"
#include <objc/runtime.h>
#import <CZToolbox/CZToolbox.h>
#import "CZRegion2DInstanceItem.h"
#import "CZPersistanceHelper.h"

#define kRegions2DInstanceItemCollectionFixedSize 64

@implementation CZRegion2DInstanceItemCollection

- (void)write:(CZBufferProcessor *)writer {
    @autoreleasepool {
        int64_t byteCountBehindPosition = [CZPersistanceHelper write:writer chunk:Analyzer2dChunkTypeRegion2dInstanceItemCollection];
        
        uint8_t bytes[kRegions2DInstanceItemCollectionFixedSize];
        memset(bytes, 0, kRegions2DInstanceItemCollectionFixedSize);
        
        [writer writeBytes:bytes length:kRegions2DInstanceItemCollectionFixedSize];
        
        NSArray *orderedKeys = [self.allKeys sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            return [a longValue] <= [b longValue] ? NSOrderedAscending : NSOrderedDescending;
        }];
        
        for (id key in orderedKeys) {
            CZRegion2DInstanceItem *regionInstanceItem = [self objectForKey:key];
            [regionInstanceItem write:writer];
        }

        [CZPersistanceHelper write:writer byteCountOfPrefix:byteCountBehindPosition];
    }
}

- (void)read:(CZBufferProcessor *)reader {
    int64_t byteCountBehind = [CZPersistanceHelper readChunkPrefix:reader];

    [reader jumpByLength:kRegions2DInstanceItemCollectionFixedSize];
    byteCountBehind -= kRegions2DInstanceItemCollectionFixedSize;
    
    uint32_t byteCountRegion2dInstanceItem = (uint32_t)[CZRegion2DInstanceItem getByteCount];
    uint32_t countRegion2dInstanceItem = (uint32_t)(byteCountBehind / byteCountRegion2dInstanceItem);
    
    NSMutableArray *indexArray = [NSMutableArray arrayWithCapacity:countRegion2dInstanceItem];
    NSMutableArray *itemsArray = [NSMutableArray arrayWithCapacity:countRegion2dInstanceItem];
    
    for (int i = 0; i < countRegion2dInstanceItem; i++)
    {
        CZRegion2DInstanceItem *item = [[CZRegion2DInstanceItem alloc] init];
        [item read:reader];
        
        [indexArray addObject:@(item.regionInstanceID)];
        [itemsArray addObject:item];

        [item release];
    }
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjects:itemsArray forKeys:indexArray];
    [self removeAllObjects];
    [self addValues:dictionary];
    [dictionary release];
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    for (id key in self.allKeys) {
        CZRegion2DInstanceItem *regionInstanceItem = [self objectForKey:key];
        memorySize += [regionInstanceItem memorySize];
    }
    
    return memorySize;
}

@end
