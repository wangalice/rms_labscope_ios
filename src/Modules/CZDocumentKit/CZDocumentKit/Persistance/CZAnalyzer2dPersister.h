//
//  CZAnalyzer2dPersister.h
//  Matscope
//
//  Created by Ralph Jin on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>
#import "CZAnalyzer2dResult.h"

@interface CZAnalyzer2dPersister : NSObject

// TODO: remove this method
- (NSData *)setAnalyzer2dResultTest;

- (NSData *)imageAnalysisBlobFromAnalyzerResult:(CZAnalyzer2dResult *)result;
- (CZAnalyzer2dResult *)analyzerResultFromImageAnalysisBlob:(NSData *)blob;

@end
