//
//  CZPackedCrack.h
//  Matscope
//
//  Created by Carl Zeiss on 12/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZBufferProcessor;

typedef struct _PackedCrackFour {
    uint8_t c1 : 2, c2 : 2, c3 : 2, c4 : 2;
} PackedCrackFour;

typedef struct _PackedCrackEight {
    uint8_t c1 : 2, c2 : 2, c3 : 2, c4 : 2, c5 : 2, c6: 2, c7 : 2, c8 : 2;
} PackedCrackEight;

#ifndef CRACK_R
enum {
    CRACK_R = 0,  // right
    CRACK_U = 1,  // up
    CRACK_L = 2,  // left
    CRACK_D = 3,  // down
};
#endif // CRACK_R

@interface CZPackedCrack : NSObject

@property (nonatomic, readwrite) int32_t startPointX;
@property (nonatomic, readwrite) int32_t startPointY;
@property (nonatomic, readonly) size_t crackCount;
@property (nonatomic, readonly) size_t crackCountPacked;

+ (uint32_t)crackCount2CrackCountPacked:(uint32_t)crackCount;

- (id)initWithStartPointX:(int32_t)x y:(int32_t)y;

- (id)initWithStartPointX:(int32_t)x
                        y:(int32_t)y
               crackCount:(size_t)crackCount
              crackPacked:(const uint16_t *)cracksPacked
        carackCountPacked:(size_t)crackCountPacked;

- (const uint16_t *)cracksPacked;

- (void)addDirection:(uint8_t)direction;

- (void)readCracksPacked:(CZBufferProcessor *)reader
              crackCount:(size_t)crackCount
        crackCountPacked:(size_t)crackCountPacked;

/** enumrate directions. */
- (void)enumUsingBlock:(void(^)(uint8_t))block;

- (NSUInteger)memorySize;

@end
