//
//  CZRegion2D.mm
//  Matscope
//
//  Created by Mike Wang on 2/21/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRegion2D.h"
#import <objc/runtime.h>
#include <vector>
#include "CZTraceScanner.h"
#import "CZScanLine.h"

static const char subRegionsAssociatedKey = 88;

@implementation CZRegion2D

- (id)initWithScanLines:(CZScanLine *)chords classID:(uint32_t)classID {
    self = [super init];
    if (self) {
        if (CZTraceScanner::shareInstance()->ScanRegions(chords, self)) {
            _classID = classID;
            
            uint32_t i = 0;
            for (CZRegion2D *innerRegion2D in self.subRegions) {
                innerRegion2D.classID = classID;
                innerRegion2D.idx = i;
                i++;
            }
        } else {
            [self release];
            self = nil;
        }
    }
    
    return self;
}

- (void)dealloc {
    objc_setAssociatedObject(self, &subRegionsAssociatedKey, nil, OBJC_ASSOCIATION_ASSIGN);

    [super dealloc];
}

- (NSUInteger)area {
    NSUInteger area = [self selfArea];
    
    for (CZRegion2D *innerRegion in self.subRegions) {
        NSUInteger holeArea = [innerRegion area];

        if (holeArea < area) {
            area -= holeArea;
        } else {
            area = 0;
            break;
        }
    }
    
    return area;
}

- (NSUInteger)selfArea {
    __block long x = 0, area = 0;
    [self enumUsingBlock:^(uint8_t direction) {
        switch (direction) {
            case CRACK_R:
                x++;
                break;
            case CRACK_U:
                area -= x;
                break;
            case CRACK_L:
                x--;
                break;
            case CRACK_D:
                area += x;
                break;
            default:
                assert(false);
                break;
        }
    }];
    
    return (area > 0) ? area : -area;
}

- (void)fillInImage:(CZScanLine *)scanLine {
    __block long x = self.startPointX;
    __block long y = self.startPointY;
    
    [self enumUsingBlock: ^(uint8_t direction) {
        switch (direction) {
            case CRACK_R:
                x++;
                break;
            case CRACK_U:
                y--;
                [scanLine addEdgePointX:x y:y];
                break;
            case CRACK_L:
                x--;
                break;
            case CRACK_D:
                [scanLine addEdgePointX:x y:y];
                y++;
                break;
            default:
                assert(false);
                break;
        }
    }];
    
    for (CZRegion2D *innerRegion2D in self.subRegions) {
        [innerRegion2D fillInImage:scanLine];
    }
}

- (BOOL)removeFromImage:(CZScanLine *)scanLine {
    __block long x = self.startPointX;
    __block long y = self.startPointY;
    
    [self enumUsingBlock:^(uint8_t direction) {
        BOOL ok;
        switch (direction) {
            case CRACK_R:
                x++;
                break;
            case CRACK_U:
                y--;
                ok = [scanLine removePointX:x y:y];
                assert(ok);
                break;
            case CRACK_L:
                x--;
                break;
            case CRACK_D:
                ok = [scanLine removePointX:x y:y];
                assert(ok);
                y++;
                break;
            default:
                assert(false);
                break;
        }
    }];
    
    return YES;
}

- (void)enumOutlineUsingBlock:(void (^)(uint8_t))block {
    [self enumUsingBlock:block];
}

- (NSUInteger)memorySize {
    NSUInteger memorySize = class_getInstanceSize([self class]);
    memorySize += [super memorySize];
    for (CZRegion2D *subRegion in self.subRegions) {
        memorySize += [subRegion memorySize];
    }
    return memorySize;
}

- (NSMutableArray *)subRegions {
    return objc_getAssociatedObject(self, &subRegionsAssociatedKey);
}

- (NSUInteger)subRegionCount {
    return self.subRegions.count;
}

- (CZRegion2D *)subRegionAtIndex:(NSUInteger)index {
    return [self.subRegions objectAtIndex:index];
}

- (void)addSubRegion:(CZRegion2D *)subRegion {
    if (subRegion == nil) {
        return;
    }
    
    NSMutableArray *subRegions = self.subRegions;
    // make sure subRegions exist
    if (subRegions == nil) {
        subRegions = [[NSMutableArray alloc] init];
        objc_setAssociatedObject(self, &subRegionsAssociatedKey, subRegions, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [subRegions release];
    }
    
    [subRegions addObject:subRegion];
}

@end
