//
//  CZMultiphase+CZIMetaData.m
//  Matscope
//
//  Created by Mike Wang on 2/13/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZMultiphase+CZIMetaData.h"
#import <stdio.h>
#include <sstream>
#import "CZPugiXmlHelper.h"
#import <CZAnnotationKit/CZAnnotationKit.h>
#import "CZParticles2DItem.h"
#import "CZParticleCuttingPath.h"

@implementation CZMultiphase (CZIMetaData)

- (id)initWithMetaData:(pugi::xml_node *)rootNode imageSize:(CGSize)imageSize {
    self = [self init];
    if (self) {
        pugi::xml_node multiphaseRootNode = rootNode->select_single_node("ImageDocument/Metadata/Appliances/Appliance[@Id='ImageAnalysis:1']/Data/MeasurementAppData/ImageAnalysisSetting/RegionClass").node();
        if (multiphaseRootNode) {
            NSUInteger phaseCount = 0;
            for (pugi::xml_node_iterator it = multiphaseRootNode.begin();
                 it != multiphaseRootNode.end(); ++it) {
                //phaeCount >= self.maxCount + 1 to include remaining phase check
                if (phaseCount >= self.maxCount + 1) {
                    break;
                }
                pugi::xml_node &phaseCollectionNode = *it;
                const char *nodeName = phaseCollectionNode.name();
                if (strcmp(nodeName, "RegionClass") != 0) {
                    continue;
                }
                
                NSString *phaseName = nil;
                //phaseName is essential
                if (!ParseAttribute(phaseCollectionNode, "Name", phaseName)) {
                    continue;
                }
                
                if ([phaseName isEqualToString:@"Remaining Phases"]) {
                    self.remainingEnabled = YES;
                    phaseCount++;
                    continue;
                }
                
                //phaseSingleNode is essential
                pugi::xml_node phaseSingleNode = phaseCollectionNode.child("RegionClass");
                if (!phaseSingleNode) {
                    continue;
                }

                CZColor phaseColor;
                //phase color is essential
                if (!pugi::ParseValue(phaseSingleNode, "Color", phaseColor)) {
                    continue;
                }
                
                pugi::xml_node parametersNode = phaseSingleNode.select_single_node("ParametersCollection/Parameters").node();
                //phase parameters is essential
                if (!parametersNode) {
                    continue;
                }
                
                BOOL hasThresholdParameters = NO;
                int minThreshold = 0;
                int maxThreshold = 0;
                int minimumArea = 0;
                
                for (pugi::xml_node_iterator it = parametersNode.begin();
                     it != parametersNode.end(); ++it) {
                    pugi::xml_node &parameternNode = *it;
                    
                    NSString *parameterKeyName = nil;
                    if (!ParseAttribute(parameternNode, "Key", parameterKeyName)) {
                        continue;
                    }
                    
                    if ([parameterKeyName isEqualToString:@"ThresholdHistogram1"]) {
                        //phase threshold is essential
                        if (!pugi::ParseValue(parameternNode, "ValueLower1", minThreshold)) {
                            continue;
                        }
                        
                        if (!pugi::ParseValue(parameternNode, "ValueUpper1", maxThreshold)) {
                            continue;
                        }
                        
                        hasThresholdParameters = YES;
                    } else if ([parameterKeyName isEqualToString:@"MinArea1"]) {
                        if (!pugi::ParseValue(parameternNode, "Minimum", minimumArea)) {
                            continue;
                        }
                    }
                }
                
                if (!hasThresholdParameters) {
                    continue;
                }
                
                if (minThreshold != 0 || maxThreshold != 0) {
                    CZPhase *phase = [[CZPhase alloc] init];
                    phase.name = phaseName;
                    phase.color = UIColorFromCZColor(phaseColor);
                    phase.minThreshold = minThreshold;
                    phase.maxThreshold = maxThreshold;
                    phase.minimumArea = minimumArea;
                    
                    [self addPhase:phase];
                    [phase release];
                    
                    phaseCount++;
                }
            }
            
            NSString *mode = nil;
            if (pugi::ParseValue(multiphaseRootNode, "Mode", mode)) {
                self.particlesMode = [mode isEqualToString:@"Particle"];
                
                // parse particle criteria
                pugi::xml_node particlesNode = multiphaseRootNode.child("ParticleCriteria");
                if (self.particlesMode && particlesNode) {
                    NSString *sortKey = nil;
                    bool sortAscending = false;
                    if (pugi::ParseValue(particlesNode, "sortKey", sortKey)) {
                        pugi::ParseValue(particlesNode, "sortAscending", sortAscending);
                    }
                    
                    NSMutableDictionary *filters = [NSMutableDictionary dictionary];
                    pugi::xml_node filtersNode = particlesNode.child("filters");
                    if (filtersNode) {
                        for (pugi::xml_node_iterator it = filtersNode.begin(); it != filtersNode.end(); ++it) {
                            NSString *key = [NSString stringWithUTF8String:it->name()];
                            double value = it->text().as_double();
                            [filters setObject:@(value) forKey:key];
                        }
                    }
                    
                    CZParticleCriteria *criteria = [[CZParticleCriteria alloc] initWithFilters:filters sortKey:sortKey ascending:sortAscending];
                    self.particleCriteria = criteria;
                    [criteria release];
                }
                
                // parse particle cutting path
                pugi::xml_node pathsNode = multiphaseRootNode.child("CuttingPaths");
                if (self.particlesMode && pathsNode) {
                    NSMutableArray *paths = [NSMutableArray array];
                    
                    for (pugi::xml_node_iterator it = pathsNode.begin(); it != pathsNode.end(); ++it) {
                        NSString *key = [NSString stringWithUTF8String:it->name()];
                        if ([key isEqualToString:@"Path"]) {
                            pugi::xml_node xNode = it->child("StartX");
                            pugi::xml_node yNode = it->child("StartY");
                            pugi::xml_node countNode = it->child("CrackCount");
                            pugi::xml_node crackNode = it->child("Crack");
                            if (xNode && yNode && countNode && crackNode) {
                                int32_t x = xNode.text().as_int();
                                int32_t y = yNode.text().as_int();
                                uint32_t crackCount = countNode.text().as_uint();
                                NSString *crackString = [NSString stringWithUTF8String:crackNode.text().as_string()];
                                
                                NSData *crack = [[NSData alloc] initWithBase64EncodedString:crackString
                                                                                    options:NSDataBase64DecodingIgnoreUnknownCharacters];

                                CZParticleCuttingPath *path = [[CZParticleCuttingPath alloc] initWithStartPointX:x
                                                                                                               y:y
                                                                                                      crackCount:crackCount
                                                                                                     crackPacked:(const uint16_t *)[crack bytes]
                                                                                               carackCountPacked:([crack length]/2)];
                                if (path) {
                                    [paths addObject:path];
                                    [path release];
                                }
                                [crack release];
                            }
                        }
                    }
                    
                    if (paths.count > 0) {
                        self.cuttingPaths = paths;
                    } else {
                        self.cuttingPaths = nil;
                    }
                }
            }
        }
    }
    return self;
}

- (void)exportParameterNodeTo:(pugi::xml_node &)parametersCollectionNode
               withParameters:(NSArray *)parameterList {
    for (NSDictionary *parameter in parameterList) {
        if (parameter.count == 0) {
            continue;
        }
        NSString *parameterKey = [parameter.allKeys objectAtIndex:0];
        NSObject *parameterValue = parameter[parameterKey];
        if ([parameterValue isKindOfClass:[NSArray class]]) {
            NSArray *parameterValueArray = (NSArray *)parameterValue;
            pugi::xml_node parameterNode = parametersCollectionNode.append_child("Parameter");
            parameterNode.append_attribute("Key") = [parameterKey cStringUsingEncoding:NSUTF8StringEncoding];
            [self exportParameterNodeTo:parameterNode withParameters:parameterValueArray];
        } else if([parameterValue isKindOfClass:[NSString class]]) {
            NSString *parameterValueString = (NSString *)parameterValue;
            pugi::AppendValue(parametersCollectionNode, [parameterKey cStringUsingEncoding:NSUTF8StringEncoding], [parameterValueString cStringUsingEncoding:NSUTF8StringEncoding]);
        }
    }
}

- (void)exportParametersCollectionNodeTo:(pugi::xml_node &)phaseNode
                               withPhase:(CZPhase *)phase {
    pugi::xml_node parametersCollectionNode = phaseNode.append_child("ParametersCollection");
    pugi::xml_node parametersNode = parametersCollectionNode.append_child("Parameters");
    parametersNode.append_attribute("Key") = "Segmentation";
    parametersNode.append_attribute("Name") = "SegmenterClassBuiltinDefault";
    
    if (phase) {
        NSArray *parameters = @[
                                   @{@"MinArea1" : @[
                                            @{@"Source": @"MinArea"},
                                            @{@"IsInteractive": @"false"},
                                            @{@"Minimum": [NSString stringWithFormat:@"%lu", (unsigned long)phase.minimumArea]},
                                            @{@"Value": @"1"},
                                            @{@"Maximum": [NSString stringWithFormat:@"%lu", (unsigned long)self.imagePixelCount]},
                                            @{@"SmallChange": @"1"},
                                            ]
                                     },
                                   
                                   @{@"ThresholdHistogram1" : @[
                                             @{@"Source": @"ThresholdHistogram"},
                                             @{@"Minimum1": @"0"},
                                             @{@"ValueLower1": [NSString stringWithFormat:@"%lu", (unsigned long)phase.minThreshold]},
                                             @{@"ValueUpper1": [NSString stringWithFormat:@"%lu", (unsigned long)phase.maxThreshold]},
                                             @{@"Maximum1": @"255"},
                                             @{@"isHLS": @"false"},
                                             @{@"isValid1": @"true"},
                                             @{@"AnalysisThresholdMode": @"Click"},
                                             @{@"AutomaticThresholdMethod": @"Otsu"},
                                             @{@"AnalysisThresholdShapeMode": @"Polygon"},
                                             @{@"PickSize": @"1"},
                                             @{@"PickTolerance": @"3"},
                                             ]
                                     }
                               ];
        [self exportParameterNodeTo:parametersNode
                     withParameters:parameters];
    }
}

- (void)exportFeaturesNodeTo:(pugi::xml_node &)phaseNode
                withFeatures:(NSArray *)featureNames {
    pugi::xml_node featuresParentNode = phaseNode.append_child("Features");
    pugi::xml_node featuresChildNode = featuresParentNode.append_child("Features");
    for (NSString *name in featureNames) {
        pugi::xml_node featureNode = featuresChildNode.append_child("Feature");
        featureNode.append_attribute("Name") = [name cStringUsingEncoding:NSUTF8StringEncoding];
    }
}

- (void)exportMetaDataTo:(pugi::xml_node &)imageAnalysisSettingNode {
    pugi::xml_node multiphasesNode = imageAnalysisSettingNode.append_child("RegionClass");
    multiphasesNode.append_attribute("Name") = "Root";
    multiphasesNode.append_attribute("Id") = "0";
    pugi::AppendValue(multiphasesNode, "Type", "Or");
    pugi::AppendValue(multiphasesNode, "Color", kCZColorWhite);
    if (self.isParticlesMode) {
        pugi::AppendValue(multiphasesNode, "Mode", "Particle");   // write private data segment for Matscope
        
        // write particle criteria
        if (self.particleCriteria) {
            pugi::xml_node particleCriteriaNode = multiphasesNode.append_child("ParticleCriteria");
            if (self.particleCriteria.sortKey) {
                pugi::AppendValue(particleCriteriaNode, "sortKey", [self.particleCriteria.sortKey UTF8String]);
                pugi::AppendValue(particleCriteriaNode, "sortAscending", (bool)self.particleCriteria.sortAscending);
            }
            NSDictionary *filters = self.particleCriteria.filters;
            if (filters) {
                pugi::xml_node filtersNode = particleCriteriaNode.append_child("filters");
                for (NSString *key in self.particleCriteria.filters.allKeys) {
                    pugi::AppendValue(filtersNode, [key UTF8String], [filters[key] doubleValue]);
                }
            }
        }

        // write particle cutting paths
        if (self.cuttingPaths.count > 0) {
            pugi::xml_node pathsNode = multiphasesNode.append_child("CuttingPaths");
            for (CZParticleCuttingPath *path in self.cuttingPaths) {
                pugi::xml_node pathNode = pathsNode.append_child("Path");
                pugi::AppendValue(pathNode, "StartX", path.startPointX);
                pugi::AppendValue(pathNode, "StartY", path.startPointY);
                pugi::AppendValue(pathNode, "CrackCount", (int)path.crackCount);
                NSData *crack = [[NSData alloc] initWithBytes:path.cracksPacked
                                                       length:path.crackCountPacked * sizeof(uint16_t)];
                
                NSString *base64Crack = [crack base64EncodedStringWithOptions:0];
                pugi::AppendValue(pathNode, "Crack", base64Crack);
                [crack release];
            }
        }
    }
    
    [self exportParametersCollectionNodeTo:multiphasesNode
                                 withPhase:nil];
    
    for (int i = 0; i < self.phaseCount; i++) {
        CZPhase *phase = [self phaseAtIndex:i];

        //export phase collection node
        pugi::xml_node phaseCollectionNode = multiphasesNode.append_child("RegionClass");
        phaseCollectionNode.append_attribute("Name") = [phase.name cStringUsingEncoding:NSUTF8StringEncoding];
        phaseCollectionNode.append_attribute("Id") = [[NSString stringWithFormat:@"%d", i*2+1] cStringUsingEncoding:NSUTF8StringEncoding];
        pugi::AppendValue(phaseCollectionNode, "Type", "Collection");
        pugi::AppendValue(phaseCollectionNode, "Color", CZColorFromUIColor(phase.color));
        
        [self exportParametersCollectionNodeTo:phaseCollectionNode
                                     withPhase:nil];
        [self exportFeaturesNodeTo:phaseCollectionNode withFeatures:@[@"RegionsCount"]];
        
        //export phase single node
        pugi::xml_node phaseSingleNode = phaseCollectionNode.append_child("RegionClass");
        phaseSingleNode.append_attribute("Name") = [[phase.name stringByAppendingString:[NSString stringWithFormat:@"-%d", i+1]] cStringUsingEncoding:NSUTF8StringEncoding];
        phaseSingleNode.append_attribute("Id") = [[NSString stringWithFormat:@"%d", i*2+2] cStringUsingEncoding:NSUTF8StringEncoding];
        pugi::AppendValue(phaseSingleNode, "Type", "Single");
        pugi::AppendValue(phaseSingleNode, "ChannelName", "C1");
        pugi::AppendValue(phaseSingleNode, "Color", CZColorFromUIColor(phase.color));
        
        [self exportParametersCollectionNodeTo:phaseSingleNode
                                     withPhase:phase];
        
        
        phaseSingleNode.append_child("InteractiveParameters");
        
        pugi::xml_node conditionsNode= phaseSingleNode.append_child("Conditions");
        pugi::xml_node conditionNode= conditionsNode.append_child("Condition");
        conditionNode.append_attribute("Key") = "MinMax1";
        pugi::AppendValue(conditionNode, "Name", "MinMax");
        pugi::AppendValue(conditionNode, "Source", "ConditionMinMax");
        
        [self exportFeaturesNodeTo:phaseSingleNode withFeatures:@[@"ID", @"Area", @"Perimeter"]];
    }
    
    if (self.remainingEnabled) {
        pugi::xml_node phaseCollectionNode = multiphasesNode.append_child("RegionClass");
        phaseCollectionNode.append_attribute("Name") = "Remaining Phases";
        phaseCollectionNode.append_attribute("Id") = [[NSString stringWithFormat:@"%lu", (unsigned long)self.phaseCount*2+1] cStringUsingEncoding:NSUTF8StringEncoding];
        pugi::AppendValue(phaseCollectionNode, "Type", "Collection");
        pugi::AppendValue(phaseCollectionNode, "Color", CZColorFromUIColor(self.remainingColor));
        
        pugi::xml_node phaseSingleNode = phaseCollectionNode.append_child("RegionClass");
        phaseSingleNode.append_attribute("Name") = "Remaining Phase";
        phaseSingleNode.append_attribute("Id") = [[NSString stringWithFormat:@"%lu", (unsigned long)self.phaseCount*2+2] cStringUsingEncoding:NSUTF8StringEncoding];
        pugi::AppendValue(phaseSingleNode, "Type", "Single");
        pugi::AppendValue(phaseSingleNode, "ChannelName", "C1");
        pugi::AppendValue(phaseSingleNode, "Color", CZColorFromUIColor(self.remainingColor));
    }
}
@end
