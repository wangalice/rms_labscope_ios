//
//  UIImage+WriteTIFF.h
//  Matscope
//
//  Created by Carl Zeiss on 12/4/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZImageProperties;

@interface UIImage (WriteTIFF)

/**
 * @param imageProperties can be nil
 */
- (BOOL)writeTifToFile:(NSString *)filePath
  usingImageProperties:(CZImageProperties *)imageProperties;

@end
