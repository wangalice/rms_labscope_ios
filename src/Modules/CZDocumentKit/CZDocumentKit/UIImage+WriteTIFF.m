//
//  UIImage+WriteTIFF.m
//  Matscope
//
//  Created by Carl Zeiss on 12/4/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "UIImage+WriteTIFF.h"
#include "tiffio.h"
#import "CZImageProperties+MakerNote.h"
#import <CZToolbox/CZToolbox.h>

//const static float kInchToUM = 25400;
const static float kCMToUM = 10000;

@implementation UIImage (WriteTIFF)

- (BOOL)writeTifToFile:(NSString *)filePath usingImageProperties:(CZImageProperties *)imageProperties {
    TIFF *tif = NULL;
    BOOL success = NO;
    do {
        CGColorSpaceRef colorSpace = CGImageGetColorSpace([self CGImage]);
        const BOOL isGray = CGColorSpaceGetModel(colorSpace) == kCGColorSpaceModelMonochrome;
        
        uint64 dir_offset = 0;
        const uint32 width = self.size.width;
        const uint32 length = self.size.height;
        const uint16 rows_per_strip = 1;
        const uint16 bps = 8;
        const uint16 planarconfig = PLANARCONFIG_CONTIG;
        const uint16 photometric = isGray ? PHOTOMETRIC_MINISBLACK : PHOTOMETRIC_RGB;
        const uint16 samplePerPixel = isGray ? 1 : 4;
        
        // We write the main directory as a simple image.
        tif = TIFFOpen([filePath fileSystemRepresentation], "w+");
        if (!tif) {
            CZLogv(@"Can't create TIFF file at path: %@.\n", filePath);
            break;
        }

        // begin write main image
        TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0);
        if (!TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width)) {
            CZLogv(@"Can't set ImageWidth tag.\n");
            break;
        }
        if (!TIFFSetField(tif, TIFFTAG_IMAGELENGTH, length)) {
            CZLogv(@"Can't set ImageLength tag.\n");
            break;
        }
        if (!TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bps)) {
            CZLogv(@"Can't set BitsPerSample tag.\n");
            break;
        }
        
        if (!TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, samplePerPixel)) {
            CZLogv(@"Can't set SamplesPerPixel tag.\n");
            break;
        }
        
        uint16 tiffOrientation;
        switch (self.imageOrientation) {
            case UIImageOrientationUp:
                tiffOrientation = ORIENTATION_TOPLEFT;
                break;
            case UIImageOrientationDown:
                tiffOrientation = ORIENTATION_BOTRIGHT;
                break;
            case UIImageOrientationLeft:
                tiffOrientation = ORIENTATION_LEFTBOT;
                break;
            case UIImageOrientationRight:
                tiffOrientation = ORIENTATION_RIGHTTOP;
                break;
            case UIImageOrientationUpMirrored:
                tiffOrientation = ORIENTATION_TOPRIGHT;
                break;
            case UIImageOrientationDownMirrored:
                tiffOrientation = ORIENTATION_BOTLEFT;
                break;
            case UIImageOrientationLeftMirrored:
                tiffOrientation = ORIENTATION_LEFTTOP;
                break;
            case UIImageOrientationRightMirrored:
                tiffOrientation = ORIENTATION_RIGHTBOT;
                break;
        }
        
        if (!TIFFSetField(tif, TIFFTAG_ORIENTATION, tiffOrientation)) {
            CZLogv(@"Can't set TIFFTAG_ORIENTATION tag.\n");
            break;
        }
        
        if (!TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rows_per_strip)) {
            CZLogv(@"Can't set SamplesPerPixel tag.\n");
            break;
        }
        if (!TIFFSetField(tif, TIFFTAG_PLANARCONFIG, planarconfig)) {
            CZLogv(@ "Can't set PlanarConfiguration tag.\n");
            break;
        }
        if (!TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, photometric)) {
            CZLogv(@"Can't set PhotometricInterpretation tag.\n");
            break;
        }
        
        if (!TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW)) { // COMPRESSION_DEFLATE
            CZLogv(@"Can't set Compression tag.\n");
            break;
        }
        
        if (imageProperties.microscopeManufacturer.length) {
            if (!TIFFSetField(tif, TIFFTAG_MAKE, [imageProperties.microscopeManufacturer UTF8String])) {
                CZLogv(@"Can't set Make tag.\n");
                break;
            }
        }
        
        NSString *modelString = nil;
        if (imageProperties.cameraModel) {
            modelString = imageProperties.cameraModel;
        } else {
            modelString = imageProperties.microscopeModel;
        }
        if (modelString.length) {
            if (!TIFFSetField(tif, TIFFTAG_MODEL, [modelString UTF8String])) {
                CZLogv(@"Can't set Make tag.\n");
                break;
            }
        }
        
        if (imageProperties.operatorName.length) {
            if (!TIFFSetField(tif, TIFFTAG_ARTIST, [imageProperties.operatorName UTF8String])) {
                CZLogv(@"Can't set Artist tag.\n");
                break;
            }
        }
        
        NSString *imageDescription = [imageProperties combineImageDescription];
        if (imageDescription.length) {
            if (!TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, [imageDescription UTF8String])) {
                CZLogv(@"Can't write TIFFTAG_IMAGEDESCRIPTION\n");
                break;
            }
        }
        
        // create raw pixel data from CGImage
        const size_t height = length;
        const size_t rowbytes = width * samplePerPixel;
        const size_t rgbImageSize = height * rowbytes;
        void *rgbImage = calloc(rgbImageSize, 1);
        colorSpace = isGray ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
        CGBitmapInfo bitmapInfo = isGray ? kCGImageAlphaNone : (kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        CGContextRef context = CGBitmapContextCreate(rgbImage, width, height, 8, rowbytes, colorSpace, bitmapInfo);
        CGContextSetInterpolationQuality(context, kCGInterpolationNone);
        CGContextSetAllowsAntialiasing(context, false);
        CGContextSetShouldAntialias(context, false);
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
        CGContextRelease(context);
        CGColorSpaceRelease(colorSpace);

        // Write pixel data.
        BOOL writeRasterSucess = YES;
        void *raster = rgbImage;
        for (uint32 row = 0; row < length; row+=1) {
            if (TIFFWriteScanline(tif, raster, row, 0) < 0) {
                CZLogv(@"Can't write image data.\n");
                writeRasterSucess = NO;
                break;
            }
            
            raster += rowbytes;
        }

        free(rgbImage);
        
        if (!writeRasterSucess) {
            break;
        }
        
        if (imageProperties == nil) {
            break;
        }
        
        if (!TIFFWriteDirectory(tif)) {
            CZLogv(@"TIFFWriteDirectory() failed.\n");
            break;
        }
        
        // begin write thumbnail image in 160x120
        CGSize thumbSize = CGSizeMake(160, 120);
        const CGFloat widthRatio = thumbSize.width / width;
        const CGFloat heightRatio = thumbSize.height / height;
        if (widthRatio < heightRatio) {
            thumbSize.height = round(thumbSize.width * height / width);
        } else {
            thumbSize.width = round(thumbSize.height * width / height);
        }
        
        TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_REDUCEDIMAGE);
        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, (uint32)thumbSize.width);
        TIFFSetField(tif, TIFFTAG_IMAGELENGTH, (uint32)thumbSize.height);
        TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bps);
        TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, (uint16) samplePerPixel);
        TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
        TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, photometric);
        TIFFSetField(tif, TIFFTAG_PLANARCONFIG, planarconfig);
        TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        
        /* write thumbnail image */ {
            const size_t width = thumbSize.width;
            const size_t height = thumbSize.height;
            const size_t rowbytes = width * samplePerPixel;
            const size_t rgbImageSize = height * rowbytes;
            void *rgbImage = calloc(rgbImageSize, 1);
            colorSpace = isGray ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB();
            CGBitmapInfo bitmapInfo = isGray ? kCGImageAlphaNone : (kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
            CGContextRef context = CGBitmapContextCreate(rgbImage, width, height, 8, rowbytes, colorSpace, bitmapInfo);
            CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
            CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
            CGContextRelease(context);
            CGColorSpaceRelease(colorSpace);

            if (!TIFFWriteEncodedStrip(tif, 0, rgbImage, rgbImageSize)) {
                free(rgbImage);
                break;
            }
            
            free(rgbImage);
            
            if (!TIFFWriteDirectory(tif)) {
                break;
            }
        }
        
        // Now create an EXIF directory.
        if (TIFFCreateEXIFDirectory(tif) != 0) {
            CZLogv(@"TIFFCreateEXIFDirectory() failed.\n");
            break;
        }
        
        if (imageProperties.channels.count == 1 && imageProperties.channels[0].channelColor == nil && imageProperties.channels[0].exposureTimeInSeconds) {
            float exposureTime = imageProperties.channels[0].exposureTimeInSeconds.floatValue;
            if (!TIFFSetField(tif, EXIFTAG_EXPOSURETIME, exposureTime)) {
                CZLogv(@"Can't write EXIFTAG_EXPOSURETIME\n");
                break;
            }
        }
        
        if (imageProperties.scaling > 0) {
            TIFF_UINT16_T unit = 3;  // cm
            if (!TIFFSetField(tif, EXIFTAG_FOCALPLANERESOLUTIONUNIT, unit)) {
                CZLogv(@"Can't write EXIFTAG_EXPOSURETIME\n");
                break;
            }
            
            float scaling = kCMToUM / imageProperties.scaling;
            if (!TIFFSetField(tif, EXIFTAG_FOCALPLANEXRESOLUTION, scaling)) {
                CZLogv(@"Can't write EXIFTAG_FOCALPLANEXRESOLUTION\n");
                break;
            }
            
            if (!TIFFSetField(tif, EXIFTAG_FOCALPLANEYRESOLUTION, scaling)) {
                CZLogv(@"Can't write EXIFTAG_FOCALPLANEYRESOLUTION\n");
                break;
            }
        }
        
        TIFF_UINT16_T colorSpaceT = 1;
        if (!TIFFSetField(tif, EXIFTAG_COLORSPACE, colorSpaceT)) {
            CZLogv(@"Can't write EXIFTAG_COLORSPACE\n");
            break;
        }
        
        NSDateFormatter *formatter = [CZImageProperties defaultDateFormat];
        NSString *dateString = [formatter stringFromDate:imageProperties.acquisitionDate];
        
        if ([dateString length] > 0) {
            if (!TIFFSetField(tif, EXIFTAG_DATETIMEORIGINAL, [dateString UTF8String])) {
                CZLogv(@"Can't write EXIFTAG_DATETIMEORIGINAL\n");
                break;
            }
        }
        
        NSString *makerNote = [imageProperties makerNoteRepresentation];
        if (makerNote.length > 0) {
            NSData *data = [makerNote dataUsingEncoding:NSUTF8StringEncoding];
            if (!TIFFSetField(tif, EXIFTAG_MAKERNOTE, data.length, data.bytes)) {
                CZLogv(@"Can't set MakerNote tag.\n");
                break;
            }
        }
        
        if (!TIFFWriteCustomDirectory(tif, &dir_offset)) {
            break;
        }
        
        TIFFSetDirectory(tif, 0);
        TIFFSetField(tif, TIFFTAG_EXIFIFD, dir_offset);
        success = YES;
    } while (0);

    if (tif) {
        TIFFClose(tif);
    }
    
    return success;
}

@end
