//
//  CZDocManager+CZIMetaData.mm
//  Hermes
//
//  Created by Ralph Jin on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDocManager+CZIMetaData.h"

#import "pugixml.hpp"
#import "CZPugiXmlHelper.h"
#import "UIImage+WriteTIFF.h"
#import "CZImagePropertiesPrivate.h"
#import "CZImageProperties+CZIMetadata.h"
#import "CZElementLayer+CZIMetadata.h"
#import <CZFileKit/CZFileKit.h>
#import "CZMultiphase+CZIMetaData.h"
#import <CZToolbox/CZToolbox.h>
#import "CZElement+ROI.h"
#import "CZGrainSizeResult+CZIMetadata.h"
#import "CZDocManager+CZIDocument.h"

static NSUInteger MaxSupportedImageSize() {
    static NSUInteger maxSupportedImageSize;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
//        if ([CZCommonUtils iPadModelNumber] >= 500) {  // iPad Air 2 and later
//            maxSupportedImageSize = 3264;
//        } else {
//            maxSupportedImageSize = 3072;
//        }
        
        // temporarily set 4096 here, to fit 4k image.
        maxSupportedImageSize = 4096;
    });

    return maxSupportedImageSize;
}

static NSUInteger MaxSupportedChannelCount = 5;

@implementation CZDocManager (CZIMetaData)

+ (CZDocManager *)newDocManagerFromFilePath:(NSString *)filePath {
    return [self newDocManagerFromFilePath:filePath withDummyImage:nil];
}

+ (CZDocManager *)newDocManagerWithDummyImageFromFilePath:(NSString *)filePath {
    UIGraphicsBeginImageContext(CGSizeMake(1, 1));
    UIImage *dummyImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [self newDocManagerFromFilePath:filePath withDummyImage:dummyImage];
}

+ (CZDocManager *)newDocManagerFromFilePath:(NSString *)filePath withDummyImage:(UIImage *)dummyImage {
    if (filePath == nil) {
        return nil;
    }
    
    // check image size and channel count
    CGSize imageSize = CGSizeZero;
    CGFloat channelCount = 0;
    
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:filePath.pathExtension];
    if (fileFormat == kCZFileFormatCZI) {
        CZIDocument *czi = [[CZIDocument alloc] initWithFile:filePath];
        imageSize = [czi imageSize];
        channelCount = [czi channelCount];
        [czi release];
    } else if (fileFormat == kCZFileFormatJPEG || fileFormat == kCZFileFormatTIF) {
        imageSize = [CZCommonUtils imageSizeFromFilePath:filePath];
    } else {
        CZLogv(@"Unsupported file format.");
        return nil;
    }
    
    if ((NSUInteger)imageSize.width > MaxSupportedImageSize() ||
        (NSUInteger)imageSize.height > MaxSupportedImageSize() ||
        CGSizeEqualToSize(imageSize, CGSizeZero) ||
        channelCount > MaxSupportedChannelCount) {
        CZLogv(@"Image has invalide width (%lu) or height (%lu).", (unsigned long)imageSize.width, (unsigned long)imageSize.height);
        return nil;
    }
    
    UIImage *image = dummyImage;
    CZDocManager *newDocManager = nil;
    if (fileFormat == kCZFileFormatCZI) {
        newDocManager = [[CZDocManager alloc] initWithContentsOfCZIFile:filePath withDummyImage:dummyImage imageSize:imageSize];
    } else if (fileFormat == kCZFileFormatJPEG) {
        @autoreleasepool {
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
            NSData *fileContent = [fileHandle readDataToEndOfFile];
            [fileHandle closeFile];
            
            if (image == nil) {
                image = [[[UIImage alloc] initWithData:fileContent] autorelease];
            }
            
            CGColorSpaceModel model = CGColorSpaceGetModel(CGImageGetColorSpace(image.CGImage));
            UIImage *outImage = [CZCommonUtils newDecompressedImageFromJPEGImage:image];
            NSString *directory = [filePath stringByDeletingLastPathComponent];
            CZImageBlock *imageBlock = [[CZImageBlock alloc] initWithImage:outImage channelProperties:nil];
            CZImageDocument *document = [[CZImageDocument alloc] initWithImageBlocks:@[imageBlock]];
            newDocManager = [[CZDocManager alloc] initWithDirectory:directory document:document];
            [outImage release];
            [imageBlock release];
            [document release];
            
            if (fileContent) {
                if ([newDocManager.imageProperties readMetadataFromImageRepresentation:fileContent]) {
                    CZImageChannelProperties *channelProperties = newDocManager.imageProperties.channels.firstObject;
                    channelProperties.grayscale = @(model == kCGColorSpaceModelMonochrome);
                    imageBlock.channelProperties = channelProperties;
                    newDocManager.elementLayer.scaling = newDocManager.imageProperties.scaling;
                }
            }
        }
    } else if (fileFormat == kCZFileFormatTIF) {
        @autoreleasepool {
            if (image == nil) {
                image = [[[UIImage alloc] initWithContentsOfTifFile:filePath] autorelease];
            }
            
            CGColorSpaceModel model = CGColorSpaceGetModel(CGImageGetColorSpace(image.CGImage));
            UIImage *outImage = image;
            if (model == kCGColorSpaceModelMonochrome) {
                outImage = [CZCommonUtils newDecompressedImageFromJPEGImage:image]; // convert Gray8 image to RGBA8888 image
            } else {
                outImage = [outImage imageByRotatingToUp];  // rotate image to up
                [outImage retain];
            }
            NSString *directory = [filePath stringByDeletingLastPathComponent];
            CZImageBlock *imageBlock = [[CZImageBlock alloc] initWithImage:outImage channelProperties:nil];
            CZImageDocument *document = [[CZImageDocument alloc] initWithImageBlocks:@[imageBlock]];
            newDocManager = [[CZDocManager alloc] initWithDirectory:directory document:document];
            [outImage release];
            [imageBlock release];
            [document release];
            
            if (newDocManager) {
                if ([newDocManager.imageProperties readFromFile:filePath]) {
                    CZImageChannelProperties *channelProperties = newDocManager.imageProperties.channels.firstObject;
                    channelProperties.grayscale = @(model == kCGColorSpaceModelMonochrome);
                    imageBlock.channelProperties = channelProperties;
                    newDocManager.elementLayer.scaling = newDocManager.imageProperties.scaling;
                }
            }
        }
    }
    
    if (newDocManager == nil) {
        CZLogv(@"Open image file failed.");
        return newDocManager;
    }
    
    newDocManager.filePath = filePath;
    newDocManager.isImageSnapped = NO;
    newDocManager.isImageNew = NO;
    
    return newDocManager;
}

- (void)writeMetaDataToFilePath:(NSString *)path {
    pugi::xml_document doc;
    CZImageProperties *imageProperties = self.imageProperties;
    
    pugi::xml_node imageDocumentNode = doc.append_child("ImageDocument");
    
    pugi::xml_node metaDataNode = imageDocumentNode.append_child("Metadata");
    
    pugi::xml_node hardwareSetting = metaDataNode.append_child("HardwareSetting");
    pugi::xml_node microscopeNode = hardwareSetting.append_child("Microscope");
    
    const char *microscopeName = [imageProperties.microscopeName UTF8String];
    if (microscopeName) {
        microscopeNode.append_attribute("Name") = microscopeName;
    }
    
    const char *cstrManufacturer = [imageProperties.microscopeManufacturer UTF8String];
    if (cstrManufacturer) {
        microscopeNode.append_child("Manufacturer").text().set(cstrManufacturer);
    }
    
    metaDataNode.append_child("CustomAttributes");
    
    //Export Information Node
    pugi::xml_node informationNode = metaDataNode.append_child("Information");
    pugi::xml_node userNode = informationNode.append_child("User");
    userNode.append_attribute("Id") = 0;
    
    {  // set application section
        pugi::xml_node applicationNode = informationNode.append_child("Application");
        NSString *productNameKey = @"ABOUT_PRODUCT_BIO_NAME";
        NSString *productName = L(productNameKey);
        applicationNode.append_child("Name").text() = [productName UTF8String];
        
        NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString *version = [infoDict objectForKey:@"CFBundleVersion"];
        applicationNode.append_child("Version").text() = [version UTF8String];
    }
    
    informationNode.append_child("Institution");
    
    pugi::xml_node documentNode = informationNode.append_child("Document");
    pugi::xml_node creationDateNode = documentNode.append_child("CreationDate");
    const char *date = "2013-01-07T21:09:29.5406315+8:00";
    creationDateNode.append_child(pugi::node_pcdata).set_value(date);
    
    pugi::xml_node userNameNode = documentNode.append_child("UserName");
    NSString *operatorName = self.imageProperties.operatorName;
    const char *name = "";
    if (operatorName) {
        name = [operatorName UTF8String];
    }
    userNameNode.append_child(pugi::node_pcdata).set_value(name);
    
    pugi::xml_node imageNode = informationNode.append_child("Image");
    
    // append AcquisitionDateAndTime node
    NSDate *originalDate = imageProperties.acquisitionDate;
    if (originalDate) {
        time_t time = [originalDate timeIntervalSince1970];
        pugi::AppendTime(imageNode, "AcquisitionDateAndTime", time);
    }
    
//    pugi::xml_node ComponentBitCountNode = imageNode.append_child("ComponentBitCount");
//    char bitCount[kStringDefaultSize];
//    sprintf(bitCount, "%d", 8);
//    ComponentBitCountNode.append_child(pugi::node_pcdata).set_value(bitCount);
//
//    pugi::xml_node pixTypeNode = imageNode.append_child("PixelType");
//    const char *pixType = NULL;
//    if (cziImageRef->_pixType == kBGR24) {
//        pixType = "Bgr24";
//    } else if (cziImageRef->_pixType == kBGR32) {
//        pixType = "Bgr32";
//    } else if (cziImageRef->_pixType == kGray8) {
//        pixType = "Gray8";
//    } else {
//        CZLogv(@"unsupported pix type");
//        return;
//    }
//    pixTypeNode.append_child(pugi::node_pcdata).set_value(pixType);
//    UIImage *image = self.image;
//    CGImageRef imageRef = [image CGImage];
//    uint32_t width = (uint32_t)CGImageGetWidth(imageRef);
//    uint32_t height = (uint32_t)CGImageGetHeight(imageRef);
    
//    pugi::xml_node sizeXNode = imageNode.append_child("SizeX");
//    char sizeX[kStringDefaultSize];
//    sprintf(sizeX, "%d", cziImageRef->_width);
//    sizeXNode.append_child(pugi::node_pcdata).set_value(sizeX);
//    
//    pugi::xml_node sizeYNode = imageNode.append_child("SizeY");
//    char sizeY[kStringDefaultSize];
//    sprintf(sizeY, "%d", cziImageRef->_height);
//    sizeYNode.append_child(pugi::node_pcdata).set_value(height);
    
    pugi::xml_node dimensionsNode = imageNode.append_child("Dimensions");
    pugi::xml_node channelsNode = dimensionsNode.append_child("Channels");
    pugi::xml_node channelidNode = channelsNode.append_child("Channel");
    channelidNode.append_attribute("Id") = "Channel:0";
    channelidNode.append_child("Color").text() = "#FFFFFFFF";

    // Add "DetectorSettings" node, so that ZEN can see Exposure Time.
    pugi::xml_node detecorSettomgsNode = channelidNode.append_child("DetectorSettings");
    if (imageProperties.cameraModel) {
        detecorSettomgsNode.append_child("Detector").append_attribute("Id") = "Detector:1";
    }
    
    // add objective reference
    pugi::xml_node objectiveRef = imageNode.append_child("ObjectiveSettings").append_child("ObjectiveRef");
    objectiveRef.append_attribute("Id") = "Objective:1";
    
    pugi::xml_node microscopeRef = imageNode.append_child("MicroscopeRef");
    microscopeRef.append_attribute("Id") = "Microscope:1";
    
    // set expouretime
    NSNumber *exposureTimeInSeconds = imageProperties.channels.firstObject.exposureTimeInSeconds;
    if (exposureTimeInSeconds) {
        int exposureTimeInMicro = exposureTimeInSeconds.floatValue * 1e6f + 0.5f;
        NSString *exposureTimeInNano = [NSString stringWithFormat:@"%d000", exposureTimeInMicro];
        channelidNode.append_child("ExposureTime").text().set([exposureTimeInNano UTF8String]);
    }
    
    // set objective
    pugi::xml_node instrument = informationNode.append_child("Instrument");
    pugi::xml_node objective = instrument.append_child("Objectives").append_child("Objective");
    objective.append_attribute("Id") = "Objective:1";
    
    const char *cstrObjectiveModel = [imageProperties.objectiveModel UTF8String];
    if (cstrObjectiveModel) {
        objective.append_attribute("Name") = cstrObjectiveModel;
    }
    
    // set microscope mode
    pugi::xml_node microscope = instrument.append_child("Microscopes").append_child("Microscope");
    microscope.append_attribute("Id") = "Microscope:1";
    const char *cstrMikModel = [imageProperties.microscopeModel UTF8String];
    if (cstrMikModel) {
        microscope.append_attribute("Name") = cstrMikModel;
    }
    
    // set zoom and total magnification
    if (imageProperties.zoom || imageProperties.totalMagnification) {
        pugi::xml_node microscopeSettingsNode = imageNode.append_child("MicroscopeSettings");
        if (imageProperties.zoom) {
            pugi::xml_node zoomSettingsNode = microscopeSettingsNode.append_child("ZoomSettings");
            pugi::AppendValue(zoomSettingsNode, "Magnification", [imageProperties.zoom doubleValue]);
        }
        
        if (imageProperties.totalMagnification) {
            pugi::xml_node eyepieceSettingsNode = microscopeSettingsNode.append_child("EyepieceSettings");
            pugi::AppendValue(eyepieceSettingsNode, "TotalMagnification", [imageProperties.totalMagnification doubleValue]);
        }
    }
    
    // set camera mode
    if (imageProperties.cameraModel) {
        pugi::xml_node detectorNode = instrument.append_child("Detectors").append_child("Detector");
        detectorNode.append_attribute("Id") = "Detector:1";
        const char *cameraModelCString = [imageProperties.cameraModel UTF8String];
        detectorNode.append_attribute("Name") = cameraModelCString;
        
        detectorNode.append_child("Manufacturer").append_child("Model").text().set(cameraModelCString);
        
        if (imageProperties.cameraAdapter) {
            NSString *string = [CZCommonUtils stringFromNumber:imageProperties.cameraAdapter precision:2];
            string = [NSString stringWithFormat:@"%@x", string];
            detectorNode.append_child("Adapter").append_child("Manufacturer").append_child("Model").text() = [string UTF8String];
        }
    }
    
    pugi::xml_node tNode = dimensionsNode.append_child("T");
    pugi::xml_node startTimeNode = tNode.append_child("StartTime");
    const char *dateStart = "0001-01-01T00:00:00";
    startTimeNode.append_child(pugi::node_pcdata).set_value(dateStart);
    
    //Export Scaling
    pugi::xml_node scalingNode = metaDataNode.append_child("Scaling");
    pugi::xml_node scalingItemsNode = scalingNode.append_child("Items");
    if (self.elementLayer.isValidScaling) {
        pugi::xml_node scalingDistanceXNode = scalingItemsNode.append_child("Distance");
        scalingDistanceXNode.append_attribute("Id") = "X";
        pugi::xml_node scalingDistanceXValueNode = scalingDistanceXNode.append_child("Value");
        char distance[kStringDefaultSize];
        //distance is meter/pixel, and currently our scaling is in micrometer
        sprintf(distance, "%g", self.elementLayer.scaling * 1e-6);
        scalingDistanceXValueNode.append_child(pugi::node_pcdata).set_value(distance);
        pugi::xml_node scalingDistanceXFormatNode = scalingDistanceXNode.append_child("DefaultUnitFormat");
        const char *const format = "\xC2\xB5m";
        scalingDistanceXFormatNode.append_child(pugi::node_pcdata).set_value(format);
        
        pugi::xml_node scalingDistanceYNode = scalingItemsNode.append_child("Distance");
        scalingDistanceYNode.append_attribute("Id") = "Y";
        pugi::xml_node scalingDistanceYValueNode = scalingDistanceYNode.append_child("Value");
        scalingDistanceYValueNode.append_child(pugi::node_pcdata).set_value(distance);
        pugi::xml_node scalingDistanceYFormatNode = scalingDistanceYNode.append_child("DefaultUnitFormat");
        scalingDistanceYFormatNode.append_child(pugi::node_pcdata).set_value(format);
    }
    
    pugi::xml_node autoScalingNode = scalingNode.append_child("AutoScaling");
    
    // set scaling unit as pixel
    if (!self.elementLayer.isValidScaling) {
        autoScalingNode.append_child("Type").text() = "Measured";
        
        pugi::xml_node pixelNode = scalingItemsNode.append_child("Pixel");
        pixelNode.append_attribute("Id") = "X";
        pixelNode.append_child("Value").set_value("1");
        
        pixelNode = scalingItemsNode.append_child("Pixel");
        pixelNode.append_attribute("Id") = "Y";
        pixelNode.append_child("Value").set_value("1");
    }
    
    // set camera adapter
    if (imageProperties.cameraAdapter) {
        float cameraAdpater = [imageProperties.cameraAdapter floatValue];
        NSString *string = [CZCommonUtils stringFromFloat:cameraAdpater precision:2];
        string = [NSString stringWithFormat:@"CameraAdapter.%@x", string];
        autoScalingNode.append_child("CameraAdapter").text() = [string UTF8String];
        
        pugi::AppendValue(autoScalingNode, "CameraAdapterMagnification", cameraAdpater);
    }
    
    // set optovar magnification
    if (imageProperties.zoom) {
        pugi::AppendValue(autoScalingNode, "OptovarMagnification", [imageProperties.zoom floatValue]);
    }
    
    // save eyepiece
    if (imageProperties.eyepieceMagnification) {
        pugi::xml_node eyepieceNode = hardwareSetting.append_child("EyePiece");
        pugi::AppendValue(eyepieceNode, "Magnification", [imageProperties.eyepieceMagnification floatValue]);
    }
    
    //Export DisplaySettings
//    xml_node chanel = metaDataNode.append_child("DisplaySetting").append_child("Channels").append_child("Channel");
//    chanel.append_attribute("Id") = "Channel:0";
//    chanel.append_attribute("Name") = "C1";
//    chanel.append_child("BitCountRange").text() = "8";
//    chanel.append_child("DyeName").text() = "Dye1";
//    chanel.append_child("ColorMode").text() = "None";
    
#if METADATA_REFACTOED
    //Export Layers
    pugi::xml_node layersNode = metaDataNode.append_child("Layers");
    [self.elementLayer exportMetaDataTo:&layersNode];
#endif
    
    NSString *filePath = path;
    if (filePath) {
        doc.save_file([filePath fileSystemRepresentation]);
    }
}

@end
