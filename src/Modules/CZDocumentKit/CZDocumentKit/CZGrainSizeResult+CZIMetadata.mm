//
//  CZGrainSizeResult+CZIMetadata.mm
//  Matscope
//
//  Created by Ralph Jin on 8/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZGrainSizeResult+CZIMetadata.h"

@interface CZGrainsChord (CZIMetadata) <CZIMetadataNodeDecodable, CZIMetadataNodeEncodable>

@end

@implementation CZGrainsChord (CZIMetadata)

- (instancetype)initFromMetadataNode:(CZIMetadataNode *)node {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"This class is abstract class, can not create object!"
                                 userInfo:nil];
}

- (void)encodeToMetadataNode:(CZIMutableMetadataNode *)node {
    // sub-class shall override
}

@end

@implementation CZGrainsChordLine (CZIMetadata)

- (instancetype)initFromMetadataNode:(CZIMetadataNode *)node {
    if (node.geometry.isEmptyNode || node.geometry.x1.isEmptyNode || node.geometry.y1.isEmptyNode || node.geometry.x2.isEmptyNode || node.geometry.y2.isEmptyNode) {
        [self release];
        self = nil;
        return nil;
    }
    
    CGPoint startPoint = CGPointMake(node.geometry.x1.floatValue, node.geometry.y1.floatValue);
    CGPoint endPoint = CGPointMake(node.geometry.x2.floatValue, node.geometry.y2.floatValue);
    NSArray<NSValue *> *intersectionPoints = node.intersectionPoints.pointsValue;
    
    return [self initWithStartPoint:startPoint endPoint:endPoint intersectionPoints:intersectionPoints];
}

- (void)encodeToMetadataNode:(CZIMutableMetadataNode *)node {
    node.type.stringValue = @"Line";
    
    node.geometry.x1.floatValue = self.startPoint.x;
    node.geometry.y1.floatValue = self.startPoint.y;
    node.geometry.x2.floatValue = self.endPoint.x;
    node.geometry.x2.floatValue = self.endPoint.y;
    
    if ([self intersectionPointsCount] > 0) {
        node.intersectionPoints.pointsValue = self.intersectionPoints;
    }
}

@end

@implementation CZGrainsChordCircle (CZIMetadata)

- (instancetype)initFromMetadataNode:(CZIMetadataNode *)node {
    if (node.geometry.isEmptyNode || node.geometry.x.isEmptyNode || node.geometry.y.isEmptyNode || node.geometry.radius.isEmptyNode) {
        [self release];
        self = nil;
        return nil;
    }
    
    CGPoint center = CGPointMake(node.geometry.x.floatValue, node.geometry.y.floatValue);
    CGFloat radius = node.geometry.radius.floatValue;
    NSArray<NSValue *> *intersectionPoints = node.intersectionPoints.pointsValue;
    
    return [self initWithCenter:center radius:radius intersectionPoints:intersectionPoints];
}

- (void)encodeToMetadataNode:(CZIMutableMetadataNode *)node {
    node.type.stringValue = @"Circle";
    
    node.geometry.x.floatValue = self.center.x;
    node.geometry.y.floatValue = self.center.y;
    node.geometry.radius.floatValue = self.radius;
    
    if ([self intersectionPointsCount] > 0) {
        node.intersectionPoints.pointsValue = self.intersectionPoints;
    }
}

@end

@implementation CZGrainSizeResult (CZIMetadata)

- (instancetype)initFromMetadataNode:(CZIMetadataNode *)node {
    if (node.version.isEmptyNode || node.version.floatValue != 1.0f || node.grainSizeNumber.isEmptyNode || node.chordPattern.isEmptyNode || node.chordPattern.type.isEmptyNode || node.chordPattern.scale.isEmptyNode) {
        [self release];
        self = nil;
        return nil;
    }
    
    float grainSize = node.grainSizeNumber.floatValue;
    float meanDistance = node.meanDistance.floatValue;
    
    NSString *patternName = node.chordPattern.type.stringValue;
    CZGrainSizePattern pattern = [CZGrainSizeResult patternFromName:patternName];
    
    float patternZoom = node.chordPattern.scale.floatValue;
    
    NSString *standardName = node.chordPattern.standard.stringValue;
    CZGrainsStandard standard = [CZGrainSizeResult standardFromName:standardName];
    
    NSMutableArray<CZGrainsChord *> *chords = [NSMutableArray array];
    for (CZIMetadataNode *chordNode in node.chordPattern) {
        if ([chordNode.nodeName isEqualToString:CZIMutableMetadata.metadata.document.chord.nodeName]) {
            CZGrainsChord *chord = nil;
            if ([chordNode.type.stringValue isEqualToString:@"Line"]) {
                chord = [[CZGrainsChordLine alloc] initFromMetadataNode:chordNode];
            } else if ([chordNode.type.stringValue isEqualToString:@"Circle"]) {
                chord = [[CZGrainsChordCircle alloc] initFromMetadataNode:chordNode];
            }
            if (chord) {
                [chords addObject:chord];
                [chord release];
            }
        }
    }
    
    return [self initWithStandard:standard grainSize:grainSize meanDistance:meanDistance patternType:pattern patternZoom:patternZoom chords:chords];
}

- (void)encodeToMetadataNode:(CZIMutableMetadataNode *)node {
    node.version.floatValue = 1.0f;
    node.grainSizeNumber.floatValue = self.grainSize;
    node.meanDistance.floatValue = self.meanDistance;
    
    node.chordPattern.scale.floatValue = self.patternZoom;
    node.chordPattern.standard.stringValue = self.standardName;
    node.chordPattern.type.stringValue = self.patternName;
    
    for (CZGrainsChord *chord in self.chords) {
        auto chordNode = CZIMutableMetadata.metadata.document.chord;
        [chord encodeToMetadataNode:chordNode];
        [node.chordPattern appendNode:chordNode];
    }
}

@end
