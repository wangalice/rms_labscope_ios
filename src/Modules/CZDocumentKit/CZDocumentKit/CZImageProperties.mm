//
//  CZImageProperties.mm
//  Hermes
//
//  Created by Ralph Jin on 3/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties.h"
#import <ImageIO/ImageIO.h>
#include <libexif/exif-loader.h>
#include <libexif/exif-data.h>
#include <tiffio.h>
#import <CZToolbox/CZToolbox.h>
#import <CZIKit/CZIKit.h>
#import "CZImageProperties+CZIMetadata.h"
#import "CZImageProperties+MakerNote.h"
#import "CZImageProperties+EXIF.h"
#import <CZMicroscopeManager/CZMicroscopeManager.h>
#import "CZImageDocument.h"
#import "CZImagePropertiesPrivate.h"

const float kInchToUM = 25400;
const float kCMToUM = 10000;
const ExifTag kLensModelTag = (ExifTag)0xa434;
static NSString *const kImageDescriptionSeparater = @" / ";

/*!
 * Convert float to rational number for EXIF writing.
 * From http://rosettacode.org/wiki/Convert_decimal_number_to_rational#C
 *
 * f : number to convert.
 * num, denom: returned parts of the rational.
 * md: max denominator value.  Note that machine floating point number
 *     has a finite resolution (10e-16 ish for 64 bit double), so specifying
 *     a "best match with minimal error" is often wrong, because one can
 *     always just retrieve the significand and return that divided by
 *     2**52, which is in a sense accurate, but generally not very useful:
 *     1.0/7.0 would be "2573485501354569/18014398509481984", for example.
 */
static ExifRational floatToExifRational(float f, ExifLong md) {
    CZLogv(@"Converted %f to rational as ", f);
    ExifRational r;
    
	// a: continued fraction coefficients.
	int64_t a, h[3] = { 0, 1, 0 }, k[3] = { 1, 0, 0 };
	int64_t x, d, n = 1;
	int i, neg = 0;
    
	if (md <= 1) {
        r.denominator = 1;
        r.numerator = (ExifLong)f;
        return r;
    }
    
	if (f < 0) {
        neg = 1;
        f = -f;
    }
    
	while (f != floor(f)) {
        n <<= 1;
        f *= 2;
    }
	d = f;
    
	/* continued fraction and check denominator each step */
	for (i = 0; i < 64; i++) {
		a = n ? d / n : 0;
		if (i && !a) {
            break;
        }
        
		x = d;
        d = n;
        n = x % n;
        
		x = a;
		if (k[1] * a + k[0] >= md) {
			x = (md - k[0]) / k[1];
			if (x * 2 >= a || k[1] >= md) {
				i = 65;
			} else {
				break;
            }
		}
        
		h[2] = x * h[1] + h[0];
        h[0] = h[1];
        h[1] = h[2];
        
		k[2] = x * k[1] + k[0];
        k[0] = k[1];
        k[1] = k[2];
	}
    
	r.denominator = (ExifLong)k[1];
	r.numerator = (ExifLong)(neg ? -h[1] : h[1]);
    
    CZLogv(@"%d / %d\n", r.numerator, r.denominator);
    
    return r;
}

/*! Get an existing tag, or create one if it doesn't exist */
static ExifEntry *init_tag(ExifData *exif, ExifIfd ifd, ExifTag tag) {
    ExifEntry *entry = exif_content_get_entry(exif->ifd[ifd], tag);
    // Return an existing tag if one exists
    if (!entry) {
        // Allocate a new entry
        entry = exif_entry_new();
        assert(entry != NULL); // catch an out of memory condition
        entry->tag = tag; // tag must be set before calling exif_content_add_entry
        
        // Attach the ExifEntry to an IFD
        exif_content_add_entry(exif->ifd[ifd], entry);
        
        // Allocate memory for the entry and fill with default data
        exif_entry_initialize(entry, tag);
        
        /* Ownership of the ExifEntry has now been passed to the IFD.
         * One must be very careful in accessing a structure after
         * unref'ing it; in this case, we know "entry" won't be freed
         * because the reference count was bumped when it was added to
         * the IFD.
         */
        exif_entry_unref(entry);
    }
    return entry;
}

/*! Create a brand-new tag with a data field of the given length, in the
 * given IFD. This is needed when exif_entry_initialize() isn't able to create
 * this type of tag itself, or the default data length it creates isn't the
 * correct length.
 */
static ExifEntry *create_tag(ExifData *exif, ExifIfd ifd, ExifTag tag, size_t len) {
    void *buf;
    ExifEntry *entry;
    
    // Create a memory allocator to manage this ExifEntry 
    ExifMem *mem = exif_mem_new_default();
    assert(mem != NULL); // catch an out of memory condition 
    
    // Create a new ExifEntry using our allocator 
    entry = exif_entry_new_mem(mem);
    assert(entry != NULL);
    
    // Allocate memory to use for holding the tag data 
    buf = exif_mem_alloc(mem, (ExifLong)len);
    assert(buf != NULL);
    
    // Fill in the entry
    entry->data = (unsigned char *)buf;
    entry->size = (unsigned int)len;
    entry->tag = tag;
    entry->components = len;
    entry->format = EXIF_FORMAT_UNDEFINED;
    
    // Attach the ExifEntry to an IFD
    exif_content_add_entry(exif->ifd[ifd], entry);
    
    // The ExifMem and ExifEntry are now owned elsewhere
    exif_mem_unref(mem);
    exif_entry_unref(entry);
    
    return entry;
}

static ExifEntry *create_string_tag(ExifData *exif, ExifIfd ifd, ExifTag tag, NSString *ns_string) {
    if (ns_string == nil) {
        return NULL;
    }
    
    ExifEntry *entry = exif_content_get_entry(exif->ifd[ifd], tag);

    // Return an existing tag if one exists
    if (entry) {
        exif_content_remove_entry(exif->ifd[ifd], entry);
    }
    
    entry = 0L;
    const char *c_string = [ns_string cStringUsingEncoding:NSUTF8StringEncoding];
    if (c_string) {
        size_t string_length = strlen(c_string);
        entry = create_tag(exif, ifd, tag, string_length + 1);
        entry->format = EXIF_FORMAT_ASCII;
        memcpy(entry->data, c_string, string_length + 1);
    }
    
    return entry;
}

@implementation CZImageChannelProperties

- (instancetype)initWithGrayscale:(NSNumber *)grayscale channelName:(NSString *)channelName channelColor:(UIColor *)channelColor channelSelected:(NSNumber *)channelSelected filterSetID:(NSString *)filterSetID filterSetName:(NSString *)filterSetName lightSource:(NSString *)lightSource wavelength:(NSString *)wavelength exposureTimeInSeconds:(NSNumber *)exposureTimeInSeconds gain:(NSNumber *)gain {
    self = [super init];
    if (self) {
        _grayscale = [grayscale retain];
        _channelName = [channelName copy];
        _channelColor = [channelColor retain];
        _channelSelection = [channelSelected retain];
        _filterSetID = [filterSetID copy];
        _filterSetName = [filterSetName copy];
        _lightSource = [lightSource copy];
        _wavelength = [wavelength copy];
        _exposureTimeInSeconds = [exposureTimeInSeconds retain];
        _gain = [gain retain];
    }
    return self;
}

- (void)dealloc {
    [_grayscale release];
    [_channelName release];
    [_channelColor release];
    [_channelSelection release];
    [_filterSetID release];
    [_filterSetName release];
    [_lightSource release];
    [_wavelength release];
    [_exposureTimeInSeconds release];
    [_gain release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZImageChannelProperties *channelProperties = [[[self class] alloc] init];
    channelProperties.grayscale = self.grayscale;
    channelProperties.channelName = self.channelName;
    channelProperties.channelColor = self.channelColor;
    channelProperties.channelSelection = self.channelSelection;
    channelProperties.filterSetID = self.filterSetID;
    channelProperties.filterSetName = self.filterSetName;
    channelProperties.lightSource = self.lightSource;
    channelProperties.wavelength = self.wavelength;
    channelProperties.exposureTimeInSeconds = self.exposureTimeInSeconds;
    channelProperties.gain = self.gain;
    return channelProperties;
}

- (instancetype)channelPropertiesByReplacingChannelName:(NSString *)channelName {
    CZImageChannelProperties *channelProperties = [[self copy] autorelease];
    channelProperties.channelName = channelName;
    return channelProperties;
}

- (instancetype)channelPropertiesByReplacingChannelColor:(UIColor *)channelColor {
    CZImageChannelProperties *channelProperties = [[self copy] autorelease];
    channelProperties.channelColor = channelColor;
    return channelProperties;
}

- (instancetype)channelPropertiesByReplacingChannelSelection:(NSNumber *)channelSelection {
    CZImageChannelProperties *channelProperties = [[self copy] autorelease];
    channelProperties.channelSelection = channelSelection;
    return channelProperties;
}

@end

@interface CZImageProperties () {
    // save other _exif data which Hermes doesn't interest.
    ExifData *_exif;
}

@end

@implementation CZImageProperties

+ (NSDateFormatter *)defaultDateFormat {
    static NSDateFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    });
    
    return formatter;
}

- (id)init {
    self = [super init];
    if (self) {
        [self addObserver:self forKeyPaths:@"objectiveModel", @"totalMagnification", @"zoom", @"microscopeModel", nil];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPaths:@"objectiveModel", @"totalMagnification", @"zoom", @"microscopeModel", nil];
    
    if (_exif) {
        exif_data_unref(_exif);
    }
    
    [_microscopeManufacturer release];
    [_microscopeName release];
    [_microscopeModel release];
    [_cameraModel release];
    [_operatorName release];
    [_acquisitionDate release];
    
    [_objectiveModel release];
    [_zoom release];
    [_cameraAdapter release];
    [_displayCameraAdapter release];
    [_scalingInPixels release];
    [_bitDepth release];
    
    [_channels release];
    
    [_magnificationFactor release];
    [_totalMagnification release];
    [_eyepieceMagnification release];
    
    [super dealloc];
}

- (NSDictionary *)metadataOfImage:(UIImage *)image {
    CGSize imageSize = image.size;

    // convert EXIF to mutable dictionary
    NSMutableDictionary *EXIFDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *TIFFDictionary = [NSMutableDictionary dictionary];

    if (self.microscopeManufacturer) {
        [CZImageProperties saveString:self.microscopeManufacturer
                         toDictionary:TIFFDictionary
                               forKey:kCGImagePropertyTIFFMake];
    }
    
    if (self.cameraModel) {
        [CZImageProperties saveString:self.cameraModel
                         toDictionary:TIFFDictionary
                               forKey:kCGImagePropertyTIFFModel];
    } else if (self.microscopeModel) {
        [CZImageProperties saveString:self.microscopeModel
                         toDictionary:TIFFDictionary
                               forKey:kCGImagePropertyTIFFModel];
    }
    
    if (self.operatorName) {
        [CZImageProperties saveString:self.operatorName
                         toDictionary:TIFFDictionary
                               forKey:kCGImagePropertyTIFFArtist];
    }
    
    if (self.channels.count == 1 && self.channels[0].channelColor == nil && self.channels[0].exposureTimeInSeconds) {
        [CZImageProperties saveFloat:self.channels[0].exposureTimeInSeconds.floatValue
                        toDictionary:EXIFDictionary
                              forKey:kCGImagePropertyExifExposureTime];
    }
    
    if (self.acquisitionDate) {
        [CZImageProperties saveDate:self.acquisitionDate
                       toDictionary:EXIFDictionary
                             forKey:kCGImagePropertyExifDateTimeOriginal];
    }
    
    [self saveScalingToEXIF:EXIFDictionary];
    
    NSString *string = [self combineImageDescription];
    if (string.length) {
        [CZImageProperties saveString:string
                         toDictionary:TIFFDictionary
                               forKey:kCGImagePropertyTIFFImageDescription];
    }

    return @{(NSString *)kCGImagePropertyExifDictionary: EXIFDictionary,
             (NSString *)kCGImagePropertyTIFFDictionary: TIFFDictionary,
             (NSString *)kCGImagePropertyPixelHeight: @(imageSize.width),
             (NSString *)kCGImagePropertyPixelWidth: @(imageSize.height),
             (NSString *)kCGImagePropertyDPIWidth: @72,
             (NSString *)kCGImagePropertyDPIHeight: @72,
             (NSString *)kCGImagePropertyOrientation: @1};  // top left
}

- (NSData *)JPEGRepresentationOfImage:(UIImage *)image compression:(CGFloat)compression {
    NSData *primaryImageData = UIImageJPEGRepresentation(image, compression);
    CGSize imageSize = image.size;
    
    CGSize thumbSize = CGSizeMake(CZImageDocumentThumbnailWidth, CZImageDocumentThumbnailHeight);
    UIImage *thumbImage = [CZCommonUtils scaleImage:image intoSize:thumbSize fillColor:[UIColor whiteColor]];
    NSData *thumbData = UIImageJPEGRepresentation(thumbImage, 0.7);

    if (!_exif) {
        _exif = exif_data_new();
    }
    if (_exif) {
        // Set the image options
        exif_data_set_option(_exif, EXIF_DATA_OPTION_FOLLOW_SPECIFICATION);
        exif_data_set_data_type(_exif, EXIF_DATA_TYPE_COMPRESSED);
        exif_data_set_byte_order(_exif, EXIF_BYTE_ORDER_INTEL);
        
        // Create the mandatory EXIF fields with default data
        exif_data_fix(_exif);
        
        NSString *makerNote = [self makerNoteRepresentation];
        create_string_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_MAKER_NOTE, makerNote);
        
        ExifEntry *entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_X_DIMENSION);
        exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, (ExifLong)imageSize.width);
        
        entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_Y_DIMENSION);
        exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, (ExifLong)imageSize.height);
        
        short exif_orientation;
        switch (image.imageOrientation) {
            case UIImageOrientationUp:
                exif_orientation = 1;
                break;
            case UIImageOrientationDown:
                exif_orientation = 3;
                break;
            case UIImageOrientationLeft:
                exif_orientation = 8;
                break;
            case UIImageOrientationRight:
                exif_orientation = 6;
                break;
            case UIImageOrientationUpMirrored:
                exif_orientation = 2;
                break;
            case UIImageOrientationDownMirrored:
                exif_orientation = 4;
                break;
            case UIImageOrientationLeftMirrored:
                exif_orientation = 5;
                break;
            case UIImageOrientationRightMirrored:
                exif_orientation = 7;
                break;
        }
        entry = init_tag(_exif, EXIF_IFD_0, EXIF_TAG_ORIENTATION);
        exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, exif_orientation);
        
        entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_COLOR_SPACE);
        exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, 1);
        
        // write exposure time
        if (self.channels.count == 1 && self.channels[0].channelColor == nil && self.channels[0].exposureTimeInSeconds) {
            entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_EXPOSURE_TIME);
            exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, floatToExifRational(self.channels[0].exposureTimeInSeconds.floatValue, 50000));  // minimal 0.02 micro-second
        }
        
        // write scaling
        if (self.scaling > 0) {
            ExifRational resolution = floatToExifRational(kInchToUM / self.scaling, INT_MAX);
            entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_PLANE_X_RESOLUTION);
            exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, resolution);
            
            entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_PLANE_Y_RESOLUTION);
            exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, resolution);
            
            entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_PLANE_RESOLUTION_UNIT);
            exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, 2);  // 2 means inch
        }

        // write original date time
        if (self.acquisitionDate) {
            NSDateFormatter *formatter = [CZImageProperties defaultDateFormat];
            NSString *dateString = [formatter stringFromDate:self.acquisitionDate];
            
            if ([dateString length] > 0) {
                entry = init_tag(_exif, EXIF_IFD_EXIF, EXIF_TAG_DATE_TIME_ORIGINAL);
                const char *dateCString = [dateString UTF8String];
                entry->format = EXIF_FORMAT_ASCII;
                memcpy(entry->data, dateCString, 20);  // 20 is the length of date format
            }
        }
        
        // write manufacturer
        create_string_tag(_exif, EXIF_IFD_0, EXIF_TAG_MAKE, self.microscopeManufacturer);
        
        // write model
        if (self.cameraModel) {
            create_string_tag(_exif, EXIF_IFD_0, EXIF_TAG_MODEL, self.cameraModel);
        } else {
            create_string_tag(_exif, EXIF_IFD_0, EXIF_TAG_MODEL, self.microscopeModel);
        }
        
        // operator name
        if (self.operatorName) {
            create_string_tag(_exif, EXIF_IFD_0, EXIF_TAG_ARTIST, self.operatorName);
        }
        
        // write objective model
        NSString *string = [self combineImageDescription];
        if (string.length) {
            create_string_tag(_exif, EXIF_IFD_0, EXIF_TAG_IMAGE_DESCRIPTION, string);
        }
        
        // Create thumbnail entry
        entry = init_tag(_exif, EXIF_IFD_1, EXIF_TAG_COMPRESSION);
        exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, (ExifShort)6);
        
        if (_exif->data) {
            free(_exif->data);
            _exif->data = NULL;
        }
        _exif->data = (unsigned char *)malloc([thumbData length]);
        _exif->size = (uint32_t)[thumbData length];
        [thumbData getBytes:_exif->data length:[thumbData length]];
        
        // Save _exif to binary data
        unsigned char *exif_data;
        unsigned int exif_data_len;
        exif_data_save_data(_exif, &exif_data, &exif_data_len);
        if (exif_data == NULL) {
            return nil;
        }
        
        NSMutableData *result = [[NSMutableData alloc] initWithCapacity:(exif_data_len + [primaryImageData length] + 1024)];
        
        // Write EXIF header
        const unsigned char exif_header[] = {
            0xff, 0xd8, 0xff, 0xe1
        };
        const unsigned int exif_header_len = sizeof(exif_header);
        [result appendBytes:exif_header length:exif_header_len];
        
        // Write EXIF block length in big-endian order
        uint16_t templength = exif_data_len + 2;
        templength = htons(templength);
        [result appendBytes:&templength length:2];
        
        // Write EXIF data block
        [result appendBytes:exif_data length:exif_data_len];
        free(exif_data);
        
        assert(exif_data_len < 0x10000);
        
        // Scan JPEG image data's offset
        unsigned int image_data_offset = 2;  // skip 0xFFD8
        while (true) {
            unsigned char tag[2];
            [primaryImageData getBytes:tag range:NSMakeRange(image_data_offset, 2)];
            
            if (tag[0] == 0xFF && (tag[1] == 0xE0 || tag[1] == 0xE1)) {  // if tag == 0xFFE0 or 0xFFE1
                image_data_offset += 2;
                
                uint16_t blockLength = 0;
                [primaryImageData getBytes:&blockLength range:NSMakeRange(image_data_offset, 2)];
                blockLength = ntohs(blockLength);
                image_data_offset += blockLength;
            } else {
                break;
            }
        }
        
        // Write JPEG image data, skipping the non-EXIF header
        const unsigned int buff_length = 2048;
        NSRange range;
        range.location = image_data_offset;
        range.length = buff_length;
        char buff[buff_length];
        for (NSUInteger remainLength = [primaryImageData length] - range.location;
             remainLength != 0; remainLength -= range.length) {
            if (remainLength < range.length) {
                range.length = remainLength;
            }
            [primaryImageData getBytes:buff range:range];
            [result appendBytes:buff length:range.length];
            range.location += range.length;
        }
        
        return [result autorelease];
    }
    
    return nil;
}

- (BOOL)readFromFile:(NSString *)filePath {
    BOOL success = NO;
    CZFileFormat fileFormat = [CZCommonUtils fileFormatForExtension:filePath.pathExtension];
    
    if (fileFormat == kCZFileFormatCZI) {
        CZIDocument *czi = [[CZIDocument alloc] initWithFile:filePath];
        [self readFromCZIMetadata:czi.metadata];
        [czi release];
        success = YES;
    } else if (fileFormat == kCZFileFormatJPEG) {
        @autoreleasepool {
            do {
                NSInputStream *fileStream = [NSInputStream inputStreamWithFileAtPath:filePath];
                if (fileStream == nil) {
                    break;
                }
                
                ExifLoader *loader = exif_loader_new();
                if (loader == NULL) {
                    break;
                }

                BOOL readEXIFBlockSuccess = NO;
                const NSInteger kBlockSize = 2048;
                uint8_t buff[kBlockSize];
                [fileStream open];
                while (true) {
                    NSInteger size = [fileStream read:buff maxLength:kBlockSize];
                    if (size == 0) {
                        break;
                    }
                    
                    if (!exif_loader_write(loader, buff, (unsigned int)size)) {
                        readEXIFBlockSuccess = YES;
                        break;
                    }
                }
                [fileStream close];
            
                ExifData *exif = NULL;
                if (readEXIFBlockSuccess) {
                    exif = exif_loader_get_data(loader);
                }
                exif_loader_unref(loader);
                
                if (exif == NULL) {
                    break;
                }
                
                if (_exif) {
                    exif_data_unref(_exif);
                }
                _exif = exif;

                success = [self readMetadataFromExif];
            } while (0);
        }
    } else if (fileFormat == kCZFileFormatTIF) {
        success = [self readFromTifFile:filePath];
    }
    
    return success;
}

- (BOOL)readMetadataFromImageRepresentation:(NSData *)imageRepresentation {    
    if (_exif) {
        exif_data_unref(_exif);
        _exif = NULL;
    }
    
    _exif = exif_data_new();
    exif_data_unset_option(_exif, EXIF_DATA_OPTION_IGNORE_UNKNOWN_TAGS);
    exif_data_load_data(_exif, (const unsigned char *)[imageRepresentation bytes], (unsigned int)[imageRepresentation length]);

    return [self readMetadataFromExif];
}

- (BOOL)readMetadataFromExif {
    NSString *makerNote = nil;
    CZImageProperties *fromEXIF = [[CZImageProperties alloc] initFromEXIF:_exif makerNote:&makerNote];
    CZImageProperties *fromMakerNote = [[CZImageProperties alloc] initFromMakerNote:makerNote];
    
    [self combineImagePropertiesFromEXIF:fromEXIF withImagePropertiesFromMakerNote:fromMakerNote];
    
    [fromEXIF release];
    [fromMakerNote release];
    
    return YES;
}

- (void)combineImagePropertiesFromEXIF:(CZImageProperties *)fromEXIF withImagePropertiesFromMakerNote:(CZImageProperties *)fromMakerNote {
    if (fromMakerNote.microscopeManufacturer.length > 0) {
        self.microscopeManufacturer = fromMakerNote.microscopeManufacturer;
    } else {
        self.microscopeManufacturer = fromEXIF.microscopeManufacturer;
    }
    
    if (fromMakerNote.microscopeName.length > 0) {
        self.microscopeName = fromMakerNote.microscopeName;
    } else {
        self.microscopeName = fromEXIF.microscopeName;
    }
    
    if (fromMakerNote.microscopeModel.length > 0) {
        self.microscopeModel = fromMakerNote.microscopeModel;
    } else {
        self.microscopeModel = fromEXIF.microscopeModel;
    }
    
    if (fromMakerNote.cameraModel.length > 0) {
        self.cameraModel = fromMakerNote.cameraModel;
    } else {
        self.cameraModel = fromEXIF.cameraModel;
    }
    
    if (fromMakerNote.operatorName.length > 0) {
        self.operatorName = fromMakerNote.operatorName;
    } else {
        self.operatorName = fromEXIF.operatorName;
    }
    
    if (fromMakerNote.acquisitionDate) {
        self.acquisitionDate = fromMakerNote.acquisitionDate;
    } else {
        self.acquisitionDate = fromEXIF.acquisitionDate;
    }
    
    if (fromMakerNote.objectiveModel.length > 0) {
        self.objectiveModel = fromMakerNote.objectiveModel;
    } else {
        self.objectiveModel = fromEXIF.objectiveModel;
    }
    
    if (fromMakerNote.zoom.floatValue > 0.0) {
        self.zoom = fromMakerNote.zoom;
    } else {
        self.zoom = fromEXIF.zoom;
    }
    
    if (fromMakerNote.cameraAdapter.floatValue > 0.0) {
        self.cameraAdapter = fromMakerNote.cameraAdapter;
    } else {
        self.cameraAdapter = fromEXIF.cameraAdapter;
    }
    
    if (fromMakerNote.scaling > 0.0) {
        self.scaling = fromMakerNote.scaling;
    } else if (fromEXIF.scaling > 0.0) {
        self.scaling = fromEXIF.scaling;
    }
    
    self.bitDepth = fromMakerNote.bitDepth;
    
    if (fromMakerNote.channels.count > 0) {
        self.channels = fromMakerNote.channels;
    } else {
        self.channels = fromEXIF.channels;
    }
}

- (NSString *)magnificationFactorString {
    if (self.magnificationFactor == nil) {
        return nil;
    }
    
    NSString *magnification = [CZCommonUtils stringFromNumber:self.magnificationFactor precision:2];
    return [magnification stringByAppendingString:@"x"];
}

- (BOOL)isStereoMicroscopeMode {
    if (self.totalMagnification) {
        return YES;
    }
    if ([self.microscopeModel isEqualToString:kModelNameStereo]) {
        return YES;
    }

    return NO;
}

- (BOOL)isMacroImageMicroscopeMode {
    if ([self.microscopeModel isEqualToString:kModelNameMacroImage] ||
        ([self.microscopeModel isEqualToString:kCameraModelNameBuiltIn])) {
        return YES;
    }
    
    return NO;
}

- (NSString *)combineImageDescription {
    NSMutableArray *imageDescriptions = [NSMutableArray array];

    NSString *string;
    if (self.totalMagnification) {
        string = [NSString stringWithFormat:@"Magnification: %@x",
                  [CZCommonUtils stringFromNumber:self.totalMagnification precision:2]];
        [imageDescriptions addObject:string];
    }
    
    if (self.eyepieceMagnification) {
        string = [NSString stringWithFormat:@"EyepieceMagnification: %@x",
                  [CZCommonUtils stringFromNumber:self.eyepieceMagnification precision:2]];
        [imageDescriptions addObject:string];
    }
    
    if (self.zoom) {
        string = [NSString stringWithFormat:@"Zoom: %@x",
                  [CZCommonUtils stringFromNumber:self.zoom precision:2]];
        [imageDescriptions addObject:string];
    }
    
    if (self.cameraAdapter) {
        string = [NSString stringWithFormat:@"CameraAdapter: %@x",
                  [CZCommonUtils stringFromNumber:self.cameraAdapter precision:2]];
        [imageDescriptions addObject:string];
    }
    
    if (self.objectiveModel.length) {
        string = [NSString stringWithFormat:@"Objective: %@", self.objectiveModel];
        [imageDescriptions addObject:string];
    }
    
    if (self.microscopeName.length) {
        string = [NSString stringWithFormat:@"MicroscopeName: %@", self.microscopeName];
        [imageDescriptions addObject:string];
    }
    
    string = [imageDescriptions componentsJoinedByString:kImageDescriptionSeparater];
    return string;
}

- (void)parseFromImageDescription:(NSString *)imageDescription {
    NSString *string = imageDescription;
    NSArray *items = [string componentsSeparatedByString:kImageDescriptionSeparater];
    
    if (items.count == 0) {
        self.objectiveModel = string;
    } else {
        BOOL parseSuccess = YES;
        for (NSString *pair in items) {
            NSArray *keyAndValue = [pair componentsSeparatedByString:@": "];
            if (keyAndValue.count != 2) {
                parseSuccess = NO;
                break;
            }
            
            NSString *key = keyAndValue[0];
            NSString *value = keyAndValue[1];
            
            if ([key isEqualToString:@"Magnification"]) {
                self.totalMagnification = @([value floatValue]);
            } else if ([key isEqualToString:@"Zoom"]) {
                self.zoom = @([value floatValue]);
            } else if ([key isEqualToString:@"Objective"]) {
                self.objectiveModel = value;
            } else if ([key isEqualToString:@"CameraAdapter"]) {
                self.cameraAdapter = @([value floatValue]);
            } else if ([key isEqualToString:@"MicroscopeName"]) {
                self.microscopeName = value;
            } else if ([key isEqualToString:@"EyepieceMagnification"]) {
                self.eyepieceMagnification = @([value floatValue]);
            } else if ([key isEqualToString:@"Reflector"]) {
                // Ignore reflector
            } else {
                parseSuccess = NO;
                break;
            }
        }
        
        if (!parseSuccess) {
            self.eyepieceMagnification = nil;
            self.totalMagnification = nil;
            self.zoom = nil;
            self.microscopeName = nil;
            self.cameraAdapter = nil;
            self.objectiveModel = string;
        }
    }
}

- (void)parseFromModelName:(NSString *)modelName {
    if ([modelName isEqualToString:kCameraModelNameAxiocamERc5s]) {
        self.cameraModel = modelName;
        if (self.totalMagnification) {
            self.microscopeModel = kModelNameStereo;
        } else {
            self.microscopeModel = kModelNameCompound;
        }
    } else if ([modelName isEqualToString:kCameraModelNameBuiltIn]) {
        self.cameraModel = modelName;
        self.microscopeModel = kModelNameMacroImage;
    } else {  // TODO: add camera name to microscope model name mapping here
        self.microscopeModel = modelName;
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if ([self isStereoMicroscopeMode]) {
        [self updateMagnificationFactorFromTotalMagnification];
    } else {
        [self updateMagnificationFactorFromObjectiveModel];
    }
}

#pragma mark - private

- (void)updateMagnificationFactorFromObjectiveModel {
    NSTextCheckingResult *match = nil;
    if (self.objectiveModel) {
        NSError *error = NULL;
        NSRegularExpression *regx = [NSRegularExpression regularExpressionWithPattern:@"([\\d|\\.]+)x"
                                                                              options:NSRegularExpressionCaseInsensitive
                                                                                error:&error];
        
        match = [regx firstMatchInString:self.objectiveModel
                                 options:0
                                   range:NSMakeRange(0, self.objectiveModel.length)];
    }
    
    if (match && [match numberOfRanges] > 1) {
        NSRange range = [match rangeAtIndex:1];
        NSString *magnificationFactorString = [self.objectiveModel substringWithRange:range];
        self.magnificationFactor = @([magnificationFactorString doubleValue]);
    } else {
        self.magnificationFactor = nil;
    }
}

- (void)updateMagnificationFactorFromTotalMagnification {
    self.magnificationFactor = self.totalMagnification;
}

- (BOOL)readFromTifFile:(NSString *)filePath {
    TIFF *tif = TIFFOpen([filePath fileSystemRepresentation], "r");
    if (!tif) {
        CZLogv(@"Can't create TIFF file at path: %@.\n", filePath);
        return NO;
    }
    
    NSString *makerNote = nil;
    CZImageProperties *fromEXIF = [[CZImageProperties alloc] initFromTIFF:tif makerNote:&makerNote];
    CZImageProperties *fromMakerNote = [[CZImageProperties alloc] initFromMakerNote:makerNote];
    
    [self combineImagePropertiesFromEXIF:fromEXIF withImagePropertiesFromMakerNote:fromMakerNote];
    
    [fromEXIF release];
    [fromMakerNote release];
    
    if (tif) {
        TIFFClose(tif);
    }
    
    return YES;
}

/*! save metaData for Labscope iPhone. */

+ (void)saveFloat:(float)value toDictionary:(NSMutableDictionary *)dict forKey:(CFStringRef)key {
    NSNumber *number = [[NSNumber alloc] initWithFloat:value];
    [dict setObject:number forKey:(NSString *)key];
    [number release];
}

+ (void)saveDate:(NSDate *)date toDictionary:(NSMutableDictionary *)dict forKey:(CFStringRef)key {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:date];
    [dict setObject:dateString forKey:(NSString *)key];
    
    [formatter release];
}

+ (void)saveString:(NSString *)string toDictionary:(NSMutableDictionary *)dict forKey:(CFStringRef)key {
    [dict setObject:string forKey:(NSString *)key];
}

+ (NSNumber *)loadNumberFromDict:(NSDictionary *)dict forKey:(CFStringRef)key {
    id obj = [dict objectForKey:(NSString *)key];
    if ([obj isKindOfClass:[NSNumber class]]) {
        return (NSNumber *)obj;
    } else {
        return nil;
    }
}

+ (NSDate *)loadDateFromDict:(NSDictionary *)dict forKey:(CFStringRef)key {
    id obj = [dict objectForKey:(NSString *)key];
    if ([obj isKindOfClass:[NSString class]]) {
        NSString *dateTimeStr = (NSString *)obj;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
        
        NSDate *date = [formatter dateFromString:dateTimeStr];
        [formatter release];
        
        return date;
    } else {
        return nil;
    }
}

+ (NSString *)loadStringFromDict:(NSDictionary *)dict forKey:(CFStringRef)key {
    id obj = [dict objectForKey:(NSString *)key];
    if ([obj isKindOfClass:[NSString class]]) {
        return (NSString *)obj;
    } else {
        return nil;
    }
}

- (BOOL)readMetadataFromDict:(NSDictionary *)dict {
    BOOL success = NO;
    if (dict) {
        NSDictionary *EXIFDictionary = dict[(NSString *)kCGImagePropertyExifDictionary];
        if (EXIFDictionary) {
            [self loadExpoureTimeFromEXIF:EXIFDictionary];
            [self loadOriginalDateTimeFromEXIF:EXIFDictionary];
            [self loadScalingFromEXIF:EXIFDictionary];
            success = YES;
        }
        NSDictionary *TIFFDictionary = dict[(NSString *)kCGImagePropertyTIFFDictionary];
        if (TIFFDictionary) {
            [self loadGenericImageDescriptionFromTIFF:TIFFDictionary];
            success = YES;
        }
    }
    return success;
}

- (void)updateScaling:(float)scaling {
    self.scaling = scaling;
}

- (void)loadGenericImageDescriptionFromTIFF:(NSDictionary *)TIFFDictionary {
    NSString *microscopeManufacturer = [CZImageProperties loadStringFromDict:TIFFDictionary forKey:kCGImagePropertyTIFFMake];
    self.microscopeManufacturer = microscopeManufacturer;
    
    NSString *operatorName = [CZImageProperties loadStringFromDict:TIFFDictionary forKey:kCGImagePropertyTIFFArtist];
    self.operatorName = operatorName;
    
    NSString *combineImageDescription = [CZImageProperties loadStringFromDict:TIFFDictionary forKey:kCGImagePropertyTIFFImageDescription];
    [self parseFromImageDescription:combineImageDescription];
    
    NSString *modelName = [CZImageProperties loadStringFromDict:TIFFDictionary forKey:kCGImagePropertyTIFFModel];
    [self parseFromModelName:modelName];
}

// Parse From EXIF
- (void)loadOriginalDateTimeFromEXIF:(NSDictionary *)EXIFDictionary {
    NSDate *date = [CZImageProperties loadDateFromDict:EXIFDictionary
                                                forKey:kCGImagePropertyExifDateTimeOriginal];
    self.acquisitionDate = date;
}

- (void)loadExpoureTimeFromEXIF:(NSDictionary *)EXIFDictionary {
    CZImageChannelProperties *channelProperties = [[CZImageChannelProperties alloc] init];
    NSNumber *expoureTime = [CZImageProperties loadNumberFromDict:EXIFDictionary forKey:kCGImagePropertyExifExposureTime];
    channelProperties.exposureTimeInSeconds = expoureTime;
    self.channels = @[channelProperties];
    [channelProperties release];
}

- (void)saveScalingToEXIF:(NSMutableDictionary *)EXIFDictionary {
    if (self.scaling <= 0.0) {
        [EXIFDictionary removeObjectForKey:(NSString *)kCGImagePropertyExifFocalPlaneResolutionUnit];
        [EXIFDictionary removeObjectForKey:(NSString *)kCGImagePropertyExifFocalPlaneXResolution];
        [EXIFDictionary removeObjectForKey:(NSString *)kCGImagePropertyExifFocalPlaneYResolution];
    } else {
        float scalingInInch = kInchToUM / self.scaling;  // convert from um/pixel to DPI
        [CZImageProperties saveFloat:scalingInInch
                        toDictionary:EXIFDictionary
                              forKey:kCGImagePropertyExifFocalPlaneXResolution];
        
        [CZImageProperties saveFloat:scalingInInch
                        toDictionary:EXIFDictionary
                              forKey:kCGImagePropertyExifFocalPlaneYResolution];
        
        // @2 is inch
        [EXIFDictionary setObject:@2 forKey:(NSString *)kCGImagePropertyExifFocalPlaneResolutionUnit];
    }
}

- (void)loadScalingFromEXIF:(NSDictionary *)EXIFDictionary {
    NSNumber *value = [CZImageProperties loadNumberFromDict:EXIFDictionary
                                                     forKey:kCGImagePropertyExifFocalPlaneXResolution];
    if (value) {
        self.scaling = kInchToUM / [value floatValue];  // convert from DPI to um/pixel
    } else {
        self.scaling = 0;
    }
}

@end
