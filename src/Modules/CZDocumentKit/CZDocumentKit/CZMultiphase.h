//
//  CZMultiphase.h
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CZElement;
@class CZParticleCriteria;

@interface CZPhase : NSObject <NSCopying>

@property (nonatomic, copy) NSString *name;

/** minimum threshold, range in [0,256) */
@property (nonatomic, assign) NSUInteger minThreshold;

/** maximum threshold, range in [0,256) */
@property (nonatomic, assign) NSUInteger maxThreshold;

/** represent color of phase, default is red */
@property (nonatomic, retain) UIColor *color;

/** minimum area */
@property (nonatomic, assign) NSUInteger minimumArea;

/** real pixels count of phase */
@property (nonatomic, assign) NSUInteger area;

/** pixels count of phase */
@property (nonatomic, assign) NSUInteger originalArea;

/** pixels deducted because of minimum area */
@property (nonatomic, assign) NSUInteger areaDeducted;

@end

@interface CZMultiphase : NSObject <NSCopying, NSFastEnumeration>

/* Maximum phase count*/
@property (nonatomic, assign) NSUInteger maxCount;  // default is 4
@property (nonatomic, readonly) float remainingPercent;
@property (nonatomic, assign, getter = isRemainingEnabled) BOOL remainingEnabled;
@property (nonatomic, retain) UIColor *remainingColor;

/** pixel count of remain phase */
@property (nonatomic, assign) NSUInteger remainingArea;
/** valid image pixel count, exclude transparent pixel*/
@property (nonatomic, assign) NSUInteger imagePixelCount;

@property (nonatomic, assign, readonly) BOOL hasMinimumArea;

@property (nonatomic, assign, getter = isParticlesMode) BOOL particlesMode;

@property (nonatomic, retain) CZElement *roiRegion;

@property (nonatomic, retain) CZParticleCriteria *particleCriteria;

@property (nonatomic, copy) NSArray *cuttingPaths;  // array of CZParticleCuttingPath

- (NSUInteger)indexOfPhase:(CZPhase *)phase;
- (BOOL)addPhase:(CZPhase *)phase;
- (CZPhase *)phaseAtIndex:(NSUInteger)index;
- (void)removePhaseAtIndex:(NSUInteger)index;
- (void)insertPhase:(CZPhase *)phase atIndex:(NSUInteger)index;
- (NSUInteger)phaseCount;
- (void)swapPhaseAtIndex:(NSUInteger)index1 withIndex:(NSUInteger)index2;
- (void)movePhaseFromIndex:(NSUInteger)index toIndex:(NSUInteger)toIndex;

/** percent of phase at index, range in [0.0, 1.0]*/
- (float)percentOfPhaseAtIndex:(NSUInteger)index;

- (BOOL)setProcessingImage:(UIImage *)image;
/** @return NO, if processing image is not set yet; YES, if success. */
- (BOOL)updatePhaseAreas;

- (BOOL)isEqualMultiphase:(CZMultiphase *)targetMultiphase;

/** convert multiphase to ARGB mode color table, from 0~256, for color replacement*/
- (NSData *)convertToColorTable;

- (NSRange)validRangeWithGrayscaleValue:(NSUInteger)grayscaleValue
                                 margin:(NSUInteger)margin
                         excludingPhase:(CZPhase *)excludedPhase;

- (BOOL)hasOverlappingPhases;

@end
