//
//  CZGrainSizeResult+CZIMetadata.h
//  Matscope
//
//  Created by Ralph Jin on 8/22/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZGrainSizeResult.h"
#import <CZIKit/CZIKit.h>

@interface CZGrainSizeResult (CZIMetadata) <CZIMetadataNodeDecodable, CZIMetadataNodeEncodable>

@end
