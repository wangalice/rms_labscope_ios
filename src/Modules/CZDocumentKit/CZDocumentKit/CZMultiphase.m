//
//  CZMultiphase.m
//  Matscope
//
//  Created by Ralph Jin on 12/30/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMultiphase.h"
#import <Accelerate/Accelerate.h>
#import <CZAnnotationKit/CZAnnotationKit.h>

static const NSUInteger kMaxPhaseCount = 4;

@implementation CZPhase

- (void)dealloc {
    [_name release];
    [_color release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZPhase *phaseCopy = [[[self class] allocWithZone:zone]init];
    if (phaseCopy) {
        phaseCopy.name = self.name;
        phaseCopy.minThreshold = self.minThreshold;
        phaseCopy.maxThreshold = self.maxThreshold;
        phaseCopy.minimumArea = self.minimumArea;
        phaseCopy.originalArea = self.originalArea;
        phaseCopy.areaDeducted = self.areaDeducted;
        phaseCopy.color = self.color;
    }
    
    return phaseCopy;
}

- (BOOL)isEqualPhase:(id)object {
    if (![object isMemberOfClass:[CZPhase class]]) {
        return NO;
    }
    
    CZPhase *targetPhase = (CZPhase *)object;
    if (![self.name isEqualToString:targetPhase.name]) {
        return NO;
    }
    
    CZColor srcColor = CZColorFromUIColor(self.color);
    CZColor targetColor = CZColorFromUIColor(targetPhase.color);
    if (!CZColorEqualToColor(srcColor, targetColor)) {
        return NO;
    }
    
    if (self.minThreshold != targetPhase.minThreshold ||
        self.maxThreshold != targetPhase.maxThreshold ||
        self.minimumArea != targetPhase.minimumArea) {
        return NO;
    }
    
    return YES;
}

- (UIColor *)color {
    if (_color == nil) {
        return [UIColor redColor];
    } else {
        return _color;
    }
}

- (NSUInteger)area {
    return _originalArea - _areaDeducted;
}

- (void)setArea:(NSUInteger)area {
    _originalArea = area;
    _areaDeducted = 0;
}

@end

@interface CZMultiphase () {
    NSMutableArray *_phases;
    vImagePixelCount histogram[256];
}

@property (nonatomic, retain) CZPhase *remainPhase;

@end

@implementation CZMultiphase

- (id)init {
    self = [super init];
    if (self) {
        _maxCount = kMaxPhaseCount;
    }
    return self;
}

- (void)dealloc {
    [_phases release];
    [_remainPhase release];
    [_roiRegion release];
    [_particleCriteria release];
    [_cuttingPaths release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZMultiphase *multiphaseCopy = [[[self class] allocWithZone:zone]init];
    if (multiphaseCopy) {
        memcpy(multiphaseCopy->histogram, histogram, 256 * sizeof(vImagePixelCount));
        for (CZPhase *selfPhase in self.phases) {
            CZPhase *copyPhase = [selfPhase copy];
            [multiphaseCopy.phases addObject:copyPhase];
            [copyPhase release];
        }
        CZPhase *remainPhaseCopy = [self.remainPhase copy];
        multiphaseCopy.remainPhase = remainPhaseCopy;
        [remainPhaseCopy release];
        
        multiphaseCopy.remainingColor = self.remainingColor;
        multiphaseCopy.remainingEnabled = self.remainingEnabled;
        multiphaseCopy.maxCount = self.maxCount;
        multiphaseCopy.imagePixelCount = self.imagePixelCount;
        
        multiphaseCopy->_roiRegion = [_roiRegion copy];
        multiphaseCopy->_particlesMode = _particlesMode;
        multiphaseCopy->_particleCriteria = [_particleCriteria copy];
        
        // TODO: copy cutting path
        multiphaseCopy->_cuttingPaths = [_cuttingPaths copy];
    }
    
    return multiphaseCopy;
}

- (BOOL)isEqualMultiphase:(CZMultiphase *)targetMultiphase {
    if (self.phaseCount != targetMultiphase.phaseCount) {
        return NO;
    }
    
    if (self.remainingEnabled != targetMultiphase.remainingEnabled) {
        return NO;
    }
    
    for (int i = 0; i < self.phaseCount; i++) {
        CZPhase *srcPhase = [self phaseAtIndex:i];
        CZPhase *targetPhase = [targetMultiphase phaseAtIndex:i];
        if (![srcPhase isEqualPhase:targetPhase]) {
            return NO;
        }
    }
    
    if (self.roiRegion) {
        if (![self.roiRegion isEqualShapeTo:targetMultiphase.roiRegion]) {
            return NO;
        }
    } else {
        if (targetMultiphase.roiRegion) {
            return NO;
        }
    }
    
    if (_particlesMode != targetMultiphase->_particlesMode) {
        return NO;
    }
    
    if (self.particleCriteria) {
        if (![self.particleCriteria isEqual:targetMultiphase.particleCriteria]) {
            return NO;
        }
    } else {
        if (targetMultiphase.particleCriteria) {
            return NO;
        }
    }

    if (self.cuttingPaths.count != targetMultiphase.cuttingPaths.count) {
        return NO;
    }
    
    return YES;
}

- (NSUInteger)indexOfPhase:(CZPhase *)phase {
    return [self.phases indexOfObject:phase];
}

- (BOOL)addPhase:(CZPhase *)phase {
    if ([self phaseCount] < kMaxPhaseCount) {
        [self.phases addObject:phase];
        return YES;
    } else {
        return NO;
    }
}

- (CZPhase *)phaseAtIndex:(NSUInteger)index {
    return [self.phases objectAtIndex:index];
}

- (void)removePhaseAtIndex:(NSUInteger)index {
    [self.phases removeObjectAtIndex:index];
}

- (void)insertPhase:(CZPhase *)phase atIndex:(NSUInteger)index {
    [self.phases insertObject:phase atIndex:index];
}

- (NSUInteger)phaseCount {
    return [self.phases count];
}

- (void)swapPhaseAtIndex:(NSUInteger)index1 withIndex:(NSUInteger)index2 {
    id phase1 = [[self.phases objectAtIndex:index1] retain];
    id phase2 = [[self.phases objectAtIndex:index2] retain];
    
    [self.phases replaceObjectAtIndex:index1 withObject:phase2];
    [self.phases replaceObjectAtIndex:index2 withObject:phase1];
    
    [phase1 release];
    [phase2 release];
}

- (void)movePhaseFromIndex:(NSUInteger)index toIndex:(NSUInteger)toIndex {
    if (index == toIndex) {
        return;
    }
    
    id phase = [self.phases objectAtIndex:index];
    
    [phase retain];
    [self.phases removeObjectAtIndex:index];
    
    [self.phases insertObject:phase atIndex:toIndex];

    [phase release];
}

- (float)percentOfPhaseAtIndex:(NSUInteger)index {
    NSInteger sum = self.imagePixelCount;
    
    CZPhase *phase = [self phaseAtIndex:index];
    if (phase) {
        if (!self.remainingEnabled) {
            sum -= self.remainingArea;
        }
        return sum > 0 ? (float)phase.area / sum : 0.0;
    } else {
        return 0.0f;
    }
}

- (BOOL)setProcessingImage:(UIImage *)image {
    if (image == nil) {
        return NO;
    }
    
    size_t width = image.size.width;
    size_t height = image.size.height;
    void *rgbImage = calloc(width * height, sizeof(uint32_t));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImage, width, height, 8, width * 4, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextSetShouldAntialias(context, NO);
    CGContextSetAllowsAntialiasing(context, NO);
    
    if (self.roiRegion) {
        CGAffineTransform flipTransform = CGAffineTransformMake(1, 0, 0, -1, 0, height);
        CGContextConcatCTM(context, flipTransform);
        
        CGPathRef path = [self.roiRegion primaryNodePath];
        CGContextAddPath(context, path);
        CGContextClip(context);
        CGPathRelease(path);
        
        // restore CTM
        CGContextConcatCTM(context, flipTransform);
    }
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [image CGImage]);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    memset(histogram, 0, sizeof(histogram));
    
    // convert to grayscale one pixel by one pixel
    uint8_t gray, *pPixel = (uint8_t *)rgbImage;
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            if (*pPixel) {
                gray = *(pPixel + 1);  // use red channel only
                ++histogram[gray];
            }
            pPixel +=4;
        }
    }
    
    _imagePixelCount = 0;
    for (int i = 0; i < 256; i++) {
        _imagePixelCount += histogram[i];
    }
    
    free(rgbImage);
    
    return YES;
}

- (BOOL)updatePhaseAreas {
    NSUInteger i;
    for (i = 0; i < 256; i++) {
        if (histogram[i]) {
            break;
        }
    }
    
    if (i == 256) { // histogram is empty
        return NO;
    }
    
    NSUInteger remainCount = [self imagePixelCount];
    for (CZPhase *phase in self.phases) {
        NSUInteger phaseSum = 0;
        
        const NSUInteger min = MIN(256, phase.minThreshold);
        const NSUInteger max = MIN(256, phase.maxThreshold);
        
        for (NSUInteger i = min; i < max; i++) {
            phaseSum += histogram[i];
        }
        
        phase.originalArea = phaseSum;
        remainCount -= phaseSum;
    }
    
    self.remainPhase.area = remainCount;
    
    return YES;
}

/** convert multiphase to color table, from 0~255, for color replacement*/
- (NSData *)convertToColorTable {
    u_int8_t colorTable[256 * 4];
    memset(colorTable, 0, 256 * 4);
    
    for (CZPhase *phase in self.phases) {
        CGFloat r, g, b, a;
        [phase.color getRed:&r green:&g blue:&b alpha:&a];
        
        const NSUInteger min = MIN(256, phase.minThreshold);
        const NSUInteger max = MIN(256, phase.maxThreshold);

        for (NSUInteger i = min, j = i * 4; i < max; ++i, j += 4) {
            colorTable[j] = a * 255;
            colorTable[j + 1] = r * 255;
            colorTable[j + 2] = g * 255;
            colorTable[j + 3] = b * 255;
        }
    }

    return [NSData dataWithBytes:colorTable length:256 * 4];
}

- (NSRange)validRangeWithGrayscaleValue:(NSUInteger)grayscaleValue
                                 margin:(NSUInteger)margin
                         excludingPhase:(CZPhase *)excludedPhase {
    // Calculate min/max possible threshold for the selected phase.
    NSUInteger minPossibleThreshold = MAX(0, (int)grayscaleValue - (int)margin);
    NSUInteger maxPossibleThreshold = MIN(256, (int)grayscaleValue + (int)margin);
    
    for (CZPhase *phase in self.phases) {
        if (phase == excludedPhase) {
            continue;
        }
        
        if (grayscaleValue >= phase.minThreshold && grayscaleValue < phase.maxThreshold) {
            // Overlay is not allowed.
            return NSMakeRange(0, 0);
        }
        
        if (phase.maxThreshold <= grayscaleValue) {
            minPossibleThreshold = MAX(minPossibleThreshold, phase.maxThreshold);
        } else if (phase.minThreshold >= grayscaleValue) {
            maxPossibleThreshold = MIN(maxPossibleThreshold, phase.minThreshold);
        }
    }
    
    return NSMakeRange(minPossibleThreshold, maxPossibleThreshold - minPossibleThreshold);
}

- (BOOL)hasOverlappingPhases {
    for (int i = 0; i < self.phaseCount; ++i) {
        CZPhase *phaseA = [self phaseAtIndex:i];
        if (phaseA.minThreshold == phaseA.maxThreshold) {
            return YES;
        }
        for (int j = i + 1; j < self.phaseCount; ++j) {
            CZPhase *phaseB = [self phaseAtIndex:j];
            
            if (phaseB.minThreshold == phaseB.maxThreshold) {
                return YES;
            }
            
            if (phaseA.minThreshold <= phaseB.maxThreshold - 1 &&
                phaseB.minThreshold <= phaseA.maxThreshold - 1) {
                return YES;
            }
        }
    }
    return NO;
}

- (NSMutableArray *)phases {
    if (_phases == NULL) {
        _phases = [[NSMutableArray alloc] initWithCapacity:kMaxPhaseCount];
    }
    
    return _phases;
}

- (float)remainingPercent {
    if (self.imagePixelCount == 0 || !self.isRemainingEnabled) {
        return 0.0;
    } else {
        return (float)self.remainingArea / self.imagePixelCount;
    }
}

- (CZPhase *)remainPhase {
    if (_remainPhase == nil) {
        _remainPhase = [[CZPhase alloc] init];
        _remainPhase.color = [UIColor orangeColor];
    }
    
    return _remainPhase;
}

- (void)setRemainingColor:(UIColor *)remainingColor {
    self.remainPhase.color = remainingColor;
}

- (UIColor *)remainingColor {
    return self.remainPhase.color;
}

- (NSUInteger)remainingArea {
    NSUInteger totalDeductedArea = 0;
    for (CZPhase *phase in self.phases) {
        totalDeductedArea += phase.areaDeducted;
    }
    return self.remainPhase.area + totalDeductedArea;
}

- (void)setRemainingArea:(NSUInteger)remainingArea {
    self.remainPhase.area = remainingArea;
}

- (BOOL)hasMinimumArea {
    for (int i = 0; i < self.phaseCount; i++) {
        CZPhase *singlePhase = [self phaseAtIndex:i];
        if (singlePhase.minimumArea > 0) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - NSFastEnumerator methods

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id [])buffer count:(NSUInteger)len {
    return [self.phases countByEnumeratingWithState:state objects:buffer count:len];
}

@end
