//
//  CZImageProperties+CZIMetadata.m
//  Hermes
//
//  Created by Ralph Jin on 4/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZImageProperties+CZIMetadata.h"
#import "CZImagePropertiesPrivate.h"
#import <CZIKit/CZIKit.h>

@implementation CZImageProperties (CZIMetadata)

- (void)readFromCZIMetadata:(CZIMetadata *)metadata {
    CZIMetadataNode *microscopeNode = metadata.root.hardwareSetting.microscope;
    if (microscopeNode.isEmptyNode) {
        microscopeNode = metadata.document.imageDocument.hardwareSetting.microscope;
    }
    
    if (!microscopeNode.isEmptyNode) {
        self.microscopeName = microscopeNode.nodeAttribute.name;
        self.microscopeManufacturer = microscopeNode.manufacturer.stringValue;
    }
    
    CZIMetadataNode *imageNode = nil;
    CZIMetadataNode *informationNode = metadata.root.information;
    if (!informationNode.isEmptyNode) {
        // read microscope model and objective model
        self.objectiveModel = informationNode.instrument.objectives[0].nodeAttribute.name;
        self.microscopeModel = informationNode.instrument.microscopes[0].nodeAttribute.name;
        self.operatorName = informationNode.document.userName.stringValue;
        
        imageNode = informationNode.image;
    }
    
    if (!imageNode.isEmptyNode) {
        // read expsure time and creation date
        self.acquisitionDate = imageNode.acquisitionDateAndTime.dateValue;
        
        // read zoom and totalMagnification
        self.zoom = @(imageNode.microscopeSettings.zoomSettings.magnification.doubleValue);
        self.eyepieceMagnification = @(imageNode.microscopeSettings.eyepieceSettings.totalMagnification.doubleValue);
    }
    
    // read scaling
    CZIMetadataNode *scalingItemsNode = metadata.root.scaling.items;
    self.scaling = 1.0e6f * scalingItemsNode.distance.value.floatValue;
    
    if (!scalingItemsNode.pixel.value.isEmptyNode) {
        self.scalingInPixels = @(scalingItemsNode.pixel.value.integerValue);
    }
    
    // read camera model
    CZIMetadataNode *detectorNode = informationNode.instrument.detectors[0];
    if (detectorNode.nodeAttribute.name.length > 0) {
        self.cameraModel = detectorNode.nodeAttribute.name;
    } else {
        self.cameraModel = detectorNode.manufacturer.model.stringValue;
    }
    
    // read camera adapter
    CZIMetadataNode *autoscalingNode = metadata.root.scaling.autoScaling;
    if (!autoscalingNode.isEmptyNode) {
        self.cameraAdapter = @(autoscalingNode.cameraAdapterMagnification.doubleValue);
        
        if (self.zoom == nil && !autoscalingNode.optovarMagnification.isEmptyNode) {
            self.zoom = @(autoscalingNode.optovarMagnification.doubleValue);
        }
    }
    
    CZIMetadataNode *adapterModelNode = detectorNode.adapter.manufacturer.model;
    if (!adapterModelNode.isEmptyNode) {
        self.displayCameraAdapter = adapterModelNode.stringValue;
    }
    
    // read eyepiece
    self.eyepieceMagnification = @(metadata.root.hardwareSetting.eyePiece.magnification.doubleValue);
    CZIMetadataNode *eyepieceNode = metadata.root.hardwareSetting.eyePiece;
    if (!eyepieceNode.isEmptyNode) {
        self.eyepieceMagnification = @(eyepieceNode.magnification.doubleValue);
    }
    
    // read bit depth
    self.bitDepth = @(metadata.root.information.image.componentBitCount.integerValue);
}

@end
