//
//  CZElementText.h
//  Hermes
//
//  Created by Ralph Jin on 3/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"
#import "CZElementRotatable.h"

@interface CZElementText : CZElement<CZElementRotatable>

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) NSString *string;
@property (nonatomic, assign) NSString *fontFamily;
@property (nonatomic, assign, readonly) CGFloat fontSize;

- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;

@end
