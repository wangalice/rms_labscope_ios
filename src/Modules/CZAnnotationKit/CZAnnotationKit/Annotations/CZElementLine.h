//
//  CZElementLine.h
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"

@interface CZElementLine : CZElement

@property (nonatomic) CGPoint p1;
@property (nonatomic) CGPoint p2;

- (instancetype)initWithPoint:(CGPoint)p1 anotherPoint:(CGPoint)p2 NS_DESIGNATED_INITIALIZER;

@end
