//
//  CZElementCommon.h
//  Hermes
//
//  Created by Ralph Jin on 4/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

extern const CGFloat kCZMeasurementTextPadding;

@class CZNodeRectangle;
@class CZNodeArc;

@interface CZElementCommon : NSObject

+ (void)putMeasurement:(CZNodeRectangle *)node onLine:(CGPoint)pt1 secondPoint:(CGPoint)pt2;
+ (void)putMeasurement:(CZNodeRectangle *)node onLine:(CGPoint)pt1 secondPoint:(CGPoint)pt2 distance:(CGFloat)distance;

+ (void)putMeasurement:(CZNodeRectangle *)node onArc:(CZNodeArc *)arc startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

/** length of cubic bezier segments' total length
 * @param controlPoints control points of bezier segements, last point of segment share with first point of next segment
 * @param pointsCount points count of |controlPoints|
 */
+ (double)lengthOfCubicBezierSegments:(const CGPoint *)controlPoints pointsCount:(NSUInteger)pointsCount;

/** area of cubic bezier segments' total area
 * @param controlPoints control points of bezier segements, last point of segment share with first point of next segment
 * @param pointsCount points count of |controlPoints|
 */
+ (double)areaOfCubicBezierSegments:(const CGPoint *)controlPoints pointsCount:(NSUInteger)pointsCount;

@end

typedef double (^CZMathFunctionBlock)(double x);

/**
 * Romberg method, calculate integration of the function |f|; see http://en.wikipedia.org/wiki/Romberg%27s_method for detail
 * @param a the start x of integration;
 * @param b the end x of integration;
 * @param e epsilon, precision required, e.g. 1e-5;
 * @param f the math function to integrate;
 * @return the result of the integration.
 */
double Romberg(double a, double b, double e, CZMathFunctionBlock f);

// One Dimension Fast Fourier/Cosine/Sine Transform

/** Complex Discrete Fourier Transform
 * -------- Complex DFT (Discrete Fourier Transform) --------
 * <case1>
 * X[k] = sum_j=0^n-1 x[j]*exp(2*pi*i*j*k/n), 0<=k<n
 * <case2>
 * X[k] = sum_j=0^n-1 x[j]*exp(-2*pi*i*j*k/n), 0<=k<n
 * (notes: sum_j=0^n-1 is a summation from j=0 to n-1)
 * [usage]
 * <case1>
 * cdft(2*n, cos(M_PI/n), sin(M_PI/n), a);
 * <case2>
 * cdft(2*n, cos(M_PI/n), -sin(M_PI/n), a);
 *
 * @param n 2*n          :data length (int) n >= 1, n = power of 2
 * @param a[0...2*n-1] :input/output data (double *)
 * input data
 * a[2*j] = Re(x[j]), a[2*j+1] = Im(x[j]), 0<=j<n
 * output data
 * a[2*k] = Re(X[k]), a[2*k+1] = Im(X[k]), 0<=k<n
 * [remark]
 * Inverse of
 * cdft(2*n, cos(M_PI/n), -sin(M_PI/n), a);
 * is
 * cdft(2*n, cos(M_PI/n), sin(M_PI/n), a);
 * for (j = 0; j <= 2 * n - 1; j++) {
 * a[j] *= 1.0 / n;
 * }
 */
void cdft(int n, double wr, double wi, double *a);

/** Real Discrete Fourier Transform
 * -------- Real DFT / Inverse of Real DFT --------
 * <case1> RDFT
 * R[k] = sum_j=0^n-1 a[j]*cos(2*pi*j*k/n), 0<=k<=n/2
 * I[k] = sum_j=0^n-1 a[j]*sin(2*pi*j*k/n), 0<k<n/2
 * <case2> IRDFT (excluding scale)
 * a[k] = R[0]/2 + R[n/2]/2 +
 * sum_j=1^n/2-1 R[j]*cos(2*pi*j*k/n) +
 * sum_j=1^n/2-1 I[j]*sin(2*pi*j*k/n), 0<=k<n
 * [usage]
 * <case1>
 * rdft(n, cos(M_PI/n), sin(M_PI/n), a);
 * <case2>
 * rdft(n, cos(M_PI/n), -sin(M_PI/n), a);
 *
 * @param n            :data length (int) n >= 2, n = power of 2
 * @param a[0...n-1]   :input/output data (double *)
 * <case1>
 * output data
 * a[2*k] = R[k], 0<=k<n/2
 * a[2*k+1] = I[k], 0<k<n/2
 * a[1] = R[n/2]
 * <case2>
 * input data
 * a[2*j] = R[j], 0<=j<n/2
 * a[2*j+1] = I[j], 0<j<n/2
 * a[1] = R[n/2]
 * [remark]
 * Inverse of
 * rdft(n, cos(M_PI/n), sin(M_PI/n), a);
 * is
 * rdft(n, cos(M_PI/n), -sin(M_PI/n), a);
 * for (j = 0; j <= n - 1; j++) {
 * a[j] *= 2.0 / n;
 * }
 */
void rdft(int n, double wr, double wi, double *a);

/** Discrete Cosine Transform
 * -------- DCT (Discrete Cosine Transform) / Inverse of DCT --------
 * <case1> IDCT (excluding scale)
 * C[k] = sum_j=0^n-1 a[j]*cos(pi*j*(k+1/2)/n), 0<=k<n
 * <case2> DCT
 * C[k] = sum_j=0^n-1 a[j]*cos(pi*(j+1/2)*k/n), 0<=k<n
 * [usage]
 * <case1>
 * ddct(n, cos(M_PI/n/2), sin(M_PI/n/2), a);
 * <case2>
 * ddct(n, cos(M_PI/n/2), -sin(M_PI/n/2), a);
 *
 * @param n            :data length (int) n >= 2, n = power of 2
 * @param a[0...n-1]   :input/output data (double *)
 * output data
 * a[k] = C[k], 0<=k<n
 * [remark]
 * Inverse of
 * ddct(n, cos(M_PI/n/2), -sin(M_PI/n/2), a);
 * is
 * a[0] *= 0.5;
 * ddct(n, cos(M_PI/n/2), sin(M_PI/n/2), a);
 * for (j = 0; j <= n - 1; j++) {
 * a[j] *= 2.0 / n;
 * }
 */
void ddct(int n, double wr, double wi, double *a);

/** Discrete Sine Transform
 * -------- DST (Discrete Sine Transform) / Inverse of DST --------
 * <case1> IDST (excluding scale)
 * S[k] = sum_j=1^n A[j]*sin(pi*j*(k+1/2)/n), 0<=k<n
 * <case2> DST
 * S[k] = sum_j=0^n-1 a[j]*sin(pi*(j+1/2)*k/n), 0<k<=n
 * [usage]
 * <case1>
 * ddst(n, cos(M_PI/n/2), sin(M_PI/n/2), a);
 * <case2>
 * ddst(n, cos(M_PI/n/2), -sin(M_PI/n/2), a);
 *
 * @param n            :data length (int) n >= 2, n = power of 2
 * @param a[0...n-1]   :input/output data (double *)
 * <case1>
 * input data
 * a[j] = A[j], 0<j<n
 * a[0] = A[n]
 * output data
 * a[k] = S[k], 0<=k<n
 * <case2>
 * output data
 * a[k] = S[k], 0<k<n
 * a[0] = S[n]
 * [remark]
 * Inverse of
 * ddst(n, cos(M_PI/n/2), -sin(M_PI/n/2), a);
 * is
 * a[0] *= 0.5;
 * ddst(n, cos(M_PI/n/2), sin(M_PI/n/2), a);
 * for (j = 0; j <= n - 1; j++) {
 * a[j] *= 2.0 / n;
 */
void ddst(int n, double wr, double wi, double *a);

/** Cosine Transform of RDFT (Real Symmetric DFT)
 * -------- Cosine Transform of RDFT (Real Symmetric DFT) --------
 * C[k] = sum_j=0^n a[j]*cos(pi*j*k/n), 0<=k<=n
 * [usage]
 * dfct(n, cos(M_PI/n), sin(M_PI/n), a);
 * @param n            :data length - 1 (int) n >= 2, n = power of 2
 * @param a[0...n]     :input/output data (double *)
 *                      output data
 *                      [k] = C[k], 0<=k<=n
 * [remark]
 * Inverse of
 * a[0] *= 0.5;
 * a[n] *= 0.5;
 * dfct(n, cos(M_PI/n), sin(M_PI/n), a);
 * is
 * a[0] *= 0.5;
 * a[n] *= 0.5;
 * dfct(n, cos(M_PI/n), sin(M_PI/n), a);
 * for (j = 0; j <= n; j++) {
 *     a[j] *= 2.0 / n;
 * }
 */
void dfct(int n, double wr, double wi, double *a);

/** Sine Transform of RDFT (Real Anti-symmetric DFT)
 * -------- Sine Transform of RDFT (Real Anti-symmetric DFT) --------
 * S[k] = sum_j=1^n-1 a[j]*sin(pi*j*k/n), 0<k<n
 * [usage]
 * dfst(n, cos(M_PI/n), sin(M_PI/n), a);
 *
 * @param n            :data length + 1 (int) n >= 2, n = power of 2
 * @param a[0...n-1]   :input/output data (double *)
 *                      output data a[k] = S[k], 0<k<n (a[0] is used for work area)
 * [remark]
 * Inverse of
 * dfst(n, cos(M_PI/n), sin(M_PI/n), a);
 * is
 * dfst(n, cos(M_PI/n), sin(M_PI/n), a);
 * for (j = 1; j <= n - 1; j++) {
 *     a[j] *= 2.0 / n;
 * }
 */
void dfst(int n, double wr, double wi, double *a);

/** initialization of w for Clenshaw-Curtis-Quadrature */
void ClenshawCurtisInit(int lenw, double *w);

/**
 * Clenshaw-Curtis-Quadrature
 * Numerical Automatic Integrator
 * method    : Chebyshev Series Expansion
 * dimension : one
 * table     : use
 * function
 * ClenshawCurtis : integrator of f(x) over [a,b].
 *
 * @param lenw      : (length of w[]) - 1 (int)
 * @param w         : work area and weights of the quadrature formula, w[0...lenw] (double *)
 * @param f         : integrand f(x) (double (*f)(double))
 * @param a         : lower limit of integration (double)
 * @param b         : upper limit of integration (double)
 * @param eps       : relative error requested (double)
 * @param i         : approximation to the integral (double *)
 * @param err       : estimate of the absolute error (double *)
 */
void ClenshawCurtis(CZMathFunctionBlock f, double a, double b, double eps,
                  int lenw, double *w, double *i, double *err);
