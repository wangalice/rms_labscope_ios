//
//  CZElementGroup.h
//  Matscope
//
//  Created by Ralph Jin on 1/9/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZElementLayer;
@class CZElement;

/** class of element group, you can enuerate elements with NSFastEnumeration.*/
@interface CZElementGroup : NSObject<NSFastEnumeration>

/*! parent layer of this group.*/
@property (nonatomic, assign, readonly) CZElementLayer *parentLayer;

- (void)removeFromParentLayer;

/*! index of group in parenet layer*/
- (NSUInteger)index;

/*! Sub elements' count. */
- (NSUInteger)elementCount;

- (CZElement *)elementAtIndex:(NSUInteger)index;

- (NSUInteger)indexOfElement:(CZElement *)element;

/*! Add element to the annotation Layer. */
- (void)addElement:(CZElement *)element;

- (void)notifyDidChange;

@end
