//
//  CZElementLayer+CSV.h
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayer.h"

@interface CZElementLayer (CSV)

- (BOOL)writeMeasurementsToCSVFile:(NSString *)filePath error:(NSError **)error;

@end
