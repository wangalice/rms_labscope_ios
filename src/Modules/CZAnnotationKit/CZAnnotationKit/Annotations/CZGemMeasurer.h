//
//  CZGemMeasure.h
//  Hermes
//
//  Created by Ralph Jin on 8/19/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface CZGemMeasure : NSObject

/*
 * Sets the polygon as the region to be measured.
 * 
 * @param polygonCoordinates, The pointer to the polygon points coordinates.
 * @param pointsCount, The polygon points count.
 */
- (void)setRegion:(const CGFloat *)polygonCoordinates pointsCount:(long)pointsCount;

/*
 * Sets the cirle as the region to be measured.
 *
 * @param centerX, The X coordinate of the circle center.
 * @param centerY, The Y coordinate of the circle center.
 * @param radius, The circle radius. 
 */
- (void)setCircle:(double)centerX centerY:(double)centerY radius:(double)radius;

/*
 * Calculates the area of the specified region.
 * @return The area in pixels.
 */
- (double)area;

/*
 * Calculates the perimeter of the specified region.
 * @return The perimeter in pixels.
 */
- (double)perimeter;

@end
