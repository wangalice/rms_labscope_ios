//
//  CZElementDisconnectAngle.h
//  Hermes
//
//  Created by Ralph Jin on 7/01/13.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiPoint.h"

//         / A
//        /
//       /
//     B
//
//   D -------- C

@interface CZElementDisconnectedAngle : CZElementMultiPoint

@property (nonatomic, assign) CGPoint ptA;
@property (nonatomic, assign) CGPoint ptB;
@property (nonatomic, assign) CGPoint ptC;
@property (nonatomic, assign) CGPoint ptD;

@end
