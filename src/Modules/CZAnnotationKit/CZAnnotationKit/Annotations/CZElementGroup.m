//
//  CZElementGroup.m
//  Matscope
//
//  Created by Ralph Jin on 1/9/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZElementGroup.h"
#import "CZElement.h"
#import "CZElementLayer.h"

@interface CZElement (CZElementGroup)
@property (nonatomic, assign, readwrite) CZElementGroup *parentGroup;
@end

@implementation CZElement (CZElementGroup)
@dynamic parentGroup;
@end

@interface CZElementLayer (CZElementGroup)
- (void)removeGroup:(CZElementGroup *)group;
@end

@interface CZElementGroup ()

@property (nonatomic, retain) NSMutableArray *elements;
@property (nonatomic, assign, readwrite) CZElementLayer *parentLayer;

@end

@implementation CZElementGroup

- (void)dealloc {
    [self removeAllElements];
    [_elements release];
    [super dealloc];
}

- (void)removeFromParentLayer {
    [self retain];
    
    [self.parentLayer removeGroup:self];
    
    if (self.parentLayer == nil) {
        [self removeAllElements];
    }
    
    [self release];
}

- (NSUInteger)index {
    return [self.parentLayer indexOfGroup:self];
}

- (NSUInteger)elementCount {
    return [self.elements count];
}

- (CZElement *)elementAtIndex:(NSUInteger)index {
    return [self.elements objectAtIndex:index];
}

- (NSUInteger)indexOfElement:(CZElement *)element {
    return [self.elements indexOfObject:element];
}

- (void)addElement:(CZElement *)element {
    if (element == nil ||
        self == element.parentGroup ||
        element.parent != self.parentLayer) {
        NSLog(@"failed to add element to group");
        return;
    }
    
    [element removeFromGroup];
    
    [self.elements addObject:element];
    element.parentGroup = self;
    
    [self notifyDidChange];
}

#pragma mark - Lazy-load properties

- (NSMutableArray *)elements {
    if (_elements == nil) {
        _elements = [[NSMutableArray alloc] init];
    }
    
    return _elements;
}

#pragma mark - NSFastEnumeration protocol

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id [])buffer count:(NSUInteger)len {
    return [self.elements countByEnumeratingWithState:state objects:buffer count:len];
}

#pragma mark - private

- (void)removeAllElements {
    NSArray *tempArray = [_elements copy];
    for (CZElement *element in tempArray) {
        [element removeFromGroup];
    }
    [tempArray release];
}

- (void)notifyDidChange {
    if ([_parentLayer.delegate respondsToSelector:@selector(layer:didChangeGroup:)]) {
        [_parentLayer.delegate layer:_parentLayer didChangeGroup:self];
    }
}

@end

#pragma mark - implement friend class method

@implementation CZElementGroup (CZElement)

- (void)removeElement:(CZElement *)element {
    [self retain];
    
    element.parentGroup = nil;
    [self.elements removeObject:element];
    
    // destroy self if no more than two element exist.
    if (self.elements.count < 2) {
        [self removeFromParentLayer];
    }
    
    [self notifyDidChange];
    
    [self release];
}

@end
