//
//  CZElementDisconnectAngle.m
//  Hermes
//
//  Created by Ralph Jin on 7/01/13.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZElementDisconnectAngle.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodeLineSegments.h"
#import "CZNodeRectangle.h"
#import "CZNodeArc.h"
#import "CZFeature.h"
#import "CZElementCommon.h"
#import "CZElementLayer.h"

@interface CZElementDisconnectedAngle ()

@property (nonatomic, assign, readonly) CZNodeLineSegments *lineNode;
@property (nonatomic, retain) CZNodeArc *arcNode;
@property (nonatomic, retain) CZNodeLineSegments *extendLine;

- (void)updateFeatures;

@end

@implementation CZElementDisconnectedAngle

+ (Class)nodeClass {
    return [CZNodeLineSegments class];
}

+ (NSUInteger)minimumPointsCount {
    return 4;
}

- (instancetype)init {
    self = [super initWithPointsCount:4];
    if (self) {
        _arcNode = [[CZNodeArc alloc] init];
        _extendLine = [[CZNodeLineSegments alloc] init];
        _extendLine.dash = YES;
    }
    
    return self;
}

- (void)dealloc {
    [_arcNode release];
    [_extendLine release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementDisconnectedAngle *newAngle = [super copyWithZone:zone];
    
    if (newAngle) {
        CZNode *primaryNode = [newAngle primaryNode];
        if ([primaryNode subNodeCount] > 0) {
            newAngle.arcNode = (CZNodeArc *)[primaryNode subNodeAtIndex:0];
        } else {
            newAngle->_arcNode = [[CZNodeArc alloc] init];
        }
        
        if ([primaryNode subNodeCount] > 1) {
            newAngle.extendLine = (CZNodeLineSegments *)[primaryNode subNodeAtIndex:1];
        } else {
            newAngle->_extendLine = [[CZNodeLineSegments alloc] init];
            newAngle->_extendLine.dash = YES;
        }
    }
    
    return newAngle;
}

- (CZNodeLineSegments *)lineNode {
    return (CZNodeLineSegments *)[self primaryNode];
}

- (CGPoint)ptA {
    return [self pointAtIndex:0];
}

- (void)setPtA:(CGPoint)ptA {
    [self setPoint:ptA atIndex:0];
    self.featureValidated = NO;
}

- (CGPoint)ptB {
    return [self pointAtIndex:1];
}

- (void)setPtB:(CGPoint)ptB {
    [self setPoint:ptB atIndex:1];
    self.featureValidated = NO;
}

- (CGPoint)ptC {
    return [self pointAtIndex:2];
}

- (void)setPtC:(CGPoint)ptC {
    [self setPoint:ptC atIndex:2];
    self.featureValidated = NO;
}

- (CGPoint)ptD {
    return [self pointAtIndex:3];
}

- (void)setPtD:(CGPoint)ptA {
    [self setPoint:ptA atIndex:3];
    self.featureValidated = NO;
}

#pragma mark - Override CZElement

- (NSUInteger)precision {
    // Angle element's precision is no more than 2.
    NSUInteger precision = [super precision];
    precision = MIN(precision, 2);
    return precision;
}

- (CALayer *)newLayer {
    [self updateFeatures];
    
    self.lineNode.lineWidth = [self suggestLineWidth];
    _arcNode.lineWidth = self.lineNode.lineWidth;
    _arcNode.strokeColor = self.lineNode.strokeColor;
    _extendLine.lineWidth = self.lineNode.lineWidth;
    _extendLine.strokeColor = self.lineNode.strokeColor;

    return [super newLayer];
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];

    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    if ([self.extendLine pointsCount] >= 3 && self.arcNode.parentNode) {
        CZFeature *feature = self.features.count > 0 ? self.features[0] : nil;
        NSString *angleString = [feature toLocalizedStringWithPrecision:[self precision] showType:NO];
        CZNodeRectangle *textBox = [self newMeasurementNodeWithString:angleString];
        
        CZNodeShape *lineNode = (CZNodeShape *)[self primaryNode];
        CGRect lineNodeBox = CGPathGetBoundingBox([lineNode relativePath]);
        CGPoint offset = lineNodeBox.origin;
        
        CGPoint pt1 = [self.extendLine pointAtIndex:0];
        CGPoint pt2 = [self.extendLine pointAtIndex:2];
        
        [CZElementCommon putMeasurement:textBox
                                  onArc:self.arcNode
                             startPoint:pt1
                               endPoint:pt2];
        
        textBox.x = textBox.x + offset.x;
        textBox.y = textBox.y + offset.y;
        
        layer = [textBox newLayer];
        [textBox release];
        return layer;
    } else {
        return [[CAShapeLayer alloc] init];  // empty layer for placeholder
    }
}

+ (NSUInteger)featureCount {
    return 1;
}

- (CGPoint)measurementTextPosition {
    CGPoint pt;
    
    CALayer *layer = [self newMeasurementTextLayer];
    if (layer) {
        pt = layer.frame.origin;
        [layer release];
    } else {
        CGPoint topLeft = CGPointMake(CGFLOAT_MAX, CGFLOAT_MAX);
        
        for (NSUInteger i = 0; i < [self pointsCount]; ++i) {
            CGPoint point = [self pointAtIndex:i];
            topLeft.x = MIN(topLeft.x, point.x);
            topLeft.y = MIN(topLeft.y, point.y);
        }
        
        pt = topLeft;
    }
    return pt;
}

- (BOOL)containsPoint:(CGPoint)p {
    [self updateFeatures];
    
    if (self.arcNode.parentNode) {
        CZNodeShape *lineNode = (CZNodeShape *)[self primaryNode];
        CGRect lineNodeBox = CGPathGetBoundingBox([lineNode relativePath]);
        CGPoint offset = lineNodeBox.origin;

        p.x -= offset.x;
        p.y -= offset.y;
        
        CGPoint center = CGPointMake(self.arcNode.cx, self.arcNode.cy);
        return CZPieContainsPoint(p, self.arcNode.startAngle, self.arcNode.endAngle, center, self.arcNode.radius);
    } else {
        return NO;
    }
}

- (NSString *)type {
    return @"ANNOTATION_TYPE_CZELEMENTANGLE";
}

#pragma mark - Private methods

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    [self updateAuxNodes];
    
    CGFloat angle = self.arcNode.endAngle - self.arcNode.startAngle;
    angle = CZNormalizeAngle(angle);
    
    [self removeAllFeatures];
    
    CZFeature *feature = [[CZFeature alloc] initWithValue:(angle * 180 / M_PI) unit:kCZFeatureUnitDegree type:kCZFeatureTypeAngle];
    [self appendFeature:feature];
    [feature release];
}

- (void)updateAuxNodes {
    CGPoint ptP = CZCalcCrossPoint(self.ptA, self.ptB, self.ptC, self.ptD);
    BOOL hasCross = isnormal(ptP.x) && isnormal(ptP.y);
    
    if (hasCross) {
        [self.primaryNode addSubNode:_arcNode];
        [self.primaryNode addSubNode:_extendLine];
        
        // choose long vector from (ptP->ptA) and (ptP->ptB)
        BOOL isDefaultVecLong = YES;
        CGPoint vec1 = CGPointMake(self.ptA.x - ptP.x, self.ptA.y - ptP.y);
        if (vec1.x == 0.0 && vec1.y == 0.0) {
            vec1.x = 0.01;
        }
        CGFloat radius1 = sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
        
        CGPoint vec1_1 = CGPointMake(self.ptB.x - ptP.x, self.ptB.y - ptP.y);
        if (vec1_1.x == 0.0 && vec1_1.y == 0.0) {
            vec1_1.x = 0.01;
        }
        CGFloat radius1_1 = sqrt(vec1_1.x * vec1_1.x + vec1_1.y * vec1_1.y);
        if (radius1_1 > radius1) {
            radius1 = radius1_1;
            vec1 = vec1_1;
            isDefaultVecLong = NO;
        }
        
        CGPoint pt1 = isDefaultVecLong ? self.ptA : self.ptB;
        CGPoint pt2 = isDefaultVecLong ? self.ptC : self.ptD;
        CGPoint vec2 = CGPointMake(pt2.x - ptP.x, pt2.y - ptP.y);
        if (vec2.x == 0.0 && vec2.y == 0.0) {
            vec2.x = 0.01;
        }
        
        // position arc
        CGFloat radius2 = sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
        _arcNode.radius = MIN(radius1, radius2);
        _arcNode.startAngle = atan2(vec1.y, vec1.x);
        _arcNode.endAngle = atan2(vec2.y, vec2.x);
        
        // swap start angle and end angle if angle is greater than 180 degrees
        CGFloat angle =_arcNode.endAngle - _arcNode.startAngle;
        angle = CZNormalizeAngle(angle);
        if (angle >= M_PI) {
            CGFloat tempAngle = _arcNode.startAngle;
            _arcNode.startAngle = _arcNode.endAngle;
            _arcNode.endAngle = tempAngle;
        }
        
        CZNodeShape *lineNode = (CZNodeShape *)[self primaryNode];
        CGRect lineNodeBox = CGPathGetBoundingBox([lineNode relativePath]);
        CGPoint offset = lineNodeBox.origin;
        _arcNode.cx = ptP.x - offset.x;
        _arcNode.cy = ptP.y - offset.y;
        
        // position extend lines
        CGRect validBoundary = self.parent.frame;
        validBoundary = CGRectInset(validBoundary, -validBoundary.size.width, -validBoundary.size.height);
        CGRect boundingBox = CGPathGetBoundingBox([(CZNodeShape *)[self primaryNode] relativePath]);
        boundingBox = CGRectInset(boundingBox, -1, -1);
        validBoundary = CGRectUnion(boundingBox, validBoundary);
        
        [_extendLine removeAllPoints];
        if (CGRectContainsPoint(validBoundary, ptP)) {
            [_extendLine appendPoint:CGPointMake(pt1.x - offset.x, pt1.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(ptP.x - offset.x, ptP.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(pt2.x - offset.x, pt2.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(ptP.x - offset.x, ptP.y - offset.y)];
        } else {
            // calculate cross points of line1(ptP, pt1), line2(ptP, pt2) with boundary
            CGPoint ptP1, ptP2;
            ptP1 = [CZElementDisconnectedAngle cullLineStartPoint:pt1 endPoint:ptP inBoundary:validBoundary];
            ptP2 = [CZElementDisconnectedAngle cullLineStartPoint:pt2 endPoint:ptP inBoundary:validBoundary];
            
            [_extendLine appendPoint:CGPointMake(pt1.x - offset.x, pt1.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(ptP1.x - offset.x, ptP1.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(pt2.x - offset.x, pt2.y - offset.y)];
            [_extendLine appendPoint:CGPointMake(ptP2.x - offset.x, ptP2.y - offset.y)];
        }
    } else {
        [_arcNode removeFromParent];
        [_extendLine removeFromParent];
    }
}

/** Assume ptStart is inside boundary, while ptEnd is outside. Cull line with boundary.
 * @return cross point
 */
+ (CGPoint)cullLineStartPoint:(CGPoint)ptStart endPoint:(CGPoint)ptEnd inBoundary:(CGRect)boundary {
    NSAssert(CGRectContainsPoint(boundary, ptStart), @"ptStart is not in boundary!");
    
    if (!CGRectContainsPoint(boundary, ptStart)) {
        return ptEnd;
    }
    
    CGFloat left = CGRectGetMinX(boundary);
    CGFloat right = CGRectGetMaxX(boundary);
    CGFloat top = CGRectGetMinY(boundary);
    CGFloat bottom = CGRectGetMaxY(boundary);
    
    CGPoint result = ptEnd;
    CGFloat tx = CGFLOAT_MAX, ty = CGFLOAT_MAX;
    if (ptEnd.x < left) {
        tx = (ptStart.x - left) / (ptStart.x - ptEnd.x);
        result.x = left;
    } else if (ptEnd.x > right) {
        tx = (ptStart.x - right) / (ptStart.x - ptEnd.x);
        result.x = right;
    }
    if (!isnormal(tx)) {
        tx = CGFLOAT_MAX;
    }
    
    if (ptEnd.y < top) {
        ty = (ptStart.y - top) / (ptStart.y - ptEnd.y);
        result.y = top;
    } else if (ptEnd.y > bottom) {
        ty = (ptStart.y - bottom) / (ptStart.y - ptEnd.y);
        result.y = bottom;
    }
    if (!isnormal(ty)) {
        ty = CGFLOAT_MAX;
    }
    
    if (tx < ty) {
        result.y = ptStart.y - tx * (ptStart.y - ptEnd.y);
    } else {
        result.x = ptStart.x - ty * (ptStart.x - ptEnd.x);
    }
    
    return result;
}

@end
