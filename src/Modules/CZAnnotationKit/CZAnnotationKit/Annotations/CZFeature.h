//
//  CZFeature.h
//  Hermes
//
//  Created by Ralph Jin on 1/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! const type of feature*/
extern NSString * const kCZFeatureTypeArea;
extern NSString * const kCZFeatureTypeDistance;
extern NSString * const kCZFeatureTypeCenterX;
extern NSString * const kCZFeatureTypeCenterY;
extern NSString * const kCZFeatureTypeWidth;
extern NSString * const kCZFeatureTypeHeight;
extern NSString * const kCZFeatureTypeAngle;
extern NSString * const kCZFeatureTypeDiameter;
extern NSString * const kCZFeatureTypePerimeter;
extern NSString * const kCZFeatureTypeNumber;

extern NSString * const kCZFeatureUnitPixel;
extern NSString * const kCZFeatureUnitCount;
extern NSString * const kCZFeatureUnitMM;
extern NSString * const kCZFeatureUnitUM;
extern NSString * const kCZFeatureUnitMil;
extern NSString * const kCZFeatureUnitInch;
extern NSString * const kCZFeatureUnitMM2;
extern NSString * const kCZFeatureUnitUM2;
extern NSString * const kCZFeatureUnitMil2;
extern NSString * const kCZFeatureUnitInch2;
extern NSString * const kCZFeatureUnitDegree;
extern NSString * const kCZFeatureUnitRadiam;

@interface CZFeature : NSObject


/*! translatable unit string*/
@property (nonatomic, readonly, copy) NSString *unit;

@property (nonatomic, readonly, copy) NSString *type;

@property (nonatomic, readonly) float value;

- (instancetype)initWithValue:(float)value unit:(NSString *)unit type:(NSString *)type NS_DESIGNATED_INITIALIZER;

/** return localized string (combine type, value and unit) with specific precision.*/
- (NSString *)toLocalizedStringWithPrecision:(NSUInteger)precision;
- (NSString *)toLocalizedStringWithPrecision:(NSUInteger)precision showType:(BOOL)showType;

/** return localized formatted value string with specific precision.*/
- (NSString *)localizedValueStringWithPrecision:(NSUInteger)precision;

/** return formatted value string with specific precision.*/
- (NSString *)valueStringWithPrecision:(NSUInteger)precision;

@end
