//
//  CZElementCommon.m
//  Hermes
//
//  Created by Ralph Jin on 4/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementCommon.h"
#import "CZNodeGeom.h"
#import "CZNodeRectangle.h"
#import "CZNodeArc.h"

const CGFloat kCZMeasurementTextPadding = 6;

static const CGFloat kArcMeasurementGap = 3.0;  // space between arc and measurement text

@implementation CZElementCommon

- (instancetype)init {
    [self release];
    return nil;
}

+ (void)putMeasurement:(CZNodeRectangle *)node onLine:(CGPoint)pt1 secondPoint:(CGPoint)pt2 {
    [CZElementCommon putMeasurement:node onLine:pt1 secondPoint:pt2 distance:kCZMeasurementTextPadding];
}

+ (void)putMeasurement:(CZNodeRectangle *)node onLine:(CGPoint)pt1 secondPoint:(CGPoint)pt2 distance:(CGFloat)distance {
    BOOL isBelow = distance > 0.0;
    distance = fabs(distance);
    distance = MAX(distance, kCZMeasurementTextPadding);
    
    // Calculate rotate angle and offset in y axis
    CGFloat offset = node.height * 0.5 + distance;
    if (!isBelow) {
        offset = -offset;
    }

    // Calculate dx, dy and convert them to 1st or 4th quadrant
    CGPoint po, pa;
    if (pt1.x > pt2.x) {
        po = pt2;
        pa = pt1;
    } else if (pt1.x == pt2.x) {  // when line is vertical
        if (pt1.y > pt2.y) {
            po = pt1;
            pa = pt2;
        } else {
            po = pt2;
            pa = pt1;
        }
    } else {
        po = pt1;
        pa = pt2;
    }
    
    CGFloat angle = atan2(pa.y - po.y, pa.x - po.x);
    node.rotateAngle = angle;

    // Calculate the center of the node
    CGPoint centerPoint;
    centerPoint.x = (pt1.x + pt2.x) * 0.5;
    centerPoint.y = (pt1.y + pt2.y) * 0.5;
    centerPoint = CZCalcOuterPoint(po, centerPoint, offset);

    // Calculate the top left of the node
    node.x = centerPoint.x - node.width * 0.5;
    node.y = centerPoint.y - node.height * 0.5;
}

+ (void)putMeasurement:(CZNodeRectangle *)textBox onArc:(CZNodeArc *)arc startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CGPoint ptP = CGPointMake(arc.cx, arc.cy);
    CGPoint ptA = startPoint;
    CGPoint ptB = endPoint;
    
    CGPoint vec1 = CGPointMake(ptA.x - ptP.x, ptA.y - ptP.y);
    CGPoint vec2 = CGPointMake(ptB.x - ptP.x, ptB.y - ptP.y);
    
    if (vec1.x == 0.0 && vec1.y == 0.0) {
        vec1.x = 0.01;
        vec1.y = 0.0;
    }
    
    if (vec2.x == 0.0 && vec2.y == 0.0) {
        vec2.x = 0.01;
        vec2.y = 0.0;
    }
    
    CGFloat radius1 = sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
    CGFloat radius2 = sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
    if (radius1 > radius2) {
        vec1.x = radius2 * vec1.x / radius1;
        vec1.y = radius2 * vec1.y / radius1;
    } else {
        vec2.x = radius1 * vec2.x / radius2;
        vec2.y = radius1 * vec2.y / radius2;
        radius2 = radius1;
    }
    
    CGFloat angle = arc.endAngle - arc.startAngle;
    angle = CZNormalizeAngle(angle) * 0.5;
    
    const CGFloat defaultOffset = textBox.height * 1.618;
    const CGFloat defaultOffsetAngle = defaultOffset / radius2;  // angle = arc length / radius
    if (angle > defaultOffsetAngle) {
        angle = defaultOffsetAngle;
    }
    
    CGAffineTransform rotateTransform = CGAffineTransformMakeRotation(-angle);
    
    CGPoint middleVec = CGPointApplyAffineTransform(vec2, rotateTransform);
    CGFloat middleRadius = radius2;

    middleVec.x = (radius2 + kArcMeasurementGap) * middleVec.x / middleRadius;
    middleVec.y = (radius2 + kArcMeasurementGap) * middleVec.y / middleRadius;
    
    // set text box top left
    if (middleVec.x > 0.0) {
        textBox.x = middleVec.x + ptP.x;
    } else {
        textBox.x = middleVec.x + ptP.x - textBox.width;
    }
    
    if (middleVec.y > 0.0) {
        textBox.y = middleVec.y + ptP.y;
    } else {
        textBox.y = middleVec.y + ptP.y - textBox.height;
    }
}

+ (double)lengthOfCubicBezierSegments:(const CGPoint *)controlPoints pointsCount:(NSUInteger)pointsCount {
    double totalLength = 0.0;
    
    NSUInteger controlPointsCount = pointsCount;
    
    __block CGFloat ax, ay, bx, by, cx, cy;
    
    // B`(t) = 3(1-t)^2(p1-p0) + 6(1-t)t(P2-P1) + 3t^2(P3-p2)
    // dP10 = P1 - P0
    // dP21 = P2 - P1
    // dP32 = P3 - P2
    // B`(t) = (3 * dP10 - 6 * dP21 + 3 * dP32) * t * t +
    //         -6 * (dP10 + dP21) * t +
    //         -3 * dP10;
    CZMathFunctionBlock f = ^(double t) {
        double dy = ay * t * t + by * t + cy;
        double dx = ax * t * t + bx * t + cx;
        return sqrt(dy * dy + dx * dx);
    };
    
    double epsilon = 1.0 / floor(controlPointsCount / 3);
    
    static const int lenw = 128;
    static double w[lenw + 1];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ClenshawCurtisInit(lenw, w);
    });

    double wCopy[lenw + 1], length, err;
    
    for (NSUInteger i = 3; i < controlPointsCount; i += 3) {
        const CGPoint *p0 = &controlPoints[i - 3];
        const CGPoint *p1 = &controlPoints[i - 2];
        const CGPoint *p2 = &controlPoints[i - 1];
        const CGPoint *p3 = &controlPoints[i];
        
        CGPoint dP10 = CGPointMake(p1->x - p0->x, p1->y - p0->y);
        CGPoint dP21 = CGPointMake(p2->x - p1->x, p2->y - p1->y);
        CGPoint dP32 = CGPointMake(p3->x - p2->x, p3->y - p2->y);
        
        ay = 3 * dP10.y - 6 * dP21.y + 3 * dP32.y;
        ax = 3 * dP10.x - 6 * dP21.x + 3 * dP32.x;
        
        by = 6 * (- dP10.y + dP21.y);
        bx = 6 * (- dP10.x + dP21.x);
        
        cy = 3 * dP10.y;
        cx = 3 * dP10.x;
        
        memcpy(wCopy, w, sizeof(double) * (lenw + 1));
        ClenshawCurtis(f, 0.0, 1.0, epsilon, lenw, wCopy, &length, &err);
        totalLength += length;
    }
    
    return totalLength;
}

// y(t) = k[3]*t^3 + k[2]*t^2 + k[1] * t + k[0]
// x'(t) = g[2]*t^2 + g[1]*t + g[0]
// Area = Intergal(y(t)*x'(t)), [0, 1]
//      = k[3]*g[2]/6 +
//        (k[2]*g[2]+k[3]*g[1])/5 +
//        (k[1]*g[2]+k[2]*g[1]+k[3]*g[0])/4 +
//        (k[0]*g[2]+k[1]*g[1]+k[2]*g[0])/3 +
//        (k[0]*g[1]+k[1]*g[0])/2 +
//        k[0]*g[0]
+ (double)areaOfCubicBezierSegments:(const CGPoint *)controlPoints pointsCount:(NSUInteger)pointsCount {
    double g[3];
    double k[4];
    
    double totalArea = 0;
    
    for (NSUInteger i = 3; i < pointsCount; i += 3) {
        const CGPoint *p0 = &controlPoints[i - 3];
        const CGPoint *p1 = &controlPoints[i - 2];
        const CGPoint *p2 = &controlPoints[i - 1];
        const CGPoint *p3 = &controlPoints[i];
        
        k[0] = p0->y;
        k[1] = (-p0->y + p1->y) * 3;
        k[2] = (p0->y - p1->y * 2 + p2->y) * 3;
        k[3] = -p0->y + p1->y * 3 - p2->y * 3 + p3->y;
        
        g[0] = (-p0->x + p1->x) * 3;
        g[1] = (p0->x - p1->x * 2 + p2->x) * 6;
        g[2] = (-p0->x + p1->x * 3 - p2->x * 3 + p3->x) * 3;
        
        for (int j = 0; j < 6; j++) {
            int nEnd = MIN(j, 3);
            int n = j - 2;
            n = MAX(n, 0);
            double sum = 0;
            for (; n <= nEnd; n++) {
                sum += k[n] * g[j - n];
            }
            totalArea += sum / (j + 1);
        }
    }
    
    return fabs(totalArea);
}

@end

const static int kRombergMaxCount = 10;

// TODO: optimize; cache the result of f(a), f(b)
static double trapezoid(int n, double a, double b, CZMathFunctionBlock f) {
    double trapezoid = 0.0;
    for (int i = 1; i < n; i++) {
        trapezoid = trapezoid + f(a + i * (b - a) / n);
    }
    trapezoid = (b - a) / (2 * n) * (f(a) + f(b) + 2 * trapezoid);
    return trapezoid;
}

double Romberg(double a, double b, double e, CZMathFunctionBlock f) {
    double s[kRombergMaxCount];
    double var = 0.0;
    for (int i = 1; i < kRombergMaxCount; i++) {
        s[i] = 1;
    }
    
    for (int k = 1; k < kRombergMaxCount; k++) {
        for (int i = 1; i <= k; i++) {
            if (i == 1) {
                var = s[i];
                s[i] = trapezoid(1 << (k - 1), a, b, f);
            } else {
                s[k]= ((1 << i) * s[i - 1] - var) / ((1 << i) - 1);
                
                if (i == k && fabs(var - s[i]) < e) {
                    return s[i];
                }
                var = s[i];
                s[i]= s[k];
            }
        }
    }
    
    return s[kRombergMaxCount - 1];
}

#pragma mark - Clenshaw-Curtis-Quadrature

void bitrv(int n, double *a) {
    int j, k, l, m, m2, n1;
    double xr;
    
    if (n > 2) {
        m = n >> 2;
        m2 = m << 1;
        n1 = n - 1;
        k = 0;
        for (j = 0; j <= m2 - 2; j += 2) {
            if (j < k) {
                xr = a[j];
                a[j] = a[k];
                a[k] = xr;
            } else if (j > k) {
                xr = a[n1 - j];
                a[n1 - j] = a[n1 - k];
                a[n1 - k] = xr;
            }
            xr = a[j + 1];
            a[j + 1] = a[m2 + k];
            a[m2 + k] = xr;
            l = m;
            while (k >= l) {
                k -= l;
                l >>= 1;
            }
            k += l;
        }
    }
}

static void bitrv2(int n, double *a) {
    int j, j1, k, k1, l, m, m2, n2;
    double xr, xi;
    
    m = n >> 2;
    m2 = m << 1;
    n2 = n - 2;
    k = 0;
    for (j = 0; j <= m2 - 4; j += 4) {
        if (j < k) {
            xr = a[j];
            xi = a[j + 1];
            a[j] = a[k];
            a[j + 1] = a[k + 1];
            a[k] = xr;
            a[k + 1] = xi;
        } else if (j > k) {
            j1 = n2 - j;
            k1 = n2 - k;
            xr = a[j1];
            xi = a[j1 + 1];
            a[j1] = a[k1];
            a[j1 + 1] = a[k1 + 1];
            a[k1] = xr;
            a[k1 + 1] = xi;
        }
        k1 = m2 + k;
        xr = a[j + 2];
        xi = a[j + 3];
        a[j + 2] = a[k1];
        a[j + 3] = a[k1 + 1];
        a[k1] = xr;
        a[k1 + 1] = xi;
        l = m;
        while (k >= l) {
            k -= l;
            l >>= 1;
        }
        k += l;
    }
}

void cdft(int n, double wr, double wi, double *a) {
    int i, j, k, l, m;
    double wkr, wki, wdr, wdi, ss, xr, xi;
    
    m = n;
    while (m > 4) {
        l = m >> 1;
        wkr = 1;
        wki = 0;
        wdr = 1 - 2 * wi * wi;
        wdi = 2 * wi * wr;
        ss = 2 * wdi;
        wr = wdr;
        wi = wdi;
        for (j = 0; j <= n - m; j += m) {
            i = j + l;
            xr = a[j] - a[i];
            xi = a[j + 1] - a[i + 1];
            a[j] += a[i];
            a[j + 1] += a[i + 1];
            a[i] = xr;
            a[i + 1] = xi;
            xr = a[j + 2] - a[i + 2];
            xi = a[j + 3] - a[i + 3];
            a[j + 2] += a[i + 2];
            a[j + 3] += a[i + 3];
            a[i + 2] = wdr * xr - wdi * xi;
            a[i + 3] = wdr * xi + wdi * xr;
        }
        for (k = 4; k <= l - 4; k += 4) {
            wkr -= ss * wdi;
            wki += ss * wdr;
            wdr -= ss * wki;
            wdi += ss * wkr;
            for (j = k; j <= n - m + k; j += m) {
                i = j + l;
                xr = a[j] - a[i];
                xi = a[j + 1] - a[i + 1];
                a[j] += a[i];
                a[j + 1] += a[i + 1];
                a[i] = wkr * xr - wki * xi;
                a[i + 1] = wkr * xi + wki * xr;
                xr = a[j + 2] - a[i + 2];
                xi = a[j + 3] - a[i + 3];
                a[j + 2] += a[i + 2];
                a[j + 3] += a[i + 3];
                a[i + 2] = wdr * xr - wdi * xi;
                a[i + 3] = wdr * xi + wdi * xr;
            }
        }
        m = l;
    }
    if (m > 2) {
        for (j = 0; j <= n - 4; j += 4) {
            xr = a[j] - a[j + 2];
            xi = a[j + 1] - a[j + 3];
            a[j] += a[j + 2];
            a[j + 1] += a[j + 3];
            a[j + 2] = xr;
            a[j + 3] = xi;
        }
    }
    if (n > 4) {
        bitrv2(n, a);
    }
}

void rdft(int n, double wr, double wi, double *a) {
    int j, k;
    double wkr, wki, wdr, wdi, ss, xr, xi, yr, yi;
    
    if (n > 4) {
        wkr = 0;
        wki = 0;
        wdr = wi * wi;
        wdi = wi * wr;
        ss = 4 * wdi;
        wr = 1 - 2 * wdr;
        wi = 2 * wdi;
        if (wi >= 0) {
            cdft(n, wr, wi, a);
            xi = a[0] - a[1];
            a[0] += a[1];
            a[1] = xi;
        }
        for (k = (n >> 1) - 4; k >= 4; k -= 4) {
            j = n - k;
            xr = a[k + 2] - a[j - 2];
            xi = a[k + 3] + a[j - 1];
            yr = wdr * xr - wdi * xi;
            yi = wdr * xi + wdi * xr;
            a[k + 2] -= yr;
            a[k + 3] -= yi;
            a[j - 2] += yr;
            a[j - 1] -= yi;
            wkr += ss * wdi;
            wki += ss * (0.5 - wdr);
            xr = a[k] - a[j];
            xi = a[k + 1] + a[j + 1];
            yr = wkr * xr - wki * xi;
            yi = wkr * xi + wki * xr;
            a[k] -= yr;
            a[k + 1] -= yi;
            a[j] += yr;
            a[j + 1] -= yi;
            wdr += ss * wki;
            wdi += ss * (0.5 - wkr);
        }
        j = n - 2;
        xr = a[2] - a[j];
        xi = a[3] + a[j + 1];
        yr = wdr * xr - wdi * xi;
        yi = wdr * xi + wdi * xr;
        a[2] -= yr;
        a[3] -= yi;
        a[j] += yr;
        a[j + 1] -= yi;
        if (wi < 0) {
            a[1] = 0.5 * (a[0] - a[1]);
            a[0] -= a[1];
            cdft(n, wr, wi, a);
        }
    } else {
        if (wi < 0) {
            a[1] = 0.5 * (a[0] - a[1]);
            a[0] -= a[1];
        }
        if (n > 2) {
            xr = a[0] - a[2];
            xi = a[1] - a[3];
            a[0] += a[2];
            a[1] += a[3];
            a[2] = xr;
            a[3] = xi;
        }
        if (wi >= 0) {
            xi = a[0] - a[1];
            a[0] += a[1];
            a[1] = xi;
        }
    }
}

void ddct(int n, double wr, double wi, double *a) {
    int j, k, m;
    double wkr, wki, wdr, wdi, ss, xr;
    
    if (n > 2) {
        wkr = 0.5;
        wki = 0.5;
        wdr = 0.5 * (wr - wi);
        wdi = 0.5 * (wr + wi);
        ss = 2 * wi;
        if (wi < 0) {
            xr = a[n - 1];
            for (k = n - 2; k >= 2; k -= 2) {
                a[k + 1] = a[k] - a[k - 1];
                a[k] += a[k - 1];
            }
            a[1] = 2 * xr;
            a[0] *= 2;
            rdft(n, 1 - ss * wi, ss * wr, a);
            xr = wdr;
            wdr = wdi;
            wdi = xr;
            ss = -ss;
        }
        m = n >> 1;
        for (k = 1; k <= m - 3; k += 2) {
            j = n - k;
            xr = wdi * a[k] - wdr * a[j];
            a[k] = wdr * a[k] + wdi * a[j];
            a[j] = xr;
            wkr -= ss * wdi;
            wki += ss * wdr;
            xr = wki * a[k + 1] - wkr * a[j - 1];
            a[k + 1] = wkr * a[k + 1] + wki * a[j - 1];
            a[j - 1] = xr;
            wdr -= ss * wki;
            wdi += ss * wkr;
        }
        k = m - 1;
        j = n - k;
        xr = wdi * a[k] - wdr * a[j];
        a[k] = wdr * a[k] + wdi * a[j];
        a[j] = xr;
        a[m] *= wki + ss * wdr;
        if (wi >= 0) {
            rdft(n, 1 - ss * wi, ss * wr, a);
            xr = a[1];
            for (k = 2; k <= n - 2; k += 2) {
                a[k - 1] = a[k] - a[k + 1];
                a[k] += a[k + 1];
            }
            a[n - 1] = xr;
        }
    } else {
        if (wi >= 0) {
            xr = 0.5 * (wr + wi) * a[1];
            a[1] = a[0] - xr;
            a[0] += xr;
        } else {
            xr = a[0] - a[1];
            a[0] += a[1];
            a[1] = 0.5 * (wr - wi) * xr;
        }
    }
}

void ddst(int n, double wr, double wi, double *a) {
    int j, k, m;
    double wkr, wki, wdr, wdi, ss, xr;
    
    if (n > 2) {
        wkr = 0.5;
        wki = 0.5;
        wdr = 0.5 * (wr - wi);
        wdi = 0.5 * (wr + wi);
        ss = 2 * wi;
        if (wi < 0) {
            xr = a[n - 1];
            for (k = n - 2; k >= 2; k -= 2) {
                a[k + 1] = a[k] + a[k - 1];
                a[k] -= a[k - 1];
            }
            a[1] = -2 * xr;
            a[0] *= 2;
            rdft(n, 1 - ss * wi, ss * wr, a);
            xr = wdr;
            wdr = -wdi;
            wdi = xr;
            wkr = -wkr;
        }
        m = n >> 1;
        for (k = 1; k <= m - 3; k += 2) {
            j = n - k;
            xr = wdi * a[j] - wdr * a[k];
            a[k] = wdr * a[j] + wdi * a[k];
            a[j] = xr;
            wkr -= ss * wdi;
            wki += ss * wdr;
            xr = wki * a[j - 1] - wkr * a[k + 1];
            a[k + 1] = wkr * a[j - 1] + wki * a[k + 1];
            a[j - 1] = xr;
            wdr -= ss * wki;
            wdi += ss * wkr;
        }
        k = m - 1;
        j = n - k;
        xr = wdi * a[j] - wdr * a[k];
        a[k] = wdr * a[j] + wdi * a[k];
        a[j] = xr;
        a[m] *= wki + ss * wdr;
        if (wi >= 0) {
            rdft(n, 1 - ss * wi, ss * wr, a);
            xr = a[1];
            for (k = 2; k <= n - 2; k += 2) {
                a[k - 1] = a[k + 1] - a[k];
                a[k] += a[k + 1];
            }
            a[n - 1] = -xr;
        }
    } else {
        if (wi >= 0) {
            xr = 0.5 * (wr + wi) * a[1];
            a[1] = xr - a[0];
            a[0] += xr;
        } else {
            xr = a[0] + a[1];
            a[0] -= a[1];
            a[1] = 0.5 * (wr - wi) * xr;
        }
    }
}

void dfct(int n, double wr, double wi, double *a) {
    int j, k, m, mh;
    double xr, xi, an;
    
    m = n >> 1;
    for (j = 0; j <= m - 1; j++) {
        k = n - j;
        xr = a[j] + a[k];
        a[j] -= a[k];
        a[k] = xr;
    }
    an = a[n];
    while (m >= 2) {
        ddct(m, wr, wi, a);
        xr = 1 - 2 * wi * wi;
        wi *= 2 * wr;
        wr = xr;
        bitrv(m, a);
        mh = m >> 1;
        xi = a[m];
        a[m] = a[0];
        a[0] = an - xi;
        an += xi;
        for (j = 1; j <= mh - 1; j++) {
            k = m - j;
            xr = a[m + k];
            xi = a[m + j];
            a[m + j] = a[j];
            a[m + k] = a[k];
            a[j] = xr - xi;
            a[k] = xr + xi;
        }
        xr = a[mh];
        a[mh] = a[m + mh];
        a[m + mh] = xr;
        m = mh;
    }
    xi = a[1];
    a[1] = a[0];
    a[0] = an + xi;
    a[n] = an - xi;
    bitrv(n, a);
}

void dfst(int n, double wr, double wi, double *a) {
    int j, k, m, mh;
    double xr, xi;
    
    m = n >> 1;
    for (j = 1; j <= m - 1; j++) {
        k = n - j;
        xr = a[j] - a[k];
        a[j] += a[k];
        a[k] = xr;
    }
    a[0] = a[m];
    while (m >= 2) {
        ddst(m, wr, wi, a);
        xr = 1 - 2 * wi * wi;
        wi *= 2 * wr;
        wr = xr;
        bitrv(m, a);
        mh = m >> 1;
        for (j = 1; j <= mh - 1; j++) {
            k = m - j;
            xr = a[m + k];
            xi = a[m + j];
            a[m + j] = a[j];
            a[m + k] = a[k];
            a[j] = xr + xi;
            a[k] = xr - xi;
        }
        a[m] = a[0];
        a[0] = a[m + mh];
        a[m + mh] = a[mh];
        m = mh;
    }
    a[1] = a[0];
    a[0] = 0;
    bitrv(n, a);
}

void ClenshawCurtisInit(int lenw, double *w) {
    void dfct(int, double, double, double *);
    int j, k, l, m;
    double cos2, sin1, sin2, hl;
    
    cos2 = 0;
    sin1 = 1;
    sin2 = 1;
    hl = 0.5;
    k = lenw;
    l = 2;
    while (l < k - l - 1) {
        w[0] = hl * 0.5;
        for (j = 1; j <= l; j++) {
            w[j] = hl / (1 - 4 * j * j);
        }
        w[l] *= 0.5;
        dfct(l, 0.5 * cos2, sin1, w);
        cos2 = sqrt(2 + cos2);
        sin1 /= cos2;
        sin2 /= 2 + cos2;
        w[k] = sin2;
        w[k - 1] = w[0];
        w[k - 2] = w[l];
        k -= 3;
        m = l;
        while (m > 1) {
            m >>= 1;
            for (j = m; j <= l - m; j += (m << 1)) {
                w[k] = w[j];
                k--;
            }
        }
        hl *= 0.5;
        l *= 2;
    }
}

void ClenshawCurtis(CZMathFunctionBlock f, double a, double b, double eps,
                    int lenw, double *w, double *i, double *err) {
    int j, k, l;
    double esf, eref, erefh, hh, ir, iback, irback, ba, ss, x, y, fx,
    errir;
    
    esf = 10;
    ba = 0.5 * (b - a);
    ss = 2 * w[lenw];
    x = ba * w[lenw];
    w[0] = 0.5 * f(a);
    w[3] = 0.5 * f(b);
    w[2] = f(a + x);
    w[4] = f(b - x);
    w[1] = f(a + ba);
    eref = 0.5 * (fabs(w[0]) + fabs(w[1]) + fabs(w[2]) + fabs(w[3]) +
                  fabs(w[4]));
    w[0] += w[3];
    w[2] += w[4];
    ir = w[0] + w[1] + w[2];
    *i = w[0] * w[lenw - 1] + w[1] * w[lenw - 2] + w[2] * w[lenw - 3];
    erefh = eref * sqrt(eps);
    eref *= eps;
    hh = 0.25;
    l = 2;
    k = lenw - 5;
    do {
        iback = *i;
        irback = ir;
        x = ba * w[k + 1];
        y = 0;
        *i = w[0] * w[k];
        for (j = 1; j <= l; j++) {
            x += y;
            y += ss * (ba - x);
            fx = f(a + x) + f(b - x);
            ir += fx;
            *i += w[j] * w[k - j] + fx * w[k - j - l];
            w[j + l] = fx;
        }
        ss = 2 * w[k + 1];
        *err = esf * l * fabs(*i - iback);
        hh *= 0.25;
        errir = hh * fabs(ir - 2 * irback);
        l *= 2;
        k -= l + 2;
    } while ((*err > erefh || errir > eref) && k > 4 * l);
    *i *= b - a;
    if (*err > erefh || errir > eref) {
        *err *= -fabs(b - a);
    } else {
        *err = eref * fabs(b - a);
    }
}
