//
//  CZElementMultiCalipers.h
//  Matscope
//
//  Created by sherry on 4/7/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"
#import "CZElementMultiPoint.h"

@interface CZElementMultiCalipers :CZElementMultiPoint

- (CGPoint)othorPointAtIndex:(NSUInteger)index;

@end

typedef CZElementMultiCalipers CZElementMultiDistance;  // Hermes treat multi distance annotation as multi calipers
