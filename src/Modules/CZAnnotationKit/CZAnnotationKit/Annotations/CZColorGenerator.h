//
//  CZColorGenerator.h
//  Matscope
//
//  Created by Sherry Xu on 8/6/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZColor.h"

@interface CZColorGenerator : NSObject

+ (CZColorGenerator *)sharedInstance;
- (CZColor )colorFromColorMapAtIndex:(NSUInteger)index;
- (void)setDisableMeasurementColorCoding:(BOOL)disableMeasurementColorCoding;
@end
