//
//  CZElementSpline.h
//  Hermes
//
//  Created by Jin Ralph on 13-12-16.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiPoint.h"

@interface CZElementSpline : CZElementMultiPoint
@end
