//
//  CZElementArrow.m
//  Hermes
//
//  Created by Ralph Jin on 3/22/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementArrow.h"
#import "CZNodeGeom.h"
#import "CZNodeLine.h"
#import "CZNodePolygon.h"
#import "CZNodeText.h"

static const CGFloat kDefaultArrowWidth = 12;
static const CGFloat kDefaultArrowHeight = 4;

@interface CZElementArrow () {
    CGPoint _p1;
}

@property (nonatomic, retain) CZNodeLine *lineNode;
@property (nonatomic, retain) CZNodePolygon *arrowNode;

@end

@implementation CZElementArrow

- (instancetype)init {
    return [self initWithPoint:CGPointZero anotherPoint:CGPointZero];
}

- (instancetype)initWithPoint:(CGPoint)p1 anotherPoint:(CGPoint)p2 {
    self = [super init];
    if (self) {
        _p1 = p1;
        _lineNode = [[CZNodeLine alloc] initWithPoint:p1 anotherPoint:p2];
        _arrowNode = [[CZNodePolygon alloc] initWithPointsCount:3U];
        [_lineNode addSubNode:_arrowNode];
    }
    
    return self;
}

- (void)dealloc {
    [_lineNode release];
    [_arrowNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementArrow *newArrow = [super copyWithZone:zone];
    if (newArrow) {
        newArrow->_p1 = self->_p1;
        
        [newArrow->_lineNode release];
        newArrow->_lineNode = [self.lineNode copyWithZone:zone];
        
        if ([newArrow->_lineNode subNodeCount] > 0) {
            newArrow.arrowNode = (CZNodePolygon *)[newArrow->_lineNode subNodeAtIndex:0];
        }
    }
    
    return newArrow;
}

- (CZNode *)primaryNode {
    return _lineNode;
}

- (CGPoint)p1 {
    return _p1;
}

- (void)setP1:(CGPoint)p1 {
    _p1 = p1;
    self.featureValidated = NO;
}

- (CGPoint)p2 {
    return _lineNode.p2;
}

- (void)setP2:(CGPoint)p2 {
    _lineNode.p2 = p2;
    self.featureValidated = NO;
}

#pragma mark - Override CZElement

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementArrow *anArrow = (CZElementArrow *)anElement;
    
    return (CGPointEqualToPoint(anArrow.p1, self.p1) &&
            CGPointEqualToPoint(anArrow.p2, self.p2));
}

- (CALayer *)newLayer {
    [self updateArrow];
    
    _lineNode.lineWidth = [self suggestLineWidth];
    CALayer *layer = [_lineNode newLayer];
    
    CALayer *arrowLayer = [_arrowNode newLayer];
    [layer addSublayer:arrowLayer];
    [arrowLayer release];
    
    return layer;
}

- (void)setSize:(CZElementSize)size {
    [super setSize:size];
    self.featureValidated = NO;
}

- (void)setStrokeColor:(CZColor)strokeColor {
    _lineNode.strokeColor = strokeColor;
    _arrowNode.fillColor = strokeColor;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    CGPoint p = self.p1;
    p = CGPointApplyAffineTransform(p, transform);
    self.p1 = p;
    
    p = self.p2;
    p = CGPointApplyAffineTransform(p, transform);
    self.p2 = p;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    NSArray *pointArray = @[@(_lineNode.p1.x), @(_lineNode.p1.y), @(_lineNode.p2.x), @(_lineNode.p2.y)];
    NSDictionary *memo = [[NSDictionary alloc] initWithObjectsAndKeys:pointArray, @"CGPointArray", nil];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSArray *pointArray = [memo objectForKey:@"CGPointArray"];
    if ([pointArray isKindOfClass:[NSArray class]] && [pointArray count] >= 4) {
        [self notifyWillChange];
        
        CGPoint p1;
        p1.x = [pointArray[0] floatValue];
        p1.y = [pointArray[1] floatValue];
        
        CGPoint p2;
        p2.x = [pointArray[2] floatValue];
        p2.y = [pointArray[3] floatValue];
        
        self.p1 = p1;
        self.p2 = p2;
        
        [self notifyDidChange];
        return YES;
    }
    
    return NO;
}

#pragma mark - Private method

- (void)updateArrow {
    if (self.featureValidated) {
        return;
    }
    
    CGPoint p1 = _p1;
    CGPoint p2 = _lineNode.p2;
    CGFloat dx = p2.x - p1.x;
    CGFloat dy = p2.y - p1.y;
    if (dx == 0.0 && dy == 0.0) {
        dx = 1.0;
    }
    CGFloat distance = hypot(dx, dy);
    CGFloat sina, cosa;
    if (distance != 0.0) {
        sina = dy / distance;
        cosa = dx / distance;
    } else {
        sina = 1.0;
        cosa = 0.0;
    }
    
    // calculate arrow size
    CGFloat lineWidth = [self suggestLineWidth];
    CGFloat scale = lineWidth;
    CGSize arrowSize;
    arrowSize.width = kDefaultArrowWidth * scale;
    arrowSize.height = kDefaultArrowHeight * scale;
    
    //   /|
    //  / |
    // <  |<--- center point
    //  \ |
    //   \|
    CGPoint centerPoint;
    centerPoint.x = p1.x + arrowSize.width * cosa;
    centerPoint.y = p1.y + arrowSize.width * sina;
    _lineNode.p1 = centerPoint;

    // offset to line's coordinate
    CGFloat minX = MIN(centerPoint.x, p2.x);
    CGFloat minY = MIN(centerPoint.y, p2.y);
    p1.x -= minX;
    p2.x -= minX;
    p1.y -= minY;
    p2.y -= minY;
    centerPoint.x -= minX;
    centerPoint.y -= minY;
    
    // rotate angle 90 degree
    CGFloat temp = cosa;
    cosa = sina;
    sina = -temp;
    
    // set arrow's 3 points
    [_arrowNode setPoint:p1 atIndex:0];
    p1.x = centerPoint.x + arrowSize.height * cosa;
    p1.y = centerPoint.y + arrowSize.height * sina;
    p2.x = centerPoint.x - arrowSize.height * cosa;
    p2.y = centerPoint.y - arrowSize.height * sina;
    [_arrowNode setPoint:p1 atIndex:1];
    [_arrowNode setPoint:p2 atIndex:2];
    
    self.featureValidated = YES;
}

@end
