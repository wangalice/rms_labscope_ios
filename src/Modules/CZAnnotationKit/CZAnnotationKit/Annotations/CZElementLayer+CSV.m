//
//  CZElementLayer+CSV.m
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayer+CSV.h"
#import <CZToolbox/CZToolbox.h>
#import "CZFeature.h"
#import "CZElementMultiCalipers.h"

@interface CZCSVFeatureAndSubscript : NSObject<NSCopying>

@property (nonatomic, copy, readonly) NSString *type;
@property (nonatomic, assign, readonly) NSUInteger subscript; // if NSNotFound
@property (nonatomic, readonly) BOOL hasSubscript;
@property (nonatomic, copy, readonly) NSString *humanReadableType;
@property (nonatomic, copy, readonly) NSString *translatedType;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithType:(NSString *)type;
- (instancetype)initWithType:(NSString *)type subscript:(NSUInteger)subscript NS_DESIGNATED_INITIALIZER;

@end

@implementation CZCSVFeatureAndSubscript

- (instancetype)initWithType:(NSString *)type {
    return [self initWithType:type subscript:NSNotFound];
}

- (instancetype)initWithType:(NSString *)type subscript:(NSUInteger)subscript {
    if (type == nil) {
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _type = [type copy];
        _subscript = subscript;
    }
    return self;
}

- (void)dealloc {
    [_type release];
    [super dealloc];
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if ([self class] != [object class]) {
        return NO;
    }
    
    CZCSVFeatureAndSubscript *other = object;
    if (_subscript != other.subscript) {
        return NO;
    }
    
    if (![_type isEqualToString:other.type]) {
        return NO;
    }
    
    return YES;
}

- (NSUInteger)hash {
    NSUInteger prime = 31;
    NSUInteger result = 1;
    
    result = prime * result + _type.hash;
    result = prime * result + @(_subscript).hash;
    return result;
}

- (id)copyWithZone:(NSZone *)zone {
    return [[[self class] allocWithZone:zone] initWithType:_type subscript:_subscript];
}

- (BOOL)hasSubscript {
    return _subscript != NSNotFound;
}

- (NSString *)humanReadableType {
    NSString *pluralTypeDistance = [kCZFeatureTypeDistance stringByAppendingString:@"S"];
    NSDictionary *humanReadableTypeMap = @{kCZFeatureTypeArea: @"Area",
                                           kCZFeatureTypeDistance: @"Distance",
                                           pluralTypeDistance : @"Distances",
                                           kCZFeatureTypeCenterX: @"Center X",
                                           kCZFeatureTypeCenterY: @"Center Y",
                                           kCZFeatureTypeAngle: @"Angle",
                                           kCZFeatureTypeDiameter: @"Diameter",
                                           kCZFeatureTypePerimeter: @"Perimeter",
                                           kCZFeatureTypeNumber: @"Number"};

    NSString *typeString;
    if (self.hasSubscript) {
        NSString *pluralType = [_type stringByAppendingString:@"S"];
        typeString = humanReadableTypeMap[pluralType];
        NSAssert(typeString, @"Unknown type: %@, when genterating CSV report.", pluralType);
        
        typeString = [typeString stringByAppendingFormat:@"_%ld", (long)_subscript];
    } else {
        typeString = humanReadableTypeMap[_type];
        
        NSAssert(typeString, @"Unknown type: %@, when genterating CSV report.", _type);
    }
    return typeString;
}

- (NSString *)translatedType {
    NSString *typeString;
    if (self.hasSubscript) {
        NSString *pluralType = [_type stringByAppendingString:@"S"];
        typeString = L(pluralType);
        NSAssert(![typeString isEqualToString:pluralType], @"Not translated type: %@, when genterating CSV report.", pluralType);
        
        typeString = [typeString stringByAppendingFormat:@"_%ld", (long)_subscript];
    } else {
        typeString = L(_type);
        NSAssert(![typeString isEqualToString:_type], @"Not translated type: %@, when genterating CSV report.", _type);
    }
    return typeString;
}

@end

@implementation CZElementLayer (CSV)

- (BOOL)writeMeasurementsToCSVFile:(NSString *)filePath error:(NSError **)error {
    // white list for exporting feature type
    NSSet *typeSet = [NSSet setWithObjects:kCZFeatureTypeArea,
                      kCZFeatureTypeDistance,
                      kCZFeatureTypeAngle,
                      kCZFeatureTypeDiameter,
                      kCZFeatureTypePerimeter,
                      kCZFeatureTypeNumber,
                      kCZFeatureTypeCenterX,
                      kCZFeatureTypeCenterY, nil];
    
    // decide unit map
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    NSString *unit2d = [self unitNameOfAreaByStyle:unitStyle];
    NSDictionary *unitMap = @{kCZFeatureTypeArea: unit2d,
                              kCZFeatureTypeDistance: unit1d,
                              kCZFeatureTypeAngle: kCZFeatureUnitDegree,
                              kCZFeatureTypeDiameter: unit1d,
                              kCZFeatureTypePerimeter: unit1d,
                              kCZFeatureTypeNumber: kCZFeatureUnitCount};
    
    // count feature type (array/set of CZCSVFeatureAndSubscript)
    NSMutableArray *realFeatureTypes = [NSMutableArray array];
    NSMutableSet *realFeatureTypesSet = [NSMutableSet set];
    for (NSUInteger i = 0; i < [self elementCount]; i++) {
        CZElement *element = [self elementAtIndex:i];
        if (element.isMeasurementHidden) {
            continue;
        }
        
        NSUInteger subscript = 0;
        BOOL featureHasSubscript = [element isKindOfClass:[CZElementMultiCalipers class]];
        
        for (CZFeature *feature in element.features) {
            if (feature.value >= 0 && [typeSet containsObject:feature.type]) {
                CZCSVFeatureAndSubscript *featureType = featureHasSubscript ?
                    [[CZCSVFeatureAndSubscript alloc] initWithType:feature.type subscript:subscript] :
                    [[CZCSVFeatureAndSubscript alloc] initWithType:feature.type];

                if (![realFeatureTypesSet containsObject:featureType]) {
                    [realFeatureTypesSet addObject:featureType];
                    [realFeatureTypes addObject:featureType];
                }
                [featureType release];
            }
            
            subscript++;
        }
    }
    
    if ([realFeatureTypes count] == 0) {
        if (error) {
            *error = [NSError errorWithDomain:CZAnnotationErrorDomain
                                         code:kCZAnnotationNoMeasurmentsError
                                     userInfo:nil];
        }
        return NO;
    }
    
    // scan feature type (array of CZCSVFeatureAndSubscript)
    NSArray *featureTypes = [NSArray arrayWithArray:realFeatureTypes];
    
    // Append title of csv
    NSMutableString *csvContent = [[NSMutableString alloc] initWithCapacity:4096U];
    [csvContent appendString:@"\xEF\xBB\xBF"];  // append BOM
    [csvContent appendString:@"\"Name::Name\""];
    
    [csvContent appendString:@",\"Time::"];
    [csvContent appendString:L(@"EXPORT_CSV_TIME")];
    [csvContent appendString:@"!!I\""];
    
    if (featureTypes.count > 0UL) {
        for (CZCSVFeatureAndSubscript *featureType in featureTypes) {
            [csvContent appendString:@",\""];
            [csvContent appendString:featureType.humanReadableType];
            [csvContent appendString:@"::"];
            [csvContent appendString:featureType.translatedType];
            if ([featureType.type isEqualToString:kCZFeatureTypeNumber]) {
                [csvContent appendString:@"!!I\""];
            } else {
                [csvContent appendString:@"!!R\""];
            }
        }
        [csvContent appendString:@"\n"];
        
        // Append unit row
        [csvContent appendString:@","];
        for (CZCSVFeatureAndSubscript *featureType in featureTypes) {
            [csvContent appendString:@",\""];
            NSString *unit = unitMap[featureType.type];
            [csvContent appendString:L(unit)];
            [csvContent appendString:@"\""];
        }
        [csvContent appendString:@"\n"];
        
        // Append measurement rows
        NSUInteger precision = [self precision];
        for (NSUInteger i = 0; i < [self elementCount]; i++) {
            CZElement *element = [self elementAtIndex:i];
            if (element.isMeasurementHidden) {
                continue;
            }
            
            if (element.features.count > 0) {
                [csvContent appendString:@"\""];
                [csvContent appendString:L([element type])];
                [csvContent appendString:@"\""];
                
                [csvContent appendString:@",1"];
                
                NSUInteger subscript = 0;
                BOOL featureHasSubscript = [element isKindOfClass:[CZElementMultiCalipers class]];
                
                // mapping {key: CZCSVFeatureAndSubscript, value: CZFeature}
                NSMutableDictionary *featureDict = [[NSMutableDictionary alloc] initWithCapacity:featureTypes.count];
                for (CZFeature *feature in element.features) {
                    if (feature.value >= 0 && [typeSet containsObject:feature.type]) {
                        
                        CZCSVFeatureAndSubscript *featureType = featureHasSubscript ?
                        [[CZCSVFeatureAndSubscript alloc] initWithType:feature.type subscript:subscript] :
                        [[CZCSVFeatureAndSubscript alloc] initWithType:feature.type];
                        
                        [featureDict setObject:feature forKey:featureType];
                        [featureType release];
                    }
                    subscript++;
                }
                
                for (CZCSVFeatureAndSubscript *featureType in featureTypes) {
                    [csvContent appendString:@","];
                    CZFeature *feature = [featureDict objectForKey:featureType];
                    if (feature) {
                        [csvContent appendString:@"\""];
                        [csvContent appendString:[feature valueStringWithPrecision:precision]];
                        [csvContent appendString:@"\""];
                    }
                }
                
                [featureDict release];
                
                [csvContent appendString:@"\n"];
            }
        }
    }

    BOOL success = [csvContent writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:error];
    [csvContent release];
    
    return success;
}

@end
