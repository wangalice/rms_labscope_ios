//
//  CZElementLine.m
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLine.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodeLine.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"
#import "CZElementCommon.h"

@interface CZElementLine ()

@property (nonatomic, retain) CZNodeLine *lineNode;
@property (nonatomic, retain) CZNodeLine *leftTick;
@property (nonatomic, retain) CZNodeLine *rightTick;

- (void)updateFeatures;

@end

@implementation CZElementLine

- (instancetype)init {
    return [self initWithPoint:CGPointZero anotherPoint:CGPointZero];
}

- (instancetype)initWithPoint:(CGPoint)p1 anotherPoint:(CGPoint)p2 {
    self = [super init];
    if (self) {
        _lineNode = [[CZNodeLine alloc] initWithPoint:p1 anotherPoint:p2];
    }
    
    return self;
}

- (void)dealloc {
    [_lineNode release];
    [_leftTick release];
    [_rightTick release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementLine *newLine = [super copyWithZone:zone];
    
    if (newLine) {
        [newLine->_lineNode release];
        newLine->_lineNode = [self->_lineNode copyWithZone:zone];
        
        if ([newLine->_lineNode subNodeCount] > 0) {
            newLine.leftTick = (CZNodeLine *)[newLine->_lineNode subNodeAtIndex:0];
        }
        
        if ([newLine->_lineNode subNodeCount] > 1) {
            newLine.rightTick = (CZNodeLine *)[newLine->_lineNode subNodeAtIndex:1];
        }
    }
    
    return newLine;
}

#pragma mark - Override CZElement

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementLine *aLine = (CZElementLine *)anElement;
    
    return (CGPointEqualToPoint(aLine.p1, self.p1) &&
            CGPointEqualToPoint(aLine.p2, self.p2));
}

- (CZNode *)primaryNode {
    return _lineNode;
}

- (CGPoint)p1 {
    return _lineNode.p1;
}

- (void)setP1:(CGPoint)p1 {
    _lineNode.p1 = p1;
    self.featureValidated = NO;
}

- (CGPoint)p2 {
    return _lineNode.p2;
}

- (void)setP2:(CGPoint)p2 {
    _lineNode.p2 = p2;
    self.featureValidated = NO;
}

- (CALayer *)newLayer {
    _lineNode.lineWidth = [self suggestLineWidth];
    [self updateTicks];
    
    CALayer *layer = [_lineNode newLayer];
    return layer;
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    CZFeature *feature = self.features.count > 0 ? self.features[0] : nil;
    NSString *feastureString = [feature toLocalizedStringWithPrecision:[self precision] showType:!self.measurementNameHidden];
    CZNodeRectangle *background = [self newMeasurementNodeWithString:feastureString];

    if (self.measurementNameHidden) {
        CGPoint p1 = _lineNode.p1;
        CGPoint p2 = _lineNode.p2;
        CGFloat dx = fabs(p1.x - p2.x);
        CGFloat dy = fabs(p1.y - p2.y);
        if (dx < dy) {
            background.x = (p1.x + p2.x - background.width) * 0.5;
            background.y = MAX(p1.y, p2.y) + 8;
        } else {
            background.x = MAX(p1.x, p2.x) + 8;
            background.y = (p1.y + p2.y - background.height) * 0.5;
        }
    } else {
        CGFloat tickHeight = [self tickHeight];
        [CZElementCommon putMeasurement:background onLine:_lineNode.p1 secondPoint:_lineNode.p2 distance:-tickHeight - 2];
    }
    
    // Create CALayer for rendering
    layer = [background newLayer];
    [background release];
    return layer;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    CGPoint p = _lineNode.p1;
    p = CGPointApplyAffineTransform(p, transform);
    _lineNode.p1 = p;
    
    p = _lineNode.p2;
    p = CGPointApplyAffineTransform(p, transform);
    _lineNode.p2 = p;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    NSArray *pointArray = @[@(_lineNode.p1.x), @(_lineNode.p1.y), @(_lineNode.p2.x), @(_lineNode.p2.y)];
    NSDictionary *memo = [[NSDictionary alloc] initWithObjectsAndKeys:pointArray, @"CGPointArray", nil];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSArray *pointArray = [memo objectForKey:@"CGPointArray"];
    if ([pointArray isKindOfClass:[NSArray class]] && [pointArray count] >= 4) {
        [self notifyWillChange];
        
        CGPoint p1;
        p1.x = [pointArray[0] floatValue];
        p1.y = [pointArray[1] floatValue];
        
        CGPoint p2;
        p2.x = [pointArray[2] floatValue];
        p2.y = [pointArray[3] floatValue];
        
        self.p1 = p1;
        self.p2 = p2;
        
        [self notifyDidChange];
        return YES;
    }
    return NO;
}

+ (NSUInteger)featureCount {
    return 1;
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    
    CGFloat distance = CZLengthOfLine(_lineNode.p1, _lineNode.p2);
    CGFloat scaledDistance = scaling * distance;
    
    [self removeAllFeatures];
    
    CZFeature *feature = [[CZFeature alloc] initWithValue:scaledDistance unit:unit1d type:kCZFeatureTypeDistance];
    [self appendFeature:feature];
    [feature release];
}

#pragma mark - Private methods

- (CZNodeLine *)leftTick {
    if (_leftTick == nil) {
        _leftTick = [[CZNodeLine alloc] init];
    }
    return _leftTick;
}

- (CZNodeLine *)rightTick {
    if (_rightTick == nil) {
        _rightTick = [[CZNodeLine alloc] init];
    }
    return _rightTick;
}

- (CGFloat)tickHeight {
    return 6.0 * [self suggestLineWidth];
}

- (void)updateTicks {
    if (self.isMeasurementHidden) {
        [_leftTick removeFromParent];
        [_rightTick removeFromParent];
        self.leftTick = nil;
        self.rightTick = nil;
    } else {
        [_lineNode addSubNode:self.leftTick];
        [_lineNode addSubNode:self.rightTick];
        
        _leftTick.lineWidth = _lineNode.lineWidth;
        _leftTick.strokeColor = _lineNode.strokeColor;
        
        _rightTick.lineWidth = _lineNode.lineWidth;
        _rightTick.strokeColor = _lineNode.strokeColor;
    
        CGPoint p1 = _lineNode.p1;
        CGPoint p2 = _lineNode.p2;
        
        CGPoint topLeft;
        topLeft.x = MIN(p1.x, p2.x);
        topLeft.y = MIN(p1.y, p2.y);
        
        p1.x -= topLeft.x;
        p1.y -= topLeft.y;
        p2.x -= topLeft.x;
        p2.y -= topLeft.y;
        
        CGFloat tickHeight = [self tickHeight];
        
        _leftTick.p1 = CZCalcOuterPoint(p2, p1, tickHeight);
        _leftTick.p2 = CZCalcOuterPoint(p2, p1, -tickHeight);
        
        _rightTick.p1 = CZCalcOuterPoint(p1, p2, tickHeight);
        _rightTick.p2 = CZCalcOuterPoint(p1, p2, -tickHeight);
    }
}

@end
