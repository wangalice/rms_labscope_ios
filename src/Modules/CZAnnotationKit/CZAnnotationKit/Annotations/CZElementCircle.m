//
//  CZElementCircle.m
//  Hermes
//
//  Created by Ralph Jin on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementCircle.h"
#import "CZElementSubclass.h"

#import "CZNodeGeom.h"
#import "CZNodeEllipse.h"
#import "CZNodeRectangle.h"
#import "CZNodePolygon.h"
#import "CZElementCommon.h"
#import "CZFeature.h"

#if CALCULATE_AERA_USE_CRACK_ALGO
#import "CZElementLayer.h"
#import "CZGemMeasurer.h"
#endif

const NSUInteger CZElementCirclePointCount = 3;

static const double CZ_MATH_SQRT3_2 = 0.86602540378443859658830206171842292;  // sqrt(3)/2

static const CGFloat kCZArrowDefaultWidth = 12;
static const CGFloat kCZArrowDefaultHeight = 4;
static const CGFloat kCZCircleMaxRadius = 5e4;

@interface CZElementCircle() {
    CGPoint _measurementTextPosition;
}

@property (nonatomic, retain) CZNodeEllipse *ellipseNode;
@property (nonatomic, assign) CGPoint pt1;
@property (nonatomic, assign) CGPoint pt2;
@property (nonatomic, assign) CGPoint pt3;

- (void)generateArrowPoints:(CGPoint[])points;

@end

@implementation CZElementCircle

+ (NSUInteger)featureCount {
    return CZElementCircleFeatureCount;
}

- (instancetype)init {
    return [self initWithCenter:CGPointZero radius:0];
}

- (instancetype)initWithCenter:(CGPoint)center radius:(CGFloat)radius {
    self = [super init];
    if (self) {
        _ellipseNode = [[CZNodeEllipse alloc] initWithCenterX:center.x
                                                      centerY:center.y
                                                      radiusX:radius
                                                      radiusY:radius];
        [self updateThreePoints];
    }
    return self;
}

- (void)dealloc {
    [_ellipseNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementCircle *newCircle = [super copyWithZone:zone];
    if (newCircle) {
        [newCircle->_ellipseNode release];
        newCircle->_ellipseNode = [self->_ellipseNode copyWithZone:zone];
        
        newCircle->_pt1 = self->_pt1;
        newCircle->_pt2 = self->_pt2;
        newCircle->_pt3 = self->_pt3;
    }
    
    return newCircle;
}

- (CZNode *)primaryNode {
    return _ellipseNode;
}

- (CGPoint)center {
    return CGPointMake(_ellipseNode.cx, _ellipseNode.cy);
}

- (void)setCenter:(CGPoint)center {
    _ellipseNode.cx = center.x;
    _ellipseNode.cy = center.y;
    self.featureValidated = NO;
    [self updateThreePoints];
}

- (CGFloat)radius {
    return _ellipseNode.rx;
}

- (void)setRadius:(CGFloat)radius {
    _ellipseNode.rx = radius;
    _ellipseNode.ry = radius;
    self.featureValidated = NO;
    [self updateThreePoints];
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return _pt1;
        case 1:
            return _pt2;
        case 2:
            return _pt3;
        default:
            return CGPointZero;
    }
}

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index {
    CGPoint backup;
    switch (index) {
        case 0:
            backup = _pt1;
            _pt1 = pt;
            break;
        case 1:
            backup = _pt2;
            _pt2 = pt;
            break;
        case 2:
            backup = _pt3;
            _pt3 = pt;
            break;
        default:
            return;
    }
    
    CGPoint center1;
    center1.x = (_pt1.x + _pt2.x) * 0.5;
    center1.y = (_pt1.y + _pt2.y) * 0.5;
    
    CGPoint center2;
    center2.x = (_pt3.x + _pt2.x) * 0.5;
    center2.y = (_pt3.y + _pt2.y) * 0.5;
    
    // rotate 90 degree on center 1
    CGFloat dx = _pt2.x - center1.x;
    CGFloat dy = _pt2.y - center1.y;
    CGPoint orthorPoint1;
    orthorPoint1.x = center1.x + dy;
    orthorPoint1.y = center1.y - dx;
    
    // rotate 90 degree on center 2
    dx = _pt2.x - center2.x;
    dy = _pt2.y - center2.y;
    CGPoint orthorPoint2;
    orthorPoint2.x = center2.x + dy;
    orthorPoint2.y = center2.y - dx;
    
    CGPoint cross = CZCalcCrossPoint(center1, orthorPoint1, center2, orthorPoint2);
    CGFloat radius = hypot(cross.x - _pt1.x, cross.y - _pt1.y);
    BOOL success = !isnan(cross.x) || !isnan(cross.y);
    if (radius > kCZCircleMaxRadius) {
        success = NO;
    }
    
    if (success) {
        _ellipseNode.cx = cross.x;
        _ellipseNode.cy = cross.y;
        _ellipseNode.rx = radius;
        _ellipseNode.ry = radius;
        self.featureValidated = NO;
    } else {
        switch (index) {
            case 0:
                _pt1 = backup;
                break;
            case 1:
                _pt2 = backup;
                break;
            case 2:
                _pt3 = backup;
                break;
            default:
                return;
        }
    }
}

#pragma mark - override CZElement methods

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementCircle *aCircle = (CZElementCircle *)anElement;
    
    return (CGPointEqualToPoint(aCircle.center, self.center) &&
            aCircle.radius == self.radius);
}

- (CALayer *)newLayer {
    _ellipseNode.lineWidth = [self suggestLineWidth];
    CALayer *layer = [_ellipseNode newLayer];
    return layer;
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    layer = [[CALayer alloc] init];
    
    CZFeature *areaFeature = [self.features objectAtIndex:CZElementCircleFeatureArea];
    CZFeature *perimeterFeature = [self.features objectAtIndex:CZElementCircleFeaturePerimeter];
    NSString *featureString = [NSString stringWithFormat:@"%@\n%@",
                               [areaFeature toLocalizedStringWithPrecision:[self precision]],
                               [perimeterFeature toLocalizedStringWithPrecision:[self precision]]];

    // area text
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:featureString];
    _measurementTextPosition = [self putMeasurementAtDefaultPosition:measurementText];
    CALayer *textlayer = [measurementText newLayer];
    [layer addSublayer:textlayer];
    [textlayer release];
    [measurementText release];
    measurementText = nil;
    
    // diameter line
    const NSUInteger kArrowPointCount = 10;
    CGPoint arrowPoints[kArrowPointCount];
    [self generateArrowPoints:arrowPoints];
    CZNodePolygon *arrowNode = [[CZNodePolygon alloc] initWithPointsCount:kArrowPointCount];
    arrowNode.fillColor = self.strokeColor;
    
    for (NSUInteger i = 0; i < kArrowPointCount; i++) {
        [arrowNode setPoint:arrowPoints[i] atIndex:i];
    }
    
    CALayer *lineLayer = [arrowNode newLayer];
    [layer addSublayer:lineLayer];
    [lineLayer release];
    [arrowNode release];
    
    // diameter text
    CZFeature *diameterFeature = [self.features objectAtIndex:CZElementCircleFeatureDiameter];    
    featureString = [diameterFeature toLocalizedStringWithPrecision:[self precision] showType:NO];
    measurementText = [self newMeasurementNodeWithString:featureString];
    CGPoint center = [_ellipseNode anchorPoint];
    measurementText.x = center.x - measurementText.width * 0.5;
    measurementText.y = center.y - measurementText.height - kCZMeasurementTextPadding;

    textlayer = [measurementText newLayer];
    [measurementText release];
    
    [layer addSublayer:textlayer];
    [textlayer release];
    return layer;
}

/** put the measurement text box beside the circle. The psuedo code as follow,
 *    textBox's bottom center := circle's top
 *    candidates[0] := textBox
 *    textBox's bottom left := circle's top right
 *    candidates[1] := textBox
 *    textBox's left center := circle's right
 *    candidates[2] := textBox
 *    textBox's top left := circle's bottom right
 *    candidates[3] := textBox
 *    textBox's top center := circle's bottom
 *    candidates[4] := textBox
 *    textBox's top right := circle's bottom left
 *    candidates[5] := textBox
 *    textBox's right center := circle's left
 *    candidates[6] := textBox
 *    textBox's bottom right := circle's top left
 *    candidates[7] := textBox
 *
 *    choose one position from candidates using another function
 */
- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement {
    const CGFloat sqrt2Radius = M_SQRT2 * self.radius / 2;
    const CGPoint center = self.center;
    const CGFloat radius = self.radius;
    
    NSMutableArray *positions = [NSMutableArray arrayWithCapacity:8];
    NSMutableArray *directions = [NSMutableArray arrayWithCapacity:8];
    
    CGPoint point;
    
    // top
    point.x = center.x;
    point.y = center.y - radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionBottom|kCZMeasurementDirectionCenter)];
    
    // top right
    point.x = center.x + sqrt2Radius;
    point.y = center.y - sqrt2Radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionBottom|kCZMeasurementDirectionLeft)];
    
    // right
    point.x = center.x + radius;
    point.y = center.y;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionCenter|kCZMeasurementDirectionLeft)];
    
    // bottom right
    point.x = center.x + sqrt2Radius;
    point.y = center.y + sqrt2Radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionTop|kCZMeasurementDirectionLeft)];
    
    // bottom
    point.x = center.x;
    point.y = center.y + radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionTop|kCZMeasurementDirectionCenter)];
    
    // bottom left
    point.x = center.x - sqrt2Radius;
    point.y = center.y + sqrt2Radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionTop|kCZMeasurementDirectionRight)];
    
    // left
    point.x = center.x - radius;
    point.y = center.y;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionCenter|kCZMeasurementDirectionRight)];
    
    // top left
    point.x = center.x - sqrt2Radius;
    point.y = center.y - sqrt2Radius;
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionBottom|kCZMeasurementDirectionRight)];
    
    return [self putMeasurement:measurement possiblePositions:positions directions:directions];
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    CGPoint center = CGPointMake(_ellipseNode.cx, _ellipseNode.cy);
    center = CGPointApplyAffineTransform(center, transform);
    _ellipseNode.cx = center.x;
    _ellipseNode.cy = center.y;
    
    _pt1 = CGPointApplyAffineTransform(_pt1, transform);
    _pt2 = CGPointApplyAffineTransform(_pt2, transform);
    _pt3 = CGPointApplyAffineTransform(_pt3, transform);
    
    CGSize radius = CGSizeMake(self.radius, self.radius);
    radius = CGSizeApplyAffineTransform(radius, transform);
    self.radius = radius.width;
    
    self.featureValidated = NO;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    NSDictionary *memo = @{@"cx": @(_ellipseNode.cx),
                           @"cy": @(_ellipseNode.cy),
                           @"r": @(self.radius)};
    return [memo retain];
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSNumber *cx = [memo objectForKey:@"cx"];
    NSNumber *cy = [memo objectForKey:@"cy"];
    NSNumber *radius = [memo objectForKey:@"r"];
    
    if ([cx isKindOfClass:[NSNumber class]] &&
        [cy isKindOfClass:[NSNumber class]] &&
        [radius isKindOfClass:[NSNumber class]]) {
        [self notifyWillChange];
        
        CGPoint center;
        center.x = [cx floatValue];
        center.y = [cy floatValue];
        self.center = center;
        self.radius = [radius floatValue];

        [self notifyDidChange];
        
        return YES;
    }
    
    return NO;
}

- (CGPoint)measurementTextPosition {
    return _measurementTextPosition;
}

#pragma mark - private methods

/*!
 generate double arrow line polygon points.
    9         6
   /| 8     7 |\
 0/ |---------| \
  \ |---------| / 5
   \| 2     3 |/
    1         4
*/
- (void)generateArrowPoints:(CGPoint[])arrowPoints {
    CGPoint center = self.center;
    CGFloat radius = _ellipseNode.rx;
    CGFloat lineWidth = [self suggestLineWidth];
    CGFloat scale = lineWidth;
    CGSize arrowSize;
    arrowSize.width = kCZArrowDefaultWidth * scale;
    arrowSize.height = kCZArrowDefaultHeight * scale;
    
    CGFloat halfLineWidth = lineWidth * 0.5;
    arrowPoints[0].x = center.x - radius;
    arrowPoints[0].y = center.y;
    arrowPoints[1].x = arrowPoints[0].x + arrowSize.width;
    arrowPoints[1].y = center.y - arrowSize.height;
    arrowPoints[2].x = arrowPoints[1].x;
    arrowPoints[2].y = center.y - halfLineWidth;
    arrowPoints[3].x = center.x + radius - arrowSize.width;
    arrowPoints[3].y = arrowPoints[2].y;
    arrowPoints[4].x = arrowPoints[3].x;
    arrowPoints[4].y = arrowPoints[1].y;
    arrowPoints[5].x = center.x + radius;
    arrowPoints[5].y = center.y;
    
    // flip upside-down
    for (NSUInteger i = 1; i < 5; i++) {
        arrowPoints[10 - i].x = arrowPoints[i].x;
        arrowPoints[10 - i].y = center.y - (arrowPoints[i].y - center.y);
    }
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];

    CGFloat scaling = [self scalingOfUnit:unitStyle];
    
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    NSString *unit2d = [self unitNameOfAreaByStyle:unitStyle];

    CGFloat scaledCx = scaling * _ellipseNode.cx;
    CGFloat scaledCy = scaling * _ellipseNode.cy;
    CGFloat scaledR = scaling * _ellipseNode.rx;
    
#if CALCULATE_AERA_USE_CRACK_ALGO
    const CGFloat maxSize = 30000;
    CZGemMeasure *measurer = [[CZGemMeasure alloc] init];
	
    CGFloat scaledArea = 0;
    CGFloat scaledPerimeter = 0;

    if (_ellipseNode.rx * 2 < maxSize) {  // refuse to calculate area, if cirle is bigger than parent frame
        const CGFloat logicalScaling = self.logicalScaling;
        const CGFloat scalingOfLogicalPoint = scaling / logicalScaling;
        [measurer setCircle:_ellipseNode.cx * logicalScaling
                    centerY:_ellipseNode.cy * logicalScaling
                     radius:_ellipseNode.rx * logicalScaling];
        scaledArea = [measurer area] * scalingOfLogicalPoint * scalingOfLogicalPoint;
        scaledPerimeter = [measurer perimeter] * scalingOfLogicalPoint;
    }
    
    [measurer release];
#else
    CGFloat scaledArea = M_PI * scaledR * scaledR;
    CGFloat scaledPerimeter = M_PI * scaledR * 2;
#endif

    [self removeAllFeatures];
    
    // NOTICE: keep order as in CZElementCircleFeature
    // area measurement
    CZFeature *areaFeature = [[CZFeature alloc] initWithValue:scaledArea unit:unit2d type:kCZFeatureTypeArea];
    [self appendFeature:areaFeature];
    [areaFeature release];

    // center x measuremnt
    CZFeature *centerXFeature = [[CZFeature alloc] initWithValue:scaledCx unit:unit1d type:kCZFeatureTypeCenterX];
    [self appendFeature:centerXFeature];
    [centerXFeature release];
    
    // center y measuremnt
    CZFeature *centerYFeature = [[CZFeature alloc] initWithValue:scaledCy unit:unit1d type:kCZFeatureTypeCenterY];
    [self appendFeature:centerYFeature];
    [centerYFeature release];
    
    // diameter
    CZFeature *diameterFeature = [[CZFeature alloc] initWithValue:scaledR * 2 unit:unit1d type:kCZFeatureTypeDiameter];
    [self appendFeature:diameterFeature];
    [diameterFeature release];
    
    // perimeter
    CZFeature *perimeterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypePerimeter];
    [self appendFeature:perimeterFeature];
    [perimeterFeature release];
}

- (void)updateThreePoints {
    // put 3 points to default position on Circle
    _pt1.x = _ellipseNode.cx;
    _pt1.y = _ellipseNode.cy - self.radius;
    _pt2.x = _ellipseNode.cx + self.radius * CZ_MATH_SQRT3_2;
    _pt2.y = _ellipseNode.cy + self.radius * 0.5;
    _pt3.x = _ellipseNode.cx - self.radius * CZ_MATH_SQRT3_2;
    _pt3.y = _ellipseNode.cy + self.radius * 0.5;
}

@end
