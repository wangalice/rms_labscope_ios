//
//  CZElementPolygon.h
//  Hermes
//
//  Created by Jin Ralph on 13-3-23.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiPoint.h"

enum {
    CZElementPolygonFeatureArea = 0,
    CZElementPolygonFeaturePerimeter,
    
    CZElementPolygonFeatureCount
};

@interface CZElementPolygon : CZElementMultiPoint
@end
