//
//  CZElementPolygon.m
//  Hermes
//
//  Created by Jin Ralph on 13-3-23.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiPoint.h"
#import "CZNodeGeom.h"
#import "CZNodeMultiPointShape.h"
#import "CZNodeLine.h"
#import "CZNodeRectangle.h"
#import "CZElementLayer.h"

enum {
    CZElementPolygonFeatureArea = 0,
    CZElementPolygonFeaturePerimeter,
    
    CZElementPolygonFeatureCount
};

@interface CZElementMultiPoint ()

@property (nonatomic, retain) CZNodeMultiPointShape *shapeNode;
@property (nonatomic, retain) CZNodeLine *leftTick;
@property (nonatomic, retain) CZNodeLine *rightTick;

@end

@implementation CZElementMultiPoint

+ (CGFloat)calcLengthOfPoints:(const CGPoint *)pointsData pointsCount:(NSUInteger)pointsCount {
    CGFloat perimeter = 0.0f;
    CGPoint lastPoint = pointsCount > 0 ? pointsData[0] : CGPointZero;
    for (NSUInteger i = 1; i < pointsCount; i++) {
        perimeter += hypot(pointsData[i].x - lastPoint.x, pointsData[i].y - lastPoint.y);
        lastPoint = pointsData[i];
    }
    
    return perimeter;
}

+ (CGFloat)calcAreaOfPolygonPoints:(const CGPoint *)pointsData pointsCount:(NSUInteger)pointsCount {
    CGFloat area = 0;
    if (pointsCount >= 3) {
        // calculate if the polygon is self intersection
        BOOL isSelfIntersection = NO;
        if (pointsCount > 3) {
            CGPoint line1p1 = pointsData[pointsCount - 1];
            for (NSUInteger i = 0; i < pointsCount - 1; i++) {
                CGPoint line1p2 = pointsData[i];
                CGPoint line2p1 = pointsData[i + 1];
                
                NSUInteger jCount = (i == 0) ? pointsCount - 1 : pointsCount;  // skip the first line and last line
                for (NSUInteger j = i + 2; j < jCount; j++) {
                    CGPoint line2p2 = pointsData[j];
                    if (CZLineSegmentsAreCrossed(line1p1, line1p2, line2p1, line2p2)) {
                        isSelfIntersection = YES;
                        break;
                    }
                    line2p1 = line2p2;
                }
                
                if (isSelfIntersection) {
                    break;
                }
                
                line1p1 = line1p2;
            }
        }
        
        if (!isSelfIntersection) {  // calculate simple polygon area using Surveyor's Formula
            CGPoint lastPoint = pointsData[pointsCount - 1];
            for (NSUInteger i = 0; i < pointsCount; i++) {
                CGPoint point = pointsData[i];
                area += lastPoint.x * point.y - lastPoint.y * point.x;
                lastPoint = point;
            }
            area = fabs(area) / 2;
        } else {
            area = -1;  // failed to calculate area, so set a minus value for indicator.
        }
    }
    
    return area;
}

+ (Class)nodeClass {
    return [CZNodeMultiPointShape class];
}

+ (BOOL)hasTickes {
    return NO;
}

+ (NSUInteger)minimumPointsCount {
    return 1;
}

- (instancetype)init {
    return [self initWithPointsCount:0];
}

- (instancetype)initWithPointsCount:(NSUInteger)count {
    self = [super init];
    if (self) {
        _shapeNode = [[[self.class nodeClass] alloc] initWithPointsCount:count];
    }
    return self;
}

- (NSUInteger)minimumPointsCount {
    return [[self class] minimumPointsCount];
}

- (void)dealloc {
    [_leftTick release];
    [_rightTick release];
    [_shapeNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementMultiPoint *newElement = [super copyWithZone:zone];
    if (newElement) {
        [newElement->_shapeNode release];
        newElement->_shapeNode = [self->_shapeNode copyWithZone:zone];
        
        if ([newElement->_shapeNode subNodeCount] > 0) {
            newElement.leftTick = (CZNodeLine *)[newElement->_shapeNode subNodeAtIndex:0];
        }
        
        if ([newElement->_shapeNode subNodeCount] > 1) {
            newElement.rightTick = (CZNodeLine *)[newElement->_shapeNode subNodeAtIndex:1];
        }
    }
    
    return newElement;
}

- (NSUInteger)pointsCount {
    return [_shapeNode pointsCount];
}

- (const CGPoint *)points {
    return [_shapeNode pointArray];
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    return [_shapeNode pointAtIndex:index];
}

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index {
    self.featureValidated = NO;
    [_shapeNode setPoint:pt atIndex:index];
}

- (void)appendPoint:(CGPoint)pt {
    self.featureValidated = NO;
    [_shapeNode appendPoint:pt];
}

- (void)removePointAtIndex:(NSUInteger)index {
    self.featureValidated = NO;
    [_shapeNode removePointAtIndex:index];
}

#pragma mark - override CZElement

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementMultiPoint *aMultiPoint = (CZElementMultiPoint *)anElement;
    
    if ([self pointsCount] != [aMultiPoint pointsCount]) {
        return NO;
    } else if ([self pointsCount] == 0) {  // special case, no point treat as not equal; Otherwise cascade element logic won't work
        return NO;
    }

    for (NSUInteger i = 0; i < [self pointsCount]; ++i) {
        CGPoint pt = [self pointAtIndex:i];
        CGPoint pt2 = [aMultiPoint pointAtIndex:i];
        if (!CGPointEqualToPoint(pt, pt2)) {
            return NO;
        }
    }
    
    return YES;
}

- (CZNode *)primaryNode {
    return _shapeNode;
}

- (CALayer *)newLayer {
    _shapeNode.lineWidth = [self suggestLineWidth];
    
    if ([self.class hasTickes]) {
        [self updateTicks];
    }
    
    CALayer *layer = [_shapeNode newLayer];
    return layer;
}

- (BOOL)hasMeasurement {
    return YES;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    for (NSUInteger i = 0; i < [self pointsCount]; ++i) {
        CGPoint pt = [self pointAtIndex:i];
        pt = CGPointApplyAffineTransform(pt, transform);
        [self setPoint:pt atIndex:i];
    }

    self.featureValidated = NO;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    NSMutableArray *pointArray = [[NSMutableArray alloc] initWithCapacity:[self pointsCount]];
    for (NSUInteger i = 0; i < [self pointsCount]; i++) {
        CGPoint p = [self pointAtIndex:i];
        [pointArray addObject:@(p.x)];
        [pointArray addObject:@(p.y)];
    }
    NSDictionary *memo = [[NSDictionary alloc] initWithObjectsAndKeys:pointArray, @"CGPointArray", nil];
    [pointArray release];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSArray *pointArray = [memo objectForKey:@"CGPointArray"];
    if ([pointArray isKindOfClass:[NSArray class]]) {
        [self notifyWillChange];
        
        NSUInteger pointsCount = [pointArray count] / 2;
        while (pointsCount < [self pointsCount]) {
            NSUInteger lastIndex = [self pointsCount] - 1;
            [self removePointAtIndex:lastIndex];
        }
        
        NSUInteger i;
        for (i = 0; i < [self pointsCount]; i++) {
            NSNumber *x = pointArray[i * 2];
            NSNumber *y = pointArray[i * 2 + 1];
            CGPoint pt;
            pt.x = [x floatValue];
            pt.y = [y floatValue];
            [self setPoint:pt atIndex:i];
        }
        
        for (; i < pointsCount; i++) {
            NSNumber *x = pointArray[i * 2];
            NSNumber *y = pointArray[i * 2 + 1];
            CGPoint pt;
            pt.x = [x floatValue];
            pt.y = [y floatValue];
            [self appendPoint:pt];
        }
        
        self.featureValidated = NO;
        
        [self notifyDidChange];
        
        return YES;
    }
    
    return NO;
}

/** put the measurement text box besize multi-point shape, for example polygon. The psuedo code as follow,
 *    boundary := polygon's boundary
 *    center := center of boundary
 *    topLeftArray, topRightArray, bottomLeftArray, bottomRightArray
 *    For point in polygon's corners do
 *        If point in top left block then
 *            topLeftArray add point
 *        Else If point at top right block then
 *            topRightArray add point
 *        Else If point at bottom right block then
 *            bottomRightArray add point
 *        Else
 *            bottomLeftArray add point
 *    end
 *    topLeft := most top left in topLeftArray
 *    topRight := most top right in topRightArray
 *    bottomLeft := most bottom left point in bottomLeftArray
 *    bottomRight := most bottom right point in bottomRightArray
 *    If topLeft != NULL then
 *        cadidatesArray add topLeft
 *    If topRight != NULL then
 *        cadidatesArray add topRight
 *    If bottomLeft != NULL then
 *        cadidatesArray add bottomLeft
 *    If bottomRight != NULL then
 *        cadidatesArray add bottomRight
 *
 *    call super class's method, put measurement at one of possible positions in cadidatesArray
 */
- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement {
    if ([self pointsCount] == 0) {
        return CGPointZero;
    }
    
    const CGRect frame = [[self primaryNode] frame];
    const CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));

    NSUInteger areaIndex[4];  // top left, top right, bottom left, bottom right
    for (int i = 0; i < 4; ++i) {
        areaIndex[i] = NSNotFound;
    }
    
    const CGPoint *points = [self points];
    
    for (NSUInteger i = 0; i < [self pointsCount]; ++i) {
        int col = (points[i].x < center.x) ? 0 : 1;
        int row = (points[i].y < center.y) ? 0 : 1;
        
        int j = row * 2 + col;
        if (areaIndex[j] == NSNotFound) {
            areaIndex[j] = i;
        } else {
            NSUInteger oldId = areaIndex[j];
            switch (j) {
                case 0:  // top left
                    if ((points[i].y + points[i].x) < (points[oldId].y + points[oldId].x)) {
                        areaIndex[j] = i;
                    }
                    break;
                case 1:  // top right
                    if ((points[i].y - points[i].x) < (points[oldId].y - points[oldId].x)) {
                        areaIndex[j] = i;
                    }
                    break;
                case 2: // bottom left
                    if ((points[i].y - points[i].x) > (points[oldId].y - points[oldId].x)) {
                        areaIndex[j] = i;
                    }
                    break;
                case 3: // bottom right
                    if ((points[i].y + points[i].x) > (points[oldId].y + points[oldId].x)) {
                        areaIndex[j] = i;
                    }
                default:
                    break;
            }
        }
    }
    
    // put measurement text
    NSMutableArray *positions = [NSMutableArray arrayWithCapacity:4];
    NSMutableArray *directions = [NSMutableArray arrayWithCapacity:4];
    
    NSUInteger vDirections[] = {
        kCZMeasurementDirectionBottom,
        kCZMeasurementDirectionTop};
    
    NSUInteger hDirections[] = {
        kCZMeasurementDirectionRight,
        kCZMeasurementDirectionLeft};
    
    for (int j = 0, row = 0; row < 2; ++row) {
        for (int col = 0; col < 2; ++col) {
            if (areaIndex[j] != NSNotFound) {
                NSUInteger i = areaIndex[j];
                [positions addObject:[NSValue valueWithCGPoint:points[i]]];
                [directions addObject:@(vDirections[row]|hDirections[col])];
            }
            ++j;
        }
    }
    
    return [self putMeasurement:measurement possiblePositions:positions directions:directions];
}

#pragma mark - Private Methods

- (CZNodeLine *)leftTick {
    if (_leftTick == nil) {
        _leftTick = [[CZNodeLine alloc] init];
    }
    return _leftTick;
}

- (CZNodeLine *)rightTick {
    if (_rightTick == nil) {
        _rightTick = [[CZNodeLine alloc] init];
    }
    return _rightTick;
}

- (void)updateTicks {
    CZNodeMultiPointShape *primaryNode = self.shapeNode;
    
    if (!self.hasMeasurement || self.isMeasurementHidden || primaryNode.pointsCount < 2) {
        [_leftTick removeFromParent];
        [_rightTick removeFromParent];
        
        self.leftTick = nil;
        self.rightTick = nil;
    } else {
        [primaryNode addSubNode:self.leftTick];        
        [primaryNode addSubNode:self.rightTick];
        
        _leftTick.lineWidth = primaryNode.lineWidth;
        _leftTick.strokeColor = primaryNode.strokeColor;
        
        _rightTick.lineWidth = primaryNode.lineWidth;
        _rightTick.strokeColor = primaryNode.strokeColor;
        
        CGPoint p1 = [primaryNode pointAtIndex:0];
        CGPoint p2 = [primaryNode pointAtIndex:1];
        
        CGPoint p3 = [primaryNode pointAtIndex:[primaryNode pointsCount] - 2];
        CGPoint p4 = [primaryNode pointAtIndex:[primaryNode pointsCount] - 1];
        
        CGPoint topLeft = CGPathGetBoundingBox(primaryNode.relativePath).origin;
        p1.x -= topLeft.x;
        p1.y -= topLeft.y;
        p2.x -= topLeft.x;
        p2.y -= topLeft.y;
        p3.x -= topLeft.x;
        p3.y -= topLeft.y;
        p4.x -= topLeft.x;
        p4.y -= topLeft.y;
        
        CGFloat tickHeight = 6.0 * [self suggestLineWidth];
        
        _leftTick.p1 = CZCalcOuterPoint(p2, p1, tickHeight);
        _leftTick.p2 = CZCalcOuterPoint(p2, p1, -tickHeight);
        
        _rightTick.p1 = CZCalcOuterPoint(p3, p4, tickHeight);
        _rightTick.p2 = CZCalcOuterPoint(p3, p4, -tickHeight);
    }
}

@end
