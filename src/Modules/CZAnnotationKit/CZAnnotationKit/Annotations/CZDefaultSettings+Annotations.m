//
//  CZDefaultSettings+Annotations.m
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import "CZDefaultSettings+Annotations.h"
#import "CZColorGenerator.h"

NSString * const CZDisableMeasurementColorCodingDidChangeNotification = @"CZDisableMeasurementColorCodingDidChangeNotification";

@implementation CZDefaultSettings (Annotations)

- (CZColor)color {
    CZColor color = kCZColorRed;
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultColor];
    if ([valueString isEqualToString:@"0"]) {
        color = kCZColorRed;
    } else if ([valueString isEqualToString:@"1"]) {
        color = kCZColorGreen;
    } else if ([valueString isEqualToString:@"2"]) {
        color = kCZColorBlue;
    } else if ([valueString isEqualToString:@"3"]) {
        color = kCZColorBlack;
    } else if ([valueString isEqualToString:@"4"]) {
        color = kCZColorYellow;
    }
    return color;
}

- (void)setColor:(CZColor)color {
    NSString *valueString;
    if (CZColorEqualToColor(color, kCZColorRed)) {
        valueString = @"0";
    } else if (CZColorEqualToColor(color, kCZColorGreen)) {
        valueString = @"1";
    } else if (CZColorEqualToColor(color, kCZColorBlue)) {
        valueString = @"2";
    } else if (CZColorEqualToColor(color, kCZColorBlack)) {
        valueString = @"3";
    } else if (CZColorEqualToColor(color, kCZColorYellow)) {
        valueString = @"4";
    } else {
        valueString = @"0";
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:valueString forKey:kDefaultColor];
}

- (CZColor)backgroundColor {
    CZColor backgroundColor = kCZColorTransparent;
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultBackgroundColor];
    if ([valueString isEqualToString:@"0"]) {
        backgroundColor = kCZColorTransparent;
    } else if ([valueString isEqualToString:@"1"]) {
        backgroundColor = kCZColorRed;
    } else if ([valueString isEqualToString:@"2"]) {
        backgroundColor = kCZColorBlue;
    } else if ([valueString isEqualToString:@"3"]) {
        backgroundColor = kCZColorGreen;
    } else if ([valueString isEqualToString:@"4"]) {
        backgroundColor = kCZColorWhite;
    } else if ([valueString isEqualToString:@"5"]) {
        backgroundColor = kCZColorBlack;
    }
    return backgroundColor;
}

- (void)setBackgroundColor:(CZColor)backgroundColor {
    NSString *valueString;
    if (CZColorEqualToColor(backgroundColor, kCZColorTransparent)) {
        valueString = @"0";
    } else if (CZColorEqualToColor(backgroundColor, kCZColorRed)) {
        valueString = @"1";
    } else if (CZColorEqualToColor(backgroundColor, kCZColorBlue)) {
        valueString = @"2";
    } else if (CZColorEqualToColor(backgroundColor, kCZColorGreen)) {
        valueString = @"3";
    } else if (CZColorEqualToColor(backgroundColor, kCZColorWhite)) {
        valueString = @"4";
    } else if (CZColorEqualToColor(backgroundColor, kCZColorBlack)) {
        valueString = @"5";
    } else {
        valueString = @"0";
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:valueString forKey:kDefaultBackgroundColor];
}

- (CZElementSize)elementSize {
    CZElementSize size = CZElementSizeMedium;
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultSize];
    if ([valueString isEqualToString:@"0"]) {
        size = CZElementSizeExtraSmall;
    } else if ([valueString isEqualToString:@"1"]) {
        size = CZElementSizeSmall;
    } else if ([valueString isEqualToString:@"2"]) {
        size = CZElementSizeMedium;
    } else if ([valueString isEqualToString:@"3"]) {
        size = CZElementSizeLarge;
    } else if ([valueString isEqualToString:@"4"]) {
        size = CZElementSizeExtraLarge;
    } else {
        size = CZElementSizeMedium;
    }
    return size;
}

- (void)setElementSize:(CZElementSize)elementSize {
    NSString *string;
    switch (elementSize) {
        case CZElementSizeExtraSmall:
            string = @"0";
            break;
        case CZElementSizeSmall:
            string = @"1";
            break;
        case CZElementSizeMedium:
            string = @"2";
            break;
        case CZElementSizeLarge:
            string = @"3";
            break;
        case CZElementSizeExtraLarge:
            string = @"4";
            break;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:string forKey:kDefaultSize];
}

- (CZScaleBarDisplayMode)scaleBarDisplayMode {
    CZScaleBarDisplayMode position;
    
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kDefaultScaleBarCorner];
    if ([valueString isEqualToString:@"1"]) {
        position = kCZScaleBarPositionHidden;
    } else {
        position = kCZScaleBarPositionLastPosition;
    }
    
    return position;
}

- (void)setScaleBarDisplayMode:(CZScaleBarDisplayMode)scaleBarPosition {
    if (self.scaleBarDisplayMode == scaleBarPosition) {
        return;
    }
    // last position as default
    NSString *valueString = @"0";
    switch (scaleBarPosition) {
        case kCZScaleBarPositionLastPosition: {
            valueString = @"0";
        }
            break;
        case kCZScaleBarPositionHidden: {
            valueString = @"1";
        }
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] setObject:valueString forKey:kDefaultScaleBarCorner];
}

- (CZElementUnitStyle)unitStyle {
    CZElementUnitStyle unitStyle;
    
    NSString *valueString = [[NSUserDefaults standardUserDefaults] stringForKey:kMeasurementUnit];
    NSInteger index = [valueString integerValue];
    switch (index) {
        case 0:
            unitStyle = CZElementUnitStyleMicrometer;
            break;
        case 1:
            unitStyle = CZElementUnitStyleMil;
            break;
        case 2:
            unitStyle = CZElementUnitStyleMM;
            break;
        case 3:
            unitStyle = CZElementUnitStyleInch;
            break;
        case 4: // fallthrough
        default: {
            unitStyle = CZElementUnitStyleAuto;
            break;
        }
    }
    
    return unitStyle;
}

- (void)setUnitStyle:(CZElementUnitStyle)unitStyle {
    switch (unitStyle) {
        case CZElementUnitStyleMicrometer:
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleMil:
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleMM:
            [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleInch:
            [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleMetricAuto:
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleImperialAuto:
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kMeasurementUnit];
            break;
        case CZElementUnitStyleAuto: {
            [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:kMeasurementUnit];
            break;
        }
    }
}

- (BOOL)enableMeasurement {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kMeasurementEnabled];
}

- (void)setEnableMeasurement:(BOOL)enableMeasurement {
    [[NSUserDefaults standardUserDefaults] setBool:enableMeasurement forKey:kMeasurementEnabled];
}

- (BOOL)disableMeasurementColorCoding {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kMeasurementColorCodingDisabled];
}

- (void)setDisableMeasurementColorCoding:(BOOL)disableMeasurementColorCoding {
    [[NSUserDefaults standardUserDefaults] setBool:disableMeasurementColorCoding forKey:kMeasurementColorCodingDisabled];
    [[CZColorGenerator sharedInstance] setDisableMeasurementColorCoding:disableMeasurementColorCoding];
    [[NSNotificationCenter defaultCenter] postNotificationName:CZDisableMeasurementColorCodingDidChangeNotification object:self userInfo:nil];
}

@end
