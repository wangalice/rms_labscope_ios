//
//  CZDefaultSettings+Annotations.h
//  Hermes
//
//  Created by Li, Junlin on 12/21/18.
//  Copyright © 2018 Carl Zeiss. All rights reserved.
//

#import <CZToolbox/CZToolbox.h>
#import "CZColor.h"
#import "CZElement.h"
#import "CZElementScaleBar.h"

/*! default color of new created annotation; in system setting. since v1 [integer]
 *
 * 0 ... red (default) \n
 * 1 ... green \n
 * 2 ... blue \n
 * 3 ... black \n
 * 4 ... yellow \n
 */
#define kDefaultColor @"default_color"

/*! default background color of measurement/text annotation; in system setting. since v6 [integer]
 *
 * 0 ... transparent (default) \n
 * 1 ... red \n
 * 2 ... blue \n
 * 3 ... green \n
 * 4 ... white \n
 * 5 ... black \n
 */
#define kDefaultBackgroundColor @"default_background_color"

/*! default annotation size of new created annotation; in system setting. since v1 [integer]
 *
 * 0 ... XS \n
 * 1 ... S \n
 * 2 ... M (default) \n
 * 3 ... L \n
 * 4 ... XL \n
 */
#define kDefaultSize @"default_size"

#define kDefaultScaleBarCorner @"default_scalebar_corner"

/*! Default display unit of annotation. in system setting. since v1 [integer]
 *
 * 0 ... micrometer \n
 * 1 ... mil/thou \n
 * 2 ... millimeter ........... since v2 \n
 * 3 ... inch ................. since v2 \n
 * 4 ... metric auto mode ..... since v5 ~ v7 \n
 * 5 ... imperial auto mode ... since v5 ~ v7 \n
 * 4 ... fully auto mode ...... since v8 \n
 */
#define kMeasurementUnit @"measurement_unit"

/*! Enable/disable displaying measurement of new created annotation. in system setting. since v1 [boolean]*/
#define kMeasurementEnabled @"measurement_enabled"

/*! Enable/disable measurement ID background color. in system setting. since v5 [boolean]*/
#define kMeasurementColorCodingDisabled @"measurement_color_coding_disabled"

extern NSString * const CZDisableMeasurementColorCodingDidChangeNotification;

@interface CZDefaultSettings (Annotations)

@property (nonatomic, readwrite) CZColor color;
@property (nonatomic, readwrite) CZColor backgroundColor;
@property (nonatomic, readwrite) CZElementSize elementSize;
@property (nonatomic, readwrite) CZScaleBarDisplayMode scaleBarDisplayMode;
@property (nonatomic, readwrite) CZElementUnitStyle unitStyle;
@property (nonatomic, readwrite) BOOL enableMeasurement;
@property (nonatomic, readwrite) BOOL disableMeasurementColorCoding;

@end
