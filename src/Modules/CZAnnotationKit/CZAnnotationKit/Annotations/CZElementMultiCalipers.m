//
//  CZElementMultiCalipers.m
//  Matscope
//
//  Created by sherry on 4/7/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiCalipers.h"
#import "CZElementSubclass.h"
#import "CZNodeLineSegments.h"
#import "CZNodeGeom.h"
#import "CZNodeLine.h"
#import "CZFeature.h"
#import "CZElementCommon.h"

@interface CZElementMultiCalipers () {
    NSUInteger _pointCount;
}

@property (nonatomic, retain) CZNodeLine *baseLine;
@property (nonatomic, assign) CGPoint p1;
@property (nonatomic, assign) CGPoint p2;

@end

@implementation CZElementMultiCalipers

+ (Class)nodeClass {
    return [CZNodeLineSegments class];
}

+ (NSUInteger)minimumPointsCount {
    return 3;
}

+ (BOOL)hasTickes {
    return NO;  // multicalipers handle tickes by itself.
}

- (void)dealloc {
    [_baseLine release];
    
    [super dealloc];
}

- (instancetype)copyWithZone:(NSZone *)zone {
    CZElementMultiCalipers *newMultiCalipers = [super copyWithZone:zone];
    
    if (newMultiCalipers) {
        newMultiCalipers->_pointCount = self->_pointCount;
        newMultiCalipers->_p1 = self->_p1;
        newMultiCalipers->_p2 = self->_p2;
        
        CZNode *primaryNode = [newMultiCalipers primaryNode];
        if ([primaryNode subNodeCount] > 0) {
            newMultiCalipers.baseLine = (CZNodeLine *)[primaryNode subNodeAtIndex:0];
        }
    }
    
    return newMultiCalipers;
}

- (CZNodeLineSegments *)lineNode {
    return (CZNodeLineSegments *)[self primaryNode];
}

- (CZNodeLine *)baseLine {
    if (_baseLine == nil) {
        _baseLine = [[CZNodeLine alloc] init];
        _baseLine.dash = YES;
    }
    return _baseLine;
}

- (void)setP1:(CGPoint)p1 {
    _p1 = p1;
    self.featureValidated = NO;
}

- (void)setP2:(CGPoint)p2{
    _p2 = p2;
    self.featureValidated = NO;
}

- (CGPoint)othorPointAtIndex:(NSUInteger)index {
    [self updateFeatures];
    if (index >= 2) {
        NSUInteger i = (index - 2) * 4 + 1;
        return [super pointAtIndex:i];
    }
    return CGPointZero;
}

#pragma mark - Override CZElement

- (void)setMeasurementHidden:(BOOL)measurementHidden {
    [super setMeasurementHidden:measurementHidden];
    self.featureValidated = NO;
}

#pragma mark - Override CZElementMultiPoint

- (CALayer *)newLayer {
    [self updateFeatures];
    
    self.baseLine.lineWidth = self.suggestLineWidth;
    self.baseLine.strokeColor = self.primaryNode.strokeColor;
    
    return [super newLayer];
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    layer = [[CALayer alloc] init];
    CGFloat tickHeight = 6.0 * [self suggestLineWidth];
    CZNodeLineSegments *lineNode = (CZNodeLineSegments *)self.primaryNode;
    const NSUInteger pointsCount = [lineNode pointsCount];
    for (NSUInteger i = 0, j = 0; i < pointsCount; i += 4, j++) {
        CZFeature *feature = self.features.count > j ? self.features[j] : nil;
        NSString *feastureString = [feature toLocalizedStringWithPrecision:[self precision] showType:NO];
        NSString *subscriptString = [NSString stringWithFormat:@"%ld: ", (long)j];
        NSString *combinedString = [subscriptString stringByAppendingString:feastureString];
        CZNodeRectangle *background = [self newMeasurementNodeWithString:combinedString];
        
        CGPoint pt = [lineNode pointAtIndex:i];
        CGPoint orthoPoint = [lineNode pointAtIndex:i + 1];
        
        [CZElementCommon putMeasurement:background onLine:orthoPoint secondPoint:pt distance:-(tickHeight + 2)];
        
        // Create CALayer for rendering
        CALayer *textlayer = [background newLayer];
        [layer addSublayer:textlayer];
        [background release];
        [textlayer release];
    }
    return layer;
}

#pragma mark - Private methods

// TODO: refactor common code with CZElementLine
- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    
    CGFloat tickHeight = 6.0 * [self suggestLineWidth];
    
    [self removeAllFeatures];
    
    CZNodeLineSegments *lineNode = (CZNodeLineSegments *)self.primaryNode;
    const NSUInteger pointsCount = [lineNode pointsCount];
    for (NSUInteger i = 0, j = 0; i < pointsCount; i += 4, j++) {
        CGPoint pt = [lineNode pointAtIndex:i];
        CGPoint othorPoint = CalcOrthoPoint(self.p1, self.p2, pt);
        [lineNode setPoint:othorPoint atIndex:i + 1];
        
        if (self.isMeasurementHidden) {
            [lineNode setPoint:CGPointZero atIndex:i + 2];
            [lineNode setPoint:CGPointZero atIndex:i + 3];
        } else {
            CGPoint leftTickp1 = CZCalcOuterPoint(othorPoint, pt, tickHeight);
            CGPoint leftTickp2 = CZCalcOuterPoint(othorPoint, pt, -tickHeight);
            [lineNode setPoint:leftTickp1 atIndex:i + 2];
            [lineNode setPoint:leftTickp2 atIndex:i + 3];
        }

        CGFloat distance = CZLengthOfLine(othorPoint, pt);
        CGFloat scaledDistance = scaling * distance;
        
        CZFeature *feature = [[CZFeature alloc] initWithValue:scaledDistance unit:unit1d type:kCZFeatureTypeDistance];
        [self appendFeature:feature];
        [feature release];
    }
    
    [lineNode addSubNode:self.baseLine];
    
    CGRect lineNodeBox = CGPathGetBoundingBox([lineNode relativePath]);
    CGPoint offset = lineNodeBox.origin;
    CGPoint point1, point2;
    point1.x = _p1.x - offset.x;
    point1.y = _p1.y - offset.y;
    point2.x = _p2.x - offset.x;
    point2.y = _p2.y - offset.y;
    
    self.baseLine.p1 = point1;
    self.baseLine.p2 = point2;
}

#pragma mark - CZElementMultiPointKind methods

- (NSUInteger)minimumPointsCount {
    return 3;
}

- (NSUInteger)pointsCount {
    return _pointCount + [super pointsCount] / 4 ;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    if (index == 0) {
        return self.p1;
    } if (index == 1) {
        return self.p2;
    } else {
        NSUInteger i = (index - 2) * 4;
        return [super pointAtIndex:i];
    }
}

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index {
    self.featureValidated = NO;
    
    if (index == 0) {
        self.p1 = pt;
    } else if (index == 1) {
        self.p2 = pt;
    } else {
        NSUInteger i = (index - 2) * 4;
        [super setPoint:pt atIndex:i];
    }
}

- (void)appendPoint:(CGPoint)pt {
    self.featureValidated = NO;
    
    if (_pointCount >= 2) {
        [super appendPoint:pt];
        for (int i = 1; i < 4; i++) {
            [super appendPoint:CGPointZero];
        }
    } else {
        [self setPoint:pt atIndex:_pointCount];
        ++_pointCount;
    }
}

- (void)removePointAtIndex:(NSUInteger)index {
    self.featureValidated = NO;
    
    if (_pointCount && index < 2) {
        --_pointCount;
    } else {
        NSUInteger i = (index - 2) * 4;
        for (int j = 0; j < 4; j++) {
            [super removePointAtIndex:i];
        }
    }
}

@end
