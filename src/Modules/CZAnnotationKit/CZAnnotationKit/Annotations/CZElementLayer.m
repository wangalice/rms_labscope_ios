//
//  CZElementLayer.m
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayer.h"
#import <QuartzCore/QuartzCore.h>
#import <CZToolbox/CZToolbox.h>
#import "CZElement.h"
#import "CZElementGroup.h"
#import "CZElementScaleBar.h"
#import "CZFeature.h"

const static CGFloat kLineWidthExtraSmall = 0.5;
const static CGFloat kLineWidthSmall = 1;
const static CGFloat kLineWidthMedium = 2;
const static CGFloat kLineWidthLarge = 4;
const static CGFloat kLineWidthExtraLarge = 6;
const static CGFloat kLineWidthExtraSmallThreshold = (kLineWidthExtraSmall + kLineWidthSmall) * 0.5;
const static CGFloat kLineWidthSmallThreshold = (kLineWidthSmall + kLineWidthMedium) * 0.5;
const static CGFloat kLineWidthMediumThreshold = (kLineWidthMedium + kLineWidthLarge) * 0.5;
const static CGFloat kLineWidthLargeThreshold = (kLineWidthLarge + kLineWidthExtraLarge) * 0.5;

const static CGFloat kFontSizeExtraSmall = 9;
const static CGFloat kFontSizeSmall = 14;
const static CGFloat kFontSizeMedium = 18;
const static CGFloat kFontSizeLarge = 32;
const static CGFloat kFontSizeExtraLarge = 48;
const static CGFloat kFontSizeExtraSmallThreshold = (kFontSizeExtraSmall + kFontSizeSmall) * 0.5;
const static CGFloat kFontSizeSmallThreshold = (kFontSizeSmall + kFontSizeMedium) * 0.5;
const static CGFloat kFontSizeMediumThreshold = (kFontSizeMedium + kFontSizeLarge) * 0.5;
const static CGFloat kFontSizeLargeThreshold = (kFontSizeLarge + kFontSizeExtraLarge) * 0.5;

static CGSize standardImageViewSize = {1024.0f, 768.0f};

@interface CZElementGroup (CZElementLayer)
@property (nonatomic, assign, readwrite) CZElementLayer *parentLayer;
@end

@implementation CZElementGroup (CZElementLayer)
@dynamic parentLayer;
@end

@interface CZElement (CZElementLayer)
@property (nonatomic, assign, readwrite) CZElementLayer *parent;
@end

@implementation CZElement (CZElementLayer)
@dynamic parent;
@end

#pragma mark - class CZElementLayer

@interface CZElementLayer() {
    NSInteger _idSeed;
}

@property (nonatomic, retain) NSMutableArray *elements;
@property (nonatomic, retain) NSMutableArray *groups;
@property (nonatomic, assign, readonly) NSArray *elementsWithMeasurements;

@end

@implementation CZElementLayer

+ (void)setStandardImageViewSize:(CGSize)size {
    if (size.width <= 0 || size.height <= 0) {
        standardImageViewSize = CGSizeMake(1024, 768);
    } else {
        standardImageViewSize = size;
    }
}

+ (CGSize)standardImageViewSize {
    return standardImageViewSize;
}

+ (CGFloat)suggestLineWidthForSize:(CZElementSize)size {
    CGFloat lineWidth = kLineWidthMedium;
    switch (size) {
        case CZElementSizeExtraSmall:
            lineWidth = kLineWidthExtraSmall;
            break;
        case CZElementSizeSmall:
            lineWidth = kLineWidthSmall;
            break;
        case CZElementSizeMedium:
            lineWidth = kLineWidthMedium;
            break;
        case CZElementSizeLarge:
            lineWidth = kLineWidthLarge;
            break;
        case CZElementSizeExtraLarge:
            lineWidth = kLineWidthExtraLarge;
            break;
        default:
            break;
    }
    
    return lineWidth;
}

+ (CZElementSize)elementSizeFromLineWidth:(CGFloat)lineWidth {
    if (lineWidth < kLineWidthExtraSmallThreshold) {
        return CZElementSizeExtraSmall;
    } else if (lineWidth < kLineWidthSmallThreshold) {
        return CZElementSizeSmall;
    } else if (lineWidth < kLineWidthMediumThreshold) {
        return CZElementSizeMedium;
    } else if (lineWidth < kLineWidthLargeThreshold) {
        return CZElementSizeLarge;
    } else {
        return CZElementSizeExtraLarge;
    }
}

+ (CGFloat)suggestFontSizeForSize:(CZElementSize)size {
    CGFloat fontSize = kFontSizeMedium;
    switch (size) {
        case CZElementSizeExtraSmall:
            fontSize = kFontSizeExtraSmall;
            break;
        case CZElementSizeSmall:
            fontSize = kFontSizeSmall;
            break;
        case CZElementSizeMedium:
            fontSize = kFontSizeMedium;
            break;
        case CZElementSizeLarge:
            fontSize = kFontSizeLarge;
            break;
        case CZElementSizeExtraLarge:
            fontSize = kFontSizeExtraLarge;
            break;
        default:
            break;
    }
    
    return fontSize;
}

+ (CZElementSize)elementSizeFromFontSize:(CGFloat)fontSize {
    if (fontSize < kFontSizeExtraSmallThreshold) {
        return CZElementSizeExtraSmall;
    } else if (fontSize < kFontSizeSmallThreshold) {
        return CZElementSizeSmall;
    } else if (fontSize < kFontSizeMediumThreshold) {
        return CZElementSizeMedium;
    } else if (fontSize < kFontSizeLargeThreshold) {
        return CZElementSizeLarge;
    } else {
        return CZElementSizeExtraLarge;
    }
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _scaling = kInvalidScaling;
        _logicalScaling = 1.0;
        _elements = [[NSMutableArray alloc] init];
        _groups = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [self removeAllGroups];
    [self removeAllElements];
    
    [_elements release];
    [_groups release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementLayer *newLayer = [[[self class] allocWithZone:zone] init];

    // following properties shall keep as default
    // delegate
    // _idSeed
    
    if (newLayer) {
        newLayer->_scaling = self->_scaling;
        newLayer->_logicalScaling = self->_logicalScaling;
        newLayer->_frame = self->_frame;
        newLayer->_lineWidth = self->_lineWidth;
        newLayer->_prefersBigUnit = self->_prefersBigUnit;
        
        for (NSUInteger i = 0; i < self->_groups.count; ++i) {
            CZElementGroup *newGroup = [[CZElementGroup alloc] init];
            [newLayer addGroup:newGroup];
            [newGroup release];
        }
        
        for (CZElement *element in self->_elements) {
            CZElement *newElement = [element copy];
            [newLayer addElement:newElement];
            
            if (element.parentGroup) {
                NSUInteger groupIndex = [self->_groups indexOfObject:element.parentGroup];
                CZElementGroup *newGroup = newLayer->_groups[groupIndex];
                [newGroup addElement:newElement];
            }
            
            [newElement release];
        }
    }
    return newLayer;
}

- (void)setScaling:(CGFloat)scaling {
    _scaling = scaling;
    
    [self refreshElementsAfterScalingChanged];
}

- (void)setLogicalScaling:(CGFloat)logicalScaling {
    if (_logicalScaling != logicalScaling) {
        _logicalScaling = logicalScaling;
        [self refreshElementsAfterScalingChanged];
    }
}

- (void)setFrame:(CGRect)frame {
    _frame = frame;
    
    for (CZElement *element in _elements)  {
        element.featureValidated = NO;
    }
}

- (BOOL)isValidScaling {
    return _scaling > 0.0f;
}

- (CGFloat)scalingOfUnit:(CZElementUnitStyle)unitStyle {
    CGFloat scalingOfUnit;
    if ([self isValidScaling]) {
        scalingOfUnit = [CZElement scaling:[self scaling]
                               ofUnitStyle:[CZElement unitStyle]
                             prefersBigUnit:self.prefersBigUnit];
    } else {
        scalingOfUnit = self.logicalScaling;
    }
    return scalingOfUnit;
}

- (NSUInteger)precision {
    if ([self isValidScaling]) {
        CGFloat scaling = [self scalingOfUnit:[CZElement unitStyle]];
        NSUInteger precision;
        if (scaling <= 0.0001f) {
            precision = 5;
        } else if (scaling <= 0.001f) {
            precision = 4;
        } else if (scaling <= 0.01f) {
            precision = 3;
        } else if (scaling <= 0.1f) {
            precision = 2;
        } else {
            precision = 1;
        }
        
        return precision;
    } else {
        return 0;
    }
}

- (CALayer *)newLayer {
    CALayer *rootLayer = [[CALayer alloc] init];
    
    rootLayer.position = CGPointZero;
    rootLayer.anchorPoint = CGPointZero;
    
    CGRect rect = [self frame];
    rect.origin.x = 0.0f;
    rect.origin.y = 0.0f;
    rootLayer.frame = rect;
    rootLayer.bounds = rect;
    rootLayer.contentsRect = rect;
    
    for (CZElement *element in _elements) {
        CALayer *subLayer = [element newLayer];
        if (subLayer) {
            subLayer.name = element.identifier;
            [rootLayer addSublayer:subLayer];
            [subLayer release];
        } else {
            CZLogv(@"failed to create layer by element: %@", element);
        }
        
        if ([element hasMeasurement]) {
            CALayer *measurementLayer = nil;
            if (element.measurementHidden) {
                measurementLayer = [[CAShapeLayer alloc] init];
            } else {
                measurementLayer = [element newMeasurementTextLayer];
            }
            
            if (measurementLayer) {
                measurementLayer.hidden = element.measurementHidden;
                measurementLayer.name = [element.identifier stringByAppendingString:kCZMeasurementTextLayerSuffix];
                [rootLayer addSublayer:measurementLayer];
                [measurementLayer release];
            }
        }
    }

    return rootLayer;
}

- (CZElement *)hitTest:(CGPoint)p
             tolerance:(CGFloat)tolerance {
    for (CZElement *element in [_elements reverseObjectEnumerator]) {
        if ([element hitTest:p tolerance:tolerance]) {
            return element;
        }
    }
    
    for (CZElement *element in [_elements reverseObjectEnumerator]) {
        if ([element containsPoint:p]) {
            return element;
        }
    }
    
    return nil;
}

#pragma mark - element methods

- (NSUInteger)elementCount {
    return [_elements count];
}

- (NSUInteger)elementsWithMeasurementCount {
    return [[self elementsWithMeasurements] count];
}

- (CZElement *)firstElement {
    return [_elements firstObject];
}

- (CZElement *)firstElementOfClass:(Class)aClass {
    for (CZElement *element in _elements) {
        if ([element isKindOfClass:aClass]) {
            return element;
        }
    }
    
    return nil;
}

- (CZElement *)lastElement {
    return [_elements lastObject];
}

- (CZElement *)elementAtIndex:(NSUInteger)index {
    return [_elements objectAtIndex:index];
}

- (NSArray *)elementsWithMeasurements {
    NSMutableArray *elementsWithMeasurements = [[NSMutableArray alloc] init];
    for (CZElement *element in _elements) {
        if ([element hasMeasurement]) {
            if (![element isMeasurementHidden]) {
                [elementsWithMeasurements addObject:element];
            }
        }
    }
    return [elementsWithMeasurements autorelease];
}

- (NSUInteger)indexOfElement:(CZElement *)element {
    return [_elements indexOfObject:element];
}

- (void)addElement:(CZElement *)element {
    [self insertElement:element atIndex:self.elementCount];
}

- (void)insertElement:(CZElement *)element atIndex:(NSUInteger)index {
    if (element == nil || self == element.parent) {
        return;
    }
    
    [element retain];
    [element removeFromParent];
    
    [_elements insertObject:element atIndex:index];
    
    element.parent = self;
    [self antoIncreaseIDByElement:element];
    [self rearrangeMeasurementIDForElement:element];
    
    // Notify did add element
    if ([self.delegate respondsToSelector:@selector(layer:didAddElement:)]) {
        [self.delegate layer:self didAddElement:element];
    }
    
    if ([self.delegate respondsToSelector:@selector(layer:updateElementMeasurementsLargerThanIndex:)]) {
        [self.delegate layer:self updateElementMeasurementsLargerThanIndex:(index + 1)];
    }
    
    [element release];
}

- (void)removeAllElements {
    NSArray *tempArray = [_elements copy];
    for (CZElement *element in tempArray) {
        [element removeFromParent];
    }
    [tempArray release];
}

- (void)applyTransform:(CGAffineTransform)transform {
    for (CZElement *element in self) {
        [element applyTransform:transform];
    }
}

- (void)rearrangeMeasurementIDForElement:(CZElement *)element {
    for (int i = 0,j = 1; i < [self elementCount] ; i++) {
        CZElement *element = [self elementAtIndex:i];
        if (element.hasMeasurement && !element.measurementHidden && ![element isKindOfClass:[CZElementScaleBar class]]) {
            NSString *tempStr = [[NSString alloc] initWithFormat:@"%d", j];
            element.measurementID = tempStr;
            [tempStr release];
            j++;
        } else {
            element.measurementID = nil;
        }
    }
}

#pragma mark - Group methods

/*! Sub groups' count. */
- (NSUInteger)groupCount {
    return [self.groups count];
}

- (CZElementGroup *)groupAtIndex:(NSUInteger)index {
    return [self.groups objectAtIndex:index];
}

- (NSUInteger)indexOfGroup:(CZElementGroup *)group {
    return [self.groups indexOfObject:group];
}

/*! Add group to the annotation Layer. */
- (void)addGroup:(CZElementGroup *)group {
    if (group == nil || self == group.parentLayer) {
        return;
    }
    
    [group retain];
    [group removeFromParentLayer];
    
    [self.groups addObject:group];
    group.parentLayer = self;
    
    [group release];
}

- (void)groupNormalize {
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:self.groups.count];
    for (CZElementGroup *group in self.groups) {
        if ([group elementCount] < 2) {
            [tempArray addObject:group];
        }
    }
    
    for (CZElementGroup *group in tempArray) {
        [group removeFromParentLayer];
    }
    
    [tempArray release];
}

- (NSEnumerator *)groupEnumerator {
    return [self.groups objectEnumerator];
}

#pragma mark - NSFastEnumeration protocol

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id [])buffer count:(NSUInteger)len {
    return [self.elements countByEnumeratingWithState:state objects:buffer count:len];
}

#pragma mark - Others

- (CGFloat)standardZoom {
    CGSize standardSize = [CZElementLayer standardImageViewSize];
    CGFloat viewSize = MAX(standardSize.width, standardSize.height);
    CGFloat imageSize = MAX(_frame.size.width, _frame.size.height);
    return (imageSize == 0.0f) ? 1.0f : viewSize / imageSize;
}

- (CGFloat)suggestLineWidthForSize:(CZElementSize)size {
    CGFloat lineWidth = [CZElementLayer suggestLineWidthForSize:size];
    
    CGFloat zoom = [self standardZoom];
    
    lineWidth = lineWidth / zoom;
    lineWidth = round(lineWidth * 4) * 0.25;
    lineWidth = MAX(0.25, lineWidth);
    
    return lineWidth;
}

- (CZElementSize)elementSizeFromLineWidth:(CGFloat)lineWidth {
    CGFloat zoom = [self standardZoom];
    lineWidth = lineWidth * zoom;
    CZElementSize size = [CZElementLayer elementSizeFromLineWidth:lineWidth];
    return size;
}

- (CGFloat)suggestFontSizeForSize:(CZElementSize)size {
    CGFloat fontSize = [CZElementLayer suggestFontSizeForSize:size];
    
    fontSize = fontSize / [self standardZoom];
    fontSize = round(fontSize);
    
    return fontSize;
}

- (CZElementSize)elementSizeFromFontSize:(CGFloat)fontSize {
    fontSize = fontSize * [self standardZoom];
    
    CZElementSize size = [CZElementLayer elementSizeFromFontSize:fontSize];
    return size;
}

- (NSString *)unitNameOfDistanceByStyle:(CZElementUnitStyle)unitStyle {
    NSString *unit1d;
    if ([self isValidScaling]) {
        unit1d = [CZElement distanceUnitStringByStyle:unitStyle prefersBigUnit:self.prefersBigUnit];
    } else {
        unit1d = kCZFeatureUnitPixel;
    }
    
    return unit1d;
}

- (NSString *)unitNameOfAreaByStyle:(CZElementUnitStyle)unitStyle {
    NSString *unit2d;
    if ([self isValidScaling]) {
        unit2d = [CZElement areaUnitStringByStyle:unitStyle prefersBigUnit:self.prefersBigUnit];
    } else {
        unit2d = kCZFeatureUnitPixel;
    }
    
    return unit2d;
}

#pragma mark -
#pragma mark private methods

- (void)antoIncreaseIDByElement:(CZElement *)element {
    if ([element.identifier length] == 0) {
        ++_idSeed;
        NSString *tempStr = [[NSString alloc] initWithFormat:@"%ld", (long)_idSeed];
        element.identifier = tempStr;
        [tempStr release];
    } else {
        NSUInteger identifier = [element.identifier integerValue];
        if (identifier > _idSeed) {
            _idSeed = identifier;
        } // TODO: else make identifier unique
    }
}

- (void)removeAllGroups {
    NSArray *tempArray = [_groups copy];
    for (CZElementGroup *group in tempArray) {
        [group removeFromParentLayer];
    }
    [tempArray release];
}

- (void)refreshElementsAfterScalingChanged {
    for (CZElement *element in _elements)  {
        element.featureValidated = NO;
    }
    
    if ([self.delegate respondsToSelector:@selector(layerDidChangeScaling:)]) {
        [self.delegate layerDidChangeScaling:self];
    }
}

@end

#pragma mark - implement friend class method

@implementation CZElementLayer (CZElementGroup)

- (void)removeGroup:(CZElementGroup *)group {
    // Notify will remove group
    if ([self.delegate respondsToSelector:@selector(layer:willRemoveGroup:)]) {
        [self.delegate layer:self willRemoveGroup:group];
    }
    
    if (group.parentLayer != self) {
        return;
    }
    
    [group retain];
    [self.groups removeObject:group];
    group.parentLayer = nil;
    
    // Notify did remove group
    if ([self.delegate respondsToSelector:@selector(layer:didRemoveGroup:)]) {
        [self.delegate layer:self didRemoveGroup:group];
    }
    
    [group release];
}

@end

@implementation CZElementLayer (CZElement)

- (void)removeElement:(CZElement *)element {
    // Notify will remove element
    if ([self.delegate respondsToSelector:@selector(layer:willRemoveElement:)]) {
        [self.delegate layer:self willRemoveElement:element];
    }
    
    [element removeFromGroup];
    
    [element retain];
    NSUInteger i = [self indexOfElement:element];
    [_elements removeObject:element];
    element.parent = nil;
    
    [self rearrangeMeasurementIDForElement:element];
    
    // Notify did remove element
    if ([self.delegate respondsToSelector:@selector(layer:didRemoveElement:)]) {
        [self.delegate layer:self didRemoveElement:element];
    }
    
    if ([self.delegate respondsToSelector:@selector(layer:updateElementMeasurementsLargerThanIndex:)]) {
        [self.delegate layer:self updateElementMeasurementsLargerThanIndex:i];
    }
    
    [element release];
}

@end

@implementation CZElementLayer (NonScaleBarElements)

/** @return Has element which is not a scale bar. */
- (BOOL)hasNonScaleBarElements {
    //Ignore this case.
    //    if (self.elementCount <= 0) {
    //        return YES;
    //    }
    for (CZElement *element in self) {
        if (![element isKindOfClass:[CZElementScaleBar class]]) {
            return YES;
        }
    }
    return NO;
}

/** @return Has element which is a scale bar. */
- (BOOL)hasScaleBarElement {
    for (CZElement *element in self) {
        if ([element isKindOfClass:[CZElementScaleBar class]]) {
            return YES;
        }
    }
    return NO;
}

@end
