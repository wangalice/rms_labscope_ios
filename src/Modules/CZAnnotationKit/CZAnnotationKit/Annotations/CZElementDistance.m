//
//  CZElementDistance.m
//  Matscope
//
//  Created by Ralph Jin on 10/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementDistance.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodeLine.h"
#import "CZFeature.h"
#import "CZElementCommon.h"

@interface CZElementCalipers () {
    NSUInteger _pointCount;
}

@property (nonatomic, retain) CZNodeLine *lineNode;
@property (nonatomic, retain) CZNodeLine *rightTick;
@property (nonatomic, retain) CZNodeLine *baseLine;

- (void)updateFeatures;

@end

@implementation CZElementCalipers

- (instancetype)init {
    self = [super init];
    if (self) {
        _lineNode = [[CZNodeLine alloc] init];
    }
    return self;
}

- (instancetype)initWithPoint1:(CGPoint)p1 point2:(CGPoint)p2 point3:(CGPoint)p3 {
    self = [super init];
    if (self) {
        _p1 = p1;
        _p2 = p2;
        _p3 = p3;
        _pointCount = 3;
        
        CGPoint othorPoint = CalcOrthoPoint(_p1, _p2, _p3);

        _lineNode = [[CZNodeLine alloc] initWithPoint:othorPoint anotherPoint:p3];
    }
    
    return self;
}

- (void)dealloc {
    [_lineNode release];
    [_rightTick release];
    [_baseLine release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementCalipers *newDistance = [super copyWithZone:zone];
    
    if (newDistance) {
        newDistance->_p1 = self->_p1;
        newDistance->_p2 = self->_p2;
        newDistance->_p3 = self->_p3;
        newDistance->_pointCount = self->_pointCount;
        
        [newDistance->_lineNode release];
        newDistance->_lineNode = [self->_lineNode copyWithZone:zone];

        if ([newDistance->_lineNode subNodeCount] > 0) {
            newDistance.baseLine = (CZNodeLine *)[newDistance->_lineNode subNodeAtIndex:0];
        }
        
        if ([newDistance->_lineNode subNodeCount] > 1) {
            newDistance.rightTick = (CZNodeLine *)[newDistance->_lineNode subNodeAtIndex:1];
        }
    }
    return newDistance;
}

- (CGPoint)othorPoint {
    [self updateFeatures];
    return _lineNode.p1;
}

- (void)setP1:(CGPoint)p1 {
    _p1 = p1;
    self.featureValidated = NO;
}

- (void)setP2:(CGPoint)p2 {
    _p2 = p2;
    self.featureValidated = NO;
}

- (void)setP3:(CGPoint)p3 {
    _p3 = p3;
    self.featureValidated = NO;
}

#pragma mark - Override CZElement

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementCalipers *calipers = (CZElementCalipers *)anElement;
    
    return (CGPointEqualToPoint(calipers.p1, self.p1) &&
            CGPointEqualToPoint(calipers.p2, self.p2) &&
            CGPointEqualToPoint(calipers.p3, self.p3));
}


- (CZNode *)primaryNode {
    return _lineNode;
}

- (CALayer *)newLayer {
    [self updateFeatures];
    
    _lineNode.lineWidth = [self suggestLineWidth];
    
    [self updateTicks];
    
    CALayer *layer = [_lineNode newLayer];
    return layer;
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    CZFeature *feature = self.features.count > 0 ? self.features[0] : nil;
    NSString *feastureString = [feature toLocalizedStringWithPrecision:[self precision]];
    CZNodeRectangle *background = [self newMeasurementNodeWithString:feastureString];
    
    CGFloat tickHeight = [self tickHeight];
    [CZElementCommon putMeasurement:background onLine:_lineNode.p1 secondPoint:_lineNode.p2 distance:-(tickHeight + 2)];
    
    // Create CALayer for rendering
    layer = [background newLayer];
    [background release];
    return layer;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    CGPoint p = self.p1;
    p = CGPointApplyAffineTransform(p, transform);
    self.p1 = p;
    
    p = self.p2;
    p = CGPointApplyAffineTransform(p, transform);
    self.p2 = p;
    
    p = self.p3;
    p = CGPointApplyAffineTransform(p, transform);
    self.p3 = p;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    NSArray *pointArray = @[@(_p1.x), @(_p1.y), @(_p2.x), @(_p2.y), @(_p3.x), @(_p3.y)];
    NSDictionary *memo = [[NSDictionary alloc] initWithObjectsAndKeys:pointArray, @"CGPointArray", nil];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSArray *pointArray = [memo objectForKey:@"CGPointArray"];
    if (pointArray && [pointArray count] >= 6) {
        [self notifyWillChange];
        
        CGPoint p1;
        p1.x = [pointArray[0] floatValue];
        p1.y = [pointArray[1] floatValue];
        
        CGPoint p2;
        p2.x = [pointArray[2] floatValue];
        p2.y = [pointArray[3] floatValue];
        
        CGPoint p3;
        p3.x = [pointArray[4] floatValue];
        p3.y = [pointArray[5] floatValue];
        
        self.p1 = p1;
        self.p2 = p2;
        self.p3 = p3;
        
        [self notifyDidChange];
        
        return YES;
    }
    
    return NO;
}

+ (NSUInteger)featureCount {
    return 1;
}

// TODO: refactor common code with CZElementLine
- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    
    CGPoint othorPoint = CalcOrthoPoint(self.p1, self.p2, self.p3);
    _lineNode.p1 = othorPoint;
    _lineNode.p2 = self.p3;
    CGFloat distance = CZLengthOfLine(othorPoint, self.p3);
    CGFloat scaledDistance = scaling * distance;
    
    [self removeAllFeatures];
    
    CZFeature *feature = [[CZFeature alloc] initWithValue:scaledDistance unit:unit1d type:kCZFeatureTypeDistance];
    [self appendFeature:feature];
    [feature release];
}

#pragma mark - CZElementMultiPointKind methods

- (NSUInteger)pointsCount {
    return _pointCount;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return _p1;
        case 1:
            return _p2;
        case 2:
            return _p3;
        default:
            return CGPointZero;
    }
}

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            self.p1 = pt;
            break;
        case 1:
            self.p2 = pt;
            break;
        case 2:
            self.p3 = pt;
            break;
        default:
            break;
    }
}

- (void)appendPoint:(CGPoint)pt {
    if (_pointCount < 3) {
        [self setPoint:pt atIndex:_pointCount];
        ++_pointCount;
    }
}

- (void)removePointAtIndex:(NSUInteger)index {
    if (_pointCount) {
        --_pointCount;
    }
}

- (NSUInteger)minimumPointsCount {
    return 3;
}

- (NSUInteger)maximumPointsCount {
    return 3;
}

#pragma mark - Private methods

- (CZNodeLine *)rightTick {
    if (_rightTick == nil) {
        _rightTick = [[CZNodeLine alloc] init];
    }
    return _rightTick;
}

- (CZNodeLine *)baseLine {
    if (_baseLine == nil) {
        _baseLine = [[CZNodeLine alloc] init];
        _baseLine.dash = YES;
    }
    return _baseLine;
}

- (CGFloat)tickHeight {
    return 6.0 * [self suggestLineWidth];
}

- (void)updateTicks {
    // calculate topleft
    CGPoint p1 = _lineNode.p1;
    CGPoint p2 = _lineNode.p2;
    CGPoint topLeft;
    topLeft.x = MIN(p1.x, p2.x);
    topLeft.y = MIN(p1.y, p2.y);
    
    // update base line
    [_lineNode addSubNode:self.baseLine];
    _baseLine.lineWidth = _lineNode.lineWidth;
    _baseLine.strokeColor = _lineNode.strokeColor;
    
    // update base line position
    p1 = self.p1;
    p2 = self.p2;
    p1.x -= topLeft.x;
    p1.y -= topLeft.y;
    p2.x -= topLeft.x;
    p2.y -= topLeft.y;
    _baseLine.p1 = p1;
    _baseLine.p2 = p2;
    
    // update tick
    if (self.isMeasurementHidden) {
        [_rightTick removeFromParent];
        self.rightTick = nil;
    } else {
        [_lineNode addSubNode:self.rightTick];
        
        _rightTick.lineWidth = _lineNode.lineWidth;
        _rightTick.strokeColor = _lineNode.strokeColor;
    

        // update tick position
        p1 = _lineNode.p1;
        p2 = _lineNode.p2;
        p1.x -= topLeft.x;
        p1.y -= topLeft.y;
        p2.x -= topLeft.x;
        p2.y -= topLeft.y;
        
        CGFloat tickHeight = [self tickHeight];
        _rightTick.p1 = CZCalcOuterPoint(p1, p2, tickHeight);
        _rightTick.p2 = CZCalcOuterPoint(p1, p2, -tickHeight);
    }
}

@end
