//
//  CZElementCircle.h
//  Hermes
//
//  Created by Ralph Jin on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"

const extern NSUInteger CZElementCirclePointCount;

enum {
    CZElementCircleFeatureArea = 0,
    CZElementCircleFeatureCenterX,
    CZElementCircleFeatureCenterY,
    CZElementCircleFeatureDiameter,
    CZElementCircleFeaturePerimeter,
    
    CZElementCircleFeatureCount
};

@interface CZElementCircle : CZElement

@property (nonatomic, assign) CGPoint center;
@property (nonatomic, assign) CGFloat radius;

- (instancetype)initWithCenter:(CGPoint)center radius:(CGFloat)radius NS_DESIGNATED_INITIALIZER;

/*! get 1 of the 3 points on the circle */
- (CGPoint)pointAtIndex:(NSUInteger)index;

/*! set 1 of the 3 points on the circle */
- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index;

@end
