//
//  CZElementPolyline.m
//  Hermes
//
//  Created by Jin Ralph on 13-12-17.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementPolyline.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodePolyline.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"

enum {
    CZElementPolylineFeaturePerimeter = 0,
    
    CZElementPolylineFeatureCount
};

@interface CZElementPolyline() {
    CGPoint _measurementTextPosition;
}
@end

@implementation CZElementPolyline

+ (Class)nodeClass {
    return [CZNodePolyline class];
}

+ (NSUInteger)minimumPointsCount {
    return 2;
}

+ (BOOL)hasTickes {
    return YES;
}

#pragma mark - override CZElement

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }

    CZFeature *perimeterFeature = self.features[CZElementPolylineFeaturePerimeter];
    NSString *perimeterString = [perimeterFeature toLocalizedStringWithPrecision:[self precision]];

    NSString *feastureString;
    feastureString = perimeterString;
    
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:feastureString];
    _measurementTextPosition = [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

+ (NSUInteger)featureCount {
    return CZElementPolylineFeatureCount;
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    
    CGFloat scaledPerimeter = [self calcDistance] * scaling;
    
    [self removeAllFeatures];
    
    // perimeter
    CZFeature *diameterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypeDistance];
    [self appendFeature:diameterFeature];
    [diameterFeature release];
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CGPoint)measurementTextPosition {
    return _measurementTextPosition;
}

#pragma mark - Private Methods

- (CGFloat)calcDistance {
    CGFloat perimeter = 0.0f;
    CZNodePolyline *polylineNode = (CZNodePolyline *)self.primaryNode;
    const NSUInteger pointsCount = [polylineNode pointsCount];
    if (pointsCount == 0) {
        return perimeter;
    }
    
    CGPoint lastPoint = [polylineNode pointAtIndex:0];
    for (NSUInteger i = 1; i < pointsCount; i++) {
        CGPoint point = [polylineNode pointAtIndex:i];
        perimeter += hypot(point.x - lastPoint.x, point.y - lastPoint.y);
        lastPoint = point;
    }
    
    return perimeter;
}


@end
