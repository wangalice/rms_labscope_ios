//
//  CZElementText.m
//  Hermes
//
//  Created by Ralph Jin on 3/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementText.h"
#import "CZNodeText.h"

@interface CZElementText()
@property (nonatomic, retain) CZNodeText *textNode;
@end

@implementation CZElementText

- (instancetype)init {
    return [self initWithFrame:CGRectMake(0, 0, 10, 10)];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super init];
    if (self) {
        _textNode = [[CZNodeText alloc] initWithFrame:frame];
        _textNode.wrapped = YES;
    }
    
    return self;
}

- (void)dealloc {
    [_textNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementText *newText = [super copyWithZone:zone];
    if (newText) {
        [newText->_textNode release];
        newText->_textNode = [self->_textNode copyWithZone:zone];
    }
    return newText;
}

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementText *aText = (CZElementText *)anElement;
    
    return CGRectEqualToRect([self frame], [aText frame]);
}

- (CZNode *)primaryNode {
    return _textNode;
}

- (NSString *)string {
    return _textNode.string;
}

- (void)setString:(NSString *)string {
    _textNode.string = string;
}


- (NSString *)fontFamily {
    return [_textNode fontFamily];
}

- (void)setFontFamily:(NSString *)fontFamily {
    [_textNode setFontFamily:fontFamily];
}

- (CGRect)frame {
    CGRect r;
    r.origin.x = _textNode.x;
    r.origin.y = _textNode.y;
    r.size.width = _textNode.width;
    r.size.height = _textNode.height;
    return r;
}

- (void)setFrame:(CGRect)frame {
    _textNode.x = frame.origin.x;
    _textNode.y = frame.origin.y;
    _textNode.width = frame.size.width;
    _textNode.height = frame.size.height;
}


#pragma mark -
#pragma mark Override CZElement

- (CZColor)measurementBackgroundColor {
    return kCZColorTransparent;
}

- (CGFloat)fontSize {
    return [self suggestFontSize];
}

- (CALayer *)newLayer {
    _textNode.fontSize = self.fontSize;
    CALayer *layer = [_textNode newLayer];
    return layer;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    CGRect rect = CGRectMake(_textNode.x, _textNode.y, _textNode.width, _textNode.height);
    rect = CGRectApplyAffineTransform(rect, transform);
    _textNode.x = rect.origin.x;
    _textNode.y = rect.origin.y;
    _textNode.width = rect.size.width;
    _textNode.height = rect.size.height;
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    CGRect rect = self.frame;
    NSDictionary *value = @{@"x": @(rect.origin.x),
                            @"y": @(rect.origin.y),
                            @"w": @(rect.size.width),
                            @"h": @(rect.size.height)};
    
    NSNumber *rotateAngle = [[NSNumber alloc] initWithFloat:self.rotateAngle];
    NSDictionary *memo = [[NSDictionary alloc]
                          initWithObjectsAndKeys:value, @"CGRect", rotateAngle, @"rotateAngle", nil];
    [rotateAngle release];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSDictionary *data = [memo objectForKey:@"CGRect"];
    NSNumber *rotateAngle = [memo objectForKey:@"rotateAngle"];
    if ([data isKindOfClass:[NSDictionary class]] &&
        [rotateAngle isKindOfClass:[NSNumber class]]) {
        [self notifyWillChange];
        
        CGRect rect;
        rect.origin.x = [data[@"x"] floatValue];
        rect.origin.y = [data[@"y"] floatValue];
        rect.size.width = [data[@"w"] floatValue];
        rect.size.height = [data[@"h"] floatValue];
        
        self.frame = rect;
        
        self.rotateAngle = [rotateAngle floatValue];
        
        [self notifyDidChange];
        return YES;
    }
    
    return NO;
}

#pragma mark - CZElementRotatable

- (void)setRotateAngle:(CGFloat)rotateAngle {
    _textNode.rotateAngle = rotateAngle;
}

@end
