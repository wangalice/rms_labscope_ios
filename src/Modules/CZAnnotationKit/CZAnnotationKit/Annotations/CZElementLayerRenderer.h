//
//  CZElementLayerRenderer.h
//  Hermes
//
//  Created by Ralph Jin on 4/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZElement.h"
#import "CZElementLayer.h"

extern NSString * const kAnnotationLayerName;

/** Helper class for renderering (update CALayers) CZElementLayer and its sub elements. */
@interface CZElementLayerRenderer : NSObject<CZElementLayerDelegate>

@property(nonatomic, assign, readonly) UIView *targetView;
@property(nonatomic, copy) NSString *annotationLayerName;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithTargetView:(UIView *)view NS_DESIGNATED_INITIALIZER;

/** hide element and show others for temporary. */
- (void)hideElementOnly:(CZElement *)element inElementLayer:(CZElementLayer *)elementLayer;

- (void)updateAllElements:(CZElementLayer *)elementLayer;

@end
