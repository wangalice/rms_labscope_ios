//
//  CZGemMeasurer.mm
//  Hermes
//
//  Created by Ralph Jin on 8/19/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZGemMeasurer.h"
#import <ZEN/GemMeasurerClr.h>

@interface CZGemMeasure() {
    GemMeasurerClr::GemMeasurer *_measurer;
}

@end

@implementation CZGemMeasure

- (instancetype)init {
    self = [super init];
    if (self) {
        _measurer = new GemMeasurerClr::GemMeasurer();
    }
    return self;
}

- (void)dealloc {
    delete _measurer;
    [super dealloc];
}

- (void)setRegion:(const CGFloat *)polygonCoordinates pointsCount:(long)pointsCount {
    _measurer->SetRegion(polygonCoordinates, pointsCount);
}

- (void)setCircle:(double)centerX centerY:(double)centerY radius:(double)radius {
    double *points = NULL;
    long pointsCount = GemMeasurerClr::GemMeasurer::ConvertCircleToPolygon(centerX, centerY, radius, &points);
    _measurer->SetRegion(points, pointsCount);
    delete[] points;
}

- (double)area {
    return _measurer->Area();
}

- (double)perimeter {
    return _measurer->Perimeter();
}


@end
