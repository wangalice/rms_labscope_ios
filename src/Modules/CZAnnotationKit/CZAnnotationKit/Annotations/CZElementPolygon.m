//
//  CZElementPolygon.m
//  Hermes
//
//  Created by Jin Ralph on 13-3-23.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementPolygon.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodePolygon.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"

#if CALCULATE_AERA_USE_CRACK_ALGO
#import "CZElementLayer.h"
#import "CZGemMeasurer.h"
#endif

@interface CZElementPolygon() {
    CGPoint _measurementTextPosition;
}
@end

@implementation CZElementPolygon

+ (Class)nodeClass {
    return [CZNodePolygon class];
}

+ (NSUInteger)minimumPointsCount {
    return 3;
}

#pragma mark - override CZElement

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }

    CZFeature *areaFeature = self.features[CZElementPolygonFeatureArea];
    NSString *areaString = nil;
    if (areaFeature.value >= 0.0f) {
        areaString = [areaFeature toLocalizedStringWithPrecision:[self precision]];
    }

    CZFeature *perimeterFeature = self.features[CZElementPolygonFeaturePerimeter];
    NSString *perimeterString = [perimeterFeature toLocalizedStringWithPrecision:[self precision]];

    NSString *feastureString;
    if (areaString) {
        feastureString = [areaString stringByAppendingString:@"\n"];
        feastureString = [feastureString stringByAppendingString:perimeterString];
    } else {
        feastureString = perimeterString;
    }

    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:feastureString];
    _measurementTextPosition = [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

+ (NSUInteger)featureCount {
    return CZElementPolygonFeatureCount;
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    NSString *unit2d = [self unitNameOfAreaByStyle:unitStyle];
    
#if CALCULATE_AERA_USE_CRACK_ALGO
    CZGemMeasure *measurer = [[CZGemMeasure alloc] init];
    const CGFloat logicalScaling = self.logicalScaling;
    const CGFloat scalingOfLogicalPoint = scaling / logicalScaling;
	
    CGFloat scaledArea = 0;
    CGFloat scaledPerimeter = 0;
    
    CZNodePolygon *polygonNode = (CZNodePolygon *)self.primaryNode;
    const NSUInteger pointsCount = [polygonNode pointsCount];
    if (pointsCount > 0) {
        CGFloat * const points = (CGFloat *)malloc(sizeof(CGFloat) * 2 * (pointsCount + 1));
        CGFloat *pPoint = points;
        CGPoint lastPoint = [polygonNode pointAtIndex:pointsCount - 1];
        *pPoint = lastPoint.x * logicalScaling;
        ++pPoint;
        *pPoint = lastPoint.y * logicalScaling;
        ++pPoint;
        
        for (NSUInteger i = 0; i < pointsCount; i++) {
            CGPoint point = [polygonNode pointAtIndex:i];
            *pPoint = point.x * logicalScaling;
            ++pPoint;
            *pPoint = point.y * logicalScaling;
            ++pPoint;
        }
        
        [measurer setRegion:points pointsCount:pointsCount + 1];
        free(points);
        
        scaledArea = [measurer area] * scalingOfLogicalPoint * scalingOfLogicalPoint;
        scaledPerimeter = [measurer perimeter] * scalingOfLogicalPoint;
    }
    
    [measurer release];
#else
    CGFloat scaledArea = [self calcAreaInPixel] * scaling * scaling;
    CGFloat scaledPerimeter = [self calcPerimeter] * scaling;
#endif
    
    [self removeAllFeatures];

    // area measurement
    CZFeature *areaFeature = [[CZFeature alloc] initWithValue:scaledArea unit:unit2d type:kCZFeatureTypeArea];
    [self appendFeature:areaFeature];
    [areaFeature release];
    
    // perimeter
    CZFeature *diameterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypePerimeter];
    [self appendFeature:diameterFeature];
    [diameterFeature release];
}

- (CGPoint)measurementTextPosition {
    return _measurementTextPosition;
}

#pragma mark - Private Methods

- (CGFloat)calcPerimeter {
    CGFloat perimeter = 0.0f;
    CZNodePolygon *polygonNode = (CZNodePolygon *)self.primaryNode;
    const NSUInteger pointsCount = [polygonNode pointsCount];
    CGPoint lastPoint = pointsCount > 0 ? [polygonNode pointAtIndex:pointsCount - 1] : CGPointZero;
    for (NSUInteger i = 0; i < pointsCount; i++) {
        CGPoint point = [polygonNode pointAtIndex:i];
        perimeter += hypot(point.x - lastPoint.x, point.y - lastPoint.y);
        lastPoint = point;
    }
    
    return perimeter;
}

- (CGFloat)calcAreaInPixel {
    CZNodePolygon *polygonNode = (CZNodePolygon *)self.primaryNode;
    const NSUInteger pointsCount = [polygonNode pointsCount];
    CGFloat area = 0;
    if (pointsCount >= 3) {
        // calculate if the polygon is self intersection
        BOOL isSelfIntersection = NO;
        if (pointsCount > 3) {
            CGPoint line1p1 = [polygonNode pointAtIndex:pointsCount - 1];
            for (NSUInteger i = 0; i < pointsCount - 1; i++) {
                CGPoint line1p2 = [polygonNode pointAtIndex:i];
                CGPoint line2p1 = [polygonNode pointAtIndex:i + 1];
                
                NSUInteger jCount = (i == 0) ? pointsCount - 1 : pointsCount;  // skip the first line and last line
                for (NSUInteger j = i + 2; j < jCount; j++) {
                    CGPoint line2p2 = [polygonNode pointAtIndex:j];
                    if (CZLineSegmentsAreCrossed(line1p1, line1p2, line2p1, line2p2)) {
                        isSelfIntersection = YES;
                        break;
                    }
                    line2p1 = line2p2;
                }
                
                if (isSelfIntersection) {
                    break;
                }
                
                line1p1 = line1p2;
            }
        }
        
        if (!isSelfIntersection) {  // calculate simple polygon area using Surveyor's Formula
            CGPoint lastPoint = [polygonNode pointAtIndex:pointsCount - 1];
            for (NSUInteger i = 0; i < pointsCount; i++) {
                CGPoint point = [polygonNode pointAtIndex:i];
                area += lastPoint.x * point.y - lastPoint.y * point.x;
                lastPoint = point;
            }
            area = fabs(area) / 2;
        } else {
            area = -1;  // failed to calculate area, so set a minus value for indicator.
        }
    }
    
    return area;
}


@end
