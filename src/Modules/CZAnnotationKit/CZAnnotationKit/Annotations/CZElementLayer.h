//
//  CZElementLayer.h
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CZColor.h"
#import "CZElement.h"

#define kInvalidScaling -1.0f;

@class CALayer;
@protocol CZElementLayerDelegate;

/** class of element layer, you can enuerate elements with NSFastEnumeration.*/
@interface CZElementLayer : NSObject<NSCopying, NSFastEnumeration>

/*! Default value is 1.0 um per pixel. */
@property (nonatomic, assign) CGFloat scaling;
/*! device point to logical point scaling. LP = DP * logicalScaling. */
@property (nonatomic, assign) CGFloat logicalScaling;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) BOOL prefersBigUnit;

@property (nonatomic, assign) id<CZElementLayerDelegate> delegate;

+ (void)setStandardImageViewSize:(CGSize)size;
+ (CGSize)standardImageViewSize;

/*! suggest line with for element size WITHOUT consider scaling.*/
+ (CGFloat)suggestLineWidthForSize:(CZElementSize)size;

/*! suggest font size for element size WITHOUT consider scaling.*/
+ (CGFloat)suggestFontSizeForSize:(CZElementSize)size;

- (BOOL)isValidScaling;

- (CGFloat)scalingOfUnit:(CZElementUnitStyle)unitStyle;

/*! get the propper precision for measurement value.*/
- (NSUInteger)precision;

/*! Create CALayer for rendering. */
- (CALayer *)newLayer;

#pragma mark - element methods

/*! Sub elements' count. */
- (NSUInteger)elementCount;

- (CZElement *)firstElement;
- (CZElement *)firstElementOfClass:(Class)aClass;
- (CZElement *)lastElement;

- (CZElement *)elementAtIndex:(NSUInteger)index;

- (NSUInteger)indexOfElement:(CZElement *)element;

/*! Add element to the annotation Layer. */
- (void)addElement:(CZElement *)element;

- (void)insertElement:(CZElement *)element atIndex:(NSUInteger)index;

- (void)removeAllElements;

- (void)applyTransform:(CGAffineTransform)transform;

#pragma mark - group methods

/*! Sub groups' count. */
- (NSUInteger)groupCount;

- (CZElementGroup *)groupAtIndex:(NSUInteger)index;

- (NSUInteger)indexOfGroup:(CZElementGroup *)group;

/*! Add group to the annotation Layer. */
- (void)addGroup:(CZElementGroup *)group;

/*! remove any group with less than or equals 1 element*/
- (void)groupNormalize;

- (NSEnumerator *)groupEnumerator;

#pragma mark - other methods

/*! @return the hit element; or nil, if nothing is hit. */
- (CZElement *)hitTest:(CGPoint)p tolerance:(CGFloat)tolerance;

- (CGFloat)standardZoom;

/*! Suggest line with for element size and consider scaling.*/
- (CGFloat)suggestLineWidthForSize:(CZElementSize)size;
- (CZElementSize)elementSizeFromLineWidth:(CGFloat)lineWidth;

/*! Suggest font size for element size and consider scaling.*/
- (CGFloat)suggestFontSizeForSize:(CZElementSize)size;
- (CZElementSize)elementSizeFromFontSize:(CGFloat)fontSize;

/*! get 1 dimension unit name, based on the unit type.*/
- (NSString *)unitNameOfDistanceByStyle:(CZElementUnitStyle)unitStyle;
/*! get 2 dimension unit name, based on the unit type.*/
- (NSString *)unitNameOfAreaByStyle:(CZElementUnitStyle)unitStyle;
/*! rearrange the measurement id when hide or unhide the measurement.*/
- (void)rearrangeMeasurementIDForElement:(CZElement *)element;

@end

#pragma mark - CZElementLayerDelegate

@protocol CZElementLayerDelegate <NSObject>

@optional

- (void)layer:(CZElementLayer *)layer didAddElement:(CZElement *)element;

- (void)layer:(CZElementLayer *)layer willRemoveElement:(CZElement *)element;
- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element;

- (void)layer:(CZElementLayer *)layer willChangeElement:(CZElement *)element;
- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element;
- (void)layer:(CZElementLayer *)layer updateElementMeasurementsLargerThanIndex:(NSUInteger)index;
- (void)layerDidChangeScaling:(CZElementLayer *)layer;

- (void)layer:(CZElementLayer *)layer willRemoveGroup:(CZElementGroup *)group;
- (void)layer:(CZElementLayer *)layer didRemoveGroup:(CZElementGroup *)group;
- (void)layer:(CZElementLayer *)layer didChangeGroup:(CZElementGroup *)group;

- (void)layerUpdateAllMeasurements:(CZElementLayer *)layer;

@end

// Add extension method to CZElementLayer since we need to judge whether there
// exists non-scale-bar annotations for image rotation/cropping/mirroring.
@interface CZElementLayer (NonScaleBarElements)

- (BOOL)hasNonScaleBarElements;

- (BOOL)hasScaleBarElement;

@end

