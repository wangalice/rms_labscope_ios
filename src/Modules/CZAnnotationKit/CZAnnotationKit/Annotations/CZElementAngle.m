//
//  CZElementAngle.m
//  Hermes
//
//  Created by Ralph Jin on 3/22/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementAngle.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodePolyline.h"
#import "CZNodeRectangle.h"
#import "CZNodeArc.h"
#import "CZFeature.h"
#import "CZElementCommon.h"

@interface CZElementAngle ()

@property (nonatomic, assign, readonly) CZNodePolyline *lineNode;
@property (nonatomic, retain) CZNodeArc *arcNode;

- (void)updateFeatures;

@end

@implementation CZElementAngle

+ (Class)nodeClass {
    return [CZNodePolyline class];
}

+ (NSUInteger)minimumPointsCount {
    return 3;
}

- (instancetype)init {
    self = [super initWithPointsCount:3];
    if (self) {
        _arcNode = [[CZNodeArc alloc] init];
        [self.lineNode addSubNode:_arcNode];
    }
    
    return self;
}

- (void)dealloc {
    [_arcNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementAngle *newAngle = [super copyWithZone:zone];

    if (newAngle) {
        CZNode *primaryNode = [newAngle primaryNode];
        if ([primaryNode subNodeCount] > 0) {
            newAngle.arcNode = (CZNodeArc *)[primaryNode  subNodeAtIndex:0];
        }
    }

    return newAngle;
}

- (CZNodePolyline *)lineNode {
    return (CZNodePolyline *)[self primaryNode];
}

- (CGPoint)ptA {
    return [self pointAtIndex:0];
}

- (void)setPtA:(CGPoint)ptA {
    [self setPoint:ptA atIndex:0];
    self.featureValidated = NO;
}

- (CGPoint)ptB {
    return [self pointAtIndex:2];
}

- (void)setPtB:(CGPoint)ptB {
    [self setPoint:ptB atIndex:2];
    self.featureValidated = NO;
}

- (CGPoint)ptP {
    return [self pointAtIndex:1];
}

- (void)setPtP:(CGPoint)ptP {
    [self setPoint:ptP atIndex:1];
    self.featureValidated = NO;
}

#pragma mark - Override CZElement

- (NSUInteger)precision {
    // Angle element's precision is no more than 2.
    NSUInteger precision = [super precision];
    precision = MIN(precision, 2);
    return precision;
}

- (CALayer *)newLayer {
    [self updateFeatures];
    
    self.lineNode.lineWidth = [self suggestLineWidth];
    _arcNode.lineWidth = self.lineNode.lineWidth;
    _arcNode.strokeColor = self.lineNode.strokeColor;

    return [super newLayer];
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];

    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    CGRect lineNodeBox = CGPathGetBoundingBox([self.lineNode relativePath]);
    CGPoint offset = lineNodeBox.origin;
    
    CZFeature *feature = self.features.count > 0 ? self.features[0] : nil;
    NSString *angleString = [feature toLocalizedStringWithPrecision:[self precision] showType:NO];
    CZNodeRectangle *textBox = [self newMeasurementNodeWithString:angleString];
    
    CZNodeArc *offsetArc = [self.arcNode copy];
    offsetArc.cx += offset.x;
    offsetArc.cy += offset.y;
    
    [CZElementCommon putMeasurement:textBox onArc:offsetArc startPoint:self.ptA endPoint:self.ptB];
    [offsetArc release];

    layer = [textBox newLayer];
    [textBox release];
    return layer;
}

+ (NSUInteger)featureCount {
    return 1;
}

- (CGPoint)measurementTextPosition {
    CALayer *layer = [self newMeasurementTextLayer];
    CGPoint pt = layer.frame.origin;
    [layer release];
    
    return pt;
}

- (BOOL)containsPoint:(CGPoint)p {
    [self updateFeatures];
    
    CZNodeShape *lineNode = (CZNodeShape *)[self primaryNode];
    CGRect lineNodeBox = CGPathGetBoundingBox([lineNode relativePath]);
    CGPoint offset = lineNodeBox.origin;
    
    p.x -= offset.x;
    p.y -= offset.y;
    
    CGPoint center = CGPointMake(self.arcNode.cx, self.arcNode.cy);
    return CZPieContainsPoint(p, self.arcNode.startAngle, self.arcNode.endAngle, center, self.arcNode.radius);
}

#pragma mark - Private methods

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    [self updateArcNode];
    
    CGFloat angle = self.arcNode.endAngle - self.arcNode.startAngle;
    angle = CZNormalizeAngle(angle);
    
    [self removeAllFeatures];
    
    CZFeature *feature = [[CZFeature alloc] initWithValue:(angle * 180 / M_PI) unit:kCZFeatureUnitDegree type:kCZFeatureTypeAngle];
    [self appendFeature:feature];
    [feature release];
}

- (void)updateArcNode {
    CGPoint ptA = self.ptA;
    CGPoint ptP = self.ptP;
    CGPoint ptB = self.ptB;
    
    CGPoint vec1 = CGPointMake(ptA.x - ptP.x, ptA.y - ptP.y);
    CGPoint vec2 = CGPointMake(ptB.x - ptP.x, ptB.y - ptP.y);
    
    CGFloat radius1 = sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
    CGFloat radius2 = sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
    _arcNode.radius = MIN(radius1, radius2);
    _arcNode.startAngle = atan2(vec1.y, vec1.x);
    _arcNode.endAngle = atan2(vec2.y, vec2.x);
    
    CGRect lineNodeBox = CGPathGetBoundingBox([self.lineNode relativePath]);
    _arcNode.cx = ptP.x - lineNodeBox.origin.x;
    _arcNode.cy = ptP.y - lineNodeBox.origin.y;
}

@end
