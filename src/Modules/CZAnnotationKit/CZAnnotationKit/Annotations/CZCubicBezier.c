//
//  CZCubicBezier.c
//  Matscope
//
//  Created by Ralph Jin, Paul Heckbert on 3/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#include "CZCubicBezier.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define INV_EPS (1L<<14)

static inline CGPoint
CGPointMiddle(CGPoint a, CGPoint b) {
    CGPoint p; p.x = (a.x + b.x) * 0.5; p.y = (a.y + b.y) * 0.5; return p;
}

static inline
CGPoint CGPointAbs(CGPoint a) {
    a.x = fabs(a.x);a.y = fabs(a.y); return a;
}

static inline
CGPoint CGPointMinusCGPoint(CGPoint a, CGPoint b) {
    CGPoint p; p.x = a.x - b.x; p.y = a.y - b.y; return p;
}

static inline
double log4(double x) {
    return 0.5 * log2(x);
}

// Calculate the roots of quadratic equation
// ax^2 + bx + c = 0
// @param x x[2] = a, x[1] = b, x[0] = c.
// @param outResult, max length is 2.
// @return count of roots
static int QuadraticEquation(const double x[], double *outResult) {
    double q1, q2, delta, a, b, c;
    
    a = x[2];
    b = x[1];
    c = x[0];
    
    if (a == 0.0) {  // fall back to simple equation
        if (b == 0.0) {
            return 0;
        } else {
            outResult[0] = -c / b;
            return 1;
        }
    } else {
        delta = b * b - 4 * a * c;
        
        if(delta < 0) {
            return 0;
        } else {
            q1 = sqrt(delta);
            q2 = (- b - q1) * 0.5;
            q1 = (- b + q1) * 0.5;
            
            if (delta == 0.0) {
                outResult[0] = q1 / a;
                return 1;
            } else {
                outResult[0] = q1 / a;
                outResult[1] = q2 / a;
                return 2;
            }
        }
    }
}

void CubicBezierSplit(const CubicBezier *a, CubicBezier *left, CubicBezier *right) {
    left->p0 = a->p0;
    right->p3 = a->p3;
    left->p1 = CGPointMiddle(a->p0, a->p1);
    right->p2 = CGPointMiddle(a->p2, a->p3);
    right->p1 = CGPointMiddle(a->p1, a->p2); // temporary holding spot
    left->p2 = CGPointMiddle(left->p1, right->p1);
    right->p1 = CGPointMiddle(right->p1, right->p2); // Real value this time
    left->p3 = right->p0 = CGPointMiddle(left->p2, right->p1);
}

bool IsCubicBezierIntersectBB(const CubicBezier *a, const CubicBezier *b) {
    // Compute bounding box for a
    double minAx, maxAx, minAy, maxAy;
    if (a->p0.x > a->p3.x) {     // These are the most likely to be extremal
        minAx = a->p3.x, maxAx = a->p0.x;
    } else {
        minAx = a->p0.x, maxAx = a->p3.x;
    }
    if (a->p2.x < minAx) {
        minAx = a->p2.x;
    } else if (a->p2.x > maxAx) {
        maxAx = a->p2.x;
    }
    if (a->p1.x < minAx) {
        minAx = a->p1.x;
    } else if (a->p1.x > maxAx) {
        maxAx = a->p1.x;
    }
    
    if (a->p0.y > a->p3.y) {
        minAy = a->p3.y, maxAy = a->p0.y;
    } else {
        minAy = a->p0.y, maxAy = a->p3.y;
    }
    if (a->p2.y < minAy) {
        minAy = a->p2.y;
    } else if (a->p2.y > maxAy) {
        maxAy = a->p2.y;
    }
    if (a->p1.y < minAy) {
        minAy = a->p1.y;
    } else if (a->p1.y > maxAy) {
        maxAy = a->p1.y;
    }

    // Compute bounding box for b
    double minbx, maxbx, minby, maxby;
    if (b->p0.x > b->p3.x) {
        minbx = b->p3.x, maxbx = b->p0.x;
    } else {
        minbx = b->p0.x, maxbx = b->p3.x;
    }
    if (b->p2.x < minbx) {
        minbx = b->p2.x;
    } else if (b->p2.x > maxbx) {
        maxbx = b->p2.x;
    }
    if (b->p1.x < minbx) {
        minbx = b->p1.x;
    } else if (b->p1.x > maxbx) {
        maxbx = b->p1.x;
    }
    
    if (b->p0.y > b->p3.y) {
        minby = b->p3.y, maxby = b->p0.y;
    } else {
        minby = b->p0.y, maxby = b->p3.y;
    }
    if (b->p2.y < minby) {
        minby = b->p2.y;
    } else if (b->p2.y > maxby) {
        maxby = b->p2.y;
    }
    if (b->p1.y < minby) {
        minby = b->p1.y;
    } else if (b->p1.y > maxby) {
        maxby = b->p1.y;
    }
    
    // Test bounding box of b against bounding box of a
    if ((minAx > maxbx) || (minAy > maxby)  // Not >= : need boundary case
        || (minbx > maxAx) || (minby > maxAy)) {
        return 0; // they don't intersect
    } else {
        return 1; // they intersect
    }
}

void CubicBezierRecursivelyIntersect(const CubicBezier *a, double t0, double t1, int deptha,
                                     const CubicBezier *b, double u0, double u1, int depthb,
                                     double **parameters, int *index) {
    if (deptha > 0) {
        CubicBezier A[2];
        CubicBezierSplit(a, &A[0], &A[1]);
        double tmid = (t0 + t1) * 0.5;
        deptha--;
        if (depthb > 0) {
            CubicBezier B[2];
            CubicBezierSplit(b, &B[0], &B[1]);
            double umid = (u0 + u1) * 0.5;
            depthb--;
            if (IsCubicBezierIntersectBB(&A[0], &B[0])) {
                CubicBezierRecursivelyIntersect(&A[0], t0, tmid, deptha,
                                                &B[0], u0, umid, depthb,
                                                parameters, index);
            }
            if (IsCubicBezierIntersectBB(&A[1], &B[0])) {
                CubicBezierRecursivelyIntersect(&A[1], tmid, t1, deptha,
                                                &B[0], u0, umid, depthb,
                                                parameters, index);
            }
            if (IsCubicBezierIntersectBB(&A[0], &B[1])) {
                CubicBezierRecursivelyIntersect(&A[0], t0, tmid, deptha,
                                                &B[1], umid, u1, depthb,
                                                parameters, index);
            }
            if (IsCubicBezierIntersectBB(&A[1], &B[1])){
                CubicBezierRecursivelyIntersect(&A[1], tmid, t1, deptha,
                                                &B[1], umid, u1, depthb,
                                                parameters, index);
            }
        } else {
            if (IsCubicBezierIntersectBB(&A[0], b)) {
                CubicBezierRecursivelyIntersect(&A[0], t0, tmid, deptha,
                                                b, u0, u1, depthb,
                                                parameters, index);
            }
            if (IsCubicBezierIntersectBB(&A[1], b)) {
                CubicBezierRecursivelyIntersect(&A[1], tmid, t1, deptha,
                                                b, u0, u1, depthb,
                                                parameters, index);
            }
        }
    } else if (depthb > 0) {
        CubicBezier B[2];
        CubicBezierSplit(b, &B[0], &B[1]);
        double umid = (u0 + u1) * 0.5;
        depthb--;
        if (IsCubicBezierIntersectBB(a, &B[0])) {
            CubicBezierRecursivelyIntersect(a, t0, t1, deptha,
                                            &B[0], u0, umid, depthb,
                                            parameters, index);
        }
        if (IsCubicBezierIntersectBB(a, &B[1])) {
            CubicBezierRecursivelyIntersect(a, t0, t1, deptha,
                                            &B[0], umid, u1, depthb,
                                            parameters, index);
        }
    } else { // Both segments are fully subdivided; now do line segments
        double xlk = a->p3.x - a->p0.x;
        double ylk = a->p3.y - a->p0.y;
        double xnm = b->p3.x - b->p0.x;
        double ynm = b->p3.y - b->p0.y;
        double xmk = b->p0.x - a->p0.x;
        double ymk = b->p0.y - a->p0.y;
        double det = xnm * ylk - ynm * xlk;
        
        if (1.0 + det == 1.0) {
            return;
        } else {
            double detinv = 1.0 / det;
            double s = (xnm * ymk - ynm *xmk) * detinv;
            double t = (xlk * ymk - ylk * xmk) * detinv;
            if ((s < 0.0) || (s > 1.0) || (t < 0.0) || (t > 1.0)) {
                return;
            }
            parameters[0][*index] = t0 + s * (t1 - t0);
            parameters[1][*index] = u0 + t * (u1 - u0);
            (*index)++;
        }
    }
}

int CubicBezierFindIntersections(const CubicBezier *a, const CubicBezier *b, double *paramA, double *paramB) {
    int index = 0;
    if (IsCubicBezierIntersectBB(a, b)) {
        double *parameters[2];
        parameters[0] = paramA;
        parameters[1] = paramB;
        
        CGPoint la1 = CGPointAbs(CGPointMinusCGPoint(CGPointMinusCGPoint(a->p2, a->p1), CGPointMinusCGPoint(a->p1, a->p0)));
        CGPoint la2 = CGPointAbs(CGPointMinusCGPoint(CGPointMinusCGPoint(a->p3, a->p2), CGPointMinusCGPoint(a->p2, a->p1)));
        CGPoint la;
        if (la1.x > la2.x) la.x = la1.x; else la.x = la2.x;
        if (la1.y > la2.y) la.y = la1.y; else la.y = la2.y;
        
        CGPoint lb1 = CGPointAbs(CGPointMinusCGPoint(CGPointMinusCGPoint(b->p2, b->p1), CGPointMinusCGPoint(b->p1, b->p0)));
        CGPoint lb2 = CGPointAbs(CGPointMinusCGPoint(CGPointMinusCGPoint(b->p3, b->p2), CGPointMinusCGPoint(b->p2, b->p1)));
        CGPoint lb;
        if (lb1.x > lb2.x) lb.x = lb1.x; else lb.x = lb2.x;
        if (lb1.y > lb2.y) lb.y = lb1.y; else lb.y = lb2.y;
        
        double l0;
        if (la.x > la.y) {
            l0 = la.x;
        } else {
            l0 = la.y;
        }
        int ra;
        if (l0 * 0.75 * M_SQRT2 + 1.0 == 1.0) {
            ra = 0;
        } else {
            ra = (int)ceil(log4(M_SQRT2 * 0.75 * INV_EPS * l0));
        }
        if (lb.x > lb.y) {
            l0 = lb.x;
        } else {
            l0 = lb.y;
        }
        int rb;
        if (l0 * 0.75 * M_SQRT2 + 1.0 == 1.0) {
            rb = 0;
        } else {
            rb = (int)ceil(log4(M_SQRT2 * 0.75 * INV_EPS * l0));
        }
        
        CubicBezierRecursivelyIntersect(a, 0.0, 1.0, ra, b, 0.0, 1.0, rb, parameters, &index);
    }
    return index;
}


/** Cubic Bezier(t) may self intersected
 * solve the following two equations to get the parameter t0, t1 of bezier line cross point
 * k3*t1^3 + k2*t1^2 + k1*t1 + k0 = k3*t0^3 + k2*t0^2 + k1*t0 + k0
 * g3*t1^3 + g2*t1^2 + g1*t1 + g0 = g3*t0^3 + g2*t0^2 + g1*t0 + g0 , (t0 != t1)
 * let sT = t1 + t0, pT = t1*t0, and simplify the equations get,
 *        k3*T^2 + k2*T + k1 = k3*K
 *        g3*T^2 + g2*T + g1 = g3*K
 * so,   T = (k3*g1 - k1*g3) / (k2*g3 - k3*g2);
 * and t0 and t1 can be calculate by the result of T and K.
 */
int CubicBezierFindSelfIntersections(const CubicBezier *a, double *param) {
    int index = 0, count = 0;
    double sT, pT;  // sT = sum t0, t1; pT = product t0, t1
    double g[4];
    double k[4];
    double t[2];
    double equationParam[3];
    
    k[0] = a->p0.y;
    k[1] = (-a->p0.y + a->p1.y) * 3.0;
    k[2] = (a->p0.y - a->p1.y * 2.0 + a->p2.y) * 3.0;
    k[3] = -a->p0.y + a->p1.y * 3.0 - a->p2.y * 3.0 + a->p3.y;
    
    g[0] = a->p0.x;
    g[1] = (-a->p0.x + a->p1.x) * 3.0;
    g[2] = (a->p0.x - a->p1.x * 2.0 + a->p2.x) * 3.0;
    g[3] = -a->p0.x + a->p1.x * 3.0 - a->p2.x * 3.0 + a->p3.x;
    
    if (g[3] == 0.0 && k[3] == 0.0) {
        return 0;
    }

    double temp = k[2] * g[3] - k[3] * g[2];
    if (temp == 0.0) {
        return 0;  // no root or too many roots for pT, sT
    }
    sT = (k[3] * g[1] - k[1] * g[3]) / temp;
    
    if (g[3] == 0) {
        pT = (k[3] * sT * sT + k[2] * sT + k[1]) / k[3];
    } else {
        pT = (g[3] * sT * sT + g[2] * sT + g[1]) / g[3];
    }
    
    if (pT < 0.0 || pT > 1.0 || sT < 0.0 || sT > 2.0) {  // 0 <= t0 <= 1, 0 <= t1 <= 1 ---> 0 <= t0*t1 <= 1, 0 <= t0+t1 <= 2
        return 0;
    }
    
    equationParam[0] = pT;
    equationParam[1] = -sT;
    equationParam[2] = 1;
    
    count = QuadraticEquation(equationParam, t);
    
    if (count == 2) {
        for (int i = 0; i < count; i++) {
            if (t[i] >= 0.0 && t[i] <= 1.0) {
                param[index] = t[i];
                index++;
            }
        }
    }
    
    return index;
}