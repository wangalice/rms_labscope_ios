//
//  CZElementMultiPointKind.h
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CZElementMultiPointKind <NSObject>

@required
- (NSUInteger)pointsCount;

- (CGPoint)pointAtIndex:(NSUInteger)index;

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index;

- (void)appendPoint:(CGPoint)pt;

- (void)removePointAtIndex:(NSUInteger)index;

/** @return minimum point count that can make up this multi-point shape. */
- (NSUInteger)minimumPointsCount;

@optional
/** @return maximum point count that multi-point shape can have.
 * If not implemented means there's no limitation.
 */
- (NSUInteger)maximumPointsCount;

@end
