//
//  CZColorGenerator.m
//  Matscope
//
//  Created by Sherry Xu on 8/6/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZColorGenerator.h"
#import "CZDefaultSettings+Annotations.h"

@interface CZColorGenerator () {
   @private BOOL _disableColorfulPalette;
}

@property (nonatomic, retain) NSArray *colorMap;

@end


@implementation CZColorGenerator

static CZColorGenerator *uniqueInstance = nil;

#pragma mark - Singleton Design Pattern

+ (CZColorGenerator *)sharedInstance {
    @synchronized ([CZColorGenerator class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (instancetype)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (instancetype)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (instancetype)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _disableColorfulPalette = [[CZDefaultSettings sharedInstance] disableMeasurementColorCoding];
    }
    return self;
}

- (void)dealloc {
    [_colorMap release];
    
    [super dealloc];
}

#pragma mark - Public interfaces

- (CZColor)colorFromColorMapAtIndex:(NSUInteger)index {
    CZColor color;
    if (!_disableColorfulPalette) {
        NSNumber *hex = [[self colorMap] objectAtIndex:(index % self.colorMap.count)];
        long hexValue = [hex longValue];
        uint8_t alpha,red, green, blue;
        alpha = (uint8_t)((hexValue & 0xFF000000) >> 24);
        red = (uint8_t)((hexValue & 0xFF0000) >> 16);
        green = (uint8_t)((hexValue & 0xFF00) >> 8);
        blue = (uint8_t)(hexValue & 0xFF);
        color = CZColorMakeRGBA(red, green, blue, alpha);
    } else {
        color = CZColorMake(0xA9, 0xA9, 0xA9);
    }
    return color;
}

- (void)setDisableMeasurementColorCoding:(BOOL)disableMeasurementColorCoding {
    _disableColorfulPalette = disableMeasurementColorCoding;
}

- (BOOL)disableMeasurementColorCoding {
    return _disableColorfulPalette;
}

- (NSArray *)colorMap {
    if ([_colorMap count] == 0) {
        self.colorMap = @[@0XFFFF0000, //Red
                          @0XFF00FF00, //lime
                          @0XFF0000FF, //Blue
                          @0XFFFFFF00, //Yellow
                          @0XFFFF00FF, //Magenta
                          @0XFF00FFFF, //Cyan
                          @0XFF800000, //Maroon
                          @0XFF008000, //Green
                          @0XFF808000, //Oliver
                          @0XFF800080, //Purple //10
                          @0XFF000080, //Navy
                          @0XFF008080, //Teal
                          @0XFF808080, //Gray
                          @0XFFFFC0CB, //Pink
                          @0XFFDC143C, //Crimmson
                          @0XFFFF1493, //Deepink
                          @0XFF1E90FF, //DodgerBlue
                          @0XFFC0C0C0, //Sliver
                          @0XFFB8860B, //DarkGoldenRod
                          @0XFFFFDAB9, //PeachPuff//20
                          @0XFFFFA500, //Orange
                          @0XFF556B2F, //DarkOliveGreen
                          @0XFF4169E1, //RoyalBlue
                          @0XFFF4A460, //SandyBrown
                          @0XFFDDA0DD, //Plum
                          @0XFF87CEEB, //SkyBlue
                          @0XFF708090, //SlateGray
                          @0XFF9ACD32, //YellowGreen
                          @0XFFB22222, //FireBrick
                          @0XFF8A2BE2, //BlueViolet //30
                          @0XFFFA8072, //Salmon
                          @0XFF8B4513, //SaddleBrown
                          @0XFFDB7093, //PaleVilotRed
                          @0XFF9370DB, //MediumPurple
                          @0XFF696969, //DimGray
                          @0XFFFFFFE0, //LightYellow
                          @0XFFFF4500, //OrangeRed
                          @0XFF2E8B57, //SeaGreen
                          @0XFFEE82EE, //Violet //40
                          @0XFF00FF7F, //SpringGreen
                          @0XFFBC8F8F, //RosyBrown
                          @0XFFB0E0E6, //PowderBlue
                          @0XFFD2691E, //Chocolate
                          @0XFFFFD700, //Gold
                          @0XFFFF69B4, //HotPink
                          @0XFFF08080, //LightCoral
                          @0XFFEEE8AA, //PaleGoldenRod
                          @0XFFE6E6FA, //Lavender
                          @0XFFFFF8DC, //Cornsilk //50
                          @0XFFFFF0F5, //Lavender
                          @0XFFD8BFD8, //Thistle
                          @0XFFCD5C5C, //IndiaRed
                          @0XFFBDB76B, //Darkkhaki
                          @0XFFCD853F, //Peru
                          @0XFFD2B48C, //Tan
                          @0XFFE9967A, //DarkSalmon
                          @0XFFF0E68C, //Khaki
                          @0XFFFFE4E1, //MistyRose
                          @0XFF8FBC8F, //DarkSeaGreen //60
                          @0XFF7B68EE, //MediumSlateBlue
                          @0XFF6B8E23, //OliverDrab
                          @0XFF4B0082, //Indigo
                          @0XFFFFDEAD, //NavajoWhite
                          @0XFF7FFFD4, //Aquamarine
                          @0XFF2F4F4f, //DarkSlateGray
                          @0XFF5F9EA0, //CadetBlue
                          @0XFF90EE90, //LightGreen
                          @0XFF4BD1CC, //Sienna
                          @0XFFB0C4DE, //LightSteelBlue
                          @0XFF483D8B  //DarkSlateBlue //70
                          ];
    }
    return _colorMap;
}

@end
