//
//  CZElementSplineContour.m
//  Hermes
//
//  Created by Jin Ralph on 13-12-16.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementSplineContour.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZGeometryHelper.h"
#import "CZNodeSplineContour.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"

#if CALCULATE_AERA_USE_CRACK_ALGO
#import "CZElementLayer.h"
#import "CZGemMeasurer.h"
#import "CZGeometryHelper.h"
#else
#import "CZElementCommon.h"
#import "CZNodeGeom.h"
#import "CZCubicBezier.h"
#endif

enum {
    CZElementSplineContourFeatureArea = 0,
    CZElementSplineContourFeaturePerimeter,
    
    CZElementSplineContourFeatureCount
};

@interface CZElementSplineContour () {
    CGPoint _measurementTextPosition;
}

@end

@implementation CZElementSplineContour

+ (Class)nodeClass {
    return [CZNodeSplineContour class];
}

+ (NSUInteger)minimumPointsCount {
    return 3;
}

#pragma mark - override CZElement

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }

    CZFeature *areaFeature = self.features[CZElementSplineContourFeatureArea];
    NSString *areaString = nil;
    if (areaFeature.value >= 0.0f) {
        areaString = [areaFeature toLocalizedStringWithPrecision:[self precision]];
    }

    CZFeature *perimeterFeature = self.features[CZElementSplineContourFeaturePerimeter];
    NSString *perimeterString = [perimeterFeature toLocalizedStringWithPrecision:[self precision]];

    NSString *feastureString;
    if (areaString) {
        feastureString = [areaString stringByAppendingString:@"\n"];
        feastureString = [feastureString stringByAppendingString:perimeterString];
    } else {
        feastureString = perimeterString;
    }
    
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:feastureString];
    _measurementTextPosition = [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

+ (NSUInteger)featureCount {
    return CZElementSplineContourFeatureCount;
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    NSString *unit2d = [self unitNameOfAreaByStyle:unitStyle];
	
    CGFloat scaledArea = 0;
    CGFloat scaledPerimeter = 0;
    
    const CGFloat logicalScaling = self.logicalScaling;
    const CGFloat scalingOfLogicalPoint = scaling / logicalScaling;
    
    CZNodeSplineContour *splineNode = (CZNodeSplineContour *)self.primaryNode;
    const NSUInteger controlPointsCount = [splineNode pointsCount];
    if (controlPointsCount > 2) {
        CGPoint *controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);
        for (NSUInteger i = 0; i < controlPointsCount; i++) {
            controlPoints[i] = [splineNode pointAtIndex:i];
            controlPoints[i].x *= logicalScaling;
            controlPoints[i].y *= logicalScaling;
        }
        CFDataRef pointsData = ComputeBezierCurvePoints(controlPoints, controlPointsCount, true, true);
        free((void *)controlPoints);
        
        if (pointsData) {
            size_t pointsCount = CFDataGetLength(pointsData) / sizeof(CGPoint);
            
#if CALCULATE_AERA_USE_CRACK_ALGO
            CZGemMeasure *measurer = [[CZGemMeasure alloc] init];
            [measurer setRegion:(const CGFloat *)CFDataGetBytePtr(pointsData) pointsCount:pointsCount];
            CFRelease(pointsData);
            
            scaledArea = [measurer area] * scalingOfLogicalPoint * scalingOfLogicalPoint;
            scaledPerimeter = [measurer perimeter] * scalingOfLogicalPoint;
            [measurer release];
#else
            
            scaledArea = [CZElementMultiPoint calcAreaOfPolygonPoints:(const CGPoint *)CFDataGetBytePtr(pointsData) pointsCount:pointsCount] * scalingOfLogicalPoint * scalingOfLogicalPoint;
            scaledPerimeter = [CZElementMultiPoint calcLengthOfPoints:(const CGPoint *)CFDataGetBytePtr(pointsData) pointsCount:pointsCount] * scalingOfLogicalPoint;
            
            CFRelease(pointsData);
#endif
        }
    } else if (controlPointsCount == 2) {
        const CGPoint *controlPoints = [splineNode pointArray];
        CGFloat dx = controlPoints[0].x - controlPoints[1].x;
        CGFloat dy = controlPoints[0].y - controlPoints[1].y;
        double perimeter = sqrt(dx * dx + dy * dy) * 2.0;
        scaledPerimeter = perimeter * scaling;
    }
    

    // calculate in theory
//    double lengthInPixel, areaInPixel;
//    [self calculateLength:&lengthInPixel area:&areaInPixel];
//    CGFloat scaledPerimeter = scaling * lengthInPixel;
//    CGFloat scaledArea = scaling * areaInPixel;

    
    [self removeAllFeatures];
    
    // area measurement
    CZFeature *areaFeature = [[CZFeature alloc] initWithValue:scaledArea unit:unit2d type:kCZFeatureTypeArea];
    [self appendFeature:areaFeature];
    [areaFeature release];
    
    // perimeter
    CZFeature *diameterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypePerimeter];
    [self appendFeature:diameterFeature];
    [diameterFeature release];
}

- (BOOL)hasMeasurement {
    return YES;  // TODO: sync CZGemMeasure class from Munich
}

- (CGPoint)measurementTextPosition {
    return _measurementTextPosition;
}

#pragma mark - Private methods

#if (!CALCULATE_AERA_USE_CRACK_ALGO)
- (void)calculateLength:(double *)length area:(double *)area{
    NSUInteger pointCount = [self pointsCount];
    if (pointCount <= 1) {
        if (length) {
            *length = 0;
        }
        if (area) {
            *area = 0;
        }
    } else if (pointCount == 2) {
        if (length) {
            CGPoint p0 = [self pointAtIndex:0];
            CGPoint p1 = [self pointAtIndex:1];
            *length = sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y));
        }
        if (area) {
            *area = 0;
        }
    } else {
        NSUInteger controlPointsCount = pointCount * 3 + 1;
        CGPoint *controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);
        ComputeCloseBezierControlPoints([self points], pointCount, controlPoints);
        
        if (length) {
            *length = [CZElementCommon lengthOfCubicBezierSegments:controlPoints pointsCount:controlPointsCount];
        }
        
        if (area) {
            BOOL hasIntersection = NO;
            double ta[9], tb[9];
            NSUInteger lastSegmentIndex = pointCount - 1;
            
            for (NSUInteger i = 0; i < pointCount; i++) {
                CubicBezier *a = (CubicBezier *)&controlPoints[i * 3];
                int length = CubicBezierFindSelfIntersections(a, ta);
                if (length == 2) {
                    hasIntersection = YES;
                    break;
                }
                
                for (NSUInteger j = i + 1; j < pointCount; j++) {
                    CubicBezier *b = (CubicBezier *)&controlPoints[j * 3];
                    int length = CubicBezierFindIntersections(a, b, ta, tb);
                    if (length > 0) {
                        if ((j - i) == 1 || (i == 0 && j == lastSegmentIndex)) {  // segment next to each other
                            if ((length == 1) && ((ta[0] == 1 && tb[0] == 0) ||
                                                  (ta[0] == 0 && tb[0] == 1))) {
                                continue;
                            } else {
                                hasIntersection = YES;
                                break;
                            }
                        } else {
                            hasIntersection = YES;
                            break;
                        }
                    }
                }
                
                if (hasIntersection) {
                    break;
                }
            }
            
            if (!hasIntersection) {
                *area = [CZElementCommon areaOfCubicBezierSegments:controlPoints pointsCount:controlPointsCount];
            } else {
                *area = -1;  // invalid area
            }
        }
        free(controlPoints);
    }
}
#endif  // (!CALCULATE_AERA_USE_CRACK_ALGO)

@end
