//
//  CZElementRectangle.h
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"
#import "CZElementRotatable.h"

enum {
    CZElementRectangleFeatureArea = 0,
    CZElementRectangleFeatureCenterX,
    CZElementRectangleFeatureCenterY,
    CZElementRectangleFeaturePerimeter,
    
    CZElementRectangleFeatureCount
};

@interface CZElementRectangle : CZElement<CZElementRotatable>

@property (nonatomic) CGRect rect;

- (instancetype)initWithRect:(CGRect)rect NS_DESIGNATED_INITIALIZER;

@end
