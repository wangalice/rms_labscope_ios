//
//  CZElementScaleBar.m
//  Hermes
//
//  Created by Ralph Jin on 2/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementScaleBar.h"
#import "CZNodeLine.h"
#import "CZNodeText.h"
#import "CZNodeRectangle.h"
#import "CZElementLayer.h"
#import "CZFeature.h"
#import <CZToolbox/CZToolbox.h>

const CGFloat kDefaultScaleBarMargin = 20.0f;
const static float kDefaultCornerRadius = 12.0f;
const static CGFloat kInvalidScaleBarPercent = -1.0;

@interface CZElementScaleBar()

@property (nonatomic, retain) CZNodeRectangle *background;
@property (nonatomic, retain) CZNodeLine *bar;
@property (nonatomic, retain) CZNodeLine *leftTick;
@property (nonatomic, retain) CZNodeLine *rightTick;
@property (nonatomic, retain) CZNodeText *label;
@property (nonatomic, assign) CGFloat tolerance;
@property (nonatomic, assign) CGFloat percentX;
@property (nonatomic, assign) CGFloat percentY;
@property (nonatomic, assign) CGRect lastParentFrameBound;
@property (nonatomic, assign) BOOL copyInSnapping;

@end

@implementation CZElementScaleBar

- (instancetype)init {
    return [self initWithPoint:CGPointZero length:200.0f];
}

- (instancetype)initWithPoint:(CGPoint)ponit length:(CGFloat)length {
    return [self initWithPoint:ponit length:length bound:CGRectZero];
}

- (instancetype)initWithPoint:(CGPoint)point length:(CGFloat)length bound:(CGRect)frameBound {
    self = [super init];
    if (self) {
        _point = point;
        _length = length;
        _margin = kDefaultScaleBarMargin;
        
        _background = [[CZNodeRectangle alloc] init];
        _background.lineWidth = 0.0f;
        _background.fillColor = kCZColorWhite;
        _background.cornerRadius = kDefaultCornerRadius;
        
        _bar = [[CZNodeLine alloc] initWithPoint:point anotherPoint:point];
        [_background addSubNode:_bar];
        
        _leftTick = [[CZNodeLine alloc] init];
        [_background addSubNode:_leftTick];
        _rightTick = [[CZNodeLine alloc] init];
        [_background addSubNode:_rightTick];
        _label = [[CZNodeText alloc] init];
        [_background addSubNode:_label];
        
        CZColor color = kCZColorBlack;
        _bar.strokeColor = color;
        _leftTick.strokeColor = color;
        _rightTick.strokeColor = color;
        _label.strokeColor = color;
        
        self.identifier = @"0";  // 0 is reserved by scale bar
        
        _tolerance = 88; //Default value
        _lastParentFrameBound = frameBound;
        [self calculatePercent:point atBound:frameBound];
    }
    
    return self;
}

- (void)dealloc {
    [_bar release];
    [_leftTick release];
    [_rightTick release];
    [_label release];
    [_background release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementScaleBar *newScaleBar = [super copyWithZone:zone];
    if (newScaleBar) {
        [newScaleBar->_background release];
        newScaleBar->_background = [self->_background copyWithZone:zone];
        newScaleBar->_length = self->_length;
        newScaleBar->_point = self->_point;
        newScaleBar->_position = self->_position;
        
        newScaleBar.bar = (CZNodeLine *)[newScaleBar->_background subNodeAtIndex:0];
        newScaleBar.leftTick = (CZNodeLine *)[newScaleBar->_background subNodeAtIndex:1];
        newScaleBar.rightTick = (CZNodeLine *)[newScaleBar->_background subNodeAtIndex:2];
        newScaleBar.label = (CZNodeText *)[newScaleBar->_background subNodeAtIndex:3];
        newScaleBar.percentX = self.percentX;
        newScaleBar.percentY = self.percentY;
        newScaleBar.copyInSnapping = YES;
    }
    
    return newScaleBar;
}

#pragma mark - Override CZElement

- (CZNode *)primaryNode {
    return _background;
}

- (void)setPoint:(CGPoint)point {
    _point = point;
    [self calculatePercent:point atBound:self.lastParentFrameBound];
    self.featureValidated = NO;
}

- (void)setLength:(CGFloat)length {
    _length = length;
    self.featureValidated = NO;
}

- (void)setSize:(CZElementSize)size {
    [super setSize:size];
    self.featureValidated = NO;
}

#pragma mark - Public method

+ (BOOL)isValidPosition:(CZScaleBarPosition)position {
    switch (position) {
        case kCZScaleBarPositionTopLeft:
        case kCZScaleBarPositionTopRight:
        case kCZScaleBarPositionBottomLeft:
        case kCZScaleBarPositionBottomRight:
            return YES;
        default:
            return NO;
    }
}

+ (CGFloat)truncateValue:(CGFloat)value {
    int times = 0;
    
    if (!isnormal(value)) {
        return 0.0f;
    }
    
    if (value == 0.0f) {
        return 0.0f;
    }
    
    CGFloat v = fabs(value);
    while (v < 1.0f) {
        v *= 10.0f;
        --times;
    }
    
    while (v >= 10.0f) {
        v /= 10.0f;
        ++times;
    };
    
    if (v < 1.5f) {
        v = 1.0f;
    } else if (v < 3.5f) {
        v = 2.0f;
    } else if (v < 7.5f) {
        v = 5.0f;
    } else {
        v = 10.0f;
    }
    
    return v * pow(10.0f, times);
}

- (BOOL)isScaleBarOverlappedWithRect:(CGRect)rect {
    if (CGRectEqualToRect(rect, CGRectZero) == NO) {
        CGRect frame = CGRectMake(self.background.x, self.background.y, self.background.width, self.background.height);
        CGFloat widthDistance = (frame.size.width + rect.size.width)/2.0;
        CGFloat heightDistance = (frame.size.height + rect.size.height)/2.0;
        CGFloat oWidthDistance = fabs(CGRectGetMidX(frame) - CGRectGetMidX(rect));
        CGFloat oHeightDistance = fabs(CGRectGetMidY(frame) - CGRectGetMidY(rect));
        if ( oHeightDistance <= heightDistance  &&
            oWidthDistance <= widthDistance ) {
            return YES;
        }
    }
    return NO;
}

- (void)snapToDefaultPosition:(CGFloat)tolerance {
    
    if (self.parent == NULL) {
        return;
    }
    
    //Only record when tolerace is an support situation;
    _tolerance = tolerance;
    
    self.position = [self scaleBarPosition];
}

- (void)updateLength {
    self.featureValidated = NO;
}

- (CZScaleBarPosition)calucationScaleBarPositionWithTolerance:(CGFloat)tolerance {
    if (self.parent == NULL) {
        return kCZScaleBarPositionFree;
    }
    
    BOOL parentHasLogicalScaling = self.parent.logicalScaling > 0;
    
    const CGFloat suggestLineWidth = [self suggestLineWidth];
    const CGFloat halfLineWidth = 0.5f * suggestLineWidth;
    const CGFloat tickHeight = suggestLineWidth * 12;  // 12 times of the line width
    const CGFloat halfTickHeight = tickHeight * 0.5;
    const CGFloat radius = parentHasLogicalScaling ? kDefaultCornerRadius / self.parent.logicalScaling : kDefaultCornerRadius;
    CGRect parentBounds = [self.parent frame];
    parentBounds.origin = CGPointZero;
    
    // calculate label size
    const CGFloat labelWidth = _label.width;
    const CGFloat labelHeight = _label.height;
    const CGFloat scaleBarWidth = MAX(labelWidth, _length) + radius * 2;
    const CGFloat scaleBarHeight = labelHeight + tickHeight + radius * 2;
    
    // calculate coordinates
    const CGFloat barOffsetX = labelWidth > _length ? (labelWidth - self.length) * 0.5 : 0;
    const CGFloat barLeft = barOffsetX + radius + halfLineWidth;
    const CGFloat tickTop = radius + labelHeight;
    const CGFloat barY = tickTop + halfTickHeight;
    const CGFloat marginToEdge = parentHasLogicalScaling? _margin / self.parent.logicalScaling : _margin;
    
    CGPoint p1 = CGPointMake(_point.x - barLeft, _point.y - barY);
    CGPoint p;
    p.x = marginToEdge;
    p.y = marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= tolerance) {
        return kCZScaleBarPositionTopLeft;
    }
    
    p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
    p.y = marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= tolerance) {
        return kCZScaleBarPositionTopRight;
    }
    
    p.x = marginToEdge;
    p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= tolerance) {
        return kCZScaleBarPositionBottomLeft;
    }
    
    p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
    p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= tolerance) {
        return kCZScaleBarPositionBottomRight;
    }
    
    return kCZScaleBarPositionFree;
}

#pragma mark - Setters

- (void)setColor:(CZColor)color {
}

- (void)setPosition:(CZScaleBarPosition)position {
    if (_position == position) {
        return;
    }
    
    _position = position;
    self.featureValidated = NO;
}

- (void)setFillColor:(CZColor)fillColor {
    _background.fillColor = fillColor;
}

- (void)setStrokeColor:(CZColor)strokeColor {
    _bar.strokeColor = strokeColor;
    _leftTick.strokeColor = strokeColor;
    _rightTick.strokeColor = strokeColor;
    _label.strokeColor = strokeColor;
}

#pragma mark - Getters

- (CALayer *)newLayer {
    [self updateScaleBar];
    CALayer *layer = [_background newLayer];
    return layer;
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    CGPoint p1 = [self point];
    p1 = CGPointApplyAffineTransform(p1, transform);
    [self setPoint:p1];
    [self notifyDidChange];
}

- (CZColor)fillColor {
    return _background.fillColor;
}

- (CZColor)strokeColor {
    return _bar.strokeColor;
}

#pragma mark - Private Methods

//create scale bar
- (void)updateScaleBar {
    if (self.featureValidated) {
        return;
    }
    
    [self calculatePreferLength];
    
    BOOL parentHasLogicalScaling = self.parent && self.parent.logicalScaling > 0;
    
    const CGFloat suggestFontSize = [self suggestFontSize];
    const CGFloat suggestLineWidth = [self suggestLineWidth];
    const CGFloat halfLineWidth = 0.5f * suggestLineWidth;
    const CGFloat tickHeight = MAX(6, (_length / 6));  // 1/6th of scale bar length
    const CGFloat halfTickHeight = tickHeight * 0.5;
    const CGFloat radius = parentHasLogicalScaling ? kDefaultCornerRadius/self.parent.logicalScaling : kDefaultCornerRadius;

    // Update bar's location according to _position.
    CGRect parentBounds = [self.parent frame];
    parentBounds.origin = CGPointZero;
    
    // calculate label text
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    CGFloat value = scaling * self.length;
    
    NSString *unitOfDisplay = L([self unitNameOfDistanceByStyle:unitStyle]);
    
    NSString *str = @"";
    CGFloat threshold = 1.0;
    const CGFloat kEpislon = 1e-6;
    for (NSUInteger precision = 0; precision < 9; precision++, threshold *= 0.1) {
        if (value >= (threshold - kEpislon)) {
            NSString *format = [NSString stringWithFormat:@"%%.%luf %%@", (unsigned long)precision];
            str = [NSString stringWithFormat:format, value, unitOfDisplay];
            break;
        }
    }
    
    _label.string = str;
    
    // calculate label size
    _label.fontSize = suggestFontSize;
    _label.width = self.length;
    [_label setSuggestSize];
    const CGFloat labelWidth = _label.width;
    const CGFloat labelHeight = _label.height;
    const CGFloat scaleBarWidth = MAX(labelWidth, _length) + radius * 2;
    const CGFloat scaleBarHeight = labelHeight + tickHeight + radius * 2;
    
    // calculate coordinates
    const CGFloat barOffsetX = labelWidth > _length ? (labelWidth - self.length) * 0.5 : 0;
    const CGFloat barLeft = barOffsetX + radius + halfLineWidth;
    const CGFloat barRight = barLeft + _length - suggestLineWidth;
    const CGFloat tickTop = radius + labelHeight;
    const CGFloat barY = tickTop + halfTickHeight;
    const CGFloat tickBottom = tickTop + tickHeight;

    // horizontal bar
    _bar.p1 = CGPointMake(barLeft, barY);
    _bar.p2 = CGPointMake(barRight, barY);
    _bar.lineWidth = suggestLineWidth;
    
    // left tick
    _leftTick.p1 = CGPointMake(barLeft, tickTop);
    _leftTick.p2 = CGPointMake(barLeft, tickBottom);
    _leftTick.lineWidth = suggestLineWidth;
    
    // right tick
    _rightTick.p1 = CGPointMake(barRight, tickTop);
    _rightTick.p2 = CGPointMake(barRight, tickBottom);
    _rightTick.lineWidth = suggestLineWidth;
    
    // label position
    _label.x = (barLeft + barRight - labelWidth) * 0.5;
    _label.y = radius;    
    
    
    const CGFloat marginToEdge = parentHasLogicalScaling? _margin / self.parent.logicalScaling : _margin;
    
    BOOL parentFrameChanged = !CGSizeEqualToSize(parentBounds.size, CGSizeMake(1, 1)) && !CGRectEqualToRect(self.lastParentFrameBound, parentBounds);
    if (parentFrameChanged) {
        if (self.copyInSnapping) {
            self.copyInSnapping = NO;
            if (![self verifyPercent]) {
                [self calculatePercent:_point atBound:parentBounds];
            }
        } else {
            if ([self verifyPercent]) {
                _point.x = self.percentX * parentBounds.size.width;
                _point.y = self.percentY * parentBounds.size.height;
            } else {
                // if percent is invalid, set to bottom right corner.
                _point.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
                _point.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
            }
            
            self.lastParentFrameBound = parentBounds;
        }
    }
    
    CGPoint p;
    switch (_position) {
        case kCZScaleBarPositionTopLeft:
            p.x = marginToEdge;
            p.y = marginToEdge;
            break;
        case kCZScaleBarPositionTopRight:
            p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
            p.y = marginToEdge;
            break;
        case kCZScaleBarPositionBottomLeft:
            p.x = marginToEdge;
            p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
            break;
        case kCZScaleBarPositionBottomRight:
            p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
            p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
            break;
        default:
            p.x = _point.x - barLeft;
            p.y = _point.y - barY;
            break;
    }
    
    if ([CZElementScaleBar isValidPosition:_position]) {
        _point = CGPointMake(p.x + barLeft, p.y + barY);
        [self calculatePercent:_point atBound:parentBounds];
    }
    
    if (parentFrameChanged) {
        CGRect childBound = CGRectMake(_point.x, _point.y, scaleBarWidth, scaleBarHeight);
        CGVector vector = [CZElementScaleBar calculateOffsetVectorIfCrossBound:childBound :parentBounds :marginToEdge];
        _point.x -= vector.dx;
        _point.y -= vector.dy;
    }
   

    _background.x = p.x;
    _background.y = p.y;
    _background.width = scaleBarWidth;
    _background.height = scaleBarHeight;
    _background.cornerRadius = radius;
    
    self.featureValidated = YES;
}

- (CZScaleBarPosition)scaleBarPosition {
    CZScaleBarPosition position = kCZScaleBarPositionFree;
    
    BOOL parentHasLogicalScaling = self.parent.logicalScaling > 0;
    const CGFloat suggestLineWidth = [self suggestLineWidth];
    const CGFloat halfLineWidth = 0.5f * suggestLineWidth;
    const CGFloat tickHeight = suggestLineWidth * 12;  // 12 times of the line width
    const CGFloat halfTickHeight = tickHeight * 0.5;
    const CGFloat radius = parentHasLogicalScaling ? kDefaultCornerRadius / self.parent.logicalScaling : kDefaultCornerRadius;
    CGRect parentBounds = [self.parent frame];
    parentBounds.origin = CGPointZero;
    
    // calculate label size
    const CGFloat labelWidth = _label.width;
    const CGFloat labelHeight = _label.height;
    const CGFloat scaleBarWidth = MAX(labelWidth, _length) + radius * 2;
    const CGFloat scaleBarHeight = labelHeight + tickHeight + radius * 2;
    
    // calculate coordinates
    const CGFloat barOffsetX = labelWidth > _length ? (labelWidth - self.length) * 0.5 : 0;
    const CGFloat barLeft = barOffsetX + radius + halfLineWidth;
    const CGFloat tickTop = radius + labelHeight;
    const CGFloat barY = tickTop + halfTickHeight;
    const CGFloat marginToEdge = parentHasLogicalScaling? _margin / self.parent.logicalScaling : _margin;
    
    // calucate position
    CGPoint p1 = CGPointMake(_point.x - barLeft, _point.y - barY);
    CGPoint p;
    p.x = marginToEdge;
    p.y = marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= self.tolerance) {
        position = kCZScaleBarPositionTopLeft;
    }
    
    p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
    p.y = marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= self.tolerance) {
        position = kCZScaleBarPositionTopRight;
    }
    
    p.x = marginToEdge;
    p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= self.tolerance) {
        position = kCZScaleBarPositionBottomLeft;
    }
    
    p.x = CGRectGetMaxX(parentBounds) - scaleBarWidth - marginToEdge;
    p.y = CGRectGetMaxY(parentBounds) - scaleBarHeight - marginToEdge;
    if ((fabs(p.x - p1.x) + fabs(p.y - p1.y)) <= self.tolerance) {
        position = kCZScaleBarPositionBottomRight;
    }

    return position;
}

- (void)calculatePreferLength {
    CGSize parentSize = self.parent.frame.size;
    CGFloat scaleBarSizeInPixel = MAX(parentSize.width, parentSize.height) / 12;  // use 1/12 of the image as the scale bar
    
    switch ([self size]) {
        case CZElementSizeExtraSmall:
            scaleBarSizeInPixel *= 0.75f;
            break;
        case CZElementSizeSmall:
            break;
        case CZElementSizeMedium:
            scaleBarSizeInPixel *= 1.5f;
            break;
        case CZElementSizeLarge:
            scaleBarSizeInPixel *= 2.0f;
            break;
        case CZElementSizeExtraLarge:
            scaleBarSizeInPixel *= 3.0f;
            break;
        default:
            break;
    }
    
    if (![self.parent isValidScaling]) {
        scaleBarSizeInPixel = [CZElementScaleBar truncateValue:scaleBarSizeInPixel];
        self.length = floor(scaleBarSizeInPixel + 0.5f);
    } else {
        CGFloat scaling = [self scalingOfUnit:[CZElement unitStyle]];
        CGFloat scaleBarSizeInUnit = scaleBarSizeInPixel * scaling;
        scaleBarSizeInUnit = [CZElementScaleBar truncateValue:scaleBarSizeInUnit];
        scaleBarSizeInPixel = (scaling == 0.0) ? scaleBarSizeInUnit : (scaleBarSizeInUnit / scaling);
        self.length = scaleBarSizeInPixel;
    }
}

- (void)calculatePercent:(CGPoint)point atBound:(CGRect) bound{
    if (!CGRectEqualToRect(bound, CGRectZero)) {
        CGSize frameSize = bound.size;
        if (frameSize.width > 0 && frameSize.height > 0) {
            self.percentX = point.x / frameSize.width;
            self.percentY = point.y /  frameSize.height;
            [self rollbackToInvalidIfIsNotNormalized];
        } else {
            [self toInvalidPercent];
        }
    } else {
        [self toInvalidPercent];
    }
}

- (void)rollbackToInvalidIfIsNotNormalized {
    if (self.percentX < 0 || self.percentX > 1 || self.percentY < 0 || self.percentY > 1) {
        [self toInvalidPercent];
    }
}

- (BOOL)verifyPercent {
    return self.percentX >=0 && self.percentX <= 1 && self.percentY >= 0 && self.percentY <= 1;
}

- (void)toInvalidPercent {
    self.percentX = kInvalidScaleBarPercent;
    self.percentY = kInvalidScaleBarPercent;
}

+ (CGVector)calculateOffsetVectorIfCrossBound:(CGRect)childBound
                                             :(CGRect)parentBound
                                             :(CGFloat)marginToEdge {
    if (childBound.size.width >= parentBound.size.width || childBound.size.height >= parentBound.size.height) {
        return CGVectorMake(0, 0);
    } else {
        CGFloat childEndX = childBound.origin.x + childBound.size.width;
        CGFloat childEndY = childBound.origin.y + childBound.size.height;
        CGFloat parentEndX = parentBound.origin.x + parentBound.size.width;
        CGFloat parentEndY = parentBound.origin.y + parentBound.size.height;
        CGFloat dx = [CZElementScaleBar calculateOffsetIfCrossBound:childEndX :parentEndX :marginToEdge];
        CGFloat dy = [CZElementScaleBar calculateOffsetIfCrossBound:childEndY :parentEndY :marginToEdge];
        return CGVectorMake(dx, dy);
    }
}

+ (CGFloat)calculateOffsetIfCrossBound:(CGFloat)childEnd
                                      :(CGFloat)parentEnd
                                      :(CGFloat)marginToEnd {
    if (childEnd >= parentEnd) {
        return childEnd - parentEnd + marginToEnd;
    } else {
        return 0;
    }
}

@end
