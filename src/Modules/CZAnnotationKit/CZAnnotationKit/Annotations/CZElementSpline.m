//
//  CZElementSpline.m
//  Hermes
//
//  Created by Jin Ralph on 13-12-16.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementSpline.h"
#import "CZElementSubclass.h"
#import "CZNodeGeom.h"
#import "CZNodeSpline.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"

#import "CZGeometryHelper.h"

#if (!CALCULATE_AERA_USE_CRACK_ALGO)
#import "CZElementCommon.h"
#import "CZNodeGeom.h"
#endif

enum {
    CZElementSplineFeaturePerimeter = 0,
    
    CZElementSplineFeatureCount
};

@interface CZElementSpline() {
    CGPoint _measurementTextPosition;
}

@end

@implementation CZElementSpline

+ (Class)nodeClass {
    return [CZNodeSpline class];
}

+ (NSUInteger)minimumPointsCount {
    return 2;
}

+ (BOOL)hasTickes {
    return YES;
}

#pragma mark - override CZElement

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }

    CZFeature *perimeterFeature = self.features[CZElementSplineFeaturePerimeter];
    NSString *perimeterString = [perimeterFeature toLocalizedStringWithPrecision:[self precision]];

    NSString *feastureString = perimeterString;
    
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:feastureString];
    _measurementTextPosition = [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

+ (NSUInteger)featureCount {
    return CZElementSplineFeatureCount;
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    
    CGFloat scaledPerimeter = 0;
    
    CZNodeSpline *splineNode = (CZNodeSpline *)self.primaryNode;
    const NSUInteger controlPointsCount = [splineNode pointsCount];
    if (controlPointsCount > 2) {
        const CGPoint *controlPoints = [splineNode pointArray];
        CFDataRef pointsData = ComputeBezierCurvePoints(controlPoints, controlPointsCount, false, true);
        if (pointsData) {
            NSUInteger pointsCount = CFDataGetLength(pointsData) / sizeof(CGPoint);
            const CGPoint *points = (const CGPoint *)CFDataGetBytePtr(pointsData);
            
            double perimeter = [CZElementMultiPoint calcLengthOfPoints:points pointsCount:pointsCount];
            
            CFRelease(pointsData);
            
            scaledPerimeter = perimeter * scaling;
        }
    } else if (controlPointsCount == 2) {
        const CGPoint *controlPoints = [splineNode pointArray];
        CGFloat dx = controlPoints[0].x - controlPoints[1].x;
        CGFloat dy = controlPoints[0].y - controlPoints[1].y;
        double perimeter = sqrt(dx * dx + dy * dy);
        scaledPerimeter = perimeter * scaling;
    }

    // calculate in theory
//    CGFloat scaledPerimeter = scaling * [self lengthOfSpline];
    
    [self removeAllFeatures];

    // perimeter
    CZFeature *diameterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypeDistance];
    [self appendFeature:diameterFeature];
    [diameterFeature release];
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CGPoint)measurementTextPosition {
    return _measurementTextPosition;
}

#pragma mark - Private methods

#if (!CALCULATE_AERA_USE_CRACK_ALGO)
- (double)lengthOfSpline {
    double totalLength;
    NSUInteger pointCount = [self pointsCount];
    
    if (pointCount <= 1) {
        totalLength = 0.0;
    } else if (pointCount == 2) {
        CGPoint p0 = [self pointAtIndex:0];
        CGPoint p1 = [self pointAtIndex:1];
        totalLength = sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y));
    } else {
        NSUInteger controlPointsCount = pointCount * 3 - 1;
        CGPoint *controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);
        ComputeOpenBezierControlPoints([self points], pointCount, controlPoints);
        totalLength = [CZElementCommon lengthOfCubicBezierSegments:controlPoints pointsCount:controlPointsCount];
        free(controlPoints);
    }

    return totalLength;
}
#endif // (!CALCULATE_AERA_USE_CRACK_ALGO)

@end
