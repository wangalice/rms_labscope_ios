//
//  CZElement.m
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementSubclass.h"
#import "CZNodeShape.h"
#import "CZNodeRectangle.h"
#import "CZNodeText.h"
#import "CZElementLayer.h"
#import "CZElementGroup.h"
#import "CZFeature.h"
#import "CZNodeGeom.h"
#import "CZColorGenerator.h"

#define kMinLineWidth 0.5f

NSString * const kCZMeasurementTextLayerSuffix = @".m";

NSString * const CZAnnotationErrorDomain = @"AnnotationError";

const float kCZUMtoMil = 1.0f / 25.4f;
const float kCZUMtoInch = kCZUMtoMil * 1e-3f;
const float kCZUMtoMM = 1e-3f;

static CZElementUnitStyle globalUnitStyle = CZElementUnitStyleAuto;
static const CGFloat kDefaultPadding = 10;

// friend method between CZElementGroup and CZElement
@interface CZElementGroup (CZElement)
- (void)removeElement:(CZElement *)element;
@end

// friend method between CZElementLayer and CZElement
@interface CZElementLayer (CZElement)
- (void)removeElement:(CZElement *)element;
@end

@interface CZElement () {
    bool _didAddObserver;
}

@property (nonatomic, assign, readwrite) CZElementLayer *parent;
@property (nonatomic, assign, readwrite) CZElementGroup *parentGroup;
@property (nonatomic, retain) NSMutableArray *mutableFeatures;
@end

@implementation CZElement

+ (void)setUnitStyle:(CZElementUnitStyle)unitStyle {
    globalUnitStyle = unitStyle;
}

+ (CZElementUnitStyle)unitStyle {
    return globalUnitStyle;
}

+ (CZElementUnitStyle)determineUnitStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit {
    switch (unitStyle) {
        case CZElementUnitStyleMetricAuto:
            unitStyle = prefersBigUnit ? CZElementUnitStyleMM : CZElementUnitStyleMicrometer;
            break;
        case CZElementUnitStyleImperialAuto:
            unitStyle = prefersBigUnit ? CZElementUnitStyleInch : CZElementUnitStyleMil;
            break;
        case CZElementUnitStyleAuto: {
            BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
            if (isMetric) {
                unitStyle = prefersBigUnit ? CZElementUnitStyleMM : CZElementUnitStyleMicrometer;
            } else {
                unitStyle = prefersBigUnit ? CZElementUnitStyleInch : CZElementUnitStyleMil;
            }
            break;
        }
        default:
            break;
    }
    return unitStyle;
}

+ (CGFloat)scaling:(CGFloat)scaling ofUnitStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit {
    unitStyle = [CZElement determineUnitStyle:unitStyle prefersBigUnit:prefersBigUnit];

    switch (unitStyle) {
        case CZElementUnitStyleMil:
            scaling *= kCZUMtoMil;
            break;
        case CZElementUnitStyleInch:
            scaling *= kCZUMtoInch;
            break;
        case CZElementUnitStyleMM:
            scaling *= kCZUMtoMM;
            break;
        case CZElementUnitStyleMicrometer:
        default:
            break;
    }
    return scaling;
}

+ (NSString *)distanceUnitStringByStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit {
    unitStyle = [CZElement determineUnitStyle:unitStyle prefersBigUnit:prefersBigUnit];
    
    NSString *unitString = nil;
    switch (unitStyle) {
        case CZElementUnitStyleMicrometer:
            unitString = kCZFeatureUnitUM;
            break;
        case CZElementUnitStyleMil:
            unitString = kCZFeatureUnitMil;
            break;
        case CZElementUnitStyleMM:
            unitString = kCZFeatureUnitMM;
            break;
        case CZElementUnitStyleInch:
            unitString = kCZFeatureUnitInch;
            break;
        default:
            @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                           reason:[NSString stringWithFormat:@"%lu is a invalid unit type for this function", (unsigned long)unitStyle]
                                         userInfo:nil];
            break;
    }
    return unitString;
}

+ (NSString *)areaUnitStringByStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit {
    unitStyle = [CZElement determineUnitStyle:unitStyle prefersBigUnit:prefersBigUnit];
    
    NSString *unitString = nil;
    switch (unitStyle) {
        case CZElementUnitStyleMicrometer:
            unitString = kCZFeatureUnitUM2;
            break;
        case CZElementUnitStyleMil:
            unitString = kCZFeatureUnitMil2;
            break;
        case CZElementUnitStyleMM:
            unitString = kCZFeatureUnitMM2;
            break;
        case CZElementUnitStyleInch:
            unitString = kCZFeatureUnitInch2;
            break;
        default:
            @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                           reason:[NSString stringWithFormat:@"%lu is a invalid unit type for this function", (unsigned long)unitStyle]
                                         userInfo:nil];

            break;
    }
    return unitString;
}

+ (NSUInteger)featureCount {
    return 0;
}

+ (NSUInteger)defaultPrecisionFromScaling:(CGFloat)scaling {
    NSUInteger precision;
    if (scaling <= 0.0001f) {
        precision = 5;
    } else if (scaling <= 0.001f) {
        precision = 4;
    } else if (scaling <= 0.01f) {
        precision = 3;
    } else if (scaling <= 0.1f) {
        precision = 2;
    } else {
        precision = 1;
    }
    return precision;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _parent = nil;
        _identifier = @"";
        _size = CZElementSizeSmall;
        
        [self addObserver:self forKeyPath:@"parent" options:0 context:NULL];
        _didAddObserver = true;
    }
    return self;
}

- (void)dealloc {
    if (_didAddObserver) {
        [self removeObserver:self forKeyPath:@"parent" context:NULL];
    }
    
    _parent = nil;
    [_mutableFeatures release];
    [_identifier release];
    [_auxLayerCreater release];
    [_measurementID release];
    
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElement *newElement = [[[self class] allocWithZone:zone] init];
// following properties shall keep as default
//    newElement->_parent = nil;
//    newElement->_parentGroup = nil;
//    newElement->_identifier = nil;
//    newElement->_features = nil;
//    newElement->_measurementID = nil;
//    newElement->_featureValidated = NO;
    
    if (newElement) {
        newElement->_auxLayerCreater = [self->_auxLayerCreater retain];
        
        newElement->_keepAspect = self->_keepAspect;
        newElement->_measurementHidden = self->_measurementHidden;
        newElement->_measurementNameHidden = self->_measurementNameHidden;
        newElement->_size = self->_size;
        newElement->_measurementIDHidden = self->_measurementIDHidden;
        newElement->_measurementBackgroundColor = self->_measurementBackgroundColor;
    }
    return newElement;
}

- (void)setMeasurementHidden:(BOOL)measurementHidden {
    if (![self hasMeasurement]) {
        return;
    }
    
    if (_measurementHidden != measurementHidden) {
        _measurementHidden = measurementHidden;
        [self.parent rearrangeMeasurementIDForElement:self];
        
        if ([self.parent.delegate respondsToSelector:@selector(layerUpdateAllMeasurements:)]) {
            [self.parent.delegate layerUpdateAllMeasurements:self.parent];
        }
    }
}

- (CZColor)measurementIDColor {
    CZColor color = [[CZColorGenerator sharedInstance] colorFromColorMapAtIndex:([self.measurementID intValue] - 1)];
    return color;
}

- (BOOL)isDashLine {
    CZNode *primaryNode = [self primaryNode];
    return [primaryNode isKindOfClass:[CZNodeShape class]] ? [(CZNodeShape *)primaryNode isDash] : NO;
}

- (void)setDashLine:(BOOL)dashLine {
    CZNode *primaryNode = [self primaryNode];
    if ([primaryNode isKindOfClass:[CZNodeShape class]]) {
        [(CZNodeShape *)primaryNode setDash:dashLine];
    }
}

- (BOOL)canKeepAspect {
    return NO;
}

- (NSArray *)features {
    [self updateFeatures];
    return [NSArray arrayWithArray:self.mutableFeatures];
}

- (NSMutableArray *)mutableFeatures {
    if (_mutableFeatures == nil) {
        NSUInteger reserveCount = [[self class] featureCount];
        if (reserveCount > 0) {
            _mutableFeatures = [[NSMutableArray alloc] initWithCapacity:reserveCount];
        } else {
            _mutableFeatures = [[NSMutableArray alloc] init];
        }
    }
    return _mutableFeatures;
}

- (NSUInteger)featureCount {
    return _mutableFeatures.count;
}

- (void)updateFeatures {
}

- (void)removeFromParent {
    [_parent removeElement:self];
}

- (void)removeFromGroup {
    [_parentGroup removeElement:self];
}

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    return NO;
}

- (CGFloat)scaling {
    return self.parent.scaling;
}

- (CGFloat)logicalScaling {
    return self.parent == nil ? 1 : self.parent.logicalScaling;
}

- (BOOL)isValidScaling {
    return [self.parent isValidScaling];
}

- (CGFloat)scalingOfUnit:(CZElementUnitStyle)unitStyle {
    return [self.parent scalingOfUnit:unitStyle];
}

- (NSUInteger)precision {
    return [self.parent precision];
}

- (CZNode *)primaryNode {
    return nil;
}

- (CGPathRef)primaryNodePath {
    CZNode *primarayNode = [self primaryNode];
    if ([primarayNode isKindOfClass:[CZNodeShape class]]) {
        CZNodeShape *shape = (CZNodeShape *)primarayNode;
        if (shape.rotateAngle == 0) {
            return CGPathRetain([shape relativePath]);
        } else {
            CGPathRef relativePath = [shape relativePath];
            CGRect relativePathBB = CGPathGetBoundingBox(relativePath);
            CGPoint rotateCenter;
            rotateCenter.x = CGRectGetMidX(relativePathBB);
            rotateCenter.y = CGRectGetMidY(relativePathBB);
            
            CGAffineTransform rotateTransform = CGAffineTransformMakeRotationAtPoint(shape.rotateAngle, rotateCenter);
            CGPathRef resultPath = CGPathCreateCopyByTransformingPath(relativePath, &rotateTransform);
            return resultPath;
        }
    } else {
        return nil;
    }
}

- (CALayer *)newLayer {
    return nil;
}

- (BOOL)hasMeasurement {
    return NO;
}

- (CGFloat)measurementTextCornerRadius {
    CGFloat radius = 4;
    switch ([self size]) {
        case CZElementSizeExtraSmall:
            radius = 2;
            break;
        case CZElementSizeSmall:
            radius = 3;
            break;
        case CZElementSizeMedium:
            radius = 4;
            break;
        case CZElementSizeLarge:
            radius = 6;
            break;
        case CZElementSizeExtraLarge:
            radius = 8;
            break;
        default:
            break;
    }
    
    return radius;
}

- (CZNodeRectangle *)newMeasurementNodeWithString:(NSString *)string {
    CZNodeText *measurementText = [[CZNodeText alloc] init];
    measurementText.string = string;
    measurementText.fontSize = [self suggestFontSize];
    measurementText.strokeColor = self.strokeColor;
    [measurementText setSuggestSize];
    
    CGSize size = measurementText.boundary.size;
    CGFloat cornerRadius = [self measurementTextCornerRadius];
    size.width += cornerRadius * 2;
    size.height += cornerRadius * 2;
    measurementText.x = cornerRadius;
    measurementText.y = cornerRadius;
    measurementText.height += cornerRadius;
    
    CZNodeRectangle *background = nil;
    
    if (!self.measurementIDHidden) {
        CZNodeText *measurementIDText = [[CZNodeText alloc] init];
        measurementIDText.string = self.measurementID;
        measurementIDText.fontSize = [self suggestFontSize];
        
        CGFloat Y = 0.299 * self.measurementIDColor.r + 0.587 * self.measurementIDColor.g + 0.114 * self.measurementIDColor.b;
        measurementIDText.strokeColor = (Y > 0.5 * 255.0) ? kCZColorBlack : kCZColorWhite;
        [measurementIDText setSuggestSize];
        measurementIDText.x = cornerRadius;
        measurementIDText.y = cornerRadius;
        measurementIDText.height += cornerRadius;
        
        CGSize colorRectSize = measurementIDText.boundary.size;
        colorRectSize.width += cornerRadius * 2;
        colorRectSize.height += cornerRadius;
        
        CZNodeRectangle *colorRect = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(0, 0, colorRectSize.width, colorRectSize.height)];
        colorRect.fillColor = self.measurementIDColor;
        colorRect.cornerRadius = [self measurementTextCornerRadius];
        [colorRect addSubNode:measurementIDText];
        
        size.width += colorRectSize.width + kDefaultPadding - cornerRadius;
        measurementText.x = colorRectSize.width + kDefaultPadding;
        
        background = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(0, 0, size.width, size.height)];
        [background addSubNode:colorRect];
        [measurementIDText release];
        [colorRect release];
    } else {
        background = [[CZNodeRectangle alloc] initWithCGRect:CGRectMake(0, 0, size.width, size.height)];
    }
    
    background.fillColor = self.measurementBackgroundColor;
    background.cornerRadius = [self measurementTextCornerRadius];
    [background addSubNode:measurementText];
    [measurementText release];
    
    return background;
}

- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement {
    CGRect frame = [[self primaryNode] frame];
    return [self putMeasurement:measurement besideFrame:frame];
}

- (CGPoint)putMeasurement:(CZNodeRectangle *)measurement besideFrame:(CGRect)frame {
    NSMutableArray *positions = [NSMutableArray arrayWithCapacity:4];
    NSMutableArray *directions = [NSMutableArray arrayWithCapacity:4];
    
    CGPoint point;
    point.x = CGRectGetMinX(frame);
    point.y = CGRectGetMinY(frame);
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionBottom|kCZMeasurementDirectionLeft)];
    
    point.x = CGRectGetMaxX(frame);
    point.y = CGRectGetMinY(frame);
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionTop|kCZMeasurementDirectionLeft)];
    
    point.x = CGRectGetMaxX(frame);
    point.y = CGRectGetMaxY(frame);
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionTop|kCZMeasurementDirectionRight)];
    
    point.x = CGRectGetMinX(frame);
    point.y = CGRectGetMaxY(frame);
    [positions addObject:[NSValue valueWithCGPoint:point]];
    [directions addObject:@(kCZMeasurementDirectionBottom|kCZMeasurementDirectionRight)];
    
    return [self putMeasurement:measurement possiblePositions:positions directions:directions];
}

- (CGPoint)putMeasurement:(CZNodeRectangle *)measurement possiblePositions:(NSArray *)positions directions:(NSArray *)directions {
    NSAssert(positions.count == directions.count, @"Wrong input parameters, positions' count shall equal to directions' count");

    CGRect measurementRect = measurement.frame;
    CGPoint defaultPosition = measurementRect.origin;
    
    const CGRect boundary = self.parent.frame;
    const CGFloat offset = [self suggestLineWidth];
    
    BOOL foundPosition = NO;
    NSUInteger i = 0;
    for (NSValue *value in positions) {
        CGPoint position = [value CGPointValue];
        NSUInteger direction = [directions[i] unsignedIntegerValue];
        
        if (direction & kCZMeasurementDirectionLeft) {
            measurementRect.origin.x = position.x + offset;
        } else if (direction & kCZMeasurementDirectionRight) {
            measurementRect.origin.x = position.x - measurementRect.size.width - offset;
        } else if (direction & kCZMeasurementDirectionCenter) {
            measurementRect.origin.x = position.x - measurementRect.size.width / 2;
        }
        
        if (direction & kCZMeasurementDirectionTop) {
            measurementRect.origin.y = position.y + offset;
        } else if (direction & kCZMeasurementDirectionBottom) {
            measurementRect.origin.y = position.y - measurementRect.size.height - offset;
        } else if (direction & kCZMeasurementDirectionCenter) {
            measurementRect.origin.y = position.y - measurementRect.size.height / 2;
        }
        
        if (CGRectContainsRect(boundary, measurementRect)) {
            foundPosition = YES;
            break;
        }
        
        if (i == 0) {
            defaultPosition = measurementRect.origin;
        }
        ++i;
    }
    if (!foundPosition) {
        measurementRect.origin = defaultPosition;
    }
    
    measurement.x = measurementRect.origin.x;
    measurement.y = measurementRect.origin.y;
    
    return measurementRect.origin;
}

- (CALayer *)newMeasurementTextLayer {
    if ([self.auxLayerCreater respondsToSelector:@selector(newMeasurementTextLayerOfElement:)]) {
        return [self.auxLayerCreater newMeasurementTextLayerOfElement:self];
    } else {
        return nil;
    }
}

- (BOOL)hitTest:(CGPoint)p tolerance:(CGFloat)tolerance {
    return [self.primaryNode hitTest:p tolerance:tolerance];
}

- (BOOL)containsPoint:(CGPoint)p {
    return [self.primaryNode containsPoint:p];
}

- (NSString *)type {
    NSString *suffix = @"ANNOTATION_TYPE_";
    NSString *appendix = NSStringFromClass([self class]);
    appendix = [appendix uppercaseString];
    return [suffix stringByAppendingString:appendix];
}

- (NSString *)unitNameOfDistanceByStyle:(CZElementUnitStyle)unitStyle {
    return [self.parent unitNameOfDistanceByStyle:unitStyle];
}

- (NSString *)unitNameOfAreaByStyle:(CZElementUnitStyle)unitStyle {
    return [self.parent unitNameOfAreaByStyle:unitStyle];
}

- (CGPoint)anchorPoint {
    return [self.primaryNode anchorPoint];
}

/*! get the outline boudary of element*/
- (CGRect)boundary {
    return [self.primaryNode frame];
}

- (CGFloat)rotateAngle {
    return self.primaryNode.rotateAngle;
}

- (CZColor)strokeColor {
    return self.primaryNode.strokeColor;
}

- (void)setStrokeColor:(CZColor)strokeColor {
    self.primaryNode.strokeColor = strokeColor;
}

- (CZColor)fillColor {
    BOOL fillable = NO;
    if ([self.primaryNode conformsToProtocol:@protocol(CZNodeShapeFillable)]) {
        fillable = YES;
    }
    
    return fillable ? self.primaryNode.fillColor : kCZColorTransparent;
}

- (void)setFillColor:(CZColor)fillColor {
    self.primaryNode.fillColor = fillColor;
}

- (void)applyOffset:(CGSize)offset {
    CGAffineTransform t = CGAffineTransformMakeTranslation(offset.width, offset.height);
    [self applyTransform:t];
}

- (void)applyTransform:(CGAffineTransform)transform {
}

- (void)notifyWillChange {
    if ([_parent.delegate respondsToSelector:@selector(layer:willChangeElement:)]) {
        [_parent.delegate layer:_parent willChangeElement:self];
    }
}

- (void)notifyDidChange {
    if ([_parent.delegate respondsToSelector:@selector(layer:didChangeElement:)]) {
        [_parent.delegate layer:_parent didChangeElement:self];
    }
}

- (NSDictionary *)newShapeMemo {
    return nil;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    return NO;
}

- (CGFloat)suggestFontSize {
    if (_parent) {
        return [_parent suggestFontSizeForSize:self.size];
    } else {
        return [CZElementLayer suggestFontSizeForSize:self.size];
    }
}

- (CGFloat)suggestLineWidth {
    CGFloat lineWidth;
    if (_parent) {
        lineWidth = [_parent suggestLineWidthForSize:self.size];
    } else {
        lineWidth = [CZElementLayer suggestLineWidthForSize:self.size];
    }
    
    lineWidth = MAX(kMinLineWidth, lineWidth);
    return lineWidth;
}

- (CGPoint)measurementTextPosition {
    return CGPointZero;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self && [keyPath isEqualToString:@"parent"]) {
        self.featureValidated = NO;  // invalid feature when parent changed.
    }
}

@end

@implementation CZElement (ForSubclassEyesOnly)

- (void)removeAllFeatures {
    [self.mutableFeatures removeAllObjects];
}

- (void)appendFeature:(CZFeature *)feature {
    [self.mutableFeatures addObject:feature];
}

@end
