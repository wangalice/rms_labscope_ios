//
//  CZElementScaleBar.h
//  Hermes
//
//  Created by Ralph Jin on 2/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"

/**
 Scale Bar Position

 - kCZScaleBarPositionFree: N/A or wrong
 - kCZScaleBarPositionTopLeft: Top-Left
 - kCZScaleBarPositionTopRight: Top-Right
 - kCZScaleBarPositionBottomLeft: Bottom-Left
 - kCZScaleBarPositionBottomRight: Bottom-Right
 - kCZScaleBarPositionHidden: Hidden
 - kCZScaleBarPositionLastPosition: LastPosition
 */
typedef NS_ENUM(NSInteger, CZScaleBarPosition) {
    kCZScaleBarPositionFree = -1,
    kCZScaleBarPositionTopLeft = 0,
    kCZScaleBarPositionTopRight,
    kCZScaleBarPositionBottomLeft,
    kCZScaleBarPositionBottomRight,
};

typedef NS_ENUM(NSInteger, CZScaleBarDisplayMode) {
    kCZScaleBarPositionHidden,
    kCZScaleBarPositionLastPosition
};


const extern CGFloat kDefaultScaleBarMargin;

@interface CZElementScaleBar : CZElement

/*! Truncate input value to 1.0eN, 2.0eN or 5.0eN. */
+ (CGFloat)truncateValue:(CGFloat)value;

/*! Left of the scale bar. */
@property (nonatomic, assign) CGPoint point;
@property (nonatomic, assign) CGFloat length;
@property (nonatomic, assign) CZScaleBarPosition position;

//Only use when need calucate
@property (nonatomic, assign, readonly) CGFloat tolerance;

@property (nonatomic, assign, readonly) CGFloat percentX;
@property (nonatomic, assign, readonly) CGFloat percentY;

@property (nonatomic, assign) CGFloat margin;

+ (BOOL)isValidPosition:(CZScaleBarPosition)position;

- (instancetype)initWithPoint:(CGPoint)point
                       length:(CGFloat)length;

- (instancetype)initWithPoint:(CGPoint)point
                       length:(CGFloat)length
                        bound:(CGRect)frameBound  NS_DESIGNATED_INITIALIZER;

- (void)updateLength;

/*! if failed to snap */
- (void)snapToDefaultPosition:(CGFloat)tolerance;

//Depent on user's tolerance setting
- (CZScaleBarPosition)calucationScaleBarPositionWithTolerance:(CGFloat)tolerance;

/*! Detect whether the scale bar frame overlapped with the rect. */
- (BOOL)isScaleBarOverlappedWithRect:(CGRect)rect;

@end
