//
//  CZFeature.m
//  Hermes
//
//  Created by Ralph Jin on 1/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFeature.h"
#import <CZToolbox/CZToolbox.h>

NSString* const kCZFeatureTypeArea = @"FEATURE_AREA";
NSString* const kCZFeatureTypeDistance = @"FEATURE_DISTANCE";
NSString* const kCZFeatureTypeCenterX = @"FEATURE_CENTER_X";
NSString* const kCZFeatureTypeCenterY = @"FEATURE_CENTER_Y";
NSString* const kCZFeatureTypeWidth = @"FEATURE_WIDTH";
NSString* const kCZFeatureTypeHeight = @"FEATURE_HEIGHT";
NSString* const kCZFeatureTypeAngle = @"FEATURE_ANGLE";
NSString* const kCZFeatureTypeDiameter = @"FEATURE_DIAMETER";
NSString* const kCZFeatureTypePerimeter = @"FEATURE_PERIMETER";
NSString* const kCZFeatureTypeNumber = @"FEATURE_NUMBER";

NSString* const kCZFeatureUnitPixel = @"UNIT_PIXELS";
NSString* const kCZFeatureUnitCount = @"UNIT_COUNT";
NSString* const kCZFeatureUnitMM = @"UNIT_MM";
NSString* const kCZFeatureUnitUM = @"UNIT_UM";
NSString* const kCZFeatureUnitMil = @"UNIT_MIL";
NSString* const kCZFeatureUnitInch = @"UNIT_INCH";
NSString* const kCZFeatureUnitMM2 = @"UNIT_MM^2";
NSString* const kCZFeatureUnitUM2 = @"UNIT_UM^2";
NSString* const kCZFeatureUnitMil2 = @"UNIT_MIL^2";
NSString* const kCZFeatureUnitInch2 = @"UNIT_INCH^2";
NSString* const kCZFeatureUnitDegree = @"UNIT_DEGREE";
NSString* const kCZFeatureUnitRadiam = @"UNIT_RADIAM";

static NSNumberFormatter *numberFormatter = nil;
static NSNumberFormatter *valueNumberFormatter = nil;
static NSMutableDictionary *localizedUnitTypeMap = nil;

@interface CZFeature()

@property (nonatomic, readwrite, copy) NSString *unit;

@property (nonatomic, readwrite, copy) NSString *type;

@property (nonatomic, readwrite) float value;

@end

@implementation CZFeature

+ (NSString *)localizedString:(NSString *)keyString {
    if (keyString == nil) {
        return nil;
    }
    
    if (localizedUnitTypeMap == nil) {
        localizedUnitTypeMap = [[NSMutableDictionary alloc] initWithCapacity:20];
    }
    
    NSString *localizedString = [localizedUnitTypeMap objectForKey:keyString];
    if (localizedString == nil) {
        localizedString = L(keyString);
        localizedUnitTypeMap[keyString] = localizedString;
    }
    return localizedString;
}

- (instancetype)init {
    return [self initWithValue:-1 unit:@"" type:@""];
}

- (instancetype)initWithValue:(float)value
                         unit:(NSString *)unit
                         type:(NSString *)type {
    self = [super init];
    if (self) {
        _value = value;
        _unit = [unit copy];
        _type = [type copy];
    }
    return self;
}

- (void)dealloc {
    [_unit release];
    [_type release];
    [super dealloc];
}

- (NSString *)toLocalizedStringWithPrecision:(NSUInteger)precision {
    return [self toLocalizedStringWithPrecision:precision showType:YES];
}

- (NSString *)toLocalizedStringWithPrecision:(NSUInteger)precision showType:(BOOL)showType {
    NSString *numberString = [self localizedValueStringWithPrecision:precision];
    
    if (showType && _type) {
        NSString *typeString = [CZFeature localizedString:_type];
        NSString *unitString = [CZFeature localizedString:_unit];
        return [NSString stringWithFormat:@"%@ %@ %@", typeString, numberString, unitString];
    } else {
        NSString *unitString = [CZFeature localizedString:_unit];
        return [NSString stringWithFormat:@"%@ %@", numberString, unitString];
    }
}

- (NSString *)localizedValueStringWithPrecision:(NSUInteger)precision {
    if (numberFormatter == nil) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:precision];
        [numberFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    } else {
        if (numberFormatter.maximumFractionDigits != precision) {
            [numberFormatter setMaximumFractionDigits:precision];
        }
    }
    NSString *numberString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:_value]];
    return numberString;
}

- (NSString *)valueStringWithPrecision:(NSUInteger)precision {
    if (valueNumberFormatter == nil) {
        valueNumberFormatter = [[NSNumberFormatter alloc] init];
        [valueNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [valueNumberFormatter setMaximumFractionDigits:precision];
        [valueNumberFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
        [valueNumberFormatter setDecimalSeparator:@"."];
        [valueNumberFormatter setUsesGroupingSeparator:NO];
    } else {
        if (valueNumberFormatter.maximumFractionDigits != precision) {
            [valueNumberFormatter setMaximumFractionDigits:precision];
        }
    }
    NSString *numberString = [valueNumberFormatter stringFromNumber:[NSNumber numberWithFloat:_value]];
    
    return numberString;
}

@end
