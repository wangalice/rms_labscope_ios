//
//  CZElementLayerRenderer.m
//  Hermes
//
//  Created by Ralph Jin on 4/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementLayerRenderer.h"
#import <QuartzCore/QuartzCore.h>
#import <CZToolbox/CZToolbox.h>

NSString * const kAnnotationLayerName = @"annotationLayer";

@implementation CZElementLayerRenderer

- (instancetype)initWithTargetView:(UIView *)targetView {
    self = [super init];
    if (self) {
        _targetView = targetView;
        _annotationLayerName = [kAnnotationLayerName copy];
    }
    return self;
}

- (void)dealloc {
    [_annotationLayerName release];
    [super dealloc];
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer updateElementMeasurementsLargerThanIndex:(NSUInteger)index {
    CALayer *annotationLayer = [self checkAndGetAnnotationLayer:layer];
    
    for (NSUInteger i = 0, j = 0; i < [layer elementCount]; i++) {
        CZElement *element = [layer elementAtIndex:i];
        if (element.hasMeasurement) {
            if (!element.measurementHidden) {
                if (i >= index) {
                    if (j  < [annotationLayer sublayers].count - 1) {
                        CALayer *sub = [annotationLayer sublayers][j + 1];
                        CALayer *measurementLayer = nil;
                        if ([sub.name isEqualToString:[element.identifier stringByAppendingString:kCZMeasurementTextLayerSuffix]]) {
                            measurementLayer = sub;
                            CALayer *newMeasurementLayer = [element newMeasurementTextLayer];
                            if (newMeasurementLayer) {
                                newMeasurementLayer.name = [element.identifier stringByAppendingString:kCZMeasurementTextLayerSuffix];
                                [annotationLayer replaceSublayer:measurementLayer with:newMeasurementLayer];
                                [newMeasurementLayer release];
                            } else {
                                CZLogv(@"Error!! Failed to create measurement CALayer!!");
                            }
                        }
                    }
                }
            }
            j = j + 2;
        } else {
            j++;
        }
    }
    
    [annotationLayer removeAllAnimations];
}

- (void)layer:(CZElementLayer *)layer didAddElement:(CZElement *)element {
    // calculate insert position
    uint32_t insertPosition = 0;
    for (CZElement *subElement in layer) {
        if (subElement == element) {
            break;
        }
        insertPosition++;
        
        if ([subElement hasMeasurement]) {
            insertPosition++;
        }
    }
    
    CALayer *annotationLayer = [self checkAndGetAnnotationLayer:layer];
    NSUInteger subLayerCount = annotationLayer.sublayers.count;
    if (insertPosition > subLayerCount) {
        if (subLayerCount > UINT32_MAX) {
            insertPosition = UINT32_MAX;
        } else {
            insertPosition = (uint32_t)annotationLayer.sublayers.count;
        }
    }
    
    // insert new layer
    CALayer *subLayer = [element newLayer];
    if (subLayer) {
        subLayer.name = element.identifier;
        [annotationLayer insertSublayer:subLayer atIndex:insertPosition];
        insertPosition++;
        [subLayer release];
    } else {
        CZLogv(@"failed to create layer by element: %@", element);
    }
    
    // insert new measurement layer
    if (element.hasMeasurement) {
        CALayer *measurementLayer = nil;
        if (element.measurementHidden) {
            measurementLayer = [[CAShapeLayer alloc] init];
        } else {
            measurementLayer = [element newMeasurementTextLayer];
        }
        
        if (measurementLayer) {
            measurementLayer.name = [element.identifier stringByAppendingString:kCZMeasurementTextLayerSuffix];
            measurementLayer.hidden = element.measurementHidden;
            [annotationLayer insertSublayer:measurementLayer atIndex:insertPosition];
            [measurementLayer release];
        } else {
            CZLogv(@"Error!! Failed to create measurement CALayer!!");
        }
    }
    
    if (annotationLayer.sublayers.count != 0) {
        annotationLayer.hidden = NO;
    }
    
    [annotationLayer removeAllAnimations];
}

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    CALayer *annotationLayer = [self checkAndGetAnnotationLayer:layer];
    
    BOOL foundLayer = NO;
    
    NSString *key = element.identifier;
    NSMutableArray *removeArray = [[NSMutableArray alloc] init];
    for (CALayer *sub in [annotationLayer sublayers]) {
        if (foundLayer) {
            if ([sub.name isEqualToString:key]) {
                [removeArray addObject:sub];
            }
            break;
        } else {
            if ([sub.name isEqualToString:key]) {
                [removeArray addObject:sub];
                foundLayer = YES;
                if (element.hasMeasurement) {
                    // append key with suffix and try to find measurement text layer next time
                    key = [key stringByAppendingString:kCZMeasurementTextLayerSuffix];
                } else {
                    break;
                }
            }
        }
    }
    
    for (CALayer *sub in removeArray) {
        [sub removeFromSuperlayer];
    }
    [removeArray release];
    
    if (annotationLayer.sublayers.count == 0) {
        annotationLayer.hidden = YES;
    }
    
    [annotationLayer removeAllAnimations];
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {
    BOOL foundLayer = NO;
    NSString *key = element.identifier;
    
    CALayer *annotationLayer = [self checkAndGetAnnotationLayer:layer];
    for (CALayer *sub in [annotationLayer sublayers]) {
        if (foundLayer) {
            if ([sub.name isEqualToString:key]) {  // looking for measurement CALayer
                if (element.measurementHidden) {
                    sub.hidden = YES;
                } else {
                    CALayer *newLayer = [element newMeasurementTextLayer];
                    newLayer.name = sub.name;
                    [annotationLayer replaceSublayer:sub with:newLayer];
                    [newLayer release];
                }
            } else {
                CZLogv(@"Error!! Failed to find measurement layer!");
            }
            break;
        } else {
            if ([sub.name isEqualToString:key]) {
                foundLayer = YES;
                
                CALayer *newLayer = [element newLayer];
                newLayer.name = sub.name;
                [newLayer removeAllAnimations];
                [annotationLayer replaceSublayer:sub with:newLayer];
                [newLayer release];
                
                if (element.hasMeasurement) {
                    // append key with suffix and try to find measurement text layer next time
                    key = [element.identifier stringByAppendingString:kCZMeasurementTextLayerSuffix];
                } else {
                    break;
                }
            }
        }
    }
    
    [annotationLayer removeAllAnimations];
}

- (void)hideElementOnly:(CZElement *)element inElementLayer:(CZElementLayer *)elementLayer {
    CALayer *annotationLayer = [self checkAndGetAnnotationLayer:elementLayer];
    NSArray *subLayers = [annotationLayer sublayers];
    
    for (NSUInteger i = 0, j = 0; i < [elementLayer elementCount] && j < [subLayers count]; i++, j++) {
        CZElement *subElement = [elementLayer elementAtIndex:i];
        BOOL hidden = subElement == element;
        
        CALayer *layer = (CALayer *)subLayers[j];
        layer.hidden = hidden;
        
        if (subElement.hasMeasurement) {
            j++;
            
            if (j >= [subLayers count]) {
                CZLogv(@"Error!! CALayer count is less than needed!");  // Unknow Error
                break;
            }
            
            BOOL hiddenMeasurement = hidden ? YES : subElement.measurementHidden;
            layer = (CALayer *)subLayers[j];
            layer.hidden = hiddenMeasurement;
        }
    }
    
    [annotationLayer removeAllAnimations];
}

- (void)layerUpdateAllMeasurements:(CZElementLayer *)elementLayer { //TODO:optimize
    [self updateAllElements:elementLayer];
}

- (void)updateAllElements:(CZElementLayer *)elementLayer {
    CALayer *annotationLayer = [self annotationLayer];
    
    CALayer *newLayer = [elementLayer newLayer];
    CGRect rect = elementLayer.frame;
    newLayer.frame = rect;
    rect.origin = CGPointZero;
    newLayer.bounds = rect;
    if (newLayer.sublayers.count == 0) {
        newLayer.hidden = YES;
    }
    
    CGFloat scale = 1.0f / [[UIScreen mainScreen] scale];
    newLayer.transform = CATransform3DMakeScale(scale, scale, 1.0f);
    newLayer.name = _annotationLayerName;

    CALayer *superLayer = [_targetView layer];
    if (annotationLayer) {
        [superLayer replaceSublayer:annotationLayer with:newLayer];
    } else {
        [superLayer addSublayer:newLayer];
    }
    
    [newLayer release];
}

- (CALayer *)annotationLayer {
    CALayer *superLayer = [_targetView layer];
    CALayer *annotationLayer = nil;
    for (CALayer *sub in [superLayer sublayers]) {
        if ([sub.name isEqualToString:_annotationLayerName]) {
            annotationLayer = sub;
            break;
        }
    }
    
    return annotationLayer;
}

#pragma mark - Private Methods

/** Lazy load algorithm of creating annotation root CALayer. */
- (CALayer *)checkAndGetAnnotationLayer:(CZElementLayer *)elementLayer {
    CALayer *superLayer = [_targetView layer];
    CALayer *annotationLayer = nil;
    for (CALayer *sub in [superLayer sublayers]) {
        if ([sub.name isEqualToString:_annotationLayerName]) {
            annotationLayer = sub;
            break;
        }
    }

    if (annotationLayer == nil) {
        CALayer *renderLayer = [[CALayer alloc] init];
        
        renderLayer.position = CGPointZero;
        renderLayer.anchorPoint = CGPointZero;
        
        CGRect rect = elementLayer.frame;
        rect.origin = CGPointZero;
        renderLayer.frame = rect;
        renderLayer.bounds = rect;
        renderLayer.contentsRect = rect;
        
        CGFloat scale = 1.0f / [[UIScreen mainScreen] scale];
        renderLayer.transform = CATransform3DMakeScale(scale, scale, 1.0f);

        renderLayer.name = _annotationLayerName;
        [superLayer addSublayer:renderLayer];
        annotationLayer = renderLayer;
        [renderLayer release];
    }
    
    return annotationLayer;
}

@end
