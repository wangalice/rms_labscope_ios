//
//  CZElementCount.m
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementCount.h"
#import "CZElementSubclass.h"
#import "CZNodeMultiMarks.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"

@implementation CZElementCount

+ (Class)nodeClass {
    return [CZNodeMultiMarks class];
}

#pragma mark - override CZElement

- (CALayer *)newLayer {
    CZNodeMultiMarks *marksNode = (CZNodeMultiMarks *)self.primaryNode;
    marksNode.lineWidth = [self suggestLineWidth];
    marksNode.markSize = [self suggestMarkSize];
    CALayer *layer = [marksNode newLayer];
    return layer;
}

- (BOOL)isMeasurementHidden {
    return NO;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    CZFeature *areaFeature = self.features.count > 0 ? self.features[0] : nil;
    NSString *countString = nil;
    if (areaFeature.value >= 0.0f) {
        countString = [areaFeature toLocalizedStringWithPrecision:0];
    }
    
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:countString];
    [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

/** put the measurement text box on the first point. The psuedo code as follow,
 *    point := first point of count element
 *    markSize := mark size of count element
 *    boundary.left := point.x - markSize
 *    boundary.top := point.y - markSize
 *    boundary.width := 2 * markSize
 *    boundary.height := 2 * markSize
 *    call super class's method, put measurement beside boundary
 */
- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement {
    CGPoint firstPoint = [self pointAtIndex:0];
    CGFloat offset = [self suggestMarkSize] + [self suggestLineWidth];
    CGRect frame = CGRectMake(firstPoint.x - offset, firstPoint.y - offset, offset * 2, offset * 2);
    return [self putMeasurement:measurement besideFrame:frame];
}

- (BOOL)containsPoint:(CGPoint)p {
    BOOL containsPoint = [super containsPoint:p];
    if (!containsPoint) {
        CZFeature *areaFeature = self.features.count > 0 ? self.features[0] : nil;
        NSString *countString = nil;
        if (areaFeature.value >= 0.0f) {
            countString = [areaFeature toLocalizedStringWithPrecision:0];
        }
        
        CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:countString];
        [self putMeasurementAtDefaultPosition:measurementText];
        
        containsPoint = [measurementText containsPoint:p];
        [measurementText release];
    }
    
    return containsPoint;
}

+ (NSUInteger)featureCount {
    return 1;
}

- (CGPoint)measurementTextPosition {
    CGRect rect = [self boundary];
    CGFloat offset = [self suggestLineWidth];
    return CGPointMake(rect.origin.x - offset, rect.origin.y - offset);
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;
    
    CZNodeMultiMarks *marksNode = (CZNodeMultiMarks *)self.primaryNode;
    
    [self removeAllFeatures];
    
    CZFeature *feature = [[CZFeature alloc] initWithValue:[marksNode pointsCount] unit:kCZFeatureUnitCount type:kCZFeatureTypeNumber];
    [self appendFeature:feature];
    [feature release];
}

# pragma mark - Private methods

- (CGFloat)suggestMarkSize {
    CGFloat lineWidth = [self suggestLineWidth];
    return MAX(3, 3 * lineWidth);
}

@end
