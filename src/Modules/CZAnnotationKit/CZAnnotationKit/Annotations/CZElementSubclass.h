//
//  CZElementSubclass.h
//  Hermes
//
//  Created by Ralph Jin on 4/10/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"

@interface CZElement (ForSubclassEyesOnly)

- (void)removeAllFeatures;
- (void)appendFeature:(CZFeature *)feature;

//- (CZFeature *)featureOfType:(NSString *)type; {
//    for (...) {
//        feature.type == type:
//        return feature;
//    }
//}

@end