//
//  CZElementRectangle.m
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementRectangle.h"
#import "CZElementSubclass.h"
#import "CZNodeRectangle.h"
#import "CZFeature.h"
#import "CZNodeGeom.h"

#if CALCULATE_AERA_USE_CRACK_ALGO
#import "CZGemMeasurer.h"
#endif

@interface CZElementRectangle()

@property (nonatomic, retain) CZNodeRectangle *rectNode;

@end

@implementation CZElementRectangle

- (instancetype)init {
    return [self initWithRect:CGRectZero];
}

- (instancetype)initWithRect:(CGRect)rect {
    self = [super init];
    if (self) {
        _rectNode = [[CZNodeRectangle alloc] initWithCGRect:rect];
    }
    return self;
}

- (void)dealloc {
    [_rectNode release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZElementRectangle *newRectangle = [super copyWithZone:zone];
    if (newRectangle) {
        [newRectangle->_rectNode release];
        newRectangle->_rectNode = [self->_rectNode copyWithZone:zone];
    }
    return newRectangle;
}

- (BOOL)canKeepAspect {
    return YES;
}

- (CZNode *)primaryNode {
    return _rectNode;
}

- (CGRect)rect {
    CGRect r;
    r.origin.x = _rectNode.x;
    r.origin.y = _rectNode.y;
    r.size.width = _rectNode.width;
    r.size.height = _rectNode.height;
    return r;
}

- (void)setRect:(CGRect)rect {
    _rectNode.x = rect.origin.x;
    _rectNode.y = rect.origin.y;
    _rectNode.width = rect.size.width;
    _rectNode.height = rect.size.height;
    self.featureValidated = NO;
}

- (BOOL)isEqualShapeTo:(CZElement *)anElement {
    if (![anElement isMemberOfClass:self.class]) {
        return NO;
    }
    
    CZElementRectangle *aRect = (CZElementRectangle *)anElement;
    
    return CGRectEqualToRect([self rect], [aRect rect]);
}

- (CALayer *)newLayer {
    _rectNode.lineWidth = [self suggestLineWidth];
    CALayer *layer = [_rectNode newLayer];
    return layer;
}

- (BOOL)hasMeasurement {
    return YES;
}

- (CALayer *)newMeasurementTextLayer {
    [self updateFeatures];
    
    CALayer *layer = [super newMeasurementTextLayer];
    if (layer) {
        return layer;
    }
    
    CZFeature *areaFeature = [self.features objectAtIndex:CZElementRectangleFeatureArea];
    CZFeature *perimeterFeature = [self.features objectAtIndex:CZElementRectangleFeaturePerimeter];
    NSString *featureString = [NSString stringWithFormat:@"%@\n%@",
                               [areaFeature toLocalizedStringWithPrecision:[self precision]],
                               [perimeterFeature toLocalizedStringWithPrecision:[self precision]]];
    
    CZNodeRectangle *measurementText = [self newMeasurementNodeWithString:featureString];
    [self putMeasurementAtDefaultPosition:measurementText];
    
    layer = [measurementText newLayer];
    [measurementText release];
    return layer;
}

/** put the measurement text box besize rectangle only when rotate angle is not 0. The psuedo code as follow,
 *    boundaryNoRotate := rectangle element's boundary
 *    pointArray[0] := boundaryNoRotate's topLeft
 *    pointArray[1] := boundaryNoRotate's topRight
 *    pointArray[2] := boundaryNoRotate's bottomRight
 *    pointArray[3] := boundaryNoRotate's bottomLeft
 *    rotateMatrix := make rotate matrix with rectangle's center and rotate angle
 *    For i := 0 to 3 do
 *        pointArray[i] := pointArray[i] apply rotateMatrix
 *    end
 *
 *    // find the most top left point
 *    value := pointArray[0].x + pointArray[0].y
 *    topLeftIndex := 0
 *    For i := 1 to 3 do
 *       tempValue := pointArray[i].x + pointArray[i].y
 *       If tempValue < value then
 *           value := tempValue; topLeftIndex := i
 *    end
 *
 *    textBox's bottom right := pointArray[topleftIndex]
 *    cadidates[0] := textBox
 *    textBox's bottom left := pointArray[(topleftIndex + 1) mod 4]
 *    cadidates[1] := textBox
 *    textBox's top left := pointArray[(topleftIndex + 2) mod 4]
 *    cadidates[2] := textBox
 *    textBox's top right := pointArray[(topleftIndex + 3) mod 4]
 *    cadidates[3] := textBox
 *
 *    call super class's method, put measurement at one of possible positions in cadidates
 */
- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement {
    if (self.rotateAngle == 0) {
        return [super putMeasurementAtDefaultPosition:measurement];
    } else {
        CGRect rect = self.rect;

        CGPoint points[4];
        points[0] = CGPointZero;
        points[1].x = rect.size.width;
        points[1].y = 0;
        points[2].x = rect.size.width;
        points[2].y = rect.size.height;
        points[3].x = 0;
        points[3].y = rect.size.height;
        
        CGAffineTransform t = [self.primaryNode transformSelf];
        for (int i = 0; i < 4; i++) {
            points[i] = CGPointApplyAffineTransform(points[i], t);
        }
        
        int topLeftIndex = 0;
        for (int i = 1; i < 4; i++) {
            if (points[i].x + points[i].y < points[topLeftIndex].x + points[topLeftIndex].y) {
                topLeftIndex = i;
            }
        }
        
        NSMutableArray *positions = [NSMutableArray arrayWithCapacity:4];
        NSMutableArray *directions = [NSMutableArray arrayWithCapacity:4];
        
        NSUInteger i = topLeftIndex;
        [positions addObject:[NSValue valueWithCGPoint:points[i]]];
        [directions addObject:@(kCZMeasurementDirectionBottomRight)];
        
        if (++i == 4) {
            i = 0;
        }
        
        [positions addObject:[NSValue valueWithCGPoint:points[i]]];
        [directions addObject:@(kCZMeasurementDirectionBottomLeft)];
        
        if (++i == 4) {
            i = 0;
        }
        
        [positions addObject:[NSValue valueWithCGPoint:points[i]]];
        [directions addObject:@(kCZMeasurementDirectionTopLeft)];
        
        if (++i == 4) {
            i = 0;
        }
        [positions addObject:[NSValue valueWithCGPoint:points[i]]];
        [directions addObject:@(kCZMeasurementDirectionTopRight)];
        
        return [self putMeasurement:measurement possiblePositions:positions directions:directions];
    }
}

- (void)applyTransform:(CGAffineTransform)transform {
    [self notifyWillChange];
    
    CGRect rect = CGRectMake(_rectNode.x, _rectNode.y, _rectNode.width, _rectNode.height);
    rect = CGRectApplyAffineTransform(rect, transform);
    _rectNode.x = rect.origin.x;
    _rectNode.y = rect.origin.y;
    _rectNode.width = rect.size.width;
    _rectNode.height = rect.size.height;
    
    self.featureValidated = NO;
    
    [self notifyDidChange];
}

- (NSDictionary *)newShapeMemo {
    CGRect rect = self.rect;
    NSDictionary *value = @{@"x": @(rect.origin.x),
                            @"y": @(rect.origin.y),
                            @"w": @(rect.size.width),
                            @"h": @(rect.size.height)};
    
    NSNumber *rotateAngle = @(self.rotateAngle);
    NSDictionary *memo = [[NSDictionary alloc] initWithObjectsAndKeys:value, @"CGRect", rotateAngle, @"rotateAngle", nil];
    return memo;
}

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo {
    NSDictionary *data = [memo objectForKey:@"CGRect"];
    NSNumber *rotateAngle = [memo objectForKey:@"rotateAngle"];
    if ([data isKindOfClass:[NSDictionary class]] &&
        [rotateAngle isKindOfClass:[NSNumber class]]) {
        [self notifyWillChange];
        
        CGRect rect;
        rect.origin.x = [data[@"x"] floatValue];
        rect.origin.y = [data[@"y"] floatValue];
        rect.size.width = [data[@"w"] floatValue];
        rect.size.height = [data[@"h"] floatValue];
        self.rect = rect;
        
        self.rotateAngle = [rotateAngle floatValue];
        
        [self notifyDidChange];
        return YES;
    }
    
    return NO;
}

+ (NSUInteger)featureCount {
    return CZElementRectangleFeatureCount;
}

#pragma mark - CZElementRotatable

- (void)setRotateAngle:(CGFloat)rotateAngle {
    _rectNode.rotateAngle = rotateAngle;

#if CALCULATE_AERA_USE_CRACK_ALGO
    self.featureValidated = NO;
#endif
}

- (void)updateFeatures {
    if (self.featureValidated) {
        return;
    }
    self.featureValidated = YES;

    CZElementUnitStyle unitStyle = [CZElement unitStyle];
    CGFloat scaling = [self scalingOfUnit:unitStyle];
    NSString *unit1d = [self unitNameOfDistanceByStyle:unitStyle];
    NSString *unit2d = [self unitNameOfAreaByStyle:unitStyle];

#if CALCULATE_AERA_USE_CRACK_ALGO
    CZGemMeasure *measurer = [[CZGemMeasure alloc] init];
    CGRect rect = self.rect;
    
    CGPoint topLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint topRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint bottomLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    if (self.rotateAngle != 0) {
        CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGAffineTransform t = CGAffineTransformMakeRotationAtPoint(self.rotateAngle, center);
        
        topLeft = CGPointApplyAffineTransform(topLeft, t);
        topRight = CGPointApplyAffineTransform(topRight, t);
        bottomRight = CGPointApplyAffineTransform(bottomRight, t);
        bottomLeft = CGPointApplyAffineTransform(bottomLeft, t);
    }
    
    const CGFloat logicalScaling = self.logicalScaling;
    const CGFloat scalingOfLogicalPoint = scaling / logicalScaling;

    CGFloat points[10];
    points[0] = topLeft.x * logicalScaling;
    points[1] = topLeft.y * logicalScaling;
    
    points[2] = topRight.x * logicalScaling;
    points[3] = topRight.y * logicalScaling;
    
    points[4] = bottomRight.x * logicalScaling;
    points[5] = bottomRight.y * logicalScaling;
    
    points[6] = bottomLeft.x * logicalScaling;
    points[7] = bottomLeft.y * logicalScaling;
    
    points[8] = points[0];
    points[9] = points[1];
    
    [measurer setRegion:points pointsCount:5];
    CGFloat scaledArea = [measurer area] * scalingOfLogicalPoint * scalingOfLogicalPoint;
    CGFloat scaledPerimeter = [measurer perimeter] * scalingOfLogicalPoint;
    [measurer release];
#else
    CGFloat scaledWidth = scaling * _rectNode.width;
    CGFloat scaledHeight = scaling * _rectNode.height;
    CGFloat scaledArea = scaledWidth * scaledHeight;
    CGFloat scaledPerimeter = (scaledWidth + scaledHeight) * 2.0f;
#endif
    
    [self removeAllFeatures];
    
    // NOTICE: keep order as in CZElementRectangleFeature
    // area measurement
    CZFeature *areaFeature = [[CZFeature alloc] initWithValue:scaledArea unit:unit2d type:kCZFeatureTypeArea];
    [self appendFeature:areaFeature];
    [areaFeature release];

    // center x measuremnt
    CZFeature *centerXFeature = [[CZFeature alloc] initWithValue:scaling * CGRectGetMidX(self.rect)
                                                            unit:unit1d
                                                            type:kCZFeatureTypeCenterX];
    [self appendFeature:centerXFeature];
    [centerXFeature release];
    
    // center x measuremnt
    CZFeature *centerYFeature = [[CZFeature alloc] initWithValue:scaling * CGRectGetMidY(self.rect)
                                                           unit:unit1d
                                                           type:kCZFeatureTypeCenterY];
    [self appendFeature:centerYFeature];
    [centerYFeature release];

    // perimeter measuremnt
    CZFeature *perimeterFeature = [[CZFeature alloc] initWithValue:scaledPerimeter unit:unit1d type:kCZFeatureTypePerimeter];
    [self appendFeature:perimeterFeature];
    [perimeterFeature release];
}

#pragma mark - private methods

- (CGPoint)measurementTextPosition {
    return CGPointMake(self.rect.origin.x+10.0, self.rect.origin.y+10.0);
}

@end
