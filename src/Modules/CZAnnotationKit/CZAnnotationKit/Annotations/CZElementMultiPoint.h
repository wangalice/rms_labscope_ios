//
//  CZElementMultiPoint.h
//  Hermes
//
//  Created by Jin Ralph on 13-3-23.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"
#import "CZElementMultiPointKind.h"

/** Abstract class of multi-point shape element*/
@interface CZElementMultiPoint : CZElement<CZElementMultiPointKind>

// Append point[0] at last, if calculate closed polygon
+ (CGFloat)calcLengthOfPoints:(const CGPoint *)pointsData pointsCount:(NSUInteger)pointsCount;
+ (CGFloat)calcAreaOfPolygonPoints:(const CGPoint *)pointsData pointsCount:(NSUInteger)pointsCount;

+ (Class)nodeClass;
+ (BOOL)hasTickes;
+ (NSUInteger)minimumPointsCount;

- (instancetype)initWithPointsCount:(NSUInteger)count NS_DESIGNATED_INITIALIZER;

- (const CGPoint *)points;

@end
