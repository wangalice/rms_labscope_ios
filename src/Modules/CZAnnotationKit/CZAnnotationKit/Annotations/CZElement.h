//
//  CZElement.h
//  Hermes
//
//  Created by Ralph Jin on 1/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CZColor.h"

#define CALCULATE_AERA_USE_CRACK_ALGO 0

extern NSString * const kCZMeasurementTextLayerSuffix;

extern NSString * const CZAnnotationErrorDomain;

extern const float kCZUMtoMil;  // 1 / 25.4
extern const float kCZUMtoInch;  // 1 / (25.4 * 1000)
extern const float kCZUMtoMM;  // 1e-3

@class CZNode;
@class CZNodeRectangle;
@class CALayer;
@class CZElementLayer;
@class CZElementGroup;
@class CZFeature;
@class CZElement;

typedef NS_ENUM(NSUInteger, CZElementSize) {
    CZElementSizeExtraSmall,
    CZElementSizeSmall,
    CZElementSizeMedium,
    CZElementSizeLarge,
    CZElementSizeExtraLarge
};

typedef NS_ENUM(NSUInteger, CZElementUnitStyle) {
    CZElementUnitStyleMicrometer,
    CZElementUnitStyleMil,
    CZElementUnitStyleThou = CZElementUnitStyleMil,
    CZElementUnitStyleMM,
    CZElementUnitStyleInch,
    CZElementUnitStyleMetricAuto,
    CZElementUnitStyleImperialAuto,
    CZElementUnitStyleAuto
};

typedef NS_ENUM(NSUInteger, CZAnnotationErrorCode) {
    kCZAnnotationNoMeasurmentsError
};

/** arrow direction of measurement text. */
typedef NS_ENUM (NSUInteger, CZMeasurementDirection) {
    kCZMeasurementDirectionLeft = 1,
    kCZMeasurementDirectionRight = 2,
    kCZMeasurementDirectionTop = 4,
    kCZMeasurementDirectionBottom = 8,
    kCZMeasurementDirectionCenter = 16,
    kCZMeasurementDirectionTopLeft = kCZMeasurementDirectionTop | kCZMeasurementDirectionLeft,
    kCZMeasurementDirectionTopRight = kCZMeasurementDirectionTop | kCZMeasurementDirectionRight,
    kCZMeasurementDirectionBottomLeft = kCZMeasurementDirectionBottom | kCZMeasurementDirectionLeft,
    kCZMeasurementDirectionBottomRight = kCZMeasurementDirectionBottom | kCZMeasurementDirectionRight
};

/*! abstract class of graphic element
 
 subclass:
 CZElementLine,
 CZElementArrow,
 CZElementRectangle,
 CZElementText,
 CZElementScaleBar,
 ...
 */

@protocol CZElementLayerCreating <NSObject>
@optional
- (CALayer *)newMeasurementTextLayerOfElement:(CZElement *)element;

@end

@interface CZElement : NSObject <NSCopying>

+ (void)setUnitStyle:(CZElementUnitStyle)unitStyle;
+ (CZElementUnitStyle)unitStyle;
+ (CZElementUnitStyle)determineUnitStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit;
+ (CGFloat)scaling:(CGFloat)scaling ofUnitStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit;
+ (NSString *)distanceUnitStringByStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit;
+ (NSString *)areaUnitStringByStyle:(CZElementUnitStyle)unitStyle prefersBigUnit:(BOOL)prefersBigUnit;

/*!  identifier for this element.*/
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *measurementID;

/*!  parent layer of this element.*/
@property (nonatomic, assign, readonly) CZElementLayer *parent;
@property (nonatomic, assign, readonly) CZElementGroup *parentGroup;

@property (nonatomic, assign, readonly) CGFloat rotateAngle;

@property (nonatomic, assign) CZColor strokeColor;
@property (nonatomic, assign) CZColor fillColor;
@property (nonatomic, assign) CZColor measurementBackgroundColor;

@property (nonatomic, assign) CZElementSize size;

@property (nonatomic, assign, getter = isDashLine) BOOL dashLine;

@property (nonatomic, readonly) BOOL canKeepAspect;
@property (nonatomic, assign, getter = isKeepAspect) BOOL keepAspect;

@property (nonatomic, assign, getter = isMeasurementHidden) BOOL measurementHidden;
@property (nonatomic, assign, getter = isMeasurementNameHidden) BOOL measurementNameHidden;
@property (nonatomic, readonly) CZColor measurementIDColor;
@property (nonatomic, assign, getter = isMeasurementIDHidden) BOOL measurementIDHidden;

/* Is feature (measurement) valid or not. set NO to force update feature's value */
@property (nonatomic, assign, getter = isFeatureValidated) BOOL featureValidated;
@property (nonatomic, assign, readonly) NSArray *features;

@property (nonatomic, retain) id <CZElementLayerCreating> auxLayerCreater;

+ (NSUInteger)featureCount;
+ (NSUInteger)defaultPrecisionFromScaling:(CGFloat)scaling;

- (void)updateFeatures;

- (void)removeFromParent;
- (void)removeFromGroup;

/*! judge if shape is same, but don't compare color thickness or else. Sub class shall override it.*/
- (BOOL)isEqualShapeTo:(CZElement *)anElement;

/*! image scaling in um per pixel */
- (CGFloat)scaling;
/*! device point to logical point scaling. LP = DP * logicalScaling. */
- (CGFloat)logicalScaling;

- (BOOL)isValidScaling;

- (CGFloat)scalingOfUnit:(CZElementUnitStyle)unitStyle;

/*! get the propper precision for measurement value.*/
- (NSUInteger)precision;

/*! return the primary node of this element, sub-class shall provide it.*/
- (CZNode *)primaryNode;
- (CGPathRef)primaryNodePath CF_RETURNS_RETAINED;

/*!  create rendering layer for this element.*/
- (CALayer *)newLayer;

- (BOOL)hasMeasurement;

- (CGFloat)measurementTextCornerRadius;

- (CZNodeRectangle *)newMeasurementNodeWithString:(NSString *)string;

/** put the measurement text box beside the element, the default method is put
 * beside the boundary of element. The psuedo code as follow,
 *    procedure putMeasurementTextBesizeFrame
 *    boundary := the boundary of element
 *    textBox's bottom left := boundary's top left  // move textBox and let textBox's bottom left equals boundary's top left
 *    candidates[0] := textBox
 *    textBox's top left := boundary's top right
 *    candidates[1] := textBox
 *    textBox's top right := boundary's bottom right
 *    candidates[2] := textBox
 *    textBox's bottom right := boundary's bottom left
 *    candidates[3] := textBox
 *    // choose one position from candidates
 *    putMeasurementTextAtCandidatePositions(candidates);
 */
- (CGPoint)putMeasurementAtDefaultPosition:(CZNodeRectangle *)measurement;
- (CGPoint)putMeasurement:(CZNodeRectangle *)measurement besideFrame:(CGRect)frame;

/** put the measurement text box at one of the candidate positions. The psuedo code as follow,
 *    procedure putMeasurementTextAtPossiblePositions
 *    textBox := candidates[0]
 *    for candidateBox in candidates do
 *        If candidateBox is inside of imageBoundary then
 *            textBox := candidateBox;
 *            break;
 *    end
 *    put measurement text at textBox;
 */
- (CGPoint)putMeasurement:(CZNodeRectangle *)measurement possiblePositions:(NSArray *)positions directions:(NSArray *)directions;

/*!  create rendering layer for this element's measurement text.*/
- (CALayer *)newMeasurementTextLayer;

- (CGPoint)measurementTextPosition;

- (BOOL)hitTest:(CGPoint)p tolerance:(CGFloat)tolerance;

- (BOOL)containsPoint:(CGPoint)p;

/*! get the element's type, eg. circle, line or angle*/
- (NSString *)type;

/*! get 1 dimension unit name, based on the unit type.*/
- (NSString *)unitNameOfDistanceByStyle:(CZElementUnitStyle)unitStyle;
/*! get 2 dimension unit name, based on the unit type.*/
- (NSString *)unitNameOfAreaByStyle:(CZElementUnitStyle)unitStyle;

- (CGPoint)anchorPoint;

/*! get the outline boundary of element*/
- (CGRect)boundary;

- (void)applyOffset:(CGSize)offset;
- (void)applyTransform:(CGAffineTransform)transform;

- (void)notifyWillChange;

- (void)notifyDidChange;

- (NSDictionary *)newShapeMemo;

- (BOOL)restoreFromShapeMemo:(NSDictionary *)memo;

- (CGFloat)suggestFontSize;

- (CGFloat)suggestLineWidth;

@end
