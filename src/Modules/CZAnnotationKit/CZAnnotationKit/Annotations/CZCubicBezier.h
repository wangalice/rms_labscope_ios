//
//  CZCubicBezier.h
//  Matscope
//
//  Created by Ralph Jin, Paul Heckbert on 3/26/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#ifndef Matscope_CZCubicBezier_h
#define Matscope_CZCubicBezier_h

#include <CoreGraphics/CoreGraphics.h>

typedef struct _CubicBezier {
    CGPoint p0;
    CGPoint p1;
    CGPoint p2;
    CGPoint p3;
} CubicBezier;

/**
 * Find the intersections points of bezier |a| and |b|,
 * @param paramA (double[] length >= 9) output result of t value of cross point on bezier line |a|.
 * @param paramB (double[] length >= 9) output result of t value of cross point on bezier line |b|.
 * @return count of parameter, |paramA|'s count is same as |paramB|'s count.
 */
int CubicBezierFindIntersections(const CubicBezier *a, const CubicBezier *b, double *paramA, double *paramB);

/**
 * Find the intersections point of bezier |a|,
 * @param param (double[] length >= 2) output result of t value of cross point on bezier line |a|.
 * @return count of param, return 0 means no intersection point, return 2 means 1 intersection point.
 */
int CubicBezierFindSelfIntersections(const CubicBezier *a, double *param);

#endif
