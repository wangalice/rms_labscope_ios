//
//  CZElementDistance.h
//  Matscope
//
//  Created by Ralph Jin on 10/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElement.h"
#import "CZElementMultiPointKind.h"

//      p1
//     |
//     |
//     |
//     | othor
//     |------------ p3
//     |
//     | p2

@interface CZElementCalipers : CZElement <CZElementMultiPointKind>

@property (nonatomic) CGPoint p1;
@property (nonatomic) CGPoint p2;
@property (nonatomic) CGPoint p3;
@property (nonatomic, readonly) CGPoint othorPoint;

- (instancetype)init NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithPoint1:(CGPoint)p1
                        point2:(CGPoint)p2
                        point3:(CGPoint)p3 NS_DESIGNATED_INITIALIZER;

@end

typedef CZElementCalipers CZElementDistance; // Hermes treat distance annotation as calipers
