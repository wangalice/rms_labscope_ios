//
//  CZElementAngle.h
//  Hermes
//
//  Created by Ralph Jin on 3/22/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementMultiPoint.h"

//         / A
//        /
//       /
//      /
//     /
// P  -------- B

@interface CZElementAngle : CZElementMultiPoint

@property (nonatomic, assign) CGPoint ptA;
@property (nonatomic, assign) CGPoint ptB;
@property (nonatomic, assign) CGPoint ptP;

@end
