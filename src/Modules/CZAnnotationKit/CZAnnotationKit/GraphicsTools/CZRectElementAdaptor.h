//
//  CZRectElementAdaptor.h
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRectLikeObject.h"
#import "CZElementRectangle.h"

@interface CZRectElementAdaptor : NSObject<CZRectLikeObject>

- (id)initWithElement:(CZElementRectangle *)element;

@end
