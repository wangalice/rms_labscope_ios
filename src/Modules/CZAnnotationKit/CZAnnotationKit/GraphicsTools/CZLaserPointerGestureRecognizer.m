//
//  CZLaserPointerGestureRecognizer.m
//  Matscope
//
//  Created by Sherry Xu on 1/5/15.
//  Copyright (c) 2015 Carl Zeiss. All rights reserved.
//

#import "CZLaserPointerGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import <CZToolbox/CZToolbox.h>

static const CGFloat kPanThreshold = 10.0;

@interface CZLaserPointerGestureRecognizer ()

@property (nonatomic, assign) NSTimeInterval previousTimestamp;
@property (nonatomic, assign) CGPoint beginTouchPoint;

@end

@implementation CZLaserPointerGestureRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    NSUInteger touchCount = [touches count];
    if (touchCount > 1) {
        self.state = UIGestureRecognizerStateFailed;
    } else {
        UITouch *touch = [touches anyObject];
        self.beginTouchPoint = [touch locationInView:nil];
        self.previousTimestamp = event.timestamp;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateFailed ||
        self.state == UIGestureRecognizerStateChanged) {
        return;
    }
    if (self.state == UIGestureRecognizerStateBegan) {
        self.state = UIGestureRecognizerStateChanged;
    } else {
        NSUInteger touchCount = [touches count];
        if (touchCount == 1) {
            NSTimeInterval timeSincePrevious = event.timestamp - self.previousTimestamp;
            
            if ([self ifTouchesMovedFarEnough:touches] && (timeSincePrevious > 0.01)) {
                self.state = UIGestureRecognizerStateBegan;
            }
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.state == UIGestureRecognizerStateChanged || self.state == UIGestureRecognizerStateBegan) {
        self.state = UIGestureRecognizerStateEnded;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    self.state = UIGestureRecognizerStateFailed;
}

#pragma mark - Private mehtod

- (BOOL)ifTouchesMovedFarEnough:(NSSet *)touches {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:nil];
    if ((fabs(point.x - self.beginTouchPoint.x) + fabs(point.y - self.beginTouchPoint.y)) > kPanThreshold) {
        return YES;
    } else {
        return NO;
    }
}

@end
