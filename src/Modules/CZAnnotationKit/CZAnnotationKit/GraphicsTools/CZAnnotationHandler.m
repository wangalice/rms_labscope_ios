//
//  CZAnnotationHandler.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationHandler.h"
#import <QuartzCore/QuartzCore.h>
#import "CZElement.h"
#import "CZElementLayer.h"

NSString * const CZAnnotationHandlerLayerName = @"annotationHandlerLayer";

@implementation CZAnnotationHandler

- (id)init {
    self = [super init];
    if (self) {
        _zoomOfView = 0.0001;
    }
    return self;
}

- (void)dealloc {
    [_target release];
    [super dealloc];
}

- (CALayer *)representLayer {
    return nil;
}

- (void)updateWithZoom:(CGFloat)zoomOfView {
    if (zoomOfView == 0.0) {
        _zoomOfView = 0.0001;
    } else {
        _zoomOfView = zoomOfView;
    }
}

- (NSString *)hitTest:(CGPoint)p {
    if ([self isHidden]) {
        return @"";
    }

    CALayer *layer = [self representLayer];
    for (CAShapeLayer *sub in [[layer sublayers] reverseObjectEnumerator]) {
        if ([sub.name length] != 0 && [sub hitTest:p]) {
            return sub.name;  // The layer's name is equals to part const string. see also CZRectangleHandlerPart****.
        }
    }

    return @"";
}

- (CGPoint)cullPoint:(CGPoint)pointInImage {
    CGRect bounds = self.target.parent.frame;
    if (CGRectIsEmpty(bounds)) {
        return pointInImage;
    }
    
    if (pointInImage.x < 0.0f) {
        pointInImage.x = 0.0f;
    }
    
    if (pointInImage.y < 0.0f) {
        pointInImage.y = 0.0f;
    }
    
    if (pointInImage.x > bounds.size.width) {
        pointInImage.x = bounds.size.width;
    }
    
    if (pointInImage.y > bounds.size.height) {
        pointInImage.y = bounds.size.height;
    }
    return pointInImage;
}

- (void)setHidden:(BOOL)hidden {
    if (hidden == [self isHidden]) {
        return;
    }

    CALayer *layer = [self representLayer];
    if (layer) {
        if (hidden) {
            [layer setOpacity:0.0f];
            [layer setHidden:hidden];
        } else {
            [layer setHidden:hidden];
            [layer setOpacity:1.0f];
        }
    }
}

- (BOOL)isHidden {
    CALayer *layer = [self representLayer];
    if (layer) {
        return [layer isHidden];
    } else {
        return YES;
    }
}

- (CGPoint)positionOfPart:(CZAnnotationHandlerPart)part {
    return CGPointZero;
}

- (CGPoint)beginDragOnPart:(CZAnnotationHandlerPart)part atPoint:(CGPoint)pointInView {
    return CGPointMake(NAN, NAN);
}

- (CGPoint)dragOnPart:(CZAnnotationHandlerPart)part toPoint:(CGPoint)p {
    return CGPointMake(NAN, NAN);
}

- (CGPoint)endDragOnPart:(CZAnnotationHandlerPart)part atPoint:(CGPoint)pointInView {
    return CGPointMake(NAN, NAN);
}

@end
