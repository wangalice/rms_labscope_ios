//
//  CZAbstractGraphicsTool.h
//  Hermes
//
//  Created by Ralph Jin on 1/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CZElementDataSource.h"
#import <CZToolbox/CZToolbox.h>

#define kDefaultTolerance 28.0f

typedef id<CZUndoManaging, CZElementDataSource> CZElementDoc;

@class CZElement;

@protocol CZMagnifyDelegate <NSObject>

- (void)magnifyAtPoint:(CGPoint)point;
- (void)dismissMagnifierView;

@optional

- (void)magnifyViewAtPoint:(CGPoint)point point2:(CGPoint)point2;
- (void)temporaryHideElement:(CZElement *)element;

@end

typedef NS_ENUM(NSUInteger, CZShouldBeginTouchState) {
    CZShouldBeginTouchStateNo = 0,
    CZShouldBeginTouchStateYes,
    CZShouldBeginTouchStateMaybe
};

@interface CZAbstractGestureTool : NSObject

/*!
 Check if touch should begin at the point |p|, which may begin or not depend on following movement.
 @param p point of touch, the coordinate is depend, e.g. of view or of image .
 @return CZShouldBeginTouchStateMaybe, if tool may handle this touch begin event; CZShouldBeginTouchStateYes, if tool definitely will handle this touch, otherwise false.
 */
- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p;

/*!
 Touch begins at the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)touchBegin:(CGPoint)p;

/*!
 Touch is hold and moved to the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)touchMoved:(CGPoint)p;

/*!
 Two touches are hold and moved to the points |p1| & |p2|. The coordinate is depend.
 @return true, if tool has handled touch event; otherwise false.
 */
- (BOOL)twoTouchsMoved:(CGPoint)p1 p2:(CGPoint)p2;

/*!
 Touch ends at the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)touchEnd:(CGPoint)p;

/*!
 Touch cancelled at the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)touchCancelled:(CGPoint)p;

/*!
 Tap happens at the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)tap:(CGPoint)p;

/*!
 Double tap happens at the point |p|. The coordinate is depend.
 @return true, if tool want to handle this touch event; otherwise false.
 */
- (BOOL)doubleTap:(CGPoint)p;

@end

@interface CZAbstractGraphicsTool : CZAbstractGestureTool

// TODO: move to subclass
@property (nonatomic, assign, readonly) CZElementDoc docManager;

// TODO: move to subclass
- (id)initWithDocManager:(CZElementDoc)docManager;

/*! Cull point according to image boundary.*/
- (CGPoint)cullPoint:(CGPoint)p;

@end
