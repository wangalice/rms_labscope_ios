//
//  CZDeleteElementAction.m
//  Hermes
//
//  Created by Ralph Jin on 3/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDeleteElementAction.h"

#import "CZElementLayer.h"
#import "CZElementGroup.h"
#import "CZElement.h"

@interface CZDeleteElementAction () {
    NSUInteger _originalIndex;
}

@property (nonatomic, assign) CZElementLayer *layer;
@property (nonatomic, retain) CZElement *element;

@property (nonatomic, retain) CZElementGroup *elementGroup;
@property (nonatomic, assign) CZElement *lastElement;

@end

@implementation CZDeleteElementAction

- (id)initWithElement:(CZElement *)element {
    self = [super init];
    if (self) {
        _layer = element.parent;
        _element = [element retain];
        _elementGroup = [element.parentGroup retain];
        if ([_elementGroup elementCount] == 2) {
            if ([_elementGroup indexOfElement:element] == 0) {
                _lastElement = [_elementGroup elementAtIndex:1];
            } else {
                _lastElement = [_elementGroup elementAtIndex:0];
            }
        }
        _originalIndex = [_layer indexOfElement:_element];
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [_elementGroup release];
    _layer = nil;
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 0U;
}

- (void)do {
    [self redo];
}

- (void)undo {
    [self.layer insertElement:self.element atIndex:_originalIndex];
    if (self.elementGroup) {
        [self.elementGroup addElement:self.element];  // TODO: insert to original index
        if (self.lastElement) {
            [self.elementGroup addElement:self.lastElement];
            [self.layer addGroup:self.elementGroup];
        }
    }
}

- (void)redo {
    [self.element removeFromParent];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end
