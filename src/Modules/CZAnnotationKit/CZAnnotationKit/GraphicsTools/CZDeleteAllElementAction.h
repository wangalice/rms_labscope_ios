//
//  CZDeleteAllElementAction.h
//  Hermes
//
//  Created by Ralph Jin on 7/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

@class CZElementLayer;

@interface CZDeleteAllElementAction : NSObject<CZUndoAction>

@property (nonatomic, assign) CZElementLayer *layer;

@end
