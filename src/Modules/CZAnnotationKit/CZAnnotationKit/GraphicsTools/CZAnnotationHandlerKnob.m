//
//  CZAnnotationHandlerKnob.m
//  Hermes
//
//  Created by Ralph Jin on 2/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationHandlerKnob.h"
#import <QuartzCore/QuartzCore.h>
#import <CZToolbox/CZToolbox.h>

const CGFloat kCZAnnotationHandlerKnobSize = 44.0f;

@implementation CZAnnotationHandlerKnob

- (id)initWithParent:(CZAnnotationHandler *)parent {
    self = [super init];
    if (self) {
        _parent = parent;
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (CGFloat)zoomOfView {
    return self.parent.zoomOfView;
}

- (CALayer *)newKnobLayer {
    CALayer *knobLayer = [[CALayer alloc] init];
    NSString *partStr = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)self.part];
    knobLayer.name = partStr;
    [partStr release];

    UIImage *knobImage = [self knobImageByPart];
    knobLayer.contents = (id)knobImage.CGImage;
    return knobLayer;
}

- (CALayer *)newConnectLayer {
    CAShapeLayer *connectLayer = [[CAShapeLayer alloc] init];
    return connectLayer;
}

- (void)updateKnobLayer:(CALayer *)layer rotateAngle:(CGFloat)angle {
    const CGFloat frameSize = kCZAnnotationHandlerKnobSize / self.zoomOfView;
    const CGFloat halfFrameSize = frameSize * 0.5f;
    
    BOOL needRotate = _part == CZAnnotationHandlerPartResizeBottomRight ||
                    _part == CZAnnotationHandlerPartResizeTopLeft ||
                    _part == CZAnnotationHandlerPartResizeBottomLeft ||
                    _part == CZAnnotationHandlerPartResizeTopRight ||
                    _part == CZAnnotationHandlerPartRotate;
    
    if (needRotate) {  // It must reset to identity, then set the new bounds and frame
        layer.transform = CATransform3DIdentity;
    }
    
    layer.bounds = CGRectMake(0, 0, frameSize, frameSize);
    layer.frame = CGRectMake(_knobPoint.x - halfFrameSize, _knobPoint.y - halfFrameSize, frameSize, frameSize);
    
    if (needRotate) {
        switch(_part) {
            case CZAnnotationHandlerPartResizeBottomRight:
            case CZAnnotationHandlerPartResizeTopLeft:
                layer.transform = CATransform3DMakeRotation(M_PI_4 + angle, 0, 0, 1);
                break;
            case CZAnnotationHandlerPartResizeBottomLeft:
            case CZAnnotationHandlerPartResizeTopRight:
                layer.transform = CATransform3DMakeRotation(-M_PI_4 + angle, 0, 0, 1);
                break;
            default:
                layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);
                break;
        }
    }
    
    // update image
    NSString *partStr = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)self.part];
    if (![layer.name isEqualToString:partStr]) {
        layer.name = partStr;
        
        UIImage *knobImage = [self knobImageByPart];
        layer.contents = (id)knobImage.CGImage;
    }
    [partStr release];
    
    [layer removeAllAnimations];  // remove all animations after change path.
}

- (void)updateConnectLayer:(CAShapeLayer *)layer {
    if (CGPointEqualToPoint(_connectPoint, _knobPoint)) {
        layer.hidden = YES;
        return;
    } else {
        layer.hidden = NO;
    
        // dash pattern
        NSNumber *dotSize = @(1.0 / self.zoomOfView);
        NSArray *dashPattern = [[NSArray alloc] initWithObjects:dotSize, dotSize, nil];
        layer.lineDashPattern = dashPattern;
        [dashPattern release];
        
        layer.lineWidth = 1.0f / self.zoomOfView;
        
        // path
        CGMutablePathRef relativePath = CGPathCreateMutable();
        CGPathMoveToPoint(relativePath, NULL, _connectPoint.x, _connectPoint.y);
        CGPathAddLineToPoint(relativePath, NULL, _knobPoint.x, _knobPoint.y);
        
        CGRect relativePathBB = CGPathGetBoundingBox(relativePath);
        CGAffineTransform offsetAffine = CGAffineTransformMakeTranslation(-relativePathBB.origin.x, -relativePathBB.origin.y);
        CGPathRef pathToPlaceInLayer = CGPathCreateCopyByTransformingPath(relativePath, &offsetAffine);
        CGPathRelease(relativePath);
        
        layer.path = pathToPlaceInLayer;
        CGPathRelease(pathToPlaceInLayer);
        
        layer.frame = relativePathBB;
        
        CGRect bounds = relativePathBB;
        bounds.origin.x = 0;
        bounds.origin.y = 0;
        layer.bounds = bounds;
    }
    
    [layer removeAllAnimations];  // remove all animations after change path.
}

#pragma mark - Private methods

- (UIImage *)knobImageByPart {
    CZAnnotationHandlerPart mergedPartCode = self.part;
    if (self.part >= CZAnnotationHandlerPartRemove && self.part < CZAnnotationHandlerPartRemoveLast) {
        mergedPartCode = CZAnnotationHandlerPartRemove;
    } else {
        switch (self.part) {
            case CZAnnotationHandlerPartRotate:
                break;
            case CZAnnotationHandlerPartDone:
            case CZAnnotationHandlerPartExitEditing:
                mergedPartCode = CZAnnotationHandlerPartDone;
                break;
            case CZAnnotationHandlerPartResizeBottomLeft:
            case CZAnnotationHandlerPartResizeBottomRight:
            case CZAnnotationHandlerPartResizeTopLeft:
            case CZAnnotationHandlerPartResizeTopRight:
                mergedPartCode = CZAnnotationHandlerPartResizeTopLeft;
                break;
            case CZAnnotationHandlerPartEnterEditing:
                break;
            case CZAnnotationHandlerPartRemoveAll:
                break;
            default:
                mergedPartCode = CZAnnotationHandlerPartMove;
                break;
        }
    }

    return [[self class] knobImageByPart:mergedPartCode];
}

+ (UIImage *)knobImageByPart:(CZAnnotationHandlerPart)part {
    static NSMutableDictionary *imageCache = nil;
    if (imageCache == nil) {
        imageCache = [[NSMutableDictionary alloc] init];
    }
    
    UIImage *image = imageCache[@(part)];
    if (image) {
        return image;
    }
    
    switch (part) {
        case CZAnnotationHandlerPartRemove:
            image = [UIImage imageNamed:A(@"group-remove-icon")];
            break;
        case CZAnnotationHandlerPartRotate:
            image = [UIImage imageNamed:A(@"rotate-knob")];
            break;
        case CZAnnotationHandlerPartDone:
            image = [UIImage imageNamed:A(@"close-knob")];
            break;
        case CZAnnotationHandlerPartResizeTopLeft:
            image = [UIImage imageNamed:A(@"resize-knob")];
            break;
        case CZAnnotationHandlerPartEnterEditing:
            image = [UIImage imageNamed:A(@"edit-knob")];
            break;
        case CZAnnotationHandlerPartRemoveAll:
            image = [UIImage imageNamed:A(@"group-remove-icon")];
            break;
        case CZAnnotationHandlerPartMove:
            image = [UIImage imageNamed:A(@"move-knob")];
            break;
        default:
            NSAssert(NO, @"wrong part code");
            break;
    }
    
    imageCache[@(part)] = image;
    
    return image;
}

@end
