//
//  CZGroupTool.m
//  Matscope
//
//  Created by Ralph Jin on 1/10/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZGroupTool.h"
#import "CALayer+ReplaceWithSameName.h"
#import "CZAnnotationHandler.h"
#import "CZElementLayer.h"
#import "CZElementGroup.h"
#import "CZElementScaleBar.h"
#import <CZToolbox/CZToolbox.h>
#import "CZAnnotationHandlerKnob.h"
#import "CZGroupElementAction.h"
#import "CZRemoveGroupElementAction.h"
#import "CZNodeSpline.h"
#import "CZNodePolyline.h"

#define kGroupHandleColor CZColorMakeRGBA(0, 180, 180, 180)
static const NSUInteger kMaxTracePointCount = 300;

#pragma mark - Inner class CZGroupToolHandler

static const CGFloat kDefaultGroupTolerance = kDefaultTolerance;
typedef CZNodePolyline CZGroupToolHandlerTrace;

@interface CZGroupToolHandler : CZAnnotationHandler

@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, assign) BOOL showsTrace;

@property (nonatomic, retain) CZGroupToolHandlerTrace *trace;
@property (nonatomic, retain) CALayer *handlerLayer;
@property (nonatomic, assign) CZElementLayer *elementLayer;

- (BOOL)addTracePoint:(CGPoint)point;
- (CGPoint)lastTracePoint;
- (void)clearTrace;

- (void)updateTraceHandler;
- (void)updateGroupHandler;

@end

@implementation CZGroupToolHandler

- (void)dealloc {
    [_trace release];
    [_handlerLayer release];
    [super dealloc];
}

- (CALayer *)representLayer {
    return self.handlerLayer;
}

- (void)updateWithZoom:(CGFloat)zoomOfView {
    [super updateWithZoom:zoomOfView];
    [self updateGroupHandler];
    [self updateTraceHandler];
}

- (void)updateGroupHandler {
    if (self.hidden) {
        self.handlerLayer.hidden = YES;
        [self.handlerLayer removeAllAnimations];
        return;
    }
    
    self.trace.lineWidth = kDefaultGroupTolerance / self.zoomOfView;  // transform to image coordinate

    self.handlerLayer = nil;
    _handlerLayer = [[CALayer alloc] init];
    
    self.handlerLayer.name = CZAnnotationHandlerLayerName;
    self.handlerLayer.hidden = self.hidden;

    NSUInteger groupID = 0;
    
    for (CZElementGroup *group in [self.elementLayer groupEnumerator]) {
        if (group && [group elementCount] >= 2) {
            CZAnnotationHandlerKnob *knob = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
            
            CZNodeSpline *spline = [[CZNodeSpline alloc] initWithPointsCount:[group elementCount] + 1];
            spline.strokeColor = kGroupHandleColor;
            spline.lineWidth = 4 / self.zoomOfView;
            
            NSUInteger i = 1;
            for (CZElement *element in group) {
                CGPoint pt = element.anchorPoint;
                pt = [CZCommonUtils imagePointToViewPoint:pt];
                [spline setPoint:pt atIndex:i];
                ++i;
            }
            
            // calculate position of remove knob
            CGPoint pt1 = [spline pointAtIndex:1];
            CGPoint pt2 = [spline pointAtIndex:2];
            CGPoint diff = CGPointMake(pt2.x - pt1.x, pt2.y - pt1.y);
            CGFloat slope = hypot(diff.x, diff.y);
            CGFloat distance = kCZAnnotationHandlerKnobSize / self.zoomOfView;
            diff.x = distance * diff.x / slope;
            diff.y = distance * diff.y / slope;
            if (isnan(diff.x)) {
                diff.x = -distance;
            }
            if (isnan(diff.y)) {
                diff.y = 0;
            }
            
            CGPoint pt0;
            pt0.x = pt1.x - diff.x;
            pt0.y = pt1.y - diff.y;
            [spline setPoint:pt0 atIndex:0];
            
            CALayer *connectLineLayer = [spline newLayer];
            [self.handlerLayer addSublayer:connectLineLayer];
            [connectLineLayer release];
            
            for (i = 0; i < spline.pointsCount; ++i) {
                CGPoint pt = [spline pointAtIndex:i];
                knob.knobPoint = pt;
                knob.part = (i == 0) ? (CZAnnotationHandlerPartRemove + groupID) : CZAnnotationHandlerPartMove;
                
                CALayer *knobLayer = [knob newKnobLayer];
                [knob updateKnobLayer:knobLayer rotateAngle:0];
                
                [self.handlerLayer addSublayer:knobLayer];
                [knobLayer release];
            }
            
            [knob release];
            [spline release];
        }
        
        groupID++;
    }
    
    [self.handlerLayer removeAllAnimations];
}

- (void)updateTraceHandler {
    // update tracelayer
    CAShapeLayer *traceLayer = (CAShapeLayer *)[self.trace newLayer];
    traceLayer.lineCap = kCALineCapRound;
    traceLayer.lineJoin = kCALineJoinRound;
    traceLayer.name = @"groupTool.trace";
    
    [self.handlerLayer replaceSublayerWithSameNameLayer:traceLayer];
    [traceLayer release];
    
    [self.handlerLayer removeAllAnimations];
}

- (BOOL)addTracePoint:(CGPoint)point {
    if ([self.trace pointsCount] > kMaxTracePointCount) {
        return NO;
    }
    [self.trace appendPoint:point];
    return YES;
}

- (CGPoint)lastTracePoint {
    if ([self.trace pointsCount] == 0) {
        return CGPointZero;
    } else {
        return [self.trace pointAtIndex:[self.trace pointsCount] - 1];
    }
}

- (void)clearTrace {
    [self.trace removeAllPoints];
}

- (CZGroupToolHandlerTrace *)trace {
    if (_trace == nil) {
        _trace = [[CZGroupToolHandlerTrace alloc] init];
        _trace.identifier = CZAnnotationHandlerLayerName;
        _trace.strokeColor = kGroupHandleColor;
    }
    return _trace;
}

@end

#pragma mark - class CZGroupTool

@interface CZGroupTool ()
@property (nonatomic, retain, readwrite) CZGroupToolHandler *handler;
@property (nonatomic, assign) CZElementGroup *currentGroup;
@end

@implementation CZGroupTool

- (id)initWithDocManager:(CZElementDoc)docManager targetView:(UIView *)targetView {
    self = [super initWithDocManager:docManager];
    if (self) {
        _targetView = targetView;
    }
    return self;
}

- (void)dealloc {
    [_handler release];
    [super dealloc];
}

- (void)setZoomOfView:(CGFloat)zoomOfView {
    _zoomOfView = zoomOfView;
    [self.handler updateWithZoom:zoomOfView];
    
    [self updateGroupHandler];
    [self updateTraceHandler];
}

#pragma mark - override super class

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    return CZShouldBeginTouchStateMaybe;
}

- (BOOL)touchBegin:(CGPoint)pointOfView {
    [self endGroupingWithCancel:YES];
    
    [self.handler addTracePoint:pointOfView];
    [self groupElementAtTouchPoint:[CZCommonUtils viewPointToImagePoint:pointOfView]];
    
    self.handler.showsTrace = YES;
    [self updateTraceHandler];
    return YES;
}

- (BOOL)touchMoved:(CGPoint)pointOfView {
    if ([self.handler.trace pointsCount] == 0) {  // if touch is not begin yet.
        return NO;
    }
    
    CGPoint lastPoint = [self.handler lastTracePoint];
    if ([self.handler addTracePoint:pointOfView]) {
        lastPoint = [CZCommonUtils viewPointToImagePoint:lastPoint];
        CGPoint pointOfImage = [CZCommonUtils viewPointToImagePoint:pointOfView];
        
        CGPoint diff = CGPointMake(pointOfImage.x - lastPoint.x, pointOfImage.y - lastPoint.y);
        CGFloat slope = hypot(diff.x, diff.y);
        if (slope == 0.0f) {
            return YES;
        }
        
        // hit test along with the line |lastPoint -->pointOfImage|
        CGFloat remainDistance = slope;
        CGPoint currentPoint = lastPoint;
        const CGFloat step = kDefaultGroupTolerance / self.zoomOfView;
        while (remainDistance > 0.0) {
            if (remainDistance < step) {
                [self groupElementAtTouchPoint:pointOfImage];
            } else {
                currentPoint.x += step * diff.x / slope;
                currentPoint.y += step * diff.y / slope;
                [self groupElementAtTouchPoint:currentPoint];
            }
                 
            remainDistance -= step;
        }
    } else {
        [self endGrouping];
    }
    
    [self updateTraceHandler];
    return YES;
}

- (BOOL)touchEnd:(CGPoint)pointOfView {
    [self endGrouping];
    return YES;
}

- (BOOL)touchCancelled:(CGPoint)pointOfView {
    [self endGroupingWithCancel:YES];
    return YES;
}

- (BOOL)tap:(CGPoint)pointOfView {
    CZAnnotationHandlerPart partCode = [[self.handler hitTest:pointOfView] integerValue];
    if (partCode >= CZAnnotationHandlerPartRemove && partCode <= CZAnnotationHandlerPartRemoveLast) {
        NSUInteger groupID = partCode - CZAnnotationHandlerPartRemove;
        
        if (groupID < self.docManager.elementLayer.groupCount) {
            CZElementGroup *tempGroup = [self.docManager.elementLayer groupAtIndex:groupID];
            CZRemoveGroupElementAction *action = [[CZRemoveGroupElementAction alloc] initWithGroup:tempGroup];
            [self.docManager pushAction:action];
            [action release];
        }
    }
    
    return YES;
}

- (void)showHandler:(BOOL)show {
    self.handler.hidden = !show;
    [self updateGroupHandler];
}

#pragma mark - Lazy-load objects

- (CZGroupToolHandler *)handler {
    if (_handler == nil) {
        _handler = [[CZGroupToolHandler alloc] init];
        _handler.hidden = YES;
        _handler.elementLayer = self.docManager.elementLayer;
        
        CALayer *layer = _handler.representLayer;
        [self.targetView.layer replaceSublayerWithSameNameLayer:layer bringTop:YES];
    }
    return _handler;
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer didChangeGroup:(CZElementGroup *)group {
    [self updateGroupHandler];
}

- (void)layer:(CZElementLayer *)layer didRemoveGroup:(CZElementGroup *)group {
    [self updateGroupHandler];
}

#pragma mark - Private

- (void)endGrouping {
    [self endGroupingWithCancel:NO];
}

- (void)endGroupingWithCancel:(BOOL)cancel {
    if (self.currentGroup) {
        if (cancel || [self.currentGroup elementCount] < 2) {
            [self.currentGroup removeFromParentLayer];
        } else {
            CZGroupElementAction *action = [[CZGroupElementAction alloc] initWithGroup:self.currentGroup];
            [self.docManager pushAction:action];
            [action release];
        }
        
        self.currentGroup = nil;
    }
    
    [self.handler clearTrace];
    self.handler.showsTrace = NO;
    [self updateTraceHandler];
}

- (BOOL)groupElementAtTouchPoint:(CGPoint)pointOfImage {
    BOOL result = NO;
    CGFloat tolerance = kDefaultGroupTolerance / self.zoomOfView;
    for (CZElement *element in self.docManager.elementLayer) {
        if (element.parentGroup == nil &&
            ![element isKindOfClass:[CZElementScaleBar class]] &&
            [element hitTest:pointOfImage tolerance:tolerance]) {  // hit-test element has no group
            if (self.currentGroup == nil) {
                CZElementGroup *group = [[CZElementGroup alloc] init];
                self.currentGroup = group;
                [self.docManager.elementLayer addGroup:group];
                [group release];
            }
            
            [self.currentGroup addElement:element];
            result = YES;
        }
    }
    
    return result;
}

- (void)updateGroupHandler {
    [self.handler updateGroupHandler];
    [self.targetView.layer replaceSublayerWithSameNameLayer:self.handler.representLayer bringTop:YES];
}

- (void)updateTraceHandler {
    [self.handler updateTraceHandler];
    [self.targetView.layer replaceSublayerWithSameNameLayer:self.handler.representLayer bringTop:YES];
}

@end
