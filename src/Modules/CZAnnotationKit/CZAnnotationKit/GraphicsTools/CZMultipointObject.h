//
//  CZMultipointObject.h
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZMultipointHandler;

@protocol CZMultipointObject <NSObject>

@required
- (NSUInteger)pointsCount;

- (CGPoint)pointAtIndex:(NSUInteger)index;

/** @return actual set location*/
- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index;

@optional

- (BOOL)canRemovePointAtIndex:(NSUInteger)index;
- (void)removePointAtIndex:(NSUInteger)index;

- (void)handler:(CZMultipointHandler *)handle willUpdateConnectLineInLayer:(CAShapeLayer *)layer;

@end
