//
//  CZToggleMeasurementAction.m
//  Hermes
//
//  Created by Ralph Jin on 3/26/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZToggleMeasurementAction.h"

@interface CZToggleMeasurementAction()
@property (nonatomic, retain) CZElement *element;
@end

@implementation CZToggleMeasurementAction

- (id)initWithElement:(CZElement *)element {
    self = [super init];
    if (self) {
        self.element = element;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 0U;
}

- (void)do {
    [self toggle];
}

- (void)undo {
    [self toggle];
}

- (void)redo {
    [self toggle];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)toggle {
    BOOL hidden = !_element.measurementHidden;
    [_element notifyWillChange];
    _element.measurementHidden = hidden;
    [_element notifyDidChange];
}

@end