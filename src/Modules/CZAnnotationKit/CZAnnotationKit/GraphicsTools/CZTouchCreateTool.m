//
//  CZTouchCreateTool.m
//  Hermes
//
//  Created by Ralph Jin on 1/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZTouchCreateTool.h"
#import "CZElementLayer.h"
#import <CZToolbox/CZToolbox.h>
#import "CZElementRectangle.h"
#import "CZElementCircle.h"
#import "CZElementLine.h"
#import "CZElementArrow.h"
#import "CZElementAngle.h"
#import "CZElementDisconnectAngle.h"
#import "CZElementText.h"
#import "CZElementDistance.h"

#import "CZSelectTool.h"
#import "CZCreateElementAction.h"
#import "CZDefaultSettings+Annotations.h"

const static double kTan22_5 = M_SQRT2 - 1.0;  // tan(22.5(degree))

@interface CZTouchCreateTool ()

@property (nonatomic, assign) CZElementDoc docManager;

@end

@implementation CZTouchCreateTool

- (id)initWithDocManager:(CZElementDoc)docManager {
    self = [super init];
    if (self) {
        _docManager = docManager;
        if (docManager == nil) {
            [self release];
            self = nil;
        }
    }
    return self;
}

- (CZElement *)createElementInRect:(CGRect)rect horizontal:(BOOL)horizontal {
    if (self.elementClass == [CZElementText class]) {
        CGFloat fontSize = [self.docManager.elementLayer suggestFontSizeForSize:[[CZDefaultSettings sharedInstance] elementSize]];
        CGSize defaultTextSize = CGSizeMake(fontSize * 20, fontSize * 3);
        defaultTextSize.width = MIN(defaultTextSize.width, (rect.size.width * 2));
        defaultTextSize.height = MIN(defaultTextSize.height, rect.size.height);
        
        rect.origin.x = rect.origin.x + (rect.size.width - defaultTextSize.width) / 2;
        rect.origin.y = rect.origin.y + (rect.size.height - defaultTextSize.height) / 2;
        rect.size = defaultTextSize;
    }
    
    CZElement *element = [self newElementByClass:self.elementClass inRect:rect horizontal:horizontal ];
    if (element == nil) {
        return nil;
    }
    
    BOOL isMeasurementLabelsTransparant = CZColorEqualToColor([[CZDefaultSettings sharedInstance] backgroundColor], kCZColorTransparent);

    if (self.elementClass != [CZElementScaleBar class]) {
        element.strokeColor = [[CZDefaultSettings sharedInstance] color];
        
        if (self.elementClass == [CZElementText class]) {
            element.fillColor = [[CZDefaultSettings sharedInstance] backgroundColor];
        }
    } else {
        element.fillColor = isMeasurementLabelsTransparant ? kCZColorTransparent : [[CZDefaultSettings sharedInstance] backgroundColor];
        element.strokeColor = isMeasurementLabelsTransparant ? [[CZDefaultSettings sharedInstance] color] : kCZColorBlack;
    }
    
    element.size = [[CZDefaultSettings sharedInstance] elementSize];
    element.measurementHidden = ![[NSUserDefaults standardUserDefaults] boolForKey:kMeasurementEnabled];
    element.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];


    [self cascadeElement:element inRect:rect];
    
    if (self.elementClass == [CZElementText class]) {
        [self.docManager.elementLayer addElement:element];
    } else {
        if ([element isMemberOfClass:[CZElementScaleBar class]]) {
            CZElementScaleBar *scaleBar = (CZElementScaleBar *)element;
            [scaleBar setPosition:kCZScaleBarPositionBottomRight];
        }
        
        CZCreateElementAction *action = [[CZCreateElementAction alloc] init];
        action.element = element;
        action.layer = self.docManager.elementLayer;
        [self.docManager.elementLayer addElement:element];
        [self.docManager pushAction:action];
        [action release];
    }

    if ([_delegate respondsToSelector:@selector(touchCreateTool:didCreateElement:)]) {
        [_delegate touchCreateTool:self didCreateElement:element];
    }
    
    [element release];

    return element;
}

- (CZElement *)newElementByClass:(Class)elementClass inRect:(CGRect)rectInImage horizontal:(BOOL)horizontal {
    if (elementClass == [CZElementRectangle class]) {
        CGRect rect = rectInImage;
        if (rect.size.width > rect.size.height) {
            rect.origin.x += (rect.size.width - rect.size.height) * 0.5;
            rect.size.width = rect.size.height;
        } else {
            rect.origin.y += (rect.size.height - rect.size.width) * 0.5;
            rect.size.height = rect.size.width;
        }
        return [[CZElementRectangle alloc] initWithRect:rect];
    } else if (elementClass == [CZElementCircle class]) {
        CGPoint center;
        center.x = CGRectGetMidX(rectInImage);
        center.y = CGRectGetMidY(rectInImage);
        
        CGFloat radius = MIN(rectInImage.size.width, rectInImage.size.height);
        radius *= 0.5;
        
        return [[CZElementCircle alloc] initWithCenter:center radius:radius];
    } else if (elementClass == [CZElementLine class]) {
        CGPoint p1, p2;
        if (horizontal) {
            p1.y = p2.y = CGRectGetMidY(rectInImage);
            p1.x = CGRectGetMinX(rectInImage);
            p2.x = CGRectGetMaxX(rectInImage);
        } else {
            p1.x = p2.x = CGRectGetMidX(rectInImage);
            p1.y = CGRectGetMinY(rectInImage);
            p2.y = CGRectGetMaxY(rectInImage);
        }
        
        return [[CZElementLine alloc] initWithPoint:p1 anotherPoint:p2];
    } else if (elementClass == [CZElementArrow class]) {
        CGPoint center;
        center.x = CGRectGetMidX(rectInImage);
        center.y = CGRectGetMidY(rectInImage);
        
        CGFloat size = MIN(rectInImage.size.width, rectInImage.size.height);
        size *= 0.5;
        
        CGPoint p1, p2;
        p1.x = center.x - size;
        p1.y = center.y - size;
        p2.x = center.x + size;
        p2.y = center.y + size;
        
        return [[CZElementArrow alloc] initWithPoint:p1 anotherPoint:p2];
    } else if (elementClass == [CZElementAngle class]) {
        CGPoint centerLeft;
        centerLeft.x = CGRectGetMinX(rectInImage);
        centerLeft.y = CGRectGetMidY(rectInImage);
        CGFloat right = CGRectGetMaxX(rectInImage);
        
        double height = rectInImage.size.width * kTan22_5;
        if ((height + height) > rectInImage.size.height) {
            height = rectInImage.size.height * 0.5;
            right = centerLeft.x + height / kTan22_5;
        }
        
        CGPoint ptA, ptB;
        ptA.x = right;
        ptA.y = centerLeft.y - height;
        ptB.x = right;
        ptB.y = centerLeft.y + height;
        
        CZElementAngle *angle = [[CZElementAngle alloc] init];
        angle.ptA = ptA;
        angle.ptB = ptB;
        angle.ptP = centerLeft;
        return angle;
    }  else if (elementClass == [CZElementDisconnectedAngle class]) {
        CGFloat left = CGRectGetMinX(rectInImage);
        CGFloat centerY = CGRectGetMidY(rectInImage);
        CGFloat right = CGRectGetMaxX(rectInImage);
        
        double height = rectInImage.size.width * kTan22_5;
        if ((height + height) > rectInImage.size.height) {
            height = rectInImage.size.height * 0.5;
            right = left + height / kTan22_5;
        }
        
        CGFloat centerX = (left + right) * 0.5;
        
        CZElementDisconnectedAngle *angle = [[CZElementDisconnectedAngle alloc] init];
        angle.ptA = CGPointMake(right, centerY - height);
        angle.ptB = CGPointMake(centerX, centerY - height * 0.5);
        angle.ptC = CGPointMake(right, centerY + height);
        angle.ptD = CGPointMake(centerX, centerY + height * 0.5);
        return angle;
    } else if (elementClass == [CZElementText class]) {
        return [[CZElementText alloc] initWithFrame:rectInImage];
    } else if (elementClass == [CZElementScaleBar class]) {
        return [[CZElementScaleBar alloc] init];
    } else {
        return nil;
    }
}

#pragma mark - Private methods

- (void)cascadeElement:(CZElement *)element inRect:(CGRect)rect{
    CGFloat minWidthHeight = MIN(rect.size.width, rect.size.height);
    CGFloat step = floor(minWidthHeight / 10);
    step = MAX(1, step);
    step = MIN(50, step);
    CGSize offsetSize = CGSizeMake(step, step);
    NSUInteger maxOffsetCount = ceil(minWidthHeight / step);
    NSUInteger offsetCount = 0;

    BOOL conflicted;
    do {
        conflicted = NO;
        for (CZElement *anElement in self.docManager.elementLayer) {
            if ([element isEqualShapeTo:anElement]) {
                if (offsetCount >= maxOffsetCount) {  // Go to next row, if offset count reaches limit.
                    CGSize nextRowOffsetSize = CGSizeMake(step * (1.0 - maxOffsetCount), -step * maxOffsetCount);
                    [element applyOffset:nextRowOffsetSize];
                    offsetCount = 0;
                } else {
                    [element applyOffset:offsetSize];
                    offsetCount++;
                }
                
                conflicted = YES;
            }
        }
    } while (conflicted);
}

@end
