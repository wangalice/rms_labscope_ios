//
//  CZElementDataSource.h
//  Hermes
//
//  Created by Ralph Jin on 8/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CZElementLayer;

@protocol CZElementDataSource <NSObject>

@property (nonatomic, retain, readonly) CZElementLayer *elementLayer;

@end
