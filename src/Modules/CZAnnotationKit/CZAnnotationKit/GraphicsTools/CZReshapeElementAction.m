//
//  CZReshapeElementAction.m
//  Matscope
//
//  Created by Ralph Jin on 11/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZReshapeElementAction.h"
#import "CZElement.h"

@implementation CZReshapeElementAction

- (void)dealloc {
    [_element release];
    [_shapeMemo release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 128U;
}

- (void)do {
}

- (void)undo {
    [self swapWithMemo];
}

- (void)redo {
    [self swapWithMemo];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)swapWithMemo {
    NSDictionary *tempMemo = [_element newShapeMemo];
    [_element restoreFromShapeMemo:self.shapeMemo];
    self.shapeMemo = tempMemo;
    [tempMemo release];
}

@end

