//
//  CZTextElementAdaptor.h
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CZRectLikeObject.h"
#import "CZElementText.h"

@interface CZTextElementAdaptor : NSObject<CZRectLikeObject>

- (id)initWithElement:(CZElementText *)textElement;

@end
