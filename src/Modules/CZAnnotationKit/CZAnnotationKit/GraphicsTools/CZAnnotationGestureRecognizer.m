//
//  CZAnnotationGestureRecognizer.m
//  Matscope
//
//  Created by Ralph Jin on 11/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationGestureRecognizer.h"
#import <UIKit/UIGestureRecognizerSubclass.h>

static const CGFloat kPanThreshold = 10.0;

@interface CZAnnotationGestureRecognizer () {
@private
    BOOL _accepted;
    CGPoint _beginTouchPoint;
}

@property (nonatomic, retain) NSTimer *timer;

@end

@implementation CZAnnotationGestureRecognizer

- (void)dealloc {
    [_timer release];
    [super dealloc];
}

- (void)setAnnotationDelegate:(id<CZAnnotationGestureRecognizerDelegate>)annotationDelegate {
    self.delegate = annotationDelegate;
}

- (id<CZAnnotationGestureRecognizerDelegate>)annotationDelegate {
    if ([self.delegate conformsToProtocol:@protocol(CZAnnotationGestureRecognizerDelegate)]) {
        return (id<CZAnnotationGestureRecognizerDelegate>)self.delegate;
    } else {
        return nil;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    NSUInteger touchCount = [touches count];
    
    if (self.isMultiTouchEnable) {
        if (touchCount > 2) {
            self.state = UIGestureRecognizerStateFailed;
        } else if (touchCount == 1) {
            UITouch *touch  = [touches anyObject];
            
            CZShouldBeginTouchState state = CZShouldBeginTouchStateMaybe;
            if ([self.delegate respondsToSelector:@selector(annotationGetstureShouldBeginTouch:)]) {
                state = [self.annotationDelegate annotationGetstureShouldBeginTouch:touch];
            }
            if (state) {
                _accepted = YES;
                _beginTouchPoint = [touch locationInView:nil];
                self.state = (state == CZShouldBeginTouchStateYes) ? UIGestureRecognizerStateBegan : UIGestureRecognizerStatePossible;
            }
        } else if (touchCount == 2) {
            if (_accepted) {
                self.state = UIGestureRecognizerStateBegan;
            } else {
                UITouch *touch = [touches anyObject];
                CZShouldBeginTouchState state = CZShouldBeginTouchStateMaybe;
                if ([self.delegate respondsToSelector:@selector(annotationGetstureShouldBeginTouch:)]) {
                    state = [self.annotationDelegate annotationGetstureShouldBeginTouch:touch];
                }
                if (state) {
                    _accepted = YES;
                    _beginTouchPoint = [touch locationInView:nil];
                    self.state = (state == CZShouldBeginTouchStateYes) ? UIGestureRecognizerStateBegan : UIGestureRecognizerStatePossible;
                } else {
                    self.state = UIGestureRecognizerStateFailed;
                }
            }
        }
    } else {
        if (touchCount > 1) {
            self.state = UIGestureRecognizerStateFailed;
            return;
        }
        
        if (!_accepted) {
            if ([self.delegate respondsToSelector:@selector(annotationGetstureShouldBeginTouch:)]) {
                UITouch *touch = [touches anyObject];
                CZShouldBeginTouchState state = [self.annotationDelegate annotationGetstureShouldBeginTouch:touch];
                if (state) {
                    _accepted = YES;
                    _beginTouchPoint = [touch locationInView:nil];
                    
                    self.state = (state == CZShouldBeginTouchStateYes) ? UIGestureRecognizerStateBegan : UIGestureRecognizerStatePossible;
                    if (self.state == UIGestureRecognizerStatePossible) {
                        [self.timer invalidate];
                        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.35
                                                                      target:self
                                                                    selector:@selector(handleTimeOut:)
                                                                    userInfo:nil
                                                                     repeats:NO];
                    }
                } else {
                    self.state = UIGestureRecognizerStateFailed;
                }
            }
        }

    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateFailed) {
        return;
    }
    
    if (!self.isMultiTouchEnable) {
        NSUInteger touchCount = [touches count];
        if (touchCount > 1) {
            self.state = UIGestureRecognizerStateFailed;
            return;
        }
    }
    
    if (self.state == UIGestureRecognizerStateBegan) {
        self.state = UIGestureRecognizerStateChanged;
    } else {
        if (self.isMultiTouchEnable) {
            if (_accepted) {
                if ([self ifTouchesMovedFarEnough:touches]) {
                    self.state = UIGestureRecognizerStateBegan;
                }
            } else {
                // if 1 finger move too far away, then fail this gesture and let scroll view pan gesture take response.
                if ([self numberOfTouches] == 1) {
                    if ([self ifTouchesMovedFarEnough:touches]) {
                        self.state = UIGestureRecognizerStateFailed;
                    }
                }
            }
        } else {
            if (_accepted) {
                if ([self ifTouchesMovedFarEnough:touches]) {
                    self.state = UIGestureRecognizerStateBegan;
                }
            } else {
                self.state = UIGestureRecognizerStateFailed;
            }
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (self.state == UIGestureRecognizerStateChanged || self.state == UIGestureRecognizerStateBegan) {
        self.state = UIGestureRecognizerStateEnded;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    self.state = UIGestureRecognizerStateFailed;
}

- (void)reset {
    [super reset];
    _accepted = NO;
    _beginTouchPoint = CGPointZero;
    [self.timer invalidate];
    self.timer = nil;
    
    if (self.state == UIGestureRecognizerStatePossible) {
        [self setState:UIGestureRecognizerStateFailed];
    }
}

#pragma mark - Private mehtod

- (BOOL)ifTouchesMovedFarEnough:(NSSet *)touches {
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:nil];
    if ((fabs(point.x - _beginTouchPoint.x) + fabs(point.y - _beginTouchPoint.y)) > kPanThreshold) {
        return YES;
    } else {
        return NO;
    }
}

- (void)handleTimeOut:(NSTimer *)timer {
    if (timer == self.timer) {
        [timer invalidate];
        
        self.timer = nil;

        if (self.state == UIGestureRecognizerStatePossible) {
            self.state = UIGestureRecognizerStateBegan;
        }
    }
}

@end
