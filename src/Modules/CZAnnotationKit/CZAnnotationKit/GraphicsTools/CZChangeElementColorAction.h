//
//  CZChangeElementColorAction.h
//  Hermes
//
//  Created by Ralph Jin on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZColor.h"

@class CZElement;

@interface CZChangeElementColorAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element newColor:(CZColor)color;

@end

@interface CZChangeElementFillColorAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element newStrokeColor:(CZColor) strokeColor newFillColor:(CZColor)fillColor;

@end


@interface CZChangeMeasurementBackgroundAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element newMeasurementBackgroundColor:(CZColor)measurementBackgroundColor;

@end
