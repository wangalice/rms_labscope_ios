//
//  CZCreateElementAction.m
//  Hermes
//
//  Created by Ralph Jin on 3/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCreateElementAction.h"
#import "CZElement.h"
#import "CZElementLayer.h"

@implementation CZCreateElementAction

- (void)dealloc {
    [_element release];
    _layer = nil;
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 0U;
}

- (void)do {
}

- (void)undo {
    [_element removeFromParent];
}

- (void)redo {
    [_layer addElement:self.element];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end
