//
//  CZDragTool.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDragTool.h"
#import <QuartzCore/QuartzCore.h>
#import "CZElement.h"
#import "CZElementLayer.h"
#import "CZElementScaleBar.h"
#import <CZToolbox/CZToolbox.h>

static const CGFloat kDefaultSnapTolerance = 88;

static inline void MoveToNewAnchorPoint(CZElement *element, CGPoint point) {
    CGPoint currentPoint = [element anchorPoint];
    CGSize offset;
    offset.width = point.x - currentPoint.x;
    offset.height = point.y - currentPoint.y;
    [element applyOffset:offset];
}

@interface CZDragElementAction : NSObject <CZUndoAction> {
    CGPoint _newPosition;
}

@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CGPoint originPosition;

@end

@implementation CZDragElementAction

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 0U;
}

- (void)do {
    _newPosition = [_element anchorPoint];
}

- (void)undo {
    MoveToNewAnchorPoint(_element, _originPosition);
}

- (void)redo {
    MoveToNewAnchorPoint(_element, _newPosition);
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end

#pragma mark - class CZDragScaleBarAction

@interface CZDragScaleBarAction : CZDragElementAction {
    CZScaleBarPosition _newScaleBarPosition;
}

@property (nonatomic, assign, readonly) CZElementScaleBar *scaleBar;
@property (nonatomic, assign) CZScaleBarPosition originScaleBarPosition;

@end

@implementation CZDragScaleBarAction

- (CZElementScaleBar *)scaleBar {
    return (CZElementScaleBar *)[super element];
}

- (void)do {
    [super do];
    _newScaleBarPosition = [self.scaleBar position];
}

- (void)undo {
    [self.scaleBar setPosition:_originScaleBarPosition];
    [super undo];
}

- (void)redo {
    [self.scaleBar setPosition:_newScaleBarPosition];
    [super redo];
}

@end

#pragma mark - CZDragTool

@interface CZDragTool()

@property (nonatomic, assign) CGPoint originPosition;
@property (nonatomic, assign) CZScaleBarPosition originScaleBarPosition;
@property (nonatomic, assign) CGSize anchorOffset;
@property (nonatomic, retain, readwrite) CZElement *element;

@end

@implementation CZDragTool

- (id)initWithDocManager:(CZElementDoc)docManager {
    self = [super initWithDocManager:docManager];
    if (self) {
        _element = nil;
        _zoomOfImage = 0.0001;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (void)setZoomOfImage:(CGFloat)zoomOfImage {
    if (zoomOfImage == 0) {
        _zoomOfImage = 0.0001;
    } else {
        _zoomOfImage = zoomOfImage;
    }
}

#pragma mark - override CZAbstractGraphicsTool

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    CGFloat tolerance = kDefaultTolerance / self.zoomOfImage;
    CZElement *element = [self.docManager.elementLayer hitTest:p tolerance:tolerance];
    if (![self isElementEditable:element]) {
        element = nil;
    }
    
    return element != nil ? CZShouldBeginTouchStateMaybe : CZShouldBeginTouchStateNo;
}

- (BOOL)touchBegin:(CGPoint)p {
    CGFloat tolerance = kDefaultTolerance / self.zoomOfImage;
    CZElement *element = [self.docManager.elementLayer hitTest:p tolerance:tolerance];
    if (![self isElementEditable:element]) {
        self.element = nil;
    } else {
        self.element = element;
    }
    
    if (_element) {
        self.originPosition = [_element anchorPoint];
        if ([_element isKindOfClass:[CZElementScaleBar class]]) {
            self.originScaleBarPosition = ((CZElementScaleBar *)_element).position;
        }
        _anchorOffset.width = _originPosition.x - p.x;
        _anchorOffset.height = _originPosition.y - p.y;
    }
    
    return _element != nil;
}

- (BOOL)touchMoved:(CGPoint)p {
    CGPoint newAnchorPoint;
    newAnchorPoint.x = p.x + _anchorOffset.width;
    newAnchorPoint.y = p.y + _anchorOffset.height;
    
    if ([_element isKindOfClass:[CZElementScaleBar class]]) {
        // calculate touch offset
        CGPoint anchorPoint = [_element anchorPoint];
        CGSize touchOffset;
        touchOffset.width = newAnchorPoint.x - anchorPoint.x;
        touchOffset.height = newAnchorPoint.y - anchorPoint.y;
        
        // set new point and try to snap
        CZElementScaleBar *scaleBar = (CZElementScaleBar *)_element;
        [scaleBar notifyWillChange];
        CGPoint newPoint = [scaleBar point];
        newPoint.x += touchOffset.width;
        newPoint.y += touchOffset.height;
        [scaleBar setPoint:newPoint];
        [scaleBar snapToDefaultPosition:kDefaultSnapTolerance / self.zoomOfImage];
        [scaleBar notifyDidChange];
    } else {
        MoveToNewAnchorPoint(_element, newAnchorPoint);
    }

    return YES;
}

- (BOOL)touchEnd:(CGPoint)p {
    id<CZUndoAction> action = nil;
    if ([_element isKindOfClass:[CZElementScaleBar class]]) {
        CZDragScaleBarAction *dragAction = [[CZDragScaleBarAction alloc] init];
        dragAction.element = _element;
        dragAction.originPosition = self.originPosition;
        dragAction.originScaleBarPosition = self.originScaleBarPosition;
        action = dragAction;
    } else {
        CZDragElementAction *dragAction = [[CZDragElementAction alloc] init];
        dragAction.element = _element;
        dragAction.originPosition = self.originPosition;
        action = dragAction;
    }

    [self.docManager pushAction:action];

    [action release];
    
    self.element = nil;
    return YES;
}

- (BOOL)touchCancelled:(CGPoint)p {
    self.element = nil;
    return YES;
}

#pragma mark - Private methods

- (BOOL)isElementEditable:(CZElement *)element {
    if ([_delegate respondsToSelector:@selector(shouldDragTool:editingElement:)]) {
        return [_delegate shouldDragTool:self editingElement:element];
    } else {
        return YES;
    }
}

@end
