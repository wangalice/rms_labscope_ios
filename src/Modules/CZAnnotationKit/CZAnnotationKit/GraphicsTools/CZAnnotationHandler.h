//
//  CZAnnotationHandler.h
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const CZAnnotationHandlerLayerName;

#define kMultipointShapeMaxCount 16383

typedef NS_ENUM(NSUInteger, CZAnnotationHandlerPart) {
    CZAnnotationHandlerPartNone = 0,
    CZAnnotationHandlerPartResizeTopLeft,
    CZAnnotationHandlerPartResizeTopRight,
    CZAnnotationHandlerPartResizeBottomLeft,
    CZAnnotationHandlerPartResizeBottomRight,
    CZAnnotationHandlerPartResizeLeft,
    CZAnnotationHandlerPartResizeRight,
    CZAnnotationHandlerPartResizeTop,
    CZAnnotationHandlerPartResizeBottom,
    CZAnnotationHandlerPartRotate,
    
    CZAnnotationHandlerPartDone,
    CZAnnotationHandlerPartEnterEditing,
    CZAnnotationHandlerPartExitEditing,
    
    CZAnnotationHandlerPartRemoveAll,
    CZAnnotationHandlerPartRemove,
    CZAnnotationHandlerPartRemoveLast = CZAnnotationHandlerPartRemove + kMultipointShapeMaxCount,  // reserve 16384 for remove index

    CZAnnotationHandlerPartMove,
    CZAnnotationHandlerPartMoveLast = CZAnnotationHandlerPartMove + kMultipointShapeMaxCount,  // reserve 16384 for move index
};

#define kHandlerDefaultDistance 0.0f

@class CZElement;

/*! Abstract class of annotation handler, which manipulate the annotation element.
 Sub-class shall override some of its methods. */
@interface CZAnnotationHandler : NSObject

@property (nonatomic, retain) CZElement *target;
@property (nonatomic, readonly, assign) CGFloat zoomOfView;
/** if YES, show remove knob nearby. */
@property (nonatomic, assign, getter = isRemoveKnobShown) BOOL removeKnobShown;

/*! @return the represent layer for renderring in a UIView. */
- (CALayer *)representLayer; // sub class shall override it

/*! When zoom of view is changed, update the handler with the new zoom scale.
 * Sub-class must call super class, if override it.
 */
- (void)updateWithZoom:(CGFloat)zoomOfView;

/*! @return CZRectangleHandlerPart**** const string, or empty string if nothing hit. */
- (NSString *)hitTest:(CGPoint)pointInView;

/*! @return postion of part in view coordinate.*/
- (CGPoint)positionOfPart:(CZAnnotationHandlerPart)part;

/*! @return the actual dragged point. */
- (CGPoint)beginDragOnPart:(CZAnnotationHandlerPart)part atPoint:(CGPoint)pointInView;

/*! @return the actual dragged point. */
- (CGPoint)dragOnPart:(CZAnnotationHandlerPart)part toPoint:(CGPoint)pointInView;

/*! @return the actual dragged point. */
- (CGPoint)endDragOnPart:(CZAnnotationHandlerPart)part atPoint:(CGPoint)pointInView;

/*! Cull point according to image boundary.*/
- (CGPoint)cullPoint:(CGPoint)pointInImage;

- (void)setHidden:(BOOL)hidden;

- (BOOL)isHidden;

@end
