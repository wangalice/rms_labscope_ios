//
//  CALayer+ReplaceWithSameName.h
//  Matscope
//
//  Created by Ralph Jin on 1/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (ReplaceWithSameName)

/*! Find sub-layer with same name and replace it.
 * If not found, put at last.
 */
- (void)replaceSublayerWithSameNameLayer:(CALayer *)layer;
- (void)replaceSublayerWithSameNameLayer:(CALayer *)layer bringTop:(BOOL)bringTop;

- (void)removeSubLayerWithName:(NSString *)name;

@end
