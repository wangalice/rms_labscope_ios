//
//  CZSelectTool.h
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZAbstractGraphicsTool.h"
#import "CZDragTool.h"
#import "CZElementTextInputTool.h"
#import "CZTouchCreateTool.h"
#import "CZMultiTapCreateTool.h"
#import "CZElementLayer.h"

@class CZAnnotationHandler;
@class CZElement;
@class CZElementText;

@protocol CZSelectToolDelegate;

typedef NS_ENUM(NSUInteger, CZSelectToolCreationMode) {
    kCZSelectToolCreationModeNormal,
    kCZSelectToolCreationModePolygon,
    kCZSelectToolCreationModePolyline,
    kCZSelectToolCreationModeCount,
    kCZSelectToolCreationModeSplineContour,
    kCZSelectToolCreationModeSpline,
    kCZSelectToolCreationModeCalipers,
    kCZSelectToolCreationModeMultiCalipers
};

typedef NS_ENUM(NSUInteger, CZSelectToolEditingMode) {
    kCZSelectToolEditingModeNormal,
    kCZSelectToolEditingModePCB
};

/*!
 CZSelectTool regards point in view coordinate.
 */
@interface CZSelectTool : CZAbstractGraphicsTool<
    CZElementTextInputToolDelegate,
    CZTouchCreateDelegate,
    CZMultiTapCreateToolDelegate,
    CZElementLayerDelegate>

+ (BOOL)isMultitapCreationMode:(CZSelectToolCreationMode)creationMode;

@property (nonatomic, assign, readonly) UIView *view;
@property (nonatomic, assign) CGFloat zoomOfView;
@property (nonatomic, assign) CZSelectToolCreationMode creationMode;
@property (nonatomic, assign) id<CZSelectToolDelegate> delegate;

@property (nonatomic, assign) CZSelectToolEditingMode editingMode;

/** Show remove knob near by element, so that user can remove the element. */
@property (nonatomic, assign, getter = isRemoveKnobShown) BOOL removeKnobShown;

- (id)initWithDocManager:(CZElementDoc)docManager view:(UIView *)view;

- (void)selectElement:(CZElement *)element;

- (CZElement *)selectedElement;

- (CZElement *)temporarilyHiddenElement;

/*! Deselect what was selected, and if it's in a typing mode, it'll quit the typing mode.*/
- (void)deselect;

- (void)deleteSelected;

- (BOOL)canCancelJob;

/** Force cancel job*/
- (void)cancelJob;

- (void)scrollElementToVisible:(CZElement *)element;

- (void)setMagnifyDelegate:(id<CZMagnifyDelegate>)magnifyDelegate;

- (CZElement *)createElementByClass:(Class)elementClass
                             inRect:(CGRect)rect;

- (CZElement *)createElementByClass:(Class)elementClass
                             inRect:(CGRect)rect
                         horizontal:(BOOL)horizontal;

@end

/*!
 CZSelectToolDelegate protocol
 */
@protocol CZSelectToolDelegate<NSObject>
@optional

- (void)selectTool:(CZSelectTool *)selectTool didSelectElement:(CZElement *)element;
- (void)selectTool:(CZSelectTool *)selectTool willDeselectElement:(CZElement *)element;
- (void)selectToolDidDeselectElement:(CZSelectTool *)selectTool;

- (void)selectToolDidCancelJob:(CZSelectTool *)selectTool;

// count tool methods
- (void)selectTool:(CZSelectTool *)selectTool didCountTo:(NSUInteger)count;
- (void)selectToolDidFinishCounting:(CZSelectTool *)selectTool;

// text tool methods
- (void)selectToolDidBeginEdting:(CZSelectTool *)selectTool;
- (void)selectToolDidEndEdting:(CZSelectTool *)selectTool;
- (void)selectToolDidChangeMode:(CZSelectTool *)selectTool;

// caliper tool methods
/** caliper tool waits for next (index) point, when creation.*/
- (void)selectTool:(CZSelectTool *)selectTool waitsCaliperPoint:(NSUInteger)pointIndex;
- (void)selectToolDidFinishCaliper:(CZSelectTool *)selectTool;

@end
