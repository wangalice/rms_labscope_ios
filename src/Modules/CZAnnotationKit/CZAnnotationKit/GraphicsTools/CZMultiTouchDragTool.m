//
//  CZDragLineTool.m
//  Matscope
//
//  Created by Ralph Jin on 11/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMultiTouchDragTool.h"
#import <CoreGraphics/CoreGraphics.h>
#import <CZToolbox/CZToolbox.h>
#import "CZElementLine.h"
#import "CZReshapeElementAction.h"

@interface CZMultiTouchDragTool ()

@property (nonatomic, retain) NSDictionary *elementMemo;

@end

@implementation CZMultiTouchDragTool

- (void)dealloc {
    [_elementMemo release];
    [super dealloc];
}

- (void)setTarget:(CZElement *)target {
    if (target && ![target isKindOfClass:[CZElementLine class]]) {
        return;
    }
    
    if (_target != target) {
        _target = target;

        NSDictionary *memo = [target newShapeMemo];
        self.elementMemo = memo;
        [memo release];
    }
}

- (BOOL)twoTouchsMoved:(CGPoint)p1 p2:(CGPoint)p2 {
    if (self.target == nil) {
        return NO;
    }
    
    CZElementLine *line = (CZElementLine *)self.target;
    CGPoint pt1 = line.p1, pt2 = line.p2;
    BOOL isHorizontal = fabs(pt1.x - pt2.x) >= fabs(pt1.y - pt2.y);
    
    pt1 = p1;
    pt2 = p2;
    if (isHorizontal) {
        pt1.y = pt2.y = (pt1.y + pt2.y) * 0.5;
    } else {
        pt1.x = pt2.x = (pt1.x + pt2.x) * 0.5;
    }
    
    [line notifyWillChange];
    line.p1 = pt1;
    line.p2 = pt2;
    [line notifyDidChange];
    
    pt1 = [CZCommonUtils imagePointToViewPoint:pt1];
    pt2 = [CZCommonUtils imagePointToViewPoint:pt2];
    if (!isnan(pt1.x) && !isnan(pt1.y)) {
        if ([_magnifyDelegate respondsToSelector:@selector(magnifyViewAtPoint:point2:)]) {
            [_magnifyDelegate magnifyViewAtPoint:pt1 point2:pt2];
        }
    }
    
    return YES;
}

- (BOOL)touchEnd:(CGPoint)p {
    CZReshapeElementAction *action = [[CZReshapeElementAction alloc] init];
    action.element = self.target;
    action.shapeMemo = self.elementMemo;
    [self.docManager pushAction:action];
    [action release];
    
    self.target = nil;
    return YES;
}

@end
