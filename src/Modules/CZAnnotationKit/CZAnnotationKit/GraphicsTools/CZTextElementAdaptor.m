//
//  CZTextElementAdaptor.m
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZTextElementAdaptor.h"

@interface CZTextElementAdaptor ()

@property (nonatomic, retain) CZElementText *element;

@end

@implementation CZTextElementAdaptor

- (id)initWithElement:(CZElementText *)element {
    self = [super init];
    if (self) {
        _element = [element retain];
    }
    
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (CGRect)rect {
    return self.element.frame;
}

- (void)setRect:(CGRect)rect {
    [self.element notifyWillChange];
    self.element.frame = rect;
    [self.element notifyDidChange];
}

- (CGFloat)rotateAngle {
    return self.element.rotateAngle;
}

- (void)setRotateAngle:(CGFloat)rotateAngle {
    [self.element notifyWillChange];
    self.element.rotateAngle = rotateAngle;
    [self.element notifyDidChange];
}

- (CGPoint)anchorPoint {
    return self.element.anchorPoint;
}

- (BOOL)showsBoundary {
    return YES;
}

@end
