//
//  CZAbstractGraphicsTool.m
//  Hermes
//
//  Created by Ralph Jin on 1/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAbstractGraphicsTool.h"
#import "CZElementLayer.h"


@implementation CZAbstractGestureTool

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    return CZShouldBeginTouchStateNo;
}

- (BOOL)touchBegin:(CGPoint)p {
    return FALSE;
}

- (BOOL)touchMoved:(CGPoint)p {
    return FALSE;
}

- (BOOL)twoTouchsMoved:(CGPoint)p1 p2:(CGPoint)p2 {
    return FALSE;
}

- (BOOL)touchEnd:(CGPoint)p {
    return FALSE;
}

- (BOOL)touchCancelled:(CGPoint)p {
    return FALSE;
}

- (BOOL)tap:(CGPoint)p {
    return FALSE;
}

- (BOOL)doubleTap:(CGPoint)p {
    return FALSE;
}

@end

@implementation CZAbstractGraphicsTool

- (id)initWithDocManager:(CZElementDoc)docManager {
    self = [super init];
    if (self) {
        if (docManager) {
            _docManager = docManager;
        } else {
            [self release];
            self = nil;
        }
    }
    return self;
}

- (CGPoint)cullPoint:(CGPoint)p {
    CGRect bounds = _docManager.elementLayer.frame;
    if (CGRectIsEmpty(bounds)) {
        return p;
    }
    
    if (p.x < 0.0f) {
        p.x = 0.0f;
    }
    
    if (p.y < 0.0f) {
        p.y = 0.0f;
    }
    
    if (p.x > bounds.size.width) {
        p.x = bounds.size.width;
    }
    
    if (p.y > bounds.size.height) {
        p.y = bounds.size.height;
    }
    
    return p;
}

@end
