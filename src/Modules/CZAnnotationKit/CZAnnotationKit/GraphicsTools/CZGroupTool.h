//
//  CZGroupTool.h
//  Matscope
//
//  Created by Ralph Jin on 1/10/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZAbstractGraphicsTool.h"
#import "CZElementLayer.h"

@class CZAnnotationHandler;

@interface CZGroupTool : CZAbstractGraphicsTool <CZElementLayerDelegate>

@property (nonatomic, assign, readonly) UIView *targetView;
@property (nonatomic, assign) CGFloat zoomOfView;

- (id)initWithDocManager:(CZElementDoc)docManager targetView:(UIView *)targetView;

- (void)showHandler:(BOOL)show;

- (void)updateGroupHandler;
- (void)updateTraceHandler;

@end
