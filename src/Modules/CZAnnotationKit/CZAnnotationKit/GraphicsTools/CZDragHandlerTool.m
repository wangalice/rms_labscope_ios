//
//  CZDragHandlerTool.m
//  Hermes
//
//  Created by Ralph Jin on 2/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDragHandlerTool.h"
#import <CZToolbox/CZToolbox.h>
#import "CZElement.h"
#import "CZAnnotationHandler.h"
#import "CZReshapeElementAction.h"
#import "CZDeleteElementAction.h"

@interface CZDragHandlerTool()

@property (nonatomic, assign) CZAnnotationHandlerPart hitPart;
@property (nonatomic, retain) NSDictionary *elementMemo;
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CGSize dragOffset;

@end

@implementation CZDragHandlerTool

- (void)dealloc {
    [_elementMemo release];
    [_element release];
    [_toolHandler release];
    [super dealloc];
}

#pragma mark -
#pragma mark override CZAbstractGraphicsTool

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    if ([_toolHandler isHidden]) {
        return CZShouldBeginTouchStateNo;
    }
    
    self.hitPart = [[_toolHandler hitTest:p] integerValue];
    
    BOOL hitOn = _hitPart != 0;
    return hitOn ? CZShouldBeginTouchStateYes : CZShouldBeginTouchStateNo;
}

- (BOOL)touchBegin:(CGPoint)p {
    if ([_toolHandler isHidden]) {
        return NO;
    }
    
    self.hitPart = [[_toolHandler hitTest:p] integerValue];
    
    BOOL hitOn = _hitPart != 0;
    
    if (hitOn) {
        CGPoint partPosition = [_toolHandler positionOfPart:self.hitPart];
        CGSize offset;
        offset.width = p.x - partPosition.x;
        offset.height = p.y - partPosition.y;
        
        self.dragOffset = offset;
        
        CGPoint dragPoint = [_toolHandler beginDragOnPart:_hitPart atPoint:partPosition];
        
        if (!isnan(dragPoint.x) && !isnan(dragPoint.y)) {
            if ([_magnifyDelegate respondsToSelector:@selector(temporaryHideElement:)]) {
                [_magnifyDelegate temporaryHideElement:_toolHandler.target];
            }
            [_magnifyDelegate magnifyAtPoint:partPosition];
        }
    }
    
    return hitOn;
}

- (BOOL)touchMoved:(CGPoint)p {
    if (_hitPart == 0) {
        return NO;
    }
    
    if (_elementMemo == nil) {
        NSDictionary *memo = [_toolHandler.target newShapeMemo];
        self.elementMemo = memo;
        [memo release];
    }
    
    p.x -= _dragOffset.width;
    p.y -= _dragOffset.height;
    
    CGPoint dragPoint = [_toolHandler dragOnPart:_hitPart toPoint:p];
    
    if (!isnan(dragPoint.x) && !isnan(dragPoint.y)) {
        [_magnifyDelegate magnifyAtPoint:dragPoint];
    }
    
    return YES;
}

- (BOOL)touchEnd:(CGPoint)p {
    if (_hitPart == 0) {
        return NO;
    }
    
    BOOL isRemovePart = (_hitPart >= CZAnnotationHandlerPartRemove && _hitPart <= CZAnnotationHandlerPartRemoveLast);
    if (isRemovePart) {  // save element shape before modification
        NSDictionary *memo = [_toolHandler.target newShapeMemo];
        self.elementMemo = memo;
        [memo release];
    }
    
    p.x -= _dragOffset.width;
    p.y -= _dragOffset.height;
    
    [_toolHandler endDragOnPart:_hitPart atPoint:p];

    if (self.elementMemo) {
        CZReshapeElementAction *action = [[CZReshapeElementAction alloc] init];
        action.element = _toolHandler.target;
        action.shapeMemo = self.elementMemo;
        [self.docManager pushAction:action];
        [action release];
    } else if (_hitPart == CZAnnotationHandlerPartRemoveAll) {
        CZDeleteElementAction *action = [[CZDeleteElementAction alloc] initWithElement:_toolHandler.target];
        if (self.docManager) {
            [self.docManager pushAction:action];
        } else {
            [action do];
        }
        [action release];
    }

    // clean up
    self.elementMemo = nil;
    self.hitPart = CZAnnotationHandlerPartNone;
    self.element = nil;
    
    return YES;
}

- (BOOL)tap:(CGPoint)p {
    self.hitPart = [[_toolHandler hitTest:p] integerValue];
    
    BOOL hitOn = _hitPart != 0;
    if (hitOn) {
        [_toolHandler endDragOnPart:_hitPart atPoint:p];
    }
    
    // clean up
    self.elementMemo = nil;
    self.hitPart = CZAnnotationHandlerPartNone;
    self.element = nil;
    
    return hitOn;
}

@end
