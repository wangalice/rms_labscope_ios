//
//  CZMultipointHandler.m
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMultipointHandler.h"
#import <QuartzCore/QuartzCore.h>
#import "CZColor.h"
#import "CZNodeShape.h"
#import <CZToolbox/CZToolbox.h>
#import "CZAnnotationHandlerKnob.h"
#import "CZAnnotationHandlerBoundary.h"
#import "CZMultipointObject.h"
#import "CZElementLayer.h"
#import "CZElementLine.h"
#import "CZElementArrow.h"
#import "CZElementCircle.h"
#import "CZElementDistance.h"
#import "CZElementMultiCalipers.h"
#import "CZElementMultiPoint.h"

//const static CZColor kCZHandlerColor = {155, 187, 89, 128};

#pragma mark - CZElementLineAdaptor

@interface CZElementLineAdaptor : NSObject<CZMultipointObject>

@property (nonatomic, readonly, retain) CZElementLine *line;

- (id)initWithElement:(CZElementLine *)line;

@end

#pragma mark - CZElementArrowAdaptor

@interface CZElementArrowAdaptor : NSObject<CZMultipointObject>

@property (nonatomic, readonly, retain) CZElementArrow *arrow;

- (id)initWithElement:(CZElementArrow *)arrow;

@end

#pragma mark - CZElementMultipointAdaptor

@interface CZElementMultipointAdaptor : NSObject<CZMultipointObject>
@property (nonatomic, readonly, retain) CZElement<CZElementMultiPointKind> *polygon;

- (id)initWithElement:(CZElement<CZElementMultiPointKind> *)polygon;

@end

#pragma mark - CZElementCircleAdaptor

@interface CZElementCircleAdaptor : NSObject<CZMultipointObject>
@property (nonatomic, readonly, retain) CZElementCircle *circle;

- (id)initWithElement:(CZElementCircle *)circle;

@end

#pragma mark - CZElementCaliperAdaptor

@interface CZElementCaliperAdaptor : NSObject<CZMultipointObject>
@property (nonatomic, readonly, retain) CZElement<CZElementMultiPointKind> *caliper;

- (id)initWithElement:(CZElement<CZElementMultiPointKind> *)countElement;

@end

#pragma mark - CZMultipointHandler

@interface CZMultipointHandler ()

@property (nonatomic, retain, readonly) CALayer *layer;
@property (nonatomic, retain) NSMutableArray *knobs;
@property (nonatomic, retain) CZAnnotationHandlerBoundary *boundary;
@property (nonatomic, retain) CZAnnotationHandlerKnob *auxKnob;
@property (nonatomic, retain) CALayer *auxKnobLayer;

- (void)checkAndCreateHandlerLayer;

@end

@implementation CZMultipointHandler

- (void)dealloc {
    [_layer release];
    [_knobs release];
    [_boundary release];
    [_multipointObject release];
    [_auxKnob release];
    [_auxKnobLayer release];
    
    [super dealloc];
}

- (CZAnnotationHandlerKnob *)auxKnob {
    if (!_auxKnob) {
        _auxKnob = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        _auxKnob.part = CZAnnotationHandlerPartDone;
    }
    return _auxKnob;
}

- (CALayer *)auxKnobLayer {
    if (!_auxKnobLayer) {
        _auxKnobLayer = [self.auxKnob newKnobLayer];
    }
    return _auxKnobLayer;
}

- (void)checkAndCreateHandlerLayer {
    if (_layer == nil) {  // Create a new layer and it's sub layers.
        _layer = [[CALayer alloc] init];
        _layer.name = CZAnnotationHandlerLayerName;
        _layer.frame = self.target.parent.frame;
        
        if (self.showConnectLine) {  // add connect line
            CALayer *layer = [[CAShapeLayer alloc] init];
            [_layer addSublayer:layer];
            [layer release];
        }
        [_layer addSublayer:self.auxKnobLayer];
    }
    
    if (_knobs == nil) {
        _knobs = [[NSMutableArray alloc] init];
    }
    
    NSUInteger oldCount = [_knobs count];
    NSUInteger newCount = [_multipointObject pointsCount];
    
    if (newCount > oldCount) {
        NSUInteger toAddCount = newCount - oldCount;
        for (NSUInteger i = 0; i < toAddCount; ++i) {
            CZAnnotationHandlerKnob *knob = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
            knob.part = i + CZAnnotationHandlerPartMove;
            [_knobs addObject:knob];
            [knob release];
            
            CALayer *layer = [knob newKnobLayer];
            [_layer addSublayer:layer];
            [layer release];
        }
    } else {
        NSUInteger toDeleteCount = oldCount - newCount;
        for (NSUInteger i = 0; i < toDeleteCount; ++i) {
            [_knobs removeLastObject];
            CALayer *last = [[_layer sublayers] lastObject];
            [last removeFromSuperlayer];
        }
    }
}

#pragma mark -
#pragma mark Override CZAnnotationHandler

- (void)setTarget:(CZElement *)target {
    self.showConnectLine = NO;
    
    Class elementClass = [target class];
    if (elementClass == [CZElementLine class]) {
        CZElementLine *elementLine = (CZElementLine *)target;
        [super setTarget:elementLine];
        
        CZElementLineAdaptor *adaptor = [[CZElementLineAdaptor alloc] initWithElement:elementLine];
        self.multipointObject = adaptor;
        [adaptor release];
    } else if (elementClass == [CZElementArrow class]) {
        CZElementArrow *elementArrow = (CZElementArrow *)target;
        [super setTarget:elementArrow];
        
        CZElementArrowAdaptor *adaptor = [[CZElementArrowAdaptor alloc] initWithElement:elementArrow];
        self.multipointObject = adaptor;
        [adaptor release];
    } else if (elementClass == [CZElementCircle class]) {
        CZElementCircle *elementCircle = (CZElementCircle *)target;
        [super setTarget:elementCircle];
        CZElementCircleAdaptor *adaptor = [[CZElementCircleAdaptor alloc] initWithElement:elementCircle];
        self.multipointObject = adaptor;
        [adaptor release];
    } else if (elementClass == [CZElementCalipers class] || elementClass == [CZElementMultiCalipers class]) {
        CZElement<CZElementMultiPointKind> *element = (CZElement<CZElementMultiPointKind> *)target;
        [super setTarget:element];
        CZElementCaliperAdaptor *adaptor = [[CZElementCaliperAdaptor alloc] initWithElement:element];
        self.multipointObject = adaptor;
        self.showConnectLine = YES;
        [adaptor release];
    } else if ([target conformsToProtocol:@protocol(CZElementMultiPointKind)]) {
        CZElement<CZElementMultiPointKind> *elementPolygon = (CZElement<CZElementMultiPointKind> *)target;
        [super setTarget:elementPolygon];
        CZElementMultipointAdaptor *adaptor = [[CZElementMultipointAdaptor alloc] initWithElement:elementPolygon];
        self.multipointObject = adaptor;
        [adaptor release];
    } else {
        [super setTarget:nil];
    }
}

- (CALayer *)representLayer {
    return self.layer;
}

- (void)updateWithDoneTap {
    if (self.multiTapDone) {
        if (self.isEditingPoint) {
            self.auxKnob.part = CZAnnotationHandlerPartExitEditing;
        } else {
            if (self.isRemoveKnobShown) {
                self.auxKnob.part = CZAnnotationHandlerPartRemoveAll;
            } else {
                self.auxKnob.part = CZAnnotationHandlerPartEnterEditing;
            }
        }
    } else {
        self.auxKnob.part = CZAnnotationHandlerPartDone;
    }
    
    if (self.doneButtonEnabled || self.isRemoveKnobShown) {
        BOOL hidden = NO;
        if ([self.target conformsToProtocol:@protocol(CZElementMultiPointKind)] &&
            [self.target respondsToSelector:@selector(maximumPointsCount)]) {
            CZElement <CZElementMultiPointKind> *polyElement = (CZElement <CZElementMultiPointKind> *)self.target;
            if ([polyElement minimumPointsCount] == [polyElement maximumPointsCount]) {
                hidden = YES;
            }
        }
        self.auxKnobLayer.hidden = hidden;
    } else {
        self.auxKnobLayer.hidden = YES;
    }
    
    CGPoint lastKnobPt = [self.multipointObject pointAtIndex:self.knobs.count - 1];
    lastKnobPt = [CZCommonUtils imagePointToViewPoint:lastKnobPt];
    
    CGFloat offset = kCZAnnotationHandlerKnobSize / self.zoomOfView * M_SQRT2 * 0.5;
    self.auxKnob.knobPoint = CGPointMake(lastKnobPt.x - offset, lastKnobPt.y - offset);
    [self.auxKnob updateKnobLayer:self.auxKnobLayer rotateAngle:0.0f];
}

- (void)updateWithZoom:(CGFloat)zoom {
    [super updateWithZoom:zoom];
    
    if (self.target == nil) {
        [_layer setHidden:YES];
        return;
    }
    
    [self checkAndCreateHandlerLayer];
    
    NSUInteger indexOffset = 0;
    if (self.showConnectLine) {  // add connect line
        indexOffset++;
        
        // udpate connect line's path
        CAShapeLayer *connectLineLayer = (CAShapeLayer *)[[_layer sublayers] objectAtIndex:0];
        if ([_multipointObject respondsToSelector:@selector(handler:willUpdateConnectLineInLayer:)]) {
            [_multipointObject handler:self willUpdateConnectLineInLayer:connectLineLayer];
        }
        
        if (connectLineLayer.path == NULL) {
            CGMutablePathRef path = CGPathCreateMutable();
            if ([self.knobs count] > 0) {
                CGPoint pt = [_multipointObject pointAtIndex:0];
                pt = [CZCommonUtils imagePointToViewPoint:pt];
                CGPathMoveToPoint(path, NULL, pt.x, pt.y);
            }
            for (NSUInteger i = 1; i < [self.knobs count]; ++i) {
                CGPoint pt = [_multipointObject pointAtIndex:i];
                pt = [CZCommonUtils imagePointToViewPoint:pt];
                CGPathAddLineToPoint(path, NULL, pt.x, pt.y);
            }
            connectLineLayer.path = path;
            CGPathRelease(path);
        }
        connectLineLayer.fillColor = [[UIColor clearColor] CGColor];
        CZColor lineColor = self.target.strokeColor;
        lineColor.a = 128;
        connectLineLayer.strokeColor = CGColorFromCZColor(lineColor);
        connectLineLayer.lineWidth = 1.0f / self.zoomOfView;
        
        [connectLineLayer removeAllAnimations];
    }
    
    if (self.auxKnobLayer) {
        indexOffset++;
    }
    
    const NSUInteger basePartID = self.isEditingPoint ? CZAnnotationHandlerPartRemove : CZAnnotationHandlerPartMove;
    BOOL needCheckRemovePoint = self.isEditingPoint && [self.multipointObject respondsToSelector:@selector(canRemovePointAtIndex:)];

    NSUInteger knobsCount = self.knobs.count;
    for (NSUInteger i = 0; i < knobsCount; ++i) {
        CGPoint pt = [self.multipointObject pointAtIndex:i];
        pt = [CZCommonUtils imagePointToViewPoint:pt];
        
        CZAnnotationHandlerKnob *knob = [self.knobs objectAtIndex:i];
        knob.connectPoint = pt;
        knob.knobPoint = pt;
        if (needCheckRemovePoint && ![self.multipointObject canRemovePointAtIndex:i]) {
            knob.part = CZAnnotationHandlerPartMove + i;
        } else {
            knob.part = basePartID + i;
        }
        
        CALayer *knobLayer = [[_layer sublayers] objectAtIndex:i + indexOffset];
        [knob updateKnobLayer:knobLayer rotateAngle:0.0f];
    }
    
    [self updateWithDoneTap];
    
    [_layer setHidden:NO];
}

- (CGPoint)positionOfPart:(CZAnnotationHandlerPart)part {
    NSUInteger index;
    if (part >= CZAnnotationHandlerPartMove && part <= CZAnnotationHandlerPartMoveLast) {
        index = (NSUInteger)part - CZAnnotationHandlerPartMove;
    } else if (part >= CZAnnotationHandlerPartRemove && part <= CZAnnotationHandlerPartRemoveLast) {
        index = (NSUInteger)part - CZAnnotationHandlerPartRemove;
    } else {
        index = NSUIntegerMax;
    }
    
    if (index < [self.multipointObject pointsCount]) {
        CGPoint pt = [self.multipointObject pointAtIndex:index];
        pt = [CZCommonUtils imagePointToViewPoint:pt];
        return pt;
    } else {
        return CGPointZero;  // unknown part
    }
}

- (CGPoint)beginDragOnPart:(CZAnnotationHandlerPart)part
                   atPoint:(CGPoint)pointInView {
    if (self.isEditingPoint) {
        return [super beginDragOnPart:part atPoint:pointInView];
    } else {
        return pointInView;
    }
}

- (CGPoint)dragOnPart:(CZAnnotationHandlerPart)part
              toPoint:(CGPoint)pointInView {
    if (self.isEditingPoint) {
        return [super dragOnPart:part toPoint:pointInView];
    } else {
        CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:pointInView];
        if (part >= CZAnnotationHandlerPartMove && part <= CZAnnotationHandlerPartMoveLast) {
            NSUInteger index = (NSUInteger)part - CZAnnotationHandlerPartMove;
            pointInImage = [self.multipointObject setPoint:pointInImage atIndex:index];
            pointInView = [CZCommonUtils imagePointToViewPoint:pointInImage];
        }
        return pointInView;
    }
}

- (CGPoint)endDragOnPart:(CZAnnotationHandlerPart)part atPoint:(CGPoint)pointInView {
    if (part == CZAnnotationHandlerPartEnterEditing) {
        if ([self.target isKindOfClass:[CZElementMultiPoint class]]) {
            CZElementMultiPoint *multipointElement = (CZElementMultiPoint *)self.target;
            if ([multipointElement minimumPointsCount] < [multipointElement pointsCount]) {
                self.editingPoint = YES;
                [self updateWithZoom:self.zoomOfView];
            }
        }
    } else if (part == CZAnnotationHandlerPartExitEditing) {
        self.editingPoint = NO;
        [self updateWithZoom:self.zoomOfView];
    } else {
        if (self.isEditingPoint) {
            if (part >= CZAnnotationHandlerPartRemove && part <= CZAnnotationHandlerPartRemoveLast) {
                NSUInteger index = (NSUInteger)part - CZAnnotationHandlerPartRemove;
                if ([self.multipointObject respondsToSelector:@selector(removePointAtIndex:)]) {
                    // exit editing point mode, if there is too less points
                    if ([self.target isKindOfClass:[CZElementMultiPoint class]]) {
                        CZElementMultiPoint *multipointElement = (CZElementMultiPoint *)self.target;
                        NSUInteger pointsCount = [multipointElement pointsCount] - 1;
                        if ([multipointElement minimumPointsCount] >= pointsCount) {
                            self.doneButtonEnabled = NO;
                            self.editingPoint = NO;
                        }
                    }
                    
                    // remove point
                    [self.multipointObject removePointAtIndex:index];
                }
            }
        }
    }
    
    return [super endDragOnPart:part atPoint:pointInView];
}

@end

#pragma mark - CZElementLineAdaptor

@implementation CZElementLineAdaptor

- (id)initWithElement:(CZElementLine *)line {
    self = [super init];
    if (self) {
        _line = [line retain];
    }
    return self;
}

- (void)dealloc {
    [_line release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return 2U;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    if (index == 0) {
        return _line.p1;
    } else if (index == 1) {
        return _line.p2;
    } else {
        return CGPointZero;
    }
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    if (index == 0) {
        [_line notifyWillChange];
        _line.p1 = point;
        [_line notifyDidChange];
    } else if (index == 1) {
        [_line notifyWillChange];
        _line.p2 = point;
        [_line notifyDidChange];
    }
    
    return point;
}

@end

#pragma mark - CZElementArrowAdaptor

@implementation CZElementArrowAdaptor

- (id)initWithElement:(CZElementArrow *)arrow {
    self = [super init];
    if (self) {
        _arrow = [arrow retain];
    }
    return self;
}

- (void)dealloc {
    [_arrow release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return 2U;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    if (index == 0) {
        return _arrow.p1;
    } else if (index == 1) {
        return _arrow.p2;
    } else {
        return CGPointZero;
    }
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    if (index == 0) {
        [_arrow notifyWillChange];
        _arrow.p1 = point;
        [_arrow notifyDidChange];
    } else if (index == 1) {
        [_arrow notifyWillChange];
        _arrow.p2 = point;
        [_arrow notifyDidChange];
    }
    return point;
}

@end

#pragma mark - CZElementMultipointAdaptor

@implementation CZElementMultipointAdaptor

- (id)initWithElement:(CZElement<CZElementMultiPointKind> *)polygon {
    self = [super init];
    if (self) {
        _polygon = [polygon retain];
    }
    return self;
}

- (void)dealloc {
    [_polygon release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return [_polygon pointsCount];
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    return [_polygon pointAtIndex:index];
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    [_polygon notifyWillChange];
    [_polygon setPoint:point atIndex:index];
    [_polygon notifyDidChange];
    return point;
}

- (void)removePointAtIndex:(NSUInteger)index {
    [_polygon notifyWillChange];
    [_polygon removePointAtIndex:index];
    [_polygon notifyDidChange];
}

- (void)handler:(CZMultipointHandler*)handler willUpdateConnectLineInLayer:(CAShapeLayer *)layer {
    if ([_polygon.primaryNode isKindOfClass:[CZNodeShape class]]) {
        CZNodeShape *spline = (CZNodeShape *)_polygon.primaryNode;
        
        CGFloat scale = [[UIScreen mainScreen] scale];
        scale = 1.0f / scale;  // convert image point to view point
        
        CGMutablePathRef path = CGPathCreateMutable();
        CGAffineTransform t = CGAffineTransformMakeScale(scale, scale);
        CGPathAddPath(path, &t, spline.relativePath);
        
        layer.path = path;
        layer.lineWidth = 2.0;
        CGPathRelease(path);
    } else {
        layer.path = NULL;
    }
}

@end

#pragma mark - CZElementCircleAdaptor

@implementation CZElementCircleAdaptor

- (id)initWithElement:(CZElementCircle *)circle {
    self = [super init];
    if (self) {
        _circle = [circle retain];
    }
    return self;
}

- (void)dealloc {
    [_circle release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return CZElementCirclePointCount;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    return [_circle pointAtIndex:index];
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    [_circle notifyWillChange];
    [_circle setPoint:point atIndex:index];
    [_circle notifyDidChange];
    return point;
}

@end

#pragma mark - CZElementCaliperAdaptor

@implementation CZElementCaliperAdaptor

- (id)initWithElement:(CZElement<CZElementMultiPointKind> *)caliper {
    self = [super init];
    if (self) {
        _caliper = [caliper retain];
    }
    return self;
}

- (void)dealloc {
    [_caliper release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return [_caliper pointsCount];
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    return [_caliper pointAtIndex:index];
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    [_caliper notifyWillChange];
    
    [_caliper setPoint:point atIndex:index];
    
    [_caliper notifyDidChange];
    
    return point;
}

- (void)handler:(CZMultipointHandler*)handler willUpdateConnectLineInLayer:(CAShapeLayer *)layer {
    CGMutablePathRef path = CGPathCreateMutable();
    
    if (_caliper.pointsCount == 2) {
        CGPoint p1 = [CZCommonUtils imagePointToViewPoint:[_caliper pointAtIndex:0]];
        CGPoint p2 = [CZCommonUtils imagePointToViewPoint:[_caliper pointAtIndex:1]];
        
        CGPathMoveToPoint(path, NULL, p1.x, p1.y);
        CGPathAddLineToPoint(path, NULL, p2.x, p2.y);
    } else if (_caliper.pointsCount >= 3) {
        CGPoint p1 = [CZCommonUtils imagePointToViewPoint:[_caliper pointAtIndex:0]];
        CGPoint p2 = [CZCommonUtils imagePointToViewPoint:[_caliper pointAtIndex:1]];
        
        CGPoint p3 = CGPointZero, othorPoint = CGPointZero;
        BOOL showsParallelLine = [_caliper isKindOfClass:[CZElementCalipers class]];
        for (NSUInteger index = 2; index < [_caliper pointsCount]; ++index) {
            p3 = [CZCommonUtils imagePointToViewPoint:[_caliper pointAtIndex:index]];
            if ([_caliper respondsToSelector:@selector(othorPointAtIndex:)]) {
                othorPoint = [CZCommonUtils imagePointToViewPoint:[(id)_caliper othorPointAtIndex:index]];
            } else if ([_caliper respondsToSelector:@selector(othorPoint)]) {
                othorPoint = [CZCommonUtils imagePointToViewPoint:[(id)_caliper othorPoint]];
            } else {
                othorPoint = p2;
            }
        
            // extend p1 or p2 if othor point is not
            if (p1.x != p2.x) {
                if (p1.x > othorPoint.x && p2.x > othorPoint.x) {
                    if (p1.x < p2.x) {
                        p1 = othorPoint;
                    } else {
                        p2 = othorPoint;
                    }
                } else if (p1.x < othorPoint.x && p2.x < othorPoint.x) {
                    if (p1.x > p2.x) {
                        p1 = othorPoint;
                    } else {
                        p2 = othorPoint;
                    }
                }
            } else {
                if (p1.y > othorPoint.y && p2.y > othorPoint.y) {
                    if (p1.y < p2.y) {
                        p1 = othorPoint;
                    } else {
                        p2 = othorPoint;
                    }
                } else if (p1.y < othorPoint.y && p2.y < othorPoint.y) {
                    if (p1.y > p2.y) {
                        p1 = othorPoint;
                    } else {
                        p2 = othorPoint;
                    }
                }
            }
            
            // draw perpendicular line
            CGPathMoveToPoint(path, NULL, othorPoint.x, othorPoint.y);
            CGPathAddLineToPoint(path, NULL, p3.x, p3.y);
            
            if (!handler.isMultiTapDone) {
                CGPathMoveToPoint(path, NULL, p1.x, p1.y);
                CGPathAddLineToPoint(path, NULL, p2.x, p2.y);
            }
        }

        if (showsParallelLine) {
            // calculate parallel line to the base line
            p1.x = p3.x + p1.x - othorPoint.x;
            p1.y = p3.y + p1.y - othorPoint.y;
            p2.x = p3.x + p2.x - othorPoint.x;
            p2.y = p3.y + p2.y - othorPoint.y;
            
            CGPathMoveToPoint(path, NULL, p1.x, p1.y);
            CGPathAddLineToPoint(path, NULL, p2.x, p2.y);
        }
    }
    
    layer.path = path;
    layer.lineWidth = 2.0;
    CGPathRelease(path);
}

- (BOOL)canRemovePointAtIndex:(NSUInteger)index {
    return index >= 2;
}

- (void)removePointAtIndex:(NSUInteger)index {
    if (index >= 2) {  // base line can't be removed.
        [_caliper notifyWillChange];
        [_caliper removePointAtIndex:index];
        [_caliper notifyDidChange];
    }
}

@end
