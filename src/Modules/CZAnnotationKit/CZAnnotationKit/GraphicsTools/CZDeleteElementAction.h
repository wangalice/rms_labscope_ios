//
//  CZDeleteElementAction.h
//  Hermes
//
//  Created by Ralph Jin on 3/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

@class CZElement;
@class CZElementLayer;

@interface CZDeleteElementAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element;

@end
