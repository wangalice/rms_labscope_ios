//
//  CZDeleteAllElementAction.m
//  Hermes
//
//  Created by Ralph Jin on 7/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZDeleteAllElementAction.h"

#import "CZElementLayer.h"
#import "CZElement.h"

@interface CZDeleteAllElementAction () {
    NSArray *_elementArray;
}
@end

@implementation CZDeleteAllElementAction

- (void)dealloc {
    [_elementArray release];
    _layer = nil;
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 4U * [_layer elementCount];
}

- (void)do {
    NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:[_layer elementCount]];
    for (NSUInteger i = 0; i < [_layer elementCount]; ++i) {
        CZElement *element = [_layer elementAtIndex:i];
        if (element) {
            [tempArray addObject:element];
        }
    }
    
    _elementArray = [[NSArray arrayWithArray:tempArray] retain];
    
    [self redo];
}

- (void)undo {
    for (CZElement *element in _elementArray) {
        [_layer addElement:element];
    }
}

- (void)redo {
    for (CZElement *element in _elementArray) {
        [element removeFromParent];
    }
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end
