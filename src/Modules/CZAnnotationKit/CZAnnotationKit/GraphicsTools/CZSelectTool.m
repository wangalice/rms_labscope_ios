//
//  CZSelectTool.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZSelectTool.h"
#import <QuartzCore/QuartzCore.h>
#import <CZToolbox/CZToolbox.h>
#import "CALayer+ReplaceWithSameName.h"
#import "CZElementLayer.h"
#import "CZElementRectangle.h"
#import "CZElementCircle.h"
#import "CZElementText.h"
#import "CZElementLine.h"
#import "CZElementArrow.h"
#import "CZElementAngle.h"
#import "CZElementDisconnectAngle.h"
#import "CZElementPolygon.h"
#import "CZElementPolyline.h"
#import "CZElementSpline.h"
#import "CZElementSplineContour.h"
#import "CZElementCount.h"
#import "CZElementScaleBar.h"
#import "CZElementDistance.h"
#import "CZElementMultiCalipers.h"
#import "CZAnnotationHandler.h"
#import "CZRectangleHandler.h"
#import "CZBoundaryHandler.h"
#import "CZMultipointHandler.h"
#import "CZTouchCreateTool.h"
#import "CZMultiTapCreateTool.h"
#import "CZDragTool.h"
#import "CZMultiTouchDragTool.h"
#import "CZDragHandlerTool.h"
#import "CZElementTextInputTool.h"
#import "CZDeleteElementAction.h"

const static CGFloat kAutoScrollTreshold = 30.0;
const static CGFloat kAutoScrollStep = 8.0;
const static CGFloat kAutoScrollMaxSpeed = 5.0f;

static inline BOOL FLOAT_EQUAL(CGFloat a, CGFloat b) {
    return fabs(a-b) < 1e-5f;
}

#pragma mark - CZElementPCBLineAdaptor

@interface CZElementPCBLineAdaptor : NSObject<CZMultipointObject>

@property (nonatomic, readonly, retain) CZElementLine *line;

- (id)initWithElement:(CZElementLine *)line;

@end

@implementation CZElementPCBLineAdaptor

- (id)initWithElement:(CZElementLine *)line {
    self = [super init];
    if (self) {
        _line = [line retain];
    }
    return self;
}

- (void)dealloc {
    [_line release];
    [super dealloc];
}

- (NSUInteger)pointsCount {
    return 2U;
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    if (index == 0) {
        return _line.p1;
    } else if (index == 1) {
        return _line.p2;
    } else {
        return CGPointZero;
    }
}

- (CGPoint)setPoint:(CGPoint)point atIndex:(NSUInteger)index {
    BOOL isHorz = fabs(_line.p1.x - _line.p2.x) > fabs(_line.p1.y - _line.p2.y);
    if (index == 0) {
        [_line notifyWillChange];
        _line.p1 = isHorz ? CGPointMake(point.x, _line.p1.y) : CGPointMake(_line.p1.x, point.y);
        point = _line.p1;
        [_line notifyDidChange];
    } else if (index == 1) {
        [_line notifyWillChange];
        _line.p2 = isHorz ? CGPointMake(point.x, _line.p2.y) : CGPointMake(_line.p2.x, point.y);
        point = _line.p2;
        [_line notifyDidChange];
    }
    
    return point;
}

@end

#pragma mark - CZSelectTool

@interface CZSelectTool() <CZDragToolDelegate> {
    CGSize _autoScrollDistance;
    CGPoint _lastMovedPoint;
    CZAbstractGraphicsTool *_currentTool;
}

@property (nonatomic, retain) CZAnnotationHandler *toolHandler;
@property (nonatomic, retain) CZDragTool *dragTool;
@property (nonatomic, retain) CZDragHandlerTool *dragHandlerTool;  // This tool's coordinate is based on view, others are based on image.
@property (nonatomic, retain) CZMultiTouchDragTool *multiTouchDragTool;

@property (nonatomic, retain) CZTouchCreateTool *createTool;
@property (nonatomic, retain) CZMultiTapCreateTool *multiTapCreateTool;

@property (nonatomic, retain) CZElementTextInputTool *textInputTool;

@property (nonatomic, retain) NSTimer *autoScrollTimer;

@end

@implementation CZSelectTool

+ (BOOL)isMultitapCreationMode:(CZSelectToolCreationMode)creationMode {
    return (creationMode != kCZSelectToolCreationModeNormal);
}

- (id)initWithDocManager:(CZElementDoc)docManager view:(UIView *)view {
    self = [super initWithDocManager:docManager];
    if (self) {
        if (view == nil) {
            [self release];
            self = nil;
        } else {
            _view = view;
            _zoomOfView = 1.0f;
            _toolHandler = nil;
            
            _autoScrollTimer = nil;
            _autoScrollDistance = CGSizeZero;
            
            _dragTool = [[CZDragTool alloc] initWithDocManager:self.docManager];
            _dragTool.delegate = self;
            
            _dragHandlerTool = [[CZDragHandlerTool alloc] initWithDocManager:self.docManager];
            
            _multiTouchDragTool = [[CZMultiTouchDragTool alloc] initWithDocManager:self.docManager];
            
            _createTool = [[CZTouchCreateTool alloc] initWithDocManager:self.docManager];
            _createTool.delegate = self;
            
            _multiTapCreateTool = [[CZMultiTapCreateTool alloc] initWithDocManager:self.docManager view:view];
            _multiTapCreateTool.delegate = self;
            
            _textInputTool = [[CZElementTextInputTool alloc] initWithDocManager:self.docManager];
            _textInputTool.imageView = view;
            _textInputTool.delegate = self;
        }
    }
    
    return self;
}

- (void)dealloc {
    [_autoScrollTimer invalidate];
    _autoScrollTimer = nil;
    [_toolHandler release];
    [_createTool release];
    [_multiTapCreateTool release];
    [_dragTool release];
    [_dragHandlerTool release];
    [_multiTouchDragTool release];
    [_textInputTool release];
    
    [super dealloc];
}

// override setter
- (void)setCreationMode:(CZSelectToolCreationMode)mode {
    BOOL modeChanged = _creationMode != mode;
    
    _creationMode = mode;
    
    switch (mode) {
        case kCZSelectToolCreationModePolygon:
            _multiTapCreateTool.elementClass = [CZElementPolygon class];
            break;
        case kCZSelectToolCreationModePolyline:
            _multiTapCreateTool.elementClass = [CZElementPolyline class];
            break;
        case kCZSelectToolCreationModeCount:
            _multiTapCreateTool.elementClass = [CZElementCount class];
            break;
        case kCZSelectToolCreationModeSplineContour:
            _multiTapCreateTool.elementClass = [CZElementSplineContour class];
            break;
        case kCZSelectToolCreationModeSpline:
            _multiTapCreateTool.elementClass = [CZElementSpline class];
            break;
        case kCZSelectToolCreationModeCalipers:
            _multiTapCreateTool.elementClass = [CZElementCalipers class];
            break;
        case kCZSelectToolCreationModeMultiCalipers:
            _multiTapCreateTool.elementClass = [CZElementMultiCalipers class];
            break;
        case kCZSelectToolCreationModeNormal:
            break;
    }
    
    if (modeChanged) {
        if ([self.delegate respondsToSelector:@selector(selectToolDidChangeMode:)]) {
            [self.delegate selectToolDidChangeMode:self];
        }
    }
}

- (void)setMagnifyDelegate:(id<CZMagnifyDelegate>)magnifyDelegate {
    self.dragHandlerTool.magnifyDelegate = magnifyDelegate;
    self.multiTouchDragTool.magnifyDelegate = magnifyDelegate;
}

- (void)selectElement:(CZElement *)element {
    if ([self.toolHandler target] == element) {
        if (element) {
            [self.toolHandler setHidden:NO];
        }
        
        return;
    }
    
    CZAnnotationHandler *handler = nil;
    
    if ([element isMemberOfClass:[CZElementRectangle class]] ||
        [element isMemberOfClass:[CZElementText class]]) {
        handler = [[CZRectangleHandler alloc] init];
        handler.removeKnobShown = self.isRemoveKnobShown;
        handler.target = element;
    } else if ([element isMemberOfClass:[CZElementText class]] ||
               [element isMemberOfClass:[CZElementCircle class]] ||
               [element isMemberOfClass:[CZElementLine class]] ||
               [element isMemberOfClass:[CZElementArrow class]] ||
               [element isMemberOfClass:[CZElementAngle class]] ||
               [element isMemberOfClass:[CZElementDisconnectedAngle class]] ||
               [element isMemberOfClass:[CZElementPolygon class]] ||
               [element isMemberOfClass:[CZElementPolyline class]] ||
               [element isMemberOfClass:[CZElementSpline class]] ||
               [element isMemberOfClass:[CZElementSplineContour class]] ||
               [element isMemberOfClass:[CZElementCount class]] ||
               [element isMemberOfClass:[CZElementCalipers class]] ||
               [element isMemberOfClass:[CZElementMultiCalipers class]]) {
        CZMultipointHandler *multipointHandler = [[CZMultipointHandler alloc] init];
        multipointHandler.multiTapDone = YES;
        multipointHandler.editingPoint = NO;
        multipointHandler.removeKnobShown = self.isRemoveKnobShown;
        multipointHandler.target = element;
        if ([element isKindOfClass:[CZElementMultiPoint class]]) {
            NSUInteger minPointsCount = [(CZElementMultiPoint *)element minimumPointsCount];
            NSUInteger pointsCount = [(CZElementMultiPoint *)element pointsCount];
            multipointHandler.doneButtonEnabled = pointsCount > minPointsCount;
        } else {
            multipointHandler.doneButtonEnabled = NO;
        }
        
        handler = multipointHandler;
    } else if ([element isMemberOfClass:[CZElementScaleBar class]]) {
        handler = [[CZBoundaryHandler alloc] init];
        handler.target = element;
    }
    
    if (handler) {
        if (self.editingMode == kCZSelectToolEditingModePCB && [element isMemberOfClass:[CZElementLine class]]) {
            CZElementPCBLineAdaptor *adaptor = [[CZElementPCBLineAdaptor alloc] initWithElement:(CZElementLine *)element];
            ((CZMultipointHandler *)handler).multipointObject = adaptor;
            [adaptor release];
        }
        
        [handler updateWithZoom:self.zoomOfView];
        self.toolHandler = handler;
        [handler release];

        CALayer *representLayer = [self.toolHandler representLayer];
        [self.view.layer replaceSublayerWithSameNameLayer:representLayer bringTop:YES];
        
        // NOTICE: since it don't support multi-selection, it just skip the deselect call-back.
        if ([self.delegate respondsToSelector:@selector(selectTool:didSelectElement:)]) {
            [self.delegate selectTool:self didSelectElement:self.toolHandler.target];
        }
    }
}

- (CZElement *)selectedElement {
    return self.toolHandler.target;
}

- (CZElement *)temporarilyHiddenElement {
    if (_textInputTool.targetTextElemnt) {
        return _textInputTool.targetTextElemnt;
    } else if (_multiTapCreateTool.elementBeingCreated) {
        return _multiTapCreateTool.elementBeingCreated;
    } else {
        return nil;
    }
}

- (void)checkAutoScrollByElement:(CZElement *)element {
    _autoScrollDistance = CGSizeZero;

    if (element) {
        CGRect elementFrame = [element boundary];
        CGRect rectInView = [CZCommonUtils imageRectToViewRect:elementFrame];
        [self checkAutoScrollByRect:rectInView];
    }
}

- (void)checkAutoScrollByPoint:(CGPoint)pointInView {
    CGRect rect = CGRectMake(pointInView.x, pointInView.y, 0, 0);
    [self checkAutoScrollByRect:rect];
}

- (void)checkAutoScrollByRect:(CGRect)rectInView {
    _autoScrollDistance = CGSizeZero;
    
    UIScrollView *scrollView = [self superScrollView];
    if (!scrollView) {
        return;
    }

    // Convert point from content view to scroll view
    CGRect rectInScrollView = [scrollView convertRect:rectInView fromView:self.view];

    // Calculate the limitation of the srcoll area
    CGRect visibleBounds;
    visibleBounds.origin = scrollView.contentOffset;
    visibleBounds.size = scrollView.bounds.size;
    CGRect noneAutoScrollBounds = CGRectInset(visibleBounds, kAutoScrollTreshold, kAutoScrollTreshold);
    
    CGFloat leftDistance = rectInScrollView.origin.x - noneAutoScrollBounds.origin.x;
    CGFloat rightDistance = CGRectGetMaxX(rectInScrollView) - CGRectGetMaxX(noneAutoScrollBounds);
    
    if (leftDistance < 0) {
        _autoScrollDistance.width = MAX(-kAutoScrollMaxSpeed, ceil(leftDistance / kAutoScrollStep));
    } else if (rightDistance > 0) {
        _autoScrollDistance.width = MIN(kAutoScrollMaxSpeed, ceil(rightDistance / kAutoScrollStep));
    }
    
    CGFloat topDistance = rectInScrollView.origin.y - noneAutoScrollBounds.origin.y;
    CGFloat bottomDistance = CGRectGetMaxY(rectInScrollView) - CGRectGetMaxY(noneAutoScrollBounds);
    if (topDistance < 0) {
        _autoScrollDistance.height = MAX(-kAutoScrollMaxSpeed, ceil(topDistance / kAutoScrollStep));
    } else if (bottomDistance > 0) {
        _autoScrollDistance.height = MIN(kAutoScrollMaxSpeed, ceil(bottomDistance / kAutoScrollStep));
    }
}

- (void)scrollElementToVisible:(CZElement *)element {
    UIScrollView *scrollView = [self superScrollView];
    
    if (scrollView) {        
        UIEdgeInsets inset = scrollView.contentInset;
        CGFloat limitTop = -inset.top;
        CGFloat limitLeft = -inset.left;
        CGFloat limitBottom = scrollView.contentSize.height + inset.bottom;
        CGFloat limitRight = scrollView.contentSize.width + inset.right;
        
        CGFloat scale = 1.0 / [[UIScreen mainScreen] scale];  // image to view coordinate
        scale *= scrollView.zoomScale;  // content view coordinate to scroll view visible coordinate
        CGAffineTransform imageToContentTransform = CGAffineTransformMakeScale(scale, scale);
        
        CGRect frame = [element boundary];
        frame = CGRectApplyAffineTransform(frame, imageToContentTransform);
        
        CGRect visibleBounds;
        visibleBounds.origin = scrollView.contentOffset;
        visibleBounds.size = scrollView.bounds.size;
        
        if (visibleBounds.origin.x > frame.origin.x) {
            visibleBounds.origin.x = MAX(limitLeft, frame.origin.x);
        } else {
            CGFloat elementRight = CGRectGetMaxX(frame);
            if (elementRight > CGRectGetMaxX(visibleBounds)) {
                CGFloat minX = MIN(limitRight, elementRight);
                visibleBounds.origin.x = minX - visibleBounds.size.width;
            }
        }
        
        if (visibleBounds.origin.y > frame.origin.y) {
            visibleBounds.origin.y = MAX(limitTop, frame.origin.y);
        } else {
            CGFloat elementBottom = CGRectGetMaxY(frame);
            if (elementBottom > CGRectGetMaxY(visibleBounds)) {
                CGFloat minY = MIN(limitBottom, elementBottom);
                visibleBounds.origin.y = minY - visibleBounds.size.height;
            }
        }
        
        scrollView.contentOffset = visibleBounds.origin;
    }
}

- (void)deselect {
    if (self.toolHandler.target == nil) {
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(selectTool:willDeselectElement:)]) {
        [self.delegate selectTool:self willDeselectElement:self.toolHandler.target];
    }
    
    self.toolHandler.target = nil;
    [self.toolHandler setHidden:YES];
    
    if ([self.delegate respondsToSelector:@selector(selectToolDidDeselectElement:)]) {
        [self.delegate selectToolDidDeselectElement:self];
    }
}

- (BOOL)canCancelJob {
    if ([CZSelectTool isMultitapCreationMode:self.creationMode]) {
        return ![self.multiTapCreateTool canFinishCreating];
    } else {
        return YES;
    }
}

- (void)cancelJob {
    if ([self canCancelJob]) {
        [self.textInputTool dismissEditing];
        [self.multiTapCreateTool touchCancelled:CGPointZero];
        [self deselect];
        [self stopAutoScroll];
        
        if ([self.delegate respondsToSelector:@selector(selectToolDidCancelJob:)]) {
            [self.delegate selectToolDidCancelJob:self];
        }        
    }
}

- (void)setZoomOfView:(CGFloat)zoomOfView {
    if (zoomOfView == 0) {
        zoomOfView = 0.0001;
    }
    
    _zoomOfView = zoomOfView;
    
    if (_toolHandler && !FLOAT_EQUAL(_toolHandler.zoomOfView, zoomOfView)) {
        [_toolHandler updateWithZoom:zoomOfView];
    }
    
    if (_multiTapCreateTool && !FLOAT_EQUAL(_multiTapCreateTool.zoomOfView, zoomOfView)) {
        [_multiTapCreateTool updateWithZoom:zoomOfView];
    }
    
    if (_dragTool) {
        CGFloat scale = zoomOfView / [[UIScreen mainScreen] scale];
        if (!FLOAT_EQUAL(_dragTool.zoomOfImage, scale)) {
            _dragTool.zoomOfImage = scale;
        }
    }
}

- (void)deleteSelected {
    if (self.toolHandler.target == nil) {
        return;
    }
    
    CZElement *toDelete = [self.toolHandler.target retain];
   
    [self deselect];
    
    CZDeleteElementAction *action = [[CZDeleteElementAction alloc] initWithElement:toDelete];
    [toDelete release];
    
    [self.docManager pushAction:action];
    [action release];
}

- (CZElement *)createElementByClass:(Class)elementClass inRect:(CGRect)rect {
    self.createTool.elementClass = elementClass;
    return [self.createTool createElementInRect:rect horizontal:YES];
}

- (CZElement *)createElementByClass:(Class)elementClass inRect:(CGRect)rect horizontal:(BOOL)horizontal {
    self.createTool.elementClass = elementClass;
    return [self.createTool createElementInRect:rect horizontal:(BOOL)horizontal];
}

#pragma mark - override CZAbstractGraphicsTool

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    if ([_textInputTool isEditing]) {
        return CZShouldBeginTouchStateNo;
    }
    
    CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
    
    if (self.creationMode == kCZSelectToolCreationModeNormal) {
        _dragHandlerTool.toolHandler = self.toolHandler;
        CZShouldBeginTouchState state =  [_dragHandlerTool shouldBeginTouch:p];
        if (!state) {
            state = [_dragTool shouldBeginTouch:pointInImage];
        }
        return state;
    } else {  // kCZSelectToolModeCreatePolygon
        return [_multiTapCreateTool shouldBeginTouch:pointInImage];
    }
}

- (BOOL)touchBegin:(CGPoint)p {
    CZLogv(@"Select tool begin touch");
    
    [self stopAutoScroll];  // Stop auto scroll anyway
    
    _currentTool = nil;
    
    if ([_textInputTool isEditing]) {
        return NO;
    }
    
    CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
    
    BOOL handled = NO;
    if (self.creationMode == kCZSelectToolCreationModeNormal) {
        _dragHandlerTool.toolHandler = self.toolHandler;
        handled = [_dragHandlerTool touchBegin:p];
        if (handled) {
            _currentTool = _dragHandlerTool;
            return handled;
        }
        
        handled = [_dragTool touchBegin:pointInImage];
        if (handled) {
            _currentTool = _dragTool;
            return handled;
        }
    } else if ([CZSelectTool isMultitapCreationMode:self.creationMode]) {
        handled = [_multiTapCreateTool touchBegin:pointInImage];
        if (handled) {
            _currentTool = _multiTapCreateTool;
            return handled;
        }
    }
    
    return handled;
}

- (BOOL)touchMoved:(CGPoint)pointOfView {
    BOOL handled = NO;

    _lastMovedPoint = pointOfView;
        
    if (_currentTool == _dragHandlerTool) {
        handled = [_currentTool touchMoved:pointOfView];
    } else {
        CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:pointOfView];
        handled = [_currentTool touchMoved:pointInImage];
    }
    
    if (handled) {        
        if (_currentTool == _dragTool) {
            [self checkAutoScrollByElement:_dragTool.element];
        } else if (_currentTool != nil) {
            [self checkAutoScrollByPoint:pointOfView];
        }
        
        if (CGSizeEqualToSize(_autoScrollDistance, CGSizeZero)) {
            [self stopAutoScroll];
        } else {
            [self startAutoScroll];
        }
    }

    return handled;
}

- (BOOL)twoTouchsMoved:(CGPoint)p1 p2:(CGPoint)p2 {
    BOOL handled = NO;
    
    _lastMovedPoint = p1;
    
    CGPoint ip1 = [CZCommonUtils viewPointToImagePoint:p1];
    CGPoint ip2 = [CZCommonUtils viewPointToImagePoint:p2];
    
    if (_multiTouchDragTool != _currentTool) {
        // find current operation target element
        CZElement* targetElement = nil;
        if (_currentTool == _dragTool) {
            targetElement = _dragTool.element;
        } else if (_currentTool == _dragHandlerTool) {
            targetElement = _dragHandlerTool.toolHandler.target;
        }
        
        // keep finding
        if (targetElement == nil) {
            CGFloat tolerance = kDefaultTolerance / self.zoomOfView;
            targetElement = [self.docManager.elementLayer hitTest:ip1 tolerance:tolerance];
            if (targetElement == nil) {
                targetElement = [self.docManager.elementLayer hitTest:ip2 tolerance:tolerance];
            }
            
            if (![self isElementEditable:targetElement]) {
                targetElement = nil;
            }
            
            if (targetElement == nil) {
                targetElement = self.selectedElement;
            }
        }
        
        // pass current target element to multi-touch drag tool.
        if (targetElement) {
            _multiTouchDragTool.target = targetElement;
            
            [self selectElement:targetElement];
            
            // cancel what drag tool or drag handle tool is previously done.
            [_currentTool touchCancelled:CGPointZero];
            _currentTool = _multiTouchDragTool;
        }
    }
    
    handled = [_currentTool twoTouchsMoved:ip1 p2:ip2];
    
    // handle auto scrolling
    if (handled) {
        [self checkAutoScrollByElement:self.selectedElement];
        
        if (CGSizeEqualToSize(_autoScrollDistance, CGSizeZero)) {
            [self stopAutoScroll];
        } else {
            [self startAutoScroll];
        }
    }
    
    return handled;
}

- (BOOL)touchEnd:(CGPoint)p {
    CZLogv(@"Select tool end touch");

    [self stopAutoScroll];  // Stop auto scroll anyway

    BOOL handled = NO;
    if (_currentTool == _dragHandlerTool) {
        handled = [_currentTool touchEnd:p];
    } else {
        CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
        if (_currentTool) {
            if (_currentTool == _multiTouchDragTool) {
                [self selectElement:_multiTouchDragTool.target];
            } else if (_currentTool == _dragTool) {
                [self selectElement:_dragTool.element];
            }
            
            handled = [_currentTool touchEnd:pointInImage];
        } else {
            [self deselect];
        }
    }
    _currentTool = nil;
    return handled;
}

- (BOOL)touchCancelled:(CGPoint)p {
    [self stopAutoScroll];
    
    BOOL handled = NO;
    if (_currentTool == _dragHandlerTool) {
        handled = [_currentTool touchCancelled:p];
    } else {
        CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
        handled = [_currentTool touchCancelled:pointInImage];
    }
    _currentTool = nil;
    return handled;
}

- (BOOL)tap:(CGPoint)p {
    BOOL handled = NO;
    
    if (self.creationMode == kCZSelectToolCreationModeNormal) {
        _dragHandlerTool.toolHandler = self.toolHandler;
        handled = [_dragHandlerTool tap:p];
        if (handled) {
            return handled;
        }
    }
    
    CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
    
    if (self.creationMode == kCZSelectToolCreationModeNormal) {
        handled = [self.textInputTool dismissEditing];
        if (handled) {
            return handled;
        }

        CGFloat tolerance = kDefaultTolerance / self.zoomOfView;
        CZElement *hitElement = [self.docManager.elementLayer hitTest:pointInImage tolerance:tolerance];
        if (![self isElementEditable:hitElement]) {
            hitElement = nil;
        }
        if (hitElement) {
            [self selectElement:hitElement];
        } else {
            [self deselect];
        }
        handled = hitElement != nil;
    } else if ([self.class isMultitapCreationMode:self.creationMode]) {
        handled = [_multiTapCreateTool tap:pointInImage];
    }
    return handled;
}

- (BOOL)doubleTap:(CGPoint)p {
    BOOL handled = NO;
    CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:p];
    
    if (self.creationMode == kCZSelectToolCreationModeNormal) {
        CGFloat tolerance = kDefaultTolerance / self.zoomOfView;
        CZElement *hitElement = [self.docManager.elementLayer hitTest:pointInImage tolerance:tolerance];
        if (![self isElementEditable:hitElement]) {
            hitElement = nil;
        }
        
        handled = [self.textInputTool tryEditOnElement:hitElement];
        if (handled) {
            [self deselect];
            
            if ([self.delegate respondsToSelector:@selector(selectToolDidBeginEdting:)]) {
                [self.delegate selectToolDidBeginEdting:self];
            }
        }
    } else if ([self.class isMultitapCreationMode:self.creationMode]) {
        handled = [_multiTapCreateTool doubleTap:pointInImage];
    }
    
    return handled;
}

#pragma mark - CZDragToolDelegate methods

- (BOOL)shouldDragTool:(CZDragTool *)dragTool editingElement:(CZElement *)element {
    return [self isElementEditable:element];
}

#pragma mark - CZTouchCreateDelegate methods

- (void)touchCreateTool:(CZTouchCreateTool *)tool didCreateElement:(CZElement *)element {
    if ([element isMemberOfClass:[CZElementText class]]) {
        BOOL handled = [_textInputTool tryEditOnElement:element];
        _textInputTool.creating = YES;
        if (handled && [self.delegate respondsToSelector:@selector(selectToolDidBeginEdting:)]) {
            [self.delegate selectToolDidBeginEdting:self];
        }
    } else {
        [self selectElement:element];
    }
}

#pragma mark - CZElementTextInputToolDelegate methods

- (void)textInputTool:(CZElementTextInputTool *)textInputTool didEndCreateElement:(CZElement *)element {
    [self selectElement:element];
}

- (void)textInputToolDidEndEditing:(CZElementTextInputTool *)textInputTool {
    if ([self.delegate respondsToSelector:@selector(selectToolDidEndEdting:)]) {
        [self.delegate selectToolDidEndEdting:self];
    }
}

#pragma mark - CZMultiTapCreateToolDelegate methods

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didDragPoint:(CGPoint)pointOfImage {
    CGPoint pointOfView = [CZCommonUtils imagePointToViewPoint:pointOfImage];
    [_dragHandlerTool.magnifyDelegate magnifyAtPoint:pointOfView];
}

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didCreateElement:(CZElement *)element {
    self.creationMode = kCZSelectToolCreationModeNormal;
    [self selectElement:element];
    [element notifyDidChange];   // force update element once more
    
    if (tool.elementClass == [CZElementCount class]) {
        if ([self.delegate respondsToSelector:@selector(selectToolDidFinishCounting:)]) {
            [self.delegate selectToolDidFinishCounting:self];
        }
    } else if (tool.elementClass == [CZElementCalipers class] ||
               tool.elementClass == [CZElementMultiCalipers class]) {
        if ([self.delegate respondsToSelector:@selector(selectToolDidFinishCaliper:)]) {
            [self.delegate selectToolDidFinishCaliper:self];
        }
    }
}

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didAddPointAtCount:(NSUInteger)count {
    if (tool.elementClass == [CZElementCount class]) {
        if ([self.delegate respondsToSelector:@selector(selectTool:didCountTo:)]) {
            [self.delegate selectTool:self didCountTo:count];
        }
    } else if (tool.elementClass == [CZElementCalipers class] ||
               tool.elementClass == [CZElementMultiCalipers class]) {
        if ([self.delegate respondsToSelector:@selector(selectTool:waitsCaliperPoint:)]) {
            [self.delegate selectTool:self waitsCaliperPoint:count];
        }
    }
}

- (void)multiTapCreateToolDidCancelled:(CZMultiTapCreateTool *)tool {
    if (tool.elementClass == [CZElementCalipers class] ||
        tool.elementClass == [CZElementMultiCalipers class]) {
        if ([self.delegate respondsToSelector:@selector(selectToolDidFinishCaliper:)]) {
            [self.delegate selectToolDidFinishCaliper:self];
        }
    }
    
    self.creationMode = kCZSelectToolCreationModeNormal;
    
    if ([self.delegate respondsToSelector:@selector(selectTool:willDeselectElement:)]) {
        [self.delegate selectTool:self willDeselectElement:nil];
    }
}

#pragma mark - CZElementLayerDelegate methods

- (void)layer:(CZElementLayer *)layer didRemoveElement:(CZElement *)element {
    // deselect the element, if it's going to disappear.
    if ([self selectedElement] == element) {
        [self deselect];
    }
    
    if (_multiTapCreateTool.elementBeingCreated == element) {
        [_multiTapCreateTool cancelCreation];
    }
}

- (void)layer:(CZElementLayer *)layer didChangeElement:(CZElement *)element {    
    if ([self selectedElement] == element) {
        [_toolHandler updateWithZoom:self.zoomOfView];
    } else if (_multiTapCreateTool.elementBeingCreated == element) {
        [_multiTapCreateTool updateWithZoom:self.zoomOfView];
    }
}

#pragma mark - private methods

- (UIScrollView *)superScrollView {
    UIView *scrollView = self.view.superview.superview;
    
    if ([scrollView isKindOfClass:[UIScrollView class]]) {
        return (UIScrollView *)scrollView;
    } else {
        return nil;
    }
}

- (void)stopAutoScroll {
    _autoScrollDistance = CGSizeZero;
    if (_autoScrollTimer) {
        [self.autoScrollTimer invalidate];
        self.autoScrollTimer = nil;
    }
}

- (void)startAutoScroll {
    if (CGSizeEqualToSize(_autoScrollDistance, CGSizeZero)) {
        return;
    }
    
    if (_autoScrollTimer == nil) {
        self.autoScrollTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0 / 60.0)
                                                                target:self
                                                              selector:@selector(autoScrollTimerFired:)
                                                              userInfo:nil
                                                               repeats:YES];
    }
}

- (void)autoScrollTimerFired:(NSTimer *)timer {
    [self performAutoScroll];
    
    if (CGSizeEqualToSize(_autoScrollDistance, CGSizeZero)) {
        [self.autoScrollTimer invalidate];
        self.autoScrollTimer = nil;
    }
}

- (void)performAutoScroll {
    UIScrollView *scrollView = [self superScrollView];
    if (!scrollView) {
        return;
    }
    
    // Calculate the limitation of the srcoll area
    UIEdgeInsets inset = scrollView.contentInset;
    CGFloat limitTop = -inset.top;
    CGFloat limitLeft = -inset.left;
    CGFloat limitBottom = scrollView.contentSize.height + inset.bottom;
    CGFloat limitRight = scrollView.contentSize.width + inset.right;
    
    // Scroll according to |direction|
    CGPoint offset = scrollView.contentOffset;
    CGPoint oldOffset = offset;
    CGSize visibleSize = scrollView.bounds.size;

    offset.x += _autoScrollDistance.width;
            
    if (offset.x < limitLeft) {
        offset.x = limitLeft;
        _autoScrollDistance.width = 0;
    } else if (offset.x + visibleSize.width > limitRight) {
        offset.x = limitRight - visibleSize.width;
        if (offset.x < 0) {
            offset.x = 0;
        }
        _autoScrollDistance.width = 0;
    }
    
    offset.y += _autoScrollDistance.height;
    
    if (offset.y < limitTop) {
        offset.y = limitTop;
        _autoScrollDistance.height = 0;
    } else if (offset.y + visibleSize.height > limitBottom) {
        offset.y = limitBottom - visibleSize.height;
        if (offset.y < 0) {
            offset.y = 0;
        }
        _autoScrollDistance.height = 0;
    }
    
    [scrollView setContentOffset:offset];
    
    // send the new point to tool
    CGSize movedDistance;
    movedDistance.width = offset.x - oldOffset.x;
    movedDistance.height = offset.y - oldOffset.y;
    
    CGFloat zoomScale = MAX(0.0001, scrollView.zoomScale);
    
    _lastMovedPoint.x += movedDistance.width / zoomScale;
    _lastMovedPoint.y += movedDistance.height / zoomScale;
    
    if (_currentTool == _dragHandlerTool) {
        [_currentTool touchMoved:_lastMovedPoint];
    } else {
        CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:_lastMovedPoint];
        [_currentTool touchMoved:pointInImage];
    }
}

- (BOOL)isElementEditable:(CZElement *)element {
    if (self.editingMode == kCZSelectToolEditingModePCB) {
        if ([element isKindOfClass:[CZElementLine class]]) {
            CZElementLine *elementLine = (CZElementLine *)element;
            return elementLine.measurementNameHidden;
        } else {
            return NO;
        }
    } else {  // kCZSelectToolEditingModeNormal
        if ([element isKindOfClass:[CZElementLine class]]) {
            CZElementLine *elementLine = (CZElementLine *)element;
            return !elementLine.measurementNameHidden;
        } else {
            return YES;
        }
    }
}

@end
