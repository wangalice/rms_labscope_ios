//
//  CZElementTextInputTool.m
//  Hermes
//
//  Created by Ralph Jin on 3/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZElementTextInputTool.h"
#import <CZToolbox/CZToolbox.h>
#import "CZElementLayer.h"
#import "CZElementText.h"
#import "CZCreateElementAction.h"
#import "CZDeleteElementAction.h"

const static CGFloat kDefaultTextViewMargin = 8.0;

#pragma mark - Inner class CZModifyTextElementAction

@interface CZModifyTextElementAction : NSObject <CZUndoAction>

@property (nonatomic, retain) CZElementText *element;
@property (nonatomic, copy) NSString *textMemo;

@end

@implementation CZModifyTextElementAction

- (void)dealloc {
    [_element release];
    [_textMemo release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return [_textMemo length] * 2U;  // UTF16
}

- (void)do {
    [self swapText];
}

- (void)undo {
    [self swapText];
}

- (void)redo {
    [self swapText];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

// Private method
- (void)swapText {
    NSString *tempStr = [_element.string retain];
    [_element notifyWillChange];
    _element.string = self.textMemo;
    [_element notifyDidChange];
    self.textMemo = tempStr;
    [tempStr release];
}

@end

@interface CZElementTextInputTool() {
    CGRect _originalFrame;
}

@property (nonatomic, retain) UITextView *textView;
@property (nonatomic, retain) CZElementText *targetTextElemnt;

@end

@implementation CZElementTextInputTool

- (id)initWithDocManager:(CZElementDoc)docManager {
    self = [super init];
    if (self) {
        if (docManager) {
            _docManager = docManager;
        } else {
            [self release];
            self = nil;
        }
    }
    return self;
}

- (void)dealloc {
    [_textView removeFromSuperview];
    [_textView release];
    [_targetTextElemnt release];
    [super dealloc];
}

- (BOOL)tryEditOnElement:(CZElement *)element {
    CZElementText *textElement = nil;
    if ([element isMemberOfClass:[CZElementText class]]) {
        textElement = (CZElementText *)element;
    }
    
    if (self.imageView == nil ||
        textElement == nil ||
        textElement == self.targetTextElemnt) {
        return NO;
    }
    
    if (self.targetTextElemnt) {
        [self dismissEditing];
    }
    
    self.creating = NO;

    // notify that text element enter into editing mode, it shall hide in some case.
    [textElement notifyWillChange];
    self.targetTextElemnt = textElement;
    [textElement notifyDidChange];
    
    CGRect frame = textElement.frame;
    frame = [CZCommonUtils imageRectToViewRect:frame];
    
    if (self.textView == nil) {
        UITextView *tempTextView = [[UITextView alloc] initWithFrame:frame];
        self.textView = tempTextView;
        tempTextView.backgroundColor = (textElement.fillColor.a == 0) ?
            [UIColor colorWithWhite:1.0 alpha:0.8] :
            [UIColorFromCZColor(textElement.fillColor) colorWithAlphaComponent:0.8];
        tempTextView.delegate = self;
        
        [self.imageView addSubview:tempTextView];
        [tempTextView release];
        
        _originalFrame = CGRectNull;
        [self registerKeyboardNotification];
    }
    
    _textView.text = textElement.string;
    frame = CGRectInset(frame, -kDefaultTextViewMargin, -kDefaultTextViewMargin);
    _textView.frame = frame;
    _textView.textColor = UIColorFromCZColor(textElement.strokeColor);
    _textView.tintColor = _textView.textColor;
    CGSize fontSize = CGSizeMake(textElement.fontSize, textElement.fontSize);
    fontSize = [CZCommonUtils imageSizeToViewSize:fontSize];
    _textView.font = [UIFont fontWithName:textElement.fontFamily size:fontSize.height];
    
    [_textView setNeedsDisplay];
    [_textView becomeFirstResponder];
    [_textView setSelectedRange:NSMakeRange(textElement.string.length, 0)];
    
    return YES;
}

- (BOOL)dismissEditing {
    if (_textView) {
        [_textView resignFirstResponder];
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isEditing {
    return (_textView != nil);
}

#pragma mark - Private methods

- (void)registerKeyboardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterKeyboardNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (UIScrollView *)scrollViewOfImageView {
    UIView *parenetView = self.imageView.superview;
    UIScrollView *scrollView = nil;
    for (int i= 0; i < 2; i++) {  // retry at most twice
        if ([parenetView isKindOfClass:[UIScrollView class]]) {
            scrollView = (UIScrollView *)parenetView;
            break;
        }
        parenetView = parenetView.superview;
    }
    
    return scrollView;
}


- (void)keyboardWasShown:(NSNotification *)aNotification {
    UIScrollView *scrollView = [self scrollViewOfImageView];
    if (scrollView) {
        if (CGRectIsNull(_originalFrame)) {
            _originalFrame = scrollView.frame;
        }
        
        NSDictionary *info = [aNotification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        kbRect = [scrollView.superview convertRect:kbRect fromView:nil];
        CGRect frame = _originalFrame;
        frame.size.height = kbRect.origin.y - frame.origin.y;
        if (frame.size.height > 0) {
            scrollView.frame = frame;
        }
        
        [self scrollTextViewToVisible:scrollView];
    } else {
        if (CGRectIsNull(_originalFrame)) {
            _originalFrame = self.textView.frame;
        }
        
        // Move text view to visible
        NSDictionary *info = [aNotification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        kbRect = [self.textView.superview convertRect:kbRect fromView:nil];
        if (CGRectGetMaxY(_originalFrame) > CGRectGetMinY(kbRect)) {
            CGRect frame = _originalFrame;
            frame.origin.y = kbRect.origin.y - frame.size.height - 4;
            frame.origin.y  = MAX(0, frame.origin.y);
            self.textView.frame = frame;
        }
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIScrollView *scrollView = [self scrollViewOfImageView];
    if (scrollView) {
        scrollView.frame = _originalFrame;
    } else {
        self.textView.frame = _originalFrame;
    }
}

#pragma mark - UITextViewDelegate methods

- (void)textViewDidChange:(UITextView *)textView {
    // reference from http://stackoverflow.com/questions/18966675/uitextview-in-ios7-clips-the-last-line-of-text-string
    CGRect line = [textView caretRectForPosition:textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height -
                    (textView.contentOffset.y + textView.bounds.size.height -
                     textView.contentInset.bottom - textView.contentInset.top);
    if (overflow > 0) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + 7; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:0.2 animations:^ {
            [textView setContentOffset:offset];
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (_textView == nil) {
        return;
    }

    if (self.targetTextElemnt) {
        CZElementText *tempTextElement = [self.targetTextElemnt retain];
        self.targetTextElemnt = nil;
        
        if (self.creating) {
            tempTextElement.string = _textView.text;
            
            if ([tempTextElement.string length] != 0) {
                [self addTextElement:tempTextElement];
                
                if ([self.delegate respondsToSelector:@selector(textInputTool:didEndCreateElement:)]) {
                    [self.delegate textInputTool:self didEndCreateElement:tempTextElement];
                }
                
            } else {
                [tempTextElement removeFromParent];  // Remove empty text element
            }
        } else {
            if (![self.textView.text isEqualToString:tempTextElement.string]) {                    
                if ([self.textView.text length] != 0) {
                    [self modifyTextElement:tempTextElement withText:self.textView.text];
                } else {
                    [self deleteTextElement:tempTextElement];
                }
            } else {
                [tempTextElement notifyDidChange];  // just notify that needs redraw.
            }
        }
        
       [tempTextElement release];
        
        if ([self.delegate respondsToSelector:@selector(textInputToolDidEndEditing:)]) {
            [self.delegate textInputToolDidEndEditing:self];
        }
    } else {
        CZLogv(@"Error!! End editing without text element!!");
    }
    
    [_textView removeFromSuperview];
    [self unregisterKeyboardNotification];
    
    self.textView = nil;
}

#pragma mark - Private methods

- (void)addTextElement:(CZElementText *)tempTextElement {
    CZCreateElementAction *action = [[CZCreateElementAction alloc] init];
    action.element = tempTextElement;
    action.layer = self.docManager.elementLayer;
    [self.docManager pushAction:action];
    [action release];
    
    [tempTextElement notifyDidChange];  // just notify that needs redraw.
}

- (void)deleteTextElement:(CZElementText *)tempTextElement {
    CZDeleteElementAction *action = [[CZDeleteElementAction alloc] initWithElement:tempTextElement];
    [self.docManager pushAction:action];
    [action release];
}

- (void)modifyTextElement:(CZElementText *)tempTextElement withText:(NSString *)text {
    // push action of modify text element
    CZModifyTextElementAction *action = [[CZModifyTextElementAction alloc] init];
    action.element = tempTextElement;
    action.textMemo = text;
    [self.docManager pushAction:action];
    [action release];
}

- (void)scrollTextViewToVisible:(UIScrollView *)scrollView {
    if (!scrollView) {
        return;
    }
    
    CGAffineTransform t = CGAffineTransformMakeScale(scrollView.zoomScale, scrollView.zoomScale);
    
    CGRect rect = self.textView.frame;
    rect = [self.imageView convertRect:rect toView:self.imageView.superview];
    rect = CGRectApplyAffineTransform(rect, t);
    
    [scrollView scrollRectToVisible:rect animated:YES];
}

@end
