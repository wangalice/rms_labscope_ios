//
//  CALayer+ReplaceWithSameName.m
//  Matscope
//
//  Created by Ralph Jin on 1/14/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CALayer+ReplaceWithSameName.h"

@implementation CALayer (ReplaceWithSameName)

- (void)replaceSublayerWithSameNameLayer:(CALayer *)layer {
    [self replaceSublayerWithSameNameLayer:layer bringTop:NO];
}

- (void)replaceSublayerWithSameNameLayer:(CALayer *)layer bringTop:(BOOL)bringTop {
    if (layer == nil) {
        return;
    }
    
    BOOL found = NO;
    NSString *searchName = layer.name;
    for (CALayer *sub in [self sublayers]) {
        if ([sub.name isEqualToString:searchName]) {
            if (sub != layer) {
                [self replaceSublayer:sub with:layer];
            }
            found = YES;
            break;
        }
    }
    
    if (!found) { // if not found, just add at end.
        [self addSublayer:layer];
    } else {
        if (bringTop) {
            CALayer *lastLayer = self.sublayers.lastObject;
            if (lastLayer != layer) {
                [self insertSublayer:layer above:lastLayer];
            }
        }
    }
}

- (void)removeSubLayerWithName:(NSString *)name {
    CALayer *foundLayer = nil;
    for (CALayer *sub in [self sublayers]) {
        if ([sub.name isEqualToString:name]) {
            foundLayer = sub;
            break;
        }
    }
    [foundLayer removeFromSuperlayer];
}

@end
