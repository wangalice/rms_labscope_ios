//
//  CZReshapeElementAction.h
//  Matscope
//
//  Created by Ralph Jin on 11/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>

@class CZElement;

@interface CZReshapeElementAction : NSObject<CZUndoAction>

@property (nonatomic, retain) NSDictionary *shapeMemo;
@property (nonatomic, retain) CZElement *element;

@end
