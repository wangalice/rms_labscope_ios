//
//  CZScaleBarSetCornerAction.m
//  Hermes
//
//  Created by Ralph Jin on 5/3/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZScaleBarSetCornerAction.h"

@interface CZScaleBarSetCornerAction() {
    CZScaleBarPosition _pos;
}

@property (nonatomic, retain) CZElementScaleBar *scaleBar;

@end

@implementation CZScaleBarSetCornerAction

- (id)initWithScaleBar:(CZElementScaleBar *)scaleBar newPosition:(CZScaleBarPosition)pos {
    self = [super init];
    if (self) {
        self.scaleBar = scaleBar;
        _pos = pos;
    }
    return self;
}

- (void)dealloc {
    [_scaleBar release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 4U;
}

- (void)do {
    [self toggle];
}

- (void)undo {
    [self toggle];
}

- (void)redo {
    [self toggle];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)toggle {
    CZScaleBarPosition pos = _scaleBar.position;
    [_scaleBar notifyWillChange];
    _scaleBar.position = _pos;
    [_scaleBar notifyDidChange];
    _pos = pos;
}

@end
