//
//  CZBoundaryHandler.m
//  Hermes
//
//  Created by Ralph Jin on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZBoundaryHandler.h"

#import <QuartzCore/QuartzCore.h>
#import "CZElementLayer.h"
#import "CZAnnotationHandlerBoundary.h"
#import <CZToolbox/CZToolbox.h>

const static CZColor kCZHandlerColorOrange = {247, 150, 70, 255};

@interface CZBoundaryHandler()

/*! check if annotation handler layer is exist, otherwise create it.*/
- (void)checkAndCreateHandlerLayer;

@property (nonatomic, retain, readonly) CALayer *layer;
@property (nonatomic, retain) CZAnnotationHandlerBoundary *boundary;

@end

@implementation CZBoundaryHandler

- (void)dealloc {
    [_layer release];
    [_boundary release];
    [super dealloc];
}

- (void)checkAndCreateHandlerLayer {
    if (_layer == nil) {  // Create a new layer and it's sub layers.
        _layer = [[CALayer alloc] init];
        _layer.name = CZAnnotationHandlerLayerName;
        _layer.frame = self.target.parent.frame;
        
        // Add boundary layer
        CZAnnotationHandlerBoundary *temp = [[CZAnnotationHandlerBoundary alloc] initWithParent:self];
        temp.color = kCZHandlerColorOrange;
        self.boundary = temp;
        [temp release];
        
        CAShapeLayer *boundaryLayer = [_boundary newLayer];
        [_layer addSublayer:boundaryLayer];
        [boundaryLayer release];
    }
}

#pragma mark -
#pragma mark override CZAnnotationHandler

- (void)updateWithZoom:(CGFloat)zoom {
    [super updateWithZoom:zoom];
    
    if (self.target == nil) {
        [_layer setHidden:YES];
        return;
    }
    
    [self checkAndCreateHandlerLayer];

    // update boundary layer
    CGRect targetRectInView = [CZCommonUtils imageRectToViewRect:self.target.boundary];
    CAShapeLayer *boundaryLayer = (CAShapeLayer *)[[_layer sublayers] objectAtIndex:0];
    _boundary.rect = targetRectInView;
    [_boundary updateLayer:boundaryLayer];
    
    [_layer setHidden:NO];
}

- (CALayer *)representLayer {
    return self.layer;
}

@end

