//
//  CZRemoveGroupElementAction.m
//  Matscope
//
//  Created by Halley Gu on 3/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZRemoveGroupElementAction.h"
#import "CZElement.h"
#import "CZElementLayer.h"
#import "CZElementGroup.h"

@interface CZRemoveGroupElementAction ()

@property (nonatomic, retain) NSMutableArray *elements;

@end

@implementation CZRemoveGroupElementAction

- (id)initWithGroup:(CZElementGroup *)group {
    if (group.parentLayer == nil || group.elementCount < 2) {
        [group removeFromParentLayer];
        [self release];
        return nil;
    }
    
    self = [super init];
    if (self) {
        _elements = [[NSMutableArray alloc] initWithCapacity:[group elementCount]];
        
        for (CZElement *element in group) {
            [_elements addObject:element];
        }
    }
    
    return self;
}

- (void)dealloc {
    [_elements release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return 128U;
}

- (void)do {
    [self redo];
}

- (void)undo {
    CZElementLayer *layer = ((CZElement *)self.elements[0]).parent;
    NSAssert(layer, @"group's parent is nil");
    
    CZElementGroup *group = [[CZElementGroup alloc] init];
    [layer addGroup:group];
    
    for (CZElement *element in self.elements) {
        [group addElement:element];
    }
    
    [group release];
}

- (void)redo {
    CZElement *element = [self.elements firstObject];
    if (element) {
        CZElementGroup *group = element.parentGroup;
        if (group) {
            [group removeFromParentLayer];
        }
    }
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end
