//
//  CZRectangleHandler.m
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZRectangleHandler.h"
#import <QuartzCore/QuartzCore.h>
#import "CZNodeGeom.h"
#import "CZNodeLine.h"
#import "CZElementLayer.h"
#import "CZElementRectangle.h"
#import "CZElementText.h"
#import "CZRectLikeObject.h"
#import "CZRectElementAdaptor.h"
#import "CZTextElementAdaptor.h"
#import "CZAnnotationHandlerKnob.h"
#import "CZAnnotationHandlerBoundary.h"
#import <CZToolbox/CZToolbox.h>

const static CZColor kCZHandlerColorOrange = {247, 150, 70, 255};

const static float kMinRectangleWidth = 8.0f;
const static float kMinRectangleHeight = 8.0f;
const static float kRotateKnobDistance = 30.0f;

enum {
    kResizeTopLeftKnobID = 0,
    kResizeTopRightKnobID,
    kResizeBottomLeftKnobID,
    kResizeBottomRightKnobID,
    kRotateKnobID,
    kRemoveAllID,
    
    kRectangleHandleKnobCount
};

@interface CZRectangleHandler()

/*! check if annotation handler layer is exist, otherwise create it.*/
- (void)checkAndCreateHandlerLayer;

@property (nonatomic, retain, readonly) CALayer *layer;
@property (nonatomic, retain) NSArray *knobs;
@property (nonatomic, retain) CZAnnotationHandlerBoundary *boundary;
@property (nonatomic, assign) CGPoint rotateStartPoint;
@property (nonatomic, assign) CGFloat originalRotateAngle;
@property (nonatomic, retain) id<CZRectLikeObject> rectAdaptor;

@end

@implementation CZRectangleHandler

- (void)dealloc {
    [_layer release];
    [_knobs release];
    [_boundary release];
    [_rectAdaptor release];
    [super dealloc];
}

- (void)checkAndCreateHandlerLayer {
    if (_layer == nil) {  // Create a new layer and it's sub layers.
        CZAnnotationHandlerKnob *resizeTopLeft = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        resizeTopLeft.part = CZAnnotationHandlerPartResizeTopLeft;
        
        CZAnnotationHandlerKnob *resizeTopRight = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        resizeTopRight.part = CZAnnotationHandlerPartResizeTopRight;
        
        CZAnnotationHandlerKnob *resizeBottomLeft = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        resizeBottomLeft.part = CZAnnotationHandlerPartResizeBottomLeft;
        
        CZAnnotationHandlerKnob *resizeBottomRight = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        resizeBottomRight.part = CZAnnotationHandlerPartResizeBottomRight;
        
        CZAnnotationHandlerKnob *rotateKnob = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        rotateKnob.part = CZAnnotationHandlerPartRotate;
        
        CZAnnotationHandlerKnob *removeAllKnob = [[CZAnnotationHandlerKnob alloc] initWithParent:self];
        removeAllKnob.part = CZAnnotationHandlerPartRemoveAll;
        
        NSArray *array = [[NSArray alloc] initWithObjects:resizeTopLeft, resizeTopRight, resizeBottomLeft, resizeBottomRight, rotateKnob, removeAllKnob, nil];
        self.knobs = array;
        [array release];
        
        [resizeTopLeft release];
        [resizeTopRight release];
        [resizeBottomLeft release];
        [resizeBottomRight release];
        [rotateKnob release];
        [removeAllKnob release];
        
        _layer = [[CALayer alloc] init];
        _layer.name = CZAnnotationHandlerLayerName;
        _layer.frame = self.target.parent.frame;
        
        for (CZAnnotationHandlerKnob *knob in self.knobs) {
            CALayer *layer = [knob newConnectLayer];
            [_layer addSublayer:layer];
            [layer release];
            
            layer = [knob newKnobLayer];
            [_layer addSublayer:layer];
            [layer release];
        }
        
        // Add boundary layer
        if ([_rectAdaptor showsBoundary]) {
            CZAnnotationHandlerBoundary *temp = [[CZAnnotationHandlerBoundary alloc] initWithParent:self];
            temp.color = self.target ? self.target.strokeColor : kCZHandlerColorOrange;
            self.boundary = temp;
            [temp release];
            
            CAShapeLayer *boundaryLayer = [_boundary newLayer];
            [_layer addSublayer:boundaryLayer];
            [boundaryLayer release];
        }
    }
}

- (CGPoint)calcOppositePointOfPart:(CZAnnotationHandlerPart)part {
    CGRect oldRect = self.rectAdaptor.rect;
    CGPoint oppositePoint = CGPointZero;
    
    if (part == CZAnnotationHandlerPartResizeTopLeft) {
        oppositePoint.x = CGRectGetMaxX(oldRect);
        oppositePoint.y = CGRectGetMaxY(oldRect);
    } else if (part == CZAnnotationHandlerPartResizeTopRight) {
        oppositePoint.x = CGRectGetMinX(oldRect);
        oppositePoint.y = CGRectGetMaxY(oldRect);
    } else if (part == CZAnnotationHandlerPartResizeBottomLeft) {
        oppositePoint.x = CGRectGetMaxX(oldRect);
        oppositePoint.y = CGRectGetMinY(oldRect);
    } else if (part == CZAnnotationHandlerPartResizeBottomRight) {
        oppositePoint.x = CGRectGetMinX(oldRect);
        oppositePoint.y = CGRectGetMinY(oldRect);
    }
    return oppositePoint;
}

#pragma mark -
#pragma mark override CZAnnotationHandler

- (void)updateWithZoom:(CGFloat)zoom {
    [super updateWithZoom:zoom];
    
    if (self.target == nil) {
        [_layer setHidden:YES];
        return;
    }

    [self checkAndCreateHandlerLayer];
 
    CGRect targetRectInView = [CZCommonUtils imageRectToViewRect:self.rectAdaptor.rect];
    CGPoint rotateCenter = CGPointMake(CGRectGetMidX(targetRectInView), CGRectGetMidY(targetRectInView));
    CGAffineTransform rotateTransform = CGAffineTransformMakeRotationAtPoint(self.rectAdaptor.rotateAngle, rotateCenter);

    // update resize knob (top left)
    CGPoint connectPoint = [self positionOfPart:CZAnnotationHandlerPartResizeTopLeft];
    CZAnnotationHandlerKnob *knob = [self.knobs objectAtIndex:kResizeTopLeftKnobID];
    knob.connectPoint = connectPoint;
    knob.knobPoint = knob.connectPoint;
    
    // update resize knob (top right)
    connectPoint = [self positionOfPart:CZAnnotationHandlerPartResizeTopRight];
    knob = [self.knobs objectAtIndex:kResizeTopRightKnobID];
    knob.connectPoint =connectPoint;
    knob.knobPoint = knob.connectPoint;

    // update vert resize knob (bottom left)
    connectPoint = [self positionOfPart:CZAnnotationHandlerPartResizeBottomLeft];
    knob = [self.knobs objectAtIndex:kResizeBottomLeftKnobID];
    knob.connectPoint = connectPoint;
    knob.knobPoint = knob.connectPoint;
    
    // update vert resize knob (bottom right)
    connectPoint = [self positionOfPart:CZAnnotationHandlerPartResizeBottomRight];
    knob = [self.knobs objectAtIndex:kResizeBottomRightKnobID];
    knob.connectPoint = connectPoint;
    knob.knobPoint = knob.connectPoint;
    
    // update rotate knob
    const CGFloat distance = kRotateKnobDistance / knob.zoomOfView;
    connectPoint.x = CGRectGetMidX(targetRectInView);
    connectPoint.y = CGRectGetMinY(targetRectInView);
    CGPoint knobCenter;
    knobCenter.x = connectPoint.x;
    knobCenter.y = connectPoint.y - distance;
    
    knob = [self.knobs objectAtIndex:kRotateKnobID];
    knob.connectPoint = CGPointApplyAffineTransform(connectPoint, rotateTransform);
    knob.knobPoint = CGPointApplyAffineTransform(knobCenter, rotateTransform);
    
    // update remove all knob
    connectPoint.x = CGRectGetMinX(targetRectInView);
    connectPoint.y = CGRectGetMidY(targetRectInView);
    knobCenter.x = connectPoint.x - distance;
    knobCenter.y = connectPoint.y;
    
    knob = [self.knobs objectAtIndex:kRemoveAllID];
    knob.connectPoint = CGPointApplyAffineTransform(connectPoint, rotateTransform);
    knob.knobPoint = CGPointApplyAffineTransform(knobCenter, rotateTransform);
    
    // udpate CALayers
    CGColorRef strokeColor = CGColorFromCZColor(self.target ? self.target.strokeColor : kCZColorRed);
    
    for (NSUInteger i = 0; i < kRectangleHandleKnobCount; ++i) {
        CZAnnotationHandlerKnob *knob = [self.knobs objectAtIndex:i];
        
        CAShapeLayer *lineLayer = (CAShapeLayer *)[[_layer sublayers] objectAtIndex:(i * 2)];
        lineLayer.strokeColor = strokeColor;
        [knob updateConnectLayer:lineLayer];
        
        CAShapeLayer *knobLayer = (CAShapeLayer *)[[_layer sublayers] objectAtIndex:(i * 2 + 1)];
        [knob updateKnobLayer:knobLayer rotateAngle:self.target.rotateAngle];
        
        if (i == kRemoveAllID) {
            lineLayer.hidden = !self.isRemoveKnobShown;
            knobLayer.hidden = !self.isRemoveKnobShown;
            [lineLayer removeAllAnimations];
            [knobLayer removeAllAnimations];
        }
    }
    
    // update boundary layer
    if ([_rectAdaptor showsBoundary]) {
        CAShapeLayer *boundaryLayer = (CAShapeLayer *)[[_layer sublayers] objectAtIndex:(kRectangleHandleKnobCount * 2)];
        _boundary.rect = targetRectInView;
        _boundary.rotateAngle = _rectAdaptor.rotateAngle;
        _boundary.color = self.target ? self.target.strokeColor : kCZHandlerColorOrange;
        [_boundary updateLayer:boundaryLayer];
    }

    [_layer setHidden:NO];
}

- (CGPoint)positionOfPart:(CZAnnotationHandlerPart)part {
    CGRect targetRectInView = [CZCommonUtils imageRectToViewRect:self.rectAdaptor.rect];

    CGPoint connectPoint;
    if (part == CZAnnotationHandlerPartResizeBottomRight) {
        connectPoint.x = CGRectGetMaxX(targetRectInView);
        connectPoint.y = CGRectGetMaxY(targetRectInView);
    } else if (part == CZAnnotationHandlerPartResizeTopLeft) {
        connectPoint.x = CGRectGetMinX(targetRectInView);
        connectPoint.y = CGRectGetMinY(targetRectInView);
    } else if (part == CZAnnotationHandlerPartResizeTopRight) {
        connectPoint.x = CGRectGetMaxX(targetRectInView);
        connectPoint.y = CGRectGetMinY(targetRectInView);
    } else if (part == CZAnnotationHandlerPartResizeBottomLeft) {
        connectPoint.x = CGRectGetMinX(targetRectInView);
        connectPoint.y = CGRectGetMaxY(targetRectInView);
    } else if (part == CZAnnotationHandlerPartRotate) {
        CZAnnotationHandlerKnob *knob = [self.knobs objectAtIndex:kRotateKnobID];
        CGFloat distance = kRotateKnobDistance / knob.zoomOfView;
        connectPoint.x = CGRectGetMidX(targetRectInView);
        connectPoint.y = CGRectGetMinY(targetRectInView) - distance;
    } else {
        return CGPointZero;  // unknown part
    }
    
    CGPoint rotateCenter = CGPointMake(CGRectGetMidX(targetRectInView), CGRectGetMidY(targetRectInView));
    CGAffineTransform rotateTransform = CGAffineTransformMakeRotationAtPoint(self.rectAdaptor.rotateAngle, rotateCenter);
    connectPoint = CGPointApplyAffineTransform(connectPoint, rotateTransform);
    
    return connectPoint;
}

- (CGPoint)beginDragOnPart:(CZAnnotationHandlerPart)part
                atPoint:(CGPoint)pointInView {
    if (part == CZAnnotationHandlerPartRotate) {
        _rotateStartPoint = [CZCommonUtils viewPointToImagePoint:pointInView];
        _originalRotateAngle = self.rectAdaptor.rotateAngle;
        return CGPointMake(NAN, NAN);
    } else {
        return pointInView;
    }
}

- (CGPoint)dragOnPart:(CZAnnotationHandlerPart)part
           toPoint:(CGPoint)pointInView {
    CGPoint pointInImage = [CZCommonUtils viewPointToImagePoint:pointInView];
    CGRect oldRect = self.rectAdaptor.rect;
    CGRect newRect = oldRect;

    // Handle rotating part
    if (part == CZAnnotationHandlerPartRotate) {
        CGPoint rotateCenter = self.rectAdaptor.anchorPoint;
        CGFloat rotateAngle = CZCalcRotateAngle(rotateCenter, _rotateStartPoint, pointInImage);
        CGFloat newRotateAngle = rotateAngle + _originalRotateAngle;
        self.rectAdaptor.rotateAngle = newRotateAngle;
        return [super dragOnPart:part toPoint:pointInImage];
    }

    // Convert point from image coordinate to element relative coordinate
    CGPoint pointInImageRelative;
    CGAffineTransform rotateTransform;
    
    BOOL isTargetRotated = self.rectAdaptor.rotateAngle != 0;
    if (isTargetRotated) {
        CGPoint rotateCenter = self.rectAdaptor.anchorPoint;
        rotateTransform = CGAffineTransformMakeRotationAtPoint(self.rectAdaptor.rotateAngle, rotateCenter);
        
        // invert apply transform
        CGAffineTransform invertTransform = CGAffineTransformInvert(rotateTransform);
        pointInImageRelative = CGPointApplyAffineTransform(pointInImage, invertTransform);
    } else {
        pointInImageRelative = pointInImage;
    }

    CGFloat ratio = (newRect.size.width == 0.0) ? 1.0 : (newRect.size.height / newRect.size.width);

    // Calculate new rectangle size
    CGPoint oppositePoint = [self calcOppositePointOfPart:part];    
    switch (part) {
        case CZAnnotationHandlerPartResizeBottomRight: {
            newRect.size.width = pointInImageRelative.x - oppositePoint.x;
            newRect.size.height = pointInImageRelative.y - oppositePoint.y;
            break;
        }
        case CZAnnotationHandlerPartResizeTopLeft: {
            newRect.size.width = - pointInImageRelative.x + oppositePoint.x;
            newRect.size.height = - pointInImageRelative.y + oppositePoint.y;
            break;
        }
        case CZAnnotationHandlerPartResizeTopRight: {
            newRect.size.width = pointInImageRelative.x - oppositePoint.x;
            newRect.size.height = - pointInImageRelative.y + oppositePoint.y;
            break;
        }
        case CZAnnotationHandlerPartResizeBottomLeft: {
            newRect.size.width = - pointInImageRelative.x + oppositePoint.x;
            newRect.size.height = pointInImageRelative.y - oppositePoint.y;
            break;
        }
        default:
            break;
    }
    
    newRect.size.width = MAX(newRect.size.width, kMinRectangleWidth);
    newRect.size.height = MAX(newRect.size.height, kMinRectangleHeight);

    // Aspect resize correction
    if (self.target.keepAspect) {
        CGFloat newRatio = (newRect.size.width == 0.0) ? 1.0 : (newRect.size.height / newRect.size.width);
        if (newRatio > ratio) {
            newRect.size.width = newRect.size.height / ratio;
        } else {
            newRect.size.height = newRect.size.width * ratio;
        }
    }

    // Offset rectangle element according to resize corner
    switch (part) {
        case CZAnnotationHandlerPartResizeBottomRight: {
            break;
        }
        case CZAnnotationHandlerPartResizeTopLeft: {
            newRect.origin.x = oppositePoint.x - newRect.size.width;
            newRect.origin.y = oppositePoint.y - newRect.size.height;
            break;
        }
        case CZAnnotationHandlerPartResizeTopRight: {
            newRect.origin.y = oppositePoint.y - newRect.size.height;
            break;
        }
        case CZAnnotationHandlerPartResizeBottomLeft: {
            newRect.origin.x = oppositePoint.x - newRect.size.width;
            break;
        }
        default:
            break;
    }

    // Rotate offset correction
    if (isTargetRotated) {  
        CGPoint newCenterRelative;
        newCenterRelative.x = CGRectGetMidX(newRect);
        newCenterRelative.y = CGRectGetMidY(newRect);
        CGPoint realCenter = CGPointApplyAffineTransform(newCenterRelative, rotateTransform);

        CGSize offset;
        offset.width = realCenter.x - newCenterRelative.x;
        offset.height = realCenter.y - newCenterRelative.y;

        newRect.origin.x += offset.width;
        newRect.origin.y += offset.height;
    }

    self.rectAdaptor.rect = newRect;
    
    return [self positionOfPart:part];
}

- (void)setTarget:(CZElement *)target {
    if ([target isMemberOfClass:[CZElementRectangle class]]) {
        CZElementRectangle *elementRect = (CZElementRectangle *)target;

        [super setTarget:elementRect];
        CZRectElementAdaptor *adaptor = [[CZRectElementAdaptor alloc] initWithElement:elementRect];
        self.rectAdaptor = adaptor;
        [adaptor release];
        
    } else if ([target isMemberOfClass:[CZElementText class]]) {
        CZElementText *elementText = (CZElementText *)target;
    
        [super setTarget:elementText];
        CZTextElementAdaptor *adaptor = [[CZTextElementAdaptor alloc] initWithElement:elementText];
        self.rectAdaptor = adaptor;
        [adaptor release];
    } else {
        [super setTarget:nil];
    }
}

- (CALayer *)representLayer {
    return self.layer;
}

@end
