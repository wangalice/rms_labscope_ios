//
//  CZRemoveGroupElementAction.h
//  Matscope
//
//  Created by Halley Gu on 3/3/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>

@class CZElementGroup;

@interface CZRemoveGroupElementAction : NSObject<CZUndoAction>

- (id)initWithGroup:(CZElementGroup *)group;

@end
