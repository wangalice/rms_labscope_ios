//
//  CZRectangleHandler.h
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZAnnotationHandler.h"

@interface CZRectangleHandler : CZAnnotationHandler

@end
