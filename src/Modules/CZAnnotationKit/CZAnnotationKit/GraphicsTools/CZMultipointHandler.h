//
//  CZMultipointHandler.h
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationHandler.h"
#import "CZMultipointObject.h"

@interface CZMultipointHandler : CZAnnotationHandler

@property (nonatomic, assign) BOOL showConnectLine;

/** NO, if handler is in multi-tap creating mode; YES in normal editing mode. */
@property (nonatomic, assign, getter = isMultiTapDone) BOOL multiTapDone;
@property (nonatomic, assign, getter = isDoneButtonEnabled) BOOL doneButtonEnabled;
@property (nonatomic, assign, getter = isEditingPoint) BOOL editingPoint;
@property (nonatomic, retain) id<CZMultipointObject> multipointObject;

@end
