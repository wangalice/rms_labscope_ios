//
//  CZAnnotationHandlerKnob.h
//  Hermes
//
//  Created by Ralph Jin on 2/18/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZColor.h"
#import "CZAnnotationHandler.h"

extern const CGFloat kCZAnnotationHandlerKnobSize;

@interface CZAnnotationHandlerKnob : NSObject

@property (nonatomic, assign) CGPoint knobPoint;
@property (nonatomic, assign) CGPoint connectPoint;
@property (nonatomic, assign) CZAnnotationHandlerPart part;
@property (nonatomic, readonly) CGFloat zoomOfView;
@property (nonatomic, assign, readonly) CZAnnotationHandler *parent;

- (id)initWithParent:(CZAnnotationHandler *)parent;

- (CALayer *)newKnobLayer;
- (CALayer *)newConnectLayer;

- (void)updateKnobLayer:(CALayer *)layer rotateAngle:(CGFloat)angle;
- (void)updateConnectLayer:(CALayer *)layer;

@end
