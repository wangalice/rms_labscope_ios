//
//  CZDragHandlerTool.h
//  Hermes
//
//  Created by Ralph Jin on 2/5/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAbstractGraphicsTool.h"


@class CZAnnotationHandler;

/*! CZDragHandleTool
 This tool regards point in view's coordinate.
 */

@interface CZDragHandlerTool : CZAbstractGraphicsTool

@property (nonatomic, retain) CZAnnotationHandler *toolHandler;
@property (nonatomic, assign) id<CZMagnifyDelegate> magnifyDelegate;

@end
