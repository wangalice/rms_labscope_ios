//
//  CZChangeElementSizeAction.h
//  Hermes
//
//  Created by Ralph Jin on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZElement.h"

@interface CZChangeElementSizeAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element newSize:(CZElementSize)size;

@end
