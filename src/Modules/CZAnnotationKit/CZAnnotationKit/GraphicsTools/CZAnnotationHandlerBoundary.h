//
//  CZAnnotationHandlerBoundary.h
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "CZColor.h"

@class CZAnnotationHandler;

@interface CZAnnotationHandlerBoundary : NSObject

@property (nonatomic, assign) CZColor color;
@property (nonatomic, assign) CGRect rect;
@property (nonatomic, assign) CGFloat rotateAngle;
@property (nonatomic, assign, readonly) CGFloat zoomOfView;
@property (nonatomic, assign, readonly) CZAnnotationHandler *parent;

- (id)initWithParent:(CZAnnotationHandler *)parent;

- (CAShapeLayer *)newLayer;

- (void)updateLayer:(CAShapeLayer *)layer;

@end
