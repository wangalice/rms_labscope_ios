//
//  CZRectLikeObject.h
//  Hermes
//
//  Created by Ralph Jin on 3/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@protocol CZRectLikeObject <NSObject>

- (CGRect)rect;

- (void)setRect:(CGRect)rect;

- (CGFloat)rotateAngle;

- (void)setRotateAngle:(CGFloat)rotateAngle;

- (CGPoint)anchorPoint;

- (BOOL)showsBoundary;

@end
