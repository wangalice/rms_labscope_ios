//
//  CZMultiTapCreateTool.h
//  Hermes
//
//  Created by Ralph Jin on 4/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZAbstractGraphicsTool.h"

@class CZElement;
@protocol CZMultiTapCreateToolDelegate;

/** multi-tap create tool, eg. create polygon, spline etc.*/
@interface CZMultiTapCreateTool : CZAbstractGraphicsTool

@property (nonatomic, assign, readonly) CGFloat zoomOfView;
@property (nonatomic, assign) id<CZMultiTapCreateToolDelegate> delegate;
@property (nonatomic, assign) Class elementClass;
@property (nonatomic, retain, readonly) CZElement *elementBeingCreated;

- (id)initWithDocManager:(CZElementDoc)docManager view:(UIView *)view;

- (BOOL)canFinishCreating;

- (void)cancelCreation;

- (void)updateWithZoom:(CGFloat)zoomOfView;

@end


@protocol CZMultiTapCreateToolDelegate <NSObject>
@optional

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didDragPoint:(CGPoint)pointOfImage;

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didCreateElement:(CZElement *)element;

- (void)multiTapCreateTool:(CZMultiTapCreateTool *)tool didAddPointAtCount:(NSUInteger)count;

- (void)multiTapCreateToolDidCancelled:(CZMultiTapCreateTool *)tool;

@end
