//
//  CZAnnotationGestureRecognizer.h
//  Matscope
//
//  Created by Ralph Jin on 11/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZAbstractGraphicsTool.h"

@protocol CZAnnotationGestureRecognizerDelegate <UIGestureRecognizerDelegate>
@optional

- (CZShouldBeginTouchState)annotationGetstureShouldBeginTouch:(UITouch *)touch;

@end

@interface CZAnnotationGestureRecognizer : UIGestureRecognizer

@property(nonatomic, assign) id <CZAnnotationGestureRecognizerDelegate> annotationDelegate;
@property(nonatomic, assign, getter = isMultiTouchEnable) BOOL multiTouchEnable;

@end
