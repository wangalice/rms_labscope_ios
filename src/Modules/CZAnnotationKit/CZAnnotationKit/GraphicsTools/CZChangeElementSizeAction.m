//
//  CZChangeElementSizeAction.m
//  Hermes
//
//  Created by Ralph Jin on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZChangeElementSizeAction.h"

@interface CZChangeElementSizeAction()
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CZElementSize size;
@end

@implementation CZChangeElementSizeAction

- (id)initWithElement:(CZElement *)element newSize:(CZElementSize)size {
    self = [super init];
    if (self) {
        self.element = element;
        self.size = size;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return sizeof(CZElementSize);
}

- (void)do {
    [self swapSize];
}

- (void)undo {
    [self swapSize];
}

- (void)redo {
    [self swapSize];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)swapSize {
    [_element notifyWillChange];
    CZElementSize tempSize = _element.size;
    _element.size = _size;
    _size = tempSize;
    [_element notifyDidChange];
}

@end
