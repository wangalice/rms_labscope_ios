//
//  CZAnnotationHandlerBoundary.m
//  Hermes
//
//  Created by Ralph Jin on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationHandlerBoundary.h"
#import "CZAnnotationHandler.h"

@implementation CZAnnotationHandlerBoundary

- (id)initWithParent:(CZAnnotationHandler *)parent {
    self = [super init];
    if (self) {
        _parent = parent;
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (CGFloat)zoomOfView {
    return _parent.zoomOfView;
}

- (CAShapeLayer *)newLayer {
    CAShapeLayer *layer = [[CAShapeLayer alloc]init];
    layer.fillColor = [[UIColor clearColor] CGColor];
    layer.strokeColor = CGColorFromCZColor(_color);
    return layer;
}

- (void)updateLayer:(CAShapeLayer *)layer {
    // set path, frame and bounds
    layer.frame = _rect;
    CGRect bounds = _rect;
    bounds.origin = CGPointZero;
    layer.bounds = bounds;
    CGPathRef path = CGPathCreateWithRect(bounds, NULL);
    layer.path = path;
    CGPathRelease(path);
    
    // set line dash pattern
    NSNumber *dotSize = [[NSNumber alloc] initWithFloat:(2.0f / self.zoomOfView)];
    NSArray *dashPattern = [[NSArray alloc] initWithObjects:dotSize, dotSize, nil];
    [dotSize release];
    layer.lineDashPattern = dashPattern;
    layer.lineWidth = 1.0f / self.zoomOfView;
    [dashPattern release];

    // set rotate angle
    if (_rotateAngle != 0) {
        layer.transform = CATransform3DMakeRotation(_rotateAngle, 0.0, 0.0, 1.0);
    } else {
        layer.transform = CATransform3DIdentity;
    }
    
    layer.strokeColor = CGColorFromCZColor(_color);
    
    [layer removeAllAnimations];
}

@end
