//
//  CZTouchCreateTool.h
//  Hermes
//
//  Created by Ralph Jin on 1/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "CZAbstractGraphicsTool.h"

@class CZElement;
@protocol CZTouchCreateDelegate;

/*! The tool class for create element. */
@interface CZTouchCreateTool : NSObject

@property (nonatomic, assign) id<CZTouchCreateDelegate> delegate;
@property (nonatomic, assign) Class elementClass;

- (id)initWithDocManager:(CZElementDoc)docManager;

- (CZElement *)createElementInRect:(CGRect)rect horizontal:(BOOL)horizontal;

@end

@protocol CZTouchCreateDelegate <NSObject>
@optional
- (void)touchCreateTool:(CZTouchCreateTool *)tool didCreateElement:(CZElement *)element;

@end
