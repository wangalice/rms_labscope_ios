//
//  CZCreateElementAction.h
//  Hermes
//
//  Created by Ralph Jin on 3/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZToolbox/CZToolbox.h>

@class CZElement;
@class CZElementLayer;

@interface CZCreateElementAction : NSObject <CZUndoAction>

@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CZElementLayer *layer;

@end
