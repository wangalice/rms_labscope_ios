//
//  CZChangeElementColorAction.m
//  Hermes
//
//  Created by Ralph Jin on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZChangeElementColorAction.h"
#import "CZElement.h"

@interface CZChangeElementColorAction()
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CZColor stokeColor;
@end

@implementation CZChangeElementColorAction

- (id)initWithElement:(CZElement *)element newColor:(CZColor)color {
    self = [super init];
    if (self) {
        self.element = element;
        self.stokeColor = color;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return sizeof(CZColor);
}

- (void)do {
    [self swapColor];
}

- (void)undo {
    [self swapColor];
}

- (void)redo {
    [self swapColor];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)swapColor {
    [_element notifyWillChange];
    CZColor tempStokeColor = _element.strokeColor;
    _element.strokeColor = _stokeColor;
    _stokeColor = tempStokeColor;
    [_element notifyDidChange];
}

@end

@interface CZChangeElementFillColorAction()
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CZColor fillColor;
@end

@implementation CZChangeElementFillColorAction

- (id)initWithElement:(CZElement *)element newStrokeColor:(CZColor)strokeColor newFillColor:(CZColor)fillColor {
    self = [super init];
    if (self) {
        self.element = element;
        self.fillColor = fillColor;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return sizeof(CZColor);
}

- (void)do {
    [self swapColor];
}

- (void)undo {
    [self swapColor];
}

- (void)redo {
    [self swapColor];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)swapColor {
    [_element notifyWillChange];
    CZColor tempFillColor = _element.fillColor;
    _element.fillColor = _fillColor;
    _fillColor = tempFillColor;
    [_element notifyDidChange];
}

@end

@interface CZChangeMeasurementBackgroundAction()
@property (nonatomic, retain) CZElement *element;
@property (nonatomic, assign) CZColor measurementBackgroundColor;
@end


@implementation CZChangeMeasurementBackgroundAction

- (id)initWithElement:(CZElement *)element newMeasurementBackgroundColor:(CZColor)measurementBackgroundColor {
    self = [super init];
    if (self) {
        self.element = element;
        self.measurementBackgroundColor = measurementBackgroundColor;
    }
    return self;
}

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return sizeof(CZColor);
}

- (void)do {
    [self swapColor];
}

- (void)undo {
    [self swapColor];
}

- (void)redo {
    [self swapColor];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

- (void)swapColor {
    [_element notifyWillChange];
    CZColor tempMeasurementBackgroundColor = _element.measurementBackgroundColor;
    _element.measurementBackgroundColor = _measurementBackgroundColor;
    _measurementBackgroundColor = tempMeasurementBackgroundColor;
    [_element notifyDidChange];
}

@end
