//
//  CZDragTool.h
//  Hermes
//
//  Created by Ralph Jin on 1/29/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZAbstractGraphicsTool.h"

@class CZDocManager;
@class CZAnnotationHandler;
@class CZElement;
@class CZSelectTool;
@class CZDragTool;

@protocol CZDragToolDelegate <NSObject>
@optional
- (BOOL)shouldDragTool:(CZDragTool *)dragTool editingElement:(CZElement *)element;
@end

/*! CZDragTool
    This tool treats point in image's coordinate.
 */
@interface CZDragTool : CZAbstractGraphicsTool

@property (nonatomic, assign) id<CZDragToolDelegate> delegate;

@property (nonatomic, retain, readonly) CZElement *element;
@property (nonatomic, assign) CGFloat zoomOfImage;

@end
