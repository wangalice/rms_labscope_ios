//
//  CZToggleMeasurementAction.h
//  Hermes
//
//  Created by Ralph Jin on 3/26/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CZToolbox/CZToolbox.h>
#import "CZElement.h"

@interface CZToggleMeasurementAction : NSObject<CZUndoAction>

- (id)initWithElement:(CZElement *)element;

@end
