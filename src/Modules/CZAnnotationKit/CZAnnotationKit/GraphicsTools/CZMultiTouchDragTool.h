//
//  CZDragLineTool.h
//  Matscope
//
//  Created by Ralph Jin on 11/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAbstractGraphicsTool.h"

@class CZElement;
@protocol CZMagnifyDelegate;

/*! Drag line tool class, with which user can use two fingers to drag the line at once.*/
@interface CZMultiTouchDragTool : CZAbstractGraphicsTool

@property (nonatomic, assign) CZElement *target;
@property (nonatomic, assign) id<CZMagnifyDelegate> magnifyDelegate;

@end
