//
//  CZBoundaryHandler.h
//  Hermes
//
//  Created by Ralph Jin on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZAnnotationHandler.h"

@interface CZBoundaryHandler : CZAnnotationHandler

@end
