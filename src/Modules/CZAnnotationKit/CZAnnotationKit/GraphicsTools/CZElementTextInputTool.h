//
//  CZElementTextInputTool.h
//  Hermes
//
//  Created by Ralph Jin on 3/11/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CZAbstractGraphicsTool.h"

@class CZElement;
@class CZElementText;

@protocol CZElementTextInputToolDelegate;

@interface CZElementTextInputTool : NSObject<UITextViewDelegate>

@property (nonatomic, assign) UIView *imageView;
@property (nonatomic, readonly) CZElementText *targetTextElemnt;
@property (nonatomic, assign, readonly) CZElementDoc docManager;
@property (nonatomic, assign) id<CZElementTextInputToolDelegate> delegate;
@property (nonatomic, assign) BOOL creating;  // whether it's a first time creating, or editing again.

- (id)initWithDocManager:(CZElementDoc)docManager;

/*! Begin editing on elment. element must be CZElementText object, otherwise nothing happens and return NO.*/
- (BOOL)tryEditOnElement:(CZElement *)element;

/*! @return YES, if handled; othersize NO.*/
- (BOOL)dismissEditing;

- (BOOL)isEditing;

@end

@protocol CZElementTextInputToolDelegate<NSObject>
@optional
- (void)textInputTool:(CZElementTextInputTool *)textInputTool didEndCreateElement:(CZElement *)element;
- (void)textInputToolDidEndEditing:(CZElementTextInputTool *)textInputTool;

@end
