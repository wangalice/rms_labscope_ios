//
//  CZScaleBarSetCornerAction.h
//  Hermes
//
//  Created by Ralph Jin on 5/3/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <CZToolbox/CZToolbox.h>
#import "CZElementScaleBar.h"

@interface CZScaleBarSetCornerAction : NSObject<CZUndoAction>

- (id)initWithScaleBar:(CZElementScaleBar *)scaleBar newPosition:(CZScaleBarPosition)pos;

@end
