//
//  CZMultiTapCreateTool.m
//  Hermes
//
//  Created by Ralph Jin on 4/10/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZMultiTapCreateTool.h"

#import <QuartzCore/QuartzCore.h>
#import "CALayer+ReplaceWithSameName.h"
#import <CZToolbox/CZToolbox.h>
#import "CZDefaultSettings+Annotations.h"
#import "CZElementPolygon.h"
#import "CZElementPolyline.h"
#import "CZElementSpline.h"
#import "CZElementSplineContour.h"
#import "CZElementCount.h"
#import "CZElementLayer.h"
#import "CZCreateElementAction.h"
#import "CZMultipointHandler.h"

#define kCZClosePolygonThreshold 22.0f

// TODO: extract to anther file, if need reuse
@interface CZAppendPointAction : NSObject <CZUndoAction> {
    CGPoint _point;
}

@property (nonatomic, retain) CZElement<CZElementMultiPointKind> *element;

@end

@implementation CZAppendPointAction

- (void)dealloc {
    [_element release];
    [super dealloc];
}

- (NSUInteger)costMemory {
    return sizeof(CGPoint) + 16U;
}

- (void)do {
}

- (void)undo {
    NSUInteger index = [_element pointsCount] - 1;
    _point = [_element pointAtIndex:index];
    
    [_element notifyWillChange];
    [_element removePointAtIndex:index];
    [_element notifyDidChange];
}

- (void)redo {
    [_element notifyWillChange];
    [_element appendPoint:_point];
    [_element notifyDidChange];
}

- (CZUndoActionType)type {
    return CZUndoActionTypeAnnotation;
}

@end

@interface CZMultiTapCreateTool() {
    BOOL _firstTimeMoved;
    BOOL _pointAppended;
    BOOL _polygonCreated;
}

@property (nonatomic, assign) UIView *view;

@property (nonatomic, retain) CZElement<CZElementMultiPointKind> *currentPolygon;

@property (nonatomic, retain, readwrite) CZMultipointHandler *handler;

@end

@implementation CZMultiTapCreateTool

- (id)initWithDocManager:(CZElementDoc)docManager view:(UIView *)view {
    self = [super initWithDocManager:docManager];
    if (self) {
        _view = view;
        _elementClass = [CZElementPolygon class];
        _pointAppended = NO;
        _firstTimeMoved = NO;
    }
    
    return self;
}

- (void)dealloc {
    [_currentPolygon release];
    [_handler release];
    [super dealloc];
}

- (BOOL)canFinishCreating {
    const NSUInteger kMinCount = [_currentPolygon minimumPointsCount];
    return ([_currentPolygon pointsCount] >= kMinCount);
}

- (void)updateWithZoom:(CGFloat)zoomOfView {
    _zoomOfView = zoomOfView;
    [_handler updateWithZoom:zoomOfView];
}

- (CZElement *)elementBeingCreated {
    return _currentPolygon;
}

#pragma mark - Override CZAbastractGraphicsTool

- (CZShouldBeginTouchState)shouldBeginTouch:(CGPoint)p {
    return CZShouldBeginTouchStateMaybe;
}

- (BOOL)touchBegin:(CGPoint)p {
    _firstTimeMoved = YES;
    _pointAppended = NO;
    
    if ([_delegate respondsToSelector:@selector(multiTapCreateTool:didDragPoint:)]) {
        [_delegate multiTapCreateTool:self didDragPoint:p];
    }
    
    return YES;
}

- (BOOL)touchMoved:(CGPoint)p {
    if (_firstTimeMoved) {
        _firstTimeMoved = NO;
        [self appendPoint:p];
        _pointAppended = YES;
        [self updateHandler];
    } else {
        NSUInteger i = [_currentPolygon pointsCount] - 1;
        [_currentPolygon setPoint:p atIndex:i];
        [self updateHandler];
    }
    if ([_delegate respondsToSelector:@selector(multiTapCreateTool:didDragPoint:)]) {
        [_delegate multiTapCreateTool:self didDragPoint:p];
    }
    
    return YES;
}

- (BOOL)touchEnd:(CGPoint)p {
    if (_firstTimeMoved) {
        _firstTimeMoved = NO;
        [self appendPoint:p];
        _pointAppended = YES;
        [self updateHandler];
    }
    [self endDragPoint:p];
    [self checkIfReachMaxPointsCount];
    return YES;
}

- (BOOL)tap:(CGPoint)p {
    CGPoint pointOfView = [CZCommonUtils imagePointToViewPoint:p];
    if ([self checkIfDone:pointOfView]) {
        [self finishCreating];
    } else {
        _firstTimeMoved = NO;
        [self appendPoint:p];
        _pointAppended = YES;
        [self updateHandler];
        [self endDragPoint:p];
        [self checkIfReachMaxPointsCount];
    }
    return YES;
}

- (BOOL)doubleTap:(CGPoint)p {
    // do nothing
    return YES;
}

- (BOOL)touchCancelled:(CGPoint)p {
    BOOL success = [self finishCreating];
    if (!success) {
        [self cancelCreation];
    }
    return YES;
}

- (void)cancelCreation {
    BOOL willCancel = self.currentPolygon != nil;
    
    [self resetData];
    
    if (willCancel && [_delegate respondsToSelector:@selector(multiTapCreateToolDidCancelled:)]) {
        [_delegate multiTapCreateToolDidCancelled:self];
    }
}

#pragma mark - Private Methods

- (BOOL)checkIfDone:(CGPoint)p {
    NSString *partName = [self.handler hitTest:p];
    return [partName integerValue] == CZAnnotationHandlerPartDone;
}

- (BOOL)checkIfReachMaxPointsCount {
    BOOL reachesMax = NO;
    if ([_currentPolygon respondsToSelector:@selector(maximumPointsCount)]) {
        NSUInteger maximumPointsCount = [_currentPolygon maximumPointsCount];
        if (_currentPolygon.pointsCount >= maximumPointsCount) {
            reachesMax = YES;
            [self finishCreating];
        }
    }
    return reachesMax;
}

- (void)appendPoint:(CGPoint)p {
    if (_currentPolygon == nil) {
        id obj = [[_elementClass alloc] init];
        if ([obj isKindOfClass:[CZElement class]] &&
            [obj conformsToProtocol:@protocol(CZElementMultiPointKind)]) {
            CZElement<CZElementMultiPointKind> *polygon = obj;
            polygon.strokeColor = [[CZDefaultSettings sharedInstance] color];
            polygon.size = [[CZDefaultSettings sharedInstance] elementSize];
            polygon.measurementHidden = ![[NSUserDefaults standardUserDefaults] boolForKey:kMeasurementEnabled];
            self.currentPolygon = polygon;
        } else {
            NSAssert(NO, @"elementClass is invalid for CZMultiTapCreateTool.");
        }
        [obj release];
    }
    
    [_currentPolygon appendPoint:p];
}

- (BOOL)endDragPoint:(CGPoint)p {
    const NSUInteger kMinCount = [_currentPolygon minimumPointsCount];

    if (_pointAppended) {
        if ([self.delegate respondsToSelector:@selector(multiTapCreateTool:didAddPointAtCount:)]) {
            [self.delegate multiTapCreateTool:self didAddPointAtCount:[_currentPolygon pointsCount]];
        }
        
        if (!_polygonCreated) {
            if ([_currentPolygon pointsCount] >= kMinCount) {
                BOOL showMeasurement = [[NSUserDefaults standardUserDefaults] boolForKey:kMeasurementEnabled];
                _currentPolygon.measurementHidden = !showMeasurement;
                _currentPolygon.measurementBackgroundColor = [[CZDefaultSettings sharedInstance] backgroundColor];
                
                CZCreateElementAction *action = [[CZCreateElementAction alloc] init];
                action.element = _currentPolygon;
                action.layer = self.docManager.elementLayer;
                [self.docManager.elementLayer addElement:_currentPolygon];
                [self.docManager pushAction:action];
                [action release];
                
                _polygonCreated = YES;
            }
        } else {
            if ([_currentPolygon pointsCount] > kMinCount) {
                CZAppendPointAction *action = [[CZAppendPointAction alloc] init];
                action.element = _currentPolygon;
                [self.docManager pushAction:action];
                [action release];
                
                [_currentPolygon notifyDidChange];
            }
        }
        
        _pointAppended = NO;
    }

    return YES;
}

- (BOOL)finishCreating {
    if (![self canFinishCreating]) {
        return NO;
    }
    
    CZElement *tempElement = [_currentPolygon retain];
    [self resetData];
    
    if ([_delegate respondsToSelector:@selector(multiTapCreateTool:didCreateElement:)]) {
        [_delegate multiTapCreateTool:self didCreateElement:tempElement];
    }
    [tempElement release];
    
    return YES;
}

- (void)createHandler {
    if (self.handler == nil && self.currentPolygon != nil) {
        CZMultipointHandler *handler = [[CZMultipointHandler alloc] init];
        handler.target = _currentPolygon;
        handler.showConnectLine = (![self.currentPolygon isKindOfClass:[CZElementCount class]]);
        handler.doneButtonEnabled = [self canFinishCreating];
        [handler updateWithZoom:_zoomOfView];
        self.handler = handler;
        [handler release];
        
        CALayer *layer = handler.representLayer;
        [self.view.layer replaceSublayerWithSameNameLayer:layer bringTop:YES];
    }
}

- (void)updateHandler {
    if (self.handler == nil) {
        [self createHandler];
    } else {
        _handler.doneButtonEnabled = [self canFinishCreating];
        [_handler updateWithZoom:_zoomOfView];
    }
}

- (void)resetData {
    [self.handler setHidden:YES];
    self.handler = nil;
    self.currentPolygon = nil;
    _polygonCreated = NO;
    
    _pointAppended = NO;
    _firstTimeMoved = NO;
}

@end
