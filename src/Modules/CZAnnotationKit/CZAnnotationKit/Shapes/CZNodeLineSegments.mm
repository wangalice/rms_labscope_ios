//
//  CZNodeLineSegments.mm
//  Hermes
//
//  Created by Ralph Jin on 7/01/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeLineSegments.h"
#include <vector>

@implementation CZNodeLineSegments

#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    
    const CGPoint *points = [self pointArray];
    
    for (NSUInteger i = 1; i < [self pointsCount]; i += 2) {
        CGPathMoveToPoint(relativePath, NULL, points[i - 1].x, points[i - 1].y);
        CGPathAddLineToPoint(relativePath, NULL, points[i].x, points[i].y);
    }

    return relativePath;
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    return NO;
}

@end
