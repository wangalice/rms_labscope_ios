//
//  CZNodeLine.h
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"

@interface CZNodeLine : CZNodeShape

@property (nonatomic) CGPoint p1;
@property (nonatomic) CGPoint p2;

- (id) initWithPoint:(CGPoint)p1 anotherPoint:(CGPoint)p2;

@end
