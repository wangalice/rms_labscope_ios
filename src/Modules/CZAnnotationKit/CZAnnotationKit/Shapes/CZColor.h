//
//  CZColor.h
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>

#if TARGET_OS_IPHONE

#import <UIKit/UIKit.h>

#endif

typedef struct {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} CZColor;

#ifdef __cplusplus
extern "C" {
#endif
    
    static inline CZColor
    CZColorMakeRGBA(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
        CZColor color;
        color.a = a;
        color.r = r;
        color.b = b;
        color.g = g;
        return color;
    }
    
    static inline CZColor
    CZColorMake(uint8_t r, uint8_t g, uint8_t b) {
        CZColor color;
        color.a = 255;
        color.r = r;
        color.b = b;
        color.g = g;
        return color;
    }
    
    bool static inline CZColorEqualToColor(CZColor c1, CZColor c2) {
        return (c1.a == c2.a) && (c1.r == c2.r) && (c1.g == c2.g) && (c1.b == c2.b);
    }
    
    bool static inline CZColorIsLight(CZColor color) {
        CGFloat brightness = 0.2126 * color.r / 255 + 0.7152 * color.g / 255 + 0.0722 * color.b / 255;
        return brightness >= 0.5;
    }
    
    CGColorRef CGColorFromCZColor(CZColor color);
    
    UIColor *UIColorFromCZColor(CZColor color);
    CZColor CZColorFromUIColor(UIColor *color);
    
    NSString *NSStringFromColor(CZColor color);
    CZColor CZColorFromString(NSString *string);
    
#ifdef __cplusplus
}
#endif

#define kCZColorWhite       CZColorMake(255, 255, 255)
#define kCZColorBlack       CZColorMake(0, 0, 0)
#define kCZColorRed         CZColorMake(255, 0, 0)
#define kCZColorGreen       CZColorMake(0, 255, 0)
#define kCZColorBlue        CZColorMake(0, 0, 255)
#define kCZColorYellow      CZColorMake(255, 255, 0)
#define kCZColorOrange      CZColorMake(255, 127, 0)
#define kCZColorTransparent CZColorMakeRGBA(0, 0, 0, 0)

#define kCZColorValueWhite          [NSValue valueWithCZColor:kCZColorWhite]
#define kCZColorValueBlack          [NSValue valueWithCZColor:kCZColorBlack]
#define kCZColorValueRed            [NSValue valueWithCZColor:kCZColorRed]
#define kCZColorValueGreen          [NSValue valueWithCZColor:kCZColorGreen]
#define kCZColorValueBlue           [NSValue valueWithCZColor:kCZColorBlue]
#define kCZColorValueYellow         [NSValue valueWithCZColor:kCZColorYellow]
#define kCZColorValueOrange         [NSValue valueWithCZColor:kCZColorOrange]
#define kCZColorValueTransparent    [NSValue valueWithCZColor:kCZColorTransparent]

@interface NSValue (CZColor)

+ (instancetype)valueWithCZColor:(CZColor)color;
@property (readonly) CZColor CZColorValue;

@end
