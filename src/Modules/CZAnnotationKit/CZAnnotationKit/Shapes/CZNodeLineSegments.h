//
//  CZNodeLineSegments.h
//  Hermes
//
//  Created by Ralph Jin on 7/01/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"

@interface CZNodeLineSegments : CZNodeMultiPointShape

@end
