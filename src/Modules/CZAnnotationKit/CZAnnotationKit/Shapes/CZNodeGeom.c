//
//  CZNodeGeom.c
//  Hermes
//
//  Created by Ralph Jin on 2/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "CZNodeGeom.h"
#include <math.h>
#include <stdlib.h>
#include "CZGeometryHelper.h"

#define EPSILON 1e-5

static inline CGRect CZMakeRectFromTwoPoint(CGPoint p1, CGPoint p2) {
    CGFloat x, y, width, height;
    if (p1.x > p2.x) {
        x = p2.x;
        width = p1.x - p2.x;
    } else {
        x = p1.x;
        width = p2.x - p1.x;
    }
    
    if (p1.y > p2.y) {
        y = p2.y;
        height = p1.y - p2.y;
    } else {
        y = p1.y;
        height = p2.y - p1.y;
    }
    return CGRectMake(x, y, width, height);
}

/**
 * Calculate cross point in parameter equation. see also http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
 * line1 = a.x + da.x * t1;
 * line2 = c.x + dc.x * t2;
 * @param t is output value, equals CGPoint{t1, t2}
 * @return true if success, false if no cross point.
 */
static bool CZCalcLineCrossPointInEquation(CGPoint a, CGPoint da, CGPoint c, CGPoint dc, CGPoint *t) {
    CGFloat daXdc = da.x * dc.y - dc.x * da.y;  // cross-product of da and dc
    
    if (fabs(daXdc) < EPSILON) {
        return false;
    }
    
    CGPoint c_a = CGPointMake(c.x - a.x, c.y - a.y);
    CGFloat c_aXdc = c_a.x * dc.y - dc.x * c_a.y;  // cross-product of c_a and dc
    t->x = c_aXdc / daXdc;
    
    CGFloat c_aXda = c_a.x * da.y - da.x * c_a.y;
    t->y = c_aXda / daXdc;
    
    return true;
}

inline static CGFloat CGPointDistanceToPoint(CGPoint pt1, CGPoint pt2) {
    pt1.x -= pt2.x;
    pt1.y -= pt2.y;
    return sqrt(pt1.x * pt1.x + pt1.y * pt1.y);
}

CGFloat CZCalcRotateAngle(CGPoint p, CGPoint a, CGPoint b) {
    if (CGPointEqualToPoint(p, a) ||
        CGPointEqualToPoint(b, a) ||
        CGPointEqualToPoint(p, b)) {
        return 0;
    }
    
    CGPoint vec1;
    vec1.x = a.x - p.x;
    vec1.y = a.y - p.y;
    
    CGPoint vec2;
    vec2.x = b.x - p.x;
    vec2.y = b.y - p.y;
    
    return atan2(vec2.y, vec2.x) - atan2(vec1.y, vec1.x);
}

CGFloat CZNormalizeAngle(CGFloat radians) {
    const double kDoublePi = M_PI * 2.0;
    CGFloat circles = floor(radians / kDoublePi);
    return radians - circles * kDoublePi;
}

CGAffineTransform CGAffineTransformMakeRotationAtPoint(CGFloat angle, CGPoint p) {
    CGAffineTransform rotateTransform = CGAffineTransformMakeTranslation(p.x, p.y);
    rotateTransform = CGAffineTransformRotate(rotateTransform, angle);
    rotateTransform = CGAffineTransformTranslate(rotateTransform, -p.x, -p.y);
    return rotateTransform;
}

CGAffineTransform CGAffineTransformMakeRotationFromVector(CGFloat vx, CGFloat vy) {
    if (vx == 0.0 && vy == 0.0) {
        return CGAffineTransformIdentity;
    } else {
        CGFloat slope = sqrt(vx * vx + vy * vy);
        CGFloat cosa = vx / slope;
        CGFloat sina = vy / slope;
        return CGAffineTransformMake(cosa, sina, -sina, cosa, 0, 0);
    }
}

CGFloat CZLengthOfLine(CGPoint a, CGPoint b) {
    CGFloat dx = a.x - b.x;
    CGFloat dy = a.y - b.y;
    return hypot(dx, dy);
}

CGFloat CZRadiusOfVector(CGPoint a, CGPoint b) {
    CGFloat dx = b.x - a.x;
    CGFloat dy = b.y - a.y;
    
    return atan2(dy, dx);
}

CGPoint CalcOrthoPoint(CGPoint a, CGPoint b, CGPoint p) {
    if (CGPointEqualToPoint(a, b)) {
        return a;
    }
    
    if (a.x == b.x) {
        return CGPointMake(a.x, p.y);
    }
    
    if (a.y == b.y) {
        return CGPointMake(p.x, a.y);
    }
    
    CGFloat vecX = b.x - a.x;
    CGFloat vecY = b.y - a.y;
    
    // rotate to zero degree, make line a->b horizontal
    CGAffineTransform t = CGAffineTransformMakeRotationFromVector(vecX, -vecY);
    CGPoint delta;
    delta.x = p.x - a.x;
    delta.y = p.y - a.y;
    delta = CGPointApplyAffineTransform(delta, t);
    delta.y = 0.0;

    // rotate back
    CGFloat temp = t.b; t.b = t.c; t.c = temp;  // swap t.b with t.c
    delta = CGPointApplyAffineTransform(delta, t);
    
    return CGPointMake(a.x + delta.x, a.y + delta.y);
}

CGPoint CZCalcOuterPoint(CGPoint a, CGPoint b, double length) {
    // rotate the vector 90 degrees
    double dx = -b.y + a.y;
    double dy = b.x - a.x;
    
    double distance = hypot(dx, dy);
    
    double cosr, sinr;
    if (distance != 0.0) {
        cosr = dx / distance;
        sinr = dy / distance;
    } else {
        cosr = 0;
        sinr = 1;
    }
    
    CGPoint p = b;
    p.x += length * cosr;
    p.y += length * sinr;
    
    return p;
}

CGPoint CZCalcCrossPoint(CGPoint a, CGPoint b, CGPoint c, CGPoint d) {
    CGPoint t;
    CGPoint da = CGPointMake(b.x - a.x, b.y - a.y);
    CGPoint dc = CGPointMake(d.x - c.x, d.y - c.y);
    
    if (!CZCalcLineCrossPointInEquation(a, da, c, dc, &t)) {
        return CGPointMake(NAN, NAN);
    }
    
    CGPoint cross;
    cross.x = c.x + t.y * dc.x;
    cross.y = c.y + t.y * dc.y;
    return cross;
}

CGPoint CZCalcCrossPointOfHalfLine(CGPoint p1, CGPoint dp1, CGPoint p2, CGPoint dp2) {
    CGPoint t;
    if (!CZCalcLineCrossPointInEquation(p1, dp1, p2, dp2, &t)) {
        return CGPointMake(NAN, NAN);
    }
    
    if (t.x >= 0 && t.y >= 0) {
        CGPoint cross;
        cross.x = p2.x + t.y * dp2.x;
        cross.y = p2.y + t.y * dp2.y;
            
        return cross;
    } else {
        return CGPointMake(NAN, NAN);
    }
}

bool CZLineSegmentsAreCrossed(CGPoint a, CGPoint b, CGPoint c, CGPoint d) {
    CGRect r1 = CZMakeRectFromTwoPoint(a, b);
    CGRect r2 = CZMakeRectFromTwoPoint(c, d);
    if (!CGRectIntersectsRect(r1, r2)) {
        return false;
    }
    
    CGPoint t;
    CGPoint da = CGPointMake(b.x - a.x, b.y - a.y);
    CGPoint dc = CGPointMake(d.x - c.x, d.y - c.y);
    if (CZCalcLineCrossPointInEquation(a, da, c, dc, &t)) {
        return(t.x > 0 && t.x < 1 && t.y > 0 && t.y < 1);
    } else {
        return false;
    }
}

void CGPathAddSpline(CGMutablePathRef path, const CGAffineTransform *m, const CGPoint points[], size_t pointCount, bool closePath) {
    if (points == NULL) {
        return;
    }
    
    if (pointCount < 3) {
        CGPathAddLines(path, m, points, pointCount);
        return;
    }
    
    size_t controlPointsCount;
    CGPoint *controlPoints = NULL;
    
    if (closePath) {
        controlPointsCount = pointCount * 3 + 1;
        controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);
        ComputeCloseBezierControlPoints(points, pointCount, controlPoints);
    } else {
        controlPointsCount = pointCount * 3 - 1;
        controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);
        ComputeOpenBezierControlPoints(points, pointCount, controlPoints);
    }
    
    CGPathMoveToPoint(path, m, controlPoints[0].x, controlPoints[0].y);
    
    for (int i = 3; i < controlPointsCount; i += 3) {
        CGPathAddCurveToPoint(path, m, controlPoints[i - 2].x, controlPoints[i - 2].y,
                              controlPoints[i - 1].x, controlPoints[i - 1].y,
                              controlPoints[i].x, controlPoints[i].y);
    }
    
    if (closePath) {
        CGPathCloseSubpath(path);
    }
    
    free(controlPoints);
}

bool CZPieContainsPoint(CGPoint p, CGFloat startAngle, CGFloat endAngle, CGPoint ptCenter, CGFloat radius) {
    CGPoint vec3 = CGPointMake(p.x - ptCenter.x, p.y - ptCenter.y);
    CGFloat distance = sqrt(vec3.x * vec3.x + vec3.y * vec3.y);
    if (distance > radius) {
        return false;
    }
    
    if (endAngle < startAngle) {
        endAngle += M_PI * 2.0;
    }
    
    CGFloat angle = atan2(vec3.y, vec3.x);
    if (angle < startAngle) {
        angle += M_PI * 2.0;
    }
    
    return (startAngle <= angle && angle <= endAngle);
}

bool CGRectIntersectsCircle(CGRect rect, CGPoint center, CGFloat radius) {
    CGRect outerBoundary = CGRectInset(rect, -radius, -radius);
    if (CGRectContainsPoint(outerBoundary, center)) {
        // top left corner
        CGRect cornerRect = outerBoundary;
        cornerRect.size.width = radius;
        cornerRect.size.height = radius;
        CGPoint cornerPoint = rect.origin;
        if (CGRectContainsPoint(cornerRect, center)) {
            CGFloat distance = CGPointDistanceToPoint(cornerPoint, center);
            return distance < radius;
        }
        
        cornerRect.origin.x = CGRectGetMaxX(rect);
        cornerPoint.x = cornerRect.origin.x;
        if (CGRectContainsPoint(cornerRect, center)) {
            CGFloat distance = CGPointDistanceToPoint(cornerPoint, center);
            return distance < radius;
        }
        
        cornerRect.origin.y = CGRectGetMaxY(rect);
        cornerPoint.y = cornerRect.origin.y;
        if (CGRectContainsPoint(cornerRect, center)) {
            CGFloat distance = CGPointDistanceToPoint(cornerPoint, center);
            return distance < radius;
        }
        
        cornerRect.origin.x = CGRectGetMinX(outerBoundary);
        cornerPoint.x = CGRectGetMinX(rect);
        if (CGRectContainsPoint(cornerRect, center)) {
            CGFloat distance = CGPointDistanceToPoint(cornerPoint, center);
            return distance < radius;
        }
        
        return true;
    } else {
        return false;
    }
}