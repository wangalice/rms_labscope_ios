//
//  CZNode.m
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNode.h"

@interface CZNode()

@property(nonatomic, retain) NSMutableArray *subNodes;

/*! @return YES, if success; otherwise NO, if node is not found. */
- (BOOL)removeSubNode:(CZNode *)node;

@end

@implementation CZNode

- (id)init {
    self = [super init];
    if (self) {
        _identifier = @"";
        _subNodes = [[NSMutableArray alloc] init];
        _strokeColor = CZColorMakeRGBA(0, 0, 0, 0);
        _fillColor = CZColorMakeRGBA(0, 0, 0, 0);
    }
    return self;
}

- (void)dealloc {
    _parentNode = nil;
    [_identifier release];
    [_subNodes release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNode *newNode = [[[self class] allocWithZone:zone] init];
    
    if (newNode) {
        newNode->_strokeColor = self->_strokeColor;
        newNode->_fillColor = self->_fillColor;
        newNode->_rotateAngle = self->_rotateAngle;
        
        for (CZNode *subNode in self->_subNodes) {
            CZNode *newSubNode = [subNode copyWithZone:zone];
            [newNode addSubNode:newSubNode];
            [newSubNode release];
        }
    }
    
    return newNode;
}

- (BOOL)isFilled {
    return (self.fillColor.a != 0);
}

- (NSUInteger)subNodeCount {
    return [self.subNodes count];
}

- (BOOL)addSubNode:(CZNode *)node {
    if (node == nil || self == node.parentNode) {
        return NO;
    }
    
    // prevent loop in a tree
    CZNode *parent = self;
    while (parent) {
        if (parent == node) {
            return NO;
        }
        parent = parent.parentNode;
    }
    
    if (node.parentNode) {
        [node.parentNode removeSubNode:node];
    }
    
    [self.subNodes addObject:node];
    node.parentNode = self;

    return YES;
}

- (CZNode *)subNodeAtIndex:(NSUInteger)index {
    return [self.subNodes objectAtIndex:index];
}

- (BOOL)removeSubNode:(CZNode *)node {
    if (node == nil || self != node.parentNode) {
        return NO;
    }
    
    node.parentNode = nil;
    [self.subNodes removeObject:node];
    
    return YES;
}

- (BOOL)removeFromParent {
    if (self.parentNode == nil) {
        return NO;
    }
    
    return [self.parentNode removeSubNode:self];
}

- (CGAffineTransform)transformSelf {
    return CGAffineTransformIdentity;
}

- (CGAffineTransform)transformAbsolute {
    CGAffineTransform parentAbsoluteTransform;
    if (self.parentNode == nil) {
        parentAbsoluteTransform = CGAffineTransformIdentity;
    } else {
        parentAbsoluteTransform = [self.parentNode transformAbsolute];
    }
    
    CGAffineTransform selfRelativeTransform = [self transformSelf];
    CGAffineTransform result = CGAffineTransformConcat(selfRelativeTransform, parentAbsoluteTransform);

    return result;
}

- (CGRect)frame {
    CGRect rect = CGRectNull;
    
    for (CZNode *subNode in self.subNodes) {
        CGRect subBoundary = [subNode frame];
        rect = CGRectUnion(subBoundary, rect);
    }
    
    CGRect boundary = [self boundary];
    rect = CGRectUnion(boundary, rect);
    
    CGAffineTransform transform = [self transformSelf];
    if (! CGAffineTransformIsIdentity(transform)) {
        rect = CGRectApplyAffineTransform(rect, transform);
    }
    
    return rect;
}

- (CGRect)boundary {
    return CGRectNull;
}

- (BOOL)hitTest:(CGPoint)p tolerance:(CGFloat)tolerance {
    // Convert point from parent coordinate to self coordinate
    CGAffineTransform t = [self transformSelf];
    t = CGAffineTransformInvert(t);
    CGPoint pointOfSelf = CGPointApplyAffineTransform(p, t);

    for (CZNode *node in self.subNodes) {
        if ([node hitTest:pointOfSelf tolerance:tolerance]) {
            return YES;
        }
    }
    
    return [self hitTestSelf:pointOfSelf tolerance:tolerance];
}

- (BOOL)hitTestSelf:(CGPoint)pointOfSelf tolerance:(CGFloat)tolerance {
    return NO;
}

- (BOOL)containsPoint:(CGPoint)p {
    // Convert point from parent coordinate to self coordinate
    CGAffineTransform t = [self transformSelf];
    t = CGAffineTransformInvert(t);
    CGPoint pointOfSelf = CGPointApplyAffineTransform(p, t);
    
    for (CZNode *node in self.subNodes) {
        if ([node containsPoint:pointOfSelf]) {
            return YES;
        }
    }
    
    return [self selfContainsPoint:pointOfSelf];
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    return NO;
}

- (CALayer *)newLayer {
    CALayer *ownLayer = [self newOwnLayer];
    if (ownLayer == nil) {
        return nil;
    }
    
    for (CZNode *node in self.subNodes) {
        CALayer *subLayer = [node newLayer];
        if (subLayer) {
            [ownLayer addSublayer:subLayer];
            [subLayer release];
        }
    }
    return ownLayer;
}

- (CALayer *)newOwnLayer {
    return [[CALayer alloc] init];
}

- (void)updateOwnLayer:(CALayer *)ownLayer {
}

- (CGPoint)anchorPoint {
    return CGPointZero;
}

@end
