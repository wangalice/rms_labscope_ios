//
//  CZNodeShape.m
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"
#include "CZNodeGeom.h"

@interface CZNodeShape() {
  @private
    CGPathRef _relativePath;
}
@end

@implementation CZNodeShape

- (id)init {
    self = [super init];
    if (self) {
        _lineWidth = 1.0f;
    }
    return self;
}

- (void)dealloc {
    if (_relativePath) {
        CGPathRelease(_relativePath);
        _relativePath = NULL;
    }
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeShape *newShape = [super copyWithZone:zone];
    if (newShape) {
        newShape->_dash = self->_dash;
        newShape->_lineWidth = self->_lineWidth;
        if (self->_relativePath) {
            newShape->_relativePath = CGPathRetain(self->_relativePath);
        }
    }
    
    return newShape;
}

- (CGPathRef)relativePath {
    if (_relativePath == nil) {
        CGMutablePathRef path = [self newRelativePath];
        _relativePath = CGPathCreateCopy(path);
        CGPathRelease(path);
    }
    return _relativePath;
}

- (void)invalidateRelativePath {
    if (_relativePath) {
        CGPathRelease(_relativePath);
        _relativePath = NULL;
    }
}

- (CGMutablePathRef) newRelativePath {
    return nil;
}

#pragma mark -
#pragma mark override CZNode

- (CGAffineTransform)transformSelf {
    CGRect relativePathBB = CGPathGetBoundingBox(self.relativePath);
    if (!isnormal(relativePathBB.origin.x)) {
        relativePathBB.origin.x = 0;
    }
    
    if (!isnormal(relativePathBB.origin.y)) {
        relativePathBB.origin.y = 0;
    }
    
    CGAffineTransform translateTransform = CGAffineTransformMakeTranslation(relativePathBB.origin.x, relativePathBB.origin.y);
    
    if (self.rotateAngle == 0) {
        return translateTransform;
    } else {
        CGPoint rotateCenter;
        rotateCenter.x = relativePathBB.size.width * 0.5;
        rotateCenter.y = relativePathBB.size.height * 0.5;
        CGAffineTransform rotateTransform = CGAffineTransformMakeRotationAtPoint(self.rotateAngle, rotateCenter);

        // combine the rotateTransform
        CGAffineTransform result = CGAffineTransformConcat(rotateTransform, translateTransform);
        return result;
    }
}

- (CGRect)boundary {
    CGRect rect = CGPathGetBoundingBox(self.relativePath);
    rect.origin.x = 0;
    rect.origin.y = 0;
    return rect;
}

- (BOOL)hitTestSelf:(CGPoint)pointOfSelf
          tolerance:(CGFloat)tolerance {
    CGPathRef relativePath = [self newRelativePath];
    
    CGRect relativePathBB = CGPathGetBoundingBox(self.relativePath);
    CGPoint pointOfRelative = pointOfSelf;
    pointOfRelative.x += relativePathBB.origin.x;
    pointOfRelative.y += relativePathBB.origin.y;
    
    if (relativePath == NULL) {
        return NO;
    }
    
    bool hit = NO;

    // If shape can be fill and is filled.
    if ([self conformsToProtocol:@protocol(CZNodeShapeFillable)]) {
        if (self.isFilled) {
            hit = CGPathContainsPoint(relativePath, NULL, pointOfRelative, true);
        }
    }
    
    if (!hit) {
        CGFloat lineWidth = MAX(self.lineWidth, tolerance * 2);
        CGPathRef strokePath = CGPathCreateCopyByStrokingPath(relativePath, NULL, lineWidth, kCGLineCapSquare, kCGLineJoinBevel, 0.1);
        hit = CGPathContainsPoint(strokePath, NULL, pointOfRelative, false);
        CGPathRelease(strokePath);
    }
    
    CGPathRelease(relativePath);
    
    return hit ? YES : NO;
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    CGPathRef relativePath = [self newRelativePath];
    if (relativePath == NULL) {
        return NO;
    }
    
    CGRect relativePathBB = CGPathGetBoundingBox(self.relativePath);
    CGPoint pointOfRelative = pointOfSelf;
    pointOfRelative.x += relativePathBB.origin.x;
    pointOfRelative.y += relativePathBB.origin.y;
    
    BOOL hitOn = CGPathContainsPoint(relativePath, NULL, pointOfRelative, true);
    
    CGPathRelease(relativePath);
    return hitOn;
}

- (CGPoint)anchorPoint {
    CGPathRef theRelativePath = self.relativePath;
    if (theRelativePath == NULL) {
        return CGPointZero;
    }
    
    CGRect relativePathBB = CGPathGetBoundingBox(self.relativePath);
    CGFloat x = CGRectGetMidX(relativePathBB);
    CGFloat y = CGRectGetMidY(relativePathBB);
    return CGPointMake(x, y);
}

- (CALayer *)newOwnLayer {
    CGPathRef theRelativePath = self.relativePath;
    if (theRelativePath == NULL) {
        return nil;
    }
    
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    [self updateOwnLayer:shapeLayer];

    return shapeLayer;
}

- (void)updateOwnLayer:(CALayer *)ownLayer {
    CGPathRef theRelativePath = self.relativePath;
    if (theRelativePath == NULL) {
        return;
    }
    
    if (![ownLayer isKindOfClass:[CAShapeLayer class]]) {
        return;
    }
    
    CAShapeLayer *shapeLayer = (CAShapeLayer *)ownLayer;
    shapeLayer.name = self.identifier;
    
    CGRect relativePathBB = CGPathGetBoundingBox(self.relativePath);
    if (CGRectIsInfinite(relativePathBB) || CGRectIsNull(relativePathBB)) {
        relativePathBB.origin = CGPointZero;
    }
    CGAffineTransform offsetAffine = CGAffineTransformMakeTranslation(-relativePathBB.origin.x, -relativePathBB.origin.y);
    CGPathRef pathToPlaceInLayer = CGPathCreateCopyByTransformingPath(theRelativePath, &offsetAffine);
    
    shapeLayer.frame = relativePathBB;
    if ([self conformsToProtocol:@protocol(CZNodeShapeFillable)] &&
        self.isFilled) {
        if (self.fillColor.a == 0) {
            shapeLayer.fillColor = nil;
        } else {
            shapeLayer.fillColor = CGColorFromCZColor(self.fillColor);
        }
    } else {
        shapeLayer.lineWidth = self.lineWidth;
        if (self.isDash) {
            shapeLayer.lineDashPattern = @[@(self.lineWidth * 5), @(self.lineWidth * 5)];
        }
        shapeLayer.fillColor = nil;
    }
    if (self.strokeColor.a == 0) {
        shapeLayer.strokeColor = nil;
    } else {
        shapeLayer.strokeColor = CGColorFromCZColor(self.strokeColor);
    }
    
    shapeLayer.bounds = CGRectMake(0, 0, relativePathBB.size.width, relativePathBB.size.height);
    shapeLayer.path = pathToPlaceInLayer;
    CGPathRelease(pathToPlaceInLayer);
    
    if (fabs(self.rotateAngle) > EPSILON) {
        shapeLayer.transform = CATransform3DMakeRotation(self.rotateAngle, 0.0, 0.0, 1.0);
    }
    
    [shapeLayer removeAllAnimations];
}

#pragma mark -
#pragma mark KVO

// observe self change and invalidate the relatvePath
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == self) {
        [self invalidateRelativePath];
    }
}

@end
