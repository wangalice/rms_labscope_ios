//
//  CZGeometryHelper.h
//  Matscope
//
//  Created by Ralph Jin on 4/16/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#ifndef Matscope_CZGeometryHelper_h
#define Matscope_CZGeometryHelper_h

#include <CoreGraphics/CoreGraphics.h>

// migration code from ZEN's GeometryHelper.cs
#ifdef __cplusplus
extern "C" {
#endif
    
/**
 * Computes the open bezier control points from the specified list of curve segment start/end points (SP)
 * The returned control points list has the start point of the first curve segment following with
 * two control points (CP) and then the end point.The end point is the start point of the second segment.
 * This way we have a sequence :
 *   SP - CP - CP - SP - CP - CP - SP ...
 *
 * @param endPoints The input end points.
 * @param endPointsCount The input end points count.
 * @param controlPoints The output control points.
 */
void ComputeOpenBezierControlPoints(const CGPoint *endPoints, size_t endPointsCount, CGPoint *controlPoints);

/**
 * Computes the close bezier control points from the specified list of curve segment start/end points (SP)
 * The returned control points list has the start point of the first curve segment following with
 * two control points (CP) and then the end point.The end point is the start point of the second segment.
 * This way we have a sequence :
 *   SP - CP - CP - SP - CP - CP - SP ...
 *
 * @param endPoints The input end points.
 * @param endPointsCount The input end points count.
 * @param controlPoints The output control points.
 */
void ComputeCloseBezierControlPoints(const CGPoint *endPoints, size_t endPointsCount, CGPoint *controlPoints);

CF_RETURNS_RETAINED
CFDataRef ComputeBezierCurvePoints(const CGPoint *endPoints, size_t endPointsCount, bool closed, bool flattened);

/**
 * Computes the bezier curve points from the specified list of curve segment start/end points (SP).
 * Approximates the bezier curve with a polygon (or polyline) depending on the parameters to reduce the number of points.
 *
 * @param endPoints The input end points.
 * @param closed If true, the bezier curve is closed. Otherwise, open.
 * @param flattened If true, approximate the bezier curve with a polygon. Otherwise, return all curve points.
 * @param tolerance The tolerance (default 0.1) for approximation of a polygon to the bezier curve.
 * @return The bezier curve points.
 */
CF_RETURNS_RETAINED
CFDataRef ComputeBezierCurvePointsWithTolerance(const CGPoint *endPoints, size_t endPointsCount, bool closed, bool flattened, double tolerance);

#ifdef __cplusplus
}
#endif

#endif
