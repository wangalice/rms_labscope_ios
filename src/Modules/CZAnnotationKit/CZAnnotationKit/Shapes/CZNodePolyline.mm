//
//  CZNodePolyline.mm
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodePolyline.h"
#include <vector>

@implementation CZNodePolyline

#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    if ([self pointsCount] > 0) {
        CGPathAddLines(relativePath, NULL, [self pointArray], [self pointsCount]);
    }
    return relativePath;
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    return NO;
}

@end
