//
//  CZNodeLine.m
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeLine.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZNodeLine

- (id)init {
    self = [super init];
    if (self) {
        _p1.x = 0;
        _p1.y = 0;
        _p2.x = 1;
        _p2.y = 1;
        
        [self addObserver:self forKeyPaths:@"p1", @"p2", nil];
    }
    
    return self;
}

- (id)initWithPoint:(CGPoint)p1
       anotherPoint:(CGPoint)p2 {
    self = [super init];
    if (self) {
        _p1 = p1;
        _p2 = p2;
        
        [self addObserver:self forKeyPaths:@"p1", @"p2", nil];
    }
    
    return self;
}

- (void) dealloc {
    [self removeObserver:self forKeyPaths:@"p1", @"p2", nil];
    
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeLine *newLine = [super copyWithZone:zone];
    if (newLine) {
        newLine->_p1 = self->_p1;
        newLine->_p2 = self->_p2;
    }
    return newLine;
}

#pragma mark -
#pragma mark override CZNode

- (CGPoint)anchorPoint {
    CGPoint p;
    p.x = (_p1.x + _p2.x) * 0.5;
    p.y = (_p1.y + _p2.y) * 0.5;
    return p;
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    return NO;
}

#pragma mark -
#pragma mark override CZNodeShape

- (CGMutablePathRef) newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathMoveToPoint(relativePath, NULL, _p1.x, _p1.y);
    CGPathAddLineToPoint(relativePath, NULL, _p2.x, _p2.y);
    return relativePath;
}

@end
