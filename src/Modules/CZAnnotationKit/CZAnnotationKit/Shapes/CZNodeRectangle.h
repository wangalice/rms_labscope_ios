//
//  CZNodeRectangle.h
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"

@interface CZNodeRectangle : CZNodeShape<CZNodeShapeFillable>

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

@property (nonatomic) CGFloat cornerRadius;

- (id)initWithCGRect:(CGRect)rect;

@end
