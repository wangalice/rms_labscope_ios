//
//  CZNodeSpline.h
//  Hermes
//
//  Created by Ralph Jin on 12/16/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"

@interface CZNodeSpline : CZNodeMultiPointShape

@end
