//
//  CZNodeText.h
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNode.h"

@interface CZNodeText : CZNode<CZNodeShapeFillable>

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic) BOOL wrapped;
@property (nonatomic, copy) NSString *string;
@property (nonatomic, copy) NSString *fontFamily;
@property (nonatomic) CGFloat fontSize;

- (id)initWithFrame:(CGRect)frame;

- (void)setSuggestSize;

@end
