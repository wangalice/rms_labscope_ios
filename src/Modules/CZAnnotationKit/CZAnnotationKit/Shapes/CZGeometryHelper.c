//
//  CZGeometryHelper.c
//  Matscope
//
//  Created by Ralph Jin on 4/16/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#include "CZGeometryHelper.h"
//#include <CoreFoundation/CFData.h>
#include <stdlib.h>
#include <math.h>

#pragma mark - private functions

/**
 * Solves a linear equation system with three diagonal coefficient matrix.
 *
 * @param matrixDimension The matrix dimension.
 * @param matrix The coefficent matrix. If firstCalled is set true, the matrix is inverted.
 * @param vector The vector of results.
 * @param firstCalled If set to <c>true</c>, matrix will be inverted.
 * @returns False if the matrix is too small or singular. True without error.
 */
static bool TriagonalElimination(size_t matrixDimension, double **matrix, double *vector, bool firstCalled) {
    if (matrixDimension < 2) {
        return false;
    }
    
    if (firstCalled) {
        for (int i = 1; i < matrixDimension; i++) {
            if (!isnormal(matrix[1][i - 1])) {
                return false;
            }
            
            matrix[0][i] /= matrix[1][i - 1];
            matrix[1][i] -= matrix[0][i] * matrix[2][i - 1];
        }
        
        if (!isnormal(matrix[1][matrixDimension - 1])) {
            return false;
        }
    }
    
    for (int i = 1; i < matrixDimension; i++) {
        vector[i] -= matrix[0][i] * vector[i - 1];
    }
    
    vector[matrixDimension - 1] /= matrix[1][matrixDimension - 1];
    
    for (long i = matrixDimension - 2; i >= 0; i--) {
        vector[i] = (vector[i] - (matrix[2][i] * vector[i + 1])) / matrix[1][i];
    }
    
    return true;
}

/**
 * Solves a linear equation system with cyclic three diagonal coefficent matrix.
 *
 * @param matrixDimension The matrix dimension.
 * @param matrix The coefficent matrix. If firstCalled is set true, it is inverted.
 * @param vector The vector of results.
 * @param firstCalled If set to <c>true</c>, matrix will be inverted.
 * @returns False if the matrix is too small or singular. True without error.
 */
static bool CyclicTriagonalElimination(size_t matrixDimension, double **matrix, double *vector, bool firstCalled) {
    if (matrixDimension < 3) {
        return false;
    }
    
    double temp = 0.0;
    if (firstCalled) {
        matrix[0][0] = matrix[1][0];
        matrix[4][0] = matrix[3][matrixDimension - 1];
        matrix[1][0] = matrix[3][matrixDimension - 1] = 0;
        if (!isnormal(matrix[2][0])) {
            return false;
        }
        
        matrix[3][0] /= matrix[2][0];
        matrix[4][0] /= matrix[2][0];
        
        for (int i = 1; i < matrixDimension - 2; i++) {
            matrix[2][i] -= matrix[1][i] * matrix[3][i - 1];
            if (!isnormal(matrix[2][i])) {
                return false;
            }
            
            matrix[3][i] /= matrix[2][i];
            matrix[4][i] = -matrix[1][i] * matrix[4][i - 1] / matrix[2][i];
        }
        
        matrix[2][matrixDimension - 2] -= matrix[1][matrixDimension - 2] * matrix[3][matrixDimension - 3];
        if (!isnormal(matrix[2][matrixDimension - 2])) {
            return false;
        }
        
        for (int i = 1; i < matrixDimension - 2; i++) {
            matrix[0][i] = -matrix[0][i - 1] * matrix[3][i - 1];
        }
        
        matrix[1][matrixDimension - 1] -= matrix[0][matrixDimension - 3] * matrix[3][matrixDimension - 3];
        matrix[3][matrixDimension - 2] = (matrix[3][matrixDimension - 2] - (matrix[1][matrixDimension - 2] * matrix[4][matrixDimension - 3])) / matrix[2][matrixDimension - 2];
        
        temp = 0.0;
        for (int i = 0; i < matrixDimension - 2; i++) {
            temp -= matrix[0][i] * matrix[4][i];
        }
        
        matrix[2][matrixDimension - 1] += temp - (matrix[1][matrixDimension - 1] * matrix[3][matrixDimension - 2]);
        if (!isnormal(matrix[2][matrixDimension - 1])) {
            return false;
        }
    }
    
    vector[0] /= matrix[2][0];
    for (int i = 1; i < matrixDimension - 1; i++) {
        vector[i] = (vector[i] - (vector[i - 1] * matrix[1][i])) / matrix[2][i];
    }
    
    temp = 0.0;
    for (int i = 0; i < matrixDimension - 2; i++) {
        temp -= matrix[0][i] * vector[i];
    }
    
    vector[matrixDimension - 1] = (vector[matrixDimension - 1] + temp - (matrix[1][matrixDimension - 1] * vector[matrixDimension - 2])) / matrix[2][matrixDimension - 1];
    vector[matrixDimension - 2] -= vector[matrixDimension - 1] * matrix[3][matrixDimension - 2];
    
    for (long i = matrixDimension - 3; i >= 0; i--) {
        vector[i] -= (matrix[3][i] * vector[i + 1]) + (matrix[4][i] * vector[matrixDimension - 1]);
    }
    
    return true;
}


/**
 * Computes the distance from a point to a line.
 *
 * @param lineStartPoint The line start point.
 * @param lineEndPoint The line end point.
 * @param point The point.
 * @return The distance from the point to the line.</returns>
 */
static double PointToLineDistance(CGPoint lineStartPoint, CGPoint lineEndPoint, CGPoint point) {
    if (CGPointEqualToPoint(lineStartPoint, lineEndPoint)) {
        CGFloat dx = lineEndPoint.x - point.x;
        CGFloat dy = lineEndPoint.y - point.y;
        return sqrt(dx * dx + dy * dy);
    }
    CGFloat dx = lineEndPoint.x - lineStartPoint.x;
    CGFloat dy = lineEndPoint.y - lineStartPoint.y;
    return fabs(dx * (lineStartPoint.y - point.y) - (lineStartPoint.x - point.x) * dy) / sqrt(dx * dx + dy * dy);
}

/**
 * Computes the distance^2 from a point to a line.
 *
 * @param lineStartPoint The line start point.
 * @param lineEndPoint The line end point.
 * @param point The point.
 * @return The distance^2 from the point to the line.</returns>
 */
static double PointToLineDistancePow(CGPoint lineStartPoint, CGPoint lineEndPoint, CGPoint point) {
    if (CGPointEqualToPoint(lineStartPoint, lineEndPoint)) {
        CGFloat dx = lineEndPoint.x - point.x;
        CGFloat dy = lineEndPoint.y - point.y;
        return dx * dx + dy * dy;
    }
    
    double x1 = lineStartPoint.x;
    double y1 = lineStartPoint.y;
    double x2 = lineEndPoint.x;
    double y2 = lineEndPoint.y;
    double d1 = ((x2 - x1) * (y1 - point.y)) - ((x1 - point.x) * (y2 - y1));
    d1 *= d1;
    double tx = x2 - x1;
    tx *= tx;
    double ty = y2 - y1;
    ty *= ty;
    return d1 / (tx + ty);
}

/**
 * Gets the flattened shape.
 *
 * @param oldShapePoints The old shape points.
 * @param newShapePoints The new shape points.
 * @param tolerance The tolerance.
 */
static void GetFlattenedShape(const CGPoint *oldShapePoints, size_t oldShapePointsCount, CFMutableDataRef newShapePoints, double tolerance) {
    if (oldShapePoints == NULL || newShapePoints == NULL) {
        return;
    }
    
    if (oldShapePointsCount < 3) {
        CFDataAppendBytes(newShapePoints, (const UInt8 *)oldShapePoints, sizeof(CGPoint) * oldShapePointsCount);
        return;
    }
    
    CFDataAppendBytes(newShapePoints, (const UInt8 *)oldShapePoints, sizeof(CGPoint));
    
    double tolerancePow = tolerance * tolerance;
    for (int j = 0; j < oldShapePointsCount - 1; j++) {
        CGPoint p1 = oldShapePoints[j];
        for (int i = j + 2; i < oldShapePointsCount; i++) {
            CGPoint p3 = oldShapePoints[i];
            double maxDistance = DBL_MIN;
            int maxId = -1;
            for (int k = j + 1; k < i; k++) {
                CGPoint p2 = oldShapePoints[k];
                double d = PointToLineDistancePow(p1, p3, p2);
                if (d >= tolerancePow && d > maxDistance) {
                    maxDistance = d;
                    maxId = k;
                    break;
                }
            }
            
            if (maxId != -1) {
                CFDataAppendBytes(newShapePoints, (const UInt8 *)(oldShapePoints + maxId), sizeof(CGPoint));
                j = maxId - 1;
                break;
            }
        }
    }
    
    CFDataAppendBytes(newShapePoints, (const UInt8 *)(oldShapePoints + oldShapePointsCount - 1), sizeof(CGPoint));
}

/**
 * Computes the bezier segment curve points from the specified list of 4 control points that are in following sequence:
 *  SP - CP - CP - SP
 * Approximates the bezier curve with a polygon (or polyline) depending on the parameters to reduce the number of points.
 * The start and end points (SP) remain unchanged.
 *
 * @param controlPoints The control points.
 * @param flattened If true, approximate the bezier curve with a polygon. Otherwise, return all curve points.
 * @param tolerance The tolerance (default 0.1) for approximation of a polygon to the bezier curve.
 * @return The bezier segment curve points.
 */
CF_RETURNS_RETAINED
static CFDataRef ComputeBezierSegmentCurvePoints(const CGPoint *controlPoints, size_t controlPointsCount, bool flattened, double tolerance) {
    if (controlPoints == NULL) {
        return NULL;
    }
    
    if (controlPointsCount != 4) {
        return NULL;
    }
    
    CFMutableDataRef segmentCurvePoints = NULL;
    
    // estimate the points count according to the distance between the control points
    double distance = 0;
    for (int i = 0; i < controlPointsCount - 1; i++) {
        int j = i + 1;
        const CGPoint *p1 = &controlPoints[i];
        const CGPoint *p2 = &controlPoints[j];
        double dx = p1->x - p2->x;
        double dy = p1->y - p2->y;
        distance += sqrt(dx * dx + dy * dy);
    }
    
    int estimatePointsCount = distance < 1 ? 1 : (int)distance;
    if (estimatePointsCount == 1) {
        CFDataRef temp = CFDataCreate(kCFAllocatorDefault, (const UInt8 *)controlPoints, sizeof(CGPoint) * controlPointsCount);
        segmentCurvePoints = CFDataCreateMutableCopy(kCFAllocatorDefault, 0, temp);
        CFRelease(temp);
    } else {
        double dt = 1.0 / estimatePointsCount;
        CGPoint p0 = controlPoints[0];
        CGPoint p1 = controlPoints[1];
        CGPoint p2 = controlPoints[2];
        CGPoint p3 = controlPoints[3];
        CFMutableDataRef points = CFDataCreateMutable(kCFAllocatorDefault, 0);
        for (int i = 1; i < estimatePointsCount - 1; i++) {
            double t = dt * i;
            double t1 = 1 - t;
            double t2 = t1 * t1;
            double t3 = t2 * t1;
            
            double a0 = t3;
            double a1 = 3 * t2 * t;
            double a2 = 3 * t1 * t * t;
            double a3 = t * t * t;
            
            CGPoint pt;
            pt.x = a0 * p0.x + a1 * p1.x + a2 * p2.x + a3 * p3.x;
            pt.y = a0 * p0.y + a1 * p1.y + a2 * p2.y + a3 * p3.y;
            CFDataAppendBytes(points, (UInt8 *)&pt, sizeof(CGPoint));
        }
        
        segmentCurvePoints = CFDataCreateMutable(kCFAllocatorDefault, 0);
        CFDataAppendBytes(segmentCurvePoints, (UInt8 *)&p0, sizeof(CGPoint));
        
        if (flattened) {
            CFMutableDataRef flattenedShapePoints = CFDataCreateMutable(kCFAllocatorDefault, 0);
            GetFlattenedShape((const CGPoint *)CFDataGetBytePtr(points), CFDataGetLength(points) / sizeof(CGPoint), flattenedShapePoints, tolerance);
            CFDataAppendBytes(segmentCurvePoints, CFDataGetBytePtr(flattenedShapePoints), CFDataGetLength(flattenedShapePoints));
            CFRelease(flattenedShapePoints);
        } else {
            CFDataAppendBytes(segmentCurvePoints, CFDataGetBytePtr(points), CFDataGetLength(points));
        }
        
        CFRelease(points);
        
        CFDataAppendBytes(segmentCurvePoints, (UInt8 *)&p3, sizeof(CGPoint));
    }
    
    return segmentCurvePoints;
}

/**
 * Computes the bezier curve points from the specified list of control points that are in following sequence:
 *  SP - CP - CP - SP - CP - CP - SP ...
 * Approximates the bezier curve with a polygon (or polyline) depending on the parameters to reduce the number of points.
 *
 * @param controlPoints The control points.
 * @param flattened If true, approximate the bezier curve with a polygon. Otherwise, return all curve points.
 * @param tolerance The tolerance (default 0.1) for approximation of a polygon to the bezier curve.
 * @return The bezier curve points.
 */
CF_RETURNS_RETAINED
static CFDataRef ComputeBezierCurvePointsFromControlPoints(const CGPoint *controlPoints, size_t controlPointsCount, bool flattened, double tolerance) {
    if (controlPoints == NULL) {
        return NULL;
    }
    
    if (controlPointsCount < 4) {
        return CFDataCreate(kCFAllocatorDefault, (const UInt8 *)controlPoints, sizeof(CGPoint) * controlPointsCount);
    }
    
    CFMutableDataRef curvePoints = CFDataCreateMutable(kCFAllocatorDefault, 0);
    size_t m = 3;
    size_t n = controlPointsCount - m;
    size_t k = 0;
    while (k < n) {
        const CGPoint *segmentControlPoints = controlPoints + k;
        size_t end = k + m;
        
        CFDataRef segmentCurvePoints = ComputeBezierSegmentCurvePoints(segmentControlPoints, m + 1, flattened, tolerance);
        if (segmentCurvePoints != NULL) {
            CFIndex length = CFDataGetLength(segmentCurvePoints);
            // remove the last point of the current segment since that will be the start point of the next segment
            if (end < n) {
                length -= sizeof(CGPoint);
            }
            
            const UInt8 *bytes = CFDataGetBytePtr(segmentCurvePoints);
            CFDataAppendBytes(curvePoints, bytes, length);
            
            CFRelease(segmentCurvePoints);
        }
        
        k = end;
    }
    
    return curvePoints;
}

#pragma mark - implementations

/**
 * Computes the open bezier control points from the specified list of curve segment start/end points (SP)
 * The returned control points list has the start point of the first curve segment following with
 * two control points (CP) and then the end point.The end point is the start point of the second segment.
 * This way we have a sequence :
 *   SP - CP - CP - SP - CP - CP - SP ...
 *
 * @param endPoints The input end points.
 * @param endPointsCount The input end points count.
 * @param controlPoints The output control points.
 */
void ComputeOpenBezierControlPoints(const CGPoint *endPoints, size_t endPointsCount, CGPoint *controlPoints) {
    if (!endPoints || endPointsCount == 0 || !controlPoints) {
        return;
    }
    
    double *vecX = malloc(endPointsCount * sizeof(double));
    double *vecY = malloc(endPointsCount * sizeof(double));
    
    double **coefMatrix = malloc(3 * sizeof(double *));
    coefMatrix[0] = malloc(endPointsCount * sizeof(double));
    coefMatrix[1] = malloc(endPointsCount * sizeof(double));
    coefMatrix[2] = malloc(endPointsCount * sizeof(double));
    
    for (int i = 0; i < endPointsCount; i++) {
        vecX[i] = endPoints[i].x;
        vecY[i] = endPoints[i].y;
    }
    
    for (int i = 0; i < endPointsCount - 1; i++) {
        coefMatrix[0][i] = 3.0;
    }
    
    for (int i = 1; i < endPointsCount - 1; i++) {
        coefMatrix[1][i] = 12.0;
    }
    
    for (int i = 1; i < endPointsCount - 1; i++) {
        coefMatrix[2][i] = coefMatrix[0][i - 1];
    }
    
    coefMatrix[0][0] = 0.0;
    coefMatrix[1][0] = 2.0;
    coefMatrix[2][0] = 1.0;
    
    coefMatrix[0][endPointsCount - 1] = 1.0;
    coefMatrix[1][endPointsCount - 1] = 2.0;
    coefMatrix[2][endPointsCount - 1] = 0.0;
    
    for (int i = 1; i < endPointsCount - 1; i++) {
        vecX[i] *= 18.0;
        vecY[i] *= 18.0;
    }
    
    vecX[0] *= 3.0;
    vecY[0] *= 3.0;
    vecX[endPointsCount - 1] *= 3.0;
    vecY[endPointsCount - 1] *= 3.0;
    
    size_t resultPointsCount = ((endPointsCount - 1) * 3) + 1;
    CGPoint *resultPoints = malloc(resultPointsCount * sizeof(CGPoint));
    
    if (TriagonalElimination(endPointsCount, coefMatrix, vecX, true) &&
        TriagonalElimination(endPointsCount, coefMatrix, vecY, false)) {
        for (int i = 0; i < endPointsCount; i++) {
            resultPoints[3 * i] = endPoints[i];
        }
        
        for (int i = 0; i < endPointsCount - 1; i++) {
            resultPoints[(3 * i) + 1] = CGPointMake((vecX[i + 1] + (2 * vecX[i])) / 3.0, (vecY[i + 1] + (2 * vecY[i])) / 3.0);
            resultPoints[(3 * i) + 2] = CGPointMake(((2 * vecX[i + 1]) + vecX[i]) / 3.0, ((2 * vecY[i + 1]) + vecY[i]) / 3.0);
        }
    }
    
    for (int i = 0; i < resultPointsCount; i++) {
        controlPoints[i] = resultPoints[i];
    }
    
    free(vecX);
    free(vecY);
    free(resultPoints);
    free(coefMatrix[0]);
    free(coefMatrix[1]);
    free(coefMatrix[2]);
    free(coefMatrix);
}

/**
 * Computes the close bezier control points from the specified list of curve segment start/end points (SP)
 * The returned control points list has the start point of the first curve segment following with
 * two control points (CP) and then the end point.The end point is the start point of the second segment.
 * This way we have a sequence :
 *   SP - CP - CP - SP - CP - CP - SP ...
 *
 * @param endPoints The input end points.
 * @param endPointsCount The input end points count.
 * @param controlPoints The output control points.
 */
void ComputeCloseBezierControlPoints(const CGPoint *endPoints, size_t endPointsCount, CGPoint *controlPoints) {
    if (!endPoints || endPointsCount < 1 || !controlPoints) {
        return;
    }
    
    double *vecX = malloc((endPointsCount + 1) * sizeof(double));
    double *vecY = malloc((endPointsCount + 1) * sizeof(double));
    
    double **coefMatrix = malloc(5 * sizeof(double *));
    coefMatrix[0] = malloc(endPointsCount * sizeof(double));
    coefMatrix[1] = malloc(endPointsCount * sizeof(double));
    coefMatrix[2] = malloc(endPointsCount * sizeof(double));
    coefMatrix[3] = malloc(endPointsCount * sizeof(double));
    coefMatrix[4] = malloc(endPointsCount * sizeof(double));
    
    for (int i = 0; i < endPointsCount; i++) {
        vecX[i] = endPoints[i].x * 18.0;
        vecY[i] = endPoints[i].y * 18.0;
    }
    
    for (int i = 0; i < endPointsCount; i++) {
        coefMatrix[1][i] = 3.0;
        coefMatrix[2][i] = 12.0;
        coefMatrix[3][i] = 3.0;
    }
    
    size_t resultPointsCount = (endPointsCount * 3) + 1;
    CGPoint *resultPoints = malloc(sizeof(CGPoint) * resultPointsCount);
    
    if (CyclicTriagonalElimination(endPointsCount, coefMatrix, vecX, true) &&
        CyclicTriagonalElimination(endPointsCount, coefMatrix, vecY, false)) {
        for (int i = 0; i < endPointsCount; i++) {
            resultPoints[3 * i] = endPoints[i];
        }
        
        resultPoints[3 * endPointsCount] = endPoints[0];
        vecX[endPointsCount] = vecX[0];
        vecY[endPointsCount] = vecY[0];
        
        for (int i = 0; i < endPointsCount; i++) {
            resultPoints[(3 * i) + 1] = CGPointMake((vecX[i + 1] + (2 * vecX[i])) / 3, (vecY[i + 1] + (2 * vecY[i])) / 3.0);
            resultPoints[(3 * i) + 2] = CGPointMake(((2 * vecX[i + 1]) + vecX[i]) / 3, ((2 * vecY[i + 1]) + vecY[i]) / 3.0);
        }
    }
    
    for (int i = 0; i < resultPointsCount; i++) {
        controlPoints[i] = resultPoints[i];
    }
    
    free(vecX);
    free(vecY);
    free(resultPoints);
    free(coefMatrix[0]);
    free(coefMatrix[1]);
    free(coefMatrix[2]);
    free(coefMatrix[3]);
    free(coefMatrix[4]);
    free(coefMatrix);
}

CF_RETURNS_RETAINED
CFDataRef ComputeBezierCurvePoints(const CGPoint *endPoints, size_t endPointsCount, bool closed, bool flattened) {
    return ComputeBezierCurvePointsWithTolerance(endPoints, endPointsCount, closed, flattened, 0.1);
}

CF_RETURNS_RETAINED
CFDataRef ComputeBezierCurvePointsWithTolerance(const CGPoint *endPoints, size_t endPointsCount, bool closed, bool flattened, double tolerance) {
    size_t controlPointsCount = endPointsCount * 3 + 1;
    CGPoint *controlPoints = malloc(sizeof(CGPoint) * controlPointsCount);

    if (closed) {
        ComputeCloseBezierControlPoints(endPoints, endPointsCount, controlPoints);
    } else {
        controlPointsCount = endPointsCount * 3 - 2;
        ComputeOpenBezierControlPoints(endPoints, endPointsCount, controlPoints);
    }

    CFDataRef result = ComputeBezierCurvePointsFromControlPoints(controlPoints, controlPointsCount, flattened, tolerance);
    
    free(controlPoints);
    return result;
}
