//
//  CZNodeMultiMarks.h
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"

@interface CZNodeMultiMarks : CZNodeMultiPointShape

@property (nonatomic, assign) CGFloat markSize;

@end
