//
//  CZColor.m
//  Hermes
//
//  Created by Ralph Jin on 1/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZColor.h"

static inline CGFloat rgbToFloat(uint8_t value) {
    return value / 255.0f;
}

static inline uint8_t floatToRGB(CGFloat value) {
    return (uint8_t)(value * 255.0f);
}

CGColorRef CGColorFromCZColor(CZColor color) {
    return UIColorFromCZColor(color).CGColor;
}

UIColor *UIColorFromCZColor(CZColor color) {
    return [UIColor colorWithRed:rgbToFloat(color.r)
                           green:rgbToFloat(color.g)
                            blue:rgbToFloat(color.b)
                           alpha:rgbToFloat(color.a)];
}

CZColor CZColorFromUIColor(UIColor *color) {
    CGFloat red, green, blue, alpha;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    return CZColorMake(floatToRGB(red), floatToRGB(green), floatToRGB(blue));
}

NSString *NSStringFromColor(CZColor color) {
    return [NSString stringWithFormat:@"#%02X%02X%02X%02X", color.a, color.r, color.g, color.b];
}

CZColor CZColorFromString(NSString *string) {
    NSString *colorString = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner = [[NSScanner alloc] initWithString:colorString];
    uint32_t hexValue = 0;
    [scanner scanHexInt:&hexValue];
    [scanner release];
    
    uint8_t red, green, blue, alpha;
    alpha = (uint8_t)((hexValue & 0xFF000000) >> 24);
    red = (uint8_t)((hexValue & 0xFF0000) >> 16);
    green = (uint8_t)((hexValue & 0xFF00) >> 8);
    blue = (uint8_t)(hexValue & 0xFF);
    
    return CZColorMakeRGBA(red, green, blue, alpha);
}

@implementation NSValue (CZColor)

+ (instancetype)valueWithCZColor:(CZColor)color {
    return [self valueWithBytes:&color objCType:@encode(CZColor)];
}

- (CZColor)CZColorValue {
    CZColor color;
    [self getValue:&color];
    return color;
}

@end
