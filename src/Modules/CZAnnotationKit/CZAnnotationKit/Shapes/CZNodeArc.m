//
//  CZNodeArc.m
//  Matscope
//
//  Created by Ralph Jin on 11/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeArc.h"
#import <CZToolbox/CZToolbox.h>

const static CGFloat kDefaultRadius = 25;

@implementation CZNodeArc

- (id)init {
    self = [super init];
    if (self) {
        _cx = 0.0;
        _cy = 0.0;
        _radius = kDefaultRadius;
        _startAngle = 0.0;
        _endAngle = M_PI_2;
        
        [self addObserver:self forKeyPaths:@"cx", @"cy", @"radius", @"startAngle", @"endAngle", nil];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPaths:@"cx", @"cy", @"radius", @"startAngle", @"endAngle", nil];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeArc *newArc = [super copyWithZone:zone];
    if (newArc) {
        newArc->_cx = self->_cx;
        newArc->_cy = self->_cy;
        newArc->_endAngle = self->_endAngle;
        newArc->_radius = self->_radius;
        newArc->_startAngle = self->_startAngle;
    }
    return newArc;
}

#pragma mark - override CZNode

- (CGPoint)anchorPoint {
    return CGPointMake(_cx, _cy);
}

#pragma mark - override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathAddArc(relativePath, NULL, _cx, _cy, _radius, _endAngle, _startAngle, true);
    return relativePath;
}

@end
