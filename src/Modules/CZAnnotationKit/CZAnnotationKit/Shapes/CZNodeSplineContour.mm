//
//  CZNodeSplineContour.mm
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeSplineContour.h"
#import "CZNodeGeom.h"

@implementation CZNodeSplineContour

#pragma mark -
#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathAddSpline(relativePath, NULL, [self pointArray], [self pointsCount], true);
    if ([self pointsCount] > 2) {
        CGPathCloseSubpath(relativePath);
    }
    return relativePath;
}

@end
