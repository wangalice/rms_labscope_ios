//
//  CZNodeEllipse.h
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"

@interface CZNodeEllipse : CZNodeShape <CZNodeShapeFillable>

@property (nonatomic) CGFloat cx;
@property (nonatomic) CGFloat cy;
@property (nonatomic) CGFloat rx;
@property (nonatomic) CGFloat ry;

- (id)initWithCenterX:(CGFloat)cx
              centerY:(CGFloat)cy
              radiusX:(CGFloat)rx
              radiusY:(CGFloat)ry;

@end
