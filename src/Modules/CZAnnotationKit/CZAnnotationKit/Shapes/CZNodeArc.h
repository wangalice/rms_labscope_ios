//
//  CZNodeArc.h
//  Matscope
//
//  Created by Ralph Jin on 11/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"

// Node class of arc, (cx, cy) is the center point;

@interface CZNodeArc : CZNodeShape

@property (nonatomic) CGFloat cx;
@property (nonatomic) CGFloat cy;
@property (nonatomic) CGFloat radius;
@property (nonatomic) CGFloat startAngle;
@property (nonatomic) CGFloat endAngle;

@end
