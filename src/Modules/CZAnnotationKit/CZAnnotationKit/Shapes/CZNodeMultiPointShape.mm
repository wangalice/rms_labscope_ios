//
//  CZNodeMultiPointShape.m
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"
#include <vector>

@interface CZNodeMultiPointShape() {
    std::vector<CGPoint> *_points;
}
@end

@implementation CZNodeMultiPointShape

- (id)init {
    self = [super init];
    if (self) {
        _points = new std::vector<CGPoint>();
    }
    return self;
}

- (id)initWithPointsCount:(NSUInteger)count {
    self = [super init];
    if (self) {
        _points = new std::vector<CGPoint>();
        _points->resize(count);
    }
    return self;
}

- (void)dealloc {
    if (_points) {
        delete _points;
        _points = NULL;
    }
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeMultiPointShape *newShape = [super copyWithZone:zone];
    if (newShape) {
        *newShape->_points = *self->_points;
    }
    
    return newShape;
}

- (NSUInteger)pointsCount {
    return _points->size();
}

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index {
    if (index < _points->size()) {
        (*_points)[index] = pt;
    }
    
    [self invalidateRelativePath];
}

- (CGPoint)pointAtIndex:(NSUInteger)index {
    if (index < _points->size()) {
        return (*_points)[index];
    } else {
        return CGPointZero;
    }
}

- (void)appendPoint:(CGPoint)pt {
    _points->push_back(pt);
    [self invalidateRelativePath];
}

- (void)removePointAtIndex:(NSUInteger)index {
    if (index < _points->size()) {
        std::vector<CGPoint>::iterator it = _points->begin() + index;
        _points->erase(it);
        [self invalidateRelativePath];
    }
}

- (void)removeAllPoints {
    if (!_points->empty()) {
        _points->clear();
        [self invalidateRelativePath];
    }
}

- (const CGPoint *)pointArray {
    return &(*_points)[0];
}

@end
