//
//  CZNode.h
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "CZColor.h"

#define EPSILON 1e-5

/*! virtual class of Node object.*/
@interface CZNode : NSObject <NSCopying>

/*! identifier of the node. Since id is a key word in objc, we use identifier instead. */
@property (nonatomic, copy) NSString *identifier;

@property (nonatomic, assign) CZNode *parentNode;

/*! assume rotate at center of the relativeBounds. */
@property (nonatomic, assign) CGFloat rotateAngle;

@property (nonatomic, assign) CZColor strokeColor;
@property (nonatomic, assign) CZColor fillColor;
@property (nonatomic, readonly) BOOL isFilled;

/*! @return sub CZShapeNode count. */
- (NSUInteger)subNodeCount;

- (BOOL)addSubNode:(CZNode *)node;

- (CZNode *)subNodeAtIndex:(NSUInteger)index;

- (BOOL)removeFromParent;

- (CGAffineTransform)transformAbsolute;

- (CGAffineTransform)transformSelf;  // Sub-class shall override

/*! boundary in parent coordinate.*/
- (CGRect)frame;

/*! Get the boundary box in self coordinate.*/
- (CGRect)boundary;  // Sub-class shall override this method to provide boundary in self coordinate.

- (BOOL)hitTest:(CGPoint)p tolerance:(CGFloat)tolerance;

/*! hitTest in node's own coordinate*/
- (BOOL)hitTestSelf:(CGPoint)pointOfSelf tolerance:(CGFloat)tolerance;

/*! Check if the closed shape (either filled or not) contains the point. If test on an opened shape, it always return NO.*/
- (BOOL)containsPoint:(CGPoint)p;

/*! Check if the closed shape contains the point in node's own coordinate. Sub-class shall override this method.*/
- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf;

/*!  create rendering layers tree from this node.*/
- (CALayer *)newLayer;

/*! sub class shall override this method to create its own layer*/
- (CALayer *)newOwnLayer;

/*! sub class shall override this method to update its own layer*/
- (void)updateOwnLayer:(CALayer *)ownLayer;

/*!  anchor point for rotate and transform in parent's coordinate. Sub-class shall override. */
- (CGPoint)anchorPoint;

@end

@protocol CZNodeShapeFillable <NSObject>
@end
