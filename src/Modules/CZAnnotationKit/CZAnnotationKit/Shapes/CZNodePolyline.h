//
//  CZNodePolyline.h
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"

@interface CZNodePolyline : CZNodeMultiPointShape

@end
