//
//  CZNodeText.m
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeText.h"
#import <CoreText/CoreText.h>
#include "CZNodeGeom.h"

static const CGFloat kMaxTextWidth = 10000.0f;  // If use CGFLOAT_MAX here, the text will disappear. So we use a big enough value for text showing.
static const CGFloat kMaxTextHeight = kMaxTextWidth;

static CGSize CalcSuggestSizeOfText(NSString *text, NSString *fontName, CGFloat fontSize, CGFloat suggestWidth) {
    if (text == nil || fontName == nil) {
        return CGSizeZero;
    }

    CTFontRef font = CTFontCreateWithName((CFStringRef) fontName, fontSize, NULL);
    NSMutableAttributedString *tempString = [[NSMutableAttributedString alloc] initWithString:text];
    [tempString addAttribute:(NSString *)kCTFontAttributeName
                       value:(id)font
                       range:NSMakeRange(0, tempString.string.length)];

    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFMutableAttributedStringRef) tempString);
    [tempString release];

    CGSize suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), NULL, CGSizeMake(suggestWidth, CGFLOAT_MAX), NULL);

    CFRelease(framesetter);
    CFRelease(font);
    return suggestedSize;
}

@implementation CZNodeText

- (id)init {
    self = [super init];
    if (self) {
        _x = 0.0f;
        _y = 0.0f;
        _width = kMaxTextWidth;
        _height = kMaxTextHeight;
        self.string = @"";
        
        _fontFamily = @"HelveticaNeue";
        _fontSize = 17;
        _wrapped = NO;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super init];
    if (self) {
        _x = frame.origin.x;
        _y = frame.origin.y;
        _width = frame.size.width;
        _height = frame.size.height;
        self.string = @"";
        
        _fontFamily = @"HelveticaNeue";
        _fontSize = 17;
        _wrapped = NO;
    }
    return self;
}

- (void)dealloc {
    self.string = nil;
    self.fontFamily = nil;
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeText *newText = [super copyWithZone:zone];
    if (newText) {
        newText.fontFamily = self.fontFamily;
        newText->_fontSize = self->_fontSize;
        newText->_height = self->_height;
        newText.string = self.string;
        newText->_width = self->_width;
        newText->_wrapped = self->_wrapped;
        newText->_x = self->_x;
        newText->_y = self->_y;
    }
    
    return newText;
}

#pragma mark -
#pragma mark Override CZNode

- (CGAffineTransform)transformSelf {
    CGAffineTransform translateTransform = CGAffineTransformMakeTranslation(_x, _y);
    
    if (self.rotateAngle == 0) {
        return translateTransform;
    } else {
        CGPoint rotateCenter;
        rotateCenter.x = _width * 0.5;
        rotateCenter.y = _height * 0.5;
        CGAffineTransform rotateTransform = CGAffineTransformMakeRotationAtPoint(self.rotateAngle, rotateCenter);
        
        // combine the rotateTransform
        CGAffineTransform result = CGAffineTransformConcat(rotateTransform, translateTransform);
        return result;
    }
}

- (CGRect)boundary {
    return CGRectMake(0, 0, _width, _height);
}

- (BOOL)hitTestSelf:(CGPoint)pointOfSelf tolerance:(CGFloat)tolerance {
    CGFloat tolerance2 = tolerance + tolerance;
    CGRect rect = CGRectMake(0.0f - tolerance, 0.0f - tolerance,
                             _width + tolerance2, _height + tolerance2);
    
    return CGRectContainsPoint(rect, pointOfSelf);
}

- (CALayer *)newOwnLayer {
    CATextLayer *layer = [[CATextLayer alloc] init];
    [self updateOwnLayer:layer];

    return layer;
}

- (void)updateOwnLayer:(CALayer *)ownLayer {
    if (![ownLayer isKindOfClass:[CATextLayer class]]) {
        return;
    }
    
    const CGFloat zoomScale = ([[UIScreen mainScreen] scale] >= 2.0) ? 1.0 : 2.0;  // increase zoom scale for non-retina screen
    
    CATextLayer *layer = (CATextLayer *)ownLayer;
    layer.name = self.identifier;
    
    layer.string = self.string;
    layer.fontSize = self.fontSize * zoomScale;
    
    if (self.strokeColor.a == 0) {
        layer.foregroundColor = nil;
    } else {
        layer.foregroundColor = CGColorFromCZColor(self.strokeColor);
    }
    if (self.fillColor.a == 0) {
        layer.backgroundColor = nil;
    } else {
        layer.backgroundColor = CGColorFromCZColor(self.fillColor);
    }

    layer.contentsScale = [[UIScreen mainScreen] scale];
    
    CTFontRef font = CTFontCreateWithName((CFStringRef) self.fontFamily, self.fontSize, NULL);
    layer.font = font;
    CFRelease(font);
    
    layer.frame = CGRectMake(_x, _y, _width, _height);
    layer.bounds = CGRectMake(0, 0, _width * zoomScale, _height * zoomScale);
    layer.wrapped = self.wrapped;
    if (layer.wrapped) {
        layer.truncationMode = kCATruncationEnd;
    } else {
        layer.truncationMode = kCATruncationNone;
    }
    
    CATransform3D t = (zoomScale == 2.f) ? CATransform3DMakeScale(1.0 / zoomScale, 1.0 / zoomScale, 1.0) : CATransform3DIdentity;
    if (fabs(self.rotateAngle) > EPSILON) {
        t = CATransform3DRotate(t, self.rotateAngle, 0.0, 0.0, 1.0);
    }
    layer.transform = t;
    
    layer.contentsScale = [[UIScreen mainScreen] scale];
    [layer removeAllAnimations];
}

- (CGPoint)anchorPoint {
    CGPoint p;
    p.x = _x + _width * 0.5;
    p.y = _y + _height * 0.5;
    return p;
}

- (void)setSuggestSize {
    CGFloat suggestWidth = self.wrapped ? self.width : CGFLOAT_MAX;
    NSAssert(self.fontFamily, @"font family can't be nil!");
    CGSize suggetstSize = CalcSuggestSizeOfText(self.string, self.fontFamily, self.fontSize, suggestWidth);
    self.width = ceil(suggetstSize.width);
    self.height = ceil(suggetstSize.height);
}

@end
