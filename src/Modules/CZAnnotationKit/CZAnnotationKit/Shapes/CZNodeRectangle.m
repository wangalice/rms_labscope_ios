//
//  CZNodeRectangle.m
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeRectangle.h"
#import <CZToolbox/CZToolbox.h>

@implementation CZNodeRectangle


- (id)init {
    self = [super init];
    if (self) {
        _x = 0;
        _y = 0;
        _width = 50;
        _height = 50;
        _cornerRadius = 0;
        [self addObserver:self forKeyPaths:@"x", @"y", @"width", @"height", nil];
    }
    return self;
}

- (id)initWithCGRect:(CGRect)rect {
    self = [super init];
    if (self) {
        _x = rect.origin.x;
        _y = rect.origin.y;
        _width = rect.size.width;
        _height = rect.size.height;
        _cornerRadius = 0;
        [self addObserver:self forKeyPaths:@"x", @"y", @"width", @"height", nil];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPaths:@"x", @"y", @"width", @"height", nil];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeRectangle *newRectangle = [super copyWithZone:zone];
    if (newRectangle) {
        newRectangle->_cornerRadius = self->_cornerRadius;
        newRectangle->_height = self->_height;
        newRectangle->_width = self->_width;
        newRectangle->_x = self->_x;
        newRectangle->_y = self->_y;
    }
    return newRectangle;
}

#pragma mark -
#pragma mark override CZNode

- (BOOL)hitTestSelf:(CGPoint)pointOfSelf tolerance:(CGFloat)tolerance {
    CGFloat halfLineWidth = self.lineWidth / 2.0;
    if (tolerance < halfLineWidth) {
        tolerance = halfLineWidth;
    }
    
    const CGFloat tolerance2 = tolerance + tolerance;
    
    if (self.isFilled || tolerance2 >= _width || tolerance2 >= _height) {
        // If rectangle is filled, inside rectangle means hit on.
        CGRect rect = CGRectMake(-tolerance, -tolerance,
                             _width + tolerance2, _height + tolerance2);

        return CGRectContainsPoint(rect, pointOfSelf);
    } else {
        // If rectangle is not filled, hit on outline means hit on.
        CGRect outRect = CGRectMake(- tolerance, - tolerance,
                                    _width + tolerance2, _height + tolerance2);
        
        CGRect innerRect = CGRectMake(tolerance, tolerance,
                                      _width - tolerance2, _height - tolerance2);
        
        return CGRectContainsPoint(outRect, pointOfSelf) && (!CGRectContainsPoint(innerRect, pointOfSelf));
    }
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    CGRect rect = CGRectMake(0, 0, _width, _height);
    return CGRectContainsPoint(rect, pointOfSelf);
}

- (CGPoint)anchorPoint {
    CGPoint p;
    p.x = _x + _width * 0.5;
    p.y = _y + _height * 0.5;
    return p;
}

#pragma mark -
#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    if (_cornerRadius > 0.0) {
        UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(_x, _y, _width, _height)
                                                              cornerRadius:_cornerRadius];
        CGPathAddPath(relativePath, NULL, [bezierPath CGPath]);
        // TODO: use CGPathAddRoundedRect when minimum iOS version is 7.0
//        CGPathAddRoundedRect(relativePath, NULL, CGRectMake(_x, _y, _width, _height), _cornerRadius, _cornerRadius);
    } else {
        CGPathAddRect(relativePath, NULL, CGRectMake(_x, _y, _width, _height));
    }
    return relativePath;
}

@end
