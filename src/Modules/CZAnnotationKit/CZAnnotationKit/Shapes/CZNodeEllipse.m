
//
//  CZNodeEllipse.m
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeEllipse.h"
#import <CZToolbox/CZToolbox.h>

const static CGFloat kDefaultCX = 0;
const static CGFloat kDefaultCY = 0;
const static CGFloat kDefaultRadius = 25;

@implementation CZNodeEllipse


- (id)init {
    self = [super init];
    if (self) {
        _cx = kDefaultCX;
        _cy = kDefaultCY;
        _rx = kDefaultRadius;
        _ry = kDefaultRadius;
        
        [self addObserver:self forKeyPaths:@"cx", @"cy", @"rx", @"ry", nil];
    }
    return self;
}

- (id)initWithCenterX:(CGFloat)cx
              centerY:(CGFloat)cy
              radiusX:(CGFloat)rx
              radiusY:(CGFloat)ry {
    self = [super init];
    if (self) {
        _cx = cx;
        _cy = cy;
        _rx = rx;
        _ry = ry;
        
        [self addObserver:self forKeyPaths:@"cx", @"cy", @"rx", @"ry", nil];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPaths:@"cx", @"cy", @"rx", @"ry", nil];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeEllipse *newEllipse = [super copyWithZone:zone];
    if (newEllipse) {
        newEllipse->_cx = self->_cx;
        newEllipse->_cy = self->_cy;
        newEllipse->_rx = self->_rx;
        newEllipse->_ry = self->_ry;
    }
    return newEllipse;
}

#pragma mark -
#pragma mark override CZNode

- (BOOL)hitTestSelf:(CGPoint)pointOfSelf tolerance:(CGFloat)tolerance {
    pointOfSelf.x -= _rx;
    pointOfSelf.y -= _ry;
    
    CGFloat halfLineWidth = self.lineWidth / 2.0;
    if (tolerance < halfLineWidth) {
        tolerance = halfLineWidth;
    }
    
    CGPoint bigRadius = CGPointMake(_rx + tolerance, _ry + tolerance);
    
    if (self.isFilled || tolerance >= _rx || tolerance >= _ry) {
        // If ellipse is filled, inside ellipse means hit on.
        return [CZNodeEllipse ellipse:bigRadius containsPoint:pointOfSelf];
    } else {
        // If ellipse is not filled, hit on outline means hit on.
        CGPoint smallRadius = CGPointMake(_rx - tolerance, _ry - tolerance);
        
        return [CZNodeEllipse ellipse:bigRadius containsPoint:pointOfSelf]
            && (![CZNodeEllipse ellipse:smallRadius containsPoint:pointOfSelf]);
    }
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    pointOfSelf.x -= _rx;
    pointOfSelf.y -= _ry;
    
    CGPoint radius = CGPointMake(_rx, _ry);
    
    return [CZNodeEllipse ellipse:radius containsPoint:pointOfSelf];
}

- (CGPoint)anchorPoint {
    return CGPointMake(_cx, _cy);
}

#pragma mark -
#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathAddEllipseInRect(relativePath, NULL, CGRectMake(_cx - _rx, _cy - _ry, _rx + _rx, _ry + _ry));
    return relativePath;
}

#pragma mark - Private method

+ (BOOL)ellipse:(CGPoint)r containsPoint:(CGPoint)point {
    if (r.x == 0.0 || r.y == 0.0) {
        return NO;
    }
    
    CGFloat x2 = point.x * point.x;
    CGFloat y2 = point.y * point.y;
    
    CGFloat rx2 = r.x * r.x;
    CGFloat ry2 = r.y * r.y;
    
    return ((x2 / rx2 + y2 / ry2) <= 1.0f);
}

@end
