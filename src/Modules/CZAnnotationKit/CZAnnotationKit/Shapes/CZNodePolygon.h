//
//  CZNodePolygon.h
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiPointShape.h"

@interface CZNodePolygon : CZNodeMultiPointShape<CZNodeShapeFillable>

@end
