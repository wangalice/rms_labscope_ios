//
//  CZNodeShape.h
//  Hermes
//
//  Created by Ralph Jin on 1/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNode.h"

/*! A shape shall be either fill or stroke, but not both. */
@interface CZNodeShape : CZNode {
}

@property (nonatomic) CGFloat lineWidth;
@property (nonatomic, getter = isDash) BOOL dash;

/* represent path in parent coordinate, but just without rotating */
@property (nonatomic, readonly) CGPathRef relativePath;

- (void)invalidateRelativePath;

/*! Sub-class should override this method to create specific path
 in parent's coordinate but without rotation. */
- (CGMutablePathRef) newRelativePath;

@end
