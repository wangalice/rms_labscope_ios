//
//  CZNodeSpline.mm
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeSpline.h"
#include <vector>
#import "CZNodeGeom.h"

@implementation CZNodeSpline

#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathAddSpline(relativePath, NULL, [self pointArray], [self pointsCount], false);
    return relativePath;
}

- (BOOL)selfContainsPoint:(CGPoint)pointOfSelf {
    return NO;
}

@end
