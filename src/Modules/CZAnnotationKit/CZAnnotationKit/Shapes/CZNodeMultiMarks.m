//
//  CZNodeMultiMarks.m
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeMultiMarks.h"
#import <CZToolbox/CZToolbox.h>

static const CGFloat kDefaultMarkSize = 5.0;

@implementation CZNodeMultiMarks

- (id)init {
    self = [super init];
    if (self) {
        _markSize = kDefaultMarkSize;
        
        [self addObserver:self forKeyPaths:@"markSize", nil];
    }
    
    return self;
}

- (id)initWithPointsCount:(NSUInteger)count {
    self = [super initWithPointsCount:count];
    if (self) {
        _markSize = kDefaultMarkSize;
        
        [self addObserver:self forKeyPaths:@"markSize", nil];
    }
    
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPaths:@"markSize", nil];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    CZNodeMultiMarks *newMarks = [super copyWithZone:zone];
    if (newMarks) {
        newMarks->_markSize = self->_markSize;
    }
    return newMarks;
}

#pragma mark override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    
    CGFloat markSize = self.markSize;
    
    NSUInteger pointsCount = [self pointsCount];
    for (NSUInteger i = 0; i < pointsCount; i++) {
        CGPoint point = [self pointAtIndex:i];
        
        // cross mark
        CGPathMoveToPoint(relativePath, NULL, point.x - markSize, point.y - markSize);
        CGPathAddLineToPoint(relativePath, NULL, point.x + markSize, point.y + markSize);
        CGPathMoveToPoint(relativePath, NULL, point.x - markSize, point.y + markSize);
        CGPathAddLineToPoint(relativePath, NULL, point.x + markSize, point.y - markSize);

    }
    return relativePath;
}

@end
