//
//  CZNodePolygon.mm
//  Hermes
//
//  Created by Ralph Jin on 1/17/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodePolygon.h"

@implementation CZNodePolygon

#pragma mark - override CZNodeShape

- (CGMutablePathRef)newRelativePath {
    CGMutablePathRef relativePath = CGPathCreateMutable();
    CGPathAddLines(relativePath, NULL, [self pointArray], [self pointsCount]);
    if ([self pointsCount] > 2) {
        CGPathCloseSubpath(relativePath);
    }
    return relativePath;
}

@end
