    //
//  CZNodeGeom.h
//  Hermes
//
//  Created by Ralph Jin on 1/28/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef INCLUDED_CZNODE_GEOM_H
#define INCLUDED_CZNODE_GEOM_H

#include <CoreGraphics/CoreGraphics.h>

#ifdef __cplusplus
extern "C" {
#endif

/*! Calculate angle from point a to point b, relative to the rotation center point p.
 @return angle in radians, range from -2*PI to 2*PI.
 */
CGFloat CZCalcRotateAngle(CGPoint p, CGPoint a, CGPoint b);

/*! Normailize an angle to range [0 ~ 2Pi) */
CGFloat CZNormalizeAngle(CGFloat radians);

CGAffineTransform CGAffineTransformMakeRotationAtPoint(CGFloat angle, CGPoint p);
    
CGAffineTransform CGAffineTransformMakeRotationFromVector(CGFloat vx, CGFloat vy);

/*! Calculate the length of the line(a->b) */
CGFloat CZLengthOfLine(CGPoint a, CGPoint b);

/*! Calculate the angle in radians of vector(a->b), range from -PI to PI. */
CGFloat CZRadiusOfVector(CGPoint a, CGPoint b);
    
/*! Caluclate the foot of a perpendicular
 * ab is a line
 * one of the orthogonal lines of ab passes p
 * get the intersection point of ab and that orthogonal line
 */
CGPoint CalcOrthoPoint(CGPoint a, CGPoint b, CGPoint p);

/*! a->b is a line
 * an orthogonal line of a->b passes b
 * p is a point on orthogonal line, and pb is with length of |length|
 * @return point p
 */
CGPoint CZCalcOuterPoint(CGPoint a, CGPoint b, double length);

/*! Calculate the crossover point of two lines. The line can extend far away.
 a->b is a line, c->d is another line.
 */
CGPoint CZCalcCrossPoint(CGPoint a, CGPoint b, CGPoint c, CGPoint d);

/*! Calculate the crossover point of two half-lines. The line can extend on 1 direction.
 p1 is start point, dp1 is vector, so (p1 + dp1) is another point on the half-line.
 */
CGPoint CZCalcCrossPointOfHalfLine(CGPoint p1, CGPoint dp1, CGPoint p2, CGPoint dp2);

/*! Calculate if two line segments have cross point.
 a->b is a line segment, c->d is another line segment.
 */
bool CZLineSegmentsAreCrossed(CGPoint a, CGPoint b, CGPoint c, CGPoint d);

/*! Convert spline to bezier path and add to |path|
 *
 * @param path the path which spline will added to.
 * @param m if not NULL, transform the point first then add to the path.
 * @param points the spline control points in C array type.
 * @param pointCount the point count of |points| array.
 * @param closePath true if the spline is a closed contour, otherwise false.
 */
void CGPathAddSpline(CGMutablePathRef path, const CGAffineTransform *m, const CGPoint points[], size_t pointCount, bool closePath);
    
/**
 * @param p the point to check.
 * @param startAngle endAngle, ptCenter and radius make the pie. from edge (pt1, ptCenter) to edge (pt2, ptCenter).
 */
bool CZPieContainsPoint(CGPoint p, CGFloat startAngle, CGFloat endAngle, CGPoint ptCenter, CGFloat radius);
    
bool CGRectIntersectsCircle(CGRect rect, CGPoint center, CGFloat radius);
    
#ifdef __cplusplus
}
#endif


#endif  // INCLUDED_CZNODE_GEOM_H
