//
//  CZNodeMultiPointShape.h
//  Matscope
//
//  Created by Ralph Jin on 10/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZNodeShape.h"

/*! abstract class of multi-point shape, like polygon, polyline etc.*/
@interface CZNodeMultiPointShape : CZNodeShape

- (id)initWithPointsCount:(NSUInteger)count;

- (NSUInteger)pointsCount;

- (void)setPoint:(CGPoint)pt atIndex:(NSUInteger)index;

- (CGPoint)pointAtIndex:(NSUInteger)index;

- (void)appendPoint:(CGPoint)pt;

- (void)removePointAtIndex:(NSUInteger)index;

- (void)removeAllPoints;

- (const CGPoint *)pointArray;

@end
