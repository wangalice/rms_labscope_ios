<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta content="text/html;charset=UTF-8" http-equiv="Content-Type" />

  <title>Daily IPA upload</title>

  <meta content="Patrick Nagel" name="author" />
  <meta content="Daily IPA upload" name="description" />

</head>
<style>
body {
	margin-left: 20%;
	margin-right: 20%;
	margin-top: 5%;
        font-family: sans-serif;
        }
p { 
        margin-top: 0px;
        margin-left: 3px;
        }

h1 {
        clear: left;
        letter-spacing: 2px;
        margin-bottom: 32px;
        }

h2 {
        margin-top: 40px;
        margin-bottom: 15px;
        background-color: transparent;
        }

h3 {
        margin-top: 10px;
        margin-bottom: 10px;
        background-color: transparent;
        font-size: 110%;
        }

ul {
        list-style-type: square;
        }

li {
        margin-top: 5px;
        }

a:link {
        font-weight: bold;
        text-decoration: none;
        color: #7777ff;
        background-color: transparent;
        }

a:visited {
        font-weight: bold;
        text-decoration: none;
        color: #7777ff;
        background-color: transparent;
        }

a:hover, a:active {
        text-decoration: underline;
        color: #9685BA;
        background-color: transparent;
        }

a img { border: none; }
</style>
<body style="direction: ltr;">

<h1>Daily IPA Upload</h1>

<?php

######################### SCRIPT #########################

// This script requires PEAR-HTTP_Upload and
// thus the open_basedir variable must contain
// "/usr/share/php/" for this to work.

if ($_GET['submit'] == 1) {
	require_once "HTTP/Upload.php";
	error_reporting(E_ALL);
	
	$upload = new HTTP_Upload("en");
	$file = $upload->getFiles("f");
	$file->setName("safe");
  $fileext = $file->getProp("ext");

	if ($file->isValid() && $fileext == "ipa") {
		$moved = $file->moveTo("./ipa/");
		if (!$file->isError($moved)) {

			// Success
			echo "<p>\n";
			echo "  Upload complete.<br />\n  <br />\n";
			echo "</p>\n";

			// Create *.plist and index.html
			echo "<p><tt>\n";
			print(passthru("./create_plists_and_index.sh 2>&1"));
			echo "</tt></p>\n";

			echo "<p style=\"margin-top: 40px\">\n";
			echo "<a href=\"/daily/labscope/\">Index</a>\n";
			echo "</p>\n";
		}
		else {
			echo "<p>\n  <strong>" . $moved->getMessage() . "\n</p>\n";
		}
	}
	elseif ($file->isMissing()) {
		echo "<p>\n  <strong>You did not select a file.</strong>\n</p>\n";
	}	
	else {
		echo "<p>\n  <strong>An error occured while processing the file.</strong>\n</p>\n";
	}
}
else {
	echo "<form action=\"" . htmlspecialchars($_SERVER['PHP_SELF']) . "?submit=1\" method=\"post\" enctype=\"multipart/form-data\">\n";
	echo "  <p>\n";
	echo "    <br />\n";
	echo "    <br />\n";
	echo "    <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"200000000\" />\n";
	echo "    File:<br /><input name=\"f\" type=\"file\" size=\"50\" /><br /><br />\n";
	echo "    <br />\n";
	echo "    <input type=\"submit\" value=\"Upload\" />\n";
	echo "    <br />\n";
	echo "    <br />\n";
	echo "  </p>\n";
	echo "</form>\n\n";
	echo "<p>\n(Sorry, no progress indicator during upload. Only click the Upload button once, please!)\n</p>";
}

######################### /SCRIPT ########################

?>
</body>

</html>
