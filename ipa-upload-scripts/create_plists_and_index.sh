#!/bin/bash

function getBundleIdentifier() {
# Expects IPA file name as first parameter (example: Labscope_1234.ipa)
  TF=$(mktemp)
  unzip -p "${1}" "Payload/Labscope.app/Info.plist" >${TF}
  file -i ${TF} | grep -q 'binary' && perl ../plutil.pl ${TF} >/dev/null && mv ${TF}.text ${TF}
  grep -A 1 '<key>CFBundleIdentifier</key>' ${TF} | tail -n 1 | sed -e 's|.*<string>||' -e 's|</string>||'
  rm -f "${TF}"
}

function generatePlist() {
# Expects IPA file name as first parameter (example: Labscope_1234.ipa)
# Expects bundle identifier as second parameter (example: com.zeisscn.labscope.iphone)
  echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
<plist version=\"1.0\">
<dict>
	<key>items</key>
	<array>
		<dict>
			<key>assets</key>
			<array>
				<dict>
					<key>kind</key>
					<string>software-package</string>
					<key>url</key>
					<string>https://hermes-builds.bucuo.de/daily/labscope/ipa/${1}</string>
				</dict>
			</array>
			<key>metadata</key>
			<dict>
				<key>bundle-identifier</key>
				<string>${2}</string>
				<key>bundle-version</key>
				<string>1.0</string>
				<key>kind</key>
				<string>software</string>
				<key>title</key>
				<string>Labscope</string>
			</dict>
		</dict>
	</array>
</dict>
</plist>"
}

function generateIndexBullet() {
  echo "<li style=\"margin-top: 40px\"><a href=\"itms-services://?action=download-manifest&url=https://hermes-builds.bucuo.de/daily/labscope/plist/${1%???}plist\">Install ${1%.???}</a><br><small>(<a href=\"ipa/${1}\">Download IPA</a> for installation through iTunes)</small></li>"
}

echo '<p>'
echo 'Deleting plist files... '
rm -f plist/*.plist && echo 'done<br>'
echo 'Creating plist files... '
cd ipa
echo '<ul>'
for file in `ls -tr *.ipa`; do
  bundleId=$(getBundleIdentifier ${file})
  if [[ "${bundleId}" == "" ]]; then bundleId="ERROR: CFBundleIdentifier not found in ${file}/Payload/Labscope.app/Info.plist"; fi
  echo "<li>${file} = ${bundleId}</li>"
  generatePlist "${file}" "${bundleId}" > "../plist/${file%???}plist"
done && echo '</ul>done<br>'
chown -f patrick:ftpgroup "../plist/*.plist"
cd "$OLDPWD"
echo 'Creating index.html... '
echo '<html>
<head><title>Labscope daily builds</title></head>
<style>
body {
	margin-left: 20%;
	margin-right: 20%;
	margin-top: 5%;
        font-family: sans-serif;
        }
p { 
        margin-top: 0px;
        margin-left: 3px;
        }

h1 {
        clear: left;
        letter-spacing: 2px;
        margin-bottom: 32px;
        }

h2 {
        margin-top: 40px;
        margin-bottom: 15px;
        background-color: transparent;
        }

h3 {
        margin-top: 10px;
        margin-bottom: 10px;
        background-color: transparent;
        font-size: 110%;
        }

ul {
        list-style-type: square;
        }

li {
        margin-top: 5px;
        }

a:link {
        font-weight: bold;
        text-decoration: none;
        color: #7777ff;
        background-color: transparent;
        }

a:visited {
        font-weight: bold;
        text-decoration: none;
        color: #7777ff;
        background-color: transparent;
        }

a:hover, a:active {
        text-decoration: underline;
        color: #9685BA;
        background-color: transparent;
        }

a img { border: none; }
</style>
<body>
<h1>Labscope daily builds</h1>
<p><big>
<ul>' > index.html

cd ipa
for file in `ls -tr *.ipa`; do
  generateIndexBullet "${file}" >> ../index.html
done
cd "$OLDPWD"

echo '</ul>
</big>
<p style="margin-top: 40px">
<a href="upload">Upload IPA</a>
</p>
</body>
</html>' >> index.html

echo 'done<br>'
