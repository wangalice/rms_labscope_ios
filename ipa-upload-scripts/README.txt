Date  : 2015-12-04
Author: Patrick Nagel


Description
-----------

These scripts can be used to facilitate the easy upload of IPA files. They are supposed to be located on the web server in a protected (password authenticated) area.

Currently the scripts contain hard-coded references to the app name ("Labscope") and the server we use as of 2015-12 ("hermes-builds.bucuo.de") - so if you want to use it for another app or on another server, please make sure to replace these occurrences.



put.php
-------

This is the "entry point" for the IPA uploader. I.e. the person that is supposed to upload an IPA file should go to https://webserver/dailybuilds/put.php - a "Browse" button lets the user choose the IPA file from the local drive, and a "Upload" button starts the upload. After the upload is successful, the script calls the bash script 'create_plists_and_index.sh' (see below).

Dependencies:
- PHP 5.x
- PEAR-HTTP_Upload



create_plists_and_index.sh
--------------------------

This bash script assumes that there is a sub-directory 'ipa' and 'plist'. It goes through all the .ipa files in the 'ipa' sub-directory, and creates matching .plist files in the 'plist' sub-directory. It outputs HTML to indicate the progress.
As "sometimes" (for an unknown reason) the "Info.plist" that is contained within the IPA file is in "binary plist" format, and sometimes it is in the normal text-based XML format, the script needs to check whether it's binary and then convert it, before the 'bundle identifier' can be extracted from the IPA's "Info.plist" and placed in the generated deployment .plist. The binary-to-text converter script is called 'plutil.pl' (see below).

Dependencies:
- bash
  - unzip
  - grep
  - sed
- perl (to interpret plutil.pl)



plutil.pl
---------

Converts binary .plist files to text-based XML plist files and vice versa. Downloaded from http://scw.us/iPhone/plutil/ on 2015-12-04
