//
//  CZNameStatusQuery.m
//  SmbfindDemo
//
//  Created by Sherry Xu on 7/10/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZNameStatusQuery.h"
#import "CZBufferProcessor.h"

@interface CZNameQuery ()  {
    uint16_t _name_trn_id; //transaction ID:0x3626
    uint16_t _operation;     //Flags:0x8500
    NSUInteger _qdcount;     //Questions count:0x0000(2 bytes)
    NSUInteger _ancount;     //Answer count:0x0001
    NSUInteger _nscount;    //Authority count:0x0000
    NSUInteger _arcount;        //Additional count:0x0000
    
    NSUInteger _stringLength;
    NSString *_nameString;
    nbt_name_type _nameType;
    nbt_qtype _type;
    nbt_qclass _class;
    uint32_t _timeToLive;
    NSUInteger _dataLength; // 6*n
}

@property (nonatomic, strong) NSMutableArray *ipAddresses;
@property (nonatomic, strong) NSMutableArray *nameFlags;

- (void)parseFrom:(CZBufferProcessor *)reader;
- (NSArray *)ipAddresses;

@end

@implementation CZNameQuery

- (id)init {
    self = [super init];
    if (self) {
        _ipAddresses = [[NSMutableArray alloc] init];
        _nameFlags = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)parseFrom:(CZBufferProcessor *)reader {
    _name_trn_id = [reader readShortInHostOrder];
    _operation = [reader readShortInHostOrder];
    _qdcount = [reader readShortInHostOrder];
    _ancount = [reader readShortInHostOrder];
    _nscount = [reader readShortInHostOrder];
    _arcount = [reader readShortInHostOrder];
    _stringLength = [reader readByte];
    _nameString = [reader readStringWithLength:_stringLength];
    _nameType = [reader readByte];
    _type = [reader readShortInHostOrder];
    _class = [reader readShortInHostOrder];
    _timeToLive = [reader readLongInHostOrder]; // 32bits
    _dataLength = [reader readShortInHostOrder];
    if ((_ancount != 1) || (_type != NBT_QTYPE_NETBIOS) || (_class != NBT_QCLASS_IP) || (_dataLength % 6 != 0)) {
        return;
    }
    for (int i = 0; i < _dataLength / 6; i++) {
        uint16_t nameFlag = [reader readShortInHostOrder];
        NSNumber *nameFlagNumber = @(nameFlag);
        [_nameFlags addObject:nameFlagNumber];
        NSString *ipAddress = [reader readIpAddressString];
        [_ipAddresses addObject:ipAddress];
    }
}

- (NSArray *)ipAddresses {
    return _ipAddresses;
}

@end

@interface CZNameStatusQuery () {
    uint16_t _name_trn_id; //transaction ID:0x3626
    uint16_t _operation;     //Flags:0x8500
    NSUInteger _qdcount;     //Questions count:0x0000(2 bytes)
    NSUInteger _ancount;     //Answer count:0x0001
    NSUInteger _nscount;    //Authority count:0x0000
    NSUInteger _arcount;        //Additional count:0x0000
    
    NSUInteger _stringLength;
    NSString *_nameString;
    nbt_name_type _nameType;
    nbt_qtype _type;
    nbt_qclass _class;
    uint32_t _timeToLive;
    NSUInteger _dataLength; // num_names * 18 + 47(uint16_t)
    NSUInteger _num_names; //uint8_t
}

- (void)parseNameFrom:(CZBufferProcessor *)reader;

@end

@implementation CZNameStatusQuery

- (id)init {
    self = [super init];
    if (self) {
        _names = [[NSMutableArray alloc] init];
        _nameTypes = [[NSMutableArray alloc] init];
        _nameFlags = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)parseNameFrom:(CZBufferProcessor *)reader {
    _name_trn_id = [reader readShortInHostOrder];
    _operation = [reader readShortInHostOrder];
    _qdcount = [reader readShortInHostOrder];
    _ancount = [reader readShortInHostOrder];
    _nscount = [reader readShortInHostOrder];
    _arcount = [reader readShortInHostOrder];
    _stringLength = [reader readByte];
    _nameString = [reader readStringWithLength:_stringLength];
    _nameType = [reader readByte];
    _type = [reader readShortInHostOrder];
    _class = [reader readShortInHostOrder];
    _timeToLive = [reader readLongInHostOrder]; // 32bits
    _dataLength = [reader readShortInHostOrder];
    _num_names = [reader readByte];
    if ((_ancount != 1) || (_type != NBT_QTYPE_STATUS) || (_class != NBT_QCLASS_IP) || (_num_names <= 0)) {
        return;
    }
    for (NSUInteger i = 0; i < _num_names; i++) {
        NSString *name = [reader readStringWithLength:(15)]; //15 bytes
        [_names addObject:name];
        nbt_name_type nameType = [reader readByte];
        NSNumber *nameTypeNumber = @(nameType);
        [_nameTypes addObject:nameTypeNumber];
        nb_flags nameFlag = [reader readShortInHostOrder];
        NSNumber *nameFlagNumber = @(nameFlag);
        [_nameFlags addObject:nameFlagNumber];
    }
}

@end
