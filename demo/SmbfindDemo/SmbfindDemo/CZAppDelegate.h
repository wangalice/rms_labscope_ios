//
//  CZAppDelegate.h
//  SmbfindDemo
//
//  Created by Ralph Jin on 7/9/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
