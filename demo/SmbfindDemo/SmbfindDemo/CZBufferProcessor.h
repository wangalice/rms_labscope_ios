//
//  CZBufferProcessor.h
//  Hermes
//
//  Created by Halley Gu on 5/2/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * This class is a reader/writer for byte stream memory buffer. At this moment
 * it only supports buffer with a certain length.
 *
 * The class provides a series of methods which can be used to convert byte
 * order between host and network while reading and writing different data type.
 * 
 * The methods also automatically handles the offset while reading/writing data.
 * You can use seeking/jumping methods to manually adjust the offset as well.
 */
@interface CZBufferProcessor : NSObject

@property (nonatomic, assign, readonly) uint64_t offset;

- (id)initWithBuffer:(const void *)buffer length:(uint64_t)length;
- (id)initWithMutableData:(NSMutableData *)data;

- (BOOL)seekToOffset:(uint64_t)offset;
- (BOOL)jumpByLength:(uint64_t)length;

- (uint8_t)readByte;
- (uint16_t)readShort;
- (uint16_t)readShortInHostOrder;
- (uint32_t)readLong;
- (int64_t)readLongLong;
- (uint32_t)readLongInHostOrder;
- (NSString *)readUTF8StringInRange:(NSRange)range;
- (NSString *)readStringWithLength:(size_t)length;
- (NSString *)readIpAddressString;
- (NSString *)readIpAddressStringInHostOrder;
- (double)readDouble;
- (void)readBytes:(void *)bytes length:(uint64_t)length;

- (void)writeByte:(uint8_t)data;
- (void)writeShort:(uint16_t)data;
- (void)writeShortInNetworkOrder:(uint16_t)data;
- (void)writeLong:(uint32_t)data;
- (void)writeLongInNetworkOrder:(uint32_t)data;
- (void)writeLongLong:(int64_t)data;
- (void)writeDouble:(double)data;
- (void)writeBytes:(const void *)bytes length:(uint64_t)length;
- (void)writeString:(const char *)str;

- (CZBufferProcessor *)newSubBufferReaderWithLength:(NSUInteger)length;

@end
