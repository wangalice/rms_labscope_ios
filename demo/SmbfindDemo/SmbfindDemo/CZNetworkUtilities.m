//
//  CZNetworkUtilities.m
//  WNFSSDK
//
//  Created by BCD-MAC-004 on 4/19/13.
//  Copyright (c) 2013 Bleum. All rights reserved.
//

#import "CZNetworkUtilities.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#import <SystemConfiguration/SCNetworkReachability.h>

@implementation CZNetworkUtilities

+ (BOOL)isIPAddress:(NSString*) ip {
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
                                  options:0
                                  error:nil];
    
    NSUInteger matches = [regex numberOfMatchesInString:ip
                                                options:0
                                                  range:NSMakeRange(0, [ip length])];
    if (matches == 1) {
        return YES;
    }
    return NO;
}

+ (BOOL)isNetworkEnable {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL retrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!retrieveFlags) {
        return NO;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

+ (NSString *)getIPAddress {
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    if(!getifaddrs(&interfaces)) {

        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                
                if([name isEqualToString:@"en0"]) {
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }

        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    return addr ? addr : nil;
}

+ (NSArray *)hostNameFrom:(NSString *)address {
//    struct addrinfo      hints;
//    struct addrinfo      *result = NULL;
//    memset(&hints, 0, sizeof(hints));
//    hints.ai_flags    = AI_NUMERICHOST;
//    hints.ai_family   = PF_UNSPEC;
//    hints.ai_socktype = SOCK_STREAM;
//    hints.ai_protocol = 0;
//    int errorStatus = getaddrinfo([address cStringUsingEncoding:NSASCIIStringEncoding], NULL, &hints, &result);
//    if (errorStatus != 0) return nil;
//    CFDataRef addressRef = CFDataCreate(NULL, (UInt8 *)result->ai_addr, result->ai_addrlen);
//    if (addressRef == nil) return nil;
//    freeaddrinfo(result);
//    CFHostRef hostRef = CFHostCreateWithAddress(kCFAllocatorDefault, addressRef);
//    if (hostRef == nil) return nil;
//    CFRelease(addressRef);
//    BOOL isSuccess = CFHostStartInfoResolution(hostRef, kCFHostNames, NULL);
//    if (!isSuccess) return nil;
//    
//    CFArrayRef hostnamesRef = CFHostGetNames(hostRef, NULL);
//    NSMutableArray *hostnames = [NSMutableArray array];
//    for (int currentIndex = 0; currentIndex < [(NSArray *)hostnamesRef count]; currentIndex++) {
//        [hostnames addObject:[(NSArray *)hostnamesRef objectAtIndex:currentIndex]];
//    }

//    return hostnames;
    return nil;
}
@end
