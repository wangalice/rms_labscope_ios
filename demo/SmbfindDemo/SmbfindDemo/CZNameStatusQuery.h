//
//  CZNameStatusQuery.h
//  SmbfindDemo
//
//  Created by Sherry Xu on 7/10/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

/* the opcodes are in the operation field, masked with
 NBT_OPCODE */
typedef enum {
    NBT_OPCODE_QUERY          =  (0x0<<11),
    NBT_OPCODE_REGISTER       =  (0x5<<11),
    NBT_OPCODE_RELEASE        =  (0x6<<11),
    NBT_OPCODE_WACK           =  (0x7<<11),
    NBT_OPCODE_REFRESH        =  (0x8<<11),
    NBT_OPCODE_REFRESH2       =  (0x9<<11),
    NBT_OPCODE_MULTI_HOME_REG =  (0xf<<11)
} nbt_opcode;

typedef enum {
    NBT_RCODE                   = 0x000F, // TODO:the original is bitmap ???
    NBT_FLAG_BROADCAST          = 0x0010,
    NBT_FLAG_RECURSION_AVAIL    = 0x0080,
    NBT_FLAG_RECURSION_DESIRED  = 0x0100,
    NBT_FLAG_TRUNCATION         = 0x0200,
    NBT_FLAG_AUTHORITATIVE      = 0x0400,
    NBT_OPCODE                  = 0x7800,
    NBT_FLAG_REPLY              = 0x8000
} nbt_operation;

/* we support any 8bit name type, but by defining the common
 ones here we get better debug displays */
typedef NS_ENUM(uint8_t, nbt_name_type) {
    NBT_NAME_CLIENT   = 0x00, //8 bits
    NBT_NAME_MS       = 0x01,
    NBT_NAME_USER     = 0x03,
    NBT_NAME_SERVER   = 0x20,
    NBT_NAME_PDC      = 0x1B,
    NBT_NAME_LOGON    = 0x1C,
    NBT_NAME_MASTER   = 0x1D,
    NBT_NAME_BROWSER  = 0x1E
};

/* the ndr parser for nbt_name is separately defined in
 nbtname.c (along with the parsers for nbt_string) */
typedef struct {
    nbt_name_type type;
} nbt_name;

typedef NS_ENUM(uint16_t, nbt_qclass) {
    NBT_QCLASS_IP = 0x0001
};

typedef NS_ENUM(uint16_t, nbt_qtype) {
    NBT_QTYPE_ADDRESS     = 0x0001,
    NBT_QTYPE_NAMESERVICE = 0x0002,
    NBT_QTYPE_NULL        = 0x000A,
    NBT_QTYPE_NETBIOS     = 0x0020,
    NBT_QTYPE_STATUS      = 0x0021
};

typedef struct {
    nbt_name   name;
    nbt_qtype  question_type;
    nbt_qclass question_class;
} nbt_name_question;

/* these are the possible values of the NBT_NM_OWNER_TYPE
 field */
typedef enum {
    NBT_NODE_B = 0x0000,
    NBT_NODE_P = 0x2000,
    NBT_NODE_M = 0x4000,
    NBT_NODE_H = 0x6000
} nbt_node_type;

typedef NS_ENUM(uint16_t, nb_flags) {
    NBT_NM_PERMANENT        = 0x0200,
    NBT_NM_ACTIVE           = 0x0400,
    NBT_NM_CONFLICT         = 0x0800,
    NBT_NM_DEREGISTER       = 0x1000,
    NBT_NM_OWNER_TYPE       = 0x6000,
    NBT_NM_GROUP            = 0x8000
};

typedef struct {
    nb_flags nb_flags; //Name flags:0xe000(2bytes) *variables ???
    uint32_t ipaddr; // ip address
} nbt_rdata_address;

typedef struct {
    uint8_t name[15];
    nbt_name_type type;
    nb_flags nb_flags;
} nbt_status_name;

typedef struct {
    uint16_t length;
    nbt_rdata_address addresses[1];  // length/6
} nbt_rdata_netbios;

typedef struct {
    uint16_t length; // num_names * 18 + 47
    uint8_t num_names;
    nbt_status_name names[1];  // TODO: it should be length/6
} nbt_rdata_status;

/*
 * this macro works around the problem
 * that we need to use nbt_rdata_data
 * together with NBT_QTYPE_NETBIOS
 * for WACK replies
 */
typedef struct {
    nbt_name   name;
    nbt_qtype  rr_type;  //type:NB(0x0020)
    nbt_qclass rr_class; //class:IN(0x0001)
    uint32_t     ttl; //Time to live:(4 bytes)
    nbt_rdata_netbios netbios;
} nbt_res_rec;

typedef struct {
    nbt_name   name;
    nbt_qtype  rr_type;  //type:NB(0x0020)
    nbt_qclass rr_class; //class:IN(0x0001)
    uint32_t     ttl; //Time to live:(4 bytes)
    nbt_rdata_status status;
} nbt_res_rec1;


@class CZBufferProcessor;

@interface CZNameQuery : NSObject

- (void)parseFrom:(CZBufferProcessor *)reader;
- (NSArray *)ipAddresses;

@end

@interface CZNameStatusQuery : NSObject

@property (nonatomic, strong) NSMutableArray *names;
@property (nonatomic, strong) NSMutableArray *nameTypes;
@property (nonatomic, strong) NSMutableArray *nameFlags;

- (void)parseNameFrom:(CZBufferProcessor *)reader;
@end
