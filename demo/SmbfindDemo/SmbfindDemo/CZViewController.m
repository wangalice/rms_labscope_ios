//
//  CZViewController.m
//  SmbfindDemo
//
//  Created by Ralph Jin on 7/9/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZViewController.h"
#import "GCDAsyncUdpSocket.h"
#import "CZNetworkUtilities.h"


@interface CZViewController () <GCDAsyncUdpSocketDelegate> {
    dispatch_queue_t _discoveryQueue;
}
@property (weak, nonatomic) IBOutlet UITextView *logView;

@property (nonatomic, assign) GCDAsyncUdpSocket *browsingSocket;
@property (nonatomic, assign) GCDAsyncUdpSocket *nameQuerySocket;
@property (nonatomic, strong) NSArray *discoveryIpAddresses;

- (IBAction)BrowseButtonTapped:(id)sender;
- (IBAction)clearButtonTapped:(id)sender;
- (IBAction)searchNameButtonTapped:(id)sender;

@end

@implementation CZViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _discoveryIpAddresses = [[NSArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BrowseButtonTapped:(id)sender {
    GCDAsyncUdpSocket *socket = [self browsingSocket];
    
    NSError *error = nil;
    if (![socket enableBroadcast:YES error:&error]) {
        NSLog(@"Samba discovery: failed to enable broadcast for the socket.");
        return;
    }
    
    if (![socket beginReceiving:&error]) {
        NSLog(@"Samba discovery: failed to begin receiving data from socket.");
        return;
    }
    
    char buff[] = {0x36, 0x26, 0x01, 0x10, 0x00, 0x01,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x43,
    0x4b, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
    0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
    0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
    0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x00,
    0x00, 0x20, 0x00, 0x01 };  // smb browse packet
    const size_t bufferLength = sizeof(buff);
    
    NSData *packetData = [[NSData alloc] initWithBytes:buff
                                                length:bufferLength];
    
    NSString * localIp = [CZNetworkUtilities getIPAddress];
    if (localIp == nil) {
        return;
    }

    NSArray * array = [localIp componentsSeparatedByString:@"."];
    if (array == nil || [array count] < 4) {
        return;
    }

    NSString *ip = [NSString stringWithFormat:@"%@.%@.%@.%d",
                    [array objectAtIndex:0],
                    [array objectAtIndex:1],
                    [array objectAtIndex:2],
                    255];
 
    [socket sendData:packetData
              toHost:ip // Broadcast
                port:137
         withTimeout:5
                 tag:0];
}

- (IBAction)searchNameButtonTapped:(id)sender {
    GCDAsyncUdpSocket *socket = [self browsingSocket];
    
    NSError *error = nil;
    if (![socket enableBroadcast:YES error:&error]) {
        NSLog(@"Samba discovery: failed to enable broadcast for the socket.");
        return;
    }
    
    if (![socket beginReceiving:&error]) {
        NSLog(@"Samba nameQuery: failed to begin receiving data from socket.");
        return;
    }
    
    char buff[] = {0x7a, 0x03, 0x00, 0x00, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x43,
        0x4b, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41,
        0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x00,
        0x00, 0x21, 0x00, 0x01 };  // smb name query packet
    const size_t bufferLength = sizeof(buff);
    
    NSData *packetData = [[NSData alloc] initWithBytes:buff
                                                length:bufferLength];
    
    NSString * localIp = [CZNetworkUtilities getIPAddress];
    if (localIp == nil) {
        return;
    }
    
    NSArray * array = [localIp componentsSeparatedByString:@"."];
    if (array == nil || [array count] < 4) {
        return;
    }
    
    NSUInteger localSubIP = [[array lastObject] integerValue];
    NSString *ip;
    for (int i = 1; i < 254; i++) {
        if (i == localSubIP) {
            continue;
        }
        ip = [NSString stringWithFormat:@"%@.%@.%@.%d",
              [array objectAtIndex:0],
              [array objectAtIndex:1],
              [array objectAtIndex:2],
              i];
        [socket sendData:packetData
                  toHost:ip // Broadcast
                    port:137
             withTimeout:5
                     tag:0];
    }
}

- (IBAction)clearButtonTapped:(id)sender {
    self.logView.text = @"";
}

- (GCDAsyncUdpSocket *)browsingSocket {
    if (!_browsingSocket) {
        if (_discoveryQueue == NULL) {
            _discoveryQueue = dispatch_queue_create("com.zeisscn.smb.discovery", NULL);
        }
        GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                  delegateQueue:_discoveryQueue];
        [socket setIPv4Enabled:YES];
        [socket setIPv6Enabled:NO];
        _browsingSocket = socket;
    }
    
    return _browsingSocket;
}

#pragma mark - GCDAsyncUdpSocketDelegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    if (sock && sock == _browsingSocket) {
        CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                       length:data.length];
        
        uint8_t transcationID = [reader readByte];
        BOOL isSeekSuceess =[reader seekToOffset:0];
        if (transcationID == 0x36) {
            // TODO: parse smb packet
            if (isSeekSuceess) {
                NSString *text = [NSString stringWithFormat:@"%@", data];
                NSLog(@"%@", text);
                
                CZNameQuery *nameQuery = [[CZNameQuery alloc] init];
                [nameQuery parseFrom:reader];
                NSArray *ipAddresses = [[NSArray alloc] init];
                ipAddresses = nameQuery.ipAddresses;
                self.discoveryIpAddresses = ipAddresses;
                
                NSString *msg = @"found IP address:\n";
                
                for (NSInteger i; i < [self.self.discoveryIpAddresses count]; i++) {
                    NSString *ipAddress = self.discoveryIpAddresses[i];
                    msg  = [msg stringByAppendingString:ipAddress];
                    msg  = [msg stringByAppendingString:@"\n"];
                }
                
                [self appendStringToLogView:msg];

            }
        } else if (transcationID == 0x7a) {
            if (isSeekSuceess) {
                NSString *name = [NSString stringWithFormat:@"%@", data];
                NSLog(@"%@", name);
                CZBufferProcessor *addressReader = [[CZBufferProcessor alloc] initWithBuffer:address.bytes
                                                                                      length:address.length];
                NSString *text = [NSString stringWithFormat:@"%@", address];
                NSLog(@"%@", text);
                BOOL isSuceess =[addressReader seekToOffset:4];
                NSString *ip = nil;
                if (isSuceess) {
                    ip = [addressReader readIpAddressString];
                }
                
                CZNameStatusQuery *nameStatusQuery = [[CZNameStatusQuery alloc] init];
                [nameStatusQuery parseNameFrom:reader];
                
                NSString *msg = [NSString stringWithFormat:@"Name of %@:\n", ip];
                
                for (NSInteger i = 0; i < [nameStatusQuery.names count]; i++) {
                    NSString *name = nameStatusQuery.names[i];
                    NSNumber *type = nameStatusQuery.nameTypes[i];
                    NSNumber *flag= nameStatusQuery.nameFlags[i];
                    NSString *typeString;
                    NSString *group = @"<NONE_GROUP>";
                    NSString *node;
                    
                    switch ([type unsignedCharValue]) {
                        case NBT_NAME_CLIENT:
                            typeString = @"NBT_NAME_CLIENT";
                            break;
                        case NBT_NAME_MS:
                            typeString = @"NBT_NAME_MS";
                            break;
                        case NBT_NAME_USER:
                            typeString = @"NBT_NAME_USER";
                            break;
                        case NBT_NAME_SERVER:
                            typeString = @"NBT_NAME_SERVER";
                            break;
                        case NBT_NAME_PDC:
                            typeString = @"NBT_NAME_PDC";
                            break;
                        case NBT_NAME_LOGON:
                            typeString = @"NBT_NAME_LOGON";
                            break;
                        case NBT_NAME_MASTER:
                            typeString = @"NBT_NAME_MASTER";
                            break;
                        case NBT_NAME_BROWSER:
                            typeString = @"NBT_NAME_BROWSER";
                            break;
                        default:
                            typeString = @"NBT_NAME_UNKNOWN";
                            break;
                    }
                    if([flag unsignedShortValue] & NBT_NM_GROUP) {
                        group = @"<GROUP>";
                    }
                    switch ([flag unsignedShortValue] & NBT_NM_OWNER_TYPE) {
                        case NBT_NODE_B:
                            node = @"NODE_TYPE_B";
                            break;
                        case NBT_NODE_P:
                            node = @"NBT_NODE_P";
                            break;
                        case NBT_NODE_M:
                            node = @"NBT_NODE_M";
                            break;
                        case NBT_NODE_H:
                            node = @"NBT_NODE_H";
                            break;
                            
                        default:
                            node = @"NBT_NODE_UNKNOWN";
                            break;
                    }
                    msg  = [msg stringByAppendingFormat:@"%@ (%@) (%@) (%@)\n", name, typeString, group, node];
                }
                
                [self appendStringToLogView:msg];

            }
        }
     }
}
/**
 * Called when the socket is closed.
 **/
- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    if (sock && sock == _browsingSocket) {
        self.browsingSocket = nil;
    }
}

- (void)appendStringToLogView:(NSString *)string {
    dispatch_async(dispatch_get_main_queue(), ^ {
        NSString *combinedText = self.logView.text;
        if (combinedText.length) {
            combinedText = [combinedText stringByAppendingString:@"\n ========== \n"];
            combinedText = [combinedText stringByAppendingString:string];
        } else {
            combinedText = string;
        }
        self.logView.text = combinedText;
    });
}

@end
