//
//  ViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "ViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZToggleButtonsViewController.h"
#import "CZCheckBoxViewController.h"
#import "CZButtonViewController.h"
#import "CZSliderViewController.h"
#import "CZTextFieldViewController.h"
#import "CZDropdownViewController.h"
#import "CZRadioButtonViewController.h"
#import "CZHorizontalMenuViewController.h"
#import "CZSwitchViewController.h"
#import "CZDefaultColorPicker.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *dataSourceArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"CZVisualDesignSystemKitDemo";

    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    UIButton *rightBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBarButton setTitle:@"Change Theme" forState:UIControlStateNormal];
    [rightBarButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBarButton addTarget:self
                       action:@selector(switchThemeAction:)
             forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButton];
    
    self.dataSourceArray = [NSArray arrayWithObjects:@"CZButton",
                                                     @"CZSlider",
                                                     @"CZToggleButton",
                                                     @"CZDropdownButton",
                                                     @"CZCheckBox",
                                                     @"CZRadioButton",
                                                     @"CZTextField",
                                                     @"CZHorizontalMenu",
                                                     @"CZSwitch",
                                                     nil];
    
}

#pragma mark - Action
- (void)switchThemeAction:(UIButton *)sender {
    // TODO: switch theme
    srandom((unsigned int)time(NULL));
    int32_t randomTheme = random() % 2;
    
    NSLog(@"Current theme: %d", randomTheme);
    switch (randomTheme) {
        case CZThemeLight: {
            [sender setTitle:@"Light" forState:UIControlStateNormal];
        }
            break;
        case CZThemeDark: {
            [sender setTitle:@"Dark" forState:UIControlStateNormal];
        }
            break;
        case CZThemeBlue: {
            [sender setTitle:@"Blue" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    [[CZThemeManager sharedManager] switchThemeTo:randomTheme];
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.dataSourceArray.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewController"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ViewController"];
        }
        
        cell.textLabel.text = [self.dataSourceArray objectAtIndex:indexPath.row];
        cell.textLabel.cz_textColorPicker = [CZColorTable defaultTextColor];
        cell.cz_backgroundColorPicker = [CZColorTable defaultBackgroundColor];
        return cell;
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Switch State" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonSelectAction:)];
    NSString *title = [self.dataSourceArray objectAtIndex:indexPath.row];
    switch (indexPath.row) {
        case 0: {
            // CZButton
            CZButtonViewController *buttonsVC = [[CZButtonViewController alloc] init];
            buttonsVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            buttonsVC.title = title;
            [self.navigationController pushViewController:buttonsVC animated:YES];
        }
            break;
        case 1: {
            // CZSlider
            CZSliderViewController *sliderVC = [[CZSliderViewController alloc] init];
            sliderVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            sliderVC.title = title;
            [self.navigationController pushViewController:sliderVC animated:YES];
        }
            break;
        case 2: {
            // CZToggleButton
            CZToggleButtonsViewController *toggleButtonsVC = [[CZToggleButtonsViewController alloc] init];
            toggleButtonsVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            toggleButtonsVC.title = title;
            [self.navigationController pushViewController:toggleButtonsVC animated:YES];
        }
            break;
        case 3: {
            // CZDropdownButton
            CZDropdownViewController *dropdownVC = [[CZDropdownViewController alloc] init];
            dropdownVC.title = title;
            dropdownVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:dropdownVC animated:YES];
        }
            break;
        case 4: {
            // CZCheckBox
            CZCheckBoxViewController *checkBoxVC = [[CZCheckBoxViewController alloc] init];
            checkBoxVC.title = title;
            checkBoxVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:checkBoxVC animated:YES];
        }
            break;
        case 5: {
            // CZRadioButton
            CZRadioButtonViewController *radioButtonVC = [[CZRadioButtonViewController alloc] init];
            radioButtonVC.title = title;
            radioButtonVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:radioButtonVC animated:YES];
        }
            break;
        case 6: {
            // CZTextField
            CZTextFieldViewController *textFieldVC = [[CZTextFieldViewController alloc] init];
            textFieldVC.title = title;
            textFieldVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:textFieldVC animated:YES];
        }
            break;
        case 7: {
            // CZHorizontalMenu
            CZHorizontalMenuViewController *horizontalMenuVC = [[CZHorizontalMenuViewController alloc] init];
            horizontalMenuVC.title = title;
            horizontalMenuVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:horizontalMenuVC animated:YES];
        }
            break;
        case 8: {
            CZSwitchViewController *switchVC = [[CZSwitchViewController alloc] init];
            switchVC.title = title;
            switchVC.navigationItem.rightBarButtonItem = rightBarButtonItem;
            [self.navigationController pushViewController:switchVC animated:YES];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Action
- (void)rightBarButtonSelectAction:(UIBarButtonItem *)sender {
    UIViewController *viewController = self.navigationController.topViewController;
    if (viewController && [viewController isKindOfClass:[CZBaseViewController class]]) {
        srandom((unsigned int)time(NULL));
        CZControlState randomeState = (NSInteger)[NSDate timeIntervalSinceReferenceDate] % 5;
        [(CZBaseViewController *)viewController switchButtonsState:randomeState];
        NSString *title = nil;
        switch (randomeState) {
            case CZControlStateEnable:
                title = @"CZControlStateEnable";
                break;
            case CZControlStateDisable:
                title = @"CZControlStateDisable";
                break;
            case CZControlStateFocused:
                title = @"CZControlStateFocused";
                break;
            case CZControlStatePressed:
                title = @"CZControlStatePressed";
                break;
            case CZControlStateHover:
            default:
                title = @"Switch state";
                break;
        }
        sender.title = title;
    }
}

@end
