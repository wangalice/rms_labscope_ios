//
//  CZTextFieldViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZTextFieldViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"
#import "CZSVGImageAssert.h"

@interface CZTextFieldViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *textFieldArray;
@property (nonatomic, weak)  UITextField *textField;

@end

@implementation CZTextFieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    [self setup];
    
    UITapGestureRecognizer  *tapGestureRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(stopEditing:)];
    [self.view addGestureRecognizer:tapGestureRec];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.textField = textField;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}


#pragma mark - Private methods

- (void)setup {
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 120;
    CGFloat height = 32;
    CGFloat y = 64 + 20;
    
    CZTextField *textField1 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:nil
                                      rightImagePicker:NULL];
    textField1.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField1];
    
    y = CGRectGetMaxY(textField1.frame) + verticalMargin;
    CZTextField *textField2 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:nil
                                      rightImagePicker:NULL];
    textField2.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField2];

    y = CGRectGetMaxY(textField2.frame) + verticalMargin;
        CZTextField *textField3 = [self textFieldWithStyle:CZTextFieldStyleReadOnly
                                                  unit:nil
                                      rightImagePicker:NULL];
    textField3.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField3];
    
    y = CGRectGetMaxY(textField3.frame) + verticalMargin;
    CZTextField *textField4 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:@"mm"
                                      rightImagePicker:NULL];
    textField4.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField4];
    
    y = CGRectGetMaxY(textField4.frame) + verticalMargin;
    CZTextField *textField5 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:@"mm"
                                      rightImagePicker:NULL];
    textField5.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField5];
    
    y = CGRectGetMaxY(textField5.frame) + verticalMargin;
    CZTextField *textField6 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:@"mm"
                                      rightImagePicker:NULL];
    textField6.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField6];
    
    y = CGRectGetMaxY(textField6.frame) + verticalMargin;
    CZTextField *textField7 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:nil
                                      rightImagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-counter"), SVGKitImageAssert(@"dark-counter"))];
    textField7.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField7];
    
    y = CGRectGetMaxY(textField7.frame) + verticalMargin;
    CZTextField *textField8 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:nil
                                      rightImagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))];
    textField8.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField8];
    
    y = CGRectGetMaxY(textField8.frame) + verticalMargin;
    CZTextField *textField9 = [self textFieldWithStyle:CZTextFieldStyleDefault
                                                  unit:nil
                                      rightImagePicker:NULL];
    textField9.textAlignment = NSTextAlignmentRight;
    textField9.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:textField9];

}

- (CZTextField *)textFieldWithStyle:(CZTextFieldStyle)style unit:(NSString *)unit rightImagePicker:(CZThemeImagePicker)imagePicker {
    CZTextField *textField = [[CZTextField alloc] initWithStyle:style
                                                           unit:unit
                                               rightImagePicker:imagePicker];
    textField.textAlignment = NSTextAlignmentLeft;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.placeholder = @"Enable";
    textField.delegate = self;
    return textField;
}

#pragma mark - Action

- (void)stopEditing:(UITapGestureRecognizer *)rec {
    if (self.textField) {
        [self.textField endEditing:YES];
    }
}

#pragma mark - Getters

- (NSMutableArray *)textFieldArray {
    if (!_textFieldArray) {
        _textFieldArray = [NSMutableArray array];
    }
    return _textFieldArray;
}

@end
