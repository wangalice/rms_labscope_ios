//
//  CZHorizontalMenuViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZHorizontalMenuViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZHorizontalMenuViewController ()

@end

@implementation CZHorizontalMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = [UIScreen mainScreen].bounds.size.width - horizontalMargin * 2;
    CGFloat height_L = 48;
    CGFloat y = 64 + 20;
    
    CZHorizontalMenu *horizontalMenu = [self horizontalMenuWithItems:@[@"One", @"Two"]
                                                               style:kCZHorizontalMenuStyleActive];
    horizontalMenu.tag = 0;
    horizontalMenu.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:horizontalMenu];
    
    y = CGRectGetMaxY(horizontalMenu.frame) + verticalMargin;
    
    CZHorizontalMenu *horizontalMenu2 = [self horizontalMenuWithItems:@[@"One", @"Two"]
                                                                      style:kCZHorizontalMenuStyleActivePrimary];
    horizontalMenu2.tag = 1;
    horizontalMenu2.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:horizontalMenu2];
}

- (CZHorizontalMenu *)horizontalMenuWithItems:(NSArray *)items style:(CZHorizontalMenuStyle)style{
    CZHorizontalMenu *horizontalMenu  = [[CZHorizontalMenu alloc] initWithItems:items style:style];
    [horizontalMenu addTarget:self
                       action:@selector(menuSelectionEvent:)
             forControlEvents:UIControlEventValueChanged];
    return horizontalMenu;
}

- (void)menuSelectionEvent:(CZHorizontalMenu *)sender {
    NSLog(@"Menu button select event! SelectedIndex: %lu", (unsigned long)sender.selectedIndex);
}

@end
