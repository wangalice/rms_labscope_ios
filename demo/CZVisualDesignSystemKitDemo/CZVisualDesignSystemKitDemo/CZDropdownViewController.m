//
//  CZDropdownViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/22.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDropdownViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"
#import "CZSVGImageAssert.h"

@interface CZDropdownViewController ()

@property (nonatomic, strong) NSMutableArray *dropdownArray;

@end

@implementation CZDropdownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];

    [self setup];
}

- (void)setup {
    CGFloat horizontalMargin = 20;
    CGFloat main_text_width = 160;
    CGFloat main_icon_width = 48;
    CGFloat altertive_text = 80;
    CGFloat altertive_icon_text = 104;
    CGFloat altertive_icon = 32;
    CGFloat height = 32;
    
    CGFloat y = 64 + 80;
    CGFloat x = horizontalMargin;
    CZDropdownItem *dropdownItem1 = [CZDropdownItem itemWithTitle:@"enable"
                                                      imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"),

                                                                                               SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownItem *dropdownItem2 = [CZDropdownItem itemWithTitle:@"enable"
                                                     imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"),
                                                                                              
                                                                                              SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownItem *dropdownItem3 = [CZDropdownItem itemWithTitle:@"enable"
                                                      imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"),
                                                                                               
                                                                                               SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownMenu *dropdownBtn1 = [self dropdownButtonWithStyle:kCZDropdownStyleMain
                                                           title:@"choose"
                                                     imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"),
                                                                                              
                                                                                              SVGKitImageAssert(@"dark-microphone"))
                                                     expandItems:@[dropdownItem1, dropdownItem2, dropdownItem3]];
    dropdownBtn1.tag = 1;
    dropdownBtn1.frame = CGRectMake(x, y, main_text_width, height);
    dropdownBtn1.animationDirection = CZDropdownAnimationDirectionUp;
    [dropdownBtn1 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn1, (unsigned long)index);
    }];
    
    x = CGRectGetMaxX(dropdownBtn1.frame) + horizontalMargin;
    CZDropdownItem *dropdownItem4 = [CZDropdownItem itemWithTitle:@"enable"];
    CZDropdownItem *dropdownItem5 = [CZDropdownItem itemWithTitle:@"enable"];
    CZDropdownItem *dropdownItem6 = [CZDropdownItem itemWithTitle:@"enable"];
    CZDropdownMenu *dropdownBtn2 = [self dropdownButtonWithStyle:kCZDropdownStyleMain
                                                           title:@"choose"
                                                     imagePicker:NULL
                                                     expandItems:@[dropdownItem4, dropdownItem5, dropdownItem6]];
    dropdownBtn2.frame = CGRectMake(x, y, main_text_width, height);
    dropdownBtn2.tag = 2;
    [dropdownBtn2 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn2, (unsigned long)index);
    }];
    
    x = CGRectGetMaxX(dropdownBtn2.frame) + horizontalMargin;
    CZDropdownItem *dropdownItem7 = [CZDropdownItem itemWithImagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownItem *dropdownItem8 = [CZDropdownItem itemWithImagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownItem *dropdownItem9 = [CZDropdownItem itemWithImagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))];
    CZDropdownMenu *dropdownBtn3 = [self dropdownButtonWithStyle:kCZDropdownStyleMain
                                                           title:nil
                                                     imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))
                                                     expandItems:@[dropdownItem7, dropdownItem8, dropdownItem9]];
    dropdownBtn3.frame = CGRectMake(x, y, main_icon_width, height);
    dropdownBtn3.tag = 3;
    [dropdownBtn3 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn3, (unsigned long)index);
    }];
    
    x = CGRectGetMaxX(dropdownBtn3.frame) + horizontalMargin;
    CZDropdownItem *dropdownItem10 = [CZDropdownItem itemWithTitle:@"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdef"];
    CZDropdownItem *dropdownItem11 = [CZDropdownItem itemWithTitle:@"张三李四王五赵六田七张三李四王五赵六田七张三李四王五赵六田七"];
    CZDropdownItem *dropdownItem12 = [CZDropdownItem itemWithTitle:@"abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefgh"
                                                       imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))];
    NSArray *autoSizeDropDownItems = @[dropdownItem1,
                                       dropdownItem4,
                                       dropdownItem7,
                                       dropdownItem10,
                                       dropdownItem11,
                                       dropdownItem12];
    
    CZDropdownMenu *autoSizeDropdownBtn = [self dropdownButtonWithStyle:kCZDropdownStyleMain
                                                                  title:nil
                                                            imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))
                                                            expandItems:autoSizeDropDownItems];
    autoSizeDropdownBtn.frame = CGRectMake(x, y, main_icon_width, height);
    autoSizeDropdownBtn.tag = 3;
    [autoSizeDropdownBtn selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", autoSizeDropdownBtn, (unsigned long)index);
    }];
    
    x = horizontalMargin;
    y = CGRectGetMaxY(dropdownBtn1.frame) + 300;
    CZDropdownMenu *dropdownBtn4 = [self dropdownButtonWithStyle:kCZDropdownStyleAlternative
                                                           title:@"choose"
                                                     imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))
                                                     expandItems:@[dropdownItem1, dropdownItem2, dropdownItem3]];
    dropdownBtn4.frame = CGRectMake(x, y, altertive_icon_text, height);
    dropdownBtn4.tag = 4;
    [dropdownBtn4 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn4, (unsigned long)index);
    }];

    x = horizontalMargin + CGRectGetMaxX(dropdownBtn4.frame);
    CZDropdownMenu *dropdownBtn5 = [self dropdownButtonWithStyle:kCZDropdownStyleAlternative
                                                           title:@"choose"
                                                     imagePicker:NULL
                                                     expandItems:@[dropdownItem4, dropdownItem5, dropdownItem6]];
    dropdownBtn5.frame = CGRectMake(x, y, altertive_text, height);
    dropdownBtn5.tag = 5;
    [dropdownBtn5 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn5, (unsigned long)index);
    }];

    x = horizontalMargin + CGRectGetMaxX(dropdownBtn5.frame);
    CZDropdownMenu *dropdownBtn6 = [self dropdownButtonWithStyle:kCZDropdownStyleAlternative
                                                           title:nil
                                                     imagePicker:CZThemeImagePickerWithImages(SVGKitImageAssert(@"dark-microphone"), SVGKitImageAssert(@"dark-microphone"))
                                                     expandItems:@[dropdownItem7, dropdownItem8, dropdownItem9]];
    dropdownBtn6.frame = CGRectMake(x, y, altertive_icon, height);
    dropdownBtn6.tag = 6;
    [dropdownBtn6 selectItemAction:^(NSUInteger index) {
        NSLog(@"%@: Select item index: %lu", dropdownBtn6, (unsigned long)index);
    }];

    [self.view addSubview:dropdownBtn1];
    [self.view addSubview:dropdownBtn2];
    [self.view addSubview:dropdownBtn3];
    [self.view addSubview:dropdownBtn4];
    [self.view addSubview:dropdownBtn5];
    [self.view addSubview:dropdownBtn6];
    [self.view addSubview:autoSizeDropdownBtn];
    
    [_dropdownArray addObjectsFromArray:@[dropdownBtn1,
                                          dropdownBtn2,
                                          dropdownBtn3,
                                          dropdownBtn4,
                                          dropdownBtn5,
                                          dropdownBtn6,
                                          autoSizeDropdownBtn]];
}

- (CZDropdownMenu *)dropdownButtonWithStyle:(CZDropdownStyle)style
                                      title:(NSString *)title
                                imagePicker:(CZThemeImagePicker)imagePicker
                                  expandItems:(NSArray <CZDropdownItem *> *)items {
    CZDropdownMenu *dropdownMenu = [[CZDropdownMenu alloc] initWithStyle:style title:title imagePicker:imagePicker expandItems:items];
    return dropdownMenu;
}

#pragma mark - Getters

- (NSMutableArray *)dropdownArray {
    if (!_dropdownArray) {
        _dropdownArray = [NSMutableArray array];
    }
    return _dropdownArray;
}

@end
