//
//  CZColor.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZDefaultColorPicker.h"

@implementation CZDefaultColorPicker

+ (CZThemeColorPicker)backgroundColorPicker {
    return CZThemeColorWithRGBs(0x384047, 0xFFFFFF);
}

+ (CZThemeImagePicker)defaultImage {
    return CZThemeImagePickerWithImages([UIImage imageNamed:@"dark-dropdpwn-default"], [UIImage imageNamed:@"dark-dropdpwn-default"]);
}

@end
