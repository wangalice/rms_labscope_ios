//
//  CZBaseViewController.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/6.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZNavigationControllerProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZBaseViewController : UIViewController <CZNavigationControllerProtocol>

@end

NS_ASSUME_NONNULL_END
