//
//  CZRadioButtonViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZRadioButtonViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZRadioButtonViewController ()

@property (nonatomic, strong) NSMutableArray *radioArrays;

@end

@implementation CZRadioButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    if (!_radioArrays) {
        _radioArrays = [NSMutableArray array];
    }
    
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 74;
    CGFloat height = 21;
    
    CGFloat y = 64 + 20;
    
    CZRadioButton *radioBtn1 = [[CZRadioButton alloc] initWithFrame:CGRectMake(verticalMargin, y, width, height)];
    [radioBtn1 setTitle:@"Enabled" forState:UIControlStateNormal];
    radioBtn1.tag = 1;
    [radioBtn1 addTarget:self action:@selector(selectRadioButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    y = CGRectGetMaxY(radioBtn1.frame) + horizontalMargin;
    
    CZRadioButton *radioBtn2 = [[CZRadioButton alloc] initWithFrame:CGRectMake(verticalMargin, y, width, height)];
    [radioBtn2 setTitle:@"Enabled" forState:UIControlStateNormal];
    radioBtn2.tag = 2;
    [radioBtn2 addTarget:self action:@selector(selectRadioButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:radioBtn1];
    [self.view addSubview:radioBtn2];
}

- (void)selectRadioButtonEvent:(CZRadioButton *)sender {
    sender.selected = !sender.selected;
    NSLog(@"%s", __func__);
}

@end
