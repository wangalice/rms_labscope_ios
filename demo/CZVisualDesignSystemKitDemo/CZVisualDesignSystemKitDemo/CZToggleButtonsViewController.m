//
//  CZToggleButtonsViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/1/29.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZToggleButtonsViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZToggleButtonsViewController ()

@property (nonatomic, strong) NSMutableArray <CZToggleButton *> *buttonsArray;

@end

@implementation CZToggleButtonsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupView];
}

#pragma mark - Private Methods

- (void)setupView {
    
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = [UIScreen mainScreen].bounds.size.width - horizontalMargin * 2;
    CGFloat height_S = 32;
    CGFloat height_L = 48;
    CGFloat y = 64 + 20;
    
    CZToggleButton *toggleNormalButton_S = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleNormalButton_S.tag = 0;
    toggleNormalButton_S.frame = CGRectMake(horizontalMargin, y, width, height_S);
    [self.view addSubview:toggleNormalButton_S];
    
    y = CGRectGetMaxY(toggleNormalButton_S.frame) + verticalMargin;
   
    CZToggleButton *toggleActiaveButton_S = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleActiaveButton_S.tag = 1;
    toggleActiaveButton_S.selectedIndex = 1;
    toggleActiaveButton_S.frame = CGRectMake(horizontalMargin, y, width, height_S);
    [self.view addSubview:toggleActiaveButton_S];
    
    y = CGRectGetMaxY(toggleActiaveButton_S.frame) + verticalMargin;
    
    CZToggleButton *toggleActiavePrimaryButton_S = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleActiavePrimaryButton_S.level = CZControlEmphasisActivePrimary;
    toggleActiavePrimaryButton_S.tag = 2;
    toggleActiavePrimaryButton_S.selectedIndex = 1;
    toggleActiavePrimaryButton_S.frame = CGRectMake(horizontalMargin, y, width, height_S);
    [self.view addSubview:toggleActiavePrimaryButton_S];
    
    y = CGRectGetMaxY(toggleActiavePrimaryButton_S.frame) + verticalMargin;
    
    CZToggleButton *toggleNormalButton_L = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleNormalButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    toggleNormalButton_L.tag = 3;
    [self.view addSubview:toggleNormalButton_L];
    
    y = CGRectGetMaxY(toggleNormalButton_L.frame) + verticalMargin;
    
    CZToggleButton *toggleActiaveButton_L = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleActiaveButton_L.tag = 4;
    toggleActiaveButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:toggleActiaveButton_L];
    
    y = CGRectGetMaxY(toggleActiaveButton_L.frame) + verticalMargin;
    
    CZToggleButton *toggleActiavePrimaryButton_L = [self toggleButtonWithItems:@[@"One", @"Two"]];
    toggleActiavePrimaryButton_S.level = CZControlEmphasisActivePrimary;
    toggleActiavePrimaryButton_L.tag = 5;
    toggleActiavePrimaryButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:toggleActiavePrimaryButton_L];
    
    y = CGRectGetMaxY(toggleActiavePrimaryButton_L.frame) + verticalMargin;
    
    CZToggleButton *togglePrimaryThreeButton_L = [self toggleButtonWithItems:@[@"One", @"Two", @"Three"]];
    togglePrimaryThreeButton_L.level = CZControlEmphasisActivePrimary;
    togglePrimaryThreeButton_L.selectedIndex = 0;
    togglePrimaryThreeButton_L.tag = 6;
    togglePrimaryThreeButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:togglePrimaryThreeButton_L];
    
    y = CGRectGetMaxY(togglePrimaryThreeButton_L.frame) + verticalMargin;
    
    CZToggleButton *toggleActiaveThreeButton_L = [self toggleButtonWithItems:@[@"One", @"Two", @"Three"]];
    toggleActiaveThreeButton_L.level = CZControlEmphasisActivePrimary;
    toggleActiaveThreeButton_L.selectedIndex = 1;
    toggleActiaveThreeButton_L.tag = 7;
    toggleActiaveThreeButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:toggleActiaveThreeButton_L];
    
    y = CGRectGetMaxY(toggleActiaveThreeButton_L.frame) + verticalMargin;
    
    CZToggleButton *toggleActiavePrimaryThreeButton_L = [self toggleButtonWithItems:@[@"One", @"Two", @"Three"]];
    toggleActiavePrimaryThreeButton_L.level = CZControlEmphasisActivePrimary;
    toggleActiavePrimaryThreeButton_L.selectedIndex = 2;
    toggleActiavePrimaryThreeButton_L.tag = 8;
    toggleActiavePrimaryThreeButton_L.frame = CGRectMake(horizontalMargin, y, width, height_L);
    [self.view addSubview:toggleActiavePrimaryThreeButton_L];
    
    [self.buttonsArray addObjectsFromArray:@[toggleNormalButton_S,
                                             toggleActiaveButton_S,
                                             toggleActiavePrimaryButton_S,
                                             toggleNormalButton_L,
                                             toggleActiaveButton_L,
                                             toggleActiaveButton_L,
                                             toggleActiavePrimaryButton_L,
                                             togglePrimaryThreeButton_L,
                                             toggleActiaveThreeButton_L,
                                             toggleActiavePrimaryThreeButton_L]];
}

- (CZToggleButton *)toggleButtonWithItems:(NSArray *)items {
    CZToggleButton *toggleButton = [[CZToggleButton alloc] initWithItems:items];
    [toggleButton addTarget:self
                     action:@selector(toggleSelectAction:)
           forControlEvents:UIControlEventValueChanged];
    return toggleButton;
}

#pragma mark - Action
- (void)toggleSelectAction:(CZToggleButton *)sender {
    NSLog(@"Toggle button select event! SelectedIndex: %lu", (unsigned long)sender.selectedIndex);
}

#pragma mark - CZNavigationControllerProtocol
- (void)switchButtonsState:(CZControlState)state {
    for (CZToggleButton *button in self.buttonsArray) {
        switch (state) {
            case CZControlStateEnable: {
                button.enabled = YES;
            }
                break;
            case CZControlStateDisable: {
                button.enabled = NO;
            }
                break;
            case CZControlStateFocused:
                break;
            case CZControlStatePressed: {
                button.selected = YES;
            }
                break;
            case CZControlStateHover:
            default:
            {
                button.selected = NO;
                button.enabled = YES;
            }
                break;
        }
    }
}

#pragma mark - Getters
- (NSMutableArray *)buttonsArray {
    if (!_buttonsArray) {
        _buttonsArray = [NSMutableArray array];
    }
    return _buttonsArray;
}

@end
