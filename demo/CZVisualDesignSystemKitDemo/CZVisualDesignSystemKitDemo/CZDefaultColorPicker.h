//
//  CZColor.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZDefaultColorPicker : NSObject

+ (CZThemeColorPicker)backgroundColorPicker;

@end

NS_ASSUME_NONNULL_END
