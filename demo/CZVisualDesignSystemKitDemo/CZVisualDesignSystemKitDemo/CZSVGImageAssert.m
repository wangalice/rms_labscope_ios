//
//  CZSVGImageAssert.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSVGImageAssert.h"
#import <SVGKit/SVGKit.h>

UIImage *SVGKitImageAssert(NSString *imageName) {
    NSDataAsset *asset = [[NSDataAsset alloc] initWithName:imageName];
    SVGKSource *source = [SVGKSourceNSData sourceFromData:asset.data URLForRelativeLinks:nil];
    if (source == nil) {
        source = [SVGKSourceLocalFile internalSourceAnywhereInBundleUsingName:imageName];
    }
    SVGKImage *image = [SVGKImage imageWithSource:source];
    return image.UIImage;
}


@implementation CZSVGImageAssert

@end
