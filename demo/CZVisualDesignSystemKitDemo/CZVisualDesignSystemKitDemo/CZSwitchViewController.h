//
//  CZSwitchViewController.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZSwitchViewController : CZBaseViewController

@end

NS_ASSUME_NONNULL_END
