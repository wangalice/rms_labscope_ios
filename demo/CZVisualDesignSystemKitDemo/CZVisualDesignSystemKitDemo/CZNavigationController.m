
//
//  CZNavigationController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZNavigationController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>

@interface CZNavigationController ()

@end

@implementation CZNavigationController


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 5, 5)];
    [self.navigationController.navigationBar setBackgroundImage:[[CZShapeImage imageWithPath:path
                                                                                   fillColor:[UIColor cz_gs120]] UIImage] forBarMetrics:UIBarMetricsDefault];

}


@end
