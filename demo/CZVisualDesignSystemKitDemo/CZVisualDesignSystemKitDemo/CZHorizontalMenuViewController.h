//
//  CZHorizontalMenuViewController.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/2/27.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZHorizontalMenuViewController : CZBaseViewController

@end

NS_ASSUME_NONNULL_END
