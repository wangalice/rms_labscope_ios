//
//  CZImageAssert.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/4.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef __cplusplus
extern "C" {
#endif
    
    UIImage *SVGKitImageAssert(NSString *imageName);
#ifdef __cplusplus
}
#endif

@interface CZSVGImageAssert : NSObject

@end
