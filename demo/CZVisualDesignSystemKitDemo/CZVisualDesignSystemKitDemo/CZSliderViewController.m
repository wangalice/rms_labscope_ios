//
//  CZSliderViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/20.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSliderViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZSliderViewController ()

@property (nonatomic, strong) NSMutableArray <CZSlider *>* slidersArray;

@end

@implementation CZSliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];

    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 192;
    CGFloat height = 32;
    CGFloat y = 64 + 20;

    
    CZSlider *slider1 = [self sliderWithIndex:1 maximumValue:1.0f minimumValue:1.0f];
    slider1.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:slider1];
    
    y = CGRectGetMaxY(slider1.frame) + verticalMargin;
    CZSlider *slider2 = [self sliderWithIndex:2 maximumValue:10.f minimumValue:0.0f];
    slider2.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:slider2];

    y = CGRectGetMaxY(slider2.frame) + verticalMargin;
    CZSlider *slider3 = [self sliderWithIndex:3 maximumValue:100.0f minimumValue:10.0f];
    slider3.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:slider3];
    
    y = CGRectGetMaxY(slider3.frame) + verticalMargin;
    CZSlider *slider4 = [self sliderWithIndex:4 maximumValue:1000.0f minimumValue:0.0f];
    slider4.frame = CGRectMake(horizontalMargin, y, width, height);
    [self.view addSubview:slider4];
    
    [self.slidersArray addObjectsFromArray:@[slider1,
                                             slider2,
                                             slider3,
                                             slider4]];
}

- (CZSlider *)sliderWithIndex:(NSInteger)index maximumValue:(float)maximumValue minimumValue:(float)minimumValue{
    CZSlider *slider = [[CZSlider alloc] init];
    [slider addTarget:self
               action:@selector(sliderValueChanged:)
     forControlEvents:UIControlEventValueChanged];
    return slider;
}

#pragma mark - Action

- (void)sliderValueChanged:(CZSlider *)sender {
    NSLog(@"%s: valueChanged:%f", __func__, sender.value);
}

#pragma mark - CZNavigationControllerProtocol
- (void)switchButtonsState:(CZControlState)state {
    
    for (CZSlider *slider in self.slidersArray) {
        switch (state) {
            case CZControlStateEnable: {
                slider.enabled = YES;
            }
                break;
            case CZControlStateDisable: {
                slider.enabled = NO;
            }
                break;
            case CZControlStateFocused:
                break;
            case CZControlStatePressed: {
                slider.selected = YES;
            }
                break;
            case CZControlStateHover:
            default:
            {
                slider.selected = NO;
                slider.enabled = YES;
            }
                break;
        }
    }
}

#pragma mark - Getters

- (NSMutableArray<CZSlider *> *)slidersArray {
    if (!_slidersArray) {
        _slidersArray = [NSMutableArray array];
    }
    return _slidersArray;
}

@end
