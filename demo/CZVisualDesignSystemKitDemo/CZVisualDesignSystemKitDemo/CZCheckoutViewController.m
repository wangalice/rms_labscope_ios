//
//  CZCheckoutViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by ZEISS on 2019/2/20.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCheckBoxViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>

@interface CZCheckBoxViewController ()

@property (nonatomic, strong) NSMutableArray *checkBoxArrays;

@end

@implementation CZCheckBoxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_checkBoxArrays) {
        _checkBoxArrays = [NSMutableArray array];
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 74;
    CGFloat height = 21;
    
    CGFloat y = 64 + 20;
    
    CZCheckBox *checkBox1 = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
    checkBox1.frame = CGRectMake(verticalMargin, y, width, height);
    checkBox1.tag = 1;
    [checkBox1 addTarget:self action:@selector(selectCheckBoxEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    y = CGRectGetMaxY(checkBox1.frame) + horizontalMargin;
    
    CZCheckBox *checkBox2 = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeMixed];
    checkBox2.frame = CGRectMake(verticalMargin, y, width, height);
    checkBox2.tag = 2;
    [checkBox2 addTarget:self action:@selector(selectCheckBoxEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:checkBox1];
    [self.view addSubview:checkBox2];
}

- (void)selectCheckBoxEvent:(UIButton *)sender {
    sender.selected = !sender.selected;
    NSLog(@"%s", __func__);
}

@end
