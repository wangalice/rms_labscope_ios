//
//  CZNavigationController.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
