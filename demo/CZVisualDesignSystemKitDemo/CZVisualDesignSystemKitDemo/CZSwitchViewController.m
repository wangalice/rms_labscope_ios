//
//  CZSwitchViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/4/8.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZSwitchViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZSwitchViewController ()

@property (nonatomic, strong) NSMutableArray *switchButtonArray;

@end

@implementation CZSwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    if (!_switchButtonArray) {
        _switchButtonArray = [NSMutableArray array];
    }
    
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 74;
    CGFloat height = 21;
    
    CGFloat y = 64 + 20;
    
    CZSwitch *switchBtn1 = [[CZSwitch alloc] initWithFrame:CGRectMake(horizontalMargin, verticalMargin + 64, width, height)];
    switchBtn1.on = YES;
    
    
    y = CGRectGetMaxY(switchBtn1.frame) + horizontalMargin;
    
    CZSwitch *switchBtn2 = [[CZSwitch alloc] initWithFrame:CGRectMake(horizontalMargin, y, width, height)];
    [switchBtn2 setLevel:CZControlEmphasisActivePrimary];
    
    y = CGRectGetMaxY(switchBtn2.frame) + horizontalMargin;
    
    UISwitch *switchBtn3 = [[UISwitch alloc] initWithFrame:CGRectMake(horizontalMargin, y, width, height)];
    switchBtn3.tintColor = [UIColor blackColor];
    switchBtn3.backgroundColor = [UIColor blackColor];
    switchBtn3.onTintColor = [UIColor redColor];
    switchBtn3.thumbTintColor = [UIColor whiteColor];

    [self.view addSubview:switchBtn1];
    [self.view addSubview:switchBtn2];
    [self.view addSubview:switchBtn3];
}



@end
