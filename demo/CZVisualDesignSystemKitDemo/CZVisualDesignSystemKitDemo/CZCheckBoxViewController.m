//
//  CZCheckoutViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/20.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZCheckBoxViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"

@interface CZCheckBoxViewController ()

@property (nonatomic, strong) NSMutableArray <CZCheckBox *> *checkBoxArrays;

@end

@implementation CZCheckBoxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_checkBoxArrays) {
        _checkBoxArrays = [NSMutableArray array];
    }
    
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 80;
    CGFloat height = 50;
    
    CGFloat y = 64 + 20;
    
    CZCheckBox *checkBox1 = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeActive];
    [checkBox1 setTitle:@"Enabled" forState:UIControlStateNormal];
    checkBox1.frame = CGRectMake(verticalMargin, y, width, height);
    checkBox1.tag = 1;
    [checkBox1 addTarget:self action:@selector(selectCheckBoxEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    y = CGRectGetMaxY(checkBox1.frame) + horizontalMargin;
    
    CZCheckBox *checkBox2 = [[CZCheckBox alloc] initWithType:kCZCheckBoxTypeMixed];
    [checkBox2 setTitle:@"Enabled" forState:UIControlStateNormal];
    checkBox2.frame = CGRectMake(verticalMargin, y, width, height);
    checkBox2.tag = 2;
    [checkBox2 addTarget:self action:@selector(selectCheckBoxEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:checkBox1];
    [self.view addSubview:checkBox2];
}

- (void)selectCheckBoxEvent:(CZCheckBox *)sender {
    sender.selected = !sender.selected;
    NSLog(@"%s", __func__);
}

#pragma mark - CZNavigationControllerProtocol
- (void)switchButtonsState:(CZControlState)state {
    for (CZCheckBox *checkBox in self.checkBoxArrays) {
        switch (state) {
            case CZControlStateEnable: {
                checkBox.enabled = YES;
            }
                break;
            case CZControlStateDisable: {
                checkBox.enabled = NO;
            }
                break;
            case CZControlStateFocused:
                break;
            case CZControlStatePressed: {
                checkBox.selected = YES;
            }
                break;
            case CZControlStateHover:
            default:
            {
                checkBox.selected = NO;
                checkBox.enabled = YES;
            }
                break;
        }
    }
}

#pragma mark - Getters
- (NSMutableArray<CZCheckBox *> *)checkBoxArrays {
    if (!_checkBoxArrays) {
        _checkBoxArrays = [NSMutableArray array];
    }
    return _checkBoxArrays;
}

@end
