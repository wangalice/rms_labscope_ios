//
//  CZNavigationControllerProtocol.h
//  CZVisualDesignSystemKitDemo
//
//  Created by Carl Zeiss on 2019/3/6.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CZNavigationControllerProtocol <NSObject>

- (void)switchButtonsState:(CZControlState)state;

@end

NS_ASSUME_NONNULL_END
