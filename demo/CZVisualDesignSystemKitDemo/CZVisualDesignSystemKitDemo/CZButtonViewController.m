//
//  CZButtonViewController.m
//  CZVisualDesignSystemKitDemo
//
//  Created by Sun, Shaoge on 2019/2/18.
//  Copyright © 2019 Carl Zeiss. All rights reserved.
//

#import "CZButtonViewController.h"
#import <CZVisualDesignSystemKit/CZVisualDesignSystemKit.h>
#import "CZDefaultColorPicker.h"
#import "CZSVGImageAssert.h"

@interface CZButtonViewController ()

@property (nonatomic, strong) NSMutableArray <CZButton *>*buttonArrays;

@end

@implementation CZButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.cz_backgroundColorPicker = [CZDefaultColorPicker backgroundColorPicker];
    
    CGFloat horizontalMargin = 20;
    CGFloat verticalMargin = 40;
    CGFloat width = 120;
    CGFloat height_S = 32;
    CGFloat height_L = 48;
    CGFloat width_S = 48;
    CGFloat y = 64 + 20;
    
    //Default button
    CZButton *button1 = [self buttonWithIndex:1
                                        frame:CGRectMake(horizontalMargin, y, width, height_S)
                                        level:CZControlEmphasisDefault];
    [button1 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button1];
    
    //Active button
    y = CGRectGetMaxY(button1.frame) + verticalMargin;
    CZButton *button2 = [self buttonWithIndex:2
                                        frame:CGRectMake(horizontalMargin, y, width, height_S)
                                        level:CZControlEmphasisActive];
    [button2 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button2];
    
    //ActivePrivary button
    y = CGRectGetMaxY(button2.frame) + verticalMargin;
    CZButton *button3 = [self buttonWithIndex:3
                                        frame:CGRectMake(horizontalMargin, y, width, height_S)
                                        level:CZControlEmphasisActivePrimary];
    [button3 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button3];

    y = CGRectGetMaxY(button3.frame) + verticalMargin;
    CZButton *button4 = [self buttonWithIndex:4
                                        frame:CGRectMake(horizontalMargin, y, width, height_L)
                                        level:CZControlEmphasisDefault];
    [button4 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button4];
    
    y = CGRectGetMaxY(button4.frame) + verticalMargin;
    CZButton *button5 = [self buttonWithIndex:5
                                        frame:CGRectMake(horizontalMargin, y, width, height_L)
                                        level:CZControlEmphasisActive];
    [button5 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button5];
    
    y = CGRectGetMaxY(button5.frame) + verticalMargin;
    CZButton *button6 = [self buttonWithIndex:6
                                        frame:CGRectMake(horizontalMargin, y, width, height_L)
                                        level:CZControlEmphasisActivePrimary];
    [button6 setTitle:@"Enable" forState:UIControlStateNormal];
    [self.view addSubview:button6];

    y = CGRectGetMaxY(button6.frame) + verticalMargin;
    CZButton *button7 = [self buttonWithIndex:7
                                        frame:CGRectMake(horizontalMargin, y, width_S, height_L)
                                        level:CZControlEmphasisDefault];
    [button7 setImage:SVGKitImageAssert(@"dark-microphone")
             forState:UIControlStateNormal];
    [self.view addSubview:button7];
    
    y = CGRectGetMaxY(button7.frame) + verticalMargin;
    CZButton *button8 = [self buttonWithIndex:8
                                        frame:CGRectMake(horizontalMargin, y, width_S, height_L)
                                        level:CZControlEmphasisActive];
    [button8 setImage:SVGKitImageAssert(@"dark-microphone")
             forState:UIControlStateNormal];
    [self.view addSubview:button8];
    
    y = CGRectGetMaxY(button8.frame) + verticalMargin;
    CZButton *button9 = [self buttonWithIndex:9
                                        frame:CGRectMake(horizontalMargin, y, width_S, height_L)
                                        level:CZControlEmphasisActivePrimary];
    [button9 setImage:SVGKitImageAssert(@"dark-microphone")
             forState:UIControlStateNormal];
     [self.view addSubview:button9];
    
    self.buttonArrays = [NSMutableArray arrayWithArray:@[button1,
                                                         button2,
                                                         button3,
                                                         button4,
                                                         button5,
                                                         button6,
                                                         button7,
                                                         button8,
                                                         button9]];
}

- (CZButton *)buttonWithIndex:(NSUInteger)index frame:(CGRect)frame level:(CZControlEmphasisLevel)level{
    CZButton *button = [CZButton buttonWithType:UIButtonTypeCustom];
    button.level = level;
    button.frame = frame;
    button.tag = index + 10000;
    [button addTarget:self
               action:@selector(selectAction:)
     forControlEvents:UIControlEventTouchUpInside];
    return button;
}

#pragma mark - CZNavigationControllerProtocol
- (void)switchButtonsState:(CZControlState)state {
    for (CZButton *button in self.buttonArrays) {
        switch (state) {
            case CZControlStateEnable: {
                button.enabled = YES;
            }
                break;
            case CZControlStateDisable: {
                button.enabled = NO;
            }
                break;
            case CZControlStateFocused:
            case CZControlStatePressed:
            case CZControlStateHover:
            default:
                break;
        }
        button.czState = state;
    }
}

#pragma mark - Action

- (void)selectAction:(UIButton *)sender {
    NSLog(@"%s: index: %ld", __func__, (long)sender.tag);
    NSInteger index = sender.tag - 10001;
    if (index % 3 == 0) {
        
    } else if (index % 3 == 1) {
        sender.selected = YES;
    } else if (index % 3 == 2) {
        sender.selected = YES;
    }
}

@end
