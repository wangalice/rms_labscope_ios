#include "stdafx.h"
#include <opencv2/opencv.hpp>

using namespace cv;

char *windowTitle = "Project Falcon";
char *subWindowTitle = "Debug";

void position_HoughContour(Mat& src) {
    const int maxOffset = 30;
    const Scalar red(0, 0, 255);
    const Scalar cyan(255, 255, 0);
    const Scalar green(0, 255, 0);
    const Scalar blue(255, 0, 0);
    const Scalar white(255, 255, 255);

    double thresholdBase = 0.0;
    {
        Mat srcGray;
        Mat srcThreshold;
        Mat srcTemp;
        
        cvtColor(src, srcGray, CV_BGR2GRAY);
        
        // http://stackoverflow.com/a/16047590/1241690
        thresholdBase = threshold(srcGray,
                                  srcTemp,
                                  0,
                                  255,
                                  CV_THRESH_BINARY + CV_THRESH_OTSU);
    }

    Mat canny;
    Canny(src, canny, thresholdBase * 0.33, thresholdBase * 0.66);

    Mat hough = Mat::zeros(canny.size(), CV_8UC1);

    vector<Vec4i> lines;
    HoughLinesP(canny, lines, 1, CV_PI/180, 50, 40, 40);
    for (size_t i = 0; i < lines.size(); i++) {
        Vec4i l = lines[i];
        line(hough, Point(l[0], l[1]), Point(l[2], l[3]), white, 1, CV_AA);
        line(canny, Point(l[0], l[1]), Point(l[2], l[3]), white, 1, CV_AA);
    }
    
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(canny, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    // Draw reticle overlay
    Point srcCenter(src.cols / 2.0, src.rows / 2.0);
    line(src, Point(srcCenter.x, 0), Point(srcCenter.x, src.rows), blue);
    line(src, Point(0, srcCenter.y), Point(src.cols, srcCenter.y), blue);
    rectangle(src, Point(srcCenter.x - maxOffset, srcCenter.y - maxOffset),
        Point(srcCenter.x + maxOffset, srcCenter.y + maxOffset), blue);

    for (size_t i = 0; i < contours.size(); i++) {
        vector<Point> approx;
        approxPolyDP(Mat(contours[i]), approx,
                     arcLength(Mat(contours[i]), true) * 0.02, true);

        if (approx.size() == 4 &&
            fabs(contourArea(Mat(approx))) > 200 &&
            isContourConvex(Mat(approx))) {
            double maxCosine = 0;

            for (int j = 2; j < 5; j++) {
                Point pt0 = approx[j - 1];
                Point pt1 = approx[j % 4];
                Point pt2 = approx[j - 2];

                double dx1 = pt1.x - pt0.x;
                double dy1 = pt1.y - pt0.y;
                double dx2 = pt2.x - pt0.x;
                double dy2 = pt2.y - pt0.y;
                double cosine = fabs((dx1 * dx2 + dy1 * dy2) / sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10));

                maxCosine = MAX(maxCosine, cosine);
            }

            if (maxCosine < 0.3) {
                double totalX = 0.0;
                double totalY = 0.0;
                for (int j = 0; j < approx.size(); j++) {
                    circle(src, approx[j], 2, cyan, 2);

                    totalX += approx[j].x;
                    totalY += approx[j].y;
                }

                Point center(totalX / approx.size(), totalY / approx.size());

                Scalar color = red;
                if (center.x > srcCenter.x - maxOffset && center.x < srcCenter.x + maxOffset &&
                    center.y > srcCenter.y - maxOffset && center.y < srcCenter.y + maxOffset) {
                    color = green;
                }

                circle(src, center, 30, color, 2);
                line(src, Point(center.x - 10, center.y), Point(center.x + 10, center.y), color, 2);
                line(src, Point(center.x, center.y - 10), Point(center.x, center.y + 10), color, 2);

                break;
            }
        }
    }

    imshow(windowTitle, src);
    imshow(subWindowTitle, canny);
}

void position_HoughContour_Images() {
    Mat src[] = {
        imread("D:/Coding/FalconSamples/position_adjustment.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_brighter.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_darker.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_hflip.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_vflip.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_offset.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_rotate.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_rotate_brighter.png"),
        imread("D:/Coding/FalconSamples/position_adjustment_rotate_darker.png"),
    };

    position_HoughContour(src[0]);
    for (size_t i = 1; i < 9; ++i) {
        waitKey();
        position_HoughContour(src[i]);
    }
}

void position_HoughContour_Video() {
    bool toExit = false;
    while (!toExit) {
        VideoCapture video("D:/Coding/FalconSamples/position_demo_720.mp4");
        if (video.isOpened()) {
            while (true) {
                Mat frame;
                if (!video.read(frame)) {
                    break;
                }
                
                position_HoughContour(frame);

                if (waitKey(30) >= 0) {
                    toExit = true;
                    break;
                }
            }
        }
    }
}

void focus_PureLaplacian() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Mat laplacian(src[i].rows, src[i].cols, CV_16SC1);
        Laplacian(srcGray, laplacian, CV_16S);
        
        short maxLaplacian = SHRT_MIN;
        int imageSize = src[i].rows * src[i].cols;
        short *pixel = (short *)laplacian.data;
        for (int j = 0; j < imageSize; ++j, ++pixel) {
            if (*pixel > maxLaplacian) {
                maxLaplacian = *pixel;
            }
        }

        printf("Focus #%d = %d\n", i, maxLaplacian);
    }

    imshow(windowTitle, src[0]);
}

void focus_TENG() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Mat sobelX(src[i].rows, src[i].cols, CV_64FC1);
        Mat sobelY(src[i].rows, src[i].cols, CV_64FC1);
        Mat abs_sobelX(src[i].rows, src[i].cols, CV_64FC1);
        Mat abs_sobelY(src[i].rows, src[i].cols, CV_64FC1);
        
        Sobel(srcGray, sobelX, CV_64F, 1, 0, 3, BORDER_REPLICATE);
        convertScaleAbs(sobelX, abs_sobelX);
        
        Sobel(srcGray, sobelY, CV_64F, 0, 1, 3, BORDER_REPLICATE);
        convertScaleAbs(sobelY, abs_sobelX);
        
        pow(abs_sobelX, 2.0f, abs_sobelX);
        pow(abs_sobelY, 2.0f, abs_sobelY);
        
        Mat g(src[i].rows, src[i].cols, CV_64FC1);
        add(abs_sobelX, abs_sobelY, g, noArray(), CV_64F);
        
        Scalar focusVal = mean(g);

        printf("Focus #%d = %lf\n", i, focusVal.val[0]);
    }

    imshow(windowTitle, src[0]);
}

void focus_HISR() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        double minVal, maxVal;
        minMaxLoc(src[i], &minVal, &maxVal);
        double focusVal = maxVal - minVal;

        printf("Focus #%d = %lf\n", i, focusVal);
    }

    imshow(windowTitle, src[0]);
}

void focus_LAPV() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Mat laplacian(src[i].rows, src[i].cols, CV_64FC1);
        Laplacian(srcGray, laplacian, CV_64F, BORDER_REPLICATE);
        
        Scalar mean;
        Scalar stddev;
        meanStdDev(laplacian, mean, stddev);
        pow(stddev, 2.0f, stddev);

        printf("Focus #%d = %lf\n", i, stddev.val[0]);
    }

    imshow(windowTitle, src[0]);
}

void focus_LAPM() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        int a[] = {-1, 2, -1};
        Mat M = Mat(1, 3, CV_64FC1, a);
        Mat Inverse = Mat(3, 1, CV_64FC1);
        invert(M, Inverse, DECOMP_SVD);
        
        Mat Lx;
        Mat Ly;
        Mat abs_Lx;
        Mat abs_Ly;
        Mat FM;
        filter2D(srcGray, Lx, CV_64F, M);
        filter2D(srcGray, Ly, CV_64F, Inverse);
        
        convertScaleAbs(Lx, abs_Lx);
        convertScaleAbs(Ly, abs_Ly);
        
        add(abs_Lx, abs_Ly, FM);
        
        Scalar focusVal = mean(FM);

        printf("Focus #%d = %lf\n", i, focusVal.val[0]);
    }

    imshow(windowTitle, src[0]);
}

void focus_LAPE() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Mat laplacian(src[i].rows, src[i].cols, CV_64FC1);
        Laplacian(srcGray, laplacian, CV_64F, BORDER_REPLICATE);
        pow(laplacian, 2.0f, laplacian);
        Scalar focusVal = mean(laplacian);

        printf("Focus #%d = %lf\n", i, focusVal.val[0]);
    }

    imshow(windowTitle, src[0]);
}

void focus_GLVA() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Scalar mean;
        Scalar stddev;
        meanStdDev(srcGray, mean, stddev);

        printf("Focus #%d = %lf\n", i, stddev.val[0]);
    }

    imshow(windowTitle, src[0]);
}

void focus_GLVN() {
    Mat src[4] = {
        imread("D:/Coding/FalconSamples/focus_adjustment.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b1.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b2.png"),
        imread("D:/Coding/FalconSamples/focus_adjustment_b5.png")
    };

    for (int i = 0; i < 4; ++i) {
        Mat srcGray;
        cvtColor(src[i], srcGray, CV_BGR2GRAY);

        Scalar mean;
        Scalar stddev;
        meanStdDev(srcGray, mean, stddev);
        
        pow(stddev, 2.0f, stddev);
        double focusVal = stddev.val[0] / mean.val[0];

        printf("Focus #%d = %lf\n", i, focusVal);
    }

    imshow(windowTitle, src[0]);
}

void layerThickness() {
    Mat srcList[] = {
        imread("D:/Coding/FalconSamples/PCB_001.jpg"),
        imread("D:/Coding/FalconSamples/PCB_002.jpg"),
        imread("D:/Coding/FalconSamples/PCB_003.jpg"),
        imread("D:/Coding/FalconSamples/PCB_004.jpg"),
        imread("D:/Coding/FalconSamples/PCB_005.jpg"),
        imread("D:/Coding/FalconSamples/PCB_006.jpg"),
        imread("D:/Coding/FalconSamples/PCB_007.jpg")
    };

    for (int i = 0; i < 7; ++i) {
        Mat src(srcList[i]);
        
        Mat srcClosed;
        {
            //Mat srcTemp;
            dilate(src, srcClosed, Mat());
            //erode(srcTemp, srcClosed, Mat());
        }

        Mat srcGray;
        cvtColor(srcClosed, srcGray, CV_BGR2GRAY);

        Mat srcMedian;
        medianBlur(srcGray, srcMedian, 5);
        /*
        Mat srcHist;
        equalizeHist(srcMedian, srcHist);
        */
        Mat srcThreshold;
        threshold(srcMedian, srcThreshold, 225, 255, CV_THRESH_BINARY);

        // http://stackoverflow.com/a/16047590/1241690
        Mat srcTemp;
        double thresholdBase = threshold(srcGray, srcTemp, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);

        Mat canny;
        Canny(srcThreshold, canny, thresholdBase * 0.33, thresholdBase * 0.66);

        imshow(subWindowTitle, srcThreshold);

        vector<Vec4i> lines;
        HoughLinesP(canny, lines, 1, CV_PI/180, 50, 40, 40);
        for (size_t i = 0; i < lines.size(); i++) {
            Vec4i l = lines[i];
            line(src, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, CV_AA);
        }

        /*
        vector<Vec2f> lines;
        HoughLines(canny, lines, 5, CV_PI/180, 30);

        for (size_t i = 0; i < lines.size(); i++) {
            float rho = lines[i][0], theta = lines[i][1];
            Point pt1, pt2;
            double a = cos(theta), b = sin(theta);
            double x0 = a * rho, y0 = b * rho;
            pt1.x = cvRound(x0 - src.rows * b);
            pt1.y = cvRound(y0 + src.rows * a);
            pt2.x = cvRound(x0 + src.rows * b);
            pt2.y = cvRound(y0 - src.rows * a);

            line(src, pt1, pt2, Scalar(0, 0, 255), 2, CV_AA);
        }
        */

        /*
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        findContours(canny, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

        RNG rng(12345);
        for (size_t i = 0; i < contours.size(); i++) {
            Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
            drawContours(src, contours, i, color, 2, 8, hierarchy, 0, Point());

            //Rect boundRect = boundingRect(Mat(contours[i]));
            //rectangle(src, boundRect.tl(), boundRect.br(), color, 2, 8, 0);
        }
        */
        imshow(windowTitle, src);

        waitKey();
    }
}

int _tmain(int argc, _TCHAR* argv[]) {
    namedWindow(subWindowTitle, CV_WINDOW_AUTOSIZE);
    namedWindow(windowTitle, CV_WINDOW_AUTOSIZE);

    int64 tick = getTickCount();

    layerThickness();
    //position_HoughContour_Video();
    //position_HoughContour_Images();
    //focus_PureLaplacian();
    //focus_TENG();
    //focus_HISR();
    //focus_LAPV();
    //focus_LAPM();
    //focus_LAPE();
    //focus_GLVA();
    //focus_GLVN();

    printf("Exec time = %lf ms\n", (getTickCount() - tick) * 1000.0 / getTickFrequency());

    waitKey();
    destroyAllWindows();

	return 0;
}
