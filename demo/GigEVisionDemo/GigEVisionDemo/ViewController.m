//
//  ViewController.m
//  GigEVisionDemo
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "ViewController.h"
#import "CZGVCPDiscoveryAck.h"

@interface ViewController ()

@property (nonatomic, retain) CZGEVClient *client;

- (void)appendLogText:(NSString *)logText;

@end

@implementation ViewController

- (void)dealloc {
    [_discoveryButton release];
    [_logView release];
    [_client release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _client = [[CZGEVClient alloc] init];
    [_client setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)discoveryButtonTouched:(id)sender {
    [_client beginDiscovery];
    [self appendLogText:@"Discovery broadcast has been sent via GVCP."];
}

- (void)appendLogText:(NSString *)logText {
    dispatch_async(dispatch_get_main_queue(), ^(){
        [_logView setText:[NSString stringWithFormat:@"%@%@\n",
                           [_logView text],
                           logText]];
        [_logView scrollRangeToVisible:NSMakeRange([[_logView text] length], 1)];
    });
}

- (void)gevClient:(CZGEVClient *)client didGetDiscoveryAck:(CZGVCPDiscoveryAck *)acknowlege {
    NSString *msg = [[NSString alloc] initWithFormat:@"Received response from a camera! (model = %@, manufacturer = %@, IP = %@, MAC = %@, SN = %@)",
                     [acknowlege modelName],
                     [acknowlege manufacturerName],
                     [acknowlege currentIpAddress],
                     [acknowlege deviceMacAddress],
                     [acknowlege serialNumber]];
    [self appendLogText:msg];
    [msg release];
}

@end
