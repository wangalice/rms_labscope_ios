//
//  ViewController.h
//  GigEVisionDemo
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CZGEVClient.h"

@interface ViewController : UIViewController<CZGEVClientDelegate>

@property (retain, nonatomic) IBOutlet UIButton *discoveryButton;
@property (retain, nonatomic) IBOutlet UITextView *logView;

- (IBAction)discoveryButtonTouched:(id)sender;

@end
