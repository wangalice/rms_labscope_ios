//
//  CZViewController.m
//  ImageProcessingDemo
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZViewController.h"
#import "CZDocManager.h"
#import "CZImageTool.h"

#define kSliderStepDefault 1.0f
#define kSliderStepSharpening 0.1f

@interface CZViewController ()

@property (nonatomic, retain) CZDocManager *docManager;
@property (nonatomic, retain) CZImageTool *tool;
@property (nonatomic, retain) NSArray *sliders;
@property (nonatomic, retain) NSArray *buttons;

@end

@implementation CZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"testimage1.jpg"];
    _docManager = [[CZDocManager alloc] initWithImage:image];
    [_docManager addObserver:self forKeyPath:@"imageModified" options:0 context:NULL];
    
    _tool = [[CZImageTool alloc] initWithDocManager:_docManager];
    
    [[_tool imageView] setFrame:CGRectMake(0, 0,
                                           _placeholder.frame.size.width,
                                           _placeholder.frame.size.height)];
    [_placeholder addSubview:[_tool imageView]];
    
    _sliders = [[NSArray alloc] initWithObjects:_brightnessSlider, _contrastSlider, _saturationSlider, _sharpeningSlider, nil];
    _buttons = [[NSArray alloc] initWithObjects:_flipHButton, _flipVButton, _rotateCWButton, _rotateCCWButton, nil];
    
    [self modeChanged:nil];
}

- (void)dealloc {
    [_sliders release];
    [_buttons release];
    [_brightnessSlider release];
    [_contrastSlider release];
    [_saturationSlider release];
    [_sharpeningSlider release];
    [_flipHButton release];
    [_flipVButton release];
    [_rotateCWButton release];
    [_rotateCCWButton release];
    [_placeholder release];
    [_tool release];
    [_docManager release];
    [_modeSwitch release];
    [super dealloc];
}

- (void)viewDidUnload {
    [_docManager removeObserver:self forKeyPath:@"imageModified"];
    [self setSliders:nil];
    [self setButtons:nil];
    [self setBrightnessSlider:nil];
    [self setContrastSlider:nil];
    [self setSaturationSlider:nil];
    [self setSharpeningSlider:nil];
    [self setFlipHButton:nil];
    [self setFlipVButton:nil];
    [self setRotateCWButton:nil];
    [self setRotateCCWButton:nil];
    [self setPlaceholder:nil];
    [self setModeSwitch:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)modeChanged:(id)sender {
    UISwitch *modeSwitch = (UISwitch *)sender;
    BOOL isEditMode = [modeSwitch isOn];
    
    for (UISlider *slider in _sliders) {
        [slider setEnabled:isEditMode];
    }
    
    CGFloat buttonAlpha = isEditMode ? 1.0 : 0.5;
    for (UIButton *button in _buttons) {
        [button setEnabled:isEditMode];
        [button setAlpha:buttonAlpha];
    }
    
    if (sender && !isEditMode) {
        [_tool apply];
    }
}

- (IBAction)curveChanged:(id)sender {
    UISegmentedControl *control = (UISegmentedControl *)sender;
    NSUInteger index = [control selectedSegmentIndex];
    
    CZDisplayCurvePreset preset = CZDisplayCurveLinear;
    switch (index) {
        case 0:
            preset = CZDisplayCurveOptimized;
            break;
            
        case 2:
            preset = CZDisplayCurveDetailsInDark;
            break;
            
        case 3:
            preset = CZDisplayCurveDetailsInBright;
            break;
            
        default:
            break;
    }
    
    [_tool applyDisplayCurvePreset:preset];
}

- (IBAction)undoTouched:(id)sender {
    [[_docManager undoManager] undo];
}

- (IBAction)redoTouched:(id)sender {
    [[_docManager undoManager] redo];
}

- (IBAction)brightnessChanged:(id)sender {
    CGFloat rounded = roundf(_brightnessSlider.value / kSliderStepDefault) * kSliderStepDefault;
    [_tool setBrightness:rounded];
}

- (IBAction)contrastChanged:(id)sender {
    CGFloat rounded = roundf(_contrastSlider.value / kSliderStepDefault) * kSliderStepDefault;
    [_tool setContrast:rounded];
}

- (IBAction)saturationChanged:(id)sender {
    CGFloat rounded = roundf(_saturationSlider.value / kSliderStepDefault) * kSliderStepDefault;
    [_tool setColorIntensity:rounded];
}

- (IBAction)sharpeningChanged:(id)sender {
    CGFloat rounded = roundf(_sharpeningSlider.value / kSliderStepSharpening) * kSliderStepSharpening;
    [_tool setSharpness:rounded];
}

- (IBAction)flipHTouched:(id)sender {
    [_tool flipHorizontally];
}

- (IBAction)flipVTouched:(id)sender {
    [_tool flipVertically];
}

- (IBAction)rotateCWTouched:(id)sender {
    [_tool rotateClockwise];
}

- (IBAction)rotateCCWTouched:(id)sender {
    [_tool rotateCounterClockwise];
}

#pragma mark -
#pragma mark Key-Value Observing

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == _docManager) {
        if ([keyPath isEqualToString:@"imageModified"]) {
            [_tool invalidate];
        }
    }
}

@end
