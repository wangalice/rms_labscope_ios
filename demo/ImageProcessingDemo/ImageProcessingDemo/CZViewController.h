//
//  CZViewController.h
//  ImageProcessingDemo
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZViewController : UIViewController

@property (retain, nonatomic) IBOutlet UISwitch *modeSwitch;
@property (retain, nonatomic) IBOutlet UISlider *brightnessSlider;
@property (retain, nonatomic) IBOutlet UISlider *contrastSlider;
@property (retain, nonatomic) IBOutlet UISlider *saturationSlider;
@property (retain, nonatomic) IBOutlet UISlider *sharpeningSlider;
@property (retain, nonatomic) IBOutlet UIButton *flipHButton;
@property (retain, nonatomic) IBOutlet UIButton *flipVButton;
@property (retain, nonatomic) IBOutlet UIButton *rotateCWButton;
@property (retain, nonatomic) IBOutlet UIButton *rotateCCWButton;
@property (retain, nonatomic) IBOutlet UIView *placeholder;

- (IBAction)modeChanged:(id)sender;
- (IBAction)curveChanged:(id)sender;

- (IBAction)undoTouched:(id)sender;
- (IBAction)redoTouched:(id)sender;

- (IBAction)brightnessChanged:(id)sender;
- (IBAction)contrastChanged:(id)sender;
- (IBAction)saturationChanged:(id)sender;
- (IBAction)sharpeningChanged:(id)sender;

- (IBAction)flipHTouched:(id)sender;
- (IBAction)flipVTouched:(id)sender;
- (IBAction)rotateCWTouched:(id)sender;
- (IBAction)rotateCCWTouched:(id)sender;


@end
