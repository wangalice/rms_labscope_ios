#include "../../src/Hermes/CZLaplacianBlendingCore.cpp"

using namespace cv;

cv::Mat myRead(const string& filename) {
    Mat source = imread(filename);
    if (source.channels() == 3) {
        cvtColor(source, source, CV_BGR2BGRA);
        return source;
    } else {
        return source;
    }
}

int main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: FocusStacking file1 file2 ...");
        return 0;
    }

    vector< Mat_<Vec4b> > image8s;
    for (int i = 1; i < argc; i++) {
        image8s.push_back(myRead(argv[i]));
    }
    
    if (image8s.size() < 2) {
        return -1;
    }
    
    Mat_<Vec4b> blend = image8s[0];
    for (int i = 1; i < image8s.size(); i++) {
        LaplacianBlendingCore lb(8);
        lb.addImage(blend, image8s[i]);
        blend = lb.blend();
    }
    
    imshow("blended",blend);
    imwrite("blended.jpg", blend);

    waitKey(0);
    return 0;
}