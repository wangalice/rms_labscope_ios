//
//  CZFocusStackingFilter.m
//  FocusStackingDemo
//
//  Created by Ralph Jin on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZFocusStackingFilter.h"

NSString *const kCZFocusStackingVertexShaderString = SHADER_STRING
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 attribute vec4 inputTextureCoordinate2;
 
 uniform float texelWidth;
 uniform float texelHeight;
 
 varying vec2 textureCoordinate;
 varying vec2 leftTextureCoordinate;
 varying vec2 rightTextureCoordinate;
 varying vec2 topTextureCoordinate;
 varying vec2 bottomTextureCoordinate;
 
 varying vec2 textureCoordinate2;
 varying vec2 leftTextureCoordinate2;
 varying vec2 rightTextureCoordinate2;
 varying vec2 topTextureCoordinate2;
 varying vec2 bottomTextureCoordinate2;
 
 void main() {
     gl_Position = position;
     
     vec2 widthStep = vec2(texelWidth, 0.0);
     vec2 heightStep = vec2(0.0, texelHeight);
     
     textureCoordinate = inputTextureCoordinate.xy;
     leftTextureCoordinate = inputTextureCoordinate.xy - widthStep;
     rightTextureCoordinate = inputTextureCoordinate.xy + widthStep;
     topTextureCoordinate = inputTextureCoordinate.xy - heightStep;
     bottomTextureCoordinate = inputTextureCoordinate.xy + heightStep;
     
     textureCoordinate2 = inputTextureCoordinate2.xy;
     leftTextureCoordinate2 = inputTextureCoordinate2.xy - widthStep;
     rightTextureCoordinate2 = inputTextureCoordinate2.xy + widthStep;
     topTextureCoordinate2 = inputTextureCoordinate2.xy - heightStep;
     bottomTextureCoordinate2 = inputTextureCoordinate2.xy + heightStep;
 }
 );

NSString *const kCZFocusStackingFragmentShaderString = SHADER_STRING
(
 precision highp float;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 varying vec2 textureCoordinate;
 varying vec2 leftTextureCoordinate;
 varying vec2 rightTextureCoordinate;
 varying vec2 topTextureCoordinate;
 varying vec2 bottomTextureCoordinate;
 
 varying vec2 textureCoordinate2;
 varying vec2 leftTextureCoordinate2;
 varying vec2 rightTextureCoordinate2;
 varying vec2 topTextureCoordinate2;
 varying vec2 bottomTextureCoordinate2;
 
 const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);
 
 void main() {
     mediump vec3 bottomColor = texture2D(inputImageTexture, bottomTextureCoordinate).rgb;
     mediump vec4 centerColor = texture2D(inputImageTexture, textureCoordinate);
     mediump vec3 leftColor = texture2D(inputImageTexture, leftTextureCoordinate).rgb;
     mediump vec3 rightColor = texture2D(inputImageTexture, rightTextureCoordinate).rgb;
     mediump vec3 topColor = texture2D(inputImageTexture, topTextureCoordinate).rgb;
     
     float contrast = dot(bottomColor, W) * 0.25 +
                      dot(leftColor, W) * 0.25 +
                      dot(rightColor, W) * 0.25 +
                      dot(topColor, W) * 0.25
                      - dot(centerColor.rgb, W);
     contrast = abs(contrast);
     
     mediump vec3 bottomColor2 = texture2D(inputImageTexture2, bottomTextureCoordinate2).rgb;
     mediump vec4 centerColor2 = texture2D(inputImageTexture2, textureCoordinate2);
     mediump vec3 leftColor2 = texture2D(inputImageTexture2, leftTextureCoordinate2).rgb;
     mediump vec3 rightColor2 = texture2D(inputImageTexture2, rightTextureCoordinate2).rgb;
     mediump vec3 topColor2 = texture2D(inputImageTexture2, topTextureCoordinate2).rgb;
     
     float contrast2 = dot(bottomColor2, W) * 0.25  + dot(leftColor2, W) * 0.25  + dot(rightColor2, W) * 0.25  + dot(topColor2, W) * 0.25  - dot(centerColor2.rgb, W);
     contrast2 = abs(contrast2);
     
     if (contrast > contrast2) {
         gl_FragColor = vec4(centerColor.rgb, contrast);
     } else {
         gl_FragColor = vec4(centerColor2.rgb, contrast2);
     }
 }
 );

NSString *const kCZFocusStackingFragmentShaderString2 = SHADER_STRING
(
 precision highp float;
 
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 varying vec2 textureCoordinate;
 varying vec2 leftTextureCoordinate;
 varying vec2 rightTextureCoordinate;
 varying vec2 topTextureCoordinate;
 varying vec2 bottomTextureCoordinate;
 
 varying vec2 textureCoordinate2;
 varying vec2 leftTextureCoordinate2;
 varying vec2 rightTextureCoordinate2;
 varying vec2 topTextureCoordinate2;
 varying vec2 bottomTextureCoordinate2;
 
 const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);
 
 void main() {
     mediump vec3 bottomColor = texture2D(inputImageTexture, bottomTextureCoordinate).rgb;
     mediump vec4 centerColor = texture2D(inputImageTexture, textureCoordinate);
     mediump vec3 leftColor = texture2D(inputImageTexture, leftTextureCoordinate).rgb;
     mediump vec3 rightColor = texture2D(inputImageTexture, rightTextureCoordinate).rgb;
     mediump vec3 topColor = texture2D(inputImageTexture, topTextureCoordinate).rgb;
     
     float contrast = dot(bottomColor, W) * 0.25 +
                        dot(leftColor, W) * 0.25 +
                        dot(rightColor, W) * 0.25 +
                        dot(topColor, W) * 0.25 -
                        dot(centerColor.rgb, W);
     contrast = abs(contrast);
     
     mediump vec3 bottomColor2 = texture2D(inputImageTexture2, bottomTextureCoordinate2).rgb;
     mediump vec4 centerColor2 = texture2D(inputImageTexture2, textureCoordinate2);
     mediump vec3 leftColor2 = texture2D(inputImageTexture2, leftTextureCoordinate2).rgb;
     mediump vec3 rightColor2 = texture2D(inputImageTexture2, rightTextureCoordinate2).rgb;
     mediump vec3 topColor2 = texture2D(inputImageTexture2, topTextureCoordinate2).rgb;
     
     float contrast2 = dot(bottomColor2, W) * 0.25 +
                        dot(leftColor2, W) * 0.25 +
                        dot(rightColor2, W) * 0.25 +
                        dot(topColor2, W) * 0.25 -
                        dot(centerColor2.rgb, W);
     contrast2 = abs(contrast2);
     
     float sum = contrast + contrast2;
     
     mediump vec3 resultColor = centerColor.rgb * (contrast / sum) + centerColor2.rgb * (contrast2 / sum);
     
     gl_FragColor = vec4(resultColor, sum);
 }
 );

@implementation CZFocusStackingFilter

@synthesize texelWidth = _texelWidth;
@synthesize texelHeight = _texelHeight;

#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    self = [super initWithVertexShaderFromString:kCZFocusStackingVertexShaderString fragmentShaderFromString:kCZFocusStackingFragmentShaderString2];
    if (!self) {
        return nil;
    }
    
    texelWidthUniform = [filterProgram uniformIndex:@"texelWidth"];
    texelHeightUniform = [filterProgram uniformIndex:@"texelHeight"];
    
    return self;
}

- (void)setupFilterForSize:(CGSize)filterFrameSize;
{
    if (!hasOverriddenImageSizeFactor)
    {
        _texelWidth = 1.0 / filterFrameSize.width;
        _texelHeight = 1.0 / filterFrameSize.height;
        
        runSynchronouslyOnVideoProcessingQueue(^{
            [GPUImageContext setActiveShaderProgram:filterProgram];
            if (GPUImageRotationSwapsWidthAndHeight(inputRotation))
            {
                glUniform1f(texelWidthUniform, _texelHeight);
                glUniform1f(texelHeightUniform, _texelWidth);
            }
            else
            {
                glUniform1f(texelWidthUniform, _texelWidth);
                glUniform1f(texelHeightUniform, _texelHeight);
            }
        });
    }
}

#pragma mark -
#pragma mark Accessors

- (void)setTexelWidth:(CGFloat)newValue;
{
    hasOverriddenImageSizeFactor = YES;
    _texelWidth = newValue;
    
    [self setFloat:_texelWidth forUniform:texelWidthUniform program:filterProgram];
}

- (void)setTexelHeight:(CGFloat)newValue;
{
    hasOverriddenImageSizeFactor = YES;
    _texelHeight = newValue;
    
    [self setFloat:_texelHeight forUniform:texelHeightUniform program:filterProgram];
}

@end