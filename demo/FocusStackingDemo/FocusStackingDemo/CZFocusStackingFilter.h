//
//  CZFocusStackingFilter.h
//  FocusStackingDemo
//
//  Created by Ralph Jin on 9/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <GPUImage.h>

@interface CZFocusStackingFilter : GPUImageTwoInputFilter {
  @private
    GLint texelWidthUniform, texelHeightUniform;
    
    CGFloat texelWidth, texelHeight;
    BOOL hasOverriddenImageSizeFactor;
}

@property(readwrite, nonatomic) CGFloat texelWidth;
@property(readwrite, nonatomic) CGFloat texelHeight;

@end
