//
//  CZViewController.m
//  ImageProcessingDemo
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZViewController.h"
#import "CZLaplacianBlending.h"

@interface CZViewController () <UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverControllerDelegate> {
    NSUInteger _currentSample;
    NSUInteger _currentImageIndex;

    NSMutableDictionary *_imageGroup;  // {NSString: NSArray( array of file path)}
}
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;

@property (retain, nonatomic) IBOutlet UIScrollView *placeholder;

- (IBAction)goTouched:(id)sender;
- (IBAction)chooseButtonAction:(id)sender;

@property (nonatomic, retain) UIPopoverController *popover;

@property (nonatomic, copy) NSString *currentGroup;
@property (nonatomic, copy) NSString *selectedGroup;
@property (nonatomic, retain) NSTimer *imageViewTimer;

@end


@implementation CZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _placeholder.delegate = self;
    _placeholder.contentSize = CGSizeMake(2048, 4096);
    _currentSample = -1;

    self.imageViewTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                             target:self
                                                           selector:@selector(imageViewTimerHandler:)
                                                           userInfo:NULL
                                                            repeats:YES];
}

- (void)dealloc {
    [_imageViewTimer invalidate];
    [_imageViewTimer release];

    [_currentGroup release];
    [_selectedGroup release];
    [_popover release];
    [_placeholder release];
    [_nameLabel release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.
    [self clearPlaceHolder];
}

- (void)imageViewTimerHandler:(NSTimer *)timer {
    UIImageView *imageView = nil;
    for (UIView *view in _placeholder.subviews) {
        if (view.tag == 2015 ) {
            imageView = (UIImageView *)view;
            break;
        }
    }

    if (imageView == nil) {
        return;
    }

    NSArray *imageArray = _imageGroup[self.currentGroup];
    if (imageArray.count == 0) {
        return;
    }

    _currentImageIndex ++;
    if (_currentImageIndex >= imageArray.count) {
        _currentImageIndex = 0;
    }

    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fileName = imageArray[_currentImageIndex];
    NSString *filePath = [docPath stringByAppendingPathComponent:fileName];
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    imageView.image = image;
}

- (IBAction)goTouched:(id)sender {
    [self showCurrentGroupImage];
}

- (IBAction)chooseButtonAction:(id)sender {
    [self scanImageGroup];

    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 280, 360)];
    UIViewController *vc = [[UIViewController alloc] init];
    [vc.view addSubview:pickerView];
    vc.preferredContentSize = CGSizeMake(280, 360);
    pickerView.dataSource = self;
    pickerView.delegate = self;
    [pickerView release];

    NSArray *groups = _imageGroup.allKeys;
    NSUInteger index = [groups indexOfObject:self.currentGroup];
    if (index != NSNotFound) {
        [pickerView selectRow:index inComponent:0 animated:NO];
        self.selectedGroup = self.currentGroup;
    } else {
        if (groups.count) {
            self.selectedGroup = groups[0];
        } else {
            self.selectedGroup = nil;
        }
        self.nameLabel.text = self.selectedGroup;
    }

    UIPopoverController *popoverChooser = [[UIPopoverController alloc] initWithContentViewController:vc];
    popoverChooser.delegate = self;
    self.popover = popoverChooser;
    [vc release];

    UIButton *chooseButton = sender;
    [popoverChooser presentPopoverFromRect:chooseButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    popoverChooser.delegate = self;
    [popoverChooser release];
}

#pragma mark - scroll view delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    NSArray *subviews = _placeholder.subviews;
    if (subviews.count > 0) {
        return [subviews lastObject];
    } else {
        return nil;
    }
}

#pragma mark - popover delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if (self.popover != popoverController) {
        return;
    }
    
    if (self.currentGroup == nil ||
        ![self.currentGroup isEqualToString:self.selectedGroup]) {

        self.currentGroup = self.selectedGroup;
        [self showCurrentGroupImage];
    }

    self.popover = nil;
}

#pragma mark - picker view

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _imageGroup.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _imageGroup.allKeys[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (_imageGroup.count > row) {
        self.selectedGroup = _imageGroup.allKeys[row];
        self.nameLabel.text = self.selectedGroup;
    }
}

#pragma mark - Private methods

- (void)clearPlaceHolder {
    NSArray *subViews = [_placeholder.subviews copy];
    for (UIView *subView in subViews) {
        [subView removeFromSuperview];
    }
    [subViews release];
}

- (void)showCurrentGroupImage {
    [self clearPlaceHolder];

    if (self.currentGroup == nil) {
        return;
    }

    NSArray *fileNames = _imageGroup[self.currentGroup];
    if (fileNames.count == 0) {
        return;
    }

    // start real task
    NSMutableArray *images = [NSMutableArray array];
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    for (NSString *fileName in fileNames) {
        NSString *filePath = [docPath stringByAppendingPathComponent:fileName];
        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
        if (image) {
            [images addObject:image];
        }
    }

    [self showImages:fileNames];
}

- (void)showImages:(NSArray *)fileNames {
    if (fileNames.count < 2) {
        return;
    }

    // show busy indicator
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [indicator setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2)];
    indicator.backgroundColor = [UIColor grayColor];
    indicator.alpha = 0.5;
    indicator.layer.cornerRadius = 6;
    indicator.layer.masksToBounds = YES;
    [self.view addSubview:indicator];
    [indicator startAnimating];
    self.view.userInteractionEnabled = NO;

    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    const CGFloat kPadding = 10;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *fileName = fileNames[0];
        NSString *filePath = [docPath stringByAppendingPathComponent:fileName];
        UIImage *firstImage = [UIImage imageWithContentsOfFile:filePath];

        const CGFloat kImageWidth = [firstImage size].width;
        const CGFloat kImageHeight = [firstImage size].height;

        UIImage *mergedImage = [firstImage retain];
        for (NSUInteger i = 1; i < fileNames.count; ++ i) {
            @autoreleasepool {
                fileName = fileNames[i];
                NSString *filePath = [docPath stringByAppendingPathComponent:fileName];
                UIImage *anotherImage = [UIImage imageWithContentsOfFile:filePath];

                CZLaplacianBlending *blending = [[CZLaplacianBlending alloc] init];
                [blending addImage:mergedImage anotherImage:anotherImage];
                [mergedImage release];
                mergedImage = nil;

                mergedImage = [[blending blend] retain];
                [blending release];
            }
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            if (mergedImage) {
                UIImageView *view1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kImageWidth, kImageHeight)];
                view1.image = mergedImage;
                [_placeholder addSubview:view1];
                [view1 release];
            }

            if (firstImage) {
                UIImageView *view1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, kImageHeight + kPadding, kImageWidth, kImageHeight)];
                view1.tag = 2015;
                view1.image = firstImage;
                [_placeholder addSubview:view1];
                [view1 release];
            }

            _placeholder.contentSize = CGSizeMake(kImageWidth, kImageHeight * 2 + kPadding);

            // stop busy indicator
            [indicator stopAnimating];
            [indicator removeFromSuperview];
            self.view.userInteractionEnabled = YES;

            _currentImageIndex = 0;
        });

        [mergedImage release];
    });
}

- (void)scanImageGroup {
    if (_imageGroup == nil) {
        _imageGroup = [[NSMutableDictionary alloc] init];
    } else {
        [_imageGroup removeAllObjects];
    }

    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];

    NSArray *subFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docPath error:nil];
    for (NSString *subFile in subFiles) {
        NSString *extension = [[subFile pathExtension] lowercaseString];
        if ([extension isEqualToString:@"jpg"] ||
            [extension isEqualToString:@"png"] ||
            [extension isEqualToString:@"tif"]) {
            NSString *fileName = [subFile stringByDeletingPathExtension];
            NSString *groupName = [fileName stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
            if (groupName.length && groupName.length != fileName.length) {
                groupName = [groupName stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"_-+"]];

                NSMutableArray *array = _imageGroup[groupName];
                if (array == nil) {
                    array = [NSMutableArray array];
                    _imageGroup[groupName] = array;
                }
                [array addObject:subFile];
            }
        }
    }
}

@end
