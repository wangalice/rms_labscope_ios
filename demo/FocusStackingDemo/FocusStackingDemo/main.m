//
//  main.m
//  ImageProcessingDemo
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CZAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CZAppDelegate class]));
    }
}
