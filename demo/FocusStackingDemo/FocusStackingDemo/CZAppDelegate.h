//
//  CZAppDelegate.h
//  ImageProcessingDemo
//
//  Created by Halley Gu on 13-1-31.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CZViewController;

@interface CZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CZViewController *viewController;

@end
