#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// global variables

Mat src, src_gray;
Mat dst, detected_edges;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;
const char* window_name = "Edge Map";
const char* contour_window_name = "Contour Window";
RNG rng(12345);


/**  @function Erosion  */
void Erosion( Mat& srcM, Mat& dstM )
{
    int erosion_type = MORPH_RECT;  // MORPH_CROSS, MORPH_ELLIPSE
    int erosion_size = 2;
    
    Mat element = getStructuringElement( erosion_type,
                                        Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                        Point( erosion_size, erosion_size ) );
    
    erode( srcM, dstM, element );
}

/** @function Dilation */
void Dilation( Mat& srcM, Mat& dstM )
{
    int dilation_type = MORPH_RECT;  // MORPH_CROSS, MORPH_ELLIPSE
    int dilation_size = 2;
    
    Mat element = getStructuringElement( dilation_type,
                                        Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                        Point( dilation_size, dilation_size ) );
    
    dilate( srcM, dstM, element );
}


/**
 * @function FindContours
 */
void FindContours(int, void*)
{
    cvtColor( src, src_gray, CV_BGR2GRAY );
    /// 使用 3x3内核降噪
    blur( src_gray, src_gray, Size(3,3) );
    
    // set value between (low, hight) to Max(255), and others to 0
    int low = 128 - lowThreshold;
    int high = 128 + lowThreshold;
    threshold(src_gray, src_gray, high, 255, CV_THRESH_TOZERO_INV);
    threshold(src_gray, src_gray, low, 255, CV_THRESH_BINARY);
    
    // reduce small areas
    Erosion(src_gray, src_gray);
    Dilation(src_gray, src_gray);
    
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    
    // find contours with hole
//    findContours(src_gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    
    // find contours without hole
    findContours(src_gray, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    
    /// draw contours
    src.copyTo(dst);
    for( int i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours( dst, contours, i, color, 2, 8, hierarchy, 0, Point() );
    }
    
    /// TODO: measure contours
    
    imshow( contour_window_name, src_gray );
    imshow( window_name, dst);
}


/** @function main */
int main( int argc, char** argv )
{
    /// read image
    src = imread( "../../FocusStackingDemo/FocusStackingDemo/Crystal2.jpg" );
    
    if( !src.data )
    { return -1; }
    
    dst.create( src.size(), src.type() );
    
    /// create windows
    namedWindow( window_name, CV_WINDOW_AUTOSIZE );
    namedWindow( contour_window_name, CV_WINDOW_AUTOSIZE );
    
    /// create trackbar
    createTrackbar( "Min Threshold:", window_name, &lowThreshold, max_lowThreshold, FindContours );
    
    /// run process
    FindContours(0, 0);
    
    waitKey(0);
    
    return 0;
}