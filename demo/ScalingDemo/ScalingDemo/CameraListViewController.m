//
//  CameraListViewController.m
//  ScalingDemo
//
//  Created by Halley Gu on 6/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CameraListViewController.h"
#import "CZBufferProcessor.h"

@interface CameraListViewController ()

@property (nonatomic, retain) NSTimer *timer;

- (void)handleTimer:(NSTimer *)timer;

@end

@implementation CameraListViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        _cameras = [[NSMutableArray alloc] init];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                      target:self
                                                    selector:@selector(handleTimer:)
                                                    userInfo:nil
                                                     repeats:YES];
        [self handleTimer:self.timer];
    }
    return self;
}

- (void)dealloc {
    [_cameras release];
    
    [_timer invalidate];
    [_timer release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

- (void)handleTimer:(NSTimer *)timer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        const uint16_t kWisionDiscoveryPort = 8995;
        const uint16_t kWisionListenPort = 8996;
        const NSTimeInterval kWisionDiscoveryTimeout = 5;
        const size_t kWisionPacketLength = 544;
        
        GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                  delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        [socket setIPv4Enabled:YES];
        [socket setIPv6Enabled:NO];
        
        do {
            NSError *error = nil;
            
            if (![socket enableBroadcast:YES error:&error]) {
                CZLog(@"Wision discovery: failed to enable broadcast for the socket.");
                break;
            }
            
            if (![socket beginReceiving:&error]) {
                CZLog(@"Wision discovery: failed to begin receiving data from socket.");
                break;
            }
            
            time_t now = time(NULL);
            struct tm *timeNow = localtime(&now);
            
            void *dataBuffer = malloc(kWisionPacketLength);
            memset(dataBuffer, 0, kWisionPacketLength);
            
            CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:dataBuffer
                                                                           length:kWisionPacketLength];
            [writer writeShort:0xAA55];                             // magic_number
            [writer writeShortInNetworkOrder:0];                    // type
            [writer writeShortInNetworkOrder:kWisionListenPort];    // port
            [writer writeShort:0];                                  // device_code
            [writer writeLongInNetworkOrder:0];                     // serial_num
            [writer writeLong:0];                                   // eventcd
            [writer writeLong:0];                                   // arg, 0 or 3600
            [writer writeLong:0];                                   // reserved, 0
            [writer writeShort:(1900 + timeNow->tm_year)];          // year, 0-2099
            [writer writeByte:(timeNow->tm_mon + 1)];               // mon, 1-12
            [writer writeByte:timeNow->tm_mday];                    // day, 1-31
            [writer writeByte:timeNow->tm_hour];                    // hr, 0-23
            [writer writeByte:timeNow->tm_min];                     // min, 0-59
            [writer writeByte:timeNow->tm_sec];                     // sec, 0-59
            [writer writeByte:0xFF];                                // tz
            [writer release];
            
            NSData *packetData = [[NSData alloc] initWithBytes:dataBuffer
                                                        length:kWisionPacketLength];
            
            [socket sendData:packetData
                      toHost:@"255.255.255.255" // Broadcast
                        port:kWisionDiscoveryPort
                 withTimeout:kWisionDiscoveryTimeout
                         tag:0];
            
            [packetData release];
            free(dataBuffer);
        } while (0);
        
        // Close and release the created socket if error occurred.
        [socket release];
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @synchronized (self) {
        return [_cameras count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    @synchronized (self) {
        cell.textLabel.text = _cameras[indexPath.row];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_delegate respondsToSelector:@selector(cameraListViewController:didSelectRow:)]) {
        [_delegate cameraListViewController:self didSelectRow:indexPath.row];
    }
}

#pragma mark - GCDAsyncUdpSocketDelegate methods

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    [reader seekToOffset:32];
    
    @autoreleasepool {
        NSString *response = [reader readStringWithLength:(data.length - 32)];
        NSArray *fields = [response componentsSeparatedByString:@";"];
        
        if ([fields count] > 7) {
            @synchronized (self) {
                NSString *ipAddress = fields[3];
                NSUInteger index = [_cameras indexOfObject:ipAddress];
                if (index == NSNotFound) {
                    [_cameras addObject:ipAddress];
                    [self.tableView reloadData];
                }
            }
        }
    }
    
    [reader release];
}

@end
