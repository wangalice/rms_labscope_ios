//
//  UIImage+CVMat.h
//  ScalingDemo
//
//  Created by Halley Gu on 5/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <opencv2/imgproc/imgproc.hpp>

@interface UIImage (CVMat)

- (id)initWithMat:(const cv::Mat &)cvMat;

+ (UIImage *)imageWithMat:(const cv::Mat &)cvMat;

@property (nonatomic, readonly) cv::Mat mat;

@end
