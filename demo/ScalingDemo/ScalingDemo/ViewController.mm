//
//  ViewController.mm
//  ScalingDemo
//
//  Created by Halley Gu on 5/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "ViewController.h"
#include <opencv2/imgproc/imgproc.hpp>
#import <GPUImage/GPUImage.h>
#import "UIImage+CVMat.h"

using namespace cv;

const static CGFloat kNonCalibratedValue = -1.0f;

@interface ViewController ()

@property (nonatomic, retain) GPUImageView *imageView;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIPopoverController *fileListPopover;
@property (nonatomic, retain) UIPopoverController *cameraListPopover;
@property (nonatomic, retain) FileListViewController *fileListView;
@property (nonatomic, retain) CameraListViewController *cameraListView;
@property (nonatomic, retain) CZRTSPClient *rtspClient;
@property (nonatomic, retain) CZHardwareDecoder *decoder;

@end

@implementation ViewController

- (void)dealloc {
    [_imageView release];
    [_scalingTextField release];
    [_fileListPopover release];
    [_cameraListPopover release];
    [_fileListView release];
    [_cameraListView release];
    [_decoder release];
    [_rtspClient release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    _imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 666)];
    [_imageView setFillMode:kGPUImageFillModePreserveAspectRatio];
    [self.view addSubview:_imageView];
    
    _fileListView = [[FileListViewController alloc] initWithStyle:UITableViewStylePlain];
    [_fileListView setDelegate:self];
    
    _cameraListView = [[CameraListViewController alloc] initWithStyle:UITableViewStylePlain];
    [_cameraListView setDelegate:self];
    
    _fileListPopover = [[UIPopoverController alloc] initWithContentViewController:_fileListView];
    _cameraListPopover = [[UIPopoverController alloc] initWithContentViewController:_cameraListView];
}

- (void)setImage:(UIImage *)image {
    [_image release];
    _image = [image retain];
    
    GPUImagePicture *picture = [[GPUImagePicture alloc] initWithImage:image];
    [picture addTarget:_imageView];
    [picture processImage];
    [picture release];
}

- (IBAction)buttonReloadAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (!_fileListPopover.popoverVisible) {
        [_fileListPopover presentPopoverFromRect:button.frame
                                          inView:self.view
                        permittedArrowDirections:UIPopoverArrowDirectionAny
                                        animated:YES];
    } else {
        [_fileListPopover dismissPopoverAnimated:YES];
    }
}

- (IBAction)buttonLiveAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (!_cameraListPopover.popoverVisible) {
        [_cameraListPopover presentPopoverFromRect:button.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    } else {
        [_cameraListPopover dismissPopoverAnimated:YES];
    }
}

- (IBAction)buttonCallibrateAction:(id)sender {
    if (self.rtspClient) {
        [self.rtspClient setDelegate:nil];
        [self.rtspClient stop];
        
        [self.decoder setDelegate:nil];
        [self.decoder stop];
    }
    
    UIImage *image = self.image;
    if (image == nil) {
        return;
    }
    
    UIImage *output = nil;
    CGFloat fov = [self fieldOfViewFromImageByHoughLinesP:image resultImage:&output];
    CGFloat scaling = fov / 2048.0; // Only support Wision camera now.
    
    if (scaling > 0.0 && output != nil) {
        NSString *scalingText = [[NSString alloc] initWithFormat:@"%lf µm/pixel", scaling];
        [_scalingTextField setText:scalingText];
        [scalingText release];
        
        [self setImage:output];
    } else {
        [_scalingTextField setText:@"n/a"];
    }
}

- (CGFloat)fieldOfViewFromImageByHoughLinesP:(UIImage *)image resultImage:(UIImage **)result {
    // Definition of constants and algorithm parameters.
    
    const double CV_HALF_PI = CV_PI / 2.0;
    const double CV_QUARTER_PI = CV_PI / 4.0;
    
    const size_t kPreTestSampleCount = 100;
    const double kPreTestMinBlackRatio = 33.0;
    const double kPreTestMaxBlackRatio = 100.0 - kPreTestMinBlackRatio;
    const double kPreTestDeviationThreshold = 1.5;
    const double kEnlargingScale = 4.0;
    const int kEnlargingLengthThreshold = 20;
    const double kAngleTolerance = 2.0 * CV_PI / 180.0;
    const double kLeaningThresholdRatio = 0.0025;
    const int kHoughThreshold = 80;
    const double kDeviationThreshold = 0.06;
    const double kCellWidthInMicrometer = 25.0;
    
    if (image == nil) {
        CZLog(@"Scaling error: source image is nil.");
        return kNonCalibratedValue;
    }
    
    Mat matSource = [image mat];
    
    int width = matSource.cols;
    int height = matSource.rows;
    
    Mat matGray;
    cvtColor(matSource, matGray, CV_BGR2GRAY);
    matSource.release();
    
    // Step 1: Roughly estimate the length of pixel cycle. This will
    // calculate the length of continous white or black pixels, except the
    // first length and the last since they could be incomplete shape.
    
    int meanGray = ceil(mean(matGray).val[0]);
    
    vector<int> lengths;
    int lengthCounts[2] = {0, 0};
    int blackLengthSums[2] = {0, 0};
    int whiteLengthSums[2] = {0, 0};
    double averageLengths[2] = {0.0, 0.0};
    double blackRatioValues[2] = {0.0, 0.0};
    
    // Two phases, first horizontal, then vertical.
    for (size_t i = 0; i < 2; ++i) {
        for (size_t j = 0; j < kPreTestSampleCount; ++j) {
            Mat matSegment;
            if (i == 0) {
                int rowNumber = j * floor((double)height / kPreTestSampleCount);
                threshold(matGray.row(rowNumber), matSegment, meanGray, 255, CV_THRESH_BINARY);
            } else {
                int colNumber = j * floor((double)width / kPreTestSampleCount);
                threshold(matGray.col(colNumber), matSegment, meanGray, 255, CV_THRESH_BINARY);
            }
            
            int lastValue = -1;
            int currentLength = 0;
            bool isFirst = true;
            
            int segmentSize = (i == 0) ? matSegment.cols : matSegment.rows;
            for (int k = 0; k < segmentSize; ++k) {
                int value = (i == 0) ? matSegment.at<uchar>(0, k) : matSegment.at<uchar>(k, 0);
                if (value != lastValue) {
                    if (!isFirst) {
                        lengths.push_back(currentLength);
                        ++lengthCounts[i];
                        
                        if (lastValue == 0) {
                            blackLengthSums[i] += currentLength;
                        } else {
                            whiteLengthSums[i] += currentLength;
                        }
                    }
                    
                    isFirst = false;
                    lastValue = value;
                    currentLength = 1;
                } else {
                    ++currentLength;
                }
            }
        }
        
        double lengthSum = blackLengthSums[i] + whiteLengthSums[i];
        averageLengths[i] = lengthSum / lengthCounts[i];
        blackRatioValues[i] = 100.0 * blackLengthSums[i] / lengthSum;
        
        CZLog(@"Scaling Pre-test #%zu: estimated distance = %lf, black = %lf%%",
              i + 1, averageLengths[i], blackRatioValues[i]);
        
        if (blackRatioValues[i] < kPreTestMinBlackRatio ||
            blackRatioValues[i] > kPreTestMaxBlackRatio) {
            CZLog(@"Scaling error: pre-calculation shows invalid pattern for the image.");
            return kNonCalibratedValue;
        }
    }
    
    // Combine the results.
    
    double accurateAverageLength = (averageLengths[0] + averageLengths[1]) / 2.0;
    int averageLength = ceil(accurateAverageLength);
    
    CZLog(@"Scaling #1: overall estimated distance = %d, black = %lf%%",
          averageLength, (blackRatioValues[0] + blackRatioValues[1]) / 2.0);
    
    if (averageLength <= 0) {
        CZLog(@"Scaling error: average length is invalid.");
        return kNonCalibratedValue;
    }
    
    // Calculate the deviation of all length samples, if the deviation is too
    // high, abandon the results and fail the process.
    
    double deviation = 0.0;
    for (size_t i = 0; i < lengths.size(); ++i) {
        double diff = lengths[i] - accurateAverageLength;
        deviation += diff * diff;
    }
    deviation = sqrt(deviation / lengths.size()) / accurateAverageLength;
    
    if (deviation > kPreTestDeviationThreshold) {
        CZLog(@"Scaling error: pre-calculation shows invalid pattern for the image.");
        return kNonCalibratedValue;
    }
    
    // Step 1.5: Enlarge the image if the predicted average length is very small.
    
    BOOL isScaled = (averageLength <= kEnlargingLengthThreshold);
    Mat matScaled;
    if (isScaled) {
        int newWidth = width / kEnlargingScale;
        int newHeight = height / kEnlargingScale;
        
        Mat matTemp = matGray(cv::Rect((width - newWidth) / 2,
                                       (height - newHeight) / 2,
                                       newWidth, newHeight)).clone();
        resize(matTemp, matScaled, cv::Size(), kEnlargingScale, kEnlargingScale);
        matTemp.release();
        
        averageLength *= kEnlargingScale;
        
        CZLog(@"Scaling #1.5: image is enlarged because of low magnification");
    } else {
        matScaled = matGray.clone();
    }
    matGray.release();
    
    // Step 2: Use Otsu's thresholding algorithm and adaptive thresholding method
    // to get binary edge image.
    
    Mat matThreshold;
    threshold(matScaled, matThreshold, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);
    
    Mat matAdaptive;
    adaptiveThreshold(matThreshold, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 3, 5);
    matThreshold.release();
    
    CZLog(@"Scaling #2: edge detection complete");
    
    // Step 3: Find lines and calculate the skewing angle of the image.
    
    int minLineLength = sqrt(averageLength) * 2;
    
    vector<Vec4i> lines;
    HoughLinesP(matAdaptive,        // 8-bit single channel binary source image
                lines,              // output vector of lines
                1,                  // distance resolution in pixels
                CV_PI / 180,        // angle resolution in radians
                kHoughThreshold,    // accumulator threshold
                minLineLength,      // minimum line length
                0);                 // maximum allowed gap between points to link
    
    double averageAngle = 0.0;
    if (lines.empty()) {
        CZLog(@"Scaling warning: cannot get correct skewing angle.");
    } else {
        double weightSum = 0.0;
        double angleSum = 0.0;
        for (size_t i = 0; i < lines.size(); i++) {
            double x = lines[i][2] - lines[i][0];
            double y = lines[i][3] - lines[i][1];
            double angle = atan2(y, x);
            double distance = hypot(x, y);
            
            // Normalize angle into [0, PI)
            if (angle < 0.0) {
                angle += CV_PI;
            } else if (angle == CV_PI) {
                angle = 0.0;
            }
            
            // Normalize angle into [0, PI/2)
            if (angle >= CV_HALF_PI) {
                angle -= CV_HALF_PI;
            }
            
            // Normalize angle into (-PI/4, PI/4]
            if (angle >= CV_QUARTER_PI) {
                angle -= CV_HALF_PI;
            }
            
            // If the angle is very close to -PI/4, consider it PI/4
            if (angle < - CV_QUARTER_PI + kAngleTolerance) {
                angle = CV_QUARTER_PI;
            }
            
            double weight = pow(distance, 10) / 100.0;
            angleSum += angle * weight;
            weightSum += weight;
        }
        
        averageAngle = angleSum / weightSum * 180.0 / CV_PI;
    }
    
    CZLog(@"Scaling #3: average angle = %lf", averageAngle);
    
    // Step 4: De-skew the original image.
    // Currently no tolerance, if it is necessary, change the angle to a
    // reasonable threshold value.
    
    if (averageAngle != 0.0) {
        Point2f centerPoint(width / 2.0, height / 2.0);
        Mat rotation = getRotationMatrix2D(centerPoint, averageAngle, 1);
        warpAffine(matScaled, matScaled, rotation, matScaled.size(), INTER_CUBIC);
        warpAffine(matAdaptive, matAdaptive, rotation, matAdaptive.size(), INTER_CUBIC);
        CZLog(@"Scaling #4: rotation complete");
    } else {
        CZLog(@"Scaling #4: no need to rotate");
    }
    
    // Step 5: Find lines in the image and group by horizontal / vertical.
    
    double leaningThreshold = width * kLeaningThresholdRatio;
    
    lines.clear();
    HoughLinesP(matAdaptive, lines, 1, CV_PI / 180, kHoughThreshold,
                minLineLength, minLineLength / 5);
    
    matAdaptive.release();
    
    vector<double> values[2]; // 0 is X-axis, 1 is Y-axis
    
    for (size_t i = 0; i < lines.size(); i++) {
        int offsetX = abs(lines[i][0] - lines[i][2]);
        int offsetY = abs(lines[i][1] - lines[i][3]);
        
        if (offsetX < leaningThreshold) {
            // Vertical line, get X value
            double value = (lines[i][0] + lines[i][2]) * 0.5;
            values[0].push_back(value);
        } else if (offsetY < leaningThreshold) {
            // Horizontal line
            double value = (lines[i][1] + lines[i][3]) * 0.5;
            values[1].push_back(value);
        }
    }
    
    for (size_t axis = 0; axis < 2; ++axis) {
        sort(values[axis].begin(), values[axis].end());
    }
    
    CZLog(@"Scaling #5: reference lines found");
    
    // Step 6: Do clustering on both horizontal and vertical values.
    
    double groupingThreshold = sqrt(averageLength);
    vector<double> clusterValues[2];
    
    for (size_t axis = 0; axis < 2; ++axis) {
        double groupSum = 0.0;
        int groupCount = 0;
        
        for (size_t i = 0; i < values[axis].size(); ++i) {
            if (i == 0 || values[axis][i] - values[axis][i-1] < groupingThreshold) {
                groupSum += values[axis][i];
                ++groupCount;
            } else {
                clusterValues[axis].push_back(groupSum / groupCount);
                
                groupSum = values[axis][i];
                groupCount = 1;
            }
        }
        clusterValues[axis].push_back(groupSum / groupCount);
    }
    
    CZLog(@"Scaling #6: coordinate data clustering done");
    
    // Step 7: Calculate the scaling value by the distance among lines.
    
    double distanceSum = 0.0;
    int distanceCount = 0;
    
    for (size_t axis = 0; axis < 2; ++axis) {
        if (clusterValues[axis].size() > 1) {
            vector<char> clusterFilter;
            clusterFilter.resize(clusterValues[axis].size());
            
            double distanceSubSum = 0.0;
            for (size_t i = 0; i < clusterValues[axis].size() - 1; ++i) {
                distanceSubSum += clusterValues[axis][i+1] - clusterValues[axis][i];
            }
            double distanceSubAverage = distanceSubSum / (clusterValues[axis].size() - 1);
            
            for (int i = 0; i < clusterValues[axis].size() - 1; ++i) {
                double distance = clusterValues[axis][i+1] - clusterValues[axis][i];
                double deviation = fabs(distance - distanceSubAverage) / distanceSubAverage;
                if (deviation < kDeviationThreshold) {
                    distanceSum += distance;
                    ++distanceCount;
                } else {
                    ++clusterFilter[i];
                    ++clusterFilter[i+1];
                }
            }
            
            for (int i = clusterValues[axis].size() - 1; i >= 0; --i) {
                if (clusterFilter[i] == 2) {
                    clusterValues[axis].erase(clusterValues[axis].begin() + i);
                }
            }
            
            for (int i = 0; i < clusterValues[axis].size(); ++i) {
                if (axis == 0) {
                    line(matScaled, cvPoint(clusterValues[axis][i], 0),
                         cvPoint(clusterValues[axis][i], height), Scalar(255), 1);
                } else {
                    line(matScaled, cvPoint(0, clusterValues[axis][i]),
                         cvPoint(width, clusterValues[axis][i]), Scalar(255), 1);
                }
            }
        } else {
            clusterValues[axis].clear();
        }
    }
    
    if (clusterValues[0].empty() || clusterValues[1].empty()) {
        CZLog(@"Scaling error: result is not reliable if only horizontal or vertical lines detected.");
        return kNonCalibratedValue;
    }
    
    if (distanceCount > 0) {
        double averageDistance = distanceSum / distanceCount;
        if (isScaled) {
            averageDistance /= kEnlargingScale;
        }
        
        double fieldOfView = kCellWidthInMicrometer / averageDistance * image.size.width;
        
        CZLog(@"Scaling #7: FOV = %lf, average distance = %lf pixels", fieldOfView, averageDistance);
        
        *result = [UIImage imageWithMat:matScaled];
        
        return fieldOfView;
    } else {
        CZLog(@"Scaling error: average distance cannot be determined");
        return kNonCalibratedValue;
    }
}

- (CGFloat)fieldOfViewFromImageByFindingContours:(UIImage *)image resultImage:(UIImage **)result {
    if (image == nil) {
        CZLog(@"Scaling error: source image is nil.");
        return kNonCalibratedValue;
    }
    
    Mat matSource = [image mat];
    
    int width = matSource.cols;
    int height = matSource.rows;
    Point2f centerPoint(width / 2.0, height / 2.0);
    
    Mat matGray;
    cvtColor(matSource, matGray, CV_BGR2GRAY);
    
    // Step 1: Roughly estimate the length of pixel cycle.
    
    int meanGray = ceil(mean(matGray).val[0]);
    const size_t kEstimationSampleCount = 100;
    
    // Calculate the length of continous white or black pixels, except the
    // first length and the last since they could be incomplete shape.
    
    // First pick some rows as horizontal test samples.
    
    int totalLengthSumHorizontal = 0;
    int totalLengthCountHorizontal = 0;
    int blackCountHorizontal = 0;
    int whiteCountHorizontal = 0;
    
    for (size_t i = 0; i < kEstimationSampleCount; ++i) {
        int rowNumber = i * floor((double)height / kEstimationSampleCount);
        
        Mat matRow;
        threshold(matGray.row(rowNumber), matRow, meanGray, 255, CV_THRESH_BINARY);
        
        int lastValue = -1;
        int currentLength = 0;
        bool isFirst = true;
        for (size_t j = 0; j < matRow.cols; ++j) {
            int value = matRow.at<uchar>(0, j);
            if (value != lastValue) {
                if (!isFirst) {
                    totalLengthSumHorizontal += currentLength;
                    ++totalLengthCountHorizontal;
                    
                    if (lastValue == 0) {
                        blackCountHorizontal += currentLength;
                    } else {
                        whiteCountHorizontal += currentLength;
                    }
                }
                
                isFirst = false;
                lastValue = value;
                currentLength = 1;
            } else {
                ++currentLength;
            }
        }
    }
    
    int averageLengthHorizontal = ceil((double)totalLengthSumHorizontal / totalLengthCountHorizontal);
    float blackRatioHorizontal = 100.0f * blackCountHorizontal / (blackCountHorizontal + whiteCountHorizontal);
    
    CZLog(@"Scaling Pre-test H: estimated distance = %d, black = %f%%", averageLengthHorizontal, blackRatioHorizontal);
    
    if (blackRatioHorizontal < 33.0f || blackRatioHorizontal > 67.0f) {
        CZLog(@"Scaling error: pre-calculation shows invalid pattern for the image.");
        return kNonCalibratedValue;
    }
    
    // Then pick test some columns as vertical test samples.
    
    int totalLengthSumVertical = 0;
    int totalLengthCountVertical = 0;
    int blackCountVertical = 0;
    int whiteCountVertical = 0;
    
    for (size_t i = 0; i < kEstimationSampleCount; ++i) {
        int colNumber = i * floor((double)width / kEstimationSampleCount);
        
        Mat matColumn;
        threshold(matGray.col(colNumber), matColumn, meanGray, 255, CV_THRESH_BINARY);
        
        int lastValue = -1;
        int currentLength = 0;
        bool isFirst = true;
        for (size_t j = 0; j < matColumn.rows; ++j) {
            int value = matColumn.at<uchar>(j, 0);
            if (value != lastValue) {
                if (!isFirst) {
                    totalLengthSumVertical += currentLength;
                    ++totalLengthCountVertical;
                    
                    if (lastValue == 0) {
                        blackCountVertical += currentLength;
                    } else {
                        whiteCountVertical += currentLength;
                    }
                }
                
                isFirst = false;
                lastValue = value;
                currentLength = 1;
            } else {
                ++currentLength;
            }
        }
    }
    
    int averageLengthVertical = ceil((double)totalLengthSumVertical / totalLengthCountVertical);
    float blackRatioVertical = 100.0f * blackCountVertical / (blackCountVertical + whiteCountVertical);
    
    CZLog(@"Scaling Pre-test V: estimated distance = %d, black = %f%%", averageLengthVertical, blackRatioVertical);
    
    if (blackRatioVertical < 33.0f || blackRatioVertical > 67.0f) {
        CZLog(@"Scaling error: pre-calculation shows invalid pattern for the image.");
        return kNonCalibratedValue;
    }
    
    // Combine the results.
    
    int averageLength = (averageLengthHorizontal + averageLengthVertical) / 2;
    float blackRatio = (blackRatioHorizontal + blackRatioVertical) / 2.0f;
    
    CZLog(@"Scaling #1: overall estimated distance = %d, black = %f%%", averageLength, blackRatio);
    
    if (averageLength <= 0) {
        CZLog(@"Scaling error: average length is invalid.");
        return kNonCalibratedValue;
    }
    
    // Step 2: Use Otsu's thresholding algorithm and adaptive thresholding method
    // to get binary edge image.
    
    Mat matThreshold;
    threshold(matGray, matThreshold, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);
    matGray.release();
    
    Mat matAdaptive;
    adaptiveThreshold(matThreshold, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 3, 5);
    matThreshold.release();
    
    CZLog(@"Scaling #2: edge detection complete");
    
    // Step 3: Find contours and filter out those are not squares. Then calculate
    // FOV width with the average width / height of the squares.
    
    RNG rng(12345);
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(matAdaptive, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
    
    vector<RotatedRect> rects;
    
    double distanceSum = 0.0;
    int distanceCount = 0;
    
    for (size_t i = 0; i < contours.size(); ++i) {
        Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
        //drawContours(matSource, contours, i, color, 1, 8, hierarchy, 0, cv::Point());
        
        RotatedRect rotatedRect = minAreaRect(contours[i]);
        Point2f rectPoints[4];
        rotatedRect.points(rectPoints);
        
        double distances[4];
        double sum = 0.0f;
        for (size_t j = 0; j < 4; ++j) {
            Point2f p1 = rectPoints[j];
            Point2f p2 = rectPoints[(j + 1) % 4];
            distances[j] = hypot(p2.x - p1.x, p2.y - p1.y);
            sum += distances[j];
        }
        double average = sum / 4;
        
        double deviationSum = 0.0;
        for (size_t j = 0; j < 4; ++j) {
            deviationSum += pow(fabs(distances[j] - average), 2.0);
        }
        double deviation = deviationSum / 4;
        
        if (deviation <= 9.0 &&
            average > averageLength * 0.5 &&
            average < averageLength * 1.5) {
            for (size_t j = 0; j < 4; ++j) {
                line(matSource, rectPoints[j],
                     rectPoints[(j + 1) % 4], color, 1, 8);
            }
            
            distanceSum += average;
            ++distanceCount;
        }
    }
    
    if (distanceCount > 0) {
        double averageDistance = distanceSum / distanceCount;
        double fieldOfView = 25.0 / averageDistance * image.size.width;
        
        CZLog(@"Scaling #3: FOV = %lf, average distance = %lf", fieldOfView, averageDistance / 2);
        
        *result = [UIImage imageWithMat:matSource];
        
        return fieldOfView;
    } else {
        CZLog(@"Scaling #3: average distance cannot be determined");
        return kNonCalibratedValue;
    }
}

- (void)fileListViewController:(FileListViewController *)controller
                  didSelectRow:(NSUInteger)index {
    [_fileListPopover dismissPopoverAnimated:YES];
    [_scalingTextField setText:nil];
    
    if (self.rtspClient) {
        [self.rtspClient setDelegate:nil];
        [self.rtspClient stop];
        
        [self.decoder setDelegate:nil];
        [self.decoder stop];
    }
    
    NSString *fileName = [[NSString alloc] initWithFormat:
                          @"sro_%@.jpg",
                          _fileListView.files[index]];
    [self setImage:[UIImage imageNamed:fileName]];
    [fileName release];
}

- (void)cameraListViewController:(CameraListViewController *)controller
                    didSelectRow:(NSUInteger)index {
    [_cameraListPopover dismissPopoverAnimated:YES];
    [_scalingTextField setText:nil];
    
    NSString *ipAddress = _cameraListView.cameras[index];
    
    if (self.rtspClient) {
        [self.rtspClient setDelegate:nil];
        [self.rtspClient stop];
        
        [self.decoder setDelegate:nil];
        [self.decoder stop];
    }
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"rtsp://admin:9999@%@/PSIA/Streaming/channels/h264cif", ipAddress];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    CZHardwareDecoder *newDecoder = [[CZHardwareDecoder alloc] init];
    [newDecoder setDelegate:self];
    [newDecoder start];
    self.decoder = newDecoder;
    [newDecoder release];
    
    CZRTSPClient *newClient = [[CZRTSPClient alloc] initWithURL:url];
    [newClient setDelegate:self];
    [newClient setResolution:CGSizeMake(640, 480)];
    [newClient play];
    self.rtspClient = newClient;
    [newClient release];
    
    [urlString release];
    [url release];
}

- (void)rtspClient:(CZRTSPClient *)client didGetVideoSegment:(id<CZVideoSegment>)segment {
    [self.decoder decodeVideoSegment:segment];
}

- (void)hardwareDecoder:(CZHardwareDecoder *)decoder didGetFrame:(UIImage *)frame {
    [self setImage:frame];
}

@end
