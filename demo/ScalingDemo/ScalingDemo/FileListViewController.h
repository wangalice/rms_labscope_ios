//
//  FileListViewController.h
//  ScalingDemo
//
//  Created by Halley Gu on 6/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FileListViewController;

@protocol FileListViewControllerDelegate <NSObject>

- (void)fileListViewController:(FileListViewController *)controller didSelectRow:(NSUInteger)index;

@end

@interface FileListViewController : UITableViewController

@property (nonatomic, assign) id<FileListViewControllerDelegate> delegate;
@property (nonatomic, retain) NSArray *files;

@end
