//
//  ViewController.h
//  ScalingDemo
//
//  Created by Halley Gu on 5/14/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileListViewController.h"
#import "CameraListViewController.h"
#import "CZRTSPClient.h"
#import "CZHardwareDecoder.h"

@interface ViewController : UIViewController<FileListViewControllerDelegate,
                                             CameraListViewControllerDelegate,
                                             CZRTSPClientDelegate,
                                             CZHardwareDecoderDelegate>

@property (retain, nonatomic) IBOutlet UITextField *scalingTextField;

- (IBAction)buttonReloadAction:(id)sender;
- (IBAction)buttonLiveAction:(id)sender;
- (IBAction)buttonCallibrateAction:(id)sender;

@end
