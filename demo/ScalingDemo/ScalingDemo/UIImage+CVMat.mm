//
//  UIImage+CVMat.m
//  ScalingDemo
//
//  Created by Halley Gu on 5/23/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "UIImage+CVMat.h"

@implementation UIImage (CVMat)

- (id)initWithMat:(const cv::Mat &)cvMat {
    CFDataRef dataRef = CFDataCreate(kCFAllocatorDefault,
                                     cvMat.data,
                                     cvMat.elemSize() * cvMat.total());
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData(dataRef);
    
    CGColorSpaceRef colorSpace;
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGImageRef imageRef = CGImageCreate(cvMat.cols,
                                        cvMat.rows,
                                        8,
                                        8 * cvMat.elemSize(),
                                        cvMat.step[0],
                                        colorSpace,
                                        kCGImageAlphaNone | kCGBitmapByteOrderDefault,
                                        dataProvider,
                                        NULL,
                                        false,
                                        kCGRenderingIntentDefault);
    self = [self initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGColorSpaceRelease(colorSpace);
    CGDataProviderRelease(dataProvider);
    CFRelease(dataRef);
    
    return self;
}

+ (UIImage *)imageWithMat:(const cv::Mat &)cvMat {
    return [[[UIImage alloc] initWithMat:cvMat] autorelease];
}

- (cv::Mat)mat {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(self.CGImage);
    CGFloat cols = self.size.width;
    CGFloat rows = self.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,
                                                    cols,
                                                    rows,
                                                    8,
                                                    cvMat.step[0],
                                                    colorSpace,
                                                    kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), self.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

@end
