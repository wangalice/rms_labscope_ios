//
//  CameraListViewController.h
//  ScalingDemo
//
//  Created by Halley Gu on 6/27/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncUdpSocket.h"

@class CameraListViewController;

@protocol CameraListViewControllerDelegate <NSObject>

- (void)cameraListViewController:(CameraListViewController *)controller didSelectRow:(NSUInteger)index;

@end

@interface CameraListViewController : UITableViewController<GCDAsyncUdpSocketDelegate>

@property (nonatomic, assign) id<CameraListViewControllerDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *cameras;

@end
