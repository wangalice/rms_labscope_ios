//
//  FileListViewController.m
//  ScalingDemo
//
//  Created by Halley Gu on 6/25/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "FileListViewController.h"

@implementation FileListViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        _files = [[NSArray alloc] initWithObjects:
                  @"5x_p",
                  @"5x_pr1",
                  @"5x_pr2",
                  @"5x_dr",
                  @"10x_1",
                  @"10x_2",
                  @"10x_pr",
                  @"20x_1",
                  @"20x_2",
                  @"20x_r",
                  @"20x_pr",
                  @"20x_d",
                  @"20x_dr",
                  @"50x",
                  @"50x_r",
                  @"50x_d",
                  @"50x_pd",
                  @"50x_p",
                  @"100x_pr",
                  @"non_1",
                  @"non_2",
                  @"non_3",
                  @"non_4",
                  nil];
    }
    return self;
}

- (void)dealloc {
    [_files release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_files count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = _files[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_delegate respondsToSelector:@selector(fileListViewController:didSelectRow:)]) {
        [_delegate fileListViewController:self didSelectRow:indexPath.row];
    }
}

@end
