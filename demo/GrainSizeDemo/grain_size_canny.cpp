#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// 全局变量
Mat src, src_gray;
Mat dst, detected_edges;
int edgeThresh = 1;
int ratio = 3;
int kernel_size = 3;
const char* window_name = "Edge Map";
RNG rng(12345);

void minMaxBrightnessOfImage(Mat &grayImage, float* min, float *max) {
    Mat hist;
    int histSize = 256;
    float range[] = {0, 255};
    const float* histRange = { range };
    calcHist(&grayImage, 1, 0, Mat(), hist, 1, &histSize, &histRange, true, true);
    
    float threshold = 0.005 * grayImage.rows * grayImage.cols;
    
    float maxBrightness = 0;
    float minBrightness = 255;
    
    for (int i = 0; i < 256; i++) {
        float count = hist.at<float>(i);
        if (count >= threshold) {
            maxBrightness = i;
            if (minBrightness > i) {
                minBrightness = i;
            }
        }
    }
    
    *min = minBrightness;
    *max = maxBrightness;
}

void minMaxBrightnessOfImage2(Mat &grayImage, float* min, float *max) {
    Mat hist;
    int histSize = 256;
    float range[] = { 0, 255 } ;
    const float* histRange = { range };
    calcHist(&grayImage, 1, 0, Mat(), hist, 1, &histSize, &histRange, true, true);
    
    // calculate averange
    float sum = 0;
    int sumCount = 0;
    for (int i = 0; i < 256; i++) {
        sum += hist.at<float>(i) * i;
        sumCount += hist.at<float>(i);
    }
    double avg = sum / sumCount;
    
    // calculate standard deviation
    sum = 0;
    for (int i = 0; i < 256; i++) {
        double d = i - avg;
        sum += d * d * hist.at<float>(i);
    }
    double deviation = sqrt(sum / sumCount);
    
    *min = avg - 2.0 * deviation;
    *max = avg + 2.0 * deviation;
}

double myAdaptiveThreshold(int histogram[], int total) {
    int threshold = 0;
    int sum = 0;
    bool up = false;
    total /= 10;
    for (int i = 0; i < 256; i++) {
        sum += histogram[i];
        
        if (sum > total) {
            if (!up) {
                up = true;
            }
        }
        
        if (histogram[i] > total) {
            continue;
        }
        
        if (up) {
            if (histogram[i] < 6) {
                threshold = i;
                break;
            }
        }
    }
    return threshold;
}

double OtsuThreshold(int histogram[], int total) {
    int sum = 0;
    for (int i = 1; i < 256; ++i) {
        sum += i * histogram[i];
    }
    int sumB = 0;
    int wB = 0;
    int wF = 0;
    double mB;
    double mF;
    double max = 0.0;
    double between = 0.0;
    double threshold1 = 0.0;
    double threshold2 = 0.0;
    for (int i = 0; i < 256; ++i) {
        wB += histogram[i];
        if (wB == 0) {
            continue;
        }
        wF = total - wB;
        if (wF == 0) {
            break;
        }
        sumB += i * histogram[i];
        mB = sumB / wB;
        mF = (sum - sumB) / wF;
        between = wB * wF * (mB - mF) * (mB - mF);
        if (between >= max) {
            threshold1 = i;
            if (between > max) {
                threshold2 = i;
            }
            max = between;
        }
    }
    return (threshold1 + threshold2) / 2.0;
}

double CZGrainSizeSmallObjectThreshold(int histogram[], int total) {
    int otsu_threshold = OtsuThreshold(histogram, total);
    otsu_threshold /= 2;
    
    // calculate averange in [0, otsu_threshold]
    double sum = 0;
    int sumCount = 0;
    for (int i = 0; i < otsu_threshold; i++) {
        sum += histogram[i] * i;
        sumCount += histogram[i];
    }
    double avg = sum / sumCount;

    // calculate standard deviation in [0, otsu_threshold]
    sum = 0;
    for (int i = 0; i < otsu_threshold; i++) {
        double d = i - avg;
        sum += d * d * histogram[i];
    }
    double deviation = sqrt(sum / sumCount);

    return avg + 2 * deviation;
}

void CannyThreshold(int, void*)
{
    float maxB, minB;
    minMaxBrightnessOfImage2(src_gray, &minB, &maxB);
    
    Mat binary;
    double thresholdBase = threshold(src_gray, binary, 0, 255, CV_THRESH_OTSU);
    double position = (thresholdBase - minB) / (maxB - minB);
    printf("minB=%.2f, maxB=%.2f, threshold=%.2f, position = %.3f\n", minB, maxB, thresholdBase, position * 100);
    
    /// 使用 5x5内核降噪
    GaussianBlur(src_gray, detected_edges, cv::Size(5, 5), 0, 0);
    imshow("Gray", detected_edges);
    
    bool useCanny = position <= 0.2;

    cv::Size smallSize;
    if (!useCanny) {
        imshow("Binary", binary);
    } else {
        binary.release();
        binary = detected_edges;
    }
    
    Mat temp;
    thresholdBase = threshold(binary, temp, 0, 255, CV_THRESH_OTSU);
    temp.release();
    
    /// 运行Canny算子
    if (useCanny) {
        Canny(binary, binary, thresholdBase * 0.5, thresholdBase, kernel_size, true);
    }
    
    /// 显示Canny计算结果图
    imshow(window_name, binary);

    /// 寻找轮廓
    Mat canny_output = binary;
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    
    // remove small objects
    vector<int> judgement;
    int histgram[256];  // histgram of judgement
    for (int i = 0; i < 256; i++) {
        histgram[i] = 0;
    }

    for (int i = 0; i < contours.size(); i++) {
        vector<cv::Point> contour = contours[i];
        const size_t contourSize = contour.size();
        if (contourSize < 2) {
            histgram[0]++;
            judgement.push_back(0);
            continue;
        }
        
        cv::Rect rbounding = cv::boundingRect(contour);
        double length = cv::arcLength(contour, false);
        
        double max = std::max(rbounding.width, rbounding.height);
        double min = std::min(rbounding.width, rbounding.height);
        if (min == 0) {
            max++;
            min = 1;
        }
        int j = length * max / min;
        if (j > 255) {
            j = 255;
        }
        judgement.push_back(j);
        histgram[j]++;
    }
    int lengthThreshold = CZGrainSizeSmallObjectThreshold(histgram, contours.size());
    printf("lengthThreshold=%d\n", lengthThreshold);
    
    // draw contours
    Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
    for (int i = 0; i< contours.size(); i++) {
        Scalar color;
        int thickness = 1;
        int j = judgement[i];
        if (j >= lengthThreshold) {
            color = Scalar(rng.uniform(0,200), rng.uniform(0,200), 0);  // draw big object as random color
            thickness = 1;
        } else {
            color = Scalar(0, 0, 255);  // draw small object as red
        }
        
        drawContours(drawing, contours, i, color, thickness, 8, hierarchy, 0, cv::Point());
    }

    /// 在窗体中显示结果
    namedWindow("Contours", CV_WINDOW_AUTOSIZE);
    imshow("Contours", drawing);
}

/** @函数 main */
int main(int argc, char** argv)
{
    /// 装载图像
    src = imread(argv[1]);
    if (!src.data) {
        printf("failed to open image");
        return -1;
    }
    
    /// 创建与src同类型和大小的矩阵(dst)
    dst.create(src.size(), src.type());
    
    /// 原图像转换为灰度图像
    cvtColor(src, src_gray, CV_BGR2GRAY);
    
    /// 显示图像
    CannyThreshold(0, 0);
    
    /// 等待用户反应
    waitKey(0);
    
    return 0;
}