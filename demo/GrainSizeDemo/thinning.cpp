#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

void ThinSubiteration1(Mat &pSrc, Mat &pDst) {
    int rows = pSrc.rows;
    int cols = pSrc.cols;
    pSrc.copyTo(pDst);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (pSrc.at<uchar>(i, j) == 1.0f) {
                /// get 8 neighbors
                /// calculate C(p)
                int neighbor0 = (int) pSrc.at<uchar>(i - 1, j - 1);
                int neighbor1 = (int) pSrc.at<uchar>(i - 1, j);
                int neighbor2 = (int) pSrc.at<uchar>(i - 1, j + 1);
                int neighbor3 = (int) pSrc.at<uchar>(i, j + 1);
                int neighbor4 = (int) pSrc.at<uchar>(i + 1, j + 1);
                int neighbor5 = (int) pSrc.at<uchar>(i + 1, j);
                int neighbor6 = (int) pSrc.at<uchar>(i + 1, j - 1);
                int neighbor7 = (int) pSrc.at<uchar>(i, j - 1);
                int C = int(~neighbor1 & ( neighbor2 | neighbor3)) +
                int(~neighbor3 & ( neighbor4 | neighbor5)) +
                int(~neighbor5 & ( neighbor6 | neighbor7)) +
                int(~neighbor7 & ( neighbor0 | neighbor1));
                if (C == 1) {
                    /// calculate N
                    int N1 = int(neighbor0 | neighbor1) +
                    int(neighbor2 | neighbor3) +
                    int(neighbor4 | neighbor5) +
                    int(neighbor6 | neighbor7);
                    int N2 = int(neighbor1 | neighbor2) +
                    int(neighbor3 | neighbor4) +
                    int(neighbor5 | neighbor6) +
                    int(neighbor7 | neighbor0);
                    int N = min(N1,N2);
                    if ((N == 2) || (N == 3)) {
                        /// calculate criteria 3
                        int c3 = ( neighbor1 | neighbor2 | ~neighbor4) & neighbor3;
                        if (c3 == 0) {
                            pDst.at<uchar>(i, j) = 0;
                        }
                    }
                }
            }
        }
    }
}

void ThinSubiteration2(Mat &pSrc, Mat &pDst) {
    int rows = pSrc.rows;
    int cols = pSrc.cols;
    pSrc.copyTo( pDst);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (pSrc.at<uchar>(i, j) == 1.0f) {
                /// get 8 neighbors
                /// calculate C(p)
                int neighbor0 = (int) pSrc.at<uchar>(i - 1, j - 1);
                int neighbor1 = (int) pSrc.at<uchar>(i - 1, j);
                int neighbor2 = (int) pSrc.at<uchar>(i - 1, j + 1);
                int neighbor3 = (int) pSrc.at<uchar>(i, j + 1);
                int neighbor4 = (int) pSrc.at<uchar>(i + 1, j + 1);
                int neighbor5 = (int) pSrc.at<uchar>(i + 1, j);
                int neighbor6 = (int) pSrc.at<uchar>(i + 1, j - 1);
                int neighbor7 = (int) pSrc.at<uchar>(i, j - 1);
                int C = int(~neighbor1 & ( neighbor2 | neighbor3)) +
                    int(~neighbor3 & ( neighbor4 | neighbor5)) +
                    int(~neighbor5 & ( neighbor6 | neighbor7)) +
                    int(~neighbor7 & ( neighbor0 | neighbor1));
                if (C == 1) {
                    /// calculate N
                    int n1 = int(neighbor0 | neighbor1) +
                    int(neighbor2 | neighbor3) +
                    int(neighbor4 | neighbor5) +
                    int(neighbor6 | neighbor7);
                    int n2 = int(neighbor1 | neighbor2) +
                    int(neighbor3 | neighbor4) +
                    int(neighbor5 | neighbor6) +
                    int(neighbor7 | neighbor0);
                    int n = min(n1, n2);
                    if ((n == 2) || (n == 3)) {
                        int E = (neighbor5 | neighbor6 | ~neighbor0) & neighbor7;
                        if (E == 0) {
                            pDst.at<uchar>(i, j) = 0;
                        }
                    }
                }
            }
        }
    }
}

void NormalizeLetter(Mat &inputarray, Mat &outputarray) {
    bool bDone = false;
    int rows = inputarray.rows;
    int cols = inputarray.cols;
    
    inputarray.convertTo(inputarray, CV_8UC1);
    
    /// pad source
    Mat p_enlarged_src = Mat(rows + 2, cols + 2, CV_8UC1);
    for(int i = 0; i < (rows + 2); i++) {
        p_enlarged_src.at<uchar>(i, 0) = 0;
        p_enlarged_src.at<uchar>(i, cols + 1) = 0;
    }
    for(int j = 0; j < (cols + 2); j++) {
        p_enlarged_src.at<uchar>(0, j) = 0;
        p_enlarged_src.at<uchar>(rows + 1, j) = 0;
    }
    
    inputarray.copyTo(p_enlarged_src(Rect(2, 2, cols, rows)));
    threshold(p_enlarged_src, p_enlarged_src, 20, 1, THRESH_BINARY);

    /// start to thin
    Mat p_thinMat1 = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    Mat p_thinMat2 = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    Mat p_cmp = Mat::zeros(rows + 2, cols + 2, CV_8UC1);
    
    while (bDone != true) {
        /// sub-iteration 1
        ThinSubiteration1(p_enlarged_src, p_thinMat1);
        /// sub-iteration 2
        ThinSubiteration2(p_thinMat1, p_thinMat2);
        /// compare
        compare(p_enlarged_src, p_thinMat2, p_cmp, CV_CMP_EQ);
        /// check
        int num_non_zero = countNonZero(p_cmp);
        if (num_non_zero == (rows + 2) * (cols + 2)) {
            bDone = true;
        }
        /// copy
        p_thinMat2.copyTo(p_enlarged_src);
    }
    
    outputarray = p_enlarged_src(Rect(1, 1, cols, rows));
}

/** @函数 main */
int main(int argc, char** argv) {
    /// 装载图像
    Mat src, src_gray;
    src = imread(argv[1]);
    if (!src.data) {
        printf("failed to open image");
        return -1;
    }

    /// 原图像转换为灰度图像
    cvtColor(src, src_gray, CV_BGR2GRAY);
    
    Mat output;
    NormalizeLetter(src_gray, output);
    
    output.convertTo(output, CV_8UC1);
    threshold(output, output, 0, 255, THRESH_BINARY);
    
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));

    std::vector<cv::Point > points;
    points.reserve(5);
    
    for (int i = 0; i < contours.size(); i++) {
        drawContours(src, contours, i, Scalar(0, 0, 255));
        int step = contours[i].size() / 100;
        int stepLength = 5;
        for (int j = stepLength; j + stepLength < contours[i].size(); j+= step) {
            points.clear();
            for (int k = j - stepLength; k <= j + stepLength; k++) {
                points.push_back(contours[i][k]);
            }
            
            cv::Vec4f line;
            cv::fitLine(points, line, CV_DIST_L2, 0, 0.001, 0.001);
            
            // rotate 90 degree
            double vx = -line[1];
            double vy = line[0];
            
            double thickness = 50;
            
            cv::Point startPoint(contours[i][j].x - vx * thickness, contours[i][j].y - vy * thickness);
            cv::Point endPoint(contours[i][j].x + vx * thickness, contours[i][j].y + vy * thickness);
            cv::line(src, startPoint, endPoint, Scalar(0, 100, 0));
        }
    }
    
    imshow("output", src);
    imwrite("output.png", src);
    
    /// 等待用户反应
    waitKey(0);
    
    return 0;
}