#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// global variables

Mat src;
const char* window_name = "Edge Map";
static const int kAdaptiveBlockSize = 7;
static const double kAdaptiveCValue = 5;

void AdaptiveThresholdFunction(int, void*)
{
    Mat src_gray;
    cvtColor( src, src_gray, CV_BGR2GRAY );
    
    ///降噪
    medianBlur(src_gray, src_gray, 3);
    
    Mat matAdaptive;
    adaptiveThreshold(src_gray, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                      THRESH_BINARY_INV, kAdaptiveBlockSize, kAdaptiveCValue);
    src_gray.release();
    
    imshow( window_name, matAdaptive );
}

void MorphologyFunction(int, void*) {
    Mat src_gray;
    cvtColor( src, src_gray, CV_BGR2GRAY );

    medianBlur(src_gray, src_gray, 3);
    
    // Calculate a base threshold using Otsu's algorithm.
    Mat matTemp;
    double thresholdBase = threshold(src_gray, matTemp, 0, 255, CV_THRESH_OTSU);
    matTemp.release();
    
    // Use a position between base threshold and 255 as new threshold, ignore
    // all the pixels which have lower grayscale value than the threshold.
    Mat matThreshold;
    double newThreshold = thresholdBase * 0.5;// + (255 - thresholdBase) * kExtraThresholdingRatio;
    threshold(src_gray, matThreshold, newThreshold, 255, CV_THRESH_BINARY);
    
    Mat element5(5,5,CV_8U,Scalar(1));
    morphologyEx(src_gray, src_gray, MORPH_GRADIENT, element5);
}


class WatershedSegment{
private:
    cv::Mat markers;
public:
    void setMarkers(cv::Mat &markerImage)
    {
        markerImage.convertTo(markers,CV_32S);//convert to image of ints
        
    }
    
    cv::Mat process(cv::Mat &image)
    {
        cv::watershed(image,markers);
        markers.convertTo(markers,CV_8U);
        return markers;
    }
};

// reference: http://docs.opencv.org/trunk/doc/py_tutorials/py_imgproc/py_watershed/py_watershed.html?highlight=watershed
//            http://stackoverflow.com/questions/11294859/how-to-define-the-markers-for-watershed-in-opencv/11438165#11438165
//            http://cmm.ensmp.fr/~beucher/wtshed.html
void distanceTransformAndAdaptiveFunction(int, void*) {
    //convert to binary image,
    cv::Mat binary;
    cv::Mat src_gray;
    cv::cvtColor(src,src_gray,CV_BGR2GRAY);
    medianBlur(src_gray, src_gray, 3);
    
    cv::threshold(src_gray,binary,0,255,THRESH_BINARY_INV|CV_THRESH_OTSU);
    
    //display them
    cv::imshow("OriginalImage",src);
    cv::imshow("OriginalBinary",binary);
    
    // noise removal
    Mat kernel(5,5,CV_8U,Scalar(1));
    Mat opening;
    morphologyEx(binary, opening, MORPH_OPEN, kernel);
    cv::imshow("Open", opening);
    
    //sure background area
    Mat sure_bg;
    dilate(opening, sure_bg, kernel,Point(-1,-1), 3);
    cv::imshow("background", sure_bg);
    
    // create distance transform image
    Mat dist_transform;
    distanceTransform(opening, dist_transform, CV_DIST_L2, 5);
    cv::normalize(dist_transform, dist_transform, 0, 1.0, cv::NORM_MINMAX);
    cv::imshow("distance transform", dist_transform);
    
    // create for sure foreground image
    Mat sure_fg;
    cv::threshold(dist_transform, dist_transform, 0.7, 1, CV_THRESH_BINARY);
    dist_transform.convertTo(sure_fg, CV_8U, 255, 0);
    cv::imshow("foreground", sure_fg);
    
    // create border image
    sure_fg = sure_bg - sure_fg;
    cv::imshow("border", sure_fg);
    
    // find edge
    Mat matAdaptive;
    adaptiveThreshold(src_gray, matAdaptive, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C,
                      THRESH_BINARY_INV, kAdaptiveBlockSize, kAdaptiveCValue);
    cv::imshow("adaptive", matAdaptive);
    
    // merge border and edge
    matAdaptive = sure_fg & matAdaptive;
    cv::imshow("result", matAdaptive);
}

void distanceTransformAndCannyFunction(int, void*) {
    //convert to binary image,
    cv::Mat binary;
    cv::Mat src_gray;
    cv::cvtColor(src,src_gray,CV_BGR2GRAY);
    medianBlur(src_gray, src_gray, 3);
    
    double thresholdBase = cv::threshold(src_gray,binary,0,255,THRESH_BINARY_INV|CV_THRESH_OTSU);
    
    //display them
    cv::imshow("OriginalImage",src);
    cv::imshow("OriginalBinary",binary);
    
    // noise removal
    Mat kernel(5,5,CV_8U,Scalar(1));
    Mat opening;
    morphologyEx(binary, opening, MORPH_OPEN, kernel);
    cv::imshow("Open", opening);
    
    //sure background area
    Mat sure_bg;
    dilate(opening, sure_bg, kernel,Point(-1,-1), 3);
    cv::imshow("background", sure_bg);
    
    //create distance transform image
    Mat dist_transform;
    distanceTransform(opening, dist_transform, CV_DIST_L2, 5);
    cv::normalize(dist_transform, dist_transform, 0, 1.0, cv::NORM_MINMAX);
    cv::imshow("distance transform", dist_transform);
    
    // create for sure foreground image
    Mat sure_fg;
    cv::threshold(dist_transform, dist_transform, 0.1, 1, CV_THRESH_BINARY);
    dist_transform.convertTo(sure_fg, CV_8U, 255, 0);
    cv::imshow("foreground", sure_fg);
    
    // create border image
    sure_fg = sure_bg - sure_fg;
    cv::imshow("border", sure_fg);
    
    /// find edge
    Mat matCanny;
    Canny( src_gray, matCanny, thresholdBase * 0.5, thresholdBase, 3, true );
    cv::imshow("Canny", matCanny);
    
    // merge border and edge
    matCanny = sure_fg & matCanny;
    cv::imshow("result", matCanny);
}

void markFunction(int, void*) {
    //convert to binary image,
    cv::Mat binary ;
    cv::cvtColor(src,binary,CV_BGR2GRAY);
    cv::threshold(binary,binary,0,255,THRESH_BINARY_INV|CV_THRESH_OTSU);
    
    //display them
    cv::imshow("OriginalImage",src);
    cv::imshow("OriginalBinary",binary);
    
    //obviously,in the binaryImage,there is so much noise around the object;
    //so next is to eliminate noise and smaller objects
    cv::Mat fg;//foreground
    cv::erode(binary,fg,cv::Mat(),cv::Point(-1,-1),2);
    cv::imshow("fg",fg);
    
    //idenify image pixeles without objects
    cv::Mat bg;//background
    cv::dilate(binary,bg,cv::Mat(),cv::Point(-1,-1),3);
    cv::threshold(bg,bg,1,128,cv::THRESH_BINARY_INV);
    cv::imshow("bg",bg);
    
    //create markers image
    cv::Mat markers(binary.size(),CV_8U,cv::Scalar(0));
    markers=bg+fg;
    cv::imshow("markers",markers);
    
    //create watershed segmentation object
    WatershedSegment segmenter;
    segmenter.setMarkers(markers);
    
    cv::Mat result=segmenter.process(src);
    cv::threshold(result,result,0,255,cv::THRESH_BINARY);
    //result=result&1;
    imshow("final_result",result);
}

/** @function main */
int main( int argc, char** argv ) {
    /// read image
    src = imread(argv[1]);
    
    if( !src.data ) {
        printf("failed to open image");
        return -1;
    }

    /// run process, pick one from following functions
    markFunction(0, 0);
//    distanceTransformAndCannyFunction(0, 0);
//    distanceTransformAndAdaptiveFunction(0, 0);
//    ...
    
    waitKey(0);
    
    return 0;
}