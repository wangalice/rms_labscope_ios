//
//  BTViewController.m
//  test
//
//  Created by Cameron Cooke on 23/01/2014.
//  Copyright (c) 2014 Brightec Ltd. All rights reserved.
//

#import "BTViewController.h"
#import "BTSlideInteractor.h"
#import "BTModalViewController.h"

@interface BTViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@property (weak, nonatomic) IBOutlet UIButton *showModalButton;
@end

@implementation BTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(orientationChanged:)
               name:UIDeviceOrientationDidChangeNotification
             object:nil];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self
                  name:UIDeviceOrientationDidChangeNotification
                object:nil];
}


- (void)orientationChanged:(NSNotification *)n {
    UIDeviceOrientation o = [[UIDevice currentDevice] orientation];
    if (o == UIDeviceOrientationLandscapeLeft || o == UIDeviceOrientationLandscapeRight) {
        [self showModalButtonWasTouched:nil];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (IBAction)showModalButtonWasTouched:(id)sender {
    self.interactor = [[BTSlideInteractor alloc] init];
    CGRect frame  = self.showModalButton.frame;
    self.interactor.fromFrame = frame;
    
    BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController.modalPresentationStyle = UIModalPresentationFullScreen; // or UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self presentViewController:modalController animated:YES completion:nil];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
