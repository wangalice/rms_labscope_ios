//
//  BTSlideInteractor.m
//  test
//
//  Created by Cameron Cooke on 21/01/2014.
//  Copyright (c) 2014 Brightec Ltd. All rights reserved.
//

#import "BTSlideInteractor.h"

@implementation BTSlideInteractor

# pragma mark -
# pragma mark UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return self;
}


- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return self;
}


# pragma mark -
# pragma mark UIViewControllerAnimatedTransitioning

- (void)animationEnded:(BOOL)transitionCompleted
{
}


- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return .5f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];
        
    const BOOL presenting = [toViewController isBeingPresented];
    
    if (presenting) {
        NSLog(@"presenting animation:");
    } else {
        NSLog(@"dismissing animation:");
    }
    
    NSLog(@"containerView frame=%@ bounds=%@", NSStringFromCGRect(containerView.frame), NSStringFromCGRect(containerView.bounds));
    
    if (presenting) {
        UIView *snapshotView = [fromViewController.view resizableSnapshotViewFromRect:self.fromFrame
                                                                   afterScreenUpdates:NO
                                                                        withCapInsets:UIEdgeInsetsZero];
        // set starting rect for animation
        CGFloat rotateAngle = M_PI_2;
        
        if (toViewController.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            rotateAngle = M_PI_2;
        } else if (toViewController.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
            rotateAngle = -M_PI_2;
        }
        
        // set start position
        CGPoint center2;
        center2.x = CGRectGetMidX(self.fromFrame);
        center2.y = CGRectGetMidY(self.fromFrame);
        center2 = [fromViewController.view convertPoint:center2 toView:containerView];
        
        CGFloat width = self.fromFrame.size.width;
        CGFloat height = self.fromFrame.size.height;
        CGRect fromRect2 = CGRectMake(center2.x - width / 2, center2.y - height / 2, width, height);
        
        snapshotView.frame = fromRect2;  // TODO: calculate rect
        snapshotView.transform = CGAffineTransformMakeRotation(-rotateAngle);
        [containerView addSubview:snapshotView];
        
        // get end position
        CGRect toFrame = containerView.frame;

        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            snapshotView.transform = CGAffineTransformIdentity;
            snapshotView.frame = toFrame;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
            
            [snapshotView removeFromSuperview];
            toViewController.view.frame = toFrame;
            [containerView addSubview:toViewController.view];
        }];
    } else {
        // show to view at back
        toViewController.view.frame = containerView.frame;
        [containerView insertSubview:toViewController.view belowSubview:fromViewController.view];
        
        fromViewController.view.hidden = YES;
        
        // snap shot view
        UIView *snapshotView = [fromViewController.view snapshotViewAfterScreenUpdates:NO];
        snapshotView.transform = fromViewController.view.transform;
        snapshotView.frame = fromViewController.view.frame;
        [containerView addSubview:snapshotView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            snapshotView.transform = CGAffineTransformIdentity;
            snapshotView.frame = self.fromFrame;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
            [snapshotView removeFromSuperview];
            [fromViewController.view removeFromSuperview];
        }];
    }
}

@end
