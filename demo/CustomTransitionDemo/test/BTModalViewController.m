//
//  BTModalViewController.m
//  test
//
//  Created by Cameron Cooke on 23/01/2014.
//  Copyright (c) 2014 Brightec Ltd. All rights reserved.
//

#import "BTModalViewController.h"

@interface BTModalViewController ()
@end

@implementation BTModalViewController

- (IBAction)dismissButtonWasTouched:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(orientationChanged:)
               name:UIDeviceOrientationDidChangeNotification
             object:nil];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self
                  name:UIDeviceOrientationDidChangeNotification
                object:nil];
}


- (void)orientationChanged:(NSNotification *)notification {
    UIDeviceOrientation o = [[UIDevice currentDevice] orientation];
    if (UIDeviceOrientationIsPortrait(o)) {
        [self dismissButtonWasTouched:nil];
    }
}

@end
