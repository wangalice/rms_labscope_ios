//
//  CZCommonUtils.m
//  Hermes
//
//  Created by Mike Wang on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCommonUtils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation CZCommonUtils

+ (NSString *)md5FromString:(NSString *)inputString {
    const char *cString = [inputString UTF8String];
    unsigned char cResult[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cString, strlen(cString), cResult);
    
    NSMutableString *mutableResult = [[NSMutableString alloc] init];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; ++i) {
        NSString *hexString = [[NSString alloc] initWithFormat:@"%02x", cResult[i]];
        [mutableResult appendString:hexString];
        [hexString release];
    }
    
    NSString *result = [[mutableResult copy] autorelease];
    
    [mutableResult release];
    
    return result;
}

@end
