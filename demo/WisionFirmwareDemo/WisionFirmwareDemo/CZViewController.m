//
//  CZViewController.m
//  WisionFirmwareDemo
//
//  Created by Halley Gu on 4/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import "CZViewController.h"
#import "CZCamera.h"
#import "CZCameraBrowser.h"

static NSString * const kTableCellIdentifier = @"TableCell";

@interface CZViewController () <UITableViewDataSource, UITableViewDelegate, CZCameraBrowserDelegate>

@property (nonatomic, retain) NSMutableDictionary *cameras;
@property (nonatomic, retain) NSMutableDictionary *status;
@property (nonatomic, retain) NSObject *lock;
@property (nonatomic, retain) UITableView *tableView;

@end

@implementation CZViewController

- (void)dealloc {
    [_cameras release];
    [_status release];
    [_lock release];
    [_tableView release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _status = [[NSMutableDictionary alloc] init];
    _cameras = [[NSMutableDictionary alloc] init];
    _lock = [[NSObject alloc] init];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
    [[CZCameraBrowser sharedInstance] setDelegate:self];
    [[CZCameraBrowser sharedInstance] beginBrowsing];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.tableView.frame = self.view.bounds;
}

#pragma mark - UITableViewDataSource methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Wision Cameras";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    @synchronized (self.lock) {
        return self.cameras.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @synchronized (self.lock) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:kTableCellIdentifier];
        }
        
        NSString *key = self.cameras.allKeys[indexPath.row];
        CZCamera *camera = [self.cameras objectForKey:key];
        NSNumber *statusNumber = [self.status objectForKey:key];
        int status = statusNumber.intValue;
        
        NSString *statusString = nil;
        UIColor *labelColor = [UIColor blackColor];
        switch (status) {
            // Normal
            case 0:
                statusString = @"Outdated";
                break;
                
            case 10:
                statusString = @"Up-to-date";
                break;
                
            // Preparing
            case 1:
            case 11:
                statusString = @"Preparing";
                labelColor = [UIColor grayColor];
                break;
            
            // Restarting before updating
            case 2:
            case 12:
                statusString = @"Restarting before updating";
                labelColor = [UIColor grayColor];
                break;
                
            // Updating
            case 3:
                statusString = @"Upgrading firmware";
                labelColor = [UIColor grayColor];
                break;
                
            case 13:
                statusString = @"Downgrading firmware";
                labelColor = [UIColor grayColor];
                break;
                
            // Restarting after updating
            case 4:
            case 14:
                statusString = @"Restarting after updating";
                labelColor = [UIColor grayColor];
                break;
                break;
                
            // Failed
            case 5:
                statusString = @"Failed to upgrade";
                break;
                
            case 15:
                statusString = @"Failed to downgrade";
                break;
                
            default:
                break;
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", key, statusString];
        cell.textLabel.textColor = labelColor;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@    %@", camera.firmwareVersion, camera.ipAddress];
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate methods

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    @synchronized (self.lock) {
        NSString *key = self.cameras.allKeys[indexPath.row];
        NSNumber *statusNumber = [self.status objectForKey:key];
        int status = statusNumber.intValue;
        return (status % 5 == 0);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    @synchronized (self.lock) {
        NSString *key = self.cameras.allKeys[indexPath.row];
        NSNumber *statusNumber = [self.status objectForKey:key];
        int status = statusNumber.intValue;
        
        CZCamera *camera = [self.cameras objectForKey:key];
        [camera restart];
        
        NSLog(@"restart command sent!");
        
        if (status == 0 || status == 5) {
            status = 1;
        } else if (status == 10 || status == 15) {
            status = 11;
        }
        
        self.status[key] = @(status);
        NSLog(@"status of %@ changed to %d", key, status);
    }
    
    [self.tableView reloadData];
}

#pragma mark - CZCameraBrowserDelegate methods

- (void)cameraBrowserDidFindCameras:(NSArray *)cameras {
    dispatch_async(dispatch_get_main_queue(), ^(){
        @synchronized (self.lock) {
            for (CZCamera *camera in cameras) {
                NSString *key = camera.macAddress;
                CZCamera *value = [self.cameras objectForKey:key];
                if (value) {
                    NSNumber *statusNumber = [self.status objectForKey:key];
                    int status = [statusNumber intValue];
                    if (status == 2 || status == 12) {
                        ++status;
                        [self.status setObject:@(status) forKey:key];
                        
                        NSLog(@"status of %@ changed to %d", key, status);
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                            NSString *firmwareVersion = nil;
                            while (firmwareVersion.length == 0) {
                                sleep(2);
                                firmwareVersion = [camera firmwareVersion];
                            }
                            
                            NSLog(@"start updating firmware!");
                            
                            if (status == 3) {
                                [camera updateFirmware:YES];
                            } else if (status == 13) {
                                [camera updateFirmware:NO];
                            }
                            
                            NSLog(@"restart command sent!");
                            
                            [camera restart];
                        });
                    } else if (status == 4) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                            NSString *firmwareVersion = nil;
                            while (firmwareVersion.length == 0) {
                                sleep(2);
                                firmwareVersion = [camera firmwareVersion];
                            }
                            
                            if ([camera isFirmwareUpdated]) {
                                [self.status setObject:@10 forKey:key];
                            } else {
                                [self.status setObject:@5 forKey:key];
                            }
                            
                            NSLog(@"status of %@ changed to %@", key, self.status[key]);
                            
                            dispatch_async(dispatch_get_main_queue(), ^(){
                                [self.tableView reloadData];
                            });
                        });
                    } else if (status == 14) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                            NSString *firmwareVersion = nil;
                            while (firmwareVersion.length == 0) {
                                sleep(2);
                                firmwareVersion = [camera firmwareVersion];
                            }
                            
                            if ([camera isFirmwareUpdated]) {
                                [self.status setObject:@15 forKey:key];
                            } else {
                                [self.status setObject:@0 forKey:key];
                            }
                            
                            NSLog(@"status of %@ changed to %@", key, self.status[key]);
                            
                            dispatch_async(dispatch_get_main_queue(), ^(){
                                [self.tableView reloadData];
                            });
                        });
                    }
                } else {
                    [self.cameras setObject:camera forKey:key];
                    if ([camera isFirmwareUpdated]) {
                        [self.status setObject:@10 forKey:key];
                    } else {
                        [self.status setObject:@0 forKey:key];
                    }
                    
                    NSLog(@"status of %@ changed to %@", key, self.status[key]);
                }
            }
        }
        
        [self.tableView reloadData];
    });
}

- (void)cameraBrowserDidGetUpdatedDataFromCameras:(NSArray *)cameras {
    // Nothing
}

- (void)cameraBrowserDidLoseCameras:(NSArray *)cameras {
    dispatch_async(dispatch_get_main_queue(), ^(){
        @synchronized (self.lock) {
            for (CZCamera *camera in cameras) {
                NSString *key = camera.macAddress;
                CZCamera *value = [self.cameras objectForKey:key];
                if (value) {
                    NSNumber *statusNumber = [self.status objectForKey:key];
                    int status = [statusNumber intValue];
                    
                    if (status == 1 || status == 11 || status == 3 || status == 13) {
                        ++status;
                        [self.status setObject:@(status) forKey:key];
                        
                        NSLog(@"status of %@ changed to %d", key, status);
                    } else if (status == 0 || status == 10) {
                        [self.cameras removeObjectForKey:key];
                        [self.status removeObjectForKey:key];
                    }
                }
            }
        }
        
        [self.tableView reloadData];
    });
}

@end
