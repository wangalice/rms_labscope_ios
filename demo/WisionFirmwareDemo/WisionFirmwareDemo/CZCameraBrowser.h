//
//  CZCameraBrowser.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

@class CZCamera;

@protocol CZCameraBrowserDelegate <NSObject>
@optional
- (void)cameraBrowserDidFindCameras:(NSArray *)cameras;
- (void)cameraBrowserDidGetUpdatedDataFromCameras:(NSArray *)cameras;
- (void)cameraBrowserDidLoseCameras:(NSArray *)cameras;

@end

/*!
 * A singleton pattern implementation that provides a unified way to discover
 * all kinds of cameras in the same local network.
 */
@interface CZCameraBrowser : NSObject<GCDAsyncUdpSocketDelegate>

@property (atomic, assign) id<CZCameraBrowserDelegate> delegate;

/**
 * The one and only way to get the shared instance.
 * \return The global shared instance of CZCameraBrowser
 */
+ (CZCameraBrowser *)sharedInstance;

/*! Returns cached list which contains all the discovered cameras. */
- (NSArray *)cameras;

/*! Starts browsing cameras. */
- (void)beginBrowsing;
 
/*! Abandons the current browsing action. */
- (void)stopBrowsing;

@end
