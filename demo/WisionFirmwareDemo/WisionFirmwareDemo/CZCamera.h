//
//  CZCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _CZCameraExposureMode {
    kCZCameraExposureAutomatic = 0,
    kCZCameraExposureManual = 1,
    kCZCameraExposureMixed = 2
} CZCameraExposureMode;

typedef enum _CZCameraBitRate {
    kCZCameraBitRateHigh = 0,
    kCZCameraBitRateMedium = 1,
    kCZCameraBitRateLow = 2
} CZCameraBitRate;

typedef enum _CZCameraImageOrientation {
    kCZCameraImageOrientationOriginal = 0,
    kCZCameraImageOrientationFlippedHorizontally = 1,
    kCZCameraImageOrientationFlippedVertically = 2,
    kCZCameraImageOrientationRotated180 = 3
} CZCameraImageOrientation;

typedef enum _CZCameraSnapResolutionPreset {
    kCZCameraSnapResolutionHigh = 0,
    kCZCameraSnapResolutionLow = 1,
    
    kCZCameraSnapResolutionCount
} CZCameraSnapResolutionPreset;

extern NSString * const CZCameraErrorDomain;

extern NSString * const CZCameraStreamingStartNotification;
extern NSString * const CZCameraStreamingStopNotification;

typedef enum _CZCameraErrorCode {
    kCZCameraTimeoutError = 1,
    kCZCameraNoControlError = 2
} CZCameraErrorCode;

@class CZCamera;

@protocol CZCameraDelegate <NSObject>

@optional
- (void)camera:(CZCamera *)camera didGetVideoFrame:(UIImage *)frame;
- (void)camera:(CZCamera *)camera didFinishSnapping:(UIImage *)image exposureTime:(CGFloat)milliseconds gray:(BOOL)isGray error:(NSError *)error;
- (void)camera:(CZCamera *)camera didFinishExposureTimeAutoAdjustWithValue:(CGFloat)milliseconds;
- (void)cameraDidFinishExposureTimeAutoAdjust:(CZCamera *)camera;
- (void)cameraDidFinishWhiteBalanceAutoAdjust:(CZCamera *)camera;
- (void)cameraDidFinishReset:(CZCamera *)camera;

@end

@protocol CZCameraThumbnailDelegate <NSObject>

- (void)camera:(CZCamera *)camera didUpdateThumbnail:(UIImage *)thumbnail;

@end

/*!
 * CZCamera is the abstract model for all kinds of camera objects. It defines
 * the common interfaces for the camera-related requirements in the project,
 * hides the difference among cameras and provides default implementations.
 */
@interface CZCamera : NSObject

@property (atomic, assign) id<CZCameraDelegate> delegate;
@property (atomic, assign) id<CZCameraThumbnailDelegate> thumbnailDelegate;

@property (atomic, copy) NSString *ipAddress;
@property (atomic, copy) NSString *macAddress;

@property (atomic, retain) NSDate *lastActiveTime;

/*! Gets all supported exposure time values in millisecond. */
+ (NSArray *)supportedExposureTimeValues;

/*! Gets the nearest supported exposure time given preferred value. */
+ (CGFloat)nearestValidExposureTime:(CGFloat)milliseconds;

/*! Gets the actual number for a certain bit rate preset. */
+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate;

/*! Determines whether the two objects refer to the same physical camera. */
- (BOOL)isEqual:(CZCamera *)otherCamera;

/*! Gets the firmware version of the camera. */
- (NSString *)firmwareVersion;

/*! Checks whether the firmware version is latest. */
- (BOOL)isFirmwareUpdated;

/*! Updates the firmware to latest built-in version. */
- (void)updateFirmware:(BOOL)upgrade;

/*! Gets the display name of the camera. */
- (NSString *)displayName;

/*! Gets the mock display name which is extracted from MAC address. */
- (NSString *)mockDisplayName;

/*! Changes the display name of the camera. */
- (void)setDisplayName:(NSString *)displayName andUpload:(BOOL)shouldUpload;

/*! Sets the full name of the camera, which consists both PIN and display name. */
- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload;

/*! Gets the maximum supported length for the camera name (excluding PIN). */
- (NSUInteger)maxSupportedNameLength;

/*! Gets the thumbnail of camera. */
- (UIImage *)thumbnail;

/*! Refreshes the thumbnail of camera. */
- (void)beginThumbnailUpdating:(CGSize)thumbnailSize;

/*! Starts playing the video stream from camera. */
- (void)play;

/*! Stops playing the video stream from camera. */
- (void)stop;

/*! Pauses playing the video stream from camera. */
- (void)pause;

/*! Resumes playing the video stream from camera. */
- (void)resume;

/*! Tells whether the video stream playing is paused at the moment. */
- (BOOL)isPaused;

/*! Returns the current snapshot resolution of camera. */
- (CGSize)snapResolution;

/*! Gets the resolution preset for snapping. */
- (CZCameraSnapResolutionPreset)snapResolutionPreset;

/*! Sets the resolution for still image to certain preset. */
- (void)setSnapResolutionPreset:(CZCameraSnapResolutionPreset)preset;

/*! Asynchronously snaps a still image. */
- (void)beginSnapping;

/*! Asynchronously snaps a single frame from live video. */
- (void)beginSnappingFromVideoStream;

/*! Starts recording the video stream from camera to a certain file. */
- (void)beginRecordingToFile:(NSString *)filePath;

/*! Ends recording the video stream. */
- (void)endRecording;

/*! Returns the current video resolution of camera. */
- (CGSize)liveResolution;

/*! Tries to retrieve control privilege from the camera. */
- (BOOL)getControl;

/*! Tries to abandon the current control privilege. */
- (void)releaseControl;

/*! Start handling the event messages sent by camera. */
- (void)beginEventListening;

/*! No more handles the event messages sent by camera. */
- (void)endEventListening;

/*! Gets the current bit rate. */
- (CZCameraBitRate)bitRate;

/*! Sets the bit rate to the chosen mode. */
- (void)setBitRate:(CZCameraBitRate)bitRate;

/*! Gets the current image orientation. */
- (CZCameraImageOrientation)imageOrientation;

/*! Sets the image orientation to a certain mode. */
- (void)setImageOrientation:(CZCameraImageOrientation)orientation;

/*! Gets whether grayscale mode is enabled currently for the camera. */
- (BOOL)isGrayscaleModeEnabled;

/*! Sets whether the camera should be in grayscale mode. */
- (void)setGrayscaleModeEnabled:(BOOL)enabled;

/*! Gets the current exposure mode. */
- (CZCameraExposureMode)exposureMode;

/*! Tells the camera to switch to a certain exposure mode. */
- (void)setExposureMode:(CZCameraExposureMode)exposureMode;

/*! Returns whether the camera supports exposure time auto-adjust. */
- (BOOL)supportsExposureTimeAutoAdjust;

/*! Tells the camera to begin exposure time auto-adjust. */
- (void)beginExposureTimeAutoAdjust;

/*! Gets the current exposure time. */
- (CGFloat)exposureTime;

/*! Sets the exposure time on camera. */
- (void)setExposureTime:(CGFloat)milliseconds;

/*! Gets the current gain. */
- (NSUInteger)gain;

/*! Sets the gain on camera. */
- (void)setGain:(NSUInteger)gain;

/*! Gets the number of possible sharpness step values. */
- (NSUInteger)sharpnessStepCount;

/*! Gets the current sharpness step. */
- (NSUInteger)sharpness;

/*! Sets the sharpness by step on camera. */
- (void)setSharpness:(NSUInteger)sharpness;

/*! Returns whether the camera supports white-balance auto-adjust. */
- (BOOL)supportsWhiteBalanceAutoAdjust;

/*! Tells the camera to begin white-balance auto-adjust. */
- (void)beginWhiteBalanceAutoAdjust;

/*! Returns whether the white-balance setting on camera is currently locked. */
- (BOOL)isWhiteBalanceLocked;

/*! Changes the status of the white-balance lock on camera. */
- (void)setIsWhiteBalanceLocked:(BOOL)locked;

/*! Tells the camera to begin resetting for exposure time, gain, etc. */
- (void)beginReset;

/*! Gets the current security PIN stored on camera. */
- (NSString *)pin;

/*! Checks whether the camera has a PIN. */
- (BOOL)hasPIN;

/*! Checks whether the security PIN matches the one stored on camera. */
- (BOOL)validatePIN:(NSString *)pin;

/*! Changes the security PIN to a new value. */
- (void)setPIN:(NSString *)pin andUpload:(BOOL)shouldUpload;

/*! Tells the camera to restart itself. */
- (void)restart;

/*! Reset the settings on camera to factory default values. */
- (void)factoryReset;

@end
