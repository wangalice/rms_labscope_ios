//
//  CZCameraBrowser.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCameraBrowser.h"
#import "CZCamera.h"
#import "CZWisionCamera.h"
#import "CZBufferProcessor.h"
#import "CZCommonUtils.h"

@interface CZCameraBrowser () {
    dispatch_group_t _browsingGroup;
    dispatch_queue_t _browsingQueue;
    NSObject *_browsingLock;
    NSObject *_cameraListLock;
    NSObject *_wisionDiscoveryLock;
    dispatch_queue_t _wisionDiscoveryQueue;
    GCDAsyncUdpSocket *_browsingSocket;
}

@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, retain) NSMutableDictionary *cameraList;
@property (nonatomic, retain) NSMutableDictionary *obsoleteCameraList;

- (void)handleTimer:(NSTimer *)timer;

- (void)invalidateCameras;
- (void)checkLostCameras;
- (void)browseWisionCamerasAsync;

- (GCDAsyncUdpSocket *)browsingSocket;

- (void)foundCamera:(CZCamera *)camera;

@end

@implementation CZCameraBrowser

static CZCameraBrowser *uniqueInstance = nil;

#pragma mark - Singleton Design Pattern

+ (CZCameraBrowser *)sharedInstance {
    @synchronized([CZCameraBrowser class]) {
        if (uniqueInstance == nil) {
            uniqueInstance = [[super allocWithZone:NULL] init];
        }
        return uniqueInstance;
    }
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedInstance] retain];
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)retain {
    return self;
}

- (oneway void)release {
    // Do nothing when release is called
}

- (id)autorelease {
    return self;
}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

#pragma mark - Lifecycle management

- (id)init {
    self = [super init];
    if (self) {
        _cameraList = [[NSMutableDictionary alloc] init];
        _obsoleteCameraList = [[NSMutableDictionary alloc] init];
        
        _browsingGroup = dispatch_group_create();
        _browsingQueue = dispatch_queue_create("com.zeisscn.browsing", NULL);
        _browsingLock = [[NSObject alloc] init];
        _cameraListLock = [[NSObject alloc] init];
        
        _wisionDiscoveryQueue = dispatch_queue_create("com.zeisscn.wision.discovery", NULL);
        _wisionDiscoveryLock = [[NSObject alloc] init];
        
        [self addObserver:self
               forKeyPath:@"thumbnailRefreshingEnabled"
                  options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                  context:NULL];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self
              forKeyPath:@"thumbnailRefreshingEnabled"
                 context:NULL];
    
    [_timer invalidate];
    [_timer release];
    
    dispatch_release(_wisionDiscoveryQueue);
    dispatch_release(_browsingQueue);
    dispatch_release(_browsingGroup);
    
    [_browsingLock release];
    [_cameraListLock release];
    [_cameraList release];
    [_obsoleteCameraList release];
    [_wisionDiscoveryLock release];
    
    _browsingSocket.delegate = nil;
    [_browsingSocket close];
    [_browsingSocket release];
    
    [super dealloc];
}

#pragma mark - Public interfaces

- (NSArray *)cameras {
    @synchronized (_cameraListLock) {
        return [_cameraList allValues];
    }
}

- (void)beginBrowsing {
    @synchronized (_browsingLock) {
        if (_timer == nil) {
            [self invalidateCameras];
            [self handleTimer:nil];
            
            const NSTimeInterval kBrowsingInterval = 5.0;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:kBrowsingInterval
                                                          target:self
                                                        selector:@selector(handleTimer:)
                                                        userInfo:nil
                                                         repeats:YES];
        }
    }
}

- (void)stopBrowsing {
    @synchronized (_browsingLock) {
        @synchronized (_wisionDiscoveryLock) {
            [_browsingSocket close];
        }

        [_timer invalidate];
        self.timer = nil;
        
        // Wait for up to 5 seconds before invalidate the camera list.
        dispatch_group_wait(_browsingGroup, dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC));
        
        [self invalidateCameras];
    }
}

#pragma mark - Private methods

- (void)handleTimer:(NSTimer *)timer {
    //NSLog(@"Camera discovery timer fired.");
    
    dispatch_group_async(_browsingGroup, _browsingQueue, ^(){
        [self checkLostCameras];
        [self browseWisionCamerasAsync];
    });
}

- (void)invalidateCameras {
    @synchronized (_cameraListLock) {
        NSArray *cameras = [_cameraList allValues];
        for (CZCamera *camera in cameras) {
            [camera setDelegate:nil];
            [camera setThumbnailDelegate:nil];
            [camera stop];
            
            [_obsoleteCameraList setObject:camera forKey:[camera macAddress]];
        }
        
        [_cameraList release];
        _cameraList = [[NSMutableDictionary alloc] init];
    }
}

- (void)checkLostCameras {
    @synchronized (_cameraListLock) {
        // This is a time interval compares with "now", so it's negative.
        const NSTimeInterval kLostTime = -15.0;
        
        NSMutableArray *obsoleteCamerasToRemove = [[NSMutableArray alloc] init];
        NSMutableArray *camerasToRemove = [[NSMutableArray alloc] init];
        
        // Drain autorelease pool for the two NSArray created by "allValues",
        // or the camera objects will be retained longer than expected.
        @autoreleasepool {
            // Check obsolete cameras which was invalidated.
            NSArray *obsoleteCameras = [_obsoleteCameraList allValues];
            for (CZCamera *camera in obsoleteCameras) {
                NSTimeInterval timeInterval = [[camera lastActiveTime] timeIntervalSinceNow];
                if (timeInterval <= kLostTime) {
                    [obsoleteCamerasToRemove addObject:camera];
                }
            }
            
            // Remove lost ones from the obsolete camera list.
            for (CZCamera *camera in obsoleteCamerasToRemove) {
                [_obsoleteCameraList removeObjectForKey:[camera macAddress]];
            }
            
            // Check living cameras.
            NSArray *cameras = [_cameraList allValues];
            for (CZCamera *camera in cameras) {
                NSTimeInterval timeInterval = [[camera lastActiveTime] timeIntervalSinceNow];
                if (timeInterval <= kLostTime) {
                    [camerasToRemove addObject:camera];
                }
            }
            
            // Remove lost cameras from the living camera list.
            for (CZCamera *camera in camerasToRemove) {
                [_cameraList removeObjectForKey:[camera macAddress]];
            }
        }
        
        if ([obsoleteCamerasToRemove count] > 0) {
            if ([_delegate respondsToSelector:@selector(cameraBrowserDidLoseCameras:)]) {
                dispatch_async(dispatch_get_main_queue(), ^() {
                    [_delegate cameraBrowserDidLoseCameras:obsoleteCamerasToRemove];
                });
            }
        }
        
        if ([camerasToRemove count] > 0) {
            if ([_delegate respondsToSelector:@selector(cameraBrowserDidLoseCameras:)]) {
                dispatch_async(dispatch_get_main_queue(), ^() {
                    [_delegate cameraBrowserDidLoseCameras:camerasToRemove];
                });
            }
        }
        
        [obsoleteCamerasToRemove release];
        [camerasToRemove release];
    }
}

- (void)browseWisionCamerasAsync {
    const uint16_t kWisionDiscoveryPort = 8995;
    const uint16_t kWisionListenPort = 8996;
    const NSTimeInterval kWisionDiscoveryTimeout = 5;
    const size_t kWisionPacketLength = 544;
    
    @synchronized (_wisionDiscoveryLock) {
        GCDAsyncUdpSocket *socket = [self browsingSocket];
        
        do {
            NSError *error = nil;
            
            if (![socket enableBroadcast:YES error:&error]) {
                NSLog(@"Wision discovery: failed to enable broadcast for the socket.");
                break;
            }
            
            if (![socket beginReceiving:&error]) {
                NSLog(@"Wision discovery: failed to begin receiving data from socket.");
                break;
            }
            
            time_t now = time(NULL);
            struct tm *timeNow = localtime(&now);
            
            void *dataBuffer = malloc(kWisionPacketLength);
            memset(dataBuffer, 0, kWisionPacketLength);
            
            CZBufferProcessor *writer = [[CZBufferProcessor alloc] initWithBuffer:dataBuffer
                                                                           length:kWisionPacketLength];
            [writer writeShort:0xAA55];                             // magic_number
            [writer writeShortInNetworkOrder:0];                    // type
            [writer writeShortInNetworkOrder:kWisionListenPort];    // port
            [writer writeShort:0];                                  // device_code
            [writer writeLongInNetworkOrder:0];                     // serial_num
            [writer writeLong:0];                                   // eventcd
            [writer writeLong:0];                                   // arg, 0 or 3600
            [writer writeLong:0];                                   // reserved, 0
            [writer writeShort:(1900 + timeNow->tm_year)];          // year, 0-2099
            [writer writeByte:(timeNow->tm_mon + 1)];               // mon, 1-12
            [writer writeByte:timeNow->tm_mday];                    // day, 1-31
            [writer writeByte:timeNow->tm_hour];                    // hr, 0-23
            [writer writeByte:timeNow->tm_min];                     // min, 0-59
            [writer writeByte:timeNow->tm_sec];                     // sec, 0-59
            [writer writeByte:0xFF];                                // tz
            [writer release];
            
            NSData *packetData = [[NSData alloc] initWithBytes:dataBuffer
                                                        length:kWisionPacketLength];
            
            [socket sendData:packetData
                      toHost:@"255.255.255.255" // Broadcast
                        port:kWisionDiscoveryPort
                 withTimeout:kWisionDiscoveryTimeout
                         tag:0];
            
            [packetData release];
            free(dataBuffer);
        } while (0);
    }
}

- (GCDAsyncUdpSocket *)browsingSocket {
    if (!_browsingSocket) {
        GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                                  delegateQueue:_wisionDiscoveryQueue];
        [socket setIPv4Enabled:YES];
        [socket setIPv6Enabled:NO];
        _browsingSocket = socket;
    }
    
    return _browsingSocket;
}

- (void)foundCamera:(CZCamera *)camera {
    @synchronized (_cameraListLock) {
        [_obsoleteCameraList removeObjectForKey:[camera macAddress]];
        [_cameraList setObject:camera forKey:[camera macAddress]];
    }
}

#pragma mark - GCDAsyncUdpSocketDelegate methods

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext {
    CZBufferProcessor *reader = [[CZBufferProcessor alloc] initWithBuffer:data.bytes
                                                                   length:data.length];
    [reader seekToOffset:32];
    
    @autoreleasepool {
        NSString *response = [reader readStringWithLength:(data.length - 32)];
        NSArray *fields = [response componentsSeparatedByString:@";"];
        
        // The format of the response:
        // [0] Name
        //     (Factory default is "IPCam", when error happens is "(null)".
        //      These two names should be ignored.)
        // [1] Model
        // [2] ID
        // [3] IP address
        // [4] Mask
        // [5] Gateway
        // [6] HTTP port
        // [7] MAC address
        // ...
        
        if ([fields count] > 7) {
            @synchronized (_cameraListLock) {
                NSString *macAddress = fields[7];
                NSString *name = fields[0];
                
                CZWisionCamera *camera = [_cameraList objectForKey:fields[7]];
                if (camera == nil) {
                    camera = [[CZWisionCamera alloc] init];
                    
                    [camera setIpAddress:fields[3]];
                    [camera setMacAddress:macAddress];
                    
                    if (name != nil &&
                        ![name isEqualToString:@"IPCam"] &&
                        ![name isEqualToString:@"(null)"]) {
                        [camera setName:name andUpload:NO];
                    }
                    
                    [self foundCamera:camera];
                    
                    if ([_delegate respondsToSelector:@selector(cameraBrowserDidFindCameras:)]) {
                        dispatch_async(dispatch_get_main_queue(), ^() {
                            [_delegate cameraBrowserDidFindCameras:[NSArray arrayWithObject:camera]];
                        });
                    }
                    
                    [camera release];
                } else {
                    // Update name for PIN refreshing.
                    if (name != nil &&
                        ![name isEqualToString:@"IPCam"] &&
                        ![name isEqualToString:@"(null)"]) {
                        [camera setName:name andUpload:NO];
                        
                        if ([_delegate respondsToSelector:@selector(cameraBrowserDidGetUpdatedDataFromCameras:)]) {
                            dispatch_async(dispatch_get_main_queue(), ^() {
                                [_delegate cameraBrowserDidGetUpdatedDataFromCameras:[NSArray arrayWithObject:camera]];
                            });
                        }
                    }
                    
                    // Update IP address.
                    [camera setIpAddress:fields[3]];
                    
                    NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
                    [camera setLastActiveTime:now];
                    [now release];
                }
            }
        }
    }
    
    [reader release];
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    @synchronized (_wisionDiscoveryLock) {
        if (_browsingSocket == sock) {
            [_browsingSocket release];
            _browsingSocket = nil;
        }
    }
}

@end
