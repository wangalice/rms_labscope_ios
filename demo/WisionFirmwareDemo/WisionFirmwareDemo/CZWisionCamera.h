//
//  CZWisionCamera.h
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CZCamera.h"

/*!
 * Represents a Wision-manufactured local IP camera.
 */
@interface CZWisionCamera : CZCamera

@end
