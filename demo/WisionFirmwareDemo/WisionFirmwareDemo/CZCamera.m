//
//  CZCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/4/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZCamera.h"
#import "CZCommonUtils.h"

NSString * const CZCameraErrorDomain = @"CameraSnappingError";

NSString * const CZCameraStreamingStartNotification = @"CameraStreamingStartNotification";
NSString * const CZCameraStreamingStopNotification = @"CameraStreamingStopNotification";

@interface CZCamera () {
    NSString *_displayName;
    NSString *_pin;
    NSUInteger _controlRetainCount;
    NSObject *_controlRetainCountLock;
    NSObject *_localCameraSettingsLock;
    NSObject *_nameLock;
}

+ (NSArray *)emptyArray;

- (void)updateName;
- (NSString *)mockDisplayName;

/*!
 * Do the actual work to get/abandon control, should be overwritten by
 * subclasses to implement the real logic.
 **/
- (BOOL)startControl;
- (void)endControl;

- (id)readLocalCameraSettingForKey:(NSString *)key;
- (void)writeLocalCameraSetting:(id)setting forKey:(NSString *)key;

@end

@implementation CZCamera

- (id)init {
    self = [super init];
    if (self) {
        _lastActiveTime = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
        _controlRetainCount = 0;
        _controlRetainCountLock = [[NSObject alloc] init];
        _localCameraSettingsLock = [[NSObject alloc] init];
        _nameLock = [[NSObject alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_displayName release];
    [_pin release];
    [_ipAddress release];
    [_macAddress release];
    [_lastActiveTime release];
    [_controlRetainCountLock release];
    [_localCameraSettingsLock release];
    [_nameLock release];
    
    [super dealloc];
}

#pragma mark - Default implementations

+ (NSArray *)supportedExposureTimeValues {
    return [CZCamera emptyArray];
}

+ (CGFloat)nearestValidExposureTime:(CGFloat)milliseconds {
    return milliseconds;
}

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    return 0.0;
}

- (BOOL)isEqual:(CZCamera *)otherCamera {
    if (self.macAddress || self.macAddress.length <= 0 ||
        otherCamera.macAddress || otherCamera.macAddress.length <= 0) {
        return NO;
    }
    return [self.macAddress isEqualToString:otherCamera.macAddress];
}

- (NSString *)firmwareVersion {
    return @"";
}

- (BOOL)isFirmwareUpdated {
    return YES;
}

- (void)updateFirmware:(BOOL)upgrade {
}

- (NSString *)displayName {
    @synchronized (_nameLock) {
        if (_displayName && _displayName.length > 0) {
            return _displayName;
        } else {
            return [self mockDisplayName];
        }
    }
}

- (NSString *)mockDisplayName {
    // Get last 3 segments of MAC address as display name.
    return [self.macAddress substringFromIndex:9];
}

- (void)setDisplayName:(NSString *)displayName andUpload:(BOOL)shouldUpload {
    if (displayName == nil || [displayName isEqualToString:[self mockDisplayName]]) {
        return;
    }
    
    @synchronized (_nameLock) {
        [_displayName release];
        _displayName = [displayName copy];
        
        if (shouldUpload) {
            [self updateName];
        }
    }
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    @synchronized (_nameLock) {
        NSString *newName = [name copy];
        [_pin release];
        _pin = nil;
        
        [_displayName release];
        _displayName = nil;
        
        // Scan for all the decimal digit characters as the PIN
        NSString *pinPart = nil;
        NSScanner *scanner = [[NSScanner alloc] initWithString:newName];
        BOOL hasPin = [scanner scanCharactersFromSet:[NSCharacterSet decimalDigitCharacterSet] intoString:&pinPart];
        if (hasPin) {
            _pin = [pinPart copy];
        }
        
        // The left part is the actual display name
        _displayName = [[newName substringFromIndex:[scanner scanLocation]] copy];
        
        [scanner release];
        [newName release];
    }
}

- (NSUInteger)maxSupportedNameLength {
    return 12;
}

- (UIImage *)thumbnail {
    return nil;
}

- (void)beginThumbnailUpdating:(CGSize)thumbnailSize {
}

- (void)play {
}

- (void)stop {
}

- (void)pause {
}

- (void)resume {
}

- (BOOL)isPaused {
    return NO;
}

- (CGSize)snapResolution {
    return CGSizeZero;
}

- (CZCameraSnapResolutionPreset)snapResolutionPreset {
    NSNumber *preset = [self readLocalCameraSettingForKey:@"snap_resolution_preset"];
    if (preset) {
        NSInteger presetValue = [preset integerValue];
        if (presetValue >= 0 && presetValue < kCZCameraSnapResolutionCount) {
            return (CZCameraSnapResolutionPreset)presetValue;
        }
    }
    return kCZCameraSnapResolutionHigh;
}

- (void)setSnapResolutionPreset:(CZCameraSnapResolutionPreset)preset {
    [self writeLocalCameraSetting:[NSNumber numberWithInteger:preset]
                           forKey:@"snap_resolution_preset"];
}

- (void)beginSnapping {
    [self beginSnappingFromVideoStream];
}

- (void)beginSnappingFromVideoStream {
    if ([self.delegate respondsToSelector:@selector(camera:didFinishSnapping:exposureTime:gray:error:)]) {
        [self.delegate camera:self didFinishSnapping:nil exposureTime:0.0 gray:NO error:nil];
    }
}

- (void)beginRecordingToFile:(NSString *)filePath {
}

- (void)endRecording {
}

- (CGSize)liveResolution {
    return CGSizeZero;
}

- (BOOL)getControl {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount == 0) {
            if ([self startControl]) {
                _controlRetainCount = 1;
                return YES;
            } else {
                return NO;
            }
        }
        
        ++_controlRetainCount;
        return YES;
    }
}

- (void)releaseControl {
    @synchronized (_controlRetainCountLock) {
        if (_controlRetainCount > 0) {
            --_controlRetainCount;
        }
        
        if (_controlRetainCount == 0) {
            [self endControl];
        }
    }
}

- (void)beginEventListening {
}

- (void)endEventListening {
}

- (CZCameraBitRate)bitRate {
    return kCZCameraBitRateMedium;
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
}

- (CZCameraImageOrientation)imageOrientation {
    return kCZCameraImageOrientationOriginal;
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
}

- (BOOL)isGrayscaleModeEnabled {
    return NO;
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
}

- (CZCameraExposureMode)exposureMode {
    return kCZCameraExposureAutomatic;
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
}

- (BOOL)supportsExposureTimeAutoAdjust {
    return NO;
}

- (void)beginExposureTimeAutoAdjust {
    if ([self.delegate respondsToSelector:@selector(camera:didFinishExposureTimeAutoAdjustWithValue:)]) {
        [self.delegate camera:self didFinishExposureTimeAutoAdjustWithValue:0.0];
    }
}

- (CGFloat)exposureTime {
    return 0.0f;
}

- (void)setExposureTime:(CGFloat)milliseconds {
}

- (NSUInteger)gain {
    return 0;
}

- (void)setGain:(NSUInteger)gain {
}

- (NSUInteger)sharpnessStepCount {
    return 0;
}

- (NSUInteger)sharpness {
    return 0;
}

- (void)setSharpness:(NSUInteger)sharpness {
}

- (BOOL)supportsWhiteBalanceAutoAdjust {
    return NO;
}

- (void)beginWhiteBalanceAutoAdjust {
    if ([self.delegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust:)]) {
        [self.delegate cameraDidFinishWhiteBalanceAutoAdjust:self];
    }
}

- (BOOL)isWhiteBalanceLocked {
    return NO;
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
}

- (void)beginReset {
    if ([self.delegate respondsToSelector:@selector(cameraDidFinishReset:)]) {
        [self.delegate cameraDidFinishReset:self];
    }
}

- (NSString *)pin {
    @synchronized (_nameLock) {
        return _pin;
    }
}

- (BOOL)hasPIN {
    @synchronized (_nameLock) {
        if (_pin && _pin.length > 0) {
            return YES;
        }
        return NO;
    }
}

- (BOOL)validatePIN:(NSString *)pin {
    @synchronized (_nameLock) {
        if (_pin == nil) {
            if (pin == nil || pin.length == 0) {
                return YES;
            } else {
                return NO;
            }
        } else {
            BOOL result = [_pin isEqualToString:pin];
            
            // Implement fallback for PIN here.
            if (!result) {
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit
                                                           fromDate:[NSDate date]];
                
                NSString *masterText = [[NSString alloc] initWithFormat:@"Hermes-Master-PIN-%04i-%02i",
                                        components.year,
                                        components.month];
                NSString *masterPin = [[CZCommonUtils md5FromString:masterText] substringToIndex:12];
                
                result = [[pin lowercaseString] isEqualToString:[masterPin lowercaseString]];
                
                [masterText release];
                [calendar release];
            }
            
            return result;
        }
    }
}

- (void)setPIN:(NSString *)pin andUpload:(BOOL)shouldUpload {
    @synchronized (_nameLock) {
        [_pin release];
        _pin = [pin copy];
        
        if (shouldUpload) {
            [self updateName];
        }
    }
}

- (void)restart {
}

- (void)factoryReset {
}

#pragma mark - Private methods

+ (NSArray *)emptyArray {
    static NSArray *emptyArray = nil;
    if (emptyArray == nil) {
        emptyArray = [NSArray array];
    }
    return emptyArray;
}

- (void)updateName {
    @synchronized (_nameLock) {
        if (_pin == nil || _pin.length == 0) {
            [self setName:_displayName andUpload:YES];
        } else {
            NSString *name = [[NSString alloc] initWithFormat:@"%@%@", _pin, _displayName];
            [self setName:name andUpload:YES];
            [name release];
        }
    }
}

- (BOOL)startControl {
    return YES;
}

- (void)endControl {
}

- (id)readLocalCameraSettingForKey:(NSString *)key {
    @synchronized (_localCameraSettingsLock) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *allCameraSettings = [defaults valueForKey:@"local_camera_settings"];
        NSDictionary *cameraSettings = allCameraSettings[self.macAddress];
        return cameraSettings[key];
    }
}

- (void)writeLocalCameraSetting:(id)setting forKey:(NSString *)key {
    @synchronized (_localCameraSettingsLock) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *allCameraSettings = [[defaults valueForKey:@"local_camera_settings"] mutableCopy];
        NSMutableDictionary *cameraSettings = [allCameraSettings[self.macAddress] mutableCopy];
        
        if (allCameraSettings == nil) {
            allCameraSettings = [[NSMutableDictionary alloc] init];
        }
        if (cameraSettings == nil) {
            cameraSettings = [[NSMutableDictionary alloc] init];
        }
        
        cameraSettings[key] = setting;
        allCameraSettings[self.macAddress] = cameraSettings;
        [defaults setValue:allCameraSettings forKey:@"local_camera_settings"];
        
        [cameraSettings release];
        [allCameraSettings release];
    }
}

@end
