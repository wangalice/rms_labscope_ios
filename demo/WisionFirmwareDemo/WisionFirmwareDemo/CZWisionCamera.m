//
//  CZWisionCamera.m
//  Hermes
//
//  Created by Halley Gu on 3/21/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "CZWisionCamera.h"
#import "AFHTTPClient.h"
#import "AFImageRequestOperation.h"
#import "CZCommonUtils.h"

const NSTimeInterval kWisionDefaultTimeout = 20;
const CGFloat kWisionExposureValues[] = {
    0.02, 0.05, 0.1, 0.2, 0.5, 1.0, 2.0, 4.0, 5.0, 8.0,
    8.33333, 10, 16.6666, 20, 25, 30.30303, 33.33333, 40, 50, 66.66666,
    76.923, 100.0, 200.0 /*, 500.0, 1000.0, 2000.0 */ // TODO: Big exposure times disabled temporarily.
};
const NSUInteger kWisionExposureValuesCount = sizeof(kWisionExposureValues) / sizeof(kWisionExposureValues[0]);
const NSUInteger kWisionGainLevels[] = {
    80, 269, 457, 646, 834, 1023
};
const NSUInteger kWisionGainLevelsCount = sizeof(kWisionGainLevels) / sizeof(kWisionGainLevels[0]);

@interface CZWisionCamera ()

@property (atomic, copy) NSString *version;

- (NSString *)getParameterFromIniHtm:(NSString *)param;
- (NSString *)getParameterFromVbHtm:(NSString *)param;
- (void)setParameters:(NSDictionary *)params;

+ (NSString *)urlEncodedStringFromObject:(id)object;
+ (NSString *)urlEncodedStringWithParameters:(NSDictionary *)params;
+ (NSUInteger)nearestValidExposureTimeIndex:(CGFloat)milliseconds;
+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain;

@end

@implementation CZWisionCamera

- (void)dealloc {
    [_version release];
    
    [super dealloc];
}

#pragma mark - Overridden methods

+ (NSArray *)supportedExposureTimeValues {
    static NSMutableArray *array = nil;
    if (array == nil) {
        array = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i < kWisionExposureValuesCount; ++i) {
            NSNumber *number = [[NSNumber alloc] initWithFloat:kWisionExposureValues[i]];
            [array addObject:number];
            [number release];
        }
    }
    return array;
}

+ (CGFloat)nearestValidExposureTime:(CGFloat)milliseconds {
    NSUInteger index = [CZWisionCamera nearestValidExposureTimeIndex:milliseconds];
    return kWisionExposureValues[index];
}

+ (CGFloat)bitRateValueForLevel:(CZCameraBitRate)bitRate {
    CGFloat bitRateValue = 0.0;
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateValue = 6.0;
            break;
            
        case kCZCameraBitRateLow:
            bitRateValue = 1.5;
            break;
            
        case kCZCameraBitRateMedium:
        default:
            bitRateValue = 3.0;
            break;
    }
    return bitRateValue;
}

- (NSString *)firmwareVersion {
    if (self.version == nil) {
        NSString *version = [self getParameter:@"version" fromPath:@"ipcam_id.htm"];
        if ([version hasSuffix:@"\";"]) {
            version = [version stringByReplacingOccurrencesOfString:@"\";" withString:@""];
        }
        if (version.length > 0) {
            self.version = version;
        }
    }
    return self.version;
}

- (BOOL)isFirmwareUpdated {
    NSString *firmwareVersion = [self firmwareVersion];
    if (firmwareVersion.length > 0) {
        NSRange zeissRange = [firmwareVersion rangeOfString:@"Zeiss"];
        if (zeissRange.location != NSNotFound && zeissRange.location > 0) {
            NSString *subVersion = [[firmwareVersion substringWithRange:NSMakeRange(zeissRange.location - 1, 1)] uppercaseString];
            unichar subVersionLetter = [subVersion characterAtIndex:0];
            if (subVersionLetter < 'J') {
                return NO;
            }
        }
    }
    return YES;
}

- (void)updateFirmware:(BOOL)upgrade {
    NSString *filePath = nil;
    if (upgrade) {
        filePath = [[NSBundle mainBundle] pathForResource:@"cramfsImage_ipnc_DM368_S1" ofType:@""];
    } else {
        filePath = [[NSBundle mainBundle] pathForResource:@"cramfsImage_ipnc_DM368_S1_old" ofType:@""];
    }
    
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    NSLog(@"Firmware binary length = %u", fileData.length);
    if (fileData) {
        NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://%@:8080/", self.ipAddress];
        NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        NSString *cookie = [NSString stringWithFormat:@"zyxcookie=IP=%@$port=8080$; items=0,%@,8080,admin,9999,0,0,%@,0",
                            self.ipAddress, self.ipAddress, self.version];
        self.version = nil;
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
        [httpClient setAuthorizationHeaderWithUsername:@"admin" password:@"9999"];
        [httpClient setDefaultHeader:@"Cookie" value:cookie];
        httpClient.parameterEncoding = AFFormURLParameterEncoding;
        
        void (^formDataBlock)(id<AFMultipartFormData> formData) = ^(id<AFMultipartFormData> formData) {
            [formData setBoundary:@"---------------------------7de8a2d60602"];
            
            [formData appendPartWithFileData:fileData
                                        name:@"upfile_tx"
                                    fileName:@"cramfsImage_ipnc_DM368_S1"
                                    mimeType:@"application/octet-stream"];
            
            // "软件升级" -_-b
            char special[12] = {0xe8, 0xbd, 0xaf, 0xe4, 0xbb, 0xb6, 0xe5, 0x8d, 0x87, 0xe7, 0xba, 0xa7};
            NSData *specialData = [NSData dataWithBytes:special length:12];
            [formData appendPartWithFormData:specialData name:@"upSoft"]; // "submit"
        };
        
        NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
                                                                             path:@"update.cgi"
                                                                       parameters:nil
                                                        constructingBodyWithBlock:formDataBlock];
        [request setTimeoutInterval:300];
        
        NSLog(@"Sending firmware to the camera:\n\n%@", request.allHTTPHeaderFields);
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error) {
            NSLog(@"Failed to send the request to update firmware of Wision camera: %@", [error localizedDescription]);
        } else {
            NSMutableData *dataWithoutZero = [[NSMutableData alloc] init];
            const unsigned char *rawData = data.bytes;
            for (NSUInteger i = 0; i < data.length; ++i) {
                if (rawData[i] > 0) {
                    [dataWithoutZero appendBytes:(rawData + i) length:1];
                }
            }
            
            NSMutableString *body = [[NSMutableString alloc] initWithData:dataWithoutZero encoding:NSUTF8StringEncoding];
            if (body == nil) {
                body = [[NSMutableString alloc] initWithData:dataWithoutZero encoding:NSASCIIStringEncoding];
            }
            
            if (body) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                NSLog(@"Got response from Wision camera:\n\n%@\n\n%@", httpResponse.allHeaderFields, body);
                [body release];
                
                // TODO: Error handling.
            }
            
            [dataWithoutZero release];
        }
        
        [httpClient release];
        [baseUrl release];
        [baseUrlString release];
    }
}

- (void)setName:(NSString *)name andUpload:(BOOL)shouldUpload {
    [super setName:name andUpload:shouldUpload];
    
    if (shouldUpload && name != nil) {
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                name, @"title",
                                nil];
        [self setParameters:params];
        [params release];
    }
}

- (NSUInteger)maxSupportedNameLength {
    NSString *firmwareVersion = [self firmwareVersion];
    if (firmwareVersion.length > 0) {
        NSRange zeissRange = [firmwareVersion rangeOfString:@"Zeiss"];
        if (zeissRange.location != NSNotFound && zeissRange.location > 0) {
            NSString *subVersion = [[firmwareVersion substringWithRange:NSMakeRange(zeissRange.location - 1, 1)] uppercaseString];
            unichar subVersionLetter = [subVersion characterAtIndex:0];
            if (subVersionLetter > 'H' && subVersionLetter <= 'Z') {
                return 12;
            }
        }
    }
    return 7;
}

- (CGSize)snapResolution {
    return CGSizeMake(2048, 1536);
}

- (CGSize)liveResolution {
    return CGSizeMake(640, 480);
}

- (CZCameraBitRate)bitRate {
    NSString *bitRateString = [self getParameterFromIniHtm:@"bitrate2"];
    
    if (!bitRateString) {
        return [super bitRate]; // Fallback to default implementation
    }
    
    NSInteger bitRateInMegabytes = [bitRateString integerValue];
    if (bitRateInMegabytes >= 4608) { // >= 4.5 Mbps
        return kCZCameraBitRateHigh;
    } else if (bitRateInMegabytes < 2048) { // < 2 Mbps
        return kCZCameraBitRateLow;
    }
    return kCZCameraBitRateMedium;
}

- (void)setBitRate:(CZCameraBitRate)bitRate {
    if (bitRate == [self bitRate]) {
        return;
    }
    
    NSInteger bitRateInMegabytes = 3072; // 3 Mbps
    switch (bitRate) {
        case kCZCameraBitRateHigh:
            bitRateInMegabytes = 6144; // 6 Mbps
            break;
            
        case kCZCameraBitRateLow:
            bitRateInMegabytes = 1536; // 1.5 Mbps
            break;
            
        default:
            break;
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    NSString *value = [[NSString alloc] initWithFormat:@"%d", bitRateInMegabytes];
    [params setObject:value forKey:@"bitrate2"];
    [value release];
    
    [self setParameters:params];
    [params release];
}

- (CZCameraImageOrientation)imageOrientation {
    NSString *orientationString = [self getParameterFromIniHtm:@"mirctrl"];
    
    if (!orientationString) {
        return [super imageOrientation]; // Fallback to default implementation
    }
    
    return [orientationString integerValue];
}

- (void)setImageOrientation:(CZCameraImageOrientation)orientation {
    NSString *value = [[NSString alloc] initWithFormat:@"%u", orientation];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"mirctrl", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (BOOL)isGrayscaleModeEnabled {
    NSString *grayscaleString = [self getParameterFromIniHtm:@"irdaynigmode"];
    if (!grayscaleString) {
        return [super isGrayscaleModeEnabled]; // Fallback to default implementation
    }
    NSInteger grayscaleValue = [grayscaleString integerValue];
    return (grayscaleValue == 1);
}

- (void)setGrayscaleModeEnabled:(BOOL)enabled {
    NSString *value = enabled ? @"1" : @"0";
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"setirdaynight", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (CZCameraExposureMode)exposureMode {
    NSString *modeString = [self getParameterFromIniHtm:@"exposuremod"];
    if (!modeString) {
        return [super exposureMode]; // Fallback to default implementation
    }
    NSInteger modeValue = [modeString integerValue];
    return (modeValue == 1) ? kCZCameraExposureManual : kCZCameraExposureAutomatic;
}

- (void)setExposureMode:(CZCameraExposureMode)exposureMode {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    NSString *value = [[NSString alloc] initWithFormat:@"%d", exposureMode];
    [params setObject:value forKey:@"setexpmod"];
    [value release];
    
    [self setParameters:params];
    [params release];
}

- (BOOL)supportsExposureTimeAutoAdjust {
    return YES;
}

- (void)beginExposureTimeAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self setExposureMode:kCZCameraExposureAutomatic];
        sleep(3); // Wait for the camera to finish auto exposure-time adjust.
        [self setExposureTime:[self exposureTime]];
        [self setExposureMode:kCZCameraExposureManual];
        
        if ([self.delegate respondsToSelector:@selector(cameraDidFinishExposureTimeAutoAdjust:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                [self.delegate cameraDidFinishExposureTimeAutoAdjust:self];
            });
        }
    });
}

- (CGFloat)exposureTime {
    CZCameraExposureMode exposureMode = [self exposureMode];
    if (exposureMode == kCZCameraExposureAutomatic) {
        NSString *exposureString = [self getParameterFromVbHtm:@"getexptime"];
        if (exposureString) {
            return [exposureString floatValue] / 1000.0;
        }
    } else {
        NSString *exposureString = [self getParameterFromIniHtm:@"fixedexposure"];
        if (exposureString) {
            NSInteger exposureIndex = [exposureString integerValue] - 1;
            if (exposureIndex < 0) {
                exposureIndex = 0;
            }
            if (exposureIndex >= kWisionExposureValuesCount) {
                exposureIndex = kWisionExposureValuesCount - 1;
            }
            return kWisionExposureValues[exposureIndex];
        }
    }
    
    return [super exposureTime]; // Fallback to default implementation
}

- (void)setExposureTime:(CGFloat)milliseconds {
    NSUInteger index = [CZWisionCamera nearestValidExposureTimeIndex:milliseconds];
    NSString *value = [[NSString alloc] initWithFormat:@"%u", index + 1];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"fixedexptime",
                            @"1", @"setexpmod",
                            nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (NSUInteger)gain {
    NSString *gainString = [self getParameterFromIniHtm:@"maxgain"];
    if (!gainString) {
        return [super gain]; // Fallback to default implementation
    }
    NSInteger gain = [gainString integerValue];
    NSUInteger gainLevel = [CZWisionCamera nearestValidGainLevelIndex:gain];
    return gainLevel;
}

- (void)setGain:(NSUInteger)gain {
    if (gain >= kWisionGainLevelsCount) {
        return;
    }
    
    NSString *value = [[NSString alloc] initWithFormat:@"%u", kWisionGainLevels[gain]];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"maxgain", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (NSUInteger)sharpnessStepCount {
    return 10; // 0-9
}

- (NSUInteger)sharpness {
    NSString *sharpnessString = [self getParameterFromIniHtm:@"sharpness"];
    if (!sharpnessString) {
        return [super sharpness];
    }
    
    NSInteger sharpness = [sharpnessString integerValue];
    NSInteger sharpnessStep = roundf(sharpness / 25.5);
    NSUInteger maxStep = [self sharpnessStepCount] - 1;
    
    return MIN(maxStep, MAX(0, sharpnessStep));
}

- (void)setSharpness:(NSUInteger)sharpness {
    if (sharpness >= [self sharpnessStepCount]) {
        return;
    }
    
    sharpness = roundf(25.5 * sharpness); // Convert steps to real value in range [0, 255]
    NSString *value = [[NSString alloc] initWithFormat:@"%u", sharpness];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"sharpness", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (BOOL)supportsWhiteBalanceAutoAdjust {
    return YES;
}

- (void)beginWhiteBalanceAutoAdjust {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        [self setIsWhiteBalanceLocked:NO];
        sleep(3); // Wait for the camera to finish auto white-balance.
        [self setIsWhiteBalanceLocked:YES];
        
        if ([self.delegate respondsToSelector:@selector(cameraDidFinishWhiteBalanceAutoAdjust:)]) {
            dispatch_async(dispatch_get_main_queue(), ^() {
                [self.delegate cameraDidFinishWhiteBalanceAutoAdjust:self];
            });
        }
    });
}

- (BOOL)isWhiteBalanceLocked {
    NSString *statusString = [self getParameterFromIniHtm:@"getWBLock"];
    if (!statusString) {
        return [super isWhiteBalanceLocked]; // Fallback to default implementation
    }
    NSInteger status = [statusString integerValue];
    return (status == 1);
}

- (void)setIsWhiteBalanceLocked:(BOOL)locked {
    NSString *value = [[NSString alloc] initWithFormat:@"%u", (locked ? 1 : 0)];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            value, @"lockwb", nil];
    [self setParameters:params];
    [value release];
    [params release];
}

- (void)restart {
    [self setParameters:@{@"ipcamrestartcmd": [NSNull null]}];
}

- (void)factoryReset {
    [self setParameters:@{@"paradefaultcmd": [NSNull null]}];
}

- (NSURL *)rtspUrl {
    NSString *urlString = [[NSString alloc] initWithFormat:@"rtsp://admin:9999@%@/PSIA/Streaming/channels/h264cif", self.ipAddress];
    NSURL *url = [NSURL URLWithString:urlString];
    [urlString release];
    return url;
}

#pragma mark - Private methods

- (NSString *)getParameter:(NSString *)param fromPath:(NSString *)urlPath {
    NSString *result = nil;
    
    if (param == nil || [param length] == 0) {
        return result;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/", self.ipAddress];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setTimeoutInterval:10]; // Shorter timeout for GET requests
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error) {
        NSLog(@"Failed to send the request to get parameters from Wision camera: %@", [error localizedDescription]);
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (body == nil) {
            body = [[NSMutableString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        }
        
        if (body) {
            [body replaceOccurrencesOfString:@"<br>" withString:@"\n" options:0 range:NSMakeRange(0, [body length])];
            
            NSString *searchPattern = [[NSString alloc] initWithFormat:@"%@=(.*)", param];
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:searchPattern options:0 error:NULL];
            NSTextCheckingResult *match = [regex firstMatchInString:body options:0 range:NSMakeRange(0, [body length])];
            
            result = [body substringWithRange:[match rangeAtIndex:1]];
            
            NSLog(@"Got parameter from %@ of Wision camera: %@ = %@", urlPath, param, result);
            
            [searchPattern release];
            [body release];
        }
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return result;
}

- (NSString *)getParameterFromIniHtm:(NSString *)param {
    NSString *result = nil;
    
    if (param == nil || [param length] == 0) {
        return result;
    }
    
    NSString *baseUrlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/", self.ipAddress];
    NSURL *baseUrl = [[NSURL alloc] initWithString:baseUrlString];
    NSString *urlPath = @"ini.htm";
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseUrl];
    httpClient.parameterEncoding = AFFormURLParameterEncoding;
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:urlPath parameters:nil];
    [request setTimeoutInterval:10]; // Shorter timeout for GET requests
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error) {
        NSLog(@"Failed to send the request to get parameters from Wision camera: %@", [error localizedDescription]);
    } else {
        NSMutableString *body = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (body == nil) {
            body = [[NSMutableString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        }
        
        if (body) {
            NSArray *lines = [body componentsSeparatedByString:@"<br>"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            for (NSString *line in lines) {
                NSArray *components = [line componentsSeparatedByString:@"="];
                if (components.count == 2) {
                    dict[components[0]] = components[1];
                }
            }
            
            result = dict[param];
            
            NSLog(@"Got parameter from %@ of Wision camera: %@ = %@", urlPath, param, result);
            
            [dict release];
            [body release];
        }
    }
    
    [httpClient release];
    [baseUrl release];
    [baseUrlString release];
    
    return result;
}

- (NSString *)getParameterFromVbHtm:(NSString *)param {
    NSString *urlPath = [NSString stringWithFormat:@"vb.htm?%@", param];
    return [self getParameter:param fromPath:urlPath];
}

- (void)setParameters:(NSDictionary *)params {
    if (params == nil || [params count] == 0) {
        return;
    }
    
    NSString *urlString = [[NSString alloc] initWithFormat:@"http://admin:9999@%@:8080/vb.htm?%@",
                           self.ipAddress,
                           [CZWisionCamera urlEncodedStringWithParameters:params]];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setTimeoutInterval:kWisionDefaultTimeout];
    [url release];
    [urlString release];
    
    void (^successBlock)(AFHTTPRequestOperation *, id) =
    ^(AFHTTPRequestOperation *operation, id responseObject) {
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *, NSError *) =
    ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to send the request to set parameters onto Wision camera: %@", [error localizedDescription]);
    };
    
    AFHTTPRequestOperation *operation;
    operation = [[[AFHTTPRequestOperation alloc] initWithRequest:request] autorelease];
    [operation setCompletionBlockWithSuccess:successBlock failure:failureBlock];
    [operation start];
}

+ (NSString *)urlEncodedStringFromObject:(id)object {
    NSString *string = [NSString stringWithFormat:@"%@", object];
    return [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)urlEncodedStringWithParameters:(NSDictionary *)params {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (id key in params) {
        id value = [params objectForKey:key];
        NSString *keyEncoded = [CZWisionCamera urlEncodedStringFromObject:key];
        
        if ([value isKindOfClass:[NSString class]]) {
            NSString *valueEncoded = [CZWisionCamera urlEncodedStringFromObject:value];
            NSString *part = [NSString stringWithFormat:@"%@=%@", keyEncoded, valueEncoded];
            [parts addObject:part];
        } else if ([value isKindOfClass:[NSNull class]]) {
            [parts addObject:keyEncoded];
        }
    }
    
    NSString *result = [parts componentsJoinedByString:@"&"];
    [parts release];
    
    return result;
}

+ (NSUInteger)nearestValidExposureTimeIndex:(CGFloat)milliseconds {
    NSInteger leftIndex = -1;
    for (NSUInteger i = 0; i < kWisionExposureValuesCount; ++i) {
        if (kWisionExposureValues[i] < milliseconds) {
            leftIndex = i;
        } else {
            break;
        }
    }
    
    if (leftIndex == -1) {
        return 0;
    } else if (leftIndex == kWisionExposureValuesCount - 1) {
        return leftIndex;
    }
    
    CGFloat leftValue = kWisionExposureValues[leftIndex];
    CGFloat rightValue = kWisionExposureValues[leftIndex + 1];
    CGFloat average = (leftValue + rightValue) / 2;
    return (milliseconds < average) ? leftIndex : leftIndex + 1;
}

+ (NSUInteger)nearestValidGainLevelIndex:(NSUInteger)gain {
    NSInteger leftIndex = -1;
    for (NSUInteger i = 0; i < kWisionGainLevelsCount; ++i) {
        if (kWisionGainLevels[i] < gain) {
            leftIndex = i;
        } else {
            break;
        }
    }
    
    if (leftIndex == -1) {
        return 0;
    } else if (leftIndex == kWisionGainLevelsCount - 1) {
        return leftIndex;
    }
    
    CGFloat leftValue = kWisionGainLevels[leftIndex];
    CGFloat rightValue = kWisionGainLevels[leftIndex + 1];
    CGFloat average = (leftValue + rightValue) / 2;
    return (gain < average) ? leftIndex : leftIndex + 1;
}

@end
