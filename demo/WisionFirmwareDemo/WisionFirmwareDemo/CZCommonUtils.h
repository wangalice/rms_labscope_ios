//
//  CZCommonUtils.h
//  Hermes
//
//  Created by Mike Wang on 2/8/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CZCommonUtils : NSObject

+ (NSString *)md5FromString:(NSString *)inputString;

@end
