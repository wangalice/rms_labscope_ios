//
//  main.m
//  WisionFirmwareDemo
//
//  Created by Halley Gu on 4/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CZAppDelegate class]));
    }
}
