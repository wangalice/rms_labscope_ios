//
//  CZAppDelegate.h
//  WisionFirmwareDemo
//
//  Created by Halley Gu on 4/24/14.
//  Copyright (c) 2014 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
