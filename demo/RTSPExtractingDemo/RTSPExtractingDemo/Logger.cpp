//
//  Logger.cpp
//  RTSPExtractingDemo
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#include "Logger.h"
#include <sys/time.h>

void Logger::log(const char *msg) {
    struct timeval time;
    gettimeofday(&time, NULL);
    long millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    
    printf("%ld | %s\n", millis, msg);
}
