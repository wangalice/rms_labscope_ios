//
//  Logger.h
//  RTSPExtractingDemo
//
//  Created by Halley Gu on 3/15/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#ifndef __RTSPExtractingDemo__Logger__
#define __RTSPExtractingDemo__Logger__

#include <iostream>

class Logger {
public:
    static void log(const char *msg);
};

#endif /* defined(__RTSPExtractingDemo__Logger__) */
