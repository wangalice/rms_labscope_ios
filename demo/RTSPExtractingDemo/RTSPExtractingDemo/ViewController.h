//
//  ViewController.h
//  RTSPExtractingDemo
//
//  Created by Halley Gu on 2/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GPUImage.h>
#import "CZRTSPClient.h"

@interface ViewController : UIViewController<CZRTSPClientDelegate, GPUImageMovieDelegate>

@property (retain, nonatomic) IBOutlet UIButton *wisionButton;
@property (retain, nonatomic) IBOutlet UIButton *kappaButton;
@property (retain, nonatomic) IBOutlet UIButton *stopButton;

- (IBAction)playButtonTouched:(id)sender;
- (IBAction)stopButtonTouched:(id)sender;

@end
