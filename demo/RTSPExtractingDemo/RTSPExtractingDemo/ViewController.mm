//
//  ViewController.m
//  RTSPExtractingDemo
//
//  Created by Halley Gu on 2/7/13.
//  Copyright (c) 2013 Carl Zeiss. All rights reserved.
//

#import "ViewController.h"
#import "CZRTSPClient.h"

@interface ViewController () {
    GPUImageView *_imageView;
}

@property (nonatomic, retain) CZRTSPClient *client;
@property (nonatomic, retain) GPUImageMovie *currentSegment;
@property (atomic, assign) BOOL isPlaying;

@end

@implementation ViewController

- (void)dealloc {
    [_client release];
    [_imageView release];
    [_currentSegment release];
    [_wisionButton release];
    [_kappaButton release];
    [_stopButton release];
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_stopButton setHidden:YES];
    self.isPlaying = NO;
}

- (void)rtspClient:(CZRTSPClient *)client didGetVideoSegment:(NSURL *)segmentURL {
    if (self.isPlaying) {
        [self.currentSegment cancelProcessing];
        
        GPUImageMovie *movie = [[GPUImageMovie alloc] initWithURL:segmentURL];
        [movie addTarget:_imageView];
        [movie setPlayAtActualSpeed:YES];
        [movie setDelegate:self];
        [movie startProcessing];
        self.currentSegment = movie;
        [movie release];
    }
}

- (void)didCompletePlayingMovie {
    
}

- (IBAction)playButtonTouched:(id)sender {
    if (!self.isPlaying) {
        self.isPlaying = YES;
        
        [_kappaButton setHidden:YES];
        [_wisionButton setHidden:YES];
        
        _imageView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
        [_imageView setFillMode:kGPUImageFillModePreserveAspectRatio];
        [self.view addSubview:_imageView];
        
        [_stopButton setHidden:NO];
        [self.view bringSubviewToFront:_stopButton];
        
        int ipSegment = 0;
        if (sender == _kappaButton) {
            ipSegment = 166;
        } else if (sender == _wisionButton) {
            ipSegment = 253;
        }
        
        NSString *urlString = [NSString stringWithFormat:@"rtsp://192.168.0.%d:8557/PSIA/Streaming/channels/2?videoCodecType=H.264", ipSegment];
        NSURL *url = [NSURL URLWithString:urlString];
        
        CZRTSPClient *client = [[CZRTSPClient alloc] initWithURL:url];
        [client setDelegate:self];
        self.client = client;
        [client release];
    }
}

- (IBAction)stopButtonTouched:(id)sender {
    if (self.isPlaying) {
        self.isPlaying = NO;
        
        [_client stop];
        
        [_stopButton setHidden:YES];
        [_kappaButton setHidden:NO];
        [_wisionButton setHidden:NO];
        
        [self.currentSegment cancelProcessing];
        self.currentSegment = nil;
        
        [_imageView removeFromSuperview];
        [_imageView release];
        _imageView = nil;
    }
}

@end
